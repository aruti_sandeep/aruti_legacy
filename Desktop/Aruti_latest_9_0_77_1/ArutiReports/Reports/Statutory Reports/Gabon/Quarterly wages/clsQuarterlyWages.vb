﻿'************************************************************************************************************************************
'Class Name : clsQuarterlyWages.vb
'Purpose    :
'Date       :01/12/2021
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System.Text
Imports System.Text.RegularExpressions

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsQuarterlyWages
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsQuarterlyWages"
    Private mstrReportId As String = enArutiReport.Gabon_Declaration_Trimestrielle_Salaires
    Private objDataOperation As clsDataOperation
    Private mdtFinal As DataTable = Nothing

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Enum "
    Public Enum enYearQuarter
        T1 = 1
        T2 = 2
        T3 = 3
        T4 = 4
    End Enum
#End Region

#Region " Private Variables "

    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintYearQuarterId As Integer = -1
    Private mstrYearQuarterName As String = String.Empty
    Private mstrPeriodIDs As String = String.Empty
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mintPeriodId1 As Integer
    Private mstrPeriodCode1 As String = String.Empty
    Private mstrPeriodName1 As String = String.Empty
    Private mintPeriodId2 As Integer
    Private mstrPeriodCode2 As String = String.Empty
    Private mstrPeriodName2 As String = String.Empty
    Private mintPeriodId3 As Integer
    Private mstrPeriodCode3 As String = String.Empty
    Private mstrPeriodCustomCode3 As String = String.Empty
    Private mstrPeriodName3 As String = String.Empty

    Private mstrMontant_deduction As String = ""
    Private mstrRemuneration_totale As String = ""
    Private mstrCotisations_nettes As String = ""
    Private mstrSalaire_Plafonne As String = ""
    Private mstrSalaire_Deplafonne As String = ""
    Private mstrNbre_HrsJrs As String = ""
    Private mdecTaux_CNSS As Decimal = 0

    Private mblnShowLogo As Boolean = True

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mstrCurrencyName As String = ""
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty


#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _YearQuarterId() As Integer
        Set(ByVal value As Integer)
            mintYearQuarterId = value
        End Set
    End Property

    Public WriteOnly Property _YearQuarterName() As String
        Set(ByVal value As String)
            mstrYearQuarterName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public Property _PeriodStart() As Date
        Get
            Return mdtPeriodStart
        End Get
        Set(ByVal value As Date)
            mdtPeriodStart = value
        End Set
    End Property

    Public Property _PeriodEnd() As Date
        Get
            Return mdtPeriodEnd
        End Get
        Set(ByVal value As Date)
            mdtPeriodEnd = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId1() As Integer
        Set(ByVal value As Integer)
            mintPeriodId1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode1() As String
        Set(ByVal value As String)
            mstrPeriodCode1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName1() As String
        Set(ByVal value As String)
            mstrPeriodName1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId2() As Integer
        Set(ByVal value As Integer)
            mintPeriodId2 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode2() As String
        Set(ByVal value As String)
            mstrPeriodCode2 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName2() As String
        Set(ByVal value As String)
            mstrPeriodName2 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId3() As Integer
        Set(ByVal value As Integer)
            mintPeriodId3 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode3() As String
        Set(ByVal value As String)
            mstrPeriodCode3 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCustomCode3() As String
        Set(ByVal value As String)
            mstrPeriodCustomCode3 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName3() As String
        Set(ByVal value As String)
            mstrPeriodName3 = value
        End Set
    End Property

    Public WriteOnly Property _Montant_deduction() As String
        Set(ByVal value As String)
            mstrMontant_deduction = value
        End Set
    End Property

    Public WriteOnly Property _Remuneration_totale() As String
        Set(ByVal value As String)
            mstrRemuneration_totale = value
        End Set
    End Property

    Public WriteOnly Property _Cotisations_nettes() As String
        Set(ByVal value As String)
            mstrCotisations_nettes = value
        End Set
    End Property

    Public WriteOnly Property _Salaire_Plafonne() As String
        Set(ByVal value As String)
            mstrSalaire_Plafonne = value
        End Set
    End Property

    Public WriteOnly Property _Salaire_Deplafonne() As String
        Set(ByVal value As String)
            mstrSalaire_Deplafonne = value
        End Set
    End Property

    Public WriteOnly Property _Nbre_HrsJrs() As String
        Set(ByVal value As String)
            mstrNbre_HrsJrs = value
        End Set
    End Property

    Public WriteOnly Property _Taux_CNSS() As Decimal
        Set(ByVal value As Decimal)
            mdecTaux_CNSS = value
        End Set
    End Property

    Public WriteOnly Property _ShowLogo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLogo = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property



#End Region

#Region " Public Function & Procedures "

    Public Function getComboListForYearQuarter(ByVal blnAddSelect As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect = True Then
                StrQ = "SELECT 0 AS Id ,@Select AS NAME UNION "
            End If

            StrQ &= "SELECT " & enYearQuarter.T1 & " AS Id, @T1 " & _
                   "UNION SELECT " & enYearQuarter.T2 & " AS Id, @T2 " & _
                   "UNION SELECT " & enYearQuarter.T3 & " AS Id, @T3 " & _
                   "UNION SELECT " & enYearQuarter.T4 & " AS Id, @T4 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select Quarter"))
            objDataOperation.AddParameter("@T1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "T1"))
            objDataOperation.AddParameter("@T2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "T2"))
            objDataOperation.AddParameter("@T3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "T3"))
            objDataOperation.AddParameter("@T4", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "T4"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboListForYearQuarter; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mintYearQuarterId = 0
            mstrYearQuarterName = String.Empty
            mstrPeriodIDs = String.Empty
            mintPeriodId1 = 0
            mstrPeriodCode1 = String.Empty
            mstrPeriodName1 = String.Empty
            mintPeriodId2 = 0
            mstrPeriodCode2 = String.Empty
            mstrPeriodName2 = String.Empty
            mintPeriodId3 = 0
            mstrPeriodCode3 = String.Empty
            mstrPeriodCustomCode3 = String.Empty
            mstrPeriodName3 = String.Empty
            mstrMontant_deduction = 0
            mstrRemuneration_totale = 0
            mstrCotisations_nettes = 0
            mstrSalaire_Plafonne = 0
            mstrSalaire_Deplafonne = 0
            mstrNbre_HrsJrs = 0
            mdecTaux_CNSS = 0
            mblnShowLogo = True
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            mstrCurrencyName = String.Empty

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = ""
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String _
                                    , ByVal intUserUnkid As Integer _
                                    , ByVal intYearUnkid As Integer _
                                    , ByVal intCompanyUnkid As Integer _
                                    , ByVal dtPeriodStart As Date _
                                    , ByVal dtPeriodEnd As Date _
                                    , ByVal strUserModeSetting As String _
                                    , ByVal blnOnlyApproved As Boolean _
                                    , ByVal dtFinancialYearStart As Date _
                                    , ByVal intBaseCurrencyId As Integer _
                                    , ByVal strFmtCurrency As String _
                                    , ByVal strExportReportPath As String _
                                    , ByVal blnOpenAfterExport As Boolean _
                                    , ByVal blnExcelHTML As Boolean _
                                    )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objMembership As New clsmembership_master
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Dim dsList As New DataSet
        Dim strBaseCurrencySign As String
        Dim colIdx As Integer = 0

        objDataOperation = New clsDataOperation
        Try
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            objMembership._Membershipunkid = mintMembershipId
            objCompany._Companyunkid = intCompanyUnkid

            Dim xUACQry, xUACFiltrQry, xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, True, False)


            Dim objHead As New clsTransactionHead
            Dim objDHead As New Dictionary(Of Decimal, String)
            Dim dsHead As DataSet = objHead.getComboList(strDatabaseName, "List", False, , , , True, , , , True, , , True)

            Dim lstPeriod As New List(Of Integer)

            If lstPeriod.Exists(Function(x) x = mintPeriodId1) = False Then
                lstPeriod.Add(mintPeriodId1)
            End If

            If lstPeriod.Exists(Function(x) x = mintPeriodId2) = False Then
                lstPeriod.Add(mintPeriodId2)
            End If

            If lstPeriod.Exists(Function(x) x = mintPeriodId3) = False Then
                lstPeriod.Add(mintPeriodId3)
            End If

            Dim strPattern As String = "([^#]*#[^#]*)#"
            Dim strReplacement As String = "$1)"

            If mstrMontant_deduction.Trim <> "" AndAlso mstrMontant_deduction.Trim.Contains("#") = True Then
                Dim strResult As String = Regex.Replace(mstrMontant_deduction, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrRemuneration_totale.Trim <> "" AndAlso mstrRemuneration_totale.Trim.Contains("#") = True Then
                Dim strResult As String = Regex.Replace(mstrRemuneration_totale, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrCotisations_nettes.Trim <> "" AndAlso mstrCotisations_nettes.Trim.Contains("#") = True Then
                Dim strResult As String = Regex.Replace(mstrCotisations_nettes, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrSalaire_Plafonne.Trim <> "" AndAlso mstrSalaire_Plafonne.Trim.Contains("#") = True Then
                Dim strResult As String = Regex.Replace(mstrSalaire_Plafonne, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrSalaire_Deplafonne.Trim <> "" AndAlso mstrSalaire_Deplafonne.Trim.Contains("#") = True Then
                Dim strResult As String = Regex.Replace(mstrSalaire_Deplafonne, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrNbre_HrsJrs.Trim <> "" AndAlso mstrNbre_HrsJrs.Trim.Contains("#") = True Then
                Dim strResult As String = Regex.Replace(mstrNbre_HrsJrs, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next

            End If

            StrQ = "SELECT hremployee_master.employeeunkid " & _
                         ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                         ", hremployee_master.employeecode " & _
                         ", ISNULL(ETERM.empl_enddate, '') AS EocDate "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') AS employeeame "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeeame "
            End If

            StrQ &= "INTO #TableEmp " & _
                    "FROM hremployee_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT TERM.TEEmpId " & _
                             ", TERM.empl_enddate " & _
                             ", TERM.termination_from_date " & _
                             ", TERM.TEfDt " & _
                             ", TERM.isexclude_payroll " & _
                             ", TERM.changereasonunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT TRM.employeeunkid AS TEEmpId " & _
                                 ", CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                                 ", CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                                 ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                                 ", TRM.isexclude_payroll " & _
                                 ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                 ", TRM.changereasonunkid " & _
                            "FROM hremployee_dates_tran AS TRM " & _
                            "WHERE isvoid = 0 " & _
                                  "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                  "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @enddate " & _
                        ") AS TERM " & _
                        "WHERE TERM.Rno = 1 " & _
                    ") AS ETERM " & _
                        "ON ETERM.TEEmpId = hremployee_master.employeeunkid " & _
                           "AND ETERM.TEfDt >= CONVERT(CHAR(8), appointeddate, 112) "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If

            StrQ &= "SELECT A.employeeunkid " & _
                         ", #TableEmp.employeecode " & _
                         ", #TableEmp.employeeame " & _
                         ", #TableEmp.appointeddate " & _
                         ", #TableEmp.EocDate " & _
                         ", Max(A.membershipno) AS membershipno"

            For Each lstitem In lstPeriod
                For Each itm In objDHead
                    StrQ &= " , SUM([" & itm.Key & "_" & lstitem & "]) AS [" & itm.Key & "_" & lstitem & "] "
                Next
            Next


            StrQ &= "FROM " & _
                    "( " & _
                             "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", '' AS membershipno "

            For Each lstitem In lstPeriod
                For Each itm In objDHead
                    StrQ &= " , 0 AS [" & itm.Key & "_" & lstitem & "] "
                Next
            Next

            StrQ &= "FROM prpayrollprocess_tran " & _
                         " JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                         " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                         " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                         " WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                         " AND prtranhead_master.isvoid = 0 AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId1 & ") " & _
                         " GROUP BY prpayrollprocess_tran.employeeunkid " & _
                         ", prtnaleave_tran.payperiodunkid " & _
                         " UNION ALL " & _
                         " SELECT prpayrollprocess_tran.employeeunkid " & _
                         ", prtnaleave_tran.payperiodunkid " & _
                         ", '' AS membershipno "


            For Each lstitem In lstPeriod
                For Each itm In objDHead
                    StrQ &= " , 0 AS [" & itm.Key & "_" & lstitem & "] "
                Next
            Next

            StrQ &= "FROM prpayrollprocess_tran " & _
                         " JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                         " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                         " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                         " WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                         " AND prtranhead_master.isvoid = 0 AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId2 & ") " & _
                         " GROUP BY prpayrollprocess_tran.employeeunkid " & _
                         ", prtnaleave_tran.payperiodunkid " & _
                         " UNION ALL " & _
                         " SELECT prpayrollprocess_tran.employeeunkid " & _
                         ", prtnaleave_tran.payperiodunkid " & _
                         ", '' AS membershipno "


            For Each lstitem In lstPeriod
                For Each itm In objDHead
                    StrQ &= " , 0 AS [" & itm.Key & "_" & lstitem & "] "
                Next
            Next

            StrQ &= " FROM prpayrollprocess_tran " & _
                         " JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                         " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                         " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                         " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                         " AND prtnaleave_tran.isvoid = 0 " & _
                         " AND prtranhead_master.isvoid = 0 " & _
                         " AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId3 & ") " & _
                         " GROUP BY prpayrollprocess_tran.employeeunkid " & _
                         ", prtnaleave_tran.payperiodunkid " & _
                         " UNION ALL " & _
                         " SELECT #TableEmp.employeeunkid " & _
                         "," & mintPeriodId1 & " AS payperiodunkid  " & _
                         ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS membershipno "


            For Each lstitem In lstPeriod
                For Each itm In objDHead
                    StrQ &= " , 0 AS [" & itm.Key & "_" & lstitem & "] "
                Next
            Next

            StrQ &= " FROM #TableEmp " & _
                         " LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
                         " AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                         " LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                         " WHERE hrmembership_master.membershipunkid = @MemId "



            For Each lstItem In lstPeriod

                For Each itm In objDHead

                    StrQ &= "UNION ALL " & _
                                 " SELECT prpayrollprocess_tran.employeeunkid " & _
                                 ", prtnaleave_tran.payperiodunkid " & _
                                 ", '' AS membershipno "

                    For Each lstItem1 In lstPeriod

                        For Each itm1 In objDHead

                            If itm.Key = itm1.Key AndAlso lstItem = lstItem1 Then
                                StrQ &= " , SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS [" & itm1.Key & "_" & lstItem1 & "] "
                    Else
                                StrQ &= " , 0 AS [" & itm1.Key & "_" & lstItem1 & "] "
                    End If

                        Next

                    Next

                    StrQ &= " FROM #TableEmp " & _
                        " LEFT JOIN prpayrollprocess_tran ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                        " LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                        " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        " AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                        " WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                    " AND prtranhead_master.tranheadunkid = " & itm.Key & " "


                    If lstItem = mintPeriodId1 Then
                        StrQ &= " AND prtnaleave_tran.payperiodunkid IN (" & lstItem & ") "
                    ElseIf lstItem = mintPeriodId2 Then
                        StrQ &= " AND prtnaleave_tran.payperiodunkid IN (" & lstItem & ") "
                    ElseIf lstItem = mintPeriodId3 Then
                        StrQ &= " AND prtnaleave_tran.payperiodunkid IN (" & lstItem & ") "
                    End If

                    StrQ &= " GROUP BY prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid "

                Next


            Next

            StrQ &= " ) AS A " & _
                         " JOIN #TableEmp ON A.employeeunkid = #TableEmp.employeeunkid " & _
                        " GROUP BY A.employeeunkid " & _
                        ", #TableEmp.employeecode " & _
                        ", #TableEmp.employeeame " & _
                        ", #TableEmp.appointeddate " & _
                        ", #TableEmp.EocDate "

            StrQ &= " DROP TABLE #TableEmp "


            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEnd))
            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then

                Dim dc As DataColumn = Nothing

                If dsList.Tables(0).Columns.Contains("Taux_CNAMGS") = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Taux_CNAMGS"
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Taux CNSS(c) Taux CNAMGS"
                    dc.DefaultValue = mdecTaux_CNSS
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Salaire_Plafonne_" & mintPeriodId1) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Salaire_Plafonne_" & mintPeriodId1
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Salaire Plafonne"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Salaire_Deplafonne_" & mintPeriodId1) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Salaire_Deplafonne_" & mintPeriodId1
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Salaire Deplafonne"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Nbre_Hrs/Jrs_" & mintPeriodId1) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Nbre_Hrs/Jrs_" & mintPeriodId1
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Nbre Hrs/Jrs"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Salaire_Plafonne_" & mintPeriodId2) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Salaire_Plafonne_" & mintPeriodId2
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Salaire Plafonne"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Salaire_Deplafonne_" & mintPeriodId2) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Salaire_Deplafonne_" & mintPeriodId2
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Salaire Deplafonne"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Nbre_Hrs/Jrs_" & mintPeriodId2) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Nbre_Hrs/Jrs_" & mintPeriodId2
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Nbre Hrs/Jrs"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Salaire_Plafonne_" & mintPeriodId3) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Salaire_Plafonne_" & mintPeriodId3
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Salaire Plafonne"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Salaire_Deplafonne_" & mintPeriodId3) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Salaire_Deplafonne_" & mintPeriodId3
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Salaire Deplafonne"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Columns.Contains("Nbre_Hrs/Jrs_" & mintPeriodId3) = False Then
                    dc = New DataColumn()
                    dc.ColumnName = "Nbre_Hrs/Jrs_" & mintPeriodId3
                    dc.DataType = Type.GetType("System.Decimal")
                    dc.Caption = "Nbre Hrs/Jrs"
                    dc.DefaultValue = 0
                    If blnExcelHTML = False Then
                        dc.ExtendedProperties.Add("style", "n8wc")
                    Else
                        dc.ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
                    End If
                    dsList.Tables(0).Columns.Add(dc)
                End If
                dc = Nothing
            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dsRow.Item("appointeddate") = Format(eZeeDate.convertDate(dsRow.Item("appointeddate").ToString), "dd-MM-yyyy")
                If dsRow.Item("EocDate").ToString.Trim <> "" Then
                    dsRow.Item("EocDate") = Format(eZeeDate.convertDate(dsRow.Item("EocDate").ToString), "dd-MM-yyyy")
                End If

                If mstrSalaire_Plafonne.Trim <> "" AndAlso mstrSalaire_Plafonne.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrSalaire_Plafonne.Trim
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId1)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Salaire_Plafonne_" & mintPeriodId1) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                    s = mstrSalaire_Plafonne.Trim
                    ans = Nothing
                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId2)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Salaire_Plafonne_" & mintPeriodId2) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                    s = mstrSalaire_Plafonne.Trim
                    ans = Nothing
                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId3)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Salaire_Plafonne_" & mintPeriodId3) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                Else
                    dsRow("Salaire_Plafonne_" & mintPeriodId1) = Format(CDec(mstrSalaire_Plafonne), strFmtCurrency.Replace(",", ""))
                    dsRow("Salaire_Plafonne_" & mintPeriodId2) = Format(CDec(mstrSalaire_Plafonne), strFmtCurrency.Replace(",", ""))
                    dsRow("Salaire_Plafonne_" & mintPeriodId3) = Format(CDec(mstrSalaire_Plafonne), strFmtCurrency.Replace(",", ""))
                End If

                If mstrSalaire_Deplafonne.Trim <> "" AndAlso mstrSalaire_Deplafonne.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrSalaire_Deplafonne.Trim
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId1)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Salaire_Deplafonne_" & mintPeriodId1) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                    s = mstrSalaire_Deplafonne.Trim
                    ans = Nothing
                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId2)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Salaire_Deplafonne_" & mintPeriodId2) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                    s = mstrSalaire_Deplafonne.Trim
                    ans = Nothing
                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId3)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Salaire_Deplafonne_" & mintPeriodId3) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                Else
                    dsRow("Salaire_Deplafonne_" & mintPeriodId1) = Format(CDec(mstrSalaire_Deplafonne), strFmtCurrency.Replace(",", ""))
                    dsRow("Salaire_Deplafonne_" & mintPeriodId2) = Format(CDec(mstrSalaire_Deplafonne), strFmtCurrency.Replace(",", ""))
                    dsRow("Salaire_Deplafonne_" & mintPeriodId3) = Format(CDec(mstrSalaire_Deplafonne), strFmtCurrency.Replace(",", ""))
                End If

                If mstrNbre_HrsJrs.Trim <> "" AndAlso mstrNbre_HrsJrs.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrNbre_HrsJrs.Trim
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId1)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Nbre_Hrs/Jrs_" & mintPeriodId1) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                    s = mstrNbre_HrsJrs.Trim
                    ans = Nothing
                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId2)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Nbre_Hrs/Jrs_" & mintPeriodId2) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                    s = mstrNbre_HrsJrs.Trim
                    ans = Nothing
                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key & "_" & mintPeriodId3)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow("Nbre_Hrs/Jrs_" & mintPeriodId3) = Format(CDec(ans), strFmtCurrency.Replace(",", ""))

                Else
                    dsRow("Nbre_Hrs/Jrs_" & mintPeriodId1) = Format(CDec(mstrNbre_HrsJrs), strFmtCurrency.Replace(",", ""))
                    dsRow("Nbre_Hrs/Jrs_" & mintPeriodId2) = Format(CDec(mstrNbre_HrsJrs), strFmtCurrency.Replace(",", ""))
                    dsRow("Nbre_Hrs/Jrs_" & mintPeriodId3) = Format(CDec(mstrNbre_HrsJrs), strFmtCurrency.Replace(",", ""))
                End If


            Next

            Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Decimal)("Salaire_Plafonne_" & mintPeriodId1) <= 0 And x.Field(Of Decimal)("Salaire_Deplafonne_" & mintPeriodId1) <= 0 And x.Field(Of Decimal)("Nbre_Hrs/Jrs_" & mintPeriodId1) <= 0 _
                                                                                                   And x.Field(Of Decimal)("Salaire_Plafonne_" & mintPeriodId2) <= 0 And x.Field(Of Decimal)("Salaire_Deplafonne_" & mintPeriodId2) <= 0 And x.Field(Of Decimal)("Nbre_Hrs/Jrs_" & mintPeriodId2) <= 0 _
                                                                                                   And x.Field(Of Decimal)("Salaire_Plafonne_" & mintPeriodId3) <= 0 And x.Field(Of Decimal)("Salaire_Deplafonne_" & mintPeriodId3) <= 0 And x.Field(Of Decimal)("Nbre_Hrs/Jrs_" & mintPeriodId3) <= 0).ToList()

            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                For i As Integer = 0 To drRow.Count - 1
                    dsList.Tables(0).Rows.Remove(drRow(i))
                Next
            End If

            Dim dc1 As DataColumn = Nothing
            If dsList.Tables(0).Columns.Contains("ROWNO") = False Then
                dc1 = New DataColumn()
                dc1.ColumnName = "ROWNO"
                dc1.DataType = Type.GetType("System.Int32")
                dc1.Caption = "ROWNO"
                dc1.AutoIncrement = True
                dc1.AutoIncrementSeed = 1
                dc1.AutoIncrementStep = 1
                dsList.Tables(0).Columns.Add(dc1)
            End If
            dc1 = Nothing

            Dim xCount As Integer = 0
            For Each dr As DataRow In dsList.Tables(0).Rows
                xCount += 1
                dr("ROWNO") = xCount
            Next


            dsList.Tables(0).AcceptChanges()

            Dim decMontant_deduction As Decimal = 0
            If mstrMontant_deduction.Trim <> "" AndAlso mstrMontant_deduction.Contains("#") = True Then
                Dim dt As New DataTable
                Dim s As String = mstrMontant_deduction
                Dim ans As Object = Nothing

                For Each itm In objDHead
                    Dim mdecValue As Decimal = 0
                    For Each lstitem In lstPeriod
                        Dim strHeadid As String = itm.Key & "_" & lstitem
                        mdecValue += (From p In dsList.Tables(0) Select CDec((p.Item(strHeadid)))).Sum
                    Next
                    s = s.Replace("#" & itm.Value & "#", Format(mdecValue, strFmtCurrency))
                Next
                Try
                    ans = dt.Compute(s.ToString.Replace(",", ""), "")
                Catch ex As Exception

                End Try
                If ans Is Nothing Then
                    ans = 0
                End If
                decMontant_deduction = Format(CDec(ans), strFmtCurrency).Replace(",", "")
            Else
                Decimal.TryParse(mstrMontant_deduction, decMontant_deduction)
            End If


            Dim decRemuneration_totale As Decimal = 0
            If mstrRemuneration_totale.Trim <> "" AndAlso mstrRemuneration_totale.Contains("#") = True Then
                Dim dt As New DataTable
                Dim s As String = mstrRemuneration_totale
                Dim ans As Object = Nothing

                For Each itm In objDHead
                    Dim mdecValue As Decimal = 0
                    For Each lstitem In lstPeriod
                        Dim strHeadid As String = itm.Key & "_" & lstitem
                        mdecValue += (From p In dsList.Tables(0) Select CDec((p.Item(strHeadid)))).Sum
                    Next
                    s = s.Replace("#" & itm.Value & "#", Format(mdecValue, strFmtCurrency))
                Next
                Try
                    ans = dt.Compute(s.ToString.Replace(",", ""), "")
                Catch ex As Exception

                End Try
                If ans Is Nothing Then
                    ans = 0
                End If
                decRemuneration_totale = Format(CDec(ans), strFmtCurrency).Replace(",", "")
            Else
                Decimal.TryParse(mstrRemuneration_totale, decRemuneration_totale)
            End If


            Dim decCotisations_nettes As Decimal = 0
            If mstrCotisations_nettes.Trim <> "" AndAlso mstrCotisations_nettes.Contains("#") = True Then
                Dim dt As New DataTable
                Dim s As String = mstrCotisations_nettes
                Dim ans As Object = Nothing

                For Each itm In objDHead
                    Dim mdecValue As Decimal = 0
                    For Each lstitem In lstPeriod
                        Dim strHeadid As String = itm.Key & "_" & lstitem
                        mdecValue += (From p In dsList.Tables(0) Select CDec((p.Item(strHeadid)))).Sum
                    Next
                    s = s.Replace("#" & itm.Value & "#", Format(mdecValue, strFmtCurrency))
                Next
                Try
                    ans = dt.Compute(s.ToString.Replace(",", ""), "")
                Catch ex As Exception

                End Try
                If ans Is Nothing Then
                    ans = 0
                End If
                decCotisations_nettes = Format(CDec(ans), strFmtCurrency).Replace(",", "")
            Else
                Decimal.TryParse(mstrCotisations_nettes, decCotisations_nettes)
            End If


            Dim mdtTableExcel As DataTable
            Dim intColIndex As Integer = -1
            mdtTableExcel = dsList.Tables(0)

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            intColIndex += 1
            mdtTableExcel.Columns("ROWNO").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 6, "N°")

            intColIndex += 1
            mdtTableExcel.Columns("MembershipNo").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 7, "N°CNSS(a)/ N°CNAMGS")

            intColIndex += 1
            mdtTableExcel.Columns("employeecode").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 1050, "N° Paie")

            intColIndex += 1
            mdtTableExcel.Columns("employeeame").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 1051, "NOM ET PRENOM")

            intColIndex += 1
            mdtTableExcel.Columns("Taux_CNAMGS").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 1052, "Taux CNSS( c )  Taux CNAMGS %")

            intColIndex += 1
            mdtTableExcel.Columns("appointeddate").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 1053, "EMBAUCHE  (d)")

            intColIndex += 1
            mdtTableExcel.Columns("EocDate").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 1054, "CESSATION ( e )")


            For Each lstItem In lstPeriod
                For Each itm1 In objDHead
                    If mdtTableExcel.Columns.Contains(itm1.Key & "_" & lstItem) Then
                        mdtTableExcel.Columns.Remove(itm1.Key & "_" & lstItem)
                    End If
                Next
            Next
            mdtTableExcel.AcceptChanges()

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing
            Dim strBuilder As New StringBuilder
            Dim arrWorkSheetStyle As List(Of WorksheetStyle) = Nothing
            Dim strWorkSheetStyle As String = ""
            Dim intInitRows As Integer = 0

            If blnExcelHTML = False Then
                arrWorkSheetStyle = New List(Of WorksheetStyle)
                Dim s10bcw As New WorksheetStyle("s10bcw")
                With s10bcw
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcw)

                Dim s10bcblue As New WorksheetStyle("s10bcblue")
                With s10bcblue
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Font.Color = "#5877FC"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcblue)

                Dim s10bred As New WorksheetStyle("s10bred")
                With s10bred
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.WrapText = True
                    .Font.Color = "#FF0000"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bred)

                Dim n8wc As New WorksheetStyle("n8wc")
                With n8wc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8wc)

                Dim n8bwc As New WorksheetStyle("n8bwc")
                With n8bwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bwc)

                Dim n8bcwc As New WorksheetStyle("n8bcwc")
                With n8bcwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bcwc)


                Dim s8Footer As New WorksheetStyle("s8Footer")
                With s8Footer
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.WrapText = True
                    .Interior.Color = "Silver"
                    .Interior.Pattern = StyleInteriorPattern.Solid
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s8Footer)

                Dim s8Footerlb As New WorksheetStyle("s8Footerlb")
                With s8Footerlb
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.WrapText = True
                    .Interior.Color = "Silver"
                    .Interior.Pattern = StyleInteriorPattern.Solid
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s8Footerlb)

                Dim s8Footerrb As New WorksheetStyle("s8Footerrb")
                With s8Footerrb
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.WrapText = True
                    .Interior.Color = "Silver"
                    .Interior.Pattern = StyleInteriorPattern.Solid
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(s8Footerrb)


                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "DECLARATION TRIMESTRIELLE DE SALAIRES"), "s10bc")
                wcell.MergeAcross = 3
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)


                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)


                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1001, "Matricule employeur"), "s8bc")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1002, "Période"), "s8bc")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1003, "Année"), "s8bc")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1004, "Régime"), "s8bc")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1005, "Sigle"), "s8bc")
                row.Cells.Add(wcell)

                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(objMembership._EmployerMemNo, "s8bc")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrYearQuarterName, "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(dtFinancialYearStart.Year.ToString(), "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1006, "Général"), "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1007, "NETIS SUARL"), "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1008, "EMPLOYEUR"), "s8bc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1009, "Nom ou Raison Sociale"), "s8w")
                wcell.MergeAcross = 8
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1010, "CACHET ET SIGNATURE"), "s8bc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(objCompany._Registerdno, "s8b")
                wcell.MergeAcross = 8
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                wcell.MergeAcross = 2
                wcell.MergeDown = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1011, "B.P :"), "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(objCompany._Address2, "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1012, "VILLE :"), "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(objCompany._City_Name, "s8bc")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1013, "TEL :"), "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(objCompany._Phone2, "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1014, "Fax :"), "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(objCompany._Fax, "s8bc")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 7
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1015, "Effectif total"), "s8bc")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 7
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mdtTableExcel.Rows.Count.ToString(), "s8bc")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 10
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1016, "CADRE RESERVE A LA CNSS"), "s8bc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1017, "Rémuneration totale plafonnée CNSS"), "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1018, "Montant déduction Allocations Familiales"), "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1019, "Rémunération totale plafonnée CNAMGS"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1020, "DATE DE RECEPTION"), "s8bc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                intColIndex = 7

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell() '<---->
                wcell.StyleID = "n8bcwc"
                wcell.Data.Type = DataType.Number
                Dim str As String = "=SUM(R" & (rowsArrayHeader.Count + intInitRows + mdtTableExcel.Rows.Count + 11) & "C" & intColIndex + 1 & ",R" & (rowsArrayHeader.Count + intInitRows + mdtTableExcel.Rows.Count + 11) & "C" & intColIndex + 4 & ",R" & (rowsArrayHeader.Count + intInitRows + mdtTableExcel.Rows.Count + 11) & "C" & intColIndex + 7 & ")"
                wcell.Formula = str
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(decMontant_deduction.ToString(), "n8bcwc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(decRemuneration_totale.ToString(), "n8bcwc")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                wcell.MergeAcross = 2
                wcell.MergeDown = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1021, "Cotisation brutes dues CNSS"), "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1022, " Cotisations nettes dues CNSS"), "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1023, " Cotisations nettes dues CNAMGS"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)


                Dim xRCount As Integer = rowsArrayHeader.Count + intInitRows

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell() '<--->
                wcell.StyleID = "n8bcwc"
                wcell.Data.Type = DataType.Number
                str = "=(R" & (xRCount - 1) & "C" & intColIndex - 5 & "*" & mdecTaux_CNSS & "/100)"
                wcell.Formula = str
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)

                wcell = New WorksheetCell() '<--->
                wcell.StyleID = "n8bcwc"
                wcell.Data.Type = DataType.Number
                str = "=(R" & (xRCount + 1) & "C" & intColIndex - 5 & " - R" & (xRCount - 1) & "C" & intColIndex - 2 & ")"
                wcell.Formula = str
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(decCotisations_nettes.ToString(), "n8bcwc")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)


                row = New WorksheetRow()
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1024, "Date"), "s8c")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrPeriodCode1, "s8c")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrPeriodCode2, "s8c")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrPeriodCode3, "s8c")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                Dim xRowCount As Integer = rowsArrayHeader.Count + intInitRows


                row = New WorksheetRow()
                For Each col As DataColumn In mdtTableExcel.Columns
                    wcell = New WorksheetCell(col.Caption, "s8c")
                    row.Cells.Add(wcell)
                Next
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1025, "SOUS TOTAL A REPORTER PAGE SUIVANTE"), "s8Footer")
                wcell.MergeAcross = 5
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8Footerlb")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8Footerlb")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8Footerlb")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1026, "RECAP"), "s8bc")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1027, "CNSS"), "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1028, "TAUX"), "s8bc")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mdecTaux_CNSS.ToString(), "n8bwc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1029, "MASSE SALARIALE PLAFONNEE CNSS"), "s8bc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)

                wcell = New WorksheetCell()  '<-->
                wcell.StyleID = "n8bwc"
                wcell.Data.Type = DataType.Number
                str = "=SUM(R" & (xRowCount + 2) & "C" & intColIndex + 1 & ":R" & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1) & "C" & intColIndex + 1 & ")"
                wcell.Formula = str
                row.Cells.Add(wcell)

                wcell = New WorksheetCell()  '<-->
                wcell.StyleID = "n8bwc"
                wcell.Data.Type = DataType.Number
                str = "=SUM(R" & (xRowCount + 2) & "C" & intColIndex + 2 & ":R" & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1) & "C" & intColIndex + 2 & ")"
                wcell.Formula = str
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell()  '<-->
                wcell.StyleID = "n8bwc"
                wcell.Data.Type = DataType.Number
                str = "=SUM(R" & (xRowCount + 2) & "C" & intColIndex + 4 & ":R" & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1) & "C" & intColIndex + 4 & ")"
                wcell.Formula = str
                row.Cells.Add(wcell)

                wcell = New WorksheetCell()  '<-->
                wcell.StyleID = "n8bwc"
                wcell.Data.Type = DataType.Number
                str = "=SUM(R" & (xRowCount + 2) & "C" & intColIndex + 5 & ":R" & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1) & "C" & intColIndex + 5 & ")"
                wcell.Formula = str
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell()  '<-->
                wcell.StyleID = "n8bwc"
                wcell.Data.Type = DataType.Number
                str = "=SUM(R" & (xRowCount + 2) & "C" & intColIndex + 7 & ":R" & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1) & "C" & intColIndex + 7 & ")"
                wcell.Formula = str
                row.Cells.Add(wcell)

                wcell = New WorksheetCell()  '<-->
                wcell.StyleID = "n8bwc"
                wcell.Data.Type = DataType.Number
                str = "=SUM(R" & (xRowCount + 2) & "C" & intColIndex + 8 & ":R" & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1) & "C" & intColIndex + 8 & ")"
                wcell.Formula = str
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8b")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1029, "COTISATION GLOBALE DUE (CNSS)"), "s8Footerlb")
                wcell.MergeAcross = 13
                row.Cells.Add(wcell)
                wcell = New WorksheetCell()  '<-->
                wcell.StyleID = "s8Footerrb"
                wcell.Data.Type = DataType.Number
                str = "=(R" & (xRCount + 1) & "C" & intColIndex - 2 & "+" & "R" & (xRCount + 1) & "C" & intColIndex + 2 & ")"
                wcell.Formula = str
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

            Else
                strWorkSheetStyle &= ".s8c {font-size:10px;font-family:Tahoma; text-align:center;} "
                strWorkSheetStyle &= ".s10bcw {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center; } "
                strWorkSheetStyle &= ".s10bcblue {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center; color:#5877FC} "
                strWorkSheetStyle &= ".s10bred {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center; color:#FF0000} "


                strWorkSheetStyle &= ".n8wc {font-size:10px; font-family:Tahoma; mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34) & ";} "
                strWorkSheetStyle &= ".n8bwc {font-size:10px; font-weight:bold; font-family:Tahoma; mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34) & ";} "
                strWorkSheetStyle &= ".n8bcwc {font-size:10px; font-weight:bold; font-family:Tahoma; text-align:right; mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34) & ";} "

                intInitRows = 3

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)

                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\DeSecurity_Logo.jpg"
                If IO.Directory.Exists(objAppSett._ApplicationPath & "Data\Images") = False Then
                    IO.Directory.CreateDirectory(objAppSett._ApplicationPath & "Data\Images")
                End If
                'objAppSett = Nothing
                'strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                'If objCompany._Image IsNot Nothing Then
                '    objCompany._Image.Save(strImPath)
                'End If
                'strBuilder.Append(" <img SRC='" & strImPath & "' ALT='' HEIGHT = '100' WIDTH ='100' />" & vbCrLf)
                'strBuilder.Append(" </TD> " & vbCrLf)


                Dim img As Image = My.Resources.DeSecurity_Logo
                img.Save(strImPath)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" <img SRC='" & strImPath & "' ALT='' HEIGHT = '80' WIDTH ='80' />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)

                'Dim ImgByte As Byte() = ms.ToArray()
                'Dim mstrBase64Img = Convert.ToBase64String(ImgByte)
                'strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                'strBuilder.Append(" <img SRC='data:image/png;base64," & mstrBase64Img & "' ALT='' HEIGHT = '80' WIDTH ='80' />" & vbCrLf)
                'strBuilder.Append(" </TD> " & vbCrLf)

                img = Nothing
                strImPath = objAppSett._ApplicationPath & "Data\Images\DeSecurity.jpg"
                'ms = Nothing
                'ImgByte = Nothing
                'mstrBase64Img = ""

                strBuilder.Append("<TD class='s8w'> </TD>" & vbCrLf)

                img = My.Resources.DeSecurity
                'ms = New IO.MemoryStream
                img.Save(strImPath)
                'ImgByte = ms.ToArray()
                'mstrBase64Img = Convert.ToBase64String(ImgByte)

                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                strBuilder.Append(" <img SRC='" & strImPath & "' ALT='' HEIGHT = '80' WIDTH ='175' />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)

                'strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                'strBuilder.Append(" <img SRC='data:image/png;base64," & mstrBase64Img & "' ALT='' HEIGHT = '80' WIDTH ='175' />" & vbCrLf)
                'strBuilder.Append(" </TD> " & vbCrLf)

                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                For j As Integer = 0 To 6
                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)
                    If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)
                Next

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' rowspan='2' class='s10bcw'>" & Language.getMessage(mstrModuleName, 18, "DECLARATION TRIMESTRIELLE DE SALAIRES") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8bc'>" & Language.getMessage(mstrModuleName, 1001, "Matricule employeur") & " </TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>&nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & Language.getMessage(mstrModuleName, 1002, "Période") & " </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & Language.getMessage(mstrModuleName, 1003, "Année") & " </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & Language.getMessage(mstrModuleName, 1004, "Régime") & " </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp; &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & Language.getMessage(mstrModuleName, 1005, "Sigle") & " </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8bc'>" & objMembership._EmployerMemNo & " </TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>&nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & mstrYearQuarterName & " </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & dtFinancialYearStart.Year & " </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & Language.getMessage(mstrModuleName, 1006, "Général") & " </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'></TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & Language.getMessage(mstrModuleName, 1007, "NETIS SUARL") & " </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8bc'>" & Language.getMessage(mstrModuleName, 1008, "EMPLOYEUR") & " </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='9' class='s8w'>" & Language.getMessage(mstrModuleName, 1009, "Nom ou Raison Sociale") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8bc'>" & Language.getMessage(mstrModuleName, 1010, "CACHET ET SIGNATURE") & " </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='9' class='s8b'>" & objCompany._Registerdno & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' rowspan='5' class='s8bc'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='12' class='s10bcw'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>" & Language.getMessage(mstrModuleName, 1011, "B.P :") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & objCompany._Address2 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>" & Language.getMessage(mstrModuleName, 1012, "VILLE :") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & objCompany._City_Name & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='12' class='s10bcw'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>" & Language.getMessage(mstrModuleName, 1013, "TEL :") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & objCompany._Phone2 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>" & Language.getMessage(mstrModuleName, 1014, "Fax :") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & objCompany._Fax & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='8' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>" & Language.getMessage(mstrModuleName, 1015, "Effectif total") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='8' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & mdtTableExcel.Rows.Count & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='11' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8bc'>" & Language.getMessage(mstrModuleName, 1016, "CADRE RESERVE A LA CNSS") & " </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'>" & Language.getMessage(mstrModuleName, 1017, "Rémuneration totale plafonnée CNSS") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'>" & Language.getMessage(mstrModuleName, 1018, "Montant déduction Allocations Familiales") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>" & Language.getMessage(mstrModuleName, 1019, "Rémunération totale plafonnée CNAMGS") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8bc'>" & Language.getMessage(mstrModuleName, 1020, "DATE DE RECEPTION") & " </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                intColIndex = 7

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='n8bcwc'>=SUM(" & ConvertToExcelColumnLetter(intColIndex + 1) & (rowsArrayHeader.Count + intInitRows + mdtTableExcel.Rows.Count + 11).ToString() & "," & ConvertToExcelColumnLetter(intColIndex + 4) & (rowsArrayHeader.Count + intInitRows + mdtTableExcel.Rows.Count + 11).ToString() & "," & ConvertToExcelColumnLetter(intColIndex + 7) & (rowsArrayHeader.Count + intInitRows + mdtTableExcel.Rows.Count + 11).ToString() & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='n8bcwc'>" & decMontant_deduction & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='n8bcwc'>" & decRemuneration_totale & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' rowspan='5'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'>" & Language.getMessage(mstrModuleName, 1021, "Cotisation brutes dues CNSS") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'>" & Language.getMessage(mstrModuleName, 1022, " Cotisations nettes dues CNSS") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>" & Language.getMessage(mstrModuleName, 1023, " Cotisations nettes dues CNAMGS") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                Dim xRCount As Integer = rowsArrayHeader.Count + intInitRows

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='n8bcwc'>=" & ConvertToExcelColumnLetter(intColIndex - 5) & (xRCount - 1).ToString() & "*" & mdecTaux_CNSS & " / 100</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='n8bcwc'>=" & ConvertToExcelColumnLetter(intColIndex - 5) & (xRCount + 1).ToString() & "-" & ConvertToExcelColumnLetter(intColIndex - 2) & (xRCount - 1).ToString() & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='n8bcwc'>" & decCotisations_nettes & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8c'>" & Language.getMessage(mstrModuleName, 1024, "Date") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8c'>" & mstrPeriodCode1 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8c'>" & mstrPeriodCode2 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8c'>" & mstrPeriodCode3 & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                Dim xRowCount As Integer = rowsArrayHeader.Count + intInitRows

                strBuilder.Append("<TR>" & vbCrLf)
                For Each col As DataColumn In mdtTableExcel.Columns
                    strBuilder.Append("<TD class='s8'>" & col.Caption & "</TD>" & vbCrLf)
                Next
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Length = 0
                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='6' class='s8b' style='background-color:#C8C8C8'>" & Language.getMessage(mstrModuleName, 1025, "SOUS TOTAL A REPORTER PAGE SUIVANTE") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8bc' style='background-color:#C8C8C8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc' style='background-color:#C8C8C8'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc' style='background-color:#C8C8C8'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)



                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc' rowspan='2'>" & Language.getMessage(mstrModuleName, 1026, "RECAP") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'>" & Language.getMessage(mstrModuleName, 1027, "CNSS") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc' rowspan='2'>" & Language.getMessage(mstrModuleName, 1028, "TAUX") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bcwc'>" & mdecTaux_CNSS & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan ='3' class='s8bc'>" & Language.getMessage(mstrModuleName, 1029, "MASSE SALARIALE PLAFONNEE CNSS") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bcwc'>=SUM(" & ConvertToExcelColumnLetter(intColIndex + 1) & (xRowCount + 2).ToString & ":" & ConvertToExcelColumnLetter(intColIndex + 1) & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1).ToString & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bcwc'>=SUM(" & ConvertToExcelColumnLetter(intColIndex + 2) & (xRowCount + 2).ToString & ":" & ConvertToExcelColumnLetter(intColIndex + 2) & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1).ToString & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bcwc'>=SUM(" & ConvertToExcelColumnLetter(intColIndex + 4) & (xRowCount + 2).ToString & ":" & ConvertToExcelColumnLetter(intColIndex + 4) & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1).ToString & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bcwc'>=SUM(" & ConvertToExcelColumnLetter(intColIndex + 5) & (xRowCount + 2).ToString & ":" & ConvertToExcelColumnLetter(intColIndex + 5) & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1).ToString & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bcwc'>=SUM(" & ConvertToExcelColumnLetter(intColIndex + 7) & (xRowCount + 2).ToString & ":" & ConvertToExcelColumnLetter(intColIndex + 7) & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1).ToString & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bcwc'>=SUM(" & ConvertToExcelColumnLetter(intColIndex + 8) & (xRowCount + 2).ToString & ":" & ConvertToExcelColumnLetter(intColIndex + 8) & (xRowCount + 2 + mdtTableExcel.Rows.Count - 1).ToString & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan ='3' class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8bc'> &nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan = '14' class='s8b' style='background-color:#C8C8C8'>" & Language.getMessage(mstrModuleName, 1029, "COTISATION GLOBALE DUE (CNSS)") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan = '2' class='n8bcwc' style='background-color:#C8C8C8'>=" & ConvertToExcelColumnLetter(intColIndex - 2) & (xRCount + 1).ToString() & "+" & ConvertToExcelColumnLetter(intColIndex + 2) & (xRCount + 1).ToString() & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

            End If


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                If ii = 0 Then
                    intArrayColumnWidth(ii) = 50
                ElseIf ii = 2 Then
                    intArrayColumnWidth(ii) = 140
                Else
                    intArrayColumnWidth(ii) = 100
                End If
            Next

            If blnExcelHTML = False Then
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, marrWorksheetStyle:=arrWorkSheetStyle.ToArray)
            Else
                Call ReportExecute(intCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, strExportReportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, mstrWorksheetStyle:=strWorkSheetStyle)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select Quarter")
            Language.setMessage(mstrModuleName, 2, "T1")
            Language.setMessage(mstrModuleName, 3, "T2")
            Language.setMessage(mstrModuleName, 4, "T3")
            Language.setMessage(mstrModuleName, 5, "T4")
            Language.setMessage(mstrModuleName, 6, "N°")
            Language.setMessage(mstrModuleName, 7, "N°CNSS(a)/ N°CNAMGS")
            Language.setMessage(mstrModuleName, 18, "DECLARATION TRIMESTRIELLE DE SALAIRES")
            Language.setMessage(mstrModuleName, 1001, "Matricule employeur")
            Language.setMessage(mstrModuleName, 1002, "Période")
            Language.setMessage(mstrModuleName, 1003, "Année")
            Language.setMessage(mstrModuleName, 1004, "Régime")
            Language.setMessage(mstrModuleName, 1005, "Sigle")
            Language.setMessage(mstrModuleName, 1006, "Général")
            Language.setMessage(mstrModuleName, 1007, "NETIS SUARL")
            Language.setMessage(mstrModuleName, 1008, "EMPLOYEUR")
            Language.setMessage(mstrModuleName, 1009, "Nom ou Raison Sociale")
            Language.setMessage(mstrModuleName, 1010, "CACHET ET SIGNATURE")
            Language.setMessage(mstrModuleName, 1011, "B.P :")
            Language.setMessage(mstrModuleName, 1012, "VILLE :")
            Language.setMessage(mstrModuleName, 1013, "TEL :")
            Language.setMessage(mstrModuleName, 1014, "Fax :")
            Language.setMessage(mstrModuleName, 1015, "Effectif total")
            Language.setMessage(mstrModuleName, 1016, "CADRE RESERVE A LA CNSS")
            Language.setMessage(mstrModuleName, 1017, "Rémuneration totale plafonnée CNSS")
            Language.setMessage(mstrModuleName, 1018, "Montant déduction Allocations Familiales")
            Language.setMessage(mstrModuleName, 1019, "Rémunération totale plafonnée CNAMGS")
            Language.setMessage(mstrModuleName, 1020, "DATE DE RECEPTION")
            Language.setMessage(mstrModuleName, 1021, "Cotisation brutes dues CNSS")
            Language.setMessage(mstrModuleName, 1022, " Cotisations nettes dues CNSS")
            Language.setMessage(mstrModuleName, 1023, " Cotisations nettes dues CNAMGS")
            Language.setMessage(mstrModuleName, 1024, "Date")
            Language.setMessage(mstrModuleName, 1025, "SOUS TOTAL A REPORTER PAGE SUIVANTE")
            Language.setMessage(mstrModuleName, 1026, "RECAP")
            Language.setMessage(mstrModuleName, 1027, "CNSS")
            Language.setMessage(mstrModuleName, 1028, "TAUX")
            Language.setMessage(mstrModuleName, 1029, "MASSE SALARIALE PLAFONNEE CNSS")
            Language.setMessage(mstrModuleName, 1050, "N° Paie")
            Language.setMessage(mstrModuleName, 1051, "NOM ET PRENOM")
            Language.setMessage(mstrModuleName, 1052, "Taux CNSS( c )  Taux CNAMGS %")
            Language.setMessage(mstrModuleName, 1053, "EMBAUCHE  (d)")
            Language.setMessage(mstrModuleName, 1054, "CESSATION ( e )")
            Language.setMessage(mstrModuleName, 1029, "COTISATION GLOBALE DUE (CNSS)")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
