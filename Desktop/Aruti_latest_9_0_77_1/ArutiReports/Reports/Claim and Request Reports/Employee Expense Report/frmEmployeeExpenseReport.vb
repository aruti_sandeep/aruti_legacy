'************************************************************************************************************************************
'Class Name : frmEmployeeExpenseReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeExpenseReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeExpenseReport"
    Private ObjEmployeeExpenseReport As clsEmployeeExpenseReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()
        ObjEmployeeExpenseReport = New clsEmployeeExpenseReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        ObjEmployeeExpenseReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, False, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
            End With

            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            With cboExpenseCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
            End With

            Dim objExRate As New clsExchangeRate
            dsCombos = objExRate.getComboList("ExRate", True)
            If dsCombos.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombos.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombos.Tables("ExRate")
                .SelectedValue = 0
            End With
            objExRate = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            ObjEmp = Nothing
            ObjMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpTranFromDate.Value = DateTime.Now
            dtpTranToDate.Value = DateTime.Now
            cboEmployee.SelectedValue = 0
            cboExpenseCategory.SelectedIndex = 0
            cboExpense.SelectedIndex = 0
            cboCurrency.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAdvanceFilter = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            ObjEmployeeExpenseReport.setDefaultOrderBy(cboExpenseCategory.SelectedIndex)
            txtOrderBy.Text = ObjEmployeeExpenseReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboExpenseCategory.Select()
                Return False

                'Pinkal (07-Jun-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'ElseIf CInt(cboExpense.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense is compulsory information.Please Select Expense."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    cboExpense.Select()
                '    Return False
                'Pinkal (07-Jun-2019) -- End

            ElseIf CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Currency is compulsory information.Please Select Currency."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboCurrency.Select()
                Return False
            End If


            ObjEmployeeExpenseReport.SetDefaultValue()

            ObjEmployeeExpenseReport._FromDate = dtpTranFromDate.Value.Date
            ObjEmployeeExpenseReport._ToDate = dtpTranToDate.Value.Date
            ObjEmployeeExpenseReport._EmployeeId = cboEmployee.SelectedValue
            ObjEmployeeExpenseReport._EmployeeName = cboEmployee.Text
            ObjEmployeeExpenseReport._ExpenseCategoryId = cboExpenseCategory.SelectedValue
            ObjEmployeeExpenseReport._ExpenseCategory = cboExpenseCategory.Text
            ObjEmployeeExpenseReport._ExpenseId = cboExpense.SelectedValue
            ObjEmployeeExpenseReport._Expense = cboExpense.Text
            ObjEmployeeExpenseReport._ReportId = cboExpense.SelectedIndex
            ObjEmployeeExpenseReport._ReportTypeName = cboExpense.Text
            ObjEmployeeExpenseReport._CurrencyId = CInt(cboCurrency.SelectedValue)
            ObjEmployeeExpenseReport._CurrencySign = cboCurrency.Text
            ObjEmployeeExpenseReport._BaseCurrencyId = mintBaseCurrId
            ObjEmployeeExpenseReport._ViewByIds = mstrStringIds
            ObjEmployeeExpenseReport._ViewIndex = mintViewIdx
            ObjEmployeeExpenseReport._ViewByName = mstrStringName
            ObjEmployeeExpenseReport._Analysis_Fields = mstrAnalysis_Fields
            ObjEmployeeExpenseReport._Analysis_Join = mstrAnalysis_Join
            ObjEmployeeExpenseReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            ObjEmployeeExpenseReport._Report_GroupName = mstrReport_GroupName
            ObjEmployeeExpenseReport._Advance_Filter = mstrAdvanceFilter
            ObjEmployeeExpenseReport._PaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmployeeExpenseReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            ObjEmployeeExpenseReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeExpenseReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeExpenseReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAirPassageReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeExpenseReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeExpenseReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

#End Region

#Region " Buttons "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            ObjEmployeeExpenseReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    FinancialYear._Object._FinancialYear_Name, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                    , enExportAction.Excel)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpenseCategory.DataSource
            frm.ValueMember = cboExpenseCategory.ValueMember
            frm.DisplayMember = cboExpenseCategory.DisplayMember
            If frm.DisplayDialog Then
                cboExpenseCategory.SelectedValue = frm.SelectedValue
                cboExpenseCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            ObjEmployeeExpenseReport.setOrderBy(0)
            txtOrderBy.Text = ObjEmployeeExpenseReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpense.DataSource
            frm.ValueMember = cboExpense.ValueMember
            frm.DisplayMember = cboExpense.DisplayMember
            If frm.DisplayDialog Then
                cboExpense.SelectedValue = frm.SelectedValue
                cboExpense.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objBtnSearchCurrency_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnSearchCurrency.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboCurrency.DataSource
            frm.ValueMember = cboCurrency.ValueMember
            frm.DisplayMember = cboCurrency.DisplayMember
            If frm.DisplayDialog Then
                cboCurrency.SelectedValue = frm.SelectedValue
                cboCurrency.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchCurrency_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objExpense = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.Name, Me.LblExpense.Text)
            Me.lblTranToDate.Text = Language._Object.getCaption(Me.lblTranToDate.Name, Me.lblTranToDate.Text)
            Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
            Me.lblTranFromDate.Text = Language._Object.getCaption(Me.lblTranFromDate.Name, Me.lblTranFromDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category.")
            Language.setMessage(mstrModuleName, 2, "Expense is compulsory information.Please Select Expense.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class
