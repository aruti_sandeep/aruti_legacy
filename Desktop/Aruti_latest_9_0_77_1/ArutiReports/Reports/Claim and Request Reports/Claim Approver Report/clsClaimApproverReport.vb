﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsClaimApproverReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsClaimApproverReport"
    Private mstrReportId As String = enArutiReport.ClaimApprover_Report   '305
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private Variables "

    Private mintExpenseCategoryId As Integer = 0
    Private mstrExpenseCategory As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ExpenseCategoryId() As Integer
        Set(ByVal value As Integer)
            mintExpenseCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategory() As String
        Set(ByVal value As String)
            mstrExpenseCategory = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintExpenseCategoryId = 0
            mstrExpenseCategory = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAdvance_Filter = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintExpenseCategoryId > 0 Then
                Me._FilterQuery &= " AND cmexpapprover_master.expensetypeid = @expensetypeid "
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Expense Category : ") & " " & mstrExpenseCategory & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee : ") & " " & mstrEmployeeName & " "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_EmpListWithApproverReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtTransferAsOnDate, xOnlyApproved, xUserModeSetting)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Methods "

    Private Function Generate_EmpListWithApproverReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation
            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrConditionQry As String

            StrInnerQry = "" : StrConditionQry = ""

            Dim objClaimApprover_master As New clsExpenseApprover_Master
            dsCompany = objClaimApprover_master.GetClaimExternalApproverList(objDataOperation, "List", False)
            objClaimApprover_master = Nothing

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)

            objDataOperation.ClearParameters()

            StrInnerQry = "SELECT ISNULL(hremployee_master.employeecode,'') AS 'EmployeeCode' " & _
                          ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'Employee' " & _
                          ",#APPR_CODE# AS 'ApproverCode' " & _
                          ",#APPR_NAME# AS 'Approver' " & _
                          ",ISNULL(cmapproverlevel_master.crlevelname,'') AS 'Level' " & _
                          ",#EMPL_DEPT# AS 'Department' " & _
                          ",#EMPL_JOB# AS 'JobTitle' " & _
                          ",ISNULL(cmapproverlevel_master.crpriority,-1) AS priority "


            If mintViewIndex > 0 Then
                StrInnerQry &= mstrAnalysis_Fields
            Else
                StrInnerQry &= ", 0 AS Id, '' AS GName "
            End If

            StrInnerQry &= " FROM cmexpapprover_tran " & _
                         " LEFT JOIN cmexpapprover_master ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid AND cmexpapprover_master.isvoid = 0  " & _
                         " LEFT JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid =  cmapproverlevel_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 " & _
                         " LEFT JOIN hremployee_master ON cmexpapprover_tran.employeeunkid= hremployee_master.employeeunkid " & _
                          " #EMP_JOIN# " & _
                         " LEFT JOIN " & _
                                                        " ( " & _
                                                        "    SELECT " & _
                                                        "         departmentunkid " & _
                                                        "        ,employeeunkid " & _
                                                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                        "    FROM #DBName#hremployee_transfer_tran " & _
                                                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                                                        " ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN #DBName#hrdepartment_master dept ON dept.departmentunkid= Alloc.departmentunkid " & _
                        " LEFT JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "         jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM #DBName#hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                         " ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
                         " LEFT JOIN #DBName#hrjob_master jb ON jb.jobunkid= Jobs.jobunkid "

            StrInnerQry &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrInnerQry &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrInnerQry &= xAdvanceJoinQry
            End If

            StrConditionQry &= " #WHERE_CONDITION# AND cmexpapprover_tran.isvoid = 0 AND cmexpapprover_master.isexternalapprover = #ExAppr# " & _
                                         " AND cmexpapprover_master.isswap = 0 AND cmexpapprover_master.isactive = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrConditionQry &= " AND " & mstrAdvance_Filter.Trim
            End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrConditionQry &= " AND " & xUACFiltrQry
            End If


            Call FilterTitleAndFilterQuery()

            StrConditionQry &= Me._FilterQuery

            StrQ = StrInnerQry
            StrQ &= StrConditionQry

            StrQ = StrQ.Replace("#APPR_CODE#", "ISNULL(h1.employeecode,'') ")
            StrQ = StrQ.Replace("#APPR_NAME#", "ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '') ")
            StrQ = StrQ.Replace("#ExAppr#", "0")
            StrQ = StrQ.Replace("#DBName#", "")
            StrQ = StrQ.Replace("#EMP_JOIN#", "JOIN hremployee_master h1 ON h1.employeeunkid = cmexpapprover_master.employeeunkid ")
            StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE 1 = 1 ")
            StrQ = StrQ.Replace("#EMPL_DEPT#", " ISNULL(dept.name,'') ")
            StrQ = StrQ.Replace("#EMPL_JOB#", " ISNULL(jb.job_name,'') ")

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrConditionQry = StrConditionQry.Replace("ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')", "ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.lastname, '')")
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_CODE#", " '' ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " ISNULL(h1.username,'') ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS h1 ON h1.userunkid = cmexpapprover_master.employeeunkid ")
                    StrQ = StrQ.Replace("#EMPL_DEPT#", " '' ")
                    StrQ = StrQ.Replace("#EMPL_JOB#", " '' ")
                    StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE h1.companyunkid = " & dr("companyunkid") & " ")
                Else
                    StrQ = StrQ.Replace("#APPR_CODE#", " CASE WHEN ISNULL(h1.employeecode, '') = '' THEN '' ELSE ISNULL(h1.employeecode, '') END ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " CASE WHEN ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') END ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmexpapprover_master.employeeunkid " & _
                                                      " JOIN #DBName#hremployee_master h1 ON UEmp.employeeunkid = h1.employeeunkid ")
                    StrQ = StrQ.Replace("#EMPL_DEPT#", " ISNULL(dept.name,'') ")
                    StrQ = StrQ.Replace("#EMPL_JOB#", " ISNULL(jb.job_name,'') ")
                    StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE UEmp.companyunkid = " & dr("companyunkid") & " ")
                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DBName#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DBName#", "")
                End If

                StrQ = StrQ.Replace("#ExAppr#", "1")

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mstrLevel As String = String.Empty
            Dim mstrLeavename As String = String.Empty
            Dim mstrApprover As String = String.Empty

            Dim dtTable As New DataTable
            dtTable = New DataView(dsList.Tables("DataTable"), "", "priority", DataViewRowState.CurrentRows).ToTable()

            For Each dtRow As DataRow In dtTable.Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("EmployeeCode").ToString() & " : " & dtRow.Item("Employee").ToString()
                rpt_Rows.Item("Column3") = dtRow.Item("ApproverCode").ToString()
                rpt_Rows.Item("Column4") = dtRow.Item("Approver").ToString()
                rpt_Rows.Item("Column5") = dtRow.Item("Department").ToString()
                rpt_Rows.Item("Column6") = dtRow.Item("JobTitle").ToString()
                rpt_Rows.Item("Column7") = dtRow.Item("Level").ToString()
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmpListWithClaimApprover_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 3, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 4, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 5, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 6, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            objRpt.SetDataSource(rpt_Data)
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 7, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "txtApproverCode", Language.getMessage(mstrModuleName, 8, "Approver Code"))
            Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 9, "Approver Name"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 10, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 11, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtApproverLevel", Language.getMessage(mstrModuleName, 12, "Approver Level"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmpListWithApproverReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Expense Category :")
            Language.setMessage(mstrModuleName, 2, "Employee :")
            Language.setMessage(mstrModuleName, 3, "Prepared By :")
            Language.setMessage(mstrModuleName, 4, "Checked By :")
            Language.setMessage(mstrModuleName, 5, "Approved By :")
            Language.setMessage(mstrModuleName, 6, "Received By :")
            Language.setMessage(mstrModuleName, 7, "Employee :")
            Language.setMessage(mstrModuleName, 8, "Approver Code")
            Language.setMessage(mstrModuleName, 9, "Approver Name")
            Language.setMessage(mstrModuleName, 10, "Department")
            Language.setMessage(mstrModuleName, 11, "Job Title")
            Language.setMessage(mstrModuleName, 12, "Approver Level")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
