﻿'************************************************************************************************************************************
'Class Name : clsClaimExpenseApprovalStatusReport.vb
'Purpose    :
'Date       : 26-Dec-2023
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Hemant Morker
''' </summary>
Public Class clsClaimExpenseApprovalStatusReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsClaimExpenseApprovalStatusReport"
    Private mstrReportId As String = enArutiReport.Claim_Expense_Approval_Status_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mintExpCateId As Integer = 0
    Private mstrExpCateName As String = ""
    Private mintExpenseID As Integer = -1
    Private mstrExpense As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = String.Empty
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateId() As Integer
        Set(ByVal value As Integer)
            mintExpCateId = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateName() As String
        Set(ByVal value As String)
            mstrExpCateName = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseID() As Integer
        Set(ByVal value As Integer)
            mintExpenseID = value
        End Set
    End Property

    Public WriteOnly Property _Expense() As String
        Set(ByVal value As String)
            mstrExpense = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _PaymentApprovalwithLeaveApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property


#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mintExpCateId = 0
            mstrExpCateName = ""
            mintExpenseID = 0
            mstrExpense = ""
            mintStatusId = 0
            mstrStatusName = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid = @empunkid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintExpCateId > 0 Then
                objDataOperation.AddParameter("@expcateid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpCateId)
                Me._FilterQuery &= " AND cmclaim_request_master.expensetypeid = @expcateid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Expense Category :") & " " & mstrExpCateName & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND cmclaim_request_master.statusunkid = @statusid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Status :") & " " & mstrStatusName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " Order By : ") & " " & Me.OrderByDisplay
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub
#End Region

#Region " Report Generation "
    Public Sub Generate_DetailReport(ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As Date _
                                     , ByVal xPeriodEnd As Date _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                     , ByVal xExportReportPath As String _
                                     , ByVal xOpenReportAfterExport As Boolean _
                                     , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                     , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                     )
        Dim dtCol As DataColumn
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            StrQ &= "SELECT  " & _
                    "       ISNULL(hremployee_master.employeecode, '') AS EmployeeCode "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
            End If

            StrQ &= ",      ISNULL(hd.name, '') AS Department " & _
                    ",      ISNULL(hsg.name, '') AS SectionGroup " & _
                    ",      ISNULL(hcg.name, '') AS ClassGroup " & _
                    ",      ISNULL(hc.name, '') AS Class " & _
                    ",      ISNULL(hjb.job_name, '') AS Job " & _
                    ",      CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                                "WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                                "WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training " & _
                                "WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                                "WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _
                                "WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_PRIVILEGE & "' THEN @EXP_REBATE_PRIVILEGE " & _
                                "WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_DUTY & "' THEN @EXP_REBATE_DUTY " & _
                                "WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT & "' THEN @EXP_REBATE_PRIVILEGE_DPNDT END AS expensetype " & _
                    ",      ISNULL(ReqTable.Expense,'') AS Expense " & _
                    ",      ISNULL(ReqTable.ReqAmt,0) AS ReqAmt " & _
                    ",CASE WHEN cmclaim_request_master.statusunkid = 1 THEN ISNULL(App.ApprovedAmt,0) " & _
                                "ELSE 0.00 " & _
                    "END AS ApprAmt "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM cmclaim_request_master " & _
                    " JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                    " LEFT JOIN ( " & _
                    "           SELECT " & _
                    "               crmasterunkid " & _
                    "               ,SUM(cmclaim_request_tran.amount) AS ReqAmt " & _
                    "               ,SUM(cmclaim_request_tran.quantity) AS ReqQty " & _
                    "               ,cmclaim_request_tran.expenseunkid " & _
                    "               ,ISNULL(cmexpense_master.name, '') AS Expense " & _
                    "           FROM cmclaim_request_tran " & _
                    "           JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid " & _
                    "           WHERE cmclaim_request_tran.isvoid = 0 "
            If mintExpenseID > 0 Then
                StrQ &= "                   AND cmclaim_request_tran.expenseunkid = " & mintExpenseID
            End If
            StrQ &= "           GROUP BY crmasterunkid " & _
                    "                  , ISNULL(cmexpense_master.name, '') " & _
                    "                  , cmclaim_request_tran.expenseunkid " & _
                    "       ) AS ReqTable ON ReqTable.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                    " LEFT JOIN ( " & _
                    "  SELECT " & _
                    "      crmasterunkid " & _
                    "      ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                    "      ,statusunkid " & _
                    "      ,expenseunkid " & _
                    "   FROM cmclaim_approval_tran " & _
                    "   WHERE isvoid = 0 And statusunkid = 1 AND " & _
                    "   crapproverunkid IN ( " & _
                    "                  SELECT crapproverunkid FROM  " & _
                    "                  ( " & _
                    "                                  SELECT " & _
                    "                                           ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                    "                                          ,cm.crmasterunkid " & _
                    "                                          ,cm.approveremployeeunkid " & _
                    "                                          ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY #PRIORITY# DESC) AS rno " & _
                    "                                  FROM cmclaim_approval_tran cm " & _
                    "                                  JOIN #APPROVER_MASTER# AM ON AM.#APPROVERUNKID# = cm.crapproverunkid " & _
                    "                                  JOIN #APPROVER_LEVEL_MASTER# ALM ON ALM.#LEVELUNKID# = AM.#LEVELUNKID# " & _
                    "                                  WHERE cm.isvoid = 0 " & _
                    "                  ) as cnt WHERE cnt.rno= 1 " & _
                    "                          AND cnt.crmasterunkid =  cmclaim_approval_tran.crmasterunkid " & _
                    "                          AND cnt.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                    "                          AND cnt.approveremployeeunkid= cmclaim_approval_tran.approveremployeeunkid " & _
                    "                                  ) " & _
                    "	GROUP BY crmasterunkid,statusunkid,expenseunkid " & _
                    " ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                    " AND app.statusunkid = cmclaim_request_master.statusunkid " & _
                    " AND app.expenseunkid = ReqTable.expenseunkid "
            StrQ &= " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             stationunkid " & _
                          "            ,deptgroupunkid " & _
                          "            ,departmentunkid " & _
                          "            ,sectiongroupunkid " & _
                          "            ,sectionunkid " & _
                          "            ,unitgroupunkid " & _
                          "            ,unitunkid " & _
                          "            ,teamunkid " & _
                          "            ,classgroupunkid " & _
                          "             ,classunkid " & _
                          "            ,employeeunkid " & _
                          "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "       FROM hremployee_transfer_tran " & _
                          "       WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                          " LEFT JOIN hrdepartment_master hd ON t.departmentunkid = hd.departmentunkid " & _
                          " LEFT JOIN hrsectiongroup_master hsg ON t.sectiongroupunkid = hsg.sectiongroupunkid " & _
                          " LEFT JOIN hrclassgroup_master hcg ON t.classgroupunkid = hcg.classgroupunkid " & _
                          " LEFT JOIN hrclasses_master hc ON t.classunkid = hc.classesunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "             SELECT " & _
                          "                 jobgroupunkid " & _
                          "                ,jobunkid " & _
                          "                ,employeeunkid " & _
                          "                ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "             FROM hremployee_categorization_tran " & _
                          "             WHERE isvoid = 0	AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate  " & _
                          " ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                          " LEFT JOIN hrjob_master hjb ON hjb.jobunkid = j.jobunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrAnalysis_Join & " "

            StrQ &= " WHERE  cmclaim_request_master.isvoid = 0  "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mintViewIndex > 0 Then
                StrQ &= " ORDER BY " & mstrAnalysis_OrderBy_GName & ", employeecode"
            Else
                StrQ &= " ORDER BY employeecode "
            End If

            If mblnPaymentApprovalwithLeaveApproval = False Then
                StrQ = StrQ.Replace("#APPROVER_MASTER#", "cmexpapprover_master")
                StrQ = StrQ.Replace("#APPROVERUNKID#", "crapproverunkid")
                StrQ = StrQ.Replace("#APPROVER_LEVEL_MASTER#", "cmapproverlevel_master")
                StrQ = StrQ.Replace("#LEVELUNKID#", "crlevelunkid")
                StrQ = StrQ.Replace("#PRIORITY#", "crpriority")
            Else
                StrQ = StrQ.Replace("#APPROVER_MASTER#", "lvleaveapprover_master")
                StrQ = StrQ.Replace("#APPROVERUNKID#", "approverunkid")
                StrQ = StrQ.Replace("#APPROVER_LEVEL_MASTER#", "lvapproverlevel_master")
                StrQ = StrQ.Replace("#LEVELUNKID#", "levelunkid")
                StrQ = StrQ.Replace("#PRIORITY#", "priority")
            End If


            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))
            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            objDataOperation.AddParameter("@EXP_REBATE_PRIVILEGE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 10, "Rebate Privilege"))
            objDataOperation.AddParameter("@EXP_REBATE_DUTY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 11, "Rebate Duty"))
            objDataOperation.AddParameter("@EXP_REBATE_PRIVILEGE_DPNDT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 13, "Rebate Privilege Dependant"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTableExcel = dsList.Tables(0).Copy

            Dim strarrGroupColumns As String() = Nothing


            If mintViewIndex > 0 Then
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns.Remove("GName")
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("EmployeeCode").Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 2, "Full Name")
            mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 3, "Function")
            mdtTableExcel.Columns("SectionGroup").Caption = Language.getMessage(mstrModuleName, 4, "Department")
            mdtTableExcel.Columns("ClassGroup").Caption = Language.getMessage(mstrModuleName, 5, "Zone")
            mdtTableExcel.Columns("Class").Caption = Language.getMessage(mstrModuleName, 6, "Branch")
            mdtTableExcel.Columns("Job").Caption = Language.getMessage(mstrModuleName, 7, "Job Title")
            mdtTableExcel.Columns("expensetype").Caption = Language.getMessage(mstrModuleName, 8, "Claim Category")
            mdtTableExcel.Columns("Expense").Caption = Language.getMessage(mstrModuleName, 9, "Expense")
            mdtTableExcel.Columns("ReqAmt").Caption = Language.getMessage(mstrModuleName, 10, "Requested Claim Amount")
            mdtTableExcel.Columns("ApprAmt").Caption = Language.getMessage(mstrModuleName, 19, "Approved Claim Amount")


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", Nothing, "", True, rowsArrayHeader)



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			
			Language.setMessage(mstrModuleName, 1, "Employee Code")
			Language.setMessage(mstrModuleName, 2, "Full Name")
			Language.setMessage(mstrModuleName, 3, "Function")
			Language.setMessage(mstrModuleName, 4, "Department")
			Language.setMessage(mstrModuleName, 5, "Zone")
			Language.setMessage(mstrModuleName, 6, "Branch")
			Language.setMessage(mstrModuleName, 7, "Job Title")
			Language.setMessage(mstrModuleName, 8, "Claim Category")
			Language.setMessage(mstrModuleName, 9, "Expense")
			Language.setMessage(mstrModuleName, 10, "Claim Amount")
			Language.setMessage(mstrModuleName, 11, "Prepared By :")
			Language.setMessage(mstrModuleName, 12, "Checked By :")
			Language.setMessage(mstrModuleName, 13, "Approved By :")
			Language.setMessage(mstrModuleName, 14, "Received By :")
			Language.setMessage(mstrModuleName, 15, "Employee :")
			Language.setMessage(mstrModuleName, 16, "Expense Category :")
			Language.setMessage(mstrModuleName, 17, "Status :")
            Language.setMessage(mstrModuleName, 18, " Order By :")
            Language.setMessage(mstrModuleName, 19, "Approved Claim Amount")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
