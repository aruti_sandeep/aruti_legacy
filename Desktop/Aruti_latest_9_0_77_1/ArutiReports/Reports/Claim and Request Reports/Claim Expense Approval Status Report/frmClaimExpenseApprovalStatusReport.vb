﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmClaimExpenseApprovalStatusReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmClaimExpenseApprovalStatusReport"
    Private objClaimExpenseApprovalStatusReport As clsClaimExpenseApprovalStatusReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
#End Region

#Region " Contructor "

    Public Sub New()
        objClaimExpenseApprovalStatusReport = New clsClaimExpenseApprovalStatusReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objClaimExpenseApprovalStatusReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Forms "

    Private Sub frmProgressUpdateNotAttemptedReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objClaimExpenseApprovalStatusReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmProgressUpdateNotAttemptedReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProgressUpdateNotAttemptedReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.EZeeHeader1.Title = objClaimExpenseApprovalStatusReport._ReportName
            Me.EZeeHeader1.Message = objClaimExpenseApprovalStatusReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProgressUpdateNotAttemptedReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, False, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
            End With

            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            With cboExpenseCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
            End With

            dsCombos = objMasterData.getLeaveStatusList("List", "")
            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombos.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "name"
                .DataSource = dtab
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing
            objMasterData = Nothing
            dsCombos.Clear()
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboExpenseCategory.SelectedValue = 0
            cboExpense.SelectedValue = 0
            cboStatus.SelectedIndex = 0

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
          
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objClaimExpenseApprovalStatusReport.SetDefaultValue()
            If cboStatus.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Status is mandatory information. Please select Status to continue."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If

            objClaimExpenseApprovalStatusReport._EmployeeId = cboEmployee.SelectedValue
            objClaimExpenseApprovalStatusReport._EmployeeName = cboEmployee.Text
            objClaimExpenseApprovalStatusReport._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            objClaimExpenseApprovalStatusReport._ExpCateName = cboExpenseCategory.Text.ToString()
            objClaimExpenseApprovalStatusReport._ExpenseID = CInt(cboExpense.SelectedValue)
            objClaimExpenseApprovalStatusReport._Expense = cboExpense.Text
            objClaimExpenseApprovalStatusReport._StatusId = CInt(cboStatus.SelectedValue)
            objClaimExpenseApprovalStatusReport._StatusName = cboStatus.Text.ToString
            objClaimExpenseApprovalStatusReport._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objClaimExpenseApprovalStatusReport._ViewByIds = mstrStringIds
            objClaimExpenseApprovalStatusReport._ViewIndex = mintViewIdx
            objClaimExpenseApprovalStatusReport._ViewByName = mstrStringName
            objClaimExpenseApprovalStatusReport._Analysis_Fields = mstrAnalysis_Fields
            objClaimExpenseApprovalStatusReport._Analysis_Join = mstrAnalysis_Join
            objClaimExpenseApprovalStatusReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objClaimExpenseApprovalStatusReport._Report_GroupName = mstrReport_GroupName
            objClaimExpenseApprovalStatusReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objClaimExpenseApprovalStatusReport._PaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function


#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objClaimExpenseApprovalStatusReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                      User._Object._Userunkid, _
                                                                      FinancialYear._Object._YearUnkid, _
                                                                      Company._Object._Companyunkid, _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                                      True, False, _
                                                                      ConfigParameter._Object._ExportReportPath, _
                                                                      ConfigParameter._Object._OpenAfterExport, _
                                                                       enPrintAction.Preview, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchStatus.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboStatus.DataSource
            frm.ValueMember = cboStatus.ValueMember
            frm.DisplayMember = cboStatus.DisplayMember
            If frm.DisplayDialog Then
                cboStatus.SelectedValue = frm.SelectedValue
                cboStatus.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSector_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpenseCategory.DataSource
            frm.ValueMember = cboExpenseCategory.ValueMember
            frm.DisplayMember = cboExpenseCategory.DisplayMember
            If frm.DisplayDialog Then
                cboExpenseCategory.SelectedValue = frm.SelectedValue
                cboExpenseCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsClaimExpenseApprovalStatusReport.SetMessages()
            objfrm._Other_ModuleNames = "clsClaimExpenseApprovalStatusReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            Dim strTableAlias = "hremployee_master"
            frm.displayDialog(strTableAlias)

            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objExpense = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblExpenseCategory.Text = Language._Object.getCaption(Me.lblExpenseCategory.Name, Me.lblExpenseCategory.Text)
            Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.Name, Me.lblExpense.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Status is mandatory information. Please select Status to continue.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class