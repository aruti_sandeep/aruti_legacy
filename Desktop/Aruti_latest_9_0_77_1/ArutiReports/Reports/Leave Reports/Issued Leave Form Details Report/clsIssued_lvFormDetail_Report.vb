'************************************************************************************************************************************
'Class Name : clsIssued_lvFormDetail_Report.vb
'Purpose    :
'Date       : 01/05/2014
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsIssued_lvFormDetail_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsIssued_lvFormDetail_Report"
    Private mstrReportId As String = enArutiReport.Issued_Leave_Form_Detail_Report  '151
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintYearId As Integer = 0
    Private mintLeaveBalanceSetting As Integer = 0
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLeaveId As Integer = 0
    Private mstrLeaveName As String = ""
    Private mintLeaveFormId As Integer = 0
    Private mstrLeaveForm As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Pinkal (06-Jan-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property


    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveFormId() As Integer
        Set(ByVal value As Integer)
            mintLeaveFormId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveForm() As String
        Set(ByVal value As String)
            mstrLeaveForm = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    'Pinkal (06-Jan-2016) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            'mintReportId = 0
            'mstrReportTypeName = ""
            mintYearId = 0
            mintLeaveBalanceSetting = 0
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintLeaveId = 0
            mstrLeaveName = ""
            mintLeaveFormId = 0
            mstrLeaveForm = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            mblnIncludeAccessFilterQry = True
            'Pinkal (06-Jan-2016) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            'Pinkal (30-Oct-2019) -- Start
            'Defect Hill Packaging [0004243] - Worked On mployees not showing in the issued leave report.
            If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then

                'Pinkal (16-Jul-2020) -- Start
                'Query Freight Forwarders Ltd. : 0004814 Issue Summary: Leave Issued Report Missing Some Leave forms when filtered By Issued Date
                'Me._FilterQuery = " AND (@startdate BETWEEN Convert(Char(8),lvleaveIssue_master.startdate,112) AND Convert(Char(8),lvleaveIssue_master.enddate,112) OR @enddate BETWEEN Convert(Char(8),lvleaveIssue_master.startdate,112) AND Convert(Char(8),lvleaveIssue_master.enddate,112))"
                Me._FilterQuery = " AND (Convert(Char(8),lvleaveIssue_master.startdate,112) BETWEEN @startdate AND @enddate OR  Convert(Char(8),lvleaveIssue_master.enddate,112) BETWEEN @startdate AND @enddate)"
                'Pinkal (16-Jul-2020) -- End


                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "From Date: ") & " " & mdtFromDate.Date & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "To Date: ") & " " & mdtToDate.Date & " "
            Else

                If mdtFromDate <> Nothing Then
                    'Pinkal (30-Oct-2019) -- Start
                    'Defect Hill Packaging [0004243] - Worked On mployees not showing in the issued leave report.
                    'Me._FilterQuery &= " AND Convert(Char(8),lvleaveIssue_master.startdate,112) >= @startdate "
                    Me._FilterQuery &= " AND @startdate BETWEEN Convert(Char(8),lvleaveIssue_master.startdate,112) AND Convert(Char(8),lvleaveIssue_master.enddate,112) "
                    'Pinkal (30-Oct-2019) -- End
                    objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "From Date: ") & " " & mdtFromDate.Date & " "
                End If

                If mdtToDate <> Nothing Then
                    'Pinkal (30-Oct-2019) -- Start
                    'Defect Hill Packaging [0004243] - Worked On mployees not showing in the issued leave report.
                    'Me._FilterQuery &= " AND Convert(Char(8),lvleaveIssue_master.enddate,112) <= @enddate "
                    Me._FilterQuery &= " AND @enddate BETWEEN Convert(Char(8),lvleaveIssue_master.startdate,112) AND Convert(Char(8),lvleaveIssue_master.enddate,112) "
                    'Pinkal (30-Oct-2019) -- End
                    objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "To Date: ") & " " & mdtToDate.Date & " "
                End If
            End If
            'Pinkal (24-Oct-2019) -- End

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintLeaveId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Leave Type: ") & " " & mstrLeaveName & " "
            End If

            If mintLeaveFormId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.formunkid = @formunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveFormId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Leave Form: ") & " " & mstrLeaveForm & " "
            End If


            'Pinkal (15-Oct-2018) -- Start
            'Bug - Support Issue Id # 0002670 [Internal Audit Observations (variance in leave issued report)]
            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                If mintYearId > 0 Then
                    Me._FilterQuery &= " AND  lvleaveIssue_master.leaveyearunkid = @yearunkid "
                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                End If
            End If
            'Pinkal (15-Oct-2018) -- End


            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, ''), " & Me.OrderByQuery
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "  ORDER BY ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, ''), " & Me.OrderByQuery
                End If
            End If

            'End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(formno, '')", Language.getMessage(mstrModuleName, 1, "Form No")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(lvleavetype_master.leavename, '')", Language.getMessage(mstrModuleName, 2, "Leave Type")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 3, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 4, "Employee")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Sub Generate_IssuedLeaveStatusReport()
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Try


    '        If mintCompanyUnkid <= 0 Then
    '            mintCompanyUnkid = Company._Object._Companyunkid
    '        End If

    '        Company._Object._Companyunkid = mintCompanyUnkid
    '        ConfigParameter._Object._Companyunkid = mintCompanyUnkid

    '        If mintUserUnkid <= 0 Then
    '            mintUserUnkid = User._Object._Userunkid
    '        End If

    '        User._Object._Userunkid = mintUserUnkid

    '        objDataOperation = New clsDataOperation

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))


    '        StrQ = "SELECT ISNULL(formno, '') AS formno " & _
    '                  ", ISNULL(hremployee_master.employeecode, '')  + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
    '                  ", ISNULL(lvleavetype_master.leavename, '') AS leavetype " & _
    '                  ", ISNULL(jb.job_name, '') AS jobtitle "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", '' AS GName "
    '        End If

    '        StrQ &= ", CONVERT(VARCHAR(max),lvleaveform.startdate) AS startdate " & _
    '                     ", CONVERT(VARCHAR(max),lvleaveform.returndate) AS enddate " & _
    '                     ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
    '                     "  AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
    '                     ", CONVERT(VARCHAR(max),lvleaveform.approve_stdate) AS app_startdate " & _
    '                     ", CONVERT(VARCHAR(max),lvleaveform.approve_eddate) AS app_enddate " & _
    '                     ", Convert(Varchar(Max),CAST(0.00 AS decimal)) balance " & _
    '                     ", ISNULL(lvleaveIssue_master.issuecount, 0.00) - ISNULL(lvleaveIssue_master.cancelcount, 0.00) AS issued_days " & _
    '                     ", Convert(Varchar(Max),CAST(0.00 AS decimal)) AS remaining_balance " & _
    '                     ", CASE WHEN  lvleaveform.statusunkid = 7 THEN @Issued END AS status " & _
    '                     ", 0.00 AS variance " & _
    '                     " ,lvleaveform.formunkid " & _
    '                     ", lvleaveform.employeeunkid " & _
    '                     ", lvleaveform.leavetypeunkid " & _
    '                     " FROM lvleaveform " & _
    '                     " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
    '                     " LEFT JOIN hrjob_master jb ON hremployee_master.jobunkid = jb.jobunkid " & _
    '                     " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
    '                     " JOIN lvleaveIssue_master ON lvleaveIssue_master.employeeunkid = lvleaveform.employeeunkid  " & _
    '                     " AND lvleaveIssue_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleaveform.formunkid =  lvleaveIssue_master.formunkid  " & _
    '                     " AND lvleaveIssue_master.isvoid = 0 "


    '        'Pinkal (21-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= " LEFT JOIN lvleavebalance_tran on lvleavebalance_tran.employeeunkid = lvleaveform.employeeunkid AND lvleavebalance_tran.leavetypeunkid = lvleaveform.leavetypeunkid  AND lvleavebalance_tran.isvoid = 0"
    '        End If
    '        'Pinkal (21-Jul-2014) -- End



    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Join
    '        End If


    '        StrQ &= " WHERE  lvleaveform.isvoid = 0 "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If


    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If

    '        If mstrUserAccessFilter.Trim.Length <= 0 Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            StrQ &= mstrUserAccessFilter
    '        End If



    '        'Pinkal (21-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= " AND CONVERT(char(8),lvleaveform.startdate,112) >= convert(char(8),lvleavebalance_tran.startdate,112) " & _
    '                         " AND convert(char(8),lvleaveform.returndate,112) >= CONVERT(char(8),lvleavebalance_tran.enddate,112) "
    '        End If
    '        'Pinkal (21-Jul-2014) -- End


    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        Dim strarrGroupColumns As String() = Nothing
    '        Dim rowsArrayHeader As New ArrayList
    '        Dim rowsArrayFooter As New ArrayList
    '        Dim row As WorksheetRow
    '        Dim wcell As WorksheetCell

    '        Dim mdtTableExcel As DataTable = dsList.Tables(0)



    '        Dim intEmpId As Integer = 0
    '        Dim dsBalance As DataSet = Nothing
    '        Dim objLeaveBal As New clsleavebalance_tran
    '        Dim mdclBalance As Double = 0
    '        For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
    '            If intEmpId <> CInt(mdtTableExcel.Rows(i)("employeeunkid")) Then
    '                mdclBalance = 0
    '                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                    dsBalance = objLeaveBal.GetList("Balance", False, False, CInt(mdtTableExcel.Rows(i)("employeeunkid")), False, False, False, " AND yearunkid = " & mintYearId & " AND lvleavebalance_tran.leavetypeunkid = " & mintLeaveId)
    '                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    dsBalance = objLeaveBal.GetList("Balance", False, False, CInt(mdtTableExcel.Rows(i)("employeeunkid")), True, True, False, " AND yearunkid = " & mintYearId & " AND lvleavebalance_tran.leavetypeunkid = " & mintLeaveId)
    '                End If
    '                mdtTableExcel.Rows(i)("balance") = Math.Round(CDbl(dsBalance.Tables(0).Rows(0)("Accrue_amount")), 2)
    '                mdclBalance = Math.Round(CDbl(dsBalance.Tables(0).Rows(0)("Accrue_amount")), 2)
    '                intEmpId = CInt(mdtTableExcel.Rows(i)("employeeunkid"))
    '            End If


    '            'Pinkal (19-Jun-2014) -- Start
    '            'Enhancement : FINCA CONGO CHANGES [DISPALYING SUBCOUNT,GRAND TOTAL AND RECORD COUNT ON ANLYSIS BY]  GIVEN BY DENNIS ON URGENT BASIS 
    '            'mdtTableExcel.Rows(i)("balance") = Math.Round(mdclBalance, 2)
    '            'mdclBalance = CDbl(mdtTableExcel.Rows(i)("remaining_balance"))
    '            'mdtTableExcel.Rows(i)("remaining_balance") = CDbl(mdtTableExcel.Rows(i)("remaining_balance")).ToString("#0.00")


    '            'Pinkal (21-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

    '            mdtTableExcel.Rows(i)("balance") = mdclBalance.ToString("#0.00")
    '            mdtTableExcel.Rows(i)("remaining_balance") = Math.Round(mdclBalance - CDbl(mdtTableExcel.Rows(i)("issued_days")), 2).ToString("#0.00")
    '            mdclBalance = CDbl(mdtTableExcel.Rows(i)("remaining_balance"))

    '            mdtTableExcel.Rows(i)("variance") = CDec(mdtTableExcel.Rows(i)("days")) - CDec(mdtTableExcel.Rows(i)("issued_days"))

    '            'Pinkal (21-Jul-2014) -- End

    '            'Pinkal (19-Jun-2014) -- End

    '            mdtTableExcel.Rows(i)("startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("startdate")), DateFormat.ShortDate)
    '            mdtTableExcel.Rows(i)("enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("enddate")), DateFormat.ShortDate)
    '            mdtTableExcel.Rows(i)("app_startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_startdate")), DateFormat.ShortDate)
    '            mdtTableExcel.Rows(i)("app_enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_enddate")), DateFormat.ShortDate)
    '        Next

    '        mdtTableExcel.AcceptChanges()

    '        If mdtTableExcel.Columns.Contains("formunkid") Then
    '            mdtTableExcel.Columns.Remove("formunkid")
    '        End If

    '        If mdtTableExcel.Columns.Contains("employeeunkid") Then
    '            mdtTableExcel.Columns.Remove("employeeunkid")
    '        End If

    '        If mdtTableExcel.Columns.Contains("leavetypeunkid") Then
    '            mdtTableExcel.Columns.Remove("leavetypeunkid")
    '        End If


    '        If mintViewIndex > 0 Then
    '            If mdtTableExcel.Columns.Contains("Id") Then
    '                mdtTableExcel.Columns.Remove("Id")
    '            End If
    '            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
    '            Dim strGrpCols As String() = {"GName", "employee"}
    '            strarrGroupColumns = strGrpCols
    '        Else
    '            If mdtTableExcel.Columns.Contains("GName") Then
    '                mdtTableExcel.Columns.Remove("GName")
    '            End If
    '            Dim strGrpCols As String() = {"employee"}
    '            strarrGroupColumns = strGrpCols
    '        End If

    '        row = New WorksheetRow()

    '        If Me._FilterTitle.ToString.Length > 0 Then
    '            wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
    '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
    '            row.Cells.Add(wcell)
    '        End If
    '        rowsArrayHeader.Add(row)

    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell("", "s10bw")
    '        row.Cells.Add(wcell)
    '        rowsArrayHeader.Add(row)


    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell("", "s10bw")
    '        row.Cells.Add(wcell)
    '        rowsArrayFooter.Add(row)
    '        '--------------------

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Checked By :"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Approved By"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Received By :"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        '--------------------

    '        'SET EXCEL CELL WIDTH  
    '        Dim intArrayColumnWidth As Integer() = Nothing
    '        ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '            intArrayColumnWidth(i) = 125
    '        Next
    '        'SET EXCEL CELL WIDTH


    '        mdtTableExcel.Columns("formno").Caption = Language.getMessage(mstrModuleName, 1, "Form No")
    '        mdtTableExcel.Columns("leavetype").Caption = Language.getMessage(mstrModuleName, 2, "Leave Type")
    '        mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 4, "Employee")
    '        mdtTableExcel.Columns("jobtitle").Caption = Language.getMessage(mstrModuleName, 5, "Job Title")
    '        mdtTableExcel.Columns("startdate").Caption = Language.getMessage(mstrModuleName, 9, "Start Date")
    '        mdtTableExcel.Columns("enddate").Caption = Language.getMessage(mstrModuleName, 10, "End Date")
    '        mdtTableExcel.Columns("days").Caption = Language.getMessage(mstrModuleName, 29, "Applied Days")
    '        mdtTableExcel.Columns("app_startdate").Caption = Language.getMessage(mstrModuleName, 11, "Approved Start Date")
    '        mdtTableExcel.Columns("app_enddate").Caption = Language.getMessage(mstrModuleName, 12, "Approved End Date")
    '        mdtTableExcel.Columns("balance").Caption = Language.getMessage(mstrModuleName, 27, "Balance")
    '        mdtTableExcel.Columns("issued_days").Caption = Language.getMessage(mstrModuleName, 13, "Issued Days")
    '        mdtTableExcel.Columns("remaining_balance").Caption = Language.getMessage(mstrModuleName, 28, "Remaining Balance")
    '        mdtTableExcel.Columns("status").Caption = Language.getMessage(mstrModuleName, 26, "Status")
    '        mdtTableExcel.Columns("variance").Caption = Language.getMessage(mstrModuleName, 30, "Variance")

    '        'Me._Repor0tName = mstrReportTypeName

    '        'Pinkal (19-Jun-2014) -- Start
    '        'Enhancement : FINCA CONGO CHANGES [DISPALYING SUBCOUNT,GRAND TOTAL AND RECORD COUNT ON ANLYSIS BY]  GIVEN BY DENNIS ON URGENT BASIS 
    '        'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)
    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)
    '        'Pinkal (19-Jun-2014) -- End



    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_IssuedLeaveStatusReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    Public Sub Generate_IssuedLeaveStatusReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                     , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                     , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try


            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objDataOperation = New clsDataOperation



            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            objDataOperation.ClearParameters()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))
            'Pinkal (11-Sep-2020) -- End




            StrQ = "SELECT ISNULL(formno, '') AS formno " & _
                      ", ISNULL(hremployee_master.employeecode, '')  + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
                      ", ISNULL(lvleavetype_master.leavename, '') AS leavetype " & _
                      ", ISNULL(jb.job_name, '') AS jobtitle "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= ", CONVERT(VARCHAR(max),lvleaveform.startdate) AS startdate " & _
                         ", CONVERT(VARCHAR(max),lvleaveform.returndate) AS enddate " & _
                         ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
                         "  AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
                         ", CONVERT(VARCHAR(max),lvleaveform.approve_stdate) AS app_startdate " & _
                         ", CONVERT(VARCHAR(max),lvleaveform.approve_eddate) AS app_enddate " & _
                         ", Convert(Varchar(Max),CAST(0.00 AS decimal)) balance " & _
                         ", ISNULL(lvleaveIssue_master.issuecount, 0.00) - ISNULL(lvleaveIssue_master.cancelcount, 0.00) AS issued_days " & _
                         ", Convert(Varchar(Max),CAST(0.00 AS decimal)) AS remaining_balance " & _
                         ", CASE WHEN  lvleaveform.statusunkid = 7 THEN @Issued END AS status " & _
                         ", 0.00 AS variance " & _
                         ", lvleaveform.formunkid " & _
                         ", lvleaveform.employeeunkid " & _
                         ", lvleaveform.leavetypeunkid " & _
                         ", ISNULL(lvleaveform.remark,'') AS Remark " & _
                         ",ISNULL(lvleavebalance_tran.accrue_amount,0.00) AS Accrue_amount " & _
                         " FROM lvleaveform " & _
                         " LEFT JOIN lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = lvleaveform.employeeunkid AND lvleavebalance_tran.leavetypeunkid = lvleaveform.leavetypeunkid " & _
                         " AND lvleavebalance_tran.isvoid = 0 "

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= " AND lvleavebalance_tran.isopenelc = 1 "
            End If

            StrQ &= " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid "


            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         jobunkid " & _
                        "        ,jobgroupunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                        ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                         " LEFT JOIN hrjob_master jb ON jb.jobunkid  = Jobs.jobunkid " & _
                         " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
                         " JOIN lvleaveIssue_master ON lvleaveIssue_master.employeeunkid = lvleaveform.employeeunkid  " & _
                         " AND lvleaveIssue_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleaveform.formunkid =  lvleaveIssue_master.formunkid  " & _
                         " AND lvleaveIssue_master.isvoid = 0 "

            'Pinkal (11-Sep-2020) -- End


            'Pinkal (26-Oct-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    StrQ &= " LEFT JOIN lvleavebalance_tran on lvleavebalance_tran.employeeunkid = lvleaveform.employeeunkid AND lvleavebalance_tran.leavetypeunkid = lvleaveform.leavetypeunkid  AND lvleavebalance_tran.isvoid = 0"
            'End If
            'Pinkal (26-Oct-2020) -- End


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If



            StrQ &= " WHERE  lvleaveform.isvoid = 0 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If


            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If




            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                'Pinkal (30-Oct-2019) -- Start
                'Defect Hill Packaging [0004243] - Worked On mployees not showing in the issued leave report.
                'StrQ &= " AND CONVERT(char(8),lvleaveform.startdate,112) >= convert(char(8),lvleavebalance_tran.startdate,112) " & _
                '             " AND convert(char(8),lvleaveform.returndate,112) <= CONVERT(char(8),lvleavebalance_tran.enddate,112) "

                StrQ &= " AND (CONVERT(char(8),lvleaveform.startdate,112) BETWEEN  CONVERT(char(8),lvleavebalance_tran.startdate,112) AND CONVERT(char(8),lvleavebalance_tran.enddate,112)  " & _
                             " OR convert(char(8),lvleaveform.returndate,112) BETWEEN CONVERT(char(8),lvleavebalance_tran.startdate,112) AND  CONVERT(char(8),lvleavebalance_tran.enddate,112)) "

                'Pinkal (30-Oct-2019) -- Start

            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0)



            Dim intEmpId As Integer = 0
            Dim dsBalance As DataSet = Nothing
            Dim objLeaveBal As New clsleavebalance_tran
            Dim mdclBalance As Double = 0
            For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
                If intEmpId <> CInt(mdtTableExcel.Rows(i)("employeeunkid")) Then
                    mdclBalance = 0


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                    'If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    '    dsBalance = objLeaveBal.GetList("Balance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                    '                                                    , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive, True _
                    '                                                    , False, CInt(mdtTableExcel.Rows(i)("employeeunkid")), False, False, False _
                    '                                                    , " yearunkid = " & mintYearId & " AND lvleavebalance_tran.leavetypeunkid = " & mintLeaveId, Nothing)
                    'ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    'dsBalance = objLeaveBal.GetList("Balance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                    '                                                , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, xOnlyApproved, True, False _
                    '                                                , CInt(mdtTableExcel.Rows(i)("employeeunkid")), True, True, False _
                    '                                                , "yearunkid = " & mintYearId & " AND lvleavebalance_tran.leavetypeunkid = " & mintLeaveId, Nothing)
                    'End If

                    'If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    '    mdtTableExcel.Rows(i)("balance") = Math.Round(CDbl(dsBalance.Tables(0).Rows(0)("Accrue_amount")), 2)
                    '    mdclBalance = Math.Round(CDbl(dsBalance.Tables(0).Rows(0)("Accrue_amount")), 2)
                    'End If

                    mdtTableExcel.Rows(i)("balance") = Math.Round(CDbl(mdtTableExcel.Rows(i)("Accrue_amount")), 2)
                    mdclBalance = Math.Round(CDbl(mdtTableExcel.Rows(i)("Accrue_amount")), 2)
                    'Pinkal (11-Sep-2020) -- End

                    intEmpId = CInt(mdtTableExcel.Rows(i)("employeeunkid"))
                End If


                'Pinkal (19-Jun-2014) -- Start
                'Enhancement : FINCA CONGO CHANGES [DISPALYING SUBCOUNT,GRAND TOTAL AND RECORD COUNT ON ANLYSIS BY]  GIVEN BY DENNIS ON URGENT BASIS 
                'mdtTableExcel.Rows(i)("balance") = Math.Round(mdclBalance, 2)
                'mdclBalance = CDbl(mdtTableExcel.Rows(i)("remaining_balance"))
                'mdtTableExcel.Rows(i)("remaining_balance") = CDbl(mdtTableExcel.Rows(i)("remaining_balance")).ToString("#0.00")


                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

                mdtTableExcel.Rows(i)("balance") = mdclBalance.ToString("#0.00")
                mdtTableExcel.Rows(i)("remaining_balance") = Math.Round(mdclBalance - CDbl(mdtTableExcel.Rows(i)("issued_days")), 2).ToString("#0.00")
                mdclBalance = CDbl(mdtTableExcel.Rows(i)("remaining_balance"))

                mdtTableExcel.Rows(i)("variance") = CDec(mdtTableExcel.Rows(i)("days")) - CDec(mdtTableExcel.Rows(i)("issued_days"))

                'Pinkal (21-Jul-2014) -- End

                'Pinkal (19-Jun-2014) -- End

                mdtTableExcel.Rows(i)("startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("startdate")), DateFormat.ShortDate)
                mdtTableExcel.Rows(i)("enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("enddate")), DateFormat.ShortDate)
                mdtTableExcel.Rows(i)("app_startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_startdate")), DateFormat.ShortDate)
                mdtTableExcel.Rows(i)("app_enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_enddate")), DateFormat.ShortDate)
            Next

            mdtTableExcel.AcceptChanges()

            If mdtTableExcel.Columns.Contains("formunkid") Then
                mdtTableExcel.Columns.Remove("formunkid")
            End If

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mdtTableExcel.Columns.Contains("leavetypeunkid") Then
                mdtTableExcel.Columns.Remove("leavetypeunkid")
            End If


            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName", "employee"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
                Dim strGrpCols As String() = {"employee"}
                strarrGroupColumns = strGrpCols
            End If

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------


            If ConfigParameter._Object._IsShowPreparedBy = True Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                row = New WorksheetRow()
                'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing
                'Pinkal (24-Aug-2015) -- End

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("formno").Caption = Language.getMessage(mstrModuleName, 1, "Form No")
            mdtTableExcel.Columns("leavetype").Caption = Language.getMessage(mstrModuleName, 2, "Leave Type")
            mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 4, "Employee")
            mdtTableExcel.Columns("jobtitle").Caption = Language.getMessage(mstrModuleName, 5, "Job Title")
            mdtTableExcel.Columns("startdate").Caption = Language.getMessage(mstrModuleName, 9, "Start Date")
            mdtTableExcel.Columns("enddate").Caption = Language.getMessage(mstrModuleName, 10, "End Date")
            mdtTableExcel.Columns("days").Caption = Language.getMessage(mstrModuleName, 29, "Applied Days")
            mdtTableExcel.Columns("app_startdate").Caption = Language.getMessage(mstrModuleName, 11, "Approved Start Date")
            mdtTableExcel.Columns("app_enddate").Caption = Language.getMessage(mstrModuleName, 12, "Approved End Date")
            mdtTableExcel.Columns("balance").Caption = Language.getMessage(mstrModuleName, 27, "Balance")
            mdtTableExcel.Columns("issued_days").Caption = Language.getMessage(mstrModuleName, 13, "Issued Days")
            mdtTableExcel.Columns("remaining_balance").Caption = Language.getMessage(mstrModuleName, 28, "Remaining Balance")
            mdtTableExcel.Columns("status").Caption = Language.getMessage(mstrModuleName, 26, "Status")
            mdtTableExcel.Columns("variance").Caption = Language.getMessage(mstrModuleName, 30, "Variance")


            'Varsha Rana (13-Oct-2017) -- Start
            'Enhancement - RefNo: 98 -  leave issued report to include a remark column (this column to carry remarks left by applicant during leave application)
            mdtTableExcel.Columns("Remark").Caption = Language.getMessage(mstrModuleName, 31, "Application Remark")

            'Varsha Rana (13-Oct-2017) -- End

            'Me._Repor0tName = mstrReportTypeName

            'Pinkal (19-Jun-2014) -- Start
            'Enhancement : FINCA CONGO CHANGES [DISPALYING SUBCOUNT,GRAND TOTAL AND RECORD COUNT ON ANLYSIS BY]  GIVEN BY DENNIS ON URGENT BASIS 
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)
            'Pinkal (19-Jun-2014) -- End



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_IssuedLeaveStatusReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End


    'Public Sub Generate_EmpIssuedLeaveDetailReport()
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Try
    '        objDataOperation = New clsDataOperation

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))


    '        StrQ = "SELECT ISNULL(formno, '') AS formno " & _
    '                  ", ISNULL(lvleavetype_master.leavename, '') AS leavetype " & _
    '                  ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
    '                  ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
    '                  ", ISNULL(hrjob_master.job_name, '') AS jobtitle "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", '' AS GName "
    '        End If

    '        StrQ &= ", lvleaveform.startdate AS startdate " & _
    '                     ", lvleaveform.returndate AS enddate " & _
    '                     ", lvleaveform.approve_stdate AS app_startdate " & _
    '                     ", lvleaveform.approve_eddate AS app_enddate " & _
    '                     ", ISNULL(lvleaveIssue_master.issuecount, 0.00) - ISNULL(lvleaveIssue_master.cancelcount, 0.00) AS issued_days " & _
    '                     ", CASE WHEN  lvleaveform.statusunkid = 7 THEN @Issued END AS status " & _
    '                     " ,lvleaveform.formunkid " & _
    '                     ", lvleaveform.employeeunkid " & _
    '                     ", lvleaveform.leavetypeunkid " & _
    '                     " FROM lvleaveform " & _
    '                     " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
    '                     " LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '                     " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
    '                     " JOIN lvleaveIssue_master ON lvleaveIssue_master.employeeunkid = lvleaveform.employeeunkid  " & _
    '                     " AND lvleaveIssue_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleaveform.formunkid =  lvleaveIssue_master.formunkid  " & _
    '                     " AND lvleaveIssue_master.isvoid = 0 "


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Join
    '        End If


    '        StrQ &= " WHERE  lvleaveform.isvoid = 0 "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If


    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        Dim strarrGroupColumns As String() = Nothing
    '        Dim rowsArrayHeader As New ArrayList
    '        Dim rowsArrayFooter As New ArrayList
    '        Dim row As WorksheetRow
    '        Dim wcell As WorksheetCell

    '        Dim mdtTableExcel As DataTable = dsList.Tables(0)


    '        If mdtTableExcel.Columns.Contains("formunkid") Then
    '            mdtTableExcel.Columns.Remove("formunkid")
    '        End If

    '        If mdtTableExcel.Columns.Contains("employeeunkid") Then
    '            mdtTableExcel.Columns.Remove("employeeunkid")
    '        End If

    '        If mdtTableExcel.Columns.Contains("leavetypeunkid") Then
    '            mdtTableExcel.Columns.Remove("leavetypeunkid")
    '        End If


    '        If mintViewIndex > 0 Then
    '            If mdtTableExcel.Columns.Contains("Id") Then
    '                mdtTableExcel.Columns.Remove("Id")
    '            End If
    '            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
    '            Dim strGrpCols As String() = {"GName"}
    '            strarrGroupColumns = strGrpCols
    '        Else
    '            If mdtTableExcel.Columns.Contains("GName") Then
    '                mdtTableExcel.Columns.Remove("GName")
    '            End If
    '        End If

    '        row = New WorksheetRow()

    '        If Me._FilterTitle.ToString.Length > 0 Then
    '            wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
    '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
    '            row.Cells.Add(wcell)
    '        End If
    '        rowsArrayHeader.Add(row)

    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell("", "s10bw")
    '        row.Cells.Add(wcell)
    '        rowsArrayHeader.Add(row)


    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell("", "s10bw")
    '        row.Cells.Add(wcell)
    '        rowsArrayFooter.Add(row)
    '        '--------------------

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Checked By :"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Approved By"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Received By :"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        '--------------------



    '        'SET EXCEL CELL WIDTH  
    '        Dim intArrayColumnWidth As Integer() = Nothing
    '        ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '            intArrayColumnWidth(i) = 125
    '        Next
    '        'SET EXCEL CELL WIDTH


    '        mdtTableExcel.Columns("employeecode").SetOrdinal(0)
    '        mdtTableExcel.Columns("employee").SetOrdinal(1)
    '        mdtTableExcel.Columns("jobtitle").SetOrdinal(2)
    '        mdtTableExcel.Columns("formno").SetOrdinal(3)
    '        mdtTableExcel.Columns("leavetype").SetOrdinal(4)
    '        mdtTableExcel.Columns("startdate").SetOrdinal(5)
    '        mdtTableExcel.Columns("enddate").SetOrdinal(6)
    '        mdtTableExcel.Columns("app_startdate").SetOrdinal(7)
    '        mdtTableExcel.Columns("app_enddate").SetOrdinal(8)
    '        mdtTableExcel.Columns("issued_days").SetOrdinal(9)
    '        mdtTableExcel.Columns("status").SetOrdinal(10)

    '        mdtTableExcel.Columns("formno").Caption = Language.getMessage(mstrModuleName, 1, "Form No")
    '        mdtTableExcel.Columns("leavetype").Caption = Language.getMessage(mstrModuleName, 2, "Leave Type")
    '        mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 3, "Employee Code")
    '        mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 4, "Employee")
    '        mdtTableExcel.Columns("jobtitle").Caption = Language.getMessage(mstrModuleName, 5, "Job Title")
    '        mdtTableExcel.Columns("startdate").Caption = Language.getMessage(mstrModuleName, 9, "Start Date")
    '        mdtTableExcel.Columns("enddate").Caption = Language.getMessage(mstrModuleName, 10, "End Date")
    '        mdtTableExcel.Columns("app_startdate").Caption = Language.getMessage(mstrModuleName, 11, "Approved Start Date")
    '        mdtTableExcel.Columns("app_enddate").Caption = Language.getMessage(mstrModuleName, 12, "Approved End Date")
    '        mdtTableExcel.Columns("issued_days").Caption = Language.getMessage(mstrModuleName, 13, "Issued Days")
    '        mdtTableExcel.Columns("status").Caption = Language.getMessage(mstrModuleName, 26, "Status")

    '        Me._ReportName = mstrReportTypeName

    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_EmpIssuedLeaveDetailReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Form No")
            Language.setMessage(mstrModuleName, 2, "Leave Type")
            Language.setMessage(mstrModuleName, 3, "Employee Code")
            Language.setMessage(mstrModuleName, 4, "Employee")
            Language.setMessage(mstrModuleName, 5, "Job Title")
            Language.setMessage(mstrModuleName, 9, "Start Date")
            Language.setMessage(mstrModuleName, 10, "End Date")
            Language.setMessage(mstrModuleName, 11, "Approved Start Date")
            Language.setMessage(mstrModuleName, 12, "Approved End Date")
            Language.setMessage(mstrModuleName, 13, "Issued Days")
            Language.setMessage(mstrModuleName, 14, "From Date:")
            Language.setMessage(mstrModuleName, 15, "To Date:")
            Language.setMessage(mstrModuleName, 16, "Employee:")
            Language.setMessage(mstrModuleName, 17, "Leave Type:")
            Language.setMessage(mstrModuleName, 20, " Leave Form:")
            Language.setMessage(mstrModuleName, 21, " Order By :")
            Language.setMessage(mstrModuleName, 22, "Prepared By :")
            Language.setMessage(mstrModuleName, 23, "Checked By :")
            Language.setMessage(mstrModuleName, 24, "Approved By")
            Language.setMessage(mstrModuleName, 25, "Received By :")
            Language.setMessage(mstrModuleName, 26, "Status")
            Language.setMessage(mstrModuleName, 27, "Balance")
            Language.setMessage(mstrModuleName, 28, "Remaining Balance")
            Language.setMessage(mstrModuleName, 29, "Applied Days")
            Language.setMessage(mstrModuleName, 30, "Variance")
            Language.setMessage(mstrModuleName, 31, "Application Remark")
            Language.setMessage("clsMasterData", 277, "Issued")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
