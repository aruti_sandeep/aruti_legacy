'************************************************************************************************************************************
'Class Name : clsLeaveStatement_Report.vb
'Purpose    :
'Date       : 07/01/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsLeaveStatement_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeaveStatement_Report"
    Private mstrReportId As String = enArutiReport.LeaveStatementReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mdtAsOnDate As Date = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mstrEmployeeCode As String = String.Empty
    Private mintDepartmentID As Integer = 0
    Private mstrDepartment As String = ""
    Private mintJobID As Integer = 0
    Private mstrJob As String = ""
    Private mintLeaveId As Integer = 0
    Private mstrLeaveName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mintLeaveBalanceSetting As Integer = -1
    Dim mstrLeaveTypeID As String = ""
    Dim mintDeductedLeaveTypeAccrueAmt As Decimal = 0
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Pinkal (24-May-2014) -- Start
    'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    Private mintYearId As Integer = 0
    'Pinkal (24-May-2014) -- End

    Private mdtdbStartDate As DateTime = Nothing
    Private mdtdbEndDate As DateTime = Nothing
    Private mblnGetDataFromPreviousYear As Boolean = False

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Private mintLeaveAccrueTenureSetting As Integer = enLeaveAccrueTenureSetting.Yearly
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    'Pinkal (18-Nov-2016) -- End

    'S.SANDEEP |11-APR-2019| -- START
    Private mblmShowAdjustmentRemark As Boolean = False
    'S.SANDEEP |11-APR-2019| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentID = value
        End Set
    End Property

    Public WriteOnly Property _Department() As String
        Set(ByVal value As String)
            mstrDepartment = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobID = value
        End Set
    End Property

    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJob = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property


    'Pinkal (24-May-2014) -- Start
    'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    'Pinkal (24-May-2014) -- End


    ''' <summary>
    ''' Purpose: Get or Set DBStartdate
    ''' Modify By: Pinkal 
    ''' </summary>
    ''' 
    Public Property _DBStartdate() As DateTime
        Get
            Return mdtdbStartDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DBEnddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DBEnddate() As DateTime
        Get
            Return mdtdbEndDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbEndDate = value
        End Set
    End Property


    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    'Pinkal (18-Nov-2016) -- End


    'S.SANDEEP |11-APR-2019| -- START
    Public WriteOnly Property _ShowAdjustmentRemark() As Boolean
        Set(ByVal value As Boolean)
            mblmShowAdjustmentRemark = value
        End Set
    End Property
    'S.SANDEEP |11-APR-2019| -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtAsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
            mintEmployeeId = 0
            mintYearId = 0
            mstrEmployeeCode = ""
            mstrEmployeeName = ""
            mintLeaveId = 0
            mstrLeaveName = ""
            mstrOrderByQuery = ""
            'S.SANDEEP |11-APR-2019| -- START
            mblmShowAdjustmentRemark = False
            'S.SANDEEP |11-APR-2019| -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            Me._FilterQuery &= " AND CONVERT(CHAR(10),lvleaveform.startdate,112) <= @Date "
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate.Date))

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND lvleaveIssue_master.employeeunkid = @EmpId "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

            'Pinkal (25-Jul-2015) -- Start
            'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 

            If mintYearId > 0 AndAlso mblnGetDataFromPreviousYear = False Then
                Me._FilterQuery &= " AND  lvleaveIssue_master.leaveyearunkid = @yearunkid"
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
            End If

            'Pinkal (25-Jul-2015) -- End

            'Pinkal (24-May-2014) -- End


            If mintLeaveId > 0 Then

                'Pinkal (18-Jun-2013) -- Start
                'Enhancement : TRA Changes
                mstrLeaveTypeID = mintLeaveId
                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = mintLeaveId

                If objLeaveType._DeductFromLeaveTypeunkid <= 0 Then
                    '    mstrLeaveTypeID &= "," & objLeaveType._DeductFromLeaveTypeunkid
                    'Else
                    Dim dsLeaveType As DataSet = objLeaveType.GetList("LeaveType", True, False)
                    Dim drRow() As DataRow = dsLeaveType.Tables(0).Select("deductfromlvtypeunkid = " & mintLeaveId)

                    If drRow.Length > 0 Then

                        For i As Integer = 0 To drRow.Length - 1
                            mstrLeaveTypeID &= "," & CInt(drRow(i)("leavetypeunkid"))
                        Next
                    End If

                Else
                    mintDeductedLeaveTypeAccrueAmt = objLeaveType._MaxAmount
                End If


                'Me._FilterQuery &= " AND lvleaveform.leavetypeunkid = @leavetypeunkid "
                'objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveId)

                Me._FilterQuery &= " AND lvleaveform.leavetypeunkid  IN (" & mstrLeaveTypeID & ") AND lvleaveform.isvoid = 0"

                'Pinkal (18-Jun-2013) -- End


                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Leave Type : ") & " " & mstrLeaveName & " "
            End If

            'Pinkal (25-Jul-2015) -- Start
            'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
            If mblnGetDataFromPreviousYear = False Then
                Me._FilterQuery &= " ORDER BY lvleaveform.startdate"
            End If
            'Pinkal (25-Jul-2015) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (01-Feb-2014) -- Start
        '    'Enhancement : TRA Changes


        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    'Pinkal (01-Feb-2014) -- End


        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If


        '    'Pinkal (01-Feb-2014) -- Start
        '    'Enhancement : TRA Changes
        '    Rpt = objRpt
        '    'Pinkal (01-Feb-2014) -- End

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            'Pinkal (09-Mar-2019) -- Start
            'Issue No 0003545[AKFTZ] - Leave statement Report not showing the accrue Days for inactive employees.
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtTransferAsOnDate, xOnlyApproved, xUserModeSetting)
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xOnlyApproved, xUserModeSetting)
            'Pinkal (09-Mar-2019) -- End

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
            Rpt = objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        objDataOperation.ClearParameters()

    '        'Pinkal (25-Jul-2015) -- Start
    '        'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 

    '        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '            Dim objBalance As New clsleavebalance_tran
    '            Dim dsBalanceList As DataSet = objBalance.GetList("Balance", True, False, mintEmployeeId, True, True, False)

    '            If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then

    '                Dim exForce As Exception = Nothing
    '                Dim mstrPreviousDBName As String = ""
    '                Dim mintPreviousYearID As Integer = 0
    '                Dim mdtFYStartDate As Date = Nothing
    '                Dim mdtFYEndDate As Date = Nothing

    '                If CDate(dsBalanceList.Tables(0).Rows(0)("startdate")).Date < mdtdbStartDate Then

    '                    StrQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompanyUnkid & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(CDate(dsBalanceList.Tables(0).Rows(0)("startdate")).Date))
    '                    Dim dsDBName As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0 Then
    '                        mstrPreviousDBName = dsDBName.Tables(0).Rows(0)("database_name").ToString()
    '                        mdtFYStartDate = CDate(dsDBName.Tables(0).Rows(0)("start_date"))
    '                        mdtFYEndDate = CDate(dsDBName.Tables(0).Rows(0)("end_date"))
    '                        mintPreviousYearID = CInt(dsDBName.Tables(0).Rows(0)("yearunkid"))
    '                        mblnGetDataFromPreviousYear = True


    '                        StrQ = " SELECT " & _
    '                                   " lvleaveform.employeeunkid " & _
    '                                   ",ISNULL(hremployee_master.employeecode,'') employeecde " & _
    '                                   ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') Employee " & _
    '                                   ",lvleaveform.leavetypeunkid " & _
    '                                   ",lvleavetype_master.leavename " & _
    '                                   ",lvleaveform.formunkid " & _
    '                                   ",lvleaveform.formno  " & _
    '                                   ",lvleaveform.startdate AS LeaveStartdate " & _
    '                                   ",lvleaveform.returndate AS LeaveEnddate " & _
    '                               ",lvleaveform.approve_stdate " & _
    '                               ",lvleaveform.approve_eddate " & _
    '                                   ",lvleavebalance_tran.startdate AS AccrueStartdate " & _
    '                                   ",lvleavebalance_tran.enddate AS AccrueEnddate " & _
    '                                   ",ISNULL(lvleavebalance_tran.actualamount,0) AS ActualAmount " & _
    '                                   ",ISNULL(lvleavebalance_tran.accrue_amount,0) AS AccrueAmount " & _
    '                                   ",ISNULL(lvleavebalance_tran.leavebf,0) AS LeaveBF " & _
    '                                   ",ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment " & _
    '                                                 ",ISNULL((SELECT SUM(dayfraction) FROM " & mstrPreviousDBName & "..lvleaveIssue_tran WHERE lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND isvoid = 0),0) AS Issued  " & _
    '                                                 " FROM " & mstrPreviousDBName & "..lvleaveform " & _
    '                                                 " LEFT JOIN " & mstrPreviousDBName & "..hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                 " LEFT JOIN " & mstrPreviousDBName & "..lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
    '                                                 " LEFT JOIN " & mstrPreviousDBName & "..lvleavebalance_tran ON lvleaveform.employeeunkid = lvleavebalance_tran.employeeunkid AND lvleavebalance_tran.leavetypeunkid=lvleaveform.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 " & _
    '                                                 " AND isopenelc = 1 AND iselc = 1  " & _
    '                                                 " LEFT JOIN " & mstrPreviousDBName & "..lvleaveIssue_master ON lvleaveIssue_master.formunkid = lvleaveform.formunkid AND lvleaveIssue_master.isvoid = 0 " & _
    '                                                 " WHERE lvleaveform.statusunkid = 7  AND hremployee_master.isapproved = 1   " & _
    '                                                  "AND CONVERT(CHAR(8), lvleaveform.approve_stdate, 112) BETWEEN CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) " & _
    '                                                  " AND  lvleaveIssue_master.leaveyearunkid = " & mintPreviousYearID
    '                        objDataOperation.ClearParameters()
    '                        Call FilterTitleAndFilterQuery()

    '                        StrQ &= Me._FilterQuery

    '                        StrQ &= " UNION "

    '                    End If

    '                End If

    '            End If
    '            objBalance = Nothing
    '        End If
    '        mblnGetDataFromPreviousYear = False
    '        'Pinkal (25-Jul-2015) -- End




    '        StrQ &= " SELECT " & _
    '                   " lvleaveform.employeeunkid " & _
    '                   ",ISNULL(hremployee_master.employeecode,'') employeecde " & _
    '                   ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') Employee " & _
    '                   ",lvleaveform.leavetypeunkid " & _
    '                   ",lvleavetype_master.leavename " & _
    '                   ",lvleaveform.formunkid " & _
    '                   ",lvleaveform.formno  " & _
    '                   ",lvleaveform.startdate AS LeaveStartdate " & _
    '                   ",lvleaveform.returndate AS LeaveEnddate " & _
    '                   ",lvleaveform.approve_stdate " & _
    '                   ",lvleaveform.approve_eddate " & _
    '                   ",lvleavebalance_tran.startdate AS AccrueStartdate " & _
    '                   ",lvleavebalance_tran.enddate AS AccrueEnddate " & _
    '                   ",ISNULL(lvleavebalance_tran.actualamount,0) AS ActualAmount " & _
    '                   ",ISNULL(lvleavebalance_tran.accrue_amount,0) AS AccrueAmount " & _
    '                   ",ISNULL(lvleavebalance_tran.leavebf,0) AS LeaveBF " & _
    '                   ",ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment " & _
    '                   ",ISNULL((SELECT SUM(dayfraction) FROM lvleaveIssue_tran WHERE lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND isvoid = 0),0) AS Issued  " & _
    '                   " FROM lvleaveform " & _
    '                   " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
    '                   " LEFT JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
    '                   " LEFT JOIN lvleavebalance_tran ON lvleaveform.employeeunkid = lvleavebalance_tran.employeeunkid AND lvleavebalance_tran.leavetypeunkid=lvleaveform.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 "

    '        'Pinkal (29-May-2015) -- Start 'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS. [ ",ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment " & _]


    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : Omna Changes  [LEAVE FORM DUPLICATION IN THIS REPORT DUE TO CONSIDERING CLOSE ELC AND OPEN ELC IN SAME YEAR ID]

    '        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= " AND isopenelc = 1 AND iselc = 1 "
    '        End If


    '        'Pinkal (06-Mar-2014) -- End

    '        StrQ &= " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.formunkid = lvleaveform.formunkid AND lvleaveIssue_master.isvoid = 0 " & _
    '                   " WHERE lvleaveform.statusunkid = 7 "


    '        StrQ &= " AND hremployee_master.isapproved = 1 "


    '        'Pinkal (21-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '            'Pinkal (28-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR VOLTAMP PREVIOUS YEAR LEAVE FORM NOT INCLUDED WHEN ELC IS ALREADY OPEN.

    '            'StrQ &= " AND CONVERT(CHAR(8), lvleaveform.startdate, 112) >= CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) " & _
    '            '             " AND CONVERT(CHAR(8), lvleaveform.returndate, 112) <= CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112)"


    '            'Pinkal (01-Dec-2014) -- Start
    '            'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .
    '            'StrQ &= " AND CONVERT(CHAR(8), lvleaveform.approve_eddate, 112) BETWEEN CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112)"
    '            StrQ &= " AND CONVERT(CHAR(8), lvleaveform.approve_stdate, 112) BETWEEN CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112)"
    '            'Pinkal (01-Dec-2014) -- End

    '            'Pinkal (28-Oct-2014) -- End

    '        End If
    '        'Pinkal (21-Jul-2014) -- End

    '        objDataOperation.ClearParameters()
    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        'Pinkal (01-May-2015) -- Start
    '        'Enhancement - CHANGE IN LEAVE STATEMENT REPORT AS PER MR.RUTTA'S COMMENT.

    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

    '            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '                Dim rpt_Row As DataRow
    '                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
    '                rpt_Row.Item("Column2") = dtRow.Item("employeecde")
    '                rpt_Row.Item("Column3") = dtRow.Item("Employee")
    '                rpt_Row.Item("Column4") = dtRow.Item("leavetypeunkid")
    '                rpt_Row.Item("Column5") = dtRow.Item("leavename")
    '                rpt_Row.Item("Column6") = dtRow.Item("formunkid")
    '                rpt_Row.Item("Column7") = dtRow.Item("formno")



    '                'Pinkal (15-Jul-2013) -- Start
    '                'Enhancement : TRA Changes

    '                'If Not IsDBNull(dtRow.Item("LeaveStartdate")) Then
    '                '    rpt_Row.Item("Column8") = CDate(dtRow.Item("LeaveStartdate")).ToShortDateString
    '                'End If

    '                'If Not IsDBNull(dtRow.Item("LeaveEnddate")) Then
    '                '    rpt_Row.Item("Column9") = CDate(dtRow.Item("LeaveEnddate")).ToShortDateString
    '                'End If

    '                If Not IsDBNull(dtRow.Item("approve_stdate")) Then
    '                    rpt_Row.Item("Column8") = CDate(dtRow.Item("approve_stdate")).ToShortDateString
    '                End If

    '                If Not IsDBNull(dtRow.Item("approve_eddate")) Then
    '                    rpt_Row.Item("Column9") = CDate(dtRow.Item("approve_eddate")).ToShortDateString
    '                End If

    '                'Pinkal (15-Jul-2013) -- End


    '                'Pinkal (18-Jun-2013) -- Start
    '                'Enhancement : TRA Changes

    '                Dim objBalance As New clsleavebalance_tran

    '                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then


    '                    'Pinkal (15-Jul-2013) -- Start
    '                    'Enhancement : TRA Changes
    '                    'Dim dsBalanceList As DataSet = objBalance.GetList("Balance", True, False, mintEmployeeId, False, False, False)
    '                    Dim dsBalanceList As DataSet = objBalance.GetList("Balance", True, False, mintEmployeeId, False, False, FinancialYear._Object._IsFin_Close)
    '                    'Pinkal (15-Jul-2013) -- End



    '                    Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")

    '                    If drRow.Length > 0 Then

    '                        'Pinkal (27-Jun-2013) -- Start
    '                        'Enhancement : TRA Changes

    '                        If Not IsDBNull(drRow(0)("startdate")) Then
    '                            rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
    '                            rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
    '                        Else
    '                            rpt_Row.Item("Column10") = FinancialYear._Object._Database_Start_Date.ToShortDateString
    '                            rpt_Row.Item("Column16") = FinancialYear._Object._Database_Start_Date.ToShortDateString
    '                        End If

    '                        If Not IsDBNull(drRow(0)("enddate")) Then
    '                            rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
    '                            rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
    '                        Else
    '                            rpt_Row.Item("Column11") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                            rpt_Row.Item("Column17") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                        End If

    '                        'Pinkal (27-Jun-2013) -- End


    '                        'Pinkal (14-Oct-2014) -- Start
    '                        'Enhancement - AKFTZ LEAVE BALANCE PROBLEM 
    '                        If IsDBNull(drRow(0)("leavebf")) = True OrElse CDec(drRow(0)("leavebf")) = 0 Then
    '                            rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                            rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                        Else
    '                            rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
    '                            rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                        End If
    '                        'Pinkal (14-Oct-2014) -- END

    '                        rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)
    '                    Else
    '                        rpt_Row.Item("Column10") = FinancialYear._Object._Database_Start_Date.ToShortDateString
    '                        rpt_Row.Item("Column11") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                        rpt_Row.Item("Column16") = FinancialYear._Object._Database_Start_Date.ToShortDateString
    '                        rpt_Row.Item("Column17") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                        rpt_Row.Item("Column81") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
    '                        rpt_Row.Item("Column82") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
    '                        rpt_Row.Item("Column83") = 0
    '                    End If


    '                    'If Not IsDBNull(dtRow.Item("AccrueStartdate")) Then
    '                    '    rpt_Row.Item("Column10") = CDate(dtRow.Item("AccrueStartdate")).ToShortDateString
    '                    'Else
    '                    '    rpt_Row.Item("Column10") = FinancialYear._Object._Database_Start_Date.ToShortDateString
    '                    'End If


    '                    'If Not IsDBNull(dtRow.Item("AccrueEnddate")) Then
    '                    '    rpt_Row.Item("Column11") = CDate(dtRow.Item("AccrueEnddate")).ToShortDateString
    '                    'Else
    '                    '    rpt_Row.Item("Column11") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                    'End If

    '                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '                    Dim dsBalanceList As DataSet = objBalance.GetList("Balance", True, False, mintEmployeeId, True, True, False)

    '                    Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")

    '                    If drRow.Length > 0 Then
    '                        rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
    '                        rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
    '                        rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
    '                        rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
    '                        rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
    '                        rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                        rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)
    '                    Else
    '                        Dim mdtStartdate As Date = Nothing
    '                        Dim mdtEnddate As Date = Nothing
    '                        Dim objEmployee As New clsEmployee_Master
    '                        objEmployee._Employeeunkid = mintEmployeeId


    '                        If objEmployee._Appointeddate.Date <> Nothing Then
    '                            mdtStartdate = objEmployee._Appointeddate.Date
    '                        End If

    '                        If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
    '                            mdtStartdate = objEmployee._Reinstatementdate.Date
    '                        End If

    '                        If mdtStartdate.Date < FinancialYear._Object._Database_Start_Date.Date Then
    '                            mdtStartdate = FinancialYear._Object._Database_Start_Date.Date
    '                        End If


    '                        'Pinkal (3-Aug-2013) -- Start
    '                        'Enhancement : TRA Changes

    '                        'If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date) Then
    '                        '    mdtEnddate = objEmployee._Termination_To_Date
    '                        'Else
    '                        '    mdtEnddate = mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date
    '                        'End If

    '                        If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)) Then
    '                            mdtEnddate = objEmployee._Termination_To_Date
    '                        Else
    '                            mdtEnddate = mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
    '                        End If

    '                        'Pinkal (3-Aug-2013) -- End

    '                        rpt_Row.Item("Column10") = mdtStartdate.ToShortDateString
    '                        rpt_Row.Item("Column11") = mdtEnddate.ToShortDateString
    '                        rpt_Row.Item("Column16") = mdtStartdate.ToShortDateString
    '                        rpt_Row.Item("Column17") = mdtEnddate.ToShortDateString
    '                        rpt_Row.Item("Column81") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
    '                        rpt_Row.Item("Column82") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
    '                        rpt_Row.Item("Column83") = 0
    '                    End If

    '                    'rpt_Row.Item("Column11") = CDate(dtRow.Item("AccrueEnddate")).ToShortDateString


    '                End If


    '                'Pinkal (04-Feb-2013) -- End


    '                'rpt_Row.Item("Column81") = Math.Round(CDec(dtRow.Item("ActualAmount")), 2)
    '                'rpt_Row.Item("Column82") = Math.Round(CDec(dtRow.Item("AccrueAmount")), 2)
    '                'rpt_Row.Item("Column83") = Math.Round(CDec(dtRow.Item("LeaveBF")), 2)
    '                rpt_Row.Item("Column84") = Math.Round(CDec(dtRow.Item("Issued")), 2)


    '                'Pinkal (29-May-2015) -- Start
    '                'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.
    '                rpt_Row.Item("Column85") = Math.Round(CDec(dtRow.Item("adjustment")), 2)
    '                'Pinkal (29-May-2015) -- End


    '                'Pinkal (06-Feb-2013) -- Start
    '                'Enhancement : TRA Changes

    '                'If Not IsDBNull(dtRow.Item("AccrueStartdate")) Then
    '                '    rpt_Row.Item("Column16") = CDate(dtRow.Item("AccrueStartdate")).ToShortDateString
    '                'End If

    '                'If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                '    If Not IsDBNull(dtRow.Item("AccrueEnddate")) Then
    '                '        rpt_Row.Item("Column17") = CDate(dtRow.Item("AccrueEnddate")).ToShortDateString
    '                '    Else
    '                '        rpt_Row.Item("Column17") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                '    End If
    '                'ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                '    rpt_Row.Item("Column17") = CDate(dtRow.Item("AccrueEnddate")).ToShortDateString

    '                'End If

    '                'Pinkal (06-Feb-2013) -- End

    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next

    '        Else
    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = mintEmployeeId
    '            rpt_Row.Item("Column2") = mstrEmployeeCode
    '            rpt_Row.Item("Column3") = mstrEmployeeName
    '            rpt_Row.Item("Column4") = mintLeaveId
    '            rpt_Row.Item("Column5") = mstrLeaveName
    '            rpt_Row.Item("Column6") = ""
    '            rpt_Row.Item("Column7") = ""
    '            rpt_Row.Item("Column8") = Nothing
    '            rpt_Row.Item("Column9") = Nothing

    '            Dim objBalance As New clsleavebalance_tran
    '            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                Dim dsBalanceList As DataSet = objBalance.GetList("Balance", True, False, mintEmployeeId, False, False, FinancialYear._Object._IsFin_Close)
    '                Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")
    '                If drRow.Length > 0 Then
    '                    If Not IsDBNull(drRow(0)("startdate")) Then
    '                        rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
    '                        rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
    '                    Else
    '                        rpt_Row.Item("Column10") = FinancialYear._Object._Database_Start_Date.ToShortDateString
    '                        rpt_Row.Item("Column16") = FinancialYear._Object._Database_Start_Date.ToShortDateString
    '                    End If

    '                    If Not IsDBNull(drRow(0)("enddate")) Then
    '                        rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
    '                        rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
    '                    Else
    '                        rpt_Row.Item("Column11") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                        rpt_Row.Item("Column17") = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                    End If

    '                    If IsDBNull(drRow(0)("leavebf")) = True OrElse CDec(drRow(0)("leavebf")) = 0 Then
    '                        rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                        rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                    Else
    '                        rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
    '                        rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                    End If

    '                    rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)

    '                End If

    '            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '                Dim dsBalanceList As DataSet = objBalance.GetList("Balance", True, False, mintEmployeeId, True, True, False)

    '                Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")

    '                If drRow.Length > 0 Then
    '                    rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
    '                    rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
    '                    rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
    '                    rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
    '                    rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
    '                    rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
    '                    rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)
    '                End If

    '            End If
    '            rpt_Row.Item("Column84") = 0

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '        End If

    '        'Pinkal (01-May-2015) -- End

    '        objRpt = New ArutiReport.Designer.rptLeaveStatement

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 3, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 4, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 5, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 6, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)
    '        Call ReportFunction.TextChange(objRpt, "txtAsonDateValue", mdtAsOnDate.ToShortDateString)
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeNameVal", mstrEmployeeName)
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeCodeVal", mstrEmployeeCode)
    '        Call ReportFunction.TextChange(objRpt, "txtDepartmentVal", mstrDepartment)
    '        Call ReportFunction.TextChange(objRpt, "txtJobVal", mstrJob)
    '        Call ReportFunction.TextChange(objRpt, "txtAsOnDate", Language.getMessage(mstrModuleName, 7, "Leave Report as on :"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 8, "Employee name :"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 9, "Employee code :"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveYear", Language.getMessage(mstrModuleName, 10, "Leave Cycle/Year :"))
    '        Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 11, "Description"))
    '        Call ReportFunction.TextChange(objRpt, "txtAccrueStartDate", Language.getMessage(mstrModuleName, 12, "Start Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtAccrueEndDate", Language.getMessage(mstrModuleName, 13, "End Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtAccrueDays", Language.getMessage(mstrModuleName, 14, "Accrued Days"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveBalBF", Language.getMessage(mstrModuleName, 15, "Leave Balance BF"))
    '        Call ReportFunction.TextChange(objRpt, "txtYearAccrue", Language.getMessage(mstrModuleName, 16, "Cycle/Year Accrue"))
    '        Call ReportFunction.TextChange(objRpt, "txtAccrueBalance", Language.getMessage(mstrModuleName, 17, "Total Balance :"))
    '        Call ReportFunction.TextChange(objRpt, "txtAccrueDetails", Language.getMessage(mstrModuleName, 18, "Leave Accrue Details"))
    '        Call ReportFunction.TextChange(objRpt, "txtTo", Language.getMessage(mstrModuleName, 19, "To"))
    '        Call ReportFunction.TextChange(objRpt, "txtIssuedDetail", Language.getMessage(mstrModuleName, 20, "Leave Issue Balance"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveForm", Language.getMessage(mstrModuleName, 21, "Leave Form No."))
    '        Call ReportFunction.TextChange(objRpt, "txtFormStartDate", Language.getMessage(mstrModuleName, 22, "Start Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtFormEndDate", Language.getMessage(mstrModuleName, 23, "End Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtIssueDays", Language.getMessage(mstrModuleName, 24, "Days Issued"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotalDaysIssued", Language.getMessage(mstrModuleName, 25, "Total Days Issued :"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotalLeaveBalance", Language.getMessage(mstrModuleName, 26, "Total Leave Balance :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 27, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 28, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 29, "Department :"))
    '        Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 30, "Job Title :"))

    '        'Pinkal (29-May-2015) -- Start
    '        'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveAdjustment", Language.getMessage(mstrModuleName, 31, "Leave Adjustment"))
    '        'Pinkal (29-May-2015) -- End


    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                Dim objBalance As New clsleavebalance_tran
                Dim dsBalanceList As DataSet = objBalance.GetList("Balance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                            , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved _
                                                                                            , False, True, True, False, False, mintEmployeeId, True, True, "", Nothing)

                If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then

                    Dim exForce As Exception = Nothing
                    Dim mstrPreviousDBName As String = ""
                    Dim mintPreviousYearID As Integer = 0
                    Dim mdtFYStartDate As Date = Nothing
                    Dim mdtFYEndDate As Date = Nothing

                    If CDate(dsBalanceList.Tables(0).Rows(0)("startdate")).Date < mdtdbStartDate Then

                        StrQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompanyUnkid & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(CDate(dsBalanceList.Tables(0).Rows(0)("startdate")).Date))
                        Dim dsDBName As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0 Then
                            mstrPreviousDBName = dsDBName.Tables(0).Rows(0)("database_name").ToString()
                            mdtFYStartDate = CDate(dsDBName.Tables(0).Rows(0)("start_date"))
                            mdtFYEndDate = CDate(dsDBName.Tables(0).Rows(0)("end_date"))
                            mintPreviousYearID = CInt(dsDBName.Tables(0).Rows(0)("yearunkid"))
                            mblnGetDataFromPreviousYear = True


                            StrQ = " SELECT " & _
                                       " lvleaveform.employeeunkid " & _
                                       ",ISNULL(hremployee_master.employeecode,'') employeecde " & _
                                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') Employee " & _
                                       ",lvleaveform.leavetypeunkid " & _
                                       ",lvleavetype_master.leavename " & _
                                       ",lvleaveform.formunkid " & _
                                       ",lvleaveform.formno  " & _
                                       ",lvleaveform.startdate AS LeaveStartdate " & _
                                       ",lvleaveform.returndate AS LeaveEnddate " & _
                                   ",lvleaveform.approve_stdate " & _
                                   ",lvleaveform.approve_eddate " & _
                                       ",lvleavebalance_tran.startdate AS AccrueStartdate " & _
                                       ",lvleavebalance_tran.enddate AS AccrueEnddate " & _
                                       ",ISNULL(lvleavebalance_tran.actualamount,0) AS ActualAmount " & _
                                       ",ISNULL(lvleavebalance_tran.accrue_amount,0) AS AccrueAmount " & _
                                       ",ISNULL(lvleavebalance_tran.leavebf,0) AS LeaveBF " & _
                                       ",ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment " & _
                                                     ",ISNULL((SELECT SUM(dayfraction) FROM " & mstrPreviousDBName & "..lvleaveIssue_tran WHERE lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND isvoid = 0),0) AS Issued  " & _
                                                     " FROM " & mstrPreviousDBName & "..lvleaveform " & _
                                                     " LEFT JOIN " & mstrPreviousDBName & "..hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
                                                     " LEFT JOIN " & mstrPreviousDBName & "..lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
                                                     " LEFT JOIN " & mstrPreviousDBName & "..lvleavebalance_tran ON lvleaveform.employeeunkid = lvleavebalance_tran.employeeunkid AND lvleavebalance_tran.leavetypeunkid=lvleaveform.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 " & _
                                                     " AND isopenelc = 1 AND iselc = 1  " & _
                                                     " LEFT JOIN " & mstrPreviousDBName & "..lvleaveIssue_master ON lvleaveIssue_master.formunkid = lvleaveform.formunkid AND lvleaveIssue_master.isvoid = 0 " & _
                                                     " WHERE lvleaveform.statusunkid = 7  AND hremployee_master.isapproved = 1   " & _
                                       " AND CONVERT(CHAR(8), lvleaveform.approve_stdate, 112) BETWEEN CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) " & _
                                                      " AND  lvleaveIssue_master.leaveyearunkid = " & mintPreviousYearID
                            objDataOperation.ClearParameters()
                            Call FilterTitleAndFilterQuery()

                            StrQ &= Me._FilterQuery

                            StrQ &= " UNION "

                        End If

                    End If

                End If
                objBalance = Nothing
            End If

            mblnGetDataFromPreviousYear = False


            StrQ &= " SELECT " & _
                       " lvleaveform.employeeunkid " & _
                       ",ISNULL(hremployee_master.employeecode,'') employeecde " & _
                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') Employee " & _
                       ",lvleaveform.leavetypeunkid " & _
                       ",lvleavetype_master.leavename " & _
                       ",lvleaveform.formunkid " & _
                       ",lvleaveform.formno  " & _
                       ",lvleaveform.startdate AS LeaveStartdate " & _
                       ",lvleaveform.returndate AS LeaveEnddate " & _
                       ",lvleaveform.approve_stdate " & _
                       ",lvleaveform.approve_eddate " & _
                       ",lvleavebalance_tran.startdate AS AccrueStartdate " & _
                       ",lvleavebalance_tran.enddate AS AccrueEnddate " & _
                       ",ISNULL(lvleavebalance_tran.actualamount,0) AS ActualAmount " & _
                       ",ISNULL(lvleavebalance_tran.accrue_amount,0) AS AccrueAmount " & _
                       ",ISNULL(lvleavebalance_tran.leavebf,0) AS LeaveBF " & _
                       ",ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment " & _
                       ",ISNULL((SELECT SUM(dayfraction) FROM lvleaveIssue_tran WHERE lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND isvoid = 0),0) AS Issued  " & _
                       " FROM lvleaveform " & _
                       " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
                       " LEFT JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
                       " LEFT JOIN lvleavebalance_tran ON lvleaveform.employeeunkid = lvleavebalance_tran.employeeunkid AND lvleavebalance_tran.leavetypeunkid=lvleaveform.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 "

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= " AND isopenelc = 1 AND iselc = 1 "
            End If

            StrQ &= " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.formunkid = lvleaveform.formunkid AND lvleaveIssue_master.isvoid = 0 " & _
                       " WHERE lvleaveform.statusunkid = 7 "

            StrQ &= " AND hremployee_master.isapproved = 1 "

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= " AND CONVERT(CHAR(8), lvleaveform.approve_stdate, 112) BETWEEN CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112)"
            End If

            objDataOperation.ClearParameters()
            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ = StrQ.Replace("#leavetypeIds#", mstrLeaveTypeID)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'S.SANDEEP |11-APR-2019| -- START
            Dim strRemarks As String = ""
            Dim objlvAdj As New clsleaveadjustment_Tran
            strRemarks = objlvAdj.GetAdustmentRemarkOnStatementReport(mintEmployeeId, mstrLeaveTypeID, mintLeaveBalanceSetting)
            objlvAdj = Nothing
            'S.SANDEEP |11-APR-2019| -- END


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                    rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                    rpt_Row.Item("Column2") = dtRow.Item("employeecde")
                    rpt_Row.Item("Column3") = dtRow.Item("Employee")
                    rpt_Row.Item("Column4") = dtRow.Item("leavetypeunkid")
                    rpt_Row.Item("Column5") = dtRow.Item("leavename")
                    rpt_Row.Item("Column6") = dtRow.Item("formunkid")

                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    'rpt_Row.Item("Column7") = dtRow.Item("formno")
                    rpt_Row.Item("Column7") = dtRow.Item("formno") & " (" & dtRow.Item("leavename").ToString() & ")"
                    'Pinkal (03-May-2019) -- End

                    'S.SANDEEP |11-APR-2019| -- START
                    rpt_Row.Item("Column12") = strRemarks
                    'S.SANDEEP |11-APR-2019| -- END

                    If Not IsDBNull(dtRow.Item("approve_stdate")) Then
                        rpt_Row.Item("Column8") = CDate(dtRow.Item("approve_stdate")).ToShortDateString
                    End If

                    If Not IsDBNull(dtRow.Item("approve_eddate")) Then
                        rpt_Row.Item("Column9") = CDate(dtRow.Item("approve_eddate")).ToShortDateString
                    End If


                    Dim objBalance As New clsleavebalance_tran

                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                        Dim dsBalanceList As DataSet = objBalance.GetList("Balance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                                    , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved _
                                                                                                    , False, True, True, False, mintEmployeeId, False, False, FinancialYear._Object._IsFin_Close, "", Nothing)

                        Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")

                        If drRow.Length > 0 Then

                            If Not IsDBNull(drRow(0)("startdate")) Then
                                rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
                                rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
                            Else
                                rpt_Row.Item("Column10") = FinancialYear._Object._Database_Start_Date.ToShortDateString
                                rpt_Row.Item("Column16") = FinancialYear._Object._Database_Start_Date.ToShortDateString
                            End If

                            If Not IsDBNull(drRow(0)("enddate")) Then
                                rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
                                rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
                            Else
                                rpt_Row.Item("Column11") = FinancialYear._Object._Database_End_Date.ToShortDateString
                                rpt_Row.Item("Column17") = FinancialYear._Object._Database_End_Date.ToShortDateString
                            End If


                            If IsDBNull(drRow(0)("leavebf")) = True OrElse CDec(drRow(0)("leavebf")) = 0 Then
                                'Pinkal (07-Jan-2019) -- Start
                                'Bug - Display problem in short Leave when do adjustment and displaying wrong accrue amount for Voltamp.
                                If CBool(drRow(0)("isshortleave")) Then
                                    rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
                                Else
                                    rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                                End If
                                'Pinkal (07-Jan-2019) -- End
                                rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                            Else
                                rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
                                rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                            End If

                            rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)
                        Else
                            rpt_Row.Item("Column10") = FinancialYear._Object._Database_Start_Date.ToShortDateString
                            rpt_Row.Item("Column11") = FinancialYear._Object._Database_End_Date.ToShortDateString
                            rpt_Row.Item("Column16") = FinancialYear._Object._Database_Start_Date.ToShortDateString
                            rpt_Row.Item("Column17") = FinancialYear._Object._Database_End_Date.ToShortDateString
                            rpt_Row.Item("Column81") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
                            rpt_Row.Item("Column82") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
                            rpt_Row.Item("Column83") = 0
                        End If


                    ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                        Dim dsBalanceList As DataSet = objBalance.GetList("Balance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                                    , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved _
                                                                                                    , False, True, True, False, mintEmployeeId, True, True, False, "", Nothing)


                        Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")

                        If drRow.Length > 0 Then
                            rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
                            rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
                            rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
                            rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
                            rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
                            rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                            rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)
                        Else
                            Dim mdtStartdate As Date = Nothing
                            Dim mdtEnddate As Date = Nothing
                            Dim objEmployee As New clsEmployee_Master
                            objEmployee._Employeeunkid(mdtEmployeeAsonDate) = mintEmployeeId


                            If objEmployee._Appointeddate.Date <> Nothing Then
                                mdtStartdate = objEmployee._Appointeddate.Date
                            End If

                            If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
                                mdtStartdate = objEmployee._Reinstatementdate.Date
                            End If

                            If mdtStartdate.Date < FinancialYear._Object._Database_Start_Date.Date Then
                                mdtStartdate = FinancialYear._Object._Database_Start_Date.Date
                            End If


                            If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)) Then
                                mdtEnddate = objEmployee._Termination_To_Date
                            Else
                                mdtEnddate = mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
                            End If

                            rpt_Row.Item("Column10") = mdtStartdate.ToShortDateString
                            rpt_Row.Item("Column11") = mdtEnddate.ToShortDateString
                            rpt_Row.Item("Column16") = mdtStartdate.ToShortDateString
                            rpt_Row.Item("Column17") = mdtEnddate.ToShortDateString
                            rpt_Row.Item("Column81") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
                            rpt_Row.Item("Column82") = Math.Round(mintDeductedLeaveTypeAccrueAmt, 2)
                            rpt_Row.Item("Column83") = 0
                        End If

                        'rpt_Row.Item("Column11") = CDate(dtRow.Item("AccrueEnddate")).ToShortDateString


                    End If
                    rpt_Row.Item("Column84") = Math.Round(CDec(dtRow.Item("Issued")), 2)
                    rpt_Row.Item("Column85") = Math.Round(CDec(dtRow.Item("adjustment")), 2)

                    Dim dsLeabeBalanceList As DataSet = Nothing

                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsLeabeBalanceList = objBalance.GetLeaveBalanceInfo(mdtAsOnDate.Date, mintLeaveId, mintEmployeeId, mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtdbStartDate, mdtdbEndDate, False, False, mintLeaveBalanceSetting, -1, mintYearId)
                    ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        dsLeabeBalanceList = objBalance.GetLeaveBalanceInfo(mdtAsOnDate.Date, mintLeaveId, mintEmployeeId, mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtdbStartDate, mdtdbEndDate, True, True, mintLeaveBalanceSetting, -1, mintYearId)
                    End If

                    If dsLeabeBalanceList IsNot Nothing AndAlso dsLeabeBalanceList.Tables(0).Rows.Count > 0 Then
                        rpt_Row.Item("Column86") = Math.Round(CDec(dsLeabeBalanceList.Tables(0).Rows(0)("LeaveEncashment")), 2)
                    Else
                        rpt_Row.Item("Column86") = 0
                    End If

                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

            Else
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = mintEmployeeId
                rpt_Row.Item("Column2") = mstrEmployeeCode
                rpt_Row.Item("Column3") = mstrEmployeeName
                rpt_Row.Item("Column4") = mintLeaveId
                rpt_Row.Item("Column5") = mstrLeaveName
                rpt_Row.Item("Column6") = ""
                rpt_Row.Item("Column7") = ""
                rpt_Row.Item("Column8") = Nothing
                rpt_Row.Item("Column9") = Nothing
                'S.SANDEEP |11-APR-2019| -- START
                rpt_Row.Item("Column12") = strRemarks
                'S.SANDEEP |11-APR-2019| -- END

                Dim objBalance As New clsleavebalance_tran
                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    Dim dsBalanceList As DataSet = objBalance.GetList("Balance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                                , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved _
                                                                                                , False, True, True, False, mintEmployeeId, False, False, FinancialYear._Object._IsFin_Close, "", Nothing)

                    Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")
                    If drRow.Length > 0 Then
                        If Not IsDBNull(drRow(0)("startdate")) Then
                            rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
                            rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
                        Else
                            rpt_Row.Item("Column10") = FinancialYear._Object._Database_Start_Date.ToShortDateString
                            rpt_Row.Item("Column16") = FinancialYear._Object._Database_Start_Date.ToShortDateString
                        End If

                        If Not IsDBNull(drRow(0)("enddate")) Then
                            rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
                            rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
                        Else
                            rpt_Row.Item("Column11") = FinancialYear._Object._Database_End_Date.ToShortDateString
                            rpt_Row.Item("Column17") = FinancialYear._Object._Database_End_Date.ToShortDateString
                        End If

                        If IsDBNull(drRow(0)("leavebf")) = True OrElse CDec(drRow(0)("leavebf")) = 0 Then

                            'Pinkal (07-Jan-2019) -- Start
                            'Bug - Display problem in short Leave when do adjustment and displaying wrong accrue amount for Voltamp.
                            If CBool(drRow(0)("isshortleave")) Then
                                rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
                            Else
                                rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                            End If
                            'Pinkal (07-Jan-2019) -- End

                            rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                        Else
                            rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
                            rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                        End If

                        rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)


                        'Pinkal (15-Feb-2018) -- Start
                        'Bug - 0002019- Wrong balances getting displayed on leave statement report.
                        rpt_Row.Item("Column85") = Math.Round(CDec(drRow(0)("adj_remaining_bal")), 2)
                        'Pinkal (15-Feb-2018) -- End


                        'Pinkal (10-Oct-2018) -- Start
                        'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.
                        Dim dsLeabeBalanceList As DataSet = objBalance.GetLeaveBalanceInfo(mdtAsOnDate.Date, mintLeaveId, mintEmployeeId, mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtdbStartDate, mdtdbEndDate, False, False, mintLeaveBalanceSetting, -1, mintYearId)

                        If dsLeabeBalanceList IsNot Nothing AndAlso dsLeabeBalanceList.Tables(0).Rows.Count > 0 Then
                            rpt_Row.Item("Column86") = Math.Round(CDec(dsLeabeBalanceList.Tables(0).Rows(0)("LeaveEncashment")), 2)
                        Else
                            rpt_Row.Item("Column86") = 0
                        End If

                        'Pinkal (10-Oct-2018) -- End


                    End If

                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                    Dim dsBalanceList As DataSet = objBalance.GetList("Balance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                                 , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved _
                                                                                                 , False, True, True, False, mintEmployeeId, True, True, False, "", Nothing)

                    Dim drRow() As DataRow = dsBalanceList.Tables(0).Select("leavetypeunkid in (" & mstrLeaveTypeID & ")", "leavetypeunkid asc")

                    If drRow.Length > 0 Then
                        rpt_Row.Item("Column10") = CDate(drRow(0)("startdate")).ToShortDateString
                        rpt_Row.Item("Column11") = CDate(drRow(0)("enddate")).ToShortDateString
                        rpt_Row.Item("Column16") = CDate(drRow(0)("startdate")).ToShortDateString
                        rpt_Row.Item("Column17") = CDate(drRow(0)("enddate")).ToShortDateString
                        rpt_Row.Item("Column81") = Math.Round(CDec(drRow(0)("ActualAmount")), 2)
                        rpt_Row.Item("Column82") = Math.Round(CDec(drRow(0)("accrue_amount")), 2)
                        rpt_Row.Item("Column83") = Math.Round(CDec(drRow(0)("leavebf")), 2)

                        'Pinkal (15-Feb-2018) -- Start
                        'Bug - 0002019- Wrong balances getting displayed on leave statement report.
                        rpt_Row.Item("Column85") = Math.Round(CDec(drRow(0)("adj_remaining_bal")), 2)
                        'Pinkal (15-Feb-2018) -- End


                        'Pinkal (10-Oct-2018) -- Start
                        'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.
                        Dim dsLeabeBalanceList As DataSet = objBalance.GetLeaveBalanceInfo(mdtAsOnDate.Date, mintLeaveId, mintEmployeeId, mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtdbStartDate, mdtdbEndDate, True, True, mintLeaveBalanceSetting, -1, mintYearId)
                        If dsLeabeBalanceList IsNot Nothing AndAlso dsLeabeBalanceList.Tables(0).Rows.Count > 0 Then
                            rpt_Row.Item("Column86") = Math.Round(CDec(dsLeabeBalanceList.Tables(0).Rows(0)("LeaveEncashment")), 2)
                        Else
                            rpt_Row.Item("Column86") = 0
                        End If
                        'Pinkal (10-Oct-2018) -- End


                    End If

                End If

                rpt_Row.Item("Column84") = 0

                'Pinkal (15-Feb-2018) -- Start
                'Bug - 0002019- Wrong balances getting displayed on leave statement report.
                'rpt_Row.Item("Column85") = 0
                'Pinkal (15-Feb-2018) -- End

                'Pinkal (10-Oct-2018) -- Start
                'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.
                'rpt_Row.Item("Column86") = 0
                'Pinkal (10-Oct-2018) -- End


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            End If


            objRpt = New ArutiReport.Designer.rptLeaveStatement

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 3, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 4, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 5, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 6, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            'S.SANDEEP |11-APR-2019| -- START
            If mblmShowAdjustmentRemark = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "PageHeaderSection2", True)
            End If
            'S.SANDEEP |11-APR-2019| -- END

            objRpt.SetDataSource(rpt_Data)
            Call ReportFunction.TextChange(objRpt, "txtAsonDateValue", mdtAsOnDate.ToShortDateString)
            Call ReportFunction.TextChange(objRpt, "txtEmployeeNameVal", mstrEmployeeName)
            Call ReportFunction.TextChange(objRpt, "txtEmployeeCodeVal", mstrEmployeeCode)
            Call ReportFunction.TextChange(objRpt, "txtDepartmentVal", mstrDepartment)
            Call ReportFunction.TextChange(objRpt, "txtJobVal", mstrJob)
            Call ReportFunction.TextChange(objRpt, "txtAsOnDate", Language.getMessage(mstrModuleName, 7, "Leave Report as on :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 8, "Employee name :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 9, "Employee code :"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveYear", Language.getMessage(mstrModuleName, 10, "Leave Cycle/Year :"))
            Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 11, "Description"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueStartDate", Language.getMessage(mstrModuleName, 12, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueEndDate", Language.getMessage(mstrModuleName, 13, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueDays", Language.getMessage(mstrModuleName, 14, "Accrued Days"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveBalBF", Language.getMessage(mstrModuleName, 15, "Leave Balance BF"))
            Call ReportFunction.TextChange(objRpt, "txtYearAccrue", Language.getMessage(mstrModuleName, 16, "Cycle/Year Accrue"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueBalance", Language.getMessage(mstrModuleName, 17, "Total Balance :"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueDetails", Language.getMessage(mstrModuleName, 18, "Leave Accrue Details"))
            Call ReportFunction.TextChange(objRpt, "txtTo", Language.getMessage(mstrModuleName, 19, "To"))
            Call ReportFunction.TextChange(objRpt, "txtIssuedDetail", Language.getMessage(mstrModuleName, 20, "Leave Issue Balance"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveForm", Language.getMessage(mstrModuleName, 21, "Leave Form No."))
            Call ReportFunction.TextChange(objRpt, "txtFormStartDate", Language.getMessage(mstrModuleName, 22, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtFormEndDate", Language.getMessage(mstrModuleName, 23, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtIssueDays", Language.getMessage(mstrModuleName, 24, "Days Issued"))
            Call ReportFunction.TextChange(objRpt, "txtTotalDaysIssued", Language.getMessage(mstrModuleName, 25, "Total Days Issued :"))
            Call ReportFunction.TextChange(objRpt, "txtTotalLeaveBalance", Language.getMessage(mstrModuleName, 26, "Total Leave Balance :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 27, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 28, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 29, "Department :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 30, "Job Title :"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveAdjustment", Language.getMessage(mstrModuleName, 31, "Leave Adjustment"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveEncashment", Language.getMessage(mstrModuleName, 32, "Total Encashment :"))

            'S.SANDEEP |11-APR-2019| -- START
            Call ReportFunction.TextChange(objRpt, "txtAdjustmentRemark", Language.getMessage(mstrModuleName, 33, "Adjustment Remark :"))
            'S.SANDEEP |11-APR-2019| -- END


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Leave Type :")
            Language.setMessage(mstrModuleName, 3, "Prepared By :")
            Language.setMessage(mstrModuleName, 4, "Checked By :")
            Language.setMessage(mstrModuleName, 5, "Approved By :")
            Language.setMessage(mstrModuleName, 6, "Received By :")
            Language.setMessage(mstrModuleName, 7, "Leave Report as on :")
            Language.setMessage(mstrModuleName, 8, "Employee name :")
            Language.setMessage(mstrModuleName, 9, "Employee code :")
            Language.setMessage(mstrModuleName, 10, "Leave Cycle/Year :")
            Language.setMessage(mstrModuleName, 11, "Description")
            Language.setMessage(mstrModuleName, 12, "Start Date")
            Language.setMessage(mstrModuleName, 13, "End Date")
            Language.setMessage(mstrModuleName, 14, "Accrued Days")
            Language.setMessage(mstrModuleName, 15, "Leave Balance BF")
            Language.setMessage(mstrModuleName, 16, "Cycle/Year Accrue")
            Language.setMessage(mstrModuleName, 17, "Total Balance :")
            Language.setMessage(mstrModuleName, 18, "Leave Accrue Details")
            Language.setMessage(mstrModuleName, 19, "To")
            Language.setMessage(mstrModuleName, 20, "Leave Issue Balance")
            Language.setMessage(mstrModuleName, 21, "Leave Form No.")
            Language.setMessage(mstrModuleName, 22, "Start Date")
            Language.setMessage(mstrModuleName, 23, "End Date")
            Language.setMessage(mstrModuleName, 24, "Days Issued")
            Language.setMessage(mstrModuleName, 25, "Total Days Issued :")
            Language.setMessage(mstrModuleName, 26, "Total Leave Balance :")
            Language.setMessage(mstrModuleName, 27, "Printed By :")
            Language.setMessage(mstrModuleName, 28, "Printed Date :")
            Language.setMessage(mstrModuleName, 29, "Department :")
            Language.setMessage(mstrModuleName, 30, "Job Title :")
            Language.setMessage(mstrModuleName, 31, "Leave Adjustment")
            Language.setMessage(mstrModuleName, 32, "Encashment")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
