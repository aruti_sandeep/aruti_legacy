'************************************************************************************************************************************
'Class Name : clsLeaveBalance_AT_Report.vb
'Purpose    :
'Date       : 25/02/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsLeaveBalance_AT_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeaveBalance_AT_Report"
    Private mstrReportId As String = enArutiReport.LeaveBalance_AT_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mdtAuditFromDate As DateTime = Nothing
    Private mdtAuditToDate As DateTime = Nothing

    Private mintEmployeeId As Integer = 0
    Private mintUserId As Integer = 0
    Private mintLeaveId As Integer = 0
    Private mintAuditTypeId As Integer = 0
    Private mintAccrueSetting As Integer = 0

    Private mstrEmployeeName As String = ""
    Private mstrUserName As String = ""
    Private mstrLeaveName As String = ""
    Private mstrAuditType As String = ""
    Private mstrAccrueSetting As String = ""



    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIncludeInactiveEmp As Boolean = False
    'Pinkal (11-MAY-2012) -- End

    Private mstrOrderByQuery As String = ""

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AudtiFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditFromDate = value
        End Set
    End Property

    Public WriteOnly Property _AuditToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _UserId() As Integer
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeId() As Integer
        Set(ByVal value As Integer)
            mintAuditTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _AccrueSettingId() As Integer
        Set(ByVal value As Integer)
            mintAccrueSetting = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _UName() As String
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public WriteOnly Property _AudtiType() As String
        Set(ByVal value As String)
            mstrAuditType = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property

    Public WriteOnly Property _AccrueSetting() As String
        Set(ByVal value As String)
            mstrAccrueSetting = value
        End Set
    End Property


    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'Pinkal (11-MAY-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""

            mdtAuditFromDate = Nothing
            mdtAuditToDate = Nothing

            mintUserId = 0
            mintAuditTypeId = 0
            mintLeaveId = 0
            mintEmployeeId = 0
            mintAccrueSetting = 0

            mstrEmployeeName = ""
            mstrUserName = ""
            mstrLeaveName = ""
            mstrAuditType = ""
            mstrAccrueSetting = ""

            mstrOrderByQuery = ""



            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            mblnIncludeInactiveEmp = False
            'Pinkal (11-MAY-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@AuditFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditFromDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, " Audit From Date: ") & " " & mdtAuditFromDate.ToShortDateString & " "

            objDataOperation.AddParameter("@AuditToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, " Audit To Date: ") & " " & mdtAuditToDate.ToShortDateString & " "

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND atlvleavebalance_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintUserId > 0 Then
                Me._FilterQuery &= " AND atlvleavebalance_tran.audituserunkid = @UserId "
                objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "User: ") & " " & mstrUserName & " "
            End If

            If mintLeaveId > 0 Then
                Me._FilterQuery &= " AND atlvleavebalance_tran.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Leave: ") & " " & mstrLeaveName & " "
            End If

            If mintAuditTypeId > 0 Then
                Me._FilterQuery &= " AND atlvleavebalance_tran.audittype = @AuditTypeId "
                objDataOperation.AddParameter("@AuditTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditTypeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, " Audit Type: ") & " " & mstrAuditType & " "
            End If


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'Pinkal (11-MAY-2012) -- End


            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'objRpt = Generate_DetailReport()
            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), xUserModeSetting, True)

            'S.SANDEEP [15 NOV 2016] -- START
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(xPeriodEnd), xUserModeSetting, True)
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, True)
            'S.SANDEEP [15 NOV 2016] -- END

            'Pinkal (06-Jan-2016) -- End


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("auditdate", Language.getMessage(mstrModuleName, 1, "Audit Date")))
            iColumn_DetailReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("LeaveName", Language.getMessage(mstrModuleName, 3, "Leave")))
            iColumn_DetailReport.Add(New IColumn("UserName", Language.getMessage(mstrModuleName, 4, "User")))
            iColumn_DetailReport.Add(New IColumn("Type", Language.getMessage(mstrModuleName, 5, "Audit Type")))
            iColumn_DetailReport.Add(New IColumn("Setting", Language.getMessage(mstrModuleName, 6, "Accrue Setting")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        objDataOperation.ClearParameters()

    '        objDataOperation.AddParameter("@add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Add"))
    '        objDataOperation.AddParameter("@edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Edit"))
    '        objDataOperation.AddParameter("@delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Delete"))
    '        objDataOperation.AddParameter("@paid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Paid Leave"))
    '        objDataOperation.AddParameter("@unpaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Unpaid Leave"))
    '        objDataOperation.AddParameter("@issuebal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Issue Balance"))
    '        objDataOperation.AddParameter("@exceedbal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Exceeding Leave Balance with Unpaid"))


    '        StrQ = " SELECT hremployee_master.employeecode,hremployee_master.firstname + ' ' + hremployee_master.surname Employee ," & _
    '                  " Leavename , CONVERT(CHAR(8), atlvleavebalance_tran.startdate, 112) startdate , " & _
    '                  " CONVERT(CHAR(8), atlvleavebalance_tran.enddate, 112) enddate , " & _
    '                  " CASE WHEN atlvleavebalance_tran.accruesetting = 1 THEN @issuebal " & _
    '                        " WHEN atlvleavebalance_tran.accruesetting = 2  THEN @exceedbal " & _
    '                  " End 'Setting', ISNULL(lvbatch_master.batchno, '') 'Batchno' , " & _
    '                  " convert(Decimal(10,2),atlvleavebalance_tran.daily_amount) as daily_amount , " & _
    '                  " convert(Decimal(10,2),atlvleavebalance_tran.accrue_amount) as accrue_amount  , " & _
    '                  " atlvleavebalance_tran.issue_amount , " & _
    '                  " convert(Decimal(10,2),atlvleavebalance_tran.remaining_bal) as remaining_bal , " & _
    '                  " convert(Decimal(10,2),atlvleavebalance_tran.uptolstyr_accrueamt) as uptolstyr_accrueamt , " & _
    '                  " atlvleavebalance_tran.uptolstyr_issueamt , " & _
    '                  " CASE WHEN atlvleavebalance_tran.audittype = 1 THEN @add " & _
    '                        " WHEN atlvleavebalance_tran.audittype = 2 THEN @edit " & _
    '                        " WHEN atlvleavebalance_tran.audittype = 3 THEN @delete " & _
    '                  " End 'Type' , ISNULL(hrmsConfiguration..cfuser_master.username, '') UserName ," & _
    '                  " CONVERT(VARCHAR(20), atlvleavebalance_tran.auditdatetime, 112) auditdate , " & _
    '                  " CONVERT(VARCHAR(20), atlvleavebalance_tran.auditdatetime, 108) audittime , " & _
    '                  " ip ,  machine_name ," & _
    '                  " CASE WHEN atlvleavebalance_tran.ispaid = 1 THEN @paid " & _
    '                       " ELSE @unpaid  End Leavetype " & _
    '                  " FROM atlvleavebalance_tran " & _
    '                 " LEFT JOIN lvbatch_master ON lvbatch_master.batchunkid = atlvleavebalance_tran.batchunkid " & _
    '                 " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = atlvleavebalance_tran.leavetypeunkid " & _
    '                 " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = atlvleavebalance_tran.employeeunkid " & _
    '                 " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlvleavebalance_tran.audituserunkid  " & _
    '                 " WHERE CONVERT(CHAR(8),atlvleavebalance_tran.auditdatetime,112) >= @AuditFromDate AND " & _
    '                 " CONVERT(CHAR(8),atlvleavebalance_tran.auditdatetime,112) <= @AuditToDate  AND atlvleavebalance_tran.ispaid = 1"




    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (11-MAY-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If
    '        'Anjan (24 Jun 2011)-End 

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        'Pinkal (11-MAY-2012) -- End

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("Employee")
    '            rpt_Row.Item("Column2") = dtRow.Item("Leavename")
    '            If IsDBNull(dtRow.Item("startdate")) Then
    '                rpt_Row.Item("Column3") = ""
    '            Else
    '                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("startdate").ToString).ToShortDateString
    '            End If

    '            If IsDBNull(dtRow.Item("enddate")) Then
    '                rpt_Row.Item("Column4") = ""
    '            Else
    '                rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("enddate").ToString).ToShortDateString
    '            End If

    '            'Anjan (19 Mar 2011)-Start Issue : removed fmt currency from leave reports as on forms we show only 2 Decimal.
    '            rpt_Row.Item("Column5") = dtRow.Item("Setting")
    '            rpt_Row.Item("Column6") = dtRow.Item("daily_amount")
    '            rpt_Row.Item("Column7") = dtRow.Item("accrue_amount")
    '            rpt_Row.Item("Column8") = dtRow.Item("issue_amount")
    '            rpt_Row.Item("Column9") = dtRow.Item("remaining_bal")
    '            rpt_Row.Item("Column10") = dtRow.Item("uptolstyr_accrueamt")
    '            rpt_Row.Item("Column11") = dtRow.Item("uptolstyr_issueamt")
    '            rpt_Row.Item("Column12") = dtRow.Item("Type")
    '            rpt_Row.Item("Column13") = dtRow.Item("UserName")
    '            rpt_Row.Item("Column14") = eZeeDate.convertDate(dtRow.Item("auditdate").ToString).ToShortDateString
    '            rpt_Row.Item("Column15") = dtRow.Item("audittime")
    '            rpt_Row.Item("Column16") = dtRow.Item("ip")
    '            rpt_Row.Item("Column17") = dtRow.Item("machine_name")
    '            rpt_Row.Item("Column18") = dtRow.Item("employeecode")
    '            'rpt_Row.Item("Column6") = dtRow.Item("Batchno")
    '            'rpt_Row.Item("Column19") = dtRow.Item("Leavetype")

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptLeaveBalance_AT_Report


    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 40, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 41, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 36, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 37, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtAuditdate", Language.getMessage(mstrModuleName, 1, "Audit Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpname", Language.getMessage(mstrModuleName, 42, "Employee :"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpcode", Language.getMessage(mstrModuleName, 43, "Code :"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeavename", Language.getMessage(mstrModuleName, 3, "Leave"))
    '        Call ReportFunction.TextChange(objRpt, "txtUsername", Language.getMessage(mstrModuleName, 38, "User Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtAudittype", Language.getMessage(mstrModuleName, 5, "Audit Type"))
    '        Call ReportFunction.TextChange(objRpt, "txtSetting", Language.getMessage(mstrModuleName, 39, "Setting"))

    '        Call ReportFunction.TextChange(objRpt, "txtStartdate", Language.getMessage(mstrModuleName, 14, "Start Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtEnddate", Language.getMessage(mstrModuleName, 15, "End Date"))
    '        '  Call ReportFunction.TextChange(objRpt, "txtBatch", Language.getMessage(mstrModuleName, 16, "Batch"))
    '        Call ReportFunction.TextChange(objRpt, "txtDailyAmt", Language.getMessage(mstrModuleName, 17, "Daily Amt"))
    '        Call ReportFunction.TextChange(objRpt, "txtAccrueAmt", Language.getMessage(mstrModuleName, 18, "Accrue Amt"))
    '        Call ReportFunction.TextChange(objRpt, "txtIssueAmt", Language.getMessage(mstrModuleName, 19, "Issue Amt"))
    '        Call ReportFunction.TextChange(objRpt, "txtRemaining", Language.getMessage(mstrModuleName, 20, "Remaining Amt"))
    '        Call ReportFunction.TextChange(objRpt, "txtLastAccureamt", Language.getMessage(mstrModuleName, 21, "Last Year Accrure Amt"))
    '        Call ReportFunction.TextChange(objRpt, "txtLastIssueamt", Language.getMessage(mstrModuleName, 22, "Last Year Issue Amt"))
    '        Call ReportFunction.TextChange(objRpt, "txtAudittime", Language.getMessage(mstrModuleName, 23, "Audit Time"))
    '        Call ReportFunction.TextChange(objRpt, "txtIp", Language.getMessage(mstrModuleName, 24, "Machine IP"))
    '        Call ReportFunction.TextChange(objRpt, "txtMachinename", Language.getMessage(mstrModuleName, 25, "Machine"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 26, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 27, "Printed Date :"))


    '        'Pinkal (11-MAY-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 28, "Page :"))
    '        'Pinkal (11-MAY-2012) -- End



    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                              , ByVal mdtEmployeeAsOnDate As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsOnDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Add"))
            objDataOperation.AddParameter("@edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Edit"))
            objDataOperation.AddParameter("@delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Delete"))
            objDataOperation.AddParameter("@paid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Paid Leave"))
            objDataOperation.AddParameter("@unpaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Unpaid Leave"))
            objDataOperation.AddParameter("@issuebal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Issue Balance"))
            objDataOperation.AddParameter("@exceedbal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Exceeding Leave Balance with Unpaid"))


            StrQ = " SELECT hremployee_master.employeecode,hremployee_master.firstname + ' ' + hremployee_master.surname Employee ," & _
                      " Leavename , CONVERT(CHAR(8), atlvleavebalance_tran.startdate, 112) startdate , " & _
                      " CONVERT(CHAR(8), atlvleavebalance_tran.enddate, 112) enddate , " & _
                      " CASE WHEN atlvleavebalance_tran.accruesetting = 1 THEN @issuebal " & _
                            " WHEN atlvleavebalance_tran.accruesetting = 2  THEN @exceedbal " & _
                      " End 'Setting', ISNULL(lvbatch_master.batchno, '') 'Batchno' , " & _
                      " convert(Decimal(10,2),atlvleavebalance_tran.daily_amount) as daily_amount , " & _
                      " convert(Decimal(10,2),atlvleavebalance_tran.accrue_amount) as accrue_amount  , " & _
                      " atlvleavebalance_tran.issue_amount , " & _
                      " convert(Decimal(10,2),atlvleavebalance_tran.remaining_bal) as remaining_bal , " & _
                      " convert(Decimal(10,2),atlvleavebalance_tran.uptolstyr_accrueamt) as uptolstyr_accrueamt , " & _
                      " atlvleavebalance_tran.uptolstyr_issueamt , " & _
                      " CASE WHEN atlvleavebalance_tran.audittype = 1 THEN @add " & _
                            " WHEN atlvleavebalance_tran.audittype = 2 THEN @edit " & _
                            " WHEN atlvleavebalance_tran.audittype = 3 THEN @delete " & _
                      " End 'Type' , ISNULL(hrmsConfiguration..cfuser_master.username, '') UserName ," & _
                      " CONVERT(VARCHAR(20), atlvleavebalance_tran.auditdatetime, 112) auditdate , " & _
                      " CONVERT(VARCHAR(20), atlvleavebalance_tran.auditdatetime, 108) audittime , " & _
                      " ip ,  machine_name ," & _
                      " CASE WHEN atlvleavebalance_tran.ispaid = 1 THEN @paid " & _
                           " ELSE @unpaid  End Leavetype " & _
                      " FROM atlvleavebalance_tran " & _
                     " LEFT JOIN lvbatch_master ON lvbatch_master.batchunkid = atlvleavebalance_tran.batchunkid " & _
                     " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = atlvleavebalance_tran.leavetypeunkid " & _
                     " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = atlvleavebalance_tran.employeeunkid " & _
                     " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlvleavebalance_tran.audituserunkid  "


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            StrQ &= " WHERE CONVERT(CHAR(8),atlvleavebalance_tran.auditdatetime,112) >= @AuditFromDate AND " & _
                        " CONVERT(CHAR(8),atlvleavebalance_tran.auditdatetime,112) <= @AuditToDate  AND atlvleavebalance_tran.ispaid = 1"


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("Employee")
                rpt_Row.Item("Column2") = dtRow.Item("Leavename")
                If IsDBNull(dtRow.Item("startdate")) Then
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("startdate").ToString).ToShortDateString
                End If

                If IsDBNull(dtRow.Item("enddate")) Then
                    rpt_Row.Item("Column4") = ""
                Else
                    rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("enddate").ToString).ToShortDateString
                End If

                'Anjan (19 Mar 2011)-Start Issue : removed fmt currency from leave reports as on forms we show only 2 Decimal.
                rpt_Row.Item("Column5") = dtRow.Item("Setting")
                rpt_Row.Item("Column6") = dtRow.Item("daily_amount")
                rpt_Row.Item("Column7") = dtRow.Item("accrue_amount")
                rpt_Row.Item("Column8") = dtRow.Item("issue_amount")
                rpt_Row.Item("Column9") = dtRow.Item("remaining_bal")
                rpt_Row.Item("Column10") = dtRow.Item("uptolstyr_accrueamt")
                rpt_Row.Item("Column11") = dtRow.Item("uptolstyr_issueamt")
                rpt_Row.Item("Column12") = dtRow.Item("Type")
                rpt_Row.Item("Column13") = dtRow.Item("UserName")
                rpt_Row.Item("Column14") = eZeeDate.convertDate(dtRow.Item("auditdate").ToString).ToShortDateString
                rpt_Row.Item("Column15") = dtRow.Item("audittime")
                rpt_Row.Item("Column16") = dtRow.Item("ip")
                rpt_Row.Item("Column17") = dtRow.Item("machine_name")
                rpt_Row.Item("Column18") = dtRow.Item("employeecode")
                'rpt_Row.Item("Column6") = dtRow.Item("Batchno")
                'rpt_Row.Item("Column19") = dtRow.Item("Leavetype")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptLeaveBalance_AT_Report


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 40, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 41, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 36, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 37, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtAuditdate", Language.getMessage(mstrModuleName, 1, "Audit Date"))
            Call ReportFunction.TextChange(objRpt, "txtEmpname", Language.getMessage(mstrModuleName, 42, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpcode", Language.getMessage(mstrModuleName, 43, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtLeavename", Language.getMessage(mstrModuleName, 3, "Leave"))
            Call ReportFunction.TextChange(objRpt, "txtUsername", Language.getMessage(mstrModuleName, 38, "User Name"))
            Call ReportFunction.TextChange(objRpt, "txtAudittype", Language.getMessage(mstrModuleName, 5, "Audit Type"))
            Call ReportFunction.TextChange(objRpt, "txtSetting", Language.getMessage(mstrModuleName, 39, "Setting"))

            Call ReportFunction.TextChange(objRpt, "txtStartdate", Language.getMessage(mstrModuleName, 14, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEnddate", Language.getMessage(mstrModuleName, 15, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtDailyAmt", Language.getMessage(mstrModuleName, 17, "Daily Amt"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueAmt", Language.getMessage(mstrModuleName, 18, "Accrue Amt"))
            Call ReportFunction.TextChange(objRpt, "txtIssueAmt", Language.getMessage(mstrModuleName, 19, "Issue Amt"))
            Call ReportFunction.TextChange(objRpt, "txtRemaining", Language.getMessage(mstrModuleName, 20, "Remaining Amt"))
            Call ReportFunction.TextChange(objRpt, "txtLastAccureamt", Language.getMessage(mstrModuleName, 21, "Last Year Accrure Amt"))
            Call ReportFunction.TextChange(objRpt, "txtLastIssueamt", Language.getMessage(mstrModuleName, 22, "Last Year Issue Amt"))
            Call ReportFunction.TextChange(objRpt, "txtAudittime", Language.getMessage(mstrModuleName, 23, "Audit Time"))
            Call ReportFunction.TextChange(objRpt, "txtIp", Language.getMessage(mstrModuleName, 24, "Machine IP"))
            Call ReportFunction.TextChange(objRpt, "txtMachinename", Language.getMessage(mstrModuleName, 25, "Machine"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 26, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 27, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Audit Date")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Leave")
            Language.setMessage(mstrModuleName, 4, "User")
            Language.setMessage(mstrModuleName, 5, "Audit Type")
            Language.setMessage(mstrModuleName, 6, "Accrue Setting")
            Language.setMessage(mstrModuleName, 7, "Add")
            Language.setMessage(mstrModuleName, 8, "Edit")
            Language.setMessage(mstrModuleName, 9, "Delete")
            Language.setMessage(mstrModuleName, 10, "Paid Leave")
            Language.setMessage(mstrModuleName, 11, "Unpaid Leave")
            Language.setMessage(mstrModuleName, 12, "Issue Balance")
            Language.setMessage(mstrModuleName, 13, "Exceeding Leave Balance with Unpaid")
            Language.setMessage(mstrModuleName, 14, "Start Date")
            Language.setMessage(mstrModuleName, 15, "End Date")
            Language.setMessage(mstrModuleName, 17, "Daily Amt")
            Language.setMessage(mstrModuleName, 18, "Accrue Amt")
            Language.setMessage(mstrModuleName, 19, "Issue Amt")
            Language.setMessage(mstrModuleName, 20, "Remaining Amt")
            Language.setMessage(mstrModuleName, 21, "Last Year Accrure Amt")
            Language.setMessage(mstrModuleName, 22, "Last Year Issue Amt")
            Language.setMessage(mstrModuleName, 23, "Audit Time")
            Language.setMessage(mstrModuleName, 24, "Machine IP")
            Language.setMessage(mstrModuleName, 25, "Machine")
            Language.setMessage(mstrModuleName, 26, "Printed By :")
            Language.setMessage(mstrModuleName, 27, "Printed Date :")
            Language.setMessage(mstrModuleName, 29, " Audit From Date:")
            Language.setMessage(mstrModuleName, 30, " Audit To Date:")
            Language.setMessage(mstrModuleName, 31, "Employee:")
            Language.setMessage(mstrModuleName, 32, "User:")
            Language.setMessage(mstrModuleName, 33, "Leave:")
            Language.setMessage(mstrModuleName, 34, " Audit Type:")
            Language.setMessage(mstrModuleName, 35, " Order By :")
            Language.setMessage(mstrModuleName, 36, "Approved By :")
            Language.setMessage(mstrModuleName, 37, "Received By :")
            Language.setMessage(mstrModuleName, 38, "User Name")
            Language.setMessage(mstrModuleName, 39, "Setting")
            Language.setMessage(mstrModuleName, 40, "Prepared By :")
            Language.setMessage(mstrModuleName, 41, "Checked By :")
            Language.setMessage(mstrModuleName, 42, "Employee :")
            Language.setMessage(mstrModuleName, 43, "Code :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
