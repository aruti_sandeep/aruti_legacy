Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmLeaveLiability_Report

#Region "Private Variables"

    Private mstrModuleName As String = "frmLeaveLiability_Report"
    Dim objLeaveLiability As clsLeaveLiability_Report
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objLeaveLiability = New clsLeaveLiability_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLeaveLiability.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing

            Dim objLeave As New clsleavetype_master
            dsList = objLeave.getListForCombo("Leave", True, 1)
            With cboLeave
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objLeave = Nothing

            Dim objTransactionHead As New clsTransactionHead
            

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            'dsList = objTransactionHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)
            dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True)
            'Pinkal (30-Jun-2021) -- End

            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("OtherEarning")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
            cboLeave.SelectedIndex = 0
            gbBasicSalaryOtherEarning.Checked = False


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            dtpAsonDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpAsonDate.Checked = False
            'Pinkal (03-May-2019) -- End

            objLeaveLiability.setDefaultOrderBy(0)
            txtOrderBy.Text = objLeaveLiability.OrderByDisplay
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objLeaveLiability.SetDefaultValue()
            objLeaveLiability._EmployeeId = CInt(cboEmployee.SelectedValue)
            objLeaveLiability._Employee = cboEmployee.Text
            objLeaveLiability._LeaveId = CInt(cboLeave.SelectedValue)
            objLeaveLiability._LeaveName = cboLeave.Text
            objLeaveLiability._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objLeaveLiability._YearId = FinancialYear._Object._YearUnkid
            objLeaveLiability._Advance_Filter = mstrAdvanceFilter
            objLeaveLiability._ViewByIds = mstrViewByIds
            objLeaveLiability._ViewIndex = mintViewIndex
            objLeaveLiability._ViewByName = mstrViewByName
            objLeaveLiability._Analysis_Fields = mstrAnalysis_Fields
            objLeaveLiability._Analysis_Join = mstrAnalysis_Join
            objLeaveLiability._Analysis_OrderBy = mstrAnalysis_OrderBy
            objLeaveLiability._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objLeaveLiability._Report_GroupName = mstrReport_GroupName
            If gbBasicSalaryOtherEarning.Checked = True Then
                objLeaveLiability._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objLeaveLiability._OtherEarningTranId = 0
            End If

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            objLeaveLiability._OtherEarningTranHead = cboOtherEarning.Text
            'Pinkal (30-Jun-2021) -- End

            objLeaveLiability._LeaveLiabilitySetting = ConfigParameter._Object._LeaveLiabilityReportSetting

            'Pinkal (12-Jan-2019) -- Start
            'Enhancement [Ref #: 0003343] - Working on TUJIJENGE for Leave Liability Report.
            objLeaveLiability._IsFinClose = FinancialYear._Object._IsFin_Close
            'Pinkal (12-Jan-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If dtpAsonDate.Checked Then
                objLeaveLiability._AsonDate = dtpAsonDate.Value.Date
            Else
                objLeaveLiability._AsonDate = Nothing
            End If

            objLeaveLiability._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objLeaveLiability._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            objLeaveLiability._DatabaseStartDate = FinancialYear._Object._Database_Start_Date.Date
            objLeaveLiability._DatabaseEndDate = FinancialYear._Object._Database_End_Date.Date
            'Pinkal (03-May-2019) -- End


            'Pinkal (26-Aug-2019) -- Start
            'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
            objLeaveLiability._IncludeLeaveBF = chkIncludeLeaveBF.Checked
            'Pinkal (26-Aug-2019) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


    'Pinkal (26-Aug-2019) -- Start
    'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet

        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Leave_Liability_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                Dim mstrIds As String = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
                If mstrIds.Trim.Length > 0 Then
                    Dim ar() As String = mstrIds.Trim().Split(",")
                    If ar.Length > 0 Then
                        chkIncludeLeaveBF.Checked = CBool(ar(0))
                        'Pinkal (30-Jun-2021)-- Start
                        'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
                        If ar.Length > 1 Then
                            gbBasicSalaryOtherEarning.Checked = True
                            cboOtherEarning.SelectedValue = CInt(ar(1))
                        End If
                        'Pinkal (30-Jun-2021) -- End
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    'Pinkal (26-Aug-2019) -- End


#End Region

#Region "Form's Events"

    Private Sub frmLeaveLiability_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLeaveLiability = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveLiability_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLeaveLiability_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objLeaveLiability._ReportName
            eZeeHeader.Message = objLeaveLiability._ReportDesc
            Call FillCombo()
            Call ResetValue()
            'Pinkal (26-Aug-2019) -- Start
            'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
            GetValue()
            'Pinkal (26-Aug-2019) -- End

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo = True Then
                gbBasicSalaryOtherEarning.Enabled = True
            Else
                gbBasicSalaryOtherEarning.Checked = False
                gbBasicSalaryOtherEarning.Enabled = False
            End If
            'Pinkal (30-Jun-2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveLiability_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveLiability_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveLiability_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveLiability_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIssued_LvFormDetail_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLeaveLiability_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsLeaveLiability_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Exit Sub
            End If

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            If gbBasicSalaryOtherEarning.Checked AndAlso dtpAsonDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "As on Date is compulsory information.Please select As on Date."), enMsgBoxStyle.Information)
                dtpAsonDate.Select()
                Exit Sub
            End If


            'Pinkal (30-Jul-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            If gbBasicSalaryOtherEarning.Checked AndAlso dtpAsonDate.Checked Then

            Dim mdtDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date

            If dtpAsonDate.Checked Then
                mdtDate = dtpAsonDate.Value.Date
            End If

            Dim objMaster As New clsMasterData
            Dim xPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtDate, FinancialYear._Object._YearUnkid)
            objMaster = Nothing

            Dim objPeriod As New clscommom_period_Tran
            If xPeriodId > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = xPeriodId
            End If

            Dim objTnALeaveTran As New clsTnALeaveTran

            Dim mstrEmployeeIds As String = ""
            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrEmployeeIds = CInt(cboEmployee.SelectedValue).ToString()
            End If

            If objTnALeaveTran.IsPayrollProcessDone(xPeriodId, mstrEmployeeIds, Nothing, enModuleReference.Payroll, Nothing, Nothing) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "you can not generate this report.Reason:Payroll Process is not done for the current open period.Please do payroll process for the current period."), enMsgBoxStyle.Information)
                dtpAsonDate.Select()
                Exit Sub
            End If

            objTnALeaveTran = Nothing
            objPeriod = Nothing

            End If
            'Pinkal (30-Jul-2021) -- End


            If SetFilter() = False Then Exit Sub

            objLeaveLiability.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                      , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                      , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboLeave
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveType_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboOtherEarning
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objLeaveLiability.setOrderBy(0)
            txtOrderBy.Text = objLeaveLiability.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (26-Aug-2019) -- Start
    'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.

#Region "Linkbutton Events"

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Leave_Liability_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 0
            Dim intUnkid As Integer = -1
            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            'objUserDefRMode._EarningTranHeadIds = IIf(chkIncludeLeaveBF.Checked, 1, 0)
            If gbBasicSalaryOtherEarning.Checked Then
                objUserDefRMode._EarningTranHeadIds = IIf(chkIncludeLeaveBF.Checked, 1, 0) & "," & CInt(cboOtherEarning.SelectedValue).ToString()
            Else
            objUserDefRMode._EarningTranHeadIds = IIf(chkIncludeLeaveBF.Checked, 1, 0)
            End If
            'Pinkal (30-Jun-2021) -- End

            intUnkid = objUserDefRMode.isExist(enArutiReport.Leave_Liability_Report, 0, 0, 0)

            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Setting saved successfully."), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

    'Pinkal (26-Aug-2019) -- End




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLeaveName.Text = Language._Object.getCaption(Me.lblLeaveName.Name, Me.lblLeaveName.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.LblAsonDate.Text = Language._Object.getCaption(Me.LblAsonDate.Name, Me.LblAsonDate.Text)
			Me.chkIncludeLeaveBF.Text = Language._Object.getCaption(Me.chkIncludeLeaveBF.Name, Me.chkIncludeLeaveBF.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Please select transaction head.")
			Language.setMessage(mstrModuleName, 2, "Setting saved successfully.")
			Language.setMessage(mstrModuleName, 3, "As on Date is compulsory information.Please select As on Date.")
			Language.setMessage(mstrModuleName, 4, "you can not generate this report.Reason:Payroll Process is not done for the current open period.Please do payroll process for the current period.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
