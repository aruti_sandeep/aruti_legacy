Imports Aruti.Data
Imports eZeeCommonLib

Public Class clsGrievanceReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsGrievanceReport"
    Private mstrReportId As String = CStr(enArutiReport.Grievance_Detail_Report)
    Dim objDataOperation As clsDataOperation


#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

    Private mdtFromGreviceDate As Date = Nothing
    Private mdtToGreviceDate As Date = Nothing
    Private mintfromEmployeeid As Integer = -1
    Private mstrfromEmployeename As String = ""
    Private mintAginstEmployeeid As Integer = -1
    Private mstrAginstEmployeename As String = ""
    Private mstrRefno As String = ""
    Private mblnShowCommiteeMembers As Boolean = False
    Private mstrCommiteeMembers As String = ""
    Private mintGrievcaneStatusid As Integer = -1
    Private mstrGrievcaneStatus As String = ""

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnFirstNamethenSurname As Boolean = False
    Private mintapprovalsetting As Integer = 0



    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
#End Region

#Region " Properties "

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property


    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public WriteOnly Property _FromGreviceDate() As Date
        Set(ByVal value As Date)
            mdtFromGreviceDate = value
        End Set
    End Property

    Public WriteOnly Property _ToGreviceDate() As Date
        Set(ByVal value As Date)
            mdtToGreviceDate = value
        End Set
    End Property

    Public WriteOnly Property _FromEmployeeid() As Integer
        Set(ByVal value As Integer)
            mintfromEmployeeid = value
        End Set
    End Property

    Public WriteOnly Property _FromEmployeename() As String
        Set(ByVal value As String)
            mstrfromEmployeename = value
        End Set
    End Property

    Public WriteOnly Property _AginstEmployeeid() As Integer
        Set(ByVal value As Integer)
            mintAginstEmployeeid = value
        End Set
    End Property

    Public WriteOnly Property _AginstEmployeename() As String
        Set(ByVal value As String)
            mstrAginstEmployeename = value
        End Set
    End Property

    Public WriteOnly Property _Refno() As String
        Set(ByVal value As String)
            mstrRefno = value
        End Set
    End Property

    Public WriteOnly Property _ShowCommiteeMembers() As Boolean
        Set(ByVal value As Boolean)
            mblnShowCommiteeMembers = value
        End Set
    End Property

    Public WriteOnly Property _CommiteeMembers() As String
        Set(ByVal value As String)
            mstrCommiteeMembers = value
        End Set
    End Property

    Public WriteOnly Property _GrievcaneStatusid() As Integer
        Set(ByVal value As Integer)
            mintGrievcaneStatusid = value
        End Set
    End Property

    Public WriteOnly Property _GrievcaneStatus() As String
        Set(ByVal value As String)
            mstrGrievcaneStatus = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _UserUnkid() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property


    Public WriteOnly Property _Approvalsetting() As Integer
        Set(ByVal value As Integer)
            mintapprovalsetting = value
        End Set
    End Property



#End Region


#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrUserAccessFilter = ""
            mstrAdvance_Filter = ""
            mintfromEmployeeid = -1
            mstrfromEmployeename = ""
            mintAginstEmployeeid = -1
            mstrAginstEmployeename = ""
            mstrRefno = ""
            mblnShowCommiteeMembers = False
            mstrCommiteeMembers = ""
            mintGrievcaneStatusid = -1
            mstrGrievcaneStatus = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mintapprovalsetting = ConfigParameter._Object._GrievanceApprovalSetting
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


            If mdtFromGreviceDate <> Nothing AndAlso mdtToGreviceDate <> Nothing Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date :") & " " & mdtFromGreviceDate.ToShortDateString & " " & _
                             Language.getMessage(mstrModuleName, 2, "To") & " " & mdtToGreviceDate.ToShortDateString & " "

                Me._FilterQuery &= "and  CONVERT(NVARCHAR(8),grievancedate,112) BETWEEN @fromdate AND @todate "
                objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromGreviceDate.Date))
                objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToGreviceDate.Date))

            End If


            If mintapprovalsetting > 0 Then
                Me._FilterQuery &= "and  gregrievance_master.approvalsettingid = " & mintapprovalsetting
            End If


            If mintfromEmployeeid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, " From Employee :") & " " & mstrfromEmployeename & " "
                Me._FilterQuery &= "and fromemployeeunkid = @fromemployeeunkid "
                objDataOperation.AddParameter("@fromemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintfromEmployeeid)

            End If

            If mintAginstEmployeeid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Aginst Employee :") & " " & mstrAginstEmployeename & " "

                Me._FilterQuery &= "and againstemployeeunkid = @againstemployeeunkid "
                objDataOperation.AddParameter("@againstemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAginstEmployeeid)

            End If

            If mstrRefno.ToString.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Ref No :") & " " & mstrRefno & " "
                Me._FilterQuery &= "and grievancerefno like  @grievancerefno"
                objDataOperation.AddParameter("@grievancerefno", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & mstrRefno & "%")
            End If

            If mintGrievcaneStatusid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Status :") & " " & mstrGrievcaneStatus & " "
                'Me._FilterQuery &= "and statusunkid = @statusunkid"
                Me._FilterQuery &= " and ISNULL(Fstatus.stid, 0) = @statusunkid"
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievcaneStatusid)
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

#End Region

#Region " Report Generation "

    Private Sub Create_OnDetailReport()
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                              ByVal intUserUnkid As Integer, _
                                              ByVal intYearUnkid As Integer, _
                                              ByVal intCompanyUnkid As Integer, _
                                              ByVal dtPeriodStart As Date, _
                                              ByVal dtPeriodEnd As Date, _
                                              ByVal strUserModeSetting As String, _
                                              ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        'Hemant (27 Oct 2023) -- Start
        'ISSUE/ENHANCEMENT(LHRC): A1X-1455 - Grievance detail report enhancement for company level grievances 
        Dim dtEmployeeLevel As New DataTable
        Dim dsCompanyLevelList As New DataSet
        Dim dtCompanyLevelLevel As New DataTable
        Dim dtFinal As New DataTable
        'Hemant (27 Oct 2023) -- End
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_subData As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "INI")
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "INI") 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "INI")

            ' Employee Level Grievance
            If mintAginstEmployeeid = 0 OrElse mintAginstEmployeeid = -1 OrElse mintAginstEmployeeid <> -999 Then

            StrQ = "SELECT " & _
                   "     grievancerefno AS RefNo " & _
                   "    ,CONVERT(NVARCHAR(8),grievancedate,112) AS gDate " & _
                   "    ,cfcommon_master.name AS gType " & _
                   "    ,grievance_description AS gDescription " & _
                   "    ,grievance_reliefwanted AS gReliefWanted " & _
                   "    ,appealremark as gAppeal " & _
                   "    ,INI.employeecode + ' - ' + INI.firstname + ' ' + INI.surname AS gRaisedBy " & _
                   "    ,IDEPT.name as rdept " & _
                   "    ,IJOB.job_name as rjob " & _
                   "    ,ADEPT.name as adept " & _
                   "    ,AJOB.job_name as ajob " & _
                   "    ,AGT.employeecode + ' - ' + AGT.firstname + ' ' + AGT.surname AS gRaisedAgainst " & _
                   "    ,CASE WHEN ISNULL(initiatortranunkid,0) <= 0 THEN  @Pending " & _
                   "          WHEN statusunkid = " & enGrievanceResponse.AgreedAndClose & " THEN @AgreedAndClose " & _
                   "          WHEN statusunkid = " & enGrievanceResponse.DisagreeAndEscalateToNextLevel & " THEN @DisagreeAndEscalateToNextLevel " & _
                   "          WHEN statusunkid = " & enGrievanceResponse.DisagreeAndClose & " THEN @DisagreeAndClose " & _
                   "          WHEN statusunkid = " & enGrievanceResponse.Appeal & " THEN @Appeal " & _
                   "          WHEN statusunkid = " & enGrievanceResponse.Disagree & " THEN @Disagree END AS gStatus " & _
                   "    ,ISNULL(greinitiater_response_tran.remark,'') AS gRemark " & _
                   "	,ISNULL(responsetypeunkid, 0)  as gResponseTypeid " & _
                   "    ,CASE responsetypeunkid WHEN 1 THEN 'Individual' WHEN 2 THEN 'Committee' ELSE '' END AS gResponseType " & _
                   "    ,ISNULL(responseremark,'') AS gResponseRemark " & _
                   "    ,ISNULL(qualifyremark,'') AS gQualifyRemark " & _
                   "    ,ISNULL(CONVERT(NVARCHAR(8),meetingdate,112),'') AS gMeetingDate " & _
                   "    ,ISNULL(M.iVals,'') AS gCommittee " & _
                   "    ,ISNULL(A.ifile,'') AS gFiles " & _
                   "    ,CASE ISNULL(Fstatus.stid, 0) " & _
                   "        WHEN '0' THEN @Pending " & _
                   "        WHEN '1' THEN @AgreedAndClose " & _
                   "        WHEN '2' THEN @DisagreeAndEscalateToNextLevel " & _
                   "        WHEN '3' THEN @DisagreeAndClose " & _
                   "        WHEN '4' THEN @Appeal " & _
                   "    END AS gFinalStatus " & _
                   "   ,gregrievance_master.grievancemasterunkid " & _
                   "   ,INI.employeeunkid " & _
                   "   ,ISNULL(greresolution_step_tran.approvermasterunkid,0) AS approvermasterunkid " & _
                   "   ,gregrievance_master.fromemployeeunkid as gfromempid " & _
                   "   ,gregrievance_master.againstemployeeunkid AS gagainstempid " & _
                   "   ,ISNULL(greresolution_step_tran.grievancemasterunkid, 0) as rgrievancemstid "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= ",CASE ISNULL(gregrievance_master.isprocessed, 0) " & _
                      "WHEN 1 THEN CASE ISNULL(Fstatus.stid, 0) " & _
                                "WHEN '0' THEN @Pending " & _
                                "WHEN " & enGrievanceResponse.AgreedAndClose & " THEN @AgreedAndClose " & _
                                "WHEN " & enGrievanceResponse.DisagreeAndEscalateToNextLevel & " THEN @DisagreeAndEscalateToNextLevel " & _
                                "WHEN " & enGrievanceResponse.DisagreeAndClose & " THEN @DisagreeAndClose " & _
                                "WHEN " & enGrievanceResponse.Appeal & " THEN @Appeal " & _
                                "WHEN " & enGrievanceResponse.Disagree & " THEN @Disagree " & _
                           "END " & _
                      "ELSE CASE " & _
                                "WHEN ISNULL(initiatortranunkid, 0) <= 0 THEN @Pending " & _
                                "WHEN statusunkid = " & enGrievanceResponse.AgreedAndClose & " THEN @AgreedAndClose " & _
                                "WHEN statusunkid = " & enGrievanceResponse.DisagreeAndEscalateToNextLevel & " THEN @DisagreeAndEscalateToNextLevel " & _
                                "WHEN statusunkid = " & enGrievanceResponse.DisagreeAndClose & " THEN @DisagreeAndClose " & _
                                "WHEN statusunkid = " & enGrievanceResponse.Appeal & " THEN @Appeal " & _
                                "WHEN statusunkid = " & enGrievanceResponse.Disagree & " THEN @Disagree " & _
                           "END " & _
                     "END AS EFinalStatus "



            StrQ &= "FROM gregrievance_master " & _
                   "    JOIN hremployee_master AS INI ON INI.employeeunkid = gregrievance_master.fromemployeeunkid " & _
                   "    JOIN hremployee_master AS AGT ON AGT.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                   "    JOIN cfcommon_master ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid AND mastertype = 63 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "            departmentunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS INIAlloc ON INIAlloc.employeeunkid = INI.employeeunkid AND INIAlloc.rno = 1 " & _
                   "    left join hrdepartment_master IDEPT on INIAlloc.departmentunkid = IDEPT.departmentunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "        jobunkid " & _
                   "        ,jobgroupunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_categorization_tran " & _
                   "        WHERE isvoid = 0 AND  CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS INIJobs ON INIJobs.employeeunkid = INI.employeeunkid AND INIJobs.rno = 1 " & _
                   "    LEFT JOIN hrjob_master IJOB ON INIJobs.jobunkid = IJOB.jobunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "        departmentunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS AGTAlloc ON AGT.employeeunkid = AGTAlloc.employeeunkid AND AGTAlloc.rno = 1 " & _
                   "    left join hrdepartment_master ADEPT on AGTAlloc.departmentunkid = ADEPT.departmentunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "        jobunkid " & _
                   "        ,jobgroupunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_categorization_tran " & _
                   "        WHERE isvoid = 0 AND  CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS AGTJobs ON AGT.employeeunkid = AGTJobs.employeeunkid AND AGTJobs.rno = 1 " & _
                   "    LEFT JOIN hrjob_master AJOB ON AGTJobs.jobunkid = AJOB.jobunkid " & _
                   "    LEFT JOIN greresolution_step_tran ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid AND greresolution_step_tran.isvoid = 0 and greresolution_step_tran.issubmitted = 1 " & _
                   "    LEFT JOIN greinitiater_response_tran ON greresolution_step_tran.resolutionsteptranunkid = greinitiater_response_tran.resolutionsteptranunkid AND greinitiater_response_tran.isvoid = 0 and greinitiater_response_tran.issubmitted = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             B.resolutionsteptranunkid " & _
                   "            ,STUFF((SELECT ', ' + CAST(employeecode +' - '+ (firstname +' '+ surname) AS VARCHAR(MAX)) " & _
                   "        FROM greresolution_meeting_tran AS A " & _
                   "            JOIN hremployee_master ON hremployee_master.employeeunkid = A.employeeunkid " & _
                   "        WHERE B.resolutionsteptranunkid = A.resolutionsteptranunkid AND A.isvoid = 0 " & _
                   "            FOR XML PATH ('')),1,2,'') AS iVals " & _
                   "        FROM greresolution_meeting_tran AS B WHERE B.isvoid = 0 " & _
                   "        GROUP BY B.resolutionsteptranunkid " & _
                   "    ) AS M ON M.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "            B.grievancemasterunkid " & _
                   "            ,STUFF((SELECT ', ' + CAST([filename] AS VARCHAR(MAX)) " & _
                   "        FROM gregrievance_master AS A " & _
                   "            JOIN hrdocuments_tran ON hrdocuments_tran.transactionunkid = A.grievancemasterunkid " & _
                   "        WHERE B.grievancemasterunkid = A.grievancemasterunkid AND A.isvoid = 0 AND form_name IN ('frmGrievanceEmployeeAddEdit') " & _
                   "            FOR XML PATH ('')),1,2,'') AS ifile " & _
                   "        FROM gregrievance_master AS B WHERE B.isvoid = 0 " & _
                   "        GROUP BY B.grievancemasterunkid " & _
                   "    ) AS A ON A.grievancemasterunkid = gregrievance_master.grievancemasterunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "            grievancemasterunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY grievancemasterunkid ORDER BY initiatortranunkid DESC) AS rno " & _
                   "            ,statusunkid as stid " & _
                   "        FROM greinitiater_response_tran WHERE isvoid = 0 " & _
                   "    ) AS Fstatus ON Fstatus.grievancemasterunkid =  gregrievance_master.grievancemasterunkid AND Fstatus.rno = 1 " & _
                   "    LEFT JOIN hremployee_master as gappr on greinitiater_response_tran.approverempid = gappr.employeeunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= "WHERE gregrievance_master.isvoid = 0 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 30, "Pending"))
            objDataOperation.AddParameter("@AgreedAndClose", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 31, "Agreed And Close"))
            objDataOperation.AddParameter("@DisagreeAndEscalateToNextLevel", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 32, "Disagree And Escalate To Next Level"))
            objDataOperation.AddParameter("@DisagreeAndClose", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 33, "Disagree And Close"))
            objDataOperation.AddParameter("@Appeal", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 34, "Appeal"))

            'Gajanan [13-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
            objDataOperation.AddParameter("@Disagree", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 35, "Disagree"))
            'Gajanan [13-July-2019] -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")
                dtEmployeeLevel = dsList.Tables(0)

                'Hemant (27 Oct 2023) -- Start
                'ISSUE/ENHANCEMENT(LHRC): A1X-1455 - Grievance detail report enhancement for company level grievances 
                dtFinal.Merge(dtEmployeeLevel)
                'Hemant (27 Oct 2023) -- End

            End If   'Hemant (27 Oct 2023) 


            ' Company Level Grievance

            'Hemant (27 Oct 2023) -- Start
            'ISSUE/ENHANCEMENT(LHRC): A1X-1455 - Grievance detail report enhancement for company level grievances 
            If mintAginstEmployeeid = 0 OrElse mintAginstEmployeeid = -1 OrElse mintAginstEmployeeid = -999 Then

                StrQ = "SELECT " & _
                       "     grievancerefno AS RefNo " & _
                       "    ,CONVERT(NVARCHAR(8),grievancedate,112) AS gDate " & _
                       "    ,cfcommon_master.name AS gType " & _
                       "    ,grievance_description AS gDescription " & _
                       "    ,grievance_reliefwanted AS gReliefWanted " & _
                       "    ,appealremark as gAppeal " & _
                       "    ,INI.employeecode + ' - ' + INI.firstname + ' ' + INI.surname AS gRaisedBy " & _
                       "    ,IDEPT.name as rdept " & _
                       "    ,IJOB.job_name as rjob " & _
                       "    ,'' as adept " & _
                       "    ,'' as ajob " & _
                       "    ,'000000 - Company' AS gRaisedAgainst " & _
                       "    ,CASE WHEN ISNULL(initiatortranunkid,0) <= 0 THEN  @Pending " & _
                       "          WHEN statusunkid = " & enGrievanceResponse.AgreedAndClose & " THEN @AgreedAndClose " & _
                       "          WHEN statusunkid = " & enGrievanceResponse.DisagreeAndEscalateToNextLevel & " THEN @DisagreeAndEscalateToNextLevel " & _
                       "          WHEN statusunkid = " & enGrievanceResponse.DisagreeAndClose & " THEN @DisagreeAndClose " & _
                       "          WHEN statusunkid = " & enGrievanceResponse.Appeal & " THEN @Appeal " & _
                       "          WHEN statusunkid = " & enGrievanceResponse.Disagree & " THEN @Disagree END AS gStatus " & _
                       "    ,ISNULL(greinitiater_response_tran.remark,'') AS gRemark " & _
                       "	,ISNULL(responsetypeunkid, 0)  as gResponseTypeid " & _
                       "    ,CASE responsetypeunkid WHEN 1 THEN 'Individual' WHEN 2 THEN 'Committee' ELSE '' END AS gResponseType " & _
                       "    ,ISNULL(responseremark,'') AS gResponseRemark " & _
                       "    ,ISNULL(qualifyremark,'') AS gQualifyRemark " & _
                       "    ,ISNULL(CONVERT(NVARCHAR(8),meetingdate,112),'') AS gMeetingDate " & _
                       "    ,ISNULL(M.iVals,'') AS gCommittee " & _
                       "    ,ISNULL(A.ifile,'') AS gFiles " & _
                       "    ,CASE ISNULL(Fstatus.stid, 0) " & _
                       "        WHEN '0' THEN @Pending " & _
                       "        WHEN '1' THEN @AgreedAndClose " & _
                       "        WHEN '2' THEN @DisagreeAndEscalateToNextLevel " & _
                       "        WHEN '3' THEN @DisagreeAndClose " & _
                       "        WHEN '4' THEN @Appeal " & _
                       "    END AS gFinalStatus " & _
                       "   ,gregrievance_master.grievancemasterunkid " & _
                       "   ,INI.employeeunkid " & _
                       "   ,ISNULL(greresolution_step_tran.approvermasterunkid,0) AS approvermasterunkid " & _
                       "   ,gregrievance_master.fromemployeeunkid as gfromempid " & _
                       "   ,gregrievance_master.againstemployeeunkid AS gagainstempid " & _
                       "   ,ISNULL(greresolution_step_tran.grievancemasterunkid, 0) as rgrievancemstid "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= ",CASE ISNULL(gregrievance_master.isprocessed, 0) " & _
                          "WHEN 1 THEN CASE ISNULL(Fstatus.stid, 0) " & _
                                    "WHEN '0' THEN @Pending " & _
                                    "WHEN " & enGrievanceResponse.AgreedAndClose & " THEN @AgreedAndClose " & _
                                    "WHEN " & enGrievanceResponse.DisagreeAndEscalateToNextLevel & " THEN @DisagreeAndEscalateToNextLevel " & _
                                    "WHEN " & enGrievanceResponse.DisagreeAndClose & " THEN @DisagreeAndClose " & _
                                    "WHEN " & enGrievanceResponse.Appeal & " THEN @Appeal " & _
                                    "WHEN " & enGrievanceResponse.Disagree & " THEN @Disagree " & _
                               "END " & _
                          "ELSE CASE " & _
                                    "WHEN ISNULL(initiatortranunkid, 0) <= 0 THEN @Pending " & _
                                    "WHEN statusunkid = " & enGrievanceResponse.AgreedAndClose & " THEN @AgreedAndClose " & _
                                    "WHEN statusunkid = " & enGrievanceResponse.DisagreeAndEscalateToNextLevel & " THEN @DisagreeAndEscalateToNextLevel " & _
                                    "WHEN statusunkid = " & enGrievanceResponse.DisagreeAndClose & " THEN @DisagreeAndClose " & _
                                    "WHEN statusunkid = " & enGrievanceResponse.Appeal & " THEN @Appeal " & _
                                    "WHEN statusunkid = " & enGrievanceResponse.Disagree & " THEN @Disagree " & _
                               "END " & _
                         "END AS EFinalStatus "



                StrQ &= "FROM gregrievance_master " & _
                       "    JOIN hremployee_master AS INI ON INI.employeeunkid = gregrievance_master.fromemployeeunkid " & _
                       "    JOIN cfcommon_master ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid AND mastertype = 63 " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "            departmentunkid " & _
                       "            ,employeeunkid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "        FROM hremployee_transfer_tran " & _
                       "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                       "    ) AS INIAlloc ON INIAlloc.employeeunkid = INI.employeeunkid AND INIAlloc.rno = 1 " & _
                       "    left join hrdepartment_master IDEPT on INIAlloc.departmentunkid = IDEPT.departmentunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "        jobunkid " & _
                       "        ,jobgroupunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "        FROM hremployee_categorization_tran " & _
                       "        WHERE isvoid = 0 AND  CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                       "    ) AS INIJobs ON INIJobs.employeeunkid = INI.employeeunkid AND INIJobs.rno = 1 " & _
                       "    LEFT JOIN hrjob_master IJOB ON INIJobs.jobunkid = IJOB.jobunkid " & _
                       "    LEFT JOIN greresolution_step_tran ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid AND greresolution_step_tran.isvoid = 0 and greresolution_step_tran.issubmitted = 1 " & _
                        "    LEFT JOIN greinitiater_response_tran ON greresolution_step_tran.resolutionsteptranunkid = greinitiater_response_tran.resolutionsteptranunkid AND greinitiater_response_tran.isvoid = 0 and greinitiater_response_tran.issubmitted = 1 " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             B.resolutionsteptranunkid " & _
                        "            ,STUFF((SELECT ', ' + CAST(employeecode +' - '+ (firstname +' '+ surname) AS VARCHAR(MAX)) " & _
                        "        FROM greresolution_meeting_tran AS A " & _
                        "            JOIN hremployee_master ON hremployee_master.employeeunkid = A.employeeunkid " & _
                        "        WHERE B.resolutionsteptranunkid = A.resolutionsteptranunkid AND A.isvoid = 0 " & _
                        "            FOR XML PATH ('')),1,2,'') AS iVals " & _
                        "        FROM greresolution_meeting_tran AS B WHERE B.isvoid = 0 " & _
                        "        GROUP BY B.resolutionsteptranunkid " & _
                        "    ) AS M ON M.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "            B.grievancemasterunkid " & _
                        "            ,STUFF((SELECT ', ' + CAST([filename] AS VARCHAR(MAX)) " & _
                        "        FROM gregrievance_master AS A " & _
                        "            JOIN hrdocuments_tran ON hrdocuments_tran.transactionunkid = A.grievancemasterunkid " & _
                        "        WHERE B.grievancemasterunkid = A.grievancemasterunkid AND A.isvoid = 0 AND form_name IN ('frmGrievanceEmployeeAddEdit') " & _
                        "            FOR XML PATH ('')),1,2,'') AS ifile " & _
                        "        FROM gregrievance_master AS B WHERE B.isvoid = 0 " & _
                        "        GROUP BY B.grievancemasterunkid " & _
                        "    ) AS A ON A.grievancemasterunkid = gregrievance_master.grievancemasterunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "            grievancemasterunkid " & _
                        "            ,ROW_NUMBER()OVER(PARTITION BY grievancemasterunkid ORDER BY initiatortranunkid DESC) AS rno " & _
                        "            ,statusunkid as stid " & _
                        "        FROM greinitiater_response_tran WHERE isvoid = 0 " & _
                        "    ) AS Fstatus ON Fstatus.grievancemasterunkid =  gregrievance_master.grievancemasterunkid AND Fstatus.rno = 1 " & _
                        "    LEFT JOIN hremployee_master as gappr on greinitiater_response_tran.approverempid = gappr.employeeunkid "
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= mstrAnalysis_Join

                StrQ &= "WHERE gregrievance_master.isvoid = 0 "

                StrQ &= " AND gregrievance_master.againstemployeeunkid = -999 "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If

                objDataOperation.ClearParameters()
                Call FilterTitleAndFilterQuery()
                StrQ &= Me._FilterQuery

                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 30, "Pending"))
                objDataOperation.AddParameter("@AgreedAndClose", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 31, "Agreed And Close"))
                objDataOperation.AddParameter("@DisagreeAndEscalateToNextLevel", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 32, "Disagree And Escalate To Next Level"))
                objDataOperation.AddParameter("@DisagreeAndClose", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 33, "Disagree And Close"))
                objDataOperation.AddParameter("@Appeal", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 34, "Appeal"))
                objDataOperation.AddParameter("@Disagree", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 35, "Disagree"))

                dsCompanyLevelList = objDataOperation.ExecQuery(StrQ, "DataTable")
                dtCompanyLevelLevel = dsCompanyLevelList.Tables(0)

                dtFinal.Merge(dtCompanyLevelLevel)

            End If
            If dsList.Tables.Count > 0 Then
                dsList.Tables.RemoveAt(0)
            End If
            dsList.Tables.Add(dtFinal)
            'Hemant (27 Oct 2023) -- End

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            objRpt = New ArutiReport.Designer.rptGrievanceReport
            rpt_subData = New ArutiReport.Designer.dsArutiReport


            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtmain As New DataTable
                Dim strcolname As String() = New String() {"grievancemasterunkid", "RefNo", "gDate", "gType", "gDescription", "gReliefWanted", "gAppeal", "gRaisedBy", "rdept", "rjob", "adept", "ajob", "gRaisedAgainst", "gFinalStatus", "gfromempid", "gagainstempid", "rgrievancemstid", "GName", "EFinalStatus"}
                dtmain = dsList.Tables(0).DefaultView.ToTable(True, strcolname)

                'Dim empid As String = dtmain.Rows(0)("gfromempid")
                Dim empid As String = dtmain.Rows(0)("gagainstempid")



                For Each dtRow As DataRow In dtmain.Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("GName")
                    rpt_Row.Item("Column2") = dtRow.Item("RefNo")
                    rpt_Row.Item("Column3") = dtRow.Item("GName")
                    rpt_Row.Item("Column4") = dtRow.Item("EFinalStatus")
                    rpt_Row.Item("Column5") = dtRow.Item("gType")
                    rpt_Row.Item("Column6") = eZeeDate.convertDate(dtRow.Item("gDate").ToString).ToShortDateString()
                    rpt_Row.Item("Column7") = dtRow.Item("gRaisedBy")
                    rpt_Row.Item("Column8") = dtRow.Item("rdept")
                    rpt_Row.Item("Column9") = dtRow.Item("rjob")
                    rpt_Row.Item("Column10") = dtRow.Item("gRaisedAgainst")
                    rpt_Row.Item("Column11") = dtRow.Item("adept")
                    rpt_Row.Item("Column12") = dtRow.Item("ajob")
                    rpt_Row.Item("Column13") = dtRow.Item("gDescription")
                    rpt_Row.Item("Column14") = dtRow.Item("gReliefWanted")
                    rpt_Row.Item("Column15") = dtRow.Item("grievancemasterunkid")


                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next





                Dim objgreappr As New clsgrievanceapprover_master()

                Dim dsexpappr As New DataSet
                Dim clsConfig As New clsConfigOptions
                clsConfig._Companyunkid = intCompanyUnkid

                Dim strempids As String = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("gagainstempid").ToString).ToArray())

                Dim eApprType As enGrievanceApproval
                If Convert.ToInt32(clsConfig._GrievanceApprovalSetting) = enGrievanceApproval.ApproverEmpMapping Then
                    eApprType = enGrievanceApproval.ApproverEmpMapping
                ElseIf Convert.ToInt32(clsConfig._GrievanceApprovalSetting) = enGrievanceApproval.UserAcess Then
                    eApprType = enGrievanceApproval.UserAcess
                ElseIf Convert.ToInt32(clsConfig._GrievanceApprovalSetting) = enGrievanceApproval.ReportingTo Then
                    eApprType = enGrievanceApproval.ReportingTo
                End If


                'Gajanan [4-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes


                'dsexpappr = objgreappr.GetApproversList(eApprType, strDatabaseName, User._Object._Userunkid, _
                '                                        intYearUnkid, intCompanyUnkid, 1202, strUserModeSetting, _
                '                                        True, True, True, ConfigParameter._Object._EmployeeAsOnDate, False, Nothing, strempids, False, "", False)



                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


                If eApprType = enGrievanceApproval.ReportingTo Then
                    dsexpappr = objgreappr.GetApproversList(eApprType, strDatabaseName, User._Object._Userunkid, _
                                                           intYearUnkid, intCompanyUnkid, 1202, strUserModeSetting, _
                                                           True, True, True, ConfigParameter._Object._EmployeeAsOnDate, False, Nothing, "", False, "", False, -1, False)
                Else
                    dsexpappr = objgreappr.GetApproversList(eApprType, strDatabaseName, User._Object._Userunkid, _
                                                     intYearUnkid, intCompanyUnkid, 1202, strUserModeSetting, _
                                                     True, True, True, ConfigParameter._Object._EmployeeAsOnDate, False, Nothing, strempids, False, "", False, -1)
                End If





                'Gajanan [24-OCT-2019] -- End


                'Gajanan [4-July-2019] -- End   


                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_subData.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("RefNo")
                    Dim dt() As DataRow = dsexpappr.Tables(0).Select("approvermasterunkid = " & dtRow.Item("approvermasterunkid"))

                    If dt.Length > 0 Then
                        rpt_Row.Item("Column2") = dt(0)("levelname") & " - " & dt(0)("name")
                    End If
                    rpt_Row.Item("Column3") = dtRow.Item("gResponseType")
                    If CInt(dtRow.Item("gResponseTypeid").ToString) = 2 Then
                        rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("gMeetingDate").ToString).ToShortDateString()
                    End If
                    rpt_Row.Item("Column5") = dtRow.Item("gCommittee").ToString().Replace(",", vbNewLine)
                    rpt_Row.Item("Column6") = dtRow.Item("gQualifyRemark")
                    rpt_Row.Item("Column7") = dtRow.Item("gResponseRemark")
                    rpt_Row.Item("Column8") = dtRow.Item("gRemark")
                    rpt_subData.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next
            End If



            Dim objReportFunction As New ReportFunction
            Call objReportFunction.GetReportSetting(CInt(mstrReportId), intCompanyUnkid)

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        objReportFunction._DisplayLogo, _
                                        objReportFunction._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        objReportFunction._LeftMargin, _
                                        objReportFunction._RightMargin, _
                                        objReportFunction._PageMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objReportFunction = Nothing

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptGrievanceDetail").SetDataSource(rpt_subData)

            Call ReportFunction.TextChange(objRpt, "txtRefNo", Language.getMessage(mstrModuleName, 7, "Ref No"))
            Call ReportFunction.TextChange(objRpt, "txtGrievanceStatus", Language.getMessage(mstrModuleName, 8, "Grievance Status"))
            Call ReportFunction.TextChange(objRpt, "txtGrievancetype", Language.getMessage(mstrModuleName, 9, "Grievance Type"))
            Call ReportFunction.TextChange(objRpt, "txtGrievanceDate", Language.getMessage(mstrModuleName, 10, "Grievance Date"))
            Call ReportFunction.TextChange(objRpt, "txtGrievant", Language.getMessage(mstrModuleName, 11, "Grievant"))
            Call ReportFunction.TextChange(objRpt, "txtgrievantDepartment", Language.getMessage(mstrModuleName, 12, "Department of the grievant"))
            Call ReportFunction.TextChange(objRpt, "txtgrievantJob", Language.getMessage(mstrModuleName, 13, "Job title of grievant"))
            Call ReportFunction.TextChange(objRpt, "txtAllegedStaff", Language.getMessage(mstrModuleName, 14, "Alleged staff"))
            Call ReportFunction.TextChange(objRpt, "txtAllegedDepartment", Language.getMessage(mstrModuleName, 16, "Department of the alleged staff"))
            Call ReportFunction.TextChange(objRpt, "txtAllegedJob", Language.getMessage(mstrModuleName, 17, "Alleged staff job title"))
            Call ReportFunction.TextChange(objRpt, "txtGrievanceDetail", Language.getMessage(mstrModuleName, 18, "Nature and description of the grievance"))
            Call ReportFunction.TextChange(objRpt, "txtReliefWanted", Language.getMessage(mstrModuleName, 19, "Relief Wanted"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptGrievanceDetail"), "txtApprover", Language.getMessage(mstrModuleName, 20, "Approver Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptGrievanceDetail"), "txtresponsetype", Language.getMessage(mstrModuleName, 21, "Response Type"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptGrievanceDetail"), "txtmeetingdate", Language.getMessage(mstrModuleName, 22, "Meeting Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptGrievanceDetail"), "txtCommittee", Language.getMessage(mstrModuleName, 23, "Committee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptGrievanceDetail"), "txtqualificationremark", Language.getMessage(mstrModuleName, 24, "Facts Established"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptGrievanceDetail"), "txtresponseremark", Language.getMessage(mstrModuleName, 25, "Resolution"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptGrievanceDetail"), "txtemployeeremark", Language.getMessage(mstrModuleName, 26, "Grievant Response"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 27, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 28, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 29, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            ' Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function

#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Date :")
            Language.setMessage(mstrModuleName, 2, "To")
            Language.setMessage(mstrModuleName, 3, " From Employee :")
            Language.setMessage(mstrModuleName, 4, "Aginst Employee :")
            Language.setMessage(mstrModuleName, 5, "Ref No :")
            Language.setMessage(mstrModuleName, 6, "Status :")
            Language.setMessage(mstrModuleName, 7, "Ref No")
            Language.setMessage(mstrModuleName, 8, "Grievance Status")
            Language.setMessage(mstrModuleName, 9, "Grievance Type")
            Language.setMessage(mstrModuleName, 10, "Grievance Date")
            Language.setMessage(mstrModuleName, 11, "Grievant")
            Language.setMessage(mstrModuleName, 12, "Department of the grievant")
            Language.setMessage(mstrModuleName, 13, "Job title of grievant")
            Language.setMessage(mstrModuleName, 14, "Alleged staff")
            Language.setMessage(mstrModuleName, 16, "Department of the alleged staff")
            Language.setMessage(mstrModuleName, 17, "Alleged staff job title")
            Language.setMessage(mstrModuleName, 18, "Nature and description of the grievance")
            Language.setMessage(mstrModuleName, 19, "Relief Wanted")
            Language.setMessage(mstrModuleName, 20, "Approver Name")
            Language.setMessage(mstrModuleName, 21, "Response Type")
            Language.setMessage(mstrModuleName, 22, "Meeting Date")
            Language.setMessage(mstrModuleName, 23, "Committee")
            Language.setMessage(mstrModuleName, 24, "Facts Established")
            Language.setMessage(mstrModuleName, 25, "Resolution")
            Language.setMessage(mstrModuleName, 26, "Grievant Response")
            Language.setMessage(mstrModuleName, 27, "Printed By :")
            Language.setMessage(mstrModuleName, 28, "Printed Date :")
            Language.setMessage(mstrModuleName, 29, "Page :")
            Language.setMessage(mstrModuleName, 30, "Pending")
            Language.setMessage(mstrModuleName, 31, "Agreed And Close")
            Language.setMessage(mstrModuleName, 32, "Disagree And Escalate To Next Level")
            Language.setMessage(mstrModuleName, 33, "Disagree And Close")
            Language.setMessage(mstrModuleName, 34, "Appeal")
            Language.setMessage(mstrModuleName, 35, "Disagree")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
