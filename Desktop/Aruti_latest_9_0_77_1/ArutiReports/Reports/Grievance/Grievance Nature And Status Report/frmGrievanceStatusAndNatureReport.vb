#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region

Public Class frmGrievanceStatusAndNatureReport

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmGrievanceStatusAndNatureReport"
    Private objGrievanceStatusAndNature_Report As clsGrievanceStatusAndNature_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty


#End Region

#Region "Constructor"
    Public Sub New()
        objGrievanceStatusAndNature_Report = New clsGrievanceStatusAndNature_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objGrievanceStatusAndNature_Report.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Private Function"
    Public Sub FillCombo()
        Try

            'cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True).Tables(0)

            'cboEmployee.ValueMember = "employeeunkid"
            'cboEmployee.DisplayMember = "employeename"

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Grievances Status Report"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Nature of Grievance Report"))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboType.SelectedIndex = 0

            txtOrderBy.Text = objGrievanceStatusAndNature_Report.OrderByDisplay
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAdvanceFilter = ""

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try


            objGrievanceStatusAndNature_Report.SetDefaultValue()
            objGrievanceStatusAndNature_Report._ReportId = cboReportType.SelectedValue
            objGrievanceStatusAndNature_Report._ReportName = cboReportType.Text
            objGrievanceStatusAndNature_Report._IncludeInactiveEmp = False

            objGrievanceStatusAndNature_Report._ViewByIds = mstrStringIds
            objGrievanceStatusAndNature_Report._ViewIndex = mintViewIdx
            objGrievanceStatusAndNature_Report._ViewByName = mstrStringName
            objGrievanceStatusAndNature_Report._Analysis_Fields = mstrAnalysis_Fields
            objGrievanceStatusAndNature_Report._Analysis_Join = mstrAnalysis_Join
            objGrievanceStatusAndNature_Report._Analysis_OrderBy = mstrAnalysis_OrderBy
            objGrievanceStatusAndNature_Report._Report_GroupName = mstrReport_GroupName
            objGrievanceStatusAndNature_Report._Advance_Filter = mstrAdvanceFilter


            If cboReportType.SelectedIndex = 0 Then
                objGrievanceStatusAndNature_Report._ReportId = 1
                If cboType.SelectedValue > 0 Then
                    objGrievanceStatusAndNature_Report._GrievanceResolutionType = cboType.SelectedValue
                End If
            Else
                objGrievanceStatusAndNature_Report._ReportId = 2
                If cboType.SelectedValue > 0 Then
                    objGrievanceStatusAndNature_Report._GrievanceType = cboType.SelectedValue
                End If
            End If


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmGrievanceStatusAndNatureReport_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objGrievanceStatusAndNature_Report = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGrievanceStatusAndNatureReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGrievanceStatusAndNatureReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objGrievanceStatusAndNature_Report._ReportName
            eZeeHeader.Message = objGrievanceStatusAndNature_Report._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveLiability_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGrievanceStatusAndNatureReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGrievanceStatusAndNatureReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGrievanceStatusAndNatureReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGrievanceStatusAndNatureReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLeaveLiability_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsGrievanceStatusAndNature_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objGrievanceStatusAndNature_Report.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                      , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                      , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._Base_CurrencyId)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
    '    Dim objfrm As New frmCommonSearch
    '    Dim objEmployee As New clsEmployee_Master
    '    Dim dtEmployee As DataTable
    '    Try
    '        dtEmployee = CType(cboEmployee.DataSource, DataTable)
    '        With cboEmployee
    '            objfrm.DataSource = dtEmployee
    '            objfrm.ValueMember = .ValueMember
    '            objfrm.DisplayMember = .DisplayMember
    '            objfrm.CodeMember = "employeecode"
    '            If objfrm.DisplayDialog Then
    '                .SelectedValue = objfrm.SelectedValue
    '            End If
    '            .Focus()
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
    '    Finally
    '        objfrm = Nothing
    '        objEmployee = Nothing
    '    End Try
    'End Sub




#End Region

#Region "Dropdown"
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try

            Dim dsList As New DataSet
            cboType.DataSource = Nothing
            If cboReportType.SelectedIndex = 0 Then
                Dim objMaster As New clsMasterData
                dsList = objMaster.GetGrievanceReportResolutionStatus(True, "StatusList")

                With cboType
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("StatusList")
                    .SelectedIndex = 0
                End With

                objlbltitle.Text = Language.getMessage(mstrModuleName, 3, "Grievance Status")

            ElseIf cboReportType.SelectedIndex = 1 Then
                objlbltitle.Text = Language.getMessage(mstrModuleName, 3, "Grievance Nature")

                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.GRIEVANCE_TYPE, True, "NatureList")


                With cboType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("NatureList")
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region
   

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Grievances Status Wise Report")
			Language.setMessage(mstrModuleName, 2, "Nature of Grievance Wise Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class
