﻿'Class Name : clsGroupTrainingCostReport.vb
'Purpose    :
'Date       : 04-Aug-2022
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsGroupTrainingCostReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsGroupTrainingCostReport"
    Private mstrReportId As String = enArutiReport.Group_Training_Cost_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintCalendarUnkid As Integer = 0
    Private mstrCalendarName As String = ""
    Private mintTrainingUnkid As Integer = 0
    Private mstrTrainingName As String = ""
    Private mdtApplicationDateFrom As Date = Nothing
    Private mdtApplicationDateTo As Date = Nothing
    Private mdtTrainingDateFrom As Date = Nothing
    Private mdtTrainingDateTo As Date = Nothing
    Private mintStatusUnkid As Integer = 0
    Private mstrStatusName As String = ""
    Private mstrfmtCurrency As String = GUI.fmtCurrency

#End Region

#Region " Properties "

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _ApplicationDateFrom() As Date
        Set(ByVal value As Date)
            mdtApplicationDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ApplicationDateTo() As Date
        Set(ByVal value As Date)
            mdtApplicationDateTo = value
        End Set
    End Property

    Public WriteOnly Property _TrainingDateFrom() As Date
        Set(ByVal value As Date)
            mdtTrainingDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _TrainingDateTo() As Date
        Set(ByVal value As Date)
            mdtTrainingDateTo = value
        End Set
    End Property

    Public WriteOnly Property _StatusUnkid() As Integer
        Set(ByVal value As Integer)
            mintStatusUnkid = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintCalendarUnkid = 0
            mstrCalendarName = ""
            mintTrainingUnkid = 0
            mstrTrainingName = ""
            mdtApplicationDateFrom = Nothing
            mdtApplicationDateTo = Nothing
            mdtTrainingDateFrom = Nothing
            mdtTrainingDateTo = Nothing
            mintStatusUnkid = 0
            mstrStatusName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.periodunkid = @periodunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Calendar :") & " " & mstrCalendarName & " "
            End If

            If mintTrainingUnkid > 0 Then
                objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.coursemasterunkid = @coursemasterunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Training :") & " " & mstrTrainingName & " "
            End If

            If mdtApplicationDateFrom <> Nothing Then
                objDataOperation.AddParameter("@ApplicationDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtApplicationDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),trtraining_request_master.application_date,112) >= @ApplicationDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Application Date From :") & " " & mdtApplicationDateFrom.Date & " "
            End If

            If mdtApplicationDateTo <> Nothing Then
                objDataOperation.AddParameter("@ApplicationDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtApplicationDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),trtraining_request_master.application_date,112) <= @ApplicationDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Application Date To") & " " & mdtApplicationDateTo.Date & " "
            End If

            If mdtTrainingDateFrom <> Nothing Then
                objDataOperation.AddParameter("@TrainingDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtTrainingDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),trtraining_request_master.start_date,112) >= @TrainingDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Training Date From :") & " " & mdtTrainingDateFrom.Date & " "
            End If

            If mdtTrainingDateTo <> Nothing Then
                objDataOperation.AddParameter("@TrainingDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtTrainingDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),trtraining_request_master.start_date,112) <= @TrainingDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Training Date To") & " " & mdtTrainingDateTo.Date & " "
            End If

            If mintStatusUnkid > 0 Then
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.statusunkid = @statusunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Status :") & " " & mstrStatusName & " "
            End If

            Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "startdate"
            OrderByQuery = "trtraining_request_master.application_date"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "
    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("SrNo", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Sr No")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Employee Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Department", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Department")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Branch", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Branch")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Zone", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Zone")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TrainingName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Training Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("StartDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Start Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EndDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "End Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            StrQ &= "SELECT " & _
                       "    ISNULL(hremployee_master.employeecode, '') AS Employeecode " & _
                       ",   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employeename " & _
                       ",   ISNULL(hsg.name, '') AS SectionGrp " & _
                       ",   ISNULL(hc.name, '') AS Class " & _
                       ",   ISNULL(hcg.name, '') AS ClassGrp " & _
                       ",   ISNULL(tcourse.name, '') AS trainingname " & _
                       ",   start_date as 'startdate' " & _
                       ",   end_date as  'enddate' " & _
                       ",   totaltrainingcost " & _
                       ",   refno " & _
                    "FROM trtraining_request_master " & _
                    "JOIN hremployee_master     ON trtraining_request_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN cfcommon_master AS tcourse     ON tcourse.masterunkid = trtraining_request_master.coursemasterunkid " & _
                              "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "   AND tcourse.isactive = 1 " & _
                    "LEFT JOIN (SELECT " & _
                              "stationunkid " & _
                            ",deptgroupunkid " & _
                            ",departmentunkid " & _
                            ",sectiongroupunkid " & _
                            ",sectionunkid " & _
                            ",unitgroupunkid " & _
                            ",unitunkid " & _
                            ",teamunkid " & _
                            ",classgroupunkid " & _
                            ",classunkid " & _
                            ",employeeunkid " & _
                            ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                         "FROM hremployee_transfer_tran " & _
                         "WHERE isvoid = 0 " & _
                         "AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate) AS T " & _
                         "ON T.employeeunkid = hremployee_master.employeeunkid " & _
                              "AND T.Rno = 1 " & _
                    "LEFT JOIN hrsectiongroup_master hsg " & _
                         "ON t.sectiongroupunkid = hsg.sectiongroupunkid " & _
                    "LEFT JOIN hrclassgroup_master hcg " & _
                         "ON t.classgroupunkid = hcg.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master hc " & _
                         "ON t.classunkid = hc.classesunkid "

            StrQ &= "WHERE trtraining_request_master.isvoid = 0 " & _
                    " AND trtraining_request_master.insertformid = " & enTrainingFormType.Group_Training

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intCount As Integer = 1
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            Dim strRefNo As String = String.Empty
            Dim decTotalAmount As Decimal = 0
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow
                If strRefNo <> drRow.Item("refno").ToString Then decTotalAmount = decTotalAmount + CDec(drRow.Item("totaltrainingcost"))
                rpt_Row.Item("SrNo") = intCount
                rpt_Row.Item("EmployeeCode") = drRow.Item("EmployeeCode")
                rpt_Row.Item("EmployeeName") = drRow.Item("EmployeeName")
                rpt_Row.Item("Department") = drRow.Item("SectionGrp")
                rpt_Row.Item("Branch") = drRow.Item("Class")
                rpt_Row.Item("Zone") = drRow.Item("ClassGrp")
                rpt_Row.Item("TrainingName") = drRow.Item("trainingname")
                rpt_Row.Item("StartDate") = CDate(drRow.Item("startdate")).ToShortDateString
                rpt_Row.Item("EndDate") = CDate(drRow.Item("enddate")).ToShortDateString

                dtFinalTable.Rows.Add(rpt_Row)
                strRefNo = drRow.Item("refno").ToString
                intCount = intCount + 1
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            wcell.MergeAcross = 5
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Total Amount:") & " " & Format(decTotalAmount, mstrfmtCurrency), "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = {100, 100, 200, 150, 100, 100, 200, 100, 100}
            'ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            'For i As Integer = 0 To intArrayColumnWidth.Length - 1
            '    intArrayColumnWidth(i) = 125
            'Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader, rowsArrayFooter)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sr No")
            Language.setMessage(mstrModuleName, 2, "Employee Code")
            Language.setMessage(mstrModuleName, 3, "Employee Name")
            Language.setMessage(mstrModuleName, 4, "Department")
            Language.setMessage(mstrModuleName, 5, "Branch")
            Language.setMessage(mstrModuleName, 6, "Zone")
            Language.setMessage(mstrModuleName, 7, "Training Name")
            Language.setMessage(mstrModuleName, 8, "Start Date")
            Language.setMessage(mstrModuleName, 9, "End Date")
            Language.setMessage(mstrModuleName, 10, "Calendar :")
            Language.setMessage(mstrModuleName, 11, "Application Date To")
            Language.setMessage(mstrModuleName, 12, "Application Date From :")
            Language.setMessage(mstrModuleName, 13, "Training :")
            Language.setMessage(mstrModuleName, 14, "Training Date To")
            Language.setMessage(mstrModuleName, 15, "Prepared By :")
            Language.setMessage(mstrModuleName, 16, "Checked By :")
            Language.setMessage(mstrModuleName, 17, "Approved By")
            Language.setMessage(mstrModuleName, 18, "Received By :")
            Language.setMessage(mstrModuleName, 19, "Status :")
            Language.setMessage(mstrModuleName, 20, "Training Date From :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
