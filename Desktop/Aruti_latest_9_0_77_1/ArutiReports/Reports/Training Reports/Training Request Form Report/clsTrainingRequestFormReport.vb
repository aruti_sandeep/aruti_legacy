﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports System

Public Class clsTrainingRequestFormReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingRequestFormReport"
    Private mstrReportId As String = enArutiReport.Training_Request_Form_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintRequestTypeId As Integer = 0
    Private mstrRequestTypeName As String = ""
    Private mintRequesterId As Integer = 0
    Private mstrRequesterName As String = ""
    Private mintCreateUserunkId As Integer = 0
    Private mintCreateLoginEmployeeUnkid As Integer = 0
    Private mstrRefNo As String = ""
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintTrainingTypeId As Integer = 0
    Private mstrTrainingTypeName As String = ""
    Private mintTrainingUnkid As Integer = 0
    Private mstrTrainingName As String = ""
    Private mintCompanyUnkid As Integer = -1
    Private mintUserUnkid As Integer = -1
    Private mstrUserName As String = ""
    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _RequestTypeId() As Integer
        Set(ByVal value As Integer)
            mintRequestTypeId = value
        End Set
    End Property

    Public WriteOnly Property _RequestTypeName() As String
        Set(ByVal value As String)
            mstrRequestTypeName = value
        End Set
    End Property

    Public WriteOnly Property _RequesterId() As Integer
        Set(ByVal value As Integer)
            mintRequesterId = value
        End Set
    End Property

    Public WriteOnly Property _CreateUserunkid() As Integer
        Set(ByVal value As Integer)
            mintCreateUserunkId = value
        End Set
    End Property

    Public WriteOnly Property _CreateLoginEmployeeunkid() As Integer
        Set(ByVal value As Integer)
            mintCreateLoginEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _RefNo() As String
        Set(ByVal value As String)
            mstrRefNo = value
        End Set
    End Property

    Public WriteOnly Property _RequesterName() As String
        Set(ByVal value As String)
            mstrRequesterName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingTypeId() As Integer
        Set(ByVal value As Integer)
            mintTrainingTypeId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingTypeName() As String
        Set(ByVal value As String)
            mstrTrainingTypeName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _User() As String
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property


    'Hemant (03 Oct 2022) -- Start
    'ISSUE(NMB) : - same employee same training same date different ref no with different cost showing in one Training Report Form Report preview.
    Private mintTrainingRequestunkid As Integer
    Public Property _TrainingRequestunkid() As Integer
        Get
            Return mintTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequestunkid = value
        End Set
    End Property
    'Hemant (03 Oct 2022) -- End   


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintRequestTypeId = 0
            mstrRequestTypeName = ""
            mintRequesterId = 0
            mstrRequesterName = ""
            mintCreateUserunkId = 0
            mintCreateLoginEmployeeUnkid = 0
            mstrRefNo = ""
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mintTrainingTypeId = 0
            mstrTrainingTypeName = ""
            mintTrainingUnkid = 0
            mstrTrainingName = ""
            mstrUserName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintRequestTypeId > 0 Then
                objDataOperation.AddParameter("@insertformid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequestTypeId)
                Me._FilterQuery &= " AND trtraining_request_master.insertformid = @insertformid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Request Type :") & " " & mstrRequestTypeName & " "
            End If

            If mintRequesterId > 0 Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = mintRequesterId
                Me._FilterQuery &= " AND ( trtraining_request_master.createuserunkid = @RequesterId #ORCOND# ) "
                objDataOperation.AddParameter("@RequesterId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequesterId)

                If objUser._EmployeeUnkid > 0 Then
                    Me._FilterQuery = Me._FilterQuery.Replace("#ORCOND#", "OR trtraining_request_master.createloginemployeeunkid = @createloginemployeeunkid")
                    objDataOperation.AddParameter("@createloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objUser._EmployeeUnkid)
                Else
                    Me._FilterQuery = Me._FilterQuery.Replace("#ORCOND#", "")
                End If
                objUser = Nothing
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Requester :") & " " & mstrRequesterName & " "
            End If

            If mintCreateUserunkId > 0 Then
                objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateUserunkId)
                Me._FilterQuery &= " AND trtraining_request_master.createuserunkid = @createuserunkid "
            End If

            If mintCreateLoginEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@createloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateLoginEmployeeUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.createloginemployeeunkid = @createloginemployeeunkid "
            End If

            If mstrRefNo.Trim.Length > 0 Then
                objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefNo)
                Me._FilterQuery &= " AND trtraining_request_master.refno = @refno "
            End If

            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintTrainingTypeId > 0 Then
                objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId)
                Me._FilterQuery &= " AND  trtraining_request_master.trainingtypeid = @trainingtypeid "
            End If

            If mintTrainingUnkid > 0 Then
                objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.coursemasterunkid = @coursemasterunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Training :") & " " & mstrTrainingName & " "
            End If

            'Hemant (03 Oct 2022) -- Start
            'ISSUE(NMB) : - same employee same training same date different ref no with different cost showing in one Training Report Form Report preview.
            If mintTrainingRequestunkid > 0 Then
                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid)
                Me._FilterQuery &= " AND trtraining_request_master.trainingrequestunkid = @trainingrequestunkid "
            End If
            'Hemant (03 Oct 2022) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid

            Dim mintCountryId As Integer = Company._Object._Countryunkid

            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function getTrainingList(ByVal intRequestTypeID As Integer, ByVal intRequesterID As Integer, _
                                   Optional ByVal intEmployeeID As Integer = -1, _
                                   Optional ByVal intTrainingType As Integer = -1, _
                                   Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as coursemasterunkid, ' ' +  @name as name  UNION "
            End If

            strQ &= "SELECT DISTINCT " & _
                    "   coursemasterunkid " & _
                    ",  ISNULL(cfcommon_master.name, '') AS name " & _
                    "FROM trtraining_request_master " & _
                    "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = trtraining_request_master.coursemasterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " AND isactive = 1 " & _
                    "WHERE trtraining_request_master.isvoid = 0 " 

            If intRequestTypeID > 0 Then
                strQ &= " AND trtraining_request_master.insertformid = '" & intRequestTypeID & "'"
            End If

            If intRequesterID > 0 Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = intRequesterID
                strQ &= " AND ( trtraining_request_master.createuserunkid = '" & intRequesterID & "' #ORCOND# ) "
                If objUser._EmployeeUnkid > 0 Then
                    strQ = strQ.Replace("#ORCOND#", "OR trtraining_request_master.createloginemployeeunkid = '" & objUser._EmployeeUnkid & "'")
                Else
                    strQ = strQ.Replace("#ORCOND#", "")
                End If
                objUser = Nothing
            End If

            If intEmployeeID > 0 Then
                strQ &= " AND trtraining_request_master.employeeunkid = '" & intEmployeeID & "'"
            End If

            If intTrainingType > 0 Then
                strQ &= " AND trtraining_request_master.trainingtypeid = '" & intTrainingType & "'"
            End If


            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getTrainingList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_TrainingApprovers As ArutiReport.Designer.dsArutiReport
        Dim rpt_CompletedTraining As ArutiReport.Designer.dsArutiReport
        Dim rpt_CostItem As ArutiReport.Designer.dsArutiReport


        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT * INTO #TrainingType FROM ( " & _
                    "SELECT 1 AS Id, @Local_Training AS Name UNION " & _
                    "SELECT 2 AS Id, @Local_Travel AS Name UNION " & _
                    "SELECT 3 AS Id, @Foreign_Travel AS Name " & _
                    ") AS A WHERE 1 = 1 "

            StrQ &= " SELECT  " & _
                       "    trtraining_request_master.trainingrequestunkid " & _
                       ",   trtraining_request_master.employeeunkid " & _
                       ",   trtraining_request_master.trainingtypeid " & _
                       ",   hremployee_master.employeecode " & _
                       ",   ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
                       ",   hrjob_master.job_name AS jobtitle " & _
                       ",   hrsectiongroup_master.name AS SectionGroup " & _
                       ",   ISNULL(tcourse.name, '') AS trainingname " & _
                       ",   ISNULL(hrinstitute_master.institute_name, '') AS  'trainingprovider'" & _
                       ",   trtraining_request_master.totaltrainingcost as totaltrainingcost " & _
                       ",   #TrainingType.Name As TrainingType " & _
                       ",   trtraining_request_master.start_date " & _
                       ",   trtraining_request_master.end_date " & _
                       ",   CASE WHEN ISNULL(trtraining_request_master.isalignedcurrentrole, 0) = 1 THEN @Yes ELSE @No END AS alignedcurrentrole " & _
                       ",   trtraining_request_master.expectedreturn " & _
                       ",   CASE WHEN trtraining_request_master.createloginemployeeunkid > 0 THEN REPLACE(ReqEmp.firstname + ' ' + ISNULL(ReqEmp.othername, '') + ' ' + ReqEmp.surname, '  ', ' ') ELSE CASE WHEN RTRIM(ReqUser.firstname) <> '' THEN ReqUser.firstname + ' ' + ReqUser.lastname ELSE ReqUser.username END END AS CreateUserName " & _
                       ",   ISNULL(trtraining_request_master.venue, '') AS Venue " & _
                       " FROM trtraining_request_master " & _
                       " JOIN hremployee_master ON trtraining_request_master.employeeunkid = hremployee_master.employeeunkid " & _
                       " JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "         jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                        " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                        " JOIN " & _
                                " ( " & _
                                "    SELECT " & _
                                "         departmentunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ISNULL(sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " LEFT JOIN hrsectiongroup_master ON Alloc.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                       " JOIN #TrainingType ON trtraining_request_master.trainingtypeid = #TrainingType.id " & _
                       " LEFT JOIN cfcommon_master AS tcourse  ON tcourse.masterunkid = trtraining_request_master.coursemasterunkid " & _
                              "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "   AND tcourse.isactive = 1 " & _
                       " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = trtraining_request_master.trainingproviderunkid AND hrinstitute_master.isactive = 1 " & _
                       "  LEFT JOIN hrmsConfiguration..cfuser_master AS ReqUser ON trtraining_request_master.createuserunkid = ReqUser.userunkid " & _
                       "  LEFT JOIN hremployee_master AS ReqEmp ON trtraining_request_master.createloginemployeeunkid = ReqEmp.employeeunkid " & _
                       " WHERE trtraining_request_master.isvoid = 0 "

            		    'Hemant (10 Nov 2022) -- [Venue]
            'StrQ &= " AND lvleaveform.formunkid = " & mintLeaveFormID


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            StrQ &= " drop table #TrainingType "

            objDataOperation.AddParameter("@Local_Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1184, "Local Training"))
            objDataOperation.AddParameter("@Local_Travel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1185, "Local Travel"))
            objDataOperation.AddParameter("@Foreign_Travel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1186, "Foreign Travel"))
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "No"))
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_TrainingApprovers = New ArutiReport.Designer.dsArutiReport
            rpt_CompletedTraining = New ArutiReport.Designer.dsArutiReport
            rpt_CostItem = New ArutiReport.Designer.dsArutiReport

            Dim objLeaveBal As New clsleavebalance_tran
            Dim objLeaveForm As New clsleaveform
            Dim objMaster As New clsMasterData

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("trainingrequestunkid")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = dtRow.Item("trainingname")
                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
                rpt_Rows.Item("Column5") = dtRow.Item("trainingprovider")
                rpt_Rows.Item("Column6") = dtRow.Item("SectionGroup")

                If Not IsDBNull(dtRow.Item("start_date")) Then
                    rpt_Rows.Item("Column7") = CDate(dtRow.Item("start_date")).ToShortDateString
                Else
                    rpt_Rows.Item("Column7") = ""
                End If

                If Not IsDBNull(dtRow.Item("end_date")) Then
                    rpt_Rows.Item("Column8") = CDate(dtRow.Item("end_date")).ToShortDateString
                Else
                    rpt_Rows.Item("Column8") = ""
                End If

                rpt_Rows.Item("Column9") = dtRow.Item("alignedcurrentrole")

                rpt_Rows.Item("Column10") = dtRow.Item("expectedreturn")

                rpt_Rows.Item("Column11") = Format(CDec(dtRow.Item("totaltrainingcost")), GUI.fmtCurrency)

                rpt_Rows.Item("Column12") = dtRow.Item("employeeunkid")
                'If mintLeaveTypeId <= 0 Then mintLeaveTypeId = CInt(dtRow.Item("leavetypeunkid"))
                'If mintEmployeeId <= 0 Then mintEmployeeId = CInt(dtRow.Item("employeeunkid"))
                'Hemant (03 Oct 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : AC2-916 - NMB - On training request form report, under applicant signature, system is picking approver’s username instead of requester’s username
                rpt_Rows.Item("Column13") = dtRow.Item("CreateUserName")
                'Hemant (03 Oct 2022) -- End
                rpt_Rows.Item("Column6") = dtRow.Item("SectionGroup")
                'Hemant (10 Nov 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-1021 - On the training request form report, I want to have the venue field populate the data from the venue filled on training request page and not from the backlog
                rpt_Rows.Item("Column14") = dtRow.Item("Venue")
                'Hemant (10 Nov 2022) -- End


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            StrQ = "SELECT " & _
                         "ISNULL(tcourse.name, '') AS trainingname " & _
                         ", employeeunkid " & _
                         ", ISNULL(trtraining_calendar_master.calendar_name, '') as calendar_name " & _
                    "FROM trtraining_request_master " & _
                    "JOIN trtraining_calendar_master on trtraining_calendar_master.calendarunkid = trtraining_request_master.periodunkid and trtraining_calendar_master.isactive = 1 " & _
                    "LEFT JOIN cfcommon_master AS tcourse " & _
                         "ON tcourse.masterunkid = trtraining_request_master.coursemasterunkid " & _
                              "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                              "AND tcourse.isactive = 1 " & _
                    "WHERE trtraining_request_master.isvoid = 0 " & _
                    "AND iscompleted_submit_approval = 1 " & _
                    "AND completed_statusunkid = " & enTrainingRequestStatus.APPROVED & " " & _
                    "and CONVERT(CHAR(8),trtraining_calendar_master.enddate,112)  >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.AddDays(-365).Date) & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_CompletedTraining.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("trainingname")
                rpt_Row.Item("Column2") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column3") = dtRow.Item("calendar_name")

                rpt_CompletedTraining.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            StrQ = "SELECT " & _
                         "trtraining_request_cost_tran.trainingrequestunkid " & _
                         ", ISNULL(trtrainingitemsinfo_master.info_name, '') as CostItem " & _
                         ", trtraining_request_cost_tran.amount as Itemamount " & _
                         ", ISNULL(trtraining_request_master.totaltrainingcost, 0) as TotalTrainingAmount " & _
                    "FROM trtraining_request_cost_tran " & _
                    "LEFT JOIN trtraining_request_master ON trtraining_request_master.trainingrequestunkid = trtraining_request_cost_tran.trainingrequestunkid AND trtraining_request_master.isvoid = 0 " & _
                    "LEFT JOIN trtrainingitemsinfo_master ON trtraining_request_cost_tran.trainingcostitemunkid = trtrainingitemsinfo_master.infounkid and trtrainingitemsinfo_master.isactive = 1 " & _
                    "AND trtraining_request_cost_tran.isvoid = 0 " & _
                    "WHERE trtraining_request_cost_tran.isvoid = 0 "

            objDataOperation.ClearParameters()
            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_CostItem.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("trainingrequestunkid")
                rpt_Row.Item("Column2") = dtRow.Item("CostItem")
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("Itemamount")), GUI.fmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("TotalTrainingAmount")), GUI.fmtCurrency)

                rpt_CostItem.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                   "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "FROM trtrainingapproval_process_tran " & _
                   "    JOIN trtrainingapprover_master ON trtrainingapprover_master.approverunkid = trtrainingapproval_process_tran.approvertranunkid  " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = trtrainingapprover_master.approverempunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0  " & _
                   "WHERE trtrainingapproval_process_tran.isvoid = 0 AND trtrainingapprover_master.isexternalapprover = 1 "

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrInnerQry = "SELECT trtraining_request_master.trainingrequestunkid " & _
                          ",    trtrainingapprover_master.approverunkid  " & _
                          ",    hrtraining_approverlevel_master.levelunkid " & _
                          ",    hrtraining_approverlevel_master.levelname " & _
                          ",    hrtraining_approverlevel_master.priority " & _
                          "    ,#APPVR_NAME# approvername " & _
                          "    ,#APPR_JOB_NAME# AS approvertitle " & _
                          ",    trtrainingapproval_process_tran.approvaldate " & _
                          ",    trtrainingapproval_process_tran.statusunkid " & _
                          ",    CASE WHEN trtrainingapproval_process_tran.statusunkid = " & enTrainingRequestStatus.PENDING & " THEN  @PENDING " & _
                          "          WHEN trtrainingapproval_process_tran.statusunkid = " & enTrainingRequestStatus.APPROVED & " THEN  @APPROVED " & _
                          "          WHEN trtrainingapproval_process_tran.statusunkid = " & enTrainingRequestStatus.REJECTED & " THEN  @REJECTED " & _
                          "     END status	 " & _
                          ",    trtrainingapproval_process_tran.remark " & _
                          " FROM trtrainingapproval_process_tran " & _
                          " JOIN trtraining_request_master ON trtrainingapproval_process_tran.trainingrequestunkid = trtraining_request_master.trainingrequestunkid AND trtraining_request_master.isvoid = 0 " & _
                          " JOIN trtrainingapprover_master ON trtrainingapproval_process_tran.approvertranunkid = trtrainingapprover_master.approverunkid " & _
                          " JOIN hrtraining_approverlevel_master ON trtrainingapprover_master.levelunkid = hrtraining_approverlevel_master.levelunkid " & _
                              " #COMM_JOIN# "

            StrConditionQry = " WHERE trtrainingapproval_process_tran.isvoid = 0 AND trtrainingapprover_master.isexternalapprover = #ExApprId# "

            'If mintEmployeeId > 0 Then
            '    StrConditionQry &= " AND lvpendingleave_tran.employeeunkid = " & mintEmployeeId
            'End If

            If mintTrainingTypeId > 0 Then
                StrConditionQry &= " AND trtraining_request_master.trainingtypeid = " & mintTrainingTypeId
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON trtrainingapprover_master.approverempunkid = hremployee_master.employeeunkid " & _
                                                                     " JOIN " & _
                                                                     " ( " & _
                                                                     "         SELECT " & _
                                                                     "         jobunkid " & _
                                                                     "        ,employeeunkid " & _
                                                                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                                     "    FROM #DB_Name#hremployee_categorization_tran " & _
                                                                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                                                     " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                                     " JOIN #DB_Name#hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid ")



            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            StrQ &= " ORDER BY hrtraining_approverlevel_master.priority "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1195, "Pending"))
            objDataOperation.AddParameter("@APPROVED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1196, "Approved"))
            objDataOperation.AddParameter("@REJECTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1197, "Rejected"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "'' ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = trtrainingapproval_process_tran.mapuserunkid ")
                Else
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = trtrainingapproval_process_tran.mapuserunkid " & _
                                                       " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid " & _
                                                       " JOIN " & _
                                                       " ( " & _
                                                       "         SELECT " & _
                                                       "         jobunkid " & _
                                                       "        ,employeeunkid " & _
                                                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                       "    FROM #DB_Name#hremployee_categorization_tran " & _
                                                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                                       " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                       " JOIN #DB_Name#hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid ")


                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next

            Dim lstTrainingRequestIds As IEnumerable(Of Int32) = dsList.Tables(0).AsEnumerable(). _
                                                            Select(Function(row) row.Field(Of Int32)("trainingrequestunkid")). _
                                                            Distinct()

            Dim arrTrainingRequestIds As Integer() = lstTrainingRequestIds.ToArray

            For Each intTrainingRequestID As Integer In arrTrainingRequestIds
                Dim drList() As DataRow = dsList.Tables(0).Select("trainingrequestunkid = " & intTrainingRequestID)
                If drList.Length > 0 Then
                    Dim dt As New DataTable
                    dt = drList.CopyToDataTable()

                    Dim mintPriorty As Integer = -1

                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
Recalculate:
                        Dim mintPrioriry As Integer = 0
                        For i As Integer = 0 To dt.Rows.Count - 1

                            If mintPrioriry <> CInt(dt.Rows(i)("priority")) Then
                                mintPrioriry = CInt(dt.Rows(i)("priority"))
                                Dim drRow() As DataRow = dt.Select("statusunkid <> 1 AND priority  = " & mintPrioriry)
                                If drRow.Length > 0 Then
                                    Dim drPending() As DataRow = dt.Select("statusunkid = 1 AND priority  = " & mintPrioriry)
                                    If drPending.Length > 0 Then
                                        Dim drPendingList() As DataRow = dsList.Tables(0).Select("trainingrequestunkid = " & intTrainingRequestID & " AND statusunkid = 1 AND priority  = " & mintPrioriry)
                                        If drPendingList.Length > 0 Then
                                            For Each dRowList As DataRow In drPendingList
                                                dsList.Tables(0).Rows.Remove(dRowList)
                                            Next
                                            dsList.Tables(0).AcceptChanges()
                                        End If
                                        For Each dRow As DataRow In drPending
                                            dt.Rows.Remove(dRow)
                                            GoTo Recalculate
                                        Next
                                        dt.AcceptChanges()
                                    End If
                                End If


                            End If

                        Next
                    End If
                End If
            Next



            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_TrainingApprovers.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("trainingrequestunkid")
                rpt_Row.Item("Column2") = dtRow.Item("approvername")
                rpt_Row.Item("Column3") = dtRow.Item("approvertitle")
                rpt_Row.Item("Column5") = dtRow.Item("status")
                rpt_Row.Item("Column4") = dtRow.Item("remark")

                rpt_TrainingApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptTrainingRequestForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)


            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 1, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 2, "Title :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 3, "Department :"))
            Call ReportFunction.TextChange(objRpt, "txtTraining", Language.getMessage(mstrModuleName, 4, "Training Name :"))
            Call ReportFunction.TextChange(objRpt, "txtVendor", Language.getMessage(mstrModuleName, 5, "Vendor Name :"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 6, "Start Date :"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 7, "End Date :"))
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            Call ReportFunction.TextChange(objRpt, "txtVenue", Language.getMessage(mstrModuleName, 27, "Venue :"))
            'Hemant (10 Nov 2022) -- End
            Call ReportFunction.TextChange(objRpt, "txtPastOneYearTraining", Language.getMessage(mstrModuleName, 9, "Have you attended any other training/trainings for the past one (1) year?  - If yes please list the trainings you have attended "))

            Call ReportFunction.TextChange(objRpt, "txtAlignedToCurrentRole", Language.getMessage(mstrModuleName, 12, "Is this training aligned with your current role?"))
            Call ReportFunction.TextChange(objRpt, "txtROI", Language.getMessage(mstrModuleName, 13, "ROI (Return On Investment) :"))
            Call ReportFunction.TextChange(objRpt, "txtApplicantSignature", Language.getMessage(mstrModuleName, 14, "Applicant’s Signature :"))
            Call ReportFunction.TextChange(objRpt, "txtUserName", mstrUserName)


            Dim dsLvBal As DataSet = Nothing
            Dim dvLeaveBal As DataTable = Nothing
            Dim mdtAsonDate As Date = Nothing


            objRpt.Subreports("rptTrainingApproverDetailReport").SetDataSource(rpt_TrainingApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainingApproverDetailReport"), "txtApprovals", Language.getMessage(mstrModuleName, 17, "APPROVALS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainingApproverDetailReport"), "txtApproverName", Language.getMessage(mstrModuleName, 18, "Approver Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainingApproverDetailReport"), "txtApproverTitle", Language.getMessage(mstrModuleName, 19, "Approver Title"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainingApproverDetailReport"), "txtRemarks", Language.getMessage(mstrModuleName, 20, "Comments"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainingApproverDetailReport"), "txtApprovalStatus", Language.getMessage(mstrModuleName, 21, "Approval Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainingApproverDetailReport"), "txtGuidelines", Language.getMessage(mstrModuleName, 22, "**Please refer to below approval matrix as per the Delegated Authority Guidelines"))

            objRpt.Subreports("rptCompletedTraining").SetDataSource(rpt_CompletedTraining)

            objRpt.Subreports("rptCostItem").SetDataSource(rpt_CostItem)
            Call ReportFunction.TextChange(objRpt.Subreports("rptCostItem"), "txtTrainingCost", Language.getMessage(mstrModuleName, 10, "Training Cost :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCostItem"), "txtCostItem", Language.getMessage(mstrModuleName, 25, "Cost Item:"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCostItem"), "txtAmount", Language.getMessage(mstrModuleName, 26, "Amount"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCostItem"), "txtTotalCost", Language.getMessage(mstrModuleName, 11, "Total Cost Implication :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 23, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 24, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-995 - On training reports, system is fetching the wrong usernames on printed by section
            'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", mstrUserName)
            'Hemant (27 Oct 2022) -- End


            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Name :")
            Language.setMessage(mstrModuleName, 2, "Title :")
            Language.setMessage(mstrModuleName, 3, "Department :")
            Language.setMessage(mstrModuleName, 4, "Training Name :")
            Language.setMessage(mstrModuleName, 5, "Vendor Name :")
            Language.setMessage(mstrModuleName, 6, "Start Date :")
            Language.setMessage(mstrModuleName, 7, "End Date :")
            Language.setMessage(mstrModuleName, 9, "Have you attended any other training/trainings for the past one (1) year?  - If yes please list the trainings you have attended")
            Language.setMessage(mstrModuleName, 10, "Training Cost :")
            Language.setMessage(mstrModuleName, 11, "Total Cost Implication :")
            Language.setMessage(mstrModuleName, 12, "Is this training aligned with your current role?")
            Language.setMessage(mstrModuleName, 13, "ROI (Return On Investment) :")
            Language.setMessage(mstrModuleName, 14, "Applicant’s Signature :")
            Language.setMessage(mstrModuleName, 15, "Yes")
            Language.setMessage(mstrModuleName, 16, "No")
            Language.setMessage(mstrModuleName, 17, "APPROVALS")
            Language.setMessage(mstrModuleName, 18, "Approver Name")
            Language.setMessage(mstrModuleName, 19, "Approver Title")
            Language.setMessage(mstrModuleName, 20, "Comments")
            Language.setMessage(mstrModuleName, 21, "Approval Status")
            Language.setMessage(mstrModuleName, 22, "**Please refer to below approval matrix as per the Delegated Authority Guidelines")
            Language.setMessage(mstrModuleName, 23, "Printed By :")
            Language.setMessage(mstrModuleName, 24, "Printed Date :")
            Language.setMessage(mstrModuleName, 25, "Cost Item:")
            Language.setMessage(mstrModuleName, 26, "Amount")
			Language.setMessage(mstrModuleName, 27, "Venue :")
			Language.setMessage(mstrModuleName, 28, "Select")
			Language.setMessage(mstrModuleName, 29, "Request Type :")
			Language.setMessage(mstrModuleName, 30, "Requester :")
			Language.setMessage(mstrModuleName, 31, "Training :")
			Language.setMessage(mstrModuleName, 32, "Employee :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
