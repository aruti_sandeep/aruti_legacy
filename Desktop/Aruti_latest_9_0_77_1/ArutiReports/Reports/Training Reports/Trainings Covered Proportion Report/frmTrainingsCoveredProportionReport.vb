﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmTrainingsCoveredProportionReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingsCoveredProportionReport"
    Private objTrainingsCoveredProportionReport As clsTrainingsCoveredProportionReport
    Private mstrSearchEmpText As String = ""
    Private dvEmployee As DataView
    Private mlstIds As List(Of String)
#End Region

#Region " Constructor "
    Public Sub New()
        objTrainingsCoveredProportionReport = New clsTrainingsCoveredProportionReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTrainingsCoveredProportionReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmTrainingsCoveredProportionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingsCoveredProportionReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingsCoveredProportionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingsCoveredProportionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingsCoveredProportionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingsCoveredProportionReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingsCoveredProportionReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingsCoveredProportionReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingsCoveredProportionReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingsCoveredProportionReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingsCoveredProportionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Dim objCMaster As New clsMasterData
        Try
            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objCMaster.GetEAllocation_Notification("AList", "", False, False)
            Dim dRRow As DataRow = dsCombo.Tables(0).NewRow
            dRRow("id") = 0
            dRRow("Name") = Language.getMessage(mstrModuleName, 4, "Select")
            dsCombo.Tables(0).Rows.InsertAt(dRRow, 0)
            With cboAllocation
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("AList")
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCalendar = Nothing
            objCommon = Nothing
            objCMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            cboAllocation.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            chkInactiveemp.Checked = False
            'Hemant (28 Oct 2021) -- End
            objTrainingsCoveredProportionReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Traininig Calendar."), enMsgBoxStyle.Information)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            If CInt(cboAllocation.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Allocation."), enMsgBoxStyle.Information)
                cboAllocation.Focus()
                Exit Function
            End If

            mlstIds = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("id").ToString)).ToList
            If mlstIds.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Atleast one Allocation."), enMsgBoxStyle.Information)
                Exit Function
            End If

            objTrainingsCoveredProportionReport.SetDefaultValue()

            objTrainingsCoveredProportionReport._CalendarUnkid = cboTrainingCalendar.SelectedValue
            objTrainingsCoveredProportionReport._CalendarName = cboTrainingCalendar.Text
            objTrainingsCoveredProportionReport._AllocationId = cboAllocation.SelectedValue
            objTrainingsCoveredProportionReport._AllocationName = cboAllocation.Text
            objTrainingsCoveredProportionReport._TrainingUnkid = cboTrainingName.SelectedValue
            objTrainingsCoveredProportionReport._TrainingName = cboTrainingName.Text
            objTrainingsCoveredProportionReport._Ids = mlstIds
            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            objTrainingsCoveredProportionReport._IsActive = CBool(chkInactiveemp.Checked)
            'Hemant (28 Oct 2021) -- End
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocation.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    dList.Tables(0).Columns("jobunkid").ColumnName = "Id"
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.getComboList("List")
                    dList.Tables(0).Columns(0).ColumnName = "Id"
                    dList.Tables(0).Columns("costcentername").ColumnName = "name"
            End Select

            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged

            Dim dtTable As DataTable = New DataView(dList.Tables("List"), "id > 0", "", DataViewRowState.CurrentRows).ToTable

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvEmployee = dtTable.DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhunkid.DataPropertyName = "id"
            dgColhName.DataPropertyName = "name"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, name "

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpText", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try

            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

            Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub


            objTrainingsCoveredProportionReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, False, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTrainingName.DataSource
            frm.ValueMember = cboTrainingName.ValueMember
            frm.DisplayMember = cboTrainingName.DisplayMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboTrainingName.SelectedValue = frm.SelectedValue
                cboTrainingName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "
    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Try
            If CInt(cboAllocation.SelectedValue) > 0 Then
                Call Fill_Data()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region "Textbox Events"
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = " name LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

#End Region


#Region " CheckBox's Events "
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.lblTrainingName.Text = Language._Object.getCaption(Me.lblTrainingName.Name, Me.lblTrainingName.Text)
			Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.Name, Me.lblTrainingCalendar.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.dgColhName.HeaderText = Language._Object.getCaption(Me.dgColhName.Name, Me.dgColhName.HeaderText)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Traininig Calendar.")
			Language.setMessage(mstrModuleName, 2, "Please Select Allocation.")
			Language.setMessage(mstrModuleName, 3, "Please Select Atleast one Allocation.")
			Language.setMessage(mstrModuleName, 4, "Select")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class