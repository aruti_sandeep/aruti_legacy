﻿Option Strict On
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Drawing.Printing

#End Region
Public Class frmTrainingBudgetReportNew

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingBudgetReportNew"
    Private objTrainingBudgetReport As clsTrainingBudgetReportNew
    Private mstrChartTitle As String = String.Empty
    Private dsChartDataSet As New DataSet
#End Region


#Region " Constructor "

    Public Sub New()
        objTrainingBudgetReport = New clsTrainingBudgetReportNew(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTrainingBudgetReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With


            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCalendar = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            chkIncludeZeroCost.Checked = False
            pnlMain.Visible = False
            chAnalysis_Chart.DataSource = Nothing
            btnOperation.Visible = False
            objTrainingBudgetReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Training Calendar."), enMsgBoxStyle.Information)
                cboTrainingCalendar.Focus()
                Exit Function
            End If


            objTrainingBudgetReport.SetDefaultValue()

            objTrainingBudgetReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingBudgetReport._CalendarName = cboTrainingCalendar.Text

            objTrainingBudgetReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingBudgetReport._TrainingName = cboTrainingName.Text

            objTrainingBudgetReport._IsIncludeZeroCost = CBool(chkIncludeZeroCost.Checked)

            objTrainingBudgetReport._TrainingNeedAllocationID = ConfigParameter._Object._TrainingNeedAllocationID

            Select Case ConfigParameter._Object._TrainingNeedAllocationID
                Case enAllocation.BRANCH
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 430, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 429, "Department Group")
                Case enAllocation.DEPARTMENT
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 428, "Department")
                Case enAllocation.SECTION_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 427, "Section Group")
                Case enAllocation.SECTION
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 426, "Section")
                Case enAllocation.UNIT_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 425, "Unit Group")
                Case enAllocation.UNIT
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 424, "Unit")
                Case enAllocation.TEAM
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 423, "Team")
                Case enAllocation.JOB_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 422, "Job Group")
                Case enAllocation.JOBS
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 421, "Jobs")
                Case enAllocation.CLASS_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 420, "Class Group")
                Case enAllocation.CLASSES
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 419, "Classes")
                Case Else
                    objTrainingBudgetReport._TrainingNeedAllocationName = ""
            End Select
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub Fill_Chart()
        Try
            chAnalysis_Chart.Series(0).Points.DataBindXY(dsChartDataSet.Tables(0).DefaultView, "xAxis", dsChartDataSet.Tables(0).DefaultView, "yAxis1")

            chAnalysis_Chart.ChartAreas(0).AxisX.LabelStyle.Interval = 1
            chAnalysis_Chart.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 2, "Training")
            chAnalysis_Chart.ChartAreas(0).AxisX.LabelStyle.Angle = -25
            chAnalysis_Chart.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 3, "Total Approved Cost")
            chAnalysis_Chart.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
            chAnalysis_Chart.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)

            chAnalysis_Chart.Titles(0).Text = Language.getMessage(mstrModuleName, 4, "Training Cost Analysis")
            chAnalysis_Chart.Titles(0).Font = New Font(Me.Font.Name, 11, FontStyle.Bold)
            chAnalysis_Chart.Series(0).IsValueShownAsLabel = True
            chAnalysis_Chart.Series(0)("DrawingStyle") = "Cylinder"

            chAnalysis_Chart.Series(0).IsVisibleInLegend = False

            chAnalysis_Chart.Series(0).Color = Color.RoyalBlue

            chAnalysis_Chart.Series(0)("PixelPointWidth") = "25"

            chAnalysis_Chart.ChartAreas(0).AxisX.MajorGrid.Enabled = False
            chAnalysis_Chart.ChartAreas(0).AxisX.MinorGrid.Enabled = False

            chAnalysis_Chart.ChartAreas(0).AxisY.MajorGrid.Enabled = False
            chAnalysis_Chart.ChartAreas(0).AxisY.MinorGrid.Enabled = False

            chAnalysis_Chart.Series(0).ChartType = SeriesChartType.Column

            For i As Integer = 0 To chAnalysis_Chart.Series.Count - 1
                For Each dp As DataPoint In chAnalysis_Chart.Series(i).Points
                    If dp.YValues(0) = 0 Then
                        dp.Label = " "
                    End If
                Next
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Chart", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmTrainingBudgetReportNew_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingBudgetReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingBudgetReportNew_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingBudgetReportNew_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingBudgetReportNew_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingBudgetReportNew_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExecutedTrainingsReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingBudgetReportNew_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "frmTrainingBudgetReportNew_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub


            objTrainingBudgetReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, True, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboTrainingName.DataSource, DataTable)
            frm.ValueMember = cboTrainingName.ValueMember
            frm.DisplayMember = cboTrainingName.DisplayMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboTrainingName.SelectedValue = frm.SelectedValue
                cboTrainingName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingBudgetReportNew.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingBudgetReportNew"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Link Buttons Events"
    
    Private Sub lnkDisplayChart_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDisplayChart.LinkClicked
        Dim dsList As DataSet
        Try
            If SetFilter() = False Then Exit Sub

            'objTrainingBudgetReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                              True, _
            '                              ConfigParameter._Object._ExportReportPath, _
            '                              ConfigParameter._Object._OpenAfterExport, _
            '                              0, enPrintAction.Preview, enExportAction.None)
            dsList = objTrainingBudgetReport.GetQueryData(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                                             True _
                                                             )

            Dim iList As List(Of Integer) = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("trainingcourseunkid")).Distinct().ToList()
            dsChartDataSet = dsList.Clone()
            For Each iTrainingId As Integer In iList
                Dim dr() As DataRow = dsList.Tables(0).Select("trainingcourseunkid = " & CInt(iTrainingId))
                If dr.Length > 0 Then
                    Dim dt As DataTable = dr.CopyToDataTable()
                    Dim decTotApprovedCost As Decimal = CDec(dt.Compute("SUM(totalapprovedbudget)", "1=1"))
                    Dim drRow As DataRow = dsChartDataSet.Tables(0).NewRow
                    drRow("trainingcourseunkid") = CInt(iTrainingId)
                    drRow("trainingcoursename") = dr(0)("trainingcoursename")
                    drRow("totalapprovedbudget") = Format(CDec(decTotApprovedCost), GUI.fmtCurrency)
                    dsChartDataSet.Tables(0).Rows.Add(drRow)
                End If

            Next
            dsChartDataSet.Tables(0).Columns("trainingcoursename").ColumnName = "xAxis"
            dsChartDataSet.Tables(0).Columns("totalapprovedbudget").ColumnName = "yAxis1"

            If dsChartDataSet.Tables(0).Rows.Count > 0 Then
                pnlMain.Visible = True
                btnOperation.Visible = True
                Fill_Chart()
            Else
                pnlMain.Visible = False
                btnOperation.Visible = False
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, No Training data present for the selected Calendar."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkDisplayChart_Click", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

#Region " Controls "
    Private Sub mnuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSave.Click
        Try
            Dim saveFileDialog1 As New SaveFileDialog()
            saveFileDialog1.Filter = "Bitmap (*.bmp)|*.bmp|JPEG (*.jpg)|*.jpg|EMF (*.emf)|*.emf|PNG (*.png)|*.png|GIF (*.gif)|*.gif|TIFF (*.tif)|*.tif"
            saveFileDialog1.FilterIndex = 2
            saveFileDialog1.RestoreDirectory = True

            If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                Dim format As ChartImageFormat = ChartImageFormat.Bmp
                If saveFileDialog1.FileName.EndsWith("bmp") Then
                    format = ChartImageFormat.Bmp
                ElseIf saveFileDialog1.FileName.EndsWith("jpg") Then
                    format = ChartImageFormat.Jpeg
                ElseIf saveFileDialog1.FileName.EndsWith("emf") Then
                    format = ChartImageFormat.Emf
                ElseIf saveFileDialog1.FileName.EndsWith("gif") Then
                    format = ChartImageFormat.Gif
                ElseIf saveFileDialog1.FileName.EndsWith("png") Then
                    format = ChartImageFormat.Png
                ElseIf saveFileDialog1.FileName.EndsWith("tif") Then
                    format = ChartImageFormat.Tiff
                End If
                chAnalysis_Chart.SaveImage(saveFileDialog1.FileName, format)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrintPreview.Click
        Try
            chAnalysis_Chart.Printing.PrintDocument.DefaultPageSettings.Landscape = False

            chAnalysis_Chart.Printing.PrintDocument.DefaultPageSettings.Margins = New System.Drawing.Printing.Margins(100, 100, 100, 100)

            Dim pr As PrinterResolution
            For Each pr In chAnalysis_Chart.Printing.PrintDocument.PrinterSettings.PrinterResolutions
                If pr.Kind.ToString() = "Low" Then
                    chAnalysis_Chart.Printing.PrintDocument.DefaultPageSettings.PrinterResolution = pr
                End If
            Next
            chAnalysis_Chart.Printing.PrintPreview()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrintPreview_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        Try
            chAnalysis_Chart.Printing.PrintDocument.DefaultPageSettings.Landscape = True
            chAnalysis_Chart.Printing.PrintDocument.DefaultPageSettings.Margins = New System.Drawing.Printing.Margins(100, 100, 100, 100)
            Dim pr As PrinterResolution
            For Each pr In chAnalysis_Chart.Printing.PrintDocument.PrinterSettings.PrinterResolutions
                If pr.Kind.ToString() = "Low" Then
                    chAnalysis_Chart.Printing.PrintDocument.DefaultPageSettings.PrinterResolution = pr
                End If
            Next
            chAnalysis_Chart.Printing.Print(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrint_Click", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkIncludeZeroCost.Text = Language._Object.getCaption(Me.chkIncludeZeroCost.Name, Me.chkIncludeZeroCost.Text)
			Me.lblTrainingName.Text = Language._Object.getCaption(Me.lblTrainingName.Name, Me.lblTrainingName.Text)
			Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.Name, Me.lblTrainingCalendar.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lnkDisplayChart.Text = Language._Object.getCaption(Me.lnkDisplayChart.Name, Me.lnkDisplayChart.Text)
			Me.chAnalysis_Chart.Text = Language._Object.getCaption(Me.chAnalysis_Chart.Name, Me.chAnalysis_Chart.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuSave.Text = Language._Object.getCaption(Me.mnuSave.Name, Me.mnuSave.Text)
			Me.mnuPrintPreview.Text = Language._Object.getCaption(Me.mnuPrintPreview.Name, Me.mnuPrintPreview.Text)
			Me.mnuPrint.Text = Language._Object.getCaption(Me.mnuPrint.Name, Me.mnuPrint.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			
            Language.setMessage(mstrModuleName, 1, "Please Select Training Calendar.")
			Language.setMessage(mstrModuleName, 2, "Training")
			Language.setMessage(mstrModuleName, 3, "Total Approved Cost")
			Language.setMessage(mstrModuleName, 4, "Training Cost Analysis")
			Language.setMessage(mstrModuleName, 5, "Sorry, No Training data present for the selected Calendar.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class