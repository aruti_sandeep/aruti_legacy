﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Drawing.Printing

#End Region

Public Class frmTrainingRequestBudgetReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingRequestBudgetReport"
    Private objTrainingBudgetReport As clsTrainingRequestBudgetReport
#End Region


#Region " Constructor "

    Public Sub New()
        objTrainingBudgetReport = New clsTrainingRequestBudgetReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTrainingBudgetReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim dsList As DataSet = Nothing
        Dim intColType As Integer = 0
        Dim dtTable As DataTable
        Dim strAllocationName As String = ""
        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With


            Select Case ConfigParameter._Object._TrainingBudgetAllocationID

                Case enAllocation.BRANCH
                    strAllocationName = Language.getMessage("clsMasterData", 430, "Branch")
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                Case enAllocation.DEPARTMENT_GROUP
                    strAllocationName = Language.getMessage("clsMasterData", 429, "Department Group")
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing

                Case enAllocation.DEPARTMENT
                    strAllocationName = Language.getMessage("clsMasterData", 428, "Department")
                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing

                Case enAllocation.SECTION_GROUP
                    strAllocationName = Language.getMessage("clsMasterData", 427, "Section Group")
                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing

                Case enAllocation.SECTION
                    strAllocationName = Language.getMessage("clsMasterData", 426, "Section")
                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing

                Case enAllocation.UNIT_GROUP
                    strAllocationName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing

                Case enAllocation.UNIT
                    strAllocationName = Language.getMessage("clsMasterData", 424, "Unit")
                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing

                Case enAllocation.TEAM
                    strAllocationName = Language.getMessage("clsMasterData", 423, "Team")
                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing

                Case enAllocation.JOB_GROUP
                    strAllocationName = Language.getMessage("clsMasterData", 422, "Job Group")
                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing

                Case enAllocation.JOBS
                    strAllocationName = Language.getMessage("clsMasterData", 421, "Jobs")
                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1

                Case enAllocation.CLASS_GROUP
                    strAllocationName = Language.getMessage("clsMasterData", 420, "Class Group")
                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing

                Case enAllocation.CLASSES
                    strAllocationName = Language.getMessage("clsMasterData", 419, "Classes")
                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing

                Case enAllocation.COST_CENTER
                    strAllocationName = Language.getMessage("clsMasterData", 586, "Cost Center")
                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2

            End Select

            If dsList IsNot Nothing Then

                dtTable = New DataView(dsList.Tables(0)).ToTable

                If intColType = 0 Then
                    dtTable.Columns(0).ColumnName = "Id"
                    dtTable.Columns("code").ColumnName = "Code"
                    dtTable.Columns("name").ColumnName = "Name"
                ElseIf intColType = 1 Then
                    dtTable.Columns("jobunkid").ColumnName = "Id"
                    dtTable.Columns("Code").ColumnName = "Code"
                    dtTable.Columns("jobname").ColumnName = "Name"
                ElseIf intColType = 2 Then
                    dtTable.Columns(0).ColumnName = "Id"
                    dtTable.Columns("costcentercode").ColumnName = "Code"
                    dtTable.Columns("costcentername").ColumnName = "Name"
                ElseIf intColType = 3 Then
                    dtTable.Columns(1).ColumnName = "Id"
                    dtTable.Columns(2).ColumnName = "Code"
                    dtTable.Columns(0).ColumnName = "Name"
                ElseIf intColType = 4 Then
                    dtTable.Columns(0).ColumnName = "Id"
                    dtTable.Columns("country_code").ColumnName = "Code"
                    dtTable.Columns("country_name").ColumnName = "Name"
                ElseIf intColType = 5 Then
                    dtTable.Columns("employeeunkid").ColumnName = "Id"
                    dtTable.Columns("employeecode").ColumnName = "Code"
                    dtTable.Columns("employeename").ColumnName = "Name"
                End If

                dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

                Dim row As DataRow = dtTable.NewRow
                row.Item("id") = 0
                row.Item("code") = ""
                row.Item("name") = Language.getMessage(mstrModuleName, 2, " Select")
                dtTable.Rows.InsertAt(row, 0)

                With cboAllocationTran
                    .DisplayMember = "Name"
                    .ValueMember = "Id"
                    .DataSource = dtTable
                    .SelectedValue = 0
                End With

                lblAllocationTran.Text = strAllocationName
            End If
           

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objCalendar = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            cboAllocationTran.SelectedIndex = 0

            objTrainingBudgetReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboTrainingCalendar.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Training Calendar."), enMsgBoxStyle.Information)
                cboTrainingCalendar.Focus()
                Exit Function
            End If


            objTrainingBudgetReport.SetDefaultValue()

            objTrainingBudgetReport._CurrentPeriodUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingBudgetReport._CurrentPeriodName = cboTrainingCalendar.Text

            objTrainingBudgetReport._PreviousPeriodUnkid = 0
            objTrainingBudgetReport._PreviousPeriodName = ""

            objTrainingBudgetReport._TrainingNeedAllocationID = ConfigParameter._Object._TrainingNeedAllocationID
            objTrainingBudgetReport._BudgetAllocationID = ConfigParameter._Object._TrainingBudgetAllocationID
            objTrainingBudgetReport._TrainingCostCenterAllocationID = ConfigParameter._Object._TrainingCostCenterAllocationID
            objTrainingBudgetReport._TrainingRemainingBalanceBasedOnID = ConfigParameter._Object._TrainingRemainingBalanceBasedOnID

            If CInt(cboAllocationTran.SelectedValue) > 0 Then
                objTrainingBudgetReport._AllocationTranIDs = CInt(cboAllocationTran.SelectedValue).ToString
                objTrainingBudgetReport._AllocationTranNAMEs = cboAllocationTran.Text
            Else
                objTrainingBudgetReport._AllocationTranIDs = ""
                objTrainingBudgetReport._AllocationTranNAMEs = ""
            End If
            


            Select Case ConfigParameter._Object._TrainingNeedAllocationID
                Case enAllocation.BRANCH
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 430, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 429, "Department Group")
                Case enAllocation.DEPARTMENT
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 428, "Department")
                Case enAllocation.SECTION_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 427, "Section Group")
                Case enAllocation.SECTION
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 426, "Section")
                Case enAllocation.UNIT_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 425, "Unit Group")
                Case enAllocation.UNIT
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 424, "Unit")
                Case enAllocation.TEAM
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 423, "Team")
                Case enAllocation.JOB_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 422, "Job Group")
                Case enAllocation.JOBS
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 421, "Jobs")
                Case enAllocation.CLASS_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 420, "Class Group")
                Case enAllocation.CLASSES
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 419, "Classes")
                Case enAllocation.COST_CENTER
                    objTrainingBudgetReport._TrainingNeedAllocationName = Language.getMessage("clsMasterData", 586, "Cost Center")
                Case Else
                    objTrainingBudgetReport._TrainingNeedAllocationName = ""
            End Select

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function



#End Region

#Region " Form's Events "

    Private Sub frmTrainingRequestBudgetReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingBudgetReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingRequestBudgetReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingRequestBudgetReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingRequestBudgetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingRequestBudgetReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExecutedTrainingsReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingRequestBudgetReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "frmTrainingRequestBudgetReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub


            objTrainingBudgetReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, True, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAllocation.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboAllocationTran.DataSource, DataTable)
            frm.ValueMember = cboAllocationTran.ValueMember
            frm.DisplayMember = cboAllocationTran.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboAllocationTran.SelectedValue = frm.SelectedValue
                cboAllocationTran.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAllocation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingRequestBudgetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingRequestBudgetReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAllocationTran.Text = Language._Object.getCaption(Me.lblAllocationTran.Name, Me.lblAllocationTran.Text)
			Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.Name, Me.lblTrainingCalendar.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Training Calendar.")
			Language.setMessage(mstrModuleName, 2, " Select")
			Language.setMessage("clsMasterData", 419, "Classes")
			Language.setMessage("clsMasterData", 420, "Class Group")
			Language.setMessage("clsMasterData", 421, "Jobs")
			Language.setMessage("clsMasterData", 422, "Job Group")
			Language.setMessage("clsMasterData", 423, "Team")
			Language.setMessage("clsMasterData", 424, "Unit")
			Language.setMessage("clsMasterData", 425, "Unit Group")
			Language.setMessage("clsMasterData", 426, "Section")
			Language.setMessage("clsMasterData", 427, "Section Group")
			Language.setMessage("clsMasterData", 428, "Department")
			Language.setMessage("clsMasterData", 429, "Department Group")
			Language.setMessage("clsMasterData", 430, "Branch")
			Language.setMessage("clsMasterData", 586, "Cost Center")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class