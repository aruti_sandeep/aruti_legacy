﻿'Class Name : clsTrainingBudgetReportNew.vb
'Purpose    :
'Date       : 03-Mar-2022
'Written By : Sohail
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsTrainingRequestBudgetReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingRequestBudgetReport"
    Private mstrReportId As String = enArutiReport.Training_Request_Budget_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintCurrentPeriodUnkId As Integer = 0
    Private mstrCurrentPeriodName As String = String.Empty
    Private mintPreviousPeriodUnkId As Integer = 0
    Private mstrPreviousPeriodName As String = String.Empty
    Private mintTrainingNeedAllocationID As Integer = -1
    Private mstrTrainingNeedAllocationName As String = String.Empty
    Private mintBudgetAllocationID As Integer = -1
    Private mstrBudgetAllocationName As String = String.Empty
    Private mintTrainingCostCenterAllocationID As Integer = -1
    Private mstrTrainingCostCenterAllocationName As String = String.Empty
    Private mintTrainingRemainingBalanceBasedOnID As Integer = -1
    Private mstrTrainingRemainingBalanceBasedOnName As String = String.Empty
    Private mstrAllocationTranIDs As String = String.Empty
    Private mstrAllocationTranNAMEs As String = String.Empty

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#End Region

#Region " Properties "

    Public WriteOnly Property _CurrentPeriodUnkid() As Integer
        Set(ByVal value As Integer)
            mintCurrentPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _CurrentPeriodName() As String
        Set(ByVal value As String)
            mstrCurrentPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PreviousPeriodUnkid() As Integer
        Set(ByVal value As Integer)
            mintPreviousPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _PreviousPeriodName() As String
        Set(ByVal value As String)
            mstrPreviousPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingNeedAllocationID() As Integer
        Set(ByVal value As Integer)
            mintTrainingNeedAllocationID = value
        End Set
    End Property

    Public WriteOnly Property _TrainingNeedAllocationName() As String
        Set(ByVal value As String)
            mstrTrainingNeedAllocationName = value
        End Set
    End Property

    Public WriteOnly Property _BudgetAllocationID() As Integer
        Set(ByVal value As Integer)
            mintBudgetAllocationID = value
        End Set
    End Property

    Public WriteOnly Property _BudgetAllocationName() As String
        Set(ByVal value As String)
            mstrBudgetAllocationName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingCostCenterAllocationID() As Integer
        Set(ByVal value As Integer)
            mintTrainingCostCenterAllocationID = value
        End Set
    End Property

    Public WriteOnly Property _TrainingCostCenterAllocationName() As String
        Set(ByVal value As String)
            mstrTrainingCostCenterAllocationName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingRemainingBalanceBasedOnID() As Integer
        Set(ByVal value As Integer)
            mintTrainingRemainingBalanceBasedOnID = value
        End Set
    End Property

    Public WriteOnly Property _TrainingRemainingBalanceBasedOnName() As String
        Set(ByVal value As String)
            mstrTrainingRemainingBalanceBasedOnName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTranIDs() As String
        Set(ByVal value As String)
            mstrAllocationTranIDs = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTranNAMEs() As String
        Set(ByVal value As String)
            mstrAllocationTranNAMEs = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

#End Region

#Region "Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCurrentPeriodUnkId = 0
            mstrCurrentPeriodName = String.Empty
            mintPreviousPeriodUnkId = 0
            mstrPreviousPeriodName = String.Empty
            mintTrainingNeedAllocationID = 0
            mstrTrainingNeedAllocationName = String.Empty
            mintBudgetAllocationID = 0
            mstrBudgetAllocationName = String.Empty
            mintTrainingCostCenterAllocationID = 0
            mstrTrainingCostCenterAllocationName = String.Empty
            mintTrainingRemainingBalanceBasedOnID = 0
            mstrTrainingRemainingBalanceBasedOnName = String.Empty
            mstrAllocationTranIDs = String.Empty
            mstrAllocationTranNAMEs = String.Empty
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid
            objConfig._Companyunkid = xCompanyUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DisplayChart(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, True)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "generateReport", mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub FilterTitleAndFilterQuery()

        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCurrentPeriodUnkId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Calendar : ") & " " & mstrCurrentPeriodName & " "
            End If

            If mintPreviousPeriodUnkId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Calendar : ") & " " & mstrPreviousPeriodName & " "
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Budget Allocation : ") & " " & mstrBudgetAllocationName & " "

            If mstrAllocationTranIDs.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Allocation : ") & " " & mstrAllocationTranNAMEs & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        'Dim dtCol As DataColumn
        'Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable
        Dim dsCombo As DataSet = Nothing
        Dim dsList As New DataSet

        Try
            objDataOperation = New clsDataOperation

            dsList = GetQueryData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)

            
            mdtTableExcel = dsList.Tables(0).Copy

            Dim mintColIdx As Integer = -1

            mintColIdx += 1
            mdtTableExcel.Columns("allocationtrancode").SetOrdinal(mintColIdx)
            mdtTableExcel.Columns(mintColIdx).Caption = mstrBudgetAllocationName & " " & Language.getMessage(mstrModuleName, 5, "Code")

            mintColIdx += 1
            mdtTableExcel.Columns("allocationtranname").SetOrdinal(mintColIdx)
            mdtTableExcel.Columns(mintColIdx).Caption = mstrBudgetAllocationName & " " & Language.getMessage(mstrModuleName, 6, "Name")

            mintColIdx += 1
            mdtTableExcel.Columns("Currmaxbudget_amt").SetOrdinal(mintColIdx)
            mdtTableExcel.Columns(mintColIdx).Caption = Language.getMessage(mstrModuleName, 7, "Max Budget Set")

            If mintTrainingRemainingBalanceBasedOnID = enTrainingRemainingBalanceBasedOn.Approved_Amount Then
                mintColIdx += 1
                mdtTableExcel.Columns("CurrentApprovedAmt").SetOrdinal(mintColIdx)
                mdtTableExcel.Columns(mintColIdx).Caption = Language.getMessage(mstrModuleName, 8, "Consumed Budget")
            Else
                mintColIdx += 1
                mdtTableExcel.Columns("CurrentEnrolledAmt").SetOrdinal(mintColIdx)
                mdtTableExcel.Columns(mintColIdx).Caption = Language.getMessage(mstrModuleName, 9, "Consumed Budget")
            End If

            mintColIdx += 1
            mdtTableExcel.Columns("CurrRemainingBalance").SetOrdinal(mintColIdx)
            mdtTableExcel.Columns(mintColIdx).Caption = Language.getMessage(mstrModuleName, 10, "Remaining Balance")

            For i = mintColIdx + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(mintColIdx + 1)
            Next

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 150
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader, rowsArrayFooter)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
            'objTItem = Nothing
        End Try

    End Sub

    Public Function Generate_DisplayChart(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim dtCol As DataColumn
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            dsList = GetQueryData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)

            Dim rpt_Row As DataRow = Nothing

            For Each dRow As DataRow In dsList.Tables(0).Rows
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dRow.Item("trainingcoursename")
                rpt_Row.Item("Column81") = dRow.Item("totalapprovedbudget")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTrainingCostAnalysis

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription" _
                                        )

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 11, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 12, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 13, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 14, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 15, "Training Request Budget Report"))
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DisplayChart; Module Name: " & mstrModuleName)
        End Try

    End Function

    Public Function GetQueryData(ByVal xDatabaseName As String, _
                                 ByVal xUserUnkid As Integer, _
                                 ByVal xYearUnkid As Integer, _
                                 ByVal xCompanyUnkid As Integer, _
                                 ByVal xPeriodStart As Date, _
                                 ByVal xPeriodEnd As Date, _
                                 ByVal xUserModeSetting As String, _
                                 ByVal xOnlyApproved As Boolean, _
                                 ByVal xIncludeIn_ActiveEmployee As Boolean _
                                 ) As DataSet

        Dim dsList As New DataSet
        Dim dsCostList As New DataSet
        Dim dsCombo As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim objDTraining As New clsDepartmentaltrainingneed_master
        Dim dtCol As DataColumn
        Try

            Dim intAllocationTranID As Integer = 0
            Integer.TryParse(mstrAllocationTranIDs, intAllocationTranID)

            Dim strAllocationName As String = ""
            Dim dtTable As DataTable = objDTraining.GetBudgetSummaryData(mintCurrentPeriodUnkId, mintPreviousPeriodUnkId, mintTrainingNeedAllocationID, mintBudgetAllocationID, mintTrainingCostCenterAllocationID, mintTrainingRemainingBalanceBasedOnID, strAllocationName, intAllocationTranID, True)
            mstrBudgetAllocationName = strAllocationName

            dtCol = New DataColumn("AllocationName", Type.GetType("System.String"))
            dtCol.DefaultValue = strAllocationName
            dtCol.AllowDBNull = False
            dtTable.Columns.Add(dtCol)

            dsList = New DataSet()
            dsList.Tables.Add(dtTable.Copy)

            objDataOperation = New clsDataOperation



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetQueryData; Module Name: " & mstrModuleName)
        Finally
            objDTraining = Nothing
        End Try
        Return dsList
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Calendar :")
			Language.setMessage(mstrModuleName, 2, "Budget Allocation :")
			Language.setMessage(mstrModuleName, 3, "Allocation :")
			Language.setMessage(mstrModuleName, 4, "Order By :")
			Language.setMessage(mstrModuleName, 5, "Code")
			Language.setMessage(mstrModuleName, 6, "Name")
			Language.setMessage(mstrModuleName, 7, "Max Budget Set")
			Language.setMessage(mstrModuleName, 8, "Consumed Budget")
			Language.setMessage(mstrModuleName, 9, "Consumed Budget")
			Language.setMessage(mstrModuleName, 10, "Remaining Balance")
			Language.setMessage(mstrModuleName, 11, "Prepared By :")
			Language.setMessage(mstrModuleName, 12, "Checked By :")
			Language.setMessage(mstrModuleName, 13, "Approved By :")
			Language.setMessage(mstrModuleName, 14, "Received By :")
			Language.setMessage(mstrModuleName, 15, "Training Request Budget Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
