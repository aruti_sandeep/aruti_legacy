﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmTrainingFeedbackFormReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingFeedbackFormReport"
    Private objTrainingFeedbackFormReport As clsTrainingFeedbackFormReport
    Private dvEmployee As DataView
    Private mstrSearchEmpText As String = ""
    Private mlstEmployeeIds As List(Of String)
#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
        objTrainingFeedbackFormReport = New clsTrainingFeedbackFormReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTrainingFeedbackFormReport.SetDefaultValue()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objMasterData As New clsMasterData
        Dim objCommon As New clsCommon_Master
        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objMasterData.GetTrainingEvaluationFeedbackModeList(True, "List")
            With cboEvaluationCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCalendar = Nothing
            objMasterData = Nothing
            objCommon = Nothing

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            cboEvaluationCategory.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0

            objTrainingFeedbackFormReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Training Calendar."), enMsgBoxStyle.Information)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            If CInt(cboEvaluationCategory.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Evaluation Category."), enMsgBoxStyle.Information)
                cboEvaluationCategory.Focus()
                Exit Function
            End If

            If CInt(cboTrainingName.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Training Name."), enMsgBoxStyle.Information)
                cboTrainingName.Focus()
                Exit Function
            End If

            mlstEmployeeIds = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList

            objTrainingFeedbackFormReport.SetDefaultValue()

            objTrainingFeedbackFormReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingFeedbackFormReport._CalendarName = cboTrainingCalendar.Text
            objTrainingFeedbackFormReport._FeedbackModeid = CInt(cboEvaluationCategory.SelectedValue)
            objTrainingFeedbackFormReport._FeedbackModeName = cboEvaluationCategory.Text
            objTrainingFeedbackFormReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingFeedbackFormReport._TrainingName = cboTrainingName.Text
            objTrainingFeedbackFormReport._EmployeeIds = mlstEmployeeIds

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-995 - On training reports, system is fetching the wrong usernames on printed by section
            Dim objUser As New clsUserAddEdit
            Dim strUserName As String = String.Empty
            objUser._Userunkid = User._Object._Userunkid
            strUserName = objUser._Firstname & " " & objUser._Lastname
            If strUserName.Trim.Length <= 0 Then strUserName = objUser._Username
            objTrainingFeedbackFormReport._User = strUserName
            objUser = Nothing
            'Hemant (27 Oct 2022) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = Language.getMessage(mstrModuleName, 4, "Search Employees")
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpText", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try

            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

            Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmTrainingFeedbackFormReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingFeedbackFormReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingFeedbackFormReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingFeedbackFormReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()

            Me._Title = objTrainingFeedbackFormReport._ReportName
            Me._Message = objTrainingFeedbackFormReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingFeedbackFormReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmTrainingFeedbackFormReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            
            objTrainingFeedbackFormReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          0, CType(e.Type, enPrintAction), enExportAction.None)
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingFeedbackFormReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingFeedbackFormReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            
            objTrainingFeedbackFormReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          0, enPrintAction.None, CType(e.Type, enExportAction))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingFeedbackFormReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingFeedbackFormReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingFeedbackFormReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingFeedbackFormReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingFeedbackFormReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboTrainingName.DataSource, DataTable)
            frm.ValueMember = cboTrainingName.ValueMember
            frm.DisplayMember = cboTrainingName.DisplayMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboTrainingName.SelectedValue = frm.SelectedValue
                cboTrainingName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmTrainingFeedbackFormReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingFeedbackFormReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingFeedbackFormReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmTrainingFeedbackFormReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Events"
    Private Sub cboTrainingName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrainingCalendar.SelectedIndexChanged, _
                                                                                                                  cboTrainingName.SelectedIndexChanged
        Dim objEmp As New clsEmployee_Master
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsCombo As New DataSet
        Dim dsTrainingRequest As New DataSet
        Dim strFilter As String = String.Empty
        Try
            If CInt(cboTrainingCalendar.SelectedValue) > 0 AndAlso CInt(cboTrainingName.SelectedValue) > 0 Then
                RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
                Call SetDefaultSearchEmpText()
                AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged

                dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                        User._Object._Userunkid, _
                                        FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        ConfigParameter._Object._UserAccessModeSetting, _
                                       True, True, "Emp", True)

                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-995 - On training reports, system is fetching the wrong usernames on printed by section
                'strFilter = "periodunkid = " & CInt(cboTrainingCalendar.SelectedValue) & " AND coursemasterunkid = " & CInt(cboTrainingName.SelectedValue) & ""
                strFilter = "trtraining_request_master.periodunkid = " & CInt(cboTrainingCalendar.SelectedValue) & " AND trtraining_request_master.coursemasterunkid = " & CInt(cboTrainingName.SelectedValue) & ""
                'Hemant (27 Oct 2022) -- End
                dsTrainingRequest = objTrainingRequest.GetList(FinancialYear._Object._DatabaseName, _
                                                               User._Object._Userunkid, _
                                                               FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, True, "List", ConfigParameter._Object._TrainingNeedAllocationID, enTrainingRequestStatus.APPROVED, _
                                                                strFilter _
                                                               )
                'Hemant (07 Mar 2022) -- [ConfigParameter._Object._TrainingNeedAllocationID]
                Dim strempids As String = String.Join(",", dsTrainingRequest.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
                Dim dtTable As DataTable

                    If strempids.Trim.Length > 0 Then
                        dtTable = New DataView(dsCombo.Tables(0), "employeeunkid IN(" & strempids & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                    dtTable = dsCombo.Tables(0).Clone()
                End If


                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dTable
                '    .SelectedValue = 0
                'End With

                If dtTable IsNot Nothing Then
                If dtTable.Columns.Contains("IsChecked") = False Then
                    Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                    dtCol.DefaultValue = False
                    dtCol.AllowDBNull = False
                    dtTable.Columns.Add(dtCol)
                End If

                dvEmployee = dtTable.DefaultView
                dgEmployee.AutoGenerateColumns = False
                objdgcolhCheck.DataPropertyName = "IsChecked"
                objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
                dgColhEmpCode.DataPropertyName = "employeecode"
                dgColhEmployee.DataPropertyName = "employeename"

                dgEmployee.DataSource = dvEmployee
                dvEmployee.Sort = "IsChecked DESC, employeename "
                Else
                    dgEmployee.DataSource = Nothing
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrainingName_SelectedIndexChanged", mstrModuleName)
        Finally
            dsCombo.Dispose() : objEmp = Nothing
            objTrainingRequest = Nothing
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Textbox Events"
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEvaluationCategory.Text = Language._Object.getCaption(Me.lblEvaluationCategory.Name, Me.lblEvaluationCategory.Text)
			Me.lblTrainingName.Text = Language._Object.getCaption(Me.lblTrainingName.Name, Me.lblTrainingName.Text)
			Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.Name, Me.lblTrainingCalendar.Text)
			Me.lblEmployeeList.Text = Language._Object.getCaption(Me.lblEmployeeList.Name, Me.lblEmployeeList.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
      
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Training Calendar.")
			Language.setMessage(mstrModuleName, 2, "Please Select Evaluation Category.")
			Language.setMessage(mstrModuleName, 3, "Please Select Training Name.")
			Language.setMessage(mstrModuleName, 4, "Search Employees")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class