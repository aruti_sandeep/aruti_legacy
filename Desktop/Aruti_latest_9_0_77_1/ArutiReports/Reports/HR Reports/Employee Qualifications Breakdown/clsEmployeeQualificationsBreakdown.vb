﻿'Class Name : clsEmployeeQualificationsBreakdown.vb
'Purpose    :
'Date       : 26-Dec-2022
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsEmployeeQualificationsBreakdown
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeQualificationsBreakdown"
    Private mstrReportId As String = enArutiReport.Employee_Qualification_Breakdown
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintQualificationGrpId As Integer = -1
    Private mstrQualificationGrpName As String = String.Empty
    Private mintQualificationId As Integer = -1
    Private mstrQualificationName As String = String.Empty
    Private mintInstituteId As Integer = -1
    Private mstrInstituteName As String = String.Empty

    Private mblnIsAcitve As Boolean = True
    Private mblnIsAppointmentDate As Boolean = True

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _QualificationGrpId() As Integer
        Set(ByVal value As Integer)
            mintQualificationGrpId = value
        End Set
    End Property

    Public WriteOnly Property _QualificationGrpName() As String
        Set(ByVal value As String)
            mstrQualificationGrpName = value
        End Set
    End Property

    Public WriteOnly Property _QualificationId() As Integer
        Set(ByVal value As Integer)
            mintQualificationId = value
        End Set
    End Property

    Public WriteOnly Property _QualificationName() As String
        Set(ByVal value As String)
            mstrQualificationName = value
        End Set
    End Property

    Public WriteOnly Property _InstituteId() As Integer
        Set(ByVal value As Integer)
            mintInstituteId = value
        End Set
    End Property

    Public WriteOnly Property _InstituteName() As String
        Set(ByVal value As String)
            mstrInstituteName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsAcitve = value
        End Set
    End Property

    Public WriteOnly Property _IsAppointmentDate() As Boolean
        Set(ByVal value As Boolean)
            mblnIsAppointmentDate = value
        End Set
    End Property

    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintQualificationGrpId = 0
            mstrQualificationGrpName = ""
            mintQualificationId = 0
            mstrQualificationName = ""
            mintInstituteId = 0
            mstrInstituteName = ""
            mblnIsAcitve = True
            mblnIsAppointmentDate = False
            mblnIncludeAccessFilterQry = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mblnIsAcitve = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintQualificationGrpId > 0 Then
                objDataOperation.AddParameter("@QualificationGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGrpId)
                Me._FilterQuery &= " AND QGrpId = @QualificationGrpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Qualification Group :") & "" & mstrQualificationGrpName & " "
            End If

            If mintQualificationId > 0 Then
                objDataOperation.AddParameter("@QualificationId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationId)
                Me._FilterQuery &= " AND QuaId = @QualificationId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Qualification :") & " " & mstrQualificationName & " "
            End If

            If mintInstituteId > 0 Then
                objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteId)
                Me._FilterQuery &= " AND InstId= @InstituteId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Place Of Award :") & " " & mstrInstituteName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, " Order By : ") & " " & Me.OrderByDisplay
                'If mintViewIndex > 0 Then
                '    Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
                'Else
                '    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                'End If
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "employeecode"
            OrderByQuery = "employeecode"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                          ByVal xExportReportPath As String, _
                                          ByVal xOpenReportAfterExport As Boolean, _
                                          Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                          Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                          Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                          )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Qualification")

            dtCol = New DataColumn("SrNo", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "SN")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Name of Staff")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Gender", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Gender")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Nationality", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Nationality")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("BirthDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Date of Birth")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            If mblnIsAppointmentDate = True Then
                dtCol = New DataColumn("AppointmentDate", System.Type.GetType("System.String"))
                dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Appointment Date")
                dtCol.DefaultValue = ""
                dtFinalTable.Columns.Add(dtCol)
            End If

            dtCol = New DataColumn("JobGroup", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Job Group")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Job", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Position")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("QualificationGroup", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Qualification Type")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Qualification", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 11, "Qualification")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Duration", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 12, "Duration (YY.MM)")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("GPA", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 13, "GPA")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Institute", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 14, "Conferring Institution")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Year", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 15, "Year obtained")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmploymentStatus", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 16, "Employment Status")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Remarks", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 17, "Remarks")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            StrQ &= "SELECT " & _
                   "    ISNULL(employeeunkid,0) AS employeeunkid " & _
                   ",   ISNULL(employeecode,'') AS employeecode " & _
                   ",   ISNULL(EmpName,'') AS EmpName " & _
                   ",   CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Gender " & _
                   ",   ISNULL(Nationality,'')  AS Nationality " & _
                   ",   ISNULL(BirthDate,'') AS BirthDate " & _
                   ",   ISNULL(AppointedDate,'') AS AppointedDate " & _
                   ",   ISNULL(JobGroup,'') AS JobGroup " & _
                   ",   ISNULL(Job,'') AS Job " & _
                   ",   ISNULL(QualificationGrp,'') AS QualificationGrp " & _
                   ",   ISNULL(Qualification,'') AS Qualification " & _
                   ",   ISNULL(StartDate,'') AS StartDate " & _
                   ",   ISNULL(EndDate,'') AS EndDate " & _
                   ",   ISNULL(Duration_Days,0) AS Duration_Days " & _
                   ",   ISNULL(gpacode,'0.00') AS gpacode " & _
                   ",   ISNULL(POA,'') AS POA " & _
                   ",   ISNULL(EmploymentType,'') AS EmploymentType " & _
                   ",   ISNULL(QDesc,'') AS QDesc " & _
                   ",   GName AS GName " & _
                   ",   Id AS Id " & _
                   ",   qlevel " & _
                   " FROM " & _
                   "( " & _
                   "   SELECT " & _
                   "    hremployee_master.employeeunkid AS employeeunkid " & _
                   ",   ISNULL(hremployee_master.employeecode,'') as employeecode "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ",	 ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            Else
                StrQ &= ",	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            End If



            StrQ &= "  ,ISNULL(hremployee_master.gender, 0) AS gender  " & _
                    "  ,ISNULL(nct.country_name,'') AS Nationality " & _
                    "  ,ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
                    "  ,ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS AppointedDate " & _
                    "  ,ISNULL(hrjobgroup_master.name,'') AS JobGroup " & _
                    "  ,ISNULL(hrjob_master.job_name,'') AS Job " & _
                    "  ,CASE WHEN ISNULL(cfcommon_master.name,'') <>'' THEN ISNULL(cfcommon_master.name,'') ELSE ISNULL(other_qualificationgrp,'') END AS QualificationGrp " & _
                    "  ,CASE WHEN ISNULL(hrqualification_master.qualificationname,'') <>'' THEN ISNULL(hrqualification_master.qualificationname,'') ELSE ISNULL(other_qualification,'') END AS Qualification " & _
                    "  ,ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112), ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112), '')) AS StartDate " & _
                    "  ,ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112), ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112), '')) AS EndDate " & _
                    "  ,DATEDIFF(DAY,ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112), ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112), '')), ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112), ISNULL(CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112), '')) ) + 1 AS Duration_Days " & _
                    "  ,hremp_qualification_tran.gpacode as gpacode  " & _
                    "  ,CASE WHEN ISNULL(hrqualification_master.qualificationname,'') <>'' THEN ISNULL(hrinstitute_master.institute_name,'') ELSE ISNULL(other_institute,'') END AS POA " & _
                    "  ,ISNULL(EmploymentType.name,'') AS EmploymentType " & _
                    "  ,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
                    "  ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
                    "  ,ISNULL(hrqualification_master.qualificationunkid,0) AS QuaId  " & _
                    "  ,ISNULL(hrinstitute_master.instituteunkid,0) AS InstId " & _
                    "  ,ISNULL(cfcommon_master.qlevel,0) as qlevel "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            StrQ &= ", 0 AS Id, '' AS GName "

            StrQ &= "FROM hremp_qualification_tran " & _
                    "   JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    "   LEFT JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
                    "   LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                    "   LEFT JOIN hrmsConfiguration..cfcountry_master AS nct ON nct.countryunkid = hremployee_master.nationalityunkid " & _
                    "   LEFT JOIN " & _
                    "           ( " & _
                    "               SELECT " & _
                    "                   jobgroupunkid " & _
                    "                   ,jobunkid " & _
                    "                   ,employeeunkid " & _
                    "                   ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "               FROM hremployee_categorization_tran " & _
                    "               WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "           ) AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 " & _
                    "   LEFT JOIN hrjob_master ON ECT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                    "   LEFT JOIN hrjobgroup_master ON ECT.jobgroupunkid = hrjobgroup_master.jobgroupunkid AND hrjobgroup_master.isactive = 1 " & _
                    "   LEFT JOIN cfcommon_master AS EmploymentType ON EmploymentType.masterunkid = hremployee_master.employmenttypeunkid AND EmploymentType.mastertype = '" & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "' "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            'StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 "

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsAcitve = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            StrQ &= " ) AS A WHERE 1 = 1 "
          
            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intCount As Integer = 0
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            Dim intEmployeeunkId As Integer = 0
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow
                If intEmployeeunkId <> CInt(drRow.Item("employeeunkid")) Then
                    intCount = intCount + 1
                End If
                rpt_Row.Item("SrNo") = intCount
                rpt_Row.Item("EmployeeCode") = drRow.Item("employeecode")
                rpt_Row.Item("EmployeeName") = drRow.Item("EmpName")
                rpt_Row.Item("Gender") = drRow.Item("Gender")
                rpt_Row.Item("Nationality") = drRow.Item("Nationality")
                rpt_Row.Item("BirthDate") = eZeeDate.convertDate(drRow.Item("BirthDate").ToString).ToShortDateString
                If mblnIsAppointmentDate = True Then
                    rpt_Row.Item("AppointmentDate") = eZeeDate.convertDate(drRow.Item("AppointedDate").ToString).ToShortDateString
                End If
                rpt_Row.Item("JobGroup") = drRow.Item("JobGroup")
                rpt_Row.Item("Job") = drRow.Item("Job")
                rpt_Row.Item("QualificationGroup") = drRow.Item("QualificationGrp")
                rpt_Row.Item("Qualification") = drRow.Item("Qualification")
                If CInt(drRow.Item("Duration_Days")) > 0 Then
                    Dim intYears As Integer = CInt(Math.Floor(CInt(drRow.Item("Duration_Days")) / 365))
                    Dim intMonths As Integer = CInt(Math.Floor(CInt((CInt(drRow.Item("Duration_Days")) - (intYears * 365)) / 30)))
                    rpt_Row.Item("Duration") = intYears & "." & IIf(intMonths.ToString.Length < 2, "0" & intMonths, intMonths)
                Else
                    rpt_Row.Item("Duration") = "0.00"
                End If

                rpt_Row.Item("GPA") = drRow.Item("gpacode")
                rpt_Row.Item("Institute") = drRow.Item("POA")
                If drRow.Item("EndDate").ToString.Trim.Length > 0 Then
                    rpt_Row.Item("Year") = eZeeDate.convertDate(drRow.Item("EndDate")).Year
                Else
                    rpt_Row.Item("Year") = ""
                End If
                rpt_Row.Item("EmploymentStatus") = drRow.Item("EmploymentType")
                rpt_Row.Item("Remarks") = drRow.Item("QDesc")

                dtFinalTable.Rows.Add(rpt_Row)

                intEmployeeunkId = CInt(drRow.Item("employeeunkid"))
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            'Dim strarrGroupColumns As String() = {"SrNo", "employeecode", "EmployeeName", "Gender", "Nationality", "BirthDate", "JobGroup", "Job", "EmploymentStatus"}
            Dim strarrGroupColumns As String() = {"SrNo", "employeecode", "EmployeeName"}

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            'Dim intArrayColumnWidth As Integer() = {100, 100, 200, 100, 100, 100, 100, 100, 150, 150, 100, 100, 150, 100, 100, 100}
            Dim intArrayColumnWidth As Integer()
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 2 Then
                    intArrayColumnWidth(i) = 200
                ElseIf (i = 8 OrElse i = 12) AndAlso mblnIsAppointmentDate = False Then
                    intArrayColumnWidth(i) = 150
                ElseIf i = 9 Then
                    intArrayColumnWidth(i) = 150
                ElseIf (i = 10 OrElse i = 13) AndAlso mblnIsAppointmentDate = True Then
                    intArrayColumnWidth(i) = 150
                Else
                    intArrayColumnWidth(i) = 100
                End If
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader, rowsArrayFooter, Nothing, , False, True)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "SN")
			Language.setMessage(mstrModuleName, 2, "Employee Code")
			Language.setMessage(mstrModuleName, 3, "Name of Staff")
			Language.setMessage(mstrModuleName, 4, "Gender")
			Language.setMessage(mstrModuleName, 5, "Nationality")
			Language.setMessage(mstrModuleName, 6, "Date of Birth")
			Language.setMessage(mstrModuleName, 7, "Appointment Date")
			Language.setMessage(mstrModuleName, 8, "Job Group")
			Language.setMessage(mstrModuleName, 9, "Position")
			Language.setMessage(mstrModuleName, 10, "Qualification Type")
			Language.setMessage(mstrModuleName, 11, "Qualification")
			Language.setMessage(mstrModuleName, 12, "Duration (YY.MM)")
			Language.setMessage(mstrModuleName, 13, "GPA")
			Language.setMessage(mstrModuleName, 14, "Conferring Institution")
			Language.setMessage(mstrModuleName, 15, "Year obtained")
			Language.setMessage(mstrModuleName, 16, "Employment Status")
			Language.setMessage(mstrModuleName, 17, "Remarks")
			Language.setMessage(mstrModuleName, 19, "Employee :")
			Language.setMessage(mstrModuleName, 20, "Qualification Group :")
			Language.setMessage(mstrModuleName, 21, "Qualification :")
			Language.setMessage(mstrModuleName, 22, "Place Of Award :")
			Language.setMessage(mstrModuleName, 23, " Order By :")
			Language.setMessage(mstrModuleName, 24, "Prepared By :")
			Language.setMessage(mstrModuleName, 25, "Checked By :")
			Language.setMessage(mstrModuleName, 26, "Approved By")
			Language.setMessage(mstrModuleName, 27, "Received By :")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
