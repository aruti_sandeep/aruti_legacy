﻿'************************************************************************************************************************************
'Class Name : clsProgressUpdateNotAttemptedReport.vb
'Purpose    :
'Date       :28-Nov-2023
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>
''' 
Public Class clsProgressUpdateNotAttemptedReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsProgressUpdateNotAttemptedReport"
    Private mstrReportId As String = enArutiReport.ProgressUpdate_NotAttempted_Report '316
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    Private mintCompanyUnkid As Integer = 0
    Private mdtEmployeeAsOnDate As DateTime
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = String.Empty
#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mintPeriodId = -1
            mstrPeriodName = String.Empty
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintCompanyUnkid = 0
            mdtEmployeeAsOnDate = Nothing
            Rpt = Nothing
            mstrAdvanceFilter = String.Empty
            mblnFirstNamethenSurname = True
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub ProgressUpdatedNotAttemptedtReport(ByVal strDatabaseName As String, _
                                              ByVal intUserUnkid As Integer, _
                                              ByVal intYearUnkid As Integer, _
                                              ByVal intCompanyUnkid As Integer, _
                                              ByVal dtPeriodStart As Date, _
                                              ByVal dtPeriodEnd As Date, _
                                              ByVal strUserModeSetting As String, _
                                              ByVal blnOnlyApproved As Boolean, _
                                              ByVal strExportPath As String, _
                                              ByVal blnOpenAfterExport As Boolean)

        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try

            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "hremployee_master")
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "hremployee_master")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "hremployee_master")


            StrQ = " SELECT " & _
                      " ISNULL(hremployee_master.employeecode,'') AS EmployeeCode " & _
                      ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')  AS Employee " & _
                      ",ISNULL(hrjob_master.job_name,'') AS Job " & _
                      ",ISNULL(dept.name,'') AS Department " & _
                      ",@NotUpdate AS Status "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         stationunkid " & _
                      "        ,deptgroupunkid " & _
                      "        ,departmentunkid " & _
                      "        ,sectiongroupunkid " & _
                      "        ,sectionunkid " & _
                      "        ,unitgroupunkid " & _
                      "        ,unitunkid " & _
                      "        ,teamunkid " & _
                      "        ,classgroupunkid " & _
                      "        ,classunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_transfer_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @ToDate " & _
                      ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " LEFT JOIN hrdepartment_master dept ON dept.departmentunkid = Alloc.departmentunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         jobunkid " & _
                      "        ,jobgroupunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @ToDate " & _
                      ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         cctranheadvalueid AS costcenterunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_cctranhead_tran " & _
                      "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                      "    AND CONVERT(CHAR(8),effectivedate,112) <= @ToDate " & _
                      " ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "   SELECT " & _
                      "        pt.gradegroupunkid " & _
                      "       ,pt.gradeunkid " & _
                      "       ,pt.gradelevelunkid " & _
                      "       ,pt.employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY pt.employeeunkid ORDER BY pt.incrementdate DESC, pt.salaryincrementtranunkid DESC) AS rno " & _
                      "   FROM prsalaryincrement_tran pt " & _
                      "   WHERE pt.isvoid = 0 AND pt.isapproved = 1 AND CONVERT(CHAR(8),pt.incrementdate,112) <= @ToDate " & _
                      " ) AS grds ON hremployee_master.employeeunkid = grds.employeeunkid AND grds.rno = 1 "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= mstrAnalysis_Join & " "

            StrQ &= "  WHERE hremployee_master.employeeunkid NOT IN " & _
                          " ( " & _
                          "     SELECT DISTINCT " & _
                          "         employeeunkid " & _
                          "     FROM hrassess_empupdate_tran " & _
                          "     WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),updatedate,112) BETWEEN @FromDate AND @ToDate " & _
                          " ) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeid "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mintViewIndex > 0 Then
                StrQ &= " ORDER BY " & mstrAnalysis_OrderBy_GName & ", employeecode"
            Else
                StrQ &= " ORDER BY employeecode "
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@FromDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromDate)
            objDataOperation.AddParameter("@ToDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtToDate)
            objDataOperation.AddParameter("@NotUpdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Not Updated"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim mdtTableExcel As DataTable = dsList.Tables(0).Copy()

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)


            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 90
                    Case 1
                        intArrayColumnWidth(i) = 140
                    Case 2
                        intArrayColumnWidth(i) = 160
                    Case Else
                        intArrayColumnWidth(i) = 200
                End Select
            Next

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns.Remove("GName")
            End If


            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "From Date:") & " " & mdtFromDate & Space(10) & Language.getMessage(mstrModuleName, 2, "To Date:") & " " & mdtToDate, "s10bw")
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            If mintEmployeeId > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "Employee:") & " " & mstrEmployeeName, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)
            End If

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = intUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            mdtTableExcel.Columns("EmployeeCode").Caption = Language.getMessage(mstrModuleName, 4, "Employee Code")
            mdtTableExcel.Columns("Status").Caption = Language.getMessage(mstrModuleName, 5, "Employee")
            mdtTableExcel.Columns("Status").Caption = Language.getMessage(mstrModuleName, 6, "Job")
            mdtTableExcel.Columns("Status").Caption = Language.getMessage(mstrModuleName, 7, "Department")
            mdtTableExcel.Columns("Status").Caption = Language.getMessage(mstrModuleName, 8, "Progress Update Status")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, "", False, True)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ProgressUpdatedNotAttemptedtReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Date:")
            Language.setMessage(mstrModuleName, 2, "To Date:")
            Language.setMessage(mstrModuleName, 3, "Employee:")
            Language.setMessage(mstrModuleName, 4, "Employee Code")
            Language.setMessage(mstrModuleName, 5, "Employee")
            Language.setMessage(mstrModuleName, 6, "Job")
            Language.setMessage(mstrModuleName, 7, "Department")
            Language.setMessage(mstrModuleName, 8, "Progress Update Status")
            Language.setMessage(mstrModuleName, 9, "Not Updated")
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
            Language.setMessage(mstrModuleName, 11, "Checked By :")
            Language.setMessage(mstrModuleName, 12, "Approved By")
            Language.setMessage(mstrModuleName, 13, "Received By :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
