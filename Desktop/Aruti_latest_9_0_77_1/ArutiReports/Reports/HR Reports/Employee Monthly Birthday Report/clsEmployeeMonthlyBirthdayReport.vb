﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class clsEmployeeMonthlyBirthdayReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeMonthlyBirthdayReport"
    Private mstrReportId As String = enArutiReport.Employee_Monthly_Birthday_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintEmployeeUnkId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mdtDateFrom As Date = Nothing
    Private mdtDateTo As Date = Nothing
    Private mstrAdvance_Filter As String = String.Empty
    Private mstrExportedFileName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _DateFrom() As Date
        Set(ByVal value As Date)
            mdtDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _DateTo() As Date
        Set(ByVal value As Date)
            mdtDateTo = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public ReadOnly Property _ExportedFileName() As String
        Get
            Return mstrExportedFileName
        End Get
    End Property

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkId = 0
            mstrEmployeeName = String.Empty
            mdtDateFrom = Nothing
            mdtDateTo = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mdtDateFrom = Nothing AndAlso mdtDateTo = Nothing Then
                Me._FilterQuery &= " AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) >= '0101' AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) <= '1231' "
            ElseIf mdtDateFrom <> Nothing AndAlso mdtDateTo = Nothing Then
                objDataOperation.AddParameter("@DateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtDateFrom.ToString("MMdd"))
                Me._FilterQuery &= " AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) >= @DateFrom AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) <= '1231' "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Date From :") & " " & mdtDateFrom.Date & " "
            ElseIf mdtDateFrom = Nothing AndAlso mdtDateTo <> Nothing Then
                objDataOperation.AddParameter("@DateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtDateTo.ToString("MMdd"))
                Me._FilterQuery &= " AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) >= '0101' AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) <= @DateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Date To") & " " & mdtDateTo.Date & " "
            ElseIf mdtDateFrom <> Nothing AndAlso mdtDateTo <> Nothing Then
                If mdtDateFrom.Date.Year <> mdtDateTo.Date.Year Then
                    Me._FilterQuery &= " AND ((SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) >= @DateFrom AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) <= '1231' ) " & _
                                       "     OR (SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) >= '0101' AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) <= @DateTo )) "
                Else
                    Me._FilterQuery &= " AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) >= @DateFrom AND SUBSTRING(CONVERT(CHAR(8),hremployee_master.birthdate,112),5,8) <= @DateTo "
                End If
                objDataOperation.AddParameter("@DateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtDateFrom.ToString("MMdd"))
                objDataOperation.AddParameter("@DateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtDateTo.ToString("MMdd"))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Date From :") & " " & mdtDateFrom.Date & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Date To") & " " & mdtDateTo.Date & " "
            End If
           
            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                         ByVal xUserUnkid As Integer, _
                                         ByVal xYearUnkid As Integer, _
                                         ByVal xCompanyUnkid As Integer, _
                                         ByVal xPeriodStart As Date, _
                                         ByVal xPeriodEnd As Date, _
                                         ByVal xUserModeSetting As String, _
                                         ByVal xOnlyApproved As Boolean, _
                                         ByVal xExportReportPath As String, _
                                         ByVal xOpenReportAfterExport As Boolean, _
                                         ByVal pintReportType As Integer, _
                                         Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                         Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                         Optional ByVal intBaseCurrencyUnkid As Integer = 0)


    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'OrderByDisplay = "startdate"
            'OrderByQuery = "trdepartmentaltrainingneed_master.startdate"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                          ByVal xExportReportPath As String, _
                                          ByVal xOpenReportAfterExport As Boolean, _
                                          ByVal intBaseCurrencyId As Integer, _
                                          Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                          Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                          )
        Dim StrQ As String = String.Empty
        Dim dtCol As DataColumn
        Dim dsList As New DataSet
        Dim mdtTableExcel As New DataTable
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            Dim dtFinalTable As DataTable
            dtFinalTable = New DataTable("Birthday")

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("DOB", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Date of Birth")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Age", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Age")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If mstrAdvance_Filter.Trim.Length > 0 Then
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            End If

            StrQ &= " SELECT " & _
                    "       employeecode " & _
                    ",      firstname + ' ' + othername + ' ' + surname AS 'EmployeeName' " & _
                    ",      birthdate " & _
                    ",      DATEDIFF(YEAR,birthdate,GETDATE()) Age " & _
                    " FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If


            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("EmployeeCode") = drRow.Item("EmployeeCode")
                rpt_Row.Item("EmployeeName") = drRow.Item("EmployeeName")
                If Not IsDBNull(drRow.Item("birthdate")) Then
                    rpt_Row.Item("DOB") = CDate(drRow.Item("birthdate")).ToShortDateString
                End If
                rpt_Row.Item("Age") = drRow.Item("Age")

                dtFinalTable.Rows.Add(rpt_Row)

            Next

            If dtFinalTable IsNot Nothing AndAlso dtFinalTable.Rows.Count <= 0 Then
                Dim dRow As DataRow = dtFinalTable.NewRow
                dRow.Item("EmployeeCode") = ""
                dRow.Item("EmployeeName") = ""
                dRow.Item("DOB") = ""
                dRow.Item("Age") = ""
                dtFinalTable.Rows.Add(dRow)
            End If

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Using package As New OfficeOpenXml.ExcelPackage()
                Dim worksheet As OfficeOpenXml.ExcelWorksheet = package.Workbook.Worksheets.Add("Data")
                Dim xTable As DataTable = mdtTableExcel.Copy
                For Each iCol As DataColumn In xTable.Columns
                    If iCol.Ordinal <> xTable.Columns.Count - 1 Then
                        xTable.Columns(iCol.Ordinal).ColumnName = iCol.Caption.Trim()
                    End If
                Next

                worksheet.Cells("A1").LoadFromDataTable(xTable, True)
                worksheet.Cells("A1:XFD").AutoFilter = False
                worksheet.Cells(2, 1, xTable.Rows.Count + 1, xTable.Columns.Count - 2).Style.Numberformat.Format = "@"
                worksheet.Cells(2, xTable.Columns.Count - 1, xTable.Rows.Count + 1, xTable.Columns.Count - 1).Style.Numberformat.Format = "0"
                worksheet.Cells(2, xTable.Columns.Count, xTable.Rows.Count + 1, xTable.Columns.Count).Style.Numberformat.Format = "0"

                worksheet.Cells.AutoFitColumns(0)

                If Me._ReportName.Contains("/") = True Then
                    Me._ReportName = Me._ReportName.Replace("/", "_")
                End If

                If Me._ReportName.Contains("\") Then
                    Me._ReportName = Me._ReportName.Replace("\", "_")
                End If

                If Me._ReportName.Contains(":") Then
                    Me._ReportName = Me._ReportName.Replace(":", "_")
                End If
                Dim strExportFileName As String = ""
                strExportFileName = Me._ReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")

                xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsx"

                Dim fi As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                package.SaveAs(fi)

                mstrExportedFileName = strExportFileName & ".xlsx"

            End Using

            If IO.File.Exists(xExportReportPath) Then
                If xOpenReportAfterExport Then
                    Process.Start(xExportReportPath)
                End If
            End If



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee Code")
			Language.setMessage(mstrModuleName, 2, "Employee Name")
			Language.setMessage(mstrModuleName, 3, "Date of Birth")
			Language.setMessage(mstrModuleName, 4, "Age")
			Language.setMessage(mstrModuleName, 5, "Employee :")
			Language.setMessage(mstrModuleName, 6, "Date To")
			Language.setMessage(mstrModuleName, 7, "Date From :")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
