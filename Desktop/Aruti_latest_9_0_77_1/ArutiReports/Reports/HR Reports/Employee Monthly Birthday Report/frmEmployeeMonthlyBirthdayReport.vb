﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmEmployeeMonthlyBirthdayReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeMonthlyBirthdayReport"
    Private objEmployeeMonthlyBirthdayReport As clsEmployeeMonthlyBirthdayReport
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeMonthlyBirthdayReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeMonthlyBirthdayReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeMonthlyBirthdayReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeMonthlyBirthdayReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeMonthlyBirthdayReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeMonthlyBirthdayReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeMonthlyBirthdayReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeMonthlyBirthdayReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeMonthlyBirthdayReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeMonthlyBirthdayReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeMonthlyBirthdayReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Constructor "
    Public Sub New()

        objEmployeeMonthlyBirthdayReport = New clsEmployeeMonthlyBirthdayReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objEmployeeMonthlyBirthdayReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0

            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpFromDate.Checked = False
            dtpToDate.Checked = False
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If dtpFromDate.Checked = True AndAlso dtpToDate.Checked = True AndAlso dtpFromDate.Value.Date > dtpToDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "To Date should not greater than From date."), enMsgBoxStyle.Information)
                dtpFromDate.Focus()
                Return False
            ElseIf dtpFromDate.Checked = True AndAlso dtpToDate.Checked = True AndAlso DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date) + 1 > 365 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Date Range should not greater than 365 days"), enMsgBoxStyle.Information)
                dtpFromDate.Focus()
                Return False
            End If

            objEmployeeMonthlyBirthdayReport.SetDefaultValue()

            objEmployeeMonthlyBirthdayReport._EmployeeUnkId = CInt(cboEmployee.SelectedValue)
            objEmployeeMonthlyBirthdayReport._EmployeeName = cboEmployee.Text


            If dtpFromDate.Checked = True Then
                objEmployeeMonthlyBirthdayReport._DateFrom = dtpFromDate.Value.Date
            End If
            If dtpToDate.Checked = True Then
                objEmployeeMonthlyBirthdayReport._DateTo = dtpToDate.Value.Date
            End If

            objEmployeeMonthlyBirthdayReport._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Call ResetValue()
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub

            objEmployeeMonthlyBirthdayReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                 User._Object._Userunkid, _
                                                                 FinancialYear._Object._YearUnkid, _
                                                                 Company._Object._Companyunkid, _
                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 True, False, ConfigParameter._Object._ExportReportPath, _
                                                                 ConfigParameter._Object._OpenAfterExport, _
                                                                 0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region


  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "To Date should not greater than From date.")
			Language.setMessage(mstrModuleName, 2, "Date Range should not greater than 365 days")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class