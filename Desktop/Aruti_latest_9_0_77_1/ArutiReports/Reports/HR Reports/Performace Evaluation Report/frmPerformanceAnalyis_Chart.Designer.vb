﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPerformanceAnalyis_Chart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim Series4 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim Title2 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocation = New System.Windows.Forms.ColumnHeader
        Me.chAnalysis_Chart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.objspcChart2 = New System.Windows.Forms.SplitContainer
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.chHpoCurve = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.pnlMain.SuspendLayout()
        CType(Me.chAnalysis_Chart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objspcChart2.Panel1.SuspendLayout()
        Me.objspcChart2.Panel2.SuspendLayout()
        Me.objspcChart2.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chHpoCurve, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.lvAllocation)
        Me.pnlMain.Controls.Add(Me.chAnalysis_Chart)
        Me.pnlMain.Location = New System.Drawing.Point(12, 12)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(464, 290)
        Me.pnlMain.TabIndex = 0
        '
        'lvAllocation
        '
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocation})
        Me.lvAllocation.Location = New System.Drawing.Point(2, 2)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(196, 490)
        Me.lvAllocation.TabIndex = 1
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocation
        '
        Me.colhAllocation.Text = ""
        Me.colhAllocation.Width = 192
        '
        'chAnalysis_Chart
        '
        Me.chAnalysis_Chart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chAnalysis_Chart.BackColor = System.Drawing.SystemColors.Control
        Me.chAnalysis_Chart.BorderlineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.chAnalysis_Chart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.chAnalysis_Chart.BorderlineWidth = 4
        Me.chAnalysis_Chart.BorderSkin.PageColor = System.Drawing.Color.Transparent
        ChartArea1.Area3DStyle.WallWidth = 5
        ChartArea1.AxisX.MajorGrid.Enabled = False
        ChartArea1.AxisX2.MajorGrid.Enabled = False
        ChartArea1.AxisY.MajorGrid.Enabled = False
        ChartArea1.AxisY.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None
        ChartArea1.AxisY.ScaleBreakStyle.Enabled = True
        ChartArea1.AxisY.ScaleBreakStyle.MaxNumberOfBreaks = 1
        ChartArea1.AxisY.ScaleBreakStyle.Spacing = 0
        ChartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.[True]
        ChartArea1.AxisY2.MajorGrid.Enabled = False
        ChartArea1.BackColor = System.Drawing.SystemColors.Control
        ChartArea1.Name = "ChartArea1"
        Me.chAnalysis_Chart.ChartAreas.Add(ChartArea1)
        Legend1.Alignment = System.Drawing.StringAlignment.Far
        Legend1.BackColor = System.Drawing.Color.Transparent
        Legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom
        Legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row
        Legend1.Name = "Legend1"
        Legend1.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Tall
        Me.chAnalysis_Chart.Legends.Add(Legend1)
        Me.chAnalysis_Chart.Location = New System.Drawing.Point(200, 2)
        Me.chAnalysis_Chart.Name = "chAnalysis_Chart"
        Series1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.MarkerSize = 1
        Series1.Name = "Series1"
        Series1.ShadowOffset = 5
        Series2.BorderWidth = 3
        Series2.ChartArea = "ChartArea1"
        Series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
        Series2.CustomProperties = "LabelStyle=Top"
        Series2.Legend = "Legend1"
        Series2.MarkerSize = 10
        Series2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond
        Series2.Name = "Series2"
        Series2.ShadowOffset = 5
        Series2.YValuesPerPoint = 2
        Me.chAnalysis_Chart.Series.Add(Series1)
        Me.chAnalysis_Chart.Series.Add(Series2)
        Me.chAnalysis_Chart.Size = New System.Drawing.Size(261, 286)
        Me.chAnalysis_Chart.TabIndex = 0
        Title1.Name = "Title1"
        Me.chAnalysis_Chart.Titles.Add(Title1)
        '
        'objspcChart2
        '
        Me.objspcChart2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspcChart2.IsSplitterFixed = True
        Me.objspcChart2.Location = New System.Drawing.Point(482, 12)
        Me.objspcChart2.Name = "objspcChart2"
        Me.objspcChart2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspcChart2.Panel1
        '
        Me.objspcChart2.Panel1.Controls.Add(Me.dgvData)
        '
        'objspcChart2.Panel2
        '
        Me.objspcChart2.Panel2.Controls.Add(Me.chHpoCurve)
        Me.objspcChart2.Size = New System.Drawing.Size(394, 290)
        Me.objspcChart2.SplitterDistance = 119
        Me.objspcChart2.SplitterWidth = 2
        Me.objspcChart2.TabIndex = 1
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(394, 119)
        Me.dgvData.TabIndex = 0
        '
        'chHpoCurve
        '
        Me.chHpoCurve.BackColor = System.Drawing.SystemColors.Control
        Me.chHpoCurve.BorderlineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.chHpoCurve.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.chHpoCurve.BorderlineWidth = 4
        Me.chHpoCurve.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash
        Me.chHpoCurve.BorderSkin.PageColor = System.Drawing.Color.Transparent
        ChartArea2.Area3DStyle.WallWidth = 5
        ChartArea2.AxisX.MajorGrid.Enabled = False
        ChartArea2.AxisX2.MajorGrid.Enabled = False
        ChartArea2.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.[True]
        ChartArea2.AxisY.LabelStyle.Interval = 10
        ChartArea2.AxisY.LabelStyle.IntervalOffset = 10
        ChartArea2.AxisY.LabelStyle.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number
        ChartArea2.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number
        ChartArea2.AxisY.MajorGrid.Enabled = False
        ChartArea2.AxisY.Maximum = 105
        ChartArea2.AxisY.Minimum = 0
        ChartArea2.AxisY.ScaleBreakStyle.MaxNumberOfBreaks = 1
        ChartArea2.AxisY.ScaleBreakStyle.Spacing = 0
        ChartArea2.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.[False]
        ChartArea2.AxisY2.MajorGrid.Enabled = False
        ChartArea2.BackColor = System.Drawing.SystemColors.Control
        ChartArea2.IsSameFontSizeForAllAxes = True
        ChartArea2.Name = "ChartArea1"
        Me.chHpoCurve.ChartAreas.Add(ChartArea2)
        Me.chHpoCurve.Dock = System.Windows.Forms.DockStyle.Fill
        Legend2.Alignment = System.Drawing.StringAlignment.Far
        Legend2.BackColor = System.Drawing.Color.Transparent
        Legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom
        Legend2.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row
        Legend2.Name = "Legend1"
        Legend2.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Tall
        Me.chHpoCurve.Legends.Add(Legend2)
        Me.chHpoCurve.Location = New System.Drawing.Point(0, 0)
        Me.chHpoCurve.Name = "chHpoCurve"
        Series3.BorderWidth = 3
        Series3.ChartArea = "ChartArea1"
        Series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
        Series3.CustomProperties = "LabelStyle=Top"
        Series3.Legend = "Legend1"
        Series3.MarkerSize = 9
        Series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle
        Series3.Name = "Series1"
        Series4.BorderWidth = 3
        Series4.ChartArea = "ChartArea1"
        Series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
        Series4.CustomProperties = "LabelStyle=Top"
        Series4.Legend = "Legend1"
        Series4.MarkerSize = 9
        Series4.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square
        Series4.Name = "Series2"
        Me.chHpoCurve.Series.Add(Series3)
        Me.chHpoCurve.Series.Add(Series4)
        Me.chHpoCurve.Size = New System.Drawing.Size(394, 169)
        Me.chHpoCurve.TabIndex = 1
        Title2.Name = "Title1"
        Me.chHpoCurve.Titles.Add(Title2)
        '
        'frmPerformanceAnalyis_Chart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 494)
        Me.Controls.Add(Me.objspcChart2)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPerformanceAnalyis_Chart"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Performance Analyis Chart"
        Me.pnlMain.ResumeLayout(False)
        CType(Me.chAnalysis_Chart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objspcChart2.Panel1.ResumeLayout(False)
        Me.objspcChart2.Panel2.ResumeLayout(False)
        Me.objspcChart2.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chHpoCurve, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocation As System.Windows.Forms.ColumnHeader
    Private WithEvents chAnalysis_Chart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents objspcChart2 As System.Windows.Forms.SplitContainer
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Private WithEvents chHpoCurve As System.Windows.Forms.DataVisualization.Charting.Chart
End Class
