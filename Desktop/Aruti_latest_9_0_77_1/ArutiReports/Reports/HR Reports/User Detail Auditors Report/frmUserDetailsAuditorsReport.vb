#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmUserDetailsAuditorsReport


#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmUserDetailsAuditorsReport"
    Private objclsUserDetailAuditorReport As clsUserDetailsAuditorsReport
    Private blnMultiCompnay As Boolean = False
#End Region

#Region " Contructor "

    Public Sub New()
        objclsUserDetailAuditorReport = New clsUserDetailsAuditorsReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objclsUserDetailAuditorReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Page Event"

    Private Sub frmUserDetailsAuditorsReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            eZeeHeader.Title = objclsUserDetailAuditorReport._ReportName
            eZeeHeader.Message = objclsUserDetailAuditorReport._ReportDesc

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserDetailsAuditorsReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUserDetailsAuditorsReport.SetMessages()
            objfrm._Other_ModuleNames = "clsUserDetailsAuditorsReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub ResetValue()
        Try
            dtpFromdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objclsUserDetailAuditorReport.SetDefaultValue()

            If dtpFromdate.Value > dtpToDate.Value Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry,From Date cannot be greater than To Date."), enMsgBoxStyle.Information)
                Return False
            End If


            objclsUserDetailAuditorReport._mdtdate1 = dtpFromdate.Value.Date
            objclsUserDetailAuditorReport._mdtdate2 = dtpToDate.Value.Date
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Button Event"

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
           
            If SetFilter() = False Then Exit Sub

            Call objclsUserDetailAuditorReport.Generate_User_Details()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Control Event"

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "User Log Details")
            Language.setMessage(mstrModuleName, 3, "User Ability Level")
            Language.setMessage(mstrModuleName, 4, "Sorry, Company is mandatory information. Please select Company to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
