﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMonthlyPayrollReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMonthlyPayrollReport))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchMembership = New eZee.Common.eZeeGradientButton
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlOtherFilter = New System.Windows.Forms.Panel
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboExitReason = New System.Windows.Forms.ComboBox
        Me.lblStaffExitReason = New System.Windows.Forms.Label
        Me.objbtnSearchExitReason = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.pnlLeaveTypes = New System.Windows.Forms.Panel
        Me.chkIncludePendingApprovedforms = New System.Windows.Forms.CheckBox
        Me.chkSkipAbsentTnA = New System.Windows.Forms.CheckBox
        Me.lnkSaveLeaveSelection = New System.Windows.Forms.LinkLabel
        Me.objchkLCheckAll = New System.Windows.Forms.CheckBox
        Me.lvLeaveType = New System.Windows.Forms.ListView
        Me.objcolhLCheck = New System.Windows.Forms.ColumnHeader
        Me.colhLName = New System.Windows.Forms.ColumnHeader
        Me.txtLeaveSearch = New System.Windows.Forms.TextBox
        Me.pnlCutomHeads = New System.Windows.Forms.Panel
        Me.chkincludesystemretirement = New System.Windows.Forms.CheckBox
        Me.lnkSaveHeadSelection = New System.Windows.Forms.LinkLabel
        Me.objchkCheckAll = New System.Windows.Forms.CheckBox
        Me.lvCustomTranHead = New System.Windows.Forms.ListView
        Me.objcolhCCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCName = New System.Windows.Forms.ColumnHeader
        Me.colhCHeadTypeID = New System.Windows.Forms.ColumnHeader
        Me.txtSearchCHeads = New System.Windows.Forms.TextBox
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboCatReason = New System.Windows.Forms.ComboBox
        Me.lblCatReason = New System.Windows.Forms.Label
        Me.objbtnSearchCatReason = New eZee.Common.eZeeGradientButton
        Me.EZeeFooter1.SuspendLayout()
        Me.gbMandatoryInfo.SuspendLayout()
        Me.pnlOtherFilter.SuspendLayout()
        Me.pnlLeaveTypes.SuspendLayout()
        Me.pnlCutomHeads.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(807, 60)
        Me.eZeeHeader.TabIndex = 21
        Me.eZeeHeader.Title = ""
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 495)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(807, 55)
        Me.EZeeFooter1.TabIndex = 22
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(411, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 6
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        Me.objbtnAdvanceFilter.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(514, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(610, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(706, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'objbtnSearchMembership
        '
        Me.objbtnSearchMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMembership.BorderSelected = False
        Me.objbtnSearchMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMembership.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMembership.Location = New System.Drawing.Point(333, 56)
        Me.objbtnSearchMembership.Name = "objbtnSearchMembership"
        Me.objbtnSearchMembership.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMembership.TabIndex = 220
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 300
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(89, 56)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(238, 21)
        Me.cboMembership.TabIndex = 219
        '
        'lblMembership
        '
        Me.lblMembership.BackColor = System.Drawing.Color.Transparent
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(5, 58)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(78, 16)
        Me.lblMembership.TabIndex = 218
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(333, 29)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 223
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(89, 29)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(238, 21)
        Me.cboEmployee.TabIndex = 222
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(5, 31)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(78, 16)
        Me.lblEmployee.TabIndex = 221
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.pnlOtherFilter)
        Me.gbMandatoryInfo.Controls.Add(Me.pnlLeaveTypes)
        Me.gbMandatoryInfo.Controls.Add(Me.pnlCutomHeads)
        Me.gbMandatoryInfo.Controls.Add(Me.cboReportType)
        Me.gbMandatoryInfo.Controls.Add(Me.lblReportType)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(694, 424)
        Me.gbMandatoryInfo.TabIndex = 224
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Filter Criteria"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlOtherFilter
        '
        Me.pnlOtherFilter.Controls.Add(Me.cboCatReason)
        Me.pnlOtherFilter.Controls.Add(Me.lblCatReason)
        Me.pnlOtherFilter.Controls.Add(Me.objbtnSearchCatReason)
        Me.pnlOtherFilter.Controls.Add(Me.cboPeriod)
        Me.pnlOtherFilter.Controls.Add(Me.objbtnSearchMembership)
        Me.pnlOtherFilter.Controls.Add(Me.lblMembership)
        Me.pnlOtherFilter.Controls.Add(Me.cboExitReason)
        Me.pnlOtherFilter.Controls.Add(Me.lblEmployee)
        Me.pnlOtherFilter.Controls.Add(Me.lblStaffExitReason)
        Me.pnlOtherFilter.Controls.Add(Me.cboMembership)
        Me.pnlOtherFilter.Controls.Add(Me.objbtnSearchExitReason)
        Me.pnlOtherFilter.Controls.Add(Me.cboEmployee)
        Me.pnlOtherFilter.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlOtherFilter.Controls.Add(Me.lblPeriod)
        Me.pnlOtherFilter.Location = New System.Drawing.Point(3, 58)
        Me.pnlOtherFilter.Name = "pnlOtherFilter"
        Me.pnlOtherFilter.Size = New System.Drawing.Size(358, 133)
        Me.pnlOtherFilter.TabIndex = 235
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 300
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(89, 2)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(238, 21)
        Me.cboPeriod.TabIndex = 228
        '
        'cboExitReason
        '
        Me.cboExitReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExitReason.DropDownWidth = 300
        Me.cboExitReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExitReason.FormattingEnabled = True
        Me.cboExitReason.Location = New System.Drawing.Point(89, 83)
        Me.cboExitReason.Name = "cboExitReason"
        Me.cboExitReason.Size = New System.Drawing.Size(238, 21)
        Me.cboExitReason.TabIndex = 230
        '
        'lblStaffExitReason
        '
        Me.lblStaffExitReason.BackColor = System.Drawing.Color.Transparent
        Me.lblStaffExitReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStaffExitReason.Location = New System.Drawing.Point(5, 85)
        Me.lblStaffExitReason.Name = "lblStaffExitReason"
        Me.lblStaffExitReason.Size = New System.Drawing.Size(78, 16)
        Me.lblStaffExitReason.TabIndex = 229
        Me.lblStaffExitReason.Text = "Exit Reason"
        Me.lblStaffExitReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchExitReason
        '
        Me.objbtnSearchExitReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExitReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExitReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExitReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExitReason.BorderSelected = False
        Me.objbtnSearchExitReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExitReason.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExitReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExitReason.Location = New System.Drawing.Point(333, 83)
        Me.objbtnSearchExitReason.Name = "objbtnSearchExitReason"
        Me.objbtnSearchExitReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExitReason.TabIndex = 231
        '
        'lblPeriod
        '
        Me.lblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(5, 4)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(78, 16)
        Me.lblPeriod.TabIndex = 227
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLeaveTypes
        '
        Me.pnlLeaveTypes.Controls.Add(Me.chkIncludePendingApprovedforms)
        Me.pnlLeaveTypes.Controls.Add(Me.chkSkipAbsentTnA)
        Me.pnlLeaveTypes.Controls.Add(Me.lnkSaveLeaveSelection)
        Me.pnlLeaveTypes.Controls.Add(Me.objchkLCheckAll)
        Me.pnlLeaveTypes.Controls.Add(Me.lvLeaveType)
        Me.pnlLeaveTypes.Controls.Add(Me.txtLeaveSearch)
        Me.pnlLeaveTypes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLeaveTypes.Location = New System.Drawing.Point(92, 194)
        Me.pnlLeaveTypes.Name = "pnlLeaveTypes"
        Me.pnlLeaveTypes.Size = New System.Drawing.Size(240, 223)
        Me.pnlLeaveTypes.TabIndex = 233
        '
        'chkIncludePendingApprovedforms
        '
        Me.chkIncludePendingApprovedforms.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludePendingApprovedforms.Location = New System.Drawing.Point(4, 201)
        Me.chkIncludePendingApprovedforms.Name = "chkIncludePendingApprovedforms"
        Me.chkIncludePendingApprovedforms.Size = New System.Drawing.Size(233, 17)
        Me.chkIncludePendingApprovedforms.TabIndex = 235
        Me.chkIncludePendingApprovedforms.Text = "Include Pending/Approve Form(s)"
        Me.chkIncludePendingApprovedforms.UseVisualStyleBackColor = True
        '
        'chkSkipAbsentTnA
        '
        Me.chkSkipAbsentTnA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSkipAbsentTnA.Location = New System.Drawing.Point(4, 178)
        Me.chkSkipAbsentTnA.Name = "chkSkipAbsentTnA"
        Me.chkSkipAbsentTnA.Size = New System.Drawing.Size(233, 17)
        Me.chkSkipAbsentTnA.TabIndex = 234
        Me.chkSkipAbsentTnA.Text = "Skip Absent From TnA"
        Me.chkSkipAbsentTnA.UseVisualStyleBackColor = True
        '
        'lnkSaveLeaveSelection
        '
        Me.lnkSaveLeaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSaveLeaveSelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSaveLeaveSelection.Location = New System.Drawing.Point(138, 5)
        Me.lnkSaveLeaveSelection.Name = "lnkSaveLeaveSelection"
        Me.lnkSaveLeaveSelection.Size = New System.Drawing.Size(95, 15)
        Me.lnkSaveLeaveSelection.TabIndex = 233
        Me.lnkSaveLeaveSelection.TabStop = True
        Me.lnkSaveLeaveSelection.Text = "Save Selection"
        '
        'objchkLCheckAll
        '
        Me.objchkLCheckAll.AutoSize = True
        Me.objchkLCheckAll.Location = New System.Drawing.Point(8, 29)
        Me.objchkLCheckAll.Name = "objchkLCheckAll"
        Me.objchkLCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkLCheckAll.TabIndex = 18
        Me.objchkLCheckAll.UseVisualStyleBackColor = True
        '
        'lvLeaveType
        '
        Me.lvLeaveType.CheckBoxes = True
        Me.lvLeaveType.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhLCheck, Me.colhLName})
        Me.lvLeaveType.FullRowSelect = True
        Me.lvLeaveType.GridLines = True
        Me.lvLeaveType.HideSelection = False
        Me.lvLeaveType.Location = New System.Drawing.Point(2, 24)
        Me.lvLeaveType.MultiSelect = False
        Me.lvLeaveType.Name = "lvLeaveType"
        Me.lvLeaveType.Size = New System.Drawing.Size(236, 148)
        Me.lvLeaveType.TabIndex = 0
        Me.lvLeaveType.UseCompatibleStateImageBehavior = False
        Me.lvLeaveType.View = System.Windows.Forms.View.Details
        '
        'objcolhLCheck
        '
        Me.objcolhLCheck.Tag = "objcolhLCheck"
        Me.objcolhLCheck.Text = ""
        Me.objcolhLCheck.Width = 25
        '
        'colhLName
        '
        Me.colhLName.Tag = "colhLName"
        Me.colhLName.Text = "Leave Type"
        Me.colhLName.Width = 203
        '
        'txtLeaveSearch
        '
        Me.txtLeaveSearch.BackColor = System.Drawing.SystemColors.Window
        Me.txtLeaveSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeaveSearch.Location = New System.Drawing.Point(2, 2)
        Me.txtLeaveSearch.Name = "txtLeaveSearch"
        Me.txtLeaveSearch.Size = New System.Drawing.Size(130, 21)
        Me.txtLeaveSearch.TabIndex = 227
        '
        'pnlCutomHeads
        '
        Me.pnlCutomHeads.Controls.Add(Me.chkincludesystemretirement)
        Me.pnlCutomHeads.Controls.Add(Me.lnkSaveHeadSelection)
        Me.pnlCutomHeads.Controls.Add(Me.objchkCheckAll)
        Me.pnlCutomHeads.Controls.Add(Me.lvCustomTranHead)
        Me.pnlCutomHeads.Controls.Add(Me.txtSearchCHeads)
        Me.pnlCutomHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCutomHeads.Location = New System.Drawing.Point(363, 34)
        Me.pnlCutomHeads.Name = "pnlCutomHeads"
        Me.pnlCutomHeads.Size = New System.Drawing.Size(322, 383)
        Me.pnlCutomHeads.TabIndex = 232
        '
        'chkincludesystemretirement
        '
        Me.chkincludesystemretirement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkincludesystemretirement.Location = New System.Drawing.Point(2, 361)
        Me.chkincludesystemretirement.Name = "chkincludesystemretirement"
        Me.chkincludesystemretirement.Size = New System.Drawing.Size(317, 17)
        Me.chkincludesystemretirement.TabIndex = 236
        Me.chkincludesystemretirement.Text = "Include Retirement Done By System"
        Me.chkincludesystemretirement.UseVisualStyleBackColor = True
        '
        'lnkSaveHeadSelection
        '
        Me.lnkSaveHeadSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSaveHeadSelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSaveHeadSelection.Location = New System.Drawing.Point(218, 5)
        Me.lnkSaveHeadSelection.Name = "lnkSaveHeadSelection"
        Me.lnkSaveHeadSelection.Size = New System.Drawing.Size(100, 15)
        Me.lnkSaveHeadSelection.TabIndex = 233
        Me.lnkSaveHeadSelection.TabStop = True
        Me.lnkSaveHeadSelection.Text = "Save Selection"
        '
        'objchkCheckAll
        '
        Me.objchkCheckAll.AutoSize = True
        Me.objchkCheckAll.Location = New System.Drawing.Point(8, 29)
        Me.objchkCheckAll.Name = "objchkCheckAll"
        Me.objchkCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkCheckAll.TabIndex = 18
        Me.objchkCheckAll.UseVisualStyleBackColor = True
        '
        'lvCustomTranHead
        '
        Me.lvCustomTranHead.CheckBoxes = True
        Me.lvCustomTranHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCCheck, Me.colhCName, Me.colhCHeadTypeID})
        Me.lvCustomTranHead.FullRowSelect = True
        Me.lvCustomTranHead.GridLines = True
        Me.lvCustomTranHead.HideSelection = False
        Me.lvCustomTranHead.Location = New System.Drawing.Point(2, 24)
        Me.lvCustomTranHead.MultiSelect = False
        Me.lvCustomTranHead.Name = "lvCustomTranHead"
        Me.lvCustomTranHead.Size = New System.Drawing.Size(317, 331)
        Me.lvCustomTranHead.TabIndex = 0
        Me.lvCustomTranHead.UseCompatibleStateImageBehavior = False
        Me.lvCustomTranHead.View = System.Windows.Forms.View.Details
        '
        'objcolhCCheck
        '
        Me.objcolhCCheck.Tag = "objcolhCCheck"
        Me.objcolhCCheck.Text = ""
        Me.objcolhCCheck.Width = 25
        '
        'colhCName
        '
        Me.colhCName.Tag = "colhCName"
        Me.colhCName.Text = "Name"
        Me.colhCName.Width = 288
        '
        'colhCHeadTypeID
        '
        Me.colhCHeadTypeID.Tag = "colhCHeadTypeID"
        Me.colhCHeadTypeID.Text = "Head Type ID"
        Me.colhCHeadTypeID.Width = 0
        '
        'txtSearchCHeads
        '
        Me.txtSearchCHeads.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchCHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchCHeads.Location = New System.Drawing.Point(2, 2)
        Me.txtSearchCHeads.Name = "txtSearchCHeads"
        Me.txtSearchCHeads.Size = New System.Drawing.Size(210, 21)
        Me.txtSearchCHeads.TabIndex = 227
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 120
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(92, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(238, 21)
        Me.cboReportType.TabIndex = 226
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.Color.Transparent
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(78, 16)
        Me.lblReportType.TabIndex = 225
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCatReason
        '
        Me.cboCatReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCatReason.DropDownWidth = 300
        Me.cboCatReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCatReason.FormattingEnabled = True
        Me.cboCatReason.Location = New System.Drawing.Point(89, 110)
        Me.cboCatReason.Name = "cboCatReason"
        Me.cboCatReason.Size = New System.Drawing.Size(238, 21)
        Me.cboCatReason.TabIndex = 233
        '
        'lblCatReason
        '
        Me.lblCatReason.BackColor = System.Drawing.Color.Transparent
        Me.lblCatReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCatReason.Location = New System.Drawing.Point(5, 112)
        Me.lblCatReason.Name = "lblCatReason"
        Me.lblCatReason.Size = New System.Drawing.Size(78, 16)
        Me.lblCatReason.TabIndex = 232
        Me.lblCatReason.Text = "Cat. Reason"
        Me.lblCatReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCatReason
        '
        Me.objbtnSearchCatReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCatReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCatReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCatReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCatReason.BorderSelected = False
        Me.objbtnSearchCatReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCatReason.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCatReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCatReason.Location = New System.Drawing.Point(333, 110)
        Me.objbtnSearchCatReason.Name = "objbtnSearchCatReason"
        Me.objbtnSearchCatReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCatReason.TabIndex = 234
        '
        'frmMonthlyPayrollReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 550)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMonthlyPayrollReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmMonthlyPayrollReport"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.pnlOtherFilter.ResumeLayout(False)
        Me.pnlLeaveTypes.ResumeLayout(False)
        Me.pnlLeaveTypes.PerformLayout()
        Me.pnlCutomHeads.ResumeLayout(False)
        Me.pnlCutomHeads.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchMembership As eZee.Common.eZeeGradientButton
    Public WithEvents cboMembership As System.Windows.Forms.ComboBox
    Private WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Private WithEvents lblReportType As System.Windows.Forms.Label
    Public WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Private WithEvents lblPeriod As System.Windows.Forms.Label
    Public WithEvents cboExitReason As System.Windows.Forms.ComboBox
    Private WithEvents lblStaffExitReason As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchExitReason As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlCutomHeads As System.Windows.Forms.Panel
    Friend WithEvents objchkCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvCustomTranHead As System.Windows.Forms.ListView
    Friend WithEvents objcolhCCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCHeadTypeID As System.Windows.Forms.ColumnHeader
    Private WithEvents txtSearchCHeads As System.Windows.Forms.TextBox
    Friend WithEvents lnkSaveHeadSelection As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlLeaveTypes As System.Windows.Forms.Panel
    Friend WithEvents lnkSaveLeaveSelection As System.Windows.Forms.LinkLabel
    Friend WithEvents objchkLCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvLeaveType As System.Windows.Forms.ListView
    Friend WithEvents objcolhLCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLName As System.Windows.Forms.ColumnHeader
    Private WithEvents txtLeaveSearch As System.Windows.Forms.TextBox
    Friend WithEvents pnlOtherFilter As System.Windows.Forms.Panel
    Friend WithEvents chkSkipAbsentTnA As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludePendingApprovedforms As System.Windows.Forms.CheckBox
    Friend WithEvents chkincludesystemretirement As System.Windows.Forms.CheckBox
    Public WithEvents cboCatReason As System.Windows.Forms.ComboBox
    Private WithEvents lblCatReason As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCatReason As eZee.Common.eZeeGradientButton
End Class
