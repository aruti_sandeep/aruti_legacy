#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmployeeStatus

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeStatus"

    'Shani (15-Jul-2016) -- Start
    'Issue : ASP not match with Payroll report 
    'Private objEmployeeDetail As clsEmployeeDetail_withED
    Private objEmployeeDetail As clsEmployee_Status
    'Shani (15-Jul-2016) -- End


    Private mstrAdvanceFilter As String = String.Empty

    'Shani(21-Dec-2015) -- Start
    'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Shani(21-Dec-2015) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        'Shani (15-Jul-2016) -- Start
        'Issue : ASP not match with Payroll report 
        'objEmployeeDetail = New clsEmployeeDetail_withED(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmployeeDetail = New clsEmployee_Status(User._Object._Languageunkid,Company._Object._Companyunkid)
        'Shani (15-Jul-2016) -- End
    End Sub

#End Region

#Region " Forms "

    Private Sub frmEmployeeStatus_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call ResetValue()
            chkShowEmpScale.Visible = User._Object.Privilege._AllowTo_View_Scale
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDetail_BBL_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeStatus_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeDetail = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeStatus_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeStatus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    'Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeStatus_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeStatus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeStatus_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()

        Dim dsList As DataSet = Nothing
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objDept As New clsDepartment
            Dim objJob As New clsJobs
            Dim objPeriod As New clscommom_period_Tran
            Dim objTranHead As New clsTransactionHead
            Dim objMembership As New clsmembership_master

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                            ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'Shani(02-Dec-2015) -- End

            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables("Employee")

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 101, "Active Employee listing with E&D"))
                .Items.Add(Language.getMessage(mstrModuleName, 102, "New staff listing with E&D"))
                .Items.Add(Language.getMessage(mstrModuleName, 103, "Terminated Staff listing with E&D"))
                .SelectedIndex = 0
            End With

            dsList = objDept.getComboList("Department", True)
            cboDepartment.DisplayMember = "name"
            cboDepartment.ValueMember = "departmentunkid"
            cboDepartment.DataSource = dsList.Tables("Department")

            dsList = objJob.getComboList("Job", True)
            cboJob.DisplayMember = "name"
            cboJob.ValueMember = "jobunkid"
            cboJob.DataSource = dsList.Tables("Job")


            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Company._Object._YearUnkid, ConfigParameter._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Shani(21-Dec-2015) -- End

            cboPeriod.DisplayMember = "name"
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DataSource = dsList.Tables("Period")

            dsList = objMembership.getListForCombo("Membership", False)
            Dim lvItem As ListViewItem
            For Each xRow As DataRow In dsList.Tables("Membership").Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = CInt(xRow.Item("membershipunkid"))
                lvItem.SubItems.Add(xRow.Item("name").ToString)
                lvMembership.Items.Add(lvItem)
            Next

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            'If User._Object.Privilege._AllowTo_View_Scale Then
            '    dsList = objTranHead.getComboList(ConfigParameter._Object._DatabaseName, "TranHead", True)
            'Else
            '    dsList = objTranHead.getComboList(ConfigParameter._Object._DatabaseName, "TranHead", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " AND calctype_id <>" & enCalcType.NET_PAY)
            'End If
            If User._Object.Privilege._AllowTo_View_Scale Then
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True)
            Else
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " AND calctype_id <>" & enCalcType.NET_PAY)
            End If
            'Shani(21-Dec-2015) -- End

            cboTranHead1.DisplayMember = "name"
            cboTranHead1.ValueMember = "tranheadunkid"
            cboTranHead1.DataSource = dsList.Tables("TranHead")

            cboTranHead2.DisplayMember = "name"
            cboTranHead2.ValueMember = "tranheadunkid"
            cboTranHead2.DataSource = dsList.Tables("TranHead").Copy()

            cboTranHead3.DisplayMember = "name"
            cboTranHead3.ValueMember = "tranheadunkid"
            cboTranHead3.DataSource = dsList.Tables("TranHead").Copy()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboTranHead1.SelectedValue = 0
            cboTranHead2.SelectedValue = 0
            cboTranHead3.SelectedValue = 0
            objEmployeeDetail.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmployeeDetail.OrderByDisplay
            chkInactiveemp.Checked = False
            mstrAdvanceFilter = ""
            chkShowEmpScale.Checked = False
            For Each lvItem As ListViewItem In lvMembership.CheckedItems
                lvItem.Checked = False
            Next
            objchkCheckAll.Checked = False

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrReport_GroupName = ""
            'Shani(21-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim dicMembership As New Dictionary(Of Integer, String)
        Dim objPeriod As New clscommom_period_Tran
        Try
            objEmployeeDetail.SetDefaultValue()

            objEmployeeDetail._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEmployeeDetail._EmployeeName = cboEmployee.Text
            objEmployeeDetail._DepartmentId = CInt(cboDepartment.SelectedValue)
            objEmployeeDetail._Department = cboDepartment.Text
            objEmployeeDetail._JobId = CInt(cboJob.SelectedValue)
            objEmployeeDetail._Job = cboJob.Text
            objEmployeeDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmployeeDetail._Period = cboPeriod.Text
            For Each lvItem As ListViewItem In lvMembership.CheckedItems
                dicMembership.Add(lvItem.Tag, lvItem.SubItems(colhMembership.Index).Text)
            Next
            objEmployeeDetail._MembershipDic = dicMembership
            objEmployeeDetail._TranheadId1 = CInt(cboTranHead1.SelectedValue)
            objEmployeeDetail._TranHead1 = cboTranHead1.Text
            objEmployeeDetail._TranheadId2 = CInt(cboTranHead2.SelectedValue)
            objEmployeeDetail._TranHead2 = cboTranHead2.Text
            objEmployeeDetail._TranheadId3 = CInt(cboTranHead3.SelectedValue)
            objEmployeeDetail._TranHead3 = cboTranHead3.Text
            objEmployeeDetail._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmployeeDetail._Advance_Filter = mstrAdvanceFilter
            objEmployeeDetail._ShowEmployeeScale = chkShowEmpScale.Checked

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            'objPeriod._Periodunkid(ConfigParameter._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Shani(21-Dec-2015) -- End

            objEmployeeDetail._Period_Start_Date = objPeriod._Start_Date
            objEmployeeDetail._Period_End_Date = objPeriod._End_Date

            'Shani (15-Jul-2016) -- Start
            'Issue : ASP not match with Payroll report 
            'objEmployeeDetail._IsExport = True
            'Shani (15-Jul-2016) -- End

            objEmployeeDetail._ReportTypeName = cboReportType.Text

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            'objEmployeeDetail._ViewByIds = mstrStringIds
            objEmployeeDetail._ViewIndex = mintViewIdx
            'objEmployeeDetail._ViewByName = mstrStringName

            objEmployeeDetail._Analysis_Fields = mstrAnalysis_Fields
            objEmployeeDetail._Analysis_Join = mstrAnalysis_Join
            objEmployeeDetail._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmployeeDetail._Report_GroupName = mstrReport_GroupName

            'Shani(21-Dec-2015) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmp.Click
        Dim objfrm As New frmCommonSearch
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmp_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDept.Click
        Dim objfrm As New frmCommonSearch
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboDepartment.DataSource, DataTable)
            With cboDepartment
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchDept_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim objfrm As New frmCommonSearch
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboJob.DataSource, DataTable)
            With cboJob
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboPeriod.DataSource, DataTable)
            With cboPeriod
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchPeriod_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnsearchTranHead1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnsearchTranHead1.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboTranHead1.DataSource, DataTable)
            With cboTranHead1
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchTranHead1_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnsearchTranHead2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnsearchTranHead2.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboTranHead2.DataSource, DataTable)
            With cboTranHead2
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchTranHead2_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnsearchTranHead3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnsearchTranHead3.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboTranHead3.DataSource, DataTable)
            With cboTranHead3
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchTranHead3_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            ElseIf CInt(cboTranHead1.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 105, "Transaction Head1 is compulsory information.Please Select Transaction Head1."), enMsgBoxStyle.Information)
                cboTranHead1.Focus()
                Exit Sub
            ElseIf CInt(cboTranHead2.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Transaction Head2 is compulsory information.Please Select Transaction Head2."), enMsgBoxStyle.Information)
                cboTranHead2.Focus()
                Exit Sub
            ElseIf CInt(cboTranHead3.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 107, "Transaction Head3 is compulsory information.Please Select Transaction Head3."), enMsgBoxStyle.Information)
                cboTranHead3.Focus()
                Exit Sub
            ElseIf lvMembership.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Membership is compulsory information.Please Select at least one Membership."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If SetFilter() = False Then Exit Sub
            'Shani (15-Jul-2016) -- Start
            'Issue : ASP not match with Payroll report 
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Shani (15-Jul-2016) -- End

            

            objEmployeeDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                objPeriod._Start_Date, _
                                                objPeriod._End_Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                cboReportType.SelectedIndex, enPrintAction.None, enExportAction.None)
            'Shani (15-Jul-2016) -- Change[objPeriod._Start_Date,objPeriod._End_Date]
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDetail_BBL_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmployeeDetail.setOrderBy(0)
            txtOrderBy.Text = objEmployeeDetail.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeDetail_withED.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeDetail_BBL"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Control Event(S)"

    Private Sub objchkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkCheckAll.CheckedChanged
        Try
            For Each lvItem As ListViewItem In lvMembership.Items
                lvItem.Checked = objchkCheckAll.CheckState
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Shani(21-Dec-2015) -- Start
    'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
    
#Region "LinkLabel Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region
    'Shani(21-Dec-2015) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objeZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.objeZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.objeZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.objeZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.objeZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.chkShowEmpScale.Text = Language._Object.getCaption(Me.chkShowEmpScale.Name, Me.chkShowEmpScale.Text)
            Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)
            Me.LblTranHead3.Text = Language._Object.getCaption(Me.LblTranHead3.Name, Me.LblTranHead3.Text)
            Me.LblTranHead2.Text = Language._Object.getCaption(Me.LblTranHead2.Name, Me.LblTranHead2.Text)
            Me.LblTranHead1.Text = Language._Object.getCaption(Me.LblTranHead1.Name, Me.LblTranHead1.Text)
            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.colhMembership.Text = Language._Object.getCaption(CStr(Me.colhMembership.Tag), Me.colhMembership.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 101, "Active Employee listing with E&D")
            Language.setMessage(mstrModuleName, 102, "New staff listing with E&D")
            Language.setMessage(mstrModuleName, 103, "Terminated Staff listing with E&D")
            Language.setMessage(mstrModuleName, 104, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 105, "Transaction Head1 is compulsory information.Please Select Transaction Head1.")
            Language.setMessage(mstrModuleName, 106, "Transaction Head2 is compulsory information.Please Select Transaction Head2.")
            Language.setMessage(mstrModuleName, 107, "Transaction Head3 is compulsory information.Please Select Transaction Head3.")
            Language.setMessage(mstrModuleName, 108, "Membership is compulsory information.Please Select at least one Membership.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
