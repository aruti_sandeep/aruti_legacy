'************************************************************************************************************************************
'Class Name : clsEmp_AD_Report.vb
'Purpose    :
'Date       : 08-Feb-2013
'Written By : Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsEmp_AD_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmp_AD_Report"
    Private mstrReportId As String = enArutiReport.Asset_Declaration_Report
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_NonOfficialReport()
        Call Create_WithoutAssetReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mdtAsOnDate As DateTime = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnIncluderInactiveEmp As Boolean = False
    Private mintAppointmentTypeId As Integer = 0
    Private mstrAppointmentTypeName As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    Private mdecDAmtFrom As Decimal = 0
    Private mdecDAmtTo As Decimal = 0
    Private mdecLAmtFrom As Decimal = 0
    Private mdecLAmtTo As Decimal = 0
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnShowSalary As Boolean = False
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeName() As String
        Set(ByVal value As String)
            mstrAppointmentTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property

    Public WriteOnly Property _DAmtFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecDAmtFrom = value
        End Set
    End Property

    Public WriteOnly Property _DAmtTo() As Decimal
        Set(ByVal value As Decimal)
            mdecDAmtTo = value
        End Set
    End Property

    Public WriteOnly Property _LAmtFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecLAmtFrom = value
        End Set
    End Property

    Public WriteOnly Property _LAmtTo() As Decimal
        Set(ByVal value As Decimal)
            mdecLAmtTo = value
        End Set
    End Property

    Public WriteOnly Property _ShowSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSalary = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtAsOnDate = Nothing
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mblnIncluderInactiveEmp = False
            mintAppointmentTypeId = 0
            mstrAppointmentTypeName = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mdecDAmtFrom = -1
            mdecDAmtTo = -1
            mdecLAmtFrom = -1
            mdecLAmtTo = -1
            mstrUserAccessFilter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Sub FilterTitleAndFilterQuery()
    Private Sub FilterTitleAndFilterQuery()
        'Shani(24-Aug-2015) -- End

        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'Shani(24-Aug-2015) -- End
            objDataOperation.AddParameter("@SalDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate).ToString)

            objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate).ToString)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "As On Date : ") & " " & mdtAsOnDate.ToShortDateString & " "

            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate2).ToString)
                        Select Case mintReportTypeId
                            Case 0, 2
                                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN @Date1 AND @Date2 "
                            Case 1
                                Me._FilterQuery &= " AND App_Date BETWEEN @Date1 AND @Date2 "
                        End Select

                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Appointed Date From : ") & " " & mdtDate1.ToShortDateString & " " & _
                                           Language.getMessage(mstrModuleName, 102, "To : ") & " " & mdtDate2.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        Select Case mintReportTypeId
                            Case 0, 2
                                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < @Date1 "
                            Case 1
                                Me._FilterQuery &= " AND App_Date < @Date1 "
                        End Select

                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, "Appointed Before Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        Select Case mintReportTypeId
                            Case 0, 2
                                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > @Date1 "
                            Case 1
                                Me._FilterQuery &= " AND App_Date > @Date1 "
                        End Select
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 104, "Appointed After Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
            End Select

            Select Case mintReportTypeId
                Case 0, 2
                    If mdecDAmtFrom >= 0 AndAlso mdecDAmtTo >= 0 Then
                        objDataOperation.AddParameter("@DAmtFrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDAmtFrom)
                        objDataOperation.AddParameter("@DAmtTo", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDAmtTo)
                        Me._FilterQuery &= " AND (ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
                                           "      ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
                                           "      ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
                                           "      ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0)) BETWEEN @DAmtFrom AND @DAmtTo "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 105, "Declared Amount From : ") & " " & mdecDAmtFrom & " " & _
                                           Language.getMessage(mstrModuleName, 102, "To : ") & " " & mdecDAmtTo & " "
                    End If

                    If mdecLAmtFrom >= 0 AndAlso mdecLAmtTo >= 0 Then
                        objDataOperation.AddParameter("@LAmtFrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLAmtFrom)
                        objDataOperation.AddParameter("@LAmtTo", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLAmtTo)
                        Me._FilterQuery &= " AND ISNULL(debttotal,0) BETWEEN @LAmtFrom AND @LAmtTo "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 106, "Liability Amount From : ") & " " & mdecLAmtFrom & " " & _
                                           Language.getMessage(mstrModuleName, 102, "To : ") & " " & mdecLAmtTo & " "
                    End If

                    If mintEmployeeId > 0 Then
                        objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                        Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 107, "Employee : ") & " " & mstrEmployeeName & " "
                    End If

                Case 1
                    If mintEmployeeId > 0 Then
                        objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                        Me._FilterQuery &= " AND EmpId = @EmployeeId "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 107, "Employee : ") & " " & mstrEmployeeName & " "
                    End If
            End Select

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 108, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    Select Case pintReportType
        '        Case 0, 2
        '            objRpt = Generate_NonOfficial_Report()
        '        Case 1
        '            objRpt = Generate_WithoutAsset_Report()
        '    End Select

        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = xCompanyUnkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = xUserUnkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Select Case pintReportType
                Case 0, 2
                    objRpt = Generate_NonOfficial_Report(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                Case 1
                    objRpt = Generate_WithoutAsset_Report(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End Select

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            Select Case intReportType
                Case 0, 2
                    OrderByDisplay = iColumn_NonOfficial.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_NonOfficial.ColumnItem(0).Name
                Case 1
                    OrderByDisplay = iColumn_WithoutAsset.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_WithoutAsset.ColumnItem(0).Name
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Select Case intReportType
                Case 0, 2
                    Call OrderByExecute(iColumn_NonOfficial)
                Case 1
                    Call OrderByExecute(iColumn_WithoutAsset)
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

#Region " NON OFFICIAL / OFFICIAL ASSET DECLARATION REPORT "

    Dim iColumn_NonOfficial As New IColumnCollection
    Public Property Field_NonOfficial() As IColumnCollection
        Get
            Return iColumn_NonOfficial
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_NonOfficial = value
        End Set
    End Property

    Private Sub Create_NonOfficialReport()
        Try
            iColumn_NonOfficial.Clear()
            iColumn_NonOfficial.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 200, "Employee Code")))

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            ' iColumn_NonOfficial.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 201, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_NonOfficial.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 201, "Employee Name")))
            Else
                iColumn_NonOfficial.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 201, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END
            iColumn_NonOfficial.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 202, "Appointed Date")))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iColumn_NonOfficial.Add(New IColumn("ISNULL(Dept.name,'')", Language.getMessage(mstrModuleName, 203, "Department")))
            'iColumn_NonOfficial.Add(New IColumn("ISNULL(JBM.job_name,'')", Language.getMessage(mstrModuleName, 204, "Job Title")))
            'iColumn_NonOfficial.Add(New IColumn("ISNULL(ClsG.name,'')", Language.getMessage(mstrModuleName, 205, "Region")))
            'iColumn_NonOfficial.Add(New IColumn("ISNULL(Cls.name,'')", Language.getMessage(mstrModuleName, 206, "WorkStation")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(DM.name,'')", Language.getMessage(mstrModuleName, 203, "Department")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(JM.job_name,'')", Language.getMessage(mstrModuleName, 204, "Job Title")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(CGM.name,'')", Language.getMessage(mstrModuleName, 205, "Region")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(CM.name,'')", Language.getMessage(mstrModuleName, 206, "WorkStation")))
            'Shani(24-Aug-2015) -- End
            iColumn_NonOfficial.Add(New IColumn("(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
                                                "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
                                                "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
                                                "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0))", Language.getMessage(mstrModuleName, 208, "Declared Amount")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(debttotal,0)", Language.getMessage(mstrModuleName, 209, "Liability Amount")))
            iColumn_NonOfficial.Add(New IColumn("ISNULL(Salary,0)", Language.getMessage(mstrModuleName, 207, "Salary")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_NonOfficialReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_NonOfficial_Report() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_NonOfficial_Report(ByVal strDatabaseName As String, _
                                                 ByVal intUserUnkid As Integer, _
                                                 ByVal intYearUnkid As Integer, _
                                                 ByVal intCompanyUnkid As Integer, _
                                                 ByVal dtPeriodStart As Date, _
                                                 ByVal dtPeriodEnd As Date, _
                                                 ByVal strUserModeSetting As String, _
                                                 ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT " & _
                   " ISNULL(hremployee_master.employeecode,'') AS ECode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '       ",ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'') AS EName " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') AS App_Date " & _
            '       ",hremployee_master.appointeddate AS appointeddate " & _
            '       ",ISNULL(Dept.name,'') AS Department " & _
            '       ",ISNULL(JBM.job_name,'') AS Job_Title " & _
            '       ",ISNULL(ClsG.name,'') AS Region " & _
            '       ",ISNULL(Cls.name,'') AS WorkStation " & _
            '       ",ISNULL(Salary,0) AS Salary " & _
            '       ",(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
            '       "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
            '       "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
            '       "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0)) AS Tot_Decl_Amt " & _
            '       ",ISNULL(debttotal,0) AS Tot_Liab_Amt "

            StrQ &= ",ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') AS App_Date " & _
                   ",hremployee_master.appointeddate AS appointeddate " & _
                    " ,ISNULL(DM.name,'') AS Department " & _
                    " ,ISNULL(JM.job_name,'') AS Job_Title " & _
                    " ,ISNULL(CGM.name,'') AS Region " & _
                    " ,ISNULL(CM.name,'') AS WorkStation " & _
                   ",ISNULL(Salary,0) AS Salary " & _
                   ",(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
                   "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
                   "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
                   "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0)) AS Tot_Decl_Amt " & _
                   ",ISNULL(debttotal,0) AS Tot_Liab_Amt "
            'Shani(24-Aug-2015) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "FROM hrassetdeclaration_master " & _
                    "JOIN hremployee_master ON hrassetdeclaration_master.employeeunkid = hremployee_master.employeeunkid "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= mstrAnalysis_Join
            'StrQ &= "LEFT JOIN hrclasses_master AS Cls ON hremployee_master.classunkid = Cls.classesunkid " & _
            '        "LEFT JOIN hrclassgroup_master AS ClsG ON hremployee_master.classgroupunkid = ClsG.classgroupunkid " & _
            '        "JOIN hrjob_master AS JBM ON hremployee_master.jobunkid = JBM.jobunkid " & _
            '        "JOIN hrdepartment_master AS Dept ON hremployee_master.departmentunkid = Dept.departmentunkid "
            StrQ &= "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,classgroupunkid " & _
                    "           ,classunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                    "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "
            'Shani(24-Aug-2015) -- End
            StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        Eid "
            If mblnShowSalary = True Then
                StrQ &= "       ,Salary "
            Else
                StrQ &= " ,0 AS Salary "
            End If

            StrQ &= "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            employeeunkid AS Eid " & _
                    "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                    "           ,newscale AS Salary " & _
                    "       FROM prsalaryincrement_tran " & _
                    "       WHERE CONVERT(CHAR(8),incrementdate,112) <= @SalDate AND isapproved = 1 AND isvoid = 0 " & _
                    "   ) AS A " & _
                    "   WHERE   A.Rno = 1 " & _
                    ") AS B ON B.Eid = hremployee_master.employeeunkid "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Shani(24-Aug-2015) -- End

            StrQ &= "WHERE hrassetdeclaration_master.isvoid = 0 " & _
                    "AND CONVERT(CHAR(8),savedate,112) <= @AsOnDate "

            Select Case mintReportTypeId
                Case 0
                    StrQ &= " AND hrassetdeclaration_master.isfinalsaved = 0 "
                Case 2
                    StrQ &= " AND hrassetdeclaration_master.isfinalsaved = 1 "
            End Select


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END

            'If mblnIncluderInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncluderInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing
            Dim iCnt As Integer = 1 : Dim StrGrpName As String = String.Empty
            Dim dtTable As DataTable
            If mstrReport_GroupName.Trim.Length > 0 Then
                dtTable = New DataView(dsList.Tables("DataTable"), "", "GName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("DataTable")
            End If

            For Each dtRow As DataRow In dtTable.Rows
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                'If StrGrpName <> dtRow.Item("GName") Then
                '    iCnt = 1
                '    StrGrpName = dtRow.Item("GName")
                'End If

                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                rpt_Rows.Item("Column2") = dtRow.Item("EName")
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("App_Date").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = dtRow.Item("Job_Title")
                rpt_Rows.Item("Column5") = dtRow.Item("Department")
                rpt_Rows.Item("Column6") = dtRow.Item("Region")
                rpt_Rows.Item("Column7") = dtRow.Item("WorkStation")
                rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("Tot_Decl_Amt")), GUI.fmtCurrency)
                rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("Tot_Liab_Amt")), GUI.fmtCurrency)
                rpt_Rows.Item("Column11") = iCnt.ToString
                rpt_Rows.Item("Column12") = dtRow.Item("GName")
                rpt_Rows.Item("Column82") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Salary)", "GName = '" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Rows.Item("Column83") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Tot_Decl_Amt)", "GName = '" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Rows.Item("Column84") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Tot_Liab_Amt)", "GName = '" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)

                rpt_Rows.Item("Column85") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Salary)", "")), GUI.fmtCurrency)
                rpt_Rows.Item("Column86") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Tot_Decl_Amt)", "")), GUI.fmtCurrency)
                rpt_Rows.Item("Column87") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Tot_Liab_Amt)", "")), GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                iCnt += 1
            Next

            Select Case mintReportTypeId
                Case 0
                    objRpt = New ArutiReport.Designer.rptUncommittedAD
                Case 2
                    objRpt = New ArutiReport.Designer.rptSubmittedAD
            End Select

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 300, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 301, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 302, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 303, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            If mblnShowSalary = False Then
                ReportFunction.EnableSuppress(objRpt, "txtSalAmt", True)
                ReportFunction.EnableSuppress(objRpt, "Column81", True)
                ReportFunction.EnableSuppress(objRpt, "Column821", True)
                ReportFunction.EnableSuppress(objRpt, "txtTotalAmt1", True)

                objRpt.ReportDefinition.ReportObjects("txtDecAmt").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                objRpt.ReportDefinition.ReportObjects("Column91").Left = objRpt.ReportDefinition.ReportObjects("Column81").Left
                objRpt.ReportDefinition.ReportObjects("Column831").Left = objRpt.ReportDefinition.ReportObjects("Column821").Left
                objRpt.ReportDefinition.ReportObjects("txtTotalAmt2").Left = objRpt.ReportDefinition.ReportObjects("txtTotalAmt1").Left

                Dim iPosition As Integer = objRpt.ReportDefinition.ReportObjects("txtDecAmt").Left
                iPosition = iPosition + objRpt.ReportDefinition.ReportObjects("txtDecAmt").Width + 30
                objRpt.ReportDefinition.ReportObjects("txtLiability").Left = iPosition
                objRpt.ReportDefinition.ReportObjects("Column101").Left = iPosition
                objRpt.ReportDefinition.ReportObjects("Column841").Left = iPosition
                objRpt.ReportDefinition.ReportObjects("txtTotalAmt3").Left = iPosition
            End If

            ReportFunction.TextChange(objRpt, "txtSN", Language.getMessage(mstrModuleName, 304, "Sno."))
            ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 200, "Employee Code"))
            ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 201, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtAdate", Language.getMessage(mstrModuleName, 202, "Appointed Date"))
            ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 203, "Department"))
            ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 204, "Job Title"))
            ReportFunction.TextChange(objRpt, "txtClassGrp", Language.getMessage(mstrModuleName, 205, "Region"))
            ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 206, "WorkStation"))
            ReportFunction.TextChange(objRpt, "txtSalAmt", Language.getMessage(mstrModuleName, 207, "Salary"))
            ReportFunction.TextChange(objRpt, "txtDecAmt", Language.getMessage(mstrModuleName, 208, "Declared Amount"))
            ReportFunction.TextChange(objRpt, "txtLiability", Language.getMessage(mstrModuleName, 209, "Liability Amount"))

            ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 307, "Group Count : "))
            ReportFunction.TextChange(objRpt, "txtGrandCount", Language.getMessage(mstrModuleName, 308, "Grand Count : "))
            ReportFunction.TextChange(objRpt, "txtGrpTotal", Language.getMessage(mstrModuleName, 309, "Group Total : "))
            ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 310, "Grand Total :"))


            ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                If IsDBNull(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column82")) = False Then
                    Call ReportFunction.TextChange(objRpt, "txtTotalAmt1", Format(CDec(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column85")), GUI.fmtCurrency))
                End If

                If IsDBNull(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column83")) = False Then
                    Call ReportFunction.TextChange(objRpt, "txtTotalAmt2", Format(CDec(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column86")), GUI.fmtCurrency))
                End If

                If IsDBNull(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column84")) = False Then
                    Call ReportFunction.TextChange(objRpt, "txtTotalAmt3", Format(CDec(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column87")), GUI.fmtCurrency))
                End If
            End If

            ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 305, "Printed By :"))
            ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 306, "Printed Date :"))

            ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_NonOfficial_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " NOT DECLARED ANY ASSET REPORT "
    Dim iColumn_WithoutAsset As New IColumnCollection

    Public Property Field_WithoutAsset() As IColumnCollection
        Get
            Return iColumn_WithoutAsset
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_WithoutAsset = value
        End Set
    End Property

    Private Sub Create_WithoutAssetReport()
        Try
            iColumn_WithoutAsset.Clear()
            iColumn_WithoutAsset.Add(New IColumn("ECode", Language.getMessage(mstrModuleName, 200, "Employee Code")))
            iColumn_WithoutAsset.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 201, "Employee Name")))
            iColumn_WithoutAsset.Add(New IColumn("appointeddate", Language.getMessage(mstrModuleName, 202, "Appointed Date")))
            iColumn_WithoutAsset.Add(New IColumn("Department", Language.getMessage(mstrModuleName, 203, "Department")))
            iColumn_WithoutAsset.Add(New IColumn("Job_Title", Language.getMessage(mstrModuleName, 204, "Job Title")))
            iColumn_WithoutAsset.Add(New IColumn("Region", Language.getMessage(mstrModuleName, 205, "Region")))
            iColumn_WithoutAsset.Add(New IColumn("WorkStation", Language.getMessage(mstrModuleName, 206, "WorkStation")))
            iColumn_WithoutAsset.Add(New IColumn("Salary", Language.getMessage(mstrModuleName, 207, "Salary")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_WithoutAssetReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_WithoutAsset_Report() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_WithoutAsset_Report(ByVal strDatabaseName As String, _
                                                  ByVal intUserUnkid As Integer, _
                                                  ByVal intYearUnkid As Integer, _
                                                  ByVal intCompanyUnkid As Integer, _
                                                  ByVal dtPeriodStart As Date, _
                                                  ByVal dtPeriodEnd As Date, _
                                                  ByVal strUserModeSetting As String, _
                                                  ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT ECode,EName,App_Date,Department,Job_Title,Region,WorkStation,appointeddate,Id,GName,Salary,EmpId " & _
                   " FROM ( "
            StrQ &= "SELECT " & _
                       " ECode AS ECode " & _
                       ",EName AS EName " & _
                       ",App_Date AS App_Date " & _
                       ",Department AS Department " & _
                       ",Job_Title AS Job_Title " & _
                       ",Region AS Region " & _
                       ",WorkStation AS WorkStation " & _
                       ",appointeddate AS appointeddate " & _
                       ",Id AS Id " & _
                       ",GName AS GName "
            If mblnShowSalary = True Then
                StrQ &= ",Salary AS Salary "
            Else
                StrQ &= ",0 AS Salary "
            End If
            StrQ &= ",EmpId AS EmpId " & _
                    "FROM " & _
                    "( " & _
                              "SELECT " & _
                              " ISNULL(hremployee_master.employeecode,'') AS ECode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '      ",ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'') AS EName " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",ISNULL(CONVERT(CHAR(8), appointeddate,112),'') AS App_Date " & _
            '                  ",ISNULL(Dept.name,'') AS Department " & _
            '                  ",ISNULL(JBM.job_name,'') AS Job_Title " & _
            '                  ",ISNULL(ClsG.name,'') AS Region " & _
            '                  ",ISNULL(Cls.name,'') AS WorkStation " & _
            '                  ",ISNULL(Salary,0) AS Salary " & _
            '                  ",hremployee_master.appointeddate AS appointeddate " & _
            '                  ",employeeunkid AS EmpId "

            StrQ &= ",ISNULL(CONVERT(CHAR(8), appointeddate,112),'') AS App_Date " & _
                              ",ISNULL(DM.name,'') AS Department " & _
                              ",ISNULL(JM.job_name,'') AS Job_Title " & _
                              ",ISNULL(CGM.name,'') AS Region " & _
                              ",ISNULL(CM.name,'') AS WorkStation " & _
                              ",ISNULL(Salary,0) AS Salary " & _
                              ",hremployee_master.appointeddate AS appointeddate " & _
                              ",hremployee_master.employeeunkid AS EmpId "

            'Shani(24-Aug-2015) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "FROM hremployee_master "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= mstrAnalysis_Join
            'StrQ &= "LEFT JOIN hrclasses_master AS Cls ON hremployee_master.classunkid = Cls.classesunkid " & _
            '             "LEFT JOIN hrclassgroup_master AS ClsG ON hremployee_master.classgroupunkid = ClsG.classgroupunkid " & _
            '             "JOIN hrjob_master JBM ON hremployee_master.jobunkid = JBM.jobunkid " & _
            '             "JOIN hrdepartment_master AS Dept ON hremployee_master.departmentunkid = Dept.departmentunkid " & _
            '             "LEFT JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "Eid " & _
            '                       ",Salary " & _
            '                  "FROM " & _
            '                  "( " & _
            '                       "SELECT " & _
            '                             "employeeunkid AS Eid " & _
            '                            ",ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
            '                            ",newscale AS Salary " & _
            '                       "FROM prsalaryincrement_tran " & _
            '                       "WHERE incrementdate < = @SalDate AND isapproved = 1 AND isvoid = 0 " & _
            '                  ") AS A " & _
            '                  "WHERE   A.Rno = 1 " & _
            '             ") AS B ON B.Eid = hremployee_master.employeeunkid " & _
            '             "WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM hrassetdeclaration_master WHERE isvoid = 0) "
            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END

            'If mblnIncluderInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            'StrQ &= "UNION " & _
            '             "SELECT " & _
            '                   "ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            '                  ",ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'') AS EName " & _
            '                  ",ISNULL(CONVERT(CHAR(8), appointeddate,112),'') AS App_Date " & _
            '                  ",ISNULL(Dept.name,'') AS Department " & _
            '                  ",ISNULL(JBM.job_name,'') AS Job_Title " & _
            '                  ",ISNULL(ClsG.name,'') AS Region " & _
            '                  ",ISNULL(Cls.name,'') AS WorkStation " & _
            '                  ",ISNULL(Salary,0) AS Salary " & _
            '                  ",hremployee_master.appointeddate AS appointeddate " & _
            '                  ",hremployee_master.employeeunkid AS EmpId "

            StrQ &= "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,classgroupunkid " & _
                    "           ,classunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                    "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "Eid " & _
                                   ",Salary " & _
                              "FROM " & _
                              "( " & _
                                   "SELECT " & _
                                         "employeeunkid AS Eid " & _
                                        ",ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                        ",newscale AS Salary " & _
                                   "FROM prsalaryincrement_tran " & _
                                   "WHERE incrementdate < = @SalDate AND isapproved = 1 AND isvoid = 0 " & _
                              ") AS A " & _
                              "WHERE   A.Rno = 1 " & _
                    "   ) AS B ON B.Eid = hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM hrassetdeclaration_master WHERE isvoid = 0) "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncluderInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "UNION " & _
                         "SELECT " & _
                               "ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                              ",ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'') AS EName " & _
                              ",ISNULL(CONVERT(CHAR(8), appointeddate,112),'') AS App_Date " & _
                              ",ISNULL(DM.name,'') AS Department " & _
                              ",ISNULL(JM.job_name,'') AS Job_Title " & _
                              ",ISNULL(CGM.name,'') AS Region " & _
                              ",ISNULL(CM.name,'') AS WorkStation " & _
                              ",ISNULL(Salary,0) AS Salary " & _
                              ",hremployee_master.appointeddate AS appointeddate " & _
                              ",hremployee_master.employeeunkid AS EmpId "

            'Shani(24-Aug-2015) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "FROM hrassetdeclaration_master " & _
                         "JOIN hremployee_master ON hrassetdeclaration_master.employeeunkid = hremployee_master.employeeunkid "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'StrQ &= mstrAnalysis_Join

            'StrQ &= "LEFT JOIN hrclasses_master AS Cls ON hremployee_master.classunkid = Cls.classesunkid " & _
            '             "LEFT JOIN hrclassgroup_master AS ClsG ON hremployee_master.classgroupunkid = ClsG.classgroupunkid " & _
            '             "JOIN hrjob_master AS JBM ON hremployee_master.jobunkid = JBM.jobunkid " & _
            '             "JOIN hrdepartment_master AS Dept ON hremployee_master.departmentunkid = Dept.departmentunkid " & _
            '             "LEFT JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "Eid " & _
            '                       ",Salary " & _
            '                  "FROM " & _
            '                  "( " & _
            '                       "SELECT " & _
            '                             "employeeunkid AS Eid " & _
            '                            ",ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
            '                            ",newscale AS Salary " & _
            '                       "FROM prsalaryincrement_tran " & _
            '                       "WHERE incrementdate < = @SalDate AND isapproved = 1 AND isvoid = 0 " & _
            '                  ") AS A " & _
            '                  "WHERE   A.Rno = 1 " & _
            '             ") AS B ON B.Eid = hremployee_master.employeeunkid " & _
            '             "WHERE hrassetdeclaration_master.isvoid = 0 AND hrassetdeclaration_master.isfinalsaved = 0 " & _
            '             "AND CONVERT(CHAR(8),savedate,112) >= @AsOnDate "

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END

            'If mblnIncluderInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If


            StrQ &= "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,classgroupunkid " & _
                    "           ,classunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                    "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "Eid " & _
                                   ",Salary " & _
                              "FROM " & _
                              "( " & _
                                   "SELECT " & _
                                         "employeeunkid AS Eid " & _
                                        ",ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                        ",newscale AS Salary " & _
                                   "FROM prsalaryincrement_tran " & _
                                   "WHERE incrementdate < = @SalDate AND isapproved = 1 AND isvoid = 0 " & _
                              ") AS A " & _
                              "WHERE   A.Rno = 1 " & _
                    "   ) AS B ON B.Eid = hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE hrassetdeclaration_master.isvoid = 0 AND hrassetdeclaration_master.isfinalsaved = 0 " & _
                         "AND CONVERT(CHAR(8),savedate,112) >= @AsOnDate "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncluderInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Shani(24-Aug-2015) -- End

            StrQ &= ") AS B WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= ") AS C WHERE 1 = 1 "
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing
            Dim iCnt As Integer = 1 : Dim StrGrpName As String = String.Empty
            Dim dtTable As DataTable
            If mstrReport_GroupName.Trim.Length > 0 Then
                dtTable = New DataView(dsList.Tables("DataTable"), "", "GName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("DataTable")
            End If

            For Each dtRow As DataRow In dtTable.Rows
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                rpt_Rows.Item("Column2") = dtRow.Item("EName")
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("App_Date").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = dtRow.Item("Job_Title")
                rpt_Rows.Item("Column5") = dtRow.Item("Department")
                rpt_Rows.Item("Column6") = dtRow.Item("Region")
                rpt_Rows.Item("Column7") = dtRow.Item("WorkStation")
                rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = iCnt.ToString
                rpt_Rows.Item("Column10") = dtRow.Item("GName")

                rpt_Rows.Item("Column82") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Salary)", "GName = '" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Rows.Item("Column83") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Salary)", "")), GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                iCnt += 1
            Next

            objRpt = New ArutiReport.Designer.rptNotSubmittedAD

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'All testing after Commneted this code also set Company logo....
            'If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
            '    rpt_Data.Tables("ArutiTable").Rows.Add("")
            'End If
            'Shani(24-Aug-2015) -- End

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 300, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 301, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 302, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 303, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            If mblnShowSalary = False Then
                ReportFunction.EnableSuppress(objRpt, "txtSalAmt", True)
                ReportFunction.EnableSuppress(objRpt, "Column81", True)
                ReportFunction.EnableSuppress(objRpt, "Column821", True)
                ReportFunction.EnableSuppress(objRpt, "txtTotalAmt1", True)
                ReportFunction.EnableSuppress(objRpt, "txtTotal", True)
                ReportFunction.EnableSuppress(objRpt, "txtGrpTotal", True) 'Sohail (07 Feb 2013)
            End If

            ReportFunction.TextChange(objRpt, "txtSN", Language.getMessage(mstrModuleName, 304, "Sno."))
            ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 200, "Employee Code"))
            ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 201, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtAdate", Language.getMessage(mstrModuleName, 202, "Appointed Date"))
            ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 203, "Department"))
            ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 204, "Job Title"))
            ReportFunction.TextChange(objRpt, "txtClassGrp", Language.getMessage(mstrModuleName, 205, "Region"))
            ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 206, "WorkStation"))
            ReportFunction.TextChange(objRpt, "txtSalAmt", Language.getMessage(mstrModuleName, 207, "Salary"))

            ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 307, "Group Count : "))
            ReportFunction.TextChange(objRpt, "txtGrandCount", Language.getMessage(mstrModuleName, 308, "Grand Count : "))
            ReportFunction.TextChange(objRpt, "txtGrpTotal", Language.getMessage(mstrModuleName, 309, "Group Total : "))
            ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 310, "Grand Total :"))
            ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                If IsDBNull(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column83")) = False Then
                    Call ReportFunction.TextChange(objRpt, "txtTotalAmt1", Format(CDec(rpt_Data.Tables("ArutiTable").Rows(0).Item("Column83")), GUI.fmtCurrency))
                End If
            End If

            ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 305, "Printed By :"))
            ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 306, "Printed Date :"))

            ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_WithoutAsset_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "As On Date :")
            Language.setMessage(mstrModuleName, 101, "Appointed Date From :")
            Language.setMessage(mstrModuleName, 102, "To :")
            Language.setMessage(mstrModuleName, 103, "Appointed Before Date :")
            Language.setMessage(mstrModuleName, 104, "Appointed After Date :")
            Language.setMessage(mstrModuleName, 105, "Declared Amount From :")
            Language.setMessage(mstrModuleName, 106, "Liability Amount From :")
            Language.setMessage(mstrModuleName, 107, "Employee :")
            Language.setMessage(mstrModuleName, 108, "Order By :")
            Language.setMessage(mstrModuleName, 200, "Employee Code")
            Language.setMessage(mstrModuleName, 201, "Employee Name")
            Language.setMessage(mstrModuleName, 202, "Appointed Date")
            Language.setMessage(mstrModuleName, 203, "Department")
            Language.setMessage(mstrModuleName, 204, "Job Title")
            Language.setMessage(mstrModuleName, 205, "Region")
            Language.setMessage(mstrModuleName, 206, "WorkStation")
            Language.setMessage(mstrModuleName, 207, "Salary")
            Language.setMessage(mstrModuleName, 208, "Declared Amount")
            Language.setMessage(mstrModuleName, 209, "Liability Amount")
            Language.setMessage(mstrModuleName, 300, "Prepared By :")
            Language.setMessage(mstrModuleName, 301, "Checked By :")
            Language.setMessage(mstrModuleName, 302, "Approved By :")
            Language.setMessage(mstrModuleName, 303, "Received By :")
            Language.setMessage(mstrModuleName, 304, "Sno.")
            Language.setMessage(mstrModuleName, 305, "Printed By :")
            Language.setMessage(mstrModuleName, 306, "Printed Date :")
            Language.setMessage(mstrModuleName, 307, "Group Count :")
            Language.setMessage(mstrModuleName, 308, "Grand Count :")
            Language.setMessage(mstrModuleName, 309, "Group Total :")
            Language.setMessage(mstrModuleName, 310, "Grand Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
