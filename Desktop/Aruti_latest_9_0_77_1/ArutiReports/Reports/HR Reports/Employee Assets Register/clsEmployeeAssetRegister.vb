'************************************************************************************************************************************
'Class Name : clsEmployeeAssetRegister.vb
'Purpose    :
'Date       : 04/30/2011
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsEmployeeAssetRegister
    Inherits IReportData

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeAssetRegister"
    Private mstrReportId As String = enArutiReport.EmployeeAssetsRegister '51
    Dim objDataOperation As clsDataOperation

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Variables "
    Private mintEmployeeId As Integer
    Private mstrEmployeeName As String
    Private mintAssetsId As Integer
    Private mstrAssetsName As String
    Private mintAssetsStatus As Integer
    Private mstrAssetsStatus As String
    Private mintAssetsConditionId As Integer
    Private mstrAssetsCondition As String
    Private mintReportId As Integer = 0
    Private mstrReportType As String


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 06 SEP 2011 ] -- START
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'S.SANDEEP [ 06 SEP 2011 ] -- END 


    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Anjan (17 May 2012)-End 

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Property "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    Public WriteOnly Property _AssetsId() As Integer
        Set(ByVal value As Integer)
            mintAssetsId = value
        End Set
    End Property

    Public WriteOnly Property _AssetsName() As String
        Set(ByVal value As String)
            mstrAssetsName = value
        End Set
    End Property
    Public WriteOnly Property _AssetsCondition_Id() As Integer
        Set(ByVal value As Integer)
            mintAssetsConditionId = value
        End Set
    End Property
    Public WriteOnly Property _AssetsConditionName() As String
        Set(ByVal value As String)
            mstrAssetsCondition = value
        End Set
    End Property

    Public WriteOnly Property _AssetsStatus() As String
        Set(ByVal value As String)
            mstrAssetsName = value
        End Set
    End Property

    Public WriteOnly Property _AssetsStatusId() As Integer
        Set(ByVal value As Integer)
            mintAssetsStatus = value
        End Set
    End Property
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property
    Public WriteOnly Property _ReportType() As String
        Set(ByVal value As String)
            mstrReportType = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 06 SEP 2011 ] -- START
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property
    'S.SANDEEP [ 06 SEP 2011 ] -- END 

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Anjan (17 May 2012)-End 


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'Pinkal (14-Aug-2012) -- End


    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()

        mintEmployeeId = 0
        mstrEmployeeName = ""
        mintAssetsId = 0
        mstrAssetsName = ""
        mintAssetsStatus = 0
        mstrAssetsStatus = ""
        mintAssetsConditionId = 0
        mstrAssetsCondition = ""


        'Pinkal (24-Jun-2011) -- Start
        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
        mblnIsActive = True
        'Pinkal (24-Jun-2011) -- End

        'S.SANDEEP [ 06 SEP 2011 ] -- START
        mintViewIndex = -1
        mstrViewByIds = ""
        mstrViewByName = ""
        'S.SANDEEP [ 06 SEP 2011 ] -- END 

        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        mstrAdvance_Filter = ""
        'S.SANDEEP [ 13 FEB 2013 ] -- END

        'Shani(15-Feb-2016) -- Start
        'Report not showing Data in ESS due to Access Level Filter
        mblnIncludeAccessFilterQry = True
        'Shani(15-Feb-2016) -- End

    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Assigned"))
            objDataOperation.AddParameter("@Returned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Returned"))
            objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Written Off"))
            objDataOperation.AddParameter("@Lost", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Lost"))
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintAssetsConditionId > 0 Then
                objDataOperation.AddParameter("@AssetsCondition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetsConditionId)
                Me._FilterQuery &= " AND condition.masterunkid = @AssetsCondition "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Condition : ") & " " & mstrAssetsCondition & " "
            End If

            If mintAssetsId > 0 Then
                objDataOperation.AddParameter("@AssetsId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetsId)
                Me._FilterQuery &= " AND assets.masterunkid = @AssetsId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Assets :") & " " & mstrAssetsName & " "
            End If

            If mintAssetsStatus > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetsStatus)
                Me._FilterQuery &= " AND hremployee_assets_tran.asset_statusunkid = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Status : ") & " " & mstrAssetsStatus & " "
            End If

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'If mstrViewByName.Length > 0 Then
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Analysis By :") & " " & mstrViewByName & " "
            'End If
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            If mintViewIndex <= 0 Then
                If Me.OrderByQuery <> "" Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Order By :") & " " & Me.OrderByDisplay & " "
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If


        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()

        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("empcode", Language.getMessage(mstrModuleName, 13, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("empname", Language.getMessage(mstrModuleName, 14, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("assets", Language.getMessage(mstrModuleName, 15, "Assets")))

            iColumn_DetailReport.Add(New IColumn("assetno", Language.getMessage(mstrModuleName, 16, "Assets No.")))
            iColumn_DetailReport.Add(New IColumn("condition", Language.getMessage(mstrModuleName, 17, "Condition")))
            iColumn_DetailReport.Add(New IColumn("status", Language.getMessage(mstrModuleName, 18, "Status")))
            iColumn_DetailReport.Add(New IColumn("serialno", Language.getMessage(mstrModuleName, 19, "Serial No.")))
            iColumn_DetailReport.Add(New IColumn("returndate", Language.getMessage(mstrModuleName, 20, "Assign Date")))
            iColumn_DetailReport.Add(New IColumn("remark", Language.getMessage(mstrModuleName, 21, "Remark")))



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim objCommonMaster As New clsCommon_Master
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation



            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            ''S.SANDEEP [ 16 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            ''StrQ = "SELECT "
            ''Select Case mintViewIndex
            ''    Case enAnalysisReport.Branch
            ''        StrQ &= "stationunkid , name AS GName " & _
            ''                        ",ISNULL(empcode,'') AS empcode " & _
            ''                        ",ISNULL(empname,'') AS empname " & _
            ''                        ",ISNULL(assets,'') AS assets " & _
            ''                        ",ISNULL(assetno,'') AS assetno " & _
            ''                        ",ISNULL(condition,'') AS condition " & _
            ''                        ",ISNULL(status,'') AS status " & _
            ''                        ",ISNULL(serialno,'') AS serialno " & _
            ''                        ",ISNULL(returndate,'') AS returndate " & _
            ''                        ",ISNULL(remark,'') AS remark " & _
            ''                        " FROM hrstation_master " & _
            ''                        " LEFT JOIN " & _
            ''                        " ( "
            ''        blnFlag = True
            ''    Case enAnalysisReport.Department
            ''        StrQ &= "stationunkid , name AS GName " & _
            ''                        ",ISNULL(empcode,'') AS empcode " & _
            ''                        ",ISNULL(empname,'') AS empname " & _
            ''                        ",ISNULL(assets,'') AS assets " & _
            ''                        ",ISNULL(assetno,'') AS assetno " & _
            ''                        ",ISNULL(condition,'') AS condition " & _
            ''                        ",ISNULL(status,'') AS status " & _
            ''                        ",ISNULL(serialno,'') AS serialno " & _
            ''                        ",ISNULL(returndate,'') AS returndate " & _
            ''                        ",ISNULL(remark,'') AS remark " & _
            ''                        " FROM hrdepartment_master " & _
            ''                        " LEFT JOIN " & _
            ''                        " ( "
            ''        blnFlag = True
            ''    Case enAnalysisReport.Section
            ''        StrQ &= " sectionunkid , name AS GName " & _
            ''                        ",ISNULL(empcode,'') AS empcode " & _
            ''                        ",ISNULL(empname,'') AS empname " & _
            ''                        ",ISNULL(assets,'') AS assets " & _
            ''                        ",ISNULL(assetno,'') AS assetno " & _
            ''                        ",ISNULL(condition,'') AS condition " & _
            ''                        ",ISNULL(status,'') AS status " & _
            ''                        ",ISNULL(serialno,'') AS serialno " & _
            ''                        ",ISNULL(returndate,'') AS returndate " & _
            ''                        ",ISNULL(remark,'') AS remark " & _
            ''                        " FROM hrsection_master " & _
            ''                        " LEFT JOIN " & _
            ''                        " ( "
            ''        blnFlag = True
            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " unitunkid , name AS GName " & _
            ''                        ",ISNULL(empcode,'') AS empcode " & _
            ''                        ",ISNULL(empname,'') AS empname " & _
            ''                        ",ISNULL(assets,'') AS assets " & _
            ''                        ",ISNULL(assetno,'') AS assetno " & _
            ''                        ",ISNULL(condition,'') AS condition " & _
            ''                        ",ISNULL(status,'') AS status " & _
            ''                        ",ISNULL(serialno,'') AS serialno " & _
            ''                        ",ISNULL(returndate,'') AS returndate " & _
            ''                        ",ISNULL(remark,'') AS remark " & _
            ''                        " FROM hrunit_master " & _
            ''                        " LEFT JOIN " & _
            ''                        " ( "
            ''        blnFlag = True
            ''    Case enAnalysisReport.Job
            ''        StrQ &= " jobunkid , job_name AS GName " & _
            ''                        ",ISNULL(empcode,'') AS empcode " & _
            ''                        ",ISNULL(empname,'') AS empname " & _
            ''                        ",ISNULL(assets,'') AS assets " & _
            ''                        ",ISNULL(assetno,'') AS assetno " & _
            ''                        ",ISNULL(condition,'') AS condition " & _
            ''                        ",ISNULL(status,'') AS status " & _
            ''                        ",ISNULL(serialno,'') AS serialno " & _
            ''                        ",ISNULL(returndate,'') AS returndate " & _
            ''                        ",ISNULL(remark,'') AS remark " & _
            ''                        " FROM hrjob_master " & _
            ''                        " LEFT JOIN " & _
            ''                        " ( "
            ''        blnFlag = True
            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " costcenterunkid  , costcentername AS GName " & _
            ''                        ",ISNULL(empcode,'') AS empcode " & _
            ''                        ",ISNULL(empname,'') AS empname " & _
            ''                        ",ISNULL(assets,'') AS assets " & _
            ''                        ",ISNULL(assetno,'') AS assetno " & _
            ''                        ",ISNULL(condition,'') AS condition " & _
            ''                        ",ISNULL(status,'') AS status " & _
            ''                        ",ISNULL(serialno,'') AS serialno " & _
            ''                        ",ISNULL(returndate,'') AS returndate " & _
            ''                        ",ISNULL(remark,'') AS remark " & _
            ''                        " FROM prcostcenter_master " & _
            ''                        " LEFT JOIN " & _
            ''                        " ( "
            ''        blnFlag = True
            ''    Case Else


            ''        'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''        'ENHANCEMENT : TRA CHANGES
            ''        ' StrQ &= "   ' ' AS GName " & _
            ''        '                "  ,hremployee_master.employeecode as empcode " & _
            ''        '       ",hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            ''        '       ",assets.name AS assets " & _
            ''        '       ",asset_no AS assetno " & _
            ''        '       ",condition.name AS condition " & _
            ''        '       ",status.name AS status " & _
            ''        '       ",assetserial_no AS serialno " & _
            ''        '       ",CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            ''        '       ",hremployee_assets_tran.remark AS remark " & _
            ''        '       ",hremployee_master.employeeunkid as empid " & _
            ''        '       ",condition.masterunkid as AssetsConditionId " & _
            ''        '       ",assets.masterunkid as assetsId " & _
            ''        '       ",status.masterunkid as statusId " & _
            ''        '"FROM hremployee_assets_tran " & _
            ''        '"JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''        '"JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            ''        '"JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            ''        '"JOIN cfcommon_master AS status ON status.masterunkid = hremployee_assets_tran.asset_statusunkid AND status.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_STATUS & " " & _
            ''        '"WHERE ISNULL(hremployee_assets_tran.isvoid,0)=0 "

            '' StrQ &= "   ' ' AS GName " & _
            ''                "  ,hremployee_master.employeecode as empcode " & _
            ''       ",hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            ''       ",assets.name AS assets " & _
            ''       ",asset_no AS assetno " & _
            ''       ",condition.name AS condition " & _
            ''                "  ,CASE WHEN hremployee_assets_tran.asset_statusunkid = 1 THEN @Assigned " & _
            ''                "        WHEN hremployee_assets_tran.asset_statusunkid = 2 THEN @Returned " & _
            ''                "        WHEN hremployee_assets_tran.asset_statusunkid = 3 THEN @WrittenOff " & _
            ''                "        WHEN hremployee_assets_tran.asset_statusunkid = 4 THEN @Lost " & _
            ''                "   END AS status " & _
            ''       ",assetserial_no AS serialno " & _
            ''       ",CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            ''       ",hremployee_assets_tran.remark AS remark " & _
            ''       ",hremployee_master.employeeunkid as empid " & _
            ''       ",condition.masterunkid as AssetsConditionId " & _
            ''       ",assets.masterunkid as assetsId " & _
            ''                "  ,hremployee_assets_tran.asset_statusunkid statusId " & _
            ''"FROM hremployee_assets_tran " & _
            ''"JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''"JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            ''"JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            ''"WHERE ISNULL(hremployee_assets_tran.isvoid,0)=0 "
            ''        'S.SANDEEP [ 12 MAY 2012 ] -- END




            ''        If mblnIsActive = False Then
            ''            'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''            'ISSUE : TRA ENHANCEMENTS
            ''            'StrQ &= " AND hremployee_master.isactive = 1 "
            ''            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            ''                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            ''            'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''        End If

            ''        'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''        'ENHANCEMENT : TRA CHANGES
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            StrQ &= UserAccessLevel._AccessLevelFilterString
            ''        End If
            ''        'S.SANDEEP [ 12 MAY 2012 ] -- END

            ''        Call FilterTitleAndFilterQuery()

            ''        StrQ &= Me._FilterQuery

            ''End Select

            ''If blnFlag = True Then


            ''    'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''    'ENHANCEMENT : TRA CHANGES
            ''    'StrQ &= " SELECT " & _
            ''    '                "  hremployee_master.employeecode as empcode " & _
            ''    '                ", hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            ''    '                ", assets.name AS assets " & _
            ''    '                ", asset_no AS assetno " & _
            ''    '                ", condition.name AS condition " & _
            ''    '                ", status.name AS status " & _
            ''    '                ", assetserial_no AS serialno " & _
            ''    '                ", CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            ''    '                ", hremployee_assets_tran.remark AS remark " & _
            ''    '                ", hremployee_master.employeeunkid as empid " & _
            ''    '                ", condition.masterunkid as AssetsConditionId " & _
            ''    '                ", assets.masterunkid as assetsId " & _
            ''    '                ", status.masterunkid as statusId "

            ''StrQ &= " SELECT " & _
            ''                "  hremployee_master.employeecode as empcode " & _
            ''                ", hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            ''                ", assets.name AS assets " & _
            ''                ", asset_no AS assetno " & _
            ''                ", condition.name AS condition " & _
            ''                    ", CASE WHEN hremployee_assets_tran.asset_statusunkid = 1 THEN @Assigned " & _
            ''                    "       WHEN hremployee_assets_tran.asset_statusunkid = 2 THEN @Returned " & _
            ''                    "       WHEN hremployee_assets_tran.asset_statusunkid = 3 THEN @WrittenOff " & _
            ''                    "       WHEN hremployee_assets_tran.asset_statusunkid = 4 THEN @Lost " & _
            ''                    "  END AS status " & _
            ''                ", assetserial_no AS serialno " & _
            ''                ", CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            ''                ", hremployee_assets_tran.remark AS remark " & _
            ''                ", hremployee_master.employeeunkid as empid " & _
            ''                ", condition.masterunkid as AssetsConditionId " & _
            ''                ", assets.masterunkid as assetsId " & _
            ''                    ", hremployee_assets_tran.asset_statusunkid statusId "
            ''    'S.SANDEEP [ 12 MAY 2012 ] -- END



            ''    Select Case mintViewIndex
            ''        Case enAnalysisReport.Branch
            ''            StrQ &= ", hremployee_master.stationunkid AS BranchId "
            ''        Case enAnalysisReport.Department
            ''            StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            ''        Case enAnalysisReport.Section
            ''            StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            ''        Case enAnalysisReport.Unit
            ''            StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            ''        Case enAnalysisReport.Job
            ''            StrQ &= ", hremployee_master.jobunkid AS JobId  "
            ''        Case enAnalysisReport.CostCenter
            ''            StrQ &= ", hremployee_master.costcenterunkid As CCId "
            ''    End Select

            ''    'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''    'ENHANCEMENT : TRA CHANGES
            ''    'StrQ &= "FROM hremployee_assets_tran " & _
            ''    '        "  JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''    '        "  JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            ''    '        "  JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            ''    '        "  JOIN cfcommon_master AS status ON status.masterunkid = hremployee_assets_tran.asset_statusunkid AND status.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_STATUS & " " & _
            ''    '        "WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "

            ''    StrQ &= "FROM hremployee_assets_tran " & _
            ''                    "  JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''                    "  JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            ''                    "  JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            ''                    "WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "
            ''    'S.SANDEEP [ 12 MAY 2012 ] -- END

            ''    If mblnIsActive = False Then
            ''        'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''        'ISSUE : TRA ENHANCEMENTS
            ''        'StrQ &= " AND hremployee_master.isactive = 1 "
            ''        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            ''                " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            ''        'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''    End If

            ''    'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''    'ENHANCEMENT : TRA CHANGES
            ''    If UserAccessLevel._AccessLevel.Length > 0 Then
            ''        StrQ &= UserAccessLevel._AccessLevelFilterString
            ''    End If
            ''    'S.SANDEEP [ 12 MAY 2012 ] -- END

            ''    Call FilterTitleAndFilterQuery()

            ''    Select Case mintViewIndex
            ''        Case enAnalysisReport.Branch
            ''            StrQ &= " )AS ASR ON hrstation_master.stationunkid = ASR.BranchId " & _
            ''                            " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            ''        Case enAnalysisReport.Department
            ''            StrQ &= " )AS ASR ON hrdepartment_master.departmentunkid = ASR.DeptId " & _
            ''                            " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            ''        Case enAnalysisReport.Section
            ''            StrQ &= " )AS ASR ON hrsection_master.sectionunkid = ASR.SecId " & _
            ''                            " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            ''        Case enAnalysisReport.Unit
            ''            StrQ &= " )AS ASR ON hrunit_master.unitunkid = ASR.UnitId " & _
            ''                            " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            ''        Case enAnalysisReport.Job
            ''            StrQ &= " )AS ASR ON hrjob_master.jobunkid = ASR.JobId " & _
            ''                             " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            ''        Case enAnalysisReport.CostCenter
            ''            StrQ &= " ) AS ASR ON prcostcenter_master.costcenterunkid = ASR.CCId " & _
            ''                            " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            ''    End Select

            ''    StrQ &= "ORDER BY " & Me.OrderByQuery

            ''End If

            ' ''StrQ = " SELECT " & _
            ' ''               " hremployee_master.employeecode as empcode " & _
            ' ''               ",hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            ' ''               ",assets.name AS assets " & _
            ' ''               ",asset_no AS assetno " & _
            ' ''               ",condition.name AS condition " & _
            ' ''               ",status.name AS status " & _
            ' ''               ",assetserial_no AS serialno " & _
            ' ''               ",assign_return_date AS returndate " & _
            ' ''               ",hremployee_assets_tran.remark AS remark " & _
            ' ''               ",hremployee_master.employeeunkid as empid " & _
            ' ''               ",condition.masterunkid as AssetsConditionId " & _
            ' ''               ",assets.masterunkid as assetsId " & _
            ' ''               ",status.masterunkid as statusId " & _
            ' ''        "FROM hremployee_assets_tran " & _
            ' ''        "JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ' ''        "JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            ' ''        "JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            ' ''        "JOIN cfcommon_master AS status ON status.masterunkid = hremployee_assets_tran.asset_statusunkid AND status.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_STATUS & " " & _
            ' ''        "WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "


            '' ''Pinkal (24-Jun-2011) -- Start
            '' ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            ' ''If mblnIsActive = False Then
            ' ''    StrQ &= " AND hremployee_master.isactive = 1 "
            ' ''End If
            '' ''Pinkal (24-Jun-2011) -- End


            ' ''Call FilterTitleAndFilterQuery()

            ' ''StrQ &= Me._FilterQuery

            'StrQ = "SELECT " & _
            '       "  hremployee_master.employeecode as empcode "
            ''S.SANDEEP [ 26 MAR 2014 ] -- START
            ''", hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            'If mblnFirstNamethenSurname = False Then
            '    StrQ &= ", ISNULL(surname, '') + ' ' " & _
            '                "+ ISNULL(firstname, '') + ' ' " & _
            '                "+ ISNULL(othername, '') AS empname "
            'Else
            '    StrQ &= ", ISNULL(firstname, '') + ' ' " & _
            '                "+ ISNULL(othername, '') + ' ' " & _
            '                "+ ISNULL(surname, '') AS empname "
            'End If
            ''S.SANDEEP [ 26 MAR 2014 ] -- END

            'StrQ &= ", assets.name AS assets " & _
            '                    ", asset_no AS assetno " & _
            '                    ", condition.name AS condition " & _
            '                    ", CASE WHEN hremployee_assets_tran.asset_statusunkid = 1 THEN @Assigned " & _
            '                    "       WHEN hremployee_assets_tran.asset_statusunkid = 2 THEN @Returned " & _
            '                    "       WHEN hremployee_assets_tran.asset_statusunkid = 3 THEN @WrittenOff " & _
            '                    "       WHEN hremployee_assets_tran.asset_statusunkid = 4 THEN @Lost " & _
            '                    "  END AS status " & _
            '                    ", assetserial_no AS serialno " & _
            '                    ", CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            '                    ", hremployee_assets_tran.remark AS remark " & _
            '                    ", hremployee_master.employeeunkid as empid " & _
            '                    ", condition.masterunkid as AssetsConditionId " & _
            '                    ", assets.masterunkid as assetsId " & _
            '                    ", hremployee_assets_tran.asset_statusunkid statusId "
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            'StrQ &= "FROM hremployee_assets_tran " & _
            '                "  JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                "  JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            '    "  JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION
            'StrQ &= mstrAnalysis_Join
            'StrQ &= "WHERE ISNULL(hremployee_assets_tran.isvoid,0)=0 "


            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END


            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End


            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'StrQ = "SELECT "
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= "stationunkid , name AS GName " & _
            '                        ",ISNULL(empcode,'') AS empcode " & _
            '                        ",ISNULL(empname,'') AS empname " & _
            '                        ",ISNULL(assets,'') AS assets " & _
            '                        ",ISNULL(assetno,'') AS assetno " & _
            '                        ",ISNULL(condition,'') AS condition " & _
            '                        ",ISNULL(status,'') AS status " & _
            '                        ",ISNULL(serialno,'') AS serialno " & _
            '                        ",ISNULL(returndate,'') AS returndate " & _
            '                        ",ISNULL(remark,'') AS remark " & _
            '                        " FROM hrstation_master " & _
            '                        " LEFT JOIN " & _
            '                        " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Department
            '        StrQ &= "stationunkid , name AS GName " & _
            '                        ",ISNULL(empcode,'') AS empcode " & _
            '                        ",ISNULL(empname,'') AS empname " & _
            '                        ",ISNULL(assets,'') AS assets " & _
            '                        ",ISNULL(assetno,'') AS assetno " & _
            '                        ",ISNULL(condition,'') AS condition " & _
            '                        ",ISNULL(status,'') AS status " & _
            '                        ",ISNULL(serialno,'') AS serialno " & _
            '                        ",ISNULL(returndate,'') AS returndate " & _
            '                        ",ISNULL(remark,'') AS remark " & _
            '                        " FROM hrdepartment_master " & _
            '                        " LEFT JOIN " & _
            '                        " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Section
            '        StrQ &= " sectionunkid , name AS GName " & _
            '                        ",ISNULL(empcode,'') AS empcode " & _
            '                        ",ISNULL(empname,'') AS empname " & _
            '                        ",ISNULL(assets,'') AS assets " & _
            '                        ",ISNULL(assetno,'') AS assetno " & _
            '                        ",ISNULL(condition,'') AS condition " & _
            '                        ",ISNULL(status,'') AS status " & _
            '                        ",ISNULL(serialno,'') AS serialno " & _
            '                        ",ISNULL(returndate,'') AS returndate " & _
            '                        ",ISNULL(remark,'') AS remark " & _
            '                        " FROM hrsection_master " & _
            '                        " LEFT JOIN " & _
            '                        " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Unit
            '        StrQ &= " unitunkid , name AS GName " & _
            '                        ",ISNULL(empcode,'') AS empcode " & _
            '                        ",ISNULL(empname,'') AS empname " & _
            '                        ",ISNULL(assets,'') AS assets " & _
            '                        ",ISNULL(assetno,'') AS assetno " & _
            '                        ",ISNULL(condition,'') AS condition " & _
            '                        ",ISNULL(status,'') AS status " & _
            '                        ",ISNULL(serialno,'') AS serialno " & _
            '                        ",ISNULL(returndate,'') AS returndate " & _
            '                        ",ISNULL(remark,'') AS remark " & _
            '                        " FROM hrunit_master " & _
            '                        " LEFT JOIN " & _
            '                        " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Job
            '        StrQ &= " jobunkid , job_name AS GName " & _
            '                        ",ISNULL(empcode,'') AS empcode " & _
            '                        ",ISNULL(empname,'') AS empname " & _
            '                        ",ISNULL(assets,'') AS assets " & _
            '                        ",ISNULL(assetno,'') AS assetno " & _
            '                        ",ISNULL(condition,'') AS condition " & _
            '                        ",ISNULL(status,'') AS status " & _
            '                        ",ISNULL(serialno,'') AS serialno " & _
            '                        ",ISNULL(returndate,'') AS returndate " & _
            '                        ",ISNULL(remark,'') AS remark " & _
            '                        " FROM hrjob_master " & _
            '                        " LEFT JOIN " & _
            '                        " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " costcenterunkid  , costcentername AS GName " & _
            '                        ",ISNULL(empcode,'') AS empcode " & _
            '                        ",ISNULL(empname,'') AS empname " & _
            '                        ",ISNULL(assets,'') AS assets " & _
            '                        ",ISNULL(assetno,'') AS assetno " & _
            '                        ",ISNULL(condition,'') AS condition " & _
            '                        ",ISNULL(status,'') AS status " & _
            '                        ",ISNULL(serialno,'') AS serialno " & _
            '                        ",ISNULL(returndate,'') AS returndate " & _
            '                        ",ISNULL(remark,'') AS remark " & _
            '                        " FROM prcostcenter_master " & _
            '                        " LEFT JOIN " & _
            '                        " ( "
            '        blnFlag = True
            '    Case Else


            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        ' StrQ &= "   ' ' AS GName " & _
            '        '                "  ,hremployee_master.employeecode as empcode " & _
            '        '       ",hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            '        '       ",assets.name AS assets " & _
            '        '       ",asset_no AS assetno " & _
            '        '       ",condition.name AS condition " & _
            '        '       ",status.name AS status " & _
            '        '       ",assetserial_no AS serialno " & _
            '        '       ",CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            '        '       ",hremployee_assets_tran.remark AS remark " & _
            '        '       ",hremployee_master.employeeunkid as empid " & _
            '        '       ",condition.masterunkid as AssetsConditionId " & _
            '        '       ",assets.masterunkid as assetsId " & _
            '        '       ",status.masterunkid as statusId " & _
            '        '"FROM hremployee_assets_tran " & _
            '        '"JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        '"JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            '        '"JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            '        '"JOIN cfcommon_master AS status ON status.masterunkid = hremployee_assets_tran.asset_statusunkid AND status.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_STATUS & " " & _
            '        '"WHERE ISNULL(hremployee_assets_tran.isvoid,0)=0 "

            ' StrQ &= "   ' ' AS GName " & _
            '                "  ,hremployee_master.employeecode as empcode " & _
            '       ",hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            '       ",assets.name AS assets " & _
            '       ",asset_no AS assetno " & _
            '       ",condition.name AS condition " & _
            '                "  ,CASE WHEN hremployee_assets_tran.asset_statusunkid = 1 THEN @Assigned " & _
            '                "        WHEN hremployee_assets_tran.asset_statusunkid = 2 THEN @Returned " & _
            '                "        WHEN hremployee_assets_tran.asset_statusunkid = 3 THEN @WrittenOff " & _
            '                "        WHEN hremployee_assets_tran.asset_statusunkid = 4 THEN @Lost " & _
            '                "   END AS status " & _
            '       ",assetserial_no AS serialno " & _
            '       ",CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            '       ",hremployee_assets_tran.remark AS remark " & _
            '       ",hremployee_master.employeeunkid as empid " & _
            '       ",condition.masterunkid as AssetsConditionId " & _
            '       ",assets.masterunkid as assetsId " & _
            '                "  ,hremployee_assets_tran.asset_statusunkid statusId " & _
            '"FROM hremployee_assets_tran " & _
            '"JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '"JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            '"JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            '"WHERE ISNULL(hremployee_assets_tran.isvoid,0)=0 "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END




            '        If mblnIsActive = False Then
            '            'S.SANDEEP [ 12 MAY 2012 ] -- START
            '            'ISSUE : TRA ENHANCEMENTS
            '            'StrQ &= " AND hremployee_master.isactive = 1 "
            '            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '            'S.SANDEEP [ 12 MAY 2012 ] -- END
            '        End If

            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= UserAccessLevel._AccessLevelFilterString
            '        End If
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END

            '        Call FilterTitleAndFilterQuery()

            '        StrQ &= Me._FilterQuery

            'End Select

            'If blnFlag = True Then


            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'StrQ &= " SELECT " & _
            '    '                "  hremployee_master.employeecode as empcode " & _
            '    '                ", hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            '    '                ", assets.name AS assets " & _
            '    '                ", asset_no AS assetno " & _
            '    '                ", condition.name AS condition " & _
            '    '                ", status.name AS status " & _
            '    '                ", assetserial_no AS serialno " & _
            '    '                ", CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            '    '                ", hremployee_assets_tran.remark AS remark " & _
            '    '                ", hremployee_master.employeeunkid as empid " & _
            '    '                ", condition.masterunkid as AssetsConditionId " & _
            '    '                ", assets.masterunkid as assetsId " & _
            '    '                ", status.masterunkid as statusId "

            'StrQ &= " SELECT " & _
            '                "  hremployee_master.employeecode as empcode " & _
            '                ", hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            '                ", assets.name AS assets " & _
            '                ", asset_no AS assetno " & _
            '                ", condition.name AS condition " & _
            '                    ", CASE WHEN hremployee_assets_tran.asset_statusunkid = 1 THEN @Assigned " & _
            '                    "       WHEN hremployee_assets_tran.asset_statusunkid = 2 THEN @Returned " & _
            '                    "       WHEN hremployee_assets_tran.asset_statusunkid = 3 THEN @WrittenOff " & _
            '                    "       WHEN hremployee_assets_tran.asset_statusunkid = 4 THEN @Lost " & _
            '                    "  END AS status " & _
            '                ", assetserial_no AS serialno " & _
            '                ", CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
            '                ", hremployee_assets_tran.remark AS remark " & _
            '                ", hremployee_master.employeeunkid as empid " & _
            '                ", condition.masterunkid as AssetsConditionId " & _
            '                ", assets.masterunkid as assetsId " & _
            '                    ", hremployee_assets_tran.asset_statusunkid statusId "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END



            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= ", hremployee_master.stationunkid AS BranchId "
            '        Case enAnalysisReport.Department
            '            StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            '        Case enAnalysisReport.Section
            '            StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            '        Case enAnalysisReport.Unit
            '            StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            '        Case enAnalysisReport.Job
            '            StrQ &= ", hremployee_master.jobunkid AS JobId  "
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= ", hremployee_master.costcenterunkid As CCId "
            '    End Select

            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'StrQ &= "FROM hremployee_assets_tran " & _
            '    '        "  JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    '        "  JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            '    '        "  JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            '    '        "  JOIN cfcommon_master AS status ON status.masterunkid = hremployee_assets_tran.asset_statusunkid AND status.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_STATUS & " " & _
            '    '        "WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "

            '    StrQ &= "FROM hremployee_assets_tran " & _
            '                    "  JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "  JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            '                    "  JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            '                    "WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END

            '    If mblnIsActive = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= " AND hremployee_master.isactive = 1 "
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If

            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END

            '    Call FilterTitleAndFilterQuery()

            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= " )AS ASR ON hrstation_master.stationunkid = ASR.BranchId " & _
            '                            " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Department
            '            StrQ &= " )AS ASR ON hrdepartment_master.departmentunkid = ASR.DeptId " & _
            '                            " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Section
            '            StrQ &= " )AS ASR ON hrsection_master.sectionunkid = ASR.SecId " & _
            '                            " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " )AS ASR ON hrunit_master.unitunkid = ASR.UnitId " & _
            '                            " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Job
            '            StrQ &= " )AS ASR ON hrjob_master.jobunkid = ASR.JobId " & _
            '                             " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " ) AS ASR ON prcostcenter_master.costcenterunkid = ASR.CCId " & _
            '                            " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            '    End Select

            '    StrQ &= "ORDER BY " & Me.OrderByQuery

            'End If

            ''StrQ = " SELECT " & _
            ''               " hremployee_master.employeecode as empcode " & _
            ''               ",hremployee_master.firstname +' '+ hremployee_master.surname AS empname " & _
            ''               ",assets.name AS assets " & _
            ''               ",asset_no AS assetno " & _
            ''               ",condition.name AS condition " & _
            ''               ",status.name AS status " & _
            ''               ",assetserial_no AS serialno " & _
            ''               ",assign_return_date AS returndate " & _
            ''               ",hremployee_assets_tran.remark AS remark " & _
            ''               ",hremployee_master.employeeunkid as empid " & _
            ''               ",condition.masterunkid as AssetsConditionId " & _
            ''               ",assets.masterunkid as assetsId " & _
            ''               ",status.masterunkid as statusId " & _
            ''        "FROM hremployee_assets_tran " & _
            ''        "JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''        "JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
            ''        "JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " " & _
            ''        "JOIN cfcommon_master AS status ON status.masterunkid = hremployee_assets_tran.asset_statusunkid AND status.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_STATUS & " " & _
            ''        "WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "


            ' ''Pinkal (24-Jun-2011) -- Start
            ' ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            ''If mblnIsActive = False Then
            ''    StrQ &= " AND hremployee_master.isactive = 1 "
            ''End If
            ' ''Pinkal (24-Jun-2011) -- End


            ''Call FilterTitleAndFilterQuery()

            ''StrQ &= Me._FilterQuery

            StrQ = "SELECT " & _
                   "  hremployee_master.employeecode as empcode "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
            Else
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
            End If

            StrQ &= ", assets.name AS assets " & _
                    ", asset_no AS assetno " & _
                    ", condition.name AS condition " & _
                    ", CASE WHEN hremployee_assets_tran.asset_statusunkid = 1 THEN @Assigned " & _
                    "       WHEN hremployee_assets_tran.asset_statusunkid = 2 THEN @Returned " & _
                    "       WHEN hremployee_assets_tran.asset_statusunkid = 3 THEN @WrittenOff " & _
                    "       WHEN hremployee_assets_tran.asset_statusunkid = 4 THEN @Lost " & _
                    "  END AS status " & _
                    ", assetserial_no AS serialno " & _
                    ", CONVERT(CHAR(8),assign_return_date,112) AS returndate " & _
                    ", hremployee_assets_tran.remark AS remark " & _
                    ", hremployee_master.employeeunkid as empid " & _
                    ", condition.masterunkid as AssetsConditionId " & _
                    ", assets.masterunkid as assetsId " & _
                    ", hremployee_assets_tran.asset_statusunkid statusId "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_assets_tran " & _
                    "  JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= "  JOIN cfcommon_master AS assets ON assets.masterunkid = hremployee_assets_tran.assetunkid AND assets.mastertype = " & clsCommon_Master.enCommonMaster.ASSETS & " " & _
                    "  JOIN cfcommon_master AS condition ON condition.masterunkid = hremployee_assets_tran.conditionunkid AND condition.mastertype = " & clsCommon_Master.enCommonMaster.ASSET_CONDITION

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery
            'S.SANDEEP [ 16 MAY 2012 ] -- END



            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    If dtRow.Item("empcode").ToString.Trim.Length > 0 Then
                        Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Row.Item("Column1") = dtRow.Item("empcode")
                        rpt_Row.Item("Column2") = dtRow.Item("empname")
                        rpt_Row.Item("Column3") = dtRow.Item("assets")
                        rpt_Row.Item("Column4") = dtRow.Item("assetno")
                        rpt_Row.Item("Column5") = dtRow.Item("condition")
                        rpt_Row.Item("Column6") = dtRow.Item("status")
                        rpt_Row.Item("Column7") = dtRow.Item("serialno")
                        rpt_Row.Item("Column8") = eZeeDate.convertDate(dtRow.Item("returndate").ToString).ToShortDateString
                        rpt_Row.Item("Column9") = dtRow.Item("remark")
                        rpt_Row.Item("Column10") = dtRow.Item("GName")

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                    End If
                Next
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            If mintReportId = 0 Then
                objRpt = New ArutiReport.Designer.rptEmployeeAssetsRegister
            Else
                objRpt = New ArutiReport.Designer.rptAssetswiseRegister
            End If


            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow

            ReportFunction.Logo_Display(objRpt, _
                                       ConfigParameter._Object._IsDisplayLogo, _
                                       ConfigParameter._Object._ShowLogoRightSide, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "txtCompanyName", _
                                       "txtReportName", _
                                       "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 14, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 13, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtAssets", Language.getMessage(mstrModuleName, 15, "Assets"))
            Call ReportFunction.TextChange(objRpt, "txtAssetsNo", Language.getMessage(mstrModuleName, 16, "Assets No."))
            Call ReportFunction.TextChange(objRpt, "txtCondition", Language.getMessage(mstrModuleName, 17, "Condition"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 18, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtSerialNo", Language.getMessage(mstrModuleName, 19, "Serial No."))
            Call ReportFunction.TextChange(objRpt, "txtReturnDate", Language.getMessage(mstrModuleName, 20, "Assign Date"))
            Call ReportFunction.TextChange(objRpt, "txtRemark", Language.getMessage(mstrModuleName, 21, "Remark"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 22, "Total"))


            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGName", Language.getMessage(mstrModuleName, 121, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGName", Language.getMessage(mstrModuleName, 122, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGName", Language.getMessage(mstrModuleName, 123, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGName", Language.getMessage(mstrModuleName, 124, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGName", Language.getMessage(mstrModuleName, 125, "Job :"))
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGName", Language.getMessage(mstrModuleName, 126, "Cost Center :"))
            '    Case Else
            '        Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
            'End Select

            If mstrReport_GroupName.ToString.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtGName", mstrReport_GroupName)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
            End If
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 9, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 10, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 11, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 12, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 6, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 7, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 8, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Condition :")
            Language.setMessage(mstrModuleName, 3, "Assets :")
            Language.setMessage(mstrModuleName, 4, "Status :")
            Language.setMessage(mstrModuleName, 5, "Order By :")
            Language.setMessage(mstrModuleName, 6, "Printed By :")
            Language.setMessage(mstrModuleName, 7, "Printed Date :")
            Language.setMessage(mstrModuleName, 8, "Page :")
            Language.setMessage(mstrModuleName, 9, "Prepared By :")
            Language.setMessage(mstrModuleName, 10, "Checked By :")
            Language.setMessage(mstrModuleName, 11, "Approved By :")
            Language.setMessage(mstrModuleName, 12, "Received By :")
            Language.setMessage(mstrModuleName, 13, "Employee Code")
            Language.setMessage(mstrModuleName, 14, "Employee Name")
            Language.setMessage(mstrModuleName, 15, "Assets")
            Language.setMessage(mstrModuleName, 16, "Assets No.")
            Language.setMessage(mstrModuleName, 17, "Condition")
            Language.setMessage(mstrModuleName, 18, "Status")
            Language.setMessage(mstrModuleName, 19, "Serial No.")
            Language.setMessage(mstrModuleName, 20, "Assign Date")
            Language.setMessage(mstrModuleName, 21, "Remark")
            Language.setMessage(mstrModuleName, 22, "Total")
            Language.setMessage(mstrModuleName, 23, "Assigned")
            Language.setMessage(mstrModuleName, 24, "Returned")
            Language.setMessage(mstrModuleName, 25, "Written Off")
            Language.setMessage(mstrModuleName, 26, "Lost")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
