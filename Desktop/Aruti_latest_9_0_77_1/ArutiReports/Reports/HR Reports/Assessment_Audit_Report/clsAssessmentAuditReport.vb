'************************************************************************************************************************************
'Class Name : clsAssessmentAuditReport.vb
'Purpose    :
'Date       : 12-Apr-2019
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsAssessmentAuditReport
    Inherits IReportData

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_BSCPlanReport()
        Call Create_AssessReport()
    End Sub

#End Region

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsAssessmentAuditReport"
    Private mstrReportId As String = enArutiReport.Assessment_Audit_Report
    Private mdtFinalTable As DataTable = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintStatusId As Integer = -1
    Private mstrStatusName As String = String.Empty
    Private mintReportModeId As Integer = 0
    Private mstrReportModeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mdtTableExcel As DataTable
    Private menExportAction As enExportAction
    Dim objDataOperation As clsDataOperation
    Private mstrAdvanceFilter As String = String.Empty
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _ReportModeId() As Integer
        Set(ByVal value As Integer)
            mintReportModeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportModeName() As String
        Set(ByVal value As String)
            mstrReportModeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

#End Region

#Region " Public Function(s) & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mintStatusId = 0
            mstrStatusName = String.Empty
            mintReportModeId = 0
            mstrReportModeName = String.Empty
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mstrAdvanceFilter = String.Empty
            mintViewIndex = 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            If intReportType = 1 Then
                OrderByDisplay = iColumn_BSCPlanReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_BSCPlanReport.ColumnItem(0).Name
            ElseIf intReportType = 2 Then
                OrderByDisplay = iColumn_AssessReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_AssessReport.ColumnItem(0).Name
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_BSCPlanReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Me._FilterQuery = ""

        Try
            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@Period", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintStatusId > 0 AndAlso mintReportTypeId = 1 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                'S.SANDEEP |20-APR-2019| -- START

                Me._FilterQuery &= " AND ISNULL(eSt.statustypeid,999) = @StatusId "
                'S.SANDEEP |20-APR-2019| -- END
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Status : ") & " " & mstrStatusName & " "
            ElseIf mintStatusId >= 0 AndAlso mintReportTypeId = 2 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND ISNULL(SF.iscommitted, 0) = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Status : ") & " " & mstrStatusName & " "
            End If

            If Me.OrderByQuery <> "" Then
                'S.SANDEEP |20-APR-2019| -- END
                'If mintViewIndex > 0 Then
                '                        Me._FilterQuery &= " ORDER BY GName, " & Me.OrderByQuery
                '                    Else
                '                        Me._FilterQuery &= "ORDER BY GName,  " & Me.OrderByQuery
                '                    End If
                If mintReportTypeId = 1 Then
                    If mintViewIndex > 0 Then
                        Me._FilterQuery &= " ORDER BY eSt.Date,eSt.Time,GName, " & Me.OrderByQuery
                    Else
                        Me._FilterQuery &= "ORDER BY eSt.Date,eSt.Time,GName,  " & Me.OrderByQuery
                    End If
                Else
                    If mintViewIndex > 0 Then
                        Me._FilterQuery &= " ORDER BY GName, " & Me.OrderByQuery
                    Else
                        Me._FilterQuery &= "ORDER BY GName,  " & Me.OrderByQuery
                    End If
                End If
                'S.SANDEEP |20-APR-2019| -- END
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, " Order By : ") & " " & Me.OrderByDisplay
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_BSCPlanReport As New IColumnCollection
    Public Property Field_BSCPlanReport() As IColumnCollection
        Get
            Return iColumn_BSCPlanReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_BSCPlanReport = value
        End Set
    End Property

    Private Sub Create_BSCPlanReport()
        Try
            iColumn_BSCPlanReport.Clear()
            iColumn_BSCPlanReport.Add(New IColumn("Employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_BSCPlanReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_BSCPlanReport.Add(New IColumn("EmpJob", Language.getMessage(mstrModuleName, 3, "Job Name")))
            iColumn_BSCPlanReport.Add(New IColumn("EmpDept ", Language.getMessage(mstrModuleName, 4, "Department Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_BSCPlanReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Dim iColumn_AssessReport As New IColumnCollection
    Public Property Field_AssessReport() As IColumnCollection
        Get
            Return iColumn_AssessReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_AssessReport = value
        End Set
    End Property

    Private Sub Create_AssessReport()
        Try
            iColumn_AssessReport.Clear()
            iColumn_AssessReport.Add(New IColumn("Employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_AssessReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_AssessReport.Add(New IColumn("EmpJob", Language.getMessage(mstrModuleName, 3, "Job Name")))
            iColumn_AssessReport.Add(New IColumn("EmpDept ", Language.getMessage(mstrModuleName, 4, "Department Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_AssessReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Export_Assessment_Report(ByVal intReportType As Integer, _
                                             ByVal strDatabaseName As String, _
                                             ByVal intUserUnkid As Integer, _
                                             ByVal intYearUnkid As Integer, _
                                             ByVal intCompanyUnkid As Integer, _
                                             ByVal dtPeriodStart As Date, _
                                             ByVal dtPeriodEnd As Date, _
                                             ByVal strUserModeSetting As String, _
                                             ByVal blnOnlyApproved As Boolean, _
                                             ByVal strExportPath As String, _
                                             ByVal blnOpenAfterExport As Boolean, _
                                             Optional ByVal ExportAction As enExportAction = enExportAction.None _
                                             )
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        menExportAction = ExportAction

        Dim mdtTableExcel As New DataTable
        Dim dtCol As DataColumn

        dtCol = New DataColumn("Sr.No", System.Type.GetType("System.Int32"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 13, "Sr.No")
        dtCol.DefaultValue = 0
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("EmpCode", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("Employee", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Name")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("eStatus", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Status")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("EmpJob", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Job Name")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("EmpDept", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Department Name")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("User/Employee", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 5, "User")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("DateTime", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Date & Time")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            If mintReportTypeId = 1 Then
                '/*REPORT MODE : BSC Planning Audit Report */
                StrQ = "SELECT " & _
                       "hremployee_master.employeecode As empcode " & _
                      ",hremployee_master.firstname+' '+hremployee_master.surname AS employee " & _
                      ",eJb.job_name AS empjob " & _
                      ",eDP.name AS empdept " & _
                      ",ISNULL(eSt.BSC_Status,@NotPlanned) AS eStatus " & _
                      ",CASE WHEN eSt.statustypeid IN (1,4) THEN hremployee_master.displayname ELSE ISNULL(eSt.username,'') END AS [User/Employee] " & _
                      ",ISNULL(eSt.Date,'') AS [Date] " & _
                      ",ISNULL(eSt.Time,'') AS [Time] "
                If mintViewIndex > 0 Then
                    'S.SANDEEP |20-APR-2019| -- START
                    '", ISNULL(eSt.statustypeid, 0 ) AS Id, ISNULL(eSt.BSC_Status, @NotPlanned) AS GName "
                    StrQ &= ", ISNULL(eSt.statustypeid, 999) AS Id, ISNULL(eSt.BSC_Status, @NotPlanned) AS GName "
                    'S.SANDEEP |20-APR-2019| -- END
                Else
                    StrQ &= ",0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM hremployee_master  " & _
                    "LEFT JOIN " & _
                    "( " & _
                      "SELECT " & _
                           "employeeunkid " & _
                          ",jobunkid " & _
                          ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                      "FROM hremployee_categorization_tran " & _
                      "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                    ") AS eJ ON eJ.employeeunkid = hremployee_master.employeeunkid AND eJ.xno = 1 " & _
                    "LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                      "SELECT " & _
                           "employeeunkid " & _
                          ",departmentunkid " & _
                          ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                      "FROM hremployee_transfer_tran " & _
                      "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                    ") AS eD ON eD.employeeunkid = hremployee_master.employeeunkid AND eD.xno = 1 " & _
                    "LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                      "SELECT " & _
                         "hrassess_empstatus_tran.employeeunkid " & _
                        ",CASE WHEN statustypeid = '1' THEN @Submit_Approval " & _
                                    "WHEN statustypeid = '2' THEN @Final_Save " & _
                              "WHEN statustypeid = '3' THEN @Open_Changes " & _
                              "WHEN statustypeid = '4' THEN @Planned " & _
                              "WHEN statustypeid = '5' THEN @NOT_COMMITTED " & _
                              "WHEN statustypeid = '6' THEN @FINAL_COMMITTED " & _
                              "WHEN statustypeid = '7' THEN @PERIODIC_REVIEW " & _
                         "END AS BSC_Status " & _
                        ",CASE WHEN statustypeid = 3 AND hrassess_empstatus_tran.assessormasterunkid <=0 AND assessoremployeeunkid <= 0 THEN usr.username ELSE cfuser_master.username END AS username " & _
                        ",CONVERT(CHAR(8),status_date,112) AS [Date] " & _
                        ",CONVERT(CHAR(8),status_date,108) AS [Time] " & _
                        ",statustypeid " & _
                      "FROM hrassess_empstatus_tran " & _
                        "LEFT JOIN hrassessor_master ON hrassess_empstatus_tran.assessoremployeeunkid = hrassessor_master.assessormasterunkid " & _
                        "LEFT JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid and usertypeid = 2 " & _
                        "LEFT JOIN hrmsConfiguration..cfuser_master ON hrapprover_usermapping.userunkid = hrmsConfiguration.dbo.cfuser_master.userunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfuser_master AS usr ON hrassess_empstatus_tran.userunkid = usr.userunkid " & _
                      "WHERE periodunkid = @Period " & _
                    ") AS eSt ON eSt.employeeunkid = hremployee_master.employeeunkid "

                'S.SANDEEP |25-APR-2019| -- START
                '******************* REMOVED 
                'JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid

                '******************* ADDED
                'AND eD.xno = 1
                'LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid
                'S.SANDEEP |25-APR-2019| -- END

            ElseIf mintReportTypeId = 2 AndAlso mintReportModeId = 1 Then
                '/*REPORT MODE : SELF ASSESSMENT */

                'S.SANDEEP |20-APR-2019| -- START
                'StrQ = "SELECT " & _
                '   "hremployee_master.employeecode As empcode " & _
                '  ",hremployee_master.firstname+' '+hremployee_master.surname AS employee " & _
                '  ",eJb.job_name AS empjob " & _
                '  ",eDP.name AS empdept " & _
                '  ",ISNULL(SF.eStatus,@NotAssessd) AS eStatus " & _
                '  ",'' AS [User/Employee] " & _
                '  ",ISNULL(SF.Date,'') AS [Date] " & _
                '  ",ISNULL(ISNULL(SF.Time,AT.TIME),'') AS [Time] "

                StrQ = "SELECT " & _
                       "     hremployee_master.employeecode As empcode " & _
                       "    ,hremployee_master.firstname+' '+hremployee_master.surname AS employee " & _
                       "    ,eJb.job_name AS empjob " & _
                       "    ,eDP.name AS empdept " & _
                       "    ,ISNULL(SF.eStatus,@NotAssessd) AS eStatus " & _
                       "    ,hremployee_master.firstname+' '+hremployee_master.surname AS [User/Employee] " & _
                       "    ,ISNULL(SF.Date,'') AS [Date] " & _
                       "    ,ISNULL(CASE WHEN ISNULL(SF.Time,'00:00') = '00:00' THEN AT.TIME ELSE SF.Time END,'') [Time] "
                'S.SANDEEP |20-APR-2019| -- END
                'S.SANDEEP |25-APR-2019| -- START {ELSE SF.Time - ADDED} -- END

                If mintViewIndex > 0 Then
                    StrQ &= ", ISNULL(SF.iscommitted, 0 ) AS Id,  ISNULL(SF.eStatus, @NotAssessd) AS GName "
                Else
                    StrQ &= ",0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM hremployee_master " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT " & _
                       "employeeunkid " & _
                      ",jobunkid " & _
                      ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                  "FROM hremployee_categorization_tran " & _
                  "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                ") AS eJ ON eJ.employeeunkid = hremployee_master.employeeunkid AND eJ.xno = 1 " & _
                "LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT " & _
                       "employeeunkid " & _
                      ",departmentunkid " & _
                      ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                  "FROM hremployee_transfer_tran " & _
                  "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                ") AS eD ON eD.employeeunkid = hremployee_master.employeeunkid AND eD.xno = 1 " & _
                "LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT " & _
                     "EV.selfemployeeunkid " & _
                            ",CASE WHEN EV.iscommitted = 0 THEN @NotSubmitted ELSE @Submitted END AS eStatus " & _
                    ",CONVERT(NVARCHAR(8),ISNULL(EV.committeddatetime,EV.assessmentdate),112) AS [DATE] " & _
                    ",CONVERT(NVARCHAR(5),ISNULL(EV.committeddatetime,EV.assessmentdate),108) AS [TIME] " & _
                    ",EV.analysisunkid " & _
                    ",EV.iscommitted " & _
                  "FROM hrevaluation_analysis_master AS EV " & _
                  "WHERE EV.isvoid = 0 AND EV.periodunkid = @Period AND EV.assessmodeid = 1 " & _
                ") AS SF ON SF.selfemployeeunkid = hremployee_master.employeeunkid " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT DISTINCT " & _
                     "ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/analysisunkid)[1]', 'INT') AS AnalysisId " & _
                    ",CONVERT(NVARCHAR(5),ACT.auditdatetime,108) AS [TIME] " & _
                            ",ROW_NUMBER()OVER(PARTITION BY ACT.parentunkid ORDER BY ACT.auditdatetime) as xno " & _
                  "FROM atcommon_tranlog AS ACT " & _
                  "WHERE ACT.parenttable_name = 'hrevaluation_analysis_master' " & _
                  "AND ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/periodunkid)[1]', 'INT') = @Period " & _
                ") AS AT ON AT.AnalysisId = SF.analysisunkid AND AT.xno = 1 "
                'S.SANDEEP |20-APR-2019| -- START
                '*************************** ADDED
                'ROW_NUMBER()OVER(PARTITION BY ACT.parentunkid ORDER BY ACT.auditdatetime) as xno
                'AT.xno = 1
                'S.SANDEEP |20-APR-2019| -- END


                'S.SANDEEP |25-APR-2019| -- START
                '******************* REMOVED 
                'JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid

                '******************* ADDED
                'AND eD.xno = 1
                'LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid
                'S.SANDEEP |25-APR-2019| -- END

            ElseIf mintReportTypeId = 2 AndAlso mintReportModeId = 2 Then
                '/*REPORT MODE : ASSESSOR ASSESSMENT */
                StrQ = "SELECT DISTINCT " & _
                           "hremployee_master.employeecode As empcode " & _
                          ",hremployee_master.firstname+' '+hremployee_master.surname AS employee " & _
                          ",eJb.job_name AS empjob " & _
                          ",eDP.name AS empdept " & _
                          ",ISNULL(SF.eStatus,@NotAssessd) AS eStatus " & _
                          ",ISNULL(SF.username,'') AS [User/Employee] " & _
                          ",ISNULL(SF.Date,'') AS [Date] " & _
                          ",ISNULL(CASE WHEN ISNULL(SF.Time,'00:00') = '00:00' THEN AT.TIME ELSE SF.Time END,'') [Time] "
                'S.SANDEEP |25-APR-2019| -- START {ELSE SF.Time - ADDED} -- END

                If mintViewIndex > 0 Then
                    StrQ &= ", ISNULL(SF.iscommitted, 0 ) AS Id,  ISNULL(SF.eStatus, @NotAssessd) AS GName "
                Else
                    StrQ &= ",0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM hremployee_master " & _
                        "LEFT JOIN " & _
                        "( " & _
                          "SELECT " & _
                               "employeeunkid " & _
                              ",jobunkid " & _
                              ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                          "FROM hremployee_categorization_tran " & _
                          "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        ") AS eJ ON eJ.employeeunkid = hremployee_master.employeeunkid AND eJ.xno = 1 " & _
                        "LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                          "SELECT " & _
                               "employeeunkid " & _
                              ",departmentunkid " & _
                              ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                          "FROM hremployee_transfer_tran " & _
                          "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        ") AS eD ON eD.employeeunkid = hremployee_master.employeeunkid AND eD.xno = 1 " & _
                        "LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                          "SELECT " & _
                             "EV.assessedemployeeunkid " & _
                            ",CASE WHEN EV.iscommitted = 0 THEN @NotSubmitted ELSE @Submitted END AS eStatus " & _
                            ",CONVERT(NVARCHAR(8),ISNULL(EV.committeddatetime,EV.assessmentdate),112) AS [DATE] " & _
                            ",CONVERT(NVARCHAR(5),ISNULL(EV.committeddatetime,EV.assessmentdate),108) AS [TIME] " & _
                            ",EV.analysisunkid " & _
                            ",UR.username " & _
                            ",EV.iscommitted " & _
                          "FROM hrevaluation_analysis_master AS EV " & _
                            "JOIN hrassessor_master AS AM ON EV.assessormasterunkid = AM.assessormasterunkid " & _
                            "JOIN hrapprover_usermapping ON AM.assessormasterunkid = hrapprover_usermapping.approverunkid and usertypeid = 2 " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master UR ON hrapprover_usermapping.userunkid = UR.userunkid " & _
                          "WHERE EV.isvoid = 0 AND EV.periodunkid = @Period AND EV.assessmodeid = 2 " & _
                        ") AS SF ON SF.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                          "SELECT DISTINCT " & _
                             "ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/analysisunkid)[1]', 'INT') AS AnalysisId " & _
                            ",CONVERT(NVARCHAR(5),ACT.auditdatetime,108) AS [TIME] " & _
                            ",ROW_NUMBER()OVER(PARTITION BY ACT.parentunkid ORDER BY ACT.auditdatetime) as xno " & _
                          "FROM atcommon_tranlog AS ACT " & _
                          "WHERE ACT.parenttable_name = 'hrevaluation_analysis_master' " & _
                          "AND ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/periodunkid)[1]', 'INT') = @Period " & _
                          "AND ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/assessmodeid)[1]', 'INT') = 2 " & _
                        ") AS AT ON AT.AnalysisId = SF.analysisunkid AND AT.xno = 1 "
                'S.SANDEEP |20-APR-2019| -- START
                '*************************** ADDED
                'ROW_NUMBER()OVER(PARTITION BY ACT.parentunkid ORDER BY ACT.auditdatetime) as xno
                'AT.xno = 1
                'S.SANDEEP |20-APR-2019| -- END


                'S.SANDEEP |25-APR-2019| -- START
                '******************* REMOVED 
                'JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid

                '******************* ADDED
                'AND eD.xno = 1
                'LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid
                'S.SANDEEP |25-APR-2019| -- END

            ElseIf mintReportTypeId = 2 AndAlso mintReportModeId = 3 Then
                '/*REPORT MODE : REVIEWER ASSESSMENT */
                StrQ = "SELECT DISTINCT " & _
                   "hremployee_master.employeecode As empcode " & _
                  ",hremployee_master.firstname+' '+hremployee_master.surname AS employee " & _
                  ",eJb.job_name AS empjob " & _
                  ",eDP.name AS empdept " & _
                  ",ISNULL(SF.eStatus,@NotAssessd) AS eStatus " & _
                  ",ISNULL(SF.username,'') AS [User/Employee] " & _
                  ",ISNULL(SF.Date,'') AS [Date] " & _
                  ",ISNULL(CASE WHEN ISNULL(SF.Time,'00:00') = '00:00' THEN AT.TIME ELSE SF.Time END,'') [Time] "
                'S.SANDEEP |25-APR-2019| -- START {ELSE SF.Time - ADDED} -- END

                If mintViewIndex > 0 Then
                    StrQ &= ", ISNULL(SF.iscommitted, 0 ) AS Id,  ISNULL(SF.eStatus, @NotAssessd) AS GName "
                Else
                    StrQ &= ",0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM hremployee_master  " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT " & _
                       "employeeunkid " & _
                      ",jobunkid " & _
                      ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                  "FROM hremployee_categorization_tran " & _
                  "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                ") AS eJ ON eJ.employeeunkid = hremployee_master.employeeunkid AND eJ.xno = 1 " & _
                "LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT " & _
                       "employeeunkid " & _
                      ",departmentunkid " & _
                      ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                  "FROM hremployee_transfer_tran " & _
                  "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                ") AS eD ON eD.employeeunkid = hremployee_master.employeeunkid AND eD.xno = 1 " & _
                "LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT " & _
                     "EV.assessedemployeeunkid " & _
                    ",CASE WHEN EV.iscommitted = 0 THEN @NotSubmitted ELSE @Submitted END AS eStatus " & _
                    ",CONVERT(NVARCHAR(8),ISNULL(EV.committeddatetime,EV.assessmentdate),112) AS [DATE] " & _
                    ",CONVERT(NVARCHAR(5),ISNULL(EV.committeddatetime,EV.assessmentdate),108) AS [TIME] " & _
                    ",EV.analysisunkid " & _
                    ",UR.username " & _
                    ",EV.iscommitted " & _
                  "FROM hrevaluation_analysis_master AS EV " & _
                    "JOIN hrassessor_master AS AM ON EV.assessormasterunkid = AM.assessormasterunkid " & _
                    "JOIN hrapprover_usermapping ON AM.assessormasterunkid = hrapprover_usermapping.approverunkid and usertypeid = 2 " & _
                    "LEFT JOIN hrmsConfiguration..cfuser_master UR ON hrapprover_usermapping.userunkid = UR.userunkid " & _
                  "WHERE EV.isvoid = 0 AND EV.periodunkid = @Period AND EV.assessmodeid = 3 " & _
                ") AS SF ON SF.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                "LEFT JOIN " & _
                "( " & _
                  "SELECT DISTINCT " & _
                     "ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/analysisunkid)[1]', 'INT') AS AnalysisId " & _
                    ",CONVERT(NVARCHAR(5),ACT.auditdatetime,108) AS [TIME] " & _
                    ",ROW_NUMBER()OVER(PARTITION BY ACT.parentunkid ORDER BY ACT.auditdatetime) as xno " & _
                  "FROM atcommon_tranlog AS ACT " & _
                  "WHERE ACT.parenttable_name = 'hrevaluation_analysis_master' " & _
                  "AND ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/periodunkid)[1]', 'INT') = @Period " & _
                  "AND ACT.parent_xmlvalue.value('(hrevaluation_analysis_master/assessmodeid)[1]', 'INT') = 3 " & _
                ") AS AT ON AT.AnalysisId = SF.analysisunkid AND AT.xno = 1 "
                'S.SANDEEP |20-APR-2019| -- START
                '*************************** ADDED
                'ROW_NUMBER()OVER(PARTITION BY ACT.parentunkid ORDER BY ACT.auditdatetime) as xno
                'AT.xno = 1
                'S.SANDEEP |20-APR-2019| -- END


                'S.SANDEEP |25-APR-2019| -- START
                '******************* REMOVED 
                'JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid

                '******************* ADDED
                'AND eD.xno = 1
                'LEFT JOIN hrjob_master AS eJb ON eJb.jobunkid = eJ.jobunkid
                'LEFT JOIN hrdepartment_master AS eDP ON eD.departmentunkid = eDP.departmentunkid
                'S.SANDEEP |25-APR-2019| -- END

            End If
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= "WHERE 1 = 1 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If


            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery
            objDataOperation.AddParameter("@Submit_Approval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 1, "Submitted For Approval"))
            objDataOperation.AddParameter("@Final_Save", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 101, "Approved"))
            objDataOperation.AddParameter("@Open_Changes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 3, "Opened For Changes"))
            objDataOperation.AddParameter("@Planned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 4, "Not Submitted For Approval"))
            objDataOperation.AddParameter("@NotPlanned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 55, "Not Planned"))
            objDataOperation.AddParameter("@NOT_COMMITTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 49, "Not Committed"))
            objDataOperation.AddParameter("@FINAL_COMMITTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 50, "Final Committed"))
            objDataOperation.AddParameter("@PERIODIC_REVIEW", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 51, "Periodic Review"))
            objDataOperation.AddParameter("@Submitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmAssessmentAuditReport", 6, "Committed Assessment"))
            objDataOperation.AddParameter("@NotSubmitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmAssessmentAuditReport", 7, "Not Committed Assessment"))
            objDataOperation.AddParameter("@NotAssessd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Assessment Not Done"))
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim dt_Row As DataRow = Nothing
            Dim iCnt As Integer = 1 : Dim StrGrpName As String = String.Empty
            Dim dtTable As DataTable
            If mstrReport_GroupName.Trim.Length > 0 Then
                dtTable = New DataView(dsList.Tables("DataTable"), "", "eStatus", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("DataTable")
            End If

            For Each dtRow As DataRow In dtTable.Rows
                dt_Row = mdtTableExcel.NewRow

                dt_Row.Item("EmpCode") = dtRow.Item("EmpCode")
                dt_Row.Item("Employee") = dtRow.Item("Employee")
                dt_Row.Item("EmpJob") = dtRow.Item("EmpJob")
                dt_Row.Item("EmpDept") = dtRow.Item("EmpDept")
                dt_Row.Item("EStatus") = dtRow.Item("EStatus")
                dt_Row.Item("User/Employee") = dtRow.Item("User/Employee")
                Dim strDate As String = String.Empty
                If dtRow.Item("Date") IsNot DBNull.Value AndAlso dtRow.Item("Date").ToString.Trim <> "" Then
                    strDate = eZeeDate.convertDate(dtRow.Item("Date")).ToShortDateString
                End If
                dt_Row.Item("DateTime") = strDate & " " & dtRow.Item("Time").ToString.Trim
                dt_Row.Item("Sr.No") = iCnt.ToString

                dt_Row.Item("eStatus") = dtRow.Item("GName")

                mdtTableExcel.Rows.Add(dt_Row)
                iCnt += 1
            Next

            Dim rowsArrayFooter As New ArrayList
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strGTotal As String = Language.getMessage(mstrModuleName, 6, "Grand Total :")
            Dim strSubTotal As String = Language.getMessage(mstrModuleName, 9, "Sub Total :")
            Dim strarrGroupColumns As String() = Nothing
            If mintViewIndex > 0 Then
                Dim strGrpCols As String() = {"eStatus"}
                strarrGroupColumns = strGrpCols
            End If
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            If mintViewIndex > 0 Then
            Else
                mdtTableExcel.Columns.Remove("eStatus")
            End If
            If mintPeriodId > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Period :") & " " & mstrPeriodName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If
            If mintEmployeeId > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Employee :") & " " & mstrEmployeeName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If
            If (mintReportTypeId = 1 AndAlso mintStatusId > 0) Or (mintReportTypeId = 2 AndAlso mintStatusId > -1) Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Status :") & " " & mstrStatusName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If
            If mintReportModeId > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Report Mode :") & " " & mstrReportModeName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "As on Date :") & " " & dtPeriodEnd.ToShortDateString, "s10bw")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Language.getMessage(mstrModuleName, 12, "Assessment Audit") & " (" & mstrReportTypeName & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Assessment_Report; Module Name: " & mstrModuleName)

        End Try

    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsBSC_Planning_Report", 1, "Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 3, "Opened For Changes")
            Language.setMessage("clsBSC_Planning_Report", 4, "Not Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 49, "Not Committed")
            Language.setMessage("clsBSC_Planning_Report", 50, "Final Committed")
            Language.setMessage("clsBSC_Planning_Report", 51, "Periodic Review")
            Language.setMessage("clsBSC_Planning_Report", 55, "Not Planned")
            Language.setMessage("clsBSC_Planning_Report", 101, "Approved")
            Language.setMessage("frmAssessmentAuditReport", 6, "Committed Assessment")
            Language.setMessage("frmAssessmentAuditReport", 7, "Not Committed Assessment")
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "Job Name")
            Language.setMessage(mstrModuleName, 4, "Department Name")
            Language.setMessage(mstrModuleName, 5, "User")
            Language.setMessage(mstrModuleName, 6, "Grand Total :")
            Language.setMessage(mstrModuleName, 7, "Status :")
            Language.setMessage(mstrModuleName, 8, "Date & Time")
            Language.setMessage(mstrModuleName, 9, "Sub Total :")
            Language.setMessage(mstrModuleName, 10, "Status")
            Language.setMessage(mstrModuleName, 11, "Employee :")
            Language.setMessage(mstrModuleName, 12, "Assessment Audit")
            Language.setMessage(mstrModuleName, 13, "Sr.No")
            Language.setMessage(mstrModuleName, 14, "As on Date :")
            Language.setMessage(mstrModuleName, 15, "Report Mode :")
            Language.setMessage(mstrModuleName, 16, "Period :")
            Language.setMessage(mstrModuleName, 17, " Order By :")
            Language.setMessage(mstrModuleName, 18, "Assessment Not Done")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
