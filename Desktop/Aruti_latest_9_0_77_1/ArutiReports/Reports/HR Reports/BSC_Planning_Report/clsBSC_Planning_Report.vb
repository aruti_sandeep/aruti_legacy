'************************************************************************************************************************************
'Class Name : clsEmployeeSkills.vb
'Purpose    :
'Date       : 17 Apr 2013
'Written By : Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
''' 
Public Class clsBSC_Planning_Report
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsBSC_Planning_Report"
    Private mstrReportId As String = enArutiReport.BSC_Planning_Report '115
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mdtFilterDate1 As DateTime = Nothing
    Private mdtFilterDate2 As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    Private mblnIncludeSummary As Boolean = True
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mdtPeriod_St_Date As DateTime = Nothing
    Private mdtPeriod_Ed_Date As DateTime = Nothing
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'S.SANDEEP [ 25 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnExcludeInactiveEmployee As Boolean = False
    'S.SANDEEP [ 25 SEPT 2013 ] -- END

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Private mintAppointmentTypeId As Integer = 0
    Private mstrAppointmentTypeName As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    'S.SANDEEP [ 22 OCT 2013 ] -- END

    'S.SANDEEP [ 03 NOV 2014 ] -- START
    Private mblnConsiderEmployeeTerminationOnPeriodDate As Boolean = False
    'S.SANDEEP [ 03 NOV 2014 ] -- END

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtDatabase_Start_Date As Date
    'Sohail (21 Aug 2015) -- End

    'S.SANDEEP [05-JUN-2017] -- START
    'ISSUE/ENHANCEMENT : PLANNING REPORT
    Private minReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mDicSeletedEmpids As Dictionary(Of Integer, String)
    Private iCascadingTypeId As Integer = 0
    Private mblnShowComputedScore As Boolean = False
    'S.SANDEEP [05-JUN-2017] -- END

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Private mblnIsCalibrationSettingActive As Boolean = False
    Private mintDisplayScoreType As Integer = 0
    Private mstrDisplayScoreName As String = String.Empty
    'S.SANDEEP |27-MAY-2019| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _FilterDate1() As DateTime
        Set(ByVal value As DateTime)
            mdtFilterDate1 = value
        End Set
    End Property

    Public WriteOnly Property _FilterDate2() As DateTime
        Set(ByVal value As DateTime)
            mdtFilterDate2 = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeSummary() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeSummary = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Period_St_Date() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriod_St_Date = value
        End Set
    End Property

    Public WriteOnly Property _Period_Ed_Date() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriod_Ed_Date = value
        End Set
    End Property

    'S.SANDEEP [ 25 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _ExcludeInactiveEmployee() As Boolean
        Set(ByVal value As Boolean)
            mblnExcludeInactiveEmployee = value
        End Set
    End Property
    'S.SANDEEP [ 25 SEPT 2013 ] -- END

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeName() As String
        Set(ByVal value As String)
            mstrAppointmentTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property
    'S.SANDEEP [ 22 OCT 2013 ] -- END

    'S.SANDEEP [ 03 NOV 2014 ] -- START
    Public WriteOnly Property _ConsiderEmployeeTerminationOnPeriodDate() As Boolean
        Set(ByVal value As Boolean)
            mblnConsiderEmployeeTerminationOnPeriodDate = value
        End Set
    End Property
    'S.SANDEEP [ 03 NOV 2014 ] -- END

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public WriteOnly Property _Database_Start_Date() As Date
        Set(ByVal value As Date)
            mdtDatabase_Start_Date = value
        End Set
    End Property
    'Sohail (21 Aug 2015) -- End

    'S.SANDEEP [05-JUN-2017] -- START
    'ISSUE/ENHANCEMENT : PLANNING REPORT
    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            minReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _SeletedEmpids() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mDicSeletedEmpids = value
        End Set
    End Property

    Public WriteOnly Property _CascadingTypeId() As Integer
        Set(ByVal value As Integer)
            iCascadingTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ShowComputedScore() As Boolean
        Set(ByVal value As Boolean)
            mblnShowComputedScore = value
        End Set
    End Property
    'S.SANDEEP [05-JUN-2017] -- END

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Public WriteOnly Property _IsCalibrationSettingActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCalibrationSettingActive = value
        End Set
    End Property
    Public WriteOnly Property _DisplayScoreType() As Integer
        Set(ByVal value As Integer)
            mintDisplayScoreType = value
        End Set
    End Property
    Public WriteOnly Property _DisplayScoreName() As String
        Set(ByVal value As String)
            mstrDisplayScoreName = value
        End Set
    End Property
    'S.SANDEEP |27-MAY-2019| -- END

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mdtFilterDate1 = Nothing
            mdtFilterDate2 = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintStatusId = 0
            mstrStatusName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrUserAccessFilter = String.Empty
            mstrAdvance_Filter = String.Empty
            mdtPeriod_St_Date = Nothing
            mdtPeriod_Ed_Date = Nothing
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            'S.SANDEEP [ 25 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnExcludeInactiveEmployee = False
            'S.SANDEEP [ 25 SEPT 2013 ] -- END

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            mintAppointmentTypeId = 0
            mstrAppointmentTypeName = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'S.SANDEEP [ 03 NOV 2014 ] -- START
            mblnConsiderEmployeeTerminationOnPeriodDate = False
            'S.SANDEEP [ 03 NOV 2014 ] -- END

            'S.SANDEEP [05-JUN-2017] -- START
            'ISSUE/ENHANCEMENT : PLANNING REPORT
            minReportTypeId = 0
            mstrReportTypeName = String.Empty
            mDicSeletedEmpids = Nothing
            mblnShowComputedScore = False
            'S.SANDEEP [05-JUN-2017] -- END

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            mblnIsCalibrationSettingActive = False
            mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
            mstrDisplayScoreName = ""
            'S.SANDEEP |27-MAY-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''S.SANDEEP [ 25 SEPT 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mblnExcludeInactiveEmployee = False Then
            '    'S.SANDEEP [ 03 NOV 2014 ] -- START
            '    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriod_St_Date))
            '    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriod_Ed_Date))
            '    If mblnConsiderEmployeeTerminationOnPeriodDate = True Then
            '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriod_St_Date))
            '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriod_Ed_Date))
            '    Else
            '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    End If
            '    'S.SANDEEP [ 03 NOV 2014 ] -- END
            'Else
            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If
            ''S.SANDEEP [ 25 SEPT 2013 ] -- END


            'S.SANDEEP [04 JUN 2015] -- END




            objDataOperation.AddParameter("@Submit_Approval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Submitted For Approval"))
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
            'objDataOperation.AddParameter("@Final_Save", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Final Saved"))


            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'objDataOperation.AddParameter("@Final_Save", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Final Approved"))
            objDataOperation.AddParameter("@Final_Save", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            'Pinkal (15-Mar-2019) -- End


            'S.SANDEEP |12-MAR-2019| -- END
            objDataOperation.AddParameter("@Open_Changes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Opened For Changes"))
            objDataOperation.AddParameter("@Not_Submit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Not Submitted For Approval"))
            objDataOperation.AddParameter("@Not_Planned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Not Planned"))
            objDataOperation.AddParameter("@Not_Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Not Planned"))

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            objDataOperation.AddParameter("@NOT_COMMITTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 49, "Not Committed"))
            objDataOperation.AddParameter("@FINAL_COMMITTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 50, "Final Committed"))
            objDataOperation.AddParameter("@PERIODIC_REVIEW", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "Periodic Review"))
            'S.SANDEEP [ 05 NOV 2014 ] -- END



            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Period :") & " " & mstrPeriodName & " "
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            End If

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Employee :") & " " & mstrEmployeeName & " "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND BSC_APPR.employeeunkid = @EmpId "
            End If

            If mintStatusId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Status :") & " " & mstrStatusName & " "
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
            End If

            If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Status Date From :") & " " & mdtFilterDate1.Date & " " & _
                                   Language.getMessage(mstrModuleName, 9, "To :") & " " & mdtFilterDate2.Date & " "

                objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFilterDate1))
                objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFilterDate2))

                Me._FilterQuery &= " AND BSC_APPR.Status_Date BETWEEN @Date1 AND @Date2 "

            End If

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Appointed Date From : ") & " " & mdtDate1.ToShortDateString & " " & _
                                           Language.getMessage(mstrModuleName, 36, "To : ") & " " & mdtDate2.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "Appointed Before Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 38, "Appointed After Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
            End Select
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Order By :") & " " & Me.OrderByDisplay & " "
                'BSC_StatusId
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY Id, BSC_StatusId, " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY BSC_StatusId, " & Me.OrderByQuery
                End If


            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            'S.SANDEEP [05-JUN-2017] -- START
            'ISSUE/ENHANCEMENT : PLANNING REPORT
            'objRpt = Generate_DetailReport(xDatabaseName, _
            '                               xUserUnkid, _
            '                               xYearUnkid, _
            '                               xCompanyUnkid, _
            '                               xPeriodStart, _
            '                               xPeriodEnd, _
            '                               xUserModeSetting, _
            '                               xOnlyApproved, mdtDatabase_Start_Date)
            Select Case pintReportType
                Case 0
                    objRpt = Generate_DetailReport(xDatabaseName, _
                                                   xUserUnkid, _
                                                   xYearUnkid, _
                                                   xCompanyUnkid, _
                                                   xPeriodStart, _
                                                   xPeriodEnd, _
                                                   xUserModeSetting, _
                                                   xOnlyApproved, mdtDatabase_Start_Date)
                Case 1
                    objRpt = Generate_EmployeeReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved, mdtDatabase_Start_Date)
            End Select

            'S.SANDEEP [05-JUN-2017] -- END



            'Sohail (21 Aug 2015) - [mdtDatabase_Start_Date]
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ECode", Language.getMessage(mstrModuleName, 11, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 12, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("BSC_Status", Language.getMessage(mstrModuleName, 13, "Planning Status")))
            iColumn_DetailReport.Add(New IColumn("PName", Language.getMessage(mstrModuleName, 14, "Period")))
            'iColumn_DetailReport.Add(New IColumn("Financial_Year", Language.getMessage(mstrModuleName, 15, "Financial Year")))
            iColumn_DetailReport.Add(New IColumn("Job", Language.getMessage(mstrModuleName, 15, "Job")))
            iColumn_DetailReport.Add(New IColumn("Dept", Language.getMessage(mstrModuleName, 43, "Department")))
            iColumn_DetailReport.Add(New IColumn("Class", Language.getMessage(mstrModuleName, 44, "Workstation")))
            iColumn_DetailReport.Add(New IColumn("sort_date", Language.getMessage(mstrModuleName, 16, "Status Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [05-JUN-2017] -- START
    'ISSUE/ENHANCEMENT : PLANNING REPORT
    Private Function Generate_EmployeeReport(ByVal strDatabaseName As String, _
                                             ByVal intUserUnkid As Integer, _
                                             ByVal intYearUnkid As Integer, _
                                             ByVal intCompanyUnkid As Integer, _
                                             ByVal dtPeriodStart As Date, _
                                             ByVal dtPeriodEnd As Date, _
                                             ByVal strUserModeSetting As String, _
                                             ByVal blnOnlyApproved As Boolean, _
                                             ByVal dtDatabase_Start_Date As Date _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rptSubreport_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_EmpData As ArutiReport.Designer.dsArutiReport
        Dim rpt_Goals As ArutiReport.Designer.dsArutiReport
        Dim rpt_Cmpts As ArutiReport.Designer.dsArutiReport
        Dim rpt_Score As ArutiReport.Designer.dsArutiReport
        Dim rpt_Items As ArutiReport.Designer.dsArutiReport
        'S.SANDEEP [28-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REPORT ENHANCEMENT
        Dim rpt_Status As ArutiReport.Designer.dsArutiReport
        'S.SANDEEP [28-SEP-2017] -- END
        Try
            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = intCompanyUnkid
            mDecRoundingFactor = objConfig._PAScoringRoudingFactor
            objConfig = Nothing
            'S.SANDEEP |24-DEC-2019| -- END

            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim strEmpIds As String = ""

            strEmpIds = String.Join(",", mDicSeletedEmpids.Keys.Select(Function(x) x.ToString()).ToArray())

            If strEmpIds.Trim.Length > 0 Then
                StrQ = "IF OBJECT_ID('tempdb..#EmpTab') IS NOT NULL " & _
                       "DROP TABLE #EmpTab " & _
                       "CREATE TABLE #EmpTab(empid int) " & _
                       "DECLARE @words VARCHAR (MAX) " & _
                       "SET @words = '" & strEmpIds & "' " & _
                       "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                       "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                       "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                       "WHILE @start < @stop begin " & _
                       "    SELECT " & _
                       "         @end = CHARINDEX(',',@words,@start) " & _
                       "        ,@word = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                       "        ,@start = @end+1 " & _
                       "    INSERT @split VALUES (@word) " & _
                       "END " & _
                       "INSERT INTO #EmpTab(empid) SELECT CAST(word AS INT) FROM @split ORDER BY word "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            StrQ = "SELECT " & _
                         " hremployee_master.employeeunkid " & _
                         ",hremployee_master.employeecode AS Ecode " & _
                         ",hremployee_master.firstname+' '+hremployee_master.surname AS Ename " & _
                         ",CONVERT(NVARCHAR(8),hremployee_master.appointeddate,112) AS Edate " & _
                         ",ISNULL(etrf.Dept,'') AS Department " & _
                         ",ISNULL(ect.JobName,'') AS JobName " & _
                    "FROM hremployee_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "tr.employeeunkid AS tempid " & _
                              ",ISNULL(hrstation_master.name,'') AS Branch " & _
                              ",ISNULL(hrdepartment_group_master.name,'') AS DeptGrp " & _
                              ",ISNULL(hrdepartment_master.name,'') AS Dept " & _
                              ",ISNULL(hrsectiongroup_master.name,'') AS SecGrp " & _
                              ",ISNULL(hrsection_master.name,'') AS Sec " & _
                              ",ISNULL(hrunitgroup_master.name,'') AS UnitGrp " & _
                              ",ISNULL(hrunit_master.name,'') AS Unit " & _
                              ",ISNULL(hrteam_master.name,'') AS Team " & _
                              ",ISNULL(hrclassgroup_master.name,'') AS ClsGrp " & _
                              ",ISNULL(hrclasses_master.name,'') AS Cls " & _
                         "FROM " & _
                         "( " & _
                              "SELECT " & _
                                    "hremployee_transfer_tran.employeeunkid " & _
                                   ",hremployee_transfer_tran.stationunkid " & _
                                   ",hremployee_transfer_tran.deptgroupunkid " & _
                                   ",hremployee_transfer_tran.departmentunkid " & _
                                   ",hremployee_transfer_tran.sectiongroupunkid " & _
                                   ",hremployee_transfer_tran.sectionunkid " & _
                                   ",hremployee_transfer_tran.unitgroupunkid " & _
                                   ",hremployee_transfer_tran.unitunkid " & _
                                   ",hremployee_transfer_tran.teamunkid " & _
                                   ",hremployee_transfer_tran.classgroupunkid " & _
                                   ",hremployee_transfer_tran.classunkid " & _
                                   ",ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rno " & _
                              "FROM hremployee_transfer_tran "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hremployee_transfer_tran.employeeunkid "
            End If
            StrQ &= "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

            StrQ &= ") AS tr " & _
                         "LEFT JOIN hrclasses_master ON tr.classunkid = hrclasses_master.classesunkid " & _
                         "LEFT JOIN hrclassgroup_master ON tr.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                         "LEFT JOIN hrteam_master ON tr.teamunkid = hrteam_master.teamunkid " & _
                         "LEFT JOIN hrunit_master ON tr.unitunkid = hrunit_master.unitunkid " & _
                         "LEFT JOIN hrunitgroup_master ON tr.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                         "LEFT JOIN hrsection_master ON tr.sectionunkid = hrsection_master.sectionunkid " & _
                         "LEFT JOIN hrsectiongroup_master ON tr.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                         "LEFT JOIN hrdepartment_master ON tr.departmentunkid = hrdepartment_master.departmentunkid " & _
                         "LEFT JOIN hrdepartment_group_master ON tr.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                         "LEFT JOIN hrstation_master ON tr.stationunkid = hrstation_master.stationunkid " & _
                         "WHERE tr.Rno = 1 " & _
                    ") AS etrf ON hremployee_master.employeeunkid = etrf.tempid " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "ct.employeeunkid AS tempid " & _
                              ",ISNULL(hrjob_master.job_name,'') AS JobName " & _
                              ",ISNULL(hrjobgroup_master.name,'') AS JobGrp " & _
                         "FROM " & _
                         "( " & _
                              "SELECT " & _
                                    "hremployee_categorization_tran.employeeunkid " & _
                                   ",hremployee_categorization_tran.jobgroupunkid " & _
                                   ",hremployee_categorization_tran.jobunkid " & _
                                   ",ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS Rno " & _
                              "FROM hremployee_categorization_tran "

            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hremployee_categorization_tran.employeeunkid "
            End If

            StrQ &= "WHERE isvoid = 0  AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

            StrQ &= ") AS ct " & _
                         "LEFT JOIN hrjob_master ON ct.jobunkid = hrjob_master.jobunkid " & _
                         "LEFT JOIN hrjobgroup_master ON ct.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                         "WHERE ct.Rno = 1 " & _
                    ") AS ect ON ect.tempid = hremployee_master.employeeunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid IN (" & strEmpIds & ")"
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'S.SANDEEP [28-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REPORT ENHANCEMENT
            Dim dsasrDetails As New DataSet
            Dim objEval As New clsevaluation_analysis_master
            dsasrDetails = objEval.GetEmployee_AssessorReviewerDetails(mintPeriodId, strDatabaseName, Nothing)
            'S.SANDEEP [28-SEP-2017] -- END

            rpt_EmpData = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_EmpData.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("Ecode")
                rpt_Row.Item("Column3") = dtRow.Item("Ename")
                If dtRow.Item("Edate").ToString.Trim.Length > 0 Then
                    rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("Edate").ToString()).ToShortDateString()
                Else
                    rpt_Row.Item("Column4") = ""
                End If
                rpt_Row.Item("Column5") = dtRow.Item("Department")
                rpt_Row.Item("Column6") = dtRow.Item("JobName")

                'S.SANDEEP [28-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REPORT ENHANCEMENT
                If dsasrDetails IsNot Nothing AndAlso dsasrDetails.Tables.Count > 0 Then
                    Dim dtmp() As DataRow = Nothing
                    dtmp = dsasrDetails.Tables(0).Select("isreviewer = 0 AND isfound = 1 AND assignempid = '" & dtRow.Item("employeeunkid") & "'")
                    If dtmp.Length > 0 Then
                        rpt_Row.Item("Column7") = dtmp(0).Item("arCode") & " - " & dtmp(0).Item("arName")
                    Else
                        dtmp = dsasrDetails.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1 AND assignempid = '" & dtRow.Item("employeeunkid") & "'")
                        If dtmp.Length > 0 Then rpt_Row.Item("Column7") = dtmp(0).Item("arCode") & " - " & dtmp(0).Item("arName")
                    End If
                    dtmp = dsasrDetails.Tables(0).Select("isreviewer = 1 AND isfound = 1 AND assignempid = '" & dtRow.Item("employeeunkid") & "'")
                    If dtmp.Length > 0 Then
                        rpt_Row.Item("Column8") = dtmp(0).Item("arCode") & " - " & dtmp(0).Item("arName")
                    Else
                        dtmp = dsasrDetails.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1 AND assignempid = '" & dtRow.Item("employeeunkid") & "'")
                        If dtmp.Length > 0 Then rpt_Row.Item("Column8") = dtmp(0).Item("arCode") & " - " & dtmp(0).Item("arName")
                    End If
                End If
                'S.SANDEEP [28-SEP-2017] -- END
                rpt_EmpData.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            StrQ = "DECLARE @LinkFieldId AS INT " & _
                   "SET @LinkFieldId = ISNULL((SELECT fieldunkid FROM hrassess_field_mapping WHERE periodunkid = @periodunkid AND isactive = 1),0) " & _
                   "SELECT " & _
                         " A.perspectiveunkid " & _
                         ",ISNULL(hrassess_perspective_master.name,'') AS perspective " & _
                         ",A.employeeunkid " & _
                         ",Field1 " & _
                         ",Field2 " & _
                         ",Field3 " & _
                         ",Field4 " & _
                         ",Field5 " & _
                         ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN A.w1 " & _
                                         "WHEN @LinkFieldId = Field2Id THEN A.w2 " & _
                                         "WHEN @LinkFieldId = Field3Id THEN A.w3 " & _
                                         "WHEN @LinkFieldId = Field4Id THEN A.w4 " & _
                                         "WHEN @LinkFieldId = Field5Id THEN A.w4 " & _
                                   "END, 0) AS IWeight " & _
                         ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_SDate " & _
                                         "WHEN @LinkFieldId = Field2Id THEN f2_SDate " & _
                                         "WHEN @LinkFieldId = Field3Id THEN f3_SDate " & _
                                         "WHEN @LinkFieldId = Field4Id THEN f4_SDate " & _
                                         "WHEN @LinkFieldId = Field5Id THEN f5_SDate " & _
                                   "END, '') AS St_Date " & _
                         ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_EDate " & _
                                         "WHEN @LinkFieldId = Field2Id THEN f2_EDate " & _
                                         "WHEN @LinkFieldId = Field3Id THEN f3_EDate " & _
                                         "WHEN @LinkFieldId = Field4Id THEN f4_EDate " & _
                                         "WHEN @LinkFieldId = Field5Id THEN f5_EDate " & _
                                   "END, '') AS Ed_Date " & _
                         ",ISNULL(B.pct_completed,0) AS pct_completed " & _
                         ",ISNULL(B.dstatus,'') AS C_Status " & _
                    "FROM " & _
                    "( " & _
                         "SELECT "
            If iCascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= "hrassess_empfield1_master.perspectiveunkid "
            Else
                StrQ &= " ISNULL(hrassess_coyfield1_master.perspectiveunkid,0) AS perspectiveunkid "
            End If

            StrQ &= ",hrassess_empfield1_master.employeeunkid " & _
                      ",ISNULL(hrassess_empfield1_master.field_data,'') AS Field1 " & _
                      ",ISNULL(hrassess_empfield2_master.field_data,'') AS Field2 " & _
                      ",ISNULL(hrassess_empfield3_master.field_data,'') AS Field3 " & _
                      ",ISNULL(hrassess_empfield4_master.field_data,'') AS Field4 " & _
                      ",ISNULL(hrassess_empfield5_master.field_data,'') AS Field5 " & _
                      ",ISNULL(hrassess_empfield1_master.weight,0) AS w1 " & _
                      ",ISNULL(hrassess_empfield2_master.weight,0) AS w2 " & _
                      ",ISNULL(hrassess_empfield3_master.weight,0) AS w3 " & _
                      ",ISNULL(hrassess_empfield4_master.weight,0) AS w4 " & _
                      ",ISNULL(hrassess_empfield5_master.weight,0) AS w5 " & _
                      ",ISNULL(hrassess_empfield1_master.fieldunkid,0) AS Field1Id " & _
                      ",ISNULL(hrassess_empfield2_master.fieldunkid,0) AS Field2Id " & _
                      ",ISNULL(hrassess_empfield3_master.fieldunkid,0) AS Field3Id " & _
                      ",ISNULL(hrassess_empfield4_master.fieldunkid,0) AS Field4Id " & _
                      ",ISNULL(hrassess_empfield5_master.fieldunkid,0) AS Field5Id " & _
                      ",ISNULL(hrassess_empfield1_master.empfield1unkid,0) AS empfield1unkid " & _
                      ",ISNULL(hrassess_empfield2_master.empfield2unkid,0) AS empfield2unkid " & _
                      ",ISNULL(hrassess_empfield3_master.empfield3unkid,0) AS empfield3unkid " & _
                      ",ISNULL(hrassess_empfield4_master.empfield4unkid,0) AS empfield4unkid " & _
                      ",ISNULL(hrassess_empfield5_master.empfield5unkid,0) AS empfield5unkid " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.startdate, 112),'') AS f1_SDate " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.enddate, 112),'') AS f1_EDate " & _
                      ",ISNULL(hrassess_empfield1_master.statusunkid, 0) AS f1StatId " & _
                      ",ISNULL(hrassess_empfield1_master.pct_completed, 0) AS f1pct " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.startdate, 112),'') AS f2_SDate " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.enddate, 112),'') AS f2_EDate " & _
                      ",ISNULL(hrassess_empfield2_master.statusunkid, 0) AS f2StatId " & _
                      ",ISNULL(hrassess_empfield2_master.pct_completed, 0) AS f2pct " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.startdate, 112),'') AS f3_SDate " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.enddate, 112),'') AS f3_EDate " & _
                      ",ISNULL(hrassess_empfield3_master.statusunkid, 0) AS f3StatId " & _
                      ",ISNULL(hrassess_empfield3_master.pct_completed, 0) AS f3pct " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.startdate, 112),'') AS f4_SDate " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.enddate, 112),'') AS f4_EDate " & _
                      ",ISNULL(hrassess_empfield4_master.statusunkid, 0) AS f4StatId " & _
                      ",ISNULL(hrassess_empfield4_master.pct_completed, 0) AS f4pct " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.startdate, 112),'') AS f5_SDate " & _
                      ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.enddate, 112),'') AS f5_EDate " & _
                      ",ISNULL(hrassess_empfield5_master.statusunkid, 0) AS f5StatId " & _
                      ",ISNULL(hrassess_empfield5_master.pct_completed, 0) AS f5pct " & _
            "FROM hrassess_empfield1_master "

            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hrassess_empfield1_master.employeeunkid "
            End If

            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= " LEFT JOIN hrassess_owrfield1_master ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_coyfield1_master ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 "
            End If
            StrQ &= "LEFT JOIN hrassess_empfield2_master ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                    "LEFT JOIN hrassess_empfield3_master ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                    "LEFT JOIN hrassess_empfield4_master ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                    "LEFT JOIN hrassess_empfield5_master ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid AND hrassess_empfield4_master.isvoid = 0 " & _
            "WHERE hrassess_empfield1_master.periodunkid = @periodunkid AND hrassess_empfield1_master.isvoid = 0 " & _
            ") AS A " & _
            "   LEFT JOIN hrassess_perspective_master ON A.perspectiveunkid = hrassess_perspective_master.perspectiveunkid " & _
            "LEFT JOIN " & _
            "( " & _
                 "SELECT " & _
                           "CAST(pct_completed AS DECIMAL(36,2)) AS pct_completed " & _
                          ",CASE WHEN statusunkid = 1 THEN @ST_PENDING " & _
                                "WHEN statusunkid = 2 THEN @ST_INPROGRESS " & _
                                "WHEN statusunkid = 3 THEN @ST_COMPLETE " & _
                                "WHEN statusunkid = 4 THEN @ST_CLOSED " & _
                                "WHEN statusunkid = 5 THEN @ST_ONTRACK " & _
                                "WHEN statusunkid = 6 THEN @ST_ATRISK " & _
                                "WHEN statusunkid = 7 THEN @ST_NOTAPPLICABLE " & _
                                "WHEN statusunkid = 8 THEN @ST_POSTPONED " & _
                                "WHEN statusunkid = 9 THEN @ST_BEHIND_SCHEDULE " & _
                                "WHEN statusunkid = 10 THEN @ST_AHEAD_SCHEDULE " & _
                           "END AS dstatus " & _
                      ",hrassess_empupdate_tran.empfieldunkid " & _
                      ",hrassess_empupdate_tran.empfieldtypeid " & _
                      ",hrassess_empupdate_tran.employeeunkid " & _
                      ",hrassess_empupdate_tran.periodunkid " & _
                 ",ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid,empfieldunkid ORDER BY updatedate DESC) AS Rno " & _
                 "FROM hrassess_empupdate_tran " & _
                 "WHERE isvoid = 0 AND hrassess_empupdate_tran.approvalstatusunkid = 2 AND hrassess_empupdate_tran.periodunkid = @periodunkid " & _
            ") AS B ON A.employeeunkid = B.employeeunkid AND B.empfieldtypeid = @LinkFieldId AND B.Rno = 1 " & _
                 "AND ISNULL(CASE WHEN @LinkFieldId = A.Field1Id THEN A.empfield1unkid WHEN @LinkFieldId = A.Field2Id THEN A.empfield2unkid " & _
                                     "WHEN @LinkFieldId = A.Field3Id THEN A.empfield3unkid WHEN @LinkFieldId = A.Field4Id THEN A.empfield4unkid " & _
                                     "WHEN @LinkFieldId = A.Field5Id THEN A.empfield5unkid END, 0) = B.empfieldunkid "

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            '----------------- REMOVED
            'CAST(pct_completed AS DECIMAL(10,2)) AS pct_completed

            '----------------- ADDED
            'CAST(pct_completed AS DECIMAL(36,2)) AS pct_completed
            'S.SANDEEP |21-AUG-2019| -- END


            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))
            objDataOperation.AddParameter("@ST_POSTPONED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 748, "Postponed"))
            objDataOperation.AddParameter("@ST_BEHIND_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 749, "Behind Schedule"))
            objDataOperation.AddParameter("@ST_AHEAD_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 750, "Ahead of Schedule"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Goals = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Goals.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("Field1")
                rpt_Row.Item("Column3") = dtRow.Item("Field2")
                rpt_Row.Item("Column4") = dtRow.Item("Field3")
                rpt_Row.Item("Column5") = dtRow.Item("Field4")
                rpt_Row.Item("Column6") = dtRow.Item("Field5")
                rpt_Row.Item("Column7") = dtRow.Item("IWeight")
                rpt_Row.Item("Column8") = dtRow.Item("pct_completed")
                rpt_Row.Item("Column9") = dtRow.Item("C_Status")
                rpt_Row.Item("Column10") = dtRow.Item("perspective")

                rpt_Goals.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            StrQ = "SELECT DISTINCT " & _
                    " alloc.employeeunkid " & _
                    ",hrassess_group_master.assessgroup_name " & _
                    ",cfcommon_master.name AS cname " & _
                    ",hrassess_competencies_master.name " & _
                    ",hrassess_competence_assign_tran.weight " & _
                    "FROM hrassess_competence_assign_master " & _
                         "JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.assigncompetenceunkid = hrassess_competence_assign_master.assigncompetenceunkid " & _
                         "JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                         "JOIN cfcommon_master ON cfcommon_master.masterunkid  = hrassess_competencies_master.competence_categoryunkid AND cfcommon_master.mastertype = 40 " & _
                         "JOIN hrassess_group_master ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                         "JOIN hrassess_group_tran ON hrassess_group_master.assessgroupunkid = hrassess_group_tran.assessgroupunkid " & _
                         "LEFT JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "hremployee_master.employeeunkid " & _
                                   ",etrf.stationunkid " & _
                                   ",etrf.deptgroupunkid " & _
                                   ",etrf.departmentunkid " & _
                                   ",etrf.sectiongroupunkid " & _
                                   ",etrf.sectionunkid " & _
                                   ",etrf.unitgroupunkid " & _
                                   ",etrf.unitunkid " & _
                                   ",etrf.teamunkid " & _
                                   ",etrf.classgroupunkid " & _
                                   ",etrf.classunkid " & _
                                   ",ect.jobgroupunkid " & _
                                   ",ect.jobunkid " & _
                              "FROM hremployee_master " & _
                              "LEFT JOIN " & _
                              "( " & _
                                   "SELECT " & _
                                         "tr.employeeunkid AS tempid " & _
                                        ",tr.stationunkid " & _
                                        ",tr.deptgroupunkid " & _
                                        ",tr.departmentunkid " & _
                                        ",tr.sectiongroupunkid " & _
                                        ",tr.sectionunkid " & _
                                        ",tr.unitgroupunkid " & _
                                        ",tr.unitunkid " & _
                                        ",tr.teamunkid " & _
                                        ",tr.classgroupunkid " & _
                                        ",tr.classunkid " & _
                                   "FROM " & _
                                   "( " & _
                                        "SELECT " & _
                                              "hremployee_transfer_tran.employeeunkid " & _
                                             ",hremployee_transfer_tran.stationunkid " & _
                                             ",hremployee_transfer_tran.deptgroupunkid " & _
                                             ",hremployee_transfer_tran.departmentunkid " & _
                                             ",hremployee_transfer_tran.sectiongroupunkid " & _
                                             ",hremployee_transfer_tran.sectionunkid " & _
                                             ",hremployee_transfer_tran.unitgroupunkid " & _
                                             ",hremployee_transfer_tran.unitunkid " & _
                                             ",hremployee_transfer_tran.teamunkid " & _
                                             ",hremployee_transfer_tran.classgroupunkid " & _
                                             ",hremployee_transfer_tran.classunkid " & _
                                             ",ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS Rno " & _
                                        "FROM hremployee_transfer_tran " & _
                                        "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                   ") AS tr " & _
                                   "WHERE tr.Rno = 1 " & _
                              ") AS etrf ON hremployee_master.employeeunkid = etrf.tempid " & _
                              "LEFT JOIN " & _
                              "( " & _
                                   "SELECT " & _
                                         "ct.employeeunkid AS tempid " & _
                                        ",ct.jobgroupunkid " & _
                                        ",ct.jobunkid " & _
                                   "FROM " & _
                                   "( " & _
                                        "SELECT " & _
                                              "hremployee_categorization_tran.employeeunkid " & _
                                             ",hremployee_categorization_tran.jobgroupunkid " & _
                                             ",hremployee_categorization_tran.jobunkid " & _
                                             ",ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS Rno " & _
                                        "FROM hremployee_categorization_tran " & _
                                        "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                   ") AS ct " & _
                                   "WHERE ct.Rno = 1 " & _
                              ") AS ect ON ect.tempid = hremployee_master.employeeunkid " & _
                         ") AS alloc ON (CASE WHEN hrassess_group_master.referenceunkid = 1 THEN alloc.stationunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 2 THEN alloc.deptgroupunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 3 THEN alloc.departmentunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 4 THEN alloc.sectiongroupunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 5 THEN alloc.sectionunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 6 THEN alloc.unitgroupunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 7 THEN alloc.unitunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 8 THEN alloc.teamunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 9 THEN alloc.jobgroupunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 10 THEN alloc.jobunkid " & _
                                                  "WHEN hrassess_group_master.referenceunkid = 11 THEN alloc.employeeunkid " & _
                                           "END) = (CASE WHEN hrassess_group_master.referenceunkid = 11 THEN hrassess_group_master.employeeunkid ELSE hrassess_group_tran.allocationunkid END) " & _
                    "WHERE hrassess_competence_assign_master.isvoid = 0 AND hrassess_group_master.isactive = 1 AND hrassess_group_tran.isactive = 1 " & _
                    "AND hrassess_competence_assign_master.periodunkid = @periodunkid AND hrassess_competence_assign_tran.isvoid = 0 "


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Cmpts = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Cmpts.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("assessgroup_name")
                rpt_Row.Item("Column3") = dtRow.Item("cname")
                rpt_Row.Item("Column4") = dtRow.Item("name")
                rpt_Row.Item("Column5") = dtRow.Item("weight")

                rpt_Cmpts.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'StrQ = "SELECT " & _
            '         " scr.employeeunkid " & _
            '         ",CASE WHEN hrassess_computation_master.formula_typeid = 1 THEN @BSC_EMP_TOTAL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 2 THEN @BSC_ASR_TOTAL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 3 THEN @BSC_REV_TOTAL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 4 THEN @CMP_EMP_TOTAL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 5 THEN @CMP_ASR_TOTAL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 6 THEN @CMP_REV_TOTAL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 7 THEN @EMP_OVERALL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 8 THEN @ASR_OVERALL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 9 THEN @REV_OVERALL_SCORE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 10 THEN @POST_TO_PAYROLL_VALUE " & _
            '               "WHEN hrassess_computation_master.formula_typeid = 11 THEN @FINAL_RESULT_SCORE " & _
            '         " END AS ftype " & _
            '         ",scr.formula_value " & _
            '    "FROM hrassess_computation_master " & _
            '         "JOIN " & _
            '         "( " & _
            '              "SELECT DISTINCT " & _
            '                    "hrassess_compute_score_master.employeeunkid " & _
            '                   ",hrassess_computation_master.formula_typeid " & _
            '                   ",hrassess_compute_score_tran.formula_value " & _
            '                   ",hrassess_compute_score_master.periodunkid " & _
            '              "FROM hrassess_computation_master "
            'StrQ &= "JOIN hrassess_compute_score_tran ON hrassess_compute_score_tran.computationunkid = hrassess_computation_master.computationunkid " & _
            '                   "AND hrassess_computation_master.isvoid = 0 " & _
            '              "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid = hrassess_compute_score_tran.computescoremasterunkid " & _
            '                   "AND hrassess_computation_master.periodunkid = @periodunkid AND hrassess_computation_master.isvoid = 0 AND hrassess_compute_score_master.employeeunkid IS NOT NULL "
            'If strEmpIds.Trim.Length > 0 Then
            '    StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hrassess_compute_score_master.employeeunkid "
            'End If
            'StrQ &= "WHERE hrassess_computation_master.isvoid = 0 AND hrassess_computation_master.periodunkid = @periodunkid " & _
            '              "UNION ALL " & _
            '              "SELECT DISTINCT " & _
            '                    "hrassess_compute_score_master.employeeunkid " & _
            '                   ",11 AS formula_typeid " & _
            '                   ",hrassess_compute_score_master.finaloverallscore " & _
            '                   ",hrassess_compute_score_master.periodunkid " & _
            '              "FROM hrassess_compute_score_master "
            'If strEmpIds.Trim.Length > 0 Then
            '    StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hrassess_compute_score_master.employeeunkid "
            'End If

            If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = 0 "
            ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(mblnIsCalibrationSettingActive = True, 1, 0) & " "
            End If


            StrQ &= "SELECT " & _
                     " scr.employeeunkid " & _
                     ",CASE WHEN hrassess_computation_master.formula_typeid = 1 THEN @BSC_EMP_TOTAL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 2 THEN @BSC_ASR_TOTAL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 3 THEN @BSC_REV_TOTAL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 4 THEN @CMP_EMP_TOTAL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 5 THEN @CMP_ASR_TOTAL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 6 THEN @CMP_REV_TOTAL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 7 THEN @EMP_OVERALL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 8 THEN @ASR_OVERALL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 9 THEN @REV_OVERALL_SCORE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 10 THEN @POST_TO_PAYROLL_VALUE " & _
                           "WHEN hrassess_computation_master.formula_typeid = 11 THEN @FINAL_RESULT_SCORE " & _
                     " END AS ftype " & _
                     ",CAST(scr.formula_value AS DECIMAL(36,2)) AS formula_value " & _
                "FROM hrassess_computation_master " & _
                     "JOIN " & _
                     "( " & _
                          "SELECT DISTINCT " & _
                                "hrassess_compute_score_master.employeeunkid " & _
                               ",hrassess_computation_master.formula_typeid " & _
                               ",hrassess_compute_score_tran.formula_value " & _
                               ",hrassess_compute_score_master.periodunkid " & _
                          "FROM hrassess_computation_master "
            StrQ &= "JOIN hrassess_compute_score_tran ON hrassess_compute_score_tran.computationunkid = hrassess_computation_master.computationunkid " & _
                               "AND hrassess_computation_master.isvoid = 0 " & _
                          "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid = hrassess_compute_score_tran.computescoremasterunkid " & _
                               "AND hrassess_computation_master.periodunkid = @periodunkid AND hrassess_computation_master.isvoid = 0 AND hrassess_compute_score_master.employeeunkid IS NOT NULL "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hrassess_compute_score_master.employeeunkid "
            End If
            StrQ &= "WHERE hrassess_computation_master.isvoid = 0 AND hrassess_computation_master.periodunkid = @periodunkid " & _
                          "UNION ALL " & _
                          "SELECT DISTINCT " & _
                                "hrassess_compute_score_master.employeeunkid " & _
                               ",11 AS formula_typeid " & _
                               ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                               "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                               "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                               " END AS formula_value " & _
                               ",hrassess_compute_score_master.periodunkid " & _
                          "FROM hrassess_compute_score_master "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hrassess_compute_score_master.employeeunkid "
            End If
            'S.SANDEEP |27-MAY-2019| -- END

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            '----------------------- REMOVED
            'scr.formula_value
            '----------------------- ADDED
            'CAST(scr.formula_value AS DECIMAL(36,2)) AS formula_value
            'S.SANDEEP |21-AUG-2019| -- END


            StrQ &= "WHERE hrassess_compute_score_master.periodunkid = @periodunkid AND hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_master.employeeunkid IS NOT NULL " & _
                    ")AS scr ON scr.formula_typeid = hrassess_computation_master.formula_typeid AND scr.periodunkid = hrassess_computation_master.periodunkid " & _
                "WHERE hrassess_computation_master.isvoid = 0 AND hrassess_computation_master.periodunkid = @periodunkid "

            objDataOperation.AddParameter("@BSC_EMP_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 616, "BSC Employee Total Score"))
            objDataOperation.AddParameter("@BSC_ASR_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 617, "BSC Assessor Total Score"))
            objDataOperation.AddParameter("@BSC_REV_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 618, "BSC Reviewer Total Score"))
            objDataOperation.AddParameter("@CMP_EMP_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 619, "Competence Employee Total Score"))
            objDataOperation.AddParameter("@CMP_ASR_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 620, "Competence Assessor Total Score"))
            objDataOperation.AddParameter("@CMP_REV_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 621, "Competence Reviewer Total Score"))
            objDataOperation.AddParameter("@EMP_OVERALL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 622, "Employee Overall Score"))
            objDataOperation.AddParameter("@ASR_OVERALL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 623, "Assessor Overall Score"))
            objDataOperation.AddParameter("@REV_OVERALL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 624, "Reviewer Overall Score"))
            objDataOperation.AddParameter("@POST_TO_PAYROLL_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 625, "Post To Payroll Value"))
            objDataOperation.AddParameter("@FINAL_RESULT_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 636, "Final Result Score"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Score = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Score.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("ftype")
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'rpt_Row.Item("Column3") = Convert.ToDouble(dtRow.Item("formula_value")).ToString("######0.#0")

                'S.SANDEEP |24-DEC-2019| -- START
                'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                'rpt_Row.Item("Column3") = Convert.ToDouble(dtRow.Item("formula_value")).ToString("#####################0.#0")
                rpt_Row.Item("Column3") = Rounding.BRound(CDec(dtRow.Item("formula_value")), mDecRoundingFactor).ToString("#####################0.#0")
                'S.SANDEEP |24-DEC-2019| -- END

                'S.SANDEEP |21-AUG-2019| -- END
                rpt_Score.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            StrQ = "SELECT " & _
                   "     CT.employeeunkid " & _
                   "    ,CH.name " & _
                   "    ,CI.custom_item " & _
                   "    ,CASE WHEN EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN @SELF " & _
                   "          WHEN EM.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' THEN @ASSESSOR " & _
                   "          WHEN EM.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' THEN @REVIEWER " & _
                   "     END AS eMode " & _
                   "    ,CAST(ROW_NUMBER()OVER(PARTITION BY EM.assessmodeid,CT.customitemunkid,CT.employeeunkid ORDER BY CT.customitemunkid) AS NVARCHAR(MAX)) AS eNo " & _
                   "    ,CT.custom_value " & _
                   "    ,EM.assessmodeid " & _
                   "    ,CI.viewmodeid " & _
                   "    ,CI.itemtypeid " & _
                   "    ,CI.customitemunkid " & _
                   "    ,CI.selectionmodeid " & _
                   "FROM hrassess_custom_items CI " & _
                   "    JOIN hrassess_custom_headers CH ON CH.customheaderunkid = CI.customheaderunkid " & _
                   "    LEFT JOIN hrcompetency_customitem_tran CT ON CT.customitemunkid = CI.customitemunkid AND CI.periodunkid = CT.periodunkid AND CT.custom_value <> '' " & _
                   "    JOIN hrevaluation_analysis_master EM ON CT.analysisunkid = EM.analysisunkid AND CI.periodunkid = EM.periodunkid "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = CT.employeeunkid "
            End If
            StrQ &= "WHERE CI.isactive = 1 AND CH.isactive = 1 AND CI.periodunkid = @periodunkid AND CT.isvoid = 0 AND EM.isvoid = 0 " & _
                    "ORDER BY CT.employeeunkid,CH.name,EM.assessmodeid "

            objDataOperation.AddParameter("@SELF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 519, "SELF"))
            objDataOperation.AddParameter("@ASSESSOR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 520, "ASSESSOR"))
            objDataOperation.AddParameter("@REVIEWER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 521, "REVIEWER"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables("DataTable").Rows.Count <= 0 Then
                StrQ = "SELECT " & _
                       "     CP.employeeunkid " & _
                       "    ,CH.name " & _
                       "    ,CI.custom_item " & _
                       "    ,CAST(ROW_NUMBER()OVER(PARTITION BY CI.customitemunkid ORDER BY CI.customitemunkid) AS NVARCHAR(MAX)) AS eNo " & _
                       "    ,CP.custom_value " & _
                       "    ,CI.viewmodeid " & _
                       "    ,CI.itemtypeid " & _
                       "    ,'' AS eMode " & _
                       "    ,CI.selectionmodeid " & _
                       "FROM hrassess_custom_items CI " & _
                       "    JOIN hrassess_custom_headers CH ON CH.customheaderunkid = CI.customheaderunkid " & _
                       "    JOIN hrassess_plan_customitem_tran CP ON CP.customitemunkid = CI.customitemunkid "
                If strEmpIds.Trim.Length > 0 Then
                    StrQ &= " JOIN #EmpTab ON #EmpTab.empid = CP.employeeunkid "
                End If
                StrQ &= "WHERE CI.isactive = 1 AND CH.isactive = 1 AND CI.periodunkid = @periodunkid " & _
                        "ORDER BY CI.customitemunkid "

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

            End If

            rpt_Items = New ArutiReport.Designer.dsArutiReport
            Dim strColValue As String = String.Empty
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                strColValue = ""
                Dim rpt_Row As DataRow = rpt_Items.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("name")
                rpt_Row.Item("Column3") = dtRow.Item("custom_item")
                If dtRow.Item("eMode").ToString.Trim.Length > 0 Then
                    strColValue = dtRow.Item("eMode") & " -> "
                End If
                strColValue &= dtRow.Item("eNo") & ". "
                Select Case CInt(dtRow.Item("itemtypeid"))
                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                        strColValue &= dtRow.Item("custom_value")
                    Case clsassess_custom_items.enCustomType.SELECTION
                        Select Case CInt(dtRow.Item("selectionmodeid"))
                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                Dim objCMaster As New clsCommon_Master
                                objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                strColValue &= objCMaster._Name
                                objCMaster = Nothing
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                Dim objCMaster As New clsCommon_Master
                                objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                strColValue &= objCMaster._Name
                                objCMaster = Nothing
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                Dim objEmpField1 As New clsassess_empfield1_master
                                objEmpField1._Empfield1unkid = CInt(dtRow.Item("custom_value"))
                                strColValue &= objEmpField1._Field_Data
                                objEmpField1 = Nothing

                                'S.SANDEEP |16-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                Dim objCMaster As New clsCommon_Master
                                objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                strColValue &= objCMaster._Name
                                objCMaster = Nothing
                                'S.SANDEEP |16-AUG-2019| -- END
                        End Select
                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                        strColValue &= eZeeDate.convertDate(dtRow.Item("custom_value").ToString).ToShortDateString
                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                        strColValue &= CDbl(dtRow.Item("custom_value")).ToString()
                End Select
                rpt_Row.Item("Column4") = strColValue
                rpt_Items.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            'S.SANDEEP [28-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REPORT ENHANCEMENT
            StrQ = "SELECT " & _
                   "     B.employeeunkid " & _
                   "    ,B.agroup " & _
                   "    ,B.amode " & _
                   "    ,B.amvalue " & _
                   "    ,B.ecmmt " & _
                   "    ,B.emmtvalue " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_master.employeeunkid " & _
                   "        ,ISNULL(G.agroup,@PlanningStatus) AS agroup " & _
                   "        ,ISNULL(G.amode,@NotPlanned) AS amode " & _
                   "        ,'' AS amvalue " & _
                   "        ,'' AS ecmmt " & _
                   "        ,'' AS emmtvalue " & _
                   "        ,0 AS assessmodeid " & _
                   "    FROM hremployee_master "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hremployee_master.employeeunkid "
            End If
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hrassess_empstatus_tran.employeeunkid " & _
                    "       ,@PlanningStatus agroup " & _
                    "       ,CASE WHEN statustypeid = 1 THEN @SubmittedForApproval " & _
                    "             WHEN statustypeid = 2 THEN @Approved " & _
                    "             WHEN statustypeid = 3 THEN @OpenForChanges " & _
                    "             WHEN statustypeid = 4 THEN @NotSubmitted " & _
                    "             WHEN statustypeid = 5 THEN @NotCommitted " & _
                    "             WHEN statustypeid = 6 THEN @FinallyCommitted " & _
                    "             WHEN statustypeid = 7 THEN @OpenForPeriodicReview " & _
                    "        END AS amode " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hrassess_empstatus_tran.employeeunkid,hrassess_empstatus_tran.periodunkid ORDER BY hrassess_empstatus_tran.status_date DESC) AS rno " & _
                    "   FROM hrassess_empstatus_tran " & _
                    "   WHERE hrassess_empstatus_tran.periodunkid = @periodunkid " & _
                    ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.rno = 1 " & _
                    "UNION " & _
                    "   SELECT " & _
                    "        A.employeeunkid " & _
                    "       ,A.agroup " & _
                    "       ,A.amode " & _
                    "       ,A.amvalue " & _
                    "       ,A.ecmmt " & _
                    "       ,A.emmtvalue " & _
                    "       ,A.assessmodeid " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            hremployee_master.employeeunkid " & _
                    "           ,@Assessment AS agroup " & _
                    "           ,@Self AS amode " & _
                    "           ,CASE WHEN ISNULL(S.assessmodeid,0) <> 0 THEN S.evalue ELSE @NotDone END AS amvalue " & _
                    "           ,CASE WHEN ISNULL(S.assessmodeid,0) <> 0 THEN @IsCommitted ELSE '' END AS ecmmt " & _
                    "           ,CASE WHEN ISNULL(S.assessmodeid,0) <> 0 THEN S.emode ELSE '' END AS emmtvalue " & _
                    "           ,ISNULL(S.assessmodeid,1) AS assessmodeid " & _
                    "       FROM hremployee_master "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hremployee_master.employeeunkid "
            End If
            StrQ &= "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            em.selfemployeeunkid " & _
                    "           ,@Yes as evalue " & _
                    "           ,CASE WHEN em.iscommitted = 1 THEN @Yes ELSE @No END AS emode " & _
                    "           ,em.assessmodeid " & _
                    "       FROM hrevaluation_analysis_master AS em " & _
                    "       WHERE em.isvoid = 0 AND em.assessmodeid = 1 AND em.periodunkid = @periodunkid " & _
                    "    ) AS S ON S.selfemployeeunkid = hremployee_master.employeeunkid " & _
                    "    UNION " & _
                    "       SELECT " & _
                    "            hremployee_master.employeeunkid " & _
                    "           ,@Assessment AS agroup " & _
                    "           ,@Assessor AS amode " & _
                    "           ,CASE WHEN ISNULL(A.assessmodeid,0) <> 0 THEN A.evalue ELSE @NotDone END AS amvalue " & _
                    "           ,CASE WHEN ISNULL(A.assessmodeid,0) <> 0 THEN @IsCommitted ELSE '' END AS ecmmt " & _
                    "           ,CASE WHEN ISNULL(A.assessmodeid,0) <> 0 THEN A.emode ELSE '' END AS emmtvalue " & _
                    "           ,ISNULL(A.assessmodeid,2) AS assessmodeid " & _
                    "       FROM hremployee_master "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hremployee_master.employeeunkid "
            End If
            StrQ &= "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            em.assessedemployeeunkid " & _
                    "           ,@Yes as evalue " & _
                    "           ,CASE WHEN em.iscommitted = 1 THEN @Yes ELSE @No END AS emode " & _
                    "           ,em.assessmodeid " & _
                    "       FROM hrevaluation_analysis_master AS em " & _
                    "       WHERE em.isvoid = 0 AND em.assessmodeid = 2 AND em.periodunkid = @periodunkid " & _
                    "    ) AS A ON A.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                    "    UNION " & _
                    "       SELECT " & _
                    "            hremployee_master.employeeunkid " & _
                    "           ,@Assessment AS agroup " & _
                    "           ,@Reviewer AS amode " & _
                    "           ,CASE WHEN ISNULL(R.assessmodeid,0) <> 0 THEN R.evalue ELSE @NotDone END AS amvalue " & _
                    "           ,CASE WHEN ISNULL(R.assessmodeid,0) <> 0 THEN @IsCommitted ELSE '' END AS ecmmt " & _
                    "           ,CASE WHEN ISNULL(R.assessmodeid,0) <> 0 THEN R.emode ELSE '' END AS emmtvalue " & _
                    "           ,ISNULL(R.assessmodeid,3) AS assessmodeid " & _
                    "       FROM hremployee_master "
            If strEmpIds.Trim.Length > 0 Then
                StrQ &= " JOIN #EmpTab ON #EmpTab.empid = hremployee_master.employeeunkid "
            End If
            StrQ &= "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            em.assessedemployeeunkid " & _
                    "           ,@Yes as evalue " & _
                    "           ,CASE WHEN em.iscommitted = 1 THEN @Yes ELSE @No END AS emode " & _
                    "           ,em.assessmodeid " & _
                    "       FROM hrevaluation_analysis_master AS em " & _
                    "       WHERE em.isvoid = 0 AND em.assessmodeid = 3 AND em.periodunkid = @periodunkid " & _
                    "    ) AS R ON R.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                    ") AS A) AS B WHERE 1 = 1 ORDER BY B.employeeunkid,B.assessmodeid "

            objDataOperation.AddParameter("@PlanningStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 525, "Planning Status"))
            objDataOperation.AddParameter("@NotPlanned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Not Planned"))
            objDataOperation.AddParameter("@SubmittedForApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Submitted For Approval"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 526, "Approved"))
            objDataOperation.AddParameter("@OpenForChanges", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Opened For Changes"))
            objDataOperation.AddParameter("@NotSubmitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Not Submitted For Approval"))
            objDataOperation.AddParameter("@NotCommitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 49, "Not Committed"))
            objDataOperation.AddParameter("@FinallyCommitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 50, "Final Committed"))
            objDataOperation.AddParameter("@OpenForPeriodicReview", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "Periodic Review"))
            objDataOperation.AddParameter("@NotDone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 527, "Not Done"))
            objDataOperation.AddParameter("@IsCommitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 528, "Is Committed"))
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 530, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 531, "No"))
            objDataOperation.AddParameter("@Assessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 535, "Assessment Mode"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Status = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Status.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("agroup")
                rpt_Row.Item("Column3") = dtRow.Item("amode")
                rpt_Row.Item("Column4") = dtRow.Item("amvalue").ToString()
                rpt_Row.Item("Column5") = dtRow.Item("ecmmt").ToString()
                rpt_Row.Item("Column6") = dtRow.Item("emmtvalue").ToString()

                rpt_Status.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next
            'S.SANDEEP [28-SEP-2017] -- END

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            objRpt = New ArutiReport.Designer.rptEmplyeePlanningReport

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_EmpData.Tables("ArutiImage").NewRow

            ReportFunction.Logo_Display(objRpt, _
                                       ConfigParameter._Object._IsDisplayLogo, _
                                       ConfigParameter._Object._ShowLogoRightSide, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "txtCompanyName", _
                                       "txtReportName", _
                                       "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_EmpData.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_EmpData.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_EmpData.Tables("ArutiTable").Rows.Add("")
            End If


            objRpt.SetDataSource(rpt_EmpData)
            objRpt.Subreports("rptEmpCompetencies").SetDataSource(rpt_Cmpts)
            objRpt.Subreports("rptEmpGoals").SetDataSource(rpt_Goals)
            objRpt.Subreports("rptComputedScore").SetDataSource(rpt_Score)
            objRpt.Subreports("rptCustomItems").SetDataSource(rpt_Items)
            'S.SANDEEP [28-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REPORT ENHANCEMENT
            'If mblnShowComputedScore = False Then
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", False)
            'End If
            objRpt.Subreports("rptAssessmentStatus").SetDataSource(rpt_Status)

            If mblnShowComputedScore = False Then
                Call ReportFunction.EnableSuppress(objRpt, "rptComputedScore", True)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "rptComputedScore", False)
            End If
            'S.SANDEEP [28-SEP-2017] -- END

            Call ReportFunction.TextChange(objRpt, "lblEmployeeDetails", Language.getMessage(mstrModuleName, 500, "Employee Details"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 517, "Emp. Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 518, "Employee Name :"))
            Call ReportFunction.TextChange(objRpt, "txtAppDate", Language.getMessage(mstrModuleName, 501, "Appointed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 502, "Department :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 503, "Job :"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptComputedScore"), "lblComputedScores", Language.getMessage(mstrModuleName, 504, "Computed Score"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptComputedScore"), "txtFormula", Language.getMessage(mstrModuleName, 505, "Formula"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptComputedScore"), "txtScore", Language.getMessage(mstrModuleName, 506, "Score"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpCompetencies"), "lblEmployeeCompetencies", Language.getMessage(mstrModuleName, 507, "Employee Competencies"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpCompetencies"), "txtAssessmentGroup", Language.getMessage(mstrModuleName, 508, "Assessment Group :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpCompetencies"), "txtCompCategory", Language.getMessage(mstrModuleName, 509, "Competence Category :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpCompetencies"), "txtItem", Language.getMessage(mstrModuleName, 510, "Competence Item"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpCompetencies"), "txtWeight", Language.getMessage(mstrModuleName, 511, "Weight"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "lblEmployeeGoals", Language.getMessage(mstrModuleName, 512, "Employee Goals"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtWeight", Language.getMessage(mstrModuleName, 513, "Weight"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtPercent", Language.getMessage(mstrModuleName, 514, "Percent"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtStatus", Language.getMessage(mstrModuleName, 515, "Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtPerspective", Language.getMessage(mstrModuleName, 516, "Perspective :"))

            Dim objFMaster As New clsAssess_Field_Master(True)
            Dim mdtFields As DataTable = objFMaster.Get_Fields_Details()
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtFieldCaption1", objFMaster._Field1_Caption)
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtFieldCaption2", objFMaster._Field2_Caption)
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtFieldCaption3", objFMaster._Field3_Caption)
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtFieldCaption4", objFMaster._Field4_Caption)
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmpGoals"), "txtFieldCaption5", objFMaster._Field5_Caption)


            Call ReportFunction.TextChange(objRpt.Subreports("rptCustomItems"), "lblCustomItem", Language.getMessage(mstrModuleName, 522, "Custom Items"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCustomItems"), "txtCustomHeader", Language.getMessage(mstrModuleName, 523, "Custom Header :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCustomItems"), "txtCustomItem", Language.getMessage(mstrModuleName, 524, "Custom Item :"))

            'S.SANDEEP [28-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REPORT ENHANCEMENT
            Call ReportFunction.TextChange(objRpt.Subreports("rptAssessmentStatus"), "txtMode", Language.getMessage(mstrModuleName, 532, "Mode"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAssessmentStatus"), "txtAssessMode", Language.getMessage(mstrModuleName, 533, "Status(s)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAssessmentStatus"), "lblStatus", Language.getMessage(mstrModuleName, 534, "Assessment/Planning Status"))

            Call ReportFunction.TextChange(objRpt, "txtAssessor", Language.getMessage(mstrModuleName, 536, "Assessor :"))
            Call ReportFunction.TextChange(objRpt, "txtReviewer", Language.getMessage(mstrModuleName, 537, "Reviewer :"))
            'S.SANDEEP [28-SEP-2017] -- END

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 47, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 17, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Language.getMessage(mstrModuleName, 6, "Period :") & " " & mstrPeriodName & " ")
            If mblnShowComputedScore AndAlso mblnIsCalibrationSettingActive Then
                Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Language.getMessage(mstrModuleName, 6, "Period :") & " " & mstrPeriodName & " " & _
                                           Language.getMessage(mstrModuleName, 52, "Display Score Type") & " : " & mstrDisplayScoreName)
            Else
                Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Language.getMessage(mstrModuleName, 6, "Period :") & " " & mstrPeriodName & " ")
            End If
            'S.SANDEEP |27-MAY-2019| -- END


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmployeeReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [05-JUN-2017] -- END


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal dtDatabase_Start_Date As Date _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rptSubreport_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'StrQ = "SELECT " & _
            '       "	 ECode " & _
            '       "	,EName " & _
            '       "	,Job " & _
            '       "	,Dept " & _
            '       "	,Class " & _
            '       "	,BSC_StatusId " & _
            '       "	,BSC_Status " & _
            '       "	,PName " & _
            '       "	,Financial_Year " & _
            '       "	,Status_Date " & _
            '       "	,sort_date " & _
            '       "    ,Id " & _
            '       "    ,GName " & _
            '       "FROM " & _
            '       "( " & _
            '       "		SELECT " & _
            '       "			 ISNULL(employeecode,'') AS ECode " & _
            '       "			,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS EName " & _
            '       "			,ISNULL(job.job_name, '') AS Job " & _
            '       "			,ISNULL(dept.name, '') AS Dept " & _
            '       "			,ISNULL(class.name, '') AS Class " & _
            '       "			,ISNULL(statustypeid,'') AS BSC_StatusId " & _
            '       "			,CASE WHEN statustypeid = '" & enObjective_Status.SUBMIT_APPROVAL & "' THEN @Submit_Approval " & _
            '       "				  WHEN statustypeid = '" & enObjective_Status.FINAL_SAVE & "' THEN @Final_Save " & _
            '       "				  WHEN statustypeid = '" & enObjective_Status.OPEN_CHANGES & "' THEN @Open_Changes " & _
            '       "				  WHEN statustypeid = '" & enObjective_Status.NOT_SUBMIT & "' THEN @Not_Submit " & _
            '       "				  WHEN statustypeid = '" & enObjective_Status.NOT_COMMITTED & "' THEN @NOT_COMMITTED " & _
            '       "				  WHEN statustypeid = '" & enObjective_Status.FINAL_COMMITTED & "' THEN @FINAL_COMMITTED " & _
            '       "				  WHEN statustypeid = '" & enObjective_Status.PERIODIC_REVIEW & "' THEN @PERIODIC_REVIEW " & _
            '       "			 END AS BSC_Status " & _
            '       "			,ISNULL(period_name,'') AS PName " & _
            '       "			,ISNULL(financialyear_name,'') AS Financial_Year " & _
            '       "			,statustypeid " & _
            '       "			,hremployee_master.employeeunkid " & _
            '       "			,cfcommon_period_tran.periodunkid " & _
            '       "			,CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
            '       "			,status_date AS sort_date " & _
            '       "			,ROW_NUMBER() OVER(PARTITION BY hrassess_empstatus_tran.employeeunkid,hrassess_empstatus_tran.periodunkid ORDER BY statustranunkid DESC) AS RNo "
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            'StrQ &= "		FROM hrassess_empstatus_tran " & _
            '        "			JOIN cfcommon_period_tran ON hrassess_empstatus_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = '" & enModuleReference.Assessment & "' " & _
            '        "			JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
            '        "			JOIN hremployee_master ON hrassess_empstatus_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "           LEFT JOIN hrjob_master AS job ON hremployee_master.jobunkid = job.jobunkid " & _
            '        "           LEFT JOIN hrdepartment_master AS dept ON dept.departmentunkid = hremployee_master.departmentunkid " & _
            '        "           LEFT JOIN hrclasses_master AS class ON class.classesunkid = hremployee_master.classunkid "
            'StrQ &= mstrAnalysis_Join
            'StrQ &= "		WHERE cfcommon_period_tran.periodunkid = @PeriodId " & _
            '        "			AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        "			AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        "			AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        "			AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            'Select Case mintAppointmentTypeId
            '    Case enAD_Report_Parameter.APP_DATE_FROM
            '        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
            '            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
            '        End If
            '    Case enAD_Report_Parameter.APP_DATE_BEFORE
            '        If mdtDate1 <> Nothing Then
            '            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
            '        End If
            '    Case enAD_Report_Parameter.APP_DATE_AFTER
            '        If mdtDate1 <> Nothing Then
            '            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
            '        End If
            'End Select

            ''S.SANDEEP [ 08 OCT 2014 ] -- START
            ''Issue {07 - Oct - 2014} : [DENNIS] BSC Planning Report-> Status Not Planned is misbehaving Instead of picking employee who have not planned in July 2013(current Assessment period) its only check those who have not planned in previous period(July 2012), it seems developer had hardcoded that report.
            ''StrQ &= "	UNION ALL " & _
            ''     "		SELECT " & _
            ''     "			 ISNULL(employeecode,'') AS ECode " & _
            ''     "			,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS EName " & _
            ''     "			,ISNULL(job.job_name, '') AS Job " & _
            ''     "			,ISNULL(dept.name, '') AS Dept " & _
            ''     "			,ISNULL(class.name, '') AS Class " & _
            ''     "			,999 AS BSC_StatusId " & _
            ''     "			,@Not_Planned AS BSC_Status " & _
            ''     "			,ISNULL(period_name,'') AS PName " & _
            ''     "			,ISNULL(financialyear_name,'') AS Financial_Year " & _
            ''     "			,0 AS statustypeid " & _
            ''     "			,hremployee_master.employeeunkid " & _
            ''     "			,cfcommon_period_tran.periodunkid " & _
            ''     "			,'' AS Status_Date " & _
            ''     "			,NULL AS sort_date " & _
            ''     "			,1 AS RNo "
            'Dim dsYearName As New DataSet
            'Dim objPrd As New clscommom_period_Tran
            'objPrd._Periodunkid = mintPeriodId
            'dsYearName = objPrd.GetList("List", enModuleReference.Assessment, True, , , True, objPrd._Yearunkid)
            'Dim strYearName As String = ""
            'If dsYearName.Tables(0).Rows.Count > 0 Then
            '    Dim dtmp() As DataRow = dsYearName.Tables(0).Select("periodunkid = '" & mintPeriodId & "'")
            '    If dtmp.Length > 0 Then
            '        strYearName = dtmp(0).Item("year")
            '    End If
            'End If
            'StrQ &= "	UNION ALL " & _
            '        "		SELECT " & _
            '        "			 ISNULL(employeecode,'') AS ECode " & _
            '        "			,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS EName " & _
            '        "			,ISNULL(job.job_name, '') AS Job " & _
            '        "			,ISNULL(dept.name, '') AS Dept " & _
            '        "			,ISNULL(class.name, '') AS Class " & _
            '        "			,999 AS BSC_StatusId " & _
            '        "			,@Not_Planned AS BSC_Status " & _
            '     "			,'" & objPrd._Period_Name & "' AS PName " & _
            '     "			," & IIf(strYearName.Trim.Length <= 0, "''", strYearName) & " AS Financial_Year " & _
            '        "			,0 AS statustypeid " & _
            '        "			,hremployee_master.employeeunkid " & _
            '     "			," & mintPeriodId & " AS periodunkid " & _
            '        "			,'' AS Status_Date " & _
            '        "			,NULL AS sort_date " & _
            '        "			,1 AS RNo "

            'objPrd = Nothing
            ''S.SANDEEP [ 08 OCT 2014 ] -- END
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            ''Sohail (12 Apr 2013) -- Start
            ''TRA - ENHANCEMENT
            ''StrQ &= "		FROM hremployee_master " & _
            ''        "			JOIN hrobjective_status_tran ON hremployee_master.employeeunkid <> hrobjective_status_tran.employeeunkid " & _
            ''        "			JOIN cfcommon_period_tran ON hrobjective_status_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 5 " & _
            ''        "			JOIN hrmsConfiguration..cffinancial_year_tran ON hrobjective_status_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid "

            ''S.SANDEEP [ 08 OCT 2014 ] -- START
            ''Issue {07 - Oct - 2014} : [DENNIS] BSC Planning Report-> Status Not Planned is misbehaving Instead of picking employee who have not planned in July 2013(current Assessment period) its only check those who have not planned in previous period(July 2012), it seems developer had hardcoded that report.
            ''StrQ &= "		FROM hremployee_master " & _
            ''        "			LEFT JOIN hrobjective_status_tran ON hremployee_master.employeeunkid = hrobjective_status_tran.employeeunkid " & _
            ''        "			LEFT JOIN cfcommon_period_tran ON hrobjective_status_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 5 " & _
            ''        "			LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON hrobjective_status_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
            ''        "          LEFT JOIN hrjob_master AS job ON hremployee_master.jobunkid = job.jobunkid " & _
            ''        "          LEFT JOIN hrdepartment_master AS dept ON dept.departmentunkid = hremployee_master.departmentunkid " & _
            ''        "          LEFT JOIN hrclasses_master AS class ON class.classesunkid = hremployee_master.classunkid "

            'StrQ &= "		FROM hremployee_master " & _
            '        "           LEFT JOIN hrjob_master AS job ON hremployee_master.jobunkid = job.jobunkid " & _
            '        "           LEFT JOIN hrdepartment_master AS dept ON dept.departmentunkid = hremployee_master.departmentunkid " & _
            '        "           LEFT JOIN hrclasses_master AS class ON class.classesunkid = hremployee_master.classunkid "
            'StrQ &= mstrAnalysis_Join
            ''S.SANDEEP [ 08 OCT 2014 ] -- START
            ''Issue {07 - Oct - 2014} : [DENNIS] BSC Planning Report-> Status Not Planned is misbehaving Instead of picking employee who have not planned in July 2013(current Assessment period) its only check those who have not planned in previous period(July 2012), it seems developer had hardcoded that report.
            ''StrQ &= "		WHERE ISNULL(cfcommon_period_tran.periodunkid, @PeriodId ) = @PeriodId " & _
            ''        "			AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            ''        "			AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''        "			AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''        "			AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate " & _
            ''        "          AND hrobjective_status_tran.employeeunkid IS NULL "

            'StrQ &= "		WHERE hremployee_master.employeeunkid NOT IN(SELECT DISTINCT employeeunkid FROM hrassess_empstatus_tran WHERE periodunkid = @PeriodId) " & _
            '        "			AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        "			AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        "			AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        "			AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            ''S.SANDEEP [ 08 OCT 2014 ] -- END

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            'Select Case mintAppointmentTypeId
            '    Case enAD_Report_Parameter.APP_DATE_FROM
            '        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
            '            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
            '        End If
            '    Case enAD_Report_Parameter.APP_DATE_BEFORE
            '        If mdtDate1 <> Nothing Then
            '            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
            '        End If
            '    Case enAD_Report_Parameter.APP_DATE_AFTER
            '        If mdtDate1 <> Nothing Then
            '            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
            '        End If
            'End Select

            'If mintStatusId > 0 AndAlso mintStatusId <> 999 Then
            '    StrQ &= " AND 1 = 2 "
            'End If
            'StrQ &= ") AS BSC_APPR WHERE RNo = 1 "

            'If mintStatusId > 0 Then
            '    StrQ &= " AND BSC_StatusId = @StatusId "
            'End If

            If mblnExcludeInactiveEmployee = False Then
                If mblnConsiderEmployeeTerminationOnPeriodDate = True Then

                    dtPeriodStart = mdtPeriod_St_Date
                    dtPeriodEnd = mdtPeriod_Ed_Date

                Else

                    dtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    dtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)

                End If
            Else

                dtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)

            End If

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            StrQ = "SELECT " & _
                   "	 ECode " & _
                   "	,EName " & _
                   "	,Job " & _
                   "	,Dept " & _
                   "	,Class " & _
                   "	,BSC_StatusId " & _
                   "	,BSC_Status " & _
                   "	,PName " & _
                   "	,Financial_Year " & _
                   "	,Status_Date " & _
                   "	,sort_date " & _
                   "    ,Id " & _
                   "    ,GName " & _
                   "FROM " & _
                   "( " & _
                   "		SELECT " & _
                   "			 ISNULL(employeecode,'') AS ECode " & _
                   "			,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS EName " & _
                   "			,ISNULL(job.job_name, '') AS Job " & _
                   "			,ISNULL(dept.name, '') AS Dept " & _
                   "			,ISNULL(class.name, '') AS Class " & _
                   "			,ISNULL(statustypeid,'') AS BSC_StatusId " & _
                   "			,CASE WHEN statustypeid = '" & enObjective_Status.SUBMIT_APPROVAL & "' THEN @Submit_Approval " & _
                   "				  WHEN statustypeid = '" & enObjective_Status.FINAL_SAVE & "' THEN @Final_Save " & _
                   "				  WHEN statustypeid = '" & enObjective_Status.OPEN_CHANGES & "' THEN @Open_Changes " & _
                   "				  WHEN statustypeid = '" & enObjective_Status.NOT_SUBMIT & "' THEN @Not_Submit " & _
                   "				  WHEN statustypeid = '" & enObjective_Status.NOT_COMMITTED & "' THEN @NOT_COMMITTED " & _
                   "				  WHEN statustypeid = '" & enObjective_Status.FINAL_COMMITTED & "' THEN @FINAL_COMMITTED " & _
                   "				  WHEN statustypeid = '" & enObjective_Status.PERIODIC_REVIEW & "' THEN @PERIODIC_REVIEW " & _
                   "			 END AS BSC_Status " & _
                   "			,ISNULL(period_name,'') AS PName " & _
                   "			,ISNULL(financialyear_name,'') AS Financial_Year " & _
                   "			,statustypeid " & _
                   "			,hremployee_master.employeeunkid " & _
                   "			,cfcommon_period_tran.periodunkid " & _
                   "			,CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
                   "			,status_date AS sort_date " & _
                   "			,ROW_NUMBER() OVER(PARTITION BY hrassess_empstatus_tran.employeeunkid,hrassess_empstatus_tran.periodunkid ORDER BY statustranunkid DESC) AS RNo "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "		FROM hrassess_empstatus_tran " & _
                    "			JOIN cfcommon_period_tran ON hrassess_empstatus_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = '" & enModuleReference.Assessment & "' " & _
                    "			JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                    "			JOIN hremployee_master ON hrassess_empstatus_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "           LEFT JOIN " & _
                    "           (" & _
                    "               SELECT " & _
                    "                    employeeunkid " & _
                    "                   ,classunkid " & _
                    "                   ,departmentunkid " & _
                    "                   ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "               FROM hremployee_transfer_tran " & _
                    "               WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "           ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                    "           LEFT JOIN  hrclasses_master AS class ON Alloc.classunkid = class.classesunkid " & _
                    "           JOIN hrdepartment_master AS dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                    "           LEFT JOIN " & _
                    "           (" & _
                    "               SELECT " & _
                    "                    employeeunkid " & _
                    "                   ,jobunkid " & _
                    "                   ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "               FROM hremployee_categorization_tran " & _
                    "               WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "           ) AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                    "           JOIN hrjob_master AS job ON Catr.jobunkid = job.jobunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE cfcommon_period_tran.periodunkid = @PeriodId "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If


            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select

            Dim dsYearName As New DataSet
            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = mintPeriodId
            objPrd._Periodunkid(strDatabaseName) = mintPeriodId
            'Sohail (21 Aug 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsYearName = objPrd.GetList("List", enModuleReference.Assessment, True, , , True, objPrd._Yearunkid)
            dsYearName = objPrd.GetList("List", enModuleReference.Assessment, objPrd._Yearunkid, dtDatabase_Start_Date, True, , , True)
            'Sohail (21 Aug 2015) -- End

            Dim strYearName As String = ""
            If dsYearName.Tables(0).Rows.Count > 0 Then
                Dim dtmp() As DataRow = dsYearName.Tables(0).Select("periodunkid = '" & mintPeriodId & "'")
                If dtmp.Length > 0 Then
                    strYearName = dtmp(0).Item("year")
                End If
            End If

            StrQ &= "	UNION ALL " & _
                    "		SELECT " & _
                    "			 ISNULL(employeecode,'') AS ECode " & _
                    "			,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS EName " & _
                    "			,ISNULL(job.job_name, '') AS Job " & _
                    "			,ISNULL(dept.name, '') AS Dept " & _
                    "			,ISNULL(class.name, '') AS Class " & _
                    "			,999 AS BSC_StatusId " & _
                    "			,@Not_Planned AS BSC_Status " & _
                    "			,'" & objPrd._Period_Name & "' AS PName " & _
                    "			," & IIf(strYearName.Trim.Length <= 0, "''", "'" & strYearName & "'") & " AS Financial_Year " & _
                    "			,0 AS statustypeid " & _
                    "			,hremployee_master.employeeunkid " & _
                    "			," & mintPeriodId & " AS periodunkid " & _
                    "			,'' AS Status_Date " & _
                    "			,NULL AS sort_date " & _
                    "			,1 AS RNo "

            objPrd = Nothing

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "		FROM hremployee_master " & _
                    "       LEFT JOIN " & _
                    "       (" & _
                    "           SELECT " & _
                    "                employeeunkid " & _
                    "               ,classunkid " & _
                    "               ,departmentunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "           FROM hremployee_transfer_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                    "       LEFT JOIN  hrclasses_master AS class ON Alloc.classunkid = class.classesunkid " & _
                    "       JOIN hrdepartment_master AS dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                    "       LEFT JOIN " & _
                    "       (" & _
                    "           SELECT " & _
                    "                employeeunkid " & _
                    "               ,jobunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "           FROM hremployee_categorization_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                    "       JOIN hrjob_master AS job ON Catr.jobunkid = job.jobunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE hremployee_master.employeeunkid NOT IN(SELECT DISTINCT employeeunkid FROM hrassess_empstatus_tran WHERE periodunkid = @PeriodId) "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select

            If mintStatusId > 0 AndAlso mintStatusId <> 999 Then
                StrQ &= " AND 1 = 2 "
            End If
            StrQ &= ") AS BSC_APPR WHERE RNo = 1 "

            If mintStatusId > 0 Then
                StrQ &= " AND BSC_StatusId = @StatusId "
            End If

            'S.SANDEEP [04 JUN 2015] -- END



            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("ECode")
                rpt_Row.Item("Column3") = dtRow.Item("EName")
                rpt_Row.Item("Column4") = dtRow.Item("BSC_Status")
                rpt_Row.Item("Column5") = dtRow.Item("PName")
                rpt_Row.Item("Column6") = dtRow.Item("Financial_Year")
                If dtRow.Item("Status_Date").ToString.Trim.Length > 0 Then
                    rpt_Row.Item("Column7") = eZeeDate.convertDate(dtRow.Item("Status_Date").ToString).ToShortDateString
                Else
                    rpt_Row.Item("Column7") = ""
                End If
                rpt_Row.Item("Column81") = dtRow.Item("BSC_StatusId")
                rpt_Row.Item("Column8") = dtRow.Item("Job")
                rpt_Row.Item("Column9") = dtRow.Item("Dept")
                rpt_Row.Item("Column10") = dtRow.Item("Class")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            objRpt = New ArutiReport.Designer.rpt_BSC_Planning_Report

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow

            ReportFunction.Logo_Display(objRpt, _
                                       ConfigParameter._Object._IsDisplayLogo, _
                                       ConfigParameter._Object._ShowLogoRightSide, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "txtCompanyName", _
                                       "txtReportName", _
                                       "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            objRpt.SetDataSource(rpt_Data)

            If mblnIncludeSummary = True Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '        StrQ = "SELECT  BSC_Status " & _
                '                      ", Ord_No " & _
                '                      ", CAST(COUNT(ISNULL(RNo, 0)) AS DECIMAL(10, 2)) AS Cnt " & _
                '                      ", CAST(( SELECT   COUNT(employeeunkid) " & _
                '                               "FROM     hremployee_master " & _
                '                               "WHERE    CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
                '                                        "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
                '                                        "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
                '                                        "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "

                '        If mstrAdvance_Filter.Trim.Length > 0 Then
                '            StrQ &= " AND " & mstrAdvance_Filter
                '        End If
                '        If mstrUserAccessFilter = "" Then
                '            If UserAccessLevel._AccessLevel.Length > 0 Then
                '                StrQ &= UserAccessLevel._AccessLevelFilterString
                '            End If
                '        Else
                '            StrQ &= mstrUserAccessFilter
                '        End If
                '        If mintEmployeeId > 0 Then
                '            StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                '        End If

                '        Select Case mintAppointmentTypeId
                '            Case enAD_Report_Parameter.APP_DATE_FROM
                '                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_BEFORE
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_AFTER
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '        End Select

                '        StrQ &= ") AS DECIMAL(10, 2)) AS Total_Emp " & _
                '                      ", CAST(( CAST(COUNT(ISNULL(RNo, 0)) AS DECIMAL(10, 2)) * 100 ) " & _
                '                        "/ CAST(( SELECT COUNT(employeeunkid) " & _
                '                                 "FROM   hremployee_master " & _
                '                                 "WHERE  CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
                '                                        "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
                '                                        "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
                '                                        "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "

                '        If mstrAdvance_Filter.Trim.Length > 0 Then
                '            StrQ &= " AND " & mstrAdvance_Filter
                '        End If
                '        If mstrUserAccessFilter = "" Then
                '            If UserAccessLevel._AccessLevel.Length > 0 Then
                '                StrQ &= UserAccessLevel._AccessLevelFilterString
                '            End If
                '        Else
                '            StrQ &= mstrUserAccessFilter
                '        End If
                '        If mintEmployeeId > 0 Then
                '            StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                '        End If

                '        Select Case mintAppointmentTypeId
                '            Case enAD_Report_Parameter.APP_DATE_FROM
                '                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_BEFORE
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_AFTER
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '        End Select

                '        StrQ &= ") AS DECIMAL(10, 2)) AS DECIMAL(10, 2)) AS Perc " & _
                '"FROM    ( SELECT    b.BSC_Status " & _
                '                  ", b.Ord_No " & _
                '                  ", b.Status_Date " & _
                '                  ", b.RNo " & _
                '          "FROM      ( SELECT    CASE WHEN statustypeid = 1 THEN @Submit_Approval " & _
                '                                         "WHEN statustypeid = 2 THEN @Final_Save " & _
                '                                         "WHEN statustypeid = 3 THEN @Open_Changes " & _
                '                                         "WHEN statustypeid = 4 THEN @Not_Submit " & _
                '                                         "WHEN statustypeid = 7 THEN @PERIODIC_REVIEW " & _
                '                                         "ELSE '' " & _
                '                                    "END AS BSC_Status " & _
                '                                  ", CASE WHEN statustypeid = 1 THEN 1 " & _
                '                                         "WHEN statustypeid = 2 THEN 2 " & _
                '                                         "WHEN statustypeid = 3 THEN 3 " & _
                '                                         "WHEN statustypeid = 4 THEN 4 " & _
                '                                         "WHEN statustypeid = 7 THEN 7 " & _
                '                                          "ELSE 0 " & _
                '                                    "END AS Ord_No " & _
                '                              ", CONVERT(CHAR(8), status_date, 112) AS Status_Date " & _
                '                                  ", ROW_NUMBER() OVER ( PARTITION BY hrassess_empstatus_tran.employeeunkid, " & _
                '                                                        "hrassess_empstatus_tran.periodunkid ORDER BY statustranunkid DESC ) AS RNo " & _
                '                          "FROM      hrassess_empstatus_tran " & _
                '                                "JOIN hremployee_master ON hrassess_empstatus_tran.employeeunkid = hremployee_master.employeeunkid "

                '        StrQ &= mstrAnalysis_Join

                '        StrQ &= "WHERE     periodunkid = @PeriodId " & _
                '                                    "AND CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
                '                                    "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
                '                                    "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
                '                                    "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "


                '        Select Case mintAppointmentTypeId
                '            Case enAD_Report_Parameter.APP_DATE_FROM
                '                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_BEFORE
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_AFTER
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '        End Select

                '        If mstrAdvance_Filter.Trim.Length > 0 Then
                '            StrQ &= " AND " & mstrAdvance_Filter
                '        End If
                '        If mstrUserAccessFilter = "" Then
                '            If UserAccessLevel._AccessLevel.Length > 0 Then
                '                StrQ &= UserAccessLevel._AccessLevelFilterString
                '            End If
                '        Else
                '            StrQ &= mstrUserAccessFilter
                '        End If
                '        If mintEmployeeId > 0 Then
                '            StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                '        End If

                '        StrQ &= "UNION ALL " & _
                '                          "SELECT    @Not_Completed AS BSC_Status " & _
                '                                  ", 5 AS Ord_No " & _
                '                                  ", 1 AS RNo " & _
                '                                  ", '' AS Status_Date " & _
                '                          "FROM      hremployee_master " & _
                '                          "WHERE     employeeunkid NOT IN ( SELECT   employeeunkid " & _
                '                                                           "FROM     hrassess_empstatus_tran " & _
                '                                                           "WHERE    periodunkid = @PeriodId ) " & _
                '                                    "AND CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
                '                                    "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
                '                                    "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
                '                                    "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "

                '        Select Case mintAppointmentTypeId
                '            Case enAD_Report_Parameter.APP_DATE_FROM
                '                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_BEFORE
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '            Case enAD_Report_Parameter.APP_DATE_AFTER
                '                If mdtDate1 <> Nothing Then
                '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                '                End If
                '        End Select

                '        If mstrAdvance_Filter.Trim.Length > 0 Then
                '            StrQ &= " AND " & mstrAdvance_Filter
                '        End If
                '        If mstrUserAccessFilter = "" Then
                '            If UserAccessLevel._AccessLevel.Length > 0 Then
                '                StrQ &= UserAccessLevel._AccessLevelFilterString
                '            End If
                '        Else
                '            StrQ &= mstrUserAccessFilter
                '        End If
                '        If mintEmployeeId > 0 Then
                '            StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                '        End If
                '        If mintStatusId > 0 AndAlso mintStatusId <> 999 Then
                '            StrQ &= " AND 1 = 2 "
                '        End If

                '        StrQ &= ") AS b " & _
                '            "WHERE     RNo = 1 "

                '        If mintStatusId > 0 Then
                '            StrQ &= " AND B.Ord_No = @StatusId "
                '        End If

                '        If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
                '            StrQ &= " AND B.Status_Date BETWEEN @Date1 AND @Date2 "
                '        End If

                'StrQ &= ") AS A " & _
                '        "WHERE   RNo = 1 " & _
                '        "GROUP BY BSC_Status " & _
                '              ", Ord_No "

                StrQ = "SELECT " & _
                       " BSC_Status " & _
                       ",Ord_No " & _
                       ",CAST(COUNT(ISNULL(RNo, 0)) AS DECIMAL(10, 2)) AS Cnt " & _
                       ",CAST((SELECT COUNT(hremployee_master.employeeunkid) " & _
                       "        FROM hremployee_master "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE 1 = 1  "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                End If

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                End Select

                StrQ &= ") AS DECIMAL(10, 2)) AS Total_Emp " & _
                        ", CAST(( CAST(COUNT(ISNULL(RNo, 0)) AS DECIMAL(10, 2)) * 100 ) " & _
                        "  / CAST((SELECT COUNT(hremployee_master.employeeunkid) " & _
                        "          FROM hremployee_master "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE 1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                End If

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                End Select

                StrQ &= ") AS DECIMAL(10, 2)) AS DECIMAL(10, 2)) AS Perc " & _
        "FROM    ( SELECT    b.BSC_Status " & _
                          ", b.Ord_No " & _
                          ", b.Status_Date " & _
                          ", b.RNo " & _
                  "FROM      ( SELECT  CASE WHEN statustypeid = 1 THEN @Submit_Approval " & _
                                                 "WHEN statustypeid = 2 THEN @Final_Save " & _
                                                 "WHEN statustypeid = 3 THEN @Open_Changes " & _
                                                 "WHEN statustypeid = 4 THEN @Not_Submit " & _
                                                 "WHEN statustypeid = 7 THEN @PERIODIC_REVIEW " & _
                                                 "ELSE '' " & _
                                            "END AS BSC_Status " & _
                                          ", CASE WHEN statustypeid = 1 THEN 1 " & _
                                                 "WHEN statustypeid = 2 THEN 2 " & _
                                                 "WHEN statustypeid = 3 THEN 3 " & _
                                                 "WHEN statustypeid = 4 THEN 4 " & _
                                                 "WHEN statustypeid = 7 THEN 7 " & _
                                                  "ELSE 0 " & _
                                            "END AS Ord_No " & _
                                      ", CONVERT(CHAR(8), status_date, 112) AS Status_Date " & _
                                          ", ROW_NUMBER() OVER ( PARTITION BY hrassess_empstatus_tran.employeeunkid, " & _
                                                                "hrassess_empstatus_tran.periodunkid ORDER BY statustranunkid DESC ) AS RNo " & _
                                  "FROM hrassess_empstatus_tran " & _
                                  "JOIN hremployee_master ON hrassess_empstatus_tran.employeeunkid = hremployee_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= mstrAnalysis_Join

                StrQ &= " WHERE  periodunkid = @PeriodId "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                End Select


                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                End If

                'Shani (07-Sep-2016) -- Start 
                'StrQ &= "UNION ALL " & _
                '        "SELECT @Not_Completed AS BSC_Status " & _
                '        ", 5 AS Ord_No " & _
                '        ", 1 AS RNo " & _
                '        ", '' AS Status_Date " & _
                '        "FROM hremployee_master "
                StrQ &= "UNION ALL " & _
                        "SELECT @Not_Completed AS BSC_Status " & _
                        ", 999 AS Ord_No " & _
                        ", '' AS Status_Date " & _
                        ", 1 AS RNo " & _
                        "FROM hremployee_master "
                'Shani (07-Sep-2016) -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE hremployee_master.employeeunkid " & _
                        "NOT IN (SELECT employeeunkid " & _
                        "FROM  hrassess_empstatus_tran " & _
                        "WHERE periodunkid = @PeriodId ) "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                End Select


                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mintStatusId > 0 AndAlso mintStatusId <> 999 Then
                    StrQ &= " AND 1 = 2 "
                End If

                StrQ &= ") AS b " & _
                    "WHERE     RNo = 1 "

                If mintStatusId > 0 Then
                    StrQ &= " AND B.Ord_No = @StatusId "
                End If

                If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
                    StrQ &= " AND B.Status_Date BETWEEN @Date1 AND @Date2 "
                End If

                StrQ &= ") AS A " & _
                        "WHERE   RNo = 1 " & _
                        "GROUP BY BSC_Status " & _
                              ", Ord_No "
                'S.SANDEEP [04 JUN 2015] -- END

                dsList = objDataOperation.ExecQuery(StrQ, "Summary")


                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If


                rptSubreport_Data = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("Summary").Rows
                    Dim rpt_Row As DataRow = rptSubreport_Data.Tables("ArutiTable").NewRow

                    rpt_Row.Item("Column1") = dtRow.Item("BSC_Status")
                    rpt_Row.Item("Column2") = dtRow.Item("Ord_No")
                    rpt_Row.Item("Column3") = CInt(dtRow.Item("Cnt"))
                    rpt_Row.Item("Column4") = CInt(dtRow.Item("Total_Emp"))
                    rpt_Row.Item("Column5") = dtRow.Item("Perc")


                    rptSubreport_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                objRpt.Subreports("rpt_BSC_Planning_Summary").SetDataSource(rptSubreport_Data)

                Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtBSCStatus", Language.getMessage(mstrModuleName, 20, "BSC Status"))
                Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtOrdNo", Language.getMessage(mstrModuleName, 25, "Ord No"))
                Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtCount", Language.getMessage(mstrModuleName, 26, "Count"))
                Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtTotEmp", Language.getMessage(mstrModuleName, 27, "Total Employee"))
                Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtPerc", Language.getMessage(mstrModuleName, 28, "(%)"))
                Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtSummary", Language.getMessage(mstrModuleName, 29, "Summary"))

            End If




            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 18, "Emp. Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 19, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtBSCStatus", Language.getMessage(mstrModuleName, 20, "BSC Status"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 32, "Period"))
            Call ReportFunction.TextChange(objRpt, "txtFYear", Language.getMessage(mstrModuleName, 21, "Financial Year"))
            Call ReportFunction.TextChange(objRpt, "txtStatusDate", Language.getMessage(mstrModuleName, 22, "Status Date"))
            Call ReportFunction.TextChange(objRpt, "txtGrpCnt", Language.getMessage(mstrModuleName, 23, "Group Count :"))
            Call ReportFunction.TextChange(objRpt, "txtGrndCnt", Language.getMessage(mstrModuleName, 24, "Grand Count :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 48, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtDept", Language.getMessage(mstrModuleName, 39, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 40, "Workstation"))
            Call ReportFunction.TextChange(objRpt, "txtSubGrpName", Language.getMessage(mstrModuleName, 41, "BSC Status :"))
            Call ReportFunction.TextChange(objRpt, "txtSubGrpCnt", Language.getMessage(mstrModuleName, 42, "Sub Group Count :"))

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 33, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 34, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 45, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 46, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 47, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 17, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            If mblnIncludeSummary = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "RFSummary", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "RFSummary", True)
            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 518, "Pending")
            Language.setMessage("clsMasterData", 519, "In progress")
            Language.setMessage("clsMasterData", 520, "Complete")
            Language.setMessage("clsMasterData", 521, "Closed")
            Language.setMessage("clsMasterData", 522, "On Track")
            Language.setMessage("clsMasterData", 523, "At Risk")
            Language.setMessage("clsMasterData", 524, "Not Applicable")
            Language.setMessage("clsMasterData", 616, "BSC Employee Total Score")
            Language.setMessage("clsMasterData", 617, "BSC Assessor Total Score")
            Language.setMessage("clsMasterData", 618, "BSC Reviewer Total Score")
            Language.setMessage("clsMasterData", 619, "Competence Employee Total Score")
            Language.setMessage("clsMasterData", 620, "Competence Assessor Total Score")
            Language.setMessage("clsMasterData", 621, "Competence Reviewer Total Score")
            Language.setMessage("clsMasterData", 622, "Employee Overall Score")
            Language.setMessage("clsMasterData", 623, "Assessor Overall Score")
            Language.setMessage("clsMasterData", 624, "Reviewer Overall Score")
            Language.setMessage("clsMasterData", 625, "Post To Payroll Value")
            Language.setMessage("clsMasterData", 636, "Final Result Score")
            Language.setMessage("clsMasterData", 748, "Postponed")
            Language.setMessage("clsMasterData", 749, "Behind Schedule")
            Language.setMessage("clsMasterData", 750, "Ahead of Schedule")
            Language.setMessage(mstrModuleName, 1, "Submitted For Approval")
            Language.setMessage(mstrModuleName, 3, "Opened For Changes")
            Language.setMessage(mstrModuleName, 4, "Not Submitted For Approval")
            Language.setMessage(mstrModuleName, 5, "Not Planned")
            Language.setMessage(mstrModuleName, 6, "Period :")
            Language.setMessage(mstrModuleName, 7, "Employee :")
            Language.setMessage(mstrModuleName, 8, "Status Date From :")
            Language.setMessage(mstrModuleName, 9, "To :")
            Language.setMessage(mstrModuleName, 10, "Order By :")
            Language.setMessage(mstrModuleName, 11, "Employee Code")
            Language.setMessage(mstrModuleName, 12, "Employee Name")
            Language.setMessage(mstrModuleName, 13, "Planning Status")
            Language.setMessage(mstrModuleName, 14, "Period")
            Language.setMessage(mstrModuleName, 15, "Job")
            Language.setMessage(mstrModuleName, 16, "Status Date")
            Language.setMessage(mstrModuleName, 17, "Printed Date :")
            Language.setMessage(mstrModuleName, 18, "Emp. Code")
            Language.setMessage(mstrModuleName, 19, "Employee Name")
            Language.setMessage(mstrModuleName, 20, "BSC Status")
            Language.setMessage(mstrModuleName, 21, "Financial Year")
            Language.setMessage(mstrModuleName, 22, "Status Date")
            Language.setMessage(mstrModuleName, 23, "Group Count :")
            Language.setMessage(mstrModuleName, 24, "Grand Count :")
            Language.setMessage(mstrModuleName, 25, "Ord No")
            Language.setMessage(mstrModuleName, 26, "Count")
            Language.setMessage(mstrModuleName, 27, "Total Employee")
            Language.setMessage(mstrModuleName, 28, "(%)")
            Language.setMessage(mstrModuleName, 29, "Summary")
            Language.setMessage(mstrModuleName, 30, "Status :")
            Language.setMessage(mstrModuleName, 31, "Not Planned")
            Language.setMessage(mstrModuleName, 32, "Period")
            Language.setMessage(mstrModuleName, 33, "Prepared By :")
            Language.setMessage(mstrModuleName, 34, "Checked By :")
            Language.setMessage(mstrModuleName, 35, "Appointed Date From :")
            Language.setMessage(mstrModuleName, 36, "To :")
            Language.setMessage(mstrModuleName, 37, "Appointed Before Date :")
            Language.setMessage(mstrModuleName, 38, "Appointed After Date :")
            Language.setMessage(mstrModuleName, 39, "Department")
            Language.setMessage(mstrModuleName, 40, "Workstation")
            Language.setMessage(mstrModuleName, 41, "BSC Status :")
            Language.setMessage(mstrModuleName, 42, "Sub Group Count :")
            Language.setMessage(mstrModuleName, 43, "Department")
            Language.setMessage(mstrModuleName, 44, "Workstation")
            Language.setMessage(mstrModuleName, 45, "Approved By :")
            Language.setMessage(mstrModuleName, 46, "Received By :")
            Language.setMessage(mstrModuleName, 47, "Printed By :")
            Language.setMessage(mstrModuleName, 48, "Job")
            Language.setMessage(mstrModuleName, 49, "Not Committed")
            Language.setMessage(mstrModuleName, 50, "Final Committed")
            Language.setMessage(mstrModuleName, 51, "Periodic Review")
            Language.setMessage(mstrModuleName, 52, "Display Score Type")
            Language.setMessage(mstrModuleName, 101, "Approved")
            Language.setMessage(mstrModuleName, 500, "Employee Details")
            Language.setMessage(mstrModuleName, 501, "Appointed Date :")
            Language.setMessage(mstrModuleName, 502, "Department :")
            Language.setMessage(mstrModuleName, 503, "Job :")
            Language.setMessage(mstrModuleName, 504, "Computed Score")
            Language.setMessage(mstrModuleName, 505, "Formula")
            Language.setMessage(mstrModuleName, 506, "Score")
            Language.setMessage(mstrModuleName, 507, "Employee Competencies")
            Language.setMessage(mstrModuleName, 508, "Assessment Group :")
            Language.setMessage(mstrModuleName, 509, "Competence Category :")
            Language.setMessage(mstrModuleName, 510, "Competence Item")
            Language.setMessage(mstrModuleName, 511, "Weight")
            Language.setMessage(mstrModuleName, 512, "Employee Goals")
            Language.setMessage(mstrModuleName, 513, "Weight")
            Language.setMessage(mstrModuleName, 514, "Percent")
            Language.setMessage(mstrModuleName, 515, "Status")
            Language.setMessage(mstrModuleName, 516, "Perspective :")
            Language.setMessage(mstrModuleName, 517, "Emp. Code :")
            Language.setMessage(mstrModuleName, 518, "Employee Name :")
            Language.setMessage(mstrModuleName, 519, "SELF")
            Language.setMessage(mstrModuleName, 520, "ASSESSOR")
            Language.setMessage(mstrModuleName, 521, "REVIEWER")
            Language.setMessage(mstrModuleName, 522, "Custom Items")
            Language.setMessage(mstrModuleName, 523, "Custom Header :")
            Language.setMessage(mstrModuleName, 524, "Custom Item :")
            Language.setMessage(mstrModuleName, 525, "Planning Status")
            Language.setMessage(mstrModuleName, 526, "Approved")
            Language.setMessage(mstrModuleName, 527, "Not Done")
            Language.setMessage(mstrModuleName, 528, "Is Committed")
            Language.setMessage(mstrModuleName, 530, "Yes")
            Language.setMessage(mstrModuleName, 531, "No")
            Language.setMessage(mstrModuleName, 532, "Mode")
            Language.setMessage(mstrModuleName, 533, "Status(s)")
            Language.setMessage(mstrModuleName, 534, "Assessment/Planning Status")
            Language.setMessage(mstrModuleName, 535, "Assessment Mode")
            Language.setMessage(mstrModuleName, 536, "Assessor :")
            Language.setMessage(mstrModuleName, 537, "Reviewer :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'------------------------ BEFORE NEW ASSESSMENT PLANNING -------------------------' ON 25-SEP-2015 -- TABLE CHANGED TO hrassess_empstatus_tran

'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
'    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'    Dim StrQ As String = ""
'    Dim dsList As New DataSet
'    Dim blnFlag As Boolean = False
'    Dim rptSubreport_Data As ArutiReport.Designer.dsArutiReport = Nothing
'    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'    Try
'        objDataOperation = New clsDataOperation

'        StrQ = "SELECT " & _
'               "	 ECode " & _
'               "	,EName " & _
'               "	,Job " & _
'               "	,Dept " & _
'               "	,Class " & _
'               "	,BSC_StatusId " & _
'               "	,BSC_Status " & _
'               "	,PName " & _
'               "	,Financial_Year " & _
'               "	,Status_Date " & _
'               "	,sort_date " & _
'               "    ,Id " & _
'               "    ,GName " & _
'               "FROM " & _
'               "( " & _
'               "		SELECT " & _
'               "			 ISNULL(employeecode,'') AS ECode " & _
'               "			,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS EName " & _
'               "			,ISNULL(job.job_name, '') AS Job " & _
'               "			,ISNULL(dept.name, '') AS Dept " & _
'               "			,ISNULL(class.name, '') AS Class " & _
'               "			,ISNULL(statustypeid,'') AS BSC_StatusId " & _
'               "			,CASE WHEN statustypeid = 1 THEN @Submit_Approval " & _
'               "				  WHEN statustypeid = 2 THEN @Final_Save " & _
'               "				  WHEN statustypeid = 3 THEN @Open_Changes " & _
'               "				  WHEN statustypeid = 4 THEN @Not_Submit " & _
'               "			 END AS BSC_Status " & _
'               "			,ISNULL(period_name,'') AS PName " & _
'               "			,ISNULL(financialyear_name,'') AS Financial_Year " & _
'               "			,statustypeid " & _
'               "			,hremployee_master.employeeunkid " & _
'               "			,cfcommon_period_tran.periodunkid " & _
'               "			,CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
'               "			,status_date AS sort_date " & _
'               "			,ROW_NUMBER() OVER(PARTITION BY hrobjective_status_tran.employeeunkid,hrobjective_status_tran.yearunkid,hrobjective_status_tran.periodunkid ORDER BY objectivestatustranunkid DESC) AS RNo "
'        If mintViewIndex > 0 Then
'            StrQ &= mstrAnalysis_Fields
'        Else
'            StrQ &= ", 0 AS Id, '' AS GName "
'        End If
'        StrQ &= "		FROM hrobjective_status_tran " & _
'                "			JOIN hrmsConfiguration..cffinancial_year_tran ON hrobjective_status_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
'                "			JOIN cfcommon_period_tran ON hrobjective_status_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 5 " & _
'                "			JOIN hremployee_master ON hrobjective_status_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                "           LEFT JOIN hrjob_master AS job ON hremployee_master.jobunkid = job.jobunkid " & _
'                "           LEFT JOIN hrdepartment_master AS dept ON dept.departmentunkid = hremployee_master.departmentunkid " & _
'                "           LEFT JOIN hrclasses_master AS class ON class.classesunkid = hremployee_master.classunkid "
'        StrQ &= mstrAnalysis_Join
'        StrQ &= "		WHERE cfcommon_period_tran.periodunkid = @PeriodId " & _
'                "			AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
'                "			AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
'                "			AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
'                "			AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
'        If mstrAdvance_Filter.Trim.Length > 0 Then
'            StrQ &= " AND " & mstrAdvance_Filter
'        End If
'        If mstrUserAccessFilter = "" Then
'            If UserAccessLevel._AccessLevel.Length > 0 Then
'                StrQ &= UserAccessLevel._AccessLevelFilterString
'            End If
'        Else
'            StrQ &= mstrUserAccessFilter
'        End If

'        'S.SANDEEP [ 22 OCT 2013 ] -- START
'        Select Case mintAppointmentTypeId
'            Case enAD_Report_Parameter.APP_DATE_FROM
'                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                End If
'            Case enAD_Report_Parameter.APP_DATE_BEFORE
'                If mdtDate1 <> Nothing Then
'                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                End If
'            Case enAD_Report_Parameter.APP_DATE_AFTER
'                If mdtDate1 <> Nothing Then
'                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                End If
'        End Select
'        'S.SANDEEP [ 22 OCT 2013 ] -- END


'        'Sohail (23 Sep 2013) -- Start
'        'TRA - ENHANCEMENT
'        'If mintStatusId > 0 Then
'        '    StrQ &= " AND statustypeid = @StatusId "
'        'End If
'        'Sohail (23 Sep 2013) -- End
'        StrQ &= "	UNION ALL " & _
'                "		SELECT " & _
'                "			 ISNULL(employeecode,'') AS ECode " & _
'                "			,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS EName " & _
'                "			,ISNULL(job.job_name, '') AS Job " & _
'                "			,ISNULL(dept.name, '') AS Dept " & _
'                "			,ISNULL(class.name, '') AS Class " & _
'                "			,999 AS BSC_StatusId " & _
'                "			,@Not_Planned AS BSC_Status " & _
'                "			,ISNULL(period_name,'') AS PName " & _
'                "			,ISNULL(financialyear_name,'') AS Financial_Year " & _
'                "			,0 AS statustypeid " & _
'                "			,hremployee_master.employeeunkid " & _
'                "			,cfcommon_period_tran.periodunkid " & _
'                "			,'' AS Status_Date " & _
'                "			,NULL AS sort_date " & _
'                "			,1 AS RNo "
'        If mintViewIndex > 0 Then
'            StrQ &= mstrAnalysis_Fields
'        Else
'            StrQ &= ", 0 AS Id, '' AS GName "
'        End If
'        'Sohail (12 Apr 2013) -- Start
'        'TRA - ENHANCEMENT
'        'StrQ &= "		FROM hremployee_master " & _
'        '        "			JOIN hrobjective_status_tran ON hremployee_master.employeeunkid <> hrobjective_status_tran.employeeunkid " & _
'        '        "			JOIN cfcommon_period_tran ON hrobjective_status_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 5 " & _
'        '        "			JOIN hrmsConfiguration..cffinancial_year_tran ON hrobjective_status_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid "
'        StrQ &= "		FROM hremployee_master " & _
'                "			LEFT JOIN hrobjective_status_tran ON hremployee_master.employeeunkid = hrobjective_status_tran.employeeunkid " & _
'                "			LEFT JOIN cfcommon_period_tran ON hrobjective_status_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 5 " & _
'                "			LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON hrobjective_status_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
'                "           LEFT JOIN hrjob_master AS job ON hremployee_master.jobunkid = job.jobunkid " & _
'                "           LEFT JOIN hrdepartment_master AS dept ON dept.departmentunkid = hremployee_master.departmentunkid " & _
'                "           LEFT JOIN hrclasses_master AS class ON class.classesunkid = hremployee_master.classunkid "
'        'Sohail (12 Apr 2013) -- End]

'        StrQ &= mstrAnalysis_Join
'        StrQ &= "		WHERE ISNULL(cfcommon_period_tran.periodunkid, @PeriodId ) = @PeriodId " & _
'                "			AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
'                "			AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
'                "			AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
'                "			AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate " & _
'                "           AND hrobjective_status_tran.employeeunkid IS NULL "

'        If mstrAdvance_Filter.Trim.Length > 0 Then
'            StrQ &= " AND " & mstrAdvance_Filter
'        End If
'        If mstrUserAccessFilter = "" Then
'            If UserAccessLevel._AccessLevel.Length > 0 Then
'                StrQ &= UserAccessLevel._AccessLevelFilterString
'            End If
'        Else
'            StrQ &= mstrUserAccessFilter
'        End If

'        'S.SANDEEP [ 22 OCT 2013 ] -- START
'        Select Case mintAppointmentTypeId
'            Case enAD_Report_Parameter.APP_DATE_FROM
'                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                End If
'            Case enAD_Report_Parameter.APP_DATE_BEFORE
'                If mdtDate1 <> Nothing Then
'                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                End If
'            Case enAD_Report_Parameter.APP_DATE_AFTER
'                If mdtDate1 <> Nothing Then
'                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                End If
'        End Select
'        'S.SANDEEP [ 22 OCT 2013 ] -- END

'        If mintStatusId > 0 AndAlso mintStatusId <> 999 Then
'            'StrQ &= " AND statustypeid = @StatusId "
'            StrQ &= " AND 1 = 2 "
'        End If
'        StrQ &= ") AS BSC_APPR WHERE RNo = 1 "

'        'Sohail (23 Sep 2013) -- Start
'        'TRA - ENHANCEMENT
'        If mintStatusId > 0 Then
'            StrQ &= " AND BSC_StatusId = @StatusId "
'        End If
'        'Sohail (23 Sep 2013) -- End

'        Call FilterTitleAndFilterQuery()
'        StrQ &= Me._FilterQuery

'        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


'        If objDataOperation.ErrorMessage <> "" Then
'            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'        End If

'        rpt_Data = New ArutiReport.Designer.dsArutiReport

'        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
'            Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

'            rpt_Row.Item("Column1") = dtRow.Item("GName")
'            rpt_Row.Item("Column2") = dtRow.Item("ECode")
'            rpt_Row.Item("Column3") = dtRow.Item("EName")
'            rpt_Row.Item("Column4") = dtRow.Item("BSC_Status")
'            rpt_Row.Item("Column5") = dtRow.Item("PName")
'            rpt_Row.Item("Column6") = dtRow.Item("Financial_Year")
'            If dtRow.Item("Status_Date").ToString.Trim.Length > 0 Then
'                rpt_Row.Item("Column7") = eZeeDate.convertDate(dtRow.Item("Status_Date").ToString).ToShortDateString
'            Else
'                rpt_Row.Item("Column7") = ""
'            End If
'            rpt_Row.Item("Column81") = dtRow.Item("BSC_StatusId")
'            rpt_Row.Item("Column8") = dtRow.Item("Job")
'            rpt_Row.Item("Column9") = dtRow.Item("Dept")
'            rpt_Row.Item("Column10") = dtRow.Item("Class")

'            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'        Next


'        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'        objRpt = New ArutiReport.Designer.rpt_BSC_Planning_Report

'        Dim arrImageRow As DataRow = Nothing
'        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow

'        ReportFunction.Logo_Display(objRpt, _
'                                   ConfigParameter._Object._IsDisplayLogo, _
'                                   ConfigParameter._Object._ShowLogoRightSide, _
'                                   "arutiLogo1", _
'                                   "arutiLogo2", _
'                                   arrImageRow, _
'                                   "txtCompanyName", _
'                                   "txtReportName", _
'                                   "txtFilterDescription", _
'                                    ConfigParameter._Object._GetLeftMargin, _
'                                    ConfigParameter._Object._GetRightMargin)

'        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'            rpt_Data.Tables("ArutiTable").Rows.Add("")
'        End If


'        objRpt.SetDataSource(rpt_Data)

'        If mblnIncludeSummary = True Then

'            StrQ = "SELECT  BSC_Status " & _
'                          ", Ord_No " & _
'                          ", CAST(COUNT(ISNULL(RNo, 0)) AS DECIMAL(10, 2)) AS Cnt " & _
'                          ", CAST(( SELECT   COUNT(employeeunkid) " & _
'                                   "FROM     hremployee_master " & _
'                                   "WHERE    CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
'                                            "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
'                                            "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
'                                            "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "

'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            If mintEmployeeId > 0 Then
'                StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
'            End If

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'            If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
'                'StrQ &= " AND BSC_APPR.Status_Date BETWEEN @Date1 AND @Date2 "
'            End If

'            StrQ &= ") AS DECIMAL(10, 2)) AS Total_Emp " & _
'                          ", CAST(( CAST(COUNT(ISNULL(RNo, 0)) AS DECIMAL(10, 2)) * 100 ) " & _
'                            "/ CAST(( SELECT COUNT(employeeunkid) " & _
'                                     "FROM   hremployee_master " & _
'                                     "WHERE  CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
'                                            "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
'                                            "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
'                                            "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "

'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            If mintEmployeeId > 0 Then
'                StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
'            End If

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'            If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
'                'StrQ &= " AND BSC_APPR.Status_Date BETWEEN @Date1 AND @Date2 "
'            End If

'            'Sohail (23 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            'StrQ &= ") AS DECIMAL(10, 2)) AS DECIMAL(10, 2)) AS Perc " & _
'            '        "FROM    ( SELECT    CASE WHEN statustypeid = 1 THEN @Submit_Approval " & _
'            '                                 "WHEN statustypeid = 2 THEN @Final_Save " & _
'            '                                 "WHEN statustypeid = 3 THEN @Open_Changes " & _
'            '                                 "WHEN statustypeid = 4 THEN @Not_Submit " & _
'            '                                 "ELSE '' " & _
'            '                            "END AS BSC_Status " & _
'            '                          ", CASE WHEN statustypeid = 1 THEN 1 " & _
'            '                                 "WHEN statustypeid = 2 THEN 2 " & _
'            '                                 "WHEN statustypeid = 3 THEN 3 " & _
'            '                                 "WHEN statustypeid = 4 THEN 4 " & _
'            '                                  "ELSE 0 " & _
'            '                            "END AS Ord_No " & _
'            '                          ", ROW_NUMBER() OVER ( PARTITION BY hrobjective_status_tran.employeeunkid, " & _
'            '                                                "hrobjective_status_tran.yearunkid, " & _
'            '                                                "hrobjective_status_tran.periodunkid ORDER BY objectivestatustranunkid DESC ) AS RNo " & _
'            '                  "FROM      hrobjective_status_tran " & _
'            '                            "JOIN hremployee_master ON hrobjective_status_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '                  "WHERE     periodunkid = @PeriodId " & _
'            '                            "AND CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
'            '                            "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
'            '                            "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
'            '                            "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "
'            StrQ &= ") AS DECIMAL(10, 2)) AS DECIMAL(10, 2)) AS Perc " & _
'    "FROM    ( SELECT    b.BSC_Status " & _
'                      ", b.Ord_No " & _
'                      ", b.Status_Date " & _
'                      ", b.RNo " & _
'              "FROM      ( SELECT    CASE WHEN statustypeid = 1 " & _
'                                         "THEN @Submit_Approval " & _
'                                             "WHEN statustypeid = 2 THEN @Final_Save " & _
'                                             "WHEN statustypeid = 3 THEN @Open_Changes " & _
'                                             "WHEN statustypeid = 4 THEN @Not_Submit " & _
'                                             "ELSE '' " & _
'                                        "END AS BSC_Status " & _
'                                      ", CASE WHEN statustypeid = 1 THEN 1 " & _
'                                             "WHEN statustypeid = 2 THEN 2 " & _
'                                             "WHEN statustypeid = 3 THEN 3 " & _
'                                             "WHEN statustypeid = 4 THEN 4 " & _
'                                              "ELSE 0 " & _
'                                        "END AS Ord_No " & _
'                                  ", CONVERT(CHAR(8), status_date, 112) AS Status_Date " & _
'                                      ", ROW_NUMBER() OVER ( PARTITION BY hrobjective_status_tran.employeeunkid, " & _
'                                                            "hrobjective_status_tran.yearunkid, " & _
'                                                            "hrobjective_status_tran.periodunkid ORDER BY objectivestatustranunkid DESC ) AS RNo " & _
'                              "FROM      hrobjective_status_tran " & _
'                                    "JOIN hremployee_master ON hrobjective_status_tran.employeeunkid = hremployee_master.employeeunkid "

'            StrQ &= mstrAnalysis_Join

'            StrQ &= "WHERE     periodunkid = @PeriodId " & _
'                                        "AND CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
'                                        "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
'                                        "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
'                                        "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "
'            'Sohail (23 Sep 2013) -- End

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            If mintEmployeeId > 0 Then
'                StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
'            End If

'            'Sohail (23 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            'If mintStatusId > 0 Then
'            '    StrQ &= " AND statustypeid = @StatusId "
'            'End If
'            'Sohail (23 Sep 2013) -- End

'            If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
'                'StrQ &= " AND BSC_APPR.Status_Date BETWEEN @Date1 AND @Date2 "
'            End If

'            StrQ &= "UNION ALL " & _
'                              "SELECT    @Not_Completed AS BSC_Status " & _
'                                      ", 5 AS Ord_No " & _
'                                      ", 1 AS RNo " & _
'                                      ", '' AS Status_Date " & _
'                              "FROM      hremployee_master " & _
'                              "WHERE     employeeunkid NOT IN ( SELECT   employeeunkid " & _
'                                                               "FROM     hrobjective_status_tran " & _
'                                                               "WHERE    periodunkid = @PeriodId ) " & _
'                                        "AND CONVERT(CHAR(8), appointeddate, 112) <= @enddate " & _
'                                        "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
'                                        "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
'                                        "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "
'            'Sohail (23 Sep 2013) - [Status_Date]

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            If mintEmployeeId > 0 Then
'                StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
'            End If
'            If mintStatusId > 0 AndAlso mintStatusId <> 999 Then
'                'StrQ &= " AND statustypeid = @StatusId "
'                StrQ &= " AND 1 = 2 "
'            End If
'            If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
'                'StrQ &= " AND BSC_APPR.Status_Date BETWEEN @Date1 AND @Date2 "
'            End If

'            'Sohail (23 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            'StrQ &= ") AS A " & _
'            '        "WHERE   RNo = 1 " & _
'            '       "GROUP BY BSC_Status " & _
'            '              ", Ord_No "
'            StrQ &= ") AS b " & _
'                "WHERE     RNo = 1 "

'            If mintStatusId > 0 Then
'                StrQ &= " AND B.Ord_No = @StatusId "
'            End If

'            If mdtFilterDate1 <> Nothing AndAlso mdtFilterDate2 <> Nothing Then
'                StrQ &= " AND B.Status_Date BETWEEN @Date1 AND @Date2 "
'            End If

'            StrQ &= ") AS A " & _
'                    "WHERE   RNo = 1 " & _
'                    "GROUP BY BSC_Status " & _
'                          ", Ord_No "
'            'Sohail (23 Sep 2013) -- End

'            'StrQ &= Me._FilterQuery

'            dsList = objDataOperation.ExecQuery(StrQ, "Summary")


'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If


'            rptSubreport_Data = New ArutiReport.Designer.dsArutiReport

'            For Each dtRow As DataRow In dsList.Tables("Summary").Rows
'                Dim rpt_Row As DataRow = rptSubreport_Data.Tables("ArutiTable").NewRow

'                rpt_Row.Item("Column1") = dtRow.Item("BSC_Status")
'                rpt_Row.Item("Column2") = dtRow.Item("Ord_No")
'                rpt_Row.Item("Column3") = CInt(dtRow.Item("Cnt"))
'                rpt_Row.Item("Column4") = CInt(dtRow.Item("Total_Emp"))
'                rpt_Row.Item("Column5") = dtRow.Item("Perc")


'                rptSubreport_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'            Next

'            objRpt.Subreports("rpt_BSC_Planning_Summary").SetDataSource(rptSubreport_Data)

'            Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtBSCStatus", Language.getMessage(mstrModuleName, 20, "BSC Status"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtOrdNo", Language.getMessage(mstrModuleName, 25, "Ord No"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtCount", Language.getMessage(mstrModuleName, 26, "Count"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtTotEmp", Language.getMessage(mstrModuleName, 27, "Total Employee"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtPerc", Language.getMessage(mstrModuleName, 28, "(%)"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rpt_BSC_Planning_Summary"), "txtSummary", Language.getMessage(mstrModuleName, 29, "Summary"))

'        End If




'        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 18, "Emp. Code"))
'        Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 19, "Employee Name"))
'        Call ReportFunction.TextChange(objRpt, "txtBSCStatus", Language.getMessage(mstrModuleName, 20, "BSC Status"))
'        Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 32, "Period"))
'        Call ReportFunction.TextChange(objRpt, "txtFYear", Language.getMessage(mstrModuleName, 21, "Financial Year"))
'        Call ReportFunction.TextChange(objRpt, "txtStatusDate", Language.getMessage(mstrModuleName, 22, "Status Date"))
'        Call ReportFunction.TextChange(objRpt, "txtGrpCnt", Language.getMessage(mstrModuleName, 23, "Group Count :"))
'        Call ReportFunction.TextChange(objRpt, "txtGrndCnt", Language.getMessage(mstrModuleName, 24, "Grand Count :"))
'        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
'        Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 48, "Job"))
'        Call ReportFunction.TextChange(objRpt, "txtDept", Language.getMessage(mstrModuleName, 39, "Department"))
'        Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 40, "Workstation"))
'        Call ReportFunction.TextChange(objRpt, "txtSubGrpName", Language.getMessage(mstrModuleName, 41, "BSC Status :"))
'        Call ReportFunction.TextChange(objRpt, "txtSubGrpCnt", Language.getMessage(mstrModuleName, 42, "Sub Group Count :"))

'        If ConfigParameter._Object._IsShowPreparedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 33, "Prepared By :"))
'            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'        End If

'        If ConfigParameter._Object._IsShowCheckedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 34, "Checked By :"))
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'        End If

'        If ConfigParameter._Object._IsShowApprovedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 45, "Approved By :"))
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'        End If

'        If ConfigParameter._Object._IsShowReceivedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 46, "Received By :"))
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'        End If


'        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 47, "Printed By :"))
'        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 17, "Printed Date :"))

'        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
'        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

'        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

'        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'        If mblnIncludeSummary = True Then
'            Call ReportFunction.EnableSuppressSection(objRpt, "RFSummary", False)
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "RFSummary", True)
'        End If

'        Return objRpt

'    Catch ex As Exception
'        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
'        Return Nothing
'    End Try
'End Function
