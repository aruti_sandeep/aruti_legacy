﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

Public Class clsStaffTransfer_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsStaffTransfer_Report"
    Private mstrReportId As String = enArutiReport.Staff_Transfer_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrExportReportPath As String = ""
    Private mblnOpenAfterExport As Boolean = False
    Private mdtRequestFromDate As DateTime = Nothing
    Private mdtRequestToDate As DateTime = Nothing
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintFromClassGroupId As Integer = 0
    Private mstrFromClassGroup As String = ""
    Private mintToClassGroupId As Integer = 0
    Private mstrToClassGroup As String = ""
    Private mintJobId As Integer = 0
    Private mstrJob As String = ""
    Private mintReasonId As Integer = 0
    Private mstrReason As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatus As String = ""

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property


    Public WriteOnly Property _RequestFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtRequestFromDate = value
        End Set
    End Property

    Public WriteOnly Property _RequestToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtRequestToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _FromClassGroupId() As Integer
        Set(ByVal value As Integer)
            mintFromClassGroupId = value
        End Set
    End Property

    Public WriteOnly Property _FromClassGroup() As String
        Set(ByVal value As String)
            mstrFromClassGroup = value
        End Set
    End Property

    Public WriteOnly Property _ToClassGroupId() As Integer
        Set(ByVal value As Integer)
            mintToClassGroupId = value
        End Set
    End Property

    Public WriteOnly Property _ToClassGroup() As String
        Set(ByVal value As String)
            mstrToClassGroup = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJob = value
        End Set
    End Property

    Public WriteOnly Property _ReasonId() As Integer
        Set(ByVal value As Integer)
            mintReasonId = value
        End Set
    End Property

    Public WriteOnly Property _Reason() As String
        Set(ByVal value As String)
            mstrReason = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _Status() As String
        Set(ByVal value As String)
            mstrStatus = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mstrExportReportPath = ""
            mblnOpenAfterExport = False
            mdtRequestFromDate = Nothing
            mdtRequestToDate = Nothing
            mintEmpId = 0
            mstrEmpName = ""
            mintFromClassGroupId = 0
            mstrFromClassGroup = ""
            mintToClassGroupId = 0
            mstrToClassGroup = ""
            mintJobId = 0
            mstrJob = ""
            mintReasonId = 0
            mstrReason = ""
            mintStatusId = 0
            mstrStatus = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAdvanceFilter = ""
            setDefaultOrderBy(0)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mintEmpId > 0 Then
                Me._FilterQuery &= " AND st.employeeunkid = @EmpId "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, " Employee : ") & " " & mstrEmpName & " "
            End If

            If mdtRequestFromDate <> Nothing AndAlso mdtRequestToDate <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),st.requestdate,112) BETWEEN @FromDate AND @ToDate "

                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtRequestFromDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, " Request From Date: ") & " " & mdtRequestFromDate.ToShortDateString & " "

                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtRequestToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, " Request To Date: ") & " " & mdtRequestToDate.ToShortDateString & " "
            End If

            If mintFromClassGroupId > 0 Then
                Me._FilterQuery &= " AND pcg.classgroupunkid = @fromclassgroupunkid "
                objDataOperation.AddParameter("@fromclassgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromClassGroupId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " From Zone : ") & " " & mstrFromClassGroup & " "
            End If

            If mintToClassGroupId > 0 Then
                Me._FilterQuery &= " AND tocg.classgroupunkid = @toclassgroupunkid "
                objDataOperation.AddParameter("@toclassgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToClassGroupId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, " Destination Zone : ") & " " & mstrToClassGroup & " "
            End If

            If mintJobId > 0 Then
                Me._FilterQuery &= " AND pjob.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, " Job : ") & " " & mstrJob & " "
            End If

            If mintReasonId > 0 Then
                Me._FilterQuery &= " AND st.reasonunkid = @reasonunkid "
                objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, " Reason : ") & " " & mstrReason & " "
            End If

            If mintStatusId > 0 Then
                Me._FilterQuery &= " AND st.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Status : ") & " " & mstrStatus & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY ISNULL(hremployee_master.employeecode, '') "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 15, "Staff No")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_StaffTransferReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean)

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim mstrGroup As String = ""

        Try
            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1
            mstrGroup = objGroupMaster._Groupname
            objGroupMaster = Nothing

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            StrQ = " SELECT " & _
                      " st.transferrequestunkid " & _
                      ",ROW_NUMBER() OVER (ORDER BY st.transferrequestunkid) AS [SR.No] " & _
                      ",ISNULL(hremployee_master.employeecode, '') AS [Staff No] " & _
                      ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS [Staff Name] " & _
                      ",ISNULL(pjob.job_name, '') AS [Previous Role] " & _
                      ",ISNULL(cjob.job_name, '') AS [Current Role] " & _
                      ",ISNULL(pcg.name, '') AS [Previous Zone] " & _
                      ",ISNULL(ccg.name, '') AS [Current Zone] " & _
                      ",ISNULL(tocg.name, '') AS [To Zone] " & _
                      ",ISNULL(cfcommon_master.name, '') AS Reason " & _
                      ",CONVERT(CHAR(10),st.requestdate,103) AS [Date Recevied] " & _
                      ",CASE WHEN st.statusunkid = 1 THEN @Pending " & _
                      "          WHEN st.statusunkid = 2 THEN @Approved " & _
                      " END AS Status " & _
                      ",CASE WHEN app.approvaldate IS NOT NULL THEN CAST(DATEDIFF(DAY, st.requestdate, app.approvaldate) AS NVARCHAR(5)) ELSE '' END [TAT Days] "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM sttransfer_request_tran st " & _
                         " LEFT JOIN hrclassgroup_master tocg ON tocg.classgroupunkid = st.classgroupunkid " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = st.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            StrQ &= " LEFT JOIN ( " & _
                         "                      SELECT " & _
                         "                              hremployee_categorization_tran.jobgroupunkid " & _
                         "                             ,hremployee_categorization_tran.jobunkid " & _
                         "                             ,ROW_NUMBER() OVER (PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                         "                             ,hremployee_categorization_tran.employeeunkid " & _
                         "                             ,effectivedate " & _
                         "                       FROM hremployee_categorization_tran " & _
                         "                      WHERE hremployee_categorization_tran.isvoid = 0 " & _
                         "                ) AS pJr ON pJr.employeeunkid = hremployee_master.employeeunkid AND pJr.rno = 1 AND CONVERT(CHAR(8), pjr.effectivedate, 112) <= CONVERT(CHAR(8), st.requestdate, 112) " & _
                         " LEFT JOIN hrjob_master pjob ON pjob.jobunkid = pjr.jobunkid " & _
                         " LEFT JOIN ( " & _
                         "                      SELECT " & _
                         "                              hremployee_categorization_tran.jobgroupunkid " & _
                         "                             ,hremployee_categorization_tran.jobunkid " & _
                         "                             ,ROW_NUMBER() OVER (PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                         "                             ,hremployee_categorization_tran.employeeunkid " & _
                         "                             ,effectivedate " & _
                         "                      FROM hremployee_categorization_tran " & _
                         "                       WHERE hremployee_categorization_tran.isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE() , 112) " & _
                         "                ) AS cJr ON cJr.employeeunkid = hremployee_master.employeeunkid AND cJr.rno = 1 " & _
                         " LEFT JOIN hrjob_master cjob ON cjob.jobunkid = cjr.jobunkid " & _
                         " LEFT JOIN ( " & _
                         "                      SELECT " & _
                         "                              hremployee_transfer_tran.classgroupunkid " & _
                         "                             ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                         "                             ,hremployee_transfer_tran.employeeunkid " & _
                         "                             ,hremployee_transfer_tran.effectivedate " & _
                         "                      FROM hremployee_transfer_tran " & _
                         "                      WHERE hremployee_transfer_tran.isvoid = 0 " & _
                         "                 ) AS pTr ON pTr.employeeunkid = hremployee_master.employeeunkid AND pTr.rno = 1 AND CONVERT(NVARCHAR(8), pTr.effectivedate, 112) <= CONVERT(CHAR(8), st.requestdate, 112) " & _
                         " LEFT JOIN hrclassgroup_master pcg ON pcg.classgroupunkid = ptr.classgroupunkid " & _
                         " LEFT JOIN ( " & _
                         "                      SELECT " & _
                         "                              hremployee_transfer_tran.classgroupunkid " & _
                         "                              ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                         "                              ,hremployee_transfer_tran.employeeunkid " & _
                         "                      FROM hremployee_transfer_tran " & _
                         "                      WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                         "                ) AS cTr ON cTr.employeeunkid = hremployee_master.employeeunkid AND cTr.rno = 1 " & _
                         " LEFT JOIN hrclassgroup_master ccg ON ccg.classgroupunkid = ctr.classgroupunkid " & _
                         " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = st.reasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.STAFF_TRANSFER & _
                         " LEFT JOIN ( " & _
                         "                      SELECT " & _
                         "                              transferrequestunkid " & _
                         "                             ,sta.approvaldate " & _
                         "                             ,sta.approvalremark " & _
                         "                             ,statusunkid " & _
                         "                      FROM sttransfer_approval_tran sta " & _
                         "                      WHERE isvoid = 0 AND statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Approved & _
                         "                      AND sta.priority IN ( " & _
                         "                                                      SELECT " & _
                         "                                                              priority " & _
                         "                                                       FROM ( " & _
                         "                                                                      SELECT " & _
                         "                                                                              sttransfer_approval_tran.priority " & _
                         "                                                                             ,sttransfer_approval_tran.transferrequestunkid " & _
                         "                                                                             ,sttransfer_approval_tran.employeeunkid " & _
                         "                                                                             ,DENSE_RANK() OVER (PARTITION BY sttransfer_approval_tran.transferrequestunkid ORDER BY sttransfer_approval_tran.priority DESC) AS rno " & _
                         "                                                                      FROM sttransfer_approval_tran " & _
                         "                                                                      JOIN stapproverlevel_role_mapping ON sttransfer_approval_tran.stmappingunkid = stapproverlevel_role_mapping.stmappingunkid " & _
                         "                                                                      JOIN stapproverlevel_master ON stapproverlevel_master.stlevelunkid = stapproverlevel_role_mapping.levelunkid " & _
                         "                                                                      WHERE sttransfer_approval_tran.isvoid = 0 " & _
                         "                                                                  ) AS cnt " & _
                         "                                                        WHERE cnt.rno = 1 AND cnt.priority = sta.priority AND cnt.transferrequestunkid = sta.transferrequestunkid AND cnt.employeeunkid = sta.employeeunkid " & _
                         "                                                    ) " & _
                         "                  ) AS App ON App.transferrequestunkid = st.transferrequestunkid " & _
                         " WHERE st.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDateFilterQry & " "
            End If


            FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstransfer_request_tran", 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstransfer_request_tran", 3, "Approved"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0)

            If mdtTableExcel.Columns.Contains("transferrequestunkid") Then
                mdtTableExcel.Columns.Remove("transferrequestunkid")
            End If


            mdtTableExcel.Columns("Sr.No").Caption = Language.getMessage(mstrModuleName, 14, "Sr.No")
            mdtTableExcel.Columns("Staff No").Caption = Language.getMessage(mstrModuleName, 15, "Staff No")
            mdtTableExcel.Columns("Staff Name").Caption = Language.getMessage(mstrModuleName, 16, "Staff Name")
            mdtTableExcel.Columns("Previous Role").Caption = Language.getMessage(mstrModuleName, 17, "Previous Role")
            mdtTableExcel.Columns("Current Role").Caption = Language.getMessage(mstrModuleName, 18, "Current Role")
            mdtTableExcel.Columns("Previous Zone").Caption = Language.getMessage(mstrModuleName, 19, "Previous Zone")
            mdtTableExcel.Columns("Current Zone").Caption = Language.getMessage(mstrModuleName, 20, "Current Zone")
            mdtTableExcel.Columns("To Zone").Caption = Language.getMessage(mstrModuleName, 21, "To Zone")
            mdtTableExcel.Columns("Reason").Caption = Language.getMessage(mstrModuleName, 22, "Reason")
            mdtTableExcel.Columns("Date Recevied").Caption = Language.getMessage(mstrModuleName, 23, "Date Recevied")
            mdtTableExcel.Columns("Status").Caption = Language.getMessage(mstrModuleName, 24, "Status")
            mdtTableExcel.Columns("TAT Days").Caption = Language.getMessage(mstrModuleName, 25, "TAT Days")


            If mintViewIndex <= 0 Then
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns.Remove("GName")
            Else
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            ''SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 90
            Next
            'SET EXCEL CELL WIDTH

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_StaffTransferReport; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clstransfer_request_tran", 2, "Pending")
            Language.setMessage("clstransfer_request_tran", 3, "Approved")
            Language.setMessage(mstrModuleName, 1, " Employee :")
            Language.setMessage(mstrModuleName, 2, " Request From Date:")
            Language.setMessage(mstrModuleName, 3, " Request To Date:")
            Language.setMessage(mstrModuleName, 4, " From Zone :")
            Language.setMessage(mstrModuleName, 5, " Destination Zone :")
            Language.setMessage(mstrModuleName, 6, " Job :")
            Language.setMessage(mstrModuleName, 7, " Reason :")
            Language.setMessage(mstrModuleName, 8, "Status :")
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
            Language.setMessage(mstrModuleName, 11, "Checked By :")
            Language.setMessage(mstrModuleName, 12, "Approved By")
            Language.setMessage(mstrModuleName, 13, "Received By :")
            Language.setMessage(mstrModuleName, 14, "Sr.No")
            Language.setMessage(mstrModuleName, 15, "Staff No")
            Language.setMessage(mstrModuleName, 16, "Staff Name")
            Language.setMessage(mstrModuleName, 17, "Previous Role")
            Language.setMessage(mstrModuleName, 18, "Current Role")
            Language.setMessage(mstrModuleName, 19, "Previous Zone")
            Language.setMessage(mstrModuleName, 20, "Current Zone")
            Language.setMessage(mstrModuleName, 21, "To Zone")
            Language.setMessage(mstrModuleName, 22, "Reason")
            Language.setMessage(mstrModuleName, 23, "Date Recevied")
            Language.setMessage(mstrModuleName, 24, "Status")
            Language.setMessage(mstrModuleName, 25, "TAT Days")
            Language.setMessage(mstrModuleName, 26, "Order By :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
