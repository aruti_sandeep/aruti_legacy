#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

#End Region

Public Class frmStaffMovement_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmStaffMovement_Report"
    Private objStaffMovement As clsStaffMovementReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrLeaveTypeIDs As String = ""
    Private dsLeaveType As DataSet = Nothing
    Private mstrSelectedLeaveTypeIDs As String = ""
#End Region

#Region "Enum"

    Private Enum enControlID
        Spouse = 1
        Child = 2
        MemberShip = 3
        Absent = 4
        LeaveType = 5
        Annual_Leave = 6
        Sick_Leave = 7
        Other_Earning = 8
    End Enum

#End Region

#Region " Constructor "

    Public Sub New()
        objStaffMovement = New clsStaffMovementReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objStaffMovement.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet

        Try
            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmp.GetEmployeeList("List", True)
            'Else
            '    dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            Dim objJob As New clsJobs
            dsList = objJob.getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objJob = Nothing

            Dim objClassGrp As New clsClassGroup
            dsList = objClassGrp.getComboList("List", True)
            With cboLocation
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objClassGrp = Nothing

            Dim objRelation As New clsCommon_Master
            dsList = objRelation.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "List")
            With CboSpouse
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With

            With cboChild
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With
            objRelation = Nothing

            Dim objMembership As New clsmembership_master
            dsList = objMembership.getListForCombo("List", True)

            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objMembership = Nothing

            Dim objTranHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("List", True, enTranHeadType.Informational)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.Informational)
            'Sohail (21 Aug 2015) -- End
            With cboAbsent
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objTranHead = Nothing

            Dim objLeaveType As New clsleavetype_master
            dsLeaveType = objLeaveType.GetList("List", True, True, "")

            Dim drRow As DataRow = dsLeaveType.Tables(0).NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavetypecode") = Language.getMessage(mstrModuleName, 10, "Select")
            drRow("leavename") = Language.getMessage(mstrModuleName, 10, "Select")
            dsLeaveType.Tables(0).Rows.InsertAt(drRow, 0)

            RemoveHandler cboAnnualLeave.SelectedIndexChanged, AddressOf cboAnnualLeave_SelectedIndexChanged
            With cboAnnualLeave
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "leavename"
                .DataSource = dsLeaveType.Tables("List").Copy
                .SelectedValue = 0
            End With
            AddHandler cboAnnualLeave.SelectedIndexChanged, AddressOf cboAnnualLeave_SelectedIndexChanged

            RemoveHandler cboSickLeave.SelectedIndexChanged, AddressOf cboAnnualLeave_SelectedIndexChanged
            With cboSickLeave
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "leavename"
                .DataSource = dsLeaveType.Tables("List").Copy
                .SelectedValue = 0
            End With
            AddHandler cboSickLeave.SelectedIndexChanged, AddressOf cboAnnualLeave_SelectedIndexChanged

            Dim objTransactionHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTransactionHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("OtherEarning")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_LeaveType()
        Dim iDataTable As DataTable = Nothing
        Dim dtTable As DataTable = Nothing
        Try
            If mstrSelectedLeaveTypeIDs.Trim.Length > 0 Then
                dtTable = New DataView(dsLeaveType.Tables(0), "leavetypeunkid not in (" & mstrSelectedLeaveTypeIDs & ") AND leavetypeunkid <> 0 ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsLeaveType.Tables(0), "leavetypeunkid <> 0 ", "", DataViewRowState.CurrentRows).ToTable
            End If
            lvLeaveType.Items.Clear()
            Dim iStringValue As String = String.Empty
            For Each iRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem
                iStringValue = "" : iStringValue = iRow.Item("leavetypecode").ToString
                If iStringValue.Trim.Length > 0 Then
                    iStringValue &= " - " & iRow.Item("leavename").ToString
                Else
                    iStringValue &= iRow.Item("leavename").ToString
                End If
                lvItem.Text = iStringValue
                lvItem.Tag = CInt(iRow.Item("leavetypeunkid"))

                If mstrLeaveTypeIDs.Trim <> "" Then
                    If mstrLeaveTypeIDs.Split(",").Contains(iRow.Item("leavetypeunkid").ToString) = True Then
                        lvItem.Checked = True
                    End If
                End If

                lvLeaveType.Items.Add(lvItem)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_LeaveType", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboLocation.SelectedValue = 0
            CboSpouse.SelectedValue = 0
            cboChild.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboAbsent.SelectedValue = 0
            cboAnnualLeave.SelectedValue = 0
            cboSickLeave.SelectedValue = 0
            chkSelectAll.Checked = False
            chkInactiveemp.Checked = False
            gbBasicSalaryOtherEarning.Checked = False
            objStaffMovement.setDefaultOrderBy(0)
            mstrAdvanceFilter = ""
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Staff_Movement_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enControlID.Spouse
                            CboSpouse.SelectedValue = dsRow.Item("transactionheadid").ToString

                        Case enControlID.Child
                            cboChild.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enControlID.MemberShip
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enControlID.Absent
                            cboAbsent.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enControlID.LeaveType
                            mstrLeaveTypeIDs = dsRow.Item("transactionheadid").ToString()

                        Case enControlID.Annual_Leave
                            cboAnnualLeave.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enControlID.Sick_Leave
                            cboSickLeave.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enControlID.Other_Earning
                            cboOtherEarning.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            If CInt(cboOtherEarning.SelectedValue) > 0 Then
                                gbBasicSalaryOtherEarning.Checked = True
                            End If

                    End Select
                Next

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            ElseIf CInt(CboSpouse.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Spouse is compulsory information.Please Select Spouse."), enMsgBoxStyle.Information)
                CboSpouse.Select()
                Return False
            ElseIf CInt(cboChild.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Child is compulsory information.Please Select Child."), enMsgBoxStyle.Information)
                cboChild.Select()
                Return False
            ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Membership is compulsory information.Please Select Membership."), enMsgBoxStyle.Information)
                cboMembership.Select()
                Return False
            ElseIf CInt(cboAbsent.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Absent is compulsory information.Please Select Absent."), enMsgBoxStyle.Information)
                cboAbsent.Select()
                Return False
            ElseIf CInt(cboAnnualLeave.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Annual Leave is compulsory information.Please Select Annual Leave."), enMsgBoxStyle.Information)
                cboAnnualLeave.Select()
                Return False
            ElseIf CInt(cboSickLeave.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sick Leave is compulsory information.Please Select Sick Leave."), enMsgBoxStyle.Information)
                cboSickLeave.Select()
                Return False
            ElseIf lvLeaveType.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please tick atleast one leave type."), enMsgBoxStyle.Information)
                lvLeaveType.Select()
                Return False
            End If

            objStaffMovement.SetDefaultValue()

            Dim iSelectedTag As List(Of String) = lvLeaveType.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
            mstrLeaveTypeIDs = String.Join(",", iSelectedTag.ToArray())

            objStaffMovement._PeriodId = CInt(cboPeriod.SelectedValue)
            objStaffMovement._Period = cboPeriod.Text
            objStaffMovement._EmployeeID = cboEmployee.SelectedValue
            objStaffMovement._EmployeeName = cboEmployee.Text
            objStaffMovement._JobID = CInt(cboJob.SelectedValue)
            objStaffMovement._Job = cboJob.Text
            objStaffMovement._LocationID = CInt(cboLocation.SelectedValue)
            objStaffMovement._Location = cboLocation.Text
            objStaffMovement._SpouseID = CInt(CboSpouse.SelectedValue)
            objStaffMovement._Spouse = CboSpouse.Text
            objStaffMovement._ChildID = CInt(cboChild.SelectedValue)
            objStaffMovement._Child = cboChild.Text
            objStaffMovement._MembershipId = CInt(cboMembership.SelectedValue)
            objStaffMovement._Membership = cboMembership.Text
            objStaffMovement._AbsentId = CInt(cboAbsent.SelectedValue)
            objStaffMovement._Absent = cboAbsent.Text
            objStaffMovement._LeaveIds = mstrLeaveTypeIDs
            objStaffMovement._AnnualLeaveId = CInt(cboAnnualLeave.SelectedValue)
            objStaffMovement._AnnualLeave = cboAnnualLeave.Text
            objStaffMovement._SickLeaveId = CInt(cboSickLeave.SelectedValue)
            objStaffMovement._SickLeave = cboSickLeave.Text
            objStaffMovement._Advance_Filter = mstrAdvanceFilter
            objStaffMovement._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objStaffMovement._YearID = FinancialYear._Object._YearUnkid
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            objStaffMovement._AsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'Sohail (18 May 2019) -- End

            If gbBasicSalaryOtherEarning.Checked = True Then
                objStaffMovement._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objStaffMovement._OtherEarningTranId = 0
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmStaffMovement_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objStaffMovement = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffMovement_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffMovement_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objStaffMovement = New clsStaffMovementReport(User._Object._Languageunkid,Company._Object._Companyunkid)
            Call Language.setLanguage(Me.Name)
            eZeeHeader.Title = objStaffMovement._ReportName
            eZeeHeader.Message = objStaffMovement._ReportDesc
            Call FillCombo()
            Call ResetValue()
            GetValue()
            Fill_LeaveType()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffMovement_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffMovement_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffMovement_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffMovement_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffMovement_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Try
            Dim frm As New frmAdvanceSearch
            Try
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                frm.ShowDialog()
                mstrAdvanceFilter = frm._GetFilterString
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
            Finally
                If frm IsNot Nothing Then frm.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Sub

            For iSelect As Integer = 1 To 8
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Staff_Movement_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case iSelect

                    Case enControlID.Spouse
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = CInt(CboSpouse.SelectedValue)
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                    Case enControlID.Child
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = CInt(cboChild.SelectedValue)
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                    Case enControlID.MemberShip
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = CInt(cboMembership.SelectedValue)
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                    Case enControlID.Absent
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = CInt(cboAbsent.SelectedValue)
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                    Case enControlID.LeaveType
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = mstrLeaveTypeIDs
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                    Case enControlID.Annual_Leave
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = CInt(cboAnnualLeave.SelectedValue)
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                    Case enControlID.Sick_Leave
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = CInt(cboSickLeave.SelectedValue)
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                    Case enControlID.Other_Earning
                        objUserDefRMode._Headtypeid = iSelect
                        objUserDefRMode._EarningTranHeadIds = CInt(cboOtherEarning.SelectedValue)
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Staff_Movement_Report, 0, 0, iSelect)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Selection Saved Successfully."), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                'objStaffMovement.Generate_DetailReport()
                Dim objperiod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                objStaffMovement.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       objperiod._End_Date, _
                                                       objperiod._End_Date, _
                                                       ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._Base_CurrencyId)
                'Shani(24-Aug-2015) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
            Fill_LeaveType()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboPeriod.DataSource, DataTable)
            frm.ValueMember = cboPeriod.ValueMember
            frm.DisplayMember = cboPeriod.DisplayMember
            If frm.DisplayDialog Then
                cboPeriod.SelectedValue = frm.SelectedValue
                cboPeriod.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchjob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchjob.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboJob.DataSource, DataTable)
            frm.ValueMember = cboJob.ValueMember
            frm.DisplayMember = cboJob.DisplayMember
            If frm.DisplayDialog Then
                cboJob.SelectedValue = frm.SelectedValue
                cboJob.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchjob_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLocation.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLocation.DataSource, DataTable)
            frm.ValueMember = cboLocation.ValueMember
            frm.DisplayMember = cboLocation.DisplayMember
            If frm.DisplayDialog Then
                cboLocation.SelectedValue = frm.SelectedValue
                cboLocation.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLocation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchSpouse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSpouse.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(CboSpouse.DataSource, DataTable)
            frm.ValueMember = CboSpouse.ValueMember
            frm.DisplayMember = CboSpouse.DisplayMember
            If frm.DisplayDialog Then
                CboSpouse.SelectedValue = frm.SelectedValue
                CboSpouse.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSpouse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchChild.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboChild.DataSource, DataTable)
            frm.ValueMember = cboChild.ValueMember
            frm.DisplayMember = cboChild.DisplayMember
            If frm.DisplayDialog Then
                cboChild.SelectedValue = frm.SelectedValue
                cboChild.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchChild_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboMembership.DataSource, DataTable)
            frm.ValueMember = cboMembership.ValueMember
            frm.DisplayMember = cboMembership.DisplayMember
            If frm.DisplayDialog Then
                cboMembership.SelectedValue = frm.SelectedValue
                cboMembership.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAbsent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAbsent.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboAbsent.DataSource, DataTable)
            frm.ValueMember = cboAbsent.ValueMember
            frm.DisplayMember = cboAbsent.DisplayMember
            If frm.DisplayDialog Then
                cboAbsent.SelectedValue = frm.SelectedValue
                cboAbsent.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAbsent_ClickobjbtnSearchAbsent_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAnnualLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAnnualLeave.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboAnnualLeave.DataSource, DataTable)
            frm.ValueMember = cboAnnualLeave.ValueMember
            frm.DisplayMember = cboAnnualLeave.DisplayMember
            If frm.DisplayDialog Then
                cboAnnualLeave.SelectedValue = frm.SelectedValue
                cboAnnualLeave.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAnnualLeave_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchSickLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSickLeave.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboSickLeave.DataSource, DataTable)
            frm.ValueMember = cboSickLeave.ValueMember
            frm.DisplayMember = cboSickLeave.DisplayMember
            If frm.DisplayDialog Then
                cboSickLeave.SelectedValue = frm.SelectedValue
                cboSickLeave.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSickLeave_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboOtherEarning
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffMovementReport.SetMessages()
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            For i As Integer = 0 To lvLeaveType.Items.Count - 1
                RemoveHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
                lvLeaveType.Items(i).Checked = chkSelectAll.Checked
                AddHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"



#End Region

#Region "Listview Event"

    Private Sub lvLeaveType_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLeaveType.ItemChecked
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If lvLeaveType.CheckedItems.Count <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvLeaveType.CheckedItems.Count < lvLeaveType.Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvLeaveType.CheckedItems.Count = lvLeaveType.Items.Count Then
                chkSelectAll.CheckState = CheckState.Checked

            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveType_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboAnnualLeave_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAnnualLeave.SelectedIndexChanged, cboSickLeave.SelectedIndexChanged
        Try
            If CInt(cboAnnualLeave.SelectedValue) > 0 Then
                mstrSelectedLeaveTypeIDs = CInt(cboAnnualLeave.SelectedValue) & ","
            End If
            If CInt(cboSickLeave.SelectedValue) > 0 Then
                mstrSelectedLeaveTypeIDs &= CInt(cboSickLeave.SelectedValue) & ","
            End If

            If mstrSelectedLeaveTypeIDs.Trim.Length > 0 Then
                mstrSelectedLeaveTypeIDs = mstrSelectedLeaveTypeIDs.Substring(0, mstrSelectedLeaveTypeIDs.Trim.Length - 1)
            End If

            Fill_LeaveType()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAnnualLeave_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "eZeeCollapsiable Container Event"

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
			Me.LblLocation.Text = Language._Object.getCaption(Me.LblLocation.Name, Me.LblLocation.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblJob.Text = Language._Object.getCaption(Me.LblJob.Name, Me.LblJob.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.LblLeaveType.Text = Language._Object.getCaption(Me.LblLeaveType.Name, Me.LblLeaveType.Text)
			Me.colhLeaveType.Text = Language._Object.getCaption(CStr(Me.colhLeaveType.Tag), Me.colhLeaveType.Text)
			Me.LblSpouse.Text = Language._Object.getCaption(Me.LblSpouse.Name, Me.LblSpouse.Text)
			Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)
			Me.LblChild.Text = Language._Object.getCaption(Me.LblChild.Name, Me.LblChild.Text)
			Me.LblAbsent.Text = Language._Object.getCaption(Me.LblAbsent.Name, Me.LblAbsent.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.LblSickLeave.Text = Language._Object.getCaption(Me.LblSickLeave.Name, Me.LblSickLeave.Text)
			Me.LblAnnualLeave.Text = Language._Object.getCaption(Me.LblAnnualLeave.Name, Me.LblAnnualLeave.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period.")
			Language.setMessage(mstrModuleName, 2, "Spouse is compulsory information.Please Select Spouse.")
			Language.setMessage(mstrModuleName, 3, "Child is compulsory information.Please Select Child.")
			Language.setMessage(mstrModuleName, 4, "Membership is compulsory information.Please Select Membership.")
			Language.setMessage(mstrModuleName, 5, "Absent is compulsory information.Please Select Absent.")
			Language.setMessage(mstrModuleName, 6, "Annual Leave is compulsory information.Please Select Annual Leave.")
			Language.setMessage(mstrModuleName, 7, "Sick Leave is compulsory information.Please Select Sick Leave.")
			Language.setMessage(mstrModuleName, 8, "Please tick atleast one leave type.")
			Language.setMessage(mstrModuleName, 9, "Selection Saved Successfully.")
			Language.setMessage(mstrModuleName, 10, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class
