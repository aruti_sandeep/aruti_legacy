'************************************************************************************************************************************
'Class Name : frmCompanyActiveEmployeeReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

'Last Message Index = 2

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCompanyActiveEmployeeReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCompanyActiveEmployeeReport"
    Private objCEmpReport As clsCompanyActiveEmployeeReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnIsConfig As Boolean = False
    Private mPanel As Panel

#End Region

    Public WriteOnly Property _mPanel() As Panel
        Set(ByVal value As Panel)
            mPanel = value
        End Set
    End Property
    Public WriteOnly Property _IsConfig() As Boolean
        Set(ByVal value As Boolean)
            mblnIsConfig = value
        End Set
    End Property

#Region " Contructor "

    Public Sub New()
        objCEmpReport = New clsCompanyActiveEmployeeReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCEmpReport.SetDefaultValue()
        InitializeComponent()
        '  _Show_AdvanceFilter = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim objCompany As New clsCompany_Master
            dsList = objCompany.GetList("Company", True)

            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("companyunkid") = 0
            drRow("code") = Language.getMessage(mstrModuleName, 1, "Select")
            drRow("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Company")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCompany.SelectedValue = 0
            dtpFromdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpTodate.Value = ConfigParameter._Object._CurrentDateAndTime
            mstrAdvanceFilter = ""
            If mblnIsConfig = True Then
                If mPanel.Enabled = True Then mPanel.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objCEmpReport.SetDefaultValue()
            
            objCEmpReport._CompanyUnkId = CInt(cboCompany.SelectedValue)
            objCEmpReport._Company = cboCompany.Text
            objCEmpReport._FromDate = dtpFromdate.Value.Date
            'Sohail (20 Jan 2017) -- Start
            'Issue - 64.1 - Company Active Employee count was coming as per from date and to date but now to date is not visible.
            'objCEmpReport._ToDate = dtpTodate.Value.Date
            objCEmpReport._ToDate = dtpFromdate.Value.Date
            'Sohail (20 Jan 2017) -- End
            objCEmpReport._Advance_Filter = mstrAdvanceFilter
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmCompanyActiveEmployeeReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCEmpReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCompanyActiveEmployeeReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompanyActiveEmployeeReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Me._Title = Language.getMessage(mstrModuleName, 2, "Company Active Employee Report") 'objCEmpReport._ReportName"
            'Pinkal (10-Jul-2014) -- End
            Me._Message = objCEmpReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompanyActiveEmployeeReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompanyActiveEmployeeReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmCompanyActiveEmployeeReport_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCompanyActiveEmployeeReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompanyActiveEmployeeReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCompanyActiveEmployeeReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmCompanyActiveEmployeeReport_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Sohail (15 Feb 2016) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objCEmpReport.generateReport(0, e.Type, enExportAction.None)

            'Sohail (20 Jan 2017) -- Start
            'Issue - 64.1 - Company Active Employee count was coming as per from date and to date but now to date is not visible.
            'objCEmpReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpFromdate.Value.Date, dtpTodate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            objCEmpReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpFromdate.Value.Date, dtpFromdate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (20 Jan 2017) -- End
            'Sohail ((15 Feb 2016) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCompanyActiveEmployeeReport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCompanyActiveEmployeeReport_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Sohail (15 Feb 2016) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objCEmpReport.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (20 Jan 2017) -- Start
            'Issue - 64.1 - Company Active Employee count was coming as per from date and to date but now to date is not visible.
            'objCEmpReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpFromdate.Value.Date, dtpTodate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            objCEmpReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpFromdate.Value.Date, dtpFromdate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (20 Jan 2017) -- End
            'Sohail ((15 Feb 2016) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCompanyActiveEmployeeReport_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCompanyActiveEmployeeReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCompanyActiveEmployeeReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompanyActiveEmployeeReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            If mblnIsConfig = True Then
                If mPanel.Enabled = False Then mPanel.Enabled = True
            End If
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCompanyActiveEmployeeReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCompany.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboCompany.DataSource
            frm.ValueMember = cboCompany.ValueMember
            frm.DisplayMember = cboCompany.DisplayMember
            If frm.DisplayDialog Then
                cboCompany.SelectedValue = frm.SelectedValue
                cboCompany.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCompany_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCompanyActiveEmployeeReport.SetMessages()
            objfrm._Other_ModuleNames = "clsCompanyActiveEmployeeReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblCompany.Text = Language._Object.getCaption(Me.LblCompany.Name, Me.LblCompany.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Company Active Employee Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
