'************************************************************************************************************************************
'Class Name : clsTrainingCostReport.vb
'Purpose    :
'Date       :01/07/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text
Imports System.IO

Public Class clsTrainingBudgetReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingBudgetReport"
    Private mstrReportId As String = enArutiReport.TrainingBudgetReport
    Dim objDataOperation As clsDataOperation
    Dim dtFinalTable As DataTable
    Dim dblColTot As Decimal()
    Dim dtSummary As DataTable

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END

        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END

    End Sub

#End Region

#Region " Private variables "

    Private mintDepaermentId As Integer = -1
    Private mstrDepartmentName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintTrainingId As Integer = -1
    Private mstrTrainingName As String = String.Empty
    Private mintInstituteId As Integer = -1
    Private mstrInstituteName As String = String.Empty

    'Sandeep [ 09 MARCH 2011 ] -- Start
    Dim StrFinalPath As String = String.Empty
    'Sandeep [ 09 MARCH 2011 ] -- End 


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End
    'Anjan (10 Feb 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    'Private mstrCourseTitle As String
    Private mintTrainingCourseId As Integer
    Private mstrTrainingCourse As String
    'Anjan (10 Feb 2012)-End 

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private mstrAllocationTypeName As String = String.Empty
    Private mintAllocationTypeId As Integer = 0
    Private mstrAllocationNames As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _DepaermentId() As Integer
        Set(ByVal value As Integer)
            mintDepaermentId = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentName() As String
        Set(ByVal value As String)
            mstrDepartmentName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    'Public WriteOnly Property _TrainingId() As Integer
    '    Set(ByVal value As Integer)
    '        mintTrainingId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _TrainingName() As String
    '    Set(ByVal value As String)
    '        mstrTrainingName = value
    '    End Set
    'End Property

    Public WriteOnly Property _InstituteId() As Integer
        Set(ByVal value As Integer)
            mintInstituteId = value
        End Set
    End Property

    Public WriteOnly Property _InstituteName() As String
        Set(ByVal value As String)
            mstrInstituteName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End
    'Anjan (10 Feb 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingCourse = value
        End Set
    End Property
    Public WriteOnly Property _TrainingId() As Integer
        Set(ByVal value As Integer)
            mintTrainingCourseId = value
        End Set
    End Property
    'Anjan (10 Feb 2012)-End 
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Public WriteOnly Property _AllocationTypeName() As String
        Set(ByVal value As String)
            mstrAllocationTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationNames() As String
        Set(ByVal value As String)
            mstrAllocationNames = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintDepaermentId = 0
            mstrDepartmentName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintTrainingId = 0
            mstrTrainingName = ""
            mintInstituteId = 0
            mstrInstituteName = ""

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mintTrainingCourseId = 0
            mstrTrainingCourse = ""
            'Anjan (10 Feb 2012)-End 

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            mstrAllocationTypeName = String.Empty
            mintAllocationTypeId = 0
            mstrAllocationNames = String.Empty
            mstrAllocationIds = String.Empty
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            objDataOperation.AddParameter("@Progress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 117, "Enrolled"))
            objDataOperation.AddParameter("@Postponed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 118, "Postponed"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 119, "Completed"))

            If mintDepaermentId > 0 Then
                Me._FilterQuery &= "AND departmentunkid = '" & mintDepaermentId & "'"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Department :") & " " & mstrDepartmentName & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= "AND employeeunkid = '" & mintEmployeeId & "'"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
            End If


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If mintTrainingId > 0 Then
            '    Me._FilterQuery &= "AND trainingschedulingunkid = '" & mintTrainingId & "'"
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Training :") & " " & mstrTrainingName & " "
            'End If

            If mintTrainingCourseId > 0 Then
                Me._FilterQuery &= "AND masterunkid = '" & mintTrainingCourseId & "'"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Training :") & " " & mstrTrainingCourse & " "
            End If
            'Anjan (10 Feb 2012)-End 

            If mintInstituteId > 0 Then
                Me._FilterQuery &= "AND instituteunkid = '" & mintInstituteId & "'"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Institute :") & " " & mstrInstituteName & " "
            End If

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If mintAllocationTypeId > 0 Then
                objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
                Me._FilterQuery &= "AND allocationtypeunkid = '" & mintAllocationTypeId & "' "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Allocation Binded :") & " " & mstrAllocationTypeName & " "
            End If
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub CreateTable()
        Try
            dtFinalTable = New DataTable("Detail")
            Dim dCol As DataColumn

            dCol = New DataColumn("Department")
            dCol.Caption = Language.getMessage(mstrModuleName, 26, "Department")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Topic")
            dCol.Caption = Language.getMessage(mstrModuleName, 27, "Topic")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            dCol = New DataColumn("allocation")
            dCol.Caption = Language.getMessage(mstrModuleName, 200, "Allocation Binded")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            'S.SANDEEP [08-FEB-2017] -- END


            dCol = New DataColumn("Trainees")
            dCol.Caption = Language.getMessage(mstrModuleName, 28, "Trainees")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Eligibility")
            dCol.Caption = Language.getMessage(mstrModuleName, 29, "Eligibility")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Trainers")
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "Trainers")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("ADDRESS")
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "Address")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("StDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "Start Date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("EdDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "End Date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Remark")
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Remark")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Status")
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "Status")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Duration")
            dCol.Caption = Language.getMessage(mstrModuleName, 11, "Duration")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Fees")
            dCol.Caption = Language.getMessage(mstrModuleName, 12, "Fees")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Misc")
            dCol.Caption = Language.getMessage(mstrModuleName, 13, "Misc.")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Travel")
            dCol.Caption = Language.getMessage(mstrModuleName, 14, "Travel")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Accommodation")
            dCol.Caption = Language.getMessage(mstrModuleName, 15, "Accommodation")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Allowance")
            dCol.Caption = Language.getMessage(mstrModuleName, 16, "Allowance")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Meals")
            dCol.Caption = Language.getMessage(mstrModuleName, 17, "Meals")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Material")
            dCol.Caption = Language.getMessage(mstrModuleName, 18, "Material")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Exam")
            dCol.Caption = Language.getMessage(mstrModuleName, 30, "Exam")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("TotalCost")
            dCol.Caption = Language.getMessage(mstrModuleName, 19, "TotalCost")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            'HEADER PART
            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If

                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 21, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 22, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            For j As Integer = 0 To objDataReader.Columns.Count - 1
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    'Anjan (22 Mar 2011)-Start

                    If k > 10 Or k = 1 Then 'Anjan (10 Feb 2012)-Start  'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                    'Anjan (22 Mar 2011)-End

                    'Sandeep [ 10 FEB 2011 ] -- End 
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next
            strBuilder.Append(" <TR> " & vbCrLf)

            For k As Integer = 0 To objDataReader.Columns.Count - 1
                If k = 1 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 24, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                ElseIf k >= 12 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                End If
            Next

            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=50%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE' colspan=2><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 23, "Summary") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To dtSummary.Columns.Count - 1
                'Sandeep [ 10 FEB 2011 ] -- Start
                'strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dtSummary.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2><B>" & dtSummary.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                'Sandeep [ 10 FEB 2011 ] -- End 
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            Dim intCntTrainee As Integer = 0
            For i As Integer = 0 To dtSummary.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To dtSummary.Columns.Count - 1
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & dtSummary.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dtSummary.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    'Sandeep [ 10 FEB 2011 ] -- End 
                Next
                intCntTrainee += dtSummary.Rows(i)(1)
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Sandeep [ 10 FEB 2011 ] -- Start
            'strBuilder.Append(" <TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 24, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
            'strBuilder.Append(" <TD BORDER=1 WIDTH='60%' ALIGN='RIGHT'><FONT SIZE=2><B>" & intCntTrainee & "</B></FONT></TD>" & vbCrLf)

            strBuilder.Append(" <TD BORDER=1 WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 24, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2><B>" & intCntTrainee & "</B></FONT></TD>" & vbCrLf)
            'Sandeep [ 10 FEB 2011 ] -- End 

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" </HTML> " & vbCrLf)


            'Sandeep [ 09 MARCH 2011 ] -- Start
            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If
            'Sandeep [ 09 MARCH 2011 ] -- End 

            'Anjan (14 Apr 2011)-Start
            'Issue : To remove "\" from path which comes at end in path.
            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If
            'Anjan (14 Apr 2011)-End

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                'Sandeep [ 09 MARCH 2011 ] -- Start
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                'Sandeep [ 09 MARCH 2011 ] -- End 
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Sub ExportTrainingBudget_Report()
    Public Sub ExportTrainingBudget_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean)
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim dtTable As New DataTable
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '         " Department AS Department " & _
            '         ",Topic AS Topic " & _
            '         ",Trainees AS Trainees " & _
            '         ",Eligibility AS Eligibility " & _
            '         ",Trainers AS Trainers " & _
            '         ",Address+' '+Country+' '+State+' '+City AS ADDRESS " & _
            '         ",StDate AS StDate " & _
            '         ",EdDate AS EdDate " & _
            '         ",Remark AS Remark " & _
            '         ",Status AS Status " & _
            '         ",Duration AS Duration " & _
            '         ",Fees AS Fees " & _
            '         ",Misc AS Misc " & _
            '         ",Travel AS Travel " & _
            '         ",Accommodation AS Accommodation " & _
            '         ",Allowance AS Allowance " & _
            '         ",Meals AS Meals " & _
            '         ",Material AS Material " & _
            '         ",Exam AS Exam " & _
            '         ",(Fees+Misc+Travel+Accommodation+Allowance+Meals+Material+Exam) AS TotalCost " & _
            '         ",status_id " & _
            '         ",employeeunkid " & _
            '         ",trainingschedulingunkid " & _
            '         ",instituteunkid " & _
            '         ",departmentunkid " & _
            '    "FROM " & _
            '    "( " & _
            '         "SELECT " & _
            '              " ISNULL(hrdepartment_master.name,'') AS Department " & _
            '              ",ISNULL(hrtraining_scheduling.course_title,'') AS Topic " & _
            '              ",ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') AS Trainees " & _
            '              ",ISNULL(hrtraining_scheduling.eligibility_criteria,'') AS Eligibility " & _
            '              ",ISNULL(hrinstitute_master.institute_name,'') AS Trainers " & _
            '              ",ISNULL(hrinstitute_master.institute_address,'') AS Address " & _
            '              ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '              ",ISNULL(hrmsConfiguration.dbo.cfstate_master.name,'') AS State " & _
            '              ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '              ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS StDate " & _
            '              ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS EdDate " & _
            '              ",ISNULL(hrtraining_scheduling.training_remark,'') AS Remark " & _
            '              ",CASE WHEN  hrtraining_enrollment_tran.status_id = 1 THEN @Progress " & _
            '                          "WHEN  hrtraining_enrollment_tran.status_id = 2 THEN @Postponed " & _
            '                          "WHEN  hrtraining_enrollment_tran.status_id = 3 THEN @Completed " & _
            '              " END AS Status " & _
            '              ",DATEDIFF(DAY,hrtraining_scheduling.start_date,hrtraining_scheduling.end_date) AS Duration " & _
            '              ",ISNULL(hrtraining_scheduling.fees,0) AS Fees " & _
            '              ",ISNULL(hrtraining_scheduling.misc,0) AS Misc " & _
            '              ",ISNULL(hrtraining_scheduling.travel,0) AS Travel " & _
            '              ",ISNULL(hrtraining_scheduling.accomodation,0) AS Accommodation " & _
            '              ",ISNULL(hrtraining_scheduling.allowance,0) AS Allowance " & _
            '              ",ISNULL(hrtraining_scheduling.meals,0) AS Meals " & _
            '              ",ISNULL(hrtraining_scheduling.material,0) AS Material " & _
            '              ",ISNULL(hrtraining_scheduling.exam,0) AS Exam " & _
            '              ",hrtraining_enrollment_tran.status_id " & _
            '              ",hremployee_master.employeeunkid " & _
            '              ",hrtraining_scheduling.trainingschedulingunkid " & _
            '              ",hrinstitute_master.instituteunkid " & _
            '              ",hrdepartment_master.departmentunkid " & _
            '         "FROM hrtraining_enrollment_tran " & _
            '              "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '              "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '              "LEFT JOIN hrmsConfiguration..cfcity_master ON hrinstitute_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '              "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrinstitute_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '              "LEFT JOIN hrmsConfiguration..cfstate_master ON hrinstitute_master.stateunkid = hrmsConfiguration.dbo.cfstate_master.stateunkid " & _
            '              "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '              "JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '              "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '         "WHERE hrtraining_scheduling.iscancel = 0 " & _
            '              "AND hrtraining_enrollment_tran.iscancel = 0 " & _
            '              "AND hrtraining_enrollment_tran.isvoid = 0 " & _
            '              "AND hrtraining_scheduling.isvoid=0 "

            StrQ = "SELECT " & _
                     " Department AS Department " & _
                     ",Topic AS Topic " & _
                     ",Trainees AS Trainees " & _
                     ",Eligibility AS Eligibility " & _
                     ",Trainers AS Trainers " & _
                     ",Address+' '+Country+' '+State+' '+City AS ADDRESS " & _
                     ",StDate AS StDate " & _
                     ",EdDate AS EdDate " & _
                     ",Remark AS Remark " & _
                     ",Status AS Status " & _
                     ",Duration AS Duration " & _
                     ",Fees AS Fees " & _
                     ",Misc AS Misc " & _
                     ",Travel AS Travel " & _
                     ",Accommodation AS Accommodation " & _
                     ",Allowance AS Allowance " & _
                     ",Meals AS Meals " & _
                     ",Material AS Material " & _
                     ",Exam AS Exam " & _
                     ",(Fees+Misc+Travel+Accommodation+Allowance+Meals+Material+Exam) AS TotalCost " & _
                     ",status_id " & _
                     ",employeeunkid " & _
                     ",trainingschedulingunkid " & _
                     ",instituteunkid " & _
                     ",departmentunkid " & _
                     ",masterunkid " & _
                     ",Years " & _
                     ",Months " & _
                     ",Days " & _
                     ",allocationtypeunkid " & _
                "FROM " & _
                "( " & _
                     "SELECT " & _
                          " ISNULL(hrdepartment_master.name,'') AS Department " & _
                          ",cfcommon_master.name AS Topic "

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            '/* ADDED */
            '",Years " & _
            '",Months " & _
            '",Days " & _
            '",allocationtypeunkid " & _
            'S.SANDEEP [08-FEB-2017] -- END


            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '" ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Trainees"
            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS Trainees "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Trainees "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",ISNULL(hrtraining_scheduling.eligibility_criteria,'') AS Eligibility " & _
            '              ",ISNULL(hrinstitute_master.institute_name,'') AS Trainers " & _
            '              ",ISNULL(hrinstitute_master.institute_address,'') AS Address " & _
            '              ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '              ",ISNULL(hrmsConfiguration.dbo.cfstate_master.name,'') AS State " & _
            '              ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '              ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS StDate " & _
            '              ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS EdDate " & _
            '              ",ISNULL(hrtraining_scheduling.training_remark,'') AS Remark " & _
            '              ",CASE WHEN  hrtraining_enrollment_tran.status_id = 1 THEN @Progress " & _
            '                          "WHEN  hrtraining_enrollment_tran.status_id = 2 THEN @Postponed " & _
            '                          "WHEN  hrtraining_enrollment_tran.status_id = 3 THEN @Completed " & _
            '              " END AS Status " & _
            '              ",DATEDIFF(DAY,hrtraining_scheduling.start_date,hrtraining_scheduling.end_date) AS Duration " & _
            '              ",ISNULL(hrtraining_scheduling.fees,0) AS Fees " & _
            '              ",ISNULL(hrtraining_scheduling.misc,0) AS Misc " & _
            '              ",ISNULL(hrtraining_scheduling.travel,0) AS Travel " & _
            '              ",ISNULL(hrtraining_scheduling.accomodation,0) AS Accommodation " & _
            '              ",ISNULL(hrtraining_scheduling.allowance,0) AS Allowance " & _
            '              ",ISNULL(hrtraining_scheduling.meals,0) AS Meals " & _
            '              ",ISNULL(hrtraining_scheduling.material,0) AS Material " & _
            '              ",ISNULL(hrtraining_scheduling.exam,0) AS Exam " & _
            '              ",hrtraining_enrollment_tran.status_id " & _
            '              ",hremployee_master.employeeunkid " & _
            '              ",hrtraining_scheduling.trainingschedulingunkid " & _
            '              ",hrinstitute_master.instituteunkid " & _
            '              ",hrdepartment_master.departmentunkid " & _
            '              ",cfcommon_master.masterunkid " & _
            '         "FROM hrtraining_enrollment_tran " & _
            '              "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '              "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '              "LEFT JOIN hrmsConfiguration..cfcity_master ON hrinstitute_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '              "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrinstitute_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '              "LEFT JOIN hrmsConfiguration..cfstate_master ON hrinstitute_master.stateunkid = hrmsConfiguration.dbo.cfstate_master.stateunkid " & _
            '              "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '              "JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '              "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '              "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) " & _
            '         "WHERE hrtraining_scheduling.iscancel = 0 " & _
            '              "AND hrtraining_enrollment_tran.iscancel = 0 " & _
            '              "AND hrtraining_enrollment_tran.isvoid = 0 " & _
            '              "AND hrtraining_scheduling.isvoid=0 "

            ''Anjan (10 Feb 2012)-End 


            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= " AND hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END


            StrQ &= ",ISNULL(hrtraining_scheduling.eligibility_criteria,'') AS Eligibility " & _
                          ",ISNULL(hrinstitute_master.institute_name,'') AS Trainers " & _
                          ",ISNULL(hrinstitute_master.institute_address,'') AS Address " & _
                          ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                          ",ISNULL(hrmsConfiguration.dbo.cfstate_master.name,'') AS State " & _
                          ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
                          ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS StDate " & _
                          ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS EdDate " & _
                          ",ISNULL(hrtraining_scheduling.training_remark,'') AS Remark " & _
                          ",CASE WHEN  hrtraining_enrollment_tran.status_id = 1 THEN @Progress " & _
                                      "WHEN  hrtraining_enrollment_tran.status_id = 2 THEN @Postponed " & _
                                      "WHEN  hrtraining_enrollment_tran.status_id = 3 THEN @Completed " & _
                          " END AS Status " & _
                          ",DATEDIFF(DAY,hrtraining_scheduling.start_date,hrtraining_scheduling.end_date) AS Duration " & _
                          ",ISNULL(hrtraining_scheduling.fees,0) AS Fees " & _
                          ",ISNULL(hrtraining_scheduling.misc,0) AS Misc " & _
                          ",ISNULL(hrtraining_scheduling.travel,0) AS Travel " & _
                          ",ISNULL(hrtraining_scheduling.accomodation,0) AS Accommodation " & _
                          ",ISNULL(hrtraining_scheduling.allowance,0) AS Allowance " & _
                          ",ISNULL(hrtraining_scheduling.meals,0) AS Meals " & _
                          ",ISNULL(hrtraining_scheduling.material,0) AS Material " & _
                          ",ISNULL(hrtraining_scheduling.exam,0) AS Exam " & _
                          ",hrtraining_enrollment_tran.status_id " & _
                          ",hremployee_master.employeeunkid " & _
                          ",hrtraining_scheduling.trainingschedulingunkid " & _
                          ",hrinstitute_master.instituteunkid " & _
                          ",hrdepartment_master.departmentunkid " & _
                          ",cfcommon_master.masterunkid " & _
                          ",(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
                          ",(DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END AS Months " & _
                          ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),end_date) AS Days " & _
                          ",hrtraining_scheduling.allocationtypeunkid " & _
                     "FROM hrtraining_enrollment_tran " & _
                          "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                          "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
                          "LEFT JOIN hrmsConfiguration..cfcity_master ON hrinstitute_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
                          "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrinstitute_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                          "LEFT JOIN hrmsConfiguration..cfstate_master ON hrinstitute_master.stateunkid = hrmsConfiguration.dbo.cfstate_master.stateunkid " & _
                          "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                departmentunkid " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_transfer_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "   LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid=Alloc.departmentunkid " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                jobunkid " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_categorization_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "   LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                    "   JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) "

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            '/* ADDED */
            '",(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
            '",(DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END AS Months " & _
            '",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),end_date) AS Days " & _
            '",hrtraining_scheduling.allocationtypeunkid " & _
            'S.SANDEEP [08-FEB-2017] -- END


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE hrtraining_scheduling.iscancel = 0 " & _
                          "AND hrtraining_enrollment_tran.iscancel = 0 " & _
                          "AND hrtraining_enrollment_tran.isvoid = 0 " & _
                          "AND hrtraining_scheduling.isvoid=0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            'StrQ &= "  ) AS TBudget "
            StrQ &= "  ) AS TBudget WHERE 1 = 1 "
            'S.SANDEEP [08-FEB-2017] -- END

            Call FilterTitleAndFilterQuery()

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [08-FEB-2017] -- END

            dtSummary = New DataTable("Summary")
            Dim dtCol As New DataColumn

            dtCol = New DataColumn("DeptName")
            dtCol.DataType = System.Type.GetType("System.String")
            dtCol.Caption = Language.getMessage(mstrModuleName, 26, "Department")
            dtSummary.Columns.Add(dtCol)

            dtCol = New DataColumn("Total")
            dtCol.DataType = System.Type.GetType("System.Int32")
            dtCol.Caption = Language.getMessage(mstrModuleName, 25, "Total no of Trainee")
            dtSummary.Columns.Add(dtCol)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Me._FilterQuery.Length > 0 Then
                Me._FilterQuery = Me._FilterQuery.Substring(3)
            End If

            dtTable = New DataView(dsList.Tables("List"), Me._FilterQuery, "", DataViewRowState.CurrentRows).ToTable

            dsList.Tables.Clear()
            dsList.Tables.Add(dtTable)

            Call CreateTable()


            Dim strDepartName As String = String.Empty
            Dim dicDepEmplCnt As New Dictionary(Of String, Integer)

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim dRow As DataRow = dtFinalTable.NewRow
                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                If mstrAllocationIds.Trim.Length > 0 Then
                    If objTrainingAlloc.GetCSV_AllocationName(dtRow.Item("allocationtypeunkid"), dtRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                End If
                Dim iOwnerGroupName As String = String.Empty
                iOwnerGroupName = ""
                Select Case CInt(dtRow.Item("allocationtypeunkid"))
                    Case enAllocation.BRANCH
                        iOwnerGroupName = Language.getMessage("clsMasterData", 430, "Branch")
                    Case enAllocation.DEPARTMENT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 429, "Department Group")
                    Case enAllocation.DEPARTMENT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 428, "Department")
                    Case enAllocation.SECTION_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 427, "Section Group")
                    Case enAllocation.SECTION
                        iOwnerGroupName = Language.getMessage("clsMasterData", 426, "Section")
                    Case enAllocation.UNIT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    Case enAllocation.UNIT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 424, "Unit")
                    Case enAllocation.TEAM
                        iOwnerGroupName = Language.getMessage("clsMasterData", 423, "Team")
                    Case enAllocation.JOB_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 422, "Job Group")
                    Case enAllocation.JOBS
                        iOwnerGroupName = Language.getMessage("clsMasterData", 421, "Jobs")
                    Case enAllocation.CLASS_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 420, "Class Group")
                    Case enAllocation.CLASSES
                        iOwnerGroupName = Language.getMessage("clsMasterData", 419, "Classes")
                    Case enAllocation.COST_CENTER
                        iOwnerGroupName = Language.getMessage("clsMasterData", 586, "Cost Center")
                End Select
                dRow.Item("allocation") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objTrainingAlloc.GetCSV_AllocationName(dtRow.Item("allocationtypeunkid"), dtRow.Item("trainingschedulingunkid"), mstrAllocationIds)
                dRow.Item("allocation") = dRow.Item("allocation").ToString.Replace("&lt;", "<")
                dRow.Item("allocation") = dRow.Item("allocation").ToString.Replace("&gt;", ">")
                dRow.Item("allocation") = dRow.Item("allocation").ToString.Replace("&amp;", "&")
                'S.SANDEEP [08-FEB-2017] -- END


                If strDepartName <> dtRow.Item("Department").ToString Then
                    strDepartName = dtRow.Item("Department").ToString
                    If dicDepEmplCnt.ContainsKey(strDepartName) Then
                        dicDepEmplCnt(strDepartName) += 1
                    Else
                        dicDepEmplCnt.Add(strDepartName, 1)
                    End If
                    dRow.Item("Department") = dtRow.Item("Department")
                Else
                    If dicDepEmplCnt.ContainsKey(strDepartName) Then
                        dicDepEmplCnt(strDepartName) += 1
                    Else
                        dicDepEmplCnt.Add(strDepartName, 1)
                    End If
                End If

                dRow.Item("Topic") = dtRow.Item("Topic")
                dRow.Item("Trainees") = dtRow.Item("Trainees")
                dRow.Item("Eligibility") = dtRow.Item("Eligibility")
                dRow.Item("Trainers") = dtRow.Item("Trainers")
                dRow.Item("ADDRESS") = dtRow.Item("ADDRESS")
                dRow.Item("StDate") = eZeeDate.convertDate(dtRow.Item("StDate").ToString).ToShortDateString
                dRow.Item("EdDate") = eZeeDate.convertDate(dtRow.Item("EdDate").ToString).ToShortDateString
                dRow.Item("Remark") = dtRow.Item("Remark")
                dRow.Item("Status") = dtRow.Item("Status")
                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                'dRow.Item("Duration") = dtRow.Item("Duration")
                Dim strDurationText As String = ""
                If dtRow.Item("Years") > 0 Then
                    strDurationText = dtRow.Item("Years").ToString & " " & Language.getMessage(mstrModuleName, 100, "Year(s)")
                End If
                If dtRow.Item("Months") > 0 Then
                    strDurationText &= " " & dtRow.Item("Months").ToString & " " & Language.getMessage(mstrModuleName, 101, " Month(s)")
                End If
                If dtRow.Item("Days") > 0 Then
                    strDurationText &= " " & dtRow.Item("Days").ToString & " " & Language.getMessage(mstrModuleName, 102, " Day(s)")
                End If
                dRow.Item("Duration") = strDurationText
                'S.SANDEEP [08-FEB-2017] -- END
                dRow.Item("Fees") = Format(CDec(dtRow.Item("Fees")), GUI.fmtCurrency)
                dRow.Item("Misc") = Format(CDec(dtRow.Item("Misc")), GUI.fmtCurrency)
                dRow.Item("Travel") = Format(CDec(dtRow.Item("Travel")), GUI.fmtCurrency)
                dRow.Item("Accommodation") = Format(CDec(dtRow.Item("Accommodation")), GUI.fmtCurrency)
                dRow.Item("Allowance") = Format(CDec(dtRow.Item("Allowance")), GUI.fmtCurrency)
                dRow.Item("Meals") = Format(CDec(dtRow.Item("Meals")), GUI.fmtCurrency)
                dRow.Item("Material") = Format(CDec(dtRow.Item("Material")), GUI.fmtCurrency)
                dRow.Item("Exam") = Format(CDec(dtRow.Item("Exam")), GUI.fmtCurrency)
                dRow.Item("TotalCost") = Format(CDec(dtRow.Item("TotalCost")), GUI.fmtCurrency)

                dtFinalTable.Rows.Add(dRow)
            Next

            For i As Integer = 0 To dicDepEmplCnt.Count - 1
                Dim dtRow As DataRow = dtSummary.NewRow

                dtRow.Item("DeptName") = dicDepEmplCnt.Keys(i)
                dtRow.Item("Total") = dicDepEmplCnt(dicDepEmplCnt.Keys(i))

                dtSummary.Rows.Add(dtRow)
            Next

            Dim dblTotal As Decimal = 0
            Dim m As Integer = 12
            Dim n As Integer = 12
            dblColTot = New Decimal(dtFinalTable.Columns.Count) {}
            While m < dtFinalTable.Columns.Count
                For t As Integer = 0 To dtFinalTable.Rows.Count - 1
                    dblTotal += CDec(dtFinalTable.Rows(t)(m))
                Next
                dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
                dblTotal = 0
                m += 1
                n += 1
            End While

            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
                'Sandeep [ 09 MARCH 2011 ] -- Start
                Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
                'Sandeep [ 09 MARCH 2011 ] -- End 
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportTrainingBudget_Report; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Department :")
            Language.setMessage(mstrModuleName, 2, "Employee :")
            Language.setMessage(mstrModuleName, 3, "Training :")
            Language.setMessage(mstrModuleName, 4, "Institute :")
            Language.setMessage(mstrModuleName, 5, "Trainers")
            Language.setMessage(mstrModuleName, 6, "Address")
            Language.setMessage(mstrModuleName, 7, "Start Date")
            Language.setMessage(mstrModuleName, 8, "End Date")
            Language.setMessage(mstrModuleName, 9, "Remark")
            Language.setMessage(mstrModuleName, 10, "Status")
            Language.setMessage(mstrModuleName, 11, "Duration")
            Language.setMessage(mstrModuleName, 12, "Fees")
            Language.setMessage(mstrModuleName, 13, "Misc.")
            Language.setMessage(mstrModuleName, 14, "Travel")
            Language.setMessage(mstrModuleName, 15, "Accommodation")
            Language.setMessage(mstrModuleName, 16, "Allowance")
            Language.setMessage(mstrModuleName, 17, "Meals")
            Language.setMessage(mstrModuleName, 18, "Material")
            Language.setMessage(mstrModuleName, 19, "TotalCost")
            Language.setMessage(mstrModuleName, 20, "Report successfully exported to Report export path")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Date :")
            Language.setMessage(mstrModuleName, 23, "Summary")
            Language.setMessage(mstrModuleName, 24, "Grand Total :")
            Language.setMessage(mstrModuleName, 25, "Total no of Trainee")
            Language.setMessage(mstrModuleName, 26, "Department")
            Language.setMessage(mstrModuleName, 27, "Topic")
            Language.setMessage(mstrModuleName, 28, "Trainees")
            Language.setMessage(mstrModuleName, 29, "Eligibility")
            Language.setMessage(mstrModuleName, 30, "Exam")
            Language.setMessage("clsMasterData", 117, "Enrolled")
            Language.setMessage("clsMasterData", 118, "Postponed")
            Language.setMessage("clsMasterData", 119, "Completed")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
End Class
