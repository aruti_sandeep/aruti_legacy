'************************************************************************************************************************************
'Class Name : clsUserDetailsLogReport.vb
'Purpose    :
'Date       : 16/10/2014
'Written By : Shani Sheladiya
'Modified   :
'************************************************************************************************************************************

#Region " Import "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class clsUserDetailsLogReport
    Inherits IReportData

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "clsUserDetailsLogReport"
    Private mintCompanyId As Integer = -1
    Private mstrCompanyName As String = String.Empty
    Private mintReportTypeId As Integer = -1
    Private mstrReportTypeName As String = String.Empty
    Private mdtdate1 As Date = Nothing
    Private mdtdate2 As Date = Nothing

#End Region

#Region " Properties "

    Public WriteOnly Property _mintCompanyId() As Integer
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property

    Public WriteOnly Property _mstrCompanyName() As String
        Set(ByVal value As String)
            mstrCompanyName = value
        End Set
    End Property

    Public WriteOnly Property _mintReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _mstrReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _mdtdate1() As Date
        Set(ByVal value As Date)
            mdtdate1 = value
        End Set
    End Property

    Public WriteOnly Property _mdtdate2() As Date
        Set(ByVal value As Date)
            mdtdate2 = value
        End Set
    End Property

#End Region

#Region "Private Methods"

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub SetDefaultValue()
        Try
            mintCompanyId = -1
            mstrCompanyName = String.Empty
            mintReportTypeId = -1
            mstrReportTypeName = String.Empty
            mdtdate1 = Nothing
            mdtdate2 = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_User_Details()
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try
            Dim dtTable As DataTable = Nothing
            Dim objCompany As New clsCompany_Master
            dtTable = objCompany.Get_DataBaseList("Database", False)
            If dtTable.Rows.Count > 0 Then
                Dim StrDataBaseName As String = String.Empty
                Dim dtemp() As DataRow = dtTable.Select("GrpId = '" & mintCompanyId & "' AND isclosed = 0 AND IsGrp = 0")
                If dtemp.Length > 0 Then StrDataBaseName = dtemp(0).Item("Database")
                Dim dtUserDetial As New DataTable("User")

                Dim dCol As DataColumn
                dCol = New DataColumn("column1")
                dCol.Caption = Language.getMessage(mstrModuleName, 1, "User Name")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column2")
                dCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Code")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column3")
                dCol.Caption = Language.getMessage(mstrModuleName, 3, "Employee Name")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column4")
                dCol.Caption = Language.getMessage(mstrModuleName, 4, "Job Title")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column5")
                dCol.Caption = Language.getMessage(mstrModuleName, 5, "Department")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column6")
                dCol.Caption = Language.getMessage(mstrModuleName, 6, "Email")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column7")
                dCol.Caption = Language.getMessage(mstrModuleName, 7, "Role")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column8")
                dCol.Caption = Language.getMessage(mstrModuleName, 8, "Last Login Date")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column9")
                dCol.Caption = Language.getMessage(mstrModuleName, 9, "Last Login Time")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column10")
                dCol.Caption = Language.getMessage(mstrModuleName, 10, "Account Creation Date")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column11")
                dCol.Caption = Language.getMessage(mstrModuleName, 11, "Account Creation Time")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                dCol = New DataColumn("column12")
                dCol.Caption = Language.getMessage(mstrModuleName, 12, "Active Status")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                'Hemant (04 Aug 2023) -- Start
                'Enhancement(Precision Air) : A1X-1147 : User Log Detail report enhancement to include the last password change
                dCol = New DataColumn("column13")
                dCol.Caption = Language.getMessage(mstrModuleName, 27, "Last Password Changed")
                dCol.DataType = System.Type.GetType("System.String")
                dtUserDetial.Columns.Add(dCol)

                StrQ = " SELECT " & _
                       "      parentunkid " & _
                       ",     [parent_xmlvalue].value('(cfuser_master/username)[1]', 'NVARCHAR(MAX)') AS 'username' " & _
                       ",     [parent_xmlvalue].value('(cfuser_master/password)[1]', 'NVARCHAR(MAX)') AS 'password' " & _
                       ",     auditdatetime " & _
                       " FROM hrmsConfiguration..atconfigcommon_tranlog " & _
                       " WHERE " & _
                       "    parenttable_name = 'cfuser_master' " & _
                       "    AND childtable_name = '' " & _
                       "    AND form_name <> 'frmPasswordOptions' " & _
                       " ORDER by parentunkid, auditdatetime "

                Dim dsUserPassword As New DataSet
                dsUserPassword = objDataOperation.ExecQuery(StrQ, "List")
                'Hemant (04 Aug 2023) -- End

                StrQ = "SELECT " & _
                                 " username AS column1 " & _
                                 ",EMP.employeecode AS column2 " & _
                                 ",EMP.firstname+' '+EMP.surname AS column3 " & _
                                 ",JM.job_name AS column4 " & _
                                 ",DM.name AS column5 " & _
                                 ",cfuser_master.email AS column6 " & _
                                 ",ISNULL(cfrole_master.name,'') AS column7 " & _
                                 ",ISNULL([ldate],'') AS column8 " & _
                                 ",ISNULL([ltime],'') AS column9 " & _
                                 ",CONVERT(CHAR(8),cfuser_master.creationdate,112) AS column10 " & _
                                 ",CONVERT(CHAR(8),cfuser_master.creationdate,108) AS column11 " & _
                                 ",CASE WHEN CONVERT(CHAR(8), cfuser_master.appointeddate, 112) <= CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112) " & _
                                           "AND ISNULL(CONVERT(CHAR(8), cfuser_master.termination_from_date, 112), CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112)) >= CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112) " & _
                                           "AND ISNULL(CONVERT(CHAR(8), cfuser_master.termination_to_date, 112), CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112)) >= CONVERT(CHAR(8), CAST('" & mdtdate1 & "' AS DATETIME), 112) " & _
                                           "AND ISNULL(CONVERT(CHAR(8), cfuser_master.empl_enddate, 112), CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112)) >= CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112) " & _
                                    "THEN @Active ELSE @Inactive END AS column12 "

                'Hemant (04 Aug 2023) -- Start
                'Enhancement(Precision Air) : A1X-1147 : User Log Detail report enhancement to include the last password change
                StrQ &= ",'' AS column13 "
                'Hemant (04 Aug 2023) -- End

                StrQ &= "FROM cfuser_master " & _
                                 "LEFT JOIN " & _
                                 "( " & _
                                      "SELECT " & _
                                           " userunkid AS Uid " & _
                                           ",ISNULL(CONVERT(CHAR(8),MAX(logindate),112),'') AS [ldate] " & _
                                           ",ISNULL(CONVERT(CHAR(8),MAX(logindate),108),'') AS [ltime] " & _
                                      "FROM cfuser_tracking_tran " & _
                                      "GROUP BY userunkid " & _
                                 ")AS lgdt ON lgdt.Uid = cfuser_master.userunkid " & _
                                 "LEFT JOIN cfrole_master ON cfrole_master.roleunkid = cfuser_master.roleunkid " & _
                                 "JOIN " & StrDataBaseName & "..hremployee_master AS EMP ON EMP.employeeunkid = cfuser_master.employeeunkid AND EMP.companyunkid = cfuser_master.companyunkid " & _
                                 "JOIN " & StrDataBaseName & "..hrjob_master AS JM ON JM.jobunkid = EMP.jobunkid " & _
                                 "JOIN " & StrDataBaseName & "..hrdepartment_master AS DM ON DM.departmentunkid = EMP.departmentunkid " & _
                            "WHERE cfuser_master.employeeunkid > 0 " & _
                            "ORDER BY EMP.firstname+' '+EMP.surname "

                objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Active"))
                objDataOperation.AddParameter("@Inactive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Inactive"))
                Dim dsUser As New DataSet
                dsUser = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                For Each dr As DataRow In dsUser.Tables(0).Rows
                    If dr("column10").ToString().Trim.Length > 0 Then
                        dr("column10") = eZeeDate.convertDate(dr("column10").ToString).ToShortDateString
                    End If
                    If dr("column8").ToString().Trim.Length > 0 Then
                        dr("column8") = eZeeDate.convertDate(dr("column8").ToString).ToShortDateString
                    End If
                    'Hemant (04 Aug 2023) -- Start
                    'Enhancement(Precision Air) : A1X-1147 : User Log Detail report enhancement to include the last password change
                    Dim dtLastChangeDate As Date = Nothing
                    Dim strCurrentPassword As String = String.Empty, strOldPassword As String = String.Empty
                    Dim drPassword() As DataRow = dsUserPassword.Tables(0).Select("Username = '" & dr("column1").ToString() & "'", "auditdatetime desc", DataViewRowState.CurrentRows)
                    If drPassword.Length > 0 Then
                        For i As Integer = 0 To drPassword.Length - 1
                            If i = 0 Then strOldPassword = drPassword(i).Item("password")
                            strCurrentPassword = drPassword(i).Item("password")
                            If strOldPassword <> strCurrentPassword.ToString Then
                                Exit For
                            End If
                            strOldPassword = drPassword(i).Item("password").ToString
                            dtLastChangeDate = CDate(drPassword(i).Item("auditdatetime"))
                        Next

                        dr("column13") = dtLastChangeDate.ToString("dd-MMM-yyyy HH:mm:ss")
                    End If
                    'Hemant (04 Aug 2023) -- End
                    dtUserDetial.ImportRow(dr)
                Next

                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(dtUserDetial.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 125
                Next

                Dim strExtraFilter As String = String.Empty
                strExtraFilter = Language.getMessage(mstrModuleName, 15, "As On Date : ") & " " & mdtdate1.Date & " "
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dtUserDetial, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", strExtraFilter, Nothing, "", True, Nothing, Nothing, Nothing)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_User_Ability()
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try
            Dim dtUserAbility As New DataTable("User")
            Dim dCol As DataColumn
            dCol = New DataColumn("column1")
            dCol.Caption = Language.getMessage(mstrModuleName, 16, "User")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column2")
            dCol.Caption = Language.getMessage(mstrModuleName, 17, "Module Name")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column3")
            dCol.Caption = Language.getMessage(mstrModuleName, 18, "Event Type")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column4")
            dCol.Caption = Language.getMessage(mstrModuleName, 19, "Audit Date")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column5")
            dCol.Caption = Language.getMessage(mstrModuleName, 20, "Audit Time")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column6")
            dCol.Caption = Language.getMessage(mstrModuleName, 21, "User Role")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column7")
            dCol.Caption = Language.getMessage(mstrModuleName, 22, "Changed For User")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column8")
            dCol.Caption = Language.getMessage(mstrModuleName, 23, "Privilege Group")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            dCol = New DataColumn("column9")
            dCol.Caption = Language.getMessage(mstrModuleName, 24, "Privilege")
            dCol.DataType = System.Type.GetType("System.String")
            dtUserAbility.Columns.Add(dCol)

            StrQ = "SELECT " & _
                          "ISNULL(hrmsconfiguration..cfuser_master.username, '') AS column1, " & _
                          "ISNULL(CASE " & _
                            "WHEN @LangId = 0 THEN REPLACE(Mod1.LANGUAGE, '&', '') " & _
                            "WHEN @LangId = 1 THEN REPLACE(Mod1.language1, '&', '') " & _
                            "WHEN @LangId = 2 THEN REPLACE(Mod1.language2, '&', '') " & _
                          "END, 'module_name1') + CASE " & _
                            "WHEN ISNULL(atconfigcommon_log.module_name2, '') <> '' THEN +' -> ' + CASE " & _
                                "WHEN @LangId = 0 THEN REPLACE(Mod2.LANGUAGE, '&', '') " & _
                                "WHEN @LangId = 1 THEN REPLACE(Mod2.language1, '&', '') " & _
                                "WHEN @LangId = 2 THEN REPLACE(Mod2.language2, '&', '') " & _
                              "END " & _
                            "ELSE '' " & _
                          "END + CASE " & _
                            "WHEN ISNULL(atconfigcommon_log.module_name3, '') <> '' THEN +' -> ' + CASE " & _
                                "WHEN @LangId = 0 THEN REPLACE(Mod3.LANGUAGE, '&', '') " & _
                                "WHEN @LangId = 1 THEN REPLACE(Mod3.language1, '&', '') " & _
                                "WHEN @LangId = 2 THEN REPLACE(Mod3.language2, '&', '') " & _
                              "END " & _
                            "ELSE '' " & _
                          "END + CASE " & _
                            "WHEN ISNULL(atconfigcommon_log.module_name4, '') <> '' THEN +' -> ' + CASE " & _
                                "WHEN @LangId = 0 THEN REPLACE(Mod4.LANGUAGE, '&', '') " & _
                                "WHEN @LangId = 1 THEN REPLACE(Mod4.language1, '&', '') " & _
                                "WHEN @LangId = 2 THEN REPLACE(Mod4.language2, '&', '') " & _
                              "END " & _
                            "ELSE '' " & _
                          "END + CASE " & _
                            "WHEN ISNULL(atconfigcommon_log.module_name5, '') <> '' THEN +' -> ' + CASE " & _
                                "WHEN @LangId = 0 THEN REPLACE(Mod5.LANGUAGE, '&', '') " & _
                                "WHEN @LangId = 1 THEN REPLACE(Mod5.language1, '&', '') " & _
                                "WHEN @LangId = 2 THEN REPLACE(Mod5.language2, '&', '') " & _
                              "END " & _
                            "ELSE '' " & _
                          "END AS column2, " & _
                          "CASE " & _
                            "WHEN atconfigcommon_log.audittype = 1 THEN 'Add' " & _
                            "WHEN atconfigcommon_log.audittype = 2 THEN 'Edit' " & _
                            "WHEN atconfigcommon_log.audittype = 3 THEN 'Delete' " & _
                          "END AS column3, " & _
                          "CONVERT(char(8), atconfigcommon_log.auditdatetime, 112) AS column4, " & _
                          "CONVERT(char(8), atconfigcommon_log.auditdatetime, 108) AS column5, " & _
                          "ISNULL(hrmsConfiguration..cfrole_master.name, '') AS column6 , " & _
                          "ISNULL(CUsr.username, '') AS column7 , " & _
                          "ISNULL(hrmsConfiguration..cfuserprivilegegroup_master.name, '') AS column8, " & _
                          "ISNULL(hrmsConfiguration..cfuserprivilege_master.privilege_name, '') AS column9 " & _
                        "FROM atconfigcommon_log " & _
                             "LEFT JOIN cflanguage AS Mod1 ON Mod1.controlname = atconfigcommon_log.module_name1 " & _
                             "LEFT JOIN cflanguage AS Mod2 ON Mod2.controlname = atconfigcommon_log.module_name2 " & _
                             "LEFT JOIN cflanguage AS Mod3 ON Mod3.controlname = atconfigcommon_log.module_name3 " & _
                             "LEFT JOIN cflanguage AS Mod4 ON Mod4.controlname = atconfigcommon_log.module_name4 " & _
                             "LEFT JOIN cflanguage AS Mod5 ON Mod5.controlname = atconfigcommon_log.module_name5 " & _
                             "JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = atconfigcommon_log.audituserunkid " & _
                             "JOIN hrmsConfiguration..cfuserprivilege_master ON hrmsConfiguration..cfuserprivilege_master.privilegeunkid = [value].value('(cfuser_privilege/privilegeunkid)[1]', 'INT') " & _
                             "JOIN hrmsConfiguration..cfuserprivilegegroup_master ON hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid = hrmsConfiguration..cfuserprivilege_master.privilegegroupunkid " & _
                             "JOIN hrmsConfiguration..cfuser_master AS CUsr ON CUsr.userunkid = [value].value('(cfuser_privilege/userunkid)[1]', 'INT') " & _
                             "JOIN hrmsConfiguration..cfrole_master ON hrmsConfiguration..cfrole_master.roleunkid = CUsr.roleunkid " & _
                        "WHERE form_name = 'frmAbilityLevel_Privilege' " & _
                        "AND CONVERT(char(8), atconfigcommon_log.auditdatetime, 112) BETWEEN CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112) AND CONVERT(CHAR(8), CAST('" & mdtdate2.Date & "' AS DATETIME), 112) "

            objDataOperation.AddParameter("@LangId", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Languageunkid)
            Dim dsUser As New DataSet
            dsUser = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr As DataRow In dsUser.Tables(0).Rows
                If dr("column4").ToString().Trim.Length > 0 Then
                    dr("column4") = eZeeDate.convertDate(dr("column4").ToString).ToShortDateString
                End If
                dtUserAbility.ImportRow(dr)
            Next

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtUserAbility.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next

            Dim strExtraFilter As String = String.Empty
            strExtraFilter = Language.getMessage(mstrModuleName, 25, "From Date : ") & " " & mdtdate1.Date & " " & Language.getMessage(mstrModuleName, 26, "To") & " " & mdtdate2.Date & " "
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dtUserAbility, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", strExtraFilter, Nothing, "", True, Nothing, Nothing, Nothing)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_User_Ability; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [21-SEP-2018] -- START
    'EXPORT USER PRIVILEGE DUE TO EXCEL APPLICATION NOT ABLE TO OPEN THE FILE
    ''' <summary>
    ''' Used To Export Data For Privilege/Report Based On Role And User
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <param name="strFileName"></param>
    ''' <param name="intExportType">1=Privilege Ability Level ,2=Report Ability Level, 3=Specific User Privilege</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExportPrivilegeList(ByVal dt As DataTable, ByVal strFileName As String, ByVal intExportType As Integer, _
                                        Optional ByVal strExtraHeader As String = "", _
                                        Optional ByVal blnShowReportHeader As Boolean = False, _
                                        Optional ByVal strUserName As String = "", _
                                        Optional ByVal strRoleName As String = "" _
                                        ) As Boolean
        Try

            Dim strarrGroupColumns As String() = Nothing
            Dim strFilterString As String = String.Empty

            Select Case intExportType
                Case 1  'PRIVILEGE ABILITY LEVEL
                    dt.Columns.Remove("Privilege")
                    dt.Columns("objPrivilegeName").ColumnName = "PrivilegeName"
                    dt.Columns("objPrivilegeGrp").ColumnName = "PrivilegeGrp"
                    dt.Columns("PrivilegeGrp").Caption = Language.getMessage("clsUserAddEdit", 999, "Privilege Group")
                    Dim strGrpCols As String() = {"PrivilegeGrp"}
                    strarrGroupColumns = strGrpCols
                    dt.Columns("PrivilegeGrp").SetOrdinal(0)
                    dt.Columns("PrivilegeName").SetOrdinal(1)

                    'S.SANDEEP [29-SEP-2018] -- START
                    dt = New DataView(dt, "", "PrivilegeGrp,PrivilegeName", DataViewRowState.CurrentRows).ToTable()
                    'S.SANDEEP [29-SEP-2018] -- END

                Case 2  'REPORT ABILITY LEVEL
                    Dim dview As DataView = dt.DefaultView
                    dview.Sort = "objreportcategoryunkid,objreportunkid"
                    dt = dview.ToTable()
                    dt.Columns.Remove("Reports")
                    dt.Columns("objReportGrp").ColumnName = "ReportGrp"
                    dt.Columns("objReportName").ColumnName = "ReportName"
                    dt.Columns("ReportGrp").Caption = Language.getMessage("clsReport_role_mapping_tran", 100, "Report Group")
                    Dim strGrpCols As String() = {"ReportGrp"}
                    strarrGroupColumns = strGrpCols
                    dt.Columns("ReportGrp").SetOrdinal(0)
                    dt.Columns("ReportName").SetOrdinal(1)

                    'S.SANDEEP [29-SEP-2018] -- START
                    dt = New DataView(dt, "", "ReportGrp,ReportName", DataViewRowState.CurrentRows).ToTable()
                    'S.SANDEEP [29-SEP-2018] -- END

                Case 3  'SPECIFIC USER PRIVILEGE

                    dt.Columns.Remove("privilegeunkid")
                    dt.Columns.Remove("privilegegroupunkid")
                    dt.Columns.Remove("assign")

                    Dim strGrpCols As String() = {"group_name"}
                    strarrGroupColumns = strGrpCols
                    dt.Columns("group_name").SetOrdinal(0)
                    dt.Columns("privilege_name").SetOrdinal(1)

                    dt.Columns("group_name").Caption = Language.getMessage("clsUserAddEdit", 999, "Privilege Group")
                    dt.Columns("privilege_name").Caption = "PRIVILEGE(S)"
                    dt.Columns("iassign").Caption = "ASSIGNED"

                    strFilterString = Language.getMessage("frmUserCreationList", 4, "Operational Privilege for User : ") & " " & strUserName & ", " & _
                                      Language.getMessage("frmUserCreationList", 5, "User Role : ") & " " & strRoleName

                    'S.SANDEEP [29-SEP-2018] -- START
                    dt = New DataView(dt, "", "group_name,privilege_name", DataViewRowState.CurrentRows).ToTable()
                    'S.SANDEEP [29-SEP-2018] -- END

            End Select

            Dim iCols As String() = dt.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName.StartsWith("obj") = True).Select(Function(x) x.ColumnName).ToArray()
            If iCols IsNot Nothing AndAlso iCols.Length > 0 Then
                For index As Integer = 0 To iCols.Length - 1
                    dt.Columns.Remove(iCols(index))
                Next
            End If


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dt.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case Is < 1
                        intArrayColumnWidth(i) = 150
                    Case Else
                        intArrayColumnWidth(i) = 100
                End Select
            Next
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, "", True, dt, intArrayColumnWidth, blnShowReportHeader, True, False, strarrGroupColumns, strFileName, strExtraHeader, strFilterString, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportPrivilegeList; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [21-SEP-2018] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "User Name")
            Language.setMessage(mstrModuleName, 2, "Employee Code")
            Language.setMessage(mstrModuleName, 3, "Employee Name")
            Language.setMessage(mstrModuleName, 4, "Job Title")
            Language.setMessage(mstrModuleName, 5, "Department")
            Language.setMessage(mstrModuleName, 6, "Email")
            Language.setMessage(mstrModuleName, 7, "Role")
            Language.setMessage(mstrModuleName, 8, "Last Login Date")
            Language.setMessage(mstrModuleName, 9, "Last Login Time")
            Language.setMessage(mstrModuleName, 10, "Account Creation Date")
            Language.setMessage(mstrModuleName, 11, "Account Creation Time")
            Language.setMessage(mstrModuleName, 12, "Active Status")
            Language.setMessage(mstrModuleName, 13, "Active")
            Language.setMessage(mstrModuleName, 14, "Inactive")
            Language.setMessage(mstrModuleName, 15, "As On Date :")
            Language.setMessage(mstrModuleName, 16, "User")
            Language.setMessage(mstrModuleName, 17, "Module Name")
            Language.setMessage(mstrModuleName, 18, "Event Type")
            Language.setMessage(mstrModuleName, 19, "Audit Date")
            Language.setMessage(mstrModuleName, 20, "Audit Time")
            Language.setMessage(mstrModuleName, 21, "User Role")
            Language.setMessage(mstrModuleName, 22, "Changed For User")
            Language.setMessage(mstrModuleName, 23, "Privilege Group")
            Language.setMessage(mstrModuleName, 24, "Privilege")
            Language.setMessage(mstrModuleName, 25, "From Date :")
            Language.setMessage(mstrModuleName, 26, "To")
            Language.setMessage(mstrModuleName, 27, "Last Password Changed")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
End Class
