Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmJustification_Report

#Region "Private Variables"

    Private mstrModuleName As String = "frmJustification_Report"
    Dim objJustification As clsJustification_Report
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objJustification = New clsJustification_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objJustification.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Terminated"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Rehire"))
            cboReportType.SelectedIndex = 0


            Dim objEmp As New clsEmployee_Master

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
            cboReportType.SelectedIndex = 0
            dtpFromdate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            mstrAdvanceFilter = String.Empty
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objJustification.SetDefaultValue()

            objJustification._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objJustification._ReportType = cboReportType.Text
            objJustification._FromDate = dtpFromdate.Value.Date
            objJustification._ToDate = dtpToDate.Value.Date
            objJustification._EmployeeID = CInt(cboEmployee.SelectedValue)
            objJustification._EmployeeName = cboEmployee.Text

            objJustification._Advance_Filter = mstrAdvanceFilter
            objJustification._ViewByIds = mstrViewByIds
            objJustification._ViewIndex = mintViewIndex
            objJustification._ViewByName = mstrViewByName
            objJustification._Analysis_Fields = mstrAnalysis_Fields
            objJustification._Analysis_Join = mstrAnalysis_Join
            objJustification._Analysis_OrderBy = mstrAnalysis_OrderBy
            objJustification._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objJustification._Report_GroupName = mstrReport_GroupName

            objJustification.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmJustification_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objJustification = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJustification_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmJustification_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objJustification._ReportName
            eZeeHeader.Message = objJustification._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJustification_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJustification_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJustification_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJustification_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJustification_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJustification_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsJustification_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If CInt(cboReportType.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Report Type is compulsory information.Please Select Report Type."))
                cboReportType.Select()
                Exit Sub
            End If

            If SetFilter() = False Then Exit Sub


            If CInt(cboReportType.SelectedIndex) = 1 Then   'Terminated
                objJustification.Generate_TerminatedEmployeeReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate.ToString, ConfigParameter._Object._UserAccessModeSetting, True)

            ElseIf CInt(cboReportType.SelectedIndex) = 2 Then  'Rehire
                objJustification.Generate_RehiredEmployeeReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate.ToString, ConfigParameter._Object._UserAccessModeSetting, True)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
			Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Terminated")
			Language.setMessage(mstrModuleName, 3, "Rehire")
			Language.setMessage(mstrModuleName, 4, "Report Type is compulsory information.Please Select Report Type.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
