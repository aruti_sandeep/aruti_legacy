Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

Public Class clsEmpSameBankAccReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpSameBankAccReport"
    Private mstrReportId As String = enArutiReport.Employee_SameBankAccount
    Dim objDataOperation As clsDataOperation


#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintBankId As Integer = -1
    Private mstrBank As String = String.Empty
    Private mintBankBranchId As Integer = -1
    Private mstrBankBranch As String = String.Empty
    Private mintAccountTypeID As Integer = -1
    Private mstrAccountType As String = String.Empty
    Private mblnIsActive As Boolean = True
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mstrOrderByQuery As String = ""
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _BankId() As Integer
        Set(ByVal value As Integer)
            mintBankId = value
        End Set
    End Property

    Public WriteOnly Property _BankName() As String
        Set(ByVal value As String)
            mstrBank = value
        End Set
    End Property

    Public WriteOnly Property _BankBranchId() As Integer
        Set(ByVal value As Integer)
            mintBankBranchId = value
        End Set
    End Property

    Public WriteOnly Property _BankBrachName() As String
        Set(ByVal value As String)
            mstrBankBranch = value
        End Set
    End Property

    Public WriteOnly Property _AccountTypeId() As Integer
        Set(ByVal value As Integer)
            mintAccountTypeID = value
        End Set
    End Property

    Public WriteOnly Property _AccountType() As String
        Set(ByVal value As String)
            mstrAccountType = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintBankId = -1
            mstrBank = ""
            mintBankBranchId = -1
            mstrBankBranch = ""
            mintAccountTypeID = -1
            mstrAccountType = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintBankId > 0 Then
                Me._FilterQuery &= " AND hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid = @bankId "
                objDataOperation.AddParameter("@bankId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Bank :") & " " & mstrBank & " "
            End If

            If mintBankBranchId > 0 Then
                Me._FilterQuery &= " AND hrmsconfiguration..cfbankbranch_master.branchunkid = @bankbranchId "
                objDataOperation.AddParameter("@bankbranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankBranchId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Bank Branch :") & " " & mstrBankBranch & " "
            End If

            If mintAccountTypeID > 0 Then
                Me._FilterQuery &= " AND hrmsconfiguration..cfbankacctype_master.accounttypeunkid  = @accountTypeId "
                objDataOperation.AddParameter("@accountTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountTypeID)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Account Type :") & " " & mstrAccountType & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND premployee_bank_tran.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    If Me.OrderByQuery.Trim = "" Then
                        Me._FilterQuery &= "ORDER BY GNAME"
                    Else
                        Me._FilterQuery &= "ORDER BY GNAME, " & Me.OrderByQuery
                    End If

                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, " Order By : ") & " " & Me.OrderByDisplay
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objUser As New clsUserAddEdit

        Try
            objUser._Userunkid = xUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objUser._Username)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("Bank", Language.getMessage(mstrModuleName, 3, "Bank")))
            iColumn_DetailReport.Add(New IColumn("branch", Language.getMessage(mstrModuleName, 4, "Bank Branch")))
            iColumn_DetailReport.Add(New IColumn("accountno", Language.getMessage(mstrModuleName, 5, "Account No")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                          , ByVal xUserUnkid As Integer _
                                          , ByVal xYearUnkid As Integer _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As Boolean _
                                          , ByVal strUserName As String _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            StrQ &= " SELECT  premployee_bank_tran.employeeunkid  " & _
                         " ,ISNULL(hremployee_master.employeecode,'') employeecode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') employeename " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS employeename "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS employeename "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END


            StrQ &= ",premployee_bank_tran.accountno  " & _
                         ",ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname,'') AS Bank " & _
                         ",ISNULL(hrmsconfiguration..cfbankbranch_master.branchname,'') AS branch " & _
                         ",ISNULL(hrmsconfiguration..cfbankacctype_master.accounttype_name,'') AS AccType " & _
                         ",ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '') + ' - ' + premployee_bank_tran.accountno BAccno " & _
                         ",dept.name as department " & _
                         ",jb.job_name as job "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'StrQ &= " FROM  premployee_bank_tran " & _
            '               " JOIN ( SELECT   REPLACE(accountno,' ','') accountno ,COUNT(accountno) AS cnt,employeeunkid  FROM premployee_bank_tran " & _
            '              "  GROUP BY REPLACE(accountno,' ',''),employeeunkid  HAVING(COUNT(accountno) > 1) ) AS B ON B.accountno =  REPLACE(premployee_bank_tran.accountno,' ','')  AND B.employeeunkid = premployee_bank_tran.employeeunkid" & _
            '              " JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '              " JOIN hrmsconfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsconfiguration..cfbankbranch_master.branchunkid  AND hrmsconfiguration..cfbankbranch_master.isactive = 1 " & _
            '              " JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.grouptype_id " & _
            '              " AND hrmsconfiguration..cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & " AND hrmsconfiguration..cfpayrollgroup_master.isactive = 1 " & _
            '              " JOIN hrmsconfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsconfiguration..cfbankacctype_master.accounttypeunkid  "

            StrQ &= " FROM  premployee_bank_tran " & _
                    " JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " JOIN hrmsconfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsconfiguration..cfbankbranch_master.branchunkid  AND hrmsconfiguration..cfbankbranch_master.isactive = 1 " & _
                    " JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                    " AND hrmsconfiguration..cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & " AND hrmsconfiguration..cfpayrollgroup_master.isactive = 1 " & _
                    " JOIN hrmsconfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsconfiguration..cfbankacctype_master.accounttypeunkid  "

            StrQ &= "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         stationunkid " & _
                   "        ,deptgroupunkid " & _
                   "        ,departmentunkid " & _
                   "        ,sectiongroupunkid " & _
                   "        ,sectionunkid " & _
                   "        ,unitgroupunkid " & _
                   "        ,unitunkid " & _
                   "        ,teamunkid " & _
                   "        ,classgroupunkid " & _
                   "        ,classunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                   "    FROM hremployee_transfer_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                   ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         jobgroupunkid " & _
                   "        ,jobunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                   "    FROM hremployee_categorization_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                   ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 "


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            '    End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If

            'S.SANDEEP |28-OCT-2021| -- END
            
            'Sohail (21 Aug 2015) -- End

            StrQ &= " JOIN hrdepartment_master dept ON dept.departmentunkid = T.departmentunkid  " & _
                    " JOIN hrjob_master jb ON jb.jobunkid = J.jobunkid  "


            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE premployee_bank_tran.isvoid = 0 " & _
                    " AND REPLACE(accountno ,' ' ,'') IN(SELECT   REPLACE(accountno ,' ' ,'') accountno FROM premployee_bank_tran GROUP BY accountno HAVING (COUNT(accountno) >= 2))"

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("employeename")
                rpt_Row.Item("Column3") = dtRow.Item("employeecode")
                rpt_Row.Item("Column4") = dtRow.Item("accountno")
                rpt_Row.Item("Column5") = dtRow.Item("BAccno")
                rpt_Row.Item("Column6") = dtRow.Item("Bank")
                rpt_Row.Item("Column7") = dtRow.Item("branch")
                rpt_Row.Item("Column8") = dtRow.Item("AccType")
                rpt_Row.Item("Column9") = dtRow.Item("department")
                rpt_Row.Item("Column10") = dtRow.Item("job")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next

            objRpt = New ArutiReport.Designer.rptEmpSameBankAccount

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If


            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 10, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 11, "Employee "))
            Call ReportFunction.TextChange(objRpt, "txtAccountNo", Language.getMessage(mstrModuleName, 12, "Account No : "))
            Call ReportFunction.TextChange(objRpt, "txtBank", Language.getMessage(mstrModuleName, 13, "Bank : "))
            Call ReportFunction.TextChange(objRpt, "txtBankBranch", Language.getMessage(mstrModuleName, 14, "Bank Branch : "))
            Call ReportFunction.TextChange(objRpt, "txtAccountType", Language.getMessage(mstrModuleName, 15, "Account Type : "))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 16, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 17, "Job"))


            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 26, "Sub Total :"))

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 23, mstrReport_GroupName & " " & "Total :"))
            Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", mstrReport_GroupName & " " & Language.getMessage(mstrModuleName, 27, "Total :"))
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 24, "Grand Total"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Bank")
            Language.setMessage(mstrModuleName, 4, "Bank Branch")
            Language.setMessage(mstrModuleName, 5, "Account No")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By :")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, "Employee Code")
            Language.setMessage(mstrModuleName, 11, "Employee")
            Language.setMessage(mstrModuleName, 12, "Account No :")
            Language.setMessage(mstrModuleName, 13, "Bank :")
            Language.setMessage(mstrModuleName, 14, "Bank Branch :")
            Language.setMessage(mstrModuleName, 15, "Account Type :")
            Language.setMessage(mstrModuleName, 16, "Department")
            Language.setMessage(mstrModuleName, 17, "Job")
            Language.setMessage(mstrModuleName, 18, "Printed By :")
            Language.setMessage(mstrModuleName, 19, "Printed Date :")
            Language.setMessage(mstrModuleName, 20, "Bank :")
            Language.setMessage(mstrModuleName, 21, "Bank Branch :")
            Language.setMessage(mstrModuleName, 22, "Account Type :")
            Language.setMessage(mstrModuleName, 23, "Employee :")
            Language.setMessage(mstrModuleName, 24, "Grand Total")
            Language.setMessage(mstrModuleName, 25, " Order By :")
            Language.setMessage(mstrModuleName, 26, "Sub Total :")
            Language.setMessage(mstrModuleName, 27, "Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
