﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeBeneficiaryReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.txtLastName = New eZee.TextBox.AlphanumericTextBox
        Me.lblLastName = New System.Windows.Forms.Label
        Me.txtFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.lblBeneficiaryFName = New System.Windows.Forms.Label
        Me.lblBenefitIn = New System.Windows.Forms.Label
        Me.lblBenefitType = New System.Windows.Forms.Label
        Me.txtMobile = New eZee.TextBox.AlphanumericTextBox
        Me.lblMobile = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.cboBenefitIn = New System.Windows.Forms.ComboBox
        Me.cboBenefitType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 460)
        Me.NavPanel.Size = New System.Drawing.Size(712, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.txtLastName)
        Me.gbFilterCriteria.Controls.Add(Me.lblLastName)
        Me.gbFilterCriteria.Controls.Add(Me.txtFirstName)
        Me.gbFilterCriteria.Controls.Add(Me.lblBeneficiaryFName)
        Me.gbFilterCriteria.Controls.Add(Me.lblBenefitIn)
        Me.gbFilterCriteria.Controls.Add(Me.lblBenefitType)
        Me.gbFilterCriteria.Controls.Add(Me.txtMobile)
        Me.gbFilterCriteria.Controls.Add(Me.lblMobile)
        Me.gbFilterCriteria.Controls.Add(Me.txtCode)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployeeCode)
        Me.gbFilterCriteria.Controls.Add(Me.cboBenefitIn)
        Me.gbFilterCriteria.Controls.Add(Me.cboBenefitType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(433, 163)
        Me.gbFilterCriteria.TabIndex = 15
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(90, 141)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(188, 16)
        Me.chkInactiveemp.TabIndex = 80
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'txtLastName
        '
        Me.txtLastName.Flags = 0
        Me.txtLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLastName.Location = New System.Drawing.Point(280, 114)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(118, 21)
        Me.txtLastName.TabIndex = 78
        '
        'lblLastName
        '
        Me.lblLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(211, 117)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(63, 15)
        Me.lblLastName.TabIndex = 77
        Me.lblLastName.Text = "Last Name"
        Me.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFirstName
        '
        Me.txtFirstName.Flags = 0
        Me.txtFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFirstName.Location = New System.Drawing.Point(90, 114)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(118, 21)
        Me.txtFirstName.TabIndex = 76
        '
        'lblBeneficiaryFName
        '
        Me.lblBeneficiaryFName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBeneficiaryFName.Location = New System.Drawing.Point(8, 117)
        Me.lblBeneficiaryFName.Name = "lblBeneficiaryFName"
        Me.lblBeneficiaryFName.Size = New System.Drawing.Size(78, 15)
        Me.lblBeneficiaryFName.TabIndex = 75
        Me.lblBeneficiaryFName.Text = "First Name"
        Me.lblBeneficiaryFName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBenefitIn
        '
        Me.lblBenefitIn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitIn.Location = New System.Drawing.Point(211, 90)
        Me.lblBenefitIn.Name = "lblBenefitIn"
        Me.lblBenefitIn.Size = New System.Drawing.Size(63, 15)
        Me.lblBenefitIn.TabIndex = 74
        Me.lblBenefitIn.Text = "Benefit In"
        Me.lblBenefitIn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBenefitType
        '
        Me.lblBenefitType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitType.Location = New System.Drawing.Point(8, 90)
        Me.lblBenefitType.Name = "lblBenefitType"
        Me.lblBenefitType.Size = New System.Drawing.Size(78, 15)
        Me.lblBenefitType.TabIndex = 73
        Me.lblBenefitType.Text = "Benefit Type"
        Me.lblBenefitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMobile
        '
        Me.txtMobile.Flags = 0
        Me.txtMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMobile.Location = New System.Drawing.Point(280, 60)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(118, 21)
        Me.txtMobile.TabIndex = 72
        '
        'lblMobile
        '
        Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMobile.Location = New System.Drawing.Point(211, 63)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(63, 15)
        Me.lblMobile.TabIndex = 71
        Me.lblMobile.Text = "Mobile"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(90, 60)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(118, 21)
        Me.txtCode.TabIndex = 70
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(78, 15)
        Me.lblEmployeeCode.TabIndex = 69
        Me.lblEmployeeCode.Text = "Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBenefitIn
        '
        Me.cboBenefitIn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitIn.DropDownWidth = 210
        Me.cboBenefitIn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitIn.FormattingEnabled = True
        Me.cboBenefitIn.Location = New System.Drawing.Point(280, 87)
        Me.cboBenefitIn.Name = "cboBenefitIn"
        Me.cboBenefitIn.Size = New System.Drawing.Size(118, 21)
        Me.cboBenefitIn.TabIndex = 68
        '
        'cboBenefitType
        '
        Me.cboBenefitType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitType.DropDownWidth = 210
        Me.cboBenefitType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitType.FormattingEnabled = True
        Me.cboBenefitType.Location = New System.Drawing.Point(90, 87)
        Me.cboBenefitType.Name = "cboBenefitType"
        Me.cboBenefitType.Size = New System.Drawing.Size(118, 21)
        Me.cboBenefitType.TabIndex = 67
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(404, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(90, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(308, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(78, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 235)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(434, 63)
        Me.gbSortBy.TabIndex = 16
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(404, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(78, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(90, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(308, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmEmployeeBeneficiaryReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(712, 515)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeBeneficiaryReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Beneficiary Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents cboBenefitIn As System.Windows.Forms.ComboBox
    Friend WithEvents cboBenefitType As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitIn As System.Windows.Forms.Label
    Friend WithEvents lblBenefitType As System.Windows.Forms.Label
    Friend WithEvents txtMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents txtLastName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBeneficiaryFName As System.Windows.Forms.Label
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class
