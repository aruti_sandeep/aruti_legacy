#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplinePendingCasesDetail

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDiscipline_Casebook"
    Private objDisciplineCaseBook As clsDisciplinePendingCasesDetail
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
#End Region

#Region " Contructor "

    Public Sub New()
        objDisciplineCaseBook = New clsDisciplinePendingCasesDetail(User._Object._Languageunkid,Company._Object._Companyunkid)
        objDisciplineCaseBook.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Forms "

    Private Sub frmDiscipline_Casebook_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objDisciplineCaseBook = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDiscipline_Casebook_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_Casebook_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            eZeeHeader.Title = objDisciplineCaseBook._ReportName
            eZeeHeader.Message = objDisciplineCaseBook._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_Casebook_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_Casebook_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDiscipline_Casebook_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_Casebook_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDiscipline_Casebook_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS



            'If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, False)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Employee")
            End With

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objDStatus As New clsDiscipline_Status
            dsList = objDStatus.getComboList("List", True, clsDiscipline_Status.enDisciplineStatusType.ALL)
            With cboDStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 16 JAN 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            objDisciplineCaseBook.setDefaultOrderBy(0)
            txtOrderBy.Text = objDisciplineCaseBook.OrderByDisplay

            cboEmployee.SelectedValue = 0
            dtpFromChargeDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpFromChargeDate.Checked = False
            dtpToDateOfCharge.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDateOfCharge.Checked = False
            dtpFromInterdiction.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpFromInterdiction.Checked = False
            dtpToInterdiction.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToInterdiction.Checked = False

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboDStatus.SelectedValue = 0
            'S.SANDEEP [ 16 JAN 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objDisciplineCaseBook.SetDefaultValue()

            objDisciplineCaseBook._EmployeeId = CInt(cboEmployee.SelectedValue)
            objDisciplineCaseBook._EmployeeName = cboEmployee.Text

            If dtpFromChargeDate.Checked Then
                objDisciplineCaseBook._FromChargeDate = dtpFromChargeDate.Value.Date
            End If

            If dtpToDateOfCharge.Checked Then
                objDisciplineCaseBook._ToChargeDate = dtpToDateOfCharge.Value.Date
            End If

            If dtpFromInterdiction.Checked Then
                objDisciplineCaseBook._FromInterdictionDate = dtpFromInterdiction.Value.Date
            End If

            If dtpToInterdiction.Checked Then
                objDisciplineCaseBook._ToInterdictionDate = dtpToInterdiction.Value.Date
            End If

            objDisciplineCaseBook._ViewByIds = mstrStringIds
            objDisciplineCaseBook._ViewIndex = mintViewIdx
            objDisciplineCaseBook._ViewByName = mstrStringName
            objDisciplineCaseBook._Analysis_Fields = mstrAnalysis_Fields
            objDisciplineCaseBook._Analysis_Join = mstrAnalysis_Join
            objDisciplineCaseBook._Analysis_OrderBy = mstrAnalysis_OrderBy
            objDisciplineCaseBook._Report_GroupName = mstrReport_GroupName

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDisciplineCaseBook._DStatusId = cboDStatus.SelectedValue
            objDisciplineCaseBook._DStatusName = cboDStatus.Text
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Validation() As Boolean
        Try
            If dtpFromChargeDate.Checked And dtpToDateOfCharge.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select To Charge Date. To Charge Date is compulsory information."), enMsgBoxStyle.Information)
                dtpToDateOfCharge.Focus()
                Return False

            ElseIf dtpFromChargeDate.Checked = False And dtpToDateOfCharge.Checked Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select From Charge Date. From Charge Date is compulsory information."), enMsgBoxStyle.Information)
                dtpFromChargeDate.Focus()
                Return False

            ElseIf dtpFromInterdiction.Checked And dtpToInterdiction.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select To Interdiction Date. To Interdiction Date is compulsory information."), enMsgBoxStyle.Information)
                dtpToInterdiction.Focus()
                Return False

            ElseIf dtpFromInterdiction.Checked = False And dtpToInterdiction.Checked Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select From Interdiction Date. From Interdiction Date is compulsory information."), enMsgBoxStyle.Information)
                dtpFromInterdiction.Focus()
                Return False

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

#Region "Button's Event"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objDisciplineCaseBook.generateReport(0, enPrintAction.None, enExportAction.None)
                objDisciplineCaseBook.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        ConfigParameter._Object._UserAccessModeSetting, True, _
                                                        ConfigParameter._Object._ExportReportPath, _
                                                        ConfigParameter._Object._OpenAfterExport, _
                                                        0, enPrintAction.None, enExportAction.None)
                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.DataSource = .DataSource
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDisciplinePendingCasesDetail.SetMessages()
            objfrm._Other_ModuleNames = "clsDisciplinePendingCasesDetail"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Controls"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objDisciplineCaseBook.setOrderBy(0)
            txtOrderBy.Text = objDisciplineCaseBook.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblFromDateOfInter.Text = Language._Object.getCaption(Me.lblFromDateOfInter.Name, Me.lblFromDateOfInter.Text)
			Me.lblToDateOfInter.Text = Language._Object.getCaption(Me.lblToDateOfInter.Name, Me.lblToDateOfInter.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblToDateOfCharge.Text = Language._Object.getCaption(Me.lblToDateOfCharge.Name, Me.lblToDateOfCharge.Text)
			Me.lblFromDateOfCharge.Text = Language._Object.getCaption(Me.lblFromDateOfCharge.Name, Me.lblFromDateOfCharge.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblDisciplineStatus.Text = Language._Object.getCaption(Me.lblDisciplineStatus.Name, Me.lblDisciplineStatus.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select To Charge Date. To Charge Date is compulsory information.")
			Language.setMessage(mstrModuleName, 2, "Please select From Charge Date. From Charge Date is compulsory information.")
			Language.setMessage(mstrModuleName, 3, "Please select To Interdiction Date. To Interdiction Date is compulsory information.")
			Language.setMessage(mstrModuleName, 4, "Please select From Interdiction Date. From Interdiction Date is compulsory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
