'************************************************************************************************************************************
'Class Name : frmManningLevel_Report.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmManningLevel_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmManningLevel_Report"
    Private objManning As clsManningLevel_Report

#End Region

#Region " Contructor "

    Public Sub New()
        objManning = New clsManningLevel_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objManning.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objJob As New clsJobs
        Dim objJobGrp As New clsJobGroup
        Try
            dsCombos = objJob.getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = objJobGrp.getComboList("List", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboJob.SelectedValue = 0
            cboJobGroup.SelectedValue = 0
            txtAvailableFrom.Text = ""
            txtAvailableTo.Text = ""
            txtPlannedFrom.Text = ""
            txtPlannedTo.Text = ""
            txtVarianceFrom.Text = ""
            txtVarianceTo.Text = ""
            objManning.setDefaultOrderBy(0)
            txtOrderBy.Text = objManning.OrderByDisplay

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkInactiveemp.Checked = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objManning.SetDefaultValue()

            If txtAvailableFrom.Text.Trim.Length > 0 Then
                objManning._Available_From = txtAvailableFrom.Text
            End If

            If txtAvailableTo.Text.Trim.Length > 0 Then
                objManning._Available_To = txtAvailableTo.Text
            End If

            If txtPlannedFrom.Text.Trim.Length > 0 Then
                objManning._Planned_From = txtPlannedFrom.Text
            End If

            If txtPlannedTo.Text.Trim.Length > 0 Then
                objManning._Planned_To = txtPlannedTo.Text
            End If

            If txtVarianceFrom.Text.Trim.Length > 0 Then
                objManning._Variance_From = txtVarianceFrom.Text
            End If

            If txtVarianceTo.Text.Trim.Length > 0 Then
                objManning._Variance_To = txtVarianceTo.Text
            End If

            objManning._JobId = cboJob.SelectedValue
            objManning._Job_Name = cboJob.Text

            objManning._JobGroupId = cboJobGroup.SelectedValue
            objManning._JobGroupName = cboJobGroup.Text

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objManning._IncludeInactiveEmp = chkInactiveemp.Checked
            'S.SANDEEP [ 12 MAY 2012 ] -- END


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmManningLevel_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objManning = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmManningLevel_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmManningLevel_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objManning._ReportName
            Me._Message = objManning._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmManningLevel_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objManning.generateReport(0, e.Type, enExportAction.None)
            objManning.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objManning.generateReport(0, enPrintAction.None, e.Type)
            objManning.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objManning.setOrderBy(0)
            txtOrderBy.Text = objManning.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchJobGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchJobGroup.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboJobGroup.ValueMember
                .DisplayMember = cboJobGroup.DisplayMember
                .DataSource = cboJobGroup.DataSource
            End With
            If frm.DisplayDialog Then
                cboJobGroup.SelectedValue = frm.SelectedValue
                cboJobGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJobGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboJob.ValueMember
                .DisplayMember = cboJob.DisplayMember
                .DataSource = cboJob.DataSource
            End With
            If frm.DisplayDialog Then
                cboJob.SelectedValue = frm.SelectedValue
                cboJob.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsManningLevel_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsManningLevel_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblVariance.Text = Language._Object.getCaption(Me.lblVariance.Name, Me.lblVariance.Text)
			Me.lblAvailableCount.Text = Language._Object.getCaption(Me.lblAvailableCount.Name, Me.lblAvailableCount.Text)
			Me.lblPlannedCount.Text = Language._Object.getCaption(Me.lblPlannedCount.Name, Me.lblPlannedCount.Text)
			Me.lblVarianceTo.Text = Language._Object.getCaption(Me.lblVarianceTo.Name, Me.lblVarianceTo.Text)
			Me.lblAvailableTo.Text = Language._Object.getCaption(Me.lblAvailableTo.Name, Me.lblAvailableTo.Text)
			Me.lblPlannedTo.Text = Language._Object.getCaption(Me.lblPlannedTo.Name, Me.lblPlannedTo.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
