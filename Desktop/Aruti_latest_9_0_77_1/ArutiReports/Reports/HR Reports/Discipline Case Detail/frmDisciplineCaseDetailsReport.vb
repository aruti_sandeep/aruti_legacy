#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplineCaseDetailsReport

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineCaseDetailsReport"
    Private objCaseDetail As clsDisciplineCaseDetailReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        objCaseDetail = New clsDisciplineCaseDetailReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCaseDetail.SetDefaultValue()
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCategory As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objCategory.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "list")
            With cboOffenceCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
            objCategory = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboDisciplineType.SelectedValue = 0
            cboOffenceCategory.SelectedValue = 0
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime

            dtpFromDate.Checked = False
            dtpToDate.Checked = False
            chkIncludeInactiveEmployee.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objCaseDetail.SetDefaultValue()

            objCaseDetail._EmployeeId = cboEmployee.SelectedValue
            objCaseDetail._EmployeeName = cboEmployee.Text

            If dtpFromDate.Checked = True AndAlso dtpToDate.Checked = True Then
                objCaseDetail._ChargeDateFrom = dtpFromDate.Value.Date
                objCaseDetail._ChargeDateTo = dtpToDate.Value.Date
            End If
            objCaseDetail._OffenceCategory = cboOffenceCategory.Text
            objCaseDetail._OffenceCategoryId = CInt(cboOffenceCategory.SelectedValue)
            objCaseDetail._OffenceDescrId = CInt(cboDisciplineType.SelectedValue)
            objCaseDetail._OffenceDescription = cboDisciplineType.Text
            objCaseDetail._ViewByIds = mstrStringIds
            objCaseDetail._ViewIndex = mintViewIdx
            objCaseDetail._ViewByName = mstrStringName
            objCaseDetail._Analysis_Fields = mstrAnalysis_Fields
            objCaseDetail._Analysis_Join = mstrAnalysis_Join
            objCaseDetail._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCaseDetail._Report_GroupName = mstrReport_GroupName
            'S.SANDEEP |10-JUN-2020| -- START
            'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
            objCaseDetail._ShowAllocationBasedOnChargeDate = chkDisplayAllocationBasedOnChargeDate.Checked
            objCaseDetail._ShowAllocationBasedOnChargeDateString = chkDisplayAllocationBasedOnChargeDate.Text
            'S.SANDEEP |10-JUN-2020| -- END
            objCaseDetail._IncludeInactiveEmployee = chkIncludeInactiveEmployee.Checked

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Private Sub frmDisciplineCaseDetailsReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCaseDetail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineCaseDetailsReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineCaseDetailsReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objCaseDetail._ReportName
            Me._Message = objCaseDetail._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineCaseDetailsReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("TAB")
                    e.Handled = True
                    Exit Select

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            objCaseDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            objCaseDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDisciplineCaseDetailReport.SetMessages()
            objfrm._Other_ModuleNames = "clsDisciplineCaseDetailReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOffence_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOffence.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboDisciplineType.DataSource
            frm.DisplayMember = cboDisciplineType.DisplayMember
            frm.ValueMember = cboDisciplineType.ValueMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboDisciplineType.SelectedValue = frm.SelectedValue
                cboDisciplineType.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOffenceCategory.DataSource
            frm.DisplayMember = cboOffenceCategory.DisplayMember
            frm.ValueMember = cboOffenceCategory.ValueMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboOffenceCategory.SelectedValue = frm.SelectedValue
                cboOffenceCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
        Dim objOffence As New clsDisciplineType
        Dim dsList As New DataSet
        Try
            dsList = objOffence.getComboList("List", True, CInt(cboOffenceCategory.SelectedValue))
            With cboDisciplineType
                .ValueMember = "disciplinetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: cboOffenceCategory_SelectedIndexChanged; Module Name: " & mstrModuleName)
        Finally
            objOffence = Nothing : dsList.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog("EM")
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.Name, Me.lblDateFrom.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.Name, Me.lblOffenceCategory.Text)
            Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
