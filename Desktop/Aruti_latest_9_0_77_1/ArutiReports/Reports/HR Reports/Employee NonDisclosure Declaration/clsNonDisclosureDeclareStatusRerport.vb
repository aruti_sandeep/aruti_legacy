Imports Aruti.Data
Imports eZeeCommonLib
Imports ExcelWriter

Public Class clsNonDisclosureDeclareStatusRerport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsNonDisclosureDeclareStatusRerport"
    Private mstrReportId As String = enArutiReport.Confidentiality_Acknowledgement_Report
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mdtAsOnDate As DateTime = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnIncluderInactiveEmp As Boolean = False
    Private mdtAdOnDate As DateTime = Nothing
    Private mintDeclarationStatusID As Integer
    Private mstrDeclarationStatusName As String


    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvance_Filter As String = String.Empty


    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    Private mstrDeclareValueQuery As String = ""

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname



#End Region

#Region " Properties "
    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationStatusId() As Integer
        Set(ByVal value As Integer)
            mintDeclarationStatusID = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationStatusName() As String
        Set(ByVal value As String)
            mstrDeclarationStatusName = value
        End Set
    End Property

    'Sohail (24 Jun 2020) -- Start
    'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
    Private mintYearUnkId As Integer = 0
    Public WriteOnly Property _FinancialYearId() As Integer
        Set(ByVal value As Integer)
            mintYearUnkId = value
        End Set
    End Property

    Private mstrYearName As String = ""
    Public WriteOnly Property _FinancialYearName() As String
        Set(ByVal value As String)
            mstrYearName = value
        End Set
    End Property
    'Sohail (24 Jun 2020) -- End



    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mdtAsOnDate = Nothing
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mblnIncluderInactiveEmp = False
            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            mintYearUnkId = 0
            mstrYearName = ""
            'Sohail (24 Jun 2020) -- End
            
            mdtAsOnDate = Nothing
            mstrUserAccessFilter = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Dim strDeclareValueQuery As String = ""
        Dim strLiabValueQuery As String = ""
        Dim strADCatQuery As String = ""
        Try



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'If intReportType = 0 Then
            '    OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            '    OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            'Else

            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            'If intReportType = 0 Then
            '    Call OrderByExecute(iColumn_DetailReport)
            'Else

            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            'iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 43, "Employee Code")))
            'If mblnFirstNamethenSurname = False Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            'End If

            'iColumn_DetailReport.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 45, "Appointed Date")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Dept.name,'')", Language.getMessage(mstrModuleName, 46, "Department")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(JBM.job_name,'')", Language.getMessage(mstrModuleName, 47, "Job Title")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(ClsG.name,'')", Language.getMessage(mstrModuleName, 48, "Region")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Cls.name,'')", Language.getMessage(mstrModuleName, 49, "WorkStation")))

            'iColumn_DetailReport.Add(New IColumn("(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
            '                                    "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
            '                                    "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
            '                                    "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0))", Language.getMessage(mstrModuleName, 50, "Declared Amount")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hrassetdeclaration_master.debttotal, 0)", Language.getMessage(mstrModuleName, 51, "Liability Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Function Generate_DetailReport(ByVal strDatabaseName As String _
                                          , ByVal intUserUnkid As Integer _
                                          , ByVal intYearUnkid As Integer _
                                          , ByVal intCompanyUnkid As Integer _
                                          , ByVal dtPeriodStart As Date _
                                          , ByVal dtPeriodEnd As Date _
                                          , ByVal strUserModeSetting As String _
                                          , ByVal blnIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal xExportReportPath As String _
                                          , ByVal xOpenReportAfterExport As Boolean _
                                          , ByVal xBaseCurrencyId As Integer _
                                          ) As Boolean
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet

        Try
            'Sohail (20 Jun 2020) -- Start
            'NMB Issue : # : 4.The printed by section of the employee acknowledgement report does not show the display name of user who generated the report. Its showing blank.
            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = intUserUnkid
            Dim strUserName As String = objUser._Firstname & " " & objUser._Lastname
            If strUserName.Trim = "" Then
                strUserName = objUser._Username
            End If
            'Sohail (20 Jun 2020) -- End

            'Sohail (10 Sep 2021) -- Start
            'NMB Issue :  : Non-Disclosure status report not showing any data when past year is selected.
            If mintYearUnkId > 0 Then
                Dim objComoany As New clsCompany_Master
                objComoany._YearUnkid = mintYearUnkId
                intYearUnkid = mintYearUnkId
                strDatabaseName = objComoany._DatabaseName
                dtPeriodStart = objComoany._Database_Start_Date
                dtPeriodEnd = objComoany._Database_End_Date
            End If
            'Sohail (10 Sep 2021) -- End

            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, True, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            Dim mdtExcel As New DataTable

            mdtExcel.Columns.Add("sno", System.Type.GetType("System.String")).DefaultValue = 0
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 1, "SNo")
            mdtExcel.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 2, "Employee Code")
            mdtExcel.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 3, "Employee Name")
            mdtExcel.Columns.Add("department", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 4, "Function")
            mdtExcel.Columns.Add("sectiongroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 5, "Department")
            mdtExcel.Columns.Add("classgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 6, "Zone")
            mdtExcel.Columns.Add("class", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 7, "Branch")
            'Sohail (20 Jun 2020) -- Start
            'NMB Enhancement # : On confidentiality acknowledgement report, include a column for employee job.
            mdtExcel.Columns.Add("job_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 18, "Job")
            'Sohail (20 Jun 2020) -- End
            mdtExcel.Columns.Add("gradegroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 8, "Designation Level")
            mdtExcel.Columns.Add("Email", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 9, "Email")
            mdtExcel.Columns.Add("Status", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 10, "Status")
            mdtExcel.Columns.Add("Acknowledgement_Date", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 11, "Acknowledgement Date")
            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality acknowledgement report, provide column to show employee status i.e Active for active employees and Inactive for terminated employees.
            mdtExcel.Columns.Add("EmpActiveStatus", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 20, "Employee Status")
            'Sohail (24 Jun 2020) -- End

            mdtExcel.Columns.Add("GName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcel.Columns(mdtExcel.Columns.Count - 1).Caption = mstrAnalysis_OrderBy_GroupName


            StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", ISNULL(hrdepartment_master.name, '') AS department " & _
                            ", ISNULL(hrsectiongroup_master.name, '') AS sectiongroup " & _
                            ", ISNULL(hrclassgroup_master.name, '') AS classgroup " & _
                            ", ISNULL(hrclasses_master.name, '') AS class " & _
                            ", ISNULL(hrgradegroup_master.name, '') AS gradegroup " & _
                            ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                            ", hremployee_master.email " & _
                            ", CASE WHEN CONVERT(CHAR(8),hremployee_master.appointeddate, 112) <= @enddate  " & _
                                "AND ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date, 112),@startdate) >= @startdate " & _
                                "AND ISNULL(CONVERT(CHAR(8),ERET.termination_to_date, 112),@startdate) >= @startdate " & _
                                "AND ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate, 112),@startdate) >= @startdate " & _
                                "THEN @Active ELSE @Inactive END AS EmpActiveStatus "
            'Sohail (24 Jun 2020) - [EmpActiveStatus]
            'Sohail (20 Jun 2020) - [job_name]

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS employeename "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS employeename "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "INTO    #TableEmp " & _
                    "FROM  " & strDatabaseName & "..hremployee_master "

            StrQ &= mstrAnalysis_Join

            StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 gradegroupunkid " & _
                        "		,gradeunkid " & _
                        "		,gradelevelunkid " & _
                        "		,employeeunkid " & _
                        "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                        "	FROM " & strDatabaseName & "..prsalaryincrement_tran " & _
                        "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 TERM.TEEmpId " & _
                    "    		,TERM.empl_enddate " & _
                    "    		,TERM.termination_from_date " & _
                    "    		,TERM.TEfDt " & _
                    "    		,TERM.isexclude_payroll " & _
                    "    		,TERM.TR_REASON " & _
                    "    		,TERM.changereasonunkid " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             TRM.employeeunkid AS TEEmpId " & _
                    "            ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "            ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "            ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "            ,TRM.isexclude_payroll " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "            ,TRM.changereasonunkid " & _
                    "        FROM " & strDatabaseName & "..hremployee_dates_tran AS TRM " & _
                    "            LEFT JOIN " & strDatabaseName & "..cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN " & strDatabaseName & "..cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "        WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 RET.REmpId " & _
                    "    	,RET.termination_to_date " & _
                    "    	,RET.REfDt " & _
                    "    	,RET.RET_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             RTD.employeeunkid AS REmpId " & _
                    "            ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "            ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "        FROM " & strDatabaseName & "..hremployee_dates_tran AS RTD " & _
                    "            LEFT JOIN " & strDatabaseName & "..cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN " & strDatabaseName & "..cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "        WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = T.sectiongroupunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON hrclassgroup_master.classgroupunkid = T.classgroupunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON hrclasses_master.classesunkid = T.classunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = G.gradegroupunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrjob_master ON hrjob_master.jobunkid = J.jobunkid "
            'Sohail (24 Jun 2020) - [LEFT JOIN ETER, LEFT JOIN ERET]
            'Sohail (20 Jun 2020) - [LEFT JOIN hrjob_master]

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If




            If blnIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            StrQ &= "SELECT " & _
                      "  hrempnondisclosure_declaration_tran.nondisclosuredeclarationtranunkid " & _
                      ", hrempnondisclosure_declaration_tran.employeeunkid " & _
                      ", #TableEmp.employeecode " & _
                      ", #TableEmp.employeename " & _
                      ", #TableEmp.department " & _
                      ", #TableEmp.sectiongroup " & _
                      ", #TableEmp.classgroup " & _
                      ", #TableEmp.class " & _
                      ", #TableEmp.gradegroup " & _
                      ", #TableEmp.job_name " & _
                      ", #TableEmp.email " & _
                      ", #TableEmp.EmpActiveStatus " & _
                      ", #TableEmp.GName " & _
                      ", hrempnondisclosure_declaration_tran.yearunkid " & _
                      ", cffinancial_year_tran.financialyear_name " & _
                      ", hrempnondisclosure_declaration_tran.declaration_date " & _
                      ", CONVERT(CHAR(8), hrempnondisclosure_declaration_tran.declaration_date, 112) AS strdeclaration_date " & _
                      ", hrempnondisclosure_declaration_tran.witness1userunkid " & _
                      ", hrempnondisclosure_declaration_tran.witness1_date " & _
                      ", LTRIM(CONVERT(CHAR(8), ISNULL(hrempnondisclosure_declaration_tran.witness1_date, ''), 112)) AS strwitness1_date " & _
                      ", hrempnondisclosure_declaration_tran.witness2userunkid " & _
                      ", hrempnondisclosure_declaration_tran.witness2_date " & _
                      ", LTRIM(CONVERT(CHAR(8), ISNULL(hrempnondisclosure_declaration_tran.witness2_date, ''), 112)) AS strwitness2_date " & _
                      ", hrempnondisclosure_declaration_tran.issubmitforapproval " & _
                      ", hrempnondisclosure_declaration_tran.approvalstatusunkid " & _
                      ", hrempnondisclosure_declaration_tran.finalapproverunkid " & _
                      ", hrempnondisclosure_declaration_tran.userunkid " & _
                      ", hrempnondisclosure_declaration_tran.loginemployeeunkid " & _
                      ", hrempnondisclosure_declaration_tran.isweb " & _
                      ", hrempnondisclosure_declaration_tran.isvoid " & _
                      ", hrempnondisclosure_declaration_tran.voiduserunkid " & _
                      ", hrempnondisclosure_declaration_tran.voidloginemployeeunkid " & _
                      ", hrempnondisclosure_declaration_tran.voiddatetime " & _
                      ", hrempnondisclosure_declaration_tran.voidreason " & _
                 "FROM #TableEmp " & _
                      "LEFT JOIN " & strDatabaseName & "..hrempnondisclosure_declaration_tran ON #TableEmp.employeeunkid = hrempnondisclosure_declaration_tran.employeeunkid " & _
                            "AND hrempnondisclosure_declaration_tran.isvoid = 0 "

            'Sohail (10 Sep 2021) -- Start
            'NMB Issue :  : Non-Disclosure status report not showing any data when past year is selected.
            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrempnondisclosure_declaration_tran.declaration_date, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' "
            End If
            'Sohail (10 Sep 2021) -- End

            StrQ &= "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.yearunkid = hrempnondisclosure_declaration_tran.yearunkid "
            'Sohail (24 Jun 2020) - [EmpActiveStatus]
            'Sohail (20 Jun 2020) - [job_name]

            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            If mintYearUnkId > 0 Then
                StrQ &= " AND hrempnondisclosure_declaration_tran.yearunkid = @yearunkid "
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearUnkId)
            End If
            'Sohail (24 Jun 2020) -- End

            StrQ &= "WHERE 1 = 1 "

            If mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.NOT_SUBMITTED) Then
                StrQ &= " AND hrempnondisclosure_declaration_tran.nondisclosuredeclarationtranunkid IS NULL  "
            ElseIf mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.SUMBITTED) Then
                StrQ &= " AND hrempnondisclosure_declaration_tran.nondisclosuredeclarationtranunkid IS NOT NULL  "
            End If

            StrQ &= " ORDER BY #TableEmp.employeename "

            StrQ &= " DROP TABLE #TableEmp "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality acknowledgement report, provide column to show employee status i.e Active for active employees and Inactive for terminated employees.
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart).ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd).ToString)

            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Active"))
            objDataOperation.AddParameter("@Inactive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Inactive"))
            'Sohail (24 Jun 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Dim strAcknowledged As String = Language.getMessage(mstrModuleName, 12, "Acknowledged")
            Dim strNotAcknowledged As String = Language.getMessage(mstrModuleName, 13, "Not Acknowledged")

            Dim intSrNo As Integer = 0
            Dim dr As DataRow = Nothing
            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dr = mdtExcel.NewRow
                intSrNo += 1

                dr.Item("sno") = intSrNo
                dr.Item("employeecode") = dsRow.Item("employeecode").ToString
                dr.Item("employeename") = dsRow.Item("employeename").ToString
                dr.Item("department") = dsRow.Item("department").ToString
                dr.Item("sectiongroup") = dsRow.Item("sectiongroup").ToString
                dr.Item("classgroup") = dsRow.Item("classgroup").ToString
                dr.Item("class") = dsRow.Item("class").ToString
                dr.Item("gradegroup") = dsRow.Item("gradegroup").ToString
                'Sohail (20 Jun 2020) -- Start
                'NMB Enhancement # : On confidentiality acknowledgement report, include a column for employee job.
                dr.Item("job_name") = dsRow.Item("job_name").ToString
                'Sohail (20 Jun 2020) -- End
                dr.Item("Email") = dsRow.Item("Email").ToString
                If IsDBNull(dsRow.Item("nondisclosuredeclarationtranunkid")) = True Then
                    dr.Item("Status") = strNotAcknowledged
                Else
                    dr.Item("Status") = strAcknowledged
                End If
                If IsDBNull(dsRow.Item("declaration_date")) = True Then
                    dr.Item("Acknowledgement_Date") = ""
                Else
                    dr.Item("Acknowledgement_Date") = CDate(dsRow.Item("declaration_date")).ToString("dd-MMM-yyyy")
                End If
                dr.Item("GName") = dsRow.Item("GName").ToString
                'Sohail (24 Jun 2020) -- Start
                'NMB Enhancement # : On the confidentiality acknowledgement report, provide column to show employee status i.e Active for active employees and Inactive for terminated employees.
                dr.Item("EmpActiveStatus") = dsRow.Item("EmpActiveStatus").ToString
                'Sohail (24 Jun 2020) -- End

                mdtExcel.Rows.Add(dr)
            Next

            'Sohail (20 Jun 2020) -- Start
            'NMB Issue : # : 4.The printed by section of the employee acknowledgement report does not show the display name of user who generated the report. Its showing blank.
            Me._UserName = strUserName
            'Sohail (20 Jun 2020) -- End

            Dim intArrayColumnWidth As Integer() = Nothing
            Dim strarrGroupColumns As String() = Nothing
            ReDim intArrayColumnWidth(mdtExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(ii) = 125
            Next
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing

            If mintViewIndex <= 0 Then
                mdtExcel.Columns.Remove("GName")
            Else
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If

            Dim strFilterTitle As String = Language.getMessage(mstrModuleName, 14, "Status :")
            If mintDeclarationStatusID > 0 Then
                strFilterTitle &= mstrDeclarationStatusName
            Else
                strFilterTitle &= Language.getMessage(mstrModuleName, 15, "All")
            End If
            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            If mintYearUnkId > 0 Then
                strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 19, "Year :") & mstrYearName
            End If
            'Sohail (24 Jun 2020) -- End
            strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 16, "As On Date :") & mdtAsOnDate.ToString("dd-MMM-yyyy")

            If mintEmployeeId > 0 Then
                strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 17, "Employee :") & mstrEmployeeName
            End If
            row = New WorksheetRow()
            wcell = New WorksheetCell(strFilterTitle, "s9bw")
            wcell.MergeAcross = mdtExcel.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, Nothing)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "SNo")
            Language.setMessage(mstrModuleName, 2, "Employee Code")
            Language.setMessage(mstrModuleName, 3, "Employee Name")
            Language.setMessage(mstrModuleName, 4, "Function")
            Language.setMessage(mstrModuleName, 5, "Department")
            Language.setMessage(mstrModuleName, 6, "Zone")
            Language.setMessage(mstrModuleName, 7, "Branch")
            Language.setMessage(mstrModuleName, 8, "Designation Level")
            Language.setMessage(mstrModuleName, 9, "Email")
            Language.setMessage(mstrModuleName, 10, "Status")
            Language.setMessage(mstrModuleName, 11, "Acknowledgement Date")
            Language.setMessage(mstrModuleName, 12, "Acknowledged")
            Language.setMessage(mstrModuleName, 13, "Not Acknowledged")
            Language.setMessage(mstrModuleName, 14, "Status :")
            Language.setMessage(mstrModuleName, 15, "All")
            Language.setMessage(mstrModuleName, 16, "As On Date :")
            Language.setMessage(mstrModuleName, 17, "Employee :")
            Language.setMessage(mstrModuleName, 18, "Job")
            Language.setMessage(mstrModuleName, 19, "Year :")
            Language.setMessage(mstrModuleName, 20, "Employee Status")
            Language.setMessage(mstrModuleName, 21, "Active")
            Language.setMessage(mstrModuleName, 22, "Inactive")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
