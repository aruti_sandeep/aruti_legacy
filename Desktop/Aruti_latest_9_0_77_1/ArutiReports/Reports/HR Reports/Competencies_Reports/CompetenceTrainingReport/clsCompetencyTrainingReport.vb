﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsCompetencyTrainingReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsCompetencyTrainingReport"
    Private mstrReportId As String = enArutiReport.Competence_Training_Intervension_Report

#Region " Properties Value "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""
    Private mstrAdvanceFilter As String = ""
    Private mblnFirstNamethenSurname As Boolean = True
    Private mintCompetenceGroupId As Integer = 0
    Private mstrCompetenceGroup As String = ""
    Private mintTrainingCourseId As Integer = 0
    Private mstrTrainingCourseName As String = ""

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _CompetenceGroupId() As Integer
        Set(ByVal value As Integer)
            mintCompetenceGroupId = value
        End Set
    End Property

    Public WriteOnly Property _CompetenceGroup() As String
        Set(ByVal value As String)
            mstrCompetenceGroup = value
        End Set
    End Property

    Public WriteOnly Property _TrainingCourseId() As Integer
        Set(ByVal value As Integer)
            mintTrainingCourseId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingCourseName() As String
        Set(ByVal value As String)
            mstrTrainingCourseName = value
        End Set
    End Property

#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mstrAdvanceFilter = ""
            mblnFirstNamethenSurname = True
            mintCompetenceGroupId = 0
            mstrCompetenceGroup = ""
            mintTrainingCourseId = 0
            mstrTrainingCourseName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub ExportCompetence_TrainingReport(ByVal strDatabaseName As String, _
                                                ByVal intUserUnkid As Integer, _
                                                ByVal intYearUnkid As Integer, _
                                                ByVal intCompanyUnkid As Integer, _
                                                ByVal dtPeriodStart As Date, _
                                                ByVal dtPeriodEnd As Date, _
                                                ByVal strUserModeSetting As String, _
                                                ByVal blnOnlyApproved As Boolean, _
                                                ByVal strExportPath As String, _
                                                ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            StrQ = "IF OBJECT_ID('tempdb..#result') IS NOT NULL " & _
                "DROP TABLE #result " & _
                "IF OBJECT_ID('tempdb..#fresult') IS NOT NULL " & _
                "DROP TABLE #fresult " & _
                "SELECT DISTINCT " & _
                   "A.competenciesunkid " & _
                  ",A.competence_categoryunkid " & _
                  ",A.coursemasterunkid " & _
                  ",hremployee_master.employeeunkid INTO #result " & _
                "FROM hremployee_master "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                            "stationunkid " & _
                            ",deptgroupunkid " & _
                            ",departmentunkid " & _
                            ",sectiongroupunkid " & _
                            ",sectionunkid " & _
                            ",unitgroupunkid " & _
                            ",unitunkid " & _
                            ",teamunkid " & _
                            ",classgroupunkid " & _
                            ",classunkid " & _
                            ",employeeunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "FROM hremployee_transfer_tran " & _
                        "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                            "jobunkid " & _
                            ",jobgroupunkid " & _
                            ",employeeunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "FROM hremployee_categorization_tran " & _
                        "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                            "cctranheadvalueid AS costcenterunkid " & _
                            ",employeeunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "FROM hremployee_cctranhead_tran " & _
                        "WHERE istransactionhead = 0 AND isvoid = 0 " & _
                        "AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                            "prsalaryincrement_tran.gradeunkid " & _
                            ",prsalaryincrement_tran.employeeunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                        "FROM prsalaryincrement_tran " & _
                        "WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                        "AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    ") AS Grd ON Grd.employeeunkid = hremployee_master.employeeunkid AND Grd.rno = 1 "


            StrQ &= "LEFT JOIN ( " & _
                          "SELECT " & _
                               " CAT.competenciesunkid " & _
                               ",CM.competence_categoryunkid " & _
                               ",AGM.referenceunkid " & _
                               ",AGT.allocationunkid " & _
                               ",TR.employeeunkid " & _
                               ",TR.coursemasterunkid " & _
                               "FROM hrassess_competence_assign_master AS CAM " & _
                               "JOIN hrassess_group_master AS AGM ON CAM.assessgroupunkid = AGM.assessgroupunkid " & _
                               "JOIN hrassess_group_tran AS AGT ON AGM.assessgroupunkid = AGT.assessgroupunkid " & _
                               "JOIN hrassess_competence_assign_tran AS CAT ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                               "JOIN hrassess_competencies_master AS CM ON CM.competenciesunkid = CAT.competenciesunkid " & _
                               "JOIN hrassess_competencies_trainingcourses_tran AS CTT ON CTT.competenciesunkid = CM.competenciesunkid AND CTT.isvoid = 0 " & _
                               "JOIN trtraining_request_master AS TR ON TR.coursemasterunkid = CTT.masterunkid AND TR.isvoid = 0 " & _
                          "WHERE CAM.isvoid = 0 AND AGM.isactive = 1 AND AGT.isactive = 1 AND CAT.isvoid = 0 AND TR.iscompleted_submit_approval = 1 AND TR.completed_statusunkid = 2 "
            If mintPeriodId > 0 Then
                StrQ &= " AND TR.periodunkid = @periodunkid "
            End If
            If mintCompetenceGroupId > 0 Then
                StrQ &= " AND CM.competence_categoryunkid = @competence_categoryunkid "
            End If
            If mintTrainingCourseId > 0 Then
                StrQ &= " AND TR.coursemasterunkid = @coursemasterunkid "
            End If
            StrQ &= ") AS A ON A.employeeunkid = hremployee_master.employeeunkid " & _
                            " AND CASE WHEN A.referenceunkid = 1 THEN Alloc.stationunkid " & _
                                 "WHEN A.referenceunkid = 2 THEN Alloc.deptgroupunkid " & _
                                 "WHEN A.referenceunkid = 3 THEN Alloc.departmentunkid " & _
                                 "WHEN A.referenceunkid = 4 THEN Alloc.sectiongroupunkid " & _
                                 "WHEN A.referenceunkid = 5 THEN Alloc.sectionunkid " & _
                                 "WHEN A.referenceunkid = 6 THEN Alloc.unitgroupunkid " & _
                                 "WHEN A.referenceunkid = 7 THEN Alloc.unitunkid " & _
                                 "WHEN A.referenceunkid = 8 THEN Alloc.teamunkid " & _
                                 "WHEN A.referenceunkid = 9 THEN Jobs.jobgroupunkid " & _
                                 "WHEN A.referenceunkid = 10 THEN Jobs.jobunkid " & _
                                 "WHEN A.referenceunkid = 11 THEN hremployee_master.employeeunkid " & _
                                 "WHEN A.referenceunkid = 13 THEN Alloc.classgroupunkid " & _
                                 "WHEN A.referenceunkid = 14 THEN Alloc.classunkid " & _
                                 "WHEN A.referenceunkid = 15 THEN CC.costcenterunkid " & _
                                 "WHEN A.referenceunkid = 16 THEN Grd.gradeunkid END = A.allocationunkid " & _
                "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            StrQ &= "SELECT DISTINCT * INTO #fresult FROM #result WHERE competence_categoryunkid IS NOT NULL " & _
                    "SELECT " & _
                    "    EM.employeecode AS eCode " & _
                    "   ,EM.firstname+' '+EM.surname AS eName " & _
                    "   ,CC.name AS eCategory " & _
                    "   ,CM.name AS eTraining " & _
                    "FROM #fresult AS FT " & _
                    "   JOIN hremployee_master EM ON EM.employeeunkid = FT.employeeunkid " & _
                    "   LEFT JOIN cfcommon_master AS CC ON CC.masterunkid = FT.competence_categoryunkid " & _
                    "   LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = FT.coursemasterunkid " & _
                    "WHERE 1 = 1 "

            StrQ &= "DROP TABLE #result " & _
                    "DROP TABLE #fresult "


            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceGroupId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingCourseId)


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList.Tables(0).Columns("eCode").Caption = Language.getMessage(mstrModuleName, 100, "Code")
            dsList.Tables(0).Columns("eName").Caption = Language.getMessage(mstrModuleName, 101, "Name")
            dsList.Tables(0).Columns("eCategory").Caption = Language.getMessage(mstrModuleName, 103, "Competence Group")
            dsList.Tables(0).Columns("eTraining").Caption = Language.getMessage(mstrModuleName, 104, "Training")

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsList.Tables(0).Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 80
                    Case Else
                        intArrayColumnWidth(i) = 150
                End Select
            Next

            Dim strFilter As String = ""

            If mintPeriodId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 200, "Period") & " : " & mstrPeriodName & " "
            End If

            If mintCompetenceGroupId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 103, "Competence Group") & " : " & mstrCompetenceGroup & " "
            End If

            If mintEmployeeId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 201, "Employee") & " : " & mstrEmployeeName & " "
            End If

            If mintTrainingCourseId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 104, "Training") & " : " & mstrTrainingCourseName & " "
            End If

            strFilter = Mid(strFilter, 2)

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables(0), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName, "", strFilter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportCompetence_TrainingReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Code")
            Language.setMessage(mstrModuleName, 101, "Name")
            Language.setMessage(mstrModuleName, 103, "Competence Group")
            Language.setMessage(mstrModuleName, 104, "Training")
            Language.setMessage(mstrModuleName, 200, "Period")
            Language.setMessage(mstrModuleName, 201, "Employee")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
