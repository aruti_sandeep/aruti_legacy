﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsCompetenceFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCompetenceFormReport"
    Private mstrReportId As String = enArutiReport.Competence_Assessment_Form_Report

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mblnIsSelfAssignCompetencies As Boolean = False
    Private mblnIsUsedAgreedScore As Boolean = False
    Private mintCompanyUnkid As Integer = 0
    Private mintScoringOptionId As Integer = 0

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IsSelfAssignCompetencies() As Boolean
        Set(ByVal value As Boolean)
            mblnIsSelfAssignCompetencies = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _IsUsedAgreedScore() As Boolean
        Set(ByVal value As Boolean)
            mblnIsUsedAgreedScore = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ScoringOptionId() As Integer
        Set(ByVal value As Integer)
            mintScoringOptionId = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mblnIsSelfAssignCompetencies = False
            mblnIsUsedAgreedScore = False
            mintScoringOptionId = 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Function GetEmployee_AssessorReviewerDetails(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal strDataBaseName As String, ByVal dtEmpAsOnDate As Date) As DataSet
        Dim dsList As New DataSet
        Dim objEvalMaster As New clsevaluation_analysis_master
        Try
            dsList = objEvalMaster.GetEmployee_AssessorReviewerDetails(intPeriodId, strDataBaseName, Nothing, intEmployeeId)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_AssessorReviewerDetails; Module Name: " & mstrModuleName)
        Finally
            objEvalMaster = Nothing
        End Try
        Return dsList
    End Function

    Private Function GetCompetenciesData(ByVal strGroupIds As String, ByVal blnIsSlefAssignCompetencies As Boolean, ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer) As DataSet 'S.SANDEEP [15 FEB 2016] -- START {intPeriodId} -- END
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Try
            Using objDo As New clsDataOperation
                Dim StrTableName As String = ""
                Dim objCP As New clsassess_competencies_master
                StrTableName = objCP.GetCompetencyTableName(intPeriodId, objDo)
                objCP = Nothing

                StrQ = "SELECT " & _
                       "     assessgroup_name AS Col_AssessmentGroup_Value " & _
                       "    ,cfcommon_master.name AS Col_CompetencyCategory_Value " & _
                       "    , " & StrTableName & " .name AS Col_Competencies_Value " & _
                       "    ,hrassess_competence_assign_tran.weight AS Col_Competencies_Weight_Value " & _
                       "    , " & StrTableName & " .competenciesunkid " & _
                       "    ,hrassess_group_master.assessgroupunkid " & _
                       "    , " & StrTableName & " .competence_categoryunkid " & _
                       "    ,hrassess_group_master.weight AS Col_Group_Weight_Value " & _
                       "    , " & StrTableName & " .description AS Col_Competencies_Description_Value " & _
                       "    , cfcommon_master.description AS Col_Competencies_Category_Description_Value " & _
                       "FROM hrassess_competence_assign_tran " & _
                       "    JOIN " & StrTableName & " ON hrassess_competence_assign_tran.competenciesunkid =  " & StrTableName & " .competenciesunkid AND " & StrTableName & ".periodunkid = '" & intPeriodId & "' " & _
                       "    JOIN cfcommon_master ON cfcommon_master.masterunkid =  " & StrTableName & " .competence_categoryunkid " & _
                       "    JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                       "    JOIN hrassess_group_master ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                       "WHERE hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 " & _
                       "    AND hrassess_group_master.assessgroupunkid IN(" & strGroupIds & ") AND hrassess_competence_assign_master.periodunkid = '" & intPeriodId & "'  "

                If blnIsSlefAssignCompetencies = True Then
                    StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & intEmployeeId & "' "
                End If

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCompetenciesData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Private Function GetEmployeeCompetencyResult(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal strGroupIds As String, ByVal intAssessorId As Integer, ByVal intReviewerId As Integer, ByVal strWeightAsNumber As String, ByVal blnIsUsedAgreedScore As Boolean) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Using objDo As New clsDataOperation

            Dim StrTableName As String = ""
            Dim objCP As New clsassess_competencies_master
            StrTableName = objCP.GetCompetencyTableName(intPeriodId, objDo)
            objCP = Nothing

            StrQ = "SELECT DISTINCT " & _
                   "  hrevaluation_analysis_master.assessmodeid " & _
                   " ,CAST(CASE WHEN @WNum = 1 THEN hrassess_competence_assign_tran.weight * hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.result END AS DECIMAL(36,2)) AS result " & _
                   " ,hrcompetency_analysis_tran.remark " & _
                   " ,hrcompetency_analysis_tran.competenciesunkid " & _
                   " , " & StrTableName & " .competence_categoryunkid " & _
                   " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                   "FROM hrcompetency_analysis_tran " & _
                   " JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   " JOIN  " & StrTableName & "  ON hrassess_competence_assign_tran.competenciesunkid =  " & StrTableName & " .competenciesunkid AND  " & StrTableName & " .competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid IN(" & strGroupIds & ")" & _
                   "  AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' AND hrevaluation_analysis_master.selfemployeeunkid = '" & intEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                   "UNION ALL " & _
                   "SELECT DISTINCT " & _
                   "  hrevaluation_analysis_master.assessmodeid " & _
                   " ,CAST(CASE WHEN @WNum = 1 THEN hrassess_competence_assign_tran.weight * hrcompetency_analysis_tran." & IIf(blnIsUsedAgreedScore, "agreedscore", "result") & " ELSE hrcompetency_analysis_tran." & IIf(blnIsUsedAgreedScore, "agreedscore", "result") & " END AS DECIMAL(36,2)) AS result " & _
                   " ,hrcompetency_analysis_tran.remark " & _
                   " ,hrcompetency_analysis_tran.competenciesunkid " & _
                   " , " & StrTableName & " .competence_categoryunkid " & _
                   " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                   "FROM hrcompetency_analysis_tran " & _
                   " JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   " JOIN  " & StrTableName & "  ON hrassess_competence_assign_tran.competenciesunkid =  " & StrTableName & " .competenciesunkid AND  " & StrTableName & " .competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = '" & intAssessorId & "' " & _
                   "  AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
                   "  AND hrassess_competence_assign_master.assessgroupunkid IN(" & strGroupIds & ")" & _
                   "UNION ALL " & _
                   "SELECT DISTINCT " & _
                   "  hrevaluation_analysis_master.assessmodeid " & _
                   " ,CAST(CASE WHEN @WNum = 1 THEN hrassess_competence_assign_tran.weight * hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.result END AS DECIMAL(36,2)) AS result " & _
                   " ,hrcompetency_analysis_tran.remark " & _
                   " ,hrcompetency_analysis_tran.competenciesunkid " & _
                   " , " & StrTableName & " .competence_categoryunkid " & _
                   " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                   "FROM hrcompetency_analysis_tran " & _
                   " JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   " JOIN  " & StrTableName & "  ON hrassess_competence_assign_tran.competenciesunkid =  " & StrTableName & " .competenciesunkid AND  " & StrTableName & " .competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = " & intReviewerId & " AND hrassess_competence_assign_master.assessgroupunkid IN(" & strGroupIds & ")" & _
                   "  AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

            objDo.AddParameter("@WNum", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(strWeightAsNumber))

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If

        End Using
        Return dsList
    End Function

    Private Function GetCompetencyScaleData(ByVal intPeriodId As Integer, ByVal strCSVCompetenciesId As String) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Using objDo As New clsDataOperation

            Dim StrTableName As String = ""
            Dim objCP As New clsassess_competencies_master
            StrTableName = objCP.GetCompetencyTableName(intPeriodId, objDo)
            objCP = Nothing

            StrQ = "SELECT DISTINCT " & _
                   "    @ScaleGroup AS Col_ScaleGroup_Caption " & _
                   "   ,@Scale AS Col_Scale_Caption " & _
                   "   ,@Description AS Col_Description_Caption " & _
                   "   ,ISNULL(cfcommon_master.name,'') AS Col_ScaleGroup_Value " & _
                   "   ,scale AS Col_Scale_Value " & _
                   "   ,hrassess_scale_master.description AS Col_Description_Value " & _
                   "FROM hrassess_scale_master " & _
                   "   JOIN " & StrTableName & " ON hrassess_scale_master.scalemasterunkid = " & StrTableName & ".scalemasterunkid " & _
                   "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_scale_master.scalemasterunkid " & _
                   "WHERE hrassess_scale_master.isactive = 1 AND hrassess_scale_master.periodunkid = '" & intPeriodId & "' "

            If strCSVCompetenciesId.Trim.Length > 0 Then
                StrQ &= " AND " & StrTableName & ".competenciesunkid IN(" & strCSVCompetenciesId & ")"
            End If

            objDo.AddParameter("@ScaleGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Scale Group"))
            objDo.AddParameter("@Scale", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Scale"))
            objDo.AddParameter("@Description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Description"))

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If

        End Using
        Return dsList
    End Function

    Private Function GetEmployeeDetails(ByVal intEmployeeId As Integer, ByVal mdtEmpAsOnDate As DateTime, ByVal intPeriodId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     ISNULL(ttype.name,'') AS Col_Title " & _
                       "    ,ISNULL(hremployee_master.employeecode,'') AS Col_EmpCode " & _
                       "    ,ISNULL(hremployee_master.firstname,'') AS Col_EmpFirstName " & _
                       "    ,ISNULL(hremployee_master.surname,'') AS Col_EmpSurName " & _
                       "    ,ISNULL(hremployee_master.othername,'') AS Col_OtherName " & _
                       "    ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Col_FullEmpName " & _
                       "    ,ISNULL(hremployee_master.displayname,'') AS Col_DisplayName " & _
                       "    ,ISNULL(hremployee_master.email,'') AS Col_Email " & _
                       "    ,CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Col_Gender " & _
                       "    ,ISNULL(cfcommon_master.name,'') AS Col_EmployementType " & _
                       "    ,ISNULL(hrstation_master.name,'') AS Col_Branch " & _
                       "    ,ISNULL(hrdepartment_group_master.name,'') AS Col_DepartmentGroup " & _
                       "    ,ISNULL(hrdepartment_master.name,'') AS Col_Department " & _
                       "    ,ISNULL(hrsectiongroup_master.name, '') AS Col_SectionGroup " & _
                       "    ,ISNULL(hrsection_master.name,'') AS Col_Section " & _
                       "    ,ISNULL(hrunitgroup_master.name, '') AS Col_UnitGroup " & _
                       "    ,ISNULL(hrunit_master.name,'') AS Col_Unit " & _
                       "    ,ISNULL(hrteam_master.name,'') AS Col_Team " & _
                       "    ,ISNULL(hrclassgroup_master.name,'') AS Col_ClassGroup " & _
                       "    ,ISNULL(hrclasses_master.name,'') AS Col_Class " & _
                       "    ,ISNULL(hrjobgroup_master.name,'') AS Col_JobGroup " & _
                       "    ,ISNULL(hrjob_master.job_name,'') AS Col_Job " & _
                       "    ,ISNULL(prcostcenter_master.costcentername,'') AS Col_CostCenter " & _
                       "    ,ISNULL(Sft.ShiftName,'') AS Col_ShiftName " & _
                       "    ,ISNULL(CGrades.CurrentGradeGroup,'') AS Col_CurrentGradeGroup " & _
                       "    ,ISNULL(CGrades.CurrentGrade,'') AS Col_CurrentGrade " & _
                       "    ,ISNULL(CGrades.CurrentGradeLevel,'') AS Col_CurrentGradeLevel " & _
                       "    ,ISNULL(CGrades.CurrentSalary,0) AS Col_CurrentSalary " & _
                       "    ,ISNULL(NGrades.NextGradeGroup,'') AS Col_NextGradeGroup " & _
                       "    ,ISNULL(NGrades.NextGrade,'') AS Col_NextGrade " & _
                       "    ,ISNULL(NGrades.NextGradeLevel,'') AS Col_NextGradeLevel " & _
                       "    ,ISNULL(NGrades.NextSalary,0) AS Col_NextSalary " & _
                       "    ,ISNULL(nct.country_name,'') AS Col_Nationality " & _
                       "    ,ISNULL(mtype.name,'') AS Col_MaritalStatus " & _
                       "    ,ISNULL(RepTable.Reporting_Code,'') AS Col_ReportingCode " & _
                       "    ,ISNULL(RepTable.Reporting_Name, '') AS Col_FullReportingName " & _
                       "    ,ISNULL(RepTable.Reporting_Job, '') AS Col_ReportingJob " & _
                       "    ,ISNULL(RepTable.rfname,'') AS Col_ReportingFirstName " & _
                       "    ,ISNULL(RepTable.roname,'') AS Col_ReportingOtherName " & _
                       "    ,ISNULL(RepTable.rsname,'') AS Col_ReportingSurName " & _
                       "    ,ISNULL(Assess.ACode,'') AS Col_AssessorCode " & _
                       "    ,ISNULL(Assess.AName,'') AS Col_AssessorName " & _
                       "    ,ISNULL(Review.RCode,'') AS Col_ReviewerCode " & _
                       "    ,ISNULL(Review.RName,'') AS Col_ReviwerName " & _
                       "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.firstappointment_date,112),'') AS Col_FirstAppointmentDate " & _
                       "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS Col_AppointmentDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),ECNF.confirmation_date,112),'') AS Col_ConfirmationDate " & _
                       "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS Col_BirthDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),ESUSP.suspended_from_date,112),'') AS Col_SuspendedFromDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),ESUSP.suspended_to_date,112),'') AS Col_SuspendedToDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),EPROB.probation_from_date,112),'') AS Col_ProbationFromDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),EPROB.probation_to_date, 112),'') AS Col_ProbationToDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate , 112),'') AS Col_EndOfContract_Date " & _
                              "    ,ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date,112),'') AS Col_LeavingDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),ERET.termination_to_date,112),'') AS Col_RetirementDate " & _
                              "    ,ISNULL(CONVERT(CHAR(8),ERH.reinstatment_date , 112),'') AS Col_Reinstatement_Date " & _
                       "    ,ISNULL(present_address1,'') AS Col_PresentAddress1 " & _
                       "    ,ISNULL(present_address2,'') AS Col_PresentAddress2 " & _
                       "    ,ISNULL(pct.country_name,'') AS Col_PresentCountry " & _
                       "    ,ISNULL(pstat.name,'') AS Col_PresentState " & _
                       "    ,ISNULL(pcity.name,'') AS Col_PresentCity " & _
                       "    ,ISNULL(YOCP.YearsOnCurrentPosition,0) AS YearsOnCurrentPosition " & _
                       "    ,ISNULL(hrjob_master.desciription,'') AS Col_JobPurpose " & _
                       "    ,ISNULL(AsrJob,'') AS Col_AssessorJob " & _
                       "    ,ISNULL(RevJob,'') AS Col_ReviewerJob " & _
                       "    ,ISNULL(Col_AssessItem_Commit_Date,'') AS Col_AssessItem_Commit_Date " & _
                       "    ,ISNULL(Col_Employee_Assessment_Date,'') AS Col_Employee_Assessment_Date " & _
                       "    ,ISNULL(Col_Employee_Committed_Date,'') AS Col_Employee_Committed_Date " & _
                       "    ,ISNULL(Col_Assessor_Assessment_Date,'') AS Col_Assessor_Assessment_Date " & _
                       "    ,ISNULL(Col_Assessor_Committed_Date,'') AS Col_Assessor_Committed_Date " & _
                       "    ,ISNULL(Col_Reviewer_Assessment_Date,'') AS Col_Reviewer_Assessment_Date " & _
                       "    ,ISNULL(Col_Reviewer_Committed_Date,'') AS Col_Reviewer_Committed_Date " & _
                       "    ,ISNULL(Qulification.qualificationname,'') AS Col_Qualification " & _
                       "    ,ISNULL(Qulification.QStartDate,'') AS Col_Qualification_StartDate " & _
                       "    ,ISNULL(Qulification.QEndDate,'') AS Col_Qualification_EndDate " & _
                       "    ,ISNULL(Qulification.QStartYear,'') AS Col_Qualification_StartYear " & _
                       "    ,ISNULL(Qulification.QEndYear,'') AS Col_Qualification_EndYear " & _
                       "    ,ISNULL(Qulification.QLevel,'') AS Col_Qualification_GroupLevel " & _
                       "    ,ISNULL(OtherQulification.other_qualificationgrp,'') AS Col_Other_Qualification_Group " & _
                       "    ,ISNULL(OtherQulification.other_qualification,'') AS Col_Other_Qualification " & _
                       "    ,ISNULL(OtherQulification.other_resultcode,'') AS Col_Other_Qualification_ResultCode " & _
                       "    ,ISNULL(OtherQulification.QStartDate,'') AS Col_Other_Qualification_StartDate " & _
                       "    ,ISNULL(OtherQulification.QEndDate,'') AS Col_Other_Qualification_EndDate " & _
                       "    ,ISNULL(OtherQulification.QStartYear,'') AS Col_Other_Qualification_StartYear " & _
                       "    ,ISNULL(OtherQulification.QEndYear,'') AS Col_Other_Qualification_EndYear " & _
                       "    ,ISNULL(CGrades.incrementdate,'') AS Col_LastDateOfPromotion " & _
                       "    ,ISNULL(empTraning.Traning_Name,'') AS Col_Traning_Attended " & _
                       "    ,ISNULL(empTraning.enroll_date,'') AS Col_Traning_Enrollment_Date " & _
                       "    ,ISNULL(empTraning.start_date,'') AS Col_Traning_Start_Date " & _
                       "    ,ISNULL(empTraning.end_date,'') AS Col_Traning_End_Date " & _
                       "    ,eimg.photo AS Col_Employee_Image " & _
                       "    ,empsignature AS Col_Employee_Signature " & _
                       "    ,ISNULL(teffdate,'') AS Col_Transfer_EffectiveDate " & _
                       "    ,ISNULL(reffdate,'') AS Col_Recategorization_EffectiveDate " & _
                       "    ,ISNULL(ceffdate,'') AS Col_Costcenter_EffectiveDate " & _
                       "    ,'' AS Col_WorkPermit_EffectiveDate " & _
                       "    ,'' AS Col_Resident_EffectiveDate " & _
                       "FROM hremployee_master " & _
                       "    LEFT JOIN arutiimages..hremployee_images AS eimg ON eimg.employeeunkid = hremployee_master.employeeunkid AND eimg.companyunkid = hremployee_master.companyunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             stationunkid " & _
                       "            ,deptgroupunkid " & _
                       "            ,departmentunkid " & _
                       "            ,sectiongroupunkid " & _
                       "            ,sectionunkid " & _
                       "            ,unitgroupunkid " & _
                       "            ,unitunkid " & _
                       "            ,teamunkid " & _
                       "            ,classgroupunkid " & _
                       "            ,classunkid " & _
                       "            ,employeeunkid " & _
                       "            ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "        FROM hremployee_transfer_tran " & _
                       "        WHERE isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       "    LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid " & _
                       "    LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid " & _
                       "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                       "    LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid " & _
                       "    LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid " & _
                       "    LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid " & _
                       "    LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid " & _
                       "    LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid " & _
                       "    LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid " & _
                       "    LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             jobunkid " & _
                       "            ,jobgroupunkid " & _
                       "            ,employeeunkid " & _
                       "            ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "        FROM hremployee_categorization_tran " & _
                       "        WHERE isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                       "    LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                       "    LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = Jobs.jobgroupunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             cctranheadvalueid AS costcenterunkid " & _
                       "            ,employeeunkid " & _
                       "            ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "        FROM hremployee_cctranhead_tran " & _
                       "        WHERE istransactionhead = 0 AND isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' " & _
                       "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                       "    LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.costcenterunkid " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.employmenttypeunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "' " & _
                       "    LEFT JOIN cfcommon_master AS ttype ON ttype.masterunkid = hremployee_master.titleunkid AND ttype.mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "' " & _
                       "    LEFT JOIN hrmsConfiguration..cfcountry_master AS nct ON nct.countryunkid = hremployee_master.nationalityunkid " & _
                       "    LEFT JOIN cfcommon_master AS mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = '" & clsCommon_Master.enCommonMaster.MARRIED_STATUS & "' " & _
                       "    LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                       "    LEFT JOIN hrmsConfiguration..cfcity_master AS pcity ON pcity.cityunkid = hremployee_master.present_post_townunkid " & _
                       "    LEFT JOIN hrmsConfiguration..cfstate_master AS pstat ON pstat.stateunkid = hremployee_master.present_stateunkid " & _
                       "    LEFT JOIN hrmsConfiguration..cfcountry_master AS pct ON pct.countryunkid = hremployee_master.present_countryunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             E_Emp.employeeunkid AS employeeunkid " & _
                       "            ,ISNULL(R_Emp.employeecode, '') AS Reporting_Code " & _
                       "            ,ISNULL(R_Emp.firstname, '') + ' ' + ISNULL(R_Emp.othername, '') + ' ' + ISNULL(R_Emp.surname, '') AS Reporting_Name " & _
                       "            ,ISNULL(R_Emp.firstname, '') AS rfname " & _
                       "            ,ISNULL(R_Emp.othername, '') AS roname " & _
                       "            ,ISNULL(R_Emp.surname, '') AS rsname " & _
                       "            ,ISNULL(Jb.job_name, '') AS Reporting_Job " & _
                       "            ,CASE WHEN hremployee_reportto.ishierarchy = 1 THEN 1 ELSE 0 END AS Default_Reporting " & _
                       "            ,ROW_NUMBER() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                       "        FROM hremployee_reportto " & _
                       "            LEFT JOIN hremployee_master AS E_Emp ON hremployee_reportto.employeeunkid = E_Emp.employeeunkid " & _
                       "            LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid " & _
                       "            LEFT JOIN " & _
                       "            ( " & _
                       "                SELECT " & _
                       "                     jobunkid " & _
                       "                    ,jobgroupunkid " & _
                       "                    ,employeeunkid " & _
                       "                    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "                FROM hremployee_categorization_tran " & _
                       "                WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "            ) AS RJobs ON RJobs.employeeunkid = R_Emp.employeeunkid AND RJobs.rno = 1 " & _
                       "            LEFT JOIN hrjob_master AS Jb ON Jb.jobunkid = RJobs.jobunkid " & _
                       "        WHERE ishierarchy = 1 AND hremployee_reportto.isvoid = 0 " & _
                       "              AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    ) AS RepTable ON RepTable.employeeunkid = hremployee_master.employeeunkid AND RepTable.Rno = 1  " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                              "    	    	 SUSP.SDEmpId " & _
                              "    		    ,SUSP.suspended_from_date " & _
                              "    		    ,SUSP.suspended_to_date " & _
                              "    		    ,SUSP.SDEfDt " & _
                              "        FROM " & _
                              "        ( " & _
                              "            SELECT " & _
                              "                 SDT.employeeunkid AS SDEmpId " & _
                              "                ,CONVERT(CHAR(8),SDT.date1,112) AS suspended_from_date " & _
                              "                ,CONVERT(CHAR(8),SDT.date2,112) AS suspended_to_date " & _
                              "                ,CONVERT(CHAR(8),SDT.effectivedate,112) AS SDEfDt " & _
                              "                ,ROW_NUMBER()OVER(PARTITION BY SDT.employeeunkid ORDER BY SDT.effectivedate DESC) AS Rno " & _
                              "            FROM hremployee_dates_tran AS SDT " & _
                              "            WHERE isvoid = 0 AND SDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),SDT.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                              "        ) AS SUSP WHERE SUSP.Rno = 1 " & _
                              "    ) AS ESUSP ON ESUSP.SDEmpId = hremployee_master.employeeunkid AND ESUSP.SDEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                              "    LEFT JOIN " & _
                              "    ( " & _
                              "        SELECT " & _
                              "        		 PROB.PDEmpId " & _
                              "        		,PROB.probation_from_date " & _
                              "        		,PROB.probation_to_date " & _
                              "        		,PROB.PDEfDt " & _
                              "        FROM " & _
                              "        ( " & _
                              "            SELECT " & _
                              "                 PDT.employeeunkid AS PDEmpId " & _
                              "                ,CONVERT(CHAR(8),PDT.date1,112) AS probation_from_date " & _
                              "                ,CONVERT(CHAR(8),PDT.date2,112) AS probation_to_date " & _
                              "                ,CONVERT(CHAR(8),PDT.effectivedate,112) AS PDEfDt " & _
                              "                ,ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                              "            FROM hremployee_dates_tran AS PDT " & _
                              "            WHERE isvoid = 0 AND PDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                              "        ) AS PROB WHERE PROB.Rno = 1 " & _
                              "    ) AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid AND EPROB.PDEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                              "    LEFT JOIN " & _
                              "    ( " & _
                              "        SELECT " & _
                              "        		 TERM.TEEmpId " & _
                              "        		,TERM.empl_enddate " & _
                              "        		,TERM.termination_from_date " & _
                              "        		,TERM.TEfDt " & _
                              "        		,TERM.isexclude_payroll " & _
                              "        		,TERM.changereasonunkid " & _
                              "        FROM " & _
                              "        ( " & _
                              "            SELECT " & _
                              "                 TRM.employeeunkid AS TEEmpId " & _
                              "                ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                              "                ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                              "                ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                              "                ,TRM.isexclude_payroll " & _
                              "                ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                              "                ,TRM.changereasonunkid " & _
                              "            FROM hremployee_dates_tran AS TRM " & _
                              "            WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                              "        ) AS TERM WHERE TERM.Rno = 1 " & _
                              "    ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                              "    LEFT JOIN " & _
                              "    ( " & _
                              "        SELECT " & _
                              "        	 CNF.CEmpId " & _
                              "        	,CNF.confirmation_date " & _
                              "           ,CNF.CEfDt " & _
                              "        FROM " & _
                              "        ( " & _
                              "            SELECT " & _
                              "                 CNF.employeeunkid AS CEmpId " & _
                              "                ,CONVERT(CHAR(8),CNF.date1,112) AS confirmation_date " & _
                              "                ,CONVERT(CHAR(8),CNF.effectivedate,112) AS CEfDt " & _
                              "                ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                              "                ,CNF.changereasonunkid " & _
                              "            FROM hremployee_dates_tran AS CNF " & _
                              "            WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                              "        ) AS CNF WHERE CNF.Rno = 1 " & _
                              "    ) AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid AND ECNF.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                              "    LEFT JOIN " & _
                              "    ( " & _
                              "        SELECT " & _
                              "        	 RET.REmpId " & _
                              "        	,RET.termination_to_date " & _
                              "        	,RET.REfDt " & _
                              "        FROM " & _
                              "        ( " & _
                              "            SELECT " & _
                              "                 RTD.employeeunkid AS REmpId " & _
                              "                ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                              "                ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                              "                ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                              "            FROM hremployee_dates_tran AS RTD " & _
                              "            WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                              "        ) AS RET WHERE RET.Rno = 1 " & _
                              "    ) AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                              "    LEFT JOIN " & _
                              "    ( " & _
                              "        SELECT " & _
                              "             RH.RHEmpId " & _
                              "            ,RH.reinstatment_date " & _
                              "            ,RH.RHEfDt " & _
                              "            ,RH.changereasonunkid AS RH_changereasonunkid " & _
                              "        FROM " & _
                              "        ( " & _
                              "            SELECT " & _
                              "                 ERT.employeeunkid AS RHEmpId " & _
                              "                ,CONVERT(CHAR(8),ERT.reinstatment_date,112) AS reinstatment_date " & _
                              "                ,CONVERT(CHAR(8),ERT.effectivedate,112) AS RHEfDt " & _
                              "                ,ROW_NUMBER()OVER(PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC) AS Rno " & _
                              "                ,ERT.changereasonunkid " & _
                              "            FROM hremployee_rehire_tran AS ERT " & _
                              "            WHERE isvoid = 0 AND CONVERT(CHAR(8),ERT.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                              "        ) AS RH WHERE RH.Rno = 1 " & _
                              "    )AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid AND ERH.RHEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                              "    LEFT JOIN " & _
                              "    ( " & _
                              "        SELECT " & _
                       "             employeeunkid " & _
                       "            ,ISNULL(hrgradegroup_master.name,'') AS CurrentGradeGroup " & _
                       "            ,ISNULL(hrgrade_master.name,'') AS CurrentGrade " & _
                       "            ,ISNULL(hrgradelevel_master.name,'') AS CurrentGradeLevel " & _
                       "            ,newscale AS CurrentSalary " & _
                       "            ,CONVERT(CHAR(8),incrementdate,112) AS incrementdate " & _
                       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC) AS rno " & _
                       "        FROM prsalaryincrement_tran " & _
                       "            JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                       "            JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
                       "            JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                       "        WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND employeeunkid = '" & intEmployeeId & "' AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    )AS CGrades ON CGrades.employeeunkid = hremployee_master.employeeunkid AND CGrades.rno = 1 " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             employeeunkid " & _
                       "            ,ISNULL(hrgradegroup_master.name,'') AS NextGradeGroup " & _
                       "            ,ISNULL(hrgrade_master.name,'') AS NextGrade " & _
                       "            ,ISNULL(hrgradelevel_master.name,'') AS NextGradeLevel " & _
                       "            ,newscale AS NextSalary " & _
                       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY incrementdate ASC) AS rno " & _
                       "        FROM prsalaryincrement_tran " & _
                       "            JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                       "            JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
                       "            JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                       "        WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND employeeunkid = '" & intEmployeeId & "' AND CONVERT(CHAR(8),incrementdate,112) > '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    )AS NGrades ON NGrades.employeeunkid = hremployee_master.employeeunkid AND NGrades.rno = 1 " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             employeeunkid " & _
                       "            ,shiftname AS ShiftName " & _
                       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as rno " & _
                       "        FROM hremployee_shift_tran " & _
                       "            JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                       "        WHERE isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    )AS Sft ON Sft.employeeunkid = hremployee_master.employeeunkid AND Sft.rno = 1 " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             hrassessor_tran.employeeunkid " & _
                       "            ,employeecode AS ACode " & _
                       "            ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS AName " & _
                       "            ,ISNULL(hrjob_master.job_name,'') AS AsrJob " & _
                       "        FROM hrassessor_master " & _
                       "            JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                       "            LEFT JOIN " & _
                       "            ( " & _
                       "                SELECT " & _
                       "                     jobunkid " & _
                       "                    ,jobgroupunkid " & _
                       "                    ,employeeunkid " & _
                       "                    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "                FROM hremployee_categorization_tran " & _
                       "                WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "            ) AS AJobs ON AJobs.employeeunkid = hremployee_master.employeeunkid AND AJobs.rno = 1 " & _
                       "            LEFT JOIN hrjob_master ON hrjob_master.jobunkid = AJobs.jobunkid " & _
                       "            JOIN hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                       "        WHERE hrassessor_master.isvoid = 0 AND isreviewer = 0 AND hrassessor_tran.isvoid = 0 " & _
                       "    ) AS Assess ON Assess.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             hrassessor_tran.employeeunkid " & _
                       "            ,employeecode AS RCode " & _
                       "            ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS RName " & _
                       "            ,ISNULL(hrjob_master.job_name,'') AS RevJob " & _
                       "        FROM hrassessor_master " & _
                       "            JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                       "            LEFT JOIN " & _
                       "            ( " & _
                       "                SELECT " & _
                       "                     jobunkid " & _
                       "                    ,jobgroupunkid " & _
                       "                    ,employeeunkid " & _
                       "                    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "                FROM hremployee_categorization_tran " & _
                       "                WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "            ) AS RevJobs ON RevJobs.employeeunkid = hremployee_master.employeeunkid AND RevJobs.rno = 1 " & _
                       "            LEFT JOIN hrjob_master ON hrjob_master.jobunkid = RevJobs.jobunkid " & _
                       "            JOIN hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                       "        WHERE hrassessor_master.isvoid = 0 AND isreviewer = 1 AND hrassessor_tran.isvoid = 0 " & _
                       "    ) AS Review ON Review.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             DATEDIFF(YEAR,effectivedate,GETDATE()) AS YearsOnCurrentPosition " & _
                       "            ,employeeunkid AS EmpId " & _
                       "            ,jobunkid AS Job_ID " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "        FROM hremployee_categorization_tran " & _
                       "        WHERE employeeunkid = '" & intEmployeeId & "' " & _
                       "        AND isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmpAsOnDate) & "' " & _
                       "    ) AS YOCP ON YOCP.EmpId = hremployee_master.employeeunkid AND YOCP.rno = 1 " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         ISNULL(CONVERT(NVARCHAR(8),assessmentdate,112),'') AS Col_Employee_Assessment_Date " & _
                       "        ,ISNULL(CONVERT(NVARCHAR(8),committeddatetime,112),'') AS Col_Employee_Committed_Date " & _
                       "        ,Col_Assessor_Assessment_Date " & _
                       "        ,Col_Assessor_Committed_Date " & _
                       "        ,Col_Reviewer_Assessment_Date " & _
                       "        ,Col_Reviewer_Committed_Date " & _
                       "        ,selfemployeeunkid " & _
                       "    FROM hrevaluation_analysis_master " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ISNULL(CONVERT(NVARCHAR(8),assessmentdate,112),'') AS Col_Assessor_Assessment_Date " & _
                       "            ,ISNULL(CONVERT(NVARCHAR(8),committeddatetime,112),'') AS Col_Assessor_Committed_Date " & _
                       "            ,assessedemployeeunkid " & _
                       "        FROM hrevaluation_analysis_master WHERE assessmodeid = 2 AND isvoid = 0 " & _
                       "        AND assessedemployeeunkid = '" & intEmployeeId & "' AND periodunkid = '" & intPeriodId & "' " & _
                       "    ) AS Ar ON Ar.assessedemployeeunkid = selfemployeeunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ISNULL(CONVERT(NVARCHAR(8),assessmentdate,112),'') AS Col_Reviewer_Assessment_Date " & _
                       "            ,ISNULL(CONVERT(NVARCHAR(8),committeddatetime,112),'') AS Col_Reviewer_Committed_Date " & _
                       "            ,assessedemployeeunkid " & _
                       "        FROM hrevaluation_analysis_master WHERE assessmodeid = 3 AND isvoid = 0 " & _
                       "        AND assessedemployeeunkid = '" & intEmployeeId & "' AND periodunkid = '" & intPeriodId & "' " & _
                       "    ) AS Rr ON Ar.assessedemployeeunkid = selfemployeeunkid " & _
                       "    WHERE isvoid = 0 AND selfemployeeunkid = '" & intEmployeeId & "' AND periodunkid = '" & intPeriodId & "' " & _
                       ") AS EDt ON EDt.selfemployeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT TOP 1 " & _
                       "             ISNULL(CONVERT(NVARCHAR(8),status_date,112),'') AS Col_AssessItem_Commit_Date " & _
                       "            ,hrassess_empstatus_tran.employeeunkid " & _
                       "        FROM hrassess_empstatus_tran " & _
                       "        WHERE employeeunkid = '" & intEmployeeId & "' AND periodunkid = '" & intPeriodId & "' AND statustypeid = '" & enObjective_Status.FINAL_SAVE & "' " & _
                       "        ORDER BY status_date DESC " & _
                              "    ) AS Es ON Es.employeeunkid = hremployee_master.employeeunkid " & _
                              "LEFT JOIN ( " & _
                        "           SELECT " & _
                        "                hrqualification_master.qualificationname " & _
                        "               ,hrqualification_master.qualificationcode " & _
                        "               ,CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS QStartDate " & _
                        "               ,CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS QEndDate " & _
                        "               ,YEAR(hremp_qualification_tran.award_start_date) AS QStartYear " & _
                        "               ,YEAR(hremp_qualification_tran.award_end_date) AS QEndYear " & _
                        "               ,cfcommon_master.qlevel AS QLevel " & _
                        "               ,hremp_qualification_tran.employeeunkid " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY hremp_qualification_tran.employeeunkid ORDER BY cfcommon_master.qlevel DESC,ISNULL(hremp_qualification_tran.award_start_date,GETDATE()) DESC) AS rno " & _
                        "           FROM hremp_qualification_tran " & _
                        "               JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                        "               JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                        "           WHERE hremp_qualification_tran.isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' " & _
                        "               AND hremp_qualification_tran.qualificationgroupunkid <> 0 AND hremp_qualification_tran.qualificationunkid <> 0 " & _
                        "       ) AS Qulification ON Qulification.employeeunkid = hremployee_master.employeeunkid AND Qulification.rno = 1 " & _
                        "LEFT JOIN ( " & _
                        "           SELECT " & _
                        "                hremp_qualification_tran.other_qualificationgrp " & _
                        "               ,hremp_qualification_tran.other_qualification " & _
                        "               ,hremp_qualification_tran.other_resultcode " & _
                        "               ,CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS QStartDate " & _
                        "               ,CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS QEndDate " & _
                        "               ,YEAR(hremp_qualification_tran.award_start_date) AS QStartYear " & _
                        "               ,YEAR(hremp_qualification_tran.award_end_date) AS QEndYear " & _
                        "               ,hremp_qualification_tran.employeeunkid " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY hremp_qualification_tran.employeeunkid ORDER BY hremp_qualification_tran.transaction_date DESC) AS rno " & _
                        "           FROM hremp_qualification_tran " & _
                        "           WHERE hremp_qualification_tran.isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' " & _
                        "               AND hremp_qualification_tran.qualificationgroupunkid = 0 AND hremp_qualification_tran.qualificationunkid = 0 " & _
                        "       ) AS OtherQulification ON OtherQulification.employeeunkid = hremployee_master.employeeunkid AND OtherQulification.rno = 1 " & _
                        "LEFT JOIN " & _
                        "   (" & _
                        "       SELECT TOP 1 " & _
                        "            cfcommon_master.name AS Traning_Name " & _
                        "           ,CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112) AS enroll_date " & _
                        "           ,CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS start_date " & _
                        "           ,CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS end_date " & _
                        "           ,hrtraining_enrollment_tran.employeeunkid " & _
                        "       FROM hrtraining_enrollment_tran " & _
                        "           JOIN hrtraining_scheduling ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
                        "           JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND cfcommon_master.mastertype = 30 " & _
                        "       WHERE hrtraining_enrollment_tran.isvoid = 0 AND hrtraining_enrollment_tran.status_id = 3 " & _
                        "               AND cfcommon_master.coursetypeid = 1 AND hrtraining_enrollment_tran.employeeunkid = '" & intEmployeeId & "' " & _
                        "       ORDER BY hrtraining_enrollment_tran.enroll_date DESC " & _
                        "   ) AS empTraning ON empTraning.employeeunkid = hremployee_master.employeeunkid " & _
                       "WHERE 1 = 1 AND hremployee_master.employeeunkid = '" & intEmployeeId & "'"

                objDo.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDo.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetEvaluatedCSV_AssessGroupIds(ByVal iEmployeeId As Integer, ByVal intPeriodId As Integer) As String
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim strGroupIds As String = "0"
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT ISNULL(STUFF( " & _
                       "(SELECT  DISTINCT ',' +  CAST(CT.assessgroupunkid AS nvarchar(max)) " & _
                       "FROM hrcompetency_analysis_tran AS CT " & _
                       "JOIN hrevaluation_analysis_master AS EM ON CT.analysisunkid = EM.analysisunkid " & _
                       "WHERE CT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = @PrdId AND (EM.selfemployeeunkid = @EmpId OR EM.assessedemployeeunkid = @EmpId) " & _
                       "FOR XML PATH('')),1,1,''),'0') AS CSV "

                objDo.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
                objDo.AddParameter("@PrdId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                strGroupIds = dsList.Tables(0).Rows(0).Item("CSV").ToString.Trim

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEvaluatedCSV_AssessGroupIds; Module Name: " & mstrModuleName)
        End Try
        Return strGroupIds
    End Function

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsResult As New DataSet
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Try
            '******************************** EMPLOYEE ASSESSOR/REVIEWER ********************************* START
            Dim iEAssessorId, iEReviewerId As Integer
            Dim iEAssessorName, iEReviewerName As String

            Dim iEAssessorCode, iEReviewerCode As String
            iEAssessorCode = "" : iEReviewerCode = ""
            Dim iEAssessorJob, iEReviewerJob As String

            iEAssessorJob = "" : iEReviewerJob = ""
            iEAssessorId = 0 : iEReviewerId = 0 : iEAssessorName = "" : iEReviewerName = ""

            dsResult = GetEmployee_AssessorReviewerDetails(mintEmployeeId, mintPeriodId, strDatabaseName, dtPeriodEnd)


            If dsResult.Tables(0).Rows.Count > 0 Then
                Dim dtmp() As DataRow = Nothing
                If iEAssessorId <= 0 Then
                    dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        iEAssessorId = dtmp(0).Item("assessormasterunkid")
                        iEAssessorName = dtmp(0).Item("arName")
                        iEAssessorCode = dtmp(0).Item("arCode")
                        iEAssessorJob = dtmp(0).Item("AR_Job")
                    Else
                        dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                        If dtmp.Length > 0 Then
                            iEAssessorId = dtmp(0).Item("assessormasterunkid")
                            iEAssessorName = dtmp(0).Item("arName")
                            iEAssessorCode = dtmp(0).Item("arCode")
                            iEAssessorJob = dtmp(0).Item("AR_Job")
                        End If
                    End If
                End If
                If iEReviewerId <= 0 Then
                    dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        iEReviewerId = dtmp(0).Item("assessormasterunkid")
                        iEReviewerName = dtmp(0).Item("arName")
                        iEReviewerCode = dtmp(0).Item("arCode")
                        iEReviewerJob = dtmp(0).Item("AR_Job")
                    Else
                        dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1")

                        If dtmp.Length > 0 Then
                            iEReviewerId = dtmp(0).Item("assessormasterunkid")
                            iEReviewerName = dtmp(0).Item("arName")
                            iEReviewerCode = dtmp(0).Item("arCode")
                            iEReviewerJob = dtmp(0).Item("AR_Job")
                        End If
                    End If
                End If
            End If
            '******************************** EMPLOYEE ASSESSOR/REVIEWER ********************************* END

            '******************************** COMPANY DATA *************************** STRAT
            If mintCompanyUnkid <= 0 Then mintCompanyUnkid = Company._Object._Companyunkid
            Dim StrCompanyName As String = ""
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = mintCompanyUnkid
            StrCompanyName = objCompany._Name
            objCompany = Nothing
            '******************************** COMPANY DATA *************************** END

            '******************************** EMPLOYEE DATA *************************** STRAT
            Dim xEmpName, xEmpJob, xZone, xBranch As String
            xEmpName = "" : xEmpJob = "" : xZone = "" : xBranch = ""
            dsList = GetEmployeeDetails(mintEmployeeId, dtPeriodEnd, mintPeriodId)
            If dsList.Tables("List").Rows.Count > 0 Then
                xEmpName = dsList.Tables(0).Rows(0)("Col_EmpFirstName") & " " & dsList.Tables(0).Rows(0)("Col_EmpSurName")
                xEmpJob = dsList.Tables(0).Rows(0)("Col_Job")
                xZone = dsList.Tables(0).Rows(0)("Col_ClassGroup")
                xBranch = dsList.Tables(0).Rows(0)("Col_Class")
            End If
            '******************************** EMPLOYEE DATA *************************** END

            '******************************** COMPETENCIES DATA *************************** START
            Dim iStrGroupIds As String = String.Empty
            iStrGroupIds = GetEvaluatedCSV_AssessGroupIds(mintEmployeeId, mintPeriodId)
            If iStrGroupIds.Trim = "0" Then
                Dim objAssessGrp As New clsassess_group_master
                iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(mintEmployeeId, dtPeriodEnd, mintPeriodId)
                If iStrGroupIds.Trim.Length <= 0 Then iStrGroupIds = "0"
                objAssessGrp = Nothing
            End If
            Dim xCSVCompetenciesIds As String = String.Empty

            dsList = GetCompetenciesData(iStrGroupIds, mblnIsSelfAssignCompetencies, mintEmployeeId, mintPeriodId)
            dsResult = GetEmployeeCompetencyResult(mintEmployeeId, mintPeriodId, iStrGroupIds, iEAssessorId, iEReviewerId, False, mblnIsUsedAgreedScore)
            For Each xRow As DataRow In dsList.Tables("List").Rows
                Dim row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                row("Column1") = xRow.Item("Col_CompetencyCategory_Value")
                row("Column2") = xRow.Item("Col_Competencies_Value")
                Dim dtmp As DataRow() = Nothing
                If IIf(IsDBNull(xRow.Item("competenciesunkid")), 0, xRow.Item("competenciesunkid")) > 0 Then
                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND competenciesunkid = '" & xRow.Item("competenciesunkid") & "'")
                    If dtmp.Length > 0 Then
                        row("Column3") = dtmp(0).Item("result")
                        row("Column5") = dtmp(0).Item("remark")
                    End If
                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND competenciesunkid = '" & xRow.Item("competenciesunkid") & "'")
                    If dtmp.Length > 0 Then
                        row("Column4") = dtmp(0).Item("result")
                        row("Column6") = dtmp(0).Item("remark")
                    End If
                End If

                xCSVCompetenciesIds &= "," & xRow.Item("competenciesunkid")
                rpt_Data.Tables("ArutiTable").Rows.Add(row)
            Next
            '******************************** COMPETENCIES DATA *************************** END

            '******************************** COMPETENCIES ASSESSMENT SCALE DATA *************************** START
            If xCSVCompetenciesIds.Trim.Length > 0 Then xCSVCompetenciesIds = Mid(xCSVCompetenciesIds, 2)

            dsList = GetCompetencyScaleData(mintPeriodId, xCSVCompetenciesIds)
            Dim strContent As New System.Text.StringBuilder
            If dsList.Tables(0).Rows.Count > 0 Then
                'strContent.Append("<HTML>" & vbCrLf)
                'strContent.Append("<BODY>" & vbCrLf)
                'strContent.Append("<TABLE  width='60%'>" & vbCrLf)
                'strContent.Append("<TR  width='100%'>" & vbCrLf)
                'strContent.Append("<TD COLSPAN='2'>" & Language.getMessage(mstrModuleName, 115, "Key") & "</TD>" & vbCrLf)
                'strContent.Append("</TR>" & vbCrLf)
                For Each xRow As DataRow In dsList.Tables("List").Rows
                    'strContent.Append("<TR width='100%'>" & vbCrLf)
                    'strContent.Append("<TD width='10%'>" & xRow.Item("Col_Scale_Value") & "</TD>" & vbCrLf)
                    'strContent.Append("<TD width='50%'>" & xRow.Item("Col_Description_Value") & "</TD>" & vbCrLf)
                    'strContent.Append("</TR>" & vbCrLf)
                    strContent.Append(xRow.Item("Col_Scale_Value") & " - " & xRow.Item("Col_Description_Value") & vbCrLf)
                Next
                'strContent.Append("</TABLE>" & vbCrLf)
                'strContent.Append("</BODY>" & vbCrLf)
                'strContent.Append("</HTML>" & vbCrLf)
                If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                    For Each row In rpt_Data.Tables("ArutiTable").Rows
                        row("Column10") = strContent.ToString()
                    Next
                End If
            End If
            '******************************** COMPETENCIES ASSESSMENT SCALE DATA *************************** END

            objRpt = New ArutiReport.Designer.rptCompetencyFormReport

            Call ReportFunction.TextChange(objRpt, "txtCompany", StrCompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 100, "Competencies Assessment Form"))

            Call ReportFunction.TextChange(objRpt, "lblPeriod", Language.getMessage(mstrModuleName, 101, "PERIOD NAME"))
            Call ReportFunction.TextChange(objRpt, "lblEmployee", Language.getMessage(mstrModuleName, 102, "NAME OF STAFF"))
            Call ReportFunction.TextChange(objRpt, "lblJob", Language.getMessage(mstrModuleName, 103, "JOB TITLE"))
            Call ReportFunction.TextChange(objRpt, "lblZone", Language.getMessage(mstrModuleName, 104, "ZONE NAME"))
            Call ReportFunction.TextChange(objRpt, "lblBranch", Language.getMessage(mstrModuleName, 105, "BRANCH NAME"))
            Call ReportFunction.TextChange(objRpt, "lblAssessor", Language.getMessage(mstrModuleName, 106, "NAME OF ASSESSOR"))
            Call ReportFunction.TextChange(objRpt, "lblAssessorJob", Language.getMessage(mstrModuleName, 107, "ASSESSOR'S JOB TITLE"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt, "txtEmployee", xEmpName)
            Call ReportFunction.TextChange(objRpt, "txtJob", xEmpJob)
            Call ReportFunction.TextChange(objRpt, "txtZone", xZone)
            Call ReportFunction.TextChange(objRpt, "txtBranch", xBranch)
            Call ReportFunction.TextChange(objRpt, "txtAssessor", iEAssessorName)
            Call ReportFunction.TextChange(objRpt, "txtAssessorJob", iEAssessorJob)

            Call ReportFunction.TextChange(objRpt, "txtCategory", Language.getMessage(mstrModuleName, 108, "COMPETENCY CATEGORY"))
            Call ReportFunction.TextChange(objRpt, "txtCompetencies", Language.getMessage(mstrModuleName, 109, "COMPETENCE"))
            Call ReportFunction.TextChange(objRpt, "txtSelfRating", Language.getMessage(mstrModuleName, 110, "SELF RATING"))
            Call ReportFunction.TextChange(objRpt, "txtAsrRating", Language.getMessage(mstrModuleName, 111, "SUPERVISOR RATING"))
            Call ReportFunction.TextChange(objRpt, "txtSelfCmt", Language.getMessage(mstrModuleName, 112, "COMMENTS (SELF)"))
            Call ReportFunction.TextChange(objRpt, "txtAsrCmt", Language.getMessage(mstrModuleName, 113, "COMMENTS (SUPERVISOR)"))
            Call ReportFunction.TextChange(objRpt, "txtAvg", Language.getMessage(mstrModuleName, 114, "Average"))

            If mintScoringOptionId = enScoringOption.SC_WEIGHTED_BASED Then
                ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            ElseIf mintScoringOptionId = enScoringOption.SC_SCALE_BASED Then
                If strContent.ToString.Trim.Length <= 0 Then
                    ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
                End If
            End If

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")
            Language.setMessage(mstrModuleName, 1, "Scale Group")
            Language.setMessage(mstrModuleName, 2, "Scale")
            Language.setMessage(mstrModuleName, 3, "Description")
            Language.setMessage(mstrModuleName, 100, "Competencies Assessment Form")
            Language.setMessage(mstrModuleName, 101, "PERIOD NAME")
            Language.setMessage(mstrModuleName, 102, "NAME OF STAFF")
            Language.setMessage(mstrModuleName, 103, "JOB TITLE")
            Language.setMessage(mstrModuleName, 104, "ZONE NAME")
            Language.setMessage(mstrModuleName, 105, "BRANCH NAME")
            Language.setMessage(mstrModuleName, 106, "NAME OF ASSESSOR")
            Language.setMessage(mstrModuleName, 107, "ASSESSOR'S JOB TITLE")
            Language.setMessage(mstrModuleName, 108, "COMPETENCY CATEGORY")
            Language.setMessage(mstrModuleName, 109, "COMPETENCE")
            Language.setMessage(mstrModuleName, 110, "SELF RATING")
            Language.setMessage(mstrModuleName, 111, "SUPERVISOR RATING")
            Language.setMessage(mstrModuleName, 112, "COMMENTS (SELF)")
            Language.setMessage(mstrModuleName, 113, "COMMENTS (SUPERVISOR)")
            Language.setMessage(mstrModuleName, 114, "Average")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
