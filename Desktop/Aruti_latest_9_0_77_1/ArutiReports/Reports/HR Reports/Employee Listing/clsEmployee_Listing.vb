#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

#End Region

Public Class clsEmployee_Listing
    Inherits IReportData

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Listing"
    Private mstrReportId As String = enArutiReport.Employee_Listing
    Dim objDataOperation As clsDataOperation
    Dim dtTable As DataTable
    Dim dtRow As DataRow = Nothing
    Dim dsMemData As New DataSet : Dim dsBnkData As New DataSet
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

    'S.SANDEEP |06-Jan-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-554
#Region " Enums "

    Public Enum NameFormat
        NF_SEPARATELY = 1
        NF_USERDEFINE = 2
    End Enum

    'Hemant (15 Sep 2023) -- Start
    'ISSUE/ENHANCEMENT(Toyota): A1X-1306 - Employee listing report modification to allow user to choose applicable fields
    Public Enum enReport_Column
        Employee_Code = 1
        Employee_Name = 2
        Branch = 3
        Department_Group = 4
        Department = 5
        Section_Group = 6
        Section = 7
        Unit_Group = 8
        Unit = 9
        Team = 10
        Class_Group = 11
        Classes = 12
        Job_Group = 13
        Job = 14
        Cost_Center = 15
        Grade_Group = 16
        Grade = 17
        Grade_Level = 18
        Increment = 19
        Employment_Type = 20
        Date_Hired = 21
        Birth_Date = 22
        Age = 23
        Gender = 24
        Marital_Status = 25
        Religion = 26
        Telephone = 27
        Present_Address = 28
        Email = 29
        Emergency_Contact = 30
        Bank = 31
        Membership = 32
        Anniversary_Date = 33
        First_Appointment_Date = 34
        Confirmation_Date = 35
        Reinstatement_Date = 36
        Probation_Start_Date = 37
        Probation_End_Date = 38
        Suspended_From_Date = 39
        Suspended_To_Date = 40
        End_Of_Contract = 41
        EOC_Leaving_Reason = 42
        Qualification = 43
        Identity = 44
        Experience = 45
    End Enum
    'Hemant (15 Sep 2023) -- End

#End Region
    'S.SANDEEP |06-Jan-2023| -- END

#Region " Private variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintEmployeementTypeId As Integer = -1
    Private mstrEmployeementType As String = String.Empty
    Private mintBranchId As Integer = -1
    Private mstrBranch As String = String.Empty
    Private mintDepartmentId As Integer = -1
    Private mstrDepartment As String = String.Empty
    Private mintJobId As Integer = -1
    Private mstrJob As String = String.Empty
    Private mintCostCenterId As Integer = -1
    Private mstrCostCenter As String = String.Empty
    Private StrFinalPath As String = String.Empty
    Private mblnIsActive As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private dtData As DataTable = Nothing
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Private mstrAdvance_Filter As String = String.Empty
    Private mdtBirthDateFrom As Date
    Private mdtBirthDateTo As Date
    Private mdtAnniversaryDateFrom As Date
    Private mdtAnniversaryDateTo As Date
    Private mdtFirstAppointedDateFrom As Date
    Private mdtFirstAppointedDateTo As Date
    Private mdtAppointedDateFrom As Date
    Private mdtAppointedDateTo As Date
    Private mdtConfirmationDateFrom As Date
    Private mdtConfirmationDateTo As Date
    Private mdtProbationStartDateFrom As Date
    Private mdtProbationStartDateTo As Date
    Private mdtProbationEndDateFrom As Date
    Private mdtProbationEndDateTo As Date
    Private mdtSuspendedStartDateFrom As Date
    Private mdtSuspendedStartDateTo As Date
    Private mdtSuspendedEndDateFrom As Date
    Private mdtSuspendedEndDateTo As Date
    Private mdtReinstatementDateFrom As Date
    Private mdtReinstatementDateTo As Date
    Private mdtEOCDateFrom As Date
    Private mdtEOCDateTo As Date

    Private mblnShowEmpScale As Boolean = False

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

    'S.SANDEEP |15-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    'S.SANDEEP |15-JUL-2019| -- END

    'S.SANDEEP |06-Jan-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-554
    Private mintNameFormatId As Integer = 0
    Private mstrNameFormatValue As String = ""
    'S.SANDEEP |06-Jan-2023| -- END
    'Hemant (15 Sep 2023) -- Start
    'ISSUE/ENHANCEMENT(Toyota): A1X-1306 - Employee listing report modification to allow user to choose applicable fields
    Private mstrHideColumnIds As String = ""
    'Hemant (15 Sep 2023) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeementTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeementTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeementType() As String
        Set(ByVal value As String)
            mstrEmployeementType = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch() As String
        Set(ByVal value As String)
            mstrBranch = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentId = value
        End Set
    End Property

    Public WriteOnly Property _Department() As String
        Set(ByVal value As String)
            mstrDepartment = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJob = value
        End Set
    End Property

    Public WriteOnly Property _CostCenterId() As Integer
        Set(ByVal value As Integer)
            mintCostCenterId = value
        End Set
    End Property

    Public WriteOnly Property _CostCenter() As String
        Set(ByVal value As String)
            mstrCostCenter = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public ReadOnly Property _dtData() As DataTable
        Get
            Return dtData
        End Get
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Pinkal (09-Jan-2013) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _BirthDateFrom() As Date
        Set(ByVal value As Date)
            mdtBirthDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _BirthDateTo() As Date
        Set(ByVal value As Date)
            mdtBirthDateTo = value
        End Set
    End Property

    Public WriteOnly Property _AnniversaryDateFrom() As Date
        Set(ByVal value As Date)
            mdtAnniversaryDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AnniversaryDateTo() As Date
        Set(ByVal value As Date)
            mdtAnniversaryDateTo = value
        End Set
    End Property

    Public WriteOnly Property _FirstAppointedDateFrom() As Date
        Set(ByVal value As Date)
            mdtFirstAppointedDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _FirstAppointedDateTo() As Date
        Set(ByVal value As Date)
            mdtFirstAppointedDateTo = value
        End Set
    End Property

    Public WriteOnly Property _AppointedDateFrom() As Date
        Set(ByVal value As Date)
            mdtAppointedDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AppointedDateTo() As Date
        Set(ByVal value As Date)
            mdtAppointedDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ConfirmationDateFrom() As Date
        Set(ByVal value As Date)
            mdtConfirmationDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ConfirmationDateTo() As Date
        Set(ByVal value As Date)
            mdtConfirmationDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ProbationStartDateFrom() As Date
        Set(ByVal value As Date)
            mdtProbationStartDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ProbationStartDateTo() As Date
        Set(ByVal value As Date)
            mdtProbationStartDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ProbationEndDateFrom() As Date
        Set(ByVal value As Date)
            mdtProbationEndDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ProbationEndDateTo() As Date
        Set(ByVal value As Date)
            mdtProbationEndDateTo = value
        End Set
    End Property

    Public WriteOnly Property _SuspendedStartDateFrom() As Date
        Set(ByVal value As Date)
            mdtSuspendedStartDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _SuspendedStartDateTo() As Date
        Set(ByVal value As Date)
            mdtSuspendedStartDateTo = value
        End Set
    End Property

    Public WriteOnly Property _SuspendedEndDateFrom() As Date
        Set(ByVal value As Date)
            mdtSuspendedEndDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _SuspendedEndDateTo() As Date
        Set(ByVal value As Date)
            mdtSuspendedEndDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ReinstatementDateFrom() As Date
        Set(ByVal value As Date)
            mdtReinstatementDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ReinstatementDateTo() As Date
        Set(ByVal value As Date)
            mdtReinstatementDateTo = value
        End Set
    End Property

    Public WriteOnly Property _EOCDateFrom() As Date
        Set(ByVal value As Date)
            mdtEOCDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _EOCDateTo() As Date
        Set(ByVal value As Date)
            mdtEOCDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

    'S.SANDEEP |15-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property
    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property
    'S.SANDEEP |15-JUL-2019| -- END

    'S.SANDEEP |06-Jan-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-554
    Public WriteOnly Property _NameFormatId() As Integer
        Set(ByVal value As Integer)
            mintNameFormatId = value
        End Set
    End Property

    Public WriteOnly Property _NameFormatValue() As String
        Set(ByVal value As String)
            mstrNameFormatValue = value
        End Set
    End Property
    'S.SANDEEP |06-Jan-2023| -- END

    'Hemant (15 Sep 2023) -- Start
    'ISSUE/ENHANCEMENT(Toyota): A1X-1306 - Employee listing report modification to allow user to choose applicable fields
    Public WriteOnly Property _Hide_ColumnsIds() As String
        Set(ByVal value As String)
            mstrHideColumnIds = value
        End Set
    End Property
    'Hemant (15 Sep 2023) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintEmployeementTypeId = -1
            mstrEmployeementType = ""
            mintBranchId = -1
            mstrBranch = ""
            mintDepartmentId = -1
            mstrDepartment = ""
            mintJobId = -1
            mstrJob = ""
            mintCostCenterId = -1
            mstrCostCenter = ""
            mblnIsActive = True
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""

            mdtBirthDateFrom = Nothing
            mdtBirthDateTo = Nothing

            mdtAnniversaryDateFrom = Nothing
            mdtAnniversaryDateTo = Nothing

            mdtFirstAppointedDateFrom = Nothing
            mdtFirstAppointedDateTo = Nothing

            mdtAppointedDateFrom = Nothing
            mdtAppointedDateTo = Nothing

            mdtConfirmationDateFrom = Nothing
            mdtConfirmationDateTo = Nothing

            mdtProbationStartDateFrom = Nothing
            mdtProbationStartDateTo = Nothing

            mdtProbationEndDateFrom = Nothing
            mdtProbationEndDateTo = Nothing

            mdtSuspendedStartDateFrom = Nothing
            mdtSuspendedStartDateTo = Nothing

            mdtSuspendedEndDateFrom = Nothing
            mdtSuspendedEndDateTo = Nothing

            mdtReinstatementDateFrom = Nothing
            mdtReinstatementDateTo = Nothing

            mdtEOCDateFrom = Nothing
            mdtEOCDateTo = Nothing

            mblnShowEmpScale = False


            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            'S.SANDEEP |15-JUL-2019| -- END

            'S.SANDEEP |06-Jan-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-554
            mintNameFormatId = 0
            mstrNameFormatValue = ""
            'S.SANDEEP |06-Jan-2023| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintEmployeementTypeId > 0 Then
                objDataOperation.AddParameter("@emptypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeementTypeId)
                Me._FilterQuery &= " AND ET.masterunkid = @emptypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employeement Type : ") & " " & mstrEmployeementType & " "
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@branchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
                Me._FilterQuery &= " AND SM.stationunkid = @branchId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Branch : ") & " " & mstrBranch & " "
            End If

            If mintDepartmentId > 0 Then
                objDataOperation.AddParameter("@departmentId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
                Me._FilterQuery &= " AND DM.departmentunkid = @departmentId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Department : ") & " " & mstrDepartment & " "
            End If

            If mintJobId > 0 Then
                objDataOperation.AddParameter("@jobid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterQuery &= " AND JM.jobunkid = @jobid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Job : ") & " " & mstrJob & " "
            End If

            If mintCostCenterId > 0 Then
                objDataOperation.AddParameter("@costcenterid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterId)
                Me._FilterQuery &= " AND CCM.costcenterunkid = @costcenterid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Cost Center : ") & " " & mstrCostCenter & " "
            End If

            If mdtBirthDateFrom <> Nothing Then
                objDataOperation.AddParameter("@BirthdateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtBirthDateFrom))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.birthdate,112) >= @BirthdateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "BirthDate From : ") & " " & mdtBirthDateFrom.Date & " "
            End If

            If mdtBirthDateTo <> Nothing Then
                objDataOperation.AddParameter("@BirthdateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtBirthDateTo))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.birthdate,112) <= @BirthdateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "BirthDate To : ") & " " & mdtBirthDateTo.Date & " "
            End If

            If mdtAnniversaryDateFrom <> Nothing Then
                objDataOperation.AddParameter("@AnniversaryDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAnniversaryDateFrom))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.anniversary_date,112) >= @AnniversaryDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Anniversary Date From : ") & " " & mdtAnniversaryDateFrom.Date & " "
            End If

            If mdtAnniversaryDateTo <> Nothing Then
                objDataOperation.AddParameter("@AnniversaryDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAnniversaryDateTo))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.anniversary_date,112) <= @AnniversaryDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Anniversary Date To : ") & " " & mdtAnniversaryDateTo.Date & " "
            End If

            If ConfigParameter._Object._ShowFirstAppointmentDate Then

                If mdtFirstAppointedDateFrom <> Nothing Then
                    objDataOperation.AddParameter("@FirstAppointedDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFirstAppointedDateFrom))
                    Me._FilterQuery &= " AND Convert(char(8),hremployee_master.firstappointment_date,112) >= @FirstAppointedDateFrom "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 36, "First Appointment Date From : ") & " " & mdtFirstAppointedDateFrom.Date & " "
                End If

                If mdtFirstAppointedDateTo <> Nothing Then
                    objDataOperation.AddParameter("@FirstAppointedDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFirstAppointedDateTo))
                    Me._FilterQuery &= " AND Convert(char(8),hremployee_master.firstappointment_date,112) <= @FirstAppointedDateTo "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "First Appointment Date To : ") & " " & mdtFirstAppointedDateTo.Date & " "
                End If

            End If


            If mdtAppointedDateFrom <> Nothing Then
                objDataOperation.AddParameter("@AppointedDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAppointedDateFrom))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.appointeddate,112) >= @AppointedDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Appointment Date From : ") & " " & mdtAppointedDateFrom.Date & " "
            End If

            If mdtAppointedDateTo <> Nothing Then
                objDataOperation.AddParameter("@AppointedDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAppointedDateTo))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.appointeddate,112) <= @AppointedDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Appointment Date To : ") & " " & mdtAppointedDateTo.Date & " "
            End If

            If mdtConfirmationDateFrom <> Nothing Then
                objDataOperation.AddParameter("@ConfirmationDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtConfirmationDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), CnfDt.date1,112) >= @ConfirmationDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Confirmation Date From : ") & " " & mdtConfirmationDateFrom.Date & " "
            End If

            If mdtConfirmationDateTo <> Nothing Then
                objDataOperation.AddParameter("@ConfirmationDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtConfirmationDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), CnfDt.date1,112) <= @ConfirmationDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Confirmation Date To : ") & " " & mdtConfirmationDateTo.Date & " "
            End If

            If mdtProbationStartDateFrom <> Nothing Then
                objDataOperation.AddParameter("@ProbationStartDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationStartDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), PrbDt.date1,112) >= @ProbationStartDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Probation Start Date From : ") & " " & mdtProbationStartDateFrom.Date & " "
            End If

            If mdtProbationStartDateTo <> Nothing Then
                objDataOperation.AddParameter("@ProbationStartDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationStartDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), PrbDt.date1,112) <= @ProbationStartDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Probation Start Date To : ") & " " & mdtProbationStartDateTo.Date & " "
            End If

            If mdtProbationEndDateFrom <> Nothing Then
                objDataOperation.AddParameter("@ProbationEndDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationEndDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), PrbDt.date2,112) >= @ProbationEndDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Probation End Date From : ") & " " & mdtProbationEndDateFrom.Date & " "
            End If

            If mdtProbationEndDateTo <> Nothing Then
                objDataOperation.AddParameter("@ProbationEndDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationEndDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), PrbDt.date2,112) <= @ProbationEndDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Probation End Date To : ") & " " & mdtProbationEndDateTo.Date & " "
            End If

            If mdtSuspendedStartDateFrom <> Nothing Then
                objDataOperation.AddParameter("@SuspendedStartDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedStartDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), SuspDt.date1,112) >= @SuspendedStartDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Suspended Start Date From : ") & " " & mdtSuspendedStartDateFrom.Date & " "
            End If

            If mdtSuspendedStartDateTo <> Nothing Then
                objDataOperation.AddParameter("@SuspendedStartDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedStartDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), SuspDt.date1,112) <= @SuspendedStartDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Suspended Start Date To : ") & " " & mdtSuspendedStartDateTo.Date & " "
            End If

            If mdtSuspendedEndDateFrom <> Nothing Then
                objDataOperation.AddParameter("@SuspendedEndDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedEndDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), SuspDt.date2,112) >= @SuspendedEndDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Suspended End Date From : ") & " " & mdtSuspendedEndDateFrom.Date & " "
            End If

            If mdtSuspendedEndDateTo <> Nothing Then
                objDataOperation.AddParameter("@SuspendedEndDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedEndDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), SuspDt.date2,112) <= @SuspendedEndDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Suspended End Date To : ") & " " & mdtSuspendedEndDateTo.Date & " "
            End If

            If mdtReinstatementDateFrom <> Nothing Then
                objDataOperation.AddParameter("@ReinstatementDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtReinstatementDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), ReInstDt.reinstatment_date,112) >= @ReinstatementDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Reinstatement Date From : ") & " " & mdtReinstatementDateFrom.Date & " "
            End If

            If mdtReinstatementDateTo <> Nothing Then
                objDataOperation.AddParameter("@ReinstatementDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtReinstatementDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), ReInstDt.reinstatment_date,112) <= @ReinstatementDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Reinstatement Date To : ") & " " & mdtReinstatementDateTo.Date & " "
            End If

            If mdtEOCDateFrom <> Nothing Then
                objDataOperation.AddParameter("@EOCDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEOCDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), EocDt.date1,112) >= @EOCDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "End of Contract Date From : ") & " " & mdtEOCDateFrom.Date & " "
            End If

            If mdtEOCDateTo <> Nothing Then
                objDataOperation.AddParameter("@EOCDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEOCDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8), EocDt.date1,112) <= @EOCDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "End of Contract Date To : ") & " " & mdtEOCDateTo.Date & " "
            End If

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 506, "Period : ") & " " & mstrPeriodName & " "
            End If
            'S.SANDEEP |15-JUL-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    If Not IsNothing(objRpt) Then

        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell

        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intGroupColumn As Integer = 0

        '            If mintViewIndex > 0 Then
        '            Else
        '            End If

        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 33, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 34, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, , True)
        '    End If


        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                If mdtTableExcel IsNot Nothing Then

                    Dim intGroupColumn As Integer = 0

                    If mintViewIndex > 0 Then
                    Else
                    End If

                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------


                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 33, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 34, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, , True)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder ' 
        Dim blnFlag As Boolean = False
        Try

            strBuilder.Append(" <TITLE> " & Me._ReportName & " " & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 11, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)

            strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - 4 & " align='center' > " & vbCrLf)

            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)

            strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - 4 & "  align='center' > " & vbCrLf)

            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & " " & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Report Column Caption
            For j As Integer = 0 To objDataReader.Columns.Count - 2
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 2
                    If CBool(objDataReader.Rows(i)("IsGrp")) = True Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B> &nbsp;" & objDataReader.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    'S.SANDEEP |06-Jan-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-554
    Public Shared Function ValidNameFormatValue(ByVal strNameFormatValue As String) As String
        Dim StrMsg As String = ""
        Try
            If strNameFormatValue.Trim.Length > 0 Then
                Dim iNames() As String = strNameFormatValue.ToLower().Split("#")
                If iNames.AsEnumerable().Where(Function(x) x.Contains("fname")).Count > 1 Then
                    StrMsg = Language.getMessage(mstrModuleName, 515, "Sorry, you have added firstname <#fname#> tag more than once.")
                    Exit Try
                End If
                If iNames.AsEnumerable().Where(Function(x) x.Contains("oname")).Count > 1 Then
                    StrMsg = Language.getMessage(mstrModuleName, 516, "Sorry, you have added othername <#oname#> tag more than once.")
                    Exit Try
                End If
                If iNames.AsEnumerable().Where(Function(x) x.Contains("sname")).Count > 1 Then
                    StrMsg = Language.getMessage(mstrModuleName, 517, "Sorry, you have added surname <#sname#> tag more than once.")
                    Exit Try
                End If
                If Array.IndexOf(iNames, "-") = 0 Then
                    StrMsg = Language.getMessage(mstrModuleName, 518, "Sorry, you have added '-' in the starting.")
                    Exit Try
                End If
                If Array.LastIndexOf(iNames, "-") = iNames.Length - 1 Then
                    StrMsg = Language.getMessage(mstrModuleName, 519, "Sorry, you have added '-' in the last.")
                    Exit Try
                End If

                Dim regex As RegularExpressions.Regex = New RegularExpressions.Regex("##")
                Dim match As RegularExpressions.Match = regex.Match(strNameFormatValue)
                If match.Success Then
                    StrMsg = Language.getMessage(mstrModuleName, 520, "Sorry, you have added '#' in a continue manner.")
                    Exit Try
                End If

                regex = New RegularExpressions.Regex("--")
                match = regex.Match(strNameFormatValue)
                If match.Success Then
                    StrMsg = Language.getMessage(mstrModuleName, 521, "Sorry, you have added '-' in a continue manner.")
                    Exit Try
                End If

                regex = New RegularExpressions.Regex("#-#-#")
                match = regex.Match(strNameFormatValue)
                If match.Success Then
                    StrMsg = Language.getMessage(mstrModuleName, 522, "Sorry, you have added '#' or '-' without names tag.")
                    Exit Try
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ValidNameFormatValue; Module Name: " & mstrModuleName)
        End Try
        Return StrMsg
    End Function
    'S.SANDEEP |06-Jan-2023| -- END

    'Hemant (15 Sep 2023) -- Start
    'ISSUE/ENHANCEMENT(Toyota): A1X-1306 - Employee listing report modification to allow user to choose applicable fields
    Public Function GetDisplayColumn(ByVal StrList As String, Optional ByVal strFilter As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation
            StrQ = " SELECT * FROM ( "
            StrQ &= "          SELECT " & enReport_Column.Employee_Code & " AS Id, @EMPLOYEE_CODE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Employee_Name & " AS Id, @EMPLOYEE_NAME AS Name " & _
                   "    UNION SELECT " & enReport_Column.Branch & " AS Id, @BRANCH AS Name " & _
                   "    UNION SELECT " & enReport_Column.Department_Group & " AS Id, @DEPARTMENT_GROUP AS Name " & _
                   "    UNION SELECT " & enReport_Column.Department & " AS Id, @DEPARTMENT AS Name " & _
                   "    UNION SELECT " & enReport_Column.Section_Group & " AS Id, @SECTION_GROUP AS Name " & _
                   "    UNION SELECT " & enReport_Column.Section & " AS Id, @SECTION AS Name " & _
                   "    UNION SELECT " & enReport_Column.Unit_Group & " AS Id, @UNIT_GROUP AS Name " & _
                   "    UNION SELECT " & enReport_Column.Unit & " AS Id, @UNIT AS Name " & _
                   "    UNION SELECT " & enReport_Column.Team & " AS Id, @TEAM AS Name " & _
                   "    UNION SELECT " & enReport_Column.Class_Group & " AS Id, @CLASS_GROUP AS Name " & _
                   "    UNION SELECT " & enReport_Column.Classes & " AS Id, @CLASSES AS Name " & _
                   "    UNION SELECT " & enReport_Column.Job_Group & " AS Id, @JOB_GROUP AS Name " & _
                   "    UNION SELECT " & enReport_Column.Job & " AS Id, @JOB AS Name " & _
                   "    UNION SELECT " & enReport_Column.Cost_Center & " AS Id, @COST_CENTER AS Name " & _
                   "    UNION SELECT " & enReport_Column.Grade_Group & " AS Id, @GRADE_GROUP AS Name " & _
                   "    UNION SELECT " & enReport_Column.Grade & " AS Id, @GRADE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Grade_Level & " AS Id, @GRADE_LEVEL AS Name " & _
                   "    UNION SELECT " & enReport_Column.Increment & " AS Id, @INCREMENT AS Name " & _
                   "    UNION SELECT " & enReport_Column.Employment_Type & " AS Id, @EMPLOYMENT_TYPE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Date_Hired & " AS Id, @DATE_HIRED AS Name " & _
                   "    UNION SELECT " & enReport_Column.Birth_Date & " AS Id, @BIRTH_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Age & " AS Id, @AGE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Gender & " AS Id, @GENDER AS Name " & _
                   "    UNION SELECT " & enReport_Column.Marital_Status & " AS Id, @MARITAL_STATUS AS Name " & _
                   "    UNION SELECT " & enReport_Column.Religion & " AS Id, @RELIGION AS Name " & _
                   "    UNION SELECT " & enReport_Column.Telephone & " AS Id, @TELEPHONE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Present_Address & " AS Id, @PRESENT_ADDRESS AS Name " & _
                   "    UNION SELECT " & enReport_Column.Email & " AS Id, @EMAIL AS Name " & _
                   "    UNION SELECT " & enReport_Column.Emergency_Contact & " AS Id, @EMERGENCY_CONTACT AS Name " & _
                   "    UNION SELECT " & enReport_Column.Bank & " AS Id, @BANK AS Name " & _
                   "    UNION SELECT " & enReport_Column.Membership & " AS Id, @MEMBERSHIP AS Name " & _
                   "    UNION SELECT " & enReport_Column.Anniversary_Date & " AS Id, @ANNIVERSARY_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.First_Appointment_Date & " AS Id, @FIRST_APPOINTMENT_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Confirmation_Date & " AS Id, @CONFIRMATION_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Reinstatement_Date & " AS Id, @REINSTATEMENT_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Probation_Start_Date & " AS Id, @PROBATION_START_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Probation_End_Date & " AS Id, @PROBATION_END_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Suspended_From_Date & " AS Id, @SUSPENDED_FROM_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.Suspended_To_Date & " AS Id, @SUSPENDED_TO_DATE AS Name " & _
                   "    UNION SELECT " & enReport_Column.End_Of_Contract & " AS Id, @END_OF_CONTRACT AS Name " & _
                   "    UNION SELECT " & enReport_Column.EOC_Leaving_Reason & " AS Id, @EOC_LEAVING_REASON AS Name " & _
                   "    UNION SELECT " & enReport_Column.Qualification & " AS Id, @QUALIFICATION AS Name " & _
                   "    UNION SELECT " & enReport_Column.Identity & " AS Id, @IDENTITY AS Name " & _
                   "    UNION SELECT " & enReport_Column.Experience & " AS Id, @EXPERIENCE AS Name "
            StrQ &= " ) AS A " & _
             " WHERE 1 = 1 "

            If strFilter.Trim.Length > 0 Then
                StrQ &= strFilter
            End If

            objDataOperation.AddParameter("@EMPLOYEE_CODE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 82, "Employee Code"))
            objDataOperation.AddParameter("@EMPLOYEE_NAME", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 46, "Employee Name"))
            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 63, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 53, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 64, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 54, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 65, "Section"))
            objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 55, "Unit Group"))
            objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 66, "Unit"))
            objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 56, "Team"))
            objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 58, "Class Group"))
            objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 59, "Class"))
            objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 57, "Job Group"))
            objDataOperation.AddParameter("@JOB", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 67, "Job"))
            objDataOperation.AddParameter("@COST_CENTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 68, "CostCenter"))
            objDataOperation.AddParameter("@GRADE_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 69, "Grade Group"))
            objDataOperation.AddParameter("@GRADE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 70, "Grade"))
            objDataOperation.AddParameter("@GRADE_LEVEL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 71, "Grade Level"))
            objDataOperation.AddParameter("@INCREMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "Increment"))
            objDataOperation.AddParameter("@EMPLOYMENT_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 41, "Employment Type"))
            objDataOperation.AddParameter("@DATE_HIRED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 42, "Date Hired"))
            objDataOperation.AddParameter("@BIRTH_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 43, "Birthdate"))
            objDataOperation.AddParameter("@AGE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "Age"))
            objDataOperation.AddParameter("@GENDER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 45, "Gender"))
            objDataOperation.AddParameter("@MARITAL_STATUS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 46, "Marital Status"))
            objDataOperation.AddParameter("@RELIGION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 47, "Religion"))
            objDataOperation.AddParameter("@TELEPHONE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 48, "Telephone"))
            objDataOperation.AddParameter("@PRESENT_ADDRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 49, "Present Address"))
            objDataOperation.AddParameter("@EMAIL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 81, "Email"))
            objDataOperation.AddParameter("@EMERGENCY_CONTACT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 50, "Emergency Contact"))
            objDataOperation.AddParameter("@BANK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "Bank"))
            objDataOperation.AddParameter("@MEMBERSHIP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 52, "Membership"))
            objDataOperation.AddParameter("@ANNIVERSARY_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "Anniversary Date"))
            objDataOperation.AddParameter("@FIRST_APPOINTMENT_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "First Appointment Date"))
            objDataOperation.AddParameter("@CONFIRMATION_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Confirmation Date"))
            objDataOperation.AddParameter("@REINSTATEMENT_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 75, "Reinstatement Date"))
            objDataOperation.AddParameter("@PROBATION_START_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 76, "Probation Start Date"))
            objDataOperation.AddParameter("@PROBATION_END_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 77, "Probation End Date"))
            objDataOperation.AddParameter("@SUSPENDED_FROM_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 78, "Suspended From Date"))
            objDataOperation.AddParameter("@SUSPENDED_TO_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 79, "Suspended To Date"))
            objDataOperation.AddParameter("@END_OF_CONTRACT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 80, "End Of Contract"))
            objDataOperation.AddParameter("@EOC_LEAVING_REASON", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 507, "EOC/Leaving Reason"))
            objDataOperation.AddParameter("@QUALIFICATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 508, "Qualification"))
            objDataOperation.AddParameter("@IDENTITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 509, "Identity"))
            objDataOperation.AddParameter("@EXPERIENCE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 510, "Experience"))



            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "GetDisplayColumn", mstrModuleName)
            Return Nothing
        Finally

        End Try
    End Function
    'Hemant (15 Sep 2023) -- End

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                     ByVal intUserUnkid As Integer, _
                                     ByVal intYearUnkid As Integer, _
                                     ByVal intCompanyUnkid As Integer, _
                                     ByVal dtPeriodStart As Date, _
                                     ByVal dtPeriodEnd As Date, _
                                     ByVal strUserModeSetting As String, _
                                     ByVal blnOnlyApproved As Boolean, _
                                     ByVal strExportReportPath As String, _
                                     ByVal blnOpenReportAfterExport As Boolean, _
                                     ByVal blnIsweb As Boolean, _
                                     ByVal blnShowFirstAppointmentDate As Boolean)
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Level"))
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Female"))

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "IF OBJECT_ID('tempdb..#EQlf') IS NOT NULL DROP TABLE #EQlf " & _
                   "IF OBJECT_ID('tempdb..#Eexpr') IS NOT NULL DROP TABLE #Eexpr " & _
                   "SELECT " & _
                   "     Q2.employeeunkid " & _
                   "    ,STUFF(( " & _
                   "        SELECT + ',' + @Qualification + hrqualification_master.qualificationname + ', ' + @Institue + hrinstitute_master.institute_name " & _
                   "        FROM hremp_qualification_tran AS QT " & _
                   "            JOIN hrinstitute_master ON QT.instituteunkid = hrinstitute_master.instituteunkid " & _
                   "            JOIN hrqualification_master ON QT.qualificationunkid = hrqualification_master.qualificationunkid " & _
                   "            JOIN hremployee_master ON QT.employeeunkid = hremployee_master.employeeunkid " & _
                   "        WHERE QT.isvoid = 0 " & _
                   "        AND (Q2.employeeunkid = QT.employeeunkid) " & _
                   "        FOR XML PATH('') " & _
                   "        ), 1, 1, '') AS Qualification " & _
                   "INTO #EQlf " & _
                   "FROM hremp_qualification_tran AS Q2 " & _
                   "GROUP BY Q2.employeeunkid " & _
                   "SELECT " & _
                   "     Q2.employeeunkid " & _
                   "    ,STUFF(( " & _
                   "        SELECT + ',' + @Company + QT.company + ', ' + @Job + QT.old_job " & _
                   "        FROM hremp_experience_tran AS QT " & _
                   "        WHERE QT.isvoid = 0 " & _
                   "        AND (Q2.employeeunkid = QT.employeeunkid) " & _
                   "        ORDER BY QT.start_date " & _
                   "        FOR XML PATH('') " & _
                   "        ), 1, 1, '') AS Experience " & _
                   "INTO #Eexpr " & _
                   "FROM hremp_experience_tran AS Q2 " & _
                   "GROUP BY Q2.employeeunkid "

            StrQ &= "SELECT " & _
                   "     employeecode AS [" & IIf(Language.getMessage(mstrModuleName, 82, "Employee Code") = "", "Employee Code", Language.getMessage(mstrModuleName, 82, "Employee Code")) & "] "

            'S.SANDEEP |06-Jan-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-554
            ''If mblnFirstNamethenSurname = False Then
            ''StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employeename "
            ''Else
            'StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS [" & IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")) & "] "
            ''End If
            'StrQ &= "    /*,firstname AS [" & IIf(Language.getMessage(mstrModuleName, 83, "Firstname") = "", "Firstname", Language.getMessage(mstrModuleName, 83, "Firstname")) & "] " & _
            '       "    ,ISNULL(othername, '') AS [" & IIf(Language.getMessage(mstrModuleName, 84, "Othername") = "", "Othername", Language.getMessage(mstrModuleName, 84, "Othername")) & "] " & _
            '       "    ,surname AS [" & IIf(Language.getMessage(mstrModuleName, 85, "Surname") = "", "Surname", Language.getMessage(mstrModuleName, 85, "Surname")) & "] */ " & _
            '       "    ,ISNULL(SM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 63, "Branch") = "", "Branch", Language.getMessage(mstrModuleName, 63, "Branch")) & "] " & _
            '       "    ,ISNULL(DGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 53, "Department Group") = "", "Department Group", Language.getMessage(mstrModuleName, 53, "Department Group")) & "] " & _
            '       "    ,ISNULL(DM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 64, "Department") = "", "Department", Language.getMessage(mstrModuleName, 64, "Department")) & "] " & _
            '       "    ,ISNULL(SGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 54, "Section Group") = "", "Section Group", Language.getMessage(mstrModuleName, 54, "Section Group")) & "] " & _
            '       "    ,ISNULL(SECM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 65, "Section") = "", "Section", Language.getMessage(mstrModuleName, 65, "Section")) & "] " & _
            '       "    ,ISNULL(UGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 55, "Unit Group") = "", "Unit Group", Language.getMessage(mstrModuleName, 55, "Unit Group")) & "] " & _
            '       "    ,ISNULL(UM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 66, "Unit") = "", "Unit", Language.getMessage(mstrModuleName, 66, "Unit")) & "] " & _
            '       "    ,ISNULL(TM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 56, "Team") = "", "Team", Language.getMessage(mstrModuleName, 56, "Team")) & "] " & _
            '       "    ,ISNULL(CGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 58, "Class Group") = "", "Class Group", Language.getMessage(mstrModuleName, 58, "Class Group")) & "] " & _
            '       "    ,ISNULL(CM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 59, "Class") = "", "Class", Language.getMessage(mstrModuleName, 59, "Class")) & "] " & _
            '       "    ,ISNULL(JGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 57, "Job Group") = "", "Job Group", Language.getMessage(mstrModuleName, 57, "Job Group")) & "] " & _
            '       "    ,ISNULL(JM.job_name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 67, "Job") = "", "Job", Language.getMessage(mstrModuleName, 67, "Job")) & "] " & _
            '       "    ,ISNULL(CCM.costcentername,'') AS [" & IIf(Language.getMessage(mstrModuleName, 68, "CostCenter") = "", "CostCenter", Language.getMessage(mstrModuleName, 68, "CostCenter")) & "] " & _
            '       "    ,ISNULL(GGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 69, "Grade Group") = "", "Grade Group", Language.getMessage(mstrModuleName, 69, "Grade Group")) & "] " & _
            '       "    ,ISNULL(GM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 70, "Grade") = "", "Grade", Language.getMessage(mstrModuleName, 70, "Grade")) & "] " & _
            '       "    ,ISNULL(GLM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 71, "Grade Level") = "", "Grade Level", Language.getMessage(mstrModuleName, 71, "Grade Level")) & "] "

            If mintNameFormatId = NameFormat.NF_SEPARATELY Then
                StrQ &= "   ,firstname AS [" & IIf(Language.getMessage(mstrModuleName, 83, "Firstname") = "", "Firstname", Language.getMessage(mstrModuleName, 83, "Firstname")) & "] " & _
                        "   ,ISNULL(othername, '') AS [" & IIf(Language.getMessage(mstrModuleName, 84, "Othername") = "", "Othername", Language.getMessage(mstrModuleName, 84, "Othername")) & "] " & _
                        "   ,surname AS [" & IIf(Language.getMessage(mstrModuleName, 85, "Surname") = "", "Surname", Language.getMessage(mstrModuleName, 85, "Surname")) & "] "
            ElseIf mintNameFormatId = NameFormat.NF_USERDEFINE Then
                If mstrNameFormatValue.Trim.Length > 0 Then
                    Dim iStrQ As String = mstrNameFormatValue
                    'Dim iNames() As String = mstrNameFormatValue.Split("-")                    
                    'If Array.IndexOf(iNames, "#FName#") <> -1 Then
                    '    iStrQ &= ""
                    'End If
                    iStrQ = iStrQ.ToLower()
                    iStrQ = iStrQ.Replace("#fname#", "ISNULL(hremployee_master.firstname, '')").Replace("#oname#", "ISNULL(othername, '')").Replace("#sname#", "ISNULL(hremployee_master.surname, '')")
                    iStrQ = iStrQ.Replace("-", "+ ' ' +")
                    StrQ &= ", " & iStrQ & " AS [" & IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")) & "] "
                Else
            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS [" & IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")) & "] "
                End If
            End If
            StrQ &= "    ,ISNULL(SM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 63, "Branch") = "", "Branch", Language.getMessage(mstrModuleName, 63, "Branch")) & "] " & _
                   "    ,ISNULL(DGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 53, "Department Group") = "", "Department Group", Language.getMessage(mstrModuleName, 53, "Department Group")) & "] " & _
                   "    ,ISNULL(DM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 64, "Department") = "", "Department", Language.getMessage(mstrModuleName, 64, "Department")) & "] " & _
                   "    ,ISNULL(SGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 54, "Section Group") = "", "Section Group", Language.getMessage(mstrModuleName, 54, "Section Group")) & "] " & _
                   "    ,ISNULL(SECM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 65, "Section") = "", "Section", Language.getMessage(mstrModuleName, 65, "Section")) & "] " & _
                   "    ,ISNULL(UGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 55, "Unit Group") = "", "Unit Group", Language.getMessage(mstrModuleName, 55, "Unit Group")) & "] " & _
                   "    ,ISNULL(UM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 66, "Unit") = "", "Unit", Language.getMessage(mstrModuleName, 66, "Unit")) & "] " & _
                   "    ,ISNULL(TM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 56, "Team") = "", "Team", Language.getMessage(mstrModuleName, 56, "Team")) & "] " & _
                   "    ,ISNULL(CGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 58, "Class Group") = "", "Class Group", Language.getMessage(mstrModuleName, 58, "Class Group")) & "] " & _
                   "    ,ISNULL(CM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 59, "Class") = "", "Class", Language.getMessage(mstrModuleName, 59, "Class")) & "] " & _
                   "    ,ISNULL(JGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 57, "Job Group") = "", "Job Group", Language.getMessage(mstrModuleName, 57, "Job Group")) & "] " & _
                   "    ,ISNULL(JM.job_name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 67, "Job") = "", "Job", Language.getMessage(mstrModuleName, 67, "Job")) & "] " & _
                   "    ,ISNULL(CCM.costcentername,'') AS [" & IIf(Language.getMessage(mstrModuleName, 68, "CostCenter") = "", "CostCenter", Language.getMessage(mstrModuleName, 68, "CostCenter")) & "] " & _
                   "    ,ISNULL(GGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 69, "Grade Group") = "", "Grade Group", Language.getMessage(mstrModuleName, 69, "Grade Group")) & "] " & _
                   "    ,ISNULL(GM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 70, "Grade") = "", "Grade", Language.getMessage(mstrModuleName, 70, "Grade")) & "] " & _
                   "    ,ISNULL(GLM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 71, "Grade Level") = "", "Grade Level", Language.getMessage(mstrModuleName, 71, "Grade Level")) & "] "
            'S.SANDEEP |06-Jan-2023| -- END



            If mblnShowEmpScale Then
                StrQ &= "    ,CAST (Scale.newscale AS DECIMAL(36, 6)) AS [" & IIf(Language.getMessage(mstrModuleName, 39, "Salary") = "", "Salary", Language.getMessage(mstrModuleName, 39, "Salary")) & "] "
            End If

            StrQ &= "    ,0.00 AS [" & IIf(Language.getMessage(mstrModuleName, 40, "Increment") = "", "Increment", Language.getMessage(mstrModuleName, 40, "Increment")) & "] " & _
                    "    ,ISNULL(ET.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 41, "Employment Type") = "", "Employment Type", Language.getMessage(mstrModuleName, 41, "Employment Type")) & "] " & _
                    "    ,ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 42, "Date Hired") = "", "Date Hired", Language.getMessage(mstrModuleName, 42, "Date Hired")) & "] " & _
                    "    ,ISNULL(CONVERT(CHAR(8), hremployee_master.birthdate, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 43, "Birthdate") = "", "Birthdate", Language.getMessage(mstrModuleName, 43, "Birthdate")) & "] " & _
                    "    ,ISNULL(CAST(YEAR(GETDATE()) - YEAR(hremployee_master.birthdate) AS VARCHAR(3)), '') AS [" & IIf(Language.getMessage(mstrModuleName, 44, "Age") = "", "Age", Language.getMessage(mstrModuleName, 44, "Age")) & "] " & _
                    "    ,CASE WHEN gender <= 0 THEN '' WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END AS [" & IIf(Language.getMessage(mstrModuleName, 45, "Gender") = "", "Gender", Language.getMessage(mstrModuleName, 45, "Gender")) & "] " & _
                    "    ,ISNULL(MS.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 46, "Marital Status") = "", "Marital Status", Language.getMessage(mstrModuleName, 46, "Marital Status")) & "] " & _
                    "    ,ISNULL(RG.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 47, "Religion") = "", "Religion", Language.getMessage(mstrModuleName, 47, "Religion")) & "] " & _
                    "    ,ISNULL(hremployee_master.present_tel_no, '') AS [" & IIf(Language.getMessage(mstrModuleName, 48, "Telephone") = "", "Telephone", Language.getMessage(mstrModuleName, 48, "Telephone")) & "] " & _
                    "    ,ISNULL(hremployee_master.present_address1, '') + ' ' + ISNULL(hremployee_master.present_address2, '') AS [" & IIf(Language.getMessage(mstrModuleName, 49, "Present Address") = "", "Present Address", Language.getMessage(mstrModuleName, 49, "Present Address")) & "] " & _
                    "    ,ISNULL(hremployee_master.email, '') AS [" & IIf(Language.getMessage(mstrModuleName, 81, "Email") = "", "Email", Language.getMessage(mstrModuleName, 81, "Email")) & "] " & _
                    "    ,ISNULL(CASE WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' AND ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') + ' , ' + ISNULL(hremployee_master.emer_con_tel_no, '') " & _
                    "                 WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') " & _
                    "                 WHEN ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_tel_no, '') END, '') AS [" & IIf(Language.getMessage(mstrModuleName, 50, "Emergency Contact") = "", "Emergency Contact", Language.getMessage(mstrModuleName, 50, "Emergency Contact")) & "] " & _
                    "    ,'' AS [" & IIf(Language.getMessage(mstrModuleName, 51, "Bank") = "", "Bank", Language.getMessage(mstrModuleName, 51, "Bank")) & "] " & _
                    "    ,'' AS [" & IIf(Language.getMessage(mstrModuleName, 52, "Membership") = "", "Membership", Language.getMessage(mstrModuleName, 52, "Membership")) & "] " & _
                    "    ,ISNULL(CONVERT(CHAR(8), hremployee_master.anniversary_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 72, "Anniversary Date") = "", "Anniversary Date", Language.getMessage(mstrModuleName, 72, "Anniversary Date")) & "] "

            If blnShowFirstAppointmentDate Then
                StrQ &= "    ,ISNULL(CONVERT(CHAR(8), hremployee_master.firstappointment_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 73, "First Appointment Date") = "", "First Appointment Date", Language.getMessage(mstrModuleName, 73, "First Appointment Date")) & "]"
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "    ,ISNULL(CONVERT(CHAR(8), CnfDt.date1,112),'') AS [" & IIf(Language.getMessage(mstrModuleName, 74, "Confirmation Date") = "", "Confirmation Date", Language.getMessage(mstrModuleName, 74, "Confirmation Date")) & "]" & _
                    "    ,ISNULL(CONVERT(CHAR(8), ReInstDt.reinstatment_date,112),'') AS [" & IIf(Language.getMessage(mstrModuleName, 75, "Reinstatement Date") = "", "Reinstatement Date", Language.getMessage(mstrModuleName, 75, "Reinstatement Date")) & "]" & _
                    "    ,ISNULL(CONVERT(CHAR(8), PrbDt.date1,112),'') AS [" & IIf(Language.getMessage(mstrModuleName, 76, "Probation Start Date") = "", "Probation Start Date", Language.getMessage(mstrModuleName, 76, "Probation Start Date")) & "]" & _
                    "    ,ISNULL(CONVERT(CHAR(8), PrbDt.date2,112),'') AS [" & IIf(Language.getMessage(mstrModuleName, 77, "Probation End Date") = "", "Probation Start Date", Language.getMessage(mstrModuleName, 77, "Probation End Date")) & "]" & _
                    "    ,ISNULL(CONVERT(CHAR(8), SuspDt.date1,112),'') AS [" & IIf(Language.getMessage(mstrModuleName, 78, "Suspended From Date") = "", "Suspended From Date", Language.getMessage(mstrModuleName, 78, "Suspended From Date")) & "]" & _
                    "    ,ISNULL(CONVERT(CHAR(8), SuspDt.date2,112),'') AS [" & IIf(Language.getMessage(mstrModuleName, 79, "Suspended To Date") = "", "Suspended To Date", Language.getMessage(mstrModuleName, 79, "Suspended To Date")) & "]" & _
                    "    ,ISNULL(CONVERT(CHAR(8), EocDt.date1,112),'') AS [" & IIf(Language.getMessage(mstrModuleName, 80, "End Of Contract") = "", "End Of Contract", Language.getMessage(mstrModuleName, 80, "End Of Contract")) & "]" & _
                    "    ,ISNULL(EocDt.LReason,'') AS [" & IIf(Language.getMessage(mstrModuleName, 507, "EOC/Leaving Reason") = "", "EOC/Leaving Reason", Language.getMessage(mstrModuleName, 507, "EOC/Leaving Reason")) & "] " & _
                    "    ,ISNULL(#EQlf.Qualification,'') AS [" & IIf(Language.getMessage(mstrModuleName, 508, "Qualification") = "", "Qualification", Language.getMessage(mstrModuleName, 508, "Qualification")) & "] " & _
                    "    ,ISNULL(EIdty.Ids,'') AS [" & IIf(Language.getMessage(mstrModuleName, 509, "Identity") = "", "", Language.getMessage(mstrModuleName, 509, "Identity")) & "] " & _
                    "    ,ISNULL(#Eexpr.Experience,'') AS [" & IIf(Language.getMessage(mstrModuleName, 510, "Experience") = "", "Experience", Language.getMessage(mstrModuleName, 510, "Experience")) & "] " & _
                    "    ,hremployee_master.employeeunkid " & _
                    "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "LEFT JOIN cfcommon_master AS ET ON ET.masterunkid = employmenttypeunkid " & _
                    "LEFT JOIN cfcommon_master AS MS ON MS.masterunkid = maritalstatusunkid " & _
                    "LEFT JOIN cfcommon_master AS RG ON RG.masterunkid = religionunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         newscale " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                    "    FROM prsalaryincrement_tran " & _
                    "    WHERE prsalaryincrement_tran.isvoid = 0 " & _
                    "    AND CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) < = '" & eZeeDate.convertDate(dtPeriodEnd) & "' AND prsalaryincrement_tran.isapproved = 1 " & _
                    ") AS Scale ON Scale.employeeunkid = hremployee_master.employeeunkid AND Scale.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
                    "LEFT JOIN hrdepartment_group_master AS DGM ON Alloc.deptgroupunkid = DGM.deptgroupunkid " & _
                    "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "LEFT JOIN hrsectiongroup_master AS SGM ON Alloc.sectiongroupunkid = SGM.sectiongroupunkid " & _
                    "LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid " & _
                    "LEFT JOIN hrunitgroup_master AS UGM ON Alloc.unitgroupunkid = UGM.unitgroupunkid " & _
                    "LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid " & _
                    "LEFT JOIN hrteam_master AS TM ON Alloc.teamunkid = TM.teamunkid " & _
                    "LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN hrjobgroup_master AS JGM ON Jobs.jobgroupunkid = JGM.jobgroupunkid " & _
                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                    "JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         gradegroupunkid " & _
                    "        ,gradeunkid " & _
                    "        ,gradelevelunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                    "    FROM prsalaryincrement_tran " & _
                    "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                    "JOIN hrgradegroup_master AS GGM ON GGM.gradegroupunkid = Grds.gradegroupunkid " & _
                    "JOIN hrgrade_master AS GM ON Grds.gradeunkid = GM.gradeunkid " & _
                    "JOIN hrgradelevel_master AS GLM ON Grds.gradelevelunkid = GLM.gradelevelunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = CC.cctranheadvalueid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         date1 " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         reinstatment_date " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_rehire_tran " & _
                    "    WHERE isvoid = 0  AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS ReInstDt ON ReInstDt.employeeunkid = hremployee_master.employeeunkid AND ReInstDt.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         date1 " & _
                    "        ,date2 " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS PrbDt ON PrbDt.employeeunkid = hremployee_master.employeeunkid AND PrbDt.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         date1 " & _
                    "        ,date2 " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS SuspDt ON SuspDt.employeeunkid = hremployee_master.employeeunkid AND SuspDt.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         date1 " & _
                    "        ,date2 " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "        ,CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name, '') END AS LReason " & _
                    "    FROM hremployee_dates_tran " & _
                    "       LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_dates_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TERMINATION & _
                    "       LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_dates_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 "

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'StrQ &= "LEFT JOIN ( " & _
            '             "SELECT " & _
            '                   "Q2.employeeunkid " & _
            '                  ",STUFF((SELECT + ',' +  @Qualification + hrqualification_master.qualificationname + ', ' + @Institue + hrinstitute_master.institute_name " & _
            '                            "FROM hremp_qualification_tran AS QT " & _
            '                                 "JOIN hrinstitute_master ON QT.instituteunkid = hrinstitute_master.instituteunkid " & _
            '                                 "JOIN hrqualification_master ON QT.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                                 "JOIN hremployee_master ON QT.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "WHERE QT.isvoid = 0 AND (Q2.employeeunkid  = QT.employeeunkid) " & _
            '                            "FOR XML PATH ('')),1,1,'') AS Qualification " & _
            '             "FROM hremp_qualification_tran AS Q2 " & _
            '             "GROUP BY Q2.employeeunkid " & _
            '             ") AS EQlf ON EQlf.employeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN ( " & _
            '                  "SELECT " & _
            '                        "employeeunkid " & _
            '                       ",cfcommon_master.name + CASE WHEN identity_no <> '' THEN ' - ' + identity_no ELSE '' END AS Ids " & _
            '                       ",isdefault " & _
            '                  "FROM hremployee_idinfo_tran " & _
            '                       "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
            '                  "WHERE isactive = 1 AND isdefault = 1 " & _
            '             ") AS EIdty ON EIdty.employeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN ( " & _
            '                  "SELECT " & _
            '                        "Q2.employeeunkid " & _
            '                       ",STUFF((SELECT + ',' +  @Company + QT.company + ', ' + @Job + QT.old_job " & _
            '                                 "FROM hremp_experience_tran AS QT " & _
            '                                 "WHERE QT.isvoid = 0 AND (Q2.employeeunkid  = QT.employeeunkid) " & _
            '                                 "ORDER BY QT.start_date FOR XML PATH ('')),1,1,'') AS Experience " & _
            '                  "FROM hremp_experience_tran AS Q2 " & _
            '                  "GROUP BY Q2.employeeunkid " & _
            '             ") AS Eexpr ON Eexpr.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= "LEFT JOIN #EQlf ON #EQlf.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN #Eexpr ON #Eexpr.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN ( " & _
                              "SELECT " & _
                                    "employeeunkid " & _
                                   ",cfcommon_master.name + CASE WHEN identity_no <> '' THEN ' - ' + identity_no ELSE '' END AS Ids " & _
                                   ",isdefault " & _
                              "FROM hremployee_idinfo_tran " & _
                                   "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                              "WHERE isactive = 1 AND isdefault = 1 " & _
                         ") AS EIdty ON EIdty.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- END

            '-------------------------- ADDED
            ',CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name, '') END AS LReason
            'LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_dates_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TERMINATION
            'LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_dates_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE
            'S.SANDEEP |15-JUL-2019| -- END

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END


            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            If mintViewIndex > 0 Then
                StrQ &= "ORDER BY " & mstrAnalysis_OrderBy & ",  hremployee_master.employeecode "
            Else
                StrQ &= "ORDER BY  hremployee_master.employeecode "
            End If

            StrQ &= "IF OBJECT_ID('tempdb..#EQlf') IS NOT NULL DROP TABLE #EQlf " & _
                    "IF OBJECT_ID('tempdb..#Eexpr') IS NOT NULL DROP TABLE #Eexpr "

            objDataOperation.AddParameter("@Qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 511, "Qualification : "))
            objDataOperation.AddParameter("@Institue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 512, "Institue : "))
            objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 513, "Company : "))
            objDataOperation.AddParameter("@Job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 514, "Job : "))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsMemData = Employee_Membership()
            dsBnkData = Employee_Bank()

            Call SetData(dsList.Tables(0))

            'Hemant (15 Sep 2023) -- Start
            'ISSUE/ENHANCEMENT(Toyota): A1X-1306 - Employee listing report modification to allow user to choose applicable fields
            Dim arrSelectedField() As String = Split(mstrHideColumnIds, ",")
            For Each strFieldID As String In arrSelectedField
                If strFieldID.Trim.Length <= 0 Then Continue For
                Select Case CInt(strFieldID)
                    Case enReport_Column.Employee_Code
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 82, "Employee Code") = "", "Employee Code", Language.getMessage(mstrModuleName, 82, "Employee Code")))
                    Case enReport_Column.Employee_Name
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")))
                    Case enReport_Column.Branch
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 63, "Branch") = "", "Branch", Language.getMessage(mstrModuleName, 63, "Branch")))
                    Case enReport_Column.Department_Group
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 53, "Department Group") = "", "Department Group", Language.getMessage(mstrModuleName, 53, "Department Group")))
                    Case enReport_Column.Department
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 64, "Department") = "", "Department", Language.getMessage(mstrModuleName, 64, "Department")))
                    Case enReport_Column.Section_Group
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 54, "Section Group") = "", "Section Group", Language.getMessage(mstrModuleName, 54, "Section Group")))
                    Case enReport_Column.Section
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 65, "Section") = "", "Section", Language.getMessage(mstrModuleName, 65, "Section")))
                    Case enReport_Column.Unit_Group
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 55, "Unit Group") = "", "Unit Group", Language.getMessage(mstrModuleName, 55, "Unit Group")))
                    Case enReport_Column.Unit
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 66, "Unit") = "", "Unit", Language.getMessage(mstrModuleName, 66, "Unit")))
                    Case enReport_Column.Team
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 56, "Team") = "", "Team", Language.getMessage(mstrModuleName, 56, "Team")))
                    Case enReport_Column.Class_Group
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 58, "Class Group") = "", "Class Group", Language.getMessage(mstrModuleName, 58, "Class Group")))
                    Case enReport_Column.Classes
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 59, "Class") = "", "Class", Language.getMessage(mstrModuleName, 59, "Class")))
                    Case enReport_Column.Job_Group
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 57, "Job Group") = "", "Job Group", Language.getMessage(mstrModuleName, 57, "Job Group")))
                    Case enReport_Column.Job
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 67, "Job") = "", "Job", Language.getMessage(mstrModuleName, 67, "Job")))
                    Case enReport_Column.Cost_Center
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 68, "CostCenter") = "", "CostCenter", Language.getMessage(mstrModuleName, 68, "CostCenter")))
                    Case enReport_Column.Grade_Group
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 69, "Grade Group") = "", "Grade Group", Language.getMessage(mstrModuleName, 69, "Grade Group")))
                    Case enReport_Column.Grade
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 70, "Grade") = "", "Grade", Language.getMessage(mstrModuleName, 70, "Grade")))
                    Case enReport_Column.Grade_Level
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 71, "Grade Level") = "", "Grade Level", Language.getMessage(mstrModuleName, 71, "Grade Level")))
                    Case enReport_Column.Increment
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 40, "Increment") = "", "Increment", Language.getMessage(mstrModuleName, 40, "Increment")))
                    Case enReport_Column.Employment_Type
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 41, "Employment Type") = "", "Employment Type", Language.getMessage(mstrModuleName, 41, "Employment Type")))
                    Case enReport_Column.Date_Hired
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 42, "Date Hired") = "", "Date Hired", Language.getMessage(mstrModuleName, 42, "Date Hired")))
                    Case enReport_Column.Birth_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 43, "Birthdate") = "", "Birthdate", Language.getMessage(mstrModuleName, 43, "Birthdate")))
                    Case enReport_Column.Age
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 44, "Age") = "", "Age", Language.getMessage(mstrModuleName, 44, "Age")))
                    Case enReport_Column.Gender
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 45, "Gender") = "", "Gender", Language.getMessage(mstrModuleName, 45, "Gender")))
                    Case enReport_Column.Marital_Status
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 46, "Marital Status") = "", "Marital Status", Language.getMessage(mstrModuleName, 46, "Marital Status")))
                    Case enReport_Column.Religion
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 47, "Religion") = "", "Religion", Language.getMessage(mstrModuleName, 47, "Religion")))
                    Case enReport_Column.Telephone
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 48, "Telephone") = "", "Telephone", Language.getMessage(mstrModuleName, 48, "Telephone")))
                    Case enReport_Column.Present_Address
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 49, "Present Address") = "", "Present Address", Language.getMessage(mstrModuleName, 49, "Present Address")))
                    Case enReport_Column.Email
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 81, "Email") = "", "Email", Language.getMessage(mstrModuleName, 81, "Email")))
                    Case enReport_Column.Emergency_Contact
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 50, "Emergency Contact") = "", "Emergency Contact", Language.getMessage(mstrModuleName, 50, "Emergency Contact")))
                    Case enReport_Column.Bank
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 51, "Bank") = "", "Bank", Language.getMessage(mstrModuleName, 51, "Bank")))
                    Case enReport_Column.Membership
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 52, "Membership") = "", "Membership", Language.getMessage(mstrModuleName, 52, "Membership")))
                    Case enReport_Column.Anniversary_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 72, "Anniversary Date") = "", "Anniversary Date", Language.getMessage(mstrModuleName, 72, "Anniversary Date")))
                    Case enReport_Column.First_Appointment_Date
                        If blnShowFirstAppointmentDate Then
                            dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 73, "First Appointment Date") = "", "First Appointment Date", Language.getMessage(mstrModuleName, 73, "First Appointment Date")))
                        End If
                    Case enReport_Column.Confirmation_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 74, "Confirmation Date") = "", "Confirmation Date", Language.getMessage(mstrModuleName, 74, "Confirmation Date")))
                    Case enReport_Column.Reinstatement_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 75, "Reinstatement Date") = "", "Reinstatement Date", Language.getMessage(mstrModuleName, 75, "Reinstatement Date")))
                    Case enReport_Column.Probation_Start_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 76, "Probation Start Date") = "", "Probation Start Date", Language.getMessage(mstrModuleName, 76, "Probation Start Date")))
                    Case enReport_Column.Probation_End_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 77, "Probation End Date") = "", "Probation Start Date", Language.getMessage(mstrModuleName, 77, "Probation End Date")))
                    Case enReport_Column.Suspended_From_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 78, "Suspended From Date") = "", "Suspended From Date", Language.getMessage(mstrModuleName, 78, "Suspended From Date")))
                    Case enReport_Column.Suspended_To_Date
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 79, "Suspended To Date") = "", "Suspended To Date", Language.getMessage(mstrModuleName, 79, "Suspended To Date")))
                    Case enReport_Column.End_Of_Contract
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 80, "End Of Contract") = "", "End Of Contract", Language.getMessage(mstrModuleName, 80, "End Of Contract")))
                    Case enReport_Column.EOC_Leaving_Reason
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 507, "EOC/Leaving Reason") = "", "EOC/Leaving Reason", Language.getMessage(mstrModuleName, 507, "EOC/Leaving Reason")))
                    Case enReport_Column.Qualification
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 508, "Qualification") = "", "Qualification", Language.getMessage(mstrModuleName, 508, "Qualification")))
                    Case enReport_Column.Identity
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 509, "Identity") = "", "", Language.getMessage(mstrModuleName, 509, "Identity")))
                    Case enReport_Column.Experience
                        dsList.Tables(0).Columns.Remove(IIf(Language.getMessage(mstrModuleName, 510, "Experience") = "", "Experience", Language.getMessage(mstrModuleName, 510, "Experience")))

                End Select
            Next
            'Hemant (15 Sep 2023) -- End


            If blnIsweb = False Then

                Dim objDic As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim rowsArrayHeader As New ArrayList

                mdtTableExcel = dsList.Tables(0)


                mdtTableExcel.Columns.Remove("employeeunkid")
                mdtTableExcel.Columns.Remove("Id")

                'START TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 
                If mintViewIndex <= 0 Then
                    mdtTableExcel.Columns.Remove("GName")
                Else
                    mdtTableExcel.Columns("GName").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Remove(mstrReport_GroupName.IndexOf(":")).Trim, mstrReport_GroupName.Trim)
                    Dim strGrpCols As String() = {"GName"}
                    strarrGroupColumns = strGrpCols
                End If
                'END TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

                'SET EXCEL CELL WIDTH
                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 150
                Next
                'S.SANDEEP |15-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
                'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, False)
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", Me._FilterTitle, Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, False)
                'S.SANDEEP |15-JUL-2019| -- END
            Else
                dtData = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub SetData(ByVal dt As DataTable)

        Dim objMaster As New clsMasterData
        Dim objEmp As New clsEmployee_Master
        Dim objBank As New clsEmployeeBanks

        For Each dFRow As DataRow In dt.Rows
            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            If dFRow.Item("Qualification").ToString().Trim.Length > 0 Then dFRow.Item("Qualification") = dFRow.Item("Qualification").ToString().Replace("&amp;", "&")
            'If dFRow.Item("Qualification").ToString().Trim.Length > 0 Then dFRow.Item("Qualification") = dFRow.Item("Qualification").ToString().Replace(",", "#10;")
            If dFRow.Item("Qualification").ToString().Trim.Length > 0 Then dFRow.Item("Experience") = dFRow.Item("Experience").ToString().Replace("&amp;", "&")
            'If dFRow.Item("Experience").ToString().Trim.Length > 0 Then dFRow.Item("Experience") = dFRow.Item("Experience").ToString().Replace(",", "#10;")
            'S.SANDEEP |15-JUL-2019| -- END


            If dFRow.Item(Language.getMessage(mstrModuleName, 43, "Birthdate")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 43, "Birthdate")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 43, "Birthdate")).ToString).ToShortDateString
            End If


            If dFRow.Item(Language.getMessage(mstrModuleName, 72, "Anniversary Date")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 72, "Anniversary Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 72, "Anniversary Date")).ToString).ToShortDateString
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 42, "Date Hired")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 42, "Date Hired")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 42, "Date Hired")).ToString).ToShortDateString
            End If

            If ConfigParameter._Object._ShowFirstAppointmentDate Then
                If dFRow.Item(Language.getMessage(mstrModuleName, 73, "First Appointment Date")).ToString.Trim.Length > 0 Then
                    dFRow.Item(Language.getMessage(mstrModuleName, 73, "First Appointment Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 73, "First Appointment Date")).ToString).ToShortDateString
                End If
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 74, "Confirmation Date")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 74, "Confirmation Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 74, "Confirmation Date")).ToString).ToShortDateString
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 75, "Reinstatement Date")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 75, "Reinstatement Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 75, "Reinstatement Date")).ToString).ToShortDateString
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 76, "Probation Start Date")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 76, "Probation Start Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 76, "Probation Start Date")).ToString).ToShortDateString
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 77, "Probation End Date")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 77, "Probation End Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 77, "Probation End Date")).ToString).ToShortDateString
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 78, "Suspended From Date")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 78, "Suspended From Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 78, "Suspended From Date")).ToString).ToShortDateString
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 79, "Suspended To Date")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 79, "Suspended To Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 79, "Suspended To Date")).ToString).ToShortDateString
            End If

            If dFRow.Item(Language.getMessage(mstrModuleName, 80, "End Of Contract")).ToString.Trim.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 80, "End Of Contract")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 80, "End Of Contract")).ToString).ToShortDateString
            End If

            Dim dTmp() As DataRow = dsMemData.Tables(0).Select("employeeunkid = '" & CInt(dFRow.Item("employeeunkid")) & "'")
            If dTmp.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 52, "Membership")) = dTmp(0).Item("csv")
            End If

            dTmp = dsBnkData.Tables(0).Select("employeeunkid = '" & CInt(dFRow.Item("employeeunkid")) & "'")
            If dTmp.Length > 0 Then
                dFRow.Item(Language.getMessage(mstrModuleName, 51, "Bank")) = dTmp(0).Item("csv")
            End If
        Next
    End Sub

    Private Function Employee_Membership() As DataSet
        Dim dsData As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim StrQ As String = "" : Dim exForce As Exception
        Try
            StrQ = "SELECT " & _
                   " employeeunkid " & _
                   ",ISNULL(STUFF((SELECT ',' + " & _
                   "        ISNULL(hrmembership_master.membershipname,'')+' - '+ISNULL(hremployee_meminfo_tran.membershipno,'') " & _
                   "    FROM hremployee_meminfo_tran " & _
                   "        JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                   "    WHERE hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "        AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 FOR XML PATH('')),1,1,''),'') AS csv " & _
                   "FROM hremployee_master WHERE 1 = 1 "
            If mintEmployeeId > 0 Then
                StrQ &= "AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            End If

            If mintEmployeementTypeId > 0 Then
                StrQ &= "AND hremployee_master.employmenttypeunkid = '" & mintEmployeementTypeId & "' "
            End If

            StrQ &= " ORDER BY hremployee_master.employeeunkid "

            dsData = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsData
        Catch ex As Exception
            Throw New Exception("-1," & ex.Message & ",Employee_Membership")
        End Try
    End Function

    Private Function Employee_Bank() As DataSet
        Dim dsData As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim StrQ As String = "" : Dim exForce As Exception
        Try
            StrQ = "SELECT " & _
                   " hremployee_master.employeeunkid " & _
                   ",ISNULL(STUFF((SELECT ',' + " & _
                   "            hrmsConfiguration..cfpayrollgroup_master.groupname+' - '+hrmsConfiguration..cfbankbranch_master.branchname+' - '+premployee_bank_tran.accountno " & _
                   "        FROM premployee_bank_tran " & _
                   "            LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                   "            LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id = 3 " & _
                   "        WHERE ISNULL(isvoid,0) = 0 " & _
                   "            AND premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid FOR XML PATH('')),1,1,''),'') AS csv " & _
                   "FROM hremployee_master WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= "AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            End If

            If mintEmployeementTypeId > 0 Then
                StrQ &= "AND hremployee_master.employmenttypeunkid = '" & mintEmployeementTypeId & "' "
            End If

            StrQ &= " ORDER BY hremployee_master.employeeunkid "

            dsData = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsData
        Catch ex As Exception
            Throw New Exception("-1, " & ex.Message & ", Employee_Bank")
        End Try
    End Function

    'S.SANDEEP [19-JAN-2017] -- START
    'ISSUE/ENHANCEMENT : Listing Employee(s) From the Employee Listing Screen
    Public Function ListEmployee(ByVal strFilter As String, _
                                 ByVal dtDataSource As DataTable, _
                                 ByVal ePrint As enPrintAction, _
                                 ByVal eExport As enExportAction, _
                                 ByVal strExportPath As String, _
                                 ByVal blnOpenAfterExport As Boolean, _
                                 ByVal strReportName As String) As Boolean

        Dim intArrayColumnWidth As Integer() = Nothing
        ReDim intArrayColumnWidth(dtDataSource.Columns.Count - 1)
        For i As Integer = 0 To intArrayColumnWidth.Length - 1
            intArrayColumnWidth(i) = 100
        Next
        Call ReportExecute(Nothing, ePrint, eExport, strExportPath, blnOpenAfterExport, dtDataSource, intArrayColumnWidth, True, True, True, Nothing, strReportName, "", strFilter, Nothing, "", True, Nothing, Nothing, Nothing)
    End Function
    'S.SANDEEP [19-JAN-2017] -- END


    'S.SANDEEP |25-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : FlexCube
    Public Function ExportFlexData(ByVal strFilter As String, _
                                 ByVal dtDataSource As DataTable, _
                                 ByVal ePrint As enPrintAction, _
                                 ByVal eExport As enExportAction, _
                                 ByVal strExportPath As String, _
                                 ByVal blnOpenAfterExport As Boolean, _
                                 ByVal strReportName As String) As Boolean
        Try
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtDataSource.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = intArrayColumnWidth.Length - 1 Then
                    intArrayColumnWidth(i) = 200
                Else
                    intArrayColumnWidth(i) = 120
                End If
            Next
            Call ReportExecute(Nothing, ePrint, eExport, strExportPath, blnOpenAfterExport, dtDataSource, intArrayColumnWidth, True, True, True, Nothing, strReportName, "", strFilter, Nothing, "", True, Nothing, Nothing, Nothing)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportFlexData; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |25-NOV-2019| -- END


    'S.SANDEEP [03-APR-2017] -- START
    'ISSUE/ENHANCEMENT : Missing Employee Details
    Public Sub DisplayEmployeeMovements(ByVal iCheckEmpLst As String, _
                                        ByVal blnShowTransfer As Boolean, _
                                        ByVal blnShowCategorization As Boolean, _
                                        ByVal blnShowCostCenter As Boolean, _
                                        ByVal blnShowDates As Boolean, _
                                        ByVal blnShowRehire As Boolean, _
                                        ByVal blnShowWorkPermit As Boolean, _
                                        ByVal ePrint As enPrintAction, _
                                        ByVal eExport As enExportAction, _
                                        ByVal strExportPath As String, _
                                        ByVal blnOpenAfterExport As Boolean, _
                                        ByVal strReportName As String, _
                                        ByVal IsDisplayLogo As Boolean, _
                                        ByVal ShowLogoRightSide As Boolean)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = "" : Dim StrCommonQry As String = ""
        Dim dsList As New DataSet
        Dim rpt_Main As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_Transfer As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_Categorization As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_Dates As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_Rehire As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_WorkPermit As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_CostCenter As ArutiReport.Designer.dsArutiReport = Nothing
        Try
            objDataOperation = New clsDataOperation

            StrCommonQry = "IF OBJECT_ID('tempdb..#EmpMov') IS NOT NULL DROP TABLE #EmpMov " & _
                           "CREATE TABLE #EmpMov (EmpId Int, Ecode nvarchar(max), EName nvarchar(max), EAppDate nvarchar(max)) " & _
                           "DECLARE @words VARCHAR (MAX) " & _
                           "   SET @words = '" & iCheckEmpLst & "' " & _
                           "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                           "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                           "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                           "WHILE @start < @stop begin " & _
                           "    SELECT " & _
                           "       @end   = CHARINDEX(',',@words,@start) " & _
                           "      ,@word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                           "      ,@start = @end+1 " & _
                           "    INSERT @split VALUES (@word) " & _
                           "END " & _
                           "INSERT INTO #EmpMov(EmpId,Ecode,EName,EAppDate) " & _
                           "SELECT " & _
                           "  CAST(word AS INT) " & _
                           " ,hremployee_master.employeecode " & _
                           " ,hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname " & _
                           " ,CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                           "FROM @split " & _
                           "JOIN hremployee_master ON hremployee_master.employeeunkid = [@split].word " & _
                           "ORDER BY CAST(word AS INT) "

            dsList = objDataOperation.ExecQuery(StrCommonQry & "; SELECT * FROM #EmpMov", "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            rpt_Main = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Main.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("EmpId")
                rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                rpt_Rows.Item("Column3") = dtRow.Item("EName")
                rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("EAppDate").ToString).ToShortDateString

                rpt_Main.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next



            If blnShowTransfer Then
                StrQ = "SELECT " & _
                       "  hremployee_transfer_tran.transferunkid " & _
                       ", hremployee_transfer_tran.effectivedate " & _
                       ", hremployee_transfer_tran.employeeunkid " & _
                       ", hremployee_transfer_tran.rehiretranunkid " & _
                       ", hremployee_transfer_tran.stationunkid " & _
                       ", hremployee_transfer_tran.deptgroupunkid " & _
                       ", hremployee_transfer_tran.departmentunkid " & _
                       ", hremployee_transfer_tran.sectiongroupunkid " & _
                       ", hremployee_transfer_tran.sectionunkid " & _
                       ", hremployee_transfer_tran.unitgroupunkid " & _
                       ", hremployee_transfer_tran.unitunkid " & _
                       ", hremployee_transfer_tran.teamunkid " & _
                       ", hremployee_transfer_tran.classgroupunkid " & _
                       ", hremployee_transfer_tran.classunkid " & _
                       ", hremployee_transfer_tran.changereasonunkid " & _
                       ", hremployee_transfer_tran.isfromemployee " & _
                       ", hremployee_transfer_tran.userunkid " & _
                       ", hremployee_transfer_tran.statusunkid " & _
                       ", hremployee_transfer_tran.isvoid " & _
                       ", hremployee_transfer_tran.voiduserunkid " & _
                       ", hremployee_transfer_tran.voiddatetime " & _
                       ", hremployee_transfer_tran.voidreason " & _
                       ", #EmpMov.Ecode " & _
                       ", #EmpMov.EName " & _
                       ", #EmpMov.EmpId " & _
                       ", ISNULL(hrstation_master.name,'') AS branch " & _
                       ", ISNULL(hrdepartment_group_master.name,'') AS deptgroup " & _
                       ", ISNULL(hrdepartment_master.name,'') AS dept " & _
                       ", ISNULL(hrsectiongroup_master.name,'') AS secgroup " & _
                       ", ISNULL(hrsection_master.name,'') AS section " & _
                       ", ISNULL(hrunitgroup_master.name,'') AS unitname " & _
                       ", ISNULL(hrunit_master.name,'') AS unit " & _
                       ", ISNULL(hrteam_master.name,'') AS team " & _
                       ", ISNULL(hrclassgroup_master.name,'') AS classgrp " & _
                       ", ISNULL(hrclasses_master.name,'') AS class " & _
                       ", CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) AS edate " & _
                       ", #EmpMov.EAppDate " & _
                       ", '' AS EffDate " & _
                       ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name, '') END AS CReason " & _
                       "FROM hremployee_transfer_tran " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_transfer_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRANSFERS & _
                       "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_transfer_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                       "    LEFT JOIN hrstation_master on hrstation_master.stationunkid = hremployee_transfer_tran.stationunkid " & _
                       "    LEFT JOIN hrdepartment_group_master on hrdepartment_group_master.deptgroupunkid = hremployee_transfer_tran.deptgroupunkid " & _
                       "    LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_transfer_tran.departmentunkid " & _
                       "    LEFT JOIN hrsectiongroup_master ON hremployee_transfer_tran.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                       "    LEFT JOIN hrsection_master on hrsection_master.sectionunkid = hremployee_transfer_tran.sectionunkid " & _
                       "    LEFT JOIN hrunitgroup_master ON hremployee_transfer_tran.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                       "    LEFT JOIN hrunit_master on hrunit_master.unitunkid = hremployee_transfer_tran.unitunkid " & _
                       "    LEFT JOIN hrteam_master ON hremployee_transfer_tran.teamunkid = hrteam_master.teamunkid " & _
                       "    LEFT JOIN hrclassgroup_master on hrclassgroup_master.classgroupunkid = hremployee_transfer_tran.classgroupunkid " & _
                       "    LEFT JOIN hrclasses_master on hrclasses_master.classesunkid = hremployee_transfer_tran.classunkid " & _
                       "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_transfer_tran.employeeunkid " & _
                       "    JOIN #EmpMov ON hremployee_master.employeeunkid = #EmpMov.EmpId " & _
                       "WHERE isvoid = 0 ORDER BY hremployee_transfer_tran.effectivedate DESC "

                If StrQ.Trim.Length > 0 Then
                    StrQ = StrCommonQry & StrQ
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                rpt_Transfer = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_Transfer.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("EmpId")
                    rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                    rpt_Rows.Item("Column3") = dtRow.Item("EName")
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("EAppDate").ToString).ToShortDateString
                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(dtRow.Item("edate").ToString).ToShortDateString
                    rpt_Rows.Item("Column6") = dtRow.Item("branch")
                    rpt_Rows.Item("Column7") = dtRow.Item("deptgroup")
                    rpt_Rows.Item("Column8") = dtRow.Item("dept")
                    rpt_Rows.Item("Column9") = dtRow.Item("secgroup")
                    rpt_Rows.Item("Column10") = dtRow.Item("section")
                    rpt_Rows.Item("Column11") = dtRow.Item("unitname")
                    rpt_Rows.Item("Column12") = dtRow.Item("unit")
                    rpt_Rows.Item("Column13") = dtRow.Item("team")
                    rpt_Rows.Item("Column14") = dtRow.Item("classgrp")
                    rpt_Rows.Item("Column15") = dtRow.Item("class")
                    rpt_Rows.Item("Column16") = dtRow.Item("CReason")

                    rpt_Transfer.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

            End If

            If blnShowCategorization Then
                StrQ = ""
                StrQ = "SELECT " & _
                      "  categorizationtranunkid " & _
                      ", CONVERT(CHAR(8),hremployee_categorization_tran.effectivedate,112) AS effectivedate " & _
                      ", #EmpMov.EAppDate " & _
                      ", #EmpMov.EmpId " & _
                      ", #EmpMov.Ecode " & _
                      ", #EmpMov.EName " & _
                      ", hremployee_categorization_tran.employeeunkid " & _
                      ", hremployee_categorization_tran.jobgroupunkid " & _
                      ", hremployee_categorization_tran.jobunkid " & _
                      ", hremployee_categorization_tran.gradeunkid " & _
                      ", hremployee_categorization_tran.gradelevelunkid " & _
                      ", hremployee_categorization_tran.changereasonunkid " & _
                      ", hremployee_categorization_tran.isfromemployee " & _
                      ", hremployee_categorization_tran.statusunkid " & _
                      ", hremployee_categorization_tran.userunkid " & _
                      ", hremployee_categorization_tran.isvoid " & _
                      ", hremployee_categorization_tran.voiduserunkid " & _
                      ", hremployee_categorization_tran.voiddatetime " & _
                      ", hremployee_categorization_tran.voidreason " & _
                      ", ISNULL(hrjobgroup_master.name,'') AS JobGroup " & _
                      ", ISNULL(hrjob_master.job_name,'') AS Job " & _
                      ", ISNULL(hrgrade_master.name,'') AS Grade " & _
                      ", ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
                      ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS Reason " & _
                      ", ISNULL(hremployee_categorization_tran.rehiretranunkid,0) AS rehiretranunkid " & _
                      "FROM hremployee_categorization_tran " & _
                      " JOIN hremployee_master on hremployee_master.employeeunkid = hremployee_categorization_tran.employeeunkid  " & _
                      " JOIN #EmpMov ON hremployee_master.employeeunkid = #EmpMov.EmpId " & _
                      " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_categorization_tran.jobgroupunkid " & _
                      " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                      " LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hremployee_categorization_tran.gradeunkid " & _
                      " LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hremployee_categorization_tran.gradelevelunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_categorization_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RECATEGORIZE & _
                      " LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_categorization_tran.changereasonunkid AND RH.mastertype =  " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                      "WHERE 1 = 1 AND hremployee_categorization_tran.isvoid = 0 ORDER BY hremployee_categorization_tran.effectivedate DESC "

                If StrQ.Trim.Length > 0 Then
                    StrQ = StrCommonQry & StrQ
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If


                rpt_Categorization = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_Categorization.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("EmpId")
                    rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                    rpt_Rows.Item("Column3") = dtRow.Item("EName")
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("EAppDate").ToString).ToShortDateString
                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(dtRow.Item("effectivedate").ToString).ToShortDateString
                    rpt_Rows.Item("Column6") = dtRow.Item("JobGroup")
                    rpt_Rows.Item("Column7") = dtRow.Item("Job")
                    rpt_Rows.Item("Column8") = dtRow.Item("Reason")

                    rpt_Categorization.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next
            End If

            If blnShowCostCenter Then
                StrQ = ""
                StrQ = "SELECT " & _
                       "  hremployee_cctranhead_tran.cctranheadunkid " & _
                       ", hremployee_cctranhead_tran.effectivedate " & _
                       ", hremployee_cctranhead_tran.employeeunkid " & _
                       ", hremployee_cctranhead_tran.rehiretranunkid " & _
                       ", hremployee_cctranhead_tran.cctranheadvalueid " & _
                       ", hremployee_cctranhead_tran.istransactionhead " & _
                       ", hremployee_cctranhead_tran.changereasonunkid " & _
                       ", hremployee_cctranhead_tran.isfromemployee " & _
                       ", hremployee_cctranhead_tran.userunkid " & _
                       ", hremployee_cctranhead_tran.statusunkid " & _
                       ", hremployee_cctranhead_tran.isvoid " & _
                       ", hremployee_cctranhead_tran.voiduserunkid " & _
                       ", hremployee_cctranhead_tran.voiddatetime " & _
                       ", hremployee_cctranhead_tran.voidreason " & _
                       ", #EmpMov.EmpId " & _
                       ", #EmpMov.Ecode " & _
                       ", #EmpMov.EName " & _
                       ", CONVERT(CHAR(8),hremployee_cctranhead_tran.effectivedate,112) AS edate " & _
                       ", #EmpMov.EAppDate " & _
                       ", '' AS EffDate " & _
                       ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                       ", CASE WHEN hremployee_cctranhead_tran.istransactionhead = 1 THEN ISNULL(prtranhead_master.trnheadname,'') " & _
                       "    ELSE ISNULL(prcostcenter_master.costcentername,'') END AS DispValue " & _
                       ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                       ", ISNULL(prtranhead_master.trnheadname,'') AS trnheadname " & _
                       "FROM hremployee_cctranhead_tran " & _
                       "  JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_cctranhead_tran.employeeunkid " & _
                       "  JOIN #EmpMov ON hremployee_master.employeeunkid = #EmpMov.EmpId " & _
                       "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_cctranhead_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.COST_CENTER & _
                       "  LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_cctranhead_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                       "  LEFT JOIN prcostcenter_master ON cctranheadvalueid = prcostcenter_master.costcenterunkid " & _
                       "  LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_cctranhead_tran.cctranheadvalueid " & _
                       "WHERE hremployee_cctranhead_tran.isvoid = 0 AND hremployee_cctranhead_tran.istransactionhead = 0 ORDER BY effectivedate DESC "

                If StrQ.Trim.Length > 0 Then
                    StrQ = StrCommonQry & StrQ
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                rpt_CostCenter = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_CostCenter.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("EmpId")
                    rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                    rpt_Rows.Item("Column3") = dtRow.Item("EName")
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("EAppDate").ToString).ToShortDateString
                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(dtRow.Item("edate").ToString).ToShortDateString
                    rpt_Rows.Item("Column6") = dtRow.Item("costcentername")
                    rpt_Rows.Item("Column7") = dtRow.Item("CReason")

                    rpt_CostCenter.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

            End If

            If blnShowDates Then
                StrQ = ""
                Dim xMasterTypeName As String = ""
                Dim StrQ1 As String = "SELECT * FROM ( "
                For Each enumvalue As enEmp_Dates_Transaction In System.Enum.GetValues(GetType(enEmp_Dates_Transaction))
                    Select Case enumvalue
                        Case enEmp_Dates_Transaction.DT_PROBATION, enEmp_Dates_Transaction.DT_CONFIRMATION, enEmp_Dates_Transaction.DT_SUSPENSION, enEmp_Dates_Transaction.DT_TERMINATION, enEmp_Dates_Transaction.DT_RETIREMENT

                            xMasterTypeName = "" : Dim xMasterTypeId As Integer = 0
                            Select Case enumvalue
                                Case enEmp_Dates_Transaction.DT_PROBATION
                                    xMasterTypeName = Language.getMessage(mstrModuleName, 500, "Probation") ' 
                                    xMasterTypeId = clsCommon_Master.enCommonMaster.PROBATION
                                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                    xMasterTypeName = Language.getMessage(mstrModuleName, 501, "Confirmation") '
                                    xMasterTypeId = clsCommon_Master.enCommonMaster.CONFIRMATION
                                Case enEmp_Dates_Transaction.DT_SUSPENSION
                                    xMasterTypeName = Language.getMessage(mstrModuleName, 502, "Suspension") '
                                    xMasterTypeId = clsCommon_Master.enCommonMaster.SUSPENSION
                                Case enEmp_Dates_Transaction.DT_TERMINATION
                                    xMasterTypeName = Language.getMessage(mstrModuleName, 503, "Termination") '
                                    xMasterTypeId = clsCommon_Master.enCommonMaster.TERMINATION
                                Case enEmp_Dates_Transaction.DT_RETIREMENT
                                    xMasterTypeName = Language.getMessage(mstrModuleName, 505, "Retirement") '
                                    xMasterTypeId = clsCommon_Master.enCommonMaster.RETIREMENTS
                            End Select
                            StrQ &= "UNION ALL "
                            StrQ &= "SELECT " & _
                                    "  hremployee_dates_tran.datestranunkid " & _
                                    ", hremployee_dates_tran.effectivedate " & _
                                    ", hremployee_dates_tran.employeeunkid " & _
                                    ", hremployee_dates_tran.rehiretranunkid " & _
                                    ", hremployee_dates_tran.date1 " & _
                                    ", hremployee_dates_tran.date2 " & _
                                    ", hremployee_dates_tran.changereasonunkid " & _
                                    ", hremployee_dates_tran.isfromemployee " & _
                                    ", hremployee_dates_tran.datetypeunkid " & _
                                    ", hremployee_dates_tran.userunkid " & _
                                    ", hremployee_dates_tran.statusunkid " & _
                                    ", hremployee_dates_tran.actionreasonunkid " & _
                                    ", hremployee_dates_tran.isexclude_payroll " & _
                                    ", hremployee_dates_tran.isconfirmed " & _
                                    ", hremployee_dates_tran.otherreason " & _
                                    ", hremployee_dates_tran.isvoid " & _
                                    ", hremployee_dates_tran.voiduserunkid " & _
                                    ", hremployee_dates_tran.voiddatetime " & _
                                    ", hremployee_dates_tran.voidreason " & _
                                    ", #EmpMov.EmpId " & _
                                    ", #EmpMov.Ecode " & _
                                    ", #EmpMov.EName " & _
                                    ", CONVERT(CHAR(8),hremployee_dates_tran.effectivedate,112) AS edate " & _
                                    ", #EmpMov.EAppDate " & _
                                    ", CONVERT(CHAR(8),hremployee_dates_tran.date1,112) AS edate1 " & _
                                    ", CONVERT(CHAR(8),hremployee_dates_tran.date2,112) AS edate2 " & _
                                    ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                                    ", '" & xMasterTypeName & "' AS eTypeName " & _
                                    "FROM hremployee_dates_tran " & _
                                    "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_dates_tran.employeeunkid " & _
                                    "    JOIN #EmpMov ON hremployee_master.employeeunkid = #EmpMov.EmpId " & _
                                    "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_dates_tran.changereasonunkid AND cfcommon_master.mastertype = " & xMasterTypeId & _
                                    "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_dates_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                                    " WHERE isvoid = 0 AND hremployee_dates_tran.datetypeunkid = " & enumvalue & " "
                            If enumvalue = enEmp_Dates_Transaction.DT_TERMINATION Then
                                StrQ &= " AND hremployee_dates_tran.date1 IS NOT NULL "
                            End If
                    End Select
                Next

                StrQ = Mid(StrQ, 10)
                StrQ = StrQ1 & StrQ
                StrQ &= ") AS A WHERE 1 = 1 ORDER BY datetypeunkid ASC, effectivedate DESC "


                If StrQ.Trim.Length > 0 Then
                    StrQ = StrCommonQry & StrQ
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                rpt_Dates = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_Dates.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("EmpId")
                    rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                    rpt_Rows.Item("Column3") = dtRow.Item("EName")
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("EAppDate").ToString).ToShortDateString
                    rpt_Rows.Item("Column5") = dtRow.Item("eTypeName")
                    rpt_Rows.Item("Column6") = eZeeDate.convertDate(dtRow.Item("edate").ToString).ToShortDateString
                    If dtRow.Item("edate1").ToString.Trim.Length > 0 Then
                        rpt_Rows.Item("Column7") = eZeeDate.convertDate(dtRow.Item("edate1").ToString).ToShortDateString
                    End If
                    If dtRow.Item("edate2").ToString.Length > 0 Then
                        rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("edate2").ToString).ToShortDateString
                    End If

                    If CInt(dtRow.Item("datetypeunkid")) = enEmp_Dates_Transaction.DT_TERMINATION Then
                        rpt_Rows.Item("Column9") = dtRow.Item("isexclude_payroll").ToString
                    End If

                    rpt_Rows.Item("Column10") = dtRow.Item("CReason")

                    rpt_Dates.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

            End If

            If blnShowRehire Then
                StrQ = ""
                StrQ = "SELECT " & _
                       "  hremployee_rehire_tran.rehiretranunkid " & _
                       ", hremployee_rehire_tran.effectivedate " & _
                       ", hremployee_rehire_tran.employeeunkid " & _
                       ", hremployee_rehire_tran.reinstatment_date " & _
                       ", hremployee_rehire_tran.changereasonunkid " & _
                       ", hremployee_rehire_tran.isfromemployee " & _
                       ", hremployee_rehire_tran.userunkid " & _
                       ", hremployee_rehire_tran.statusunkid " & _
                       ", hremployee_rehire_tran.isvoid " & _
                       ", hremployee_rehire_tran.voiduserunkid " & _
                       ", hremployee_rehire_tran.voiddatetime " & _
                       ", hremployee_rehire_tran.voidreason " & _
                       ", #EmpMov.EmpId " & _
                       ", #EmpMov.Ecode " & _
                       ", #EmpMov.EName " & _
                       ", CONVERT(CHAR(8),hremployee_rehire_tran.effectivedate,112) AS edate " & _
                       ", #EmpMov.EAppDate " & _
                       ", CONVERT(CHAR(8),hremployee_rehire_tran.reinstatment_date,112) AS redate " & _
                       ", '' AS EffDate " & _
                       ", '' AS rdate " & _
                       ", ISNULL(cfcommon_master.name,'') AS CReason " & _
                       "FROM hremployee_rehire_tran " & _
                       "  JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_rehire_tran.employeeunkid " & _
                       "  JOIN #EmpMov ON hremployee_master.employeeunkid = #EmpMov.EmpId " & _
                       "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_rehire_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                       "WHERE hremployee_rehire_tran.isvoid = 0 ORDER BY effectivedate DESC "

                If StrQ.Trim.Length > 0 Then
                    StrQ = StrCommonQry & StrQ
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                rpt_Rehire = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_Rehire.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("EmpId")
                    rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                    rpt_Rows.Item("Column3") = dtRow.Item("EName")
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("EAppDate").ToString).ToShortDateString
                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(dtRow.Item("edate").ToString).ToShortDateString
                    If dtRow.Item("redate").ToString.Trim.Length > 0 Then
                        rpt_Rows.Item("Column6") = eZeeDate.convertDate(dtRow.Item("redate").ToString).ToShortDateString
                    End If
                    rpt_Rows.Item("Column7") = dtRow.Item("CReason")

                    rpt_Rehire.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

            End If

            If blnShowWorkPermit Then
                StrQ = ""
                StrQ = "SELECT " & _
                       "  hremployee_work_permit_tran.workpermittranunkid " & _
                       ", hremployee_work_permit_tran.effectivedate " & _
                       ", hremployee_work_permit_tran.employeeunkid " & _
                       ", hremployee_work_permit_tran.work_permit_no " & _
                       ", hremployee_work_permit_tran.workcountryunkid " & _
                       ", hremployee_work_permit_tran.issue_place " & _
                       ", hremployee_work_permit_tran.issue_date " & _
                       ", hremployee_work_permit_tran.expiry_date " & _
                       ", hremployee_work_permit_tran.changereasonunkid " & _
                       ", hremployee_work_permit_tran.isfromemployee " & _
                       ", hremployee_work_permit_tran.userunkid " & _
                       ", hremployee_work_permit_tran.statusunkid " & _
                       ", hremployee_work_permit_tran.isvoid " & _
                       ", hremployee_work_permit_tran.voiduserunkid " & _
                       ", hremployee_work_permit_tran.voiddatetime " & _
                       ", hremployee_work_permit_tran.voidreason " & _
                       ", #EmpMov.EmpId " & _
                       ", #EmpMov.Ecode " & _
                       ", #EmpMov.EName " & _
                       ", CONVERT(CHAR(8),hremployee_work_permit_tran.effectivedate,112) AS edate " & _
                       ", #EmpMov.EAppDate " & _
                       ", CONVERT(CHAR(8),hremployee_work_permit_tran.issue_date,112) AS isdate " & _
                       ", CONVERT(CHAR(8),hremployee_work_permit_tran.expiry_date,112) AS exdate " & _
                       ", '' AS EffDate " & _
                       ", '' AS IDate " & _
                       ", '' AS ExDate " & _
                       ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                       ", ISNULL(CM.country_name,'') AS Country " & _
                       ", ISNULL(hremployee_work_permit_tran.rehiretranunkid,0) AS rehiretranunkid " & _
                       "FROM hremployee_work_permit_tran " & _
                       "    LEFT JOIN hrmsConfiguration..cfcountry_master AS CM ON CM.countryunkid = hremployee_work_permit_tran.workcountryunkid " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_work_permit_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.WORK_PERMIT & _
                       "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_work_permit_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                       "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_work_permit_tran.employeeunkid " & _
                       "    JOIN #EmpMov ON hremployee_master.employeeunkid = #EmpMov.EmpId " & _
                       "WHERE isvoid = 0 AND hremployee_work_permit_tran.work_permit_no <> '' ORDER BY effectivedate DESC "


                If StrQ.Trim.Length > 0 Then
                    StrQ = StrCommonQry & StrQ
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                rpt_WorkPermit = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_WorkPermit.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("EmpId")
                    rpt_Rows.Item("Column2") = dtRow.Item("Ecode")
                    rpt_Rows.Item("Column3") = dtRow.Item("EName")
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("EAppDate").ToString).ToShortDateString
                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(dtRow.Item("edate").ToString).ToShortDateString
                    rpt_Rows.Item("Column6") = dtRow.Item("work_permit_no").ToString
                    rpt_Rows.Item("Column7") = dtRow.Item("issue_place").ToString
                    If dtRow.Item("isdate").ToString.Trim.Length > 0 Then
                        rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("isdate").ToString).ToShortDateString
                    End If
                    If dtRow.Item("exdate").ToString.Trim.Length > 0 Then
                        rpt_Rows.Item("Column9") = eZeeDate.convertDate(dtRow.Item("exdate").ToString).ToShortDateString
                    End If
                    rpt_Rows.Item("Column10") = dtRow.Item("Country").ToString
                    rpt_Rows.Item("Column11") = dtRow.Item("CReason").ToString


                    rpt_WorkPermit.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

            End If

            objRpt = New ArutiReport.Designer.rptMissingEmpListReport

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Main.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        IsDisplayLogo, _
                                        ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "")

            rpt_Main.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Main.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Main.Tables("ArutiTable").Rows.Add("")
            End If


            objRpt.SetDataSource(rpt_Main)
            objRpt.Subreports("rptTransfers").SetDataSource(rpt_Transfer)
            objRpt.Subreports("rptCategorization").SetDataSource(rpt_Categorization)
            objRpt.Subreports("rptCostCenter").SetDataSource(rpt_CostCenter)
            objRpt.Subreports("rptDates").SetDataSource(rpt_Dates)
            objRpt.Subreports("rptRehire").SetDataSource(rpt_Rehire)
            objRpt.Subreports("rptWorkPermit").SetDataSource(rpt_WorkPermit)

            Call ReportFunction.TextChange(objRpt, "txtMECode", Language.getMessage(mstrModuleName, 100, "Employee Code :"))
            Call ReportFunction.TextChange(objRpt, "txtMEName", Language.getMessage(mstrModuleName, 101, "Employee Name :"))
            Call ReportFunction.TextChange(objRpt, "txtMAppDate", Language.getMessage(mstrModuleName, 102, "Appointment Date :"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTEmpTransfer", Language.getMessage(mstrModuleName, 103, "Employee Transfer"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTEffDate", Language.getMessage(mstrModuleName, 104, "Effective Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTBranch", Language.getMessage(mstrModuleName, 105, "Branch"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTDeptGrp", Language.getMessage(mstrModuleName, 106, "Department Group"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTDept", Language.getMessage(mstrModuleName, 107, "Department"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTSecGrp", Language.getMessage(mstrModuleName, 108, "Section Group"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTSec", Language.getMessage(mstrModuleName, 109, "Section"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTUnitGrp", Language.getMessage(mstrModuleName, 110, "Unit Group"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtUnit", Language.getMessage(mstrModuleName, 111, "Unit"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTTeam", Language.getMessage(mstrModuleName, 112, "Team"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTClsGrp", Language.getMessage(mstrModuleName, 113, "Class Group"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTCls", Language.getMessage(mstrModuleName, 114, "Class"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTransfers"), "txtTReason", Language.getMessage(mstrModuleName, 115, "Reason"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptCategorization"), "txtREmpCategorization", Language.getMessage(mstrModuleName, 116, "Employee Recategorization"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCategorization"), "txtREffDate", Language.getMessage(mstrModuleName, 104, "Effective Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCategorization"), "txtRJobGrp", Language.getMessage(mstrModuleName, 117, "Job Group"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCategorization"), "txtRJob", Language.getMessage(mstrModuleName, 118, "Job Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCategorization"), "txtRReason", Language.getMessage(mstrModuleName, 115, "Reason"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptCostCenter"), "txtCEmpCostCenter", Language.getMessage(mstrModuleName, 119, "Employee CostCenter"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCostCenter"), "txtCEffDate", Language.getMessage(mstrModuleName, 104, "Effective Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCostCenter"), "txtCCName", Language.getMessage(mstrModuleName, 120, "Cost-Center"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptCostCenter"), "txtCReason", Language.getMessage(mstrModuleName, 115, "Reason"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptDates"), "txtDEmpDates", Language.getMessage(mstrModuleName, 121, "Employee Dates"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptDates"), "txtDEffDate", Language.getMessage(mstrModuleName, 104, "Effective Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptDates"), "txtDDate1", Language.getMessage(mstrModuleName, 122, "Date1"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptDates"), "txtDDate2", Language.getMessage(mstrModuleName, 123, "Date2"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptDates"), "txtDExPayroll", Language.getMessage(mstrModuleName, 124, "Payroll Excluded"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptDates"), "txtDReason", Language.getMessage(mstrModuleName, 115, "Reason"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptRehire"), "txtHEmpRehire", Language.getMessage(mstrModuleName, 125, "Employee Re-Hired"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptRehire"), "txtHEffDate", Language.getMessage(mstrModuleName, 104, "Effective Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptRehire"), "txtHRehireDate", Language.getMessage(mstrModuleName, 126, "Re-Hire Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptRehire"), "txtHReason", Language.getMessage(mstrModuleName, 115, "Reason"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWEmpPermit", Language.getMessage(mstrModuleName, 127, "Employee Work Permit"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWEffDate", Language.getMessage(mstrModuleName, 104, "Effective Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWPNo", Language.getMessage(mstrModuleName, 128, "Permit No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWIssuePlace", Language.getMessage(mstrModuleName, 129, "Issue Place"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWIssueDate", Language.getMessage(mstrModuleName, 130, "Issue Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWExpiryDate", Language.getMessage(mstrModuleName, 131, "Expiry Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWCountry", Language.getMessage(mstrModuleName, 132, "Country"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptWorkPermit"), "txtWReason", Language.getMessage(mstrModuleName, 115, "Reason"))

            ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 133, "Printed By :"))
            ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 134, "Printed Date :"))
            ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 135, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", strReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)


            If blnShowTransfer = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
            End If

            If blnShowCategorization = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
            End If

            If blnShowCostCenter = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", True)
            End If

            If blnShowDates = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", True)
            End If

            If blnShowRehire = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection6", True)
            End If

            If blnShowWorkPermit = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection7", True)
            End If


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, ePrint, eExport, strExportPath, blnOpenAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DisplayEmployeeMovements; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [03-APR-2017] -- END

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Public Function ViewEmployeeDetails(ByVal intEmployeeId As Integer, _
                                        ByVal blnFinYearLeave As Boolean, _
                                        ByVal strFmtCurrency As String, _
                                        ByVal ShowFirstAppointmentDate As Boolean, _
                                        ByVal AllowTo_View_Scale As Boolean, _
                                        ByVal EmpMandatoryFieldsIDs As String, _
                                        ByVal PendingEmployeeScreenIDs As String, _
                                        ByVal mdtEmpAsonDate As Date, _
                                        Optional ByVal IncludeIdInfo As Boolean = False, _
                                        Optional ByVal IncludeMembershipInfo As Boolean = False, _
                                        Optional ByVal IncludeIdOtherInfo As Boolean = False, _
                                        Optional ByVal blnAddMandatoryInformation As Boolean = False) As String

        'Pinkal (18-Aug-2018) --  'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mdtEmpAsonDate As Date, _]

        Dim strBuilder As New StringBuilder()
        Dim dsList As New DataSet
        Dim StrQ As String = ""

        Dim strCaption As String = ""
        Dim strMandatoryCSS = "color:red;"
        Dim blnNoRows As Boolean = False
        Try
            Using objDataOperation As New clsDataOperation

                Dim dctMandatoryField As New Dictionary(Of clsEmployee_Master.EmpColEnum, String)
                Dim objEmployee As New clsEmployee_Master
                dctMandatoryField = objEmployee.PrepareMandatoryFieldCollection(ShowFirstAppointmentDate, AllowTo_View_Scale, IncludeIdInfo, IncludeMembershipInfo, IncludeIdOtherInfo, blnAddMandatoryInformation)
                objEmployee = Nothing

                Dim mFields() As String = EmpMandatoryFieldsIDs.Trim().Split(CChar(","))
                Dim mScreen() As String = PendingEmployeeScreenIDs.Trim().Split(CChar(","))

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
                objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmEmployeeMaster", 902, "Yes"))
                objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmEmployeeMaster", 903, "No"))
                objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Assets_Tran", 3, "Assigned"))
                objDataOperation.AddParameter("@Returned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Assets_Tran", 4, "Returned"))
                objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Assets_Tran", 5, "WrittenOff"))
                objDataOperation.AddParameter("@Lost", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Assets_Tran", 6, "Lost"))

                strBuilder.Append("<HTML> " & vbCrLf)
                strBuilder.Append("<HEAD>" & vbCrLf)
                strBuilder.Append("<TITLE>" & Language.getMessage("frmEmployeeMaster", 904, "Employee Details") & "</TITLE> " & vbCrLf)
                strBuilder.Append("</HEAD>" & vbCrLf)
                strBuilder.Append(" <style> body { background: rgb(255,255,255); } .sectionheader { font-size: 14px; font-weight: 600; background-color: rgb(224, 224, 224); " & _
                                  "text-align: center;} .sectionpanel { padding-left: 20px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; } " & _
                                  "#page { background: white; display: block; margin: 0 auto;margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgba(0,0,0,0.5); } " & _
                                  "#page[size='A4'] {width: 21cm; min-height: 29.7cm; } #page[size='A4'][layout='portrait'] { width: 29.7cm; height: 21cm;} " & _
                                  "#page[size='A3'] {width: 29.7cm;height: 42cm;}#page[size='A3'][layout='portrait'] {width: 42cm;height: 29.7cm; } " & _
                                  "#page[size='A5'] { width: 14.8cm; height: 21cm; } #page[size='A5'][layout='portrait'] {width: 21cm; height: 14.8cm; } " & _
                                  "@media print { body, #page { margin: 0;box-shadow: 0; }} .gridHeader > td, .gridItem > td {padding: 5px; }.gridHeader { background-color: rgb(175, 175, 175);font-weight: bold; }</style>" & vbCrLf)
                strBuilder.Append("<BODY>" & vbCrLf)
                strBuilder.Append("<FORM ID = 'form1' autocomplete='off' style='font-family:Tahoma, sans-serif'>" & vbCrLf)
                strBuilder.Append("<DIV ID='PAGE' SIZE='A4'>" & vbCrLf)

                Dim strcommon As String = "DECLARE @Cols AS NVARCHAR(MAX),@oSQL AS NVARCHAR(MAX),@EmpId AS INT, @IsFinYear AS BIT " & _
                                          "SET @EmpId = " & intEmployeeId & ";SET @IsFinYear = " & IIf(blnFinYearLeave = True, 1, 0)

                StrQ = "IF OBJECT_ID('tempdb..##SerialNumber') IS NOT NULL " & _
                       "DROP TABLE ##SerialNumber " & _
                       "SELECT " & _
                       "    A.sNo INTO ##SerialNumber " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "        TOP 100000 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS sNo " & _
                       "    FROM SYS.COLUMNS A CROSS JOIN SYS.COLUMNS B " & _
                       ") AS A "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#MyTempTable') IS NOT NULL " & _
                        "DROP TABLE #MyTempTable " & _
                        "SELECT " & _
                        "     CAST(EM.employeecode AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Code & "] " & _
                        "    ,CAST(ISNULL(Title.name,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Title & "] " & _
                        "    ,CAST(ISNULL(EM.firstname,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_FirstName & "] " & _
                        "    ,CAST(ISNULL(EM.surname,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Surname & "] " & _
                        "    ,CAST(ISNULL(EM.othername,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Othername & "] " & _
                        "    ,CAST(ISNULL(EmplType.name,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "] " & _
                        "    ,CAST(ISNULL(PayType.name,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Pay_Type & "] " & _
                        "    ,CAST(CASE WHEN CAST(EM.gender AS NVARCHAR(1)) = '1' THEN @Male WHEN CAST(EM.gender AS NVARCHAR(1)) = '2' THEN @Female ELSE '' END AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Gender_Type & "] " & _
                        "    ,CAST(ISNULL(paypointname,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Pay_Point & "] " & _
                        "    ,CAST(ISNULL(shiftname,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_ShiftName & "] " & _
                        "    ,CAST(ISNULL(policyname,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_PolicyName & "] " & _
                        "    ,CAST(ISNULL(EM.loginname,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Login_Name & "] " & _
                        "    ,CAST(ISNULL(EM.displayname,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Display_Name & "] " & _
                        "    ,ISNULL(CAST(EM.email AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Email & "] " & _
                        "    ,CAST(ISNULL(EM.remark,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Remark & "] " & _
                        "    INTO #MyTempTable " & _
                        "FROM hremployee_master AS EM " & _
                        "    LEFT JOIN hremployee_policy_tran ON EM.employeeunkid = hremployee_policy_tran.employeeunkid " & _
                        "    LEFT JOIN tnapolicy_master ON hremployee_policy_tran.policyunkid = tnapolicy_master.policyunkid " & _
                        "    LEFT JOIN hremployee_shift_tran ON EM.employeeunkid = hremployee_shift_tran.employeeunkid " & _
                        "    LEFT JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                        "    LEFT JOIN prpaypoint_master ON EM.paypointunkid = prpaypoint_master.paypointunkid " & _
                        "    LEFT JOIN cfcommon_master AS Title ON Title.masterunkid = EM.titleunkid " & _
                        "    LEFT JOIN cfcommon_master AS EmplType ON EmplType.masterunkid = EM.employmenttypeunkid " & _
                        "    LEFT JOIN cfcommon_master AS PayType ON PayType.masterunkid = EM.paytypeunkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#MyTempTable') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "    SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #MyTempTable Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "    DECLARE @Cnt AS INT;SET @Cnt = 6 " & _
                        "    SELECT " & _
                        "         ISNULL(DT1.Caption,'''') AS Caption " & _
                        "        ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "        ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "        ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "        ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "        ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "    FROM ##SerialNumber SN " & _
                        "        LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "        LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "        LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "    WHERE SN.SNO <= @Cnt' " & _
                        "    EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                'S.SANDEEP [14-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                strBuilder.Append("<DIV ID='Note' Class='sectionpanel'>")
                strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & Language.getMessage(mstrModuleName, 500, "NOTE: Red Color indicates, Mandatory Information(s) set in configuration are not provided for the employee.") & "</SPAN>" & vbCrLf)
                strBuilder.Append("</DIV>")
                'S.SANDEEP [14-AUG-2018] -- END

                strBuilder.Append("<DIV ID='BasicDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 905, "Basic Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#AllocTab') IS NOT NULL " & _
                        "DROP TABLE #AllocTab " & _
                        "SELECT " & _
                        "    ISNULL(SM.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Station & "] " & _
                        "   ,ISNULL(DG.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Dept_Group & "] " & _
                        "   ,ISNULL(DM.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Department & "] " & _
                        "   ,ISNULL(SG.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Section_Group & "] " & _
                        "   ,ISNULL(SC.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Section & "] " & _
                        "   ,ISNULL(UG.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Unit_Group & "] " & _
                        "   ,ISNULL(UM.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Unit & "] " & _
                        "   ,ISNULL(TM.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Team & "] " & _
                        "   ,ISNULL(GG.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Grade_Group & "] " & _
                        "   ,ISNULL(GM.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Grade & "] " & _
                        "   ,ISNULL(GL.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Grade_Level & "] "
                If AllowTo_View_Scale Then
                    StrQ &= "   ,CAST(ISNULL(SL.newscale,0) AS NVARCHAR(255)) AS [" & clsEmployee_Master.EmpColEnum.Col_Scale & "] "
                End If
                StrQ &= "   ,ISNULL(JG.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Job_Group & "] " & _
                        "   ,ISNULL(JM.job_name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Job & "] " & _
                        "   ,ISNULL(CG.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Class_Group & "] " & _
                        "   ,ISNULL(CC.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Class & "] " & _
                        "   ,ISNULL(CT.costcentername,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Cost_Center & "] " & _
                        "   ,ISNULL(HM.trnheadname,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Transaction_Head & "] " & _
                        "   INTO #AllocTab " & _
                        "FROM hremployee_master EM " & _
                        "   LEFT JOIN hrstation_master AS SM ON EM.stationunkid = SM.stationunkid " & _
                        "   LEFT JOIN hrdepartment_group_master AS DG ON DG.deptgroupunkid = EM.deptgroupunkid " & _
                        "   JOIN hrdepartment_master DM ON DM.departmentunkid = EM.departmentunkid " & _
                        "   LEFT JOIN hrsectiongroup_master AS SG ON SG.sectiongroupunkid = EM.sectiongroupunkid " & _
                        "   LEFT JOIN hrsection_master AS SC ON SC.sectionunkid = EM.sectionunkid " & _
                        "   LEFT JOIN hrunitgroup_master AS UG ON UG.unitgroupunkid = EM.unitgroupunkid " & _
                        "   LEFT JOIN hrunit_master AS UM ON UM.unitunkid = EM.unitunkid " & _
                        "   LEFT JOIN hrteam_master AS TM ON TM.teamunkid = EM.teamunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            gradegroupunkid " & _
                        "           ,gradeunkid " & _
                        "           ,gradelevelunkid " & _
                        "           ,newscale " & _
                        "           ,employeeunkid " & _
                        "       FROM prsalaryincrement_tran " & _
                        "       WHERE employeeunkid = @EmpId AND isfromemployee = 1 " & _
                        "   ) AS SL ON SL.employeeunkid = EM.employeeunkid " & _
                        "   LEFT JOIN hrgradegroup_master AS GG ON GG.gradegroupunkid = SL.gradegroupunkid " & _
                        "   LEFT JOIN hrgrade_master AS GM ON GM.gradeunkid = SL.gradeunkid " & _
                        "   LEFT JOIN hrgradelevel_master AS GL ON GL.gradelevelunkid = SL.gradelevelunkid " & _
                        "   LEFT JOIN hrjobgroup_master AS JG ON JG.jobgroupunkid = EM.jobgroupunkid " & _
                        "   LEFT JOIN hrjob_master AS JM ON JM.jobunkid = EM.jobunkid " & _
                        "   LEFT JOIN hrclassgroup_master AS CG ON CG.classgroupunkid = EM.classgroupunkid " & _
                        "   LEFT JOIN hrclasses_master AS CC ON CC.classesunkid = EM.classunkid " & _
                        "   LEFT JOIN prcostcenter_master AS CT ON CT.costcenterunkid = EM.costcenterunkid " & _
                        "   LEFT JOIN prtranhead_master AS HM ON HM.tranheadunkid = EM.tranhedunkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#AllocTab') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #AllocTab Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 6 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='AllocationDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 906, "Allocation Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & Format(CDec(row(col).ToString()), strFmtCurrency) & "</SPAN>" & vbCrLf)
                                Catch ex As Exception
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next
                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(CM.country_name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Country & "] " & _
                        "   ,ISNULL(ST.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_State & "] " & _
                        "   ,ISNULL(CY.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_City & "] " & _
                        "   ,ISNULL(EM.birth_ward,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Ward & "] " & _
                        "   ,ISNULL(EM.birthcertificateno,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Certificate_No & "] " & _
                        "   ,ISNULL(EM.birth_village,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Village & "] " & _
                        "   ,ISNULL(T1.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Town1 & "] " & _
                        "   ,ISNULL(CF.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Chiefdom & "] " & _
                        "   ,ISNULL(V1.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Village1 & "] " & _
                        "FROM hremployee_master AS EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS CM ON CM.countryunkid = EM.birthcountryunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfstate_master AS ST ON ST.stateunkid = EM.birthstateunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfcity_master AS CY ON CY.cityunkid = EM.birthcityunkid " & _
                        "   LEFT JOIN cfcommon_master AS T1 ON T1.masterunkid = EM.birthtownunkid " & _
                        "   LEFT JOIN cfcommon_master AS CF ON CF.masterunkid = EM.birthchiefdomunkid " & _
                        "   LEFT JOIN cfcommon_master AS V1 ON V1.masterunkid = EM.birthvillageunkid " & _
                        "WHERE EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='BirthInfoDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 907, "Birth Info Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Try
                        Dim strfilter = "[" & col.ColumnName & "] IS NULL OR [" & col.ColumnName & "] = '' "
                        strCaption = ""
                        strCaption = dctMandatoryField(CType(CInt(col.ColumnName), clsEmployee_Master.EmpColEnum))
                        If Array.IndexOf(mFields, col.ColumnName) <> -1 Then
                            If blnNoRows = True Then
                                strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                            Else
                                If dsList.Tables("Data").Select(strfilter).Length > 0 Then

                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                Else
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                End If
                            End If
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                        End If
                    Catch ex As Exception
                        strCaption = ""
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName.ToString() & "</SPAN>" & vbCrLf)
                    End Try
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(EM.work_permit_no,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Work_Permit_No & "] " & _
                        "   ,ISNULL(CM.country_name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Work_Country & "] " & _
                        "   ,ISNULL(EM.work_permit_issue_place,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Work_Permit_Issue_Place & "] " & _
                        "   ,ISNULL(CONVERT(CHAR(8),EM.work_permit_issue_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Work_Permit_Issue_Date & "] " & _
                        "   ,ISNULL(CONVERT(CHAR(8),EM.work_permit_expiry_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Work_Permit_Expiry_Date & "] " & _
                        "FROM hremployee_master AS EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS CM ON CM.countryunkid = EM.workcountryunkid " & _
                        "WHERE EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='wpDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 909, "Work Permit Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Try
                        Dim strfilter = "[" & col.ColumnName & "] IS NULL OR [" & col.ColumnName & "] = '' "
                        strCaption = ""
                        strCaption = dctMandatoryField(CType(CInt(col.ColumnName), clsEmployee_Master.EmpColEnum))
                        If Array.IndexOf(mFields, col.ColumnName) <> -1 Then
                            If blnNoRows = True Then
                                strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                            Else
                                If dsList.Tables("Data").Select(strfilter).Length > 0 Then

                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                Else
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                End If
                            End If
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                        End If
                    Catch ex As Exception
                        strCaption = ""
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName.ToString() & "</SPAN>" & vbCrLf)
                    End Try
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        Try
                            If row(col).ToString().Trim.Length <= 0 Then
                                Throw New Exception
                            End If
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                        Catch ex As Exception
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        End Try
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(EM.resident_permit_no,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Resident_Permit_No & "] " & _
                        "   ,ISNULL(CM.country_name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Resident_Permit_Issue_Country & "] " & _
                        "   ,ISNULL(EM.resident_permit_issue_place,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Resident_Permit_Issue_Place & "] " & _
                        "   ,ISNULL(CONVERT(CHAR(8),EM.work_permit_issue_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Resident_Permit_Issue_Date & "] " & _
                        "   ,ISNULL(CONVERT(CHAR(8),EM.work_permit_expiry_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Resident_Permit_Expiry_Date & "] " & _
                        "FROM hremployee_master AS EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS CM ON CM.countryunkid = EM.residentcountryunkid " & _
                        "WHERE EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='rpDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 910, "Resident Permit Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Try
                        Dim strfilter = "[" & col.ColumnName & "] IS NULL OR [" & col.ColumnName & "] = '' "
                        strCaption = ""
                        strCaption = dctMandatoryField(CType(CInt(col.ColumnName), clsEmployee_Master.EmpColEnum))
                        If Array.IndexOf(mFields, col.ColumnName) <> -1 Then
                            If blnNoRows = True Then
                                strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                            Else
                                If dsList.Tables("Data").Select(strfilter).Length > 0 Then

                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                Else
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                End If
                            End If
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                        End If
                    Catch ex As Exception
                        strCaption = ""
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName.ToString() & "</SPAN>" & vbCrLf)
                    End Try
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        Try
                            If row(col).ToString().Trim.Length <= 0 Then
                                Throw New Exception
                            End If
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                        Catch ex As Exception
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        End Try
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#Dates') IS NOT NULL " & _
                        "DROP TABLE #Dates " & _
                        "SELECT "
                If ShowFirstAppointmentDate Then
                    StrQ &= "    ISNULL(CONVERT(NVARCHAR(MAX),EM.firstappointment_date ,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_FirstAppointmentDate & "] " & _
                            "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.appointeddate,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "] "
                Else
                    StrQ &= "    ISNULL(CONVERT(NVARCHAR(MAX),EM.appointeddate,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "] "
                End If

                StrQ &= "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.confirmation_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Confirmation_Reason & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.birthdate,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Birth_Date & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.probation_from_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Probation_From_Date & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.probation_to_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Probation_To_Date & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.suspended_from_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Suspended_From_Date & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.suspended_to_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Suspended_To_Date & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.empl_enddate,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_EOC_Date & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.termination_from_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Leaving_Date & "] " & _
                        "   ,ISNULL(CAST(TR.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Reason & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.termination_to_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Retirement_Date & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(MAX),EM.reinstatement_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Reinstatement_Date & "] " & _
                        "   INTO #Dates " & _
                        "FROM hremployee_master AS EM " & _
                        "   LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = EM.actionreasonunkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#Dates') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #Dates Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 5 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='DatesDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 911, "Date(s) Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    If row(col).ToString().Trim.Length <= 0 Then
                                        Throw New Exception
                                    End If
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                                Catch ex As Exception
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#PresentTab') IS NOT NULL " & _
                        "DROP TABLE #PresentTab " & _
                        "SELECT " & _
                        "    ISNULL(CAST(EM.present_address1 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Address1 & "] " & _
                        "   ,ISNULL(CAST(EM.present_address2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Address2 & "] " & _
                        "   ,ISNULL(CAST(PC.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Country & "] " & _
                        "   ,ISNULL(CAST(PS.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_State & "] " & _
                        "   ,ISNULL(CAST(PY.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Post_Town & "] " & _
                        "   ,ISNULL(CAST(PZ.zipcode_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Postcode & "] " & _
                        "   ,ISNULL(CAST(EM.present_provicnce AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Provicnce & "] " & _
                        "   ,ISNULL(CAST(EM.present_road AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Road & "] " & _
                        "   ,ISNULL(CAST(EM.present_estate AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Estate & "] " & _
                        "   ,ISNULL(CAST(PR.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Province_Region1 & "] " & _
                        "   ,ISNULL(CAST(PT.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Road_Street1 & "] " & _
                        "   ,ISNULL(CAST(PF.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Cheifdom & "] " & _
                        "   ,ISNULL(CAST(PV.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Village & "] " & _
                        "   ,ISNULL(CAST(PO.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Town1 & "] " & _
                        "   ,ISNULL(CAST(EM.present_mobile AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Mobile & "] " & _
                        "   ,ISNULL(CAST(EM.present_tel_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Tel_No & "] " & _
                        "   ,ISNULL(CAST(EM.present_plotNo AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Plot_No & "] " & _
                        "   ,ISNULL(CAST(EM.present_alternateno AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Alternate_No & "] " & _
                        "   ,ISNULL(CAST(EM.present_email AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Email & "] " & _
                        "   ,ISNULL(CAST(EM.present_fax AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Present_Fax & "] " & _
                        "   INTO #PresentTab " & _
                        "FROM hremployee_master AS EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS PC ON PC.countryunkid = EM.present_countryunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfstate_master AS PS ON PS.stateunkid = EM.present_stateunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfcity_master AS PY ON PY.cityunkid = EM.present_post_townunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfzipcode_master AS PZ ON PZ.zipcodeunkid = EM.present_postcodeunkid " & _
                        "   LEFT JOIN cfcommon_master AS PR ON PR.masterunkid = EM.present_provinceunkid " & _
                        "   LEFT JOIN cfcommon_master AS PT ON PT.masterunkid = EM.present_roadunkid " & _
                        "   LEFT JOIN cfcommon_master AS PF ON PF.masterunkid = EM.present_chiefdomunkid " & _
                        "   LEFT JOIN cfcommon_master AS PV ON PV.masterunkid = EM.present_villageunkid " & _
                        "   LEFT JOIN cfcommon_master AS PO ON PO.masterunkid = EM.present_town1unkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#PresentTab') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #PresentTab Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 7 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='pAddressDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 912, "Present Address Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#DomicileTab') IS NOT NULL " & _
                        "DROP TABLE #DomicileTab " & _
                        "SELECT " & _
                        "    ISNULL(CAST(EM.domicile_address1 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Address1 & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_address2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Address2 & "] " & _
                        "   ,ISNULL(CAST(PC.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Country & "] " & _
                        "   ,ISNULL(CAST(PS.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_State & "] " & _
                        "   ,ISNULL(CAST(PY.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Post_Town & "] " & _
                        "   ,ISNULL(CAST(PZ.zipcode_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Postcode & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_provicnce AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Provicnce & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_road AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Road & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_estate AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Estate & "] " & _
                        "   ,ISNULL(CAST(PR.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Province_Region1 & "] " & _
                        "   ,ISNULL(CAST(PT.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Road_Street1 & "] " & _
                        "   ,ISNULL(CAST(PF.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Chiefdom & "] " & _
                        "   ,ISNULL(CAST(PV.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Village & "] " & _
                        "   ,ISNULL(CAST(PO.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Town1 & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_mobile AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_tel_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Tel_No & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_plotNo AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_PlotNo & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_alternateno AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Alternate_No & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_email AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Email & "] " & _
                        "   ,ISNULL(CAST(EM.domicile_fax AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Domicile_Fax & "] " & _
                        "   INTO #DomicileTab " & _
                        "FROM hremployee_master AS EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS PC ON PC.countryunkid = EM.domicile_countryunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfstate_master AS PS ON PS.stateunkid = EM.domicile_stateunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfcity_master AS PY ON PY.cityunkid = EM.domicile_post_townunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfzipcode_master AS PZ ON PZ.zipcodeunkid = EM.domicile_postcodeunkid " & _
                        "   LEFT JOIN cfcommon_master AS PR ON PR.masterunkid = EM.domicile_provinceunkid " & _
                        "   LEFT JOIN cfcommon_master AS PT ON PT.masterunkid = EM.domicile_roadunkid " & _
                        "   LEFT JOIN cfcommon_master AS PF ON PF.masterunkid = EM.domicile_chiefdomunkid " & _
                        "   LEFT JOIN cfcommon_master AS PV ON PV.masterunkid = EM.domicile_villageunkid " & _
                        "   LEFT JOIN cfcommon_master AS PO ON PO.masterunkid = EM.domicile_town1unkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#DomicileTab') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #DomicileTab Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 7 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='dAddressDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 913, "Domicile Address Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#RecruitTab') IS NOT NULL " & _
                        "DROP TABLE #RecruitTab " & _
                        "SELECT " & _
                        "    ISNULL(CAST(EM.rc_address1 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Address1 & "] " & _
                        "   ,ISNULL(CAST(EM.rc_address2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Address2 & "] " & _
                        "   ,ISNULL(CAST(PC.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Country & "] " & _
                        "   ,ISNULL(CAST(PS.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_State & "] " & _
                        "   ,ISNULL(CAST(PY.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Post_Town & "] " & _
                        "   ,ISNULL(CAST(PZ.zipcode_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Postcode & "] " & _
                        "   ,ISNULL(CAST(EM.rc_province AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Provicnce & "] " & _
                        "   ,ISNULL(CAST(EM.rc_road AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Road & "] " & _
                        "   ,ISNULL(CAST(EM.rc_estate AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Estate & "] " & _
                        "   ,ISNULL(CAST(PR.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Province_Region1 & "] " & _
                        "   ,ISNULL(CAST(PT.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Road_Street1 & "] " & _
                        "   ,ISNULL(CAST(PF.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Chiefdom & "] " & _
                        "   ,ISNULL(CAST(PV.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Village & "] " & _
                        "   ,ISNULL(CAST(PO.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Town1 & "] " & _
                        "   ,ISNULL(CAST(EM.rc_plotno AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_PlotNo & "] " & _
                        "   ,ISNULL(CAST(EM.rc_tel_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Recruitment_Tel_No & "] " & _
                        "   INTO #RecruitTab " & _
                        "FROM hremployee_master AS EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS PC ON PC.countryunkid = EM.rc_countryunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfstate_master AS PS ON PS.stateunkid = EM.rc_stateunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfcity_master AS PY ON PY.cityunkid = EM.rc_post_townunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfzipcode_master AS PZ ON PZ.zipcodeunkid = EM.rc_postcodeunkid " & _
                        "   LEFT JOIN cfcommon_master AS PR ON PR.masterunkid = EM.recruitment_provinceunkid " & _
                        "   LEFT JOIN cfcommon_master AS PT ON PT.masterunkid = EM.recruitment_roadunkid " & _
                        "   LEFT JOIN cfcommon_master AS PF ON PF.masterunkid = EM.recruitment_chiefdomunkid " & _
                        "   LEFT JOIN cfcommon_master AS PV ON PV.masterunkid = EM.recruitment_villageunkid " & _
                        "   LEFT JOIN cfcommon_master AS PO ON PO.masterunkid = EM.recruitment_town1unkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#RecruitTab') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #RecruitTab Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 7 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='rAddressDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 914, "Recruitment Address Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#EContact1') IS NOT NULL " & _
                        "DROP TABLE #EContact1 " & _
                        "SELECT " & _
                        "    ISNULL(CAST(EM.emer_con_firstname AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Firstname & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_lastname AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Lastname & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_address AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Address & "] " & _
                        "   ,ISNULL(CAST(EC.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Country & "] " & _
                        "   ,ISNULL(CAST(ES.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_State & "] " & _
                        "   ,ISNULL(CAST(EY.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Post_Town & "] " & _
                        "   ,ISNULL(CAST(EZ.zipcode_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Postcode & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_provicnce AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Provicnce & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_road AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Road & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_estate AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Estate & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_plotno AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_PlotNo & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_mobile AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Mobile & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_alternateno AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Alternate_No & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_tel_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Tel_No & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_fax AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Fax & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_email AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Email & "] " & _
                        "INTO #EContact1 " & _
                        "FROM hremployee_master EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS EC ON EC.countryunkid = EM.emer_con_countryunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfstate_master AS ES ON ES.stateunkid = EM.emer_con_state " & _
                        "   LEFT JOIN hrmsConfiguration..cfcity_master AS EY ON EY.cityunkid = EM.emer_con_post_townunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfzipcode_master AS EZ ON EZ.zipcodeunkid = EM.emer_con_postcodeunkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#EContact1') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #EContact1 Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 6 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='ec1Details' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 915, "Emergency Contact Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#EContact2') IS NOT NULL " & _
                        "DROP TABLE #EContact2 " & _
                        "SELECT " & _
                        "    ISNULL(CAST(EM.emer_con_firstname2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Firstname2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_lastname2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Lastname2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_address2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Address2 & "] " & _
                        "   ,ISNULL(CAST(EC.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Country2 & "] " & _
                        "   ,ISNULL(CAST(ES.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_State2 & "] " & _
                        "   ,ISNULL(CAST(EY.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Post_Town2 & "] " & _
                        "   ,ISNULL(CAST(EZ.zipcode_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Postcode2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_provicnce2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Provicnce2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_road2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Road2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_estate2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Estate2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_plotno2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Plotno2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_mobile2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Mobile2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_alternateno2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Alternate_No2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_tel_no2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Tel_No2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_fax2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Fax2 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_email2 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Email2 & "] " & _
                        "   INTO #EContact2 " & _
                        "FROM hremployee_master EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS EC ON EC.countryunkid = EM.emer_con_countryunkid2 " & _
                        "   LEFT JOIN hrmsConfiguration..cfstate_master AS ES ON ES.stateunkid = EM.emer_con_state2 " & _
                        "   LEFT JOIN hrmsConfiguration..cfcity_master AS EY ON EY.cityunkid = EM.emer_con_post_townunkid2 " & _
                        "   LEFT JOIN hrmsConfiguration..cfzipcode_master AS EZ ON EZ.zipcodeunkid = EM.emer_con_postcodeunkid2 " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#EContact2') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #EContact2 Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 6 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='ec2Details' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 916, "Emergency Contact2 Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#EContact3') IS NOT NULL " & _
                        "DROP TABLE #EContact3 " & _
                        "SELECT " & _
                        "    ISNULL(CAST(EM.emer_con_firstname3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Firstname3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_lastname3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Lastname3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_address3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Address3 & "] " & _
                        "   ,ISNULL(CAST(EC.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Country3 & "] " & _
                        "   ,ISNULL(CAST(ES.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_State3 & "] " & _
                        "   ,ISNULL(CAST(EY.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Post_Town3 & "] " & _
                        "   ,ISNULL(CAST(EZ.zipcode_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emergency_Postcode3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_provicnce3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Provicnce3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_road3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Road3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_estate3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Estate3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_plotno3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_plotno3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_mobile3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Mobile3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_alternateno3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Alternate_No3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_tel_no3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Tel_No3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_fax3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Fax3 & "] " & _
                        "   ,ISNULL(CAST(EM.emer_con_email3 AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Emer_Con_Email3 & "] " & _
                        "   INTO #EContact3 " & _
                        "FROM hremployee_master EM " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS EC ON EC.countryunkid = EM.emer_con_countryunkid3 " & _
                        "   LEFT JOIN hrmsConfiguration..cfstate_master AS ES ON ES.stateunkid = EM.emer_con_state3 " & _
                        "   LEFT JOIN hrmsConfiguration..cfcity_master AS EY ON EY.cityunkid = EM.emer_con_post_townunkid3 " & _
                        "   LEFT JOIN hrmsConfiguration..cfzipcode_master AS EZ ON EZ.zipcodeunkid = EM.emer_con_postcodeunkid3 " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#EContact3') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #EContact3 Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 6 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='ec3Details' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 917, "Emergency Contact3 Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(CAST(ID.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_ID_Type & "] " & _
                        "   ,ISNULL(CAST(IT.serial_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_ID_Serial_No & "] " & _
                        "   ,ISNULL(CAST(IT.identity_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_ID_No & "] " & _
                        "   ,ISNULL(CAST(IC.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_Issue_Country & "] " & _
                        "   ,ISNULL(CAST(IT.issued_place AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_Place_Issue & "] " & _
                        "   ,ISNULL(CAST(IT.dl_class AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_DL_Class & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),IT.issue_date,112) AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_Issue_Date & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),IT.expiry_date,112) AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Identity_Expiry_Date & "] " & _
                        "FROM hremployee_idinfo_tran IT " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master AS IC ON IC.countryunkid = IT.countryunkid " & _
                        "   JOIN cfcommon_master AS ID ON ID.masterunkid = IT.idtypeunkid " & _
                        "   JOIN hremployee_master EM ON EM.employeeunkid = IT.employeeunkid " & _
                        "WHERE EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='idDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 918, "Identity Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Try
                        Dim strfilter = "[" & col.ColumnName & "] IS NULL OR [" & col.ColumnName & "] = '' "
                        strCaption = ""
                        strCaption = dctMandatoryField(CType(CInt(col.ColumnName), clsEmployee_Master.EmpColEnum))
                        If Array.IndexOf(mFields, col.ColumnName) <> -1 Then
                            If blnNoRows = True Then
                                strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                            Else
                                If dsList.Tables("Data").Select(strfilter).Length > 0 Then

                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                Else
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                End If
                            End If
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                        End If
                    Catch ex As Exception
                        strCaption = ""
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName.ToString() & "</SPAN>" & vbCrLf)
                    End Try
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        Try
                            If row(col).ToString().Trim.Length <= 0 Then
                                Throw New Exception
                            End If
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                        Catch ex As Exception
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        End Try
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(MC.name,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Membership_Category & "] " & _
                        "   ,ISNULL(membershipname,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Membership & "] " & _
                        "   ,ISNULL(MT.membershipno,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Membership_No & "] " & _
                        "   ,ISNULL(CONVERT(CHAR(8),MT.issue_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Membership_Issue_Date & "] " & _
                        "   ,ISNULL(CONVERT(CHAR(8),MT.start_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Membership_Start_Date & "] " & _
                        "   ,ISNULL(CONVERT(CHAR(8),MT.expiry_date,112),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Membership_Expiry_Date & "] " & _
                        "   ,ISNULL(MT.remark,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Membership_Remark & "] " & _
                        "FROM hremployee_meminfo_tran AS MT " & _
                        "   JOIN hrmembership_master ON MT.membershipunkid = hrmembership_master.membershipunkid " & _
                        "   JOIN cfcommon_master AS MC ON MC.masterunkid = MT.membership_categoryunkid " & _
                        "   JOIN hremployee_master EM ON EM.employeeunkid = MT.employeeunkid " & _
                        "WHERE EM.employeeunkid = @EmpId AND EM.isactive = 1 AND MT.isdeleted = 0 "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='memDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 919, "Membership Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Try
                        Dim strfilter = "[" & col.ColumnName & "] IS NULL OR [" & col.ColumnName & "] = '' "
                        strCaption = ""
                        strCaption = dctMandatoryField(CType(CInt(col.ColumnName), clsEmployee_Master.EmpColEnum))
                        If Array.IndexOf(mFields, col.ColumnName) <> -1 Then
                            If blnNoRows = True Then
                                strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                            Else
                                If dsList.Tables("Data").Select(strfilter).Length > 0 Then

                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                Else
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                End If
                            End If
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                        End If
                    Catch ex As Exception
                        strCaption = ""
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName.ToString() & "</SPAN>" & vbCrLf)
                    End Try
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        Try
                            If row(col).ToString().Trim.Length <= 0 Then
                                Throw New Exception
                            End If
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                        Catch ex As Exception
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        End Try
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "IF OBJECT_ID('tempdb..#OtherInfo') IS NOT NULL " & _
                        "DROP TABLE #OtherInfo " & _
                        "SELECT " & _
                        "    ISNULL(CAST(CO.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Complexion & "] " & _
                        "   ,ISNULL(CAST(BG.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Blood_Group & "] " & _
                        "   ,ISNULL(CAST(EY.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Eye_Color & "] " & _
                        "   ,ISNULL(CAST(NT.country_name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Nationality & "] " & _
                        "   ,ISNULL(CAST(ET.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Ethnicity & "] " & _
                        "   ,ISNULL(CAST(HR.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Hair_Color & "] " & _
                        "   ,ISNULL(CAST(MR.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Marital_Status & "] " & _
                        "   ,ISNULL(CAST(L1.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Language1 & "] " & _
                        "   ,ISNULL(CAST(L2.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Language2 & "] " & _
                        "   ,ISNULL(CAST(L3.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Language3 & "] " & _
                        "   ,ISNULL(CAST(L4.name AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Language4 & "] " & _
                        "   ,ISNULL(CAST(EM.extra_tel_no AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Extra_Tel_No & "] " & _
                        "   ,ISNULL(CASE WHEN CAST(EM.height AS NVARCHAR(MAX)) <> '0' THEN CAST(EM.height AS NVARCHAR(MAX)) ELSE '' END,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Height & "] " & _
                        "   ,ISNULL(CASE WHEN CAST(EM.weight AS NVARCHAR(MAX)) <> '0' THEN CAST(EM.weight AS NVARCHAR(MAX)) ELSE '' END,'') AS [" & clsEmployee_Master.EmpColEnum.Col_Weight & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),EM.anniversary_date,112) AS NVARCHAR(MAX)),'') AS [" & clsEmployee_Master.EmpColEnum.Col_Annivesary_Date & "] " & _
                        "   ,CAST(ISNULL((SELECT STUFF((SELECT ',' + name FROM hrallergies_tran JOIN cfcommon_master ON masterunkid = allergiesunkid WHERE employeeunkid = @EmpId FOR XML PATH('')),1,1,'')),'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Allergies & "] " & _
                        "   ,CAST(ISNULL((SELECT STUFF((SELECT ',' + name FROM hrdisabilities_tran JOIN cfcommon_master ON masterunkid = disabilitiesunkid WHERE employeeunkid = @EmpId FOR XML PATH('')),1,1,'')),'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Medical_Disabilites & "] " & _
                        "   ,CAST(ISNULL(EM.sports_hobbies,'') AS NVARCHAR(MAX)) AS [" & clsEmployee_Master.EmpColEnum.Col_Sports_Hobbies & "] " & _
                        "   INTO #OtherInfo " & _
                        "FROM hremployee_master EM " & _
                        "   LEFT JOIN cfcommon_master CO ON CO.masterunkid = EM.complexionunkid " & _
                        "   LEFT JOIN cfcommon_master BG ON BG.masterunkid = EM.bloodgroupunkid " & _
                        "   LEFT JOIN cfcommon_master EY ON EY.masterunkid = EM.eyecolorunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfcountry_master NT ON NT.countryunkid = EM.nationalityunkid " & _
                        "   LEFT JOIN cfcommon_master ET ON ET.masterunkid = EM.ethnicityunkid " & _
                        "   LEFT JOIN cfcommon_master HR ON HR.masterunkid = EM.hairunkid " & _
                        "   LEFT JOIN cfcommon_master MR ON MR.masterunkid = EM.maritalstatusunkid " & _
                        "   LEFT JOIN cfcommon_master L1 ON L1.masterunkid = EM.language1unkid " & _
                        "   LEFT JOIN cfcommon_master L2 ON L2.masterunkid = EM.language2unkid " & _
                        "   LEFT JOIN cfcommon_master L3 ON L3.masterunkid = EM.language3unkid " & _
                        "   LEFT JOIN cfcommon_master L4 ON L4.masterunkid = EM.language3unkid " & _
                        "WHERE EM.employeeunkid = @EmpId " & _
                        "SET @Cols = (SELECT STUFF((SELECT ',[' + name + ']' FROM tempdb.sys.columns where object_id = object_id('tempdb..#OtherInfo') ORDER BY column_id FOR XML PATH('')),1,1,'')) " & _
                        "SET @oSQL = 'IF OBJECT_ID(''tempdb..#tbl'') IS NOT NULL " & _
                        "DROP TABLE #tbl " & _
                        "SELECT ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS SNo,Caption,Value INTO #tbl from #OtherInfo Unpivot(Value For Caption IN ('+ @Cols +')) AS H " & _
                        "DECLARE @Cnt AS INT;SET @Cnt = 6 " & _
                        "SELECT " & _
                        "    ISNULL(DT1.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT1.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT2.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT2.Value,'''') AS oValue " & _
                        "   ,ISNULL(DT3.Caption,'''') AS Caption " & _
                        "   ,ISNULL(DT3.Value,'''') AS oValue " & _
                        "FROM ##SerialNumber SN " & _
                        "   LEFT JOIN #tbl DT1 ON DT1.SNo = SN.SNo AND DT1.SNo <= @Cnt " & _
                        "   LEFT JOIN #tbl DT2 ON DT2.SNo = SN.SNo + @Cnt AND DT2.SNo > @Cnt " & _
                        "   LEFT JOIN #tbl DT3 ON DT3.SNo = DT2.SNo + @Cnt AND DT3.SNo > @Cnt " & _
                        "WHERE SN.SNO <= @Cnt' " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='ofDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 920, "Other Info Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        Select Case col.Ordinal
                            Case 0, 2, 4, 6, 8
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    strCaption = ""
                                    strCaption = dctMandatoryField(CType(CInt(row(col)), clsEmployee_Master.EmpColEnum))
                                    If row(col.Ordinal + 1).ToString().Trim.Length <= 0 Then
                                        If Array.IndexOf(mFields, row(col)) <> -1 Then
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & strCaption & "</SPAN>" & vbCrLf)
                                        Else
                                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & strCaption & "</SPAN>" & vbCrLf)
                                    End If
                                Catch ex As Exception
                                    strCaption = ""
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                                Try
                                    If row(col).ToString().Trim.Length <= 0 Then
                                        Throw New Exception
                                    End If
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                                Catch ex As Exception
                                    strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                                End Try
                                strBuilder.Append("</TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                StrQ = "SELECT * " & _
                           "INTO #TableDepn " & _
                           "FROM " & _
                           "( " & _
                               "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                    ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                               "FROM hrdependant_beneficiaries_status_tran "

                If mintEmployeeId > 0 Then
                    StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
                End If

                StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

                If mintEmployeeId > 0 Then
                    StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @EmployeeId "
                End If

                If mdtEmpAsonDate <> Nothing Then
                    StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                    objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmpAsonDate))
                Else
                    'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
                End If

                StrQ &= ") AS A " & _
                        "WHERE 1 = 1 " & _
                        " AND A.ROWNO = 1 "
                'Sohail (18 May 2019) -- End

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(CAST(DB.first_name AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmDependantsAndBeneficiaries", 900, "Firstname") & "] " & _
                        "   ,ISNULL(CAST(DB.last_name AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmDependantsAndBeneficiaries", 901, "Lastname") & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),DB.birthdate,112) AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmDependantsAndBeneficiaries", 25, "Birth Date") & "] " & _
                        "   ,ISNULL(CAST(CM.name AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmDependantsAndBeneficiaries", 29, "Relation") & "] " & _
                        "   ,CAST(CASE WHEN CAST(EM.gender AS NVARCHAR(1)) = '1' THEN @Male WHEN CAST(EM.gender AS NVARCHAR(1)) = '2' THEN @Female ELSE '' END AS NVARCHAR(MAX)) AS [" & Language.getMessage("frmDependantsAndBeneficiaries", 30, "Gender") & "] " & _
                        "   ,CASE WHEN DB.isdependant = 1 THEN @Yes ELSE @No END AS [" & Language.getMessage("frmDependantsAndBeneficiaries", 902, "Is Beneficiary") & "] " & _
                        "FROM hrdependants_beneficiaries_tran AS DB " & _
                        "   JOIN #TableDepn ON DB.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                        "   JOIN cfcommon_master CM ON CM.masterunkid = DB.relationunkid " & _
                        "   JOIN hremployee_master EM ON DB.employeeunkid = EM.employeeunkid " & _
                        "WHERE EM.employeeunkid = @EmpId AND DB.isvoid = 0 " & _
                        "AND #TableDepn.isactive = 1 "
                'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                StrQ &= " DROP TABLE #TableDepn "
                'Sohail (18 May 2019) -- End

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='dptDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 921, "Dependant Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmDependantsAndBeneficiariesList
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        Try
                            If row(col).ToString().Trim.Length <= 0 Then
                                Throw New Exception
                            End If
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                        Catch ex As Exception
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        End Try
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next


                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(CAST(QG.name AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 14, "Qualification Group") & "] " & _
                        "   ,ISNULL(CAST(QM.qualificationname AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 16, "Qualification/Award") & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),EQ.award_start_date,112) AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 21, "Start Date") & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),EQ.award_end_date,112) AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 900, "End Date") & "] " & _
                        "   ,ISNULL(CAST(IM.institute_name AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 23, "Provider") & "] " & _
                        "   ,ISNULL(CAST(RM.resultname AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 18, "Result") & "] " & _
                        "   ,ISNULL(CAST(EQ.gpacode AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 20, "GPA") & "] " & _
                        "   ,EQ.qualificationgroupunkid " & _
                        "FROM hremp_qualification_tran EQ " & _
                        "   JOIN cfcommon_master AS QG ON QG.masterunkid = EQ.qualificationgroupunkid " & _
                        "   JOIN hrqualification_master AS QM ON EQ.qualificationunkid = QM.qualificationunkid " & _
                        "   JOIN hrinstitute_master AS IM ON EQ.instituteunkid = IM.instituteunkid " & _
                        "   JOIN hrresult_master AS RM ON EQ.resultunkid = RM.resultunkid " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = EQ.employeeunkid " & _
                        "WHERE EQ.isvoid = 0 AND  EQ.qualificationgroupunkid > 0 AND EM.employeeunkid = @EmpId " & _
                        "UNION " & _
                        "SELECT " & _
                        "    ISNULL(CAST(EQ.other_qualificationgrp AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 14, "Qualification Group") & "] " & _
                        "   ,ISNULL(CAST(EQ.other_qualification AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 16, "Qualification/Award") & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),EQ.award_start_date,112) AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 21, "Start Date") & "] " & _
                        "   ,ISNULL(CAST(CONVERT(NVARCHAR(MAX),EQ.award_end_date,112) AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 900, "End Date") & "] " & _
                        "   ,ISNULL(CAST(EQ.other_institute AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 23, "Provider") & "] " & _
                        "   ,ISNULL(CAST(EQ.other_resultcode AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 18, "Result") & "] " & _
                        "   ,ISNULL(CAST(EQ.gpacode AS NVARCHAR(MAX)),'') AS [" & Language.getMessage("frmQualifications", 20, "GPA") & "] " & _
                        "   ,EQ.qualificationgroupunkid " & _
                        "FROM hremp_qualification_tran EQ " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = EQ.employeeunkid " & _
                        "WHERE EQ.isvoid = 0 AND  EQ.qualificationgroupunkid <= 0 AND EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='qualifDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 922, "Qualification Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    If col.ColumnName = "qualificationgroupunkid" Then Continue For
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmQualificationsList
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                Dim intOrdinal = (dsList.Tables("Data").Columns("qualificationgroupunkid").Ordinal - 1)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        If col.ColumnName = "qualificationgroupunkid" Then Continue For
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        If row("qualificationgroupunkid") > 0 Then
                            Try
                                If row(col).ToString().Trim.Length <= 0 Then
                                    Throw New Exception
                                End If
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                            Catch ex As Exception
                                strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                            End Try
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & IIf(col.Ordinal = intOrdinal, "**", "") & vbCrLf)
                        End If
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD ALIGN='left' COLSPAN = '" & dsList.Tables("Data").Columns.Count - 1 & "'>" & vbCrLf)
                strBuilder.Append("<SPAN STYLE='font-size:12px; color= rgb(0, 0, 236)'>" & "** " & Language.getMessage(mstrModuleName, 930, "Note : This represents other qualification.") & "</SPAN>" & vbCrLf)
                strBuilder.Append("</TD>" & vbCrLf)
                strBuilder.Append("</TR>")
                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(ET.company,'') AS [" & Language.getMessage("frmEmployeeExperience", 11, "Company") & "] " & _
                        "   ,ISNULL(ET.address,'') AS [" & Language.getMessage("frmEmployeeExperience", 900, "Address") & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(8),ET.start_date,112),'') AS [" & Language.getMessage("frmEmployeeExperience", 13, "Start Date") & "] " & _
                        "   ,ISNULL(CONVERT(NVARCHAR(8),ET.end_date,112),'') AS [" & Language.getMessage("frmEmployeeExperience", 14, "End Date") & "] " & _
                        "   ,ISNULL(CASE WHEN ET.jobunkid > 0 THEN job_name ELSE ET.old_job END,'') AS [" & Language.getMessage("frmEmployeeExperience", 12, "Job") & "] " & _
                        "FROM hremp_experience_tran AS ET " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = ET.employeeunkid " & _
                        "   LEFT JOIN hrjob_master ON EM.jobunkid = ET.jobunkid AND hrjob_master.isactive = 1 " & _
                        "WHERE ET.isvoid = 0 AND EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='expDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 923, "Experience Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmJobHistory_ExperienceList
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        Try
                            If row(col).ToString().Trim.Length <= 0 Then
                                Throw New Exception
                            End If
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                        Catch ex As Exception
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        End Try
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(RT.name,'') AS [" & Language.getMessage("frmEmployeeReferee", 900, "Name") & "] " & _
                        "   ,ISNULL(RT.address,'') AS [" & Language.getMessage("frmEmployeeReferee", 901, "Address") & "] " & _
                        "   ,ISNULL(CASE WHEN RT.gender = 1 THEN @Male WHEN RT.gender = 2 THEN @Female ELSE '' END,'') AS [" & Language.getMessage("frmEmployeeReferee", 902, "Gender") & "] " & _
                        "   ,ISNULL(CM.name,'') AS [" & Language.getMessage("frmEmployeeReferee", 903, "Relation") & "] " & _
                        "   ,ISNULL(RT.email,'') AS [" & Language.getMessage("frmEmployeeReferee", 904, "Email") & "] " & _
                        "   ,ISNULL(RT.ref_position,'') AS [" & Language.getMessage("frmEmployeeReferee", 905, "Position") & "] " & _
                        "   ,ISNULL(RT.mobile_no,'') AS [" & Language.getMessage("frmEmployeeReferee", 906, "Mobile") & "] " & _
                        "FROM hremployee_referee_tran AS RT " & _
                        "   LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = RT.relationunkid " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = RT.employeeunkid " & _
                        "WHERE RT.isvoid = 0 AND EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='refDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 924, "Refree/References Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmEmployeeRefereeList
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & row(col).ToString & "</SPAN>" & vbCrLf)
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(CM.name,'') AS [" & Language.getMessage("frmEmployeeSkills_AddEdit", 900, "Skill Category") & "] " & _
                        "   ,ISNULL(SM.skillname,'') AS [" & Language.getMessage("frmEmployeeSkills_AddEdit", 901, "Skill Name") & "] " & _
                        "   ,ISNULL(ES.description,'') AS [" & Language.getMessage("frmEmployeeSkills_AddEdit", 902, "Description") & "] " & _
                        "FROM hremp_app_skills_tran AS ES " & _
                        "   JOIN cfcommon_master AS CM ON CM.masterunkid = ES.skillcategoryunkid " & _
                        "   JOIN hrskill_master AS SM ON SM.skillunkid = ES.skillunkid " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = ES.emp_app_unkid " & _
                        "WHERE ES.isvoid = 0 AND EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='sklDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 925, "Skill(s) Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmEmployee_Skill_List
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(CM.name,'') AS [" & Language.getMessage("frmAssignGroupBenefit", 900, "Benefit Group") & "] " & _
                        "   ,ISNULL(PB.benefitplanname,'') AS [" & Language.getMessage("frmAssignGroupBenefit", 901, "Benefit") & "] " & _
                        "FROM hremp_benefit_coverage AS EB " & _
                        "   JOIN cfcommon_master AS CM ON CM.masterunkid = EB.benefitgroupunkid " & _
                        "   JOIN hrbenefitplan_master AS PB ON PB.benefitplanunkid = EB.benefitplanunkid " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = EB.employeeunkid " & _
                        "WHERE EB.isvoid = 0 AND EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='befDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 926, "Benefit(s) Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmAssignGroupBenefit
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(BG.groupname,'') AS [" & Language.getMessage("frmEmployeeBank_AddEdit", 901, "Bank") & "] " & _
                        "   ,ISNULL(BB.branchname,'') AS [" & Language.getMessage("frmEmployeeBank_AddEdit", 902, "Branch") & "] " & _
                        "   ,ISNULL(AC.accounttype_name,'') AS [" & Language.getMessage("frmEmployeeBank_AddEdit", 903, "Account Type") & "] " & _
                        "   ,ISNULL(BT.accountno,'') AS [" & Language.getMessage("frmEmployeeBank_AddEdit", 904, "Account No") & "] " & _
                        "FROM premployee_bank_tran AS BT " & _
                        "   JOIN hrmsConfiguration..cfbankbranch_master AS BB ON BT.branchunkid = BB.branchunkid " & _
                        "   JOIN hrmsConfiguration..cfpayrollgroup_master AS BG ON BG.groupmasterunkid = BB.bankgroupunkid " & _
                        "   JOIN hrmsConfiguration..cfbankacctype_master AS AC ON AC.accounttypeunkid = BT.accounttypeunkid " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = BT.employeeunkid " & _
                        "WHERE BT.isvoid = 0 AND EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='bankDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 927, "Bank(s) Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmEmployeeBankList
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SELECT " & _
                        "    ISNULL(AE.name, '') AS [" & Language.getMessage("frmEmployeeAssets_AddEdit", 900, "Asset") & "] " & _
                        "   ,ET.asset_no AS [" & Language.getMessage("frmEmployeeAssets_AddEdit", 901, "Asset No") & "] " & _
                        "   ,ISNULL(CO.name, '') AS [" & Language.getMessage("frmEmployeeAssets_AddEdit", 902, "Condition") & "] " & _
                        "   ,CONVERT(CHAR(8),ET.assign_return_date,112) AS [" & Language.getMessage("frmEmployeeAssets_AddEdit", 903, "Date") & "] " & _
                        "   ,ET.remark [" & Language.getMessage("frmEmployeeAssets_AddEdit", 904, "Remark") & "] " & _
                        "   ,ET.assetserial_no [" & Language.getMessage("frmEmployeeAssets_AddEdit", 905, "Serial No") & "] " & _
                        "   ,CASE ET.asset_statusunkid WHEN 1 THEN @Assigned WHEN 2 THEN @Returned WHEN 3 THEN @WrittenOff WHEN 4 THEN @Lost ELSE '' END [" & Language.getMessage("frmEmployeeAssets_AddEdit", 906, "Status") & "] " & _
                        "FROM hremployee_assets_tran AS ET " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = ET.employeeunkid " & _
                        "   LEFT JOIN cfcommon_master AS AE ON AE.masterunkid = ET.assetunkid " & _
                        "   LEFT JOIN cfcommon_master AS CO ON CO.masterunkid = ET.conditionunkid " & _
                        "WHERE ET.isvoid = 0 AND EM.employeeunkid = @EmpId "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='assetDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 928, "Asset(s) Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmAssetsRegisterList
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        Try
                            If row(col).ToString().Trim.Length <= 0 Then
                                Throw New Exception
                            End If
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & eZeeDate.convertDate(row(col).ToString()).ToShortDateString() & "</SPAN>" & vbCrLf)
                        Catch ex As Exception
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        End Try
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                StrQ = ""

                StrQ &= strcommon & " "
                StrQ &= "SET @oSQL = ' " & _
                        "SELECT " & _
                        "   ISNULL(LV.leavename,'''') AS [" & Language.getMessage("frmLeaveAccrue_AddEdit", 900, "Leave Type") & "] " & _
                        "FROM lvleavebalance_tran AS LT " & _
                        "   JOIN lvleavetype_master AS LV ON LV.leavetypeunkid = LT.leavetypeunkid " & _
                        "   JOIN hremployee_master AS EM ON EM.employeeunkid = LT.employeeunkid " & _
                        "WHERE LT.isvoid = 0 AND LV.isaccrued = 1 AND EM.employeeunkid = ' + CAST(@EmpId AS NVARCHAR(MAX)) + " & _
                        "   'AND LT.yearunkid IN (SELECT LT.yearunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE database_name = (SELECT DB_NAME()) AND isclosed = 0)' " & _
                        "   IF @IsFinYear = 1 " & _
                        "   BEGIN " & _
                        "       SET @oSQL = @oSQL + ' AND LT.isclose_fy = 0 ' " & _
                        "   END " & _
                        "   ELSE " & _
                        "   BEGIN " & _
                        "       SET @oSQL = @oSQL + ' AND LT.iselc = 1 AND LT.isopenelc = 1 ' " & _
                        "   END " & _
                        "EXECUTE(@oSQL) "

                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                strBuilder.Append("<DIV ID='lvdDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmEmployeeMaster", 929, "Leave Accrued Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmLeaveAccrue_AddEdit
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next
                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)



                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                StrQ = ""

                StrQ &= strcommon & " "
                'Hemant (03 Feb 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                StrQ &= " SELECT " & _
                       "        * " & _
                       " FROM (  "
                'Hemant (03 Feb 2023) -- End
                StrQ &= "SELECT " & _
                    "    ISNULL(EM.Employeecode,'') AS [" & Language.getMessage("frmReportingToEmp", 900, "Reporting Code") & "] " & _
                    "   ,ISNULL(EM.firstname,'') + ' ' + ISNULL(EM.othername,'') + ' ' + ISNULL(EM.surname,'') AS [" & Language.getMessage("frmReportingToEmp", 901, "Reporting Employee") & "] " & _
                    "   ,ISNULL(hrjobgroup_master.name,'') AS [" & Language.getMessage("frmReportingToEmp", 902, "Job Group") & "] " & _
                    "   ,ISNULL(hrjob_master.job_name,'') AS [" & Language.getMessage("frmReportingToEmp", 903, "Job") & "] " & _
                    "   ,ISNULL(ERT.ishierarchy,0) AS [ishierarchy] " & _
                    "	,DENSE_RANK() OVER (ORDER BY effectivedate DESC) AS Rno " & _
                    " FROM hremployee_reportto AS ERT " & _
                    " JOIN hremployee_master AS EM ON EM.employeeunkid = ERT.reporttoemployeeunkid " & _
                    " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate " & _
                    ") AS J ON J.employeeunkid = ERT.reporttoemployeeunkid AND J.Rno = 1 " & _
                    " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = J.jobgroupunkid " & _
                    " JOIN hrjob_master on hrjob_master.jobunkid = J.jobunkid " & _
                    "WHERE ERT.isvoid = 0 AND ERT.employeeunkid = @EmpId "

                'Hemant (03 Feb 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                StrQ &= " AND CONVERT(CHAR(8),ERT.effectivedate,112) <= @EmpAsonDate "

                StrQ &= " ) AS A " & _
                  " WHERE 1 = 1 " & _
                  " AND A.Rno = 1 "
                'Hemant (03 Feb 2023) -- End

                objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmpAsonDate))
                dsList = objDataOperation.ExecQuery(StrQ, "Data")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If


                strBuilder.Append("<DIV ID='lvReportingToDetails' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage("frmReportingToEmp", 904, "Reporting to Employee(s) Details") & "</DIV>")
                strBuilder.Append("<TABLE STYLE='WIDTH:100%'>" & vbCrLf)
                strBuilder.Append("<TR>" & vbCrLf)

                blnNoRows = False
                If dsList.Tables("Data").Rows.Count <= 0 Then
                    blnNoRows = True
                End If

                For Each col As DataColumn In dsList.Tables("Data").Columns
                    If col.ColumnName.ToUpper() = "ISHIERARCHY" Then Continue For
                    strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                    Dim intVal As Integer = enScreenName.frmReportingToEmp
                    If Array.IndexOf(mScreen, intVal.ToString) <> -1 Then
                        If blnNoRows Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;" & strMandatoryCSS & "'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<SPAN STYLE='font-size:12px;font-weight:bold;'>" & col.ColumnName & "</SPAN>" & vbCrLf)
                    End If
                    strBuilder.Append("</TD>" & vbCrLf)
                Next

                intOrdinal = (dsList.Tables("Data").Columns("ishierarchy").Ordinal - 1)
                For Each row As DataRow In dsList.Tables("Data").Rows
                    strBuilder.Append("<TR>" & vbCrLf)
                    For Each col As DataColumn In dsList.Tables("Data").Columns
                        If col.ColumnName = "ishierarchy" Then Continue For
                        strBuilder.Append("<TD ALIGN='left'>" & vbCrLf)
                        If CBool(row("ishierarchy")) = False Then
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & vbCrLf)
                        Else
                            strBuilder.Append("<SPAN STYLE='font-size:12px;'>" & row(col).ToString() & "</SPAN>" & IIf(col.Ordinal = intOrdinal, "**", "") & vbCrLf)
                        End If
                        strBuilder.Append("</TD>" & vbCrLf)
                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD ALIGN='left' COLSPAN = '" & dsList.Tables("Data").Columns.Count - 1 & "'>" & vbCrLf)
                strBuilder.Append("<SPAN STYLE='font-size:12px; color= rgb(0, 0, 236)'>" & "** " & Language.getMessage(mstrModuleName, 931, "Note : This represents default reporting.") & "</SPAN>" & vbCrLf)
                strBuilder.Append("</TD>" & vbCrLf)
                strBuilder.Append("</TR>")
                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</DIV>" & vbCrLf)

                'Pinkal (18-Aug-2018) -- End



                strBuilder.Append("</DIV>")
                strBuilder.Append("</FORM>")
                strBuilder.Append("</BODY>")
                strBuilder.Append("</HTML>")

                StrQ = "IF OBJECT_ID('tempdb..##SerialNumber') IS NOT NULL DROP TABLE ##SerialNumber " & _
                       "IF OBJECT_ID('tempdb..#MyTempTable') IS NOT NULL DROP TABLE #MyTempTable " & _
                       "IF OBJECT_ID('tempdb..#AllocTab') IS NOT NULL DROP TABLE #AllocTab " & _
                       "IF OBJECT_ID('tempdb..#Dates') IS NOT NULL DROP TABLE #Dates " & _
                       "IF OBJECT_ID('tempdb..#PresentTab') IS NOT NULL DROP TABLE #PresentTab " & _
                       "IF OBJECT_ID('tempdb..#DomicileTab') IS NOT NULL DROP TABLE #DomicileTab " & _
                       "IF OBJECT_ID('tempdb..#RecruitTab') IS NOT NULL DROP TABLE #RecruitTab " & _
                       "IF OBJECT_ID('tempdb..#EContact1') IS NOT NULL DROP TABLE #EContact1 " & _
                       "IF OBJECT_ID('tempdb..#EContact2') IS NOT NULL DROP TABLE #EContact2 " & _
                       "IF OBJECT_ID('tempdb..#EContact3') IS NOT NULL DROP TABLE #EContact3 " & _
                       "IF OBJECT_ID('tempdb..#OtherInfo') IS NOT NULL DROP TABLE #OtherInfo " & _
                       "IF OBJECT_ID('tempdb..#tbl') IS NOT NULL DROP TABLE #tbl "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strBuilder.ToString()
    End Function
    'S.SANDEEP [20-JUN-2018] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Employeement Type :")
            Language.setMessage(mstrModuleName, 3, "Branch :")
            Language.setMessage(mstrModuleName, 4, "Department :")
            Language.setMessage(mstrModuleName, 5, "Job :")
            Language.setMessage(mstrModuleName, 6, "Cost Center :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Level")
            Language.setMessage(mstrModuleName, 9, "Male")
            Language.setMessage(mstrModuleName, 10, "Female")
            Language.setMessage(mstrModuleName, 11, "Prepared By :")
            Language.setMessage(mstrModuleName, 12, "Date :")
            Language.setMessage(mstrModuleName, 13, "BirthDate From :")
            Language.setMessage(mstrModuleName, 14, "BirthDate To :")
            Language.setMessage(mstrModuleName, 15, "Anniversary Date From :")
            Language.setMessage(mstrModuleName, 16, "Anniversary Date To :")
            Language.setMessage(mstrModuleName, 17, "Appointment Date From :")
            Language.setMessage(mstrModuleName, 18, "Appointment Date To :")
            Language.setMessage(mstrModuleName, 19, "Confirmation Date From :")
            Language.setMessage(mstrModuleName, 20, "Confirmation Date To :")
            Language.setMessage(mstrModuleName, 21, "Probation Start Date From :")
            Language.setMessage(mstrModuleName, 22, "Probation Start Date To :")
            Language.setMessage(mstrModuleName, 23, "Probation End Date From :")
            Language.setMessage(mstrModuleName, 24, "Probation End Date To :")
            Language.setMessage(mstrModuleName, 25, "Suspended Start Date From :")
            Language.setMessage(mstrModuleName, 26, "Suspended Start Date To :")
            Language.setMessage(mstrModuleName, 27, "Suspended End Date From :")
            Language.setMessage(mstrModuleName, 28, "Suspended End Date To :")
            Language.setMessage(mstrModuleName, 29, "Reinstatement Date From :")
            Language.setMessage(mstrModuleName, 30, "Reinstatement Date To :")
            Language.setMessage(mstrModuleName, 31, "End of Contract Date From :")
            Language.setMessage(mstrModuleName, 32, "End of Contract Date To :")
            Language.setMessage(mstrModuleName, 33, "Checked By :")
            Language.setMessage(mstrModuleName, 34, "Approved By :")
            Language.setMessage(mstrModuleName, 36, "First Appointment Date From :")
            Language.setMessage(mstrModuleName, 37, "First Appointment Date To :")
            Language.setMessage(mstrModuleName, 39, "Salary")
            Language.setMessage(mstrModuleName, 40, "Increment")
            Language.setMessage(mstrModuleName, 41, "Employment Type")
            Language.setMessage(mstrModuleName, 42, "Date Hired")
            Language.setMessage(mstrModuleName, 43, "Birthdate")
            Language.setMessage(mstrModuleName, 44, "Age")
            Language.setMessage(mstrModuleName, 45, "Gender")
            Language.setMessage(mstrModuleName, 46, "Marital Status")
            Language.setMessage(mstrModuleName, 47, "Religion")
            Language.setMessage(mstrModuleName, 48, "Telephone")
            Language.setMessage(mstrModuleName, 49, "Present Address")
            Language.setMessage(mstrModuleName, 50, "Emergency Contact")
            Language.setMessage(mstrModuleName, 51, "Bank")
            Language.setMessage(mstrModuleName, 52, "Membership")
            Language.setMessage(mstrModuleName, 53, "Department Group")
            Language.setMessage(mstrModuleName, 54, "Section Group")
            Language.setMessage(mstrModuleName, 55, "Unit Group")
            Language.setMessage(mstrModuleName, 56, "Team")
            Language.setMessage(mstrModuleName, 57, "Job Group")
            Language.setMessage(mstrModuleName, 58, "Class Group")
            Language.setMessage(mstrModuleName, 59, "Class")
            Language.setMessage(mstrModuleName, 63, "Branch")
            Language.setMessage(mstrModuleName, 64, "Department")
            Language.setMessage(mstrModuleName, 65, "Section")
            Language.setMessage(mstrModuleName, 66, "Unit")
            Language.setMessage(mstrModuleName, 67, "Job")
            Language.setMessage(mstrModuleName, 68, "CostCenter")
            Language.setMessage(mstrModuleName, 69, "Grade Group")
            Language.setMessage(mstrModuleName, 70, "Grade")
            Language.setMessage(mstrModuleName, 71, "Grade Level")
            Language.setMessage(mstrModuleName, 72, "Anniversary Date")
            Language.setMessage(mstrModuleName, 73, "First Appointment Date")
            Language.setMessage(mstrModuleName, 74, "Confirmation Date")
            Language.setMessage(mstrModuleName, 75, "Reinstatement Date")
            Language.setMessage(mstrModuleName, 76, "Probation Start Date")
            Language.setMessage(mstrModuleName, 77, "Probation End Date")
            Language.setMessage(mstrModuleName, 78, "Suspended From Date")
            Language.setMessage(mstrModuleName, 79, "Suspended To Date")
            Language.setMessage(mstrModuleName, 80, "End Of Contract")
            Language.setMessage(mstrModuleName, 81, "Email")
            Language.setMessage(mstrModuleName, 82, "Employee Code")
            Language.setMessage(mstrModuleName, 83, "Firstname")
            Language.setMessage(mstrModuleName, 84, "Othername")
            Language.setMessage(mstrModuleName, 85, "Surname")
            Language.setMessage(mstrModuleName, 100, "Employee Code :")
            Language.setMessage(mstrModuleName, 101, "Employee Name :")
            Language.setMessage(mstrModuleName, 102, "Appointment Date :")
            Language.setMessage(mstrModuleName, 103, "Employee Transfer")
            Language.setMessage(mstrModuleName, 104, "Effective Date")
            Language.setMessage(mstrModuleName, 105, "Branch")
            Language.setMessage(mstrModuleName, 106, "Department Group")
            Language.setMessage(mstrModuleName, 107, "Department")
            Language.setMessage(mstrModuleName, 108, "Section Group")
            Language.setMessage(mstrModuleName, 109, "Section")
            Language.setMessage(mstrModuleName, 110, "Unit Group")
            Language.setMessage(mstrModuleName, 111, "Unit")
            Language.setMessage(mstrModuleName, 112, "Team")
            Language.setMessage(mstrModuleName, 113, "Class Group")
            Language.setMessage(mstrModuleName, 114, "Class")
            Language.setMessage(mstrModuleName, 115, "Reason")
            Language.setMessage(mstrModuleName, 116, "Employee Recategorization")
            Language.setMessage(mstrModuleName, 117, "Job Group")
            Language.setMessage(mstrModuleName, 118, "Job Name")
            Language.setMessage(mstrModuleName, 119, "Employee CostCenter")
            Language.setMessage(mstrModuleName, 120, "Cost-Center")
            Language.setMessage(mstrModuleName, 121, "Employee Dates")
            Language.setMessage(mstrModuleName, 122, "Date1")
            Language.setMessage(mstrModuleName, 123, "Date2")
            Language.setMessage(mstrModuleName, 124, "Payroll Excluded")
            Language.setMessage(mstrModuleName, 125, "Employee Re-Hired")
            Language.setMessage(mstrModuleName, 126, "Re-Hire Date")
            Language.setMessage(mstrModuleName, 127, "Employee Work Permit")
            Language.setMessage(mstrModuleName, 128, "Permit No")
            Language.setMessage(mstrModuleName, 129, "Issue Place")
            Language.setMessage(mstrModuleName, 130, "Issue Date")
            Language.setMessage(mstrModuleName, 131, "Expiry Date")
            Language.setMessage(mstrModuleName, 132, "Country")
            Language.setMessage(mstrModuleName, 133, "Printed By :")
            Language.setMessage(mstrModuleName, 134, "Printed Date :")
            Language.setMessage(mstrModuleName, 135, "Page :")
            Language.setMessage(mstrModuleName, 500, "Probation")
            Language.setMessage(mstrModuleName, 501, "Confirmation")
            Language.setMessage(mstrModuleName, 502, "Suspension")
            Language.setMessage(mstrModuleName, 503, "Termination")
            Language.setMessage(mstrModuleName, 505, "Retirement")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'S.SANDEEP [04 JUN 2015] -- START
'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'Public Class clsEmployee_Listing
'    Inherits IReportData

'#Region " Private Variables "

'    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Listing"
'    Private mstrReportId As String = enArutiReport.Employee_Listing
'    Dim objDataOperation As clsDataOperation
'    Dim dtTable As DataTable
'    Dim dtRow As DataRow = Nothing
'    'S.SANDEEP [ 12 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Dim dsMemData As New DataSet : Dim dsBnkData As New DataSet
'    'S.SANDEEP [ 12 NOV 2012 ] -- END

'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    'ENHANCEMENT : Requested by Rutta
'    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
'    'S.SANDEEP [ 26 MAR 2014 ] -- END
'#End Region

'#Region " Constructor "

'    Public Sub New()
'        Me.setReportData(CInt(mstrReportID),intLangId,intCompanyId)
'        Call Create_OnDetailReport()
'    End Sub

'#End Region

'#Region " Private variables "

'    Private mintEmployeeId As Integer = -1
'    Private mstrEmployeeName As String = String.Empty
'    Private mintEmployeementTypeId As Integer = -1
'    Private mstrEmployeementType As String = String.Empty
'    Private mintBranchId As Integer = -1
'    Private mstrBranch As String = String.Empty
'    Private mintDepartmentId As Integer = -1
'    Private mstrDepartment As String = String.Empty
'    Private mintJobId As Integer = -1
'    Private mstrJob As String = String.Empty
'    Private mintCostCenterId As Integer = -1
'    Private mstrCostCenter As String = String.Empty

'    Dim StrFinalPath As String = String.Empty


'    'Pinkal (24-Jun-2011) -- Start
'    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'    Private mblnIsActive As Boolean = True
'    'Pinkal (24-Jun-2011) -- End

'    'S.SANDEEP [ 06 SEP 2011 ] -- START
'    Private mintViewIndex As Integer = -1
'    Private mstrViewByIds As String = String.Empty
'    Private mstrViewByName As String = String.Empty
'    'S.SANDEEP [ 06 SEP 2011 ] -- END 


'    'Pinkal (14-Aug-2012) -- Start
'    'Enhancement : TRA Changes
'    Private dtData As DataTable = Nothing
'    'Pinkal (14-Aug-2012) -- End

'    'S.SANDEEP [ 12 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrUserAccessFilter As String = String.Empty
'    'S.SANDEEP [ 12 NOV 2012 ] -- END

'    'Pinkal (09-Jan-2013) -- Start
'    'Enhancement : TRA Changes
'    Private mstrAnalysis_Fields As String = ""
'    Private mstrAnalysis_Join As String = ""
'    Private mstrAnalysis_OrderBy As String = ""
'    Private mstrReport_GroupName As String = ""
'    Private menExportAction As enExportAction
'    Private mdtTableExcel As DataTable
'    'Pinkal (09-Jan-2013) -- End

'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrAdvance_Filter As String = String.Empty
'    'S.SANDEEP [ 13 FEB 2013 ] -- END


'    'Pinkal (27-Mar-2013) -- Start
'    'Enhancement : TRA Changes

'    Private mdtBirthDateFrom As Date
'    Private mdtBirthDateTo As Date
'    Private mdtAnniversaryDateFrom As Date
'    Private mdtAnniversaryDateTo As Date
'    Private mdtFirstAppointedDateFrom As Date
'    Private mdtFirstAppointedDateTo As Date
'    Private mdtAppointedDateFrom As Date
'    Private mdtAppointedDateTo As Date
'    Private mdtConfirmationDateFrom As Date
'    Private mdtConfirmationDateTo As Date
'    Private mdtProbationStartDateFrom As Date
'    Private mdtProbationStartDateTo As Date
'    Private mdtProbationEndDateFrom As Date
'    Private mdtProbationEndDateTo As Date
'    Private mdtSuspendedStartDateFrom As Date
'    Private mdtSuspendedStartDateTo As Date
'    Private mdtSuspendedEndDateFrom As Date
'    Private mdtSuspendedEndDateTo As Date
'    Private mdtReinstatementDateFrom As Date
'    Private mdtReinstatementDateTo As Date
'    Private mdtEOCDateFrom As Date
'    Private mdtEOCDateTo As Date

'    'Pinkal (27-Mar-2013) -- End


'    'Pinkal (24-Apr-2013) -- Start
'    'Enhancement : TRA Changes
'    Private mblnShowEmpScale As Boolean = False
'    'Pinkal (24-Apr-2013) -- End


'#End Region

'#Region " Properties "

'    Public WriteOnly Property _EmployeeId() As Integer
'        Set(ByVal value As Integer)
'            mintEmployeeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeeName() As String
'        Set(ByVal value As String)
'            mstrEmployeeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeementTypeId() As Integer
'        Set(ByVal value As Integer)
'            mintEmployeementTypeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeementType() As String
'        Set(ByVal value As String)
'            mstrEmployeementType = value
'        End Set
'    End Property

'    Public WriteOnly Property _BranchId() As Integer
'        Set(ByVal value As Integer)
'            mintBranchId = value
'        End Set
'    End Property

'    Public WriteOnly Property _Branch() As String
'        Set(ByVal value As String)
'            mstrBranch = value
'        End Set
'    End Property

'    Public WriteOnly Property _DepartmentId() As Integer
'        Set(ByVal value As Integer)
'            mintDepartmentId = value
'        End Set
'    End Property

'    Public WriteOnly Property _Department() As String
'        Set(ByVal value As String)
'            mstrDepartment = value
'        End Set
'    End Property

'    Public WriteOnly Property _JobId() As Integer
'        Set(ByVal value As Integer)
'            mintJobId = value
'        End Set
'    End Property

'    Public WriteOnly Property _Job() As String
'        Set(ByVal value As String)
'            mstrJob = value
'        End Set
'    End Property

'    Public WriteOnly Property _CostCenterId() As Integer
'        Set(ByVal value As Integer)
'            mintCostCenterId = value
'        End Set
'    End Property

'    Public WriteOnly Property _CostCenter() As String
'        Set(ByVal value As String)
'            mstrCostCenter = value
'        End Set
'    End Property


'    'Pinkal (24-Jun-2011) -- Start
'    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'    Public WriteOnly Property _IsActive() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsActive = value
'        End Set
'    End Property
'    'Pinkal (24-Jun-2011) -- End


'    'S.SANDEEP [ 06 SEP 2011 ] -- START
'    Public WriteOnly Property _ViewIndex() As Integer
'        Set(ByVal value As Integer)
'            mintViewIndex = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewByIds() As String
'        Set(ByVal value As String)
'            mstrViewByIds = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewByName() As String
'        Set(ByVal value As String)
'            mstrViewByName = value
'        End Set
'    End Property
'    'S.SANDEEP [ 06 SEP 2011 ] -- END 



'    'Pinkal (14-Aug-2012) -- Start
'    'Enhancement : TRA Changes
'    Public ReadOnly Property _dtData() As DataTable
'        Get
'            Return dtData
'        End Get
'    End Property
'    'Pinkal (14-Aug-2012) -- End

'    'S.SANDEEP [ 12 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _UserAccessFilter() As String
'        Set(ByVal value As String)
'            mstrUserAccessFilter = value
'        End Set
'    End Property
'    'S.SANDEEP [ 12 NOV 2012 ] -- END

'    'Pinkal (09-Jan-2013) -- Start
'    'Enhancement : TRA Changes

'    Public WriteOnly Property _Analysis_Fields() As String
'        Set(ByVal value As String)
'            mstrAnalysis_Fields = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_Join() As String
'        Set(ByVal value As String)
'            mstrAnalysis_Join = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_OrderBy() As String
'        Set(ByVal value As String)
'            mstrAnalysis_OrderBy = value
'        End Set
'    End Property

'    Public WriteOnly Property _Report_GroupName() As String
'        Set(ByVal value As String)
'            mstrReport_GroupName = value
'        End Set
'    End Property

'    'Pinkal (09-Jan-2013) -- End

'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _Advance_Filter() As String
'        Set(ByVal value As String)
'            mstrAdvance_Filter = value
'        End Set
'    End Property
'    'S.SANDEEP [ 13 FEB 2013 ] -- END


'    'Pinkal (27-Mar-2013) -- Start
'    'Enhancement : TRA Changes

'    Public WriteOnly Property _BirthDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtBirthDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _BirthDateTo() As Date
'        Set(ByVal value As Date)
'            mdtBirthDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _AnniversaryDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtAnniversaryDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _AnniversaryDateTo() As Date
'        Set(ByVal value As Date)
'            mdtAnniversaryDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _FirstAppointedDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtFirstAppointedDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _FirstAppointedDateTo() As Date
'        Set(ByVal value As Date)
'            mdtFirstAppointedDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _AppointedDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtAppointedDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _AppointedDateTo() As Date
'        Set(ByVal value As Date)
'            mdtAppointedDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _ConfirmationDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtConfirmationDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _ConfirmationDateTo() As Date
'        Set(ByVal value As Date)
'            mdtConfirmationDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _ProbationStartDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtProbationStartDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _ProbationStartDateTo() As Date
'        Set(ByVal value As Date)
'            mdtProbationStartDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _ProbationEndDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtProbationEndDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _ProbationEndDateTo() As Date
'        Set(ByVal value As Date)
'            mdtProbationEndDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _SuspendedStartDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtSuspendedStartDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _SuspendedStartDateTo() As Date
'        Set(ByVal value As Date)
'            mdtSuspendedStartDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _SuspendedEndDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtSuspendedEndDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _SuspendedEndDateTo() As Date
'        Set(ByVal value As Date)
'            mdtSuspendedEndDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReinstatementDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtReinstatementDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReinstatementDateTo() As Date
'        Set(ByVal value As Date)
'            mdtReinstatementDateTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _EOCDateFrom() As Date
'        Set(ByVal value As Date)
'            mdtEOCDateFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _EOCDateTo() As Date
'        Set(ByVal value As Date)
'            mdtEOCDateTo = value
'        End Set
'    End Property

'    'Pinkal (27-Mar-2013) -- End


'    'Pinkal (24-Apr-2013) -- Start
'    'Enhancement : TRA Changes

'    Public WriteOnly Property _ShowEmployeeScale() As Boolean
'        Set(ByVal value As Boolean)
'            mblnShowEmpScale = value
'        End Set
'    End Property


'    'Pinkal (24-Apr-2013) -- End


'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    Public WriteOnly Property _FirstNamethenSurname() As Boolean
'        Set(ByVal value As Boolean)
'            mblnFirstNamethenSurname = value
'        End Set
'    End Property
'    'S.SANDEEP [ 26 MAR 2014 ] -- END

'#End Region

'#Region "Public Function & Procedures "

'    Public Sub SetDefaultValue()
'        Try
'            mintEmployeeId = -1
'            mstrEmployeeName = ""
'            mintEmployeementTypeId = -1
'            mstrEmployeementType = ""
'            mintBranchId = -1
'            mstrBranch = ""
'            mintDepartmentId = -1
'            mstrDepartment = ""
'            mintJobId = -1
'            mstrJob = ""
'            mintCostCenterId = -1
'            mstrCostCenter = ""


'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            mblnIsActive = True
'            'Pinkal (24-Jun-2011) -- End


'            'S.SANDEEP [ 06 SEP 2011 ] -- START
'            mintViewIndex = -1
'            mstrViewByIds = ""
'            mstrViewByName = ""
'            'S.SANDEEP [ 06 SEP 2011 ] -- END 



'            'Pinkal (09-Jan-2013) -- Start
'            'Enhancement : TRA Changes
'            mstrAnalysis_Fields = ""
'            mstrAnalysis_Join = ""
'            mstrAnalysis_OrderBy = ""
'            mstrReport_GroupName = ""
'            'Pinkal (09-Jan-2013) -- End

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mstrAdvance_Filter = ""
'            'S.SANDEEP [ 13 FEB 2013 ] -- END


'            'Pinkal (27-Mar-2013) -- Start
'            'Enhancement : TRA Changes

'            mdtBirthDateFrom = Nothing
'            mdtBirthDateTo = Nothing

'            mdtAnniversaryDateFrom = Nothing
'            mdtAnniversaryDateTo = Nothing

'            mdtFirstAppointedDateFrom = Nothing
'            mdtFirstAppointedDateTo = Nothing

'            mdtAppointedDateFrom = Nothing
'            mdtAppointedDateTo = Nothing

'            mdtConfirmationDateFrom = Nothing
'            mdtConfirmationDateTo = Nothing

'            mdtProbationStartDateFrom = Nothing
'            mdtProbationStartDateTo = Nothing

'            mdtProbationEndDateFrom = Nothing
'            mdtProbationEndDateTo = Nothing

'            mdtSuspendedStartDateFrom = Nothing
'            mdtSuspendedStartDateTo = Nothing

'            mdtSuspendedEndDateFrom = Nothing
'            mdtSuspendedEndDateTo = Nothing

'            mdtReinstatementDateFrom = Nothing
'            mdtReinstatementDateTo = Nothing

'            mdtEOCDateFrom = Nothing
'            mdtEOCDateTo = Nothing

'            'Pinkal (27-Mar-2013) -- End


'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes
'            mblnShowEmpScale = False
'            'Pinkal (24-Apr-2013) -- End


'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FilterTitleAndFilterQuery()
'        Me._FilterQuery = ""
'        Me._FilterTitle = ""
'        Try

'            'S.SANDEEP [ 05 MAR 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            'S.SANDEEP [ 05 MAR 2013 ] -- END

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            If mblnIsActive = False Then
'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            End If
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            If mintEmployeeId > 0 Then
'                objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmpId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
'            End If

'            If mintEmployeementTypeId > 0 Then
'                objDataOperation.AddParameter("@emptypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeementTypeId)
'                Me._FilterQuery &= " AND hremployee_master.employmenttypeunkid = @emptypeId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employeement Type : ") & " " & mstrEmployeementType & " "
'            End If

'            If mintBranchId > 0 Then
'                objDataOperation.AddParameter("@branchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
'                Me._FilterQuery &= " AND hremployee_master.stationunkid = @branchId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Branch : ") & " " & mstrBranch & " "
'            End If

'            If mintDepartmentId > 0 Then
'                objDataOperation.AddParameter("@departmentId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
'                Me._FilterQuery &= " AND hremployee_master.departmentunkid = @departmentId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Department : ") & " " & mstrDepartment & " "
'            End If

'            If mintJobId > 0 Then
'                objDataOperation.AddParameter("@jobid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'                Me._FilterQuery &= " AND hremployee_master.jobunkid = @jobid "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Job : ") & " " & mstrJob & " "
'            End If

'            If mintCostCenterId > 0 Then
'                objDataOperation.AddParameter("@costcenterid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterId)
'                Me._FilterQuery &= " AND hremployee_master.costcenterunkid = @costcenterid "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Cost Center : ") & " " & mstrCostCenter & " "
'            End If



'            'Pinkal (27-Mar-2013) -- Start
'            'Enhancement : TRA Changes

'            If mdtBirthDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@BirthdateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtBirthDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.birthdate,112) >= @BirthdateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "BirthDate From : ") & " " & mdtBirthDateFrom.Date & " "
'            End If

'            If mdtBirthDateTo <> Nothing Then
'                objDataOperation.AddParameter("@BirthdateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtBirthDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.birthdate,112) <= @BirthdateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "BirthDate To : ") & " " & mdtBirthDateTo.Date & " "
'            End If

'            If mdtAnniversaryDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@AnniversaryDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAnniversaryDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.anniversary_date,112) >= @AnniversaryDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Anniversary Date From : ") & " " & mdtAnniversaryDateFrom.Date & " "
'            End If

'            If mdtAnniversaryDateTo <> Nothing Then
'                objDataOperation.AddParameter("@AnniversaryDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAnniversaryDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.anniversary_date,112) <= @AnniversaryDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Anniversary Date To : ") & " " & mdtAnniversaryDateTo.Date & " "
'            End If

'            If ConfigParameter._Object._ShowFirstAppointmentDate Then

'                If mdtFirstAppointedDateFrom <> Nothing Then
'                    objDataOperation.AddParameter("@FirstAppointedDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFirstAppointedDateFrom))
'                    Me._FilterQuery &= " AND Convert(char(8),hremployee_master.firstappointment_date,112) >= @FirstAppointedDateFrom "
'                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 36, "First Appointment Date From : ") & " " & mdtFirstAppointedDateFrom.Date & " "
'                End If

'                If mdtFirstAppointedDateTo <> Nothing Then
'                    objDataOperation.AddParameter("@FirstAppointedDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFirstAppointedDateTo))
'                    Me._FilterQuery &= " AND Convert(char(8),hremployee_master.firstappointment_date,112) <= @FirstAppointedDateTo "
'                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "First Appointment Date To : ") & " " & mdtFirstAppointedDateTo.Date & " "
'                End If

'            End If


'            If mdtAppointedDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@AppointedDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAppointedDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.appointeddate,112) >= @AppointedDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Appointment Date From : ") & " " & mdtAppointedDateFrom.Date & " "
'            End If

'            If mdtAppointedDateTo <> Nothing Then
'                objDataOperation.AddParameter("@AppointedDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAppointedDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.appointeddate,112) <= @AppointedDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Appointment Date To : ") & " " & mdtAppointedDateTo.Date & " "
'            End If

'            If mdtConfirmationDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@ConfirmationDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtConfirmationDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.confirmation_date,112) >= @ConfirmationDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Confirmation Date From : ") & " " & mdtConfirmationDateFrom.Date & " "
'            End If

'            If mdtConfirmationDateTo <> Nothing Then
'                objDataOperation.AddParameter("@ConfirmationDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtConfirmationDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.confirmation_date,112) <= @ConfirmationDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Confirmation Date To : ") & " " & mdtConfirmationDateTo.Date & " "
'            End If

'            If mdtProbationStartDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@ProbationStartDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationStartDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.probation_from_date,112) >= @ProbationStartDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Probation Start Date From : ") & " " & mdtProbationStartDateFrom.Date & " "
'            End If

'            If mdtProbationStartDateTo <> Nothing Then
'                objDataOperation.AddParameter("@ProbationStartDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationStartDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.probation_from_date,112) <= @ProbationStartDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Probation Start Date To : ") & " " & mdtProbationStartDateTo.Date & " "
'            End If

'            If mdtProbationEndDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@ProbationEndDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationEndDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.probation_to_date,112) >= @ProbationEndDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Probation End Date From : ") & " " & mdtProbationEndDateFrom.Date & " "
'            End If

'            If mdtProbationEndDateTo <> Nothing Then
'                objDataOperation.AddParameter("@ProbationEndDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtProbationEndDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.probation_to_date,112) <= @ProbationEndDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Probation End Date To : ") & " " & mdtProbationEndDateTo.Date & " "
'            End If

'            If mdtSuspendedStartDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@SuspendedStartDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedStartDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.suspended_from_date,112) >= @SuspendedStartDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Suspended Start Date From : ") & " " & mdtSuspendedStartDateFrom.Date & " "
'            End If

'            If mdtSuspendedStartDateTo <> Nothing Then
'                objDataOperation.AddParameter("@SuspendedStartDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedStartDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.suspended_from_date,112) <= @SuspendedStartDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Suspended Start Date To : ") & " " & mdtSuspendedStartDateTo.Date & " "
'            End If

'            If mdtSuspendedEndDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@SuspendedEndDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedEndDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.suspended_to_date,112) >= @SuspendedEndDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Suspended End Date From : ") & " " & mdtSuspendedEndDateFrom.Date & " "
'            End If

'            If mdtSuspendedEndDateTo <> Nothing Then
'                objDataOperation.AddParameter("@SuspendedEndDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSuspendedEndDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.suspended_to_date,112) <= @SuspendedEndDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Suspended End Date To : ") & " " & mdtSuspendedEndDateTo.Date & " "
'            End If

'            If mdtReinstatementDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@ReinstatementDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtReinstatementDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.reinstatement_date,112) >= @ReinstatementDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Reinstatement Date From : ") & " " & mdtReinstatementDateFrom.Date & " "
'            End If

'            If mdtReinstatementDateTo <> Nothing Then
'                objDataOperation.AddParameter("@ReinstatementDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtReinstatementDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.reinstatement_date,112) <= @ReinstatementDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Reinstatement Date To : ") & " " & mdtReinstatementDateTo.Date & " "
'            End If

'            If mdtEOCDateFrom <> Nothing Then
'                objDataOperation.AddParameter("@EOCDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEOCDateFrom))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.empl_enddate,112) >= @EOCDateFrom "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "End of Contract Date From : ") & " " & mdtEOCDateFrom.Date & " "
'            End If

'            If mdtEOCDateTo <> Nothing Then
'                objDataOperation.AddParameter("@EOCDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEOCDateTo))
'                Me._FilterQuery &= " AND Convert(char(8),hremployee_master.empl_enddate,112) <= @EOCDateTo "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "End of Contract Date To : ") & " " & mdtEOCDateTo.Date & " "
'            End If

'            'Pinkal (27-Mar-2013) -- End



'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim strReportExportFile As String = ""
'        Try

'            'Pinkal (09-Jan-2013) -- Start
'            'Enhancement : TRA Changes

'            menExportAction = ExportAction
'            mdtTableExcel = Nothing

'            'Pinkal (09-Jan-2013) -- End

'            If Not IsNothing(objRpt) Then

'                'Pinkal (09-Jan-2013) -- Start
'                'Enhancement : TRA Changes

'                Dim intArrayColumnWidth As Integer() = Nothing
'                Dim rowsArrayHeader As New ArrayList
'                Dim rowsArrayFooter As New ArrayList
'                Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
'                Dim strarrGroupColumns As String() = Nothing
'                Dim row As WorksheetRow
'                Dim wcell As WorksheetCell

'                If mdtTableExcel IsNot Nothing Then

'                    Dim intGroupColumn As Integer = 0

'                    If mintViewIndex > 0 Then
'                    Else
'                    End If

'                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

'                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
'                        intArrayColumnWidth(i) = 125
'                    Next

'                    '*******   REPORT FOOTER   ******
'                    row = New WorksheetRow()
'                    wcell = New WorksheetCell("", "s8w")
'                    row.Cells.Add(wcell)
'                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
'                    rowsArrayFooter.Add(row)
'                    '----------------------


'                    If ConfigParameter._Object._IsShowCheckedBy = True Then
'                        row = New WorksheetRow()
'                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 33, "Checked By :"), "s8bw")
'                        row.Cells.Add(wcell)

'                        wcell = New WorksheetCell("", "s8w")
'                        row.Cells.Add(wcell)
'                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

'                        rowsArrayFooter.Add(row)

'                        row = New WorksheetRow()
'                        wcell = New WorksheetCell("", "s8w")
'                        row.Cells.Add(wcell)
'                        rowsArrayFooter.Add(row)

'                    End If

'                    If ConfigParameter._Object._IsShowApprovedBy = True Then

'                        row = New WorksheetRow()
'                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 34, "Approved By :"), "s8bw")
'                        row.Cells.Add(wcell)

'                        wcell = New WorksheetCell("", "s8w")
'                        row.Cells.Add(wcell)
'                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

'                        rowsArrayFooter.Add(row)

'                        row = New WorksheetRow()
'                        wcell = New WorksheetCell("", "s8w")
'                        row.Cells.Add(wcell)
'                        rowsArrayFooter.Add(row)

'                    End If

'                    If ConfigParameter._Object._IsShowReceivedBy = True Then
'                        row = New WorksheetRow()
'                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
'                        row.Cells.Add(wcell)

'                        wcell = New WorksheetCell("", "s8w")
'                        row.Cells.Add(wcell)
'                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

'                        rowsArrayFooter.Add(row)

'                        row = New WorksheetRow()
'                        wcell = New WorksheetCell("", "s8w")
'                        row.Cells.Add(wcell)
'                        rowsArrayFooter.Add(row)
'                    End If

'                End If

'                ' Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)

'                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, , True)

'            End If


'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
'        OrderByDisplay = ""
'        OrderByQuery = ""
'        Try
'            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
'            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
'        Try
'            Call OrderByExecute(iColumn_DetailReport)
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
'        Dim strBuilder As New StringBuilder ' 
'        Dim blnFlag As Boolean = False
'        Try

'            strBuilder.Append(" <TITLE> " & Me._ReportName & " " & "</TITLE> " & vbCrLf)
'            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)
'            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
'            If ConfigParameter._Object._IsDisplayLogo = True Then
'                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
'                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
'                Dim objAppSett As New clsApplicationSettings
'                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
'                objAppSett = Nothing
'                If Company._Object._Image IsNot Nothing Then
'                    Company._Object._Image.Save(strImPath)
'                End If
'                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
'                strBuilder.Append(" </TD> " & vbCrLf)
'                strBuilder.Append(" </TR> " & vbCrLf)
'            End If

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 11, "Prepared By :") & " </B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(User._Object._Username & vbCrLf)
'            strBuilder.Append(" </FONT></TD> " & vbCrLf)

'            strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - 4 & " align='center' > " & vbCrLf)

'            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
'            strBuilder.Append(" &nbsp; " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
'            strBuilder.Append(" &nbsp; " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
'            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Date :") & "</B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(Now.Date & vbCrLf)
'            strBuilder.Append(" </FONT></TD> " & vbCrLf)

'            strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - 4 & "  align='center' > " & vbCrLf)

'            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & " " & "</B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
'            strBuilder.Append(" &nbsp; " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" <HR> " & vbCrLf)
'            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
'            strBuilder.Append(" </HR> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)
'            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

'            'Report Column Caption
'            For j As Integer = 0 To objDataReader.Columns.Count - 2
'                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption.ToUpper & "</B></FONT></TD>" & vbCrLf)
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)

'            'Data Part
'            For i As Integer = 0 To objDataReader.Rows.Count - 1
'                strBuilder.Append(" <TR> " & vbCrLf)
'                For k As Integer = 0 To objDataReader.Columns.Count - 2
'                    If CBool(objDataReader.Rows(i)("IsGrp")) = True Then
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B> &nbsp;" & objDataReader.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
'                    Else
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
'                    End If
'                Next
'                strBuilder.Append(" </TR> " & vbCrLf)
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR>  " & vbCrLf)
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" </HTML> " & vbCrLf)


'            If System.IO.Directory.Exists(SavePath) = False Then
'                Dim dig As New Windows.Forms.FolderBrowserDialog
'                dig.Description = "Select Folder Where to export report."

'                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
'                    SavePath = dig.SelectedPath
'                Else
'                    Exit Function
'                End If
'            End If

'            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
'                StrFinalPath = SavePath & "\" & flFileName & ".xls"
'                blnFlag = True
'            Else
'                blnFlag = False
'            End If

'            Return blnFlag

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
'            Return False
'        End Try
'    End Function

'    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
'        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
'        Dim strWriter As New StreamWriter(fsFile)
'        Try
'            With strWriter
'                .BaseStream.Seek(0, SeekOrigin.End)
'                .WriteLine(sb)
'                .Close()
'            End With
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            sb = Nothing
'            strWriter = Nothing
'            fsFile = Nothing
'        End Try
'    End Function

'#End Region

'#Region " Report Generation "

'    Dim iColumn_DetailReport As New IColumnCollection

'    Public Property Field_OnDetailReport() As IColumnCollection
'        Get
'            Return iColumn_DetailReport
'        End Get
'        Set(ByVal value As IColumnCollection)
'            iColumn_DetailReport = value
'        End Set
'    End Property

'    Private Sub Create_OnDetailReport()
'        Try
'            iColumn_DetailReport.Clear()
'            iColumn_DetailReport.Add(New IColumn("", ""))
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (09-Jan-2013) -- Start
'    'Enhancement : TRA Changes

'    'Public Sub Generate_DetailReport(Optional ByVal Isweb As Boolean = False)
'    '    Dim dsList As New DataSet
'    '    Dim exForce As Exception
'    '    Dim StrQ As String = String.Empty
'    '    Try

'    '        If ConfigParameter._Object._ExportReportPath = "" Then
'    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
'    '            Exit Sub
'    '        End If

'    '        objDataOperation = New clsDataOperation
'    '        objDataOperation.ClearParameters()
'    '        objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Level"))
'    '        objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Male"))
'    '        objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Female"))


'    '        'S.SANDEEP [ 06 SEP 2011 ] -- START
'    '        'StrQ = "SELECT  employeecode , " & _
'    '        '            "firstname , " & _
'    '        '            "ISNULL(othername, '') othername , " & _
'    '        '            "surname , " & _
'    '        '            "ISNULL(hrstation_master.name, '') branch , " & _
'    '        '            "ISNULL(hrdepartment_master.name, '') department , " & _
'    '        '            "ISNULL(hrsection_master.name, '') SECTION , " & _
'    '        '            "ISNULL(hrunit_master.name, '') unit , " & _
'    '        '            "ISNULL(hrjob_master.job_name, '') +  ' ' + @Level  + ' ' " & _
'    '        '            "+ CAST(ISNULL(hrjob_master.job_level, 0) AS NVARCHAR(10)) job , " & _
'    '        '            "ISNULL(prcostcenter_master.costcentername, '') costcenter , " & _
'    '        '            "ISNULL(hrgrade_master.name, '') grade , " & _
'    '        '            "ISNULL(hrgradelevel_master.name, '') gradelevel , " & _
'    '        '            "hremployee_master.scale AS salary , " & _
'    '        '            "0.00 AS increment , " & _
'    '        '            "ISNULL(cfcommon_master.name, '') employeementtype , " & _
'    '        '            "ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate, 112), '') AS datehired , " & _
'    '        '            "ISNULL(CONVERT(CHAR(8), hremployee_master.birthdate, 112), '') AS birthdate , " & _
'    '        '            "ISNULL(CAST(YEAR(GETDATE()) - YEAR(hremployee_master.birthdate) AS VARCHAR(3)), " & _
'    '        '                   "'') AS age , " & _
'    '        '            "CASE WHEN gender = 0 THEN '' " & _
'    '        '                 "WHEN gender = 1 THEN @Male " & _
'    '        '                 "WHEN gender = 2 THEN @Female " & _
'    '        '            "END gender , " & _
'    '        '            "ISNULL(MS.name, '') maritalstatus , " & _
'    '        '            "ISNULL(RG.name, '') religion , " & _
'    '        '            "ISNULL(hremployee_master.present_tel_no, '') telephone , " & _
'    '        '            "ISNULL(hremployee_master.present_address1, '') + ' ' " & _
'    '        '            "+ ISNULL(hremployee_master.present_address2, '') presentadd , " & _
'    '        '            "ISNULL(CASE WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' " & _
'    '        '                             "AND ISNULL(hremployee_master.emer_con_tel_no, '') <> '' " & _
'    '        '                        "THEN ISNULL(hremployee_master.emer_con_mobile, '') + ' , ' " & _
'    '        '                             "+ ISNULL(hremployee_master.emer_con_tel_no, '') " & _
'    '        '                        "WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' " & _
'    '        '                        "THEN ISNULL(hremployee_master.emer_con_mobile, '') " & _
'    '        '                        "WHEN ISNULL(hremployee_master.emer_con_tel_no, '') <> '' " & _
'    '        '                        "THEN ISNULL(hremployee_master.emer_con_tel_no, '') " & _
'    '        '                   "END, '') emergence_contact, " & _
'    '        '                   "'' AS bank, " & _
'    '        '                   "'' AS membership " & _
'    '        '                   ", employeeunkid  " & _
'    '        '            " FROM  hremployee_master " & _
'    '        '            " LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
'    '        '            " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'    '        '            " LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
'    '        '            " LEFT JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid " & _
'    '        '            " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'    '        '            " JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
'    '        '            " JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
'    '        '            " JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'    '        '            " JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & _
'    '        '            " LEFT JOIN cfcommon_master MS ON hremployee_master.maritalstatusunkid = MS.masterunkid AND MS.mastertype =  " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
'    '        '            " LEFT JOIN cfcommon_master RG ON hremployee_master.religionunkid = RG.masterunkid AND RG.mastertype = " & clsCommon_Master.enCommonMaster.RELIGION & _
'    '        '            " WHERE 1 = 1 "

'    '        StrQ = "SELECT " & _
'    '                     " employeecode " & _
'    '                     ",firstname " & _
'    '                     ",ISNULL(othername, '') othername " & _
'    '                     ",surname " & _
'    '                     ",ISNULL(hrstation_master.name, '') branch " & _
'    '                     ",ISNULL(hrdepartment_master.name, '') department " & _
'    '                     ",ISNULL(hrsection_master.name, '') Section " & _
'    '                     ",ISNULL(hrunit_master.name, '') unit " & _
'    '                     ",ISNULL(hrjob_master.job_name, '')  job " & _
'    '                     ",ISNULL(prcostcenter_master.costcentername, '') costcenter " & _
'    '                     ",ISNULL(hrgrade_master.name, '') grade " & _
'    '                     ",ISNULL(hrgradelevel_master.name, '') gradelevel " & _
'    '                     ",Cast (hremployee_master.scale AS Decimal(36,6)) AS salary " & _
'    '                     ",0.00 AS increment " & _
'    '                     ",ISNULL(cfcommon_master.name, '') employeementtype " & _
'    '                     ",ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate, 112), '') AS datehired " & _
'    '                     ",ISNULL(CONVERT(CHAR(8), hremployee_master.birthdate, 112), '') AS birthdate " & _
'    '                     ",ISNULL(CAST(YEAR(GETDATE()) - YEAR(hremployee_master.birthdate) AS VARCHAR(3)), '') AS age " & _
'    '                     ",CASE WHEN gender <= 0 THEN '' WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END gender " & _
'    '                     ",ISNULL(MS.name, '') maritalstatus " & _
'    '                     ",ISNULL(RG.name, '') religion " & _
'    '                     ",ISNULL(hremployee_master.present_tel_no, '') telephone " & _
'    '                     ",ISNULL(hremployee_master.present_address1, '') + ' ' + ISNULL(hremployee_master.present_address2, '') presentadd " & _
'    '                     ",ISNULL(CASE WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' AND ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') + ' , ' + ISNULL(hremployee_master.emer_con_tel_no, '') " & _
'    '                                                "WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') " & _
'    '                                                "WHEN ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_tel_no, '') END, '') emergence_contact " & _
'    '                     ",'' AS bank " & _
'    '                     ",'' AS membership " & _
'    '                           ", employeeunkid  "

'    '        'Sohail (16 May 2012) -- Start
'    '        'TRA - ENHANCEMENT
'    '        StrQ &= ", ISNULL(hrdepartment_group_master.name, '') AS departmentgroup " & _
'    '                ", ISNULL(hrsectiongroup_master.name, '') AS sectiongroup " & _
'    '                ", ISNULL(hrunitgroup_master.name, '') AS unitgroup " & _
'    '                ", ISNULL(hrteam_master.name, '') AS team " & _
'    '                ", ISNULL(hrjobgroup_master.name, '') AS jobgroup " & _
'    '                ", ISNULL(hrclassgroup_master.name, '') AS classgroup " & _
'    '                ", ISNULL(hrclasses_master.name, '') AS class " & _
'    '                ", ISNULL(hrgradegroup_master.name, '') AS gradegroup "

'    '        'Sohail (16 May 2012) -- End

'    '        StrQ &= " FROM  hremployee_master " & _
'    '                    " LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
'    '                    " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'    '                    " LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
'    '                    " LEFT JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid " & _
'    '                    " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'    '                    " JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
'    '                    " JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
'    '                    " JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'    '                     "JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
'    '                     "LEFT JOIN cfcommon_master MS ON hremployee_master.maritalstatusunkid = MS.masterunkid AND MS.mastertype =   " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " " & _
'    '                     "LEFT JOIN cfcommon_master RG ON hremployee_master.religionunkid = RG.masterunkid AND RG.mastertype = " & clsCommon_Master.enCommonMaster.RELIGION & " "

'    '        'Sohail (16 May 2012) -- Start
'    '        'TRA - ENHANCEMENT
'    '        StrQ &= "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hremployee_master.deptgroupunkid " & _
'    '                "LEFT JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
'    '                "LEFT JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
'    '                "LEFT JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid " & _
'    '                "LEFT JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
'    '                "LEFT JOIN hrclassgroup_master ON hremployee_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
'    '                "LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
'    '                "LEFT JOIN hrgradegroup_master ON hremployee_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid "
'    '        'Sohail (16 May 2012) -- End

'    '        StrQ &= " WHERE 1 = 1 "


'    '        Select Case mintViewIndex
'    '            Case enAnalysisReport.Branch
'    '                StrQ &= " AND hremployee_master.stationunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Branch : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.Department
'    '                StrQ &= " AND hremployee_master.departmentunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Department : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.Section
'    '                StrQ &= " AND hremployee_master.sectionunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Section : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.Unit
'    '                StrQ &= " AND hremployee_master.unitunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Unit : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.Job
'    '                StrQ &= " AND hremployee_master.jobunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.CostCenter
'    '                StrQ &= " AND hremployee_master.costcenterunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Cost Center : ") & " " & mstrViewByName & " "
'    '                'Sohail (16 May 2012) -- Start
'    '                'TRA - ENHANCEMENT
'    '            Case enAnalysisReport.DepartmentGroup
'    '                StrQ &= " AND hremployee_master.deptgroupunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Department Group : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.SectionGroup
'    '                StrQ &= " AND hremployee_master.sectiongroupunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Section Group : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.UnitGroup
'    '                StrQ &= " AND hremployee_master.unitgroupunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Unit Group : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.Team
'    '                StrQ &= " AND hremployee_master.teamunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Team : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.JobGroup
'    '                StrQ &= " AND hremployee_master.jobgroupunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Job Group : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.ClassGroup
'    '                StrQ &= " AND hremployee_master.classgroupunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Class Group : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.Classs
'    '                StrQ &= " AND hremployee_master.classunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Class : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.GradeGroup
'    '                StrQ &= " AND hremployee_master.gradegroupunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Grade Group : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.Grade
'    '                StrQ &= " AND hremployee_master.gradeunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Grade : ") & " " & mstrViewByName & " "
'    '            Case enAnalysisReport.GradeLevel
'    '                StrQ &= " AND hremployee_master.gradelevelunkid IN (" & mstrViewByIds & ")"
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Grade Level : ") & " " & mstrViewByName & " "
'    '                'Sohail (16 May 2012) -- End

'    '        End Select
'    '        'S.SANDEEP [ 06 SEP 2011 ] -- END 





'    '        'Pinkal (24-Jun-2011) -- Start
'    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'    '        If mblnIsActive = False Then
'    '            'S.SANDEEP [ 12 MAY 2012 ] -- START
'    '            'ISSUE : TRA ENHANCEMENTS
'    '            'StrQ &= " AND hremployee_master.isactive = 1 "
'    '            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
'    '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
'    '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
'    '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
'    '            'S.SANDEEP [ 12 MAY 2012 ] -- END
'    '        End If
'    '        'Pinkal (24-Jun-2011) -- End

'    '        'S.SANDEEP [ 12 MAY 2012 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES

'    '        'S.SANDEEP [ 12 NOV 2012 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES
'    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
'    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
'    '        'End If

'    '        If mstrUserAccessFilter = "" Then
'    '            If UserAccessLevel._AccessLevel.Length > 0 Then
'    '                StrQ &= UserAccessLevel._AccessLevelFilterString
'    '            End If
'    '        Else
'    '            StrQ &= mstrUserAccessFilter
'    '        End If
'    '        'S.SANDEEP [ 12 NOV 2012 ] -- END

'    '        'S.SANDEEP [ 12 MAY 2012 ] -- END


'    '        FilterTitleAndFilterQuery()
'    '        StrQ &= Me._FilterQuery

'    '        StrQ &= "ORDER BY  hremployee_master.employeecode "

'    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If



'    '        'S.SANDEEP [ 06 SEP 2011 ] -- START
'    '        'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
'    '        '    Dim objMaster As New clsMasterData
'    '        '    Dim objEmp As New clsEmployee_Master
'    '        '    Dim objBank As New clsEmployeeBanks

'    '        '    For Each dr As DataRow In dsList.Tables(0).Rows
'    '        '        If dr("datehired").ToString.Trim <> "" Then dr("datehired") = eZeeDate.convertDate(dr("datehired").ToString).ToShortDateString
'    '        '        If dr("birthdate").ToString.Trim <> "" Then dr("birthdate") = eZeeDate.convertDate(dr("birthdate").ToString).ToShortDateString

'    '        '        'START FOR INCREMENT
'    '        '        Dim dsData As DataSet = objMaster.Get_Current_Scale("List", CInt(dr("employeeunkid")), ConfigParameter._Object._CurrentDateAndTime.Date)
'    '        '        If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
'    '        '            dr("increment") = CDbl(dsData.Tables(0).Rows(0)("increment"))
'    '        '        End If
'    '        '        'END FOR INCREMENT

'    '        '        'START FOR MULTIPLE MEMBERSHIP
'    '        '        dsData = objEmp.GetEmployee_Membership(CInt(dr("employeeunkid")))
'    '        '        Dim mstrMembership As String = ""
'    '        '        If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
'    '        '            For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
'    '        '                mstrMembership &= dsData.Tables(0).Rows(i)("Mem_Name").ToString & " - " & dsData.Tables(0).Rows(i)("Mem_No").ToString & ","
'    '        '            Next
'    '        '            If mstrMembership.Trim.Length > 0 Then mstrMembership = mstrMembership.Substring(0, mstrMembership.Length - 1)
'    '        '            dr("membership") = mstrMembership
'    '        '        End If
'    '        '        'END FOR MULTIPLE MEMBERSHIP

'    '        '        'START FOR MULTIPLE BANKS
'    '        '        dsData = objBank.GetEmployeeBanks(CInt(dr("employeeunkid")))
'    '        '        Dim mstrBank As String = ""
'    '        '        If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
'    '        '            For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
'    '        '                mstrBank &= dsData.Tables(0).Rows(i)("BankGrp").ToString & " - " & dsData.Tables(0).Rows(i)("BranchName").ToString & " - " & dsData.Tables(0).Rows(i)("accountno").ToString & ","
'    '        '            Next
'    '        '            If mstrBank.Trim.Length > 0 Then mstrBank = mstrBank.Substring(0, mstrBank.Length - 1)
'    '        '            dr("bank") = mstrBank
'    '        '        End If
'    '        '        'END FOR MULTIPLE BANKS

'    '        '    Next

'    '        'End If

'    '        dtTable = New DataTable("List")
'    '        Dim mdicIsAdded As New Dictionary(Of String, String)
'    '        Dim dtFilter As DataTable = Nothing
'    '        For Each dCol As DataColumn In dsList.Tables(0).Columns


'    '            'Pinkal (09-Jan-2013) -- Start
'    '            'Enhancement : TRA Changes

'    '            If dCol.ColumnName <> "salary" AndAlso dCol.ColumnName <> "increment" Then
'    '                dtTable.Columns.Add(dCol.ColumnName, System.Type.GetType("System.String")).DefaultValue = ""
'    '            Else
'    '                dtTable.Columns.Add(dCol.ColumnName, System.Type.GetType("System.Decimal")).DefaultValue = 0.0
'    '            End If
'    '            'Pinkal (09-Jan-2013) -- End

'    '        Next
'    '        dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False

'    '        'S.SANDEEP [ 12 NOV 2012 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES
'    '        If dsMemData.Tables.Count <= 0 Then
'    '            dsMemData = Employee_Membership()
'    '        End If
'    '        If dsBnkData.Tables.Count <= 0 Then
'    '            dsBnkData = Employee_Bank()
'    '        End If
'    '        'S.SANDEEP [ 12 NOV 2012 ] -- END

'    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
'    '            If mstrViewByIds.Trim.Length > 0 Then
'    '                For Each dr As DataRow In dsList.Tables(0).Rows
'    '                    Select Case mintViewIndex
'    '                        Case enAnalysisReport.Branch
'    '                            If mdicIsAdded.ContainsKey(dr.Item("branch")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("branch"), dr.Item("branch"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 12, "Branch : ") & " " & dr.Item("branch")

'    '                            dtFilter = New DataView(dsList.Tables(0), "branch =  '" & dr.Item("branch").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.Department
'    '                            If mdicIsAdded.ContainsKey(dr.Item("department")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("department"), dr.Item("department"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 13, "Department : ") & " " & dr.Item("department")

'    '                            dtFilter = New DataView(dsList.Tables(0), "department = '" & dr.Item("department") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.Section
'    '                            If mdicIsAdded.ContainsKey(dr.Item("Section")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("Section"), dr.Item("Section"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("IsGrp") = True
'    '                            dtTable.Rows.Add(dtRow)
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 14, "Section : ") & " " & dr.Item("Section")
'    '                            dtFilter = New DataView(dsList.Tables(0), "Section = '" & dr.Item("Section") & "'", "", DataViewRowState.CurrentRows).ToTable
'    '                        Case enAnalysisReport.Unit
'    '                            If mdicIsAdded.ContainsKey(dr.Item("unit")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("unit"), dr.Item("unit"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 15, "Unit : ") & " " & dr.Item("unit")

'    '                            dtFilter = New DataView(dsList.Tables(0), "unit = '" & dr.Item("unit") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.Job
'    '                            If mdicIsAdded.ContainsKey(dr.Item("job")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("job"), dr.Item("job"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 16, "Job : ") & " " & dr.Item("job")
'    '                            dtFilter = New DataView(dsList.Tables(0), "job = '" & dr.Item("job") & "'", "", DataViewRowState.CurrentRows).ToTable
'    '                        Case enAnalysisReport.CostCenter
'    '                            If mdicIsAdded.ContainsKey(dr.Item("costcenter")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("costcenter"), dr.Item("costcenter"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 17, "Cost Center : ") & " " & dr.Item("costcenter")
'    '                            dtFilter = New DataView(dsList.Tables(0), "costcenter = '" & dr.Item("costcenter") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                            'Sohail (16 May 2012) -- Start
'    '                            'TRA - ENHANCEMENT
'    '                        Case enAnalysisReport.DepartmentGroup
'    '                            If mdicIsAdded.ContainsKey(dr.Item("departmentgroup")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("departmentgroup"), dr.Item("departmentgroup"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 20, "Department Group : ") & " " & dr.Item("departmentgroup")

'    '                            dtFilter = New DataView(dsList.Tables(0), "departmentgroup = '" & dr.Item("departmentgroup") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.SectionGroup
'    '                            If mdicIsAdded.ContainsKey(dr.Item("sectiongroup")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("sectiongroup"), dr.Item("sectiongroup"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 21, "Section Group : ") & " " & dr.Item("sectiongroup")

'    '                            dtFilter = New DataView(dsList.Tables(0), "sectiongroup = '" & dr.Item("sectiongroup") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.UnitGroup
'    '                            If mdicIsAdded.ContainsKey(dr.Item("unitgroup")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("unitgroup"), dr.Item("unitgroup"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 22, "Unit Group : ") & " " & dr.Item("unitgroup")

'    '                            dtFilter = New DataView(dsList.Tables(0), "unitgroup = '" & dr.Item("unitgroup") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.Team
'    '                            If mdicIsAdded.ContainsKey(dr.Item("team")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("team"), dr.Item("team"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 23, "Team : ") & " " & dr.Item("team")

'    '                            dtFilter = New DataView(dsList.Tables(0), "team = '" & dr.Item("team") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.JobGroup
'    '                            If mdicIsAdded.ContainsKey(dr.Item("jobgroup")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("jobgroup"), dr.Item("jobgroup"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 24, "Job Group : ") & " " & dr.Item("jobgroup")

'    '                            dtFilter = New DataView(dsList.Tables(0), "jobgroup = '" & dr.Item("jobgroup") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.ClassGroup
'    '                            If mdicIsAdded.ContainsKey(dr.Item("classgroup")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("classgroup"), dr.Item("classgroup"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 25, "Class Group : ") & " " & dr.Item("classgroup")

'    '                            dtFilter = New DataView(dsList.Tables(0), "classgroup = '" & dr.Item("classgroup") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.Classs
'    '                            If mdicIsAdded.ContainsKey(dr.Item("class")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("class"), dr.Item("class"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 26, "Class : ") & " " & dr.Item("class")

'    '                            dtFilter = New DataView(dsList.Tables(0), "class = '" & dr.Item("class") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.GradeGroup
'    '                            If mdicIsAdded.ContainsKey(dr.Item("gradegroup")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("gradegroup"), dr.Item("gradegroup"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 27, "Grade Group : ") & " " & dr.Item("gradegroup")

'    '                            dtFilter = New DataView(dsList.Tables(0), "gradegroup = '" & dr.Item("gradegroup") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.Grade
'    '                            If mdicIsAdded.ContainsKey(dr.Item("grade")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("grade"), dr.Item("grade"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 28, "Grade : ") & " " & dr.Item("grade")

'    '                            dtFilter = New DataView(dsList.Tables(0), "grade = '" & dr.Item("grade") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                        Case enAnalysisReport.GradeLevel
'    '                            If mdicIsAdded.ContainsKey(dr.Item("gradelevel")) Then Continue For
'    '                            mdicIsAdded.Add(dr.Item("gradelevel"), dr.Item("gradelevel"))
'    '                            dtRow = dtTable.NewRow
'    '                            dtRow.Item("employeecode") = Language.getMessage(mstrModuleName, 29, "Grade Level : ") & " " & dr.Item("gradelevel")

'    '                            dtFilter = New DataView(dsList.Tables(0), "gradelevel = '" & dr.Item("gradelevel") & "'", "", DataViewRowState.CurrentRows).ToTable

'    '                            'Sohail (16 May 2012) -- End
'    '                    End Select
'    '                    dtRow.Item("IsGrp") = True
'    '                    dtTable.Rows.Add(dtRow)

'    '                    Call SetData(dtFilter)

'    '                Next
'    '            Else
'    '                Call SetData(dsList.Tables(0))
'    '            End If
'    '        End If



'    '        'S.SANDEEP [ 06 SEP 2011 ] -- END 


'    '        'Pinkal (14-Aug-2012) -- Start
'    '        'Enhancement : TRA Changes

'    '        If Isweb = False Then

'    '            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtTable) Then
'    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Report successfully exported to Report export path."), enMsgBoxStyle.Information)

'    '                'Pinkal (24-Jun-2011) -- Start
'    '                'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'    '                Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
'    '                'Pinkal (24-Jun-2011) -- End

'    '            End If

'    '        Else

'    '            dtData = dtTable

'    '        End If

'    '        'Pinkal (14-Aug-2012) -- End

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
'    '    End Try
'    'End Sub

'    Public Sub Generate_DetailReport(Optional ByVal Isweb As Boolean = False)
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim StrQ As String = String.Empty
'        Try

'            'Nilay (18-Mar-2015) -- Start
'            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
'            'If ConfigParameter._Object._ExportReportPath = "" Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
'            '    Exit Sub
'            'End If
'            'Nilay (18-Mar-2015) -- End

'            objDataOperation = New clsDataOperation
'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Level"))
'            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Male"))
'            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Female"))

'            'S.SANDEEP [ 05 MAR 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES

'            'StrQ = "SELECT " & _
'            '             " employeecode " & _
'            '             ",firstname " & _
'            '             ",ISNULL(othername, '') othername " & _
'            '             ",surname " & _
'            '             ",ISNULL(st.name, '') branch " & _
'            '             ",ISNULL(dm.name, '') department " & _
'            '             ",ISNULL(sm.name, '') Section " & _
'            '             ",ISNULL(um.name, '') unit " & _
'            '             ",ISNULL(jm.job_name, '')  job " & _
'            '             ",ISNULL(pcm.costcentername, '') costcenter " & _
'            '             ",ISNULL(gm.name, '') grade " & _
'            '             ",ISNULL(glm.name, '') gradelevel " & _
'            '             ",Cast (hremployee_master.scale AS Decimal(36,6)) AS salary " & _
'            '             ",0.00 AS increment " & _
'            '             ",ISNULL(cm.name, '') employeementtype " & _
'            '             ",ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate, 112), '') AS datehired " & _
'            '             ",ISNULL(CONVERT(CHAR(8), hremployee_master.birthdate, 112), '') AS birthdate " & _
'            '             ",ISNULL(CAST(YEAR(GETDATE()) - YEAR(hremployee_master.birthdate) AS VARCHAR(3)), '') AS age " & _
'            '             ",CASE WHEN gender <= 0 THEN '' WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END gender " & _
'            '             ",ISNULL(MS.name, '') maritalstatus " & _
'            '             ",ISNULL(RG.name, '') religion " & _
'            '             ",ISNULL(hremployee_master.present_tel_no, '') telephone " & _
'            '             ",ISNULL(hremployee_master.present_address1, '') + ' ' + ISNULL(hremployee_master.present_address2, '') presentadd " & _
'            '             ",ISNULL(CASE WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' AND ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') + ' , ' + ISNULL(hremployee_master.emer_con_tel_no, '') " & _
'            '                                        "WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') " & _
'            '                                        "WHEN ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_tel_no, '') END, '') emergence_contact " & _
'            '             ",'' AS bank " & _
'            '             ",'' AS membership " & _
'            '             ", employeeunkid  " & _
'            '             ", ISNULL(dgm.name, '') AS departmentgroup " & _
'            '             ", ISNULL(sgm.name, '') AS sectiongroup " & _
'            '             ", ISNULL(ugm.name, '') AS unitgroup " & _
'            '             ", ISNULL(tm.name, '') AS team " & _
'            '             ", ISNULL(jgm.name, '') AS jobgroup " & _
'            '             ", ISNULL(cgm.name, '') AS classgroup " & _
'            '             ", ISNULL(clm.name, '') AS class " & _
'            '             ", ISNULL(ggm.name, '') AS gradegroup "
'            StrQ = "SELECT " & _
'                   " employeecode AS [Employee Code] " & _
'                   ",firstname AS Firstname " & _
'                   ",ISNULL(othername, '') AS Othername " & _
'                   ",surname AS Surname " & _
'                   ",ISNULL(st.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 63, "Branch") = "", "Branch", Language.getMessage(mstrModuleName, 63, "Branch")) & "] " & _
'                   ",ISNULL(dm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 64, "Department") = "", "Department", Language.getMessage(mstrModuleName, 64, "Department")) & "] " & _
'                   ",ISNULL(sm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 65, "Section") = "", "Section", Language.getMessage(mstrModuleName, 65, "Section")) & "] " & _
'                   ",ISNULL(um.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 66, "Unit") = "", "Unit", Language.getMessage(mstrModuleName, 66, "Unit")) & "] " & _
'                   ",ISNULL(jm.job_name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 67, "Job") = "", "Job", Language.getMessage(mstrModuleName, 67, "Job")) & "] " & _
'                   ",ISNULL(pcm.costcentername, '') AS [" & IIf(Language.getMessage(mstrModuleName, 68, "CostCenter") = "", "CostCenter", Language.getMessage(mstrModuleName, 68, "CostCenter")) & "] " & _
'                   ",ISNULL(ggm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 69, "Grade Group") = "", "Grade Group", Language.getMessage(mstrModuleName, 69, "Grade Group")) & "] " & _
'                   ",ISNULL(gm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 70, "Grade") = "", "Grade", Language.getMessage(mstrModuleName, 70, "Grade")) & "] " & _
'                   ",ISNULL(glm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 71, "Grade Level") = "", "Grade Level", Language.getMessage(mstrModuleName, 71, "Grade Level")) & "] "


'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes
'            If mblnShowEmpScale Then
'                StrQ &= ",CAST (newscale AS DECIMAL(36, 6)) AS [" & IIf(Language.getMessage(mstrModuleName, 39, "Salary") = "", "Salary", Language.getMessage(mstrModuleName, 39, "Salary")) & "] "
'            End If
'            'Pinkal (24-Apr-2013) -- End

'            StrQ &= ",0.00 AS [" & IIf(Language.getMessage(mstrModuleName, 40, "Increment") = "", "Increment", Language.getMessage(mstrModuleName, 40, "Increment")) & "] " & _
'                   ",ISNULL(cm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 41, "Employment Type") = "", "Employment Type", Language.getMessage(mstrModuleName, 41, "Employment Type")) & "] " & _
'                   ",ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 42, "Date Hired") = "", "Date Hired", Language.getMessage(mstrModuleName, 42, "Date Hired")) & "] " & _
'                   ",ISNULL(CONVERT(CHAR(8), hremployee_master.birthdate, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 43, "Birthdate") = "", "Birthdate", Language.getMessage(mstrModuleName, 43, "Birthdate")) & "] " & _
'                   ",ISNULL(CAST(YEAR(GETDATE()) - YEAR(hremployee_master.birthdate) AS VARCHAR(3)), '') AS [" & IIf(Language.getMessage(mstrModuleName, 44, "Age") = "", "Age", Language.getMessage(mstrModuleName, 44, "Age")) & "] " & _
'                   ",CASE WHEN gender <= 0 THEN '' WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END AS [" & IIf(Language.getMessage(mstrModuleName, 45, "Gender") = "", "Gender", Language.getMessage(mstrModuleName, 45, "Gender")) & "] " & _
'                   ",ISNULL(MS.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 46, "Marital Status") = "", "Marital Status", Language.getMessage(mstrModuleName, 46, "Marital Status")) & "] " & _
'                   ",ISNULL(RG.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 47, "Religion") = "", "Religion", Language.getMessage(mstrModuleName, 47, "Religion")) & "] " & _
'                   ",ISNULL(hremployee_master.present_tel_no, '') AS [" & IIf(Language.getMessage(mstrModuleName, 48, "Telephone") = "", "Telephone", Language.getMessage(mstrModuleName, 48, "Telephone")) & "] " & _
'                   ",ISNULL(hremployee_master.present_address1, '') + ' ' + ISNULL(hremployee_master.present_address2, '') AS [" & IIf(Language.getMessage(mstrModuleName, 49, "Present Address") = "", "Present Address", Language.getMessage(mstrModuleName, 49, "Present Address")) & "] " & _
'                   ",ISNULL(hremployee_master.email, '') AS [" & IIf(Language.getMessage(mstrModuleName, 442, "Email") = "", "Email", Language.getMessage(mstrModuleName, 442, "Email")) & "] " & _
'                          ",ISNULL(CASE WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' AND ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') + ' , ' + ISNULL(hremployee_master.emer_con_tel_no, '') " & _
'                                                    "WHEN ISNULL(hremployee_master.emer_con_mobile, '') <> '' THEN ISNULL(hremployee_master.emer_con_mobile, '') " & _
'                                              "WHEN ISNULL(hremployee_master.emer_con_tel_no, '') <> '' THEN ISNULL(hremployee_master.emer_con_tel_no, '') END, '') AS [" & IIf(Language.getMessage(mstrModuleName, 50, "Emergency Contact") = "", "Emergency Contact", Language.getMessage(mstrModuleName, 50, "Emergency Contact")) & "] " & _
'                   ",'' AS [" & IIf(Language.getMessage(mstrModuleName, 51, "Bank") = "", "Bank", Language.getMessage(mstrModuleName, 51, "Bank")) & "] " & _
'                   ",'' AS [" & IIf(Language.getMessage(mstrModuleName, 52, "Membership") = "", "Membership", Language.getMessage(mstrModuleName, 52, "Membership")) & "] " & _
'                   ", employeeunkid  " & _
'                   ", ISNULL(dgm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 53, "Department Group") = "", "Department Group", Language.getMessage(mstrModuleName, 53, "Department Group")) & "] " & _
'                   ", ISNULL(sgm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 54, "Section Group") = "", "Section Group", Language.getMessage(mstrModuleName, 54, "Section Group")) & "] " & _
'                   ", ISNULL(ugm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 55, "Unit Group") = "", "Unit Group", Language.getMessage(mstrModuleName, 55, "Unit Group")) & "] " & _
'                   ", ISNULL(tm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 56, "Team") = "", "Team", Language.getMessage(mstrModuleName, 56, "Team")) & "] " & _
'                   ", ISNULL(jgm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 57, "Job Group") = "", "Job Group", Language.getMessage(mstrModuleName, 57, "Job Group")) & "] " & _
'                   ", ISNULL(cgm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 58, "Class Group") = "", "Class Group", Language.getMessage(mstrModuleName, 58, "Class Group")) & "] " & _
'                   ", ISNULL(clm.name, '') AS [" & IIf(Language.getMessage(mstrModuleName, 59, "Class") = "", "Class", Language.getMessage(mstrModuleName, 59, "Class")) & "] "
'            'S.SANDEEP [ 05 MAR 2013 ] -- END


'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes

'            'Pinkal (27-Mar-2013) -- Start
'            'Enhancement : TRA Changes

'            StrQ &= ",ISNULL(CONVERT(CHAR(8), hremployee_master.anniversary_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 72, "Anniversary Date") = "", "Anniversary Date", Language.getMessage(mstrModuleName, 72, "Anniversary Date")) & "]"

'            If ConfigParameter._Object._ShowFirstAppointmentDate Then
'                StrQ &= ",ISNULL(CONVERT(CHAR(8), hremployee_master.firstappointment_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 73, "First Appointment Date") = "", "First Appointment Date", Language.getMessage(mstrModuleName, 73, "First Appointment Date")) & "]"
'            End If

'            StrQ &= ",ISNULL(CONVERT(CHAR(8), hremployee_master.confirmation_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 74, "Confirmation Date") = "", "Confirmation Date", Language.getMessage(mstrModuleName, 74, "Confirmation Date")) & "]" & _
'                         ",ISNULL(CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 75, "Reinstatement Date") = "", "Reinstatement Date", Language.getMessage(mstrModuleName, 75, "Reinstatement Date")) & "]" & _
'                         ",ISNULL(CONVERT(CHAR(8), hremployee_master.probation_from_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 76, "Probation Start Date") = "", "Probation Start Date", Language.getMessage(mstrModuleName, 76, "Probation Start Date")) & "]" & _
'                         ",ISNULL(CONVERT(CHAR(8), hremployee_master.probation_to_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 77, "Probation End Date") = "", "Probation Start Date", Language.getMessage(mstrModuleName, 77, "Probation End Date")) & "]" & _
'                         ",ISNULL(CONVERT(CHAR(8), hremployee_master.suspended_from_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 78, "Suspended From Date") = "", "Suspended From Date", Language.getMessage(mstrModuleName, 78, "Suspended From Date")) & "]" & _
'                         ",ISNULL(CONVERT(CHAR(8), hremployee_master.suspended_to_date, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 79, "Suspended To Date") = "", "Suspended To Date", Language.getMessage(mstrModuleName, 79, "Suspended To Date")) & "]" & _
'                         ",ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), '') AS [" & IIf(Language.getMessage(mstrModuleName, 80, "End Of Contract") = "", "End Of Contract", Language.getMessage(mstrModuleName, 80, "End Of Contract")) & "]"

'            'Pinkal (27-Mar-2013) -- End

'            'Pinkal (24-Apr-2013) -- End


'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ",0 AS Id, '' AS GName "
'            End If


'            'S.SANDEEP [ 05 MAR 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ &= " FROM  hremployee_master " & _
'            '        "   LEFT JOIN hrstation_master st ON hremployee_master.stationunkid = st.stationunkid " & _
'            '        "   JOIN hrdepartment_master dm ON hremployee_master.departmentunkid = dm.departmentunkid " & _
'            '        "   LEFT JOIN hrsection_master sm ON hremployee_master.sectionunkid = sm.sectionunkid " & _
'            '        "   LEFT JOIN hrunit_master um ON hremployee_master.unitunkid = um.unitunkid " & _
'            '        "   JOIN hrjob_master jm ON hremployee_master.jobunkid = jm.jobunkid " & _
'            '        "   JOIN prcostcenter_master pcm ON hremployee_master.costcenterunkid = pcm.costcenterunkid " & _
'            '        "   JOIN hrgrade_master gm ON hremployee_master.gradeunkid = gm.gradeunkid " & _
'            '        "   JOIN hrgradelevel_master glm ON hremployee_master.gradelevelunkid = glm.gradelevelunkid " & _
'            '        "   JOIN cfcommon_master cm ON hremployee_master.employmenttypeunkid = cm.masterunkid AND cm.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
'            '        "   LEFT JOIN cfcommon_master MS ON hremployee_master.maritalstatusunkid = MS.masterunkid AND MS.mastertype =   " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " " & _
'            '        "   LEFT JOIN cfcommon_master RG ON hremployee_master.religionunkid = RG.masterunkid AND RG.mastertype = " & clsCommon_Master.enCommonMaster.RELIGION & " " & _
'            '        "   LEFT JOIN hrdepartment_group_master dgm ON dgm.deptgroupunkid = hremployee_master.deptgroupunkid " & _
'            '        "   LEFT JOIN hrsectiongroup_master sgm ON hremployee_master.sectiongroupunkid = sgm.sectiongroupunkid " & _
'            '        "   LEFT JOIN hrunitgroup_master ugm ON hremployee_master.unitgroupunkid = ugm.unitgroupunkid " & _
'            '        "   LEFT JOIN hrteam_master tm ON hremployee_master.teamunkid = tm.teamunkid " & _
'            '        "   LEFT JOIN hrjobgroup_master jgm ON hremployee_master.jobgroupunkid = jgm.jobgroupunkid " & _
'            '        "   LEFT JOIN hrclassgroup_master cgm ON hremployee_master.classgroupunkid = cgm.classgroupunkid " & _
'            '        "   LEFT JOIN hrclasses_master clm ON hremployee_master.classunkid = clm.classesunkid " & _
'            '        "   LEFT JOIN hrgradegroup_master ggm ON hremployee_master.gradegroupunkid = ggm.gradegroupunkid "

'            StrQ &= " FROM  hremployee_master " & _
'                    "   LEFT JOIN " & _
'                    "   ( " & _
'                    "       SELECT " & _
'                    "            EId,gradegroupunkid " & _
'                    "           ,gradeunkid,gradelevelunkid " & _
'                    "           ,newscale,Rno " & _
'                    "       FROM " & _
'                    "       ( " & _
'                    "           SELECT " & _
'                    "                employeeunkid AS EId " & _
'                    "               ,gradegroupunkid " & _
'                    "               ,gradeunkid " & _
'                    "               ,gradelevelunkid " & _
'                    "               ,newscale " & _
'                    "               ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC) AS Rno " & _
'                    "           FROM prsalaryincrement_tran " & _
'                    "           WHERE isvoid=0 AND incrementdate <= @Date AND isapproved = 1 " & _
'                    "       )AS A WHERE A.Rno = 1 " & _
'                    "   )AS Sal ON hremployee_master.employeeunkid = Sal.EId " & _
'                    "   JOIN hrgradegroup_master ggm ON Sal.gradegroupunkid = ggm.gradegroupunkid " & _
'                    "   JOIN hrgrade_master gm ON Sal.gradeunkid = gm.gradeunkid " & _
'                    "   JOIN hrgradelevel_master glm ON Sal.gradelevelunkid = glm.gradelevelunkid " & _
'                        " LEFT JOIN hrstation_master st ON hremployee_master.stationunkid = st.stationunkid " & _
'                        " JOIN hrdepartment_master dm ON hremployee_master.departmentunkid = dm.departmentunkid " & _
'                        " LEFT JOIN hrsection_master sm ON hremployee_master.sectionunkid = sm.sectionunkid " & _
'                        " LEFT JOIN hrunit_master um ON hremployee_master.unitunkid = um.unitunkid " & _
'                        " JOIN hrjob_master jm ON hremployee_master.jobunkid = jm.jobunkid " & _
'                        " JOIN prcostcenter_master pcm ON hremployee_master.costcenterunkid = pcm.costcenterunkid " & _
'                         "JOIN cfcommon_master cm ON hremployee_master.employmenttypeunkid = cm.masterunkid AND cm.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
'                         "LEFT JOIN cfcommon_master MS ON hremployee_master.maritalstatusunkid = MS.masterunkid AND MS.mastertype =   " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " " & _
'                         "LEFT JOIN cfcommon_master RG ON hremployee_master.religionunkid = RG.masterunkid AND RG.mastertype = " & clsCommon_Master.enCommonMaster.RELIGION & " " & _
'                         "LEFT JOIN hrdepartment_group_master dgm ON dgm.deptgroupunkid = hremployee_master.deptgroupunkid " & _
'                         "LEFT JOIN hrsectiongroup_master sgm ON hremployee_master.sectiongroupunkid = sgm.sectiongroupunkid " & _
'                         "LEFT JOIN hrunitgroup_master ugm ON hremployee_master.unitgroupunkid = ugm.unitgroupunkid " & _
'                         "LEFT JOIN hrteam_master tm ON hremployee_master.teamunkid = tm.teamunkid " & _
'                         "LEFT JOIN hrjobgroup_master jgm ON hremployee_master.jobgroupunkid = jgm.jobgroupunkid " & _
'                         "LEFT JOIN hrclassgroup_master cgm ON hremployee_master.classgroupunkid = cgm.classgroupunkid " & _
'                         "LEFT JOIN hrclasses_master clm ON hremployee_master.classunkid = clm.classesunkid "
'            'S.SANDEEP [ 05 MAR 2013 ] -- END


'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If

'            StrQ &= " WHERE 1 = 1 "

'            If mblnIsActive = False Then
'                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
'            End If

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If

'            FilterTitleAndFilterQuery()
'            StrQ &= Me._FilterQuery


'            'Pinkal (09-Jan-2013) -- Start
'            'Enhancement : TRA Changes
'            If mintViewIndex > 0 Then
'                StrQ &= "ORDER BY " & mstrAnalysis_OrderBy & ",  hremployee_master.employeecode "
'            Else
'                StrQ &= "ORDER BY  hremployee_master.employeecode "
'            End If

'            'Pinkal (09-Jan-2013) -- End



'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If


'            'Sohail (23 May 2014) -- Start
'            'Issue : Employee banks not coming when first filtered by one employee and then removing empoyee filter
'            'If dsMemData.Tables.Count <= 0 Then
'            '    dsMemData = Employee_Membership()
'            'End If
'            'If dsBnkData.Tables.Count <= 0 Then
'            '    dsBnkData = Employee_Bank()
'            'End If
'            dsMemData = Employee_Membership()
'            dsBnkData = Employee_Bank()
'            'Sohail (23 May 2014) -- End

'            Call SetData(dsList.Tables(0))

'            If Isweb = False Then


'                'Pinkal (09-Jan-2013) -- Start
'                'Enhancement : TRA Changes

'                'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtTable) Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Report successfully exported to Report export path."), enMsgBoxStyle.Information)
'                '    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
'                'End If


'                Dim objDic As New Dictionary(Of Integer, Object)
'                Dim strarrGroupColumns As String() = Nothing
'                Dim rowsArrayHeader As New ArrayList

'                mdtTableExcel = dsList.Tables(0)


'                mdtTableExcel.Columns.Remove("employeeunkid")
'                mdtTableExcel.Columns.Remove("Id")
'                'START TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

'                If mintViewIndex <= 0 Then
'                    mdtTableExcel.Columns.Remove("GName")
'                Else
'                    mdtTableExcel.Columns("GName").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Remove(mstrReport_GroupName.IndexOf(":")).Trim, mstrReport_GroupName.Trim)
'                    Dim strGrpCols As String() = {"GName"}
'                    strarrGroupColumns = strGrpCols
'                End If

'                'END TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

'                'SET EXCEL CELL WIDTH
'                Dim intArrayColumnWidth As Integer() = Nothing
'                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
'                For i As Integer = 0 To intArrayColumnWidth.Length - 1
'                    intArrayColumnWidth(i) = 150
'                Next
'                'SET EXCEL CELL WIDTH


'                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", "", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, False)

'                'Pinkal (09-Jan-2013) -- End

'            Else


'                'Pinkal (24-Apr-2013) -- Start
'                'Enhancement : TRA Changes
'                'dtData = dtTable
'                dtData = dsList.Tables(0)
'                'Pinkal (24-Apr-2013) -- End



'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (09-Jan-2013) -- End

'    Private Sub SetData(ByVal dt As DataTable)

'        Dim objMaster As New clsMasterData
'        Dim objEmp As New clsEmployee_Master
'        Dim objBank As New clsEmployeeBanks

'        For Each dFRow As DataRow In dt.Rows


'            'Pinkal (09-Jan-2013) -- Start
'            'Enhancement : TRA Changes

'            ' dtRow = dFRow.NewRow

'            'dtRow.Item("employeecode") = Space(5) & dFRow.Item("employeecode")
'            'dtRow.Item("firstname") = dFRow.Item("firstname")
'            'dtRow.Item("othername") = dFRow.Item("othername")
'            'dtRow.Item("surname") = dFRow.Item("surname")
'            'dtRow.Item("branch") = dFRow.Item("branch")
'            'dtRow.Item("department") = dFRow.Item("department")
'            'dtRow.Item("Section") = dFRow.Item("Section")
'            'dtRow.Item("unit") = dFRow.Item("unit")
'            'dtRow.Item("job") = dFRow.Item("job")
'            'dtRow.Item("costcenter") = dFRow.Item("costcenter")
'            ''Sohail (16 May 2012) -- Start
'            ''TRA - ENHANCEMENT
'            'dtRow.Item("departmentgroup") = dFRow.Item("departmentgroup")
'            'dtRow.Item("sectiongroup") = dFRow.Item("sectiongroup")
'            'dtRow.Item("unitgroup") = dFRow.Item("unitgroup")
'            'dtRow.Item("team") = dFRow.Item("team")
'            'dtRow.Item("jobgroup") = dFRow.Item("jobgroup")
'            'dtRow.Item("classgroup") = dFRow.Item("classgroup")
'            'dtRow.Item("class") = dFRow.Item("class")
'            'dtRow.Item("gradegroup") = dFRow.Item("gradegroup")
'            ''Sohail (16 May 2012) -- End
'            'dtRow.Item("grade") = dFRow.Item("grade")
'            'dtRow.Item("gradelevel") = dFRow.Item("gradelevel")

'            'dtRow.Item("salary") = Format(CDec(dFRow.Item("salary")), GUI.fmtCurrency)


'            'Dim dsData As DataSet = objMaster.Get_Current_Scale("List", CInt(dFRow.Item("employeeunkid")), ConfigParameter._Object._CurrentDateAndTime.Date)
'            'If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
'            '    dtRow.Item("increment") = CDbl(dsData.Tables(0).Rows(0)("increment"))
'            'End If
'            'dtRow.Item("employeementtype") = dFRow.Item("employeementtype")

'            'If dFRow.Item("datehired").ToString.Trim.Length > 0 Then
'            '    dtRow.Item("datehired") = eZeeDate.convertDate(dFRow.Item("datehired").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("birthdate").ToString.Trim.Length > 0 Then
'            '    dtRow.Item("birthdate") = eZeeDate.convertDate(dFRow.Item("birthdate").ToString).ToShortDateString
'            'End If

'            'dtRow.Item("age") = dFRow.Item("age")
'            'dtRow.Item("gender") = dFRow.Item("gender")
'            'dtRow.Item("maritalstatus") = dFRow.Item("maritalstatus")
'            'dtRow.Item("religion") = dFRow.Item("religion")
'            'dtRow.Item("telephone") = dFRow.Item("telephone")
'            'dtRow.Item("presentadd") = dFRow.Item("presentadd")
'            'dtRow.Item("emergence_contact") = dFRow.Item("emergence_contact")

'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'dsData = clsEmployee_Master.GetEmployee_Membership(CInt(dFRow.Item("employeeunkid")))
'            'Dim mstrMembership As String = ""
'            'If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
'            '    For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
'            '        mstrMembership &= dsData.Tables(0).Rows(i)("Mem_Name").ToString & " - " & dsData.Tables(0).Rows(i)("Mem_No").ToString & ","
'            '    Next
'            '    If mstrMembership.Trim.Length > 0 Then mstrMembership = mstrMembership.Substring(0, mstrMembership.Length - 1)
'            '    dtRow.Item("membership") = mstrMembership
'            'End If

'            'dsData = objBank.GetEmployeeBanks(CInt(dFRow.Item("employeeunkid")))
'            'Dim mstrBank As String = ""
'            'If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
'            '    For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
'            '        mstrBank &= dsData.Tables(0).Rows(i)("BankGrp").ToString & " - " & dsData.Tables(0).Rows(i)("BranchName").ToString & " - " & dsData.Tables(0).Rows(i)("accountno").ToString & ","
'            '    Next
'            '    If mstrBank.Trim.Length > 0 Then mstrBank = mstrBank.Substring(0, mstrBank.Length - 1)
'            '    dtRow.Item("bank") = mstrBank
'            'End If

'            'Dim dTmp() As DataRow = dsMemData.Tables(0).Select("employeeunkid = '" & CInt(dFRow.Item("employeeunkid")) & "'")
'            'If dTmp.Length > 0 Then
'            '    dtRow.Item("membership") = dTmp(0).Item("csv")
'            'End If

'            'dTmp = dsBnkData.Tables(0).Select("employeeunkid = '" & CInt(dFRow.Item("employeeunkid")) & "'")
'            'If dTmp.Length > 0 Then
'            '    dtRow.Item("bank") = dTmp(0).Item("csv")
'            'End If



'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes

'            'Pinkal (27-Mar-2013) -- Start
'            'Enhancement : TRA Changes

'            'Pinkal (06-May-2014) -- Start
'            'Enhancement : Language Changes 

'            'If dFRow.Item("birthdate").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("birthdate") = eZeeDate.convertDate(dFRow.Item("birthdate").ToString).ToShortDateString
'            'End If


'            'If dFRow.Item("Anniversary Date").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Anniversary Date") = eZeeDate.convertDate(dFRow.Item("Anniversary Date").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("Date Hired").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Date Hired") = eZeeDate.convertDate(dFRow.Item("Date Hired").ToString).ToShortDateString
'            'End If

'            'If ConfigParameter._Object._ShowFirstAppointmentDate Then
'            '    If dFRow.Item("First Appointment Date").ToString.Trim.Length > 0 Then
'            '        dFRow.Item("First Appointment Date") = eZeeDate.convertDate(dFRow.Item("First Appointment Date").ToString).ToShortDateString
'            '    End If
'            'End If

'            'If dFRow.Item("Confirmation Date").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Confirmation Date") = eZeeDate.convertDate(dFRow.Item("Confirmation Date").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("Reinstatement Date").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Reinstatement Date") = eZeeDate.convertDate(dFRow.Item("Reinstatement Date").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("Probation Start Date").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Probation Start Date") = eZeeDate.convertDate(dFRow.Item("Probation Start Date").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("Probation End Date").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Probation End Date") = eZeeDate.convertDate(dFRow.Item("Probation End Date").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("Suspended From Date").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Suspended From Date") = eZeeDate.convertDate(dFRow.Item("Suspended From Date").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("Suspended To Date").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("Suspended To Date") = eZeeDate.convertDate(dFRow.Item("Suspended To Date").ToString).ToShortDateString
'            'End If

'            'If dFRow.Item("End Of Contract").ToString.Trim.Length > 0 Then
'            '    dFRow.Item("End Of Contract") = eZeeDate.convertDate(dFRow.Item("End Of Contract").ToString).ToShortDateString
'            'End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 43, "Birthdate")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 43, "Birthdate")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 43, "Birthdate")).ToString).ToShortDateString
'            End If


'            If dFRow.Item(Language.getMessage(mstrModuleName, 72, "Anniversary Date")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 72, "Anniversary Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 72, "Anniversary Date")).ToString).ToShortDateString
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 42, "Date Hired")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 42, "Date Hired")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 42, "Date Hired")).ToString).ToShortDateString
'            End If

'            If ConfigParameter._Object._ShowFirstAppointmentDate Then
'                If dFRow.Item(Language.getMessage(mstrModuleName, 73, "First Appointment Date")).ToString.Trim.Length > 0 Then
'                    dFRow.Item(Language.getMessage(mstrModuleName, 73, "First Appointment Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 73, "First Appointment Date")).ToString).ToShortDateString
'                End If
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 74, "Confirmation Date")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 74, "Confirmation Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 74, "Confirmation Date")).ToString).ToShortDateString
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 75, "Reinstatement Date")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 75, "Reinstatement Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 75, "Reinstatement Date")).ToString).ToShortDateString
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 76, "Probation Start Date")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 76, "Probation Start Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 76, "Probation Start Date")).ToString).ToShortDateString
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 77, "Probation End Date")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 77, "Probation End Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 77, "Probation End Date")).ToString).ToShortDateString
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 78, "Suspended From Date")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 78, "Suspended From Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 78, "Suspended From Date")).ToString).ToShortDateString
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 79, "Suspended To Date")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 79, "Suspended To Date")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 79, "Suspended To Date")).ToString).ToShortDateString
'            End If

'            If dFRow.Item(Language.getMessage(mstrModuleName, 80, "End Of Contract")).ToString.Trim.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 80, "End Of Contract")) = eZeeDate.convertDate(dFRow.Item(Language.getMessage(mstrModuleName, 80, "End Of Contract")).ToString).ToShortDateString
'            End If

'            'Pinkal (27-Mar-2013) -- End

'            'Pinkal (24-Apr-2013) -- End


'            Dim dTmp() As DataRow = dsMemData.Tables(0).Select("employeeunkid = '" & CInt(dFRow.Item("employeeunkid")) & "'")
'            If dTmp.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 52, "Membership")) = dTmp(0).Item("csv")
'            End If

'            dTmp = dsBnkData.Tables(0).Select("employeeunkid = '" & CInt(dFRow.Item("employeeunkid")) & "'")
'            If dTmp.Length > 0 Then
'                dFRow.Item(Language.getMessage(mstrModuleName, 51, "Bank")) = dTmp(0).Item("csv")
'            End If

'            'Pinkal (06-May-2014) -- End

'            'S.SANDEEP [ 12 NOV 2012 ] -- END


'            'dtTable.Rows.Add(dtRow)

'            'Pinkal (09-Jan-2013) -- End
'        Next
'    End Sub

'    'S.SANDEEP [ 12 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Function Employee_Membership() As DataSet
'        Dim dsData As New DataSet
'        Dim objDataOperation As New clsDataOperation
'        Dim StrQ As String = "" : Dim exForce As Exception
'        Try
'            StrQ = "SELECT " & _
'                   " employeeunkid " & _
'                   ",ISNULL(STUFF((SELECT ',' + " & _
'                   "        ISNULL(hrmembership_master.membershipname,'')+' - '+ISNULL(hremployee_meminfo_tran.membershipno,'') " & _
'                   "    FROM hremployee_meminfo_tran " & _
'                   "        JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
'                   "    WHERE hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                   "        AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 FOR XML PATH('')),1,1,''),'') AS csv " & _
'                   "FROM hremployee_master WHERE 1 = 1 "
'            If mintEmployeeId > 0 Then
'                StrQ &= "AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
'            End If

'            If mintEmployeementTypeId > 0 Then
'                StrQ &= "AND hremployee_master.employmenttypeunkid = '" & mintEmployeementTypeId & "' "
'            End If

'            StrQ &= " ORDER BY hremployee_master.employeeunkid "

'            dsData = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsData
'        Catch ex As Exception
'            Throw New Exception("-1," & ex.Message & ",Employee_Membership")
'        End Try
'    End Function

'    Private Function Employee_Bank() As DataSet
'        Dim dsData As New DataSet
'        Dim objDataOperation As New clsDataOperation
'        Dim StrQ As String = "" : Dim exForce As Exception
'        Try
'            StrQ = "SELECT " & _
'                   " hremployee_master.employeeunkid " & _
'                   ",ISNULL(STUFF((SELECT ',' + " & _
'                   "            hrmsConfiguration..cfpayrollgroup_master.groupname+' - '+hrmsConfiguration..cfbankbranch_master.branchname+' - '+premployee_bank_tran.accountno " & _
'                   "        FROM premployee_bank_tran " & _
'                   "            LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
'                   "            LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id = 3 " & _
'                   "        WHERE ISNULL(isvoid,0) = 0 " & _
'                   "            AND premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid FOR XML PATH('')),1,1,''),'') AS csv " & _
'                   "FROM hremployee_master WHERE 1 = 1 "

'            If mintEmployeeId > 0 Then
'                StrQ &= "AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
'            End If

'            If mintEmployeementTypeId > 0 Then
'                StrQ &= "AND hremployee_master.employmenttypeunkid = '" & mintEmployeementTypeId & "' "
'            End If

'            StrQ &= " ORDER BY hremployee_master.employeeunkid "

'            dsData = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsData
'        Catch ex As Exception
'            Throw New Exception("-1, " & ex.Message & ", Employee_Bank")
'        End Try
'    End Function
'    'S.SANDEEP [ 12 NOV 2012 ] -- END

'#End Region



'    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

'    End Sub
'End Class
