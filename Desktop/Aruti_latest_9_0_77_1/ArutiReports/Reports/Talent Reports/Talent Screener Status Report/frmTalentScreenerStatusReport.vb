﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTalentScreenerStatusReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTalentScreenerStatusReport"
    Private objTalentScreenerStatusReport As clsTalentScreenerStatusReport

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private dvEmployee As DataView
    Private mstrSearchEmpText As String = ""
    Private mlstEmployeeIds As List(Of String)
#End Region

#Region " Constructor "
    Public Sub New()
        objTalentScreenerStatusReport = New clsTalentScreenerStatusReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTalentScreenerStatusReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmTalentScreenerStatusReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTalentScreenerStatusReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentScreenerStatusReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentScreenerStatusReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentScreenerStatusReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentScreenerStatusReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentScreenerStatusReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentScreenerStatusReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentScreenerStatusReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTalentScreenerStatusReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTalentScreenerStatusReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim obCycle As New clstlcycle_master
        Try
            'Pinkal (14-Apr-2023) -- Start
            'NMB Enhancements - Period is not coming in Report.
            'dsCombo = obCycle.getListForCombo(CInt(FinancialYear._Object._YearUnkid), FinancialYear._Object._DatabaseName, FinancialYear._Object._Financial_Start_Date, "List", True, enStatusType.OPEN)
            dsCombo = obCycle.getListForCombo(0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Financial_Start_Date, "List", True, enStatusType.OPEN)
            'Pinkal (14-Apr-2023) -- End
            With cboCycle
                .ValueMember = "cycleunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : obCycle = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCycle.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            objTalentScreenerStatusReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim objCycle As New clstlcycle_master
        Try
            If CInt(cboCycle.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Cycle."), enMsgBoxStyle.Information)
                cboCycle.Focus()
                Exit Function
            End If

            mlstEmployeeIds = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList

            objTalentScreenerStatusReport.SetDefaultValue()

            objTalentScreenerStatusReport._CycleUnkid = cboCycle.SelectedValue
            objTalentScreenerStatusReport._CycleName = cboCycle.Text

            objTalentScreenerStatusReport._ViewByIds = mstrStringIds
            objTalentScreenerStatusReport._ViewIndex = mintViewIdx
            objTalentScreenerStatusReport._ViewByName = mstrStringName
            objTalentScreenerStatusReport._Analysis_Fields = mstrAnalysis_Fields
            objTalentScreenerStatusReport._Analysis_Join = mstrAnalysis_Join
            objTalentScreenerStatusReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTalentScreenerStatusReport._Report_GroupName = mstrReport_GroupName

            objCycle._Cycleunkid = CInt(cboCycle.SelectedValue)
            objTalentScreenerStatusReport._DateAsOn = CDate(objCycle._Start_Date)

            objTalentScreenerStatusReport._Advance_Filter = mstrAdvanceFilter
            objTalentScreenerStatusReport._EmployeeIds = mlstEmployeeIds

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
            objCycle = Nothing
        End Try
    End Function

    Public Sub FillEmpList()
        Dim objtlpipeline_master As New clstlpipeline_master
        Dim objCycle As New clstlcycle_master
        Try
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged

            Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL
            objTalentScreenerStatusReport._CycleUnkid = CInt(cboCycle.SelectedValue)
            objTalentScreenerStatusReport._Advance_Filter = mstrAdvanceFilter

            objCycle._Cycleunkid = CInt(cboCycle.SelectedValue)
            objtlpipeline_master._DateAsOn = CDate(objCycle._Start_Date)

            Dim dsList As DataSet = objtlpipeline_master.GetAllPipeLineEmployeeList(FinancialYear._Object._DatabaseName, _
                                                                          User._Object._Userunkid, _
                                                                          FinancialYear._Object._YearUnkid, _
                                                                          Company._Object._Companyunkid, _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                          ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboCycle.SelectedValue), _
                                                                          "", -1, FilterType, "Emp", _
                                                                          False, "", True)

            
            Dim dtTable As DataTable = New DataView(dsList.Tables("Emp"), "", "employeename", DataViewRowState.CurrentRows).ToTable
          
            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvEmployee = dtTable.DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, employeename "
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmpList", mstrModuleName)
        Finally
            objtlpipeline_master = Nothing
            objCycle = Nothing
        End Try
    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            'mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpText", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvEmployee.ToTable.AcceptChanges()
            End If

            Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objTalentScreenerStatusReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, False, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region "Textbox Events"
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Events"
    Private Sub cboCycle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCycle.SelectedIndexChanged
        Try
            If CInt(cboCycle.SelectedValue) > 0 Then
                Call FillEmpList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCycle_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchCycle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCycle.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboCycle.DataSource
            frm.ValueMember = cboCycle.ValueMember
            frm.DisplayMember = cboCycle.DisplayMember
            If frm.DisplayDialog Then
                cboCycle.SelectedValue = frm.SelectedValue
                cboCycle.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    Private Sub gbSortBy_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub gbFilterCriteria_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbFilterCriteria.HeaderClick

    End Sub


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblCycle.Text = Language._Object.getCaption(Me.lblCycle.Name, Me.lblCycle.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.lblEmployeeList.Text = Language._Object.getCaption(Me.lblEmployeeList.Name, Me.lblEmployeeList.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Please Select Cycle.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class