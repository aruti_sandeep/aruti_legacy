
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region


Public Class clsShortListedApplicantReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsShortListedApplicantReport"
    Private mstrReportId As String = enArutiReport.ShortListed_Applicant
    Dim objDataOperation As clsDataOperation


#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintShortListedId As Integer = 0
    Private mstrRefNo As String = String.Empty
    Private blnIsFinalApplicant As Boolean = False
    Private mintVacancyId As Integer = -1
    Private mstrVacancyName As String = String.Empty

    'Anjan [ 29 May 2013 ] -- Start
    'ENHANCEMENT : Recruitment TRA changes requested by Andrew
    Private mintVacancyIdShortListReport As Integer = -1
    'Anjan [ 29 May 2013 ] -- End



    'Pinkal (12-Jun-2013) -- Start
    'Enhancement : TRA Changes
    Private mintFinalShortListStatusId As Integer = -1
    Private mstrFinalShortListStatus As String = ""
    'Pinkal (12-Jun-2013) -- End

    'S.SANDEEP [01-May-2018] -- START
    'ISSUE/ENHANCEMENT : Internal_Finding
    Private mstrCheckedApplicantIds As String = String.Empty
    Private mstrCheckedApplicantNames As String = String.Empty
    'S.SANDEEP [01-May-2018] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _ShortListedId() As Integer
        Set(ByVal value As Integer)
            mintShortListedId = value
        End Set
    End Property

    Public WriteOnly Property _ReferenceNo() As String
        Set(ByVal value As String)
            mstrRefNo = value
        End Set
    End Property

    Public WriteOnly Property _IsFinalApplicant() As Boolean
        Set(ByVal value As Boolean)
            blnIsFinalApplicant = value
        End Set
    End Property

    Public WriteOnly Property _VacancyId() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

    'Anjan [ 29 May 2013 ] -- Start
    'ENHANCEMENT : Recruitment TRA changes requested by Andrew

    Public WriteOnly Property _VacancyIdShortListReport() As Integer
        Set(ByVal value As Integer)
            mintVacancyIdShortListReport = value
        End Set
    End Property
    'Anjan [ 29 May 2013 ] -- End



    'Pinkal (12-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _FinalShortListStatusId() As Integer
        Set(ByVal value As Integer)
            mintFinalShortListStatusId = value
        End Set
    End Property

    Public WriteOnly Property _FinalShortListStatus() As String
        Set(ByVal value As String)
            mstrFinalShortListStatus = value
        End Set
    End Property

    'Pinkal (12-Jun-2013) -- End

    'S.SANDEEP [01-May-2018] -- START
    'ISSUE/ENHANCEMENT : Internal_Finding
    Public WriteOnly Property _CheckedApplicantIds() As String
        Set(ByVal value As String)
            mstrCheckedApplicantIds = value
        End Set
    End Property
    Public WriteOnly Property _CheckedApplicantNames() As String
        Set(ByVal value As String)
            mstrCheckedApplicantNames = value
        End Set
    End Property
    'S.SANDEEP [01-May-2018] -- END

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintShortListedId = 0
            mstrRefNo = ""
            blnIsFinalApplicant = False
            mintVacancyId = 0
            mstrVacancyName = ""


            'Pinkal (12-Jun-2013) -- Start
            'Enhancement : TRA Changes
            mintFinalShortListStatusId = 0
            mstrFinalShortListStatus = ""
            'Pinkal (12-Jun-2013) -- End

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            mstrCheckedApplicantIds = String.Empty
            mstrCheckedApplicantNames = String.Empty
            'S.SANDEEP [01-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintShortListedId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "ReferenceNo :") & " " & mstrRefNo & " "
                Me._FilterQuery &= "AND rcshortlist_master.refno = '" & mstrRefNo & "' "
            End If

            If blnIsFinalApplicant Then
                Me._FilterQuery &= "AND rcshortlist_finalapplicant.isfinalshortlisted = 1"

                If mintFinalShortListStatusId > 0 Then
                    Me._FilterQuery &= "AND rcshortlist_finalapplicant.App_statusid = " & mintFinalShortListStatusId
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Status :") & " " & mstrFinalShortListStatus & "   "
                End If

            End If


            'Anjan [ 29 May 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew
            If mintVacancyIdShortListReport > 0 Then
                Me._FilterQuery &= " AND rcshortlist_master.vacancyunkid = " & mintVacancyIdShortListReport & " "
                Me._FilterQuery &= " AND rcshortlist_master.vacancyunkid = " & mintVacancyIdShortListReport & " "

            End If
            'Anjan [ 29 May 2013 ] -- End

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                Me._FilterQuery &= " AND rcapplicant_master.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Filter by Checked Applicant(s).")
            End If
            'S.SANDEEP [01-May-2018] -- END


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (12-Jun-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub FilterTitleAndFilterQueryForEligibleApplicant()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintVacancyId > 0 Then
                Me._FilterQuery &= " AND rcinterviewanalysis_master.vacancyunkid = " & mintVacancyId & " "
            End If

            If mintFinalShortListStatusId > 0 Then
                Me._FilterQuery &= "AND rcinterviewanalysis_master.statustypid = " & mintFinalShortListStatusId
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Status :") & " " & mstrFinalShortListStatus & "   "
            End If


            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                Me._FilterQuery &= " AND rcapplicant_master.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Filter by Checked Applicant(s).")
            End If
            'S.SANDEEP [01-May-2018] -- END

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQueryForEligibleApplicant; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Pinkal (12-Jun-2013) -- End


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (12-Jun-2013) -- Start
        '    'Enhancement : TRA Changes

        '    If pintReportType = 0 Then
        '        objRpt = Generate_DetailReport()
        '    ElseIf pintReportType = 1 Then
        '        objRpt = Generate_EligibleApplicant_DetailReport()
        '    End If

        '    'Pinkal (12-Jun-2013) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If pintReportType = 0 Then
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            ElseIf pintReportType = 1 Then
                objRpt = Generate_EligibleApplicant_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End If
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            'OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""

        Dim dsApplicantList As New DataSet
        Dim dsQualification As New DataSet
        'Hemant (13 Apr 2021) -- Start
        'ISSUE : AH-2932- Applicant Shortlisted Report Not Showing Applicant Qualifications
        Dim dsInternalQualification As New DataSet
        'Hemant (13 Apr 2021) -- End
        Dim dsWorkExperience As New DataSet
        Dim dsOtherShortCourses As New DataSet
        'Hemant (13 Apr 2021) -- Start
        'ISSUE : AH-2932- Applicant Shortlisted Report Not Showing Applicant Qualifications
        Dim dsInternalWorkExperience As New DataSet
        Dim dsInternalOtherShortCourses As New DataSet
        'Hemant (13 Apr 2021) -- End
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = intUserUnkid
            'Shani(24-Aug-2015) -- End

            objDataOperation = New clsDataOperation

            StrQ = " SELECT  DISTINCT  rcshortlist_master.shortlistunkid " & _
                       ", refno  " & _
                       ", rcapplicant_master.applicantunkid " & _
                       ", rcapplicant_master.applicant_code AS code " & _
                       ",CASE WHEN ISNULL(CONVERT(CHAR(8), rcapplicant_master.birth_date, 112),'') = '' THEN '' ELSE CAST(ISNULL(DATEDIFF(yy, rcapplicant_master.birth_date,GETDATE()), '') AS NVARCHAR(50)) END AS age  " & _
                       ",ISNULL(CASE WHEN gender = 1 THEN @MALE  ELSE @FEMALE END, '') AS gender " & _
                       ",rcapplicant_master.firstname + ' ' +  rcapplicant_master.othername + ' ' + rcapplicant_master.surname + '\r\n' + rcapplicant_master.present_address1 + '\r\n' + rcapplicant_master.present_address2 + '\r\n' + rcapplicant_master.present_mobileno AS NAMEAdd " & _
                       ",ISNULL(employeecode, '') AS employeecode  " & _
                       ",rcshortlist_master.vacancyunkid " & _
                       ",cfcommon_master.name AS vacancy " & _
                       ",CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                       ",CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                       ",isexternalvacancy " & _
                       ",employeeunkid " & _
                       " FROM rcshortlist_master " & _
                       " JOIN rcshortlist_finalapplicant ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid " & _
                       " JOIN rcapp_vacancy_mapping ON rcshortlist_master.vacancyunkid = rcapp_vacancy_mapping.vacancyunkid " & _
                       "                          AND rcapp_vacancy_mapping.applicantunkid = rcshortlist_finalapplicant.applicantunkid AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                       " JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid  " & _
                       " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & _
                       " JOIN rcapplicant_master ON rcshortlist_finalapplicant.applicantunkid= rcapplicant_master.applicantunkid " & _
                       " WHERE 1 = 1 AND rcapp_vacancy_mapping.isimport = 0 AND rcshortlist_master.isvoid = 0 "

            'Pinkal (09-Sep-2023) -- TRA Include New Screen For Importing Final ShortListing Applicant.[rcapplicant_master.firstname + ' ' +  rcapplicant_master.othername + ' ' + rcapplicant_master.surname]

            'Hemant (13 Apr 2021) --  [employeeunkid]
            'Sohail (09 Oct 2018) - [isactive = 1]=[ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'AND rcapplicant_master.isimport = 0 " Removed
            'AND rcapp_vacancy_mapping.isimport = 0 " Added
            'S.SANDEEP [ 14 May 2013 ] -- END


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsApplicantList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            StrQ = ""
            StrQ = "SELECT " & _
                        "applicantunkid " & _
                        ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                         "hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') " & _
                         "else " & _
                        "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') END,'') AS qualification " & _
                       ",ISNULL(hrresult_master.resultname,'') AS classification " & _
                       ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                       ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                       ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM rcapplicantqualification_tran " & _
                       "JOIN hrqualification_master ON hrqualification_master.qualificationunkid=rcapplicantqualification_tran.qualificationunkid " & _
                       "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=rcapplicantqualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                       "LEFT JOIN hrresult_master ON hrresult_master.resultunkid=rcapplicantqualification_tran.resultunkid " & _
                    " WHERE rcapplicantqualification_tran.qualificationunkid > 0 AND rcapplicantqualification_tran.isvoid = 0 "

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                StrQ &= " AND rcapplicantqualification_tran.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
            End If
            'S.SANDEEP [01-May-2018] -- END

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0            
            'S.SANDEEP [24-Apr-2018] -- END

            dsQualification = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'Hemant (13 Apr 2021) -- Start
            'ISSUE : AH-2932- Applicant Shortlisted Report Not Showing Applicant Qualifications
            StrQ = ""
            StrQ = "SELECT " & _
                        "employeeunkid " & _
                        ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') ='' THEN " & _
                         "hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') " & _
                         "else " & _
                        "ISNULL(CAST(YEAR(hremp_qualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(hremp_qualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') END,'') AS qualification " & _
                       ",ISNULL(hrresult_master.resultname,'') AS classification " & _
                       ",CASE WHEN hremp_qualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(hremp_qualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                       ", ISNULL(hremp_qualification_tran.award_start_date,'') AS startdate " & _
                       ", ISNULL(hremp_qualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM hremp_qualification_tran " & _
                       "JOIN hrqualification_master ON hrqualification_master.qualificationunkid=hremp_qualification_tran.qualificationunkid " & _
                       "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=hremp_qualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                       "LEFT JOIN hrresult_master ON hrresult_master.resultunkid=hremp_qualification_tran.resultunkid " & _
                    " WHERE hremp_qualification_tran.qualificationunkid > 0 AND hremp_qualification_tran.isvoid = 0 "
          
            
            dsInternalQualification = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If
            'Hemant (13 Apr 2021) -- End

            StrQ = ""
            StrQ = "SELECT " & _
                      "applicantunkid " & _
                   ", ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcjobhistory.joiningdate ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcjobhistory.terminationdate,112),'') ='' THEN " & _
                     "ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') " & _
                     "else " & _
                    "ISNULL(CAST(YEAR(rcjobhistory.joiningdate) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(rcjobhistory.terminationdate) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') END,'') AS Experience " & _
                    " ,ISNULL(rcjobhistory.joiningdate,'')  AS startdate " & _
                    ",ISNULL(rcjobhistory.terminationdate,'') AS ENDdate " & _
                "FROM rcjobhistory WHERE 1 = 1 AND rcjobhistory.isvoid = 0 "

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                StrQ &= " AND rcjobhistory.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
            End If
            'S.SANDEEP [01-May-2018] -- END

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> WHERE 1 = 1 AND rcjobhistory.isvoid = 0            
            'S.SANDEEP [24-Apr-2018] -- END

            dsWorkExperience = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'Hemant (13 Apr 2021) -- Start
            'ISSUE : AH-2932- Applicant Shortlisted Report Not Showing Applicant Qualifications
            StrQ = ""
            StrQ = "SELECT " & _
                      "employeeunkid " & _
                   ", ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),hremp_experience_tran.start_date ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),hremp_experience_tran.end_date,112),'') ='' THEN " & _
                     "ISNULL(hremp_experience_tran.company,'') +' - '+ ISNULL(hremp_experience_tran.company ,'') +' - '+ ISNULL(hremp_experience_tran.old_job,'') +' - ' +ISNULL(hremp_experience_tran.remark,'') " & _
                     "else " & _
                    "ISNULL(CAST(YEAR(hremp_experience_tran.start_date) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(hremp_experience_tran.end_date) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(hremp_experience_tran.company,'') +' - '+ ISNULL(hremp_experience_tran.company ,'') +' - '+ ISNULL(hremp_experience_tran.old_job,'') +' - ' +ISNULL(hremp_experience_tran.remark,'') END,'') AS Experience " & _
                    " ,ISNULL(hremp_experience_tran.start_date,'')  AS startdate " & _
                    ",ISNULL(hremp_experience_tran.end_date,'') AS ENDdate " & _
                "FROM hremp_experience_tran WHERE 1 = 1 AND hremp_experience_tran.isvoid = 0 "

            dsInternalWorkExperience = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If
            'Hemant (13 Apr 2021) -- End

            StrQ = ""
            StrQ = "SELECT " & _
                            "applicantunkid " & _
                           ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                             "ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') " & _
                             "else " & _
                            "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') END,'') AS qualification " & _
                           ",ISNULL(other_resultcode,'') AS classification " & _
                           ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                           ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                           ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM rcapplicantqualification_tran " & _
                   "WHERE rcapplicantqualification_tran.qualificationunkid <= 0 AND rcapplicantqualification_tran.isvoid = 0 "

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                StrQ &= " AND rcapplicantqualification_tran.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
            End If
            'S.SANDEEP [01-May-2018] -- END

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            dsOtherShortCourses = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'Hemant (13 Apr 2021) -- Start
            'ISSUE : AH-2932- Applicant Shortlisted Report Not Showing Applicant Qualifications
            StrQ = ""
            StrQ = "SELECT " & _
                            "employeeunkid " & _
                           ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') ='' THEN " & _
                             "ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') " & _
                             "else " & _
                            "ISNULL(CAST(YEAR(hremp_qualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(hremp_qualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') END,'') AS qualification " & _
                           ",ISNULL(other_resultcode,'') AS classification " & _
                           ",CASE WHEN hremp_qualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(hremp_qualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                           ", ISNULL(hremp_qualification_tran.award_start_date,'') AS startdate " & _
                           ", ISNULL(hremp_qualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM hremp_qualification_tran " & _
                   "WHERE hremp_qualification_tran.qualificationunkid <= 0 AND hremp_qualification_tran.isvoid = 0 "

            dsInternalOtherShortCourses = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If
            'Hemant (13 Apr 2021) -- End

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim rpt_Rows As DataRow = Nothing
            Dim dtPQFilter As DataTable
            Dim dtWEFilter As DataTable
            Dim dtOQFilter As DataTable
            Dim blnIsAdded As Boolean = False
            Dim iColCnt As Integer = 1

            For Each dtRow As DataRow In dsApplicantList.Tables("DataTable").Rows
                'Hemant (13 Apr 2021) -- Start
                'ISSUE : AH-2932- Applicant Shortlisted Report Not Showing Applicant Qualifications
                If CInt(dtRow.Item("employeeunkid")) > 0 Then
                    dtPQFilter = New DataView(dsInternalQualification.Tables(0), "employeeunkid = '" & dtRow.Item("employeeunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable
                    dtWEFilter = New DataView(dsInternalWorkExperience.Tables(0), "employeeunkid = '" & dtRow.Item("employeeunkid") & "'", "startdate DESC", DataViewRowState.CurrentRows).ToTable
                    dtOQFilter = New DataView(dsInternalOtherShortCourses.Tables(0), "employeeunkid = '" & dtRow.Item("employeeunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable
                Else
                    'Hemant (13 Apr 2021) -- End
                dtPQFilter = New DataView(dsQualification.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable
                dtWEFilter = New DataView(dsWorkExperience.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "startdate DESC", DataViewRowState.CurrentRows).ToTable
                dtOQFilter = New DataView(dsOtherShortCourses.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable
                End If 'Hemant (13 Apr 2021)                

                Dim iPQCnt As Integer = dtPQFilter.Rows.Count
                Dim iWECnt As Integer = dtWEFilter.Rows.Count
                Dim iOQCnt As Integer = dtOQFilter.Rows.Count

                blnIsAdded = False

                If iPQCnt <= 0 AndAlso iWECnt <= 0 AndAlso iOQCnt <= 0 Then 'This is for  when professional Qualification, Experience and other Qualification /Short courses is blank
                    If blnIsAdded = False Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        rpt_Rows.Item("Column1") = dtRow.Item("code")
                        rpt_Rows.Item("Column2") = dtRow.Item("age")
                        rpt_Rows.Item("Column3") = dtRow.Item("gender")
                        rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column5") = ""
                        rpt_Rows.Item("Column6") = ""
                        rpt_Rows.Item("Column7") = ""
                        rpt_Rows.Item("Column8") = ""
                        rpt_Rows.Item("Column9") = ""
                        rpt_Rows.Item("Column10") = iColCnt.ToString
                        rpt_Rows.Item("Column11") = dtRow.Item("refno")
                        iColCnt += 1
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                        blnIsAdded = True

                        Continue For
                    End If
                End If

                If iPQCnt >= iWECnt AndAlso iPQCnt >= iOQCnt Then 'This is for professional Qualification is greater than Experience and other Qualification /Short courses
                    For i As Integer = 0 To dtPQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If


                        rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                        rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")

                        If i < iWECnt AndAlso iWECnt > 0 Then 'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If
                        rpt_Rows.Item("Column11") = dtRow.Item("refno")

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iWECnt >= iPQCnt AndAlso iWECnt >= iOQCnt Then 'This is for Experience  is greater than professional Qualification and other Qualification /Short courses
                    For i As Integer = 0 To dtWEFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If

                        rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If
                        rpt_Rows.Item("Column11") = dtRow.Item("refno")

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iOQCnt >= iPQCnt AndAlso iOQCnt >= iWECnt Then 'This is for other Qualification /Short courses is greater than professional Qualification and Experience 
                    For i As Integer = 0 To dtOQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If

                        rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row.
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iWECnt AndAlso iWECnt > 0 Then  'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)

                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If
                        rpt_Rows.Item("Column11") = dtRow.Item("refno")
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                End If
            Next

            objRpt = New ArutiReport.Designer.rptShortListedApplicant

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 3, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 4, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 5, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 6, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 7, "Sr No."))
            Call ReportFunction.TextChange(objRpt, "txtAppcode", Language.getMessage(mstrModuleName, 8, "App. Code"))

            Call ReportFunction.TextChange(objRpt, "txtAppAge", Language.getMessage(mstrModuleName, 9, "Age"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 10, "Gender"))
            Call ReportFunction.TextChange(objRpt, "txtNameAddressMobile", Language.getMessage(mstrModuleName, 11, "Name /Address /Mobile"))

            Call ReportFunction.TextChange(objRpt, "txtQualification", Language.getMessage(mstrModuleName, 12, "Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtResultCode", Language.getMessage(mstrModuleName, 13, "Classification"))

            Call ReportFunction.TextChange(objRpt, "txtGPA", Language.getMessage(mstrModuleName, 14, "GPA"))
            Call ReportFunction.TextChange(objRpt, "txtExperience", Language.getMessage(mstrModuleName, 15, "Experience"))
            Call ReportFunction.TextChange(objRpt, "txtOtherShortcourses", Language.getMessage(mstrModuleName, 16, "Other Qualification/Short Courses"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            If blnIsFinalApplicant Then
                Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 17, "Final Short Listed Applicants") & " - " & mstrFinalShortListStatus & Language.getMessage(mstrModuleName, 18, " For ") & mstrVacancyName)
            Else
                Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 19, "Short Listed Applicants") & Language.getMessage(mstrModuleName, 18, " For ") & mstrVacancyName)
            End If

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            'Pinkal (12-Jun-2013) -- Start
            'Enhancement : TRA Changes
            Call ReportFunction.TextChange(objRpt, "txtReferenceNo", Language.getMessage(mstrModuleName, 1, "Reference No :"))
            'Pinkal (12-Jun-2013) -- End


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


    'Pinkal (12-Jun-2013) -- Start
    'Enhancement : TRA Changes


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Function Generate_EligibleApplicant_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_EligibleApplicant_DetailReport(ByVal strDatabaseName As String, _
                                                             ByVal intUserUnkid As Integer, _
                                                             ByVal intYearUnkid As Integer, _
                                                             ByVal intCompanyUnkid As Integer, _
                                                             ByVal dtPeriodStart As Date, _
                                                             ByVal dtPeriodEnd As Date, _
                                                             ByVal strUserModeSetting As String, _
                                                             ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""

        Dim dsApplicantList As New DataSet
        Dim dsQualification As New DataSet
        Dim dsWorkExperience As New DataSet
        Dim dsOtherShortCourses As New DataSet

        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End

        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = intUserUnkid
            'Shani(24-Aug-2015) -- End

            StrQ = " SELECT  DISTINCT  " & _
                       " rcapplicant_master.applicantunkid " & _
                       ", rcapplicant_master.applicant_code AS code " & _
                       ",CASE WHEN ISNULL(CONVERT(CHAR(8), rcapplicant_master.birth_date, 112),'') = '' THEN '' ELSE CAST(ISNULL(DATEDIFF(yy, rcapplicant_master.birth_date,GETDATE()), '') AS NVARCHAR(50)) END AS age  " & _
                       ",ISNULL(CASE WHEN gender = 1 THEN @MALE  ELSE @FEMALE END, '') AS gender " & _
                       ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname + '\r\n' + rcapplicant_master.present_address1 + '\r\n' + rcapplicant_master.present_address2 + '\r\n' + rcapplicant_master.present_mobileno AS NAMEAdd " & _
                       ",ISNULL(employeecode, '') AS employeecode  " & _
                       ",rcinterviewanalysis_master.vacancyunkid " & _
                       ",cfcommon_master.name AS vacancy " & _
                       ",CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                       ",CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                       ",isexternalvacancy " & _
                       " FROM rcinterviewanalysis_master " & _
                       " JOIN rcvacancy_master ON rcinterviewanalysis_master.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                       " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & _
                       " JOIN rcapplicant_master ON rcinterviewanalysis_master.applicantunkid= rcapplicant_master.applicantunkid " & _
                       " WHERE 1 = 1 AND rcinterviewanalysis_master.isvoid = 0 AND rcinterviewanalysis_master.iseligible = 1"


            Call FilterTitleAndFilterQueryForEligibleApplicant()

            StrQ &= Me._FilterQuery

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsApplicantList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            StrQ = ""
            StrQ = "SELECT " & _
                        "applicantunkid " & _
                        ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                         "hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') " & _
                         "else " & _
                        "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') END,'') AS qualification " & _
                       ",ISNULL(hrresult_master.resultname,'') AS classification " & _
                       ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                       ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                       ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM rcapplicantqualification_tran " & _
                       "JOIN hrqualification_master ON hrqualification_master.qualificationunkid=rcapplicantqualification_tran.qualificationunkid " & _
                       "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=rcapplicantqualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                       "LEFT JOIN hrresult_master ON hrresult_master.resultunkid=rcapplicantqualification_tran.resultunkid " & _
                    " WHERE rcapplicantqualification_tran.qualificationunkid > 0 AND rcapplicantqualification_tran.isvoid = 0 "

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                StrQ &= " AND rcapplicantqualification_tran.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
            End If
            'S.SANDEEP [01-May-2018] -- END

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            dsQualification = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            StrQ = ""
            StrQ = "SELECT " & _
                      "applicantunkid " & _
                   ", ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcjobhistory.joiningdate ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcjobhistory.terminationdate,112),'') ='' THEN " & _
                     "ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') " & _
                     "else " & _
                    "ISNULL(CAST(YEAR(rcjobhistory.joiningdate) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(rcjobhistory.terminationdate) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') END,'') AS Experience " & _
                    " ,ISNULL(rcjobhistory.joiningdate,'')  AS startdate " & _
                    ",ISNULL(rcjobhistory.terminationdate,'') AS ENDdate " & _
                "FROM rcjobhistory WHERE 1 = 1 AND rcjobhistory.isvoid = 0 "

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                StrQ &= " AND rcjobhistory.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
            End If
            'S.SANDEEP [01-May-2018] -- END

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcjobhistory.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END


            dsWorkExperience = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            StrQ = ""
            StrQ = "SELECT " & _
                            "applicantunkid " & _
                           ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                             "ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') " & _
                             "else " & _
                            "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') END,'') AS qualification " & _
                           ",ISNULL(other_resultcode,'') AS classification " & _
                           ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                           ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                           ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM rcapplicantqualification_tran " & _
                   "WHERE rcapplicantqualification_tran.qualificationunkid <= 0 AND rcapplicantqualification_tran.isvoid = 0 "

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            If mstrCheckedApplicantIds.Trim.Length > 0 Then
                StrQ &= " AND rcapplicantqualification_tran.applicantunkid IN (" & mstrCheckedApplicantIds & ") "
            End If
            'S.SANDEEP [01-May-2018] -- END

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            dsOtherShortCourses = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim rpt_Rows As DataRow = Nothing
            Dim dtPQFilter As DataTable
            Dim dtWEFilter As DataTable
            Dim dtOQFilter As DataTable
            Dim blnIsAdded As Boolean = False
            Dim iColCnt As Integer = 1

            For Each dtRow As DataRow In dsApplicantList.Tables("DataTable").Rows
                dtPQFilter = New DataView(dsQualification.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable
                dtWEFilter = New DataView(dsWorkExperience.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "startdate DESC", DataViewRowState.CurrentRows).ToTable
                dtOQFilter = New DataView(dsOtherShortCourses.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable

                Dim iPQCnt As Integer = dtPQFilter.Rows.Count
                Dim iWECnt As Integer = dtWEFilter.Rows.Count
                Dim iOQCnt As Integer = dtOQFilter.Rows.Count

                blnIsAdded = False

                If iPQCnt <= 0 AndAlso iWECnt <= 0 AndAlso iOQCnt <= 0 Then 'This is for  when professional Qualification, Experience and other Qualification /Short courses is blank
                    If blnIsAdded = False Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        rpt_Rows.Item("Column1") = dtRow.Item("code")
                        rpt_Rows.Item("Column2") = dtRow.Item("age")
                        rpt_Rows.Item("Column3") = dtRow.Item("gender")
                        rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column5") = ""
                        rpt_Rows.Item("Column6") = ""
                        rpt_Rows.Item("Column7") = ""
                        rpt_Rows.Item("Column8") = ""
                        rpt_Rows.Item("Column9") = ""
                        rpt_Rows.Item("Column10") = iColCnt.ToString
                        iColCnt += 1
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                        blnIsAdded = True

                        Continue For
                    End If
                End If

                If iPQCnt >= iWECnt AndAlso iPQCnt >= iOQCnt Then 'This is for professional Qualification is greater than Experience and other Qualification /Short courses
                    For i As Integer = 0 To dtPQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If


                        rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                        rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")

                        If i < iWECnt AndAlso iWECnt > 0 Then 'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iWECnt >= iPQCnt AndAlso iWECnt >= iOQCnt Then 'This is for Experience  is greater than professional Qualification and other Qualification /Short courses
                    For i As Integer = 0 To dtWEFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If

                        rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iOQCnt >= iPQCnt AndAlso iOQCnt >= iWECnt Then 'This is for other Qualification /Short courses is greater than professional Qualification and Experience 
                    For i As Integer = 0 To dtOQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If

                        rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row.
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iWECnt AndAlso iWECnt > 0 Then  'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)

                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                End If
            Next

            objRpt = New ArutiReport.Designer.rptShortListedApplicant

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 3, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 4, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 5, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 6, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 7, "Sr No."))
            Call ReportFunction.TextChange(objRpt, "txtAppcode", Language.getMessage(mstrModuleName, 8, "App. Code"))

            Call ReportFunction.TextChange(objRpt, "txtAppAge", Language.getMessage(mstrModuleName, 9, "Age"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 10, "Gender"))
            Call ReportFunction.TextChange(objRpt, "txtNameAddressMobile", Language.getMessage(mstrModuleName, 11, "Name /Address /Mobile"))

            Call ReportFunction.TextChange(objRpt, "txtQualification", Language.getMessage(mstrModuleName, 12, "Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtResultCode", Language.getMessage(mstrModuleName, 13, "Classification"))

            Call ReportFunction.TextChange(objRpt, "txtGPA", Language.getMessage(mstrModuleName, 14, "GPA"))
            Call ReportFunction.TextChange(objRpt, "txtExperience", Language.getMessage(mstrModuleName, 15, "Experience"))
            Call ReportFunction.TextChange(objRpt, "txtOtherShortcourses", Language.getMessage(mstrModuleName, 16, "Other Qualification/Short Courses"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            If mstrFinalShortListStatus.ToString().Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 21, "Eligible Applicants") & " - " & mstrFinalShortListStatus & Language.getMessage(mstrModuleName, 18, " For ") & mstrVacancyName)
            Else
                Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 21, "Eligible Applicants") & Language.getMessage(mstrModuleName, 18, " For ") & mstrVacancyName)
            End If

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EligibleApplicant_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (12-Jun-2013) -- End



#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "ReferenceNo :")
            Language.setMessage(mstrModuleName, 2, "Order By :")
            Language.setMessage(mstrModuleName, 3, "Prepared By :")
            Language.setMessage(mstrModuleName, 4, "Checked By :")
            Language.setMessage(mstrModuleName, 5, "Approved By :")
            Language.setMessage(mstrModuleName, 6, "Received By :")
            Language.setMessage(mstrModuleName, 7, "Sr No.")
            Language.setMessage(mstrModuleName, 8, "App. Code")
            Language.setMessage(mstrModuleName, 9, "Age")
            Language.setMessage(mstrModuleName, 10, "Gender")
            Language.setMessage(mstrModuleName, 11, "Name /Address /Mobile")
            Language.setMessage(mstrModuleName, 12, "Qualification")
            Language.setMessage(mstrModuleName, 13, "Classification")
            Language.setMessage(mstrModuleName, 14, "GPA")
            Language.setMessage(mstrModuleName, 15, "Experience")
            Language.setMessage(mstrModuleName, 16, "Other Qualification/Short Courses")
            Language.setMessage(mstrModuleName, 17, "Final Short Listed Applicants")
            Language.setMessage(mstrModuleName, 18, " For")
            Language.setMessage(mstrModuleName, 19, "Short Listed Applicants")
            Language.setMessage(mstrModuleName, 20, "Status :")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
