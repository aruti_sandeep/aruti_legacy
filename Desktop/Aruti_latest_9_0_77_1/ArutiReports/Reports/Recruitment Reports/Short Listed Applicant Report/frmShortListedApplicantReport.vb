
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmShortListedApplicantReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmShortListedApplicantReport"
    Private objShortListed As clsShortListedApplicantReport
    'S.SANDEEP [01-May-2018] -- START
    'ISSUE/ENHANCEMENT : Internal_Finding
    Private dtTable As DataTable
    Private dView As DataView
    'S.SANDEEP [01-May-2018] -- END
#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        objShortListed = New clsShortListedApplicantReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objShortListed.SetDefaultValue()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objShortList As New clsshortlist_master

        'Anjan [ 29 May 2013 ] -- Start
        'ENHANCEMENT : Recruitment TRA changes requested by Andrew
        Dim objApprovalStatus As New clsMasterData
        'Anjan [ 29 May 2013 ] -- End


        Dim dsCombo As New DataSet
        Try
            dsCombo = objShortList.getComboList("List", True, True)
            With cboReferenceNo
                .ValueMember = "shortlistunkid"
                .DisplayMember = "refno"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = Nothing

            'Anjan [ 29 May 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew
            dsCombo = objApprovalStatus.GetComboforShortlistingApprovalStatus(True, "List")
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With
            'Anjan [ 29 May 2013 ] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objShortList = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReferenceNo.SelectedValue = 0
            chkFinalApplicant.Checked = False

            'Anjan [ 29 May 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew
            cboStatus.SelectedValue = 0
            'Anjan [ 29 May 2013 ] -- End

            objShortListed.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objShortListed.SetDefaultValue()
            objShortListed._ShortListedId = CInt(cboReferenceNo.SelectedValue)
            objShortListed._ReferenceNo = cboReferenceNo.Text
            objShortListed._IsFinalApplicant = chkFinalApplicant.Checked

            Dim drRow As DataRowView = cboReferenceNo.SelectedItem
            If drRow IsNot Nothing Then
                objShortListed._VacancyId = CInt(drRow.Row("vacancyunkid"))
                objShortListed._VacancyName = "(" & drRow.Row("vacancy").ToString().Trim & "  " & eZeeDate.convertDate(drRow.Row("StDate").ToString().Trim) & " - " & eZeeDate.convertDate(drRow.Row("EndDate").ToString().Trim) & ")"
            End If


            'Pinkal (12-Jun-2013) -- Start
            'Enhancement : TRA Changes
            objShortListed._FinalShortListStatusId = CInt(cboStatus.SelectedValue)
            objShortListed._FinalShortListStatus = cboStatus.Text
            'Pinkal (12-Jun-2013) -- End

            'S.SANDEEP [01-May-2018] -- START
            'ISSUE/ENHANCEMENT : Internal_Finding
            dtTable.AcceptChanges()
            If dtTable.AsEnumerable.Where(Function(x) x.Field(Of Boolean)("ischeck")).Count > 0 Then
                objShortListed._CheckedApplicantIds = String.Join(",", dtTable.AsEnumerable.Where(Function(y) y.Field(Of Boolean)("ischeck") = True).Select(Function(x) x.Field(Of Integer)("applicantunkid").ToString()).ToArray())
                objShortListed._CheckedApplicantNames = String.Join(",", dtTable.AsEnumerable.Where(Function(y) y.Field(Of Boolean)("ischeck") = True).Select(Function(x) x.Field(Of String)("refno").Trim()).ToArray())
            End If
            'S.SANDEEP [01-May-2018] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [01-May-2018] -- START
    'ISSUE/ENHANCEMENT : Internal_Finding
    Private Sub Perform_Filter(ByVal drRow As DataRowView, ByVal intStatusId As Integer)
        Try
            Dim objShortList As New clsshortlist_master
            dtTable = objShortList.Get_Table("List", drRow.Item("vacancyunkid"), "", True, False)
            objShortList = Nothing
            If dtTable IsNot Nothing Then
                If intStatusId > 0 Then
                    dtTable = New DataView(dtTable, "IsGrp = 0 AND shortlistunkid = '" & cboReferenceNo.SelectedValue & "' AND StatusId = '" & intStatusId & "'", "", DataViewRowState.CurrentRows).ToTable()
                Else
                    dtTable = New DataView(dtTable, "IsGrp = 0 AND shortlistunkid = '" & cboReferenceNo.SelectedValue & "'", "", DataViewRowState.CurrentRows).ToTable()
                End If
                dView = dtTable.DefaultView
                dgvApplicant.AutoGenerateColumns = False
                objdgcolhCheck.DataPropertyName = "ischeck"
                objdgcolhApplicant.DataPropertyName = "refno"
                objdgcolhApplicant.HeaderText = Language.getMessage(mstrModuleName, 23, "Applicant(s)") & " - [" & dView.Count.ToString() & "]"
                dgvApplicant.DataSource = dView
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Perform_Filter", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [01-May-2018] -- END

#End Region

#Region " Forms "

    Private Sub frmShortListedApplicantReport_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objShortListed = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmShortListedApplicantReport_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShortListedApplicantReport_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objShortListed._ReportName
            Me._Message = objShortListed._ReportDesc

            Call FillCombo()
            Call ResetValue()
            cboStatus.Enabled = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShortListedApplicantReport_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [01-May-2018] -- START
    'ISSUE/ENHANCEMENT : Internal_Finding
#Region " Combobox Event "

    Private Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged
        Try
            If cboReferenceNo.SelectedValue > 0 Then
                Dim drRow As DataRowView = cboReferenceNo.SelectedItem
                If drRow IsNot Nothing Then
                    txtVacancy.Text = drRow.Row("vacancy").ToString().Trim & "  " & eZeeDate.convertDate(drRow.Row("StDate").ToString().Trim) & " - " & eZeeDate.convertDate(drRow.Row("EndDate").ToString().Trim)
                End If
                Call Perform_Filter(drRow, cboStatus.SelectedValue)
            Else
                txtVacancy.Text = ""
                dgvApplicant.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            Dim drRow As DataRowView = cboReferenceNo.SelectedItem
            Call Perform_Filter(drRow, cboStatus.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Text Change "

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvApplicant.Rows.Count > 0 Then
                        If dgvApplicant.SelectedRows(0).Index = dgvApplicant.Rows(dgvApplicant.RowCount - 1).Index Then Exit Sub
                        dgvApplicant.Rows(dgvApplicant.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvApplicant.Rows.Count > 0 Then
                        If dgvApplicant.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvApplicant.Rows(dgvApplicant.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = objdgcolhApplicant.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' "
            End If
            dView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtaSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objckhAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objckhAll.CheckedChanged
        Try
            RemoveHandler dgvApplicant.CellContentClick, AddressOf dgvApplicant_CellContentClick
            For Each dr As DataRowView In dView
                dr.Item("ischeck") = CBool(objckhAll.CheckState)
            Next
            dgvApplicant.Refresh()
            AddHandler dgvApplicant.CellContentClick, AddressOf dgvApplicant_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objckhAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Gridview Event "

    Private Sub dgvApplicant_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvApplicant.CellContentClick
        Try
            RemoveHandler objckhAll.CheckedChanged, AddressOf objckhAll_CheckedChanged

            If e.ColumnIndex = objdgcolhCheck.Index Then

                If Me.dgvApplicant.IsCurrentCellDirty Then
                    Me.dgvApplicant.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dView.ToTable.Rows.Count = drRow.Length Then
                        objckhAll.CheckState = CheckState.Checked
                    Else
                        objckhAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objckhAll.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objckhAll.CheckedChanged, AddressOf objckhAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvApplicant_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [01-May-2018] -- END

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If CInt(cboReferenceNo.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Reference No is mandatory information.Please select Reference No."), enMsgBoxStyle.Information)
                cboReferenceNo.Focus()
                Exit Sub
            End If

            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objShortListed.generateReport(0, e.Type, enExportAction.None)
            objShortListed.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If CInt(cboReferenceNo.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Reference No is mandatory information.Please select Reference No."), enMsgBoxStyle.Information)
                cboReferenceNo.Focus()
                Exit Sub
            End If

            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objShortListed.generateReport(0, enPrintAction.None, e.Type)
            objShortListed.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboReferenceNo.DataSource
            frm.ValueMember = cboReferenceNo.ValueMember
            frm.DisplayMember = cboReferenceNo.DisplayMember
            If frm.DisplayDialog Then
                cboReferenceNo.SelectedValue = frm.SelectedValue
                cboReferenceNo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsShortListedApplicantReport.SetMessages()
            objfrm._Other_ModuleNames = "clsShortListedApplicantReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblReferenceNo.Text = Language._Object.getCaption(Me.lblReferenceNo.Name, Me.lblReferenceNo.Text)
			Me.chkFinalApplicant.Text = Language._Object.getCaption(Me.chkFinalApplicant.Name, Me.chkFinalApplicant.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Reference No is mandatory information.Please select Reference No.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    'Anjan [ 29 May 2013 ] -- Start
    'ENHANCEMENT : Recruitment TRA changes requested by Andrew
    Private Sub chkFinalApplicant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFinalApplicant.Click
        Try
            If chkFinalApplicant.Checked Then
                cboStatus.Enabled = True
            Else
                cboStatus.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkFinalApplicant_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan [ 29 May 2013 ] -- End


End Class
