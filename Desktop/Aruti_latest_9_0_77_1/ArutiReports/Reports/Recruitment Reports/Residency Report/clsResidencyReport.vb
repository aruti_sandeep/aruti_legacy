﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsResidencyReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsResidencyReport"
    Private mstrReportId As String = enArutiReport.ResidencyReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END        
    End Sub

#End Region

#Region " Private Variables "

    Private mintReportTypeId As Integer = -1
    Private mstrReportTypeName As String = ""
    Private mintApplicantId As Integer = -1
    Private mstrApplicantName As String = String.Empty
    Private mintVacancyTypeId As Integer = -1
    Private mstrVacancyTypeName As String = String.Empty
    Private mintVacancyId As Integer = -1
    Private mstrVacancyName As String = String.Empty
    Private mstrOrderByQuery As String = ""
    Private mintCompanyUnkid As Integer = -1
    Private mintUserUnkid As Integer = -1
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintResidencyTypeId As Integer = -1
    Private mstrResidecyTypeName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantId() As Integer
        Set(ByVal value As Integer)
            mintApplicantId = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantName() As String
        Set(ByVal value As String)
            mstrApplicantName = value
        End Set
    End Property

    Public WriteOnly Property _VacancyTypeID() As Integer
        Set(ByVal value As Integer)
            mintVacancyTypeId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyTypeName() As String
        Set(ByVal value As String)
            mstrVacancyTypeName = value
        End Set
    End Property

    Public WriteOnly Property _VacancyID() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ResidencyTypeId() As Integer
        Set(ByVal value As Integer)
            mintResidencyTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ResidecyTypeName() As String
        Set(ByVal value As String)
            mstrResidecyTypeName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = -1
            mstrReportTypeName = ""
            mintApplicantId = -1
            mstrApplicantName = String.Empty
            mintVacancyTypeId = -1
            mstrVacancyTypeName = String.Empty
            mintVacancyId = -1
            mstrVacancyName = String.Empty
            mstrOrderByQuery = ""
            mintCompanyUnkid = -1
            mintUserUnkid = -1
            mintResidencyTypeId = -1
            mstrResidecyTypeName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 301, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintResidencyTypeId > 0 Then
                objDataOperation.AddParameter("@Residency", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResidencyTypeId)
                Select Case mintReportTypeId
                    Case 0  'APPLICANT RESIDENCE REPORT
                        Me._FilterQuery &= " AND applicant.residency = @Residency "

                    Case 1  'EMPLOYEE RESIDENCE REPROT
                        Me._FilterQuery &= " AND hremployee_master.residency = @Residency "

                End Select
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 302, "Residence : ") & " " & mstrResidecyTypeName & " "
            End If

            If mintApplicantId > 0 Then
                Me._FilterQuery &= " AND applicant.applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 303, "Applicant : ") & " " & mstrApplicantName & "    "
            End If

            If mintVacancyTypeId > 0 Then
                If mintVacancyTypeId = enVacancyType.EXTERNAL_VACANCY Then
                    Me._FilterQuery &= " AND (applicant.isexternalvacancy = 1 AND ISNULL(applicant.isbothintext, 0) = 0) "
                ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_VACANCY Then
                    Me._FilterQuery &= " AND (applicant.isexternalvacancy = 0 AND ISNULL(applicant.isbothintext, 0) = 0) "
                ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_AND_EXTERNAL Then
                    Me._FilterQuery &= " AND ISNULL(applicant.isbothintext, 0) = 1 "
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 304, "Vacancy Type : ") & "   " & mstrVacancyTypeName & "    "
            End If

            If mintVacancyId > 0 Then
                Me._FilterQuery &= " AND applicant.vacancyunkid = @vacancyunkid "
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 305, "Vacancy : ") & "   " & mstrVacancyName & "    "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Dim intArrayColumnWidth As Integer() = Nothing
            Dim strarrGroupColumns As String() = Nothing
            Dim dtExcelTable As DataTable = Nothing
            Select Case mintReportTypeId
                Case 0  'APPLICANT RESIDENCE REPORT
                    dtExcelTable = Generate_ApplicantResidencyReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xExportReportPath, xOpenReportAfterExport, False, False)

                Case 1  'EMPLOYEE RESIDENCE REPORT
                    dtExcelTable = Generate_EmployeeResidencyReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xExportReportPath, xOpenReportAfterExport, False, False)

                    If dtExcelTable IsNot Nothing Then
                        If mintViewIndex > 0 Then
                            dtExcelTable.Columns.Remove("Id")
                            dtExcelTable.Columns("GName").Caption = mstrReport_GroupName
                            Dim strGrpCols As String() = {"GName"}
                            strarrGroupColumns = strGrpCols
                        Else
                            dtExcelTable.Columns.Remove("Id")
                            dtExcelTable.Columns.Remove("GName")
                        End If
                    End If
            End Select

            If mintViewIndex > 0 Then
                ReDim intArrayColumnWidth(dtExcelTable.Columns.Count - 2)
            Else
                ReDim intArrayColumnWidth(dtExcelTable.Columns.Count - 1)
            End If

            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 100
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, dtExcelTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, mstrReportTypeName, "", "", , "", True, Nothing, Nothing, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_ApplicantResidencyReport(ByVal strDatabaseName As String, _
                                                 ByVal intUserUnkid As Integer, _
                                                 ByVal intYearUnkid As Integer, _
                                                 ByVal intCompanyUnkid As Integer, _
                                                 ByVal dtPeriodStart As Date, _
                                                 ByVal dtPeriodEnd As Date, _
                                                 ByVal strUserModeSetting As String, _
                                                 ByVal blnOnlyApproved As Boolean, _
                                                 ByVal strExportReportPath As String, _
                                                 ByVal blnOpenReportAfterExport As Boolean, _
                                                 ByVal blnIsweb As Boolean, _
                                                 ByVal blnShowFirstAppointmentDate As Boolean) As DataTable
        Dim StrQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim dtFinalTable As DataTable = Nothing
        Dim exForce As Exception = Nothing
        objDataOperation = New clsDataOperation
        Try

            StrQ = "SELECT " & _
                   "     applicantunkid " & _
                   "    ,code " & _
                   "    ,NAMEAdd " & _
                   "    ,vacancyunkid " & _
                   "    ,isexternalvacancy " & _
                   "    ,isbothintext " & _
                   "    ,vacancy " & _
                   "    ,ODate " & _
                   "    ,CDate " & _
                   "    ,aResidence " & _
                   "    ,residency " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         rcapplicant_master.applicantunkid " & _
                   "        ,rcapplicant_master.applicant_code AS code " & _
                   "		,rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS NAMEAdd " & _
                   "		,ISNULL(Vac.vacancyunkid, 0) AS vacancyunkid " & _
                   "		,ISNULL(Vac.isexternalvacancy, 0) AS isexternalvacancy " & _
                   "		,ISNULL(Vac.isbothintext, 0) AS isbothintext " & _
                   "        ,Vac.vacancy AS vacancy " & _
                   "        ,Vac.ODate AS ODate " & _
                   "        ,Vac.CDate AS CDate " & _
                   "        ,CASE WHEN rcapplicant_master.residency = 1 THEN @TM WHEN rcapplicant_master.residency = 2 THEN @TZ ELSE '' END AS aResidence " & _
                   "        ,rcapplicant_master.residency " & _
                   "FROM rcapplicant_master " & _
                   "	LEFT JOIN " & _
                   "	( " & _
                   "		SELECT " & _
                   "			 rcapp_vacancy_mapping.applicantunkid " & _
                   "			,rcapp_vacancy_mapping.vacancyunkid " & _
                   "			,rcvacancy_master.isexternalvacancy " & _
                   "			,rcvacancy_master.isbothintext " & _
                   "			,cfcommon_master.name AS vacancy " & _
                   "			,CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                   "			,CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                   "        FROM  rcapp_vacancy_mapping " & _
                   "			JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcapp_vacancy_mapping.vacancyunkid " & _
                   "			JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "  " & _
                   "		WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                   "    ) AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
                   "	WHERE isimport = 0 " & _
                   ") AS applicant " & _
                   "WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= "ORDER BY NAMEAdd "

            objDataOperation.AddParameter("@TM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1220, "Tanzania Mainland"))
            objDataOperation.AddParameter("@TZ", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1221, "Tanzania Zanzibar"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each _row As DataRow In dsList.Tables(0).Rows
                If IsDBNull(_row("vacancy")) = True Then Continue For
                _row("vacancy") = _row.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(_row.Item("ODate").ToString).ToShortDateString & " - " & _
                                                                                        eZeeDate.convertDate(_row.Item("CDate").ToString).ToShortDateString & ")"
            Next

            dtFinalTable = dsList.Tables(0).DefaultView.ToTable(False, "code", "NAMEAdd", "vacancy", "aResidence")

            dtFinalTable.Columns("code").Caption = Language.getMessage(mstrModuleName, 401, "Applicant Code")
            dtFinalTable.Columns("NAMEAdd").Caption = Language.getMessage(mstrModuleName, 402, "Applicant Name")
            dtFinalTable.Columns("vacancy").Caption = Language.getMessage(mstrModuleName, 403, "Applied Vacancy")
            dtFinalTable.Columns("aResidence").Caption = Language.getMessage(mstrModuleName, 201, "Residence")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ApplicantResidencyReport; Module Name: " & mstrModuleName)
        End Try
        Return dtFinalTable
    End Function

    Private Function Generate_EmployeeResidencyReport(ByVal strDatabaseName As String, _
                                                 ByVal intUserUnkid As Integer, _
                                                 ByVal intYearUnkid As Integer, _
                                                 ByVal intCompanyUnkid As Integer, _
                                                 ByVal dtPeriodStart As Date, _
                                                 ByVal dtPeriodEnd As Date, _
                                                 ByVal strUserModeSetting As String, _
                                                 ByVal blnOnlyApproved As Boolean, _
                                                 ByVal strExportReportPath As String, _
                                                 ByVal blnOpenReportAfterExport As Boolean, _
                                                 ByVal blnIsweb As Boolean, _
                                                 ByVal blnShowFirstAppointmentDate As Boolean) As DataTable
        Dim StrQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim dtFinalTable As DataTable = Nothing
        Dim exForce As Exception = Nothing
        objDataOperation = New clsDataOperation
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT " & _
                   "    employeecode AS eCode " & _
                   "   ,ISNULL(hremployee_master.firstname, '') +' ' + ISNULL(othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS eName " & _
                   "   ,ISNULL(SM.name,'') AS eBranch " & _
                   "   ,ISNULL(DGM.name,'') AS eDeptGrp " & _
                   "   ,ISNULL(DM.name,'') AS eDept " & _
                   "   ,ISNULL(SGM.name,'') AS eSecGrp " & _
                   "   ,ISNULL(SECM.name,'') AS eSection " & _
                   "   ,ISNULL(UGM.name,'') AS eUnitGrp " & _
                   "   ,ISNULL(UM.name,'') AS eUnit " & _
                   "   ,ISNULL(TM.name,'') AS eTeam " & _
                   "   ,ISNULL(CGM.name,'') AS eClassGrp " & _
                   "   ,ISNULL(CM.name,'') AS eClass " & _
                   "   ,ISNULL(JM.job_name,'') AS eJob " & _
                   "   ,CASE WHEN hremployee_master.residency = 1 THEN @TM WHEN hremployee_master.residency = 2 THEN @TZ ELSE '' END AS eResidence "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_master "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
                    "LEFT JOIN hrdepartment_group_master AS DGM ON Alloc.deptgroupunkid = DGM.deptgroupunkid " & _
                    "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "LEFT JOIN hrsectiongroup_master AS SGM ON Alloc.sectiongroupunkid = SGM.sectiongroupunkid " & _
                    "LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid " & _
                    "LEFT JOIN hrunitgroup_master AS UGM ON Alloc.unitgroupunkid = UGM.unitgroupunkid " & _
                    "LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid " & _
                    "LEFT JOIN hrteam_master AS TM ON Alloc.teamunkid = TM.teamunkid " & _
                    "LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN hrjobgroup_master AS JGM ON Jobs.jobgroupunkid = JGM.jobgroupunkid " & _
                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            If mintViewIndex > 0 Then
                StrQ &= "ORDER BY " & mstrAnalysis_OrderBy & ",  hremployee_master.employeecode "
            Else
                StrQ &= "ORDER BY  hremployee_master.employeecode "
            End If

            objDataOperation.AddParameter("@TM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1220, "Tanzania Mainland"))
            objDataOperation.AddParameter("@TZ", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1221, "Tanzania Zanzibar"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList.Tables(0).Columns("eCode").Caption = Language.getMessage("clsEmployee_Listing", 82, "Employee Code")
            dsList.Tables(0).Columns("eName").Caption = Language.getMessage("clsEmployee_Master", 46, "Employee Name")
            dsList.Tables(0).Columns("eBranch").Caption = Language.getMessage("clsEmployee_Listing", 63, "Branch")
            dsList.Tables(0).Columns("eDeptGrp").Caption = Language.getMessage("clsEmployee_Listing", 53, "Department Group")
            dsList.Tables(0).Columns("eDept").Caption = Language.getMessage("clsEmployee_Listing", 64, "Department")
            dsList.Tables(0).Columns("eSecGrp").Caption = Language.getMessage("clsEmployee_Listing", 54, "Section Group")
            dsList.Tables(0).Columns("eSection").Caption = Language.getMessage("clsEmployee_Listing", 65, "Section")
            dsList.Tables(0).Columns("eUnitGrp").Caption = Language.getMessage("clsEmployee_Listing", 55, "Unit Group")
            dsList.Tables(0).Columns("eUnit").Caption = Language.getMessage("clsEmployee_Listing", 66, "Unit")
            dsList.Tables(0).Columns("eTeam").Caption = Language.getMessage("clsEmployee_Listing", 56, "Team")
            dsList.Tables(0).Columns("eClassGrp").Caption = Language.getMessage("clsEmployee_Listing", 58, "Class Group")
            dsList.Tables(0).Columns("eClass").Caption = Language.getMessage("clsEmployee_Listing", 59, "Classes")
            dsList.Tables(0).Columns("eJob").Caption = Language.getMessage("clsEmployee_Listing", 67, "Job")
            dsList.Tables(0).Columns("eResidence").Caption = Language.getMessage(mstrModuleName, 201, "Residence")

            dtFinalTable = dsList.Tables(0).Copy

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmployeeResidencyReport; Module Name: " & mstrModuleName)
        End Try
        Return dtFinalTable
    End Function

#End Region

End Class
