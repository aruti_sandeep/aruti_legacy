﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmAgencyWorkedReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAgencyWorkedReport"
    Private objAwf As clsAgencyWorkedReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing

#End Region

#Region " Constructor "
    Public Sub New()
        objAwf = New clsAgencyWorkedReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objAwf.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objVacancy As New clsVacancy
        Dim objApplicantList As New clsApplicant_master
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = objApplicantList.GetApplicantList("list", True, , True)
            With cboApplicant
                .ValueMember = "applicantunkid"
                .DisplayMember = "applicantname"
                .DataSource = dsList.Tables("list")
                .SelectedValue = 0
            End With

            dsList = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Applicant Agency Worked For Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 101, "Employee Agency Worked For Report"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : dsList.Dispose() : objApplicantList = Nothing : objVacancy = Nothing : objMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboVacancyType.SelectedValue = 0
            Call cboReportType_SelectedIndexChanged(New Object(), New EventArgs())
            cboApplicant.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAwf.SetDefaultValue()

            objAwf._ReportTypeId = cboReportType.SelectedIndex
            objAwf._ReportTypeName = cboReportType.Text

            objAwf._ApplicantId = cboApplicant.SelectedValue
            objAwf._ApplicantName = cboApplicant.Text

            objAwf._VacancyID = cboVacancy.SelectedValue
            objAwf._VacancyName = cboVacancy.Text

            objAwf._VacancyTypeID = cboVacancyType.SelectedValue
            objAwf._VacancyTypeName = cboVacancyType.Text

            objAwf._EmployeeId = cboEmployee.SelectedValue
            objAwf._EmployeeName = cboEmployee.Text

            objAwf._ViewByIds = mstrStringIds
            objAwf._ViewIndex = mintViewIdx
            objAwf._ViewByName = mstrStringName

            objAwf._Analysis_Fields = mstrAnalysis_Fields
            objAwf._Analysis_Join = mstrAnalysis_Join
            objAwf._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAwf._Report_GroupName = mstrReport_GroupName

            objAwf._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmAgencyWorkedReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAwf = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAgencyWorkedReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAgencyWorkedReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)


            Call OtherSettings()

            eZeeHeader.Title = objAwf._ReportName
            eZeeHeader.Message = objAwf._ReportDesc

            Call FillCombo()
            Call ResetValue()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAgencyWorkedReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAgencyWorkedReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAgencyWorkedReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAgencyWorkedReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAgencyWorkedReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub

            objAwf.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, False, enPrintAction.None, enExportAction.ExcelExtra, 0)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Listing.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Listing"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboApplicant.ValueMember
                .DisplayMember = cboApplicant.DisplayMember
                .CodeMember = "applicant_code"
                .DataSource = cboApplicant.DataSource
            End With
            If frm.DisplayDialog Then
                cboApplicant.SelectedValue = frm.SelectedValue
                cboApplicant.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = cboVacancy.DataSource
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'APPLICANT RESIDENCE REPORT
                    cboEmployee.SelectedValue = 0
                    objpnlEmployee.Visible = False
                    objpnlApplicant.Visible = True
                    lnkAnalysisBy.Visible = False
                    btnAdvanceFilter.Visible = False

                    mstrStringIds = ""
                    mstrStringName = ""
                    mintViewIdx = 0
                    mstrAnalysis_Fields = ""
                    mstrAnalysis_Join = ""
                    mstrAnalysis_OrderBy = ""
                    mstrReport_GroupName = ""
                    mstrAdvanceFilter = ""

                    cboEmployee.SelectedValue = 0

                Case 1  'EMPLOYEE RESIDENCE REPORT
                    cboApplicant.SelectedValue = 0
                    cboVacancy.SelectedValue = 0
                    cboVacancyType.SelectedValue = 0
                    objpnlApplicant.Visible = False
                    objpnlEmployee.Visible = True
                    lnkAnalysisBy.Visible = True
                    btnAdvanceFilter.Visible = True

                    cboApplicant.SelectedValue = 0
                    cboVacancyType.SelectedValue = 0

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsCombo As New DataSet
            dsCombo = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", -1, CInt(cboVacancyType.SelectedValue))
            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Applicant Agency Worked For Report")
            Language.setMessage(mstrModuleName, 101, "Employee Agency Worked For Report")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class