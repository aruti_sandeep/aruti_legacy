#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsInterviewScoreReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsInterviewScoreReport"
    Private mstrReportId As String = enArutiReport.Interview_Score_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintApplicantId As Integer = -1
    Private mstrApplicantName As String = String.Empty
    Private mintVacancyTypeId As Integer = -1
    Private mstrVacancyTypeName As String = String.Empty
    Private mintVacancyId As Integer = -1
    Private mstrVacancyName As String = String.Empty
    Private mintBatchId As Integer = -1
    Private mstrBatchName As String = String.Empty
    Private mintInterviewTypeId As Integer = -1
    Private mstrInterviewTypeName As String = String.Empty
    Private mstrOrderByQuery As String = ""
    Private mintCompanyUnkid As Integer = -1
    Private mintUserUnkid As Integer = -1
    Private mintViewIndex As Integer = -1
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mdsInterviewList As DataSet

#End Region

#Region " Properties "
    Public WriteOnly Property _VacancyTypeID() As Integer
        Set(ByVal value As Integer)
            mintVacancyTypeId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyTypeName() As String
        Set(ByVal value As String)
            mstrVacancyTypeName = value
        End Set
    End Property

    Public WriteOnly Property _VacancyID() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

    Public WriteOnly Property _BatchID() As Integer
        Set(ByVal value As Integer)
            mintBatchId = value
        End Set
    End Property

    Public WriteOnly Property _BatchName() As String
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    Public WriteOnly Property _InterviewTypeID() As Integer
        Set(ByVal value As Integer)
            mintInterviewTypeId = value
        End Set
    End Property

    Public WriteOnly Property _InterviewTypeName() As String
        Set(ByVal value As String)
            mstrInterviewTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintApplicantId = 0
            mstrApplicantName = ""
            mintVacancyTypeId = 0
            mstrVacancyTypeName = ""
            mintVacancyId = 0
            mstrVacancyName = ""
            mintBatchId = 0
            mstrBatchName = ""
            mintInterviewTypeId = 0
            mstrInterviewTypeName = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintVacancyId > 0 Then
                Me._FilterQuery &= " AND rcbatchschedule_master.vacancyunkid = @vacancyunkid "
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Vacancy: ") & " " & mstrVacancyName & " "
            End If

            If mintBatchId > 0 Then
                Me._FilterQuery &= " AND rcapplicant_batchschedule_tran.batchscheduleunkid = @BatchId "
                objDataOperation.AddParameter("@BatchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Batch: ") & " " & mstrBatchName & " "
            End If

            If mintInterviewTypeId > 0 Then
                Me._FilterQuery &= " AND rcbatchschedule_master.interviewtypeunkid = @InterviewTypeId "
                objDataOperation.AddParameter("@InterviewTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterviewTypeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Interview Type: ") & " " & mstrInterviewTypeName & " "
            End If

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY analysisunkid, " & Me.OrderByQuery
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Order By : ") & " " & Me.OrderByDisplay
        Catch ex As Exception

        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub


    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(rcbatchschedule_master.batchname, '')", Language.getMessage(mstrModuleName, 13, "Batch Name")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(rcapplicant_master.firstname + ' ' + rcapplicant_master.surname, '')", Language.getMessage(mstrModuleName, 10, "Applicant Name")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(VM.name, '')", Language.getMessage(mstrModuleName, 12, "Vacancy")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal strEmployeeAsOnDate As String, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim objBatchScheduleInterviewer_tran As New clsBatchSchedule_interviewer_tran
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim mdtTableExcel As New DataTable
        Dim dtCol As DataColumn
        'Hemant (09 Jul 2020) -- Start
        'ENHANCEMENT(ZRA) : Add Total Score and Average Score columns in Interview Score Report
        Dim objResultMaster As New clsresult_master
        Dim dsResultList As New DataSet
        'Hemant (09 Jul 2020) -- End


        Try
            dtCol = New DataColumn("Sr.No", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Sr.No")
            dtCol.DefaultValue = 0
            mdtTableExcel.Columns.Add(dtCol)


            dtCol = New DataColumn("ApplicantName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Applicant Name")
            dtCol.DefaultValue = ""
            mdtTableExcel.Columns.Add(dtCol)

            dtCol = New DataColumn("BatchName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 13, "Batch Name")
            dtCol.DefaultValue = ""
            mdtTableExcel.Columns.Add(dtCol)

            dtCol = New DataColumn("VacancyType", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 11, "Vacancy Type")
            dtCol.DefaultValue = ""
            mdtTableExcel.Columns.Add(dtCol)

            dtCol = New DataColumn("Vacancy", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 12, "Vacancy")
            dtCol.DefaultValue = ""
            mdtTableExcel.Columns.Add(dtCol)

            dtCol = New DataColumn("Analysisunkid", System.Type.GetType("System.Int32"))
            dtCol.Caption = "Analysisunkid"
            dtCol.DefaultValue = 0
            mdtTableExcel.Columns.Add(dtCol)

            mdsInterviewList = objBatchScheduleInterviewer_tran.getComboList(mintBatchId, False, "List", , , _
                                                                                      " AND rcbatchschedule_interviewer_tran.interviewtypeunkid = " & mintInterviewTypeId & " ")
            Dim intId As Integer = -1
            For Each drRow As DataRow In mdsInterviewList.Tables(0).Select("Id = -1")
                drRow("Id") = intId
                intId = intId - 1
            Next
            mdsInterviewList.Tables(0).AcceptChanges()

            If mdsInterviewList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In mdsInterviewList.Tables("List").Rows
                    dtCol = New DataColumn("ColumnScore" & dtRow.Item("Id"))
                    dtCol.DataType = System.Type.GetType("System.String")
                    dtCol.Caption = dtRow.Item("Name")
                    dtCol.DefaultValue = ""
                    mdtTableExcel.Columns.Add(dtCol)
                Next
            End If

            'Hemant (09 Jul 2020) -- Start
            'ENHANCEMENT(ZRA) : Add Total Score and Average Score columns in Interview Score Report
            dtCol = New DataColumn("TotalScore", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 14, "Total Score")
            dtCol.DefaultValue = ""
            mdtTableExcel.Columns.Add(dtCol)

            dtCol = New DataColumn("AverageScore", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 15, "Average Score")
            dtCol.DefaultValue = ""
            mdtTableExcel.Columns.Add(dtCol)

            dtCol = New DataColumn("NoOfScoreCount", System.Type.GetType("System.Int32"))
            dtCol.Caption = "No Of Score Count"
            dtCol.DefaultValue = 0
            mdtTableExcel.Columns.Add(dtCol)

            dtCol = New DataColumn("IsNumeric", System.Type.GetType("System.Boolean"))
            dtCol.Caption = "Is Numeric"
            dtCol.DefaultValue = False
            mdtTableExcel.Columns.Add(dtCol)
            'Hemant (09 Jul 2020) -- End


            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            'Hemant (09 Jul 2020) -- Start
            'ENHANCEMENT(ZRA) : Add Total Score and Average Score columns in Interview Score Report
            dsResultList = objResultMaster.GetList("List")
            'Hemant (09 Jul 2020) -- End

            objDataOperation = New clsDataOperation

            Dim StrInnerQry, StrConditionQry As String

            StrQ = "SELECT " & _
                      "  rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
                          ",rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                          ",rcapplicant_batchschedule_tran.applicantunkid " & _
                          ",rcbatchschedule_master.batchcode AS BatchCode " & _
                          ",rcbatchschedule_master.batchname AS BatchName "

            If mintViewIndex > 0 Then
                StrQ &= ", ISNULL(rcapplicant_batchschedule_tran.batchscheduleunkid, 0 ) AS Id, ISNULL(rcbatchschedule_master.BatchName, '') AS GName "
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS ApplicantName " & _
                          ",CASE " & _
                          "     WHEN rcvacancy_master.isexternalvacancy = 1 THEN @External " & _
                          "     WHEN rcvacancy_master.isexternalvacancy = 0 THEN @Internal " & _
                          "     WHEN rcvacancy_master.isbothintext = 1 THEN @INTERNAL_AND_EXTERNAL " & _
                          "END AS 'Vacancy Type' " & _
                          ",VM.name AS vacancy " & _
                          ",rcbatchschedule_master.vacancyunkid " & _
                          ",rcbatchschedule_master.interviewtypeunkid " & _
                          ",cfcommon_master.name AS InterviewType " & _
                          ",CONVERT(CHAR(8),rcbatchschedule_master.interviewdate,112) AS Interviewdate " & _
                          ",CONVERT(CHAR(5),rcbatchschedule_master.interviewtime,108) AS Interviewtime " & _
                          ", ISNULL(rcinterviewanalysis_master.analysisunkid, 0 ) AS analysisunkid " & _
                          ", ISNULL(rcinterviewanalysis_tran.resultcodeunkid, 0 ) AS resultcodeunkid " & _
                          ", ISNULL(hrresult_master.resultname,'') AS resultcode " & _
                          ", ISNULL(rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid, 0) AS batchscheduleinterviewertranunkid " & _
                          ", ISNULL(rcbatchschedule_interviewer_tran.interviewerunkid, 0) AS interviewerunkid " & _
                          ", ISNULL(hrresult_master.result_level, 0 ) AS result_level " & _
                   "FROM rcapplicant_batchschedule_tran " & _
                      "LEFT JOIN rcinterviewanalysis_master ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid AND rcinterviewanalysis_master.isvoid = 0 " & _
                      "LEFT JOIN rcinterviewanalysis_tran ON rcinterviewanalysis_master.analysisunkid = rcinterviewanalysis_tran.analysisunkid AND rcinterviewanalysis_tran.isvoid = 0  " & _
                      "LEFT JOIN rcbatchschedule_interviewer_tran ON rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
                      "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid " & _
                          "JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                          "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
                          "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid " & _
                          "JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcbatchschedule_master.vacancyunkid " & _
                          "JOIN cfcommon_master AS VM ON VM.masterunkid = rcvacancy_master.vacancytitle AND VM.mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
                     " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid " & _
                  "WHERE 1 = 1 AND rcapplicant_batchschedule_tran.isvoid = 0 "

            'Hemant (09 Jul 2020) -- [ISNULL(hrresult_master.result_level, 0 ) AS result_level]
            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@External", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsVacancy", 5, "External"))
            objDataOperation.AddParameter("@Internal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsVacancy", 6, "Internal"))
            objDataOperation.AddParameter("@INTERNAL_AND_EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsVacancy", 7, "Internal and External"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each drIntviewer As DataRow In mdsInterviewList.Tables(0).Select("Id <= -1")
                For Each drTran As DataRow In dsList.Tables(0).Select("batchscheduleinterviewertranunkid =" & CInt(drIntviewer("batchscheduleinterviewertranunkid")))
                    drTran("interviewerunkid") = drIntviewer("Id")
                Next
            Next

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim dt_Row As DataRow = Nothing
            Dim iCnt As Integer = 1 : Dim StrGrpName As String = String.Empty
            Dim dtTable As DataTable
            If mstrReport_GroupName.Trim.Length > 0 Then
                dtTable = New DataView(dsList.Tables("DataTable"), "", "BatchName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("DataTable")
            End If

            For Each dtRow As DataRow In dtTable.Rows
                Dim drIsExistRow() As DataRow = mdtTableExcel.Select("analysisunkid =" & dtRow.Item("analysisunkid"))
                If drIsExistRow.Length > 0 Then
                    drIsExistRow(0).Item("ColumnScore" & dtRow.Item("interviewerunkid")) = dtRow.Item("Resultcode")
                    'Hemant (09 Jul 2020) -- Start
                    'ENHANCEMENT(ZRA) : Add Total Score and Average Score columns in Interview Score Report
                    If IsNumeric(dtRow.Item("Resultcode")) AndAlso CBool(drIsExistRow(0).Item("IsNumeric")) = True Then
                        drIsExistRow(0).Item("TotalScore") = CDbl(drIsExistRow(0).Item("TotalScore")) + CDbl(dtRow.Item("Resultcode"))
                        drIsExistRow(0).Item("NoOfScoreCount") = CInt(drIsExistRow(0).Item("NoOfScoreCount")) + 1
                        drIsExistRow(0).Item("AverageScore") = (CDbl(drIsExistRow(0).Item("TotalScore")) / CInt(drIsExistRow(0).Item("NoOfScoreCount"))).ToString("#.##")

                    Else
                        If CBool(drIsExistRow(0).Item("IsNumeric")) = False Then
                            drIsExistRow(0).Item("TotalScore") = CDbl(drIsExistRow(0).Item("TotalScore")) + CDbl(dtRow.Item("Result_level"))
                            drIsExistRow(0).Item("NoOfScoreCount") = CInt(drIsExistRow(0).Item("NoOfScoreCount")) + 1
                            Dim intAverageScoreLevel As Integer = CInt(Math.Truncate(CDbl(drIsExistRow(0).Item("TotalScore")) / CInt(drIsExistRow(0).Item("NoOfScoreCount"))))

                            Dim ScoreLevel() As DataRow = dsResultList.Tables(0).Select("result_level = " & intAverageScoreLevel)
                            If ScoreLevel.Length > 0 Then
                                drIsExistRow(0).Item("AverageScore") = ScoreLevel(0).Item("resultname").ToString.Trim
                            End If
                        End If
                    End If
                    'Hemant (09 Jul 2020) -- End
                Else
                    If mdtTableExcel.Columns.Contains("ColumnScore" & dtRow.Item("interviewerunkid")) = True Then
                        dt_Row = mdtTableExcel.NewRow

                        dt_Row.Item("ApplicantName") = dtRow.Item("ApplicantName")
                        dt_Row.Item("VacancyType") = dtRow.Item("Vacancy Type")
                        dt_Row.Item("Vacancy") = dtRow.Item("Vacancy")
                        dt_Row.Item("BatchName") = dtRow.Item("BatchName")
                        dt_Row.Item("Sr.No") = iCnt.ToString
                        dt_Row.Item("analysisunkid") = dtRow.Item("analysisunkid")
                        dt_Row.Item("ColumnScore" & dtRow.Item("interviewerunkid")) = dtRow.Item("Resultcode")
                        'Hemant (09 Jul 2020) -- Start
                        'ENHANCEMENT(ZRA) : Add Total Score and Average Score columns in Interview Score Report
                        If IsNumeric(dtRow.Item("Resultcode")) Then
                            dt_Row.Item("TotalScore") = dtRow.Item("Resultcode")
                            dt_Row.Item("AverageScore") = dtRow.Item("Resultcode")
                            dt_Row.Item("NoOfScoreCount") = 1
                            dt_Row.Item("IsNumeric") = True
                        Else
                            dt_Row.Item("TotalScore") = dtRow.Item("Result_level")
                            dt_Row.Item("AverageScore") = dtRow.Item("Result_level")
                            dt_Row.Item("NoOfScoreCount") = 1
                            dt_Row.Item("IsNumeric") = False
                        End If
                        'Hemant (09 Jul 2020) -- End
                        mdtTableExcel.Rows.Add(dt_Row)
                        iCnt += 1
                    End If
                End If
            Next

            'Hemant (09 Jul 2020) -- Start
            'ENHANCEMENT(ZRA) : Add Total Score and Average Score columns in Interview Score Report
            For Each drRow As DataRow In mdtTableExcel.Select(" IsNumeric = 'False' ")
                drRow.Item("TotalScore") = ""
            Next

            mdtTableExcel.Columns.Remove("NoOfScoreCount")
            mdtTableExcel.Columns.Remove("IsNumeric")
            'Hemant (09 Jul 2020) -- End
            mdtTableExcel.Columns.Remove("analysisunkid")

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            'Dim mdtTableExcel As DataTable = dsList.Tables(0).Clone

            If mintViewIndex > 0 Then
                Dim strGrpCols As String() = {"BatchName"}
                strarrGroupColumns = strGrpCols
            End If


            Dim columnNames As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, columnNames)

            row = New WorksheetRow()

            If mintViewIndex > 0 Then
            Else
                mdtTableExcel.Columns.Remove("BatchName")
            End If

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 100
            Next
            'SET EXCEL CELL WIDTH

            'mdtTableExcel.Columns("formno").Caption = Language.getMessage(mstrModuleName, 1, "Form No")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsVacancy", 5, "External")
            Language.setMessage("clsVacancy", 6, "Internal")
            Language.setMessage("clsVacancy", 7, "Internal and External")
            Language.setMessage(mstrModuleName, 1, "Vacancy:")
            Language.setMessage(mstrModuleName, 2, "Batch:")
            Language.setMessage(mstrModuleName, 3, "Interview Type:")
            Language.setMessage(mstrModuleName, 4, " Order By :")
            Language.setMessage(mstrModuleName, 5, "Sr.No")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, "Applicant Name")
            Language.setMessage(mstrModuleName, 11, "Vacancy Type")
            Language.setMessage(mstrModuleName, 12, "Vacancy")
            Language.setMessage(mstrModuleName, 13, "Batch Name")
            Language.setMessage(mstrModuleName, 14, "Total Score")
            Language.setMessage(mstrModuleName, 15, "Average Score")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
