Imports System.Windows.Forms
Imports System.Drawing
Imports System.ComponentModel

Public Class TemplateBoxField
    Inherits BoxControl

    Implements ITemplatePropertyClass

    Private m_publicproperties As New TemplatePropertyList()
    Public Property PublicProperties() As TemplatePropertyList Implements ITemplatePropertyClass.PublicProperties
        Get
            Return m_publicproperties
        End Get
        Set(ByVal value As TemplatePropertyList)
            m_publicproperties = value
        End Set
    End Property

    Private mstrFieldName As String = ""
    Public Property _FieldName() As String
        Get
            Return mstrFieldName
        End Get
        Set(ByVal value As String)
            mstrFieldName = value
        End Set
    End Property

    Private intFieldUnkID As Integer = -1
    Public Property _FieldUnkID() As Integer
        Get
            Return intFieldUnkID
        End Get
        Set(ByVal value As Integer)
            intFieldUnkID = value
        End Set
    End Property

    'Private intFieldType As Integer = 0
    'Public Property _FieldType() As Integer
    '    Get
    '        Return intFieldType
    '    End Get
    '    Set(ByVal value As Integer)
    '        intFieldType = value
    '    End Set
    'End Property

    Private strValue As String = ""
    Public Property _Value() As String
        Get
            Return strValue
        End Get
        Set(ByVal value As String)
            strValue = value
            Me.Text = value
        End Set
    End Property

    Private intFieldId As Integer = 0
    Public Property _FieldId() As Integer
        Get
            Return intFieldId
        End Get
        Set(ByVal value As Integer)
            intFieldId = value
        End Set
    End Property

    Private intSectionId As Integer = 0
    Public Property _SectionId() As Integer
        Get
            Return intSectionId
        End Get
        Set(ByVal value As Integer)
            intSectionId = value
        End Set
    End Property

    Public Shadows Property LineStyle() As System.Drawing.Drawing2D.DashStyle
        Get
            Return MyBase._lineStyle
        End Get
        Set(ByVal value As System.Drawing.Drawing2D.DashStyle)
            MyBase._lineStyle = value
            Me.Refresh()
        End Set
    End Property

    Public Shadows Property LineThickness() As Integer
        Get
            Return MyBase._lineThickness
        End Get
        Set(ByVal value As Integer)
            If value > 6 Then
                value = 6
                MyBase._lineThickness = value
            Else
                MyBase._lineThickness = value
            End If
            Me.Refresh()
        End Set
    End Property

    Public Shadows Property LineColor() As Color
        Get
            Return MyBase._lineColor
        End Get
        Set(ByVal value As Color)
            MyBase._lineColor = value
            Me.Refresh()
        End Set
    End Property

    Public Shadows Property UpperLeftR() As Integer
        Get
            Return MyBase.intUpperLeftR
        End Get
        Set(ByVal value As Integer)
            If Value < 1 Then Value = 1
            MyBase.intUpperLeftR = value
            Me.Invalidate()
        End Set
    End Property

    Public Shadows Property UpperRightR() As Integer
        Get
            Return MyBase.intUpperRightR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            MyBase.intUpperRightR = value
            Me.Invalidate()
        End Set
    End Property

    Public Shadows Property LowerLeftR() As Integer
        Get
            Return MyBase.intLowerLeftR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            MyBase.intLowerLeftR = value
            Me.Invalidate()
        End Set
    End Property

    Public Shadows Property LowerRightR() As Integer
        Get
            Return MyBase.intLowerRightR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            MyBase.intLowerRightR = value
            Me.Invalidate()
        End Set
    End Property

    Public Shadows Property AllR() As Integer
        Get
            Return MyBase.intAllR
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then value = 1
            MyBase.intAllR = value
            MyBase.intUpperLeftR = value
            MyBase.intUpperRightR = value
            MyBase.intLowerLeftR = value
            MyBase.intLowerRightR = value
            Me.Invalidate()
        End Set
    End Property

    Public Shadows Property FieldType() As Integer
        Get
            Return MyBase.intFieldType
        End Get
        Set(ByVal value As Integer)
            MyBase.intFieldType = value
            Me.Invalidate()
        End Set
    End Property

#Region "Base Property "
    <Browsable(True)> _
    Public Shadows Property Height() As Integer
        Get
            Return MyBase.Height
        End Get
        Set(ByVal value As Integer)
            MyBase.Height = value
        End Set
    End Property

    <Browsable(True)> _
   Public Shadows Property Width() As Integer
        Get
            Return MyBase.Width
        End Get
        Set(ByVal value As Integer)
            MyBase.Width = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Top() As Integer
        Get
            Return MyBase.Top
        End Get
        Set(ByVal value As Integer)
            MyBase.Top = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Left() As Integer
        Get
            Return MyBase.Left
        End Get
        Set(ByVal value As Integer)
            MyBase.Left = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Font() As System.Drawing.Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As System.Drawing.Font)
            MyBase.Font = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property TextAlign() As ContentAlignment
        Get
            Return MyBase.TextAlign
        End Get
        Set(ByVal value As ContentAlignment)
            MyBase.TextAlign = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property ForeColor() As Color
        Get
            Return MyBase.ForeColor
        End Get
        Set(ByVal value As Color)
            MyBase.ForeColor = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property BackColor() As Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As Color)
            MyBase.BackColor = value
            If Me.FieldType = 4 Then
                MyBase.BackColor = Color.Transparent
            Else
                MyBase.BackColor = value
            End If
        End Set
    End Property

    'Manish Tanna (12 May 2012) -- start     
    Private mstrFormat As String = ""
    <Browsable(True)> _
    Public Property Format() As String
        Get
            Return mstrFormat
        End Get
        Set(ByVal value As String)
            mstrFormat = value
        End Set
    End Property
    'Manish Tanna (12 May 2012) -- end    

#End Region
End Class
