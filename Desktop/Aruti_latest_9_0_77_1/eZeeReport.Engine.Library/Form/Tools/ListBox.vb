﻿Public Class ListBox
    Public Event Buttons_Click()


    Public Enum PstLRTB
        Bottom = 0
        Right = 1
        Top = 2
        Left = 3
    End Enum

    Dim _Postion As PstLRTB

    Public Property Postion() As PstLRTB
        Get
            Return _Postion
        End Get
        Set(ByVal value As PstLRTB)
            _Postion = value
            PostionChange(_Postion)
        End Set
    End Property
    Private Sub PostionChange(ByVal post As String)
        If post = 0 Then
            tlp_ListBox.ColumnStyles(0).Width = 0
            tlp_ListBox.ColumnStyles(2).Width = 0
            tlp_ListBox.RowStyles(0).Height = 0
            tlp_ListBox.RowStyles(2).Height = 27
        ElseIf post = 1 Then
            tlp_ListBox.ColumnStyles(0).Width = 0
            tlp_ListBox.ColumnStyles(2).Width = 27
            tlp_ListBox.RowStyles(0).Height = 0
            tlp_ListBox.RowStyles(2).Height = 0
        ElseIf post = 2 Then
            tlp_ListBox.ColumnStyles(0).Width = 0
            tlp_ListBox.ColumnStyles(2).Width = 0
            tlp_ListBox.RowStyles(0).Height = 27
            tlp_ListBox.RowStyles(2).Height = 0
        ElseIf post = 3 Then
            tlp_ListBox.ColumnStyles(0).Width = 27
            tlp_ListBox.ColumnStyles(2).Width = 0
            tlp_ListBox.RowStyles(0).Height = 0
            tlp_ListBox.RowStyles(2).Height = 0
        End If
    End Sub

    Private Sub ListBox_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PostionChange(_Postion)
    End Sub

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        RaiseEvent Buttons_Click()
    End Sub

    Private Sub tlp_ListBox_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tlp_ListBox.Paint

    End Sub

    Private Sub Button1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button1.KeyDown

    End Sub

    Private Sub Button1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button1.MouseClick
        If e.Clicks Then

        End If
    End Sub
End Class
