﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmBatchTransaction_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBatchTransaction_AddEdit"
    Private mblnCancel As Boolean = True
    Private objBatch As clsBatchTransaction
    Private objBatchTran As clsBatchTransactionTran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintBatchTransactionUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mintItemIndex As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBatchTransactionUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintBatchTransactionUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtBatchCode.BackColor = GUI.ColorComp
            txtBatchName.BackColor = GUI.ColorComp
            cboTrnHeadType.BackColor = GUI.ColorOptional
            cboTypeOf.BackColor = GUI.ColorOptional
            cboCalcType.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtBatchCode.Text = objBatch._Batch_Code
            txtBatchName.Text = objBatch._Batch_Name
            objBatchTran._BatchTransactionUnkid = objBatch._Batchtransactionunkid
            mdtTran = objBatchTran._DataList
            Call FillItemList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBatch._Batch_Code = txtBatchCode.Text.Trim
            objBatch._Batch_Name = txtBatchName.Text.Trim
            objBatch._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objTranHead As New clsTransactionHead
        Dim dsCombo As New DataSet

        Try
            dsCombo = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("HeadType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objMaster As New clsMasterData
        Dim objTransactionHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim strFilter As String = ""
        Try
            strFilter = "typeof_id <> " & enTypeOf.Salary & " "
            If CInt(cboTrnHeadType.SelectedValue) > 0 Then
                strFilter &= "AND trnheadtype_id = " & CInt(cboTrnHeadType.SelectedValue) & " "
            End If
            If CInt(cboTypeOf.SelectedValue) > 0 Then
                strFilter &= "AND typeof_id = " & CInt(cboTypeOf.SelectedValue) & " "
            End If
            If CInt(cboCalcType.SelectedValue) > 0 Then
                strFilter &= "AND calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
            End If
            'Sohail (29 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objTransactionHead.GetList("TranHead", , CInt(cboTrnHeadType.SelectedValue), True)
            dsList = objTransactionHead.GetList("TranHead", , CInt(cboTrnHeadType.SelectedValue), True, , , True)
            'Sohail (29 Oct 2012) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), strFilter, "", DataViewRowState.CurrentRows).ToTable

            Dim lvItem As ListViewItem

            lvTrnHeadList.Items.Clear()
            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = "" 'Checkbox column

                lvItem.SubItems.Add(drRow("tranheadunkid").ToString)

                lvItem.SubItems.Add(drRow("trnheadname").ToString)

                lvItem.SubItems.Add(drRow("headtype").ToString)

                lvItem.SubItems.Add(drRow("typeof").ToString)


                lvItem.SubItems.Add(drRow("CalcType").ToString)


                lvTrnHeadList.Items.Add(lvItem)
            Next

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objMaster = Nothing
            objTransactionHead = Nothing
        End Try
    End Sub

    Private Sub FillItemList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim blnAdd As Boolean = False
        Try
            lvBatchHeads.Items.Clear()
            chkSelectAll.CheckState = CheckState.Unchecked
            Dim lvItems As ListViewItem
            Dim drawItemInfo As DataRow
            For Each drawItemInfo In mdtTran.Rows
                If CStr(IIf(IsDBNull(drawItemInfo.Item("AUD")), "A", drawItemInfo.Item("AUD"))) <> "D" Then

                    dsList = objTranHead.GetList("TranHead", CInt(drawItemInfo.Item("tranheadunkid").ToString), , True)
                    If dsList.Tables("TranHead").Rows.Count <= 0 Then Continue For

                    If CInt(dsList.Tables("TranHead").Rows(0).Item("typeof_id")) = enTypeOf.Salary Then ' Remove Salary Heads if Added in Batch Transaction
                        drawItemInfo.Item("AUD") = "D"
                        mdtTran.AcceptChanges()
                    Else
                        lvItems = New ListViewItem
                        lvItems.Text = "" 'drawItemInfo.Item("batchtransactionunkid").ToString
                        lvItems.Tag = drawItemInfo.Item("batchtransactiontranunkid").ToString


                        lvItems.SubItems.Add(drawItemInfo.Item("tranheadunkid").ToString) 'trnheadId
                        lvItems.SubItems.Add(dsList.Tables("TranHead").Rows(0).Item("trnheadname").ToString)
                        lvItems.SubItems.Add(dsList.Tables("TranHead").Rows(0).Item("headtype").ToString)  'headtype
                        lvItems.SubItems.Add(dsList.Tables("TranHead").Rows(0).Item("typeof").ToString)
                        lvItems.SubItems.Add(dsList.Tables("TranHead").Rows(0).Item("CalcType").ToString)
                        lvItems.SubItems.Add(drawItemInfo.Item("GUID").ToString)
                        RemoveHandler lvBatchHeads.ItemChecked, AddressOf lvBatchHeads_ItemChecked
                        lvBatchHeads.Items.Add(lvItems)
                        AddHandler lvBatchHeads.ItemChecked, AddressOf lvBatchHeads_ItemChecked

                        blnAdd = True
                    End If
                End If
            Next

            If blnAdd = True Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Unchecked
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillItemList", mstrModuleName)
        End Try
    End Sub
    Private Sub CheckAllTranHeads(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvTrnHeadList.Items
                RemoveHandler lvTrnHeadList.ItemChecked, AddressOf lvTrnHeadList_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvTrnHeadList.ItemChecked, AddressOf lvTrnHeadList_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllTranHeads", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllBatchHeads(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvBatchHeads.Items
                RemoveHandler lvBatchHeads.ItemChecked, AddressOf lvBatchHeads_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvBatchHeads.ItemChecked, AddressOf lvBatchHeads_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllBatchHeads", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmBT_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBatch = Nothing
            objBatchTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBT_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBT_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    Windows.Forms.SendKeys.Send("{Tab}")
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBT_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBT_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBatch = New clsBatchTransaction
        objBatchTran = New clsBatchTransactionTran
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objBatch._Batchtransactionunkid = mintBatchTransactionUnkid

            End If

            Call GetValue()

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBT_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBatchTransaction.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchTransaction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " ComboBox's Events "
    Private Sub cboTrnHeadType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedValueChanged
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable

        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedValueChanged", mstrModuleName)
        Finally
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        'Sohail (28 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION
        Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION & "," & enCalcType.TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_CASH_BENEFIT_TOTAL
        'Sohail (28 Jan 2012) -- End
        Try
            Select Case CInt(cboTypeOf.SelectedValue)
                Case Is > 1
                    dsList = objMaster.getComboListCalcType("CalcType", "2") 'Sohail (28 Jan 2012)

                    If CInt(cboTypeOf.SelectedValue) = enTypeOf.Other_Earnings Then
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.Other_Deductions_Emp Then
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.Informational Then
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                    End If
                Case Else 'Type of - Salary
                    dsList = objMaster.getComboListCalcType("CalcType", CInt(cboTypeOf.SelectedValue).ToString) 'Sohail (28 Jan 2012)
                    dtTable = New DataView(dsList.Tables("CalcType"), "", "", DataViewRowState.CurrentRows).ToTable
            End Select

            With cboCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
        Finally
            dsList = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If txtBatchCode.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Batch Code cannot be blank. Batch Code is required information."), enMsgBoxStyle.Information)
                txtBatchCode.Focus()
                Exit Sub
            ElseIf txtBatchName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Batch Name cannot be blank. Batch Name is required information."), enMsgBoxStyle.Information)
                txtBatchName.Focus()
                Exit Sub
            ElseIf lvBatchHeads.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please add atleast one transaction head."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBatch._FormName = mstrModuleName
            objBatch._LoginEmployeeunkid = 0
            objBatch._ClientIP = getIP()
            objBatch._HostName = getHostName()
            objBatch._FromWeb = False
            objBatch._AuditUserId = User._Object._Userunkid
objBatch._CompanyUnkid = Company._Object._Companyunkid
            objBatch._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBatch.Update(mdtTran)
            Else
                blnFlag = objBatch.Insert(mdtTran)
            End If

            If blnFlag = False And objBatch._Message <> "" Then
                eZeeMsgBox.Show(objBatch._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBatch = Nothing
                    objBatch = New clsBatchTransaction
                    Call GetValue()
                    lvBatchHeads.Items.Clear()
                    txtBatchCode.Focus()
                Else
                    mintBatchTransactionUnkid = objBatch._Batchtransactionunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'Anjan (02 Sep 2011)-End 
            Call objFrmLangPopup.displayDialog(txtBatchName.Text, objBatch._Batch_Name1, objBatch._Batch_Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Listview's Events "
    Private Sub lvTrnHeadList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvTrnHeadList.ItemChecked
        Try
            If lvTrnHeadList.CheckedItems.Count <= 0 Then
                RemoveHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
                chkFindCheck.CheckState = CheckState.Unchecked
                AddHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
            ElseIf lvTrnHeadList.CheckedItems.Count < lvTrnHeadList.Items.Count Then
                RemoveHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
                chkFindCheck.CheckState = CheckState.Indeterminate
                AddHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
            ElseIf lvTrnHeadList.CheckedItems.Count = lvTrnHeadList.Items.Count Then
                RemoveHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
                chkFindCheck.CheckState = CheckState.Checked
                AddHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTrnHeadList_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvBatchHeads_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvBatchHeads.ItemChecked
        Try
            If lvBatchHeads.CheckedItems.Count <= 0 Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Unchecked
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvBatchHeads.CheckedItems.Count < lvBatchHeads.Items.Count Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvBatchHeads.CheckedItems.Count = lvBatchHeads.Items.Count Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Checked
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvBatchHeads_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkFindCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFindCheck.CheckedChanged
        Try
            Call CheckAllTranHeads(chkFindCheck.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkFindCheck_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Call CheckAllBatchHeads(chkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    
    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Dim blnAdd As Boolean = False
        Try

            For Each lvItem As ListViewItem In lvTrnHeadList.CheckedItems

                Dim dtRow As DataRow() = mdtTran.Select("tranheadunkid = " & CInt(lvItem.SubItems(objcolhFindTranHeadID.Index).Text) & " AND (AUD <> 'D' OR AUD IS NULL)")

                If dtRow.Length <= 0 Then
                    Dim drawItemInfo As DataRow
                    drawItemInfo = mdtTran.NewRow()
                    With drawItemInfo
                        .Item("batchtransactiontranunkid") = -1
                        'Sohail (12 Oct 2011) -- Start
                        '.Item("batchtransactionunkid") = -1
                        .Item("batchtransactionunkid") = mintBatchTransactionUnkid
                        'Sohail (12 Oct 2011) -- End
                        .Item("tranheadunkid") = CInt(lvItem.SubItems(objcolhFindTranHeadID.Index).Text)
                        .Item("userunkid") = User._Object._Userunkid
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("GUID") = Guid.NewGuid.ToString
                        .Item("AUD") = "A"

                        blnAdd = True
                    End With
                    mdtTran.Rows.Add(drawItemInfo)
                End If
                RemoveHandler lvTrnHeadList.ItemChecked, AddressOf lvTrnHeadList_ItemChecked
                lvItem.Checked = False
                AddHandler lvTrnHeadList.ItemChecked, AddressOf lvTrnHeadList_ItemChecked

            Next
            If blnAdd = True Then
                Call FillItemList()

                RemoveHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
                chkFindCheck.CheckState = CheckState.Unchecked
                AddHandler chkFindCheck.CheckedChanged, AddressOf chkFindCheck_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnUnassign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnassign.Click
        Dim drTemp() As DataRow
        Dim blnFound As Boolean = False
        Try

            For Each lvItem As ListViewItem In lvBatchHeads.CheckedItems
                drTemp = mdtTran.Select("tranheadunkid = " & CInt(lvItem.SubItems(objcolhTranHeadID.Index).Text) & "  AND (AUD <> 'D' OR AUD IS NULL)")

                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    blnFound = True
                End If
                If blnFound = True Then Call FillItemList()
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnassign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboTrnHeadType.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Messages "
    '1, "Batch Code cannot be blank. Batch Code is required information."
    '2, "Batch Name cannot be blank. Batch Name is required information."
    '3, "Please add atleast one transaction head."
#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBatchInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBatchInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnUnassign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnUnassign.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhTranHead.Text = Language._Object.getCaption(CStr(Me.colhTranHead.Tag), Me.colhTranHead.Text)
			Me.colhHeadtype.Text = Language._Object.getCaption(CStr(Me.colhHeadtype.Tag), Me.colhHeadtype.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.lblBatchCode.Text = Language._Object.getCaption(Me.lblBatchCode.Name, Me.lblBatchCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbBatchInfo.Text = Language._Object.getCaption(Me.gbBatchInfo.Name, Me.gbBatchInfo.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEffectiveFrom.Text = Language._Object.getCaption(Me.lblEffectiveFrom.Name, Me.lblEffectiveFrom.Text)
			Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
			Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)
			Me.colhFindTranHead.Text = Language._Object.getCaption(CStr(Me.colhFindTranHead.Tag), Me.colhFindTranHead.Text)
			Me.colhFindHeadType.Text = Language._Object.getCaption(CStr(Me.colhFindHeadType.Tag), Me.colhFindHeadType.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.chkFindCheck.Text = Language._Object.getCaption(Me.chkFindCheck.Name, Me.chkFindCheck.Text)
			Me.colhFindTypeOf.Text = Language._Object.getCaption(CStr(Me.colhFindTypeOf.Tag), Me.colhFindTypeOf.Text)
			Me.colhFindCalcType.Text = Language._Object.getCaption(CStr(Me.colhFindCalcType.Tag), Me.colhFindCalcType.Text)
			Me.colhTypeOf.Text = Language._Object.getCaption(CStr(Me.colhTypeOf.Tag), Me.colhTypeOf.Text)
			Me.colhCalcType.Text = Language._Object.getCaption(CStr(Me.colhCalcType.Tag), Me.colhCalcType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Batch Code cannot be blank. Batch Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Batch Name cannot be blank. Batch Name is required information.")
			Language.setMessage(mstrModuleName, 3, "Please add atleast one transaction head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class