﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSalaryIncrementList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSalaryIncrementList))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.pnlEdList = New System.Windows.Forms.Panel
        Me.dgvSalaryIncrement = New System.Windows.Forms.DataGridView
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblAccess = New System.Windows.Forms.Label
        Me.lblSalaryChangeApprovalStatus = New System.Windows.Forms.Label
        Me.cboSalaryChangeApprovalStatus = New System.Windows.Forms.ComboBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboPayYear = New System.Windows.Forms.ComboBox
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuGlobalAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportSalaryChange = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSalaryHistroy = New System.Windows.Forms.ToolStripMenuItem
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDisApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgColhBlank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPayPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIncrDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhActualDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIncrementAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhNewScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAuto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhApprovePending = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIncrementID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhEmployeeUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhGradegroupunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhGradeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhGradelevelunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.pnlEdList.SuspendLayout()
        CType(Me.dgvSalaryIncrement, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objchkSelectAll)
        Me.pnlMainInfo.Controls.Add(Me.pnlEdList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(974, 541)
        Me.pnlMainInfo.TabIndex = 4
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(21, 144)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 19
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'pnlEdList
        '
        Me.pnlEdList.Controls.Add(Me.dgvSalaryIncrement)
        Me.pnlEdList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEdList.Location = New System.Drawing.Point(12, 138)
        Me.pnlEdList.Name = "pnlEdList"
        Me.pnlEdList.Size = New System.Drawing.Size(950, 342)
        Me.pnlEdList.TabIndex = 20
        '
        'dgvSalaryIncrement
        '
        Me.dgvSalaryIncrement.AllowUserToAddRows = False
        Me.dgvSalaryIncrement.AllowUserToDeleteRows = False
        Me.dgvSalaryIncrement.AllowUserToResizeRows = False
        Me.dgvSalaryIncrement.BackgroundColor = System.Drawing.Color.White
        Me.dgvSalaryIncrement.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvSalaryIncrement.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgColhBlank, Me.colhPayPeriod, Me.colhEmployeeName, Me.colhIncrDate, Me.colhActualDate, Me.colhScale, Me.colhIncrementAmt, Me.colhNewScale, Me.objcolhAuto, Me.colhGradeGroup, Me.colhGrade, Me.colhGradeLevel, Me.colhApprovePending, Me.objcolhIsGroup, Me.objcolhIncrementID, Me.objcolhPeriodUnkId, Me.objcolhEmployeeUnkId, Me.objcolhGradegroupunkid, Me.objcolhGradeunkid, Me.objcolhGradelevelunkid, Me.objcolhEmpCode})
        Me.dgvSalaryIncrement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvSalaryIncrement.Location = New System.Drawing.Point(0, 0)
        Me.dgvSalaryIncrement.Name = "dgvSalaryIncrement"
        Me.dgvSalaryIncrement.RowHeadersVisible = False
        Me.dgvSalaryIncrement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSalaryIncrement.Size = New System.Drawing.Size(950, 342)
        Me.dgvSalaryIncrement.TabIndex = 124
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblAccess)
        Me.gbFilterCriteria.Controls.Add(Me.lblSalaryChangeApprovalStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboSalaryChangeApprovalStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayYear)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayYear)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(950, 66)
        Me.gbFilterCriteria.TabIndex = 8
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(362, 33)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(130, 21)
        Me.cboPayPeriod.TabIndex = 4
        '
        'lblAccess
        '
        Me.lblAccess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccess.Location = New System.Drawing.Point(285, 35)
        Me.lblAccess.Name = "lblAccess"
        Me.lblAccess.Size = New System.Drawing.Size(71, 17)
        Me.lblAccess.TabIndex = 77
        Me.lblAccess.Text = "Pay Period"
        Me.lblAccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSalaryChangeApprovalStatus
        '
        Me.lblSalaryChangeApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryChangeApprovalStatus.Location = New System.Drawing.Point(648, 37)
        Me.lblSalaryChangeApprovalStatus.Name = "lblSalaryChangeApprovalStatus"
        Me.lblSalaryChangeApprovalStatus.Size = New System.Drawing.Size(95, 15)
        Me.lblSalaryChangeApprovalStatus.TabIndex = 302
        Me.lblSalaryChangeApprovalStatus.Text = "Approval Status"
        '
        'cboSalaryChangeApprovalStatus
        '
        Me.cboSalaryChangeApprovalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalaryChangeApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalaryChangeApprovalStatus.FormattingEnabled = True
        Me.cboSalaryChangeApprovalStatus.Location = New System.Drawing.Point(749, 34)
        Me.cboSalaryChangeApprovalStatus.Name = "cboSalaryChangeApprovalStatus"
        Me.cboSalaryChangeApprovalStatus.Size = New System.Drawing.Size(155, 21)
        Me.cboSalaryChangeApprovalStatus.TabIndex = 301
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(811, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 92
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'cboPayYear
        '
        Me.cboPayYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYear.FormattingEnabled = True
        Me.cboPayYear.Location = New System.Drawing.Point(360, 45)
        Me.cboPayYear.Name = "cboPayYear"
        Me.cboPayYear.Size = New System.Drawing.Size(50, 21)
        Me.cboPayYear.TabIndex = 6
        Me.cboPayYear.Visible = False
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(276, 51)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(71, 15)
        Me.lblPayYear.TabIndex = 90
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(218, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(82, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(157, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(68, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(923, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(900, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(974, 60)
        Me.eZeeHeader.TabIndex = 6
        Me.eZeeHeader.Title = "Salary Change List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnDisApprove)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 486)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(974, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 8
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuGlobalAssign, Me.mnuImportSalaryChange, Me.mnuSalaryHistroy})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(203, 70)
        '
        'mnuGlobalAssign
        '
        Me.mnuGlobalAssign.Name = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Size = New System.Drawing.Size(202, 22)
        Me.mnuGlobalAssign.Text = "&Global Assign"
        '
        'mnuImportSalaryChange
        '
        Me.mnuImportSalaryChange.Name = "mnuImportSalaryChange"
        Me.mnuImportSalaryChange.Size = New System.Drawing.Size(202, 22)
        Me.mnuImportSalaryChange.Text = "&Import Salary Change"
        '
        'mnuSalaryHistroy
        '
        Me.mnuSalaryHistroy.Name = "mnuSalaryHistroy"
        Me.mnuSalaryHistroy.Size = New System.Drawing.Size(202, 22)
        Me.mnuSalaryHistroy.Tag = "mnuSalaryHistroy"
        Me.mnuSalaryHistroy.Text = "&Historical Salary Change"
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(126, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnApprove.TabIndex = 126
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        Me.btnApprove.Visible = False
        '
        'btnDisApprove
        '
        Me.btnDisApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisApprove.BackColor = System.Drawing.Color.White
        Me.btnDisApprove.BackgroundImage = CType(resources.GetObject("btnDisApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnDisApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDisApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnDisApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDisApprove.FlatAppearance.BorderSize = 0
        Me.btnDisApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDisApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisApprove.ForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDisApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.Location = New System.Drawing.Point(229, 13)
        Me.btnDisApprove.Name = "btnDisApprove"
        Me.btnDisApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDisApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnDisApprove.TabIndex = 125
        Me.btnDisApprove.Text = "&Disapprove"
        Me.btnDisApprove.UseVisualStyleBackColor = True
        Me.btnDisApprove.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(762, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(659, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(556, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(865, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 30
        '
        'objdgColhBlank
        '
        Me.objdgColhBlank.HeaderText = ""
        Me.objdgColhBlank.Name = "objdgColhBlank"
        Me.objdgColhBlank.ReadOnly = True
        Me.objdgColhBlank.Width = 30
        '
        'colhPayPeriod
        '
        Me.colhPayPeriod.HeaderText = "Pay Period"
        Me.colhPayPeriod.Name = "colhPayPeriod"
        Me.colhPayPeriod.ReadOnly = True
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.HeaderText = "Employee Name"
        Me.colhEmployeeName.Name = "colhEmployeeName"
        Me.colhEmployeeName.ReadOnly = True
        Me.colhEmployeeName.Width = 5
        '
        'colhIncrDate
        '
        Me.colhIncrDate.HeaderText = "Increment Date"
        Me.colhIncrDate.Name = "colhIncrDate"
        Me.colhIncrDate.ReadOnly = True
        Me.colhIncrDate.Width = 90
        '
        'colhActualDate
        '
        Me.colhActualDate.HeaderText = "Actual Date"
        Me.colhActualDate.Name = "colhActualDate"
        Me.colhActualDate.ReadOnly = True
        Me.colhActualDate.Width = 85
        '
        'colhScale
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colhScale.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhScale.HeaderText = "Current Scale"
        Me.colhScale.Name = "colhScale"
        Me.colhScale.ReadOnly = True
        Me.colhScale.Width = 110
        '
        'colhIncrementAmt
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.colhIncrementAmt.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhIncrementAmt.HeaderText = "Change Amount"
        Me.colhIncrementAmt.Name = "colhIncrementAmt"
        Me.colhIncrementAmt.ReadOnly = True
        Me.colhIncrementAmt.Width = 107
        '
        'colhNewScale
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colhNewScale.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhNewScale.HeaderText = "New Scale"
        Me.colhNewScale.Name = "colhNewScale"
        Me.colhNewScale.ReadOnly = True
        Me.colhNewScale.Width = 110
        '
        'objcolhAuto
        '
        Me.objcolhAuto.HeaderText = "objcolhAuto"
        Me.objcolhAuto.Name = "objcolhAuto"
        Me.objcolhAuto.ReadOnly = True
        Me.objcolhAuto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhAuto.Visible = False
        Me.objcolhAuto.Width = 5
        '
        'colhGradeGroup
        '
        DataGridViewCellStyle4.NullValue = Nothing
        Me.colhGradeGroup.DefaultCellStyle = DataGridViewCellStyle4
        Me.colhGradeGroup.HeaderText = "Grade Group"
        Me.colhGradeGroup.Name = "colhGradeGroup"
        Me.colhGradeGroup.ReadOnly = True
        Me.colhGradeGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhGradeGroup.Width = 76
        '
        'colhGrade
        '
        Me.colhGrade.HeaderText = "Grade"
        Me.colhGrade.Name = "colhGrade"
        Me.colhGrade.ReadOnly = True
        Me.colhGrade.Width = 70
        '
        'colhGradeLevel
        '
        Me.colhGradeLevel.HeaderText = "Grade Level"
        Me.colhGradeLevel.Name = "colhGradeLevel"
        Me.colhGradeLevel.ReadOnly = True
        Me.colhGradeLevel.Width = 73
        '
        'colhApprovePending
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colhApprovePending.DefaultCellStyle = DataGridViewCellStyle5
        Me.colhApprovePending.HeaderText = "Status"
        Me.colhApprovePending.Name = "colhApprovePending"
        Me.colhApprovePending.ReadOnly = True
        Me.colhApprovePending.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhApprovePending.Width = 40
        '
        'objcolhIsGroup
        '
        Me.objcolhIsGroup.HeaderText = "IsGroup"
        Me.objcolhIsGroup.Name = "objcolhIsGroup"
        Me.objcolhIsGroup.ReadOnly = True
        Me.objcolhIsGroup.Visible = False
        '
        'objcolhIncrementID
        '
        Me.objcolhIncrementID.HeaderText = "objcolhIncrementID"
        Me.objcolhIncrementID.Name = "objcolhIncrementID"
        Me.objcolhIncrementID.ReadOnly = True
        Me.objcolhIncrementID.Visible = False
        '
        'objcolhPeriodUnkId
        '
        Me.objcolhPeriodUnkId.HeaderText = "objcolhPeriodUnkId"
        Me.objcolhPeriodUnkId.Name = "objcolhPeriodUnkId"
        Me.objcolhPeriodUnkId.ReadOnly = True
        Me.objcolhPeriodUnkId.Visible = False
        '
        'objcolhEmployeeUnkId
        '
        Me.objcolhEmployeeUnkId.HeaderText = "objcolhEmployeeUnkId"
        Me.objcolhEmployeeUnkId.Name = "objcolhEmployeeUnkId"
        Me.objcolhEmployeeUnkId.ReadOnly = True
        Me.objcolhEmployeeUnkId.Visible = False
        '
        'objcolhGradegroupunkid
        '
        Me.objcolhGradegroupunkid.HeaderText = "objcolhGradegroupunkid"
        Me.objcolhGradegroupunkid.Name = "objcolhGradegroupunkid"
        Me.objcolhGradegroupunkid.ReadOnly = True
        Me.objcolhGradegroupunkid.Visible = False
        '
        'objcolhGradeunkid
        '
        Me.objcolhGradeunkid.HeaderText = "objcolhGradeunkid"
        Me.objcolhGradeunkid.Name = "objcolhGradeunkid"
        Me.objcolhGradeunkid.ReadOnly = True
        Me.objcolhGradeunkid.Visible = False
        '
        'objcolhGradelevelunkid
        '
        Me.objcolhGradelevelunkid.HeaderText = "objcolhGradelevelunkid"
        Me.objcolhGradelevelunkid.Name = "objcolhGradelevelunkid"
        Me.objcolhGradelevelunkid.ReadOnly = True
        Me.objcolhGradelevelunkid.Visible = False
        '
        'objcolhEmpCode
        '
        Me.objcolhEmpCode.HeaderText = "objcolhEmpCode"
        Me.objcolhEmpCode.Name = "objcolhEmpCode"
        Me.objcolhEmpCode.ReadOnly = True
        Me.objcolhEmpCode.Width = 5
        '
        'frmSalaryIncrementList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(974, 541)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSalaryIncrementList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Salary Change List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.pnlEdList.ResumeLayout(False)
        CType(Me.dgvSalaryIncrement, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPayYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccess As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents lblSalaryChangeApprovalStatus As System.Windows.Forms.Label
    Friend WithEvents cboSalaryChangeApprovalStatus As System.Windows.Forms.ComboBox
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnDisApprove As eZee.Common.eZeeLightButton
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuGlobalAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportSalaryChange As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuSalaryHistroy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlEdList As System.Windows.Forms.Panel
    Friend WithEvents dgvSalaryIncrement As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgColhBlank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPayPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIncrDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhActualDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhScale As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIncrementAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhNewScale As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAuto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhApprovePending As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIncrementID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhEmployeeUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhGradegroupunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhGradeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhGradelevelunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
