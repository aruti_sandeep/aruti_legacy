﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.Web
Imports System.IO

#End Region

Public Class frmSalaryIncrement_AddEdit

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmSalaryIncrement_AddEdit"
    Private mblnCancel As Boolean = True
    Private objSalaryInc As clsSalaryIncrement
    Private menAction As enAction = enAction.ADD_ONE
    Private mintSalaryIncrementtranUnkid As Integer = -1
    Private mdtPayPeriodStartDate As Date = Nothing
    Private mdtPayPeriodEndDate As Date = Nothing
    Private mintGradeGroupID As Integer = 0
    Private mintGradeID As Integer = 0
    Private mintGradeLevelID As Integer = 0
    Private mdecIncrementAmt As Decimal = 0
    Private mintInfoSalTrnHeadUnkid As Integer = -1
    Dim dicApprlNotification As New Dictionary(Of String, String)
    Private trd As Thread
    Private mblnLastIncrApproved As Boolean = False
    Private mintLastIncrUnkId As Integer = -1
    Private mintEmployeeUnkid As Integer = 0
    Private dsCurrJob As New DataSet
    Private mintMappedGradeUnkid As Integer = 0
    'Sohail (06 Dec 2019) -- Start
    'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
    Private mblnFirstSalaryScale As Boolean = False
    'Sohail (06 Dec 2019) -- End
    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private mstrSearchText As String = ""
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = 0, Optional ByVal blnFirstSalaryScale As Boolean = False) As Boolean
        'Sohail (06 Dec 2019) - [blnFirstSalaryScale]
        Try
            mintSalaryIncrementtranUnkid = intUnkId
            menAction = eAction
            mintEmployeeUnkid = intEmployeeUnkid
            If mintEmployeeUnkid > 0 Then
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If
            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            mblnFirstSalaryScale = blnFirstSalaryScale
            'Sohail (06 Dec 2019) -- End

            Me.ShowDialog()

            intUnkId = mintSalaryIncrementtranUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPayYear.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            txtCurrentScale.BackColor = GUI.ColorComp
            txtIncrementAmount.BackColor = GUI.ColorComp
            txtPercentage.BackColor = GUI.ColorComp
            txtNewScale.BackColor = GUI.ColorComp
            dtpIncrementdate.BackColor = GUI.ColorOptional
            cboReason.BackColor = GUI.ColorComp
            cboIncrementBy.BackColor = GUI.ColorComp
            txtNewScaleGrade.BackColor = GUI.ColorComp
            cboChangeType.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objPeriod As New clscommom_period_Tran
        Try
            With objSalaryInc
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = ._Periodunkid
                cboPayYear.SelectedValue = objPeriod._Yearunkid
                cboPayPeriod.SelectedValue = ._Periodunkid
                chkChangeGrade.Checked = objSalaryInc._Isgradechange
                cboEmployee.SelectedValue = ._Employeeunkid
                If ._Incrementdate = Nothing Then
                    dtpIncrementdate.Value = Now.Date
                Else
                    dtpIncrementdate.Value = ._Incrementdate
                End If
                txtCurrentScale.Text = Format(._Currentscale, GUI.fmtCurrency)
                txtIncrementAmount.Text = Format(._Increment, GUI.fmtCurrency)
                txtNewScale.Text = Format(._Newscale, GUI.fmtCurrency)
                If txtCurrentScale.Decimal > 0 Then
                    txtPercentage.Text = IIf(txtCurrentScale.Decimal = 0, "0.00", Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, "00.00####")).ToString
                Else
                    txtPercentage.Text = Format(0, GUI.fmtCurrency)
                End If
                cboReason.SelectedValue = ._Reason_Id
                cboIncrementBy.SelectedValue = objSalaryInc._Increment_Mode
                txtPercentage.Text = Format(objSalaryInc._Percentage, GUI.fmtCurrency)
                If objSalaryInc._Isgradechange = True Then
                    txtNewScaleGrade.Text = Format(._Newscale, GUI.fmtCurrency)
                Else
                    txtNewScaleGrade.Text = Format(0, GUI.fmtCurrency)
                End If
                If menAction = enAction.EDIT_ONE Then
                    cboGradeGroup.SelectedValue = objSalaryInc._Gradegroupunkid
                    cboGrade.SelectedValue = objSalaryInc._Gradeunkid
                    cboGradeLevel.SelectedValue = objSalaryInc._Gradelevelunkid
                End If
                cboChangeType.SelectedValue = ._Changetypeid
                If ._Promotion_date <> Nothing Then
                    dtpPromotionDate.Value = ._Promotion_date
                    dtpPromotionDate.Checked = True
                End If
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                If ._ActualDate = Nothing Then
                    dtpActualDate.Value = Now.Date
                Else
                    dtpActualDate.Value = ._ActualDate
                End If
                'Sohail (21 Jan 2020) -- End
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub SetValue()
        Try
            With objSalaryInc
                ._Periodunkid = CInt(cboPayPeriod.SelectedValue)
                ._Employeeunkid = CInt(cboEmployee.SelectedValue)
                ._Incrementdate = dtpIncrementdate.Value
                ._Currentscale = txtCurrentScale.Decimal
                If chkChangeGrade.Checked = True Then
                    ._Increment = txtNewScaleGrade.Decimal - txtCurrentScale.Decimal
                    ._Newscale = txtNewScaleGrade.Decimal
                Else
                    ._Increment = txtIncrementAmount.Decimal
                    ._Newscale = txtNewScale.Decimal
                End If
                ._Gradegroupunkid = CInt(cboGradeGroup.SelectedValue)
                ._Gradeunkid = CInt(cboGrade.SelectedValue)
                ._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
                ._Isgradechange = chkChangeGrade.Checked
                ._Userunkid = User._Object._Userunkid
                ._Reason_Id = CInt(cboReason.SelectedValue)
                ._Increment_Mode = CInt(cboIncrementBy.SelectedValue)
                If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
                    ._Percentage = txtPercentage.Decimal
                Else
                    ._Percentage = 0
                End If
                'If menAction <> enAction.EDIT_ONE Then 'Sohail (21 Jan 2020)
                    If User._Object.Privilege._AllowToApproveSalaryChange = True Then
                        ._Isapproved = True
                        ._Approveruserunkid = User._Object._Userunkid
                    Else
                        ._Isapproved = False
                    End If
                'End If 'Sohail (21 Jan 2020)
                If mintInfoSalTrnHeadUnkid > 0 Then
                    ._InfoSalHeadUnkid = mintInfoSalTrnHeadUnkid
                    ._IsCopyPrevoiusSLAB = chkCopyPreviousEDSlab.Checked
                    ._IsOverwritePrevoiusSLAB = chkOverwritePrevEDSlabHeads.Checked
                End If
                ._Changetypeid = CInt(cboChangeType.SelectedValue)
                If dtpPromotionDate.Checked = True Then
                    ._Promotion_date = dtpPromotionDate.Value
                End If
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                ._ActualDate = dtpActualDate.Value
                If menAction <> enAction.EDIT_ONE Then
                    ._Arrears_Amount = 0
                    ._EMI_Amount = 0
                    ._NoOfInstallment = 1
                    ._Arrears_Countryid = Company._Object._Localization_Country
                    ._Approval_StatusUnkid = enApprovalStatus.PENDING
                    ._FinalApproverUnkid = -1
                    ._Arrears_StatusUnkid = 0 'enLoanStatus.IN_PROGRESS
                End If
                If menAction <> enAction.EDIT_ONE OrElse eZeeDate.convertDate(._ActualDate) <> eZeeDate.convertDate(dtpActualDate.Value) Then
                    Dim objMaster As New clsMasterData
                    Dim intActualDatePeriod As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpActualDate.Value, 0, 0, True, False, Nothing, True, True)
                    If intActualDatePeriod <> CInt(cboPayPeriod.SelectedValue) Then
                        Dim decDiff As Decimal = 0
                        If chkChangeGrade.Checked = True Then
                            decDiff = CDec(txtNewScaleGrade.Decimal) - CDec(txtLatestScale.Decimal)
                        Else
                            decDiff = CDec(txtNewScale.Decimal) - CDec(txtLatestScale.Decimal)
                        End If

                        Dim intMonths As Integer = 0
                        Dim objPeriod As New clscommom_period_Tran
                        Dim dsPeriod As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False, 0, False)
                        Dim dtTable As DataTable = New DataView(dsPeriod.Tables(0), "start_date >= " & eZeeDate.convertDate(dtpActualDate.Value.Date) & " AND start_date < " & eZeeDate.convertDate(mdtPayPeriodStartDate) & " ", "", DataViewRowState.CurrentRows).ToTable
                        intMonths = dtTable.Rows.Count
                        Dim decArrears As Decimal = decDiff * intMonths
                        If decDiff <> ._Arrears_Amount Then


                            ._Arrears_Amount = decDiff * intMonths
                            ._EMI_Amount = decDiff * intMonths
                            ._NoOfInstallment = 1
                            ._Approval_StatusUnkid = enApprovalStatus.PENDING
                            ._FinalApproverUnkid = -1
                            ._Arrears_StatusUnkid = 0 'enLoanStatus.IN_PROGRESS
                        End If
                    End If
                End If
                'Sohail (21 Jan 2020) -- End
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet = Nothing
        Dim objGradeGroup As New clsGradeGroup
        Dim objCommon As New clsCommon_Master
        Try
            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "PayYear", True)
            With cboPayYear
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayYear")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objGradeGroup.getComboList("GradeGroup", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("GradeGroup")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
            With cboReason
                .BeginUpdate()
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("SalReason")
                .SelectedValue = 0
                .EndUpdate()
            End With

            dsCombo = objMaster.getComboListSalaryIncrementBy(True, "IncrMode")
            With cboIncrementBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("IncrMode")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListForSalaryChangeType("List")
            With cboChangeType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = enSalaryChangeType.SIMPLE_SALARY_CHANGE
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objEmployee = Nothing
            objGradeGroup = Nothing
            objCommon = Nothing
        End Try

    End Sub

    Private Sub GetScale()
        Dim objWages As New clsWagesTran
        Dim dsList As DataSet = Nothing
        Try
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), "Scale")
            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), mdtPayPeriodEndDate, "Scale")
            If mdtPayPeriodEndDate = Nothing Then
                dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), dtpIncrementdate.Value.Date, "Scale")
            Else
            dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), mdtPayPeriodEndDate, "Scale")
            End If
            'Sohail (06 Dec 2019) -- End
            'Sohail (27 Apr 2016) -- End
            If dsList.Tables("Scale").Rows.Count > 0 Then
                txtBasicScale.Text = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), GUI.fmtCurrency)
                txtMaximum.Text = Format(dsList.Tables("Scale").Rows(0).Item("Maximum"), GUI.fmtCurrency)
                txtIncrementAmount.Text = Format(dsList.Tables("Scale").Rows(0).Item("Increment"), GUI.fmtCurrency)
                mdecIncrementAmt = txtIncrementAmount.Decimal
                txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency)
            Else
                txtBasicScale.Text = Format(0, GUI.fmtCurrency)
                txtMaximum.Text = Format(0, GUI.fmtCurrency)
                txtIncrementAmount.Text = Format(0, GUI.fmtCurrency)
                txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency)
                mdecIncrementAmt = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetScale", mstrModuleName)
        Finally
            objWages = Nothing
        End Try
    End Sub

    Private Sub ResetControls()
        Try
            If chkChangeGrade.Checked = True Then
                cboGradeLevel.Enabled = True
                txtIncrementAmount.Enabled = False
                txtNewScale.Enabled = False
                txtIncrementAmount.Text = Format(mdecIncrementAmt, GUI.fmtCurrency)
                txtPercentage.Enabled = False
                txtNewScaleGrade.Text = Format(0, GUI.fmtCurrency)
                txtNewScaleGrade.Enabled = True
                txtPercentage.Text = Format(0, GUI.fmtCurrency)
                cboIncrementBy.SelectedValue = 0
                cboIncrementBy.Enabled = False
                If mintMappedGradeUnkid > 0 Then
                    objbtnAddGrade.Enabled = False
                    objbtnAddGroup.Enabled = False
                    cboGradeGroup.Enabled = False
                    cboGrade.Enabled = False
                Else
                    cboGradeGroup.Enabled = True
                    cboGrade.Enabled = True
                    objbtnAddGrade.Enabled = True
                    objbtnAddGroup.Enabled = True
                End If
                objbtnAddGLevel.Enabled = True
                objbtnAddScale.Enabled = True
            Else
                cboGradeGroup.Enabled = False
                cboGrade.Enabled = False
                cboGradeLevel.Enabled = False
                txtIncrementAmount.Enabled = True
                txtNewScale.Enabled = True
                txtPercentage.Enabled = True
                cboIncrementBy.Enabled = True
                txtNewScaleGrade.Text = Format(0, GUI.fmtCurrency)
                txtNewScaleGrade.Enabled = False
                cboGradeGroup.SelectedValue = mintGradeGroupID
                cboGrade.SelectedValue = mintGradeID
                cboGradeLevel.SelectedValue = mintGradeLevelID
                objbtnAddGrade.Enabled = False
                objbtnAddGroup.Enabled = False
                objbtnAddGLevel.Enabled = False
                objbtnAddScale.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetControls", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCurrentScale()
        Dim objEmpMaster As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing

        Try
            objEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            mintGradeGroupID = objEmpMaster._Gradegroupunkid
            mintGradeID = objEmpMaster._Gradeunkid
            mintGradeLevelID = objEmpMaster._Gradelevelunkid
            txtPercentage.Text = Format(0, GUI.fmtCurrency)

            '*** For Current Scale
            dsList = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), mdtPayPeriodEndDate)
            If dsList.Tables("Salary").Rows.Count > 0 Then
                txtCurrentScale.Text = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), GUI.fmtCurrency)
                mintGradeGroupID = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
                mintGradeID = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
                mintGradeLevelID = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
                cboGradeGroup.SelectedValue = mintGradeGroupID
                cboGrade.SelectedValue = mintGradeID
                cboGradeLevel.SelectedValue = mintGradeLevelID
            Else
                cboGradeGroup.SelectedValue = mintGradeGroupID
                cboGrade.SelectedValue = mintGradeID
                cboGradeLevel.SelectedValue = mintGradeLevelID
                txtCurrentScale.Text = txtBasicScale.Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCurrentScale", mstrModuleName)
        Finally
            objEmpMaster = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Function Set_Notification_Approvals(ByVal StrUserName As String, ByVal intUserID As Integer, ByVal intSalaryincrementtranunkid As Integer) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim objEmployee As New clsEmployee_Master
        Try
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 33, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that following changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 34, "This is to inform you that following changes have been made for employee") & " <b>" & getTitleCase(objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 35, "with employeecode") & " " & "<b>" & objEmployee._Employeecode & "</b>." & " " & Language.getMessage(mstrModuleName, 36, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 37, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage(mstrModuleName, 38, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TABLE border = '1' WIDTH = '80%'>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TR WIDTH = '80%' bgcolor= 'SteelBlue'>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 14, "Period") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 15, "Date") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 16, "Old Scale") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 17, "New Scale") & "</span></b></TD>")
            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 18, "Reason") & "</span></b></TD>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("</TR>")
            StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & cboPayPeriod.Text & "</span></TD>")
            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dtpIncrementdate.Value.Date & "</span></TD>")
            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(txtCurrentScale.Text), GUI.fmtCurrency) & "</span></TD>")
            If chkChangeGrade.Checked = False Then
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(txtNewScale.Text), GUI.fmtCurrency) & "</span></TD>")
            Else
                StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(txtNewScaleGrade.Text), GUI.fmtCurrency) & "</span></TD>")
            End If
            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & cboReason.Text & "</span></TD>")
            StrMessage.Append("</TR>" & vbCrLf)
            StrMessage.Append("</TABLE>")
            Dim strLink As String
            strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPg_SalaryIncrementList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid.ToString & "|" & intUserID & "|" & CInt(cboEmployee.SelectedValue).ToString & "|" & CInt(cboPayPeriod.SelectedValue).ToString & "|0"))
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please click on the following link to Approve/Reject this change.")& "</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 39, "Please click on the following link to Approve/Reject this change.") & "</span></p>")

            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")
            StrMessage.Append("<a href='" & strLink & "'>" & strLink & "</a>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<br><br>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")

            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            Return StrMessage.ToString

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
            Return ""
        Finally
            objEmployee = Nothing
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If User._Object.Privilege._AllowToApproveSalaryChange = False Then
                If dicApprlNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicApprlNotification.Keys
                        objSendMail._ToEmail = sKey
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 22, "Notifications to Approve Employee Salary Change.")
                        objSendMail._Message = dicApprlNotification(sKey)
                        'objSendMail._FormName = "" 'Please pass Form Name for WEB
                        'objSendMail._LoginEmployeeunkid = -1
                        With objSendMail
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                        objSendMail._UserUnkid = User._Object._Userunkid
                        objSendMail._SenderAddress = User._Object._Email
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Dim objTnA As New clsTnALeaveTran
        Dim objPayment As New clsPayment_tran
        Try
            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'If CInt(cboPayPeriod.SelectedValue) <= 0 Then
            Dim intCurrentPeriodId As Integer = CInt(cboPayPeriod.SelectedValue)
            If intCurrentPeriodId <= 0 AndAlso mblnFirstSalaryScale = True Then
                Dim objMaster As New clsMasterData
                intCurrentPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpIncrementdate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True, False, Nothing, False, True)
                If intCurrentPeriodId > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intCurrentPeriodId
                    mdtPayPeriodStartDate = objPeriod._Start_Date
                    mdtPayPeriodEndDate = objPeriod._End_Date
                End If
            End If
            If mblnFirstSalaryScale = False AndAlso CInt(cboPayPeriod.SelectedValue) <= 0 Then
                'Sohail (06 Dec 2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Employee. Employee is mandatory information."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
                'Sohail (06 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            ElseIf dtpIncrementdate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, Increment date is mandatory information. Please provide Increment date to continue."), enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Return False
                'Sohail (06 Feb 2019) -- End
            ElseIf dtpIncrementdate.Value.Date > mdtPayPeriodEndDate OrElse dtpIncrementdate.Value.Date < mdtPayPeriodStartDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Increment Date should be in between ") & mdtPayPeriodStartDate & Language.getMessage(mstrModuleName, 13, " And ") & mdtPayPeriodEndDate & ".", enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Return False
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            ElseIf dtpActualDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Sorry, Actual date is mandatory information. Please provide Actual date to continue."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
            ElseIf txtLastIncrementDate.Text.Trim = "" AndAlso (dtpActualDate.Value.Date > mdtPayPeriodEndDate OrElse dtpActualDate.Value.Date < mdtPayPeriodStartDate) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 44, "Actual Date should be in between ") & mdtPayPeriodStartDate & Language.getMessage(mstrModuleName, 13, " And ") & mdtPayPeriodEndDate & ".", enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
            ElseIf dtpActualDate.Value.Date > mdtPayPeriodEndDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 45, "Actual Date should not greater than selected period end date."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
            ElseIf eZeeDate.convertDate(dtpActualDate.Value.Date) > eZeeDate.convertDate(dtpIncrementdate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "Actual Date should not be greater than Increment Date."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
            ElseIf menAction = enAction.EDIT_ONE AndAlso objSalaryInc._Approval_StatusUnkid > 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Sorry, You cannot change this Salary. Reason: Arrears approval is in progress."), enMsgBoxStyle.Information)
                dtpActualDate.Focus()
                Return False
                'Sohail (21 Jan 2020) -- End
            End If

            If chkChangeGrade.Checked = False Then 'Grade not changed
                If CInt(cboIncrementBy.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Please select Increment By. Increment By is mandatory information."), enMsgBoxStyle.Information)
                    cboIncrementBy.Focus()
                    'S.SANDEEP [21 JUN 2016] -- START
                    'Exit Try
                    Return False
                    'S.SANDEEP [21 JUN 2016] -- END
                End If

                If txtIncrementAmount.Decimal = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Enter Increment Amount. Increment Amount is mandatory information."), enMsgBoxStyle.Information)
                    txtIncrementAmount.Focus()
                    'S.SANDEEP [21 JUN 2016] -- START
                    'Exit Try
                    Return False
                    'S.SANDEEP [21 JUN 2016] -- END
                ElseIf txtNewScale.Decimal > txtMaximum.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, New Scale should not be greater than Maximum Scale."), enMsgBoxStyle.Information)
                    txtIncrementAmount.Focus()
                    'S.SANDEEP [21 JUN 2016] -- START
                    'Exit Try
                    Return False
                    'S.SANDEEP [21 JUN 2016] -- END
                End If
            Else 'Grade Changed
                If CInt(cboGradeGroup.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Grade Group. Grade Group is mandatory information."), enMsgBoxStyle.Information)
                    cboGradeGroup.Focus()
                    Return False
                ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Grade. Grade is mandatory information."), enMsgBoxStyle.Information)
                    cboGrade.Focus()
                    Return False
                ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Grade Level. Grade Level is mandatory information."), enMsgBoxStyle.Information)
                    cboGradeLevel.Focus()
                    Return False
                ElseIf txtNewScaleGrade.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please Enter New Scale. New Scale is mandatory information."), enMsgBoxStyle.Information)
                    txtNewScaleGrade.Focus()
                    Return False
                ElseIf txtNewScaleGrade.Decimal < txtBasicScale.Decimal OrElse txtNewScaleGrade.Decimal > txtMaximum.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, New Scale should be in between Minumum Scale and Maximum Scale."), enMsgBoxStyle.Information)
                    txtNewScaleGrade.Focus()
                    Return False
                ElseIf menAction <> enAction.EDIT_ONE Then
                    Dim objMaster As New clsMasterData
                    Dim dsList As DataSet = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), dtpIncrementdate.Value.Date)
                    If dsList.Tables("Salary").Rows.Count > 0 Then
                        If CInt(cboGradeLevel.SelectedValue) = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please set different Grade Level."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If
                    End If

                End If
            End If

            If mblnLastIncrApproved = False AndAlso mintLastIncrUnkId <> mintSalaryIncrementtranUnkid Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, Last Increment is not Approved yet. Please Approve Last Increment."), enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Return False
            End If

            If txtLastIncrementDate.Text.Trim <> "" Then
                If dtpIncrementdate.Value < CDate(txtLastIncrementDate.Text) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Increment Date should be greater than the Last Increment Date. Please set New Increment Date higher than the Last Increment Date."), enMsgBoxStyle.Information)
                    dtpIncrementdate.Focus()
                    Return False
                End If
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                If menAction <> enAction.EDIT_ONE AndAlso eZeeDate.convertDate(dtpActualDate.Value.Date) < eZeeDate.convertDate(CDate(txtLastIncrementDate.Text)) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Actual Date should be greater than the Last Increment Date. Please set New Actual Date higher than the Last Increment Date."), enMsgBoxStyle.Information)
                    dtpActualDate.Focus()
                    Return False
                End If
                If menAction = enAction.EDIT_ONE Then
                    Dim dsList As DataSet = objSalaryInc.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "Incr", Nothing, Convert.ToInt32(cboEmployee.SelectedValue).ToString, , , "prsalaryincrement_tran.incrementdate DESC")

                    If dsList.Tables("Incr").Rows.Count > 1 Then
                        If eZeeDate.convertDate(dtpActualDate.Value.Date) < dsList.Tables("Incr").Rows(1).Item("incrementdate").ToString Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Actual Date should be greater than the Last Increment Date. Please set New Actual Date higher than the Last Increment Date."), enMsgBoxStyle.Information)
                            dtpActualDate.Focus()
                            Return False
                        End If
                    End If
                End If
                'Sohail (21 Jan 2020) -- End
            End If

            Dim intTnAID As Integer
            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(cboEmployee.SelectedValue), CInt(cboPayPeriod.SelectedValue))
            intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(cboEmployee.SelectedValue), intCurrentPeriodId)
            'Sohail (06 Dec 2019) -- End
            If intTnAID > 0 Then
                If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open global void payment screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Payment of Salary is already done for this period. Please Void Payment for selected employee to give salary change."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Payment of Salary is already done for this period. Please Void Payment for selected employee to give salary change.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 41, "Do you want to void Payment?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmGlobalVoidPayment
                        objFrm.displayDialog(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT)
                    End If
                    'Sohail (19 Apr 2019) -- End
                    cboPayPeriod.Focus()
                    'S.SANDEEP [21 JUN 2016] -- START
                    'Exit Try
                    Return False
                    'S.SANDEEP [21 JUN 2016] -- END
                End If
            End If

            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'If objTnA.IsPayrollProcessDone(CInt(cboPayPeriod.SelectedValue), cboEmployee.SelectedValue.ToString, mdtPayPeriodEndDate) = True Then
            If objTnA.IsPayrollProcessDone(intCurrentPeriodId, cboEmployee.SelectedValue.ToString, mdtPayPeriodEndDate) = True Then
                'Sohail (06 Dec 2019) -- End
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to give salary change."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to give salary change.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 40, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                'S.SANDEEP [21 JUN 2016] -- START
                'Exit Try
                Return False
                'S.SANDEEP [21 JUN 2016] -- END
            End If

            If chkChangeGrade.Checked = True AndAlso txtCurrentScale.Decimal > txtNewScaleGrade.Decimal Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "New Basic Scale is less than Current Scale. Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            End If

            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'If CInt(cboReason.SelectedValue) <= 0 Then
            If mblnFirstSalaryScale = False AndAlso CInt(cboReason.SelectedValue) <= 0 Then
                'Sohail (06 Dec 2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Please select Reason. Reason is mandatory information. "), enMsgBoxStyle.Information)
                cboReason.Focus()
                Return False
            End If

            If menAction <> enAction.EDIT_ONE AndAlso chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 25, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            End If

            If mintMappedGradeUnkid > 0 Then
                If CInt(cboGrade.SelectedValue) <> mintMappedGradeUnkid Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, The job title assigned to this employee does not fall within the salary grade you given to the employee, please correct this before completing this salary change using re-categorization screen."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            If CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION Then
                If dtpPromotionDate.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, Promotion date is mandatory information. Please provide promotion date to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            'Sohail (06 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            If dtpIncrementdate.Value.Date <> mdtPayPeriodStartDate Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Increment date is not set as selected period start date, So the salary will be prorated as per increment date.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 32, "Are you sure you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    dtpIncrementdate.Focus()
                    Return False
                End If
            End If
            'Sohail (06 Feb 2019) -- End

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            If dtpActualDate.Value.Date < mdtPayPeriodStartDate Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 48, "Actual date is set in past period, Which may cause salary arrears.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 32, "Are you sure you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    dtpActualDate.Focus()
                    Return False
                End If
            End If
            'Sohail (21 Jan 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
            objTnA = Nothing
            objPayment = Nothing
        End Try
        Return True
    End Function

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            If CInt(cbo.SelectedValue) <= 0 Then Exit Sub
            mstrSearchText = Language.getMessage(mstrModuleName, 42, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Jan 2020) -- End


    'S.SANDEEP |05-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-933,934
    Private Function SaveAttachment(ByVal _data As DataTable) As Boolean
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            Dim strFileName As String
            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim mstrFolderName As String = ""
            Dim strError As String = ""

            mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.SALARY) Select (p.Item("Name").ToString)).FirstOrDefault
            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                strPath += "/"
            End If

            For Each dRow As DataRow In _data.Rows

                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))

                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Return False
                        Else
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath & "uploadimage/" & mstrFolderName & "/" + strFileName
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strDocLocalPath


                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 500, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    strFileName = dRow("fileuniquename").ToString

                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Return False
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If File.Exists(strDocLocalPath) Then
                                File.Delete(strDocLocalPath)
                            End If
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 500, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                End If
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveAttachment", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Function
    'S.SANDEEP |0-JUN-2023| -- END

#End Region

#Region " ComboBox's Events "

    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing

        Try
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open)
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim objEmpMaster As New clsEmployee_Master
        Dim dsList As DataSet = Nothing
        Dim objLastIncrementDate As Date = Nothing
        Try
            Call SetCurrentScale()

            If CInt(cboEmployee.SelectedValue) > 0 Then

                mnuBenefits.Enabled = True
                mnuRecategorization.Enabled = True

                Dim objRecatgorize As New clsemployee_categorization_Tran
                dsCurrJob = objRecatgorize.Get_Current_Job(Now.Date, CInt(cboEmployee.SelectedValue))
                objRecatgorize = Nothing
                If dsCurrJob.Tables(0).Rows.Count > 0 Then
                    mintMappedGradeUnkid = CInt(dsCurrJob.Tables(0).Rows(0).Item("gradeunkid"))
                End If

                'S.SANDEEP |18-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'If mintMappedGradeUnkid > 0 Then
                '    Call ResetControls()
                '    'S.SANDEEP [22-NOV-2018] -- START
                '    Dim objGrd As New clsGrade : Dim objGrdLvl As New clsGradeLevel
                '    objGrd._Gradeunkid = mintMappedGradeUnkid
                '    Dim dsLevel As New DataSet
                '    dsLevel = objGrdLvl.getComboList("List", False, objGrd._Gradeunkid)
                '    Dim intLevelId As Integer = 0
                '    If dsLevel.Tables(0).Rows.Count > 0 Then
                '        intLevelId = CInt(dsLevel.Tables(0).Rows(0)("gradelevelunkid"))
                '    End If
                '    cboGradeGroup.SelectedValue = objGrd._Gradegroupunkid
                '    cboGrade.SelectedValue = objGrd._Gradeunkid
                '    cboGradeLevel.SelectedValue = intLevelId
                '    objGrd = Nothing : objGrdLvl = Nothing
                '    dsLevel.Dispose()
                '    'S.SANDEEP [22-NOV-2018] -- END
                'End If

                Call ResetControls()

                If mintMappedGradeUnkid > 0 Then
                    'S.SANDEEP [22-NOV-2018] -- START
                    Dim objGrd As New clsGrade : Dim objGrdLvl As New clsGradeLevel
                    objGrd._Gradeunkid = mintMappedGradeUnkid
                    Dim dsLevel As New DataSet
                    dsLevel = objGrdLvl.getComboList("List", False, objGrd._Gradeunkid)
                    Dim intLevelId As Integer = 0
                    If dsLevel.Tables(0).Rows.Count > 0 Then
                        intLevelId = CInt(dsLevel.Tables(0).Rows(0)("gradelevelunkid"))
                    End If
                    cboGradeGroup.SelectedValue = objGrd._Gradegroupunkid
                    cboGrade.SelectedValue = objGrd._Gradeunkid
                    cboGradeLevel.SelectedValue = intLevelId
                    objGrd = Nothing : objGrdLvl = Nothing
                    dsLevel.Dispose()
                    'S.SANDEEP [22-NOV-2018] -- END
                End If
                'S.SANDEEP |18-FEB-2019| -- END

                

            End If

            objEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

            dsList = objSalaryInc.getLastIncrement("Incr", objEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)), True)

            If dsList.Tables("Incr").Rows.Count > 0 Then
                objLastIncrementDate = eZeeDate.convertDate(dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString)
                txtLastIncrementDate.Text = objLastIncrementDate.ToShortDateString
                txtLatestScale.Text = Format(dsList.Tables("Incr").Rows(0).Item("newscale"), GUI.fmtCurrency)
                mblnLastIncrApproved = CBool(dsList.Tables("Incr").Rows(0).Item("isapproved"))
                mintLastIncrUnkId = CInt(dsList.Tables("Incr").Rows(0).Item("salaryincrementtranunkid"))
            Else
                txtLastIncrementDate.Text = ""
                txtLatestScale.Text = Format(objEmpMaster._Scale, GUI.fmtCurrency)
                mblnLastIncrApproved = True
            End If

            If objEmpMaster._Tranhedunkid > 0 Then
                Dim objTHead As New clsTransactionHead
                objTHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = objEmpMaster._Tranhedunkid
                If objTHead._Trnheadtype_Id = enTranHeadType.Informational Then
                    chkCopyPreviousEDSlab.Enabled = True : chkCopyPreviousEDSlab.Checked = True
                    chkOverwritePrevEDSlabHeads.Enabled = True
                    mintInfoSalTrnHeadUnkid = objEmpMaster._Tranhedunkid
                Else
                    chkCopyPreviousEDSlab.Enabled = False : chkCopyPreviousEDSlab.Checked = False
                    chkOverwritePrevEDSlabHeads.Enabled = False : chkOverwritePrevEDSlabHeads.Checked = False
                End If
                objTHead = Nothing
            Else
                chkCopyPreviousEDSlab.Enabled = False : chkCopyPreviousEDSlab.Checked = False
                chkOverwritePrevEDSlabHeads.Enabled = False : chkOverwritePrevEDSlabHeads.Checked = False
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
            'Sohail (21 Jan 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmpMaster = Nothing
            'objGradeGroup = Nothing
            'objGrade = Nothing
            'objGradeLevel = Nothing
            'objWages = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Dim strEmployeeList As String = ""
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            'Sohail (06 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Set Increment date selection mandatory and Notify user if Salary will be prorated.
            If CInt(cboPayPeriod.SelectedValue) > 0 AndAlso menAction <> enAction.EDIT_ONE Then
                dtpIncrementdate.Value = mdtPayPeriodStartDate.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                dtpIncrementdate.Checked = False
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dtpActualDate.Checked = dtpIncrementdate.Checked
                'Sohail (21 Jan 2020) -- End
            End If
            'Sohail (06 Feb 2019) -- End

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "EmployeeList", True)

            If strEmployeeList.Length > 0 AndAlso menAction <> enAction.EDIT_ONE Then
                dtTable = New DataView(dsCombo.Tables("EmployeeList"), "employeeunkid NOT IN (" & strEmployeeList & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables("EmployeeList")).ToTable
            End If

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                If CInt(cboPayPeriod.SelectedValue) > 0 Then
                    If mintEmployeeUnkid > 0 Then
                        .SelectedValue = mintEmployeeUnkid
                    End If
                End If
            End With
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            Call SetDefaultSearchText(cboEmployee)
            'Sohail (21 Jan 2020) -- End

            Call SetCurrentScale()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName) 'Sohail (11 May 2011)
        Finally
            objPeriod = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Dim objGrade As New clsGrade
        Dim dsCombo As DataSet
        Try
            dsCombo = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Grade")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objGrade = Nothing
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim objGradeLevel As New clsGradeLevel
        Dim dsCombo As DataSet
        Try
            dsCombo = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("GradeLevel")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Try
            Call GetScale()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboIncrementBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIncrementBy.SelectedIndexChanged
        Try
            txtPercentage.Text = Format(0, GUI.fmtCurrency)
            txtIncrementAmount.ReadOnly = False
            txtPercentage.ReadOnly = False
            'Sohail (15 Dec 2018) -- Start
            'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
            txtNewScale.ReadOnly = True
            'Sohail (15 Dec 2018) -- End
            Select Case CInt(cboIncrementBy.SelectedValue)
                Case enSalaryIncrementBy.Grade
                    txtIncrementAmount.ReadOnly = True
                    txtPercentage.ReadOnly = True
                    Call GetScale()
                Case enSalaryIncrementBy.Percentage
                    txtIncrementAmount.ReadOnly = True
                Case enSalaryIncrementBy.Amount
                    txtPercentage.ReadOnly = True
                    'Sohail (15 Dec 2018) -- Start
                    'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
                    txtNewScale.ReadOnly = False
                    'Sohail (15 Dec 2018) -- End
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboIncrementBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboChangeType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChangeType.SelectedIndexChanged
        Try
            If CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION Then
                dtpPromotionDate.Enabled = True
                'S.SANDEEP [17 SEP 2016] -- START
                'chkChangeGrade.Enabled = True
                'S.SANDEEP [17 SEP 2016] -- END
                'cboGradeGroup.Enabled = True : objbtnAddGroup.Enabled = True
                'cboGrade.Enabled = True : objbtnAddGrade.Enabled = True
                'cboGradeLevel.Enabled = True : objbtnAddGLevel.Enabled = True
            Else
                dtpPromotionDate.Checked = False
                dtpPromotionDate.Enabled = False
                'S.SANDEEP [17 SEP 2016] -- START
                'chkChangeGrade.Enabled = False
                'chkChangeGrade.Checked = False
                'S.SANDEEP [17 SEP 2016] -- END
                
                'cboGradeGroup.Enabled = False : objbtnAddGroup.Enabled = False
                'cboGrade.Enabled = False : objbtnAddGrade.Enabled = False
                'cboGradeLevel.Enabled = False : objbtnAddGLevel.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChangeType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If CInt(.SelectedValue) <= 0 Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Jan 2020) -- End
#End Region

#Region " Form's Event "

    Private Sub frmSalaryIncrement_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSalaryInc = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryIncrement_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryIncrement_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
                Case Keys.S
                    If e.Control = True Then
                        btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryIncrement_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryIncrement_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSalaryInc = New clsSalaryIncrement
        Try
            Call Set_Logo(Me, gApplicationType)
            mnuBenefits.Enabled = False
            mnuRecategorization.Enabled = False
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objSalaryInc._Salaryincrementtranunkid = mintSalaryIncrementtranUnkid
                cboPayYear.Enabled = False
                cboPayPeriod.Enabled = False
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                chkChangeGrade.Enabled = False
            End If
            Call ResetControls()
            Call GetValue()
            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'cboPayYear.Focus()
            If cboPayPeriod.Enabled = True Then cboPayPeriod.Focus()
            If menAction = enAction.EDIT_ONE AndAlso mblnFirstSalaryScale = True Then
                'txtCurrentScale.ReadOnly = False
                dtpIncrementdate.Enabled = False
                cboChangeType.Enabled = False
                btnOperations.Enabled = False
                cboReason.Enabled = False
                objbtnAddReason.Enabled = False

                cboGradeGroup.Enabled = False
                cboGrade.Enabled = False
                cboGradeLevel.Enabled = False
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dtpActualDate.Enabled = False
                objbtnAddGroup.Enabled = False
                objbtnAddGrade.Enabled = False
                objbtnAddGLevel.Enabled = False
                'Sohail (21 Jan 2020) -- End
            Else
                'txtCurrentScale.ReadOnly = True
                dtpIncrementdate.Enabled = True
                cboChangeType.Enabled = True
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dtpActualDate.Enabled = True
                'Sohail (21 Jan 2020) -- End
            End If
            'Sohail (06 Dec 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryIncrement_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSalaryIncrement.SetMessages()
            objfrm._Other_ModuleNames = "clsSalaryIncrement"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValidData() = False Then Exit Sub

            Call SetValue()

'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objSalaryInc._FormName = mstrModuleName
            objSalaryInc._LoginEmployeeUnkid = 0
            objSalaryInc._ClientIP = getIP()
            objSalaryInc._HostName = getHostName()
            objSalaryInc._FromWeb = False
            objSalaryInc._AuditUserId = User._Object._Userunkid
objSalaryInc._CompanyUnkid = Company._Object._Companyunkid
            objSalaryInc._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-933,934
            Dim objfrmDocuments As New frmScanOrAttachmentInfo
            Dim dtAttachments As DataTable = Nothing
            If ConfigParameter._Object._MakeAttachmentMandatoryOnSalaryChange Then
                If objfrmDocuments.displayDialog(lblEmployee.Text, enImg_Email_RefId.Employee_Module, menAction, "", mstrModuleName, False, CInt(cboEmployee.SelectedValue), enScanAttactRefId.SALARY, True) Then
                    dtAttachments = objfrmDocuments._dtAttachment.Copy
                    If SaveAttachment(dtAttachments) = False Then Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 50, "Document Attachment is compulsory information.Please attach document(s) as per the configuration setting."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            End If
            objfrmDocuments = Nothing
            'S.SANDEEP |05-JUN-2023| -- End
            

'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD - 484 : Notification when salary change is edited/deleted.
            Dim blnIsTableDataUpdated As Boolean = False
            'Sohail (29 Sep 2021) -- End

            If menAction = enAction.EDIT_ONE Then
                If chkChangeGrade.Checked = True AndAlso User._Object.Privilege._AllowToApproveSalaryChange = True Then
                    'Sohail (24 Feb 2022) -- Start
                    'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                    'blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, , True, "", blnIsTableDataUpdated)

                    'S.SANDEEP |05-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-933,934
                    'blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, True, , True, "", blnIsTableDataUpdated)
                    blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, True, , True, "", blnIsTableDataUpdated, dtAttachments)
                    'S.SANDEEP |05-JUN-2023| -- END

                    'Sohail ((24 Feb 2022) -- End
                    'Sohail (29 Sep 2021) - [blnIsTableDataUpdated]
                Else
                    'Sohail (24 Feb 2022) -- Start
                    'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                    'blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , , True, "", blnIsTableDataUpdated)

                    'S.SANDEEP |05-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-933,934
                    'blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, , , True, "", blnIsTableDataUpdated)
                    blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, , , True, "", blnIsTableDataUpdated, dtAttachments)
                    'S.SANDEEP |05-JUN-2023| -- END

                    'Sohail ((24 Feb 2022) -- End
                    'Sohail (29 Sep 2021) - [blnIsTableDataUpdated]
                End If

            Else
                If chkChangeGrade.Checked = True AndAlso User._Object.Privilege._AllowToApproveSalaryChange = True Then
                    'Sohail (24 Feb 2022) -- Start
                    'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                    'blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, , , True, "")

                    'S.SANDEEP |05-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-933,934
                    'blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, True, , , True, "")
                    blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting _
                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, True, , , True, "", dtAttachments)
                    'S.SANDEEP |05-JUN-2023| -- END

                    'Sohail ((24 Feb 2022) -- End
                Else
                    'Sohail (24 Feb 2022) -- Start
                    'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                    'blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , , , True, "")

                    'S.SANDEEP |05-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-933,934
                    'blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, , , , True, "")
                    blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting _
                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, , , , True, "", dtAttachments)
                    'S.SANDEEP |05-JUN-2023| -- END

                    'Sohail ((24 Feb 2022) -- End
                End If
            End If

            dicApprlNotification = New Dictionary(Of String, String)
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD - 484 : Notification when salary change is edited/deleted.
            'If menAction <> enAction.EDIT_ONE Then
            If menAction <> enAction.EDIT_ONE OrElse blnIsTableDataUpdated = True Then
                'Sohail (29 Sep 2021) -- End
                If User._Object.Privilege._AllowToApproveSalaryChange = False Then

                    'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(mdtPayPeriodEndDate) = CInt(cboEmployee.SelectedValue)

                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    Dim dUList As New DataSet
                    dUList = objUsr.Get_UserBy_PrivilegeId(671)
                    If dUList.Tables(0).Rows.Count > 0 Then
                        For Each dRow As DataRow In dUList.Tables(0).Rows
                            Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(dRow.Item("UId")), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting)
                            If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                                Dim mblnFlag As Boolean = False
                                For Each AID As String In ConfigParameter._Object._UserAccessModeSetting.Trim.Split(CChar(","))
                                    Dim drRow() As DataRow = Nothing
                                    Select Case CInt(AID)

                                        Case enAllocation.DEPARTMENT
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Departmentunkid & " AND referenceunkid = " & enAllocation.DEPARTMENT)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case enAllocation.JOBS
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Jobunkid & " AND referenceunkid = " & enAllocation.JOBS)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case enAllocation.CLASS_GROUP
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classgroupunkid & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case enAllocation.CLASSES
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classunkid & " AND referenceunkid = " & enAllocation.CLASSES)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case Else
                                            mblnFlag = False
                                    End Select
                                Next

                                If mblnFlag Then
                                    StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), objSalaryInc._Salaryincrementtranunkid)
                                    If dicApprlNotification.ContainsKey(CStr(dRow.Item("UEmail"))) = False Then
                                        dicApprlNotification.Add(CStr(dRow.Item("UEmail")), StrMessage)
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If

            If blnFlag = False And objSalaryInc._Message <> "" Then
                eZeeMsgBox.Show(objSalaryInc._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objSalaryInc = Nothing
                    objSalaryInc = New clsSalaryIncrement
                    Call GetValue()
                    cboPayYear.Select()
                Else
                    mintSalaryIncrementtranUnkid = objSalaryInc._Salaryincrementtranunkid
                    Me.Close()
                End If
            End If

            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " TextBox's Events "

    Private Sub txtCurrentScale_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentScale.TextChanged
        Try
            RemoveHandler txtNewScale.TextChanged, AddressOf txtNewScale_TextChanged 'Sohail (15 Dec 2018)
            txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency)
            AddHandler txtNewScale.TextChanged, AddressOf txtNewScale_TextChanged 'Sohail (15 Dec 2018)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtCurrentScale_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtCurrentScale_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrentScale.LostFocus
        Try
            RemoveHandler txtCurrentScale.TextChanged, AddressOf txtCurrentScale_TextChanged 'Sohail (15 Dec 2018)
            txtCurrentScale.Text = Format(txtCurrentScale.Decimal, GUI.fmtCurrency)
            AddHandler txtCurrentScale.TextChanged, AddressOf txtCurrentScale_TextChanged 'Sohail (15 Dec 2018)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtCurrentScale_LostFocus", mstrModuleName)
        End Try
    End Sub

    'Sohail (15 Dec 2018) -- Start
    'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
    'Private Sub txtIncrementAmount_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtIncrementAmount.KeyUp
    '    Try
    '        txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtIncrementAmount_TextChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub txtIncrementAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIncrementAmount.TextChanged
        Try
            RemoveHandler txtNewScale.TextChanged, AddressOf txtNewScale_TextChanged 'Sohail (15 Dec 2018)
            txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency)
            AddHandler txtNewScale.TextChanged, AddressOf txtNewScale_TextChanged 'Sohail (15 Dec 2018)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtIncrementAmount_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Dec 2018) -- End

    Private Sub txtIncrementAmount_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIncrementAmount.LostFocus
        Try
            RemoveHandler txtIncrementAmount.TextChanged, AddressOf txtIncrementAmount_TextChanged 'Sohail (15 Dec 2018)
            txtIncrementAmount.Text = Format(txtIncrementAmount.Decimal, GUI.fmtCurrency)
            AddHandler txtIncrementAmount.TextChanged, AddressOf txtIncrementAmount_TextChanged 'Sohail (15 Dec 2018)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtIncrementAmount_LostFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtPercentage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentage.LostFocus
        Try
            txtPercentage.Text = Format(txtPercentage.Decimal, "00.00####")
            If txtPercentage.Decimal <> 0 Then
                RemoveHandler txtIncrementAmount.TextChanged, AddressOf txtIncrementAmount_TextChanged 'Sohail (15 Dec 2018)
                txtIncrementAmount.Text = Format(txtCurrentScale.Decimal * txtPercentage.Decimal / 100, GUI.fmtCurrency)
                AddHandler txtIncrementAmount.TextChanged, AddressOf txtIncrementAmount_TextChanged 'Sohail (15 Dec 2018)
                'Sohail (15 Dec 2018) -- End
            End If
            txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPercentage_LostFocus", mstrModuleName)
        End Try
    End Sub

    'Sohail (15 Dec 2018) -- Start
    'NMB Enhancement - On salary change screen, create a coonection between new scale text box and change amount text box. i.e if a user types in the new scale, the change amount calculates automatically in 75.1.
    Private Sub txtNewScale_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNewScale.TextChanged
        Try
            RemoveHandler txtIncrementAmount.TextChanged, AddressOf txtIncrementAmount_TextChanged 'Sohail (15 Dec 2018)
            txtIncrementAmount.Text = Format(txtNewScale.Decimal - txtCurrentScale.Decimal, GUI.fmtCurrency)
            AddHandler txtIncrementAmount.TextChanged, AddressOf txtIncrementAmount_TextChanged 'Sohail (15 Dec 2018)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewScale_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewScale_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNewScale.LostFocus
        Try
            RemoveHandler txtNewScale.TextChanged, AddressOf txtNewScale_TextChanged
            txtNewScale.Text = Format(txtNewScale.Decimal, GUI.fmtCurrency)
            AddHandler txtNewScale.TextChanged, AddressOf txtNewScale_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewScale_LostFocus", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Dec 2018) -- End

#End Region

#Region " Other Control's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim dsList As New DataSet
        Dim intGroupId As Integer = -1
        Dim objGroup As New clsCommon_Master
        Try
            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.SALINC_REASON, enAction.ADD_ONE)
            If intGroupId > -1 Then
                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "Group")
                With cboReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Group")
                    .SelectedValue = intGroupId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objGroup = Nothing
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Dim frm As New frmGradeGroup_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objGGroup As New clsGradeGroup
                dsList = objGGroup.getComboList("GGRP", True)
                With cboGradeGroup
                    .ValueMember = "gradegroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("GGRP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objGGroup = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGrade.Click
        Dim frm As New frmGrade_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objGrade As New clsGrade
                dsList = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Grade")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objGrade = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGrade_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddGLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGLevel.Click
        Dim frm As New frmGradeLevel_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objGLevel As New clsGradeLevel
                dsList = objGLevel.getComboList("GLVL", True, CInt(cboGrade.SelectedValue))
                With cboGradeLevel
                    .ValueMember = "gradelevelunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("GLVL")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objGLevel = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGLevel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddScale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddScale.Click
        Dim frm As New frmWagetable_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddScale_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub chkChangeGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChangeGrade.CheckedChanged
        Try
            Call ResetControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkChangeGrade_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub mnuBenefits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuBenefits.Click
        Dim frm As New frmEmployeeBenefitCoverage
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBenefits_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuRecategorization_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuRecategorization.Click
        Dim frm As New frmEmployeeRecategorize
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._EmployeeId = CInt(cboEmployee.SelectedValue)
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRecategorization_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Datetimepicker Event(s) "

    Private Sub dtpIncrementdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpIncrementdate.ValueChanged
        Try
            If CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION Then
                dtpPromotionDate.Value = dtpIncrementdate.Value
                dtpPromotionDate.Checked = True
            End If
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            dtpActualDate.Checked = dtpIncrementdate.Checked
            dtpActualDate.Value = dtpIncrementdate.Value
            'Sohail (21 Jan 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpIncrementdate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " S.SANDEEP [09 MAY 2016] - TRANSACTION SCREEN FOR PROMOTION "
'*************************************** S.SANDEEP [09 MAY 2016] - TRANSACTION SCREEN FOR PROMOTION ************************************|
'
'
'
'
'
'Public Class frmSalaryIncrement_AddEdit

'#Region " Private Variable "
'    Private ReadOnly mstrModuleName As String = "frmSalaryIncrement_AddEdit"
'    Private mblnCancel As Boolean = True
'    Private objSalaryInc As clsSalaryIncrement
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintSalaryIncrementtranUnkid As Integer = -1

'    Private mdtPayPeriodStartDate As Date = Nothing
'    Private mdtPayPeriodEndDate As Date = Nothing

'    'Sohail (12 Aug 2010) -- Start
'    Private mintGradeGroupID As Integer = 0
'    Private mintGradeID As Integer = 0
'    Private mintGradeLevelID As Integer = 0
'    Private mdecIncrementAmt As Decimal = 0 'Sohail (11 May 2011)
'    'Sohail (12 Aug 2010) -- End

'    'S.SANDEEP [ 04 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'    Private mintInfoSalTrnHeadUnkid As Integer = -1
'    'S.SANDEEP [ 04 SEP 2012 ] -- END

'    'S.SANDEEP [ 18 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Dim dicApprlNotification As New Dictionary(Of String, String)
'    Private trd As Thread
'    'S.SANDEEP [ 18 SEP 2012 ] -- END

'    'Sohail (07 Sep 2013) -- Start
'    'TRA - ENHANCEMENT
'    Private mblnLastIncrApproved As Boolean = False
'    Private mintLastIncrUnkId As Integer = -1
'    'Sohail (07 Sep 2013) -- End


'    'S.SANDEEP [14 MAR 2015] -- START
'    Private mintEmployeeUnkid As Integer = 0
'    Private dsCurrJob As New DataSet
'    Private mintMappedGradeUnkid As Integer = 0
'    'S.SANDEEP [14 MAR 2015] -- END

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = 0) As Boolean 'S.SANDEEP [14 MAR 2015] -- START -- END
'        'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean        
'        Try
'            mintSalaryIncrementtranUnkid = intUnkId
'            menAction = eAction
'            'S.SANDEEP [14 MAR 2015] -- START
'            mintEmployeeUnkid = intEmployeeUnkid
'            If mintEmployeeUnkid > 0 Then
'                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
'            End If
'            'S.SANDEEP [14 MAR 2015] -- END

'            Me.ShowDialog()

'            intUnkId = mintSalaryIncrementtranUnkid

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Private Methods "

'    Private Sub SetColor()
'        Try
'            cboPayYear.BackColor = GUI.ColorComp
'            cboPayPeriod.BackColor = GUI.ColorComp
'            cboEmployee.BackColor = GUI.ColorComp
'            txtCurrentScale.BackColor = GUI.ColorComp
'            txtIncrementAmount.BackColor = GUI.ColorComp
'            txtPercentage.BackColor = GUI.ColorComp 'Sohail (09 Oct 2010)
'            txtNewScale.BackColor = GUI.ColorComp
'            dtpIncrementdate.BackColor = GUI.ColorOptional
'            cboReason.BackColor = GUI.ColorComp 'Sohail (08 Nov 2011)'Anjan (01 Aug 2012)
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            cboIncrementBy.BackColor = GUI.ColorComp
'            txtNewScaleGrade.BackColor = GUI.ColorComp
'            'Sohail (31 Mar 2012) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Dim objPeriod As New clscommom_period_Tran
'        Try
'            With objSalaryInc
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'objPeriod._Periodunkid = ._Periodunkid
'                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = ._Periodunkid
'                'Sohail (21 Aug 2015) -- End
'                cboPayYear.SelectedValue = objPeriod._Yearunkid
'                cboPayPeriod.SelectedValue = ._Periodunkid
'                'Sohail (09 Oct 2010) -- Start
'                'Sohail (27 Oct 2014) -- Start
'                'Issue : While Editing Grade Levels are coming from Employee master from cboEmployee.SelectedIndexChange Event.
'                'cboGradeGroup.SelectedValue = objSalaryInc._Gradegroupunkid
'                'cboGrade.SelectedValue = objSalaryInc._Gradeunkid
'                'cboGradeLevel.SelectedValue = objSalaryInc._Gradelevelunkid
'                'Sohail (27 Oct 2014) -- End
'                chkChangeGrade.Checked = objSalaryInc._Isgradechange
'                'Sohail (09 Oct 2010) -- End

'                cboEmployee.SelectedValue = ._Employeeunkid
'                If ._Incrementdate = Nothing Then
'                    dtpIncrementdate.Value = Now.Date
'                Else
'                    dtpIncrementdate.Value = ._Incrementdate
'                End If
'                txtCurrentScale.Text = Format(._Currentscale, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                txtIncrementAmount.Text = Format(._Increment, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                txtNewScale.Text = Format(._Newscale, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                'Sohail (09 Oct 2010) -- Start
'                'Sohail (11 May 2011) -- Start
'                If txtCurrentScale.Decimal > 0 Then
'                    txtPercentage.Text = IIf(txtCurrentScale.Decimal = 0, "0.00", Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, "00.00####")).ToString
'                Else
'                    txtPercentage.Text = Format(0, GUI.fmtCurrency)
'                End If
'                'Sohail (11 May 2011) -- End
'                'cboGradeGroup.SelectedValue = objSalaryInc._Gradegroupunkid
'                'cboGrade.SelectedValue = objSalaryInc._Gradeunkid
'                'cboGradeLevel.SelectedValue = objSalaryInc._Gradelevelunkid
'                'chkChangeGrade.Checked = objSalaryInc._Isgradechange
'                'Sohail (09 Oct 2010) -- End

'                'Sohail (08 Nov 2011) -- Start
'                'txtRemark.Text = ._Remark 'Sohail (12 Oct 2011)
'                cboReason.SelectedValue = ._Reason_Id
'                'Sohail (08 Nov 2011) -- End
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                cboIncrementBy.SelectedValue = objSalaryInc._Increment_Mode
'                txtPercentage.Text = Format(objSalaryInc._Percentage, GUI.fmtCurrency)
'                If objSalaryInc._Isgradechange = True Then
'                    txtNewScaleGrade.Text = Format(._Newscale, GUI.fmtCurrency)
'                Else
'                    txtNewScaleGrade.Text = Format(0, GUI.fmtCurrency)
'                End If
'                'Sohail (31 Mar 2012) -- End

'                'Sohail (27 Oct 2014) -- Start
'                'Issue : While Editing Grade Levels are coming from Employee master from cboEmployee.SelectedIndexChange Event.
'                If menAction = enAction.EDIT_ONE Then
'                    cboGradeGroup.SelectedValue = objSalaryInc._Gradegroupunkid
'                    cboGrade.SelectedValue = objSalaryInc._Gradeunkid
'                    cboGradeLevel.SelectedValue = objSalaryInc._Gradelevelunkid
'                End If
'                'Sohail (27 Oct 2014) -- End
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        Finally
'            objPeriod = Nothing
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            With objSalaryInc
'                ._Periodunkid = CInt(cboPayPeriod.SelectedValue)
'                ._Employeeunkid = CInt(cboEmployee.SelectedValue)
'                ._Incrementdate = dtpIncrementdate.Value
'                ._Currentscale = txtCurrentScale.Decimal 'Sohail (11 May 2011)
'                If chkChangeGrade.Checked = True Then
'                    'Sohail (09 Oct 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    '._Increment = 0
'                    ._Increment = txtNewScaleGrade.Decimal - txtCurrentScale.Decimal
'                    'Sohail (09 Oct 2012) -- End
'                    'Sohail (31 Mar 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    '._Newscale = txtBasicScale.Decimal 'Sohail (11 May 2011)
'                    ._Newscale = txtNewScaleGrade.Decimal
'                    'Sohail (31 Mar 2012) -- End
'                Else
'                    ._Increment = txtIncrementAmount.Decimal 'Sohail (11 May 2011)
'                    ._Newscale = txtNewScale.Decimal 'Sohail (11 May 2011)
'                End If
'                ._Gradegroupunkid = CInt(cboGradeGroup.SelectedValue)
'                ._Gradeunkid = CInt(cboGrade.SelectedValue)
'                ._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
'                ._Isgradechange = chkChangeGrade.Checked
'                ._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
'                'Sohail (08 Nov 2011) -- Start
'                '._Remark = txtRemark.Text.Trim 'Sohail (12 Oct 2011)
'                ._Reason_Id = CInt(cboReason.SelectedValue)
'                'Sohail (08 Nov 2011) -- End
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                ._Increment_Mode = CInt(cboIncrementBy.SelectedValue)
'                If CInt(cboIncrementBy.SelectedValue) = enSalaryIncrementBy.Percentage Then
'                    ._Percentage = txtPercentage.Decimal
'                Else
'                    ._Percentage = 0
'                End If
'                'Sohail (31 Mar 2012) -- End

'                'Sohail (24 Sep 2012) -- Start
'                'TRA - ENHANCEMENT
'                If menAction <> enAction.EDIT_ONE Then
'                    If User._Object.Privilege._AllowToApproveSalaryChange = True Then
'                        ._Isapproved = True
'                        ._Approveruserunkid = User._Object._Userunkid
'                    Else
'                        ._Isapproved = False
'                    End If
'                End If
'                'Sohail (24 Sep 2012) -- End

'                'S.SANDEEP [ 04 SEP 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'                If mintInfoSalTrnHeadUnkid > 0 Then
'                    ._InfoSalHeadUnkid = mintInfoSalTrnHeadUnkid
'                    ._IsCopyPrevoiusSLAB = chkCopyPreviousEDSlab.Checked
'                    ._IsOverwritePrevoiusSLAB = chkOverwritePrevEDSlabHeads.Checked
'                End If
'                'S.SANDEEP [ 04 SEP 2012 ] -- END

'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim objMaster As New clsMasterData
'        Dim objEmployee As New clsEmployee_Master
'        Dim dsCombo As DataSet = Nothing
'        Dim objGradeGroup As New clsGradeGroup
'        Dim objCommon As New clsCommon_Master 'Sohail (08 Nov 2011)
'        Try

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombo = objMaster.getComboListPAYYEAR("PayYear", True)
'            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "PayYear", True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboPayYear
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("PayYear")
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With

'            'Sohail (03 Nov 2010) -- Start
'            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
'            'With cboEmployee
'            '    .ValueMember = "employeeunkid"
'            '    .DisplayMember = "employeename"
'            '    .DataSource = dsCombo.Tables("EmployeeList")
'            '    If .Items.Count > 0 Then .SelectedIndex = 0
'            'End With
'            'Sohail (03 Nov 2010) -- End


'            dsCombo = objGradeGroup.getComboList("GradeGroup", True)
'            With cboGradeGroup
'                .ValueMember = "gradegroupunkid"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("GradeGroup")
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With

'            'Sohail (08 Nov 2011) -- Start
'            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
'            With cboReason
'                .BeginUpdate()
'                .ValueMember = "masterunkid"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("SalReason")
'                .SelectedValue = 0
'                .EndUpdate()
'            End With
'            'Sohail (08 Nov 2011) -- End

'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            dsCombo = objMaster.getComboListSalaryIncrementBy(True, "IncrMode")
'            With cboIncrementBy
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("IncrMode")
'                .SelectedValue = 0
'            End With
'            'Sohail (31 Mar 2012) -- End


'            'S.SANDEEP [09 MAY 2016] -- START
'            'ENHANCEMENT : TRANSACTION SCREEN FOR PROMOTION
'            dsCombo = objMaster.getComboListForSalaryChangeType("List")
'            With cboChangeType
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("List")
'                .SelectedValue = enSalaryChangeType.SIMPLE_SALARY_CHANGE
'            End With
'            'S.SANDEEP [09 MAY 2016] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            objMaster = Nothing
'            objEmployee = Nothing
'            objGradeGroup = Nothing
'            objCommon = Nothing 'Sohail (08 Nov 2011)
'        End Try

'    End Sub

'    Private Sub GetScale()
'        Dim objWages As New clsWagesTran
'        Dim dsList As DataSet = Nothing
'        Try
'            dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), "Scale")
'            If dsList.Tables("Scale").Rows.Count > 0 Then
'                txtBasicScale.Text = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                txtMaximum.Text = Format(dsList.Tables("Scale").Rows(0).Item("Maximum"), GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                txtIncrementAmount.Text = Format(dsList.Tables("Scale").Rows(0).Item("Increment"), GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                mdecIncrementAmt = txtIncrementAmount.Decimal 'Sohail (11 May 2011)
'                'Sohail (09 Oct 2010) -- Start
'                txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'                'Sohail (11 May 2011) -- Start
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                'If txtCurrentScale.Decimal > 0 Then
'                '    txtPercentage.Text = Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, "00.00####")
'                'Else
'                '    txtPercentage.Text = Format(0, GUI.fmtCurrency)
'                'End If
'                'Sohail (31 Mar 2012) -- End
'                'Sohail (11 May 2011) -- End
'                'Sohail (09 Oct 2010) -- End
'            Else
'                txtBasicScale.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                txtMaximum.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                txtIncrementAmount.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                'Sohail (09 Oct 2010) -- Start
'                txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'                'txtPercentage.Text = "0.00"'Sohail (31 Mar 2012)
'                'Sohail (09 Oct 2010) -- End
'                mdecIncrementAmt = 0
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetScale", mstrModuleName)
'        Finally
'            objWages = Nothing
'        End Try
'    End Sub

'    Private Sub ResetControls()
'        Try
'            If chkChangeGrade.Checked = True Then
'                'S.SANDEEP [14 MAR 2015] -- START
'                'cboGradeGroup.Enabled = True
'                'cboGrade.Enabled = True
'                'S.SANDEEP [14 MAR 2015] -- END
'                cboGradeLevel.Enabled = True
'                txtIncrementAmount.Enabled = False
'                txtNewScale.Enabled = False

'                txtIncrementAmount.Text = Format(mdecIncrementAmt, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                'Sohail (09 Oct 2010) -- Start
'                txtPercentage.Enabled = False
'                'Sohail (11 May 2011) -- Start
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                'If txtCurrentScale.Decimal > 0 Then
'                '    txtPercentage.Text = Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, GUI.fmtCurrency)
'                'Else
'                '    txtPercentage.Text = Format(0, GUI.fmtCurrency)
'                'End If
'                txtNewScaleGrade.Text = Format(0, GUI.fmtCurrency)
'                txtNewScaleGrade.Enabled = True
'                'Sohail (31 Mar 2012) -- End
'                'Sohail (11 May 2011) -- End
'                'Sohail (09 Oct 2010) -- End

'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                txtPercentage.Text = Format(0, GUI.fmtCurrency)
'                cboIncrementBy.SelectedValue = 0
'                cboIncrementBy.Enabled = False
'                'Sohail (31 Mar 2012) -- End

'                'Sandeep [ 17 DEC 2010 ] -- Start

'                'S.SANDEEP [14 MAR 2015] -- START
'                'objbtnAddGrade.Enabled = True
'                'objbtnAddGroup.Enabled = True
'                If mintMappedGradeUnkid > 0 Then
'                    objbtnAddGrade.Enabled = False
'                    objbtnAddGroup.Enabled = False
'                    cboGradeGroup.Enabled = False
'                    cboGrade.Enabled = False
'                Else
'                    cboGradeGroup.Enabled = True
'                    cboGrade.Enabled = True
'                    objbtnAddGrade.Enabled = True
'                    objbtnAddGroup.Enabled = True
'                End If

'                'S.SANDEEP [14 MAR 2015] -- END
'                objbtnAddGLevel.Enabled = True
'                objbtnAddScale.Enabled = True
'                'Sandeep [ 17 DEC 2010 ] -- End 

'            Else
'                cboGradeGroup.Enabled = False
'                cboGrade.Enabled = False
'                cboGradeLevel.Enabled = False
'                txtIncrementAmount.Enabled = True
'                txtNewScale.Enabled = True
'                'Sohail (09 Oct 2010) -- Start
'                txtPercentage.Enabled = True
'                'Sohail (09 Oct 2010) -- End

'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                cboIncrementBy.Enabled = True
'                txtNewScaleGrade.Text = Format(0, GUI.fmtCurrency)
'                txtNewScaleGrade.Enabled = False
'                'Sohail (31 Mar 2012) -- End

'                cboGradeGroup.SelectedValue = mintGradeGroupID
'                cboGrade.SelectedValue = mintGradeID
'                cboGradeLevel.SelectedValue = mintGradeLevelID

'                'Sandeep [ 17 DEC 2010 ] -- Start
'                objbtnAddGrade.Enabled = False
'                objbtnAddGroup.Enabled = False
'                objbtnAddGLevel.Enabled = False
'                objbtnAddScale.Enabled = False
'                'Sandeep [ 17 DEC 2010 ] -- End 

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetControls", mstrModuleName)
'        End Try
'    End Sub

'    'Sohail (16 Oct 2010) -- Start
'    Private Sub SetCurrentScale()
'        Dim objEmpMaster As New clsEmployee_Master
'        Dim objMaster As New clsMasterData
'        Dim dsList As DataSet = Nothing

'        Try

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'objEmpMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
'            objEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
'            'S.SANDEEP [04 JUN 2015] -- END

'            mintGradeGroupID = objEmpMaster._Gradegroupunkid
'            mintGradeID = objEmpMaster._Gradeunkid
'            mintGradeLevelID = objEmpMaster._Gradelevelunkid

'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            txtPercentage.Text = Format(0, GUI.fmtCurrency)
'            'Sohail (31 Mar 2012) -- End

'            '*** For Current Scale
'            dsList = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), mdtPayPeriodEndDate)
'            If dsList.Tables("Salary").Rows.Count > 0 Then
'                txtCurrentScale.Text = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                mintGradeGroupID = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
'                mintGradeID = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
'                mintGradeLevelID = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
'                cboGradeGroup.SelectedValue = mintGradeGroupID
'                cboGrade.SelectedValue = mintGradeID
'                cboGradeLevel.SelectedValue = mintGradeLevelID
'            Else
'                cboGradeGroup.SelectedValue = mintGradeGroupID
'                cboGrade.SelectedValue = mintGradeID
'                cboGradeLevel.SelectedValue = mintGradeLevelID
'                txtCurrentScale.Text = txtBasicScale.Text
'            End If
'            'Sohail (11 May 2011) -- Start
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            'If txtCurrentScale.Decimal > 0 Then
'            '    txtPercentage.Text = Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, "00.00###")
'            'Else
'            '    txtPercentage.Text = Format(0, "00.00###")
'            'End If
'            'Sohail (31 Mar 2012) -- End
'            'Sohail (11 May 2011) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetCurrentScale", mstrModuleName)
'        Finally
'            objEmpMaster = Nothing
'            objMaster = Nothing
'        End Try
'    End Sub
'    'Sohail (16 Oct 2010) -- End


'    'S.SANDEEP [ 18 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Function Set_Notification_Approvals(ByVal StrUserName As String, ByVal intUserID As Integer, ByVal intSalaryincrementtranunkid As Integer) As String
'        'Sohail (24 Dec 2013) - [intUserID, intSalaryincrementtranunkid]

'        Dim StrMessage As New System.Text.StringBuilder
'        Dim objEmployee As New clsEmployee_Master
'        Try
'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
'            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
'            'S.SANDEEP [04 JUN 2015] -- END
'            StrMessage.Append("<HTML><BODY>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that following changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
'            StrMessage.Append(vbCrLf)
'            'Sohail (24 Dec 2013) -- Start
'            'Enhancement - Send link in salary change notification
'            'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please login to Main Aruti HR system to Approve/Reject this change.</span></p>")
'            'StrMessage.Append(vbCrLf)
'            'Sohail (24 Dec 2013) -- End
'            StrMessage.Append("<TABLE border = '1' WIDTH = '80%'>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<TR WIDTH = '80%' bgcolor= 'SteelBlue'>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 14, "Period") & "</span></b></TD>")
'            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 15, "Date") & "</span></b></TD>")
'            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 16, "Old Scale") & "</span></b></TD>")
'            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 17, "New Scale") & "</span></b></TD>")
'            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:16%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 18, "Reason") & "</span></b></TD>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("</TR>")
'            StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & cboPayPeriod.Text & "</span></TD>")
'            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dtpIncrementdate.Value.Date & "</span></TD>")
'            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(txtCurrentScale.Text), GUI.fmtCurrency) & "</span></TD>")
'            'Sohail (19 Jun 2014) -- Start
'            'Issue : If Grade has been changed then it doesnt pick new scale from new scale from new scale grade text box.
'            'StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(txtNewScale.Text), GUI.fmtCurrency) & "</span></TD>")
'            If chkChangeGrade.Checked = False Then
'                StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(txtNewScale.Text), GUI.fmtCurrency) & "</span></TD>")
'            Else
'                StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDec(txtNewScaleGrade.Text), GUI.fmtCurrency) & "</span></TD>")
'            End If
'            'Sohail (19 Jun 2014) -- End
'            StrMessage.Append("<TD align = 'LEFT' WIDTH = '16%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & cboReason.Text & "</span></TD>")
'            StrMessage.Append("</TR>" & vbCrLf)
'            StrMessage.Append("</TABLE>")

'            'Sohail (24 Dec 2013) -- Start
'            'Enhancement - Send link in salary change notification
'            Dim strLink As String
'            'strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPg_SalaryIncrementList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid.ToString & "|" & intUserID & "|" & CInt(cboEmployee.SelectedValue).ToString & "|" & CInt(cboPayPeriod.SelectedValue).ToString & "|" & intSalaryincrementtranunkid.ToString))
'            strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPg_SalaryIncrementList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid.ToString & "|" & intUserID & "|" & CInt(cboEmployee.SelectedValue).ToString & "|" & CInt(cboPayPeriod.SelectedValue).ToString & "|0"))

'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please click on the following link to Approve/Reject this change.</span></p>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")
'            StrMessage.Append("<br><br>")
'            'Sohail (24 Dec 2013) -- End

'            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
'            StrMessage.Append("</span></p>")
'            StrMessage.Append("</BODY></HTML>")

'            Return StrMessage.ToString

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
'            Return ""
'        Finally
'            objEmployee = Nothing
'        End Try
'    End Function

'    Private Sub Send_Notification()
'        Try
'            If User._Object.Privilege._AllowToApproveSalaryChange = False Then
'                If dicApprlNotification.Keys.Count > 0 Then
'                    Dim objSendMail As New clsSendMail
'                    For Each sKey As String In dicApprlNotification.Keys
'                        objSendMail._ToEmail = sKey
'                        objSendMail._Subject = Language.getMessage(mstrModuleName, 22, "Notifications to Approve Employee Salary Change.")
'                        objSendMail._Message = dicApprlNotification(sKey)
'                        'S.SANDEEP [ 28 JAN 2014 ] -- START
'                        'Sohail (17 Dec 2014) -- Start
'                        'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
'                        'objSendMail._Form_Name = mstrModuleName
'                        objSendMail._Form_Name = "" 'Please pass Form Name for WEB
'                        'Sohail (17 Dec 2014) -- End
'                        objSendMail._LogEmployeeUnkid = -1
'                        objSendMail._OperationModeId = enLogin_Mode.DESKTOP
'                        objSendMail._UserUnkid = User._Object._Userunkid
'                        objSendMail._SenderAddress = User._Object._Email
'                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
'                        'S.SANDEEP [ 28 JAN 2014 ] -- END
'                        objSendMail.SendMail()
'                    Next
'                End If
'            End If
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 18 SEP 2012 ] -- END


'#End Region

'#Region " ComboBox's Events "
'    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged 'Sohail (11 May 2011)
'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsCombo As DataSet = Nothing

'        Try
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True, enStatusType.Open)
'            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open)
'            'Sohail (21 Aug 2015) -- End
'            With cboPayPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("PayPeriod")
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName) 'Sohail (11 May 2011)
'        Finally
'            objPeriod = Nothing
'        End Try
'    End Sub

'    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged 'Sohail (11 May 2011)
'        Dim objEmpMaster As New clsEmployee_Master
'        'Dim objGradeGroup As New clsGradeGroup
'        'Dim objGrade As New clsGrade
'        'Dim objGradeLevel As New clsGradeLevel
'        'Dim objWages As New clsWagesTran
'        'Dim objMaster As New clsMasterData
'        Dim dsList As DataSet = Nothing
'        Dim objLastIncrementDate As Date = Nothing
'        Try

'            'Sohail (16 Oct 2010) -- Start
'            Call SetCurrentScale()
'            'Sohail (16 Oct 2010) -- End


'            'S.SANDEEP [14 MAR 2015] -- START
'            If CInt(cboEmployee.SelectedValue) > 0 Then

'                mnuBenefits.Enabled = True
'                mnuRecategorization.Enabled = True

'                Dim objRecatgorize As New clsemployee_categorization_Tran
'                dsCurrJob = objRecatgorize.Get_Current_Job(Now.Date, CInt(cboEmployee.SelectedValue))
'                objRecatgorize = Nothing
'                If dsCurrJob.Tables(0).Rows.Count > 0 Then
'                    mintMappedGradeUnkid = CInt(dsCurrJob.Tables(0).Rows(0).Item("gradeunkid"))
'                End If
'                If mintMappedGradeUnkid > 0 Then
'                    Call ResetControls()
'                End If

'            End If
'            'S.SANDEEP [14 MAR 2015] -- END


'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'objEmpMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
'            objEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
'            'S.SANDEEP [04 JUN 2015] -- END

'            'Sohail (14 Aug 2010) -- Start
'            'Sohail (16 Oct 2010) -- Start
'            'mintGradeGroupID = objEmpMaster._Gradegroupunkid
'            'mintGradeID = objEmpMaster._Gradeunkid
'            'mintGradeLevelID = objEmpMaster._Gradelevelunkid
'            'Sohail (16 Oct 2010) -- End
'            'objGradeGroup._Gradegroupunkid = objEmpMaster._Gradegroupunkid
'            'txtGradeGroupName.Text = objGradeGroup._Name
'            'cboGradeGroup.SelectedValue = mintGradeGroupID  'Sohail (09 Oct 2010)

'            'objGrade._Gradeunkid = objEmpMaster._Gradeunkid
'            'txtGradeName.Text = objGrade._Name
'            'cboGrade.SelectedValue = mintGradeID 'Sohail (09 Oct 2010)

'            'objGradeLevel._Gradelevelunkid = objEmpMaster._Gradelevelunkid
'            'txtGradeLevelName.Text = objGradeLevel._Name
'            'cboGradeLevel.SelectedValue = mintGradeLevelID 'Sohail (09 Oct 2010)

'            'For Current Scale
'            'txtCurrentScale.Text = Format(objEmpMaster._Scale, "#0.00")
'            'txtCurrentScale.Text = Format(objMaster.Get_Current_Scale(CInt(cboEmployee.SelectedValue), mdtPayPeriodEndDate), "#0.00")
'            'Sohail (16 Oct 2010) -- Start
'            'dsList = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), mdtPayPeriodEndDate)
'            'If dsList.Tables("Salary").Rows.Count > 0 Then
'            '    txtCurrentScale.Text = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), "#0.00")
'            '    mintGradeGroupID = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
'            '    mintGradeID = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
'            '    mintGradeLevelID = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
'            'Else
'            '    txtCurrentScale.Text = txtBasicScale.Text
'            'End If
'            'Sohail (16 Oct 2010) -- End
'            'Sohail (09 Oct 2010) -- Start
'            'Sohail (16 Oct 2010) -- Start
'            'cboGradeGroup.SelectedValue = mintGradeGroupID
'            'cboGrade.SelectedValue = mintGradeID
'            'cboGradeLevel.SelectedValue = mintGradeLevelID
'            'txtPercentage.Text = Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, "00.00####")
'            'Sohail (16 Oct 2010) -- End
'            'Sohail (09 Oct 2010) -- End


'            'dsList = objWages.getScaleInfo(objEmpMaster._Gradeunkid, objEmpMaster._Gradelevelunkid, "Scale")
'            'If dsList.Tables("Scale").Rows.Count > 0 Then
'            '    txtBasicScaleValue.Text = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), "#0.00")
'            '    txtMaximumValue.Text = Format(dsList.Tables("Scale").Rows(0).Item("Maximum"), "#0.00")
'            '    txtIncrementAmount.Text = Format(dsList.Tables("Scale").Rows(0).Item("Increment"), "#0.00")
'            '    mdblIncrementAmt = cdec(txtIncrementAmount.Text)
'            'Else
'            '    txtBasicScaleValue.Text = "0.00"
'            '    txtMaximumValue.Text = "0.00"
'            '    txtIncrementAmount.Text = "0.00"
'            '    mdblIncrementAmt = 0
'            'End If
'            'Sohail (14 Aug 2010) -- End

'            '*** Get Last Increment
'            'objLastIncrementDate = objSalaryInc.getLastIncrementDate(objEmpMaster._Employeeunkid)
'            'Sohail (07 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            'dsList = objSalaryInc.getLastIncrement("Incr", objEmpMaster._Employeeunkid)

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsList = objSalaryInc.getLastIncrement("Incr", objEmpMaster._Employeeunkid, True)
'            dsList = objSalaryInc.getLastIncrement("Incr", objEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)), True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            'Sohail (07 Sep 2013) -- End
'            'If objLastIncrementDate <> Nothing Then
'            If dsList.Tables("Incr").Rows.Count > 0 Then
'                'Sohail (11 Sep 2010) -- Start
'                'objLastIncrementDate = CDate(dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString)
'                objLastIncrementDate = eZeeDate.convertDate(dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString)
'                'Sohail (11 Sep 2010) -- End
'                txtLastIncrementDate.Text = objLastIncrementDate.ToShortDateString
'                txtLatestScale.Text = Format(dsList.Tables("Incr").Rows(0).Item("newscale"), GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                mblnLastIncrApproved = CBool(dsList.Tables("Incr").Rows(0).Item("isapproved")) 'Sohail (07 Sep 2013)
'                mintLastIncrUnkId = CInt(dsList.Tables("Incr").Rows(0).Item("salaryincrementtranunkid")) 'Sohail (19 Sep 2013)
'            Else
'                txtLastIncrementDate.Text = ""
'                txtLatestScale.Text = Format(objEmpMaster._Scale, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
'                mblnLastIncrApproved = True 'Sohail (07 Sep 2013)
'            End If

'            'S.SANDEEP [ 04 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'            If objEmpMaster._Tranhedunkid > 0 Then
'                Dim objTHead As New clsTransactionHead
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'objTHead._Tranheadunkid = objEmpMaster._Tranhedunkid
'                objTHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = objEmpMaster._Tranhedunkid
'                'Sohail (21 Aug 2015) -- End
'                If objTHead._Trnheadtype_Id = enTranHeadType.Informational Then
'                    chkCopyPreviousEDSlab.Enabled = True : chkCopyPreviousEDSlab.Checked = True
'                    chkOverwritePrevEDSlabHeads.Enabled = True
'                    mintInfoSalTrnHeadUnkid = objEmpMaster._Tranhedunkid
'                Else
'                    chkCopyPreviousEDSlab.Enabled = False : chkCopyPreviousEDSlab.Checked = False
'                    chkOverwritePrevEDSlabHeads.Enabled = False : chkOverwritePrevEDSlabHeads.Checked = False
'                End If
'                objTHead = Nothing
'            Else
'                chkCopyPreviousEDSlab.Enabled = False : chkCopyPreviousEDSlab.Checked = False
'                chkOverwritePrevEDSlabHeads.Enabled = False : chkOverwritePrevEDSlabHeads.Checked = False
'            End If
'            'S.SANDEEP [ 04 SEP 2012 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName) 'Sohail (11 May 2011)
'        Finally
'            objEmpMaster = Nothing
'            'objGradeGroup = Nothing
'            'objGrade = Nothing
'            'objGradeLevel = Nothing
'            'objWages = Nothing
'        End Try
'    End Sub

'    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged 'Sohail (11 May 2011)
'        Dim objPeriod As New clscommom_period_Tran
'        'Dim objMaster As New clsMasterData
'        'Dim dsList As DataSet
'        'Sohail (03 Nov 2010) -- Start
'        Dim objEmployee As New clsEmployee_Master
'        Dim dsCombo As DataSet
'        'Sohail (03 Nov 2010) -- End
'        'Sohail (31 Mar 2012) -- Start
'        'TRA - ENHANCEMENT
'        Dim dtTable As DataTable
'        Dim strEmployeeList As String = ""
'        'Sohail (31 Mar 2012) -- End
'        Try
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
'            'Sohail (21 Aug 2015) -- End
'            mdtPayPeriodStartDate = objPeriod._Start_Date
'            mdtPayPeriodEndDate = objPeriod._End_Date

'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            'Sohail (29 Dec 2012) -- Start
'            'TRA - ENHANCEMENT - Code Commented to allow to give increment more than one time in one period (Issue# 131)
'            'If CInt(cboPayPeriod.SelectedValue) > 0 Then
'            '    Dim ds As DataSet = objSalaryInc.GetEmployeeList("Salary", mdtPayPeriodStartDate)
'            '    For Each dsRow As DataRow In ds.Tables("Salary").Rows
'            '        With dsRow
'            '            If strEmployeeList.Length <= 0 Then
'            '                strEmployeeList = .Item("employeeunkid").ToString
'            '            Else
'            '                strEmployeeList &= "," & .Item("employeeunkid").ToString
'            '            End If
'            '        End With
'            '    Next
'            'End If
'            'Sohail (29 Dec 2012) -- End
'            'Sohail (31 Mar 2012) -- End

'            'Sohail (03 Nov 2010) -- Start
'            'Anjan [10 June 2015] -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
'            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                           User._Object._Userunkid, _
'                                           FinancialYear._Object._YearUnkid, _
'                                           Company._Object._Companyunkid, _
'                                           mdtPayPeriodStartDate, _
'                                           mdtPayPeriodEndDate, _
'                                           ConfigParameter._Object._UserAccessModeSetting, _
'                                           True, False, "EmployeeList", True)

'            'Anjan [10 June 2015] -- End
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            If strEmployeeList.Length > 0 AndAlso menAction <> enAction.EDIT_ONE Then
'                dtTable = New DataView(dsCombo.Tables("EmployeeList"), "employeeunkid NOT IN (" & strEmployeeList & ")", "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtTable = New DataView(dsCombo.Tables("EmployeeList")).ToTable
'            End If
'            'Sohail (31 Mar 2012) -- End

'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                '.DataSource = dsCombo.Tables("EmployeeList")
'                .DataSource = dtTable
'                'Sohail (31 Mar 2012) -- End
'                If .Items.Count > 0 Then .SelectedIndex = 0

'                'S.SANDEEP [14 MAR 2015] -- START
'                If CInt(cboPayPeriod.SelectedValue) > 0 Then
'                    If mintEmployeeUnkid > 0 Then
'                        .SelectedValue = mintEmployeeUnkid
'                    End If
'                End If
'                'S.SANDEEP [14 MAR 2015] -- END

'            End With
'            'Sohail (03 Nov 2010) -- End

'            'Sohail (16 Oct 2010) -- Start
'            Call SetCurrentScale()
'            'Sohail (16 Oct 2010) -- End

'            'Sohail (14 Aug 2010) -- Start
'            'Sohail (16 Oct 2010) -- Start
'            'dsList = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), mdtPayPeriodEndDate)
'            'If dsList.Tables("Salary").Rows.Count > 0 Then
'            '    txtCurrentScale.Text = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), "#0.00")
'            'Else
'            '    txtCurrentScale.Text = txtBasicScale.Text
'            'End If
'            'Sohail (16 Oct 2010) -- End
'            'txtCurrentScale.Text = Format(objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), mdtPayPeriodEndDate), "#0.00")
'            'Sohail (14 Aug 2010) -- End
'            'Sohail (09 Oct 2010) -- Start
'            'txtPercentage.Text = Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, "00.00####") 'Sohail (16 Oct 2010)
'            'Sohail (09 Oct 2010) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName) 'Sohail (11 May 2011)
'        Finally
'            objPeriod = Nothing
'            'objMaster = Nothing
'            objEmployee = Nothing
'        End Try
'    End Sub

'    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged 'Sohail (11 May 2011)
'        Dim objGrade As New clsGrade
'        Dim dsCombo As DataSet
'        Try
'            dsCombo = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
'            With cboGrade
'                .ValueMember = "gradeunkid"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("Grade")
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With
'            'Call GetScale() 'Sohail (16 Oct 2010)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName) 'Sohail (11 May 2011)
'        Finally
'            objGrade = Nothing
'        End Try
'    End Sub

'    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged 'Sohail (11 May 2011)
'        Dim objGradeLevel As New clsGradeLevel
'        Dim dsCombo As DataSet
'        Try
'            dsCombo = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
'            With cboGradeLevel
'                .ValueMember = "gradelevelunkid"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("GradeLevel")
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With
'            'Call GetScale() 'Sohail (16 Oct 2010)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName) 'Sohail (11 May 2011)
'        End Try
'    End Sub

'    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged 'Sohail (11 May 2011)
'        Try
'            Call GetScale()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    'Sohail (31 Mar 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private Sub cboIncrementBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIncrementBy.SelectedIndexChanged
'        Try
'            txtPercentage.Text = Format(0, GUI.fmtCurrency)
'            txtIncrementAmount.ReadOnly = False
'            txtPercentage.ReadOnly = False
'            Select Case CInt(cboIncrementBy.SelectedValue)
'                Case enSalaryIncrementBy.Grade
'                    txtIncrementAmount.ReadOnly = True
'                    txtPercentage.ReadOnly = True
'                    Call GetScale()
'                Case enSalaryIncrementBy.Percentage
'                    txtIncrementAmount.ReadOnly = True
'                Case enSalaryIncrementBy.Amount
'                    txtPercentage.ReadOnly = True
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboIncrementBy_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (31 Mar 2012) -- End

'#End Region

'#Region " Form's Event "

'    Private Sub frmSalaryIncrement_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        Try
'            objSalaryInc = Nothing
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSalaryIncrement_AddEdit_FormClosed", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSalaryIncrement_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            Select Case e.KeyCode
'                Case Keys.Return
'                    SendKeys.Send("{TAB}")
'                    e.Handled = True
'                Case Keys.S
'                    If e.Control = True Then
'                        btnSave.PerformClick()
'                    End If
'                    'Case Keys.Escape
'                    '    btnClose.PerformClick()
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSalaryIncrement_AddEdit_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSalaryIncrement_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objSalaryInc = New clsSalaryIncrement
'        Try
'            Call Set_Logo(Me, gApplicationType)

'            'S.SANDEEP [14 MAR 2015] -- START
'            mnuBenefits.Enabled = False
'            mnuRecategorization.Enabled = False
'            'S.SANDEEP [14 MAR 2015] -- END

'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'Anjan (02 Sep 2011)-End 

'            Call SetColor()
'            Call FillCombo()

'            If menAction = enAction.EDIT_ONE Then
'                objSalaryInc._Salaryincrementtranunkid = mintSalaryIncrementtranUnkid
'                cboPayYear.Enabled = False
'                'If objSalaryInc._Isautogenerated = True Then
'                '    cboPayPeriod.Enabled = True
'                'Else
'                cboPayPeriod.Enabled = False
'                'End If
'                cboEmployee.Enabled = False
'                objbtnSearchEmployee.Enabled = False

'                chkChangeGrade.Enabled = False
'            End If

'            'Sohail (12 Aug 2010) -- Start
'            Call ResetControls()
'            'Sohail (12 Aug 2010) -- End

'            Call GetValue()

'            cboPayYear.Focus()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSalaryIncrement_AddEdit_Load", mstrModuleName)
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-Start
'    'Issue : Including Language Settings.
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsSalaryIncrement.SetMessages()
'            objfrm._Other_ModuleNames = "clsSalaryIncrement"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-End 
'#End Region

'#Region " Button's Event "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Dim objTnA As New clsTnALeaveTran
'        Dim objPayment As New clsPayment_tran
'        Dim intTnAID As Integer
'        Try
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            'If CInt(cboPayYear.SelectedValue) <= 0 Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information)
'            '    cboPayYear.Focus()
'            '    Exit Try
'            'ElseIf
'            'Sohail (31 Mar 2012) -- End
'            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
'                cboPayPeriod.Focus()
'                Exit Try
'            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Employee. Employee is mandatory information."), enMsgBoxStyle.Information)
'                cboEmployee.Focus()
'                Exit Try
'            ElseIf dtpIncrementdate.Value.Date > mdtPayPeriodEndDate OrElse dtpIncrementdate.Value.Date < mdtPayPeriodStartDate Then 'Sohail (16 Oct 2010)
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Increment Date should be in between ") & mdtPayPeriodStartDate & Language.getMessage(mstrModuleName, 13, " And ") & mdtPayPeriodEndDate & ".", enMsgBoxStyle.Information)
'                dtpIncrementdate.Focus()
'                Exit Try
'            End If
'            If chkChangeGrade.Checked = False Then 'Grade not changed
'                'Sohail (31 Mar 2012) -- Start
'                'TRA - ENHANCEMENT
'                If CInt(cboIncrementBy.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Please select Increment By. Increment By is mandatory information."), enMsgBoxStyle.Information)
'                    cboIncrementBy.Focus()
'                    Exit Try
'                End If
'                'Sohail (31 Mar 2012) -- End

'                If txtIncrementAmount.Decimal = 0 Then 'Sohail (11 May 2011)
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Enter Increment Amount. Increment Amount is mandatory information."), enMsgBoxStyle.Information)
'                    txtIncrementAmount.Focus()
'                    Exit Try
'                ElseIf txtNewScale.Decimal > txtMaximum.Decimal Then 'Sohail (11 May 2011)
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, New Scale should not be greater than Maximum Scale."), enMsgBoxStyle.Information)
'                    txtIncrementAmount.Focus()
'                    Exit Try
'                End If
'            Else 'Grade Changed
'                If CInt(cboGradeGroup.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Grade Group. Grade Group is mandatory information."), enMsgBoxStyle.Information)
'                    cboGradeGroup.Focus()
'                    Exit Try
'                ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Grade. Grade is mandatory information."), enMsgBoxStyle.Information)
'                    cboGrade.Focus()
'                    Exit Try
'                ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Grade Level. Grade Level is mandatory information."), enMsgBoxStyle.Information)
'                    cboGradeLevel.Focus()
'                    Exit Try
'                    'Sohail (31 Mar 2012) -- Start
'                    'TRA - ENHANCEMENT
'                ElseIf txtNewScaleGrade.Decimal <= 0 Then 'Sohail (11 May 2011)
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please Enter New Scale. New Scale is mandatory information."), enMsgBoxStyle.Information)
'                    txtNewScaleGrade.Focus()
'                    Exit Try
'                ElseIf txtNewScaleGrade.Decimal < txtBasicScale.Decimal OrElse txtNewScaleGrade.Decimal > txtMaximum.Decimal Then 'Sohail (11 May 2011)
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, New Scale should be in between Minumum Scale and Maximum Scale."), enMsgBoxStyle.Information)
'                    txtNewScaleGrade.Focus()
'                    Exit Try
'                    'Sohail (31 Mar 2012) -- End

'                    'Sohail (10 Nov 2014) -- Start
'                    'Enhancement - Change Grade option on Import Salary Change.
'                ElseIf menAction <> enAction.EDIT_ONE Then
'                    Dim objMaster As New clsMasterData
'                    Dim dsList As DataSet = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), dtpIncrementdate.Value.Date)
'                    If dsList.Tables("Salary").Rows.Count > 0 Then
'                        If CInt(cboGradeLevel.SelectedValue) = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString) Then
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please set different Grade Level."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                            Exit Try
'                        End If
'                    End If
'                    'Sohail (10 Nov 2014) -- End
'                End If
'            End If

'            'Sohail (07 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            If mblnLastIncrApproved = False AndAlso mintLastIncrUnkId <> mintSalaryIncrementtranUnkid Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, Last Increment is not Approved yet. Please Approve Last Increment."), enMsgBoxStyle.Information)
'                dtpIncrementdate.Focus()
'                Exit Try
'            End If
'            'Sohail (07 Sep 2013) -- End

'            If txtLastIncrementDate.Text.Trim <> "" Then
'                If dtpIncrementdate.Value < CDate(txtLastIncrementDate.Text) Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Increment Date should be greater than the Last Increment Date. Please set New Increment Date higher than the Last Increment Date."), enMsgBoxStyle.Information)
'                    dtpIncrementdate.Focus()
'                    Exit Try
'                End If
'            End If

'            intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(cboEmployee.SelectedValue), CInt(cboPayPeriod.SelectedValue))
'            If intTnAID > 0 Then
'                If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then 'S.SANDEEP [ 29 May 2013 ] -- START -- END
'                    'If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Payment of Salary is already done for this period. Please Void Payment for selected employee to give salary change."), enMsgBoxStyle.Information)
'                    cboPayPeriod.Focus()
'                    Exit Try
'                End If
'            End If

'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            If objTnA.IsPayrollProcessDone(CInt(cboPayPeriod.SelectedValue), cboEmployee.SelectedValue.ToString, mdtPayPeriodEndDate) = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to give salary change."), enMsgBoxStyle.Information)
'                Exit Try
'            End If
'            'Sohail (24 Sep 2012) -- End

'            'Sohail (09 Oct 2010) -- Start
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            If chkChangeGrade.Checked = True AndAlso txtCurrentScale.Decimal > txtNewScaleGrade.Decimal Then
'                'If chkChangeGrade.Checked = True AndAlso txtCurrentScale.Decimal > txtBasicScale.Decimal Then 'Sohail (11 May 2011)
'                'Sohail (31 Mar 2012) -- End
'                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "New Basic Scale is less than Current Scale. Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
'                    Exit Try
'                End If
'            End If
'            'Sohail (09 Oct 2010) -- End

'            If CInt(cboReason.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Please select Reason. Reason is mandatory information. "), enMsgBoxStyle.Information)
'                cboReason.Focus()
'                Exit Try
'            End If

'            'S.SANDEEP [ 04 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'PURPOSE : INCLUSION OF SALARY HEAD IN INFORMATIONAL HEAD AND GIVE SELECTION TO USER
'            If menAction <> enAction.EDIT_ONE AndAlso chkCopyPreviousEDSlab.Checked = True Then
'                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 25, "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
'                    Exit Try
'                End If
'            End If
'            'S.SANDEEP [ 04 SEP 2012 ] -- END

'            '*** End Validation

'            'S.SANDEEP [14 MAR 2015] -- START
'            If mintMappedGradeUnkid > 0 Then
'                If CInt(cboGrade.SelectedValue) <> mintMappedGradeUnkid Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, The job title assigned to this employee does not fall within the salary grade you given to the employee, please correct this before completing this salary change using re-categorization screen."), enMsgBoxStyle.Information)
'                    Exit Try
'                End If
'            End If
'            'S.SANDEEP [14 MAR 2015] -- END


'            Call SetValue()

'            If menAction = enAction.EDIT_ONE Then
'                If chkChangeGrade.Checked = True AndAlso User._Object.Privilege._AllowToApproveSalaryChange = True Then
'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'blnFlag = objSalaryInc.Update(True)
'                    blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, , True, "")
'                    'Sohail (21 Aug 2015) -- End
'                Else
'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'blnFlag = objSalaryInc.Update()
'                    blnFlag = objSalaryInc.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , , True, "")
'                    'Sohail (21 Aug 2015) -- End
'                End If

'            Else
'                If chkChangeGrade.Checked = True AndAlso User._Object.Privilege._AllowToApproveSalaryChange = True Then
'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'blnFlag = objSalaryInc.Insert(True)
'                    blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, , , True, "")
'                    'Sohail (21 Aug 2015) -- End
'                Else
'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'blnFlag = objSalaryInc.Insert()
'                    blnFlag = objSalaryInc.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , , , True, "")
'                    'Sohail (21 Aug 2015) -- End
'                End If

'            End If

'            'S.SANDEEP [ 18 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            dicApprlNotification = New Dictionary(Of String, String)
'            If menAction <> enAction.EDIT_ONE Then
'                If User._Object.Privilege._AllowToApproveSalaryChange = False Then

'                    'Sohail (08 Mar 2016) -- Start
'                    'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
'                    Dim objEmployee As New clsEmployee_Master
'                    objEmployee._Employeeunkid(mdtPayPeriodEndDate) = CInt(cboEmployee.SelectedValue)
'                    'Sohail (08 Mar 2016) -- End

'                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
'                    Dim dUList As New DataSet
'                    dUList = objUsr.Get_UserBy_PrivilegeId(671)
'                    If dUList.Tables(0).Rows.Count > 0 Then
'                        For Each dRow As DataRow In dUList.Tables(0).Rows
'                            'Sohail (08 Mar 2016) -- Start
'                            'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
'                            ''Sohail (24 Dec 2013) -- Start
'                            ''Enhancement - Send link in salary change notification
'                            ''StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")))
'                            'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), objSalaryInc._Salaryincrementtranunkid)
'                            ''Sohail (24 Dec 2013) -- End
'                            Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(dRow.Item("UId")), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting)
'                            If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
'                                Dim mblnFlag As Boolean = False
'                                For Each AID As String In ConfigParameter._Object._UserAccessModeSetting.Trim.Split(CChar(","))
'                                    Dim drRow() As DataRow = Nothing
'                                    Select Case CInt(AID)

'                                        Case enAllocation.DEPARTMENT
'                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Departmentunkid & " AND referenceunkid = " & enAllocation.DEPARTMENT)
'                                            If drRow.Length > 0 Then
'                                                mblnFlag = True
'                                            Else
'                                                mblnFlag = False
'                                                Exit For
'                                            End If

'                                        Case enAllocation.JOBS
'                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Jobunkid & " AND referenceunkid = " & enAllocation.JOBS)
'                                            If drRow.Length > 0 Then
'                                                mblnFlag = True
'                                            Else
'                                                mblnFlag = False
'                                                Exit For
'                                            End If

'                                        Case enAllocation.CLASS_GROUP
'                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classgroupunkid & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
'                                            If drRow.Length > 0 Then
'                                                mblnFlag = True
'                                            Else
'                                                mblnFlag = False
'                                                Exit For
'                                            End If

'                                        Case enAllocation.CLASSES
'                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classunkid & " AND referenceunkid = " & enAllocation.CLASSES)
'                                            If drRow.Length > 0 Then
'                                                mblnFlag = True
'                                            Else
'                                                mblnFlag = False
'                                                Exit For
'                                            End If

'                                        Case Else
'                                            mblnFlag = False
'                                    End Select
'                                Next

'                                If mblnFlag Then
'                                    StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), objSalaryInc._Salaryincrementtranunkid)
'                                    If dicApprlNotification.ContainsKey(CStr(dRow.Item("UEmail"))) = False Then
'                                        dicApprlNotification.Add(CStr(dRow.Item("UEmail")), StrMessage)
'                                    End If
'                                End If
'                            End If
'                            'Sohail (08 Mar 2016) -- End
'                        Next
'                    End If
'                End If
'            End If
'            'S.SANDEEP [ 18 SEP 2012 ] -- START

'            If blnFlag = False And objSalaryInc._Message <> "" Then
'                eZeeMsgBox.Show(objSalaryInc._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                If menAction = enAction.ADD_CONTINUE Then
'                    objSalaryInc = Nothing
'                    objSalaryInc = New clsSalaryIncrement
'                    Call GetValue()
'                    cboPayYear.Select()
'                Else
'                    mintSalaryIncrementtranUnkid = objSalaryInc._Salaryincrementtranunkid
'                    Me.Close()
'                End If
'            End If

'            'S.SANDEEP [ 18 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If blnFlag = True Then
'                trd = New Thread(AddressOf Send_Notification)
'                trd.IsBackground = True
'                trd.Start()
'            End If
'            'S.SANDEEP [ 18 SEP 2012 ] -- END


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        Finally
'            objTnA = Nothing
'            objPayment = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " TextBox's Events "
'    Private Sub txtCurrentScale_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentScale.TextChanged
'        Try
'            'Sohail (11 Sep 2010) -- Start
'            'If txtCurrentScale.Text = "" Then Exit Sub
'            'If txtIncrementAmount.Text = "" Or txtIncrementAmount.Text = "." Then Exit Sub
'            'txtNewScale.Text = Format(cdec(txtCurrentScale.Text) + cdec(txtIncrementAmount.Text), "#0.00")
'            txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'            'Sohail (11 Sep 2010) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "txtCurrentScale_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtCurrentScale_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrentScale.LostFocus
'        Try
'            'Sohail (11 Sep 2010) -- Start
'            'txtCurrentScale.Text = Format(cdec(txtCurrentScale.Text), "#0.00")
'            txtCurrentScale.Text = Format(txtCurrentScale.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'            'Sohail (11 Sep 2010) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "txtCurrentScale_LostFocus", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtIncrementAmount_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtIncrementAmount.KeyUp
'        Try
'            'Sohail (11 Sep 2010) -- Start
'            'If txtCurrentScale.Text = "" Then Exit Sub
'            'If txtIncrementAmount.Text = "" Or txtIncrementAmount.Text = "." Then Exit Sub
'            'txtNewScale.Text = Format(cdec(txtCurrentScale.Text) + cdec(txtIncrementAmount.Text), "#0.00")
'            txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'            'Sohail (11 Sep 2010) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "txtIncrementAmount_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtIncrementAmount_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIncrementAmount.LostFocus
'        Try
'            'Sohail (11 Sep 2010) -- Start
'            'If txtIncrementAmount.Text = "" Or txtIncrementAmount.Text = "." Then txtIncrementAmount.Text = "0"
'            'txtIncrementAmount.Text = Format(cdec(txtIncrementAmount.Text), "#0.00")
'            txtIncrementAmount.Text = Format(txtIncrementAmount.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'            'Sohail (11 Sep 2010) -- End

'            'Sohail (09 Oct 2010) -- Start
'            'Sohail (11 May 2011) -- Start
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            'If txtCurrentScale.Decimal > 0 Then
'            '    txtPercentage.Text = Format(txtIncrementAmount.Decimal * 100 / txtCurrentScale.Decimal, "00.00####")
'            'Else
'            '    txtPercentage.Text = Format(0, "00.00####")
'            'End If
'            'Sohail (31 Mar 2012) -- End
'            'Sohail (11 May 2011) -- End

'            'Sohail (09 Oct 2010) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "txtIncrementAmount_LostFocus", mstrModuleName)
'        End Try
'    End Sub

'    'Sohail (09 Oct 2010) -- Start
'    Private Sub txtPercentage_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPercentage.KeyUp
'        Try
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            'txtIncrementAmount.Text = Format(txtCurrentScale.Decimal * txtPercentage.Decimal / 100, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'            'txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
'            'Sohail (31 Mar 2012) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "txtPercentage_KeyUp", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtPercentage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentage.LostFocus
'        Try
'            txtPercentage.Text = Format(txtPercentage.Decimal, "00.00####")
'            'Sohail (31 Mar 2012) -- Start
'            'TRA - ENHANCEMENT
'            If txtPercentage.Decimal <> 0 Then
'                txtIncrementAmount.Text = Format(txtCurrentScale.Decimal * txtPercentage.Decimal / 100, GUI.fmtCurrency)
'            End If
'            txtNewScale.Text = Format(txtCurrentScale.Decimal + txtIncrementAmount.Decimal, GUI.fmtCurrency)
'            'Sohail (31 Mar 2012) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "txtPercentage_LostFocus", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (09 Oct 2010) -- End

'#End Region

'#Region " Other Control's Events "
'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim objfrm As New frmCommonSearch
'        'Sohail (06 Jan 2012) -- Start
'        'TRA - ENHANCEMENT
'        'Dim objEmployee As New clsEmployee_Master
'        'Dim dsList As DataSet
'        'Sohail (06 Jan 2012) -- End
'        Try
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If
'            'Anjan (02 Sep 2011)-End 

'            'Sohail (06 Jan 2012) -- Start
'            'TRA - ENHANCEMENT
'            'dsList = objEmployee.GetEmployeeList("EmployeeList")
'            'Sohail (06 Jan 2012) -- End
'            With cboEmployee
'                'Sohail (06 Jan 2012) -- Start
'                'TRA - ENHANCEMENT
'                'objfrm.DataSource = dsList.Tables("EmployeeList")
'                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
'                'Sohail (06 Jan 2012) -- End
'                objfrm.ValueMember = .ValueMember
'                objfrm.DisplayMember = .DisplayMember
'                objfrm.CodeMember = "employeecode"
'                If objfrm.DisplayDialog Then
'                    .SelectedValue = objfrm.SelectedValue
'                End If
'                .Focus()
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            objfrm = Nothing
'            'objEmployee = Nothing 'Sohail (06 Jan 2012)
'        End Try
'    End Sub

'    'Sohail (08 Nov 2011) -- Start
'    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
'        Dim frm As New frmCommonMaster
'        Dim dsList As New DataSet
'        Dim intGroupId As Integer = -1
'        Dim objGroup As New clsCommon_Master
'        Try
'            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.SALINC_REASON, enAction.ADD_ONE)
'            If intGroupId > -1 Then
'                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "Group")
'                With cboReason
'                    .ValueMember = "masterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Group")
'                    .SelectedValue = intGroupId
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
'        Finally
'            frm = Nothing
'            dsList = Nothing
'            objGroup = Nothing
'        End Try
'    End Sub
'    'Sohail (08 Nov 2011) -- End

'    'Sandeep [ 17 DEC 2010 ] -- Start
'    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
'        Dim frm As New frmGradeGroup_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'Anjan (02 Sep 2011)-End 
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objGGroup As New clsGradeGroup
'                dsList = objGGroup.getComboList("GGRP", True)
'                With cboGradeGroup
'                    .ValueMember = "gradegroupunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("GGRP")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objGGroup = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGrade.Click
'        Dim frm As New frmGrade_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'Anjan (02 Sep 2011)-End 
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objGrade As New clsGrade
'                dsList = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
'                With cboGrade
'                    .ValueMember = "gradeunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Grade")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objGrade = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddGrade_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddGLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGLevel.Click
'        Dim frm As New frmGradeLevel_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'Anjan (02 Sep 2011)-End 
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objGLevel As New clsGradeLevel
'                dsList = objGLevel.getComboList("GLVL", True, CInt(cboGrade.SelectedValue))
'                With cboGradeLevel
'                    .ValueMember = "gradelevelunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("GLVL")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objGLevel = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddGLevel_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddScale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddScale.Click
'        Dim frm As New frmWagetable_AddEdit
'        Try
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'Anjan (02 Sep 2011)-End 

'            frm.ShowDialog()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddScale_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'Sandeep [ 17 DEC 2010 ] -- End 
'#End Region

'#Region " CheckBox's Events "
'    Private Sub chkChangeGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChangeGrade.CheckedChanged
'        Try
'            Call ResetControls()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkChangeGrade_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Message List "
'    '1, "Please Select Pay Year. Pay Year is mandatory information."
'    '2, "Please Select Pay Period. Pay Period is mandatory information."
'    '3, "Please Select Employee. Employee is mandatory information."
'    '4, "Increment Date should be in between " & mdtPayPeriodStartDate & " And " & mdtPayPeriodEndDate & "."
'    '5, "Please Enter Increment Amount. Increment Amount is mandatory information."
'    '6, "Sorry, New Scale should not be greater than Maximum Scale."
'    '7, "Increment Date should be greater than Last Increment Date."
'    '8, "Sorry, Payment of Salary is already done for this period. Please select different period."
'    '9, "Please select Grade Group. Grade Group is mandatory information.
'    '10, "Please select Grade. Grade is mandatory information.
'    '11, "Please select Grade Level. Grade Level is mandatory information."
'    '12, "New Basic Scale is less than Current Scale. Do you want to proceed?"
'#End Region


'    'S.SANDEEP [14 MAR 2015] -- START
'#Region " Context Menu Events "

'    Private Sub mnuBenefits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuBenefits.Click
'        Dim frm As New frmEmployeeBenefitCoverage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            frm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuBenefits_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub mnuRecategorization_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuRecategorization.Click
'        Dim frm As New frmEmployeeRecategorize
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            frm._EmployeeId = CInt(cboEmployee.SelectedValue)
'            frm.ShowDialog()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuRecategorization_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'#End Region
'    'S.SANDEEP [14 MAR 2015] -- END
#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSalaryIncrementInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSalaryIncrementInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbSalaryIncrementInfo.Text = Language._Object.getCaption(Me.gbSalaryIncrementInfo.Name, Me.gbSalaryIncrementInfo.Text)
			Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblCurrentScale.Text = Language._Object.getCaption(Me.lblCurrentScale.Name, Me.lblCurrentScale.Text)
			Me.lblIncrement.Text = Language._Object.getCaption(Me.lblIncrement.Name, Me.lblIncrement.Text)
			Me.lblMaximum.Text = Language._Object.getCaption(Me.lblMaximum.Name, Me.lblMaximum.Text)
			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblBasicScale.Text = Language._Object.getCaption(Me.lblBasicScale.Name, Me.lblBasicScale.Text)
			Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
			Me.lblLastIncrement.Text = Language._Object.getCaption(Me.lblLastIncrement.Name, Me.lblLastIncrement.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
			Me.lblIncrementDate.Text = Language._Object.getCaption(Me.lblIncrementDate.Name, Me.lblIncrementDate.Text)
			Me.lblNewScale.Text = Language._Object.getCaption(Me.lblNewScale.Name, Me.lblNewScale.Text)
			Me.lblLatestScale.Text = Language._Object.getCaption(Me.lblLatestScale.Name, Me.lblLatestScale.Text)
			Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
			Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
			Me.lblIncrementBy.Text = Language._Object.getCaption(Me.lblIncrementBy.Name, Me.lblIncrementBy.Text)
			Me.lblNewScaleGrade.Text = Language._Object.getCaption(Me.lblNewScaleGrade.Name, Me.lblNewScaleGrade.Text)
			Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
			Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuBenefits.Text = Language._Object.getCaption(Me.mnuBenefits.Name, Me.mnuBenefits.Text)
			Me.mnuRecategorization.Text = Language._Object.getCaption(Me.mnuRecategorization.Name, Me.mnuRecategorization.Text)
			Me.chkChangeGrade.Text = Language._Object.getCaption(Me.chkChangeGrade.Name, Me.chkChangeGrade.Text)
			Me.lblPromotionDate.Text = Language._Object.getCaption(Me.lblPromotionDate.Name, Me.lblPromotionDate.Text)
			Me.lblChangeType.Text = Language._Object.getCaption(Me.lblChangeType.Name, Me.lblChangeType.Text)
			Me.lblActualDate.Text = Language._Object.getCaption(Me.lblActualDate.Name, Me.lblActualDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, New Scale should be in between Minumum Scale and Maximum Scale.")
			Language.setMessage(mstrModuleName, 2, "Please Select Pay Period. Pay Period is mandatory information.")
			Language.setMessage(mstrModuleName, 3, "Please Select Employee. Employee is mandatory information.")
			Language.setMessage(mstrModuleName, 4, "Increment Date should be in between")
			Language.setMessage(mstrModuleName, 5, "Please Enter Increment Amount. Increment Amount is mandatory information.")
			Language.setMessage(mstrModuleName, 6, "Sorry, New Scale should not be greater than Maximum Scale.")
			Language.setMessage(mstrModuleName, 7, "Increment Date should be greater than the Last Increment Date. Please set New Increment Date higher than the Last Increment Date.")
			Language.setMessage(mstrModuleName, 8, "Sorry, Payment of Salary is already done for this period. Please Void Payment for selected employee to give salary change.")
			Language.setMessage(mstrModuleName, 9, "Please select Grade Group. Grade Group is mandatory information.")
			Language.setMessage(mstrModuleName, 10, "Please select Grade. Grade is mandatory information.")
			Language.setMessage(mstrModuleName, 11, "Please select Grade Level. Grade Level is mandatory information.")
			Language.setMessage(mstrModuleName, 12, "New Basic Scale is less than Current Scale. Do you want to proceed?")
			Language.setMessage(mstrModuleName, 13, " And")
			Language.setMessage(mstrModuleName, 14, "Period")
			Language.setMessage(mstrModuleName, 15, "Date")
			Language.setMessage(mstrModuleName, 16, "Old Scale")
			Language.setMessage(mstrModuleName, 17, "New Scale")
			Language.setMessage(mstrModuleName, 18, "Reason")
			Language.setMessage(mstrModuleName, 19, "Please Enter New Scale. New Scale is mandatory information.")
			Language.setMessage(mstrModuleName, 20, "Please select Increment By. Increment By is mandatory information.")
			Language.setMessage(mstrModuleName, 21, "Please select Reason. Reason is mandatory information.")
			Language.setMessage(mstrModuleName, 22, "Notifications to Approve Employee Salary Change.")
			Language.setMessage(mstrModuleName, 23, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to give salary change.")
			Language.setMessage(mstrModuleName, 24, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.")
			Language.setMessage(mstrModuleName, 25, "Do you want to continue?")
			Language.setMessage(mstrModuleName, 26, "Sorry, Last Increment is not Approved yet. Please Approve Last Increment.")
			Language.setMessage(mstrModuleName, 27, "Please set different Grade Level.")
			Language.setMessage(mstrModuleName, 28, "Sorry, The job title assigned to this employee does not fall within the salary grade you given to the employee, please correct this before completing this salary change using re-categorization screen.")
			Language.setMessage(mstrModuleName, 29, "Sorry, Promotion date is mandatory information. Please provide promotion date to continue.")
			Language.setMessage(mstrModuleName, 30, "Sorry, Increment date is mandatory information. Please provide Increment date to continue.")
			Language.setMessage(mstrModuleName, 31, "Increment date is not set as selected period start date, So the salary will be prorated as per increment date.")
			Language.setMessage(mstrModuleName, 32, "Are you sure you want to continue?")
			Language.setMessage(mstrModuleName, 33, "Dear")
			Language.setMessage(mstrModuleName, 34, "This is to inform you that following changes have been made for employee")
			Language.setMessage(mstrModuleName, 35, "with employeecode")
			Language.setMessage(mstrModuleName, 36, "Following information has been changed by user")
			Language.setMessage(mstrModuleName, 37, "from Machine")
			Language.setMessage(mstrModuleName, 38, "and IPAddress")
			Language.setMessage(mstrModuleName, 39, "Please click on the following link to Approve/Reject this change.")
			Language.setMessage(mstrModuleName, 40, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 41, "Do you want to void Payment?")
			Language.setMessage(mstrModuleName, 42, "Type to Search")
			Language.setMessage(mstrModuleName, 43, "Sorry, Actual date is mandatory information. Please provide Actual date to continue.")
			Language.setMessage(mstrModuleName, 44, "Actual Date should be in between")
			Language.setMessage(mstrModuleName, 45, "Actual Date should not greater than selected period end date.")
			Language.setMessage(mstrModuleName, 46, "Actual Date should not be greater than Increment Date.")
			Language.setMessage(mstrModuleName, 47, "Sorry, You cannot change this Salary. Reason: Arrears approval is in progress.")
			Language.setMessage(mstrModuleName, 48, "Actual date is set in past period, Which may cause salary arrears.")
			Language.setMessage(mstrModuleName, 49, "Actual Date should be greater than the Last Increment Date. Please set New Actual Date higher than the Last Increment Date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


