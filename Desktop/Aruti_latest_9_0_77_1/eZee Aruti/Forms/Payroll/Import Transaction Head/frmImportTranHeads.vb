﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmImportTranHeads

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportExportHeads"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'Sohail (26 May 2011) -- Start
    Private Sub SetColor()
        Try
            cboHeadTypeId.BackColor = GUI.ColorComp
            cboTypeOfId.BackColor = GUI.ColorComp
            cboCalcTypeId.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 May 2011) -- End

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMaster.getComboListForHeadType("HeadType")
            With cboHeadTypeId
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("HeadType")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhTranCode.DataPropertyName = "trnheadcode"
            dgcolhTranName.DataPropertyName = "trnheadname"
            dgcolhHeadType.DataPropertyName = "trnheadtype_name"
            dgcolhTypeOF.DataPropertyName = "typeof_name"
            dgcolhCalctype.DataPropertyName = "calctype_name"

            dgvImportInfo.DataSource = mdtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmImportExportHeads_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'objIExcel = New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call SetColor() 'Sohail (26 May 2011)
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportExportHeads_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Buttons "

    Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSet.Click

        If radApplytoAll.Checked = False And radApplySelected.Checked = False And radApplytoChecked.Checked = False Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please atleast one action to set the value."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(cboHeadTypeId.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Transaction Head Type is compulsory Information. Please Select Transaction Head Type to continue."), enMsgBoxStyle.Information)
            cboHeadTypeId.Focus()
            Exit Sub

        ElseIf CInt(cboTypeOfId.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Type Of is compulsory Information. Please Select Type Of to continue.."), enMsgBoxStyle.Information)
            cboTypeOfId.Focus()
            Exit Sub

        ElseIf CInt(cboCalcTypeId.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Calculation Type is compulsory Information. Please Select Calculation Type to continue.."), enMsgBoxStyle.Information)
            cboCalcTypeId.Focus()
            Exit Sub

        End If

        Try
            If radApplytoAll.Checked = True Then

                For Each dRow As DataRow In mdtTable.Rows

                    dRow.Item("typeof_id") = CInt(cboTypeOfId.SelectedValue)
                    dRow.Item("typeof_name") = cboTypeOfId.Text

                    dRow.Item("trnheadtype_id") = CInt(cboHeadTypeId.SelectedValue)
                    dRow.Item("trnheadtype_name") = cboHeadTypeId.Text

                    dRow.Item("calctype_id") = CInt(cboCalcTypeId.SelectedValue)
                    dRow.Item("calctype_name") = cboCalcTypeId.Text

                    dRow.Item("IsChange") = True

                    mdtTable.AcceptChanges()
                Next

            ElseIf radApplySelected.Checked = True Then

                Dim intSelectedIndex As Integer
                If dgvImportInfo.SelectedRows.Count > 0 Then

                    intSelectedIndex = dgvImportInfo.SelectedRows(0).Index

                    mdtTable.Rows(intSelectedIndex).Item("typeof_id") = CInt(cboTypeOfId.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("typeof_name") = cboTypeOfId.Text

                    mdtTable.Rows(intSelectedIndex).Item("trnheadtype_id") = CInt(cboHeadTypeId.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("trnheadtype_name") = cboHeadTypeId.Text

                    mdtTable.Rows(intSelectedIndex).Item("calctype_id") = CInt(cboCalcTypeId.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("calctype_name") = cboCalcTypeId.Text

                    mdtTable.Rows(intSelectedIndex).Item("IsChange") = True

                End If
            ElseIf radApplytoChecked.Checked = True Then

                Dim dtTemp() As DataRow = mdtTable.Select("IsChecked = True And IsChange = false")
                'If dtTemp.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one item to assign."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If

                For i As Integer = 0 To dtTemp.Length - 1

                    dtTemp(i)("typeof_id") = CInt(cboTypeOfId.SelectedValue)
                    dtTemp(i)("typeof_name") = cboTypeOfId.Text

                    dtTemp(i)("trnheadtype_id") = CInt(cboHeadTypeId.SelectedValue)
                    dtTemp(i)("trnheadtype_name") = cboHeadTypeId.Text

                    dtTemp(i)("calctype_id") = CInt(cboCalcTypeId.SelectedValue)
                    dtTemp(i)("calctype_name") = cboCalcTypeId.Text

                    dtTemp(i)("IsChange") = True

                Next
            End If
            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSet_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try

            'Pinkal (24-Jan-2011) -- Start

            'If ConfigParameter._Object._ExportDataPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please set Export Data Path from Aruti configuration -> Options -> Path."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            'Pinkal (24-Jan-2011) -- End

            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
            ofdlgOpen.Filter = "XML files (*.xml)|*.xml|Excel files(*.xlsx)|*.xlsx"
            ofdlgOpen.FilterIndex = 2 'Sohail (26 May 2011)
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                Select Case ofdlgOpen.FilterIndex
                    Case 1
                        dsList.Tables.Clear()
                        dsList.ReadXml(txtFilePath.Text)
                    Case 2
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'dsList = objIExcel.Import(txtFilePath.Text)
                        dsList = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                Dim frm As New frmTranHeadMapping
                If frm.displayDialog(dsList) = False Then
                    mblnCancel = False
                    Exit Sub
                End If
                mdtTable = frm._DataTable
                Call FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim blnFlag As Boolean = False
        Dim dtTemp() As DataRow

        'Sohail (26 May 2011) -- Start
        If mdtTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry!, No data found to Import."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (26 May 2011) -- End

        dtTemp = mdtTable.Select("trnheadtype_id = -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please set the transaction head type in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("typeof_id =  -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please set the transaction Type Of in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("calctype_id =  -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please set the Calculation Type Of in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            Dim objtranhead As New clsTransactionHead

            For i As Integer = 0 To mdtTable.Rows.Count - 1

                objtranhead._Trnheadcode = mdtTable.Rows(i)("trnheadcode").ToString
                objtranhead._Trnheadname = mdtTable.Rows(i)("trnheadname").ToString
                objtranhead._Typeof_id = CInt(mdtTable.Rows(i)("typeof_id").ToString)
                objtranhead._Calctype_Id = CInt(mdtTable.Rows(i)("calctype_id").ToString)
                objtranhead._Trnheadtype_Id = CInt(mdtTable.Rows(i)("Trnheadtype_Id").ToString)
                'Sohail (26 May 2011) -- Start
                'objtranhead._Computeon_Id = CInt(mdtTable.Rows(i)("computeon_Id").ToString)
                If objtranhead._Calctype_Id = enCalcType.AsComputedValue OrElse objtranhead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                    objtranhead._Computeon_Id = enComputeOn.ComputeOnSpecifiedFormula
                Else
                    objtranhead._Computeon_Id = 0
                End If
                If objtranhead._Calctype_Id = enCalcType.FlatRate_Others Then
                    objtranhead._Isrecurrent = True
                Else
                    objtranhead._Isrecurrent = False
                End If
                'Sohail (26 May 2011) -- End
                objtranhead._Formula = mdtTable.Rows(i)("formula").ToString
                objtranhead._Formulaid = mdtTable.Rows(i)("formulaid").ToString
                'Sohail (26 May 2011) -- Start
                'objtranhead._Isappearonpayslip = CBool(mdtTable.Rows(i)("isappearonpayslip").ToString) 
                objtranhead._Isappearonpayslip = True
                'Sohail (26 May 2011) -- End
                'objtranhead._Isrecurrent = CBool(mdtTable.Rows(i)("isrecurrent").ToString) 'Sohail (26 May 2011)
                objtranhead._Istaxable = CBool(mdtTable.Rows(i)("istaxable").ToString)
                objtranhead._Istaxrelief = CBool(mdtTable.Rows(i)("istaxrelief").ToString)
                objtranhead._Trnheadname1 = mdtTable.Rows(i)("trnheadname1").ToString
                objtranhead._Trnheadname2 = mdtTable.Rows(i)("trnheadname2").ToString
                objtranhead._Isvoid = CBool(mdtTable.Rows(i)("isvoid").ToString)
                If mdtTable.Rows(i)("voiddatetime").ToString <> "" Then
                    objtranhead._Voiddatetime = CDate(mdtTable.Rows(i)("voiddatetime").ToString)
                End If
                objtranhead._Voidreason = mdtTable.Rows(i)("voidreason").ToString
                objtranhead._Userunkid = User._Object._Userunkid
                objtranhead._Isactive = True 'Sohail (09 Nov 2013)
 'Hemant (23 Oct 2020) -- Start
                objtranhead._CostCenterUnkId = 0
                'Hemant (23 Oct 2020) -- End
                With objtranhead
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
                    ._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
'Sohail (03 Dec 2020) -- Start
                'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                objtranhead._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
                'Sohail (03 Dec 2020) -- End
                'Sohail (11 Dec 2020) -- Start
                'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                objtranhead._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
                'Sohail (11 Dec 2020) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objtranhead.Insert(objtranhead._Calctype_Id, Nothing)
                blnFlag = objtranhead.Insert(objtranhead._Calctype_Id, ConfigParameter._Object._CurrentDateAndTime, Nothing)
                'Sohail (21 Aug 2015) -- End

                If blnFlag = False And objtranhead._Message <> "" Then
                    eZeeMsgBox.Show(objtranhead._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Next
            mblnCancel = False


            'Gajanan [1-July-2020] -- Start
            'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED


            If mdtTable.Rows.Count > 0 Then
                Dim objConfig As New clsConfigOptions
                Dim EmailUserids As String = objConfig.GetKeyValue(ConfigParameter._Object._Companyunkid, "AddEditTransectionHeadNotificationUserIds", Nothing)
                Dim Ip As String = getIP()
                Dim Hostname As String = getHostName()
                Dim blnDisplayImportData As Boolean = CBool(objConfig.GetKeyValue(ConfigParameter._Object._Companyunkid, "OnImportIncludeTransectionHeadWhileSendingNotification", Nothing))

                objtranhead.SendMailTo_User(EmailUserids, ConfigParameter._Object._Companyunkid, _
                                            mstrForm_Name, User._Object._Userunkid, _
                                            clsEmployeeDataApproval.enOperationType.ADDED, "", Ip, _
                                            Hostname, enLogin_Mode.DESKTOP, -1, -1, True, mdtTable, blnDisplayImportData)

            End If
            'Gajanan [1-July-2020] -- End


            'Sohail (26 May 2011) -- Start
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "File Imported Successfully."), enMsgBoxStyle.Information)
            'Sohail (26 May 2011) -- End

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event"

    Private Sub cboHeadTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboHeadTypeId.SelectedIndexChanged
        Try

            Dim objMaster As New clsMasterData
            Dim dsCombos As New DataSet
            Dim dtTable As DataTable 'Sohail (28 Dec 2010)
            dsCombos = objMaster.getComboListTypeOf("TypeOF", CInt(cboHeadTypeId.SelectedValue))
            'Sohail (28 Dec 2010) -- Start
            dtTable = New DataView(dsCombos.Tables("TypeOF"), "ID <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Don't allow user to make SALARY heads
            'Sohail (28 Dec 2010) -- End
            With cboTypeOfId
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                'Sohail (28 Dec 2010) -- Start
                '.DataSource = dsCombos.Tables("TypeOF")
                .DataSource = dtTable
                'Sohail (28 Dec 2010) -- End
                .SelectedValue = 0
            End With

            'Sohail (26 May 2011) -- Start
            If CInt(cboHeadTypeId.SelectedValue) = enTranHeadType.Informational Then
                cboTypeOfId.SelectedValue = enTypeOf.Informational
                cboTypeOfId.Enabled = False
            Else
                cboTypeOfId.Enabled = True
            End If
            objMaster = Nothing
            'Sohail (26 May 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboHeadTypeId_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTypeOfId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTypeOfId.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        'Sohail (28 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION
        Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION & "," & enCalcType.TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_CASH_BENEFIT_TOTAL
        'Sohail (28 Jan 2012) -- End
        Try
            Select Case CInt(cboTypeOfId.SelectedValue)
                Case Is > 1
                    dsList = objMaster.getComboListCalcType("CalcType", "2") 'Sohail (28 Jan 2012)
                    If CInt(cboTypeOfId.SelectedValue) = enTypeOf.Other_Earnings Then
                        'Sohail (26 May 2011) -- Start
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id <> " & enCalcType.ShortHours & " ", "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (26 May 2011) -- End
                    ElseIf CInt(cboTypeOfId.SelectedValue) = enTypeOf.Other_Deductions_Emp Then
                        'Sohail (26 May 2011) -- Start
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id <> " & enCalcType.OverTimeHours & " ", "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (26 May 2011) -- End
                    Else
                        'Sohail (26 May 2011) -- Start
                        'dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.OverTimeHours & ", " & enCalcType.ShortHours & ") ", "", DataViewRowState.CurrentRows).ToTable
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        'Sohail (26 May 2011) -- End
                    End If
                Case Else 'Type of - Salary
                    dsList = objMaster.getComboListCalcType("CalcType", CInt(cboTypeOfId.SelectedValue).ToString) 'Sohail (28 Jan 2012)
                    dtTable = New DataView(dsList.Tables("CalcType"), "", "", DataViewRowState.CurrentRows).ToTable
            End Select

            With cboCalcTypeId
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedValueChanged", mstrModuleName)
            'Sohail (26 May 2011) -- Start
        Finally
            objMaster = Nothing
            dsList = Nothing
            'Sohail (26 May 2011) -- End
        End Try
    End Sub

#End Region

#Region "DataGridview event"
    'Anjan (29 Jan 2011)-Start
    'Issue : this made is been commented temporarily.
    'Private Sub dgvImportInfo_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvImportInfo.CellContentClick
    '    Try
    '        If e.RowIndex <= -1 Then Exit Sub

    '        If CBool(mdtTable.Rows(e.RowIndex)("IsChecked")) = True Then

    '            mdtTable.Rows(e.RowIndex)("IsChecked") = False
    '            mdtTable.Rows(e.RowIndex).Item("trnheadtype_id") = -1
    '            mdtTable.Rows(e.RowIndex).Item("typeof_id") = -1
    '            mdtTable.Rows(e.RowIndex).Item("calctype_id") = -1
    '            mdtTable.Rows(e.RowIndex).Item("trnheadtype_name") = ""
    '            mdtTable.Rows(e.RowIndex).Item("typeof_name") = ""
    '            mdtTable.Rows(e.RowIndex).Item("calctype_name") = ""
    '            mdtTable.Rows(e.RowIndex).Item("Ischange") = False
    '            dgvImportInfo.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = mdtTable.Rows(e.RowIndex)("IsChecked")
    '            dgvImportInfo.RefreshEdit()

    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub
    'Anjan (29 Jan 2011)-End
#End Region

    'Sohail (26 May 2011) -- Start
#Region " Message "
    '1, "Please atleast one action to set the value."
    '2, "Transaction Head Type is compulsory Information. Please Select Transaction Head Type to continue."
    '3, "Type Of is compulsory Information. Please Select Type Of to continue.."
    '4, "Calculation Type is compulsory Information. Please Select Calculation Type to continue.."
    '5, "Please set the transaction head type in order to Import file."
    '6, "Please set the transaction Type Of in order to Import file."
    '7, "Please set the Calculation Type Of in order to Import file."
    '8, "Please set Export Data Path from Aruti configuration -> Options -> Path."
    '9, "Sorry!, No data found to Import."
    '10, "File Imported Successfully."), enMsgBoxStyle.Information
#End Region
    'Sohail (26 May 2011) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnSet.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnSet.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
			Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
			Me.elMandatoryInfo.Text = Language._Object.getCaption(Me.elMandatoryInfo.Name, Me.elMandatoryInfo.Text)
			Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
			Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
			Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
			Me.radApplySelected.Text = Language._Object.getCaption(Me.radApplySelected.Name, Me.radApplySelected.Text)
			Me.lblTypeOfId.Text = Language._Object.getCaption(Me.lblTypeOfId.Name, Me.lblTypeOfId.Text)
			Me.lblHeadTypeId.Text = Language._Object.getCaption(Me.lblHeadTypeId.Name, Me.lblHeadTypeId.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.dgcolhTranCode.HeaderText = Language._Object.getCaption(Me.dgcolhTranCode.Name, Me.dgcolhTranCode.HeaderText)
			Me.dgcolhTranName.HeaderText = Language._Object.getCaption(Me.dgcolhTranName.Name, Me.dgcolhTranName.HeaderText)
			Me.dgcolhHeadType.HeaderText = Language._Object.getCaption(Me.dgcolhHeadType.Name, Me.dgcolhHeadType.HeaderText)
			Me.dgcolhTypeOF.HeaderText = Language._Object.getCaption(Me.dgcolhTypeOF.Name, Me.dgcolhTypeOF.HeaderText)
			Me.dgcolhCalctype.HeaderText = Language._Object.getCaption(Me.dgcolhCalctype.Name, Me.dgcolhCalctype.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please atleast one action to set the value.")
			Language.setMessage(mstrModuleName, 2, "Transaction Head Type is compulsory Information. Please Select Transaction Head Type to continue.")
			Language.setMessage(mstrModuleName, 3, "Type Of is compulsory Information. Please Select Type Of to continue..")
			Language.setMessage(mstrModuleName, 4, "Calculation Type is compulsory Information. Please Select Calculation Type to continue..")
			Language.setMessage(mstrModuleName, 5, "Please set the transaction head type in order to Import file.")
			Language.setMessage(mstrModuleName, 6, "Please set the transaction Type Of in order to Import file.")
			Language.setMessage(mstrModuleName, 7, "Please set the Calculation Type Of in order to Import file.")
			Language.setMessage(mstrModuleName, 8, "Sorry!, No data found to Import.")
			Language.setMessage(mstrModuleName, 9, "File Imported Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class