﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApproveDisapprovePayment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApproveDisapprovePayment))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.gbPaymentModeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objTotalpaidVal = New System.Windows.Forms.Label
        Me.objCashpaidVal = New System.Windows.Forms.Label
        Me.objBankpaidVal = New System.Windows.Forms.Label
        Me.lblTotalPaid = New System.Windows.Forms.Label
        Me.lblCashPaid = New System.Windows.Forms.Label
        Me.lblbankPaid = New System.Windows.Forms.Label
        Me.gbPaymentApprovalnfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objLevelPriorityVal = New System.Windows.Forms.Label
        Me.lblLevelPriority = New System.Windows.Forms.Label
        Me.objApproverLevelVal = New System.Windows.Forms.Label
        Me.objApproverName = New System.Windows.Forms.Label
        Me.lblApproverLevel = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.gbAmountInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.txtTotalAmount = New eZee.TextBox.NumericTextBox
        Me.lblTotalAmount = New System.Windows.Forms.Label
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvEmployeeList = New System.Windows.Forms.ListView
        Me.objcolhChecked = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhCurrency = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbPaymentInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.cboPmtVoucher = New System.Windows.Forms.ComboBox
        Me.lblPmtVoucher = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkPayrollTotalVarianceReport = New System.Windows.Forms.LinkLabel
        Me.lnkPayrollVarianceReport = New System.Windows.Forms.LinkLabel
        Me.pnlMainInfo.SuspendLayout()
        Me.gbPaymentModeInfo.SuspendLayout()
        Me.gbPaymentApprovalnfo.SuspendLayout()
        Me.gbAmountInfo.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.gbPaymentInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.txtRemarks)
        Me.pnlMainInfo.Controls.Add(Me.lblRemarks)
        Me.pnlMainInfo.Controls.Add(Me.gbPaymentModeInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbPaymentApprovalnfo)
        Me.pnlMainInfo.Controls.Add(Me.gbAmountInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbPaymentInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(754, 566)
        Me.pnlMainInfo.TabIndex = 1
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.HideSelection = False
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtRemarks.Location = New System.Drawing.Point(474, 446)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(266, 59)
        Me.txtRemarks.TabIndex = 209
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(475, 424)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(149, 16)
        Me.lblRemarks.TabIndex = 208
        Me.lblRemarks.Text = "Approval Remarks"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbPaymentModeInfo
        '
        Me.gbPaymentModeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbPaymentModeInfo.Checked = False
        Me.gbPaymentModeInfo.CollapseAllExceptThis = False
        Me.gbPaymentModeInfo.CollapsedHoverImage = Nothing
        Me.gbPaymentModeInfo.CollapsedNormalImage = Nothing
        Me.gbPaymentModeInfo.CollapsedPressedImage = Nothing
        Me.gbPaymentModeInfo.CollapseOnLoad = False
        Me.gbPaymentModeInfo.Controls.Add(Me.objelLine1)
        Me.gbPaymentModeInfo.Controls.Add(Me.objTotalpaidVal)
        Me.gbPaymentModeInfo.Controls.Add(Me.objCashpaidVal)
        Me.gbPaymentModeInfo.Controls.Add(Me.objBankpaidVal)
        Me.gbPaymentModeInfo.Controls.Add(Me.lblTotalPaid)
        Me.gbPaymentModeInfo.Controls.Add(Me.lblCashPaid)
        Me.gbPaymentModeInfo.Controls.Add(Me.lblbankPaid)
        Me.gbPaymentModeInfo.ExpandedHoverImage = Nothing
        Me.gbPaymentModeInfo.ExpandedNormalImage = Nothing
        Me.gbPaymentModeInfo.ExpandedPressedImage = Nothing
        Me.gbPaymentModeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPaymentModeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPaymentModeInfo.HeaderHeight = 25
        Me.gbPaymentModeInfo.HeaderMessage = ""
        Me.gbPaymentModeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPaymentModeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPaymentModeInfo.HeightOnCollapse = 0
        Me.gbPaymentModeInfo.LeftTextSpace = 0
        Me.gbPaymentModeInfo.Location = New System.Drawing.Point(467, 187)
        Me.gbPaymentModeInfo.Name = "gbPaymentModeInfo"
        Me.gbPaymentModeInfo.OpenHeight = 182
        Me.gbPaymentModeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPaymentModeInfo.ShowBorder = True
        Me.gbPaymentModeInfo.ShowCheckBox = False
        Me.gbPaymentModeInfo.ShowCollapseButton = False
        Me.gbPaymentModeInfo.ShowDefaultBorderColor = True
        Me.gbPaymentModeInfo.ShowDownButton = False
        Me.gbPaymentModeInfo.ShowHeader = True
        Me.gbPaymentModeInfo.Size = New System.Drawing.Size(275, 113)
        Me.gbPaymentModeInfo.TabIndex = 207
        Me.gbPaymentModeInfo.Temp = 0
        Me.gbPaymentModeInfo.Text = "Payment Mode Summary"
        Me.gbPaymentModeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(3, 78)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(270, 6)
        Me.objelLine1.TabIndex = 269
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotalpaidVal
        '
        Me.objTotalpaidVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTotalpaidVal.Location = New System.Drawing.Point(125, 88)
        Me.objTotalpaidVal.Name = "objTotalpaidVal"
        Me.objTotalpaidVal.Size = New System.Drawing.Size(64, 16)
        Me.objTotalpaidVal.TabIndex = 209
        Me.objTotalpaidVal.Text = "0"
        Me.objTotalpaidVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objCashpaidVal
        '
        Me.objCashpaidVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCashpaidVal.Location = New System.Drawing.Point(125, 57)
        Me.objCashpaidVal.Name = "objCashpaidVal"
        Me.objCashpaidVal.Size = New System.Drawing.Size(64, 16)
        Me.objCashpaidVal.TabIndex = 208
        Me.objCashpaidVal.Text = "0"
        Me.objCashpaidVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objBankpaidVal
        '
        Me.objBankpaidVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objBankpaidVal.Location = New System.Drawing.Point(125, 32)
        Me.objBankpaidVal.Name = "objBankpaidVal"
        Me.objBankpaidVal.Size = New System.Drawing.Size(64, 16)
        Me.objBankpaidVal.TabIndex = 207
        Me.objBankpaidVal.Text = "0"
        Me.objBankpaidVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalPaid
        '
        Me.lblTotalPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPaid.Location = New System.Drawing.Point(8, 88)
        Me.lblTotalPaid.Name = "lblTotalPaid"
        Me.lblTotalPaid.Size = New System.Drawing.Size(111, 16)
        Me.lblTotalPaid.TabIndex = 206
        Me.lblTotalPaid.Text = "Total Payment"
        Me.lblTotalPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCashPaid
        '
        Me.lblCashPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashPaid.Location = New System.Drawing.Point(8, 57)
        Me.lblCashPaid.Name = "lblCashPaid"
        Me.lblCashPaid.Size = New System.Drawing.Size(111, 16)
        Me.lblCashPaid.TabIndex = 204
        Me.lblCashPaid.Text = "Cash Payment"
        Me.lblCashPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblbankPaid
        '
        Me.lblbankPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblbankPaid.Location = New System.Drawing.Point(8, 32)
        Me.lblbankPaid.Name = "lblbankPaid"
        Me.lblbankPaid.Size = New System.Drawing.Size(111, 16)
        Me.lblbankPaid.TabIndex = 141
        Me.lblbankPaid.Text = "Bank Payment"
        Me.lblbankPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbPaymentApprovalnfo
        '
        Me.gbPaymentApprovalnfo.BorderColor = System.Drawing.Color.Black
        Me.gbPaymentApprovalnfo.Checked = False
        Me.gbPaymentApprovalnfo.CollapseAllExceptThis = False
        Me.gbPaymentApprovalnfo.CollapsedHoverImage = Nothing
        Me.gbPaymentApprovalnfo.CollapsedNormalImage = Nothing
        Me.gbPaymentApprovalnfo.CollapsedPressedImage = Nothing
        Me.gbPaymentApprovalnfo.CollapseOnLoad = False
        Me.gbPaymentApprovalnfo.Controls.Add(Me.objLevelPriorityVal)
        Me.gbPaymentApprovalnfo.Controls.Add(Me.lblLevelPriority)
        Me.gbPaymentApprovalnfo.Controls.Add(Me.objApproverLevelVal)
        Me.gbPaymentApprovalnfo.Controls.Add(Me.objApproverName)
        Me.gbPaymentApprovalnfo.Controls.Add(Me.lblApproverLevel)
        Me.gbPaymentApprovalnfo.Controls.Add(Me.lblApprover)
        Me.gbPaymentApprovalnfo.ExpandedHoverImage = Nothing
        Me.gbPaymentApprovalnfo.ExpandedNormalImage = Nothing
        Me.gbPaymentApprovalnfo.ExpandedPressedImage = Nothing
        Me.gbPaymentApprovalnfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPaymentApprovalnfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPaymentApprovalnfo.HeaderHeight = 25
        Me.gbPaymentApprovalnfo.HeaderMessage = ""
        Me.gbPaymentApprovalnfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPaymentApprovalnfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPaymentApprovalnfo.HeightOnCollapse = 0
        Me.gbPaymentApprovalnfo.LeftTextSpace = 0
        Me.gbPaymentApprovalnfo.Location = New System.Drawing.Point(467, 306)
        Me.gbPaymentApprovalnfo.Name = "gbPaymentApprovalnfo"
        Me.gbPaymentApprovalnfo.OpenHeight = 182
        Me.gbPaymentApprovalnfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPaymentApprovalnfo.ShowBorder = True
        Me.gbPaymentApprovalnfo.ShowCheckBox = False
        Me.gbPaymentApprovalnfo.ShowCollapseButton = False
        Me.gbPaymentApprovalnfo.ShowDefaultBorderColor = True
        Me.gbPaymentApprovalnfo.ShowDownButton = False
        Me.gbPaymentApprovalnfo.ShowHeader = True
        Me.gbPaymentApprovalnfo.Size = New System.Drawing.Size(278, 109)
        Me.gbPaymentApprovalnfo.TabIndex = 206
        Me.gbPaymentApprovalnfo.Temp = 0
        Me.gbPaymentApprovalnfo.Text = "Payment Approval Information"
        Me.gbPaymentApprovalnfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLevelPriorityVal
        '
        Me.objLevelPriorityVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLevelPriorityVal.Location = New System.Drawing.Point(141, 85)
        Me.objLevelPriorityVal.Name = "objLevelPriorityVal"
        Me.objLevelPriorityVal.Size = New System.Drawing.Size(116, 16)
        Me.objLevelPriorityVal.TabIndex = 272
        Me.objLevelPriorityVal.Text = "0"
        Me.objLevelPriorityVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLevelPriority
        '
        Me.lblLevelPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelPriority.Location = New System.Drawing.Point(8, 85)
        Me.lblLevelPriority.Name = "lblLevelPriority"
        Me.lblLevelPriority.Size = New System.Drawing.Size(127, 16)
        Me.lblLevelPriority.TabIndex = 271
        Me.lblLevelPriority.Text = "Level Priority"
        Me.lblLevelPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objApproverLevelVal
        '
        Me.objApproverLevelVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverLevelVal.Location = New System.Drawing.Point(141, 59)
        Me.objApproverLevelVal.Name = "objApproverLevelVal"
        Me.objApproverLevelVal.Size = New System.Drawing.Size(116, 16)
        Me.objApproverLevelVal.TabIndex = 208
        Me.objApproverLevelVal.Text = "0"
        Me.objApproverLevelVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objApproverName
        '
        Me.objApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverName.Location = New System.Drawing.Point(141, 34)
        Me.objApproverName.Name = "objApproverName"
        Me.objApproverName.Size = New System.Drawing.Size(116, 16)
        Me.objApproverName.TabIndex = 207
        Me.objApproverName.Text = "0"
        Me.objApproverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApproverLevel
        '
        Me.lblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverLevel.Location = New System.Drawing.Point(8, 59)
        Me.lblApproverLevel.Name = "lblApproverLevel"
        Me.lblApproverLevel.Size = New System.Drawing.Size(127, 16)
        Me.lblApproverLevel.TabIndex = 204
        Me.lblApproverLevel.Text = "Approver Level"
        Me.lblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 34)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(127, 16)
        Me.lblApprover.TabIndex = 141
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbAmountInfo
        '
        Me.gbAmountInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAmountInfo.Checked = False
        Me.gbAmountInfo.CollapseAllExceptThis = False
        Me.gbAmountInfo.CollapsedHoverImage = Nothing
        Me.gbAmountInfo.CollapsedNormalImage = Nothing
        Me.gbAmountInfo.CollapsedPressedImage = Nothing
        Me.gbAmountInfo.CollapseOnLoad = False
        Me.gbAmountInfo.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbAmountInfo.ExpandedHoverImage = Nothing
        Me.gbAmountInfo.ExpandedNormalImage = Nothing
        Me.gbAmountInfo.ExpandedPressedImage = Nothing
        Me.gbAmountInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAmountInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAmountInfo.HeaderHeight = 25
        Me.gbAmountInfo.HeaderMessage = ""
        Me.gbAmountInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAmountInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAmountInfo.HeightOnCollapse = 0
        Me.gbAmountInfo.LeftTextSpace = 0
        Me.gbAmountInfo.Location = New System.Drawing.Point(12, 446)
        Me.gbAmountInfo.Name = "gbAmountInfo"
        Me.gbAmountInfo.OpenHeight = 300
        Me.gbAmountInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAmountInfo.ShowBorder = True
        Me.gbAmountInfo.ShowCheckBox = False
        Me.gbAmountInfo.ShowCollapseButton = False
        Me.gbAmountInfo.ShowDefaultBorderColor = True
        Me.gbAmountInfo.ShowDownButton = False
        Me.gbAmountInfo.ShowHeader = True
        Me.gbAmountInfo.Size = New System.Drawing.Size(446, 59)
        Me.gbAmountInfo.TabIndex = 3
        Me.gbAmountInfo.Temp = 0
        Me.gbAmountInfo.Text = "Total Amount"
        Me.gbAmountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.txtTotalAmount)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblTotalAmount)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(441, 31)
        Me.pnlAdvanceTaken.TabIndex = 0
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.AllowNegative = True
        Me.txtTotalAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalAmount.DigitsInGroup = 3
        Me.txtTotalAmount.Flags = 0
        Me.txtTotalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAmount.Location = New System.Drawing.Point(214, 7)
        Me.txtTotalAmount.MaxDecimalPlaces = 6
        Me.txtTotalAmount.MaxWholeDigits = 21
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.Prefix = ""
        Me.txtTotalAmount.RangeMax = 1.7976931348623157E+308
        Me.txtTotalAmount.RangeMin = -1.7976931348623157E+308
        Me.txtTotalAmount.ReadOnly = True
        Me.txtTotalAmount.Size = New System.Drawing.Size(167, 21)
        Me.txtTotalAmount.TabIndex = 0
        Me.txtTotalAmount.Text = "0"
        Me.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalAmount.Location = New System.Drawing.Point(91, 9)
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Size = New System.Drawing.Size(117, 16)
        Me.lblTotalAmount.TabIndex = 0
        Me.lblTotalAmount.Text = "Total Amount"
        Me.lblTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(449, 374)
        Me.gbEmployeeList.TabIndex = 0
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee Payment Information"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvEmployeeList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(445, 347)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvEmployeeList
        '
        Me.lvEmployeeList.CheckBoxes = True
        Me.lvEmployeeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhChecked, Me.colhCode, Me.colhName, Me.colhAmount, Me.colhCurrency})
        Me.lvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmployeeList.FullRowSelect = True
        Me.lvEmployeeList.GridLines = True
        Me.lvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmployeeList.Name = "lvEmployeeList"
        Me.lvEmployeeList.Size = New System.Drawing.Size(445, 347)
        Me.lvEmployeeList.TabIndex = 0
        Me.lvEmployeeList.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeList.View = System.Windows.Forms.View.Details
        '
        'objcolhChecked
        '
        Me.objcolhChecked.Tag = "objcolhChecked"
        Me.objcolhChecked.Text = ""
        Me.objcolhChecked.Width = 30
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 90
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 120
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Paid Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 140
        '
        'colhCurrency
        '
        Me.colhCurrency.Tag = "colhCurrency"
        Me.colhCurrency.Text = "Currency"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(754, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Approve Payment"
        '
        'gbPaymentInfo
        '
        Me.gbPaymentInfo.BorderColor = System.Drawing.Color.Black
        Me.gbPaymentInfo.Checked = False
        Me.gbPaymentInfo.CollapseAllExceptThis = False
        Me.gbPaymentInfo.CollapsedHoverImage = Nothing
        Me.gbPaymentInfo.CollapsedNormalImage = Nothing
        Me.gbPaymentInfo.CollapsedPressedImage = Nothing
        Me.gbPaymentInfo.CollapseOnLoad = False
        Me.gbPaymentInfo.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbPaymentInfo.Controls.Add(Me.cboPmtVoucher)
        Me.gbPaymentInfo.Controls.Add(Me.lblPmtVoucher)
        Me.gbPaymentInfo.Controls.Add(Me.objbtnReset)
        Me.gbPaymentInfo.Controls.Add(Me.objbtnSearch)
        Me.gbPaymentInfo.Controls.Add(Me.cboCurrency)
        Me.gbPaymentInfo.Controls.Add(Me.lblCurrency)
        Me.gbPaymentInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbPaymentInfo.Controls.Add(Me.lblPayPeriod)
        Me.gbPaymentInfo.ExpandedHoverImage = Nothing
        Me.gbPaymentInfo.ExpandedNormalImage = Nothing
        Me.gbPaymentInfo.ExpandedPressedImage = Nothing
        Me.gbPaymentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPaymentInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPaymentInfo.HeaderHeight = 25
        Me.gbPaymentInfo.HeaderMessage = ""
        Me.gbPaymentInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPaymentInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPaymentInfo.HeightOnCollapse = 0
        Me.gbPaymentInfo.LeftTextSpace = 0
        Me.gbPaymentInfo.Location = New System.Drawing.Point(467, 66)
        Me.gbPaymentInfo.Name = "gbPaymentInfo"
        Me.gbPaymentInfo.OpenHeight = 182
        Me.gbPaymentInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPaymentInfo.ShowBorder = True
        Me.gbPaymentInfo.ShowCheckBox = False
        Me.gbPaymentInfo.ShowCollapseButton = False
        Me.gbPaymentInfo.ShowDefaultBorderColor = True
        Me.gbPaymentInfo.ShowDownButton = False
        Me.gbPaymentInfo.ShowHeader = True
        Me.gbPaymentInfo.Size = New System.Drawing.Size(275, 115)
        Me.gbPaymentInfo.TabIndex = 0
        Me.gbPaymentInfo.Temp = 0
        Me.gbPaymentInfo.Text = "Payment Information"
        Me.gbPaymentInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(130, 6)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(94, 13)
        Me.lnkAdvanceFilter.TabIndex = 312
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Advance Filter"
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboPmtVoucher
        '
        Me.cboPmtVoucher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPmtVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPmtVoucher.FormattingEnabled = True
        Me.cboPmtVoucher.Location = New System.Drawing.Point(102, 88)
        Me.cboPmtVoucher.Name = "cboPmtVoucher"
        Me.cboPmtVoucher.Size = New System.Drawing.Size(155, 21)
        Me.cboPmtVoucher.TabIndex = 314
        '
        'lblPmtVoucher
        '
        Me.lblPmtVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPmtVoucher.Location = New System.Drawing.Point(8, 90)
        Me.lblPmtVoucher.Name = "lblPmtVoucher"
        Me.lblPmtVoucher.Size = New System.Drawing.Size(88, 16)
        Me.lblPmtVoucher.TabIndex = 315
        Me.lblPmtVoucher.Text = "Voucher #"
        Me.lblPmtVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(249, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 311
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(226, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 310
        Me.objbtnSearch.TabStop = False
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(102, 61)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(155, 21)
        Me.cboCurrency.TabIndex = 5
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 63)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(88, 16)
        Me.lblCurrency.TabIndex = 204
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(102, 34)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(155, 21)
        Me.cboPayPeriod.TabIndex = 1
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(88, 16)
        Me.lblPayPeriod.TabIndex = 141
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkPayrollTotalVarianceReport)
        Me.objFooter.Controls.Add(Me.lnkPayrollVarianceReport)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 511)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(754, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(474, 13)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(165, 30)
        Me.btnProcess.TabIndex = 0
        Me.btnProcess.Text = "&Approve Payment"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(645, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkPayrollTotalVarianceReport
        '
        Me.lnkPayrollTotalVarianceReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollTotalVarianceReport.Location = New System.Drawing.Point(233, 22)
        Me.lnkPayrollTotalVarianceReport.Name = "lnkPayrollTotalVarianceReport"
        Me.lnkPayrollTotalVarianceReport.Size = New System.Drawing.Size(200, 13)
        Me.lnkPayrollTotalVarianceReport.TabIndex = 319
        Me.lnkPayrollTotalVarianceReport.TabStop = True
        Me.lnkPayrollTotalVarianceReport.Text = "Show Payroll Total Variance Report"
        '
        'lnkPayrollVarianceReport
        '
        Me.lnkPayrollVarianceReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollVarianceReport.Location = New System.Drawing.Point(9, 22)
        Me.lnkPayrollVarianceReport.Name = "lnkPayrollVarianceReport"
        Me.lnkPayrollVarianceReport.Size = New System.Drawing.Size(200, 13)
        Me.lnkPayrollVarianceReport.TabIndex = 318
        Me.lnkPayrollVarianceReport.TabStop = True
        Me.lnkPayrollVarianceReport.Text = "Show Payroll Variance Report"
        '
        'frmApproveDisapprovePayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 566)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApproveDisapprovePayment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approve Payment"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbPaymentModeInfo.ResumeLayout(False)
        Me.gbPaymentApprovalnfo.ResumeLayout(False)
        Me.gbAmountInfo.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.pnlAdvanceTaken.PerformLayout()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.gbPaymentInfo.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbAmountInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents txtTotalAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalAmount As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents lvEmployeeList As System.Windows.Forms.ListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbPaymentInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents colhCurrency As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbPaymentApprovalnfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents objApproverLevelVal As System.Windows.Forms.Label
    Friend WithEvents objApproverName As System.Windows.Forms.Label
    Friend WithEvents objLevelPriorityVal As System.Windows.Forms.Label
    Friend WithEvents lblLevelPriority As System.Windows.Forms.Label
    Friend WithEvents objcolhChecked As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents gbPaymentModeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objTotalpaidVal As System.Windows.Forms.Label
    Friend WithEvents objCashpaidVal As System.Windows.Forms.Label
    Friend WithEvents objBankpaidVal As System.Windows.Forms.Label
    Friend WithEvents lblTotalPaid As System.Windows.Forms.Label
    Friend WithEvents lblCashPaid As System.Windows.Forms.Label
    Friend WithEvents lblbankPaid As System.Windows.Forms.Label
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboPmtVoucher As System.Windows.Forms.ComboBox
    Friend WithEvents lblPmtVoucher As System.Windows.Forms.Label
    Friend WithEvents lnkPayrollTotalVarianceReport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPayrollVarianceReport As System.Windows.Forms.LinkLabel
End Class
