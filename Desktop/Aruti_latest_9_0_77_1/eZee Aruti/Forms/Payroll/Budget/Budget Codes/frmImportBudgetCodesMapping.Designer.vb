﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportBudgetCodesMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportBudgetCodesMapping))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboBudgetCode = New System.Windows.Forms.ComboBox
        Me.lblBudgetCode = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvMapping = New System.Windows.Forms.DataGridView
        Me.colhPercentage = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhActivityCode = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhFundprojectcodeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhProjectCodes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPercentageid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhActivityid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbFieldMapping)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(662, 552)
        Me.pnlMainInfo.TabIndex = 1
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.cboBudgetCode)
        Me.gbFieldMapping.Controls.Add(Me.lblBudgetCode)
        Me.gbFieldMapping.Controls.Add(Me.lblPeriod)
        Me.gbFieldMapping.Controls.Add(Me.cboPeriod)
        Me.gbFieldMapping.Controls.Add(Me.Panel1)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(12, 12)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(636, 479)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBudgetCode
        '
        Me.cboBudgetCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudgetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudgetCode.FormattingEnabled = True
        Me.cboBudgetCode.Location = New System.Drawing.Point(134, 58)
        Me.cboBudgetCode.Name = "cboBudgetCode"
        Me.cboBudgetCode.Size = New System.Drawing.Size(172, 21)
        Me.cboBudgetCode.TabIndex = 235
        '
        'lblBudgetCode
        '
        Me.lblBudgetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetCode.Location = New System.Drawing.Point(8, 59)
        Me.lblBudgetCode.Name = "lblBudgetCode"
        Me.lblBudgetCode.Size = New System.Drawing.Size(120, 16)
        Me.lblBudgetCode.TabIndex = 236
        Me.lblBudgetCode.Text = "Budget Code"
        Me.lblBudgetCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 34)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(120, 14)
        Me.lblPeriod.TabIndex = 233
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(134, 31)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(116, 21)
        Me.cboPeriod.TabIndex = 232
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvMapping)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(11, 85)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(607, 391)
        Me.Panel1.TabIndex = 23
        '
        'dgvMapping
        '
        Me.dgvMapping.AllowUserToAddRows = False
        Me.dgvMapping.AllowUserToDeleteRows = False
        Me.dgvMapping.AllowUserToResizeRows = False
        Me.dgvMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMapping.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhFundprojectcodeunkid, Me.colhProjectCodes, Me.colhPercentage, Me.colhActivityCode, Me.objcolhPercentageid, Me.objcolhActivityid})
        Me.dgvMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMapping.Location = New System.Drawing.Point(0, 0)
        Me.dgvMapping.Name = "dgvMapping"
        Me.dgvMapping.RowHeadersVisible = False
        Me.dgvMapping.Size = New System.Drawing.Size(607, 391)
        Me.dgvMapping.TabIndex = 0
        '
        'colhPercentage
        '
        Me.colhPercentage.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colhPercentage.HeaderText = "Project Codes (%)"
        Me.colhPercentage.Name = "colhPercentage"
        Me.colhPercentage.Width = 200
        '
        'colhActivityCode
        '
        Me.colhActivityCode.HeaderText = "Activity Code"
        Me.colhActivityCode.Name = "colhActivityCode"
        Me.colhActivityCode.Width = 200
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(446, 58)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(172, 21)
        Me.cboEmployeeCode.TabIndex = 0
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(320, 59)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(120, 16)
        Me.lblEmployeeCode.TabIndex = 1
        Me.lblEmployeeCode.Text = "Emp. / Allocation Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.lnkAutoMap)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnOk)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 497)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(662, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Location = New System.Drawing.Point(20, 22)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(124, 13)
        Me.lnkAutoMap.TabIndex = 2
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "&Auto Map"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(553, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(450, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Project Codes"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = ""
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = ""
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = ""
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'objcolhFundprojectcodeunkid
        '
        Me.objcolhFundprojectcodeunkid.HeaderText = ""
        Me.objcolhFundprojectcodeunkid.Name = "objcolhFundprojectcodeunkid"
        Me.objcolhFundprojectcodeunkid.ReadOnly = True
        Me.objcolhFundprojectcodeunkid.Visible = False
        '
        'colhProjectCodes
        '
        Me.colhProjectCodes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhProjectCodes.HeaderText = "Project Codes"
        Me.colhProjectCodes.Name = "colhProjectCodes"
        Me.colhProjectCodes.ReadOnly = True
        '
        'objcolhPercentageid
        '
        Me.objcolhPercentageid.HeaderText = ""
        Me.objcolhPercentageid.Name = "objcolhPercentageid"
        Me.objcolhPercentageid.ReadOnly = True
        Me.objcolhPercentageid.Visible = False
        '
        'objcolhActivityid
        '
        Me.objcolhActivityid.HeaderText = ""
        Me.objcolhActivityid.Name = "objcolhActivityid"
        Me.objcolhActivityid.ReadOnly = True
        Me.objcolhActivityid.Visible = False
        '
        'frmImportBudgetCodesMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(662, 552)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportBudgetCodesMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Codes Mapping"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvMapping As System.Windows.Forms.DataGridView
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboBudgetCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudgetCode As System.Windows.Forms.Label
    Friend WithEvents objcolhFundprojectcodeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhProjectCodes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPercentage As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhActivityCode As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objcolhPercentageid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhActivityid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
