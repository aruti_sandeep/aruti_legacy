﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmFundProjectCodeList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmFundProjectCodeList"
    Private objFundProjectCode As clsFundProjectCode

#End Region

#Region " Form's Events "

    Private Sub frmFundProjectCodeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmFundProjectCodeList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundProjectCodeList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundProjectCodeList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvFundProjectCode.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundProjectCodeList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundProjectCodeList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFundProjectCode.SetMessages()
            objfrm._Other_ModuleNames = "clsFundProjectCode"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmFundProjectCodeList_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmFundProjectCodeList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objFundProjectCode = New clsFundProjectCode
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            Call FillList()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmFundProjectCodeList_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objFund As New clsFundSource_Master
        Dim dsList As DataSet
        Try

            dsList = objFundProjectCode.GetComboList("List", True)
            With cboProjectCode
                .ValueMember = "fundprojectcodeunkid"
                .DisplayMember = "fundprojectname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objFund.GetComboList("List", True)
            With cboFundName
                .ValueMember = "fundsourceunkid"
                .DisplayMember = "fundname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objFund = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim mstrSearch As String = String.Empty
            Dim dsList As New DataSet
            objFundProjectCode = New clsFundProjectCode

            If User._Object.Privilege._AllowToViewFundProjectCode = False Then Exit Sub

            If CInt(cboProjectCode.SelectedValue) > 0 Then
                mstrSearch &= "AND bgfundprojectcode_master.fundprojectcodeunkid = " & CInt(cboProjectCode.SelectedValue) & " "
            End If

            If CInt(cboFundName.SelectedValue) > 0 Then
                mstrSearch &= "AND bgfundprojectcode_master.fundsourceunkid = " & CInt(cboFundName.SelectedValue) & " "
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Substring(3)
            End If

            dsList = objFundProjectCode.GetList("List", mstrSearch)

            dgvFundProjectCode.AutoGenerateColumns = False
            dgcolhProjectCode.DataPropertyName = "fundprojectcode"
            dgcolhprojectName.DataPropertyName = "fundprojectname"
            dgcolhFundName.DataPropertyName = "fundname"
            dgcolhCurrentCeilingBal.DataPropertyName = "currentceilingbalance"
            dgcolhCurrentCeilingBal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentCeilingBal.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentCeilingBal.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhExpiryDate.DataPropertyName = "fundprojectcodeexpirydate"
            dgcolhExpiryDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhNotifyAmount.DataPropertyName = "notify_amount"
            dgcolhNotifyAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNotifyAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNotifyAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            objdgcolhfundprojectcodeunkid.DataPropertyName = "fundprojectcodeunkid"

            dgvFundProjectCode.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvFundProjectCode.RowCount.ToString)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddFundProjectCode
            btnEdit.Enabled = User._Object.Privilege._AllowToEditFundProjectCode
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteFundProjectCode

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmFundProjectCodeAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
                Call FillCombo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmFundProjectCodeAddEdit
        Try
            If dgvFundProjectCode.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundProjectCode.Focus()
                Exit Sub
            End If

            If frm.displayDialog(CInt(dgvFundProjectCode.SelectedRows(0).Cells(objdgcolhfundprojectcodeunkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try
            If dgvFundProjectCode.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundProjectCode.Focus()
                Exit Sub
            End If

            If objFundProjectCode.isUsed(CInt(dgvFundProjectCode.SelectedRows(0).Cells(objdgcolhfundprojectcodeunkid.Index).Value)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot Delete this Fund Project code. Reason: This Fund Project code is in use."), enMsgBoxStyle.Information)
                dgvFundProjectCode.Focus()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to Delete this Fund Project code?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objFundProjectCode = New clsFundProjectCode

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objFundProjectCode._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objFundProjectCode._Isvoid = True
                objFundProjectCode._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objFundProjectCode._Voiduserunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objFundProjectCode._FormName = mstrModuleName
                objFundProjectCode._LoginEmployeeunkid = 0
                objFundProjectCode._ClientIP = getIP()
                objFundProjectCode._HostName = getHostName()
                objFundProjectCode._FromWeb = False
                objFundProjectCode._AuditUserId = User._Object._Userunkid
objFundProjectCode._CompanyUnkid = Company._Object._Companyunkid
                objFundProjectCode._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objFundProjectCode.Delete(CInt(dgvFundProjectCode.SelectedRows(0).Cells(objdgcolhfundprojectcodeunkid.Index).Value), ConfigParameter._Object._CurrentDateAndTime) Then
                    Call FillList()
                    Call FillCombo()
                Else
                    If objFundProjectCode._Message <> "" Then
                        eZeeMsgBox.Show(objFundProjectCode._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboProjectCode.SelectedValue = 0
            cboFundName.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFund_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchFund.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboFundName.ValueMember
                .DisplayMember = cboFundName.DisplayMember
                .DataSource = CType(cboFundName.DataSource, DataTable)
                .CodeMember = "fundcode"
            End With

            If objfrm.DisplayDialog Then
                cboFundName.SelectedValue = objfrm.SelectedValue
                cboFundName.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFund_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchProjectCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProjectCode.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboProjectCode.ValueMember
                .DisplayMember = cboProjectCode.DisplayMember
                .DataSource = CType(cboProjectCode.DataSource, DataTable)
                .CodeMember = "projectcode"
            End With

            If objfrm.DisplayDialog Then
                cboProjectCode.SelectedValue = objfrm.SelectedValue
                cboProjectCode.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProjectCode_Click", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblFundName.Text = Language._Object.getCaption(Me.lblFundName.Name, Me.lblFundName.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhProjectCode.HeaderText = Language._Object.getCaption(Me.dgcolhProjectCode.Name, Me.dgcolhProjectCode.HeaderText)
			Me.dgcolhprojectName.HeaderText = Language._Object.getCaption(Me.dgcolhprojectName.Name, Me.dgcolhprojectName.HeaderText)
			Me.dgcolhFundName.HeaderText = Language._Object.getCaption(Me.dgcolhFundName.Name, Me.dgcolhFundName.HeaderText)
			Me.dgcolhCurrentCeilingBal.HeaderText = Language._Object.getCaption(Me.dgcolhCurrentCeilingBal.Name, Me.dgcolhCurrentCeilingBal.HeaderText)
			Me.dgcolhExpiryDate.HeaderText = Language._Object.getCaption(Me.dgcolhExpiryDate.Name, Me.dgcolhExpiryDate.HeaderText)
			Me.dgcolhNotifyAmount.HeaderText = Language._Object.getCaption(Me.dgcolhNotifyAmount.Name, Me.dgcolhNotifyAmount.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one row for further operation.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to Delete this Fund Project code?")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot Delete this Fund Project code. Reason: This Fund Project code is in use.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class