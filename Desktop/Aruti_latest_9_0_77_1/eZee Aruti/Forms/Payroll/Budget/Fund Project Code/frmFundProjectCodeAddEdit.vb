﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmFundProjectCodeAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFundProjectCodeAddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private objFundProjectCode As clsFundProjectCode
    Private mintFundProjectCodeunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFundProjectCodeunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFundProjectCodeunkid = intFundProjectCodeunkid
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Form's Events "

    Private Sub frmFundProjectCodeAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objFundProjectCode = Nothing
    End Sub

    Private Sub frmFundProjectCodeAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmFundProjectCodeAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmFundProjectCodeAddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFundProjectCode.SetMessages()
            objfrm._Other_ModuleNames = "clsFundProjectCode"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmFundProjectCodeAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objFundProjectCode = New clsFundProjectCode
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objFundProjectCode._FundProjectCodeunkid = mintFundProjectCodeunkid
                Call GetValue()
            End If

            txtProjectCode.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundProjectCodeAddEdit_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtProjectCode.BackColor = GUI.ColorComp
            txtProjectName.BackColor = GUI.ColorComp
            cboFundName.BackColor = GUI.ColorComp
            txtCurrentCeilingBal.BackColor = GUI.ColorOptional
            txtNotifyAmount.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtProjectCode.Text = objFundProjectCode._FundProjectCode
            txtProjectName.Text = objFundProjectCode._FundProjectName
            cboFundName.SelectedValue = objFundProjectCode._FundSourceunkid
            txtCurrentCeilingBal.Text = Format(objFundProjectCode._CurrentCeilingBalance, GUI.fmtCurrency)
            If menAction = enAction.EDIT_ONE Then
                dtExpiryDate.Value = objFundProjectCode._FundProjectCodeExpiryDate.Date
            End If
            txtNotifyAmount.Decimal = objFundProjectCode._Notify_Amount
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objFundProjectCode._FundProjectCode = txtProjectCode.Text.Trim
            objFundProjectCode._FundProjectName = txtProjectName.Text.Trim
            objFundProjectCode._FundSourceunkid = CInt(cboFundName.SelectedValue)
            objFundProjectCode._CurrentCeilingBalance = txtCurrentCeilingBal.Decimal
            objFundProjectCode._FundProjectCodeExpiryDate = dtExpiryDate.Value
            objFundProjectCode._Notify_Amount = txtNotifyAmount.Decimal
            objFundProjectCode._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If txtProjectCode.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Project Code cannot be blank. Project Code is required information."), enMsgBoxStyle.Information)
                txtProjectCode.Focus()
                Return False
            ElseIf txtProjectName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Project Name cannot be blank. Project Name is required information."), enMsgBoxStyle.Information)
                txtProjectName.Focus()
                Return False
            ElseIf CInt(cboFundName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Fund Name is mandatory. Please select Fund Name."), enMsgBoxStyle.Information)
                cboFundName.Focus()
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub FillCombo()
        Dim objFund As New clsFundSource_Master
        Dim dsList As DataSet
        Try

            dsList = objFund.GetComboList("List", True)
            With cboFundName
                .ValueMember = "fundsourceunkid"
                .DisplayMember = "fundname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objFund = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objFundProjectCode._FormName = mstrModuleName
            objFundProjectCode._LoginEmployeeunkid = 0
            objFundProjectCode._ClientIP = getIP()
            objFundProjectCode._HostName = getHostName()
            objFundProjectCode._FromWeb = False
            objFundProjectCode._AuditUserId = User._Object._Userunkid
objFundProjectCode._CompanyUnkid = Company._Object._Companyunkid
            objFundProjectCode._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objFundProjectCode.Update(ConfigParameter._Object._CurrentDateAndTime)
            Else
                blnFlag = objFundProjectCode.Insert(ConfigParameter._Object._CurrentDateAndTime)
            End If

            If blnFlag = False And objFundProjectCode._Message <> "" Then
                eZeeMsgBox.Show(objFundProjectCode._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objFundProjectCode = New clsFundProjectCode
                    Call GetValue()
                    txtProjectCode.Focus()
                Else
                    mintFundProjectCodeunkid = objFundProjectCode._FundProjectCodeunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtProjectName.Text, objFundProjectCode._FundProjectName1, objFundProjectCode._FundProjectName2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub


    'Varsha (05 Dec 2017) -- Start
    'Bug: Search is not working
    Private Sub objbtnSearchFund_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFund.Click
        Dim objFrm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If


            With objFrm
                .ValueMember = cboFundName.ValueMember
                .DisplayMember = cboFundName.DisplayMember
                .DataSource = CType(cboFundName.DataSource, DataTable)
                .CodeMember = "fundcode"
            End With

            If objFrm.DisplayDialog Then
                cboFundName.SelectedValue = objFrm.SelectedValue
                cboFundName.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFund_Click", mstrModuleName)
        End Try
    End Sub
    'Varsha (05 Dec 2017) -- End

    

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFundSources.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFundSources.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbFundSources.Text = Language._Object.getCaption(Me.gbFundSources.Name, Me.gbFundSources.Text)
            Me.lblProjectCode.Text = Language._Object.getCaption(Me.lblProjectCode.Name, Me.lblProjectCode.Text)
            Me.lblCurrentCeilingBal.Text = Language._Object.getCaption(Me.lblCurrentCeilingBal.Name, Me.lblCurrentCeilingBal.Text)
            Me.lblProjectName.Text = Language._Object.getCaption(Me.lblProjectName.Name, Me.lblProjectName.Text)
            Me.lblExpiryDate.Text = Language._Object.getCaption(Me.lblExpiryDate.Name, Me.lblExpiryDate.Text)
            Me.lblFundSource.Text = Language._Object.getCaption(Me.lblFundSource.Name, Me.lblFundSource.Text)
            Me.lblNotifyAmount.Text = Language._Object.getCaption(Me.lblNotifyAmount.Name, Me.lblNotifyAmount.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code cannot be blank. Code is required information.")
            Language.setMessage(mstrModuleName, 2, "Fund Name cannot be blank. Fund Name is required information.")
            Language.setMessage(mstrModuleName, 3, "Project Code cannot be blank. Project Code is required information.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class