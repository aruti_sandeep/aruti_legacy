﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

Public Class frmEmployeeBank_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeBank_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objEmpBanks As clsEmployeeBanks
    Private mintEmpBankTranUnkid As Integer = -1
    Private mintSelectedEmployee As Integer = -1
    'Sohail (16 Oct 2010) -- Start
    Private mintEmployeeUnkid As Integer = -1
    Private mintItemIndex As Integer = -1
    Private mdblTotPerc As Double = 0
    Private mdtTran As DataTable
    'Sohail (16 Oct 2010) -- End

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    Private mintPeriod_Status As Integer
    'Sohail (25 Apr 2014) -- End

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtOldTable As DataTable
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmpId As Integer = -1) As Boolean 'Sohail (16 Oct 2010)
        Try
            mintEmpBankTranUnkid = intUnkId
            menAction = eAction
            mintEmployeeUnkid = intEmpId 'Sohail (16 Oct 2010)

            Me.ShowDialog()

            intUnkId = mintEmpBankTranUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboAccountType.BackColor = GUI.ColorComp
            cboBankBranch.BackColor = GUI.ColorComp
            cboBankGroup.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            txtAccountNo.BackColor = GUI.ColorComp
            'Sohail (16 Oct 2010) -- Start
            txtPercentage.BackColor = GUI.ColorComp
            'Sohail (16 Oct 2010) -- End
            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            cboMode.BackColor = GUI.ColorComp
            'Sohail (21 Apr 2014) -- End
            cboPeriod.BackColor = GUI.ColorComp 'Sohail (25 Apr 2014)
            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            cboPaymentType.BackColor = GUI.ColorComp
            txtMobileNo.BackColor = GUI.ColorComp
            'Sohail (12 Jun 2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEmpBanks._Accountno = txtAccountNo.Text
            objEmpBanks._Accounttypeunkid = CInt(cboAccountType.SelectedValue)
            objEmpBanks._Branchunkid = CInt(cboBankBranch.SelectedValue)
            objEmpBanks._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            objEmpBanks._Payment_typeid = CInt(cboPaymentType.SelectedValue)
            objEmpBanks._Mobileno = txtMobileNo.Text
            'Sohail (12 Jun 2023) -- End
            If mintEmpBankTranUnkid = -1 Then
                objEmpBanks._Isvoid = False
                objEmpBanks._Userunkid = User._Object._Userunkid
                objEmpBanks._Voiddatetime = Nothing
                objEmpBanks._Voidreason = ""
                objEmpBanks._Voiduserunkid = -1
            Else
                objEmpBanks._Isvoid = objEmpBanks._Isvoid
                objEmpBanks._Userunkid = objEmpBanks._Userunkid
                objEmpBanks._Voiddatetime = objEmpBanks._Voiddatetime
                objEmpBanks._Voidreason = objEmpBanks._Voidreason
                objEmpBanks._Voiduserunkid = objEmpBanks._Voiduserunkid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAccountNo.Text = objEmpBanks._Accountno
            cboAccountType.SelectedValue = objEmpBanks._Accounttypeunkid
            cboBankBranch.SelectedValue = objEmpBanks._Branchunkid
            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            If menAction = enAction.EDIT_ONE Then
                cboPeriod.SelectedValue = objEmpBanks._Periodunkid
            End If
            cboPaymentType.SelectedValue = objEmpBanks._Payment_typeid
            txtMobileNo.Text = objEmpBanks._Mobileno
            'Sohail (12 Jun 2023) -- End
            Dim objBranch As New clsbankbranch_master
            objBranch._Branchunkid = objEmpBanks._Branchunkid
            cboBankGroup.SelectedValue = objBranch._Bankgroupunkid
            cboEmployee.SelectedValue = objEmpBanks._Employeeunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        'Dim objEmp As New clsEmployee_Master 'Sohail (25 Apr 2014)
        Dim objBank As New clspayrollgroup_master
        Dim objAccType As New clsBankAccType
        Dim objMaster As New clsMasterData
        'Sohail (25 Apr 2014) -- Start
        'Enhancement - Employee Bank Details Period Wise.
        Dim objPeriod As New clscommom_period_Tran
        Dim intCurrPeriodID As Integer
        'Sohail (25 Apr 2014) -- End

        Try


            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombo = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombo = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombo = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , , False)
            'End If
            'Sohail (25 Apr 2014) -- End
            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombo.Tables("Emp")
            '    .SelectedValue = 0
            'End With
            'Sohail (25 Apr 2014) -- End

            dsCombo = objBank.getListForCombo(enGroupType.BankGroup, "Banks", True)
            With cboBankGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Banks")
                .SelectedValue = 0
            End With


            dsCombo = objAccType.getComboList(True, "AccType")
            With cboAccountType
                .ValueMember = "accounttypeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("AccType")
                .SelectedValue = 0
            End With

            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            dsCombo = objMaster.GetPaymentBy("Mode")
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Mode")
                .SelectedValue = 0
            End With
            'Sohail (21 Apr 2014) -- End

            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            dsCombo = objMaster.GetBankPaymentType("List")
            With cboPaymentType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = enBankPaymentType.BankAccount
            End With
            'Sohail (12 Jun 2023) -- End

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
            intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, 0)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = intCurrPeriodID
            End With
            'Sohail (25 Apr 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
        Finally
            'objEmp = Nothing 'Sohail (25 Apr 2014)
            objBank = Nothing
            objAccType = Nothing
            objMaster = Nothing
            'Sohail (21 Apr 2014) -- End
            objPeriod = Nothing 'Sohail (25 Apr 2014)
        End Try
    End Sub

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPeriod_startdate, mdtPeriod_enddate)

            'S.SANDEEP [20-JUN-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               mdtPeriod_startdate, _
            '                               mdtPeriod_enddate, _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, False, "EmployeeList", True)

            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployeeBankList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriod_startdate, _
                                           mdtPeriod_enddate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           mblnOnlyApproved, False, "EmployeeList", True, , , , , , , , , , , , , , , , , , mblnAddApprovalCondition)
            'S.SANDEEP [20-JUN-2019] -- Start

            'Anjan [10 June 2015] -- End

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo_Employee", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub
    'Sohail (25 Apr 2014) -- End

    Private Function IsValid() As Boolean
        Try
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            If cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf mintPeriod_Status = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            'Sohail (25 Apr 2014) -- End

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus() 'Sohail (16 Oct 2010)
                Return False
            End If

            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            If CInt(cboPaymentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Payment Type is compulsory information. Please select Payment Type to continue."), enMsgBoxStyle.Information)
                cboPaymentType.Focus()
                Return False
            End If
            'Sohail (12 Jun 2023) -- End

            If CInt(cboPaymentType.SelectedValue) = enBankPaymentType.BankAccount Then 'Sohail (12 Jun 2023)
                If CInt(cboBankGroup.SelectedValue) <= 0D Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Bank Group is compulsory information. Please select Bank Group to continue."), enMsgBoxStyle.Information)
                cboBankGroup.Focus() 'Sohail (16 Oct 2010)
                Return False
            End If

            If CInt(cboBankBranch.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Bank Branch is compulsory information. Please select Bank Branch to continue."), enMsgBoxStyle.Information)
                cboBankBranch.Focus() 'Sohail (16 Oct 2010)
                Return False
            End If

            If CInt(cboAccountType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Account Type is compulsory information. Please select Account Type to continue."), enMsgBoxStyle.Information)
                cboAccountType.Focus() 'Sohail (16 Oct 2010)
                Return False
            End If

            If txtAccountNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Account No cannot be blank. Please enter Account No."), enMsgBoxStyle.Information)
                'Sohail (30 Aug 2010) -- Start
                txtAccountNo.Focus()
                Return False
                'Sohail (30 Aug 2010) -- End
            End If

                'Sohail (12 Jun 2023) -- Start
                'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            Else
                If txtMobileNo.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Mobile No. cannot be blank. Please enter Mobile No."), enMsgBoxStyle.Information)
                    txtMobileNo.Focus()
                    Return False
                End If
            End If
            'Sohail (12 Jun 2023) -- End


            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            If CInt(cboMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please select Salary Distribution Mode."), enMsgBoxStyle.Information)
                cboMode.Focus()
                Return False
            End If
            'Sohail (21 Apr 2014) -- End

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            Dim intCount As Integer = (From lv In lvEmpBankList.Items.Cast(Of ListViewItem)() Where (CInt(lv.SubItems(colhPeriod.Index).Tag) = CInt(cboPeriod.SelectedValue) AndAlso CInt(lv.SubItems(colhModeID.Index).Text) <> CInt(cboMode.SelectedValue)) Select (lv.SubItems(colhModeID.Index).Text)).Count()
            If intCount > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, You can use one Mode only in one Period."), enMsgBoxStyle.Information)
                cboMode.Focus()
                Return False
            End If
            'Sohail (25 Apr 2014) -- End

            'Sohail (16 Oct 2010) -- Start
            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            'If txtPercentage.Decimal > 100 Then
            If CInt(cboMode.SelectedValue) = enPaymentBy.Percentage AndAlso txtPercentage.Decimal > 100 Then
                'Sohail (21 Apr 2014) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Percentage should not be greater than 100."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
                'Sohail (21 Apr 2014) -- Start
                'Enhancement - Salary Distribution by Amount and bank priority.
            ElseIf CInt(cboMode.SelectedValue) = enPaymentBy.Percentage AndAlso txtPercentage.Decimal < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, Percentage should be greater than Zero."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = enPaymentBy.Value AndAlso txtPercentage.Decimal < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Amount should be greater than Zero."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
                'Sohail (21 Apr 2014) -- End
            End If
            'Sohail (16 Oct 2010) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    'Sohail (16 Oct 2010) -- Start
    Private Sub FillList()
        Try
            lvEmpBankList.Items.Clear()
            mdblTotPerc = 0
            Dim lvArray As New List(Of ListViewItem)
            Dim i As Integer = 1 'Sohail (21 Apr 2014)

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            If cboPeriod.SelectedValue Is Nothing OrElse CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            Dim intPrevPeriodID As Integer = 0
            'Sohail (25 Apr 2014) -- End

            lvEmpBankList.BeginUpdate()

            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTran.Rows
                If intPrevPeriodID > 0 AndAlso intPrevPeriodID <> CInt(dtRow.Item("periodunkid")) Then Exit For 'Sohail (25 Apr 2014)

                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    'Sohail (21 Apr 2014) -- Start
                    'Enhancement - Salary Distribution by Amount and bank priority.
                    'lvItem.Text = dtRow.Item("empbanktranunkid").ToString
                    lvItem.Text = i.ToString
                    'Sohail (21 Apr 2014) -- End
                    lvItem.Tag = dtRow.Item("empbanktranunkid")

                    lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)                                        'Employee
                    lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow.Item("employeeunkid").ToString)        'EmployeeUnkid

                    lvItem.SubItems.Add(dtRow.Item("BankGrp").ToString)                                        'Bank Group
                    lvItem.SubItems(colhBankGrp.Index).Tag = CInt(dtRow.Item("BankGrpUnkid").ToString)       'Bank Group Unkid

                    lvItem.SubItems.Add(dtRow.Item("BranchName").ToString)                                     'Bank Branch
                    lvItem.SubItems(colhBankBranch.Index).Tag = CInt(dtRow.Item("branchunkid").ToString)       'Branch Unkid

                    lvItem.SubItems.Add(dtRow.Item("AccName").ToString)                                    'Account Type
                    lvItem.SubItems(colhAccType.Index).Tag = CInt(dtRow.Item("accounttypeunkid").ToString)     'Acc TypeUnkid

                    lvItem.SubItems.Add(dtRow.Item("accountno").ToString)                                      'Account No

                    'Sohail (12 Jun 2023) -- Start
                    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                    lvItem.SubItems.Add(dtRow.Item("mobileno").ToString)
                    'Sohail (12 Jun 2023) -- End

                    'Sohail (21 Apr 2014) -- Start
                    'Enhancement - Salary Distribution by Amount and bank priority.
                    'lvItem.SubItems.Add(Format(CDec(dtRow.Item("percentage")), "00.00"))                       'percentage
                    'mdblTotPerc += CDec(dtRow.Item("percentage").ToString)
                    If CInt(dtRow.Item("modeid")) = enPaymentBy.Value Then
                        lvItem.SubItems.Add(Format(CDec(dtRow.Item("amount")), "00.00"))                       'amount
                    Else
                        lvItem.SubItems.Add(Format(CDec(dtRow.Item("percentage")), "00.00"))                       'percentage
                        mdblTotPerc += CDec(dtRow.Item("percentage").ToString)
                    End If
                    'Sohail (21 Apr 2014) -- End
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)                                           'GUID
                    lvItem.SubItems(colhGUID.Index).Tag = CInt(dtRow.Item("UnkId")) 'Sohail (21 Apr 2014)

                    'Sohail (25 Apr 2014) -- Start
                    'Enhancement - Employee Bank Details Period Wise.
                    lvItem.SubItems.Add(dtRow.Item("period_name").ToString)
                    lvItem.SubItems(colhPeriod.Index).Tag = CInt(dtRow.Item("periodunkid"))
                    lvItem.SubItems.Add(dtRow.Item("modeid").ToString)
                    'Sohail (25 Apr 2014) -- End
                    'Sohail (12 Jun 2023) -- Start
                    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                    lvItem.SubItems.Add(dtRow.Item("payment_typeid").ToString)
                    'Sohail (12 Jun 2023) -- End

                    lvArray.Add(lvItem)

                    lvItem = Nothing

                    'Sohail (21 Apr 2014) -- Start
                    'Enhancement - Salary Distribution by Amount and bank priority.
                    cboMode.SelectedValue = CInt(dtRow.Item("modeid"))
                    'cboMode.Enabled = False 'Sohail (25 Apr 2014)

                    If CInt(dtRow.Item("priority")) <> i Then
                        dtRow.Item("priority") = i
                        If dtRow.Item("AUD").ToString <> "A" Then
                            dtRow.Item("AUD") = "U"
                        End If
                        dtRow.AcceptChanges()
                    End If
                    i += 1
                    'Sohail (21 Apr 2014) -- End

                End If

                intPrevPeriodID = CInt(dtRow.Item("periodunkid")) 'Sohail (25 Apr 2014)

            Next
            lvEmpBankList.Items.AddRange(lvArray.ToArray)
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            lvEmpBankList.GroupingColumn = colhPeriod
            lvEmpBankList.DisplayGroups(True)
            'Sohail (25 Apr 2014) -- End
            lvEmpBankList.EndUpdate()

            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'If mdtTran.Rows.Count <= 0 Then
            '    cboMode.Enabled = True
            'End If
            'Sohail (25 Apr 2014) -- End
            'Sohail (21 Apr 2014) -- End

            If lvEmpBankList.Items.Count > 5 Then
                colhBankGrp.Width = 150 - 18
            Else
                colhBankGrp.Width = 150
            End If

            Call ResetValue()
        Catch ex As Exception
            lvEmpBankList.EndUpdate()
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboAccountType.SelectedValue = 0
            cboBankBranch.SelectedValue = 0
            cboBankGroup.SelectedValue = 0
            txtAccountNo.Text = ""
            'Sohail (12 Oct 2011) -- Start
            'txtPercentage.Text = "00.00"
            txtPercentage.Text = "100.00"
            'Sohail (12 Oct 2011) -- End
            btnAdd.Enabled = True
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            cboMode.Enabled = True
            'Sohail (25 Apr 2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Oct 2010) -- End

    Private Sub SetVisibility()

        Try
            objbtnAddAccType.Enabled = User._Object.Privilege._AddBankAccType
            objbtnAddBankGroup.Enabled = User._Object.Privilege._AddPayrollGroup
            objbtnAddBranch.Enabled = User._Object.Privilege._AddBankBranch

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim objEmployee As New clsEmployee_Master
        Dim blnFlag As Boolean = False
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 31, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 32, "This is to inform you that changes have been made for employee") & " " & "<b>" & " " & getTitleCase(objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 33, "with employeecode") & " " & "<b>" & " " & objEmployee._Employeecode & "</b>." & " " & Language.getMessage(mstrModuleName, 34, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 35, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage(mstrModuleName, 36, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")

                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enNotificationBank.EMPLOYEE_BANK
                            '///////////// NEWLY ADDED ROWS -- START
                            Dim dTemp() As DataRow = mdtTran.Select("AUD = 'A'")
                            If dTemp.Length > 0 Then
                                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= '#58ACFA'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '5'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 12, "NEWLY ADDED INFORMATION") & "</span></b></TD>")
                                StrMessage.Append("</TR>")
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhBankGrp.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhBankBranch.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhAccType.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhAccNo.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'RIGHT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhPerc.Text & "</span></b></TD>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TR>")
                                For i As Integer = 0 To dTemp.Length - 1
                                    StrMessage.Append("<TR WIDTH = '50%'>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BankGrp").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("branchname").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("AccName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("accountno").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'RIGHT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDbl(dTemp(i).Item("percentage")), "###.#0") & "</span></TD>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("</TR>")
                                Next
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TABLE>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<BR>")
                            End If
                            '///////////// NEWLY ADDED ROWS -- END

                            '///////////// EDITED ROWS -- START
                            dTemp = mdtTran.Select("AUD = 'U' AND empbanktranunkid > 0")
                            Dim iRowIdx As Integer = -1
                            If dTemp.Length > 0 Then
                                StrMessage.Append("<TABLE border = '1' WIDTH = '60%'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TR WIDTH = '60%' bgcolor= '#58ACFA'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '6'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 13, "EDITED INFORMATION") & "</span></b></TD>")
                                StrMessage.Append("</TR>")
                                StrMessage.Append("<TR WIDTH = '60%' bgcolor= 'SteelBlue'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 14, "Value Type") & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhBankGrp.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhBankBranch.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhAccType.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhAccNo.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'RIGHT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhPerc.Text & "</span></b></TD>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TR>")
                                For i As Integer = 0 To dTemp.Length - 1
                                    iRowIdx = -1
                                    iRowIdx = mdtTran.Rows.IndexOf(dTemp(i))
                                    If iRowIdx >= 0 Then
                                        StrMessage.Append("<TR WIDTH = '60%'>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 15, "Old") & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("BankGrp").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("branchname").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("AccName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & mdtOldTable.Rows(iRowIdx).Item("accountno").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'RIGHT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDbl(mdtOldTable.Rows(iRowIdx).Item("percentage")), "###.#0") & "</span></TD>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("</TR>")

                                        StrMessage.Append("<TR WIDTH = '60%'>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 16, "New") & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BankGrp").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("branchname").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("AccName").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("accountno").ToString & "</span></TD>")
                                        StrMessage.Append("<TD align = 'RIGHT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDbl(dTemp(i).Item("percentage")), "###.#0") & "</span></TD>")
                                        StrMessage.Append(vbCrLf)
                                        StrMessage.Append("</TR>")

                                        If dTemp.Length > 1 Then
                                            StrMessage.Append("<TR WIDTH = '60%'>")
                                            StrMessage.Append(vbCrLf)
                                            StrMessage.Append("<TD COLSPAN='6'> &nbsp; </TD>")
                                            StrMessage.Append(vbCrLf)
                                            StrMessage.Append("</TR>")
                                        End If
                                    End If
                                Next
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TABLE>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<BR>")
                            End If
                            '///////////// EDITED ROWS -- END

                            '///////////// DELETED ROWS -- START
                            dTemp = mdtTran.Select("AUD = 'D' AND empbanktranunkid > 0")
                            If dTemp.Length > 0 Then
                                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= '#58ACFA'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '5'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 17, "DELETED INFORMATION") & "</span></b></TD>")
                                StrMessage.Append("</TR>")
                                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhBankGrp.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhBankBranch.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhAccType.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhAccNo.Text & "</span></b></TD>")
                                StrMessage.Append("<TD align = 'RIGHT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & colhPerc.Text & "</span></b></TD>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TR>")
                                For i As Integer = 0 To dTemp.Length - 1
                                    StrMessage.Append("<TR WIDTH = '50%'>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("BankGrp").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("branchname").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("AccName").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dTemp(i).Item("accountno").ToString & "</span></TD>")
                                    StrMessage.Append("<TD align = 'RIGHT' STYLE='WIDTH:10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Format(CDbl(dTemp(i).Item("percentage")), "###.#0") & "</span></TD>")
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("</TR>")
                                Next
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("</TABLE>")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<BR>")
                            End If
                            '///////////// DELETED ROWS -- END
                            blnFlag = True
                    End Select
                Next


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")



                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                If blnFlag = False Then
                    StrMessage = StrMessage.Remove(0, StrMessage.Length)
                End If
            End If
            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If dicNotification.Keys.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each sKey As String In dicNotification.Keys
                    If dicNotification(sKey).Trim.Length > 0 Then
                        objSendMail._ToEmail = sKey
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 18, "Notification of Changes in Employee Bank(s).")
                        objSendMail._Message = dicNotification(sKey)
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        'Sohail (17 Dec 2014) -- Start
                        'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                        'objSendMail._Form_Name = mstrModuleName
                        objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                        'Sohail (17 Dec 2014) -- End
                        objSendMail._LogEmployeeUnkid = -1
                        objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                        objSendMail._UserUnkid = User._Object._Userunkid
                        objSendMail._SenderAddress = User._Object._Email
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                        With objSendMail
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    End If
                Next
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeBank_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmpBanks = Nothing
    End Sub

    Private Sub frmEmployeeBank_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeBank_AddEdit_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeBank_AddEdit_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpBanks = New clsEmployeeBanks
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objEmpBanks._Empbanktranunkid = mintEmpBankTranUnkid
            End If

            Call GetValue()

            cboEmployee.Focus()

            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            lblMobileNo.Location = lblAccountNo.Location
            lblMobileNo.Size = lblAccountNo.Size
            txtMobileNo.Location = txtAccountNo.Location
            txtMobileNo.Size = txtAccountNo.Size
            'Sohail (12 Jun 2023) -- End


            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            If menAction = enAction.EDIT_ONE Then
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboPeriod.Enabled = False 'Sohail (25 Apr 2014)
                cboPaymentType.Enabled = False 'Sohail (12 Jun 2023)
            End If
            'Sandeep [ 09 Oct 2010 ] -- End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAssets_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeBanks.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeBanks"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnSaveInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            If cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (25 Apr 2014) -- End

            'Sohail (16 Oct 2010) -- Start
            'If IsValid() = False Then Exit Sub
            If mdtTran.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please add atleast one employee bank."), enMsgBoxStyle.Information)
                Exit Sub
                'Sohail (21 Apr 2014) -- Start
                'Enhancement - Salary Distribution by Amount and bank priority.
                'ElseIf lvEmpBankList.Items.Count > 0 AndAlso mdblTotPerc <> 100 Then
            ElseIf lvEmpBankList.Items.Count > 0 AndAlso CInt(cboMode.SelectedValue) = enPaymentBy.Percentage AndAlso mdblTotPerc <> 100 Then
                'Sohail (21 Apr 2014) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Total Percentage must be 100."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Sohail (26 Nov 2011) -- Start
            For Each dtRow As DataRow In mdtTran.Rows
                If Not IsDBNull(dtRow.Item("AUD")) Then
                    Select Case dtRow.Item("AUD").ToString
                        Case "A"
                            If CInt(dtRow.Item("payment_typeid").ToString) = enBankPaymentType.BankAccount Then 'Sohail (12 Jun 2023)
                            objEmpBanks._Branchunkid = CInt(dtRow.Item("branchunkid").ToString)
                            'Sohail (29 Apr 2014) -- Start
                            'If objEmpBanks.isExist(-1, dtRow.Item("accountno").ToString) = True Then
                            If objEmpBanks.isExist(-1, dtRow.Item("accountno").ToString, , CInt(cboPeriod.SelectedValue)) = True Then
                                'Sohail (29 Apr 2014) -- End
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This Account is already defined for particular employee.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 30, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                    Exit Sub
                                End If
                            End If
                                'Sohail (12 Jun 2023) -- Start
                                'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                            Else
                                If objEmpBanks.isExistMobileNo(CInt(cboPeriod.SelectedValue), dtRow.Item("mobileno").ToString) = True Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Sorry, This Mobile No. is already defined."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                            'Sohail (12 Jun 2023) -- End
                        Case "U"
                            If CInt(dtRow.Item("payment_typeid").ToString) = enBankPaymentType.BankAccount Then 'Sohail (12 Jun 2023)
                            objEmpBanks._Branchunkid = CInt(dtRow.Item("branchunkid").ToString)
                            'Sohail (29 Apr 2014) -- Start
                            'If objEmpBanks.isExist(-1, dtRow.Item("accountno").ToString, CInt(dtRow.Item("empbanktranunkid").ToString)) = True Then
                            If objEmpBanks.isExist(-1, dtRow.Item("accountno").ToString, CInt(dtRow.Item("empbanktranunkid").ToString), CInt(cboPeriod.SelectedValue)) = True Then
                                'Sohail (29 Apr 2014) -- End
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This Account is already defined for particular employee.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 30, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                    Exit Sub
                                End If
                            End If
                                'Sohail (12 Jun 2023) -- Start
                                'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                            Else
                                If objEmpBanks.isExistMobileNo(CInt(cboPeriod.SelectedValue), dtRow.Item("mobileno").ToString, CInt(dtRow.Item("empbanktranunkid").ToString)) = True Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Sorry, This Mobile No. is already defined."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                            'Sohail (12 Jun 2023) -- End

                    End Select
                End If
            Next
            'Sohail (26 Nov 2011) -- End

            'Call SetValue()

            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objEmpBanks.Update
            'Else
            '    blnFlag = objEmpBanks.Insert
            'End If

            objEmpBanks._DataTable = mdtTran
            objEmpBanks._Userunkid = User._Object._Userunkid

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objEmpBanks._FormName = mstrModuleName
            objEmpBanks._LoginEmployeeUnkid = 0
            objEmpBanks._ClientIP = getIP()
            objEmpBanks._HostName = getHostName()
            objEmpBanks._FromWeb = False
            objEmpBanks._AuditUserId = User._Object._Userunkid
objEmpBanks._CompanyUnkid = Company._Object._Companyunkid
            objEmpBanks._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objEmpBanks.InsertUpdateDelete_EmpBanks()
            blnFlag = objEmpBanks.InsertUpdateDelete_EmpBanks(ConfigParameter._Object._CurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            'Sohail (16 Oct 2010) -- End

            If blnFlag = False And objEmpBanks._Message <> "" Then
                eZeeMsgBox.Show(objEmpBanks._Message, enMsgBoxStyle.Information)
            End If


            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Pinkal (03-Sep-2013) -- Start
            'Enhancement : TRA Changes

            Dim drRow() As DataRow = mdtTran.Select("AUD <> ''")
            If drRow.Length > 0 Then

                If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If

                    Next
                    objUsr = Nothing
                End If
            End If

            'Pinkal (03-Sep-2013) -- End

            'S.SANDEEP [ 03 OCT 2012 ] -- END


            'Sohail (16 Oct 2010) -- Start
            'If blnFlag Then
            '    mblnCancel = False
            '    If menAction = enAction.ADD_CONTINUE Then
            '        objEmpBanks = Nothing
            '        objEmpBanks = New clsEmployeeBanks
            '        Call GetValue()
            '        cboEmployee.Focus()
            '    Else
            '        mintEmpBankTranUnkid = objEmpBanks._Empbanktranunkid
            '        Me.Close()
            '    End If
            'End If

            'If mintSelectedEmployee <> -1 Then
            '    cboEmployee.SelectedValue = mintSelectedEmployee
            'End If
            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If
            'Sohail (16 Oct 2010) -- End


            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'S.SANDEEP [ 03 OCT 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveInfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddAccType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddAccType.Click
        Dim frm As New frmBankAccountType_AddEdit
        Dim dsCombos As New DataSet
        Dim objAccType As New clsBankAccType
        Dim intAccTypeId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 
            If frm.displayDialog(intAccTypeId, enAction.ADD_ONE) Then
                dsCombos = objAccType.getComboList(True, "AccType")
                With cboAccountType
                    .ValueMember = "accounttypeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("AccType")
                    .SelectedValue = intAccTypeId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddAccType_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBranch.Click
        Dim frm As New frmBankBranch_AddEdit
        Dim dsCombos As New DataSet
        Dim objBranch As New clsbankbranch_master
        Dim intBranchId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(intBranchId, enAction.ADD_ONE) Then
                dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
                With cboBankBranch
                    .ValueMember = "branchunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Branch")
                    .SelectedValue = intBranchId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBranch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddBankGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBankGroup.Click
        Dim frm As New frmPayrollGroups_AddEdit
        Dim intBankGrp As Integer = -1
        Dim dsCombo As New DataSet
        Dim objBankGroup As New clspayrollgroup_master
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            frm.lblwebsite.Visible = True
            frm.txtwebsite.Visible = True
            frm.gbGroupInfo.Size = New Size(374, 208)
            frm.Size = New Size(403, 309)
            If frm.displayDialog(intBankGrp, enAction.ADD_ONE, enGroupType.BankGroup) Then
                dsCombo = objBankGroup.getListForCombo(enGroupType.BankGroup, "BankGroup", True)
                With cboBankGroup
                    .ValueMember = "groupmasterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("BankGroup")
                    .SelectedValue = intBankGrp
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBankGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (16 Oct 2010) -- Start
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            If IsValid() = False Then Exit Sub

            'Sohail (29 Apr 2014) -- Start
            'Dim dtRow As DataRow() = mdtTran.Select("accountno = '" & txtAccountNo.Text & "' AND (AUD <> 'D' OR AUD IS NULL) AND BankGrpUnkid = '" & CInt(cboBankGroup.SelectedValue) & "' ")
            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            'Dim dtRow As DataRow() = mdtTran.Select("accountno = '" & txtAccountNo.Text & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) AND BankGrpUnkid = '" & CInt(cboBankGroup.SelectedValue) & "' ")
            Dim dtRow As DataRow() = Nothing
            If CInt(cboPaymentType.SelectedValue) = enBankPaymentType.BankAccount Then
                dtRow = mdtTran.Select("accountno = '" & txtAccountNo.Text & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) AND BankGrpUnkid = '" & CInt(cboBankGroup.SelectedValue) & "' ")
            Else
                dtRow = mdtTran.Select("mobileno = '" & txtMobileNo.Text & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND (AUD <> 'D' OR AUD IS NULL) AND BankGrpUnkid = '" & CInt(cboBankGroup.SelectedValue) & "' ")
            End If

            'Sohail (12 Jun 2023) -- End
            'Sohail (29 Apr 2014) -- End

            If dtRow.Length > 0 Then
                'Sohail (12 Jun 2023) -- Start
                'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Selected Account Type is already added to the list for particular Branch."), enMsgBoxStyle.Information)
                If CInt(cboPaymentType.SelectedValue) = enBankPaymentType.BankAccount Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Selected Account Type is already added to the list for particular Branch."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Selected Mobile No. is already added to the list."), enMsgBoxStyle.Information)
                End If
                'Sohail (12 Jun 2023) -- End
                Exit Sub
            End If

            Dim dtEBRow As DataRow
            dtEBRow = mdtTran.NewRow

            dtEBRow.Item("empbanktranunkid") = -1
            dtEBRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            dtEBRow.Item("EmpName") = cboEmployee.Text
            dtEBRow.Item("BankGrpUnkid") = CInt(cboBankGroup.SelectedValue)
            dtEBRow.Item("BankGrp") = cboBankGroup.Text
            dtEBRow.Item("branchunkid") = CInt(cboBankBranch.SelectedValue)
            dtEBRow.Item("branchname") = cboBankBranch.Text
            dtEBRow.Item("accounttypeunkid") = CInt(cboAccountType.SelectedValue)
            dtEBRow.Item("AccName") = cboAccountType.Text
            dtEBRow.Item("accountno") = txtAccountNo.Text
            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            dtEBRow.Item("payment_typeid") = CInt(cboPaymentType.SelectedValue)
            If CInt(cboPaymentType.SelectedValue) = enBankPaymentType.MobileNumber Then
                dtEBRow.Item("mobileno") = txtMobileNo.Text
                dtEBRow.Item("BankGrpUnkid") = 0
                dtEBRow.Item("BankGrp") = ""
                dtEBRow.Item("branchunkid") = 0
                dtEBRow.Item("branchname") = ""
                dtEBRow.Item("accounttypeunkid") = 0
                dtEBRow.Item("AccName") = ""
                dtEBRow.Item("accountno") = ""
            Else
                dtEBRow.Item("mobileno") = ""
            End If
            'Sohail (12 Jun 2023) -- End
            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            'dtEBRow.Item("percentage") = Format(txtPercentage.Decimal, "00.00")
            If CInt(cboMode.SelectedValue) = enPaymentBy.Percentage Then
                dtEBRow.Item("percentage") = Format(txtPercentage.Decimal, "00.00")
                dtEBRow.Item("amount") = Format(0, "00.00")
                dtEBRow.Item("priority") = 1
            Else
                dtEBRow.Item("percentage") = Format(0, "00.00")
                dtEBRow.Item("amount") = Format(txtPercentage.Decimal, GUI.fmtCurrency)
                dtEBRow.Item("priority") = lvEmpBankList.Items.Count + 1
            End If
            dtEBRow.Item("modeid") = CInt(cboMode.SelectedValue)
            'Sohail (21 Apr 2014) -- End

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            dtEBRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
            dtEBRow.Item("period_name") = cboPeriod.Text
            dtEBRow.Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
            'Sohail (25 Apr 2014) -- End

            dtEBRow.Item("AUD") = "A"
            dtEBRow.Item("GUID") = Guid.NewGuid().ToString

            mdtTran.Rows.Add(dtEBRow)
            mdtTran = New DataView(mdtTran, "", "end_date DESC, EmpName", DataViewRowState.CurrentRows).ToTable  'Sohail (25 Apr 2014)

            Call FillList()

            If mdtTran.Rows.Count > 0 Then
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboPeriod.Enabled = False 'Sohail (25 Apr 2014)
            Else
                cboEmployee.Enabled = True
                objbtnSearchEmployee.Enabled = True
                cboPeriod.Enabled = True 'Sohail (25 Apr 2014)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvEmpBankList.SelectedItems.Count > 0 Then
                If mintItemIndex > -1 Then
                    If IsValid() = False Then Exit Sub 'Sohail (25 Apr 2014)

                    Dim drTemp As DataRow()
                    If CInt(lvEmpBankList.Items(mintItemIndex).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvEmpBankList.Items(mintItemIndex).SubItems(colhGUID.Index).Text & "'")
                    Else

                        'Sohail (25 Apr 2014) -- Start
                        'Enhancement - Employee Bank Details Period Wise.
                        If objEmpBanks.isUsed(CInt(lvEmpBankList.Items(mintItemIndex).Tag)) = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot delete this Employee Bank. This Employee Bank is in use."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'Sohail (25 Apr 2014) -- End

                        drTemp = mdtTran.Select("empbanktranunkid = " & CInt(lvEmpBankList.Items(mintItemIndex).Tag))
                    End If
                    If drTemp.Length > 0 Then
                        'If IsValid() = False Then Exit Sub 'Sohail (25 Apr 2014)
                        With drTemp(0)

                            'Pinkal (03-Sep-2013) -- Start
                            'Enhancement : TRA Changes
                            Dim blnIsChange As Boolean = False

                            If CInt(.Item("empbanktranunkid")) <> CInt(lvEmpBankList.Items(mintItemIndex).Tag) OrElse CInt(.Item("employeeunkid")) <> CInt(cboEmployee.SelectedValue) _
                                OrElse CStr(.Item("EmpName")) <> cboEmployee.Text OrElse CInt(.Item("BankGrpUnkid")) <> CInt(cboBankGroup.SelectedValue) _
                                OrElse CStr(.Item("BankGrp")) <> cboBankGroup.Text OrElse CInt(.Item("branchunkid")) <> CInt(cboBankBranch.SelectedValue) _
                                OrElse CStr(.Item("branchname")) <> cboBankBranch.Text OrElse CInt(.Item("accounttypeunkid")) <> CInt(cboAccountType.SelectedValue) _
                                OrElse CStr(.Item("AccName")) <> cboAccountType.Text OrElse CStr(.Item("accountno")) <> txtAccountNo.Text _
                                OrElse CStr(.Item("percentage")) <> Format(txtPercentage.Decimal, "00.00") OrElse CStr(.Item("mobileno")) <> txtMobileNo.Text Then
                                blnIsChange = True
                            End If

                            .Item("empbanktranunkid") = CInt(lvEmpBankList.Items(mintItemIndex).Tag)
                            .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                            .Item("EmpName") = cboEmployee.Text
                            .Item("BankGrpUnkid") = CInt(cboBankGroup.SelectedValue)
                            .Item("BankGrp") = cboBankGroup.Text
                            .Item("branchunkid") = CInt(cboBankBranch.SelectedValue)
                            .Item("branchname") = cboBankBranch.Text
                            .Item("accounttypeunkid") = CInt(cboAccountType.SelectedValue)
                            .Item("AccName") = cboAccountType.Text
                            .Item("accountno") = txtAccountNo.Text
                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Salary Distribution by Amount and bank priority.
                            '.Item("percentage") = Format(txtPercentage.Decimal, "00.00")
                            If CInt(cboMode.SelectedValue) = enPaymentBy.Percentage Then
                                .Item("percentage") = Format(txtPercentage.Decimal, "00.00")
                                .Item("amount") = Format(0, "00.00")
                                .Item("priority") = lvEmpBankList.SelectedItems(0).Index + 1
                            Else
                                .Item("percentage") = Format(0, "00.00")
                                .Item("amount") = Format(txtPercentage.Decimal, GUI.fmtCurrency)
                                .Item("priority") = lvEmpBankList.SelectedItems(0).Index + 1
                            End If
                            'Sohail (21 Apr 2014) -- End

                            'Sohail (12 Jun 2023) -- Start
                            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                            .Item("payment_typeid") = CInt(cboPaymentType.SelectedValue)
                            If CInt(cboPaymentType.SelectedValue) = enBankPaymentType.MobileNumber Then
                                .Item("mobileno") = txtMobileNo.Text
                                .Item("BankGrpUnkid") = 0
                                .Item("BankGrp") = ""
                                .Item("branchunkid") = 0
                                .Item("branchname") = ""
                                .Item("accounttypeunkid") = 0
                                .Item("AccName") = ""
                                .Item("accountno") = ""
                            Else
                                .Item("mobileno") = ""
                            End If
                            'Sohail (12 Jun 2023) -- End

                            'Sohail (25 Apr 2014) -- Start
                            'Enhancement - Employee Bank Details Period Wise.
                            .Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                            .Item("period_name") = cboPeriod.Text
                            .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)
                            'Sohail (25 Apr 2014) -- End

                            'If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            '    .Item("AUD") = "U"
                            'End If

                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Salary Distribution by Amount and bank priority.
                            'If blnIsChange Then
                            If blnIsChange = True AndAlso .Item("AUD").ToString <> "A" Then
                                'Sohail (21 Apr 2014) -- End
                                .Item("AUD") = "U"
                            End If
                            'Pinkal (03-Sep-2013) -- End
                            .Item("GUID") = Guid.NewGuid().ToString
                            .AcceptChanges()
                        End With
                        Call FillList()
                    End If
                End If
                If mdtTran.Rows.Count > 0 Then
                    cboEmployee.Enabled = False
                    objbtnSearchEmployee.Enabled = False
                    cboPeriod.Enabled = False 'Sohail (25 Apr 2014)
                Else
                    cboEmployee.Enabled = True
                    objbtnSearchEmployee.Enabled = True
                    cboPeriod.Enabled = True 'Sohail (25 Apr 2014)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvEmpBankList.SelectedItems.Count > 0 Then
                If mintItemIndex > -1 Then
                    If IsValid() = False Then Exit Sub 'Sohail (25 Apr 2014)

                    Dim drTemp As DataRow()
                    If CInt(lvEmpBankList.Items(mintItemIndex).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvEmpBankList.Items(mintItemIndex).SubItems(colhGUID.Index).Text & "'")
                    Else
                        'Sohail (03 Nov 2010) -- Start
                        If objEmpBanks.isUsed(CInt(lvEmpBankList.Items(mintItemIndex).Tag)) = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot delete this Employee Bank. This Employee Bank is in use."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'Sohail (03 Nov 2010) -- End
                        drTemp = mdtTran.Select("empbanktranunkid = '" & CInt(lvEmpBankList.Items(mintItemIndex).Tag) & "'")
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        mdblTotPerc -= CDec(drTemp(0).Item("percentage"))
                        Call FillList()
                    End If

                    If mdtTran.Rows.Count > 0 Then
                        cboEmployee.Enabled = False
                        objbtnSearchEmployee.Enabled = False
                        cboPeriod.Enabled = False 'Sohail (25 Apr 2014)
                    Else
                        cboEmployee.Enabled = True
                        objbtnSearchEmployee.Enabled = True
                        cboPeriod.Enabled = True 'Sohail (25 Apr 2014)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Oct 2010) -- End

    'Sohail (21 Apr 2014) -- Start
    'Enhancement - Salary Distribution by Amount and bank priority.
    Private Sub objbtnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If lvEmpBankList.SelectedItems.Count > 0 Then
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                If cboPeriod.SelectedValue Is Nothing OrElse CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please select Period."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Exit Sub
                End If
                'Sohail (25 Apr 2014) -- End

                Dim lvItem As ListViewItem = lvEmpBankList.SelectedItems(0)
                Dim idx As Integer = lvItem.Index

                If idx > 0 Then
                    Dim dr As DataRow = mdtTran.Select("UnkId = " & CInt(lvItem.SubItems(colhGUID.Index).Tag) & " ")(0)
                    Dim drNew As DataRow = mdtTran.NewRow
                    drNew.ItemArray = dr.ItemArray
                    Dim pos As Integer = mdtTran.Rows.IndexOf(dr)

                    mdtTran.Rows.Remove(dr)
                    mdtTran.Rows.InsertAt(drNew, pos - 1)

                    Call FillList()

                    lvEmpBankList.Items(pos - 1).Selected = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Try
            If lvEmpBankList.SelectedItems.Count > 0 Then
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                If cboPeriod.SelectedValue Is Nothing OrElse CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please select Period."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Exit Sub
                End If
                'Sohail (25 Apr 2014) -- End

                Dim lvItem As ListViewItem = lvEmpBankList.SelectedItems(0)
                Dim idx As Integer = lvItem.Index

                If idx < lvEmpBankList.Items.Count - 1 Then
                    Dim dr As DataRow = mdtTran.Select("UnkId = " & CInt(lvItem.SubItems(colhGUID.Index).Tag) & " ")(0)
                    Dim drNew As DataRow = mdtTran.NewRow
                    drNew.ItemArray = dr.ItemArray
                    Dim pos As Integer = mdtTran.Rows.IndexOf(dr)

                    mdtTran.Rows.Remove(dr)
                    mdtTran.Rows.InsertAt(drNew, pos + 1)

                    Call FillList()

                    lvEmpBankList.Items(pos + 1).Selected = True

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDown_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Apr 2014) -- End

    'Sohail (13 May 2014) -- Start
    'Enhancement - Provide Copy Previous Employee Bank Details option.
    Private Sub lnkCopySlab_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopySlab.LinkClicked
        Dim drRow() As DataRow
        Dim dRow As DataRow
        Dim dr As DataRow
        Dim strPrevEndDate As String = ""
        Try
            If cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                Exit Try
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mintPeriod_Status = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Try
            End If

            drRow = mdtTran.Select("end_date > '" & eZeeDate.convertDate(mdtPeriod_startdate) & "' AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry! Slab for this Period is already defined."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            drRow = mdtTran.Select("AUD <> 'D'", "end_date DESC")
            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1
                    dRow = drRow(i)

                    If strPrevEndDate <> "" AndAlso strPrevEndDate <> dRow.Item("end_date").ToString Then
                        Exit For
                    End If

                    dr = mdtTran.NewRow()
                    With dr
                        .Item("empbanktranunkid") = -1
                        .Item("employeeunkid") = CInt(dRow.Item("employeeunkid"))
                        .Item("EmpName") = dRow.Item("EmpName").ToString
                        .Item("BankGrpUnkid") = CInt(dRow.Item("BankGrpUnkid"))
                        .Item("BankGrp") = dRow.Item("BankGrp").ToString
                        .Item("branchunkid") = CInt(dRow.Item("branchunkid"))
                        .Item("branchname") = dRow.Item("branchname").ToString
                        .Item("accounttypeunkid") = CInt(dRow.Item("accounttypeunkid"))
                        .Item("AccName") = dRow.Item("AccName").ToString
                        .Item("accountno") = dRow.Item("accountno").ToString

                        If CInt(cboMode.SelectedValue) = enPaymentBy.Percentage Then
                            If CInt(dRow.Item("modeid")) = enPaymentBy.Percentage Then
                                .Item("percentage") = Format(CDec(dRow.Item("percentage")), "00.00")
                            ElseIf i = 0 Then
                                .Item("percentage") = Format(100, "00.00")
                            Else
                                .Item("percentage") = Format(0, "00.00")
                            End If
                            .Item("amount") = Format(0, "00.00")
                            .Item("priority") = 1
                        Else
                            .Item("percentage") = Format(0, "00.00")
                            If CInt(dRow.Item("modeid")) = enPaymentBy.Value Then
                                .Item("amount") = Format(CDec(dRow.Item("amount")), GUI.fmtCurrency)
                            Else
                                .Item("amount") = Format(100, GUI.fmtCurrency)
                            End If
                            .Item("priority") = i + 1
                        End If
                        .Item("modeid") = CInt(cboMode.SelectedValue)

                        .Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        .Item("period_name") = cboPeriod.Text
                        .Item("end_date") = eZeeDate.convertDate(mdtPeriod_enddate)

                        .Item("AUD") = "A"
                        .Item("GUID") = Guid.NewGuid().ToString


                    End With
                    mdtTran.Rows.Add(dr)

                    strPrevEndDate = dRow.Item("end_date").ToString

                Next

                mdtTran = New DataView(mdtTran, "", "end_date DESC, EmpName", DataViewRowState.CurrentRows).ToTable

                Call FillList()

                If mdtTran.Rows.Count > 0 Then
                    cboEmployee.Enabled = False
                    objbtnSearchEmployee.Enabled = False
                    cboPeriod.Enabled = False
                Else
                    cboEmployee.Enabled = True
                    objbtnSearchEmployee.Enabled = True
                    cboPeriod.Enabled = True
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopySlab_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 May 2014) -- End

    'Sohail (12 Jun 2023) -- Start
    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
    Private Sub EnableDisableMobileNo(ByVal enableMobileno As Boolean)
        Try
            lblBankGroup.Visible = Not enableMobileno
            cboBankGroup.Visible = Not enableMobileno
            objbtnSearchBankGroup.Visible = Not enableMobileno
            objbtnAddBankGroup.Visible = Not enableMobileno

            lblBank.Visible = Not enableMobileno
            cboBankBranch.Visible = Not enableMobileno
            objbtnSearchBankBranch.Visible = Not enableMobileno
            objbtnAddBranch.Visible = Not enableMobileno

            cboAccountType.Enabled = Not enableMobileno
            objbtnAddAccType.Enabled = Not enableMobileno
            If enableMobileno = True Then cboAccountType.SelectedValue = 0

            lblAccountNo.Visible = Not enableMobileno
            txtAccountNo.Visible = Not enableMobileno

            lblMobileNo.Visible = enableMobileno
            txtMobileNo.Visible = enableMobileno

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableDisableMobileNo", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Jun 2023) -- End

#End Region

#Region " Controls "
    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objBranch As New clsbankbranch_master
        Try
            'If CInt(cboBankGroup.SelectedValue) > 0 Then 'Sohail (10 Aug 2012)
            dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            With cboBankBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                '.SelectedValue = 0
                If menAction = enAction.EDIT_ONE Then
                    .SelectedValue = objEmpBanks._Branchunkid
                Else
                    .SelectedValue = 0
                End If
                'Sandeep [ 09 Oct 2010 ] -- End 
            End With
            'End If 'Sohail (10 Aug 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSelectedEmployee = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    'Sohail (16 Oct 2010) -- Start
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'objEmpBanks._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'mdtTran = objEmpBanks._DataTable
            Dim ds As DataSet
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    ds = objEmpBanks.GetList("Banks", cboEmployee.SelectedValue.ToString, mdtPeriod_enddate, "", "end_date DESC, EmpName, priority", CInt(cboPeriod.SelectedValue))
            'Else
            '    ds = objEmpBanks.GetList("Banks", "-2", mdtPeriod_enddate, "", "end_date DESC, EmpName, priority", CInt(cboPeriod.SelectedValue))
            'End If
            If CInt(cboEmployee.SelectedValue) > 0 Then
                ds = objEmpBanks.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Banks", True, , cboEmployee.SelectedValue.ToString, mdtPeriod_enddate, "end_date DESC, EmpName, priority")
            Else
                ds = objEmpBanks.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_startdate, mdtPeriod_enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Banks", True, , "-2", mdtPeriod_enddate, "end_date DESC, EmpName, priority")
            End If
            'Sohail (21 Aug 2015) -- End
            mdtTran = New DataTable
            Dim dtCol As DataColumn
            dtCol = New DataColumn("UnkId", System.Type.GetType("System.Int32"))
            dtCol.AutoIncrement = True
            dtCol.AutoIncrementSeed = 1
            mdtTran.Columns.Add(dtCol)
            mdtTran.Merge(ds.Tables("Banks"))
            'mdtTran = New DataView(ds.Tables("Banks")).ToTable
            'Sohail (25 Apr 2014) -- End

            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtOldTable = mdtTran.Copy
            'S.SANDEEP [ 03 OCT 2012 ] -- END

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtPercentage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentage.LostFocus
        Try
            txtPercentage.Text = Format(txtPercentage.Decimal, "00.00")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPercentage_LostFocus", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Oct 2010) -- End

    'Sohail (10 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchBankGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankGroup.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboBankGroup.ValueMember
                .DisplayMember = cboBankGroup.DisplayMember
                .DataSource = CType(cboBankGroup.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objfrm.DisplayDialog Then
                cboBankGroup.SelectedValue = objfrm.SelectedValue
                cboBankGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchBankBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankBranch.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboBankBranch.ValueMember
                .DisplayMember = cboBankBranch.DisplayMember
                .DataSource = CType(cboBankBranch.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objfrm.DisplayDialog Then
                cboBankBranch.SelectedValue = objfrm.SelectedValue
                cboBankBranch.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankBranch_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Aug 2012) -- End

    'Sohail (21 Apr 2014) -- Start
    'Enhancement - Salary Distribution by Amount and bank priority.
    Private Sub cboMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            Select Case CInt(cboMode.SelectedValue)
                Case enPaymentBy.Value
                    lblPerc.Text = ""
                    colhPerc.Text = Language.getMessage(mstrModuleName, 20, "Amount")
                    colhID.Text = Language.getMessage(mstrModuleName, 21, "Priority")
                    objbtnUp.Visible = True
                    objbtnDown.Visible = True
                Case Else
                    lblPerc.Text = Language.getMessage(mstrModuleName, 24, "(%)")
                    colhPerc.Text = Language.getMessage(mstrModuleName, 24, "(%)")
                    colhID.Text = Language.getMessage(mstrModuleName, 25, "Sr No")
                    objbtnUp.Visible = False
                    objbtnDown.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Apr 2014) -- End

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
                mintPeriod_Status = objPeriod._Statusid
                Call FillCombo_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 Apr 2014) -- End

    'Sohail (12 Jun 2023) -- Start
    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
    Private Sub cboPaymentType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaymentType.SelectedIndexChanged
        Try
            If CInt(cboPaymentType.SelectedValue) = enBankPaymentType.MobileNumber Then
                EnableDisableMobileNo(True)
            Else
                EnableDisableMobileNo(False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPaymentType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 Jun 2023) -- End

#End Region

    'Sohail (16 Oct 2010) -- Start
#Region " Listview Events "
    Private Sub lvEmpBankList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvEmpBankList.SelectedIndexChanged
        Try
            If lvEmpBankList.SelectedItems.Count > 0 Then
                mintItemIndex = CInt(lvEmpBankList.SelectedItems(0).Index)
                cboBankBranch.SelectedValue = 0
                cboBankGroup.SelectedValue = 0
                cboAccountType.SelectedValue = 0

                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                'cboPeriod.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
                cboMode.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhModeID.Index).Text)
                cboMode.Enabled = False
                'Sohail (25 Apr 2014) -- End
                'cboEmployee.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhEmployee.Index).Tag)
                cboEmployee.Tag = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhEmployee.Index).Tag)
                cboBankGroup.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhBankGrp.Index).Tag)
                cboBankBranch.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhBankBranch.Index).Tag)
                cboAccountType.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhAccType.Index).Tag)
                txtAccountNo.Text = lvEmpBankList.SelectedItems(0).SubItems(colhAccNo.Index).Text
                txtPercentage.Text = lvEmpBankList.SelectedItems(0).SubItems(colhPerc.Index).Text
                cboPeriod.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
                cboEmployee.SelectedValue = CInt(cboEmployee.Tag)
                'Sohail (12 Jun 2023) -- Start
                'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                cboPaymentType.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(objcolhPaymentTypeId.Index).Text)
                txtMobileNo.Text = lvEmpBankList.SelectedItems(0).SubItems(colhMobileNo.Index).Text
                cboPaymentType.Enabled = False
                'Sohail (12 Jun 2023) -- End

                btnAdd.Enabled = False

            Else
                Call ResetValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmpBankList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Message "
    '1, "Employee is compulosry information. Please select Employee to continue."
    '2, "Bank Branch is compulosry information. Please select Bank Branch to continue."
    '3, "Bank Group is compulosry information. Please select Bank Group to continue."
    '4, "Account Type is compulosry information. Please select Account Type to continue."
    '5, "Account No cannot be blank. Please enter Account No."
    '6, "Sorry, Percentage should not be greater than 100."
    '7, "Please add atleast one employee bank."
    '8, "Sorry, Total Percentage must be 100."
    '9, "Sorry, You can not delete this Employee Bank. This Employee Bank is in used."
    '10, "Selected Account Type is already added to the list for particular Branch."
    '11, "This Account is already defined for particular employee."
#End Region
    'Sohail (16 Oct 2010) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbEmpBankDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmpBankDetails.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUp.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUp.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnDown.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnDown.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbEmpBankDetails.Text = Language._Object.getCaption(Me.gbEmpBankDetails.Name, Me.gbEmpBankDetails.Text)
            Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.Name, Me.lblAccountNo.Text)
            Me.lblBank.Text = Language._Object.getCaption(Me.lblBank.Name, Me.lblBank.Text)
            Me.lblAccountType.Text = Language._Object.getCaption(Me.lblAccountType.Name, Me.lblAccountType.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.lblSalaryDistrib.Text = Language._Object.getCaption(Me.lblSalaryDistrib.Name, Me.lblSalaryDistrib.Text)
            Me.lblPerc.Text = Language._Object.getCaption(Me.lblPerc.Name, Me.lblPerc.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.colhID.Text = Language._Object.getCaption(CStr(Me.colhID.Tag), Me.colhID.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhBankGrp.Text = Language._Object.getCaption(CStr(Me.colhBankGrp.Tag), Me.colhBankGrp.Text)
            Me.colhBankBranch.Text = Language._Object.getCaption(CStr(Me.colhBankBranch.Tag), Me.colhBankBranch.Text)
            Me.colhAccType.Text = Language._Object.getCaption(CStr(Me.colhAccType.Tag), Me.colhAccType.Text)
            Me.colhAccNo.Text = Language._Object.getCaption(CStr(Me.colhAccNo.Tag), Me.colhAccNo.Text)
            Me.colhPerc.Text = Language._Object.getCaption(CStr(Me.colhPerc.Tag), Me.colhPerc.Text)
            Me.colhGUID.Text = Language._Object.getCaption(CStr(Me.colhGUID.Tag), Me.colhGUID.Text)
            Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.colhModeID.Text = Language._Object.getCaption(CStr(Me.colhModeID.Tag), Me.colhModeID.Text)
            Me.lnkCopySlab.Text = Language._Object.getCaption(Me.lnkCopySlab.Name, Me.lnkCopySlab.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Bank Branch is compulsory information. Please select Bank Branch to continue.")
            Language.setMessage(mstrModuleName, 3, "Bank Group is compulsory information. Please select Bank Group to continue.")
            Language.setMessage(mstrModuleName, 4, "Account Type is compulsory information. Please select Account Type to continue.")
            Language.setMessage(mstrModuleName, 5, "Account No cannot be blank. Please enter Account No.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Percentage should not be greater than 100.")
            Language.setMessage(mstrModuleName, 7, "Please add atleast one employee bank.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Total Percentage must be 100.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot delete this Employee Bank. This Employee Bank is in use.")
            Language.setMessage(mstrModuleName, 10, "Selected Account Type is already added to the list for particular Branch.")
            Language.setMessage(mstrModuleName, 11, "This Account is already defined for particular employee.")
            Language.setMessage(mstrModuleName, 12, "NEWLY ADDED INFORMATION")
            Language.setMessage(mstrModuleName, 13, "EDITED INFORMATION")
            Language.setMessage(mstrModuleName, 14, "Value Type")
            Language.setMessage(mstrModuleName, 15, "Old")
            Language.setMessage(mstrModuleName, 16, "New")
            Language.setMessage(mstrModuleName, 17, "DELETED INFORMATION")
            Language.setMessage(mstrModuleName, 18, "Notification of Changes in Employee Bank(s).")
            Language.setMessage(mstrModuleName, 19, "Please select Salary Distribution Mode.")
            Language.setMessage(mstrModuleName, 20, "Amount")
            Language.setMessage(mstrModuleName, 21, "Priority")
            Language.setMessage(mstrModuleName, 22, "Sorry, Percentage should be greater than Zero.")
            Language.setMessage(mstrModuleName, 23, "Sorry, Amount should be greater than Zero.")
            Language.setMessage(mstrModuleName, 24, "(%)")
            Language.setMessage(mstrModuleName, 25, "Sr No")
            Language.setMessage(mstrModuleName, 26, "Sorry, This Period is closed.")
            Language.setMessage(mstrModuleName, 27, "Please select Period.")
            Language.setMessage(mstrModuleName, 28, "Sorry, You can use one Mode only in one Period.")
            Language.setMessage(mstrModuleName, 29, "Sorry! Slab for this Period is already defined.")
            Language.setMessage(mstrModuleName, 30, "Do you want to continue?")
			Language.setMessage(mstrModuleName, 31, "Dear")
			Language.setMessage(mstrModuleName, 32, "This is to inform you that changes have been made for employee")
			Language.setMessage(mstrModuleName, 33, "with employeecode")
			Language.setMessage(mstrModuleName, 34, "Following information has been changed by user")
			Language.setMessage(mstrModuleName, 35, "from Machine")
			Language.setMessage(mstrModuleName, 36, "and IPAddress")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmEmployeeBank_AddEdit"
'    Private mblnCancel As Boolean = True
'    Private objEmpBankTran As clsEmployeeBanks
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintEmployeeBankTranId As Integer = -1
'    Private mintEmployeeUnkid As Integer = -1
'    Private mdtTran As DataTable
'    Private mintEmpBankTypeId As Integer = -1
'#End Region

'#Region " Display Dialog "
'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmpId As Integer = -1) As Boolean
'        Try
'            mintEmployeeBankTranId = intUnkId
'            menAction = eAction

'            mintEmployeeUnkid = intEmpId

'            Me.ShowDialog()

'            intUnkId = mintEmployeeBankTranId

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function
'#End Region

'#Region " Private Methods "
'    Private Sub SetColor()
'        Try
'            cboAccountType.BackColor = GUI.ColorComp
'            cboBankBranch.BackColor = GUI.ColorComp
'            cboBankGroup.BackColor = GUI.ColorComp
'            cboEmployee.BackColor = GUI.ColorComp
'            txtAccountNo.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Try
'            Dim dsCombos As New DataSet
'            Dim objBankGroup As New clspayrollgroup_master
'            Dim objEmployee As New clsEmployee_Master
'            Dim objAccType As New clsBankAccType
'            Dim objBankBranch As New clsbankbranch_master

'            dsCombos = objBankGroup.getListForCombo(enGroupType.BankGroup, "BankGrp", True)
'            With cboBankGroup
'                .ValueMember = "groupmasterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("BankGrp")
'                .SelectedValue = 0
'            End With

'            dsCombos = objEmployee.GetEmployeeList("Employee", True, True)
'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsCombos.Tables("Employee")
'                .SelectedValue = 0
'            End With

'            dsCombos = objAccType.getComboList(True, "AccType")
'            With cboAccountType
'                .ValueMember = "accounttypeunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("AccType")
'                .SelectedValue = 0
'            End With
'            dsCombos = objBankBranch.getListForCombo("Branch", True)
'            With cboBankBranch
'                .ValueMember = "branchunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("Branch")
'                .SelectedValue = 0
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillList()
'        Try
'            lvEmpBankList.Items.Clear()
'            Dim lvItem As ListViewItem
'            For Each dtRow As DataRow In mdtTran.Rows
'                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'                    lvItem = New ListViewItem
'                    Dim objBank As New clspayrollgroup_master
'                    Dim objBranch As New clsbankbranch_master
'                    Dim objAccType As New clsBankAccType
'                    Dim objEmployee As New clsEmployee_Master

'                    objBank._Groupmasterunkid = CInt(dtRow.Item("bankgroupunkid"))
'                    objBranch._Branchunkid = CInt(dtRow.Item("branchunkid"))
'                    objEmployee._Employeeunkid = CInt(dtRow.Item("employeeunkid"))
'                    objAccType._Accounttypeunkid = CInt(dtRow.Item("accounttypeunkid"))

'                    lvItem.Text = CStr(IIf(IsDBNull(objEmployee._Firstname), "", objEmployee._Firstname.ToString)) & " " & _
'                                  CStr(IIf(IsDBNull(objEmployee._Othername), "", objEmployee._Othername.ToString)) & " " & _
'                                  CStr(IIf(IsDBNull(objEmployee._Surname), "", objEmployee._Surname.ToString)) 'Employee

'                    lvItem.SubItems.Add(objBank._Groupname.ToString)                                           'Bank Group
'                    lvItem.SubItems.Add(objBranch._Branchname.ToString)                                        'Bank Branch
'                    lvItem.SubItems.Add(objAccType._Accounttype_Name.ToString)                                 'Account Type
'                    lvItem.SubItems.Add(dtRow.Item("accountno").ToString)                                      'Account No
'                    lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)                                  'EmployeeUnkid
'                    lvItem.SubItems.Add(dtRow.Item("bankgroupunkid").ToString)                                 'Bank Group Unkid
'                    lvItem.SubItems.Add(dtRow.Item("branchunkid").ToString)                                    'Branch Unkid
'                    lvItem.SubItems.Add(dtRow.Item("accounttypeunkid").ToString)                               'Acc TypeUnkid
'                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)                                           'GUID

'                    lvItem.Tag = dtRow.Item("empbanktranunkid")

'                    lvEmpBankList.Items.Add(lvItem)

'                    lvItem = Nothing
'                End If
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub ResetValue()
'        Try
'            cboAccountType.SelectedValue = 0
'            cboBankBranch.SelectedValue = 0
'            cboBankGroup.SelectedValue = 0
'            txtAccountNo.Text = ""
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Form's Events "
'    Private Sub frmEmployeeBank_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            objEmpBankTran = New clsEmployeeBanks
'            Call FillCombo()
'            objEmpBankTran._EmployeeUnkid = mintEmployeeUnkid
'            mdtTran = objEmpBankTran._DataTable
'            Call FillList()
'            cboEmployee.Focus()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmEmployeeBank_AddEdit_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmEmployeeBank_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objEmpBankTran = Nothing
'    End Sub

'    Private Sub frmEmployeeBank_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
'                Call btnSave.PerformClick()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmEmployeeBank_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmEmployeeBank_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                Windows.Forms.SendKeys.Send("{Tab}")
'                e.Handled = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmEmployeeBank_KeyPress", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Buttons "
'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Try
'            If mintEmpBankTypeId > -1 Then
'                Dim drTemp As DataRow()
'                If CInt(lvEmpBankList.Items(mintEmpBankTypeId).Tag) = -1 Then
'                    drTemp = mdtTran.Select("GUID = '" & lvEmpBankList.Items(mintEmpBankTypeId).SubItems(objcolhGUID.Index).Text & "'")
'                Else
'                    drTemp = mdtTran.Select("empbanktranunkid = '" & CInt(lvEmpBankList.Items(mintEmpBankTypeId).Tag) & "'")
'                End If
'                If drTemp.Length > 0 Then
'                    drTemp(0).Item("AUD") = "D"
'                    Call FillList()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Try
'            If lvEmpBankList.SelectedItems.Count > 0 Then
'                If mintEmpBankTypeId > -1 Then
'                    Dim drTemp As DataRow()
'                    If CInt(lvEmpBankList.Items(mintEmpBankTypeId).Tag) = -1 Then
'                        drTemp = mdtTran.Select("GUID = '" & lvEmpBankList.Items(mintEmpBankTypeId).SubItems(objcolhGUID.Index).Text & "'")
'                    Else
'                        drTemp = mdtTran.Select("empbanktranunkid = " & CInt(lvEmpBankList.Items(mintEmpBankTypeId).Tag))
'                    End If
'                    If drTemp.Length > 0 Then
'                        With drTemp(0)
'                            .Item("empbanktranunkid") = CInt(lvEmpBankList.Items(mintEmpBankTypeId).Tag)
'                            .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
'                            .Item("branchunkid") = CInt(cboBankBranch.SelectedValue)
'                            .Item("accounttypeunkid") = CInt(cboAccountType.SelectedValue)
'                            .Item("accountno") = txtAccountNo.Text
'                            .Item("bankgroupunkid") = CInt(cboBankGroup.SelectedValue)
'                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
'                                .Item("AUD") = "U"
'                            End If
'                            .Item("GUID") = Guid.NewGuid().ToString
'                            .AcceptChanges()
'                        End With
'                        Call FillList()
'                    End If
'                End If
'                Call ResetValue()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Try
'            If CInt(cboEmployee.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."))
'                Exit Sub
'            End If
'            If CInt(cboBankGroup.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Bank Group is compulsory information. Please select Bank Group to continue."))
'                Exit Sub
'            End If
'            If CInt(cboBankBranch.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Branch is compulsory information. Please select Branch to continue."))
'                Exit Sub
'            End If
'            If CInt(cboAccountType.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Account Type is compulsory information. Please select Account Type to continue."))
'                Exit Sub
'            End If
'            If txtAccountNo.Text.Trim = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Account No. compulsory information. Please enter Account No. to continue."))
'                Exit Sub
'            End If

'            Dim dtRow As DataRow() = mdtTran.Select("accountno = '" & txtAccountNo.Text & "' AND AUD <> 'D' AND bankgroupunkid = '" & CInt(cboBankGroup.SelectedValue) & "' ")

'            If dtRow.Length > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected Account Type is already added to the list for particular Branch."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim dtEBRow As DataRow
'            dtEBRow = mdtTran.NewRow

'            dtEBRow.Item("empbanktranunkid") = -1
'            dtEBRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
'            dtEBRow.Item("branchunkid") = CInt(cboBankBranch.SelectedValue)
'            dtEBRow.Item("accounttypeunkid") = CInt(cboAccountType.SelectedValue)
'            dtEBRow.Item("accountno") = txtAccountNo.Text
'            dtEBRow.Item("bankgroupunkid") = CInt(cboBankGroup.SelectedValue)
'            dtEBRow.Item("AUD") = "A"
'            dtEBRow.Item("GUID") = Guid.NewGuid().ToString

'            mdtTran.Rows.Add(dtEBRow)

'            Call FillList()
'            Call ResetValue()


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            objEmpBankTran._DataTable = mdtTran
'            blnFlag = objEmpBankTran.InsertUpdateDelete_EmpBanks()

'            If blnFlag = False And objEmpBankTran._Message <> "" Then
'                eZeeMsgBox.Show(objEmpBankTran._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                Me.Close()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Controls "
'    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
'        Try
'            If CInt(cboBankGroup.SelectedValue) > 0 Then
'                Dim dsCombos As New DataSet
'                Dim objBankBranch As New clsbankbranch_master
'                dsCombos = objBankBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
'                With cboBankBranch
'                    .ValueMember = "branchunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsCombos.Tables("Branch")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvEmpBankList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            If lvEmpBankList.SelectedItems.Count > 0 Then
'                mintEmpBankTypeId = CInt(lvEmpBankList.SelectedItems(0).Index)
'                cboEmployee.SelectedValue = 0
'                cboBankBranch.SelectedValue = 0
'                cboBankGroup.SelectedValue = 0
'                cboAccountType.SelectedValue = 0

'                cboEmployee.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(objcolhempUnkid.Index).Text)
'                cboBankGroup.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(objcolhBankGrpId.Index).Text)
'                cboBankBranch.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(objcolhBranchUnkid.Index).Text)
'                cboAccountType.SelectedValue = CInt(lvEmpBankList.SelectedItems(0).SubItems(objcolhAccUnkid.Index).Text)
'                txtAccountNo.Text = lvEmpBankList.SelectedItems(0).SubItems(colhAccNo.Index).Text
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvEmpBankList_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim objfrm As New frmCommonSearch
'        Try
'            Dim dsList As New DataSet
'            Dim objEmployee As New clsEmployee_Master
'            dsList = objEmployee.GetEmployeeList("Employee", False, True)
'            objfrm.DataSource = dsList.Tables("Employee")
'            objfrm.ValueMember = cboEmployee.ValueMember
'            objfrm.DisplayMember = cboEmployee.DisplayMember
'            objfrm.CodeMember = "employeecode"
'            If objfrm.DisplayDialog Then
'                cboEmployee.SelectedValue = objfrm.SelectedValue
'            End If
'            cboEmployee.Focus()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        End Try
'    End Sub