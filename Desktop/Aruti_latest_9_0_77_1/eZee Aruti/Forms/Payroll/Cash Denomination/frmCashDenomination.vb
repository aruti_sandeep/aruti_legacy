﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCashDenomination

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCashDenomination"
    Private mblnCancel As Boolean = True
    Private mdtTran As DataTable
    Private mintPeriodId As Integer = -1
    Private mstrEmpIds As String = String.Empty
    Private mblnFormLoad As Boolean = False
    Private mintLoopEndIdx As Integer = 0
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    'Sohail (21 Aug 2015) -- End
    Private objCashDenome As clsCashDenomination

    'Sohail (18 Dec 2010) -- Start
    Private mintCashDenominationUnkid As Integer = -1
    Private mdblPercentage As Double = 0 'Sohail (11 May 2011)
    Private mdtCashDataSource As DataTable = Nothing
    Private menAction As enAction
    'Sohail (18 Dec 2010) -- End


    'S.SANDEEP [ 23 JUNE 2011 ] -- START
    'ISSUE : EXCHANGE RATE CHANGES
    Private mintExRateUnkid As Integer = -1
    Private mstrDenomeFormat As String = "#####################.##"
    'S.SANDEEP [ 23 JUNE 2011 ] -- END 

    'Sohail (12 Oct 2011) -- Start
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    'Sohail (12 Oct 2011) -- End

    'Sohail (03 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintCountryUnkid As Integer = -1
    'Sohail (03 Sep 2012) -- Start

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _EmpIds() As String
        Set(ByVal value As String)
            mstrEmpIds = value
        End Set
    End Property

    'Sohail (18 Dec 2010) -- Start
    Public ReadOnly Property _CashDataSource() As DataTable
        Get
            Return mdtCashDataSource
        End Get
    End Property
    'Sohail (18 Dec 2010) -- End

#End Region

#Region " Display Dialog "

    'S.SANDEEP [ 23 JUNE 2011 ] -- START
    'ISSUE : EXCHANGE RATE CHANGES
    'Public Function displayDialog(ByRef intUnkId As Integer, ByVal intPeriodID As Integer, ByVal strEmpIDs As String, ByVal dblPercentage As Double, ByVal eAction As enAction) As Boolean
    Public Function displayDialog(ByRef intUnkId As Integer _
                                  , ByVal intPeriodID As Integer _
                                  , ByVal strEmpIDs As String _
                                  , ByVal dblPercentage As Double _
                                  , ByVal eAction As enAction _
                                  , ByVal dtPeriodStart As Date _
                                  , ByVal dtPeriodEnd As Date _
                                  , Optional ByVal intExRateUnkid As Integer = -1 _
                                  , Optional ByVal decBaseExRate As Decimal = 0 _
                                  , Optional ByVal decPaidExRate As Decimal = 0 _
                                  , Optional ByVal intCountryId As Integer = -1 _
                                  ) As Boolean 'Sohail (12 Oct 2011)
        'Sohail (21 Aug 2015) - [dtPeriodStart, dtPeriodEnd]
        'Sohail (03 Sep 2012)[intCountryId]

        'S.SANDEEP [ 23 JUNE 2011 ] -- END 
        Try
            mintCashDenominationUnkid = intUnkId
            mintPeriodId = intPeriodID
            mstrEmpIds = strEmpIDs
            menAction = eAction
            mdblPercentage = dblPercentage
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mdtPeriodStart = dtPeriodStart
            mdtPeriodEnd = dtPeriodEnd
            'Sohail (21 Aug 2015) -- End


            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            mintCountryUnkid = intCountryId
            'Sohail (03 Sep 2012) -- End

            'S.SANDEEP [ 23 JUNE 2011 ] -- START
            'ISSUE : EXCHANGE RATE CHANGES
            mintExRateUnkid = intExRateUnkid
            'S.SANDEEP [ 23 JUNE 2011 ] -- END 

            'Sohail (12 Oct 2011) -- Start
            mdecBaseExRate = decBaseExRate
            mdecPaidExRate = decPaidExRate
            'Sohail (12 Oct 2011) -- End

            Me.ShowDialog()

            intUnkId = mintCashDenominationUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo(Optional ByVal intExRateUnkid As Integer = -1)
        Dim dsCombo As New DataSet
        Dim objExRate As New clsExchangeRate
        Try

            'S.SANDEEP [ 23 JUNE 2011 ] -- START
            'ISSUE : EXCHANGE RATE CHANGES
            'dsCombo = objExRate.getComboList("ExRate", True)

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'If intExRateUnkid > 0 Then
            '    dsCombo = objExRate.getComboList("ExRate", True)
            'Else
            '    dsCombo = objExRate.getComboList("ExRate", True, True)
            'End If
            dsCombo = objExRate.getComboList("ExRate")
            'Sohail (03 Sep 2012) -- End
            
            'S.SANDEEP [ 23 JUNE 2011 ] -- END 
            With cboCurrency
                RemoveHandler cboCurrency.SelectedIndexChanged, AddressOf cboCurrency_SelectedIndexChanged
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (03 Sep 2012) -- End

                .DisplayMember = "currency_sign"
                .DataSource = dsCombo.Tables("ExRate")
                'S.SANDEEP [ 23 JUNE 2011 ] -- START
                'ISSUE : EXCHANGE RATE CHANGES
                '.SelectedValue = 0

                AddHandler cboCurrency.SelectedIndexChanged, AddressOf cboCurrency_SelectedIndexChanged

                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                'If intExRateUnkid > 0 Then
                '    .SelectedValue = intExRateUnkid
                'Else
                '    .SelectedValue = dsCombo.Tables("ExRate").Rows(1)("exchangerateunkid")
                'End If

                .SelectedValue = -1
                If mintCountryUnkid > 0 Then
                    .SelectedValue = mintCountryUnkid
                Else
                    .SelectedValue = 1
                End If
                'Sohail (03 Sep 2012) -- End
                
                'S.SANDEEP [ 23 JUNE 2011 ] -- END 
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objExRate = Nothing
        End Try
    End Sub

    Private Sub CreateDataTable(ByVal dsDataset As DataSet)
        mdtTran = New DataTable("DENOM")
        Try
            Dim dCol As DataColumn

            dCol = New DataColumn("employeename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT - Column index issue while inserting from web in insert/update methos of clsCashDenomination
            dCol = New DataColumn("total")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)
            'Sohail (18 May 2013) -- End

            For Each dtRow As DataRow In dsDataset.Tables(0).Rows
                dCol = New DataColumn("Column" & dtRow.Item("denomunkid").ToString)
                'S.SANDEEP [ 23 JUNE 2011 ] -- START
                'ISSUE : EXCHANGE RATE CHANGES
                'dCol.Caption = dtRow.Item("denomination").ToString
                dCol.Caption = Format(dtRow.Item("denomination"), mstrDenomeFormat)
                'S.SANDEEP [ 23 JUNE 2011 ] -- END 
                dCol.DataType = System.Type.GetType("System.Decimal")
                dCol.DefaultValue = 0
                mdtTran.Columns.Add(dCol)
            Next

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT - Column index issue while inserting from web in insert/update methos of clsCashDenomination
            'dCol = New DataColumn("total")
            'dCol.DataType = System.Type.GetType("System.Decimal")
            'dCol.DefaultValue = 0
            'mdtTran.Columns.Add(dCol)
            'Sohail (18 May 2013) -- End

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("exchagerateunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("currency_sign")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            dCol = New DataColumn("countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
            'Sohail (03 Sep 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        End Try
    End Sub

    Private Sub GenerateDataGrid()
        Try
            Dim mdicDGVColumns As New Dictionary(Of Integer, String)

            Dim dgvCol As DataGridViewColumn
            Dim strTemp As String
            Dim iCnt As Integer = 0
            dgvEmpCashDenomination.Columns.Clear()

            Dim HeaderStyle As New DataGridViewCellStyle
            HeaderStyle.BackColor = Color.SlateGray
            HeaderStyle.ForeColor = Color.White

            dgvEmpCashDenomination.Columns.Add("objdgColEmployee", Language.getMessage(mstrModuleName, 3, "Employee"))
            dgvCol = dgvEmpCashDenomination.Columns("objdgColEmployee")
            dgvCol.Frozen = True
            dgvCol.ReadOnly = True
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle = HeaderStyle
            dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.Width = 125

            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            Dim HeaderStyle1 As New DataGridViewCellStyle
            HeaderStyle1.BackColor = Color.SlateGray
            HeaderStyle1.ForeColor = Color.White
            HeaderStyle1.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvEmpCashDenomination.Columns.Add("objdgColAmount", Language.getMessage(mstrModuleName, 4, "Amount"))
            dgvCol = dgvEmpCashDenomination.Columns("objdgColAmount")
            dgvCol.Frozen = True
            dgvCol.ReadOnly = True
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle = HeaderStyle1
            dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.Width = 100
            'S.SANDEEP [ 23 JUNE 2011 ] -- START
            'ISSUE : EXCHANGE RATE CHANGES
            dgvCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvCol.DefaultCellStyle.Format = GUI.fmtCurrency
            'S.SANDEEP [ 23 JUNE 2011 ] -- END 

            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            'Sohail (18 Dec 2010) -- Start
            strTemp = "total"
            dgvEmpCashDenomination.Columns.Add(strTemp, Language.getMessage(mstrModuleName, 5, "Total"))
            dgvCol = dgvEmpCashDenomination.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.Frozen = True
            dgvCol.ReadOnly = True
            dgvCol.DefaultCellStyle.BackColor = Color.LightGray
            dgvCol.DefaultCellStyle.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.Width = 100
            'S.SANDEEP [ 23 JUNE 2011 ] -- START
            'ISSUE : EXCHANGE RATE CHANGES
            dgvCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvCol.DefaultCellStyle.Format = GUI.fmtCurrency
            'S.SANDEEP [ 23 JUNE 2011 ] -- END 
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If
            'Sohail (18 Dec 2010) -- End

            Dim objDenome As New clsDenomination
            Dim dsList As New DataSet
            dsList = objDenome.GetList("List", True, True, CInt(cboCurrency.SelectedValue))

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                strTemp = "Column" & dtRow.Item("denomunkid").ToString
                'S.SANDEEP [ 23 JUNE 2011 ] -- START
                'ISSUE : EXCHANGE RATE CHANGES
                'dgvEmpCashDenomination.Columns.Add(strTemp, CStr(CDec(dtRow.Item("denomination"))) & "'s") 'Sohail (11 May 2011)

                dgvEmpCashDenomination.Columns.Add(strTemp, Format(dtRow.Item("denomination"), mstrDenomeFormat) & "'s")
                'S.SANDEEP [ 23 JUNE 2011 ] -- END 
                dgvCol = dgvEmpCashDenomination.Columns(strTemp)
                dgvCol.Tag = dtRow.Item("denomination")
                dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.Resizable = DataGridViewTriState.False
                dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                dgvCol.DefaultCellStyle.BackColor = Color.AliceBlue
                dgvCol.DefaultCellStyle.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
                dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
                dgvCol.Width = 60
                'S.SANDEEP [ 23 JUNE 2011 ] -- START
                'ISSUE : EXCHANGE RATE CHANGES
                dgvCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                'S.SANDEEP [ 23 JUNE 2011 ] -- END 
                If mdicDGVColumns.ContainsKey(iCnt) = False Then
                    mdicDGVColumns.Add(iCnt, dgvCol.Name)
                End If
                iCnt += 1
                mintLoopEndIdx += 1
            Next

            'Sohail (18 Dec 2010) -- Start
            'strTemp = "total"
            'dgvEmpCashDenomination.Columns.Add(strTemp, Language.getMessage(mstrModuleName, 5, "Total"))
            'dgvCol = dgvEmpCashDenomination.Columns(strTemp)
            'dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            'dgvCol.Resizable = DataGridViewTriState.False
            'dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'dgvCol.ReadOnly = True
            'dgvCol.DefaultCellStyle.BackColor = Color.LightGray
            'dgvCol.DefaultCellStyle.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            'dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            'dgvCol.Width = 100
            'If mdicDGVColumns.ContainsKey(iCnt) = False Then
            '    mdicDGVColumns.Add(iCnt, dgvCol.Name)
            '    iCnt += 1
            'End If
            'Sohail (18 Dec 2010) -- End

            strTemp = "employeeunkid"
            dgvEmpCashDenomination.Columns.Add(strTemp, strTemp)
            dgvCol = dgvEmpCashDenomination.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            strTemp = "exchagerateunkid"
            dgvEmpCashDenomination.Columns.Add(strTemp, strTemp)
            dgvCol = dgvEmpCashDenomination.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            strTemp = "currency_sign"
            dgvEmpCashDenomination.Columns.Add(strTemp, strTemp)
            dgvCol = dgvEmpCashDenomination.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            strTemp = "countryunkid"
            dgvEmpCashDenomination.Columns.Add(strTemp, strTemp)
            dgvCol = dgvEmpCashDenomination.Columns(strTemp)
            dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            dgvCol.Resizable = DataGridViewTriState.False
            dgvCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvCol.ReadOnly = True
            dgvCol.Visible = False
            dgvCol.DefaultCellStyle.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.HeaderCell.Style.Font = New Font(dgvEmpCashDenomination.Font, FontStyle.Bold)
            dgvCol.Width = 100
            If mdicDGVColumns.ContainsKey(iCnt) = False Then
                mdicDGVColumns.Add(iCnt, dgvCol.Name)
                iCnt += 1
            End If
            'Sohail (03 Sep 2012) -- End
            


            dgvEmpCashDenomination.AutoGenerateColumns = False

            For i As Integer = 0 To dgvEmpCashDenomination.Columns.Count - 1
                For j As Integer = i To i
                    If j >= 2 Then
                        dgvEmpCashDenomination.Columns(mdicDGVColumns(i)).DataPropertyName = mdtTran.Columns(mdicDGVColumns(i)).ColumnName
                    Else
                        dgvEmpCashDenomination.Columns(mdicDGVColumns(i)).DataPropertyName = mdtTran.Columns(j).ColumnName
                    End If
                Next
            Next

            dgvEmpCashDenomination.DataSource = mdtTran

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateDataGrid", mstrModuleName)
        End Try
    End Sub

    Private Sub FillDataTable(ByVal dsDataSet As DataTable)
        Try
            Dim drRow As DataRow
            Dim blnIsAdded As Boolean = False
            Dim decBalAmt As Decimal
            If menAction = enAction.ADD_ONE Then
                For Each dtRow As DataRow In dsDataSet.Rows
                    drRow = mdtTran.NewRow

                    drRow.Item("employeename") = dtRow.Item("employeename")
                    'Sohail (12 Oct 2011) -- Start
                    'drRow.Item("amount") = CDec(dtRow.Item("balanceamount")) * CDec(dtRow.Item("Percentage")) / 100 'Sohail (11 May 2011)
                    decBalAmt = CDec(dtRow.Item("balanceamount")) * CDec(dtRow.Item("Percentage")) / 100
                    If mdecBaseExRate <> 0 Then
                        drRow.Item("amount") = decBalAmt * mdecPaidExRate / mdecBaseExRate
                    Else
                        drRow.Item("amount") = decBalAmt * mdecPaidExRate
                    End If
                    'Sohail (12 Oct 2011) -- End
                    drRow.Item("employeeunkid") = dtRow.Item("employeeunkid")

                    'Sohail (03 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    'drRow.Item("exchagerateunkid") = cboCurrency.SelectedValue
                    drRow.Item("exchagerateunkid") = mintExRateUnkid
                    drRow.Item("countryunkid") = cboCurrency.SelectedValue
                    'Sohail (03 Sep 2012) -- End                    

                    drRow.Item("currency_sign") = cboCurrency.Text

                    mdtTran.Rows.Add(drRow)
                Next
            ElseIf menAction = enAction.EDIT_ONE Then
                drRow = mdtTran.NewRow
                For Each dtRow As DataRow In dsDataSet.Rows
                    If blnIsAdded = False Then
                        drRow.Item("employeename") = dtRow.Item("employeename")
                        drRow.Item("amount") = dtRow.Item("amount")
                        drRow.Item("employeeunkid") = dtRow.Item("employeeunkid")
                        drRow.Item("exchagerateunkid") = dtRow.Item("exchagerateunkid")
                        drRow.Item("currency_sign") = dtRow.Item("currency_sign")
                        drRow.Item("total") = dtRow.Item("amount")
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                        drRow.Item("countryunkid") = dtRow.Item("countryunkid")
                        'Sohail (03 Sep 2012) -- End

                        blnIsAdded = True
                    End If
                    drRow.Item("Column" & dtRow.Item("denominatonunkid").ToString) = CInt(dtRow.Item("qualtity"))
                Next
                mdtTran.Rows.Add(drRow)
                blnIsAdded = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDataTable", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCashDenomination_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCashDenome = New clsCashDenomination
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 23 JUNE 2011 ] -- START
            'ISSUE : EXCHANGE RATE CHANGES
            'Call FillCombo()
            'cboCurrency.SelectedValue = 1
            'Sohail (12 Oct 2011) -- Start
            'If menAction = enAction.ADD_ONE Then
            '    Call FillCombo()
            'ElseIf menAction = enAction.EDIT_ONE Then
            '    Call FillCombo(mintExRateUnkid)
            'End If
                Call FillCombo(mintExRateUnkid)
            'Sohail (12 Oct 2011) -- End
            'S.SANDEEP [ 23 JUNE 2011 ] -- END 
            mblnFormLoad = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCashDenomination_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCashDenomination_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCashDenomination_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCashDenomination_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCashDenomination_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCashDenomination_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCashDenome = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCashDenomination_FormClosed", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            For Each dgvrow As DataGridViewRow In dgvEmpCashDenomination.Rows
                If CDec(Format(CDec(dgvrow.Cells("objdgColAmount").Value), GUI.fmtCurrency)) <> CDec(dgvrow.Cells("total").Value) Then 'Sohail (12 Oct 2011)
                    dgvrow.Cells("objdgColEmployee").Style.BackColor = Color.Red
                    dgvrow.Cells("objdgColAmount").Style.BackColor = Color.Red
                    blnFlag = True
                Else
                    dgvrow.Cells("objdgColEmployee").Style.BackColor = Color.SlateGray
                    dgvrow.Cells("objdgColAmount").Style.BackColor = Color.SlateGray
                End If
            Next

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Cash Denomination cannot be less than Net Salary. And Total is Marked in Red."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'Sandeep [ 17 DEC 2010 ] -- Start
            If menAction = enAction.ADD_ONE Then
                mdtCashDataSource = mdtTran
                mblnCancel = False
                Me.Close()
            ElseIf menAction = enAction.EDIT_ONE Then
                objCashDenome._CashDenominationId = mintCashDenominationUnkid

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCashDenome._FormName = mstrModuleName
                objCashDenome._LoginEmployeeunkid = 0
                objCashDenome._ClientIP = getIP()
                objCashDenome._HostName = getHostName()
                objCashDenome._FromWeb = False
                objCashDenome._AuditUserId = User._Object._Userunkid
objCashDenome._CompanyUnkid = Company._Object._Companyunkid
                objCashDenome._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objCashDenome.Update(mdtTran) = True Then
                If objCashDenome.Update(mdtTran, FinancialYear._Object._YearUnkid, User._Object._Userunkid) = True Then
                    'Sohail (21 Aug 2015) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Denomination saved successfully"), enMsgBoxStyle.Information)
                    Me.Close()
                End If
            End If
            'Sandeep [ 17 DEC 2010 ] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub dgvEmpCashDenomination_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpCashDenomination.CellValueChanged
        If Not mblnFormLoad Then Exit Sub
        Dim decVal As Decimal = 0 'Sohail (11 May 2011)
        Dim decTotal As Decimal = 0 'Sohail (11 May 2011)
        If e.RowIndex < 0 Then Exit Sub
        Try
            If (e.ColumnIndex > 2 And e.RowIndex < dgvEmpCashDenomination.RowCount) Then 'Sohail (18 Dec 2010)
                For i As Integer = 3 To mintLoopEndIdx + 2 'Sohail (18 Dec 2010)
                    If IsDBNull(dgvEmpCashDenomination.Rows(e.RowIndex).Cells(i).Value) Then dgvEmpCashDenomination.Rows(e.RowIndex).Cells(i).Value = 0
                    If Val(dgvEmpCashDenomination.Rows(e.RowIndex).Cells(i).Value) = 0 Then Continue For
                    decVal = CDec(dgvEmpCashDenomination.Rows(e.RowIndex).Cells(i).Value) 'Sohail (11 May 2011)
                    decVal = decVal * CDec(dgvEmpCashDenomination.Rows(e.RowIndex).Cells(i).OwningColumn.Tag) 'Sohail (11 May 2011)
                    decTotal = decTotal + decVal
                    If decTotal > CDec(Format(CDec(dgvEmpCashDenomination.Rows(e.RowIndex).Cells("objdgColAmount").Value), GUI.fmtCurrency)) Then 'Sohail (12 Oct 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Denomination amount cannot be greater than Net Salary."))
                        dgvEmpCashDenomination.Rows(e.RowIndex).Cells(i).Value = 0
                        Exit Sub
                    End If
                Next

                RemoveHandler dgvEmpCashDenomination.CellValueChanged, AddressOf dgvEmpCashDenomination_CellValueChanged
                If decTotal = CDec(dgvEmpCashDenomination.Rows(e.RowIndex).Cells("objdgColAmount").Value) Then 'Sohail (11 May 2011)
                    dgvEmpCashDenomination.Rows(e.RowIndex).Cells("objdgColEmployee").Style.BackColor = Color.SlateGray
                    dgvEmpCashDenomination.Rows(e.RowIndex).Cells("objdgColAmount").Style.BackColor = Color.SlateGray
                End If
                'Sohail (18 Dec 2010) -- Start
                'dgvEmpCashDenomination.Rows(e.RowIndex).Cells("total").Value = Format(cdec(dblTotal), GUI.fmtCurrency)
                dgvEmpCashDenomination.Rows(e.RowIndex).Cells("total").Value = CDec(decTotal)
                'Sohail (18 Dec 2010) -- End
                AddHandler dgvEmpCashDenomination.CellValueChanged, AddressOf dgvEmpCashDenomination_CellValueChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpCashDenomination_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpCashDenomination_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvEmpCashDenomination.EditingControlShowing
        Try
            If ((Me.dgvEmpCashDenomination.CurrentCell.ColumnIndex > 1 And Me.dgvEmpCashDenomination.CurrentCell.ColumnIndex <= mintLoopEndIdx)) And Not e.Control Is Nothing Then
                Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                AddHandler txt.KeyPress, AddressOf RateKeyPress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpCashDenomination_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub RateKeyPress(ByVal sender As Object, ByVal e As Windows.Forms.KeyPressEventArgs)
        Try
            If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
                If Asc(e.KeyChar) = 8 Then Exit Sub
                e.KeyChar = CChar("")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RateKeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Try
            If CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objDenome As New clsDenomination
                Dim dsList As New DataSet
                dsList = objDenome.GetList("List", True, True, CInt(cboCurrency.SelectedValue))
                If dsList.Tables("List").Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Denomination's are not defined for this currency. Please define denomination to perform this operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Call CreateDataTable(dsList)
                dsList.Dispose()
                objDenome = Nothing

                Dim objTnALeave As New clsTnALeaveTran
                Dim dsTnAData As New DataSet
                Dim dtTable As DataTable = Nothing

                If menAction = enAction.ADD_ONE Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsTnAData = objTnALeave.Get_Balance_List("Data", , FinancialYear._Object._YearUnkid, mintPeriodId)
                    dsTnAData = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "", "Data", , mintPeriodId, "")
                    'Sohail (21 Aug 2015) -- End
                    dtTable = New DataView(dsTnAData.Tables("Data"), "employeeunkid IN (" & mstrEmpIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    dtTable.Columns.Add(New DataColumn("Percentage", System.Type.GetType("System.Decimal")))
                    For Each dtRow As DataRow In dtTable.Rows
                        dtRow.Item("Percentage") = mdblPercentage
                    Next
                ElseIf menAction = enAction.EDIT_ONE Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dtTable = objCashDenome.GetList(mintCashDenominationUnkid, "List")
                    dtTable = objCashDenome.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, "List", False, "", mintCashDenominationUnkid)
                    'Sohail (21 Aug 2015) -- End
                End If

                If dtTable.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "No Employee(s) present for the given period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Call FillDataTable(dtTable)

                dsTnAData.Dispose()
                objTnALeave = Nothing

                Call GenerateDataGrid()
            Else
                If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.eZeeHeading.Text = Language._Object.getCaption(Me.eZeeHeading.Name, Me.eZeeHeading.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Denomination's are not defined for this currency. Please define denomination to perform this operation.")
			Language.setMessage(mstrModuleName, 2, "No Employee(s) present for the given period.")
			Language.setMessage(mstrModuleName, 3, "Employee")
			Language.setMessage(mstrModuleName, 4, "Amount")
			Language.setMessage(mstrModuleName, 5, "Total")
			Language.setMessage(mstrModuleName, 6, "Denomination amount cannot be greater than Net Salary.")
			Language.setMessage(mstrModuleName, 7, "Denomination saved successfully")
			Language.setMessage(mstrModuleName, 8, "Cash Denomination cannot be less than Net Salary. And Total is Marked in Red.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class