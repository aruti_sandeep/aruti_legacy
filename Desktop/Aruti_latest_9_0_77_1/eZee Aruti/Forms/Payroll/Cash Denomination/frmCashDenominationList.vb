﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCashDenominationList

#Region " Private Variabels "

    Private ReadOnly mstrModuleName As String = "frmCashDenominationList"
    Private objCDenomination As clsCashDenomination
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtPeriod_Start As Date
    Private mdtPeriod_End As Date
    'Sohail (21 Aug 2015) -- End

#End Region

#Region " Private Methods "
    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboPeriod.BackColor = GUI.ColorOptional
            txtAmount.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = ObjEmp.GetEmployeeList("Emp", True, True)

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            'dsList = ObjEmp.GetEmployeeList("Emp", True)
            'End If

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = ObjPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, 1)
            dsList = ObjPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 1)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Period")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
            ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim StrSearching As String = String.Empty
        Dim dtTable As New DataTable
        Dim dtView As New DataTable
        Dim intIndex As Integer = 0
        Dim dicAddedItem As New Dictionary(Of String, String)
        Try

            If User._Object.Privilege._AllowToViewCashDenominationList = True Then    'Pinkal (09-Jul-2012) -- Start

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dtTable = objCDenomination.GetList(, "Denome")
                dtTable = objCDenomination.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Denome", True, "", )
                'Sohail (21 Aug 2015) -- End

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue)
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
                End If

                If txtAmount.Decimal > 0 Then 'Sohail (11 May 2011)
                    StrSearching &= "AND amount <= " & txtAmount.Decimal 'Sohail (11 May 2011)
                End If

                'Anjan (21 Nov 2011)-Start
                'ENHANCEMENT : TRA COMMENTS
                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If
                'Anjan (21 Nov 2011)-End 

                If StrSearching.Trim.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    dtView = New DataView(dtTable, StrSearching, "employeeunkid", DataViewRowState.CurrentRows).ToTable
                Else
                    dtView = New DataView(dtTable, "", "employeeunkid", DataViewRowState.CurrentRows).ToTable
                End If

                lvCashDenomination.LVItems = Nothing
                If dtView.Rows.Count > 0 Then
                    lvCashDenomination.LVItems = New ListViewItem(dtView.Rows.Count - 1) {}
                End If

                Dim lvItem As ListViewItem

                lvCashDenomination.Items.Clear()
                Dim strKey As String = String.Empty
                For Each drRow As DataRow In dtView.Rows
                    strKey = drRow.Item("employeeunkid").ToString & "|" & drRow.Item("periodunkid").ToString
                    If dicAddedItem.ContainsKey(strKey) = True Then Continue For
                    dicAddedItem.Add(strKey, drRow.Item("employeename").ToString)
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("pname").ToString
                    'Sohail (28 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    lvItem.SubItems.Add(drRow.Item("employeecode").ToString)
                    'Sohail (28 Jan 2012) -- End
                    lvItem.SubItems.Add(drRow.Item("employeename").ToString)
                    lvItem.SubItems.Add(Format(CDec(drRow.Item("amount")), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    lvItem.SubItems.Add(drRow.Item("periodunkid").ToString)
                    lvItem.SubItems.Add(drRow.Item("cashdenominationtranunkid").ToString)
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    lvItem.SubItems(objPeriodId.Index).Tag = drRow.Item("start_date").ToString
                    lvItem.SubItems(objDenomonationTranId.Index).Tag = drRow.Item("end_date").ToString
                    'Sohail (21 Aug 2015) -- End
                    'S.SANDEEP [ 23 JUNE 2011 ] -- START
                    'ISSUE : EXCHANGE RATE CHANGES
                    lvItem.SubItems.Add(drRow.Item("exchagerateunkid").ToString)
                    'S.SANDEEP [ 23 JUNE 2011 ] -- END 

                    'Sohail (03 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    lvItem.SubItems.Add(drRow.Item("countryunkid").ToString)
                    'Sohail (03 Sep 2012) -- End

                    lvItem.Tag = drRow("cashdenominationunkid")

                    lvCashDenomination.LVItems(intIndex) = lvItem
                    intIndex += 1
                Next

                If intIndex > 0 Then
                    lvCashDenomination.VirtualListSize = intIndex
                Else
                    lvCashDenomination.VirtualListSize = 0
                End If

                lvCashDenomination.Invalidate()
                lvCashDenomination.Reset()

                If lvCashDenomination.Items.Count > 13 Then
                    colhEmployee.Width = 320 - 20
                Else
                    colhEmployee.Width = 320
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnEdit.Enabled = User._Object.Privilege._EditCashDenomination


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCashDenominationList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmCashDenominationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCDenomination = New clsCashDenomination
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor() 'Sohail (10 Jul 2014)

            Call SetVisibility()
            Call FillCombo()
            'Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCashDenominationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCashDenominationList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objCDenomination = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buttons "

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            txtAmount.Decimal = 0 'Sohail (11 May 2011)
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvCashDenomination.SelectedIndices.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one denomination to perform operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Dim frm As New frmCashDenomination
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvCashDenomination.SelectedIndices.Item(0)

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If frm.displayDialog(CInt(lvCashDenomination.LVItems(intSelectedIndex).Tag), _
            '                  CInt(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objPeriodId.Index).Text), "", 100, enAction.EDIT_ONE, _
            '                  CInt(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objExRateId.Index).Text), , , _
            '                  CInt(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objCountryId.Index).Text)) Then
            If frm.displayDialog(CInt(lvCashDenomination.LVItems(intSelectedIndex).Tag), _
                              CInt(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objPeriodId.Index).Text), "", 100, enAction.EDIT_ONE, _
                              eZeeDate.convertDate(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objPeriodId.Index).Tag.ToString), _
                              eZeeDate.convertDate(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objDenomonationTranId.Index).Tag.ToString), _
                              CInt(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objExRateId.Index).Text), , , _
                              CInt(lvCashDenomination.LVItems(intSelectedIndex).SubItems(objCountryId.Index).Text)) Then
                'Sohail (21 Aug 2015) -- End
                Call FillList()
            End If
            'Sohail (03 Sep 2012) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 
#End Region

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
#Region " Combobox Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                mdtPeriod_Start = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPeriod_End = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                mdtPeriod_Start = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_End = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (21 Aug 2015) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one denomination to perform operation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class