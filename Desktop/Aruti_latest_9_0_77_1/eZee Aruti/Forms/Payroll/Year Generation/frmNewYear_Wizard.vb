﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO

#End Region

Public Class frmNewYear_Wizard

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGenerateYear_Wizard"
    Private mintCurrYearUnkID As Integer
    Private mdtCurrYearStartDate As Date
    Private mdtCurrYearEndDate As Date
    Private mdtNewYearStartDate As Date
    Private mdtNewYearEndDate As Date
    Private mintLastPeriodUnkID As Integer
    Private mdtLastPeriodStartDate As Date
    Private mdtLastPeriodEndDate As Date
    Private mdtLastPeriodTnAEndDate As Date
    Private mblnCancel As Boolean = True
    Private objCloseYear As clsCloseYear
    Private mdtLeave As DataTable
    Private mdtPayroll As DataTable
    Private mstrLeave As String
    Private mstrLeaveTypeIDs As String
    Private mstrPayroll As String
    Private mstrProcessPendingLoan As String
    Private mstrLoan As String
    Private mstrSavings As String
    Private mstrApplicants As String = ""
    Private menLeavAction As clsCloseYear.enLeaveAction
    Private WithEvents objBackupDatabase As New eZeeDatabase
    Private mblnProcessFailed As Boolean = False
    Private mstrCloseYearLogFileName As String = ""
    Private mstrNextTextCaption As String

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intCurrYearID As Integer, ByVal dtCurrYrStartDt As Date, ByVal dtCurrYrEndDt As Date) As Boolean
        Try
            mintCurrYearUnkID = intCurrYearID
            mdtCurrYearStartDate = dtCurrYrStartDt
            mdtCurrYearEndDate = dtCurrYrEndDt

            mdtNewYearStartDate = DateAdd(DateInterval.Year, 1, mdtCurrYearStartDate)
            mdtNewYearEndDate = DateAdd(DateInterval.Year, 1, mdtCurrYearEndDate)

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetDate()
        Try
            dtpCurrStartdate.Value = mdtCurrYearStartDate
            dtpCurrStartdate.MinDate = mdtCurrYearStartDate
            dtpCurrStartdate.MaxDate = mdtCurrYearStartDate
            dtpCurrEnddate.Value = mdtCurrYearEndDate
            dtpCurrEnddate.MinDate = mdtCurrYearEndDate
            dtpCurrEnddate.MaxDate = mdtCurrYearEndDate
            dtpNewStartdate.Value = mdtNewYearStartDate
            dtpNewStartdate.MinDate = mdtNewYearStartDate
            dtpNewStartdate.MaxDate = mdtNewYearStartDate
            dtpNewEnddate.Value = mdtNewYearEndDate
            dtpNewEnddate.MinDate = mdtNewYearEndDate
            dtpNewEnddate.MaxDate = mdtNewYearEndDate
            dtpPeriodEnddate.Value = mdtNewYearStartDate.AddMonths(1).AddDays(-1)
            dtpPeriodEnddate.MinDate = mdtNewYearStartDate
            dtpPeriodEnddate.MaxDate = mdtNewYearEndDate
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDate", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub PayrollTransactions()
        Dim objTnALeave As New clsTnALeaveTran
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim mintPeriodUnkId As Integer = 0
        Try
            mstrPayroll = ""
            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Close)
            dtTable = New DataView(dsList.Tables("Period"), "end_date = '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "'", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mintPeriodUnkId = CInt(dtTable.Rows(0).Item("periodunkid").ToString)
                mintLastPeriodUnkID = mintPeriodUnkId
                mdtLastPeriodStartDate = eZeeDate.convertDate(dtTable.Rows(0).Item("start_date").ToString)
                mdtLastPeriodEndDate = eZeeDate.convertDate(dtTable.Rows(0).Item("end_date").ToString)
                mdtLastPeriodTnAEndDate = eZeeDate.convertDate(dtTable.Rows(0).Item("tna_enddate").ToString)
            Else
                mintLastPeriodUnkID = 0
                mdtLastPeriodStartDate = Nothing
                mdtLastPeriodEndDate = Nothing
                mdtLastPeriodTnAEndDate = Nothing
            End If
            dsList = objTnALeave.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtLastPeriodStartDate, mdtLastPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, False, "OldBalance", , mintPeriodUnkId, , , )
            mdtPayroll = New DataView(dsList.Tables("OldBalance"), "balanceamount <> 0", "", DataViewRowState.CurrentRows).ToTable
            mstrPayroll = String.Join(",", mdtPayroll.AsEnumerable().Select(Function(x) x.Field(Of Integer)("tnaleavetranunkid").ToString).ToArray)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "PayrollTransactions", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Loan_Savings()
        Dim objLoanAdvance As New clsLoan_Advance
        Dim objPendigLoan As New clsProcess_pending_loan
        Dim objSavingTran As New clsSaving_Tran
        Dim dsList As DataSet
        Try
            mstrProcessPendingLoan = ""
            mstrLoan = ""
            mstrSavings = ""
            Dim strNewJoin As String = String.Empty

            dsList = objPendigLoan.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
                                          "PendingLoan", 1, , False)

            mstrProcessPendingLoan = String.Join(",", dsList.Tables("PendingLoan").AsEnumerable().Select(Function(x) x.Field(Of Integer)("processpendingloanunkid").ToString).ToArray)

            dsList = objPendigLoan.Get_UnAssigned_ProcessPending_List(FinancialYear._Object._DatabaseName, _
                                                                      User._Object._Userunkid, _
                                                                      FinancialYear._Object._YearUnkid, _
                                                                      Company._Object._Companyunkid, _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                                      True, _
                                                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                      "UnAssigneLoan", , False)

            strNewJoin = String.Join(",", dsList.Tables("UnAssigneLoan").AsEnumerable().Select(Function(x) x.Field(Of Integer)("processpendingloanunkid").ToString).ToArray)

            If strNewJoin.Trim.Length > 0 Then
                If mstrProcessPendingLoan.Trim.Length > 0 Then
                    mstrProcessPendingLoan = mstrProcessPendingLoan & "," & strNewJoin
                Else
                    mstrProcessPendingLoan = strNewJoin
                End If
            End If

            dsList = objLoanAdvance.GetList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            ConfigParameter._Object._IsIncludeInactiveEmp, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            "Loan", "loan_statusunkid <> 4 AND balance_amount > 0", , False)

            mstrLoan = String.Join(",", dsList.Tables("Loan").AsEnumerable().Select(Function(x) x.Field(Of Integer)("loanadvancetranunkid").ToString).ToArray)

            dsList = objLoanAdvance.Get_Unpaid_LoanAdvance_List(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._UserAccessModeSetting, True, True, False, "LoanPayable")

            strNewJoin = String.Join(",", dsList.Tables("LoanPayable").AsEnumerable().Select(Function(x) x.Field(Of Integer)("loanadvancetranunkid").ToString).ToArray)

            If strNewJoin.Trim.Length > 0 Then
                If mstrLoan.Trim.Length > 0 Then
                    mstrLoan = mstrLoan & "," & strNewJoin
                Else
                    mstrLoan = strNewJoin
                End If
            End If



            dsList = objSavingTran.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
                                          "Savings", "savingstatus <> 4 or (savingstatus = 4 and balance_amount > 0)", , , , , , , False)

            mstrSavings = String.Join(",", dsList.Tables("Savings").AsEnumerable().Select(Function(x) x.Field(Of Integer)("savingtranunkid").ToString).ToArray)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Loan_Savings", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillLeaveList()
        Dim objLeaveBalance As New clsleavebalance_tran
        Dim dsList As DataSet = Nothing
        Try
            mstrLeave = "" : mstrLeaveTypeIDs = ""

            dsList = objLeaveBalance.GetList("Leave", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                             , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                             , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1 _
                                             , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year, False, True)) _
                                             , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year, False, True)) _
                                             , False, "lvleavebalance_tran.ispaid = 1 AND lvleavebalance_tran.isshortleave = 0", Nothing)



            'Pinkal (23-Jan-2021)-- Start
            'Enhancement  -  Working on Self Service Bugs from Self Service sheet.

            'Dim dsShortLeaveList As DataSet = objLeaveBalance.GetList("Leave", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                 , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1 _
            '                                 , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year, False, True)) _
            '                                 , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year, False, True)) _
            '                                 , False, "lvleavebalance_tran.ispaid = 1 AND lvleavebalance_tran.isshortleave = 1 AND lvleavebalance_tran.occurrence > 0 AND lvleavebalance_tran.enddate >= '" & eZeeDate.convertDate(mdtNewYearStartDate).ToString() & "'", Nothing)

            'If dsShortLeaveList IsNot Nothing AndAlso dsShortLeaveList.Tables(0).Rows.Count > 0 Then
            '    dsList.Tables(0).Merge(dsShortLeaveList.Tables(0))
            'End If

            Dim dsShortLeaveList As DataSet = objLeaveBalance.GetList("Leave", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                             , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                             , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1 _
                                             , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year, False, True)) _
                                             , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year, False, True)) _
                                           , False, "lvleavebalance_tran.ispaid = 1 AND lvleavebalance_tran.isshortleave = 1 AND lvleavebalance_tran.enddate >= '" & eZeeDate.convertDate(mdtNewYearStartDate).ToString() & "'", Nothing)

            If dsShortLeaveList IsNot Nothing AndAlso dsShortLeaveList.Tables(0).Rows.Count > 0 Then
                dsList.Tables(0).Merge(dsShortLeaveList.Tables(0))
            End If

            'Pinkal (23-Jan-2021) -- End

            mdtLeave = New DataView(dsList.Tables("Leave"), "", "leavetypeunkid,employeeunkid", DataViewRowState.CurrentRows).ToTable

            mstrLeave = String.Join(",", mdtLeave.AsEnumerable().Select(Function(x) x.Field(Of Integer)("leavebalanceunkid").ToString).ToArray)
            mstrLeaveTypeIDs = String.Join(",", mdtLeave.AsEnumerable().Select(Function(x) x.Field(Of Integer)("leavetypeunkid").ToString).Distinct.ToArray)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLeaveList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ApplicantList()
        Dim objApplicantMaster As New clsApplicant_master
        Dim dsList As DataSet
        Try
            mstrApplicants = ""
            dsList = objApplicantMaster.GetList("Applicant")
            mstrApplicants = String.Join(",", dsList.Tables("Applicant").AsEnumerable().Select(Function(x) x.Field(Of Integer)("applicantunkid").ToString).ToArray)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ApplicantList", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Function CreateUserViewSynonymsForSharePointTRA() As Boolean
        Dim objDataOperation As clsDataOperation
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim strQ As String = ""
        Dim mstrCurrYearDatabaseName As String = ""
        Dim mstrNewYearDatabaseName As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            If objDataOperation.RecordCount("SELECT * FROM sys.databases WHERE name = 'IDM_DATA'") > 0 Then
                mstrCurrYearDatabaseName = FinancialYear._Object._DatabaseName
                dsList = objMaster.Get_Database_Year_List("List", False, Company._Object._Companyunkid)

                If dsList.Tables("List").Rows.Count > 0 Then
                    mstrNewYearDatabaseName = dsList.Tables("List").Rows(0).Item("database_name").ToString
                Else
                    Return True
                    Exit Function
                End If

                strQ = "USE [IDM_DATA]"

                objDataOperation.ExecNonQuery(strQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwEmployeeDetail') " & _
                       " DROP VIEW vwEmployeeDetail "

                objDataOperation.ExecNonQuery(strQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "EXEC('CREATE VIEW [vwEmployeeDetail] AS " & _
                       "SELECT " & _
                       "     employeecode as code " & _
                       "    ,ISNULL(firstname,'''') +'' ''+ ISNULL(othername,'''') +'' ''+ISNULL(surname,'''') AS employeename " & _
                       "    ,ISNULL(GG.name,'''') AS gradegroup " & _
                       "    ,ISNULL(GM.name,'''') AS grade " & _
                       "    ,ISNULL(GL.name,'''') AS gradelevel " & _
                       "    ,ISNULL(DM.name,'''') as department " & _
                       "    ,ISNULL(SM.name,'''') as station " & _
                       "    ,ISNULL(JM.job_name,'''') as designation " & _
                       "    ,ISNULL(CG.name,'''') as region " & _
                       "FROM IDM_DATA..hremployee_master " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         ETT.employeeunkid AS TrfEmpId " & _
                       "        ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                       "        ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                       "        ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                       "        ,CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                       "        ,ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid,ETT.companyunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
                       "        ,ISNULL(ETT.companyunkid,0) AS companyunkid " & _
                       "    FROM IDM_DATA..hremployee_transfer_tran AS ETT " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                       ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.companyunkid = hremployee_master.companyunkid AND ETRF.Rno = 1 " & _
                       "LEFT JOIN IDM_DATA..hrstation_master AS SM ON SM.stationunkid = ETRF.stationunkid " & _
                       "LEFT JOIN IDM_DATA..hrdepartment_master AS DM ON DM.departmentunkid = ETRF.departmentunkid " & _
                       "LEFT JOIN IDM_DATA..hrclassgroup_master AS CG ON CG.classgroupunkid = ETRF.classgroupunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         ECT.employeeunkid AS CatEmpId " & _
                       "        ,ECT.jobunkid " & _
                       "        ,CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                       "        ,ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid,ECT.companyunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                       "        ,ISNULL(ECT.companyunkid,0) AS companyunkid " & _
                       "    FROM IDM_DATA..hremployee_categorization_tran AS ECT " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                       ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.companyunkid = hremployee_master.companyunkid AND ERECAT.Rno = 1 " & _
                       "LEFT JOIN IDM_DATA..hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         gradegroupunkid " & _
                       "        ,gradeunkid " & _
                       "        ,gradelevelunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER() OVER (PARTITION BY employeeunkid,ESI.companyunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                       "        ,ISNULL(ESI.companyunkid,0) AS companyunkid " & _
                       "    FROM IDM_DATA..prsalaryincrement_tran AS ESI " & _
                       "    WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                       ") AS EGRD ON EGRD.employeeunkid = hremployee_master.employeeunkid AND ERECAT.companyunkid = hremployee_master.companyunkid AND EGRD.Rno = 1 " & _
                       "LEFT JOIN IDM_DATA..hrgradegroup_master AS GG ON GG.gradegroupunkid = EGRD.gradegroupunkid " & _
                       "LEFT JOIN IDM_DATA..hrgrade_master AS GM ON GM.gradeunkid = EGRD.gradeunkid " & _
                       "LEFT JOIN IDM_DATA..hrgradelevel_master AS GL ON GL.gradelevelunkid = EGRD.gradelevelunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         TRM.employeeunkid AS TEEmpId " & _
                       "        ,TRM.date1 AS empl_enddate " & _
                       "        ,TRM.date2 AS termination_from_date " & _
                       "        ,TRM.isexclude_payroll " & _
                       "        ,TRM.changereasonunkid " & _
                       "        ,ROW_NUMBER() OVER ( PARTITION BY TRM.employeeunkid,TRM.companyunkid ORDER BY TRM.effectivedate DESC ) AS Rno " & _
                       "        ,ISNULL(TRM.companyunkid,0) AS companyunkid " & _
                       "    FROM IDM_DATA..hremployee_dates_tran AS TRM " & _
                       "    WHERE TRM.isvoid = 0 AND TRM.datetypeunkid = 4 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                       ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.companyunkid = hremployee_master.companyunkid AND ETERM.Rno = 1 " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         RTD.employeeunkid AS REmpId " & _
                       "        ,RTD.date1 AS termination_to_date " & _
                       "        ,ROW_NUMBER() OVER (PARTITION BY RTD.employeeunkid,RTD.companyunkid ORDER BY RTD.effectivedate DESC ) AS Rno " & _
                       "        ,ISNULL(RTD.companyunkid,0) AS companyunkid " & _
                       "    FROM IDM_DATA..hremployee_dates_tran AS RTD " & _
                       "    WHERE RTD.isvoid = 0 AND RTD.datetypeunkid = 6 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                       ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.companyunkid = hremployee_master.companyunkid AND ERET.Rno = 1 " & _
                       "WHERE CONVERT(CHAR(8),appointeddate,112) <= CONVERT(CHAR(8),GETDATE(),112) " & _
                       "AND ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date,112),CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112) " & _
                       "AND ISNULL(CONVERT(CHAR(8),ERET.termination_to_date,112),CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112) " & _
                       "AND ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate,112), CONVERT(CHAR(8),GETDATE(),112)) >= CONVERT(CHAR(8),GETDATE(),112)') "

                objDataOperation.ExecNonQuery(strQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If objDataOperation.RecordCount("SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'selectview'") > 0 Then
                strQ = "exec sp_helprolemember  'selectview' "
                Dim ds As DataSet = objDataOperation.ExecQuery(strQ, "UserList")
                Dim users As List(Of String) = (From p In ds.Tables(0).AsEnumerable() Select (p.Item("MemberName").ToString)).ToList

                If users.ToList.Count > 0 Then
                    For Each strUserName In users

                        strQ = "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'selectview') " & _
                               "BEGIN " & _
                               "    CREATE ROLE selectview  " & _
                               "END " & _
                               "IF EXISTS(SELECT * FROM sys.syslogins WHERE name = '" & strUserName & "') " & _
                               "BEGIN " & _
                               "   IF NOT EXISTS(SELECT * FROM sys.sysusers WHERE issqluser =1 AND islogin = 1 AND name = '" & strUserName & "') " & _
                               "   BEGIN " & _
                               "       CREATE USER " & strUserName & " FOR LOGIN " & strUserName & " " & _
                               "    END " & _
                               "    BEGIN " & _
                               "       GRANT SELECT ON vwEmployeeDetail TO selectview " & _
                               "       EXEC sp_addrolemember 'selectview','" & strUserName & "'" & _
                               "    END " & _
                               "END "

                        objDataOperation.ExecNonQuery(strQ)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateUserViewSynonymsForSharePointTRA", mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            objMaster = Nothing
        End Try
    End Function

    Private Function CreateUserViewSynonymsFor_IDMS() As Boolean
        Dim objDataOperation As clsDataOperation
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim strQ As String = ""
        Dim mstrCurrYearDatabaseName As String = ""
        Dim mstrNewYearDatabaseName As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If objDataOperation.RecordCount("SELECT * FROM sys.databases WHERE UPPER(name) = 'IDM_DATA'") <= 0 Then
                Return True
            End If

            mstrCurrYearDatabaseName = FinancialYear._Object._DatabaseName

            strQ = "USE " & mstrCurrYearDatabaseName
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objMaster.Get_Database_Year_List("List", False, Company._Object._Companyunkid)

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrNewYearDatabaseName = dsList.Tables("List").Rows(0).Item("database_name").ToString
            Else
                Return True
                Exit Function
            End If

            If objDataOperation.RecordCount("SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleview'") > 0 Then
                strQ = "exec sp_helprolemember  'oracleview' "
                Dim ds As DataSet = objDataOperation.ExecQuery(strQ, "UserList")
                Dim users As List(Of String) = (From p In ds.Tables(0).AsEnumerable() Select (p.Item("MemberName").ToString)).ToList
                If users.ToList.Count > 0 Then
                    Dim strtranSelectFields As String = "(employeeunkid ,employeecode ,titleunkid ,firstname ,surname ,othername ,appointeddate ,gender ,employmenttypeunkid ,paytypeunkid " & _
                                                        ",paypointunkid ,loginname ,password ,email ,displayname ,shiftunkid ,birthdate ,birth_ward ,birthcertificateno ,birthstateunkid " & _
                                                        ",birthcountryunkid ,birthcityunkid ,birth_village ,work_permit_no ,workcountryunkid ,work_permit_issue_place ,work_permit_issue_date " & _
                                                        ",work_permit_expiry_date ,complexionunkid ,bloodgroupunkid ,eyecolorunkid ,nationalityunkid ,ethnicityunkid ,religionunkid ,hairunkid " & _
                                                        ",language1unkid ,language2unkid ,language3unkid ,language4unkid ,extra_tel_no ,height ,weight ,maritalstatusunkid ,anniversary_date " & _
                                                        ",sports_hobbies ,present_address1 ,present_address2 ,present_countryunkid ,present_postcodeunkid ,present_stateunkid ,present_provicnce " & _
                                                        ",present_post_townunkid ,present_road ,present_estate ,present_plotNo ,present_mobile ,present_alternateno ,present_tel_no ,present_fax " & _
                                                        ",present_email ,domicile_address1 ,domicile_address2 ,domicile_countryunkid ,domicile_postcodeunkid ,domicile_stateunkid ,domicile_provicnce " & _
                                                        ",domicile_post_townunkid ,domicile_road ,domicile_estate ,domicile_plotno ,domicile_mobile ,domicile_alternateno ,domicile_tel_no ,domicile_fax " & _
                                                        ",domicile_email ,emer_con_firstname ,emer_con_lastname ,emer_con_address ,emer_con_countryunkid ,emer_con_postcodeunkid ,emer_con_state " & _
                                                        ",emer_con_provicnce ,emer_con_post_townunkid ,emer_con_road ,emer_con_estate ,emer_con_plotno ,emer_con_mobile ,emer_con_alternateno " & _
                                                        ",emer_con_tel_no ,emer_con_fax ,emer_con_email ,stationunkid ,deptgroupunkid ,departmentunkid ,sectionunkid ,unitunkid ,jobgroupunkid " & _
                                                        ",jobunkid ,gradegroupunkid ,gradeunkid ,gradelevelunkid ,accessunkid ,classgroupunkid ,classunkid ,serviceunkid ,costcenterunkid ,tranhedunkid " & _
                                                        ",actionreasonunkid ,suspended_from_date ,suspended_to_date ,probation_from_date ,probation_to_date ,termination_from_date ,termination_to_date " & _
                                                        ",remark ,isactive ,emer_con_firstname2 ,emer_con_lastname2 ,emer_con_address2 ,emer_con_countryunkid2 ,emer_con_postcodeunkid2 ,emer_con_state2 " & _
                                                        ",emer_con_provicnce2 ,emer_con_post_townunkid2 ,emer_con_road2 ,emer_con_estate2 ,emer_con_plotno2 ,emer_con_mobile2 ,emer_con_alternateno2 " & _
                                                        ",emer_con_tel_no2 ,emer_con_fax2 ,emer_con_email2 ,emer_con_firstname3 ,emer_con_lastname3 ,emer_con_address3 ,emer_con_countryunkid3 " & _
                                                        ",emer_con_postcodeunkid3 ,emer_con_state3 ,emer_con_provicnce3 ,emer_con_post_townunkid3 ,emer_con_road3 ,emer_con_estate3 ,emer_con_plotno3 " & _
                                                        ",emer_con_mobile3 ,emer_con_alternateno3 ,emer_con_tel_no3 ,emer_con_fax3 ,emer_con_email3 ,sectiongroupunkid ,unitgroupunkid " & _
                                                        ",teamunkid ,confirmation_date ,empl_enddate ,isclear ,allocationreason ,reinstatement_date ,isexclude_payroll ,isapproved ,companyunkid) "

                    For Each strUserName In users

                        strQ = "USE [hrmsConfiguration]"

                        objDataOperation.ExecNonQuery(strQ)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleview') " & _
                                       "BEGIN " & _
                                       "    CREATE ROLE oracleview  " & _
                                       "END " & _
                                       "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleviewupdate') " & _
                                       "BEGIN " & _
                                       "    CREATE ROLE oracleviewupdate  " & _
                                       "END " & _
                                       "IF EXISTS(SELECT * FROM sys.syslogins WHERE name = '" & strUserName & "') " & _
                                       "BEGIN " & _
                                       "    IF NOT EXISTS(SELECT * FROM sys.sysusers WHERE issqluser =1 AND islogin = 1 AND name = '" & strUserName & "') " & _
                                       "    BEGIN " & _
                                       "        CREATE USER [" & strUserName & "] FOR LOGIN [" & strUserName & "] " & _
                                       "    END " & _
                                       "    BEGIN " & _
                                       "        GRANT SELECT ON hrmsConfiguration..cffinancial_year_tran (database_name,companyunkid,isclosed,yearunkid) TO oracleview " & _
                                       "        GRANT SELECT ON hrmsConfiguration..cfuser_master (userunkid,username,employeeunkid,companyunkid) To oracleview " & _
                                       "        GRANT UPDATE ON hrmsConfiguration..cfuser_master (password) To oracleview " & _
                                       "        GRANT SELECT ON hrmsConfiguration..cfcountry_master (countryunkid,alias,country_name,country_name1,country_name2,isrighttoleft) To oracleview " & _
                                       "        EXEC sp_addrolemember 'oracleview','" & strUserName & "'" & _
                                       "        EXEC sp_addrolemember 'oracleviewupdate','" & strUserName & "'" & _
                                       "    END " & _
                                       "END "

                        objDataOperation.ExecNonQuery(strQ)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = " USE " & mstrNewYearDatabaseName
                        objDataOperation.ExecNonQuery(strQ)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleview') " & _
                               "BEGIN " & _
                               "    CREATE ROLE oracleview  " & _
                               "END " & _
                               "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE type_desc = 'DATABASE_ROLE' AND name = 'oracleviewupdate') " & _
                               "BEGIN " & _
                               "    CREATE ROLE oracleviewupdate  " & _
                               "END " & _
                               "IF EXISTS(SELECT * FROM sys.syslogins WHERE name = '" & strUserName & "') " & _
                               "BEGIN " & _
                               "    IF NOT EXISTS(SELECT * FROM sys.sysusers WHERE issqluser =1 AND islogin = 1 AND name = '" & strUserName & "') " & _
                               "    BEGIN " & _
                               "        CREATE USER [" & strUserName & "] FOR LOGIN [" & strUserName & "] " & _
                               "    END " & _
                               "    BEGIN " & _
                               "        GRANT SELECT ON " & mstrNewYearDatabaseName & "..hremployee_master " & strtranSelectFields & " To oracleview " & _
                               "        GRANT SELECT ON " & mstrNewYearDatabaseName & "..hremployee_reportto (employeeunkid,reporttoemployeeunkid,ishierarchy) To oracleview " & _
                               "        GRANT UPDATE ON " & mstrNewYearDatabaseName & "..hremployee_master (password,employeeunkid,companyunkid) To oracleview " & _
                               "        GRANT INSERT ON " & mstrNewYearDatabaseName & "..athremployee_master To oracleview " & _
                               "        EXEC sp_addrolemember  'oracleview','" & strUserName & "'" & _
                               "        EXEC sp_addrolemember  'oracleviewupdate','" & strUserName & "'" & _
                               "    END " & _
                               "END "
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next

                    'Enabling CLR
                    strQ = "EXEC sp_configure 'clr enable', 1 " & _
                           "RECONFIGURE; "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Setting Database Trustworthy
                    strQ = "ALTER DATABASE " & mstrNewYearDatabaseName & " SET TRUSTWORTHY ON "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Registering Assembly
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.assemblies WHERE name='EncDec') " & _
                           "BEGIN " & _
                           "  CREATE ASSEMBLY EncDec " & _
                           "  AUTHORIZATION dbo " & _
                           "  FROM '" & AppSettings._Object._ApplicationPath & "ArutiSQL_Lib.dll' " & _
                           "  WITH PERMISSION_SET = UNSAFE " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Generation Encrypt Function
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EncryptString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')) " & _
                           "BEGIN " & _
                           "EXECUTE dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[EncryptString](@PlainText [nvarchar](max), @Key [nvarchar](max)) " & _
                           "RETURNS [nvarchar](max) WITH EXECUTE AS CALLER " & _
                           "AS " & _
                           "EXTERNAL NAME [EncDec].[Security].[Encrypt]' " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Generation Decrypt Function
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DecryptString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')) " & _
                           "BEGIN " & _
                           "EXECUTE dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[DecryptString](@PlainText [nvarchar](max), @Key [nvarchar](max)) " & _
                           "RETURNS [nvarchar](max) WITH EXECUTE AS CALLER " & _
                           "AS " & _
                           "    EXTERNAL NAME [EncDec].[Security].[Decrypt]' " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Generating IP_Address Function
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetIPAddress]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')) " & _
                           "BEGIN " & _
                           "EXECUTE dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[GetIPAddress](@Host [nvarchar](max)) " & _
                           "RETURNS [nvarchar](max) WITH EXECUTE AS CALLER " & _
                           "AS " & _
                           "    EXTERNAL NAME [EncDec].[Security].[GetIpaddress]' " & _
                           "END "
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim strValue As String = "  hremployee_master.employeeunkid AS employeeunkid " & _
                                             ", hremployee_master.employeecode AS employeecode " & _
                                             ", ISNULL(title.NAME, '''') AS title " & _
                                             ", hremployee_master.firstname AS firstname " & _
                                             ", hremployee_master.surname AS surname " & _
                                             ", hremployee_master.othername AS othername " & _
                                             ", hremployee_master.appointeddate AS appointeddate " & _
                                             ", CASE WHEN hremployee_master.gender = 1 THEN ''Male'' " & _
                                             "       WHEN hremployee_master.gender = 2 THEN ''Female'' " & _
                                             "       WHEN hremployee_master.gender = 3 THEN ''Other'' " & _
                                             "  ELSE '''' END AS gendertype " & _
                                             ", ISNULL(emptype.NAME, '''') AS employmenttype " & _
                                             ", ISNULL(ptype.NAME, '''') AS paytype " & _
                                             ", ISNULL(prpaypoint_master.paypointname,'''') AS paypointname " & _
                                             ", hremployee_master.loginname AS loginname " & _
                                             ", '''' AS password " & _
                                             ", hremployee_master.email AS email " & _
                                             ", hremployee_master.displayname AS displayname " & _
                                             ", ISNULL(tnashift_master.shiftname,'''') AS shiftname " & _
                                             ", hremployee_master.birthdate AS birthdate " & _
                                             ", hremployee_master.birth_ward AS birth_ward " & _
                                             ", hremployee_master.birthcertificateno AS birthcertificateno " & _
                                             ", ISNULL(bst.NAME, '''') AS birthstatename " & _
                                             ", ISNULL(cct.country_name, '''') AS birthcountryname " & _
                                             ", ISNULL(ccty.NAME, '''') AS birthcityname " & _
                                             ", hremployee_master.birth_village AS birth_village " & _
                                             ", ISNULL(EWPT.work_permit_no,hremployee_master.work_permit_no) AS work_permit_no " & _
                                             ", ISNULL(WPCT.country_name,ISNULL(wct.country_name,'''')) AS workcountryname " & _
                                             ", ISNULL(EWPT.issue_place,hremployee_master.work_permit_issue_place) AS work_permit_issue_place " & _
                                             ", ISNULL(EWPT.issue_date,hremployee_master.work_permit_issue_date) AS work_permit_issue_date " & _
                                             ", ISNULL(EWPT.expiry_date,hremployee_master.work_permit_expiry_date) AS work_permit_expiry_date " & _
                                             ", ISNULL(ctype.NAME,'''') AS complexionname " & _
                                             ", ISNULL(btype.NAME,'''') AS bloodgroupname " & _
                                             ", ISNULL(etype.NAME,'''') AS eyecolorname " & _
                                             ", ISNULL(nct.country_name, '''') AS nationalityname " & _
                                             ", ISNULL(entype.NAME,'''') AS ethnicityname " & _
                                             ", ISNULL(rtype.NAME,'''') AS religionname " & _
                                             ", ISNULL(htype.NAME,'''') AS hairname " & _
                                             ", ISNULL(l1type.NAME,'''') AS language1name " & _
                                             ", ISNULL(l2type.NAME,'''') AS language2name " & _
                                             ", ISNULL(l3type.NAME,'''') AS language3name " & _
                                             ", ISNULL(l4type.NAME,'''') AS language4name " & _
                                             ", hremployee_master.extra_tel_no AS extra_tel_no " & _
                                             ", hremployee_master.height AS height " & _
                                             ", hremployee_master.weight AS weight " & _
                                             ", ISNULL(mtype.NAME, '''') AS maritalstatusname " & _
                                             ", hremployee_master.anniversary_date AS anniversary_date " & _
                                             ", hremployee_master.sports_hobbies AS sports_hobbies " & _
                                             ", hremployee_master.present_address1 AS present_address1 " & _
                                             ", hremployee_master.present_address2 AS present_address2 " & _
                                             ", ISNULL(pct.country_name, '''') AS present_countryname " & _
                                             ", ISNULL(pz.zipcode_no, '''') AS present_postcodename " & _
                                             ", ISNULL(pst.NAME, '''') AS present_statename " & _
                                             ", hremployee_master.present_provicnce AS present_provicnce " & _
                                             ", ISNULL(pcy.NAME, '''') AS present_post_townname " & _
                                             ", hremployee_master.present_road AS present_road " & _
                                             ", hremployee_master.present_estate AS present_estate " & _
                                             ", hremployee_master.present_plotNo AS present_plotNo " & _
                                             ", hremployee_master.present_mobile AS present_mobile " & _
                                             ", hremployee_master.present_alternateno AS present_alternateno " & _
                                             ", hremployee_master.present_tel_no AS present_tel_no " & _
                                             ", hremployee_master.present_fax AS present_fax " & _
                                             ", hremployee_master.present_email AS present_email " & _
                                             ", hremployee_master.domicile_address1 AS domicile_address1 " & _
                                             ", hremployee_master.domicile_address2 AS domicile_address2 " & _
                                             ", ISNULL(dct.country_name, '''') AS domicile_countryname " & _
                                             ", ISNULL(dz.zipcode_no, '''') AS domicile_postcodename " & _
                                             ", ISNULL(dst.NAME, '''') AS domicile_statename " & _
                                             ", hremployee_master.domicile_provicnce AS domicile_provicnce " & _
                                             ", ISNULL(dcy.NAME, '''') AS domicile_post_townname " & _
                                             ", hremployee_master.domicile_road AS domicile_road " & _
                                             ", hremployee_master.domicile_estate AS domicile_estate " & _
                                             ", hremployee_master.domicile_plotno AS domicile_plotno " & _
                                             ", hremployee_master.domicile_mobile AS domicile_mobile " & _
                                             ", hremployee_master.domicile_alternateno AS domicile_alternateno " & _
                                             ", hremployee_master.domicile_tel_no AS domicile_tel_no " & _
                                             ", hremployee_master.domicile_fax AS domicile_fax " & _
                                             ", hremployee_master.domicile_email AS domicile_email " & _
                                             ", hremployee_master.emer_con_firstname AS emer_con_firstname " & _
                                             ", hremployee_master.emer_con_lastname AS emer_con_lastname " & _
                                             ", hremployee_master.emer_con_address AS emer_con_address " & _
                                             ", ISNULL(ect.country_name, '''') AS emer_con_countryname " & _
                                             ", ISNULL(ez.zipcode_no, '''') AS emer_con_postcodename " & _
                                             ", hremployee_master.emer_con_state AS emer_con_state " & _
                                             ", ISNULL(est.NAME, '''') AS emer_con_statename " & _
                                             ", hremployee_master.emer_con_provicnce AS emer_con_provicnce " & _
                                             ", ISNULL(ecy.NAME, '''') AS emer_con_post_townname " & _
                                             ", hremployee_master.emer_con_road AS emer_con_road " & _
                                             ", hremployee_master.emer_con_estate AS emer_con_estate " & _
                                             ", hremployee_master.emer_con_plotno AS emer_con_plotno " & _
                                             ", hremployee_master.emer_con_mobile AS emer_con_mobile " & _
                                             ", hremployee_master.emer_con_alternateno AS emer_con_alternateno " & _
                                             ", hremployee_master.emer_con_tel_no AS emer_con_tel_no " & _
                                             ", hremployee_master.emer_con_fax AS emer_con_fax " & _
                                             ", hremployee_master.emer_con_email AS emer_con_email " & _
                                             ", ISNULL(SM.NAME,ISNULL(hrstation_master.NAME,'''')) AS stationname " & _
                                             ", ISNULL(DG.NAME,ISNULL(hrdepartment_group_master.NAME,'''')) AS deptgroupname " & _
                                             ", ISNULL(DM.NAME,ISNULL(hrdepartment_master.NAME,'''')) AS departmentname " & _
                                             ", ISNULL(SG.NAME,ISNULL(hrsectiongroup_master.NAME,'''')) AS sectiongroupname " & _
                                             ", ISNULL(SC.NAME,ISNULL(hrsection_master.NAME,'''')) AS sectionname " & _
                                             ", ISNULL(UG.NAME,ISNULL(hrunitgroup_master.NAME,'''')) AS unitgroupname " & _
                                             ", ISNULL(UM.NAME,ISNULL(hrunit_master.NAME,'''')) AS unitname " & _
                                             ", ISNULL(TM.NAME,ISNULL(hrteam_master.NAME,'''')) AS teamname " & _
                                             ", ISNULL(JG.NAME,ISNULL(hrjobgroup_master.NAME,'''')) AS jobgroupname " & _
                                             ", ISNULL(JM.job_name,ISNULL(hrjob_master.job_name,'''')) AS jobname " & _
                                             ", ISNULL(GG.NAME,ISNULL(hrgradegroup_master.NAME,'''')) AS gradegroupname " & _
                                             ", ISNULL(GM.NAME,ISNULL(hrgrade_master.NAME,'''')) AS gradename " & _
                                             ", ISNULL(GL.NAME,ISNULL(hrgradelevel_master.NAME,'''')) AS gradelevelname " & _
                                             ", ISNULL(CG.NAME,ISNULL(hrclassgroup_master.NAME,'''')) AS classgroupname " & _
                                             ", ISNULL(CM.NAME,ISNULL(hrclasses_master.NAME,'''')) AS classname " & _
                                             ", ISNULL(CCM.costcentername,ISNULL(prcostcenter_master.costcentername,'''')) AS costcentername " & _
                                             ", ISNULL(prtranhead_master.trnheadname,'''') AS tranhedname " & _
                                             ", ISNULL(ETERM.TR_REASON,ISNULL(TR.name,'''')) AS actionreasonname " & _
                                             ", ISNULL(ESUSP.suspended_from_date,hremployee_master.suspended_from_date) AS suspended_from_date " & _
                                             ", ISNULL(ESUSP.suspended_to_date,hremployee_master.suspended_to_date) AS suspended_to_date " & _
                                             ", ISNULL(EPROB.probation_from_date,hremployee_master.probation_from_date) AS probation_from_date " & _
                                             ", ISNULL(EPROB.probation_to_date,hremployee_master.probation_to_date) AS probation_to_date " & _
                                             ", ISNULL(ETERM.termination_from_date,hremployee_master.termination_from_date) AS termination_from_date " & _
                                             ", ISNULL(ERET.termination_to_date,hremployee_master.termination_to_date) AS termination_to_date " & _
                                             ", hremployee_master.remark AS remark " & _
                                             ", hremployee_master.emer_con_firstname2 AS emer_con_firstname2 " & _
                                             ", hremployee_master.emer_con_lastname2 AS emer_con_lastname2 " & _
                                             ", hremployee_master.emer_con_address2 AS emer_con_address2 " & _
                                             ", ISNULL(ect2.country_name, '''') AS emer_con_countryname2 " & _
                                             ", ISNULL(ez2.zipcode_no, '''') AS emer_con_postcodename2 " & _
                                             ", ISNULL(est2.NAME, '''') AS emer_con_statename2 " & _
                                             ", hremployee_master.emer_con_provicnce2 AS emer_con_provicnce2 " & _
                                             ", ISNULL(ecy2.NAME, '''') AS emer_con_post_townname2 " & _
                                             ", hremployee_master.emer_con_road2 AS emer_con_road2 " & _
                                             ", hremployee_master.emer_con_estate2 AS emer_con_estate2 " & _
                                             ", hremployee_master.emer_con_plotno2 AS emer_con_plotno2 " & _
                                             ", hremployee_master.emer_con_mobile2 AS emer_con_mobile2 " & _
                                             ", hremployee_master.emer_con_alternateno2 AS emer_con_alternateno2 " & _
                                             ", hremployee_master.emer_con_tel_no2 AS emer_con_tel_no2 " & _
                                             ", hremployee_master.emer_con_fax2 AS emer_con_fax2 " & _
                                             ", hremployee_master.emer_con_email2 AS emer_con_email2 " & _
                                             ", hremployee_master.emer_con_firstname3 AS emer_con_firstname3 " & _
                                             ", hremployee_master.emer_con_lastname3 AS emer_con_lastname3 " & _
                                             ", hremployee_master.emer_con_address3 AS emer_con_address3 " & _
                                             ", ISNULL(ect3.country_name, '''') AS emer_con_countryname3 " & _
                                             ", ISNULL(ez3.zipcode_no, '''') AS emer_con_postcodename3 " & _
                                             ", ISNULL(est3.NAME, '''') AS emer_con_statename3 " & _
                                             ", hremployee_master.emer_con_provicnce3 AS emer_con_provicnce3 " & _
                                             ", ISNULL(ecy3.NAME, '''') AS emer_con_post_townname3 " & _
                                             ", hremployee_master.emer_con_road3 AS emer_con_road3 " & _
                                             ", hremployee_master.emer_con_estate3 AS emer_con_estate3 " & _
                                             ", hremployee_master.emer_con_plotno3 AS emer_con_plotno3 " & _
                                             ", hremployee_master.emer_con_mobile3 AS emer_con_mobile3 " & _
                                             ", hremployee_master.emer_con_alternateno3 AS emer_con_alternateno3 " & _
                                             ", hremployee_master.emer_con_tel_no3 AS emer_con_tel_no3 " & _
                                             ", hremployee_master.emer_con_fax3 AS emer_con_fax3 " & _
                                             ", hremployee_master.emer_con_email3 AS emer_con_email3 " & _
                                             ", hremployee_master.isclear AS isclear " & _
                                             ", ISNULL(ECNF.confirmation_date,hremployee_master.confirmation_date) AS confirmation_date " & _
                                             ", ISNULL(ETERM.empl_enddate,hremployee_master.empl_enddate) AS empl_enddate " & _
                                             ", hremployee_master.allocationreason AS allocationreason " & _
                                             ", ISNULL(ERH.reinstatment_date,hremployee_master.reinstatement_date) AS reinstatement_date " & _
                                             ", ISNULL(ETERM.isexclude_payroll,hremployee_master.isexclude_payroll) AS isexclude_payroll " & _
                                             ", hremployee_master.isapproved AS isapproved " & _
                                             ", CASE WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "      AND ISNULL(CONVERT(CHAR(8), ISNULL(ETERM.termination_from_date,hremployee_master.termination_from_date), 112),CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "      AND ISNULL(CONVERT(CHAR(8), ISNULL(ERET.termination_to_date,hremployee_master.termination_to_date), 112),CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "      AND ISNULL(CONVERT(CHAR(8), ISNULL(ETERM.empl_enddate,hremployee_master.empl_enddate), 112), CONVERT(CHAR(8), GETDATE(), 112)) >= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             "  THEN 1  ELSE 0 END AS activestatus " & _
                                             ", hremployee_master.titleunkid AS titleunkid " & _
                                             ", hremployee_master.gender AS gender " & _
                                             ", hremployee_master.employmenttypeunkid AS employmenttypeunkid " & _
                                             ", hremployee_master.paytypeunkid AS paytypeunkid " & _
                                             ", hremployee_master.paypointunkid AS paypointunkid " & _
                                             ", hremployee_master.shiftunkid AS shiftunkid " & _
                                             ", hremployee_master.birthstateunkid AS birthstateunkid " & _
                                             ", hremployee_master.birthcountryunkid AS birthcountryunkid " & _
                                             ", hremployee_master.birthcityunkid AS birthcityunkid " & _
                                             ", ISNULL(EWPT.workcountryunkid,hremployee_master.workcountryunkid) AS workcountryunkid " & _
                                             ", hremployee_master.complexionunkid AS complexionunkid " & _
                                             ", hremployee_master.bloodgroupunkid AS bloodgroupunkid " & _
                                             ", hremployee_master.eyecolorunkid AS eyecolorunkid " & _
                                             ", hremployee_master.nationalityunkid AS nationalityunkid " & _
                                             ", hremployee_master.ethnicityunkid AS ethnicityunkid " & _
                                             ", hremployee_master.religionunkid AS religionunkid " & _
                                             ", hremployee_master.hairunkid AS hairunkid " & _
                                             ", hremployee_master.language1unkid AS language1unkid " & _
                                             ", hremployee_master.language2unkid AS language2unkid " & _
                                             ", hremployee_master.language3unkid AS language3unkid " & _
                                             ", hremployee_master.language4unkid AS language4unkid " & _
                                             ", hremployee_master.maritalstatusunkid AS maritalstatusunkid " & _
                                             ", hremployee_master.present_countryunkid AS present_countryunkid " & _
                                             ", hremployee_master.present_postcodeunkid AS present_postcodeunkid " & _
                                             ", hremployee_master.present_stateunkid AS present_stateunkid " & _
                                             ", hremployee_master.present_post_townunkid AS present_post_townunkid " & _
                                             ", hremployee_master.domicile_countryunkid AS domicile_countryunkid " & _
                                             ", hremployee_master.domicile_postcodeunkid AS domicile_postcodeunkid " & _
                                             ", hremployee_master.domicile_stateunkid AS domicile_stateunkid " & _
                                             ", hremployee_master.domicile_post_townunkid AS domicile_post_townunkid " & _
                                             ", hremployee_master.emer_con_countryunkid AS emer_con_countryunkid " & _
                                             ", hremployee_master.emer_con_postcodeunkid AS emer_con_postcodeunkid " & _
                                             ", hremployee_master.emer_con_post_townunkid AS emer_con_post_townunkid " & _
                                             ", ISNULL(SM.stationunkid,hremployee_master.stationunkid) AS stationunkid " & _
                                             ", ISNULL(DG.deptgroupunkid,hremployee_master.deptgroupunkid) AS deptgroupunkid " & _
                                             ", ISNULL(DM.departmentunkid,hremployee_master.departmentunkid) AS departmentunkid " & _
                                             ", ISNULL(SG.sectiongroupunkid,hremployee_master.sectiongroupunkid) AS sectiongroupunkid " & _
                                             ", ISNULL(SC.sectionunkid,hremployee_master.sectionunkid) AS sectionunkid " & _
                                             ", ISNULL(UG.unitgroupunkid,hremployee_master.unitgroupunkid) AS unitgroupunkid " & _
                                             ", ISNULL(UM.unitunkid,hremployee_master.unitunkid) AS unitunkid " & _
                                             ", ISNULL(TM.teamunkid,hremployee_master.teamunkid) AS teamunkid " & _
                                             ", ISNULL(JG.jobgroupunkid,hremployee_master.jobgroupunkid) AS jobgroupunkid " & _
                                             ", ISNULL(JM.jobunkid,hremployee_master.jobunkid) AS jobunkid " & _
                                             ", ISNULL(GG.gradegroupunkid,hremployee_master.gradegroupunkid) AS gradegroupunkid " & _
                                             ", ISNULL(GM.gradeunkid,hremployee_master.gradeunkid) AS gradeunkid " & _
                                             ", ISNULL(GL.gradelevelunkid,hremployee_master.gradelevelunkid) AS gradelevelunkid " & _
                                             ", ISNULL(CG.classgroupunkid,hremployee_master.classgroupunkid) AS classgroupunkid " & _
                                             ", ISNULL(CM.classesunkid,hremployee_master.classunkid) AS classunkid " & _
                                             ", ISNULL(ECCT.costcenterunkid,hremployee_master.costcenterunkid) AS costcenterunkid " & _
                                             ", hremployee_master.tranhedunkid AS tranhedunkid " & _
                                             ", ISNULL(ETERM.changereasonunkid,hremployee_master.actionreasonunkid) AS actionreasonunkid " & _
                                             ", hremployee_master.emer_con_countryunkid2 AS emer_con_countryunkid2 " & _
                                             ", hremployee_master.emer_con_postcodeunkid2 AS emer_con_postcodeunkid2 " & _
                                             ", hremployee_master.emer_con_state2 AS emer_con_state2 " & _
                                             ", hremployee_master.emer_con_post_townunkid2 AS emer_con_post_townunkid2 " & _
                                             ", hremployee_master.emer_con_countryunkid3 AS emer_con_countryunkid3 " & _
                                             ", hremployee_master.emer_con_postcodeunkid3 AS emer_con_postcodeunkid3 " & _
                                             ", hremployee_master.emer_con_state3 AS emer_con_state3 " & _
                                             ", hremployee_master.emer_con_post_townunkid3 AS emer_con_post_townunkid3 " & _
                                             ", hremployee_master.companyunkid AS companyunkid " & _
                                             ", reportingemployeecode AS reportingemployeecode " & _
                                             ", reportingtoemployee AS reportingtoemployee " & _
                                             ", GETDATE() AS modify_date "


                    Dim StrJoins As String = "LEFT JOIN cfcommon_master AS title ON title.masterunkid = hremployee_master.titleunkid AND title.mastertype = 24 " & _
                                             "LEFT JOIN cfcommon_master AS emptype ON emptype.masterunkid = hremployee_master.employmenttypeunkid AND emptype.mastertype = 8 " & _
                                             "LEFT JOIN cfcommon_master AS ptype ON ptype.masterunkid = hremployee_master.paytypeunkid AND ptype.mastertype = 17 " & _
                                             "LEFT JOIN prpaypoint_master ON prpaypoint_master.paypointunkid = hremployee_master.paypointunkid " & _
                                             "LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS bst ON bst.stateunkid = hremployee_master.birthstateunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS cct ON cct.countryunkid = hremployee_master.birthcountryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ccty ON ccty.cityunkid = hremployee_master.birthcityunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS wct ON wct.countryunkid = hremployee_master.workcountryunkid " & _
                                             "LEFT JOIN cfcommon_master AS ctype ON ctype.masterunkid = hremployee_master.complexionunkid AND ctype.mastertype = 6 " & _
                                             "LEFT JOIN cfcommon_master AS btype ON btype.masterunkid = hremployee_master.bloodgroupunkid AND btype.mastertype = 5 " & _
                                             "LEFT JOIN cfcommon_master AS etype ON etype.masterunkid = hremployee_master.eyecolorunkid AND etype.mastertype = 10 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS nct ON nct.countryunkid = hremployee_master.nationalityunkid " & _
                                             "LEFT JOIN cfcommon_master AS entype ON entype.masterunkid = hremployee_master.ethnicityunkid AND entype.mastertype = 9 " & _
                                             "LEFT JOIN cfcommon_master AS rtype ON rtype.masterunkid = hremployee_master.religionunkid AND rtype.mastertype = 20 " & _
                                             "LEFT JOIN cfcommon_master AS htype ON htype.masterunkid = hremployee_master.hairunkid AND htype.mastertype = 11 " & _
                                             "LEFT JOIN cfcommon_master AS l1type ON l1type.masterunkid = hremployee_master.language1unkid AND l1type.mastertype = 14 " & _
                                             "LEFT JOIN cfcommon_master AS l2type ON l2type.masterunkid = hremployee_master.language2unkid AND l1type.mastertype = 14 " & _
                                             "LEFT JOIN cfcommon_master AS l3type ON l3type.masterunkid = hremployee_master.language3unkid AND l1type.mastertype = 14 " & _
                                             "LEFT JOIN cfcommon_master AS l4type ON l4type.masterunkid = hremployee_master.language4unkid AND l1type.mastertype = 14 " & _
                                             "LEFT JOIN cfcommon_master AS mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = 15 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS pct ON pct.countryunkid = hremployee_master.present_countryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS pz ON pz.zipcodeunkid = hremployee_master.present_postcodeunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS pst ON pst.stateunkid = hremployee_master.present_stateunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS pcy ON pcy.cityunkid = hremployee_master.present_post_townunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS dct ON dct.countryunkid = hremployee_master.domicile_countryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS dz ON dz.zipcodeunkid = hremployee_master.domicile_postcodeunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS dst ON dst.stateunkid = hremployee_master.domicile_stateunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS dcy ON dcy.cityunkid = hremployee_master.domicile_post_townunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect ON ect.countryunkid = hremployee_master.emer_con_countryunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez ON ez.zipcodeunkid = hremployee_master.emer_con_postcodeunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS est ON est.stateunkid = hremployee_master.emer_con_state " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy ON ecy.cityunkid = hremployee_master.emer_con_post_townunkid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect2 ON ect2.countryunkid = hremployee_master.emer_con_countryunkid2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez2 ON ez2.zipcodeunkid = hremployee_master.emer_con_postcodeunkid2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS est2 ON est2.stateunkid = hremployee_master.emer_con_state2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy2 ON ecy2.cityunkid = hremployee_master.emer_con_post_townunkid2 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS ect3 ON ect3.countryunkid = hremployee_master.emer_con_countryunkid3 " & _
                                             "LEFT JOIN hrmsConfiguration..cfzipcode_master AS ez3 ON ez3.zipcodeunkid = hremployee_master.emer_con_postcodeunkid3 " & _
                                             "LEFT JOIN hrmsConfiguration..cfstate_master AS est3 ON est3.stateunkid = hremployee_master.emer_con_state3 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcity_master AS ecy3 ON ecy3.cityunkid = hremployee_master.emer_con_post_townunkid3 " & _
                                             "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hremployee_master.stationunkid " & _
                                             "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hremployee_master.deptgroupunkid " & _
                                             "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                                             "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hremployee_master.sectionunkid " & _
                                             "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hremployee_master.unitunkid " & _
                                             "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
                                             "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                                             "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = hremployee_master.gradegroupunkid " & _
                                             "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hremployee_master.gradeunkid " & _
                                             "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hremployee_master.gradelevelunkid " & _
                                             "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hremployee_master.classgroupunkid " & _
                                             "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hremployee_master.classunkid " & _
                                             "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = hremployee_master.costcenterunkid " & _
                                             "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_master.tranhedunkid " & _
                                             "LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = 48  " & _
                                             "LEFT JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid " & _
                                             "LEFT JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                                             "LEFT JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "  SELECT " & _
                                             "       hremployee_reportto.employeeunkid AS EmpId " & _
                                             "      ,remp.employeecode AS reportingemployeecode " & _
                                             "      ,ISNULL(remp.firstname,'''')+'' ''+ISNULL(remp.othername,'''')+'' ''+ISNULL(remp.surname,'''') AS reportingtoemployee " & _
                                             "      ,ROW_NUMBER() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                             "  FROM hremployee_reportto " & _
                                             "      JOIN hremployee_master AS remp ON hremployee_reportto.reporttoemployeeunkid = remp.employeeunkid " & _
                                             "  WHERE ishierarchy = 1  AND isvoid = 0 " & _
                                             "      AND CONVERT(CHAR(8), effectivedate, 112) <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' " & _
                                             ")AS A ON A.EmpId = hremployee_master.employeeunkid  AND A.Rno = 1 " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           ETT.employeeunkid AS TrfEmpId " & _
                                             "          ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                                             "          ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                                             "          ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                                             "          ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                             "          ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                                             "          ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                                             "          ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                                             "          ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                                             "          ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                                             "          ,ISNULL(ETT.classunkid,0) AS classunkid " & _
                                             "          ,CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                                             "          ,ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_transfer_tran AS ETT " & _
                                             "      WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.Rno = 1 " & _
                                             "LEFT JOIN hrstation_master AS SM ON SM.stationunkid = ETRF.stationunkid " & _
                                             "LEFT JOIN hrdepartment_group_master AS DG ON DG.deptgroupunkid = ETRF.deptgroupunkid " & _
                                             "LEFT JOIN hrdepartment_master AS DM ON DM.departmentunkid = ETRF.departmentunkid " & _
                                             "LEFT JOIN hrsection_master AS SC ON SC.sectionunkid = ETRF.sectionunkid " & _
                                             "LEFT JOIN hrunit_master AS UM ON UM.unitunkid = ETRF.unitunkid " & _
                                             "LEFT JOIN hrclassgroup_master AS CG ON CG.classgroupunkid = ETRF.classgroupunkid " & _
                                             "LEFT JOIN hrclasses_master AS CM ON CM.classesunkid = ETRF.classunkid " & _
                                             "LEFT JOIN hrteam_master AS TM ON ETRF.teamunkid = TM.teamunkid " & _
                                             "LEFT JOIN hrunitgroup_master AS UG ON ETRF.unitgroupunkid = UG.unitgroupunkid " & _
                                             "LEFT JOIN hrsectiongroup_master AS SG ON ETRF.sectiongroupunkid = SG.sectiongroupunkid " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "         ECT.employeeunkid AS CatEmpId " & _
                                             "        ,ECT.jobgroupunkid " & _
                                             "        ,ECT.jobunkid " & _
                                             "        ,CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                                             "        ,ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_categorization_tran AS ECT " & _
                                             "      WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.Rno = 1 " & _
                                             "LEFT JOIN hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid " & _
                                             "LEFT JOIN hrjobgroup_master AS JG ON JG.jobgroupunkid = ERECAT.jobgroupunkid " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           gradegroupunkid " & _
                                             "          ,gradeunkid " & _
                                             "          ,gradelevelunkid " & _
                                             "          ,employeeunkid " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                             "      FROM prsalaryincrement_tran AS ESI " & _
                                             "      WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS EGRD ON EGRD.employeeunkid = hremployee_master.employeeunkid AND EGRD.Rno = 1 " & _
                                             "LEFT JOIN hrgradegroup_master AS GG ON GG.gradegroupunkid = EGRD.gradegroupunkid " & _
                                             "LEFT JOIN hrgrade_master AS GM ON GM.gradeunkid = EGRD.gradeunkid " & _
                                             "LEFT JOIN hrgradelevel_master AS GL ON GL.gradelevelunkid = EGRD.gradelevelunkid " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           CCT.employeeunkid AS CCTEmpId " & _
                                             "          ,CCT.cctranheadvalueid AS costcenterunkid " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_cctranhead_tran AS CCT " & _
                                             "      WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.Rno = 1 " & _
                                             "LEFT JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = ECCT.costcenterunkid " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           WPT.employeeunkid AS WPTEmpId " & _
                                             "          ,WPT.work_permit_no " & _
                                             "          ,WPT.workcountryunkid " & _
                                             "          ,WPT.issue_place " & _
                                             "          ,WPT.issue_date AS issue_date " & _
                                             "          ,WPT.expiry_date AS expiry_date " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY WPT.employeeunkid ORDER BY WPT.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_work_permit_tran AS WPT " & _
                                             "      WHERE WPT.isvoid = 0 AND WPT.isresidentpermit = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) AND work_permit_no <> '''' " & _
                                             ") AS EWPT ON EWPT.WPTEmpId = hremployee_master.employeeunkid AND EWPT.Rno = 1 " & _
                                             "LEFT JOIN hrmsConfiguration..cfcountry_master AS WPCT ON WPCT.countryunkid = EWPT.workcountryunkid " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           SDT.employeeunkid AS SDEmpId " & _
                                             "          ,SDT.date1 AS suspended_from_date " & _
                                             "          ,SDT.date2 AS suspended_to_date " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY SDT.employeeunkid ORDER BY SDT.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_dates_tran AS SDT " & _
                                             "      WHERE SDT.isvoid = 0 AND SDT.datetypeunkid = 3 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ESUSP ON ESUSP.SDEmpId = hremployee_master.employeeunkid AND ESUSP.Rno = 1 " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           PDT.employeeunkid AS PDEmpId " & _
                                             "          ,PDT.date1 AS probation_from_date " & _
                                             "          ,PDT.date2 AS probation_to_date " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_dates_tran AS PDT " & _
                                             "      WHERE PDT.isvoid = 0 AND PDT.datetypeunkid = 1 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid AND EPROB.Rno = 1 " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           TRM.employeeunkid AS TEEmpId " & _
                                             "          ,TRM.date1 AS empl_enddate " & _
                                             "          ,TRM.date2 AS termination_from_date " & _
                                             "          ,TRM.isexclude_payroll " & _
                                             "          ,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.NAME,'''') ELSE ISNULL(TEC.NAME,'''') END AS TR_REASON " & _
                                             "          ,TRM.changereasonunkid " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_dates_tran AS TRM " & _
                                             "          LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = 49 " & _
                                             "          LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = 48 " & _
                                             "      WHERE TRM.isvoid = 0 AND TRM.datetypeunkid = 4 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           CNF.employeeunkid AS CEmpId " & _
                                             "          ,CNF.date1 AS confirmation_date " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_dates_tran AS CNF " & _
                                             "      WHERE CNF.isvoid = 0 AND CNF.datetypeunkid = 2 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid AND ECNF.Rno = 1 " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "         RTD.employeeunkid AS REmpId " & _
                                             "        ,RTD.date1 AS termination_to_date " & _
                                             "        ,ROW_NUMBER() OVER ( PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_dates_tran AS RTD " & _
                                             "      WHERE RTD.isvoid = 0 AND RTD.datetypeunkid = 6 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.Rno = 1 " & _
                                             "LEFT JOIN " & _
                                             "( " & _
                                             "      SELECT " & _
                                             "           ERT.employeeunkid AS RHEmpId " & _
                                             "          ,ERT.reinstatment_date AS reinstatment_date " & _
                                             "          ,ROW_NUMBER() OVER ( PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC ) AS Rno " & _
                                             "      FROM hremployee_rehire_tran AS ERT " & _
                                             "      WHERE ERT.isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), GETDATE(), 112) " & _
                                             ") AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid " & _
                                             "AND ERH.Rno = 1 "

                    'S.SANDEEP [16-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 118
                    '---------- ADDED : WPT.isresidentpermit = 0
                    'S.SANDEEP [16-Jan-2018] -- END


                    Dim StrUpdate As String = " IDM_DATA..hremployee_master.employeeunkid = B.employeeunkid " & _
                                              ",IDM_DATA..hremployee_master.employeecode = B.employeecode " & _
                                              ",IDM_DATA..hremployee_master.title = B.title " & _
                                              ",IDM_DATA..hremployee_master.firstname = B.firstname " & _
                                              ",IDM_DATA..hremployee_master.surname = B.surname " & _
                                              ",IDM_DATA..hremployee_master.othername = B.othername " & _
                                              ",IDM_DATA..hremployee_master.appointeddate = B.appointeddate " & _
                                              ",IDM_DATA..hremployee_master.gendertype = B.gendertype " & _
                                              ",IDM_DATA..hremployee_master.employmenttype = B.employmenttype " & _
                                              ",IDM_DATA..hremployee_master.paytype = B.paytype " & _
                                              ",IDM_DATA..hremployee_master.paypointname = B.paypointname " & _
                                              ",IDM_DATA..hremployee_master.loginname = B.loginname " & _
                                              ",IDM_DATA..hremployee_master.email = B.email " & _
                                              ",IDM_DATA..hremployee_master.displayname = B.displayname " & _
                                              ",IDM_DATA..hremployee_master.shiftname = B.shiftname " & _
                                              ",IDM_DATA..hremployee_master.birthdate = B.birthdate " & _
                                              ",IDM_DATA..hremployee_master.birth_ward = B.birth_ward " & _
                                              ",IDM_DATA..hremployee_master.birthcertificateno = B.birthcertificateno " & _
                                              ",IDM_DATA..hremployee_master.birthstatename = B.birthstatename " & _
                                              ",IDM_DATA..hremployee_master.birthcountryname = B.birthcountryname " & _
                                              ",IDM_DATA..hremployee_master.birthcityname = B.birthcityname " & _
                                              ",IDM_DATA..hremployee_master.birth_village = B.birth_village " & _
                                              ",IDM_DATA..hremployee_master.work_permit_no = B.work_permit_no " & _
                                              ",IDM_DATA..hremployee_master.workcountryname = B.workcountryname " & _
                                              ",IDM_DATA..hremployee_master.work_permit_issue_place = B.work_permit_issue_place " & _
                                              ",IDM_DATA..hremployee_master.work_permit_issue_date = B.work_permit_issue_date " & _
                                              ",IDM_DATA..hremployee_master.work_permit_expiry_date = B.work_permit_expiry_date " & _
                                              ",IDM_DATA..hremployee_master.complexionname = B.complexionname " & _
                                              ",IDM_DATA..hremployee_master.bloodgroupname = B.bloodgroupname " & _
                                              ",IDM_DATA..hremployee_master.eyecolorname = B.eyecolorname " & _
                                              ",IDM_DATA..hremployee_master.nationalityname = B.nationalityname " & _
                                              ",IDM_DATA..hremployee_master.ethnicityname = B.ethnicityname " & _
                                              ",IDM_DATA..hremployee_master.religionname = B.religionname " & _
                                              ",IDM_DATA..hremployee_master.hairname = B.hairname " & _
                                              ",IDM_DATA..hremployee_master.language1name = B.language1name " & _
                                              ",IDM_DATA..hremployee_master.language2name = B.language2name " & _
                                              ",IDM_DATA..hremployee_master.language3name = B.language3name " & _
                                              ",IDM_DATA..hremployee_master.language4name = B.language4name " & _
                                              ",IDM_DATA..hremployee_master.extra_tel_no = B.extra_tel_no " & _
                                              ",IDM_DATA..hremployee_master.height = B.height " & _
                                              ",IDM_DATA..hremployee_master.weight = B.weight " & _
                                              ",IDM_DATA..hremployee_master.maritalstatusname = B.maritalstatusname " & _
                                              ",IDM_DATA..hremployee_master.anniversary_date = B.anniversary_date " & _
                                              ",IDM_DATA..hremployee_master.sports_hobbies = B.sports_hobbies " & _
                                              ",IDM_DATA..hremployee_master.present_address1 = B.present_address1 " & _
                                              ",IDM_DATA..hremployee_master.present_address2 = B.present_address2 " & _
                                              ",IDM_DATA..hremployee_master.present_countryname = B.present_countryname " & _
                                              ",IDM_DATA..hremployee_master.present_postcodename = B.present_postcodename " & _
                                              ",IDM_DATA..hremployee_master.present_statename = B.present_statename " & _
                                              ",IDM_DATA..hremployee_master.present_provicnce = B.present_provicnce " & _
                                              ",IDM_DATA..hremployee_master.present_post_townname = B.present_post_townname " & _
                                              ",IDM_DATA..hremployee_master.present_road = B.present_road " & _
                                              ",IDM_DATA..hremployee_master.present_estate = B.present_estate " & _
                                              ",IDM_DATA..hremployee_master.present_plotNo = B.present_plotNo " & _
                                              ",IDM_DATA..hremployee_master.present_mobile = B.present_mobile " & _
                                              ",IDM_DATA..hremployee_master.present_alternateno = B.present_alternateno " & _
                                              ",IDM_DATA..hremployee_master.present_tel_no = B.present_tel_no " & _
                                              ",IDM_DATA..hremployee_master.present_fax = B.present_fax " & _
                                              ",IDM_DATA..hremployee_master.present_email = B.present_email " & _
                                              ",IDM_DATA..hremployee_master.domicile_address1 = B.domicile_address1 " & _
                                              ",IDM_DATA..hremployee_master.domicile_address2 = B.domicile_address2 " & _
                                              ",IDM_DATA..hremployee_master.domicile_countryname = B.domicile_countryname " & _
                                              ",IDM_DATA..hremployee_master.domicile_postcodename = B.domicile_postcodename " & _
                                              ",IDM_DATA..hremployee_master.domicile_statename = B.domicile_statename " & _
                                              ",IDM_DATA..hremployee_master.domicile_provicnce = B.domicile_provicnce " & _
                                              ",IDM_DATA..hremployee_master.domicile_post_townname = B.domicile_post_townname " & _
                                              ",IDM_DATA..hremployee_master.domicile_road = B.domicile_road " & _
                                              ",IDM_DATA..hremployee_master.domicile_estate = B.domicile_estate " & _
                                              ",IDM_DATA..hremployee_master.domicile_plotno = B.domicile_plotno " & _
                                              ",IDM_DATA..hremployee_master.domicile_mobile = B.domicile_mobile " & _
                                              ",IDM_DATA..hremployee_master.domicile_alternateno = B.domicile_alternateno " & _
                                              ",IDM_DATA..hremployee_master.domicile_tel_no = B.domicile_tel_no " & _
                                              ",IDM_DATA..hremployee_master.domicile_fax = B.domicile_fax " & _
                                              ",IDM_DATA..hremployee_master.domicile_email = B.domicile_email " & _
                                              ",IDM_DATA..hremployee_master.emer_con_firstname = B.emer_con_firstname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_lastname = B.emer_con_lastname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_address = B.emer_con_address " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryname = B.emer_con_countryname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodename = B.emer_con_postcodename " & _
                                              ",IDM_DATA..hremployee_master.emer_con_state = B.emer_con_state " & _
                                              ",IDM_DATA..hremployee_master.emer_con_statename = B.emer_con_statename " & _
                                              ",IDM_DATA..hremployee_master.emer_con_provicnce = B.emer_con_provicnce " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townname = B.emer_con_post_townname " & _
                                              ",IDM_DATA..hremployee_master.emer_con_road = B.emer_con_road " & _
                                              ",IDM_DATA..hremployee_master.emer_con_estate = B.emer_con_estate " & _
                                              ",IDM_DATA..hremployee_master.emer_con_plotno = B.emer_con_plotno " & _
                                              ",IDM_DATA..hremployee_master.emer_con_mobile = B.emer_con_mobile " & _
                                              ",IDM_DATA..hremployee_master.emer_con_alternateno = B.emer_con_alternateno " & _
                                              ",IDM_DATA..hremployee_master.emer_con_tel_no = B.emer_con_tel_no " & _
                                              ",IDM_DATA..hremployee_master.emer_con_fax = B.emer_con_fax " & _
                                              ",IDM_DATA..hremployee_master.emer_con_email = B.emer_con_email " & _
                                              ",IDM_DATA..hremployee_master.stationname = B.stationname " & _
                                              ",IDM_DATA..hremployee_master.deptgroupname = B.deptgroupname " & _
                                              ",IDM_DATA..hremployee_master.departmentname = B.departmentname " & _
                                              ",IDM_DATA..hremployee_master.sectiongroupname = B.sectiongroupname " & _
                                              ",IDM_DATA..hremployee_master.sectionname = B.sectionname " & _
                                              ",IDM_DATA..hremployee_master.unitgroupname = B.unitgroupname " & _
                                              ",IDM_DATA..hremployee_master.unitname = B.unitname " & _
                                              ",IDM_DATA..hremployee_master.teamname = B.teamname " & _
                                              ",IDM_DATA..hremployee_master.jobgroupname = B.jobgroupname " & _
                                              ",IDM_DATA..hremployee_master.jobname = B.jobname " & _
                                              ",IDM_DATA..hremployee_master.gradegroupname = B.gradegroupname " & _
                                              ",IDM_DATA..hremployee_master.gradename = B.gradename " & _
                                              ",IDM_DATA..hremployee_master.gradelevelname = B.gradelevelname " & _
                                              ",IDM_DATA..hremployee_master.classgroupname = B.classgroupname " & _
                                              ",IDM_DATA..hremployee_master.classname = B.classname " & _
                                              ",IDM_DATA..hremployee_master.costcentername = B.costcentername " & _
                                              ",IDM_DATA..hremployee_master.tranhedname = B.tranhedname " & _
                                              ",IDM_DATA..hremployee_master.actionreasonname = B.actionreasonname " & _
                                              ",IDM_DATA..hremployee_master.suspended_from_date = B.suspended_from_date " & _
                                              ",IDM_DATA..hremployee_master.suspended_to_date = B.suspended_to_date " & _
                                              ",IDM_DATA..hremployee_master.probation_from_date = B.probation_from_date " & _
                                              ",IDM_DATA..hremployee_master.probation_to_date = B.probation_to_date " & _
                                              ",IDM_DATA..hremployee_master.termination_from_date = B.termination_from_date " & _
                                              ",IDM_DATA..hremployee_master.termination_to_date = B.termination_to_date " & _
                                              ",IDM_DATA..hremployee_master.remark = B.remark " & _
                                              ",IDM_DATA..hremployee_master.emer_con_firstname2 = B.emer_con_firstname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_lastname2 = B.emer_con_lastname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_address2 = B.emer_con_address2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryname2 = B.emer_con_countryname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodename2 = B.emer_con_postcodename2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_statename2 = B.emer_con_statename2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_provicnce2 = B.emer_con_provicnce2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townname2 = B.emer_con_post_townname2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_road2 = B.emer_con_road2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_estate2 = B.emer_con_estate2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_plotno2 = B.emer_con_plotno2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_mobile2 = B.emer_con_mobile2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_alternateno2 = B.emer_con_alternateno2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_tel_no2 = B.emer_con_tel_no2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_fax2 = B.emer_con_fax2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_email2 = B.emer_con_email2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_firstname3 = B.emer_con_firstname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_lastname3 = B.emer_con_lastname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_address3 = B.emer_con_address3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryname3 = B.emer_con_countryname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodename3 = B.emer_con_postcodename3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_statename3 = B.emer_con_statename3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_provicnce3 = B.emer_con_provicnce3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townname3 = B.emer_con_post_townname3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_road3 = B.emer_con_road3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_estate3 = B.emer_con_estate3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_plotno3 = B.emer_con_plotno3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_mobile3 = B.emer_con_mobile3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_alternateno3 = B.emer_con_alternateno3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_tel_no3 = B.emer_con_tel_no3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_fax3 = B.emer_con_fax3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_email3 = B.emer_con_email3 " & _
                                              ",IDM_DATA..hremployee_master.isclear = B.isclear " & _
                                              ",IDM_DATA..hremployee_master.confirmation_date = B.confirmation_date " & _
                                              ",IDM_DATA..hremployee_master.empl_enddate = B.empl_enddate " & _
                                              ",IDM_DATA..hremployee_master.allocationreason = B.allocationreason " & _
                                              ",IDM_DATA..hremployee_master.reinstatement_date = B.reinstatement_date " & _
                                              ",IDM_DATA..hremployee_master.isexclude_payroll = B.isexclude_payroll " & _
                                              ",IDM_DATA..hremployee_master.isapproved = B.isapproved " & _
                                              ",IDM_DATA..hremployee_master.activestatus = B.activestatus " & _
                                              ",IDM_DATA..hremployee_master.titleunkid = B.titleunkid " & _
                                              ",IDM_DATA..hremployee_master.gender = B.gender " & _
                                              ",IDM_DATA..hremployee_master.employmenttypeunkid = B.employmenttypeunkid " & _
                                              ",IDM_DATA..hremployee_master.paytypeunkid = B.paytypeunkid " & _
                                              ",IDM_DATA..hremployee_master.paypointunkid = B.paypointunkid " & _
                                              ",IDM_DATA..hremployee_master.shiftunkid = B.shiftunkid " & _
                                              ",IDM_DATA..hremployee_master.birthstateunkid = B.birthstateunkid " & _
                                              ",IDM_DATA..hremployee_master.birthcountryunkid = B.birthcountryunkid " & _
                                              ",IDM_DATA..hremployee_master.birthcityunkid = B.birthcityunkid " & _
                                              ",IDM_DATA..hremployee_master.workcountryunkid = B.workcountryunkid " & _
                                              ",IDM_DATA..hremployee_master.complexionunkid = B.complexionunkid " & _
                                              ",IDM_DATA..hremployee_master.bloodgroupunkid = B.bloodgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.eyecolorunkid = B.eyecolorunkid " & _
                                              ",IDM_DATA..hremployee_master.nationalityunkid = B.nationalityunkid " & _
                                              ",IDM_DATA..hremployee_master.ethnicityunkid = B.ethnicityunkid " & _
                                              ",IDM_DATA..hremployee_master.religionunkid = B.religionunkid " & _
                                              ",IDM_DATA..hremployee_master.hairunkid = B.hairunkid " & _
                                              ",IDM_DATA..hremployee_master.language1unkid = B.language1unkid " & _
                                              ",IDM_DATA..hremployee_master.language2unkid = B.language2unkid " & _
                                              ",IDM_DATA..hremployee_master.language3unkid = B.language3unkid " & _
                                              ",IDM_DATA..hremployee_master.language4unkid = B.language4unkid " & _
                                              ",IDM_DATA..hremployee_master.maritalstatusunkid = B.maritalstatusunkid " & _
                                              ",IDM_DATA..hremployee_master.present_countryunkid = B.present_countryunkid " & _
                                              ",IDM_DATA..hremployee_master.present_postcodeunkid = B.present_postcodeunkid " & _
                                              ",IDM_DATA..hremployee_master.present_stateunkid = B.present_stateunkid " & _
                                              ",IDM_DATA..hremployee_master.present_post_townunkid = B.present_post_townunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_countryunkid = B.domicile_countryunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_postcodeunkid = B.domicile_postcodeunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_stateunkid = B.domicile_stateunkid " & _
                                              ",IDM_DATA..hremployee_master.domicile_post_townunkid = B.domicile_post_townunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryunkid = B.emer_con_countryunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodeunkid = B.emer_con_postcodeunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townunkid = B.emer_con_post_townunkid " & _
                                              ",IDM_DATA..hremployee_master.stationunkid = B.stationunkid " & _
                                              ",IDM_DATA..hremployee_master.deptgroupunkid = B.deptgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.departmentunkid = B.departmentunkid " & _
                                              ",IDM_DATA..hremployee_master.sectiongroupunkid = B.sectiongroupunkid " & _
                                              ",IDM_DATA..hremployee_master.sectionunkid = B.sectionunkid " & _
                                              ",IDM_DATA..hremployee_master.unitgroupunkid = B.unitgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.unitunkid = B.unitunkid " & _
                                              ",IDM_DATA..hremployee_master.teamunkid = B.teamunkid " & _
                                              ",IDM_DATA..hremployee_master.jobgroupunkid = B.jobgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.jobunkid = B.jobunkid " & _
                                              ",IDM_DATA..hremployee_master.gradegroupunkid = B.gradegroupunkid " & _
                                              ",IDM_DATA..hremployee_master.gradeunkid = B.gradeunkid " & _
                                              ",IDM_DATA..hremployee_master.gradelevelunkid = B.gradelevelunkid " & _
                                              ",IDM_DATA..hremployee_master.classgroupunkid = B.classgroupunkid " & _
                                              ",IDM_DATA..hremployee_master.classunkid = B.classunkid " & _
                                              ",IDM_DATA..hremployee_master.costcenterunkid = B.costcenterunkid " & _
                                              ",IDM_DATA..hremployee_master.tranhedunkid = B.tranhedunkid " & _
                                              ",IDM_DATA..hremployee_master.actionreasonunkid = B.actionreasonunkid " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryunkid2 = B.emer_con_countryunkid2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodeunkid2 = B.emer_con_postcodeunkid2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_state2 = B.emer_con_state2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townunkid2 = B.emer_con_post_townunkid2 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_countryunkid3 = B.emer_con_countryunkid3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_postcodeunkid3 = B.emer_con_postcodeunkid3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_state3 = B.emer_con_state3 " & _
                                              ",IDM_DATA..hremployee_master.emer_con_post_townunkid3 = B.emer_con_post_townunkid3 " & _
                                              ",IDM_DATA..hremployee_master.companyunkid = B.companyunkid " & _
                                              ",IDM_DATA..hremployee_master.reportingemployeecode = B.reportingemployeecode " & _
                                              ",IDM_DATA..hremployee_master.reportingtoemployee = B.reportingtoemployee " & _
                                              ",IDM_DATA..hremployee_master.modify_date = B.modify_date "

                    Dim sUpdateCol As String = "UPDATE(employeeunkid) OR  UPDATE(employeecode) OR  UPDATE(titleunkid) OR  UPDATE(firstname) OR  UPDATE(surname) OR  UPDATE(othername) OR  UPDATE(appointeddate) OR  UPDATE(gender) OR  UPDATE(employmenttypeunkid) OR  UPDATE(paytypeunkid) OR  UPDATE(paypointunkid) OR  UPDATE(loginname) OR  UPDATE(email) OR  UPDATE(displayname) OR  UPDATE(shiftunkid) OR  UPDATE(birthdate) OR  UPDATE(birth_ward) OR  UPDATE(birthcertificateno) OR  UPDATE(birthstateunkid) OR  UPDATE(birthcountryunkid) OR  UPDATE(birthcityunkid) OR  UPDATE(birth_village) OR  UPDATE(work_permit_no) OR  UPDATE(workcountryunkid) OR  UPDATE(work_permit_issue_place) OR  UPDATE(work_permit_issue_date) OR  UPDATE(work_permit_expiry_date) OR  UPDATE(complexionunkid) OR  UPDATE(bloodgroupunkid) OR  UPDATE(eyecolorunkid) OR  UPDATE(nationalityunkid) OR  UPDATE(ethnicityunkid) OR  UPDATE(religionunkid) OR  UPDATE(hairunkid) OR  UPDATE(language1unkid) OR  UPDATE(language2unkid) OR  UPDATE(language3unkid) OR  UPDATE(language4unkid) OR  UPDATE(extra_tel_no) OR  UPDATE(height) OR  UPDATE(weight) OR  UPDATE(maritalstatusunkid) OR  UPDATE(anniversary_date) OR  UPDATE(sports_hobbies) OR  UPDATE(present_address1) OR  UPDATE(present_address2) OR  UPDATE(present_countryunkid) OR  UPDATE(present_postcodeunkid) OR  UPDATE(present_stateunkid) OR  UPDATE(present_provicnce) OR  UPDATE(present_post_townunkid) OR  UPDATE(present_road) OR  UPDATE(present_estate) OR  UPDATE(present_plotNo) OR  UPDATE(present_mobile) OR  UPDATE(present_alternateno) OR  UPDATE(present_tel_no) OR  UPDATE(present_fax) OR  UPDATE(present_email) OR  UPDATE(domicile_address1) OR  UPDATE(domicile_address2) OR  UPDATE(domicile_countryunkid) OR  UPDATE(domicile_postcodeunkid) OR  UPDATE(domicile_stateunkid) OR  UPDATE(domicile_provicnce) OR  UPDATE(domicile_post_townunkid) OR  UPDATE(domicile_road) OR  UPDATE(domicile_estate) OR  UPDATE(domicile_plotno) OR  UPDATE(domicile_mobile) OR  UPDATE(domicile_alternateno) OR  UPDATE(domicile_tel_no) OR  UPDATE(domicile_fax) OR  UPDATE(domicile_email) OR  UPDATE(emer_con_firstname) OR  UPDATE(emer_con_lastname) OR  UPDATE(emer_con_address) OR  UPDATE(emer_con_countryunkid) OR  UPDATE(emer_con_postcodeunkid) OR  UPDATE(emer_con_state) OR  UPDATE(emer_con_provicnce) OR  UPDATE(emer_con_post_townunkid) OR  UPDATE(emer_con_road) OR  UPDATE(emer_con_estate) OR  UPDATE(emer_con_plotno) OR  UPDATE(emer_con_mobile) OR  UPDATE(emer_con_alternateno) OR  UPDATE(emer_con_tel_no) OR  UPDATE(emer_con_fax) OR  UPDATE(emer_con_email) OR  UPDATE(stationunkid) OR  UPDATE(deptgroupunkid) OR  UPDATE(departmentunkid) OR  UPDATE(sectionunkid) OR  UPDATE(unitunkid) OR  UPDATE(jobgroupunkid) OR  UPDATE(jobunkid) OR  UPDATE(gradegroupunkid) OR  UPDATE(gradeunkid) OR  UPDATE(gradelevelunkid) OR  UPDATE(accessunkid) OR  UPDATE(classgroupunkid) OR  UPDATE(classunkid) OR  UPDATE(serviceunkid) OR  UPDATE(costcenterunkid) OR  UPDATE(tranhedunkid) OR  UPDATE(actionreasonunkid) OR  UPDATE(suspended_from_date) OR  UPDATE(suspended_to_date) OR  UPDATE(probation_from_date) OR  UPDATE(probation_to_date) OR  UPDATE(termination_from_date) OR  UPDATE(termination_to_date) OR  UPDATE(remark) OR  UPDATE(isactive) OR  UPDATE(emer_con_firstname2) OR  UPDATE(emer_con_lastname2) OR  UPDATE(emer_con_address2) OR  UPDATE(emer_con_countryunkid2) OR  UPDATE(emer_con_postcodeunkid2) OR  UPDATE(emer_con_state2) OR  UPDATE(emer_con_provicnce2) OR  UPDATE(emer_con_post_townunkid2) OR  UPDATE(emer_con_road2) OR  UPDATE(emer_con_estate2) OR  UPDATE(emer_con_plotno2) OR  UPDATE(emer_con_mobile2) OR  UPDATE(emer_con_alternateno2) OR  UPDATE(emer_con_tel_no2) OR  UPDATE(emer_con_fax2) OR  UPDATE(emer_con_email2) OR  UPDATE(emer_con_firstname3) OR  UPDATE(emer_con_lastname3) OR  UPDATE(emer_con_address3) OR  UPDATE(emer_con_countryunkid3) OR  UPDATE(emer_con_postcodeunkid3) OR  UPDATE(emer_con_state3) OR  UPDATE(emer_con_provicnce3) OR  UPDATE(emer_con_post_townunkid3) OR  UPDATE(emer_con_road3) OR  UPDATE(emer_con_estate3) OR  UPDATE(emer_con_plotno3) OR  UPDATE(emer_con_mobile3) OR  UPDATE(emer_con_alternateno3) OR  UPDATE(emer_con_tel_no3) OR  UPDATE(emer_con_fax3) OR  UPDATE(emer_con_email3) OR  UPDATE(sectiongroupunkid) OR  UPDATE(unitgroupunkid) OR  UPDATE(teamunkid) OR  UPDATE(confirmation_date) OR  UPDATE(empl_enddate) OR  UPDATE(isclear) OR  UPDATE(allocationreason) OR  UPDATE(reinstatement_date) OR  UPDATE(isexclude_payroll) OR  UPDATE(isapproved) OR  UPDATE(companyunkid)"

                    Dim StrFields As String = "  employeeunkid , employeecode , title , firstname , surname , othername , appointeddate , gendertype , employmenttype " & _
                                              ", paytype , paypointname , loginname , password , email , displayname , shiftname , birthdate , birth_ward , birthcertificateno " & _
                                              ", birthstatename , birthcountryname , birthcityname , birth_village , work_permit_no , workcountryname , work_permit_issue_place " & _
                                              ", work_permit_issue_date , work_permit_expiry_date , complexionname , bloodgroupname , eyecolorname , nationalityname " & _
                                              ", ethnicityname , religionname , hairname , language1name , language2name , language3name , language4name , extra_tel_no " & _
                                              ", height , weight , maritalstatusname , anniversary_date , sports_hobbies , present_address1 , present_address2 , present_countryname " & _
                                              ", present_postcodename , present_statename , present_provicnce , present_post_townname , present_road , present_estate " & _
                                              ", present_plotNo , present_mobile , present_alternateno , present_tel_no , present_fax , present_email , domicile_address1 " & _
                                              ", domicile_address2 , domicile_countryname , domicile_postcodename , domicile_statename , domicile_provicnce , domicile_post_townname " & _
                                              ", domicile_road , domicile_estate , domicile_plotno , domicile_mobile , domicile_alternateno , domicile_tel_no , domicile_fax " & _
                                              ", domicile_email , emer_con_firstname , emer_con_lastname , emer_con_address , emer_con_countryname , emer_con_postcodename , emer_con_state " & _
                                              ", emer_con_statename , emer_con_provicnce , emer_con_post_townname , emer_con_road , emer_con_estate , emer_con_plotno , emer_con_mobile " & _
                                              ", emer_con_alternateno , emer_con_tel_no , emer_con_fax , emer_con_email , stationname , deptgroupname , departmentname , sectiongroupname " & _
                                              ", sectionname , unitgroupname , unitname , teamname , jobgroupname , jobname , gradegroupname , gradename , gradelevelname , classgroupname " & _
                                              ", classname , costcentername , tranhedname , actionreasonname , suspended_from_date , suspended_to_date , probation_from_date , probation_to_date " & _
                                              ", termination_from_date , termination_to_date , remark , emer_con_firstname2 , emer_con_lastname2 , emer_con_address2 , emer_con_countryname2 " & _
                                              ", emer_con_postcodename2 , emer_con_statename2 , emer_con_provicnce2 , emer_con_post_townname2 , emer_con_road2 , emer_con_estate2 , emer_con_plotno2 " & _
                                              ", emer_con_mobile2 , emer_con_alternateno2 , emer_con_tel_no2 , emer_con_fax2 , emer_con_email2 , emer_con_firstname3 , emer_con_lastname3 " & _
                                              ", emer_con_address3 , emer_con_countryname3 , emer_con_postcodename3 , emer_con_statename3 , emer_con_provicnce3 , emer_con_post_townname3 " & _
                                              ", emer_con_road3 , emer_con_estate3 , emer_con_plotno3 , emer_con_mobile3 , emer_con_alternateno3 , emer_con_tel_no3 , emer_con_fax3 " & _
                                              ", emer_con_email3 , isclear , confirmation_date , empl_enddate , allocationreason , reinstatement_date , isexclude_payroll , isapproved " & _
                                              ", activestatus , titleunkid , gender , employmenttypeunkid , paytypeunkid , paypointunkid , shiftunkid , birthstateunkid , birthcountryunkid " & _
                                              ", birthcityunkid , workcountryunkid , complexionunkid , bloodgroupunkid , eyecolorunkid , nationalityunkid , ethnicityunkid , religionunkid " & _
                                              ", hairunkid , language1unkid , language2unkid , language3unkid , language4unkid , maritalstatusunkid , present_countryunkid , present_postcodeunkid " & _
                                              ", present_stateunkid , present_post_townunkid , domicile_countryunkid , domicile_postcodeunkid , domicile_stateunkid , domicile_post_townunkid " & _
                                              ", emer_con_countryunkid , emer_con_postcodeunkid , emer_con_post_townunkid , stationunkid , deptgroupunkid , departmentunkid , sectiongroupunkid " & _
                                              ", sectionunkid , unitgroupunkid , unitunkid , teamunkid , jobgroupunkid , jobunkid , gradegroupunkid , gradeunkid , gradelevelunkid , classgroupunkid " & _
                                              ", classunkid , costcenterunkid , tranhedunkid , actionreasonunkid , emer_con_countryunkid2 , emer_con_postcodeunkid2 , emer_con_state2 " & _
                                              ", emer_con_post_townunkid2 , emer_con_countryunkid3 , emer_con_postcodeunkid3 , emer_con_state3 , emer_con_post_townunkid3 , companyunkid " & _
                                              ", reportingemployeecode , reportingtoemployee , modify_date "


                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_IU_Emp' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_IU_Emp " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [16-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 118
                    '---------- REMOVED : SELECT * INTO #TempUpdate FROM INSERTED
                    '---------- ADDED : SELECT E.* INTO #TempUpdate FROM hremployee_master AS E JOIN INSERTED ON INSERTED.employeeunkid = E.employeeunkid
                    'S.SANDEEP [16-Jan-2018] -- END

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_IU_Emp]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_IU_Emp] ON [dbo].[hremployee_master] " & _
                           "    AFTER INSERT,UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            SELECT E.* INTO #TempUpdate FROM hremployee_master AS E JOIN INSERTED ON INSERTED.employeeunkid = E.employeeunkid " & _
                           "            IF EXISTS(SELECT employeecode FROM hremployee_master WHERE employeecode <> '''') " & _
                           "            BEGIN " & _
                           "            IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_master JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_master.employeeunkid " & _
                           "                           AND #TempUpdate.companyunkid = IDM_DATA..hremployee_master.companyunkid) " & _
                           "            BEGIN " & _
                           "                INSERT  INTO IDM_DATA..hremployee_master " & _
                           "                    (" & StrFields & ") " & _
                           "                SELECT " & strValue & " " & _
                           "                FROM hremployee_master " & _
                           "                " & StrJoins & " " & _
                           "                INNER JOIN #TempUpdate ON #TempUpdate.employeeunkid = hremployee_master.employeeunkid " & _
                           "                AND #TempUpdate.companyunkid = hremployee_master.companyunkid " & _
                           "                END " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM IDM_DATA..hremployee_master JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_master.employeeunkid AND #TempUpdate.companyunkid = IDM_DATA..hremployee_master.companyunkid) " & _
                           "            BEGIN " & _
                           "                IF (" & sUpdateCol & ") " & _
                           "                BEGIN " & _
                           "                    UPDATE  IDM_DATA..hremployee_master SET " & _
                           "                            " & StrUpdate & " " & _
                           "                    FROM ( SELECT " & _
                           "                            " & strValue & " " & _
                           "                           FROM hremployee_master " & _
                           "                            " & StrJoins & " " & _
                           "                           INNER JOIN #TempUpdate ON #TempUpdate.employeeunkid = hremployee_master.employeeunkid " & _
                           "                              AND #TempUpdate.companyunkid = hremployee_master.companyunkid " & _
                           "                         ) AS B WHERE B.employeeunkid = IDM_DATA..hremployee_master.employeeunkid AND B.companyunkid = IDM_DATA..hremployee_master.companyunkid " & _
                           "                END " & _
                           "            END " & _
                           "            DROP TABLE #TempUpdate " & _
                           "          END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_EMP_TRANSFER' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_EMP_TRANSFER " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_EMP_TRANSFER]')) " & _
                           "EXEC dbo.sp_executesql @statement = N'" & _
                           "    CREATE TRIGGER [dbo].[trg_EMP_TRANSFER] ON [dbo].[hremployee_transfer_tran] " & _
                           "    AFTER INSERT, UPDATE AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'' ) " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempInsert FROM INSERTED " & _
                           "                IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_transfer_tran " & _
                           "                               JOIN #TempInsert ON #TempInsert.employeeunkid = IDM_DATA..hremployee_transfer_tran.employeeunkid " & _
                           "                               AND CONVERT(CHAR(8), #TempInsert.effectivedate, 112) = CONVERT(CHAR(8), IDM_DATA..hremployee_transfer_tran.effectivedate, 112) " & _
                           "                               WHERE IDM_DATA..hremployee_transfer_tran.companyunkid = @CompId AND IDM_DATA..hremployee_transfer_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    INSERT  INTO IDM_DATA..hremployee_transfer_tran " & _
                           "                        (transferunkid , " & _
                           "                         effectivedate , " & _
                           "                         employeeunkid , " & _
                           "                         rehiretranunkid , " & _
                           "                         stationunkid , " & _
                           "                         deptgroupunkid , " & _
                           "                         departmentunkid , " & _
                           "                         sectiongroupunkid , " & _
                           "                         sectionunkid , " & _
                           "                         unitgroupunkid , " & _
                           "                         unitunkid , " & _
                           "                         teamunkid , " & _
                           "                         classgroupunkid , " & _
                           "                         classunkid , " & _
                           "                         changereasonunkid , " & _
                           "                         isfromemployee , " & _
                           "                         statusunkid , " & _
                           "                         isvoid , " & _
                           "                         voiduserunkid , " & _
                           "                         voiddatetime , " & _
                           "                         voidreason , " & _
                           "                         companyunkid) " & _
                           "                     SELECT " & _
                           "                         transferunkid , " & _
                           "                         effectivedate , " & _
                           "                         employeeunkid , " & _
                           "                         rehiretranunkid , " & _
                           "                         stationunkid , " & _
                           "                         deptgroupunkid , " & _
                           "                         departmentunkid , " & _
                           "                         sectiongroupunkid , " & _
                           "                         sectionunkid , " & _
                           "                         unitgroupunkid , " & _
                           "                         unitunkid , " & _
                           "                         teamunkid , " & _
                           "                         classgroupunkid , " & _
                           "                         classunkid , " & _
                           "                         changereasonunkid , " & _
                           "                         isfromemployee , " & _
                           "                         statusunkid , " & _
                           "                         isvoid , " & _
                           "                         voiduserunkid , " & _
                           "                         voiddatetime , " & _
                           "                         voidreason , " & _
                           "                         @CompId " & _
                           "                      FROM #TempInsert " & _
                           "                END " & _
                           "                DROP TABLE #TempInsert " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempUpdate FROM INSERTED " & _
                           "                IF EXISTS (SELECT * FROM IDM_DATA..hremployee_transfer_tran " & _
                           "                           JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_transfer_tran.employeeunkid " & _
                           "                            AND #TempUpdate.transferunkid = IDM_DATA..hremployee_transfer_tran.transferunkid " & _
                           "                           WHERE IDM_DATA..hremployee_transfer_tran.companyunkid = @CompId AND IDM_DATA..hremployee_transfer_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    UPDATE IDM_DATA..hremployee_transfer_tran SET " & _
                           "                        transferunkid = U.transferunkid , " & _
                           "                        effectivedate = U.effectivedate , " & _
                           "                        employeeunkid = U.employeeunkid , " & _
                           "                        rehiretranunkid = U.rehiretranunkid , " & _
                           "                        stationunkid = U.stationunkid , " & _
                           "                        deptgroupunkid = U.deptgroupunkid , " & _
                           "                        departmentunkid = U.departmentunkid , " & _
                           "                        sectiongroupunkid = U.sectiongroupunkid , " & _
                           "                        sectionunkid = U.sectionunkid , " & _
                           "                        unitgroupunkid = U.unitgroupunkid , " & _
                           "                        unitunkid = U.unitunkid , " & _
                           "                        teamunkid = U.teamunkid , " & _
                           "                        classgroupunkid = U.classgroupunkid , " & _
                           "                        classunkid = U.classunkid , " & _
                           "                        changereasonunkid = U.changereasonunkid , " & _
                           "                        isfromemployee = U.isfromemployee , " & _
                           "                        statusunkid = U.statusunkid , " & _
                           "                        isvoid = U.isvoid , " & _
                           "                        voiduserunkid = U.voiduserunkid , " & _
                           "                        voiddatetime = U.voiddatetime , " & _
                           "                        voidreason = U.voidreason , " & _
                           "                        companyunkid = @CompId " & _
                           "                    FROM #TempUpdate AS U " & _
                           "                    WHERE U.employeeunkid = IDM_DATA..hremployee_transfer_tran.employeeunkid " & _
                           "                        AND U.transferunkid = IDM_DATA..hremployee_transfer_tran.transferunkid " & _
                           "                        AND IDM_DATA..hremployee_transfer_tran.companyunkid = @CompId " & _
                           "                END " & _
                           "                DROP TABLE #TempUpdate " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    'S.SANDEEP |24-APR-2019| -- START
                    '************************** ADDED, AND IDM_DATA..hremployee_transfer_tran.isvoid = 0
                    'S.SANDEEP |24-APR-2019| -- END
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_EMP_DATES' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_EMP_DATES " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_EMP_DATES]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_EMP_DATES] ON [dbo].[hremployee_dates_tran] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempInsert FROM INSERTED " & _
                           "                IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_dates_tran " & _
                           "                               JOIN #TempInsert ON #TempInsert.employeeunkid = IDM_DATA..hremployee_dates_tran.employeeunkid " & _
                           "                                    AND CONVERT(CHAR(8), #TempInsert.effectivedate, 112) = CONVERT(CHAR(8), IDM_DATA..hremployee_dates_tran.effectivedate, 112) " & _
                           "                                    AND IDM_DATA..hremployee_dates_tran.datetypeunkid = #TempInsert.datetypeunkid " & _
                           "                               WHERE IDM_DATA..hremployee_dates_tran.companyunkid = @CompId AND IDM_DATA..hremployee_dates_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    INSERT INTO IDM_DATA..hremployee_dates_tran " & _
                           "                    ( " & _
                           "                        datestranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        rehiretranunkid , " & _
                           "                        date1 , " & _
                           "                        date2 , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        datetypeunkid , " & _
                           "                        statusunkid , " & _
                           "                        actionreasonunkid , " & _
                           "                        isexclude_payroll , " & _
                           "                        isconfirmed , " & _
                           "                        otherreason , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        companyunkid " & _
                           "                    ) " & _
                           "                    SELECT " & _
                           "                        datestranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        rehiretranunkid , " & _
                           "                        date1 , " & _
                           "                        date2 , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        datetypeunkid , " & _
                           "                        statusunkid , " & _
                           "                        actionreasonunkid , " & _
                           "                        isexclude_payroll , " & _
                           "                        isconfirmed , " & _
                           "                        otherreason , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        @CompId " & _
                           "                    FROM #TempInsert " & _
                           "                END " & _
                           "                DROP TABLE #TempInsert " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempUpdate FROM INSERTED " & _
                           "                IF EXISTS (SELECT * FROM IDM_DATA..hremployee_dates_tran " & _
                           "                           JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_dates_tran.employeeunkid " & _
                           "                            AND #TempUpdate.datestranunkid = IDM_DATA..hremployee_dates_tran.datestranunkid " & _
                           "                            AND IDM_DATA..hremployee_dates_tran.datetypeunkid = #TempUpdate.datetypeunkid " & _
                           "                           WHERE IDM_DATA..hremployee_dates_tran.companyunkid = @CompId AND IDM_DATA..hremployee_dates_tran.isvoid = 0 ) " & _
                           "                BEGIN " & _
                           "                    UPDATE  IDM_DATA..hremployee_dates_tran " & _
                           "                    SET datestranunkid = U.datestranunkid , " & _
                           "                    effectivedate = U.effectivedate , " & _
                           "                    employeeunkid = U.employeeunkid , " & _
                           "                    rehiretranunkid = U.rehiretranunkid , " & _
                           "                    date1 = U.date1 , " & _
                           "                    date2 = U.date2 , " & _
                           "                    changereasonunkid = U.changereasonunkid , " & _
                           "                    isfromemployee = U.isfromemployee , " & _
                           "                    datetypeunkid = U.datetypeunkid , " & _
                           "                    statusunkid = U.statusunkid , " & _
                           "                    actionreasonunkid = U.actionreasonunkid , " & _
                           "                    isexclude_payroll = U.isexclude_payroll , " & _
                           "                    isconfirmed = U.isconfirmed , " & _
                           "                    otherreason = U.otherreason , " & _
                           "                    isvoid = U.isvoid , " & _
                           "                    voiduserunkid = U.voiduserunkid , " & _
                           "                    voiddatetime = U.voiddatetime , " & _
                           "                    voidreason = U.voidreason , " & _
                           "                    companyunkid = @CompId " & _
                           "                    FROM #TempUpdate AS U " & _
                           "                    WHERE U.employeeunkid = IDM_DATA..hremployee_dates_tran.employeeunkid " & _
                           "                    AND U.datestranunkid = IDM_DATA..hremployee_dates_tran.datestranunkid " & _
                           "                    AND U.datetypeunkid = IDM_DATA..hremployee_dates_tran.datetypeunkid " & _
                           "                    AND IDM_DATA..hremployee_dates_tran.companyunkid = @CompId " & _
                           "                END " & _
                           "                DROP TABLE #TempUpdate " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    'S.SANDEEP |24-APR-2019| -- START
                    '************************** ADDED, AND IDM_DATA..hremployee_dates_tran.isvoid = 0
                    'S.SANDEEP |24-APR-2019| -- END

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_EMP_CCENTER' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_EMP_CCENTER " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_EMP_CCENTER]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_EMP_CCENTER] ON [dbo].[hremployee_cctranhead_tran] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempInsert FROM INSERTED " & _
                           "                IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_cctranhead_tran " & _
                           "                               JOIN #TempInsert ON #TempInsert.employeeunkid = IDM_DATA..hremployee_cctranhead_tran.employeeunkid " & _
                           "                                    AND CONVERT(CHAR(8), #TempInsert.effectivedate, 112) = CONVERT(CHAR(8), IDM_DATA..hremployee_cctranhead_tran.effectivedate, 112) " & _
                           "                                    AND IDM_DATA..hremployee_cctranhead_tran.istransactionhead = #TempInsert.istransactionhead " & _
                           "                               WHERE IDM_DATA..hremployee_cctranhead_tran.companyunkid = @CompId AND IDM_DATA..hremployee_cctranhead_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    INSERT  INTO IDM_DATA..hremployee_cctranhead_tran " & _
                           "                    ( " & _
                           "                        cctranheadunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        rehiretranunkid , " & _
                           "                        cctranheadvalueid , " & _
                           "                        istransactionhead , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        companyunkid " & _
                           "                    ) " & _
                           "                    SELECT " & _
                           "                        cctranheadunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        rehiretranunkid , " & _
                           "                        cctranheadvalueid , " & _
                           "                        istransactionhead , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        @CompId " & _
                           "                    FROM #TempInsert " & _
                           "                END " & _
                           "                DROP TABLE #TempInsert " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempUpdate FROM INSERTED " & _
                           "                IF EXISTS (SELECT * FROM IDM_DATA..hremployee_cctranhead_tran " & _
                           "                           JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_cctranhead_tran.employeeunkid " & _
                           "                           AND #TempUpdate.cctranheadunkid = IDM_DATA..hremployee_cctranhead_tran.cctranheadunkid " & _
                           "                           WHERE IDM_DATA..hremployee_cctranhead_tran.companyunkid = @CompId AND IDM_DATA..hremployee_cctranhead_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    UPDATE  IDM_DATA..hremployee_cctranhead_tran " & _
                           "                        SET cctranheadunkid = U.cctranheadunkid , " & _
                           "                        effectivedate = U.effectivedate , " & _
                           "                        employeeunkid = U.employeeunkid , " & _
                           "                        rehiretranunkid = U.rehiretranunkid , " & _
                           "                        cctranheadvalueid = U.cctranheadvalueid , " & _
                           "                        istransactionhead = U.istransactionhead , " & _
                           "                        changereasonunkid = U.changereasonunkid , " & _
                           "                        isfromemployee = U.isfromemployee , " & _
                           "                        statusunkid = U.statusunkid , " & _
                           "                        isvoid = U.isvoid , " & _
                           "                        voiduserunkid = U.voiduserunkid , " & _
                           "                        voiddatetime = U.voiddatetime , " & _
                           "                        voidreason = U.voidreason , " & _
                           "                        companyunkid = @CompId " & _
                           "                    FROM  #TempUpdate AS U " & _
                           "                    WHERE U.employeeunkid = IDM_DATA..hremployee_cctranhead_tran.employeeunkid " & _
                           "                    AND U.cctranheadunkid = IDM_DATA..hremployee_cctranhead_tran.cctranheadunkid " & _
                           "                    AND IDM_DATA..hremployee_cctranhead_tran.companyunkid = @CompId " & _
                           "                END " & _
                           "                DROP TABLE #TempUpdate " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    'S.SANDEEP |24-APR-2019| -- START
                    '************************** ADDED, AND IDM_DATA..hremployee_cctranhead_tran.isvoid = 0
                    'S.SANDEEP |24-APR-2019| -- END

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_EMP_CATRG' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_EMP_CATRG " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_EMP_CATRG]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_EMP_CATRG] ON [dbo].[hremployee_categorization_tran] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempInsert FROM INSERTED " & _
                           "                IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_categorization_tran " & _
                           "                               JOIN #TempInsert ON #TempInsert.employeeunkid = IDM_DATA..hremployee_categorization_tran.employeeunkid " & _
                           "                               AND CONVERT(CHAR(8), #TempInsert.effectivedate, 112) = CONVERT(CHAR(8), IDM_DATA..hremployee_categorization_tran.effectivedate, 112) " & _
                           "                               WHERE IDM_DATA..hremployee_categorization_tran.companyunkid = @CompId AND IDM_DATA..hremployee_categorization_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    INSERT  INTO IDM_DATA..hremployee_categorization_tran " & _
                           "                    ( " & _
                           "                        categorizationtranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        jobgroupunkid , " & _
                           "                        jobunkid , " & _
                           "                        gradeunkid , " & _
                           "                        gradelevelunkid , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        rehiretranunkid , " & _
                           "                        companyunkid " & _
                           "                    ) " & _
                           "                    SELECT " & _
                           "                        categorizationtranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        jobgroupunkid , " & _
                           "                        jobunkid , " & _
                           "                        gradeunkid , " & _
                           "                        gradelevelunkid , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        rehiretranunkid , " & _
                           "                        @CompId " & _
                           "                    FROM #TempInsert " & _
                           "                END " & _
                           "                DROP TABLE #TempInsert " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempUpdate FROM INSERTED " & _
                           "                IF EXISTS (SELECT * FROM IDM_DATA..hremployee_categorization_tran " & _
                           "                           JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_categorization_tran.employeeunkid " & _
                           "                           AND #TempUpdate.categorizationtranunkid = IDM_DATA..hremployee_categorization_tran.categorizationtranunkid " & _
                           "                           WHERE IDM_DATA..hremployee_categorization_tran.companyunkid = @CompId AND IDM_DATA..hremployee_categorization_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    UPDATE  IDM_DATA..hremployee_categorization_tran " & _
                           "                    SET categorizationtranunkid = U.categorizationtranunkid , " & _
                           "                    effectivedate = U.effectivedate , " & _
                           "                    employeeunkid = U.employeeunkid , " & _
                           "                    jobgroupunkid = U.jobgroupunkid , " & _
                           "                    jobunkid = U.jobunkid , " & _
                           "                    gradeunkid = U.gradeunkid , " & _
                           "                    gradelevelunkid = U.gradelevelunkid , " & _
                           "                    changereasonunkid = U.changereasonunkid , " & _
                           "                    isfromemployee = U.isfromemployee , " & _
                           "                    statusunkid = U.statusunkid , " & _
                           "                    isvoid = U.isvoid , " & _
                           "                    voiduserunkid = U.voiduserunkid , " & _
                           "                    voiddatetime = U.voiddatetime , " & _
                           "                    voidreason = U.voidreason , " & _
                           "                    rehiretranunkid = U.rehiretranunkid , " & _
                           "                    companyunkid = @CompId " & _
                           "                    FROM #TempUpdate AS U " & _
                           "                    WHERE U.employeeunkid = IDM_DATA..hremployee_categorization_tran.employeeunkid " & _
                           "                    AND U.categorizationtranunkid = IDM_DATA..hremployee_categorization_tran.categorizationtranunkid " & _
                           "                    AND IDM_DATA..hremployee_categorization_tran.companyunkid = @CompId " & _
                           "                END " & _
                           "                DROP TABLE #TempUpdate " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    'S.SANDEEP |24-APR-2019| -- START
                    '************************** ADDED, AND IDM_DATA..hremployee_categorization_tran.isvoid = 0
                    'S.SANDEEP |24-APR-2019| -- END

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_EMP_WPT' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_EMP_WPT " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_EMP_WPT]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_EMP_WPT] ON [dbo].[hremployee_work_permit_tran] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT  companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempInsert FROM INSERTED " & _
                           "                IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_work_permit_tran " & _
                           "                               JOIN #TempInsert ON #TempInsert.employeeunkid = IDM_DATA..hremployee_work_permit_tran.employeeunkid " & _
                           "                               AND CONVERT(CHAR(8), #TempInsert.effectivedate, 112) = CONVERT(CHAR(8), IDM_DATA..hremployee_work_permit_tran.effectivedate, 112) " & _
                           "                               WHERE IDM_DATA..hremployee_work_permit_tran.companyunkid = @CompId AND IDM_DATA..hremployee_work_permit_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    INSERT  INTO IDM_DATA..hremployee_work_permit_tran " & _
                           "                    (" & _
                           "                        workpermittranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        work_permit_no , " & _
                           "                        workcountryunkid , " & _
                           "                        issue_place , " & _
                           "                        issue_date , " & _
                           "                        expiry_date , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        rehiretranunkid , " & _
                           "                        companyunkid, " & _
                           "                        isresidentpermit " & _
                           "                    ) " & _
                           "                    SELECT " & _
                           "                        workpermittranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        work_permit_no , " & _
                           "                        workcountryunkid , " & _
                           "                        issue_place , " & _
                           "                        issue_date , " & _
                           "                        expiry_date , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        rehiretranunkid , " & _
                           "                        @CompId, " & _
                           "                        isresidentpermit " & _
                           "                    FROM #TempInsert " & _
                           "                END " & _
                           "                DROP TABLE #TempInsert " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM   DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempUpdate FROM INSERTED " & _
                           "                IF EXISTS (SELECT * FROM    IDM_DATA..hremployee_work_permit_tran " & _
                           "                            JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_work_permit_tran.employeeunkid " & _
                           "                            AND #TempUpdate.workpermittranunkid = IDM_DATA..hremployee_work_permit_tran.workpermittranunkid " & _
                           "                           WHERE IDM_DATA..hremployee_work_permit_tran.companyunkid = @CompId AND IDM_DATA..hremployee_work_permit_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    UPDATE  IDM_DATA..hremployee_work_permit_tran " & _
                           "                    SET workpermittranunkid = U.workpermittranunkid , " & _
                           "                    effectivedate = U.effectivedate , " & _
                           "                    employeeunkid = U.employeeunkid , " & _
                           "                    work_permit_no = U.work_permit_no , " & _
                           "                    workcountryunkid = U.workcountryunkid , " & _
                           "                    issue_place = U.issue_place , " & _
                           "                    issue_date = U.issue_date , " & _
                           "                    expiry_date = U.expiry_date , " & _
                           "                    changereasonunkid = U.changereasonunkid , " & _
                           "                    isfromemployee = U.isfromemployee , " & _
                           "                    statusunkid = U.statusunkid , " & _
                           "                    isvoid = U.isvoid , " & _
                           "                    voiduserunkid = U.voiduserunkid , " & _
                           "                    voiddatetime = U.voiddatetime , " & _
                           "                    voidreason = U.voidreason , " & _
                           "                    rehiretranunkid = U.rehiretranunkid , " & _
                           "                    isresidentpermit = U.isresidentpermit , " & _
                           "                    companyunkid = @CompId " & _
                           "                    FROM #TempUpdate AS U " & _
                           "                    WHERE U.employeeunkid = IDM_DATA..hremployee_work_permit_tran.employeeunkid " & _
                           "                    AND U.workpermittranunkid = IDM_DATA..hremployee_work_permit_tran.workpermittranunkid " & _
                           "                    AND IDM_DATA..hremployee_work_permit_tran.companyunkid = @CompId " & _
                           "                END " & _
                           "                DROP TABLE #TempUpdate " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    'S.SANDEEP |24-APR-2019| -- START
                    '************************** ADDED, AND IDM_DATA..hremployee_work_permit_tran.isvoid = 0
                    '************************** ADDED, isresidentpermit
                    'S.SANDEEP |24-APR-2019| -- END

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_EMP_REH' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_EMP_REH " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_EMP_REH]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_EMP_REH] ON [dbo].[hremployee_rehire_tran] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempInsert FROM  INSERTED " & _
                           "                IF NOT EXISTS (SELECT * FROM IDM_DATA..hremployee_rehire_tran " & _
                           "                               JOIN #TempInsert ON #TempInsert.employeeunkid = IDM_DATA..hremployee_rehire_tran.employeeunkid " & _
                           "                               AND CONVERT(CHAR(8), #TempInsert.effectivedate, 112) = CONVERT(CHAR(8), IDM_DATA..hremployee_rehire_tran.effectivedate, 112) " & _
                           "                               WHERE IDM_DATA..hremployee_rehire_tran.companyunkid = @CompId AND IDM_DATA..hremployee_rehire_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    INSERT INTO IDM_DATA..hremployee_rehire_tran " & _
                           "                    ( " & _
                           "                        rehiretranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        reinstatment_date , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        companyunkid " & _
                           "                    ) " & _
                           "                    SELECT " & _
                           "                        rehiretranunkid , " & _
                           "                        effectivedate , " & _
                           "                        employeeunkid , " & _
                           "                        reinstatment_date , " & _
                           "                        changereasonunkid , " & _
                           "                        isfromemployee , " & _
                           "                        statusunkid , " & _
                           "                        isvoid , " & _
                           "                        voiduserunkid , " & _
                           "                        voiddatetime , " & _
                           "                        voidreason , " & _
                           "                        @CompId " & _
                           "                    FROM #TempInsert " & _
                           "                END " & _
                           "                DROP TABLE #TempInsert " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM  DELETED) " & _
                           "            BEGIN " & _
                           "                SELECT * INTO #TempUpdate FROM INSERTED " & _
                           "                IF EXISTS (SELECT * FROM IDM_DATA..hremployee_rehire_tran " & _
                           "                           JOIN #TempUpdate ON #TempUpdate.employeeunkid = IDM_DATA..hremployee_rehire_tran.employeeunkid " & _
                           "                           AND #TempUpdate.rehiretranunkid = IDM_DATA..hremployee_rehire_tran.rehiretranunkid " & _
                           "                           WHERE IDM_DATA..hremployee_rehire_tran.companyunkid = @CompId AND IDM_DATA..hremployee_rehire_tran.isvoid = 0) " & _
                           "                BEGIN " & _
                           "                    UPDATE  IDM_DATA..hremployee_rehire_tran " & _
                           "                    SET rehiretranunkid = U.rehiretranunkid , " & _
                           "                    effectivedate = U.effectivedate , " & _
                           "                    employeeunkid = U.employeeunkid , " & _
                           "                    reinstatment_date = U.reinstatment_date , " & _
                           "                    changereasonunkid = U.changereasonunkid , " & _
                           "                    isfromemployee = U.isfromemployee , " & _
                           "                    statusunkid = U.statusunkid , " & _
                           "                    isvoid = U.isvoid , " & _
                           "                    voiduserunkid = U.voiduserunkid , " & _
                           "                    voiddatetime = U.voiddatetime , " & _
                           "                    voidreason = U.voidreason , " & _
                           "                    companyunkid = @CompId " & _
                           "                    FROM #TempUpdate AS U " & _
                           "                    WHERE U.employeeunkid = IDM_DATA..hremployee_rehire_tran.employeeunkid " & _
                           "                    AND U.rehiretranunkid = IDM_DATA..hremployee_rehire_tran.rehiretranunkid " & _
                           "                    AND IDM_DATA..hremployee_rehire_tran.companyunkid = @CompId " & _
                           "                END " & _
                           "                DROP TABLE #TempUpdate " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    'S.SANDEEP |24-APR-2019| -- START
                    '************************** ADDED, AND IDM_DATA..hremployee_rehire_tran.isvoid = 0
                    'S.SANDEEP |24-APR-2019| -- END

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrstation_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrstation_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrstation_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrstation_master] ON [dbo].[hrstation_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrstation_master " & _
                           "                ( " & _
                           "                    stationunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    address1 , " & _
                           "                    address2 , " & _
                           "                    cityunkid , " & _
                           "                    postalunkid , " & _
                           "                    stateunkid , " & _
                           "                    countryunkid , " & _
                           "                    phone1 , " & _
                           "                    phone2 , " & _
                           "                    phone3 , " & _
                           "                    fax , " & _
                           "                    email , " & _
                           "                    website , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    stationunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    address1 , " & _
                           "                    address2 , " & _
                           "                    cityunkid , " & _
                           "                    postalunkid , " & _
                           "                    stateunkid , " & _
                           "                    countryunkid , " & _
                           "                    phone1 , " & _
                           "                    phone2 , " & _
                           "                    phone3 , " & _
                           "                    fax , " & _
                           "                    email , " & _
                           "                    website , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrstation_master " & _
                           "                SET stationunkid = U.stationunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                address1 = U.address1 , " & _
                           "                address2 = U.address2 , " & _
                           "                cityunkid = U.cityunkid , " & _
                           "                postalunkid = U.postalunkid , " & _
                           "                stateunkid = U.stateunkid , " & _
                           "                countryunkid = U.countryunkid , " & _
                           "                phone1 = U.phone1 , " & _
                           "                phone2 = U.phone2 , " & _
                           "                phone3 = U.phone3 , " & _
                           "                fax = U.fax , " & _
                           "                email = U.email , " & _
                           "                website = U.website , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.stationunkid = IDM_DATA..hrstation_master.stationunkid " & _
                           "                AND IDM_DATA..hrstation_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrdepartment_group_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrdepartment_group_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrdepartment_group_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrdepartment_group_master] ON [dbo].[hrdepartment_group_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT  INTO IDM_DATA..hrdepartment_group_master " & _
                           "                ( " & _
                           "                    deptgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    deptgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrdepartment_group_master " & _
                           "                SET deptgroupunkid = U.deptgroupunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.deptgroupunkid = IDM_DATA..hrdepartment_group_master.deptgroupunkid " & _
                           "                AND IDM_DATA..hrdepartment_group_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrdepartment_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_hrdepartment_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrdepartment_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrdepartment_master] ON [dbo].[hrdepartment_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT  companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrdepartment_master " & _
                           "                ( " & _
                           "                    departmentunkid , " & _
                           "                    deptgroupunkid , " & _
                           "                    stationunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    departmentunkid , " & _
                           "                    deptgroupunkid , " & _
                           "                    stationunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM  DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrdepartment_master " & _
                           "                SET departmentunkid = U.departmentunkid , " & _
                           "                deptgroupunkid = U.deptgroupunkid , " & _
                           "                stationunkid = U.stationunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.departmentunkid = IDM_DATA..hrdepartment_master.departmentunkid " & _
                           "                AND IDM_DATA..hrdepartment_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrsectiongroup_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_hrsectiongroup_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrsectiongroup_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrsectiongroup_master] ON [dbo].[hrsectiongroup_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT  companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT  INTO IDM_DATA..hrsectiongroup_master " & _
                           "                ( " & _
                           "                    sectiongroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    sectiongroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM  DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrsectiongroup_master " & _
                           "                    SET sectiongroupunkid = U.sectiongroupunkid , " & _
                           "                    code = U.code , " & _
                           "                    name = U.name , " & _
                           "                    description = U.description , " & _
                           "                    isactive = U.isactive , " & _
                           "                    name1 = U.name1 , " & _
                           "                    name2 = U.name2 , " & _
                           "                    companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.sectiongroupunkid = IDM_DATA..hrsectiongroup_master.sectiongroupunkid " & _
                           "                AND IDM_DATA..hrsectiongroup_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrsection_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_hrsection_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrsection_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrsection_master] ON [dbo].[hrsection_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrsection_master " & _
                           "                ( " & _
                           "                    sectionunkid , " & _
                           "                    departmentunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    sectiongroupunkid , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    sectionunkid , " & _
                           "                    departmentunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    sectiongroupunkid , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrsection_master " & _
                           "                SET sectionunkid = U.sectionunkid , " & _
                           "                departmentunkid = U.departmentunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                sectiongroupunkid = U.sectiongroupunkid , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.sectionunkid = IDM_DATA..hrsection_master.sectionunkid " & _
                           "                AND IDM_DATA..hrsection_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrunitgroup_master' AND type = 'TR') " & _
                         "BEGIN " & _
                         "    DROP TRIGGER trg_hrunitgroup_master " & _
                         "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrunitgroup_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrunitgroup_master] ON [dbo].[hrunitgroup_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrunitgroup_master " & _
                           "                ( " & _
                           "                    unitgroupunkid, " & _
                           "                    sectionunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    unitgroupunkid , " & _
                           "                    sectionunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrunitgroup_master " & _
                           "                    SET unitgroupunkid = U.unitgroupunkid , " & _
                           "                    sectionunkid = U.sectionunkid , " & _
                           "                    code = U.code , " & _
                           "                    name = U.name , " & _
                           "                    description = U.description , " & _
                           "                    isactive = U.isactive , " & _
                           "                    name1 = U.name1 , " & _
                           "                    name2 = U.name2 , " & _
                           "                    companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.unitgroupunkid = IDM_DATA..hrunitgroup_master.unitgroupunkid " & _
                           "                AND IDM_DATA..hrunitgroup_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END'"

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrunit_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrunit_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrunit_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrunit_master] ON [dbo].[hrunit_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrunit_master " & _
                           "                ( " & _
                           "                    unitunkid , " & _
                           "                    sectionunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    unitgroupunkid , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    unitunkid , " & _
                           "                    sectionunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    unitgroupunkid , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM  DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrunit_master " & _
                           "                SET unitunkid = U.unitunkid , " & _
                           "                sectionunkid = U.sectionunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                unitgroupunkid = U.unitgroupunkid , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.unitunkid = IDM_DATA..hrunit_master.unitunkid " & _
                           "                AND IDM_DATA..hrunit_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrteam_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrteam_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrteam_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrteam_master] ON [dbo].[hrteam_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrteam_master " & _
                           "                ( " & _
                           "                    teamunkid , " & _
                           "                    unitunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    teamunkid , " & _
                           "                    unitunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrteam_master " & _
                           "                SET teamunkid = U.teamunkid , " & _
                           "                unitunkid = U.unitunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.teamunkid = IDM_DATA..hrteam_master.teamunkid " & _
                           "                AND IDM_DATA..hrteam_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrclassgroup_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrclassgroup_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrclassgroup_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrclassgroup_master] ON [dbo].[hrclassgroup_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrclassgroup_master " & _
                           "                ( " & _
                           "                    classgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    classgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrclassgroup_master " & _
                           "                SET classgroupunkid = U.classgroupunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.classgroupunkid = IDM_DATA..hrclassgroup_master.classgroupunkid " & _
                           "                AND IDM_DATA..hrclassgroup_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrclasses_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrclasses_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrclasses_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrclasses_master] ON [dbo].[hrclasses_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrclasses_master " & _
                           "                ( " & _
                           "                    classesunkid , " & _
                           "                    classgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    classesunkid , " & _
                           "                    classgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrclasses_master " & _
                           "                SET classesunkid = U.classesunkid , " & _
                           "                classgroupunkid = U.classgroupunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.classesunkid = IDM_DATA..hrclasses_master.classesunkid " & _
                           "                AND IDM_DATA..hrclasses_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrjobgroup_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrjobgroup_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrjobgroup_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrjobgroup_master] ON [dbo].[hrjobgroup_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrjobgroup_master " & _
                           "                ( " & _
                           "                    jobgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    weight , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    jobgroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    weight , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrjobgroup_master " & _
                           "                SET jobgroupunkid = U.jobgroupunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                weight = U.weight , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.jobgroupunkid = IDM_DATA..hrjobgroup_master.jobgroupunkid " & _
                           "                AND IDM_DATA..hrjobgroup_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrjob_master' AND type = 'TR') " & _
                           "BEGIN " & _
                           "    DROP TRIGGER trg_hrjob_master " & _
                           "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrjob_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrjob_master] ON [dbo].[hrjob_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT  INTO IDM_DATA..hrjob_master " & _
                           "                ( " & _
                           "                    jobunkid , " & _
                           "                    job_code , " & _
                           "                    job_name , " & _
                           "                    desciription , " & _
                           "                    isactive , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    jobunkid , " & _
                           "                    job_code , " & _
                           "                    job_name , " & _
                           "                    desciription , " & _
                           "                    isactive , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrjob_master " & _
                           "                SET jobunkid = U.jobunkid , " & _
                           "                job_code = U.job_code , " & _
                           "                job_name = U.job_name , " & _
                           "                desciription = U.desciription , " & _
                           "                isactive = U.isactive , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.jobunkid = IDM_DATA..hrjob_master.jobunkid " & _
                           "                AND IDM_DATA..hrjob_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_prcostcenter_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_prcostcenter_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_prcostcenter_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_prcostcenter_master] ON [dbo].[prcostcenter_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM  hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..prcostcenter_master " & _
                           "                ( " & _
                           "                    costcenterunkid , " & _
                           "                    costcentercode , " & _
                           "                    costcentername , " & _
                           "                    costcentergroupmasterunkid , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    costcentername1 , " & _
                           "                    costcentername2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    costcenterunkid , " & _
                           "                    costcentercode , " & _
                           "                    costcentername , " & _
                           "                    costcentergroupmasterunkid , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    costcentername1 , " & _
                           "                    costcentername2 , " & _
                           "                    @CompId " & _
                           "                    FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM  DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..prcostcenter_master " & _
                           "                SET costcenterunkid = U.costcenterunkid , " & _
                           "                costcentercode = U.costcentercode , " & _
                           "                costcentername = U.costcentername , " & _
                           "                costcentergroupmasterunkid = U.costcentergroupmasterunkid , " & _
                           "                description = U.description , " & _
                           "                isactive = U.isactive , " & _
                           "                costcentername1 = U.costcentername1 , " & _
                           "                costcentername2 = U.costcentername2 , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.costcenterunkid = IDM_DATA..prcostcenter_master.costcenterunkid " & _
                           "                AND IDM_DATA..prcostcenter_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_cfcommon_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_cfcommon_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_cfcommon_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_cfcommon_master] ON [dbo].[cfcommon_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM  hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..cfcommon_master " & _
                           "                ( " & _
                           "                    masterunkid , " & _
                           "                    code , " & _
                           "                    alias , " & _
                           "                    name , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    mastertype , " & _
                           "                    isactive , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    masterunkid , " & _
                           "                    code , " & _
                           "                    alias , " & _
                           "                    name , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    mastertype , " & _
                           "                    isactive , " & _
                           "                    @CompId " & _
                           "                    FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM  DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..cfcommon_master " & _
                           "                SET masterunkid = U.masterunkid , " & _
                           "                code = U.code , " & _
                           "                alias = U.alias , " & _
                           "                name = U.name , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                mastertype = U.mastertype , " & _
                           "                isactive = U.isactive , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.masterunkid = IDM_DATA..cfcommon_master.masterunkid " & _
                           "                AND IDM_DATA..cfcommon_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END'"

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrgradegroup_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_hrgradegroup_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrgradegroup_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrgradegroup_master] ON [dbo].[hrgradegroup_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrgradegroup_master " & _
                           "                ( " & _
                           "                    gradegroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    gradegroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM  DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrgradegroup_master " & _
                           "                SET gradegroupunkid = U.gradegroupunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                isactive = U.isactive , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.gradegroupunkid = IDM_DATA..hrgradegroup_master.gradegroupunkid " & _
                           "                AND IDM_DATA..hrgradegroup_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END'"

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrgrade_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_hrgrade_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrgrade_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrgrade_master] ON [dbo].[hrgrade_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT  INTO IDM_DATA..hrgrade_master " & _
                           "                ( " & _
                           "                    gradeunkid , " & _
                           "                    gradegroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    gradeunkid , " & _
                           "                    gradegroupunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrgrade_master " & _
                           "                SET gradeunkid = U.gradeunkid , " & _
                           "                gradegroupunkid = U.gradegroupunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                isactive = U.isactive , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.gradeunkid = IDM_DATA..hrgrade_master.gradeunkid " & _
                           "                AND IDM_DATA..hrgrade_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_hrgradelevel_master' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_hrgradelevel_master " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_hrgradelevel_master]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_hrgradelevel_master] ON [dbo].[hrgradelevel_master] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT INTO IDM_DATA..hrgradelevel_master " & _
                           "                ( " & _
                           "                    gradelevelunkid , " & _
                           "                    gradeunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    gradelevelunkid , " & _
                           "                    gradeunkid , " & _
                           "                    code , " & _
                           "                    name , " & _
                           "                    description , " & _
                           "                    isactive , " & _
                           "                    name1 , " & _
                           "                    name2 , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..hrgradelevel_master " & _
                           "                SET gradelevelunkid = U.gradelevelunkid , " & _
                           "                gradeunkid = U.gradeunkid , " & _
                           "                code = U.code , " & _
                           "                name = U.name , " & _
                           "                description = U.description , " & _
                           "                name1 = U.name1 , " & _
                           "                name2 = U.name2 , " & _
                           "                isactive = U.isactive , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.gradelevelunkid = IDM_DATA..hrgradelevel_master.gradelevelunkid " & _
                           "                AND IDM_DATA..hrgradelevel_master.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'trg_prsalaryincrement_tran' AND type = 'TR') " & _
                          "BEGIN " & _
                          "    DROP TRIGGER trg_prsalaryincrement_tran " & _
                          "END "

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_prsalaryincrement_tran]')) " & _
                           "EXEC dbo.sp_executesql @statement = N' " & _
                           "    CREATE TRIGGER [dbo].[trg_prsalaryincrement_tran] ON [dbo].[prsalaryincrement_tran] " & _
                           "    AFTER INSERT, UPDATE " & _
                           "    AS " & _
                           "    BEGIN " & _
                           "        IF EXISTS (SELECT * FROM sys.databases WHERE name = ''IDM_DATA'') " & _
                           "        BEGIN " & _
                           "            DECLARE @CompId AS INT " & _
                           "            SET @CompId = (SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE isclosed = 0 AND UPPER(database_name) = (SELECT UPPER(DB_NAME()))) " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                INSERT  INTO IDM_DATA..prsalaryincrement_tran " & _
                           "                ( " & _
                           "                    salaryincrementtranunkid , " & _
                           "                    periodunkid , " & _
                           "                    employeeunkid , " & _
                           "                    incrementdate , " & _
                           "                    gradegroupunkid , " & _
                           "                    gradeunkid , " & _
                           "                    gradelevelunkid , " & _
                           "                    isgradechange , " & _
                           "                    isvoid , " & _
                           "                    isapproved , " & _
                           "                    companyunkid " & _
                           "                ) " & _
                           "                SELECT " & _
                           "                    salaryincrementtranunkid , " & _
                           "                    periodunkid , " & _
                           "                    employeeunkid , " & _
                           "                    incrementdate , " & _
                           "                    gradegroupunkid , " & _
                           "                    gradeunkid , " & _
                           "                    gradelevelunkid , " & _
                           "                    isgradechange , " & _
                           "                    isvoid , " & _
                           "                    isapproved , " & _
                           "                    @CompId " & _
                           "                FROM INSERTED " & _
                           "            END " & _
                           "            IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) " & _
                           "            BEGIN " & _
                           "                UPDATE IDM_DATA..prsalaryincrement_tran " & _
                           "                SET salaryincrementtranunkid = U.salaryincrementtranunkid , " & _
                           "                periodunkid = U.periodunkid , " & _
                           "                employeeunkid = U.employeeunkid , " & _
                           "                incrementdate = U.incrementdate , " & _
                           "                gradegroupunkid = U.gradegroupunkid , " & _
                           "                gradeunkid = U.gradeunkid , " & _
                           "                gradelevelunkid = U.gradelevelunkid , " & _
                           "                isgradechange = U.isgradechange , " & _
                           "                isvoid = U.isvoid , " & _
                           "                isapproved = U.isapproved , " & _
                           "                companyunkid = @CompId " & _
                           "                FROM INSERTED AS U " & _
                           "                WHERE U.salaryincrementtranunkid = IDM_DATA..prsalaryincrement_tran.salaryincrementtranunkid " & _
                           "                AND U.periodunkid = IDM_DATA..prsalaryincrement_tran.periodunkid " & _
                           "                AND U.employeeunkid = IDM_DATA..prsalaryincrement_tran.employeeunkid " & _
                           "                AND IDM_DATA..prsalaryincrement_tran.companyunkid = @CompId " & _
                           "            END " & _
                           "        END " & _
                           "    END' "


                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateUserViewSynonymsFor_IDMS", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmGenerateYear_Wizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCloseYear = New clsCloseYear
        Try
            Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            OtherSettings()
            Cursor.Current = Cursors.WaitCursor
            chkCopyPreviousEDSlab.Checked = True
            chkCopyPreviousEDSlab.Enabled = False
            mstrNextTextCaption = wz_NewYearWizard.NextText
            Call SetDate()
            Call PayrollTransactions()
            Call Loan_Savings()
            Call FillLeaveList()
            Call ApplicantList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateYear_Wizard_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGenerateYear_Wizard_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCloseYear = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateYear_Wizard_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGenerateYear_Wizard_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateYear_Wizard_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCloseYear.SetMessages()
            objfrm._Other_ModuleNames = "clsCloseYear"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Wizard Event(s) "

    Private Sub wz_NewYearWizard_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles wz_NewYearWizard.AfterSwitchPages
        Try
            wz_NewYearWizard.NextText = mstrNextTextCaption
            If (e.NewIndex = wz_NewYearWizard.Pages.IndexOf(wpProgress)) Then
                wz_NewYearWizard.NextText = Language.getMessage(mstrModuleName, 13, "Start", -1)
            Else
                wz_NewYearWizard.NextText = mstrNextTextCaption
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wz_NewYearWizard_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub wz_NewYearWizard_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wz_NewYearWizard.BeforeSwitchPages
        Try
            Dim oldIndex As Integer = e.OldIndex
            If (oldIndex = wz_NewYearWizard.Pages.IndexOf(wpWelcome)) Then

                If (e.NewIndex > e.OldIndex) Then
                    Dim message As String = String.Empty
                    message = objCloseYear.IsMultiConnected(FinancialYear._Object._DatabaseName)
                    If ((message.Trim.Length > 0) AndAlso (eZeeMsgBox.Show(message, (enMsgBoxStyle.Information Or enMsgBoxStyle.YesNo)) = DialogResult.No)) Then
                        e.Cancel = True
                    End If
                End If

            ElseIf (oldIndex = wz_NewYearWizard.Pages.IndexOf(wpYearInfo)) Then

                If (e.NewIndex > e.OldIndex) Then
                    If (txtCode.Text.Trim = "") Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please enter New Period Code.", -1), enMsgBoxStyle.Information)
                        txtCode.Focus()
                        e.Cancel = True
                    ElseIf (txtName.Text.Trim = "") Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter New Period Name.", -1), enMsgBoxStyle.Information)
                        txtName.Focus()
                        e.Cancel = True
                    Else
                        Dim tran As New clscommom_period_Tran With { _
                            ._Modulerefid = 1 _
                        }
                        If tran.isExist(txtCode.Text.Trim, "", DateTime.MinValue, DateTime.MinValue, -1) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This Period Code is already defined. Please define new Period Code.", -1), enMsgBoxStyle.Information)
                            txtCode.Focus()
                            e.Cancel = True
                        ElseIf tran.isExist("", txtName.Text.Trim, DateTime.MinValue, DateTime.MinValue, -1) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This Period Name is already defined. Please define new Period Name.", -1), enMsgBoxStyle.Information)
                            txtName.Focus()
                            e.Cancel = True
                        Else
                            tran = Nothing
                            If (chkCopyPreviousEDSlab.Checked AndAlso (eZeeMsgBox.Show((Language.getMessage(mstrModuleName, 8, "Copy Previous ED Slab is ticked. This will copy Previous Period Slab.", -1) & ChrW(13) & ChrW(10) & ChrW(13) & ChrW(10) & Language.getMessage(mstrModuleName, 12, "Do you want to continue?", -1)), (enMsgBoxStyle.Question Or enMsgBoxStyle.YesNo)) = DialogResult.No)) Then
                                e.Cancel = True
                            End If
                        End If
                    End If
                End If
            ElseIf ((oldIndex = wz_NewYearWizard.Pages.IndexOf(wpProgress)) AndAlso (e.NewIndex > e.OldIndex)) Then

                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to Close this Year?", -1), (enMsgBoxStyle.Question Or enMsgBoxStyle.YesNo)) = DialogResult.No) Then
                    e.Cancel = True
                Else
                    Dim options As New clsConfigOptions With { _
                        ._Companyunkid = Company._Object._Companyunkid _
                    }
                    If (options._DatabaseExportPath = "") Then
                        eZeeMsgBox.Show(String.Concat(New String() {Language.getMessage(mstrModuleName, 6, "Database backup ", -1), ChrW(13) & ChrW(10), Language.getMessage(mstrModuleName, 7, "path has not been set. ", -1), ChrW(13) & ChrW(10), Language.getMessage(mstrModuleName, 11, "Backup of that database(s) cannot be taken.", -1), ChrW(13) & ChrW(10), Language.getMessage(mstrModuleName, 9, "You can set the Database backup path from ", -1), ChrW(13) & ChrW(10), Language.getMessage(mstrModuleName, 10, "Aruti Configuration -> Option -> Path.", -1)}))
                        e.Cancel = True
                    Else
                        Cursor.Current = Cursors.WaitCursor
                        rtbCloseYearLog.ResetText()
                        rtbCloseYearLog.Visible = True
                        e.Cancel = True
                        objbgwCloseYear.RunWorkerAsync()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wz_NewYearWizard_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Background Worker Events "

    Private Sub objbgwCloseYear_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwCloseYear.DoWork
        Dim blnFlag As Boolean = False
        Try
            mblnProcessFailed = False
            ControlBox = False
            wz_NewYearWizard.Enabled = False
            RemoveHandler modGlobal.gfrmMDI.tmrReminder.Tick, New EventHandler(AddressOf modGlobal.gfrmMDI.tmrReminder_Tick)
            clsApplicationIdleTimer.LastIdealTime.Stop()
            Dim settings2 As New clsApplicationSettings
            Dim strFinalPath As String = String.Empty
            Dim StrFilePath As String = (settings2._ApplicationPath & "Data\Close Year Backup")
            If Not Directory.Exists(StrFilePath) Then
                Directory.CreateDirectory(StrFilePath)
            End If
            mstrCloseYearLogFileName = (settings2._ApplicationPath & "CloseYearLog_" & Strings.Format(DateAndTime.Now.Date, "yyyyMMdd") & ".log")
            settings2 = Nothing
            File.AppendAllText(mstrCloseYearLogFileName, "--------------------------------------------------------------------" & vbCrLf)
            File.AppendAllText(mstrCloseYearLogFileName, "Close Year Process Started at " & Format(Now, "dd-MMM-yyyy HH:mm:ss") & " ..." & vbCrLf)
            File.AppendAllText(mstrCloseYearLogFileName, "--------------------------------------------------------------------" & vbCrLf)
            File.AppendAllText(mstrCloseYearLogFileName, "Taking Database(s) backup..." & vbCrLf)
            rtbCloseYearLog.ResetText()
            rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
            rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
            rtbCloseYearLog.ScrollToCaret()

            For i As Integer = 0 To 2
                If i = 0 Then
                    eZeeDatabase.change_database(FinancialYear._Object._ConfigDatabaseName)
                    If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName) Then
                        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName)
                    End If
                    strFinalPath = StrFilePath & "\" & FinancialYear._Object._ConfigDatabaseName
                    IO.File.AppendAllText(mstrCloseYearLogFileName, "Taking Daatabase backup [" & FinancialYear._Object._ConfigDatabaseName & "]..." & vbCrLf)
                    rtbCloseYearLog.ResetText()
                    rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
                    rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
                    rtbCloseYearLog.ScrollToCaret()

                ElseIf i = 2 Then 'this database has to be always to be taken at last and i number has to be higher in order
                    eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                    If Not System.IO.Directory.Exists(StrFilePath & "\" & FinancialYear._Object._DatabaseName) Then
                        System.IO.Directory.CreateDirectory(StrFilePath & "\" & FinancialYear._Object._DatabaseName)
                    End If
                    strFinalPath = StrFilePath & "\" & FinancialYear._Object._DatabaseName
                    IO.File.AppendAllText(mstrCloseYearLogFileName, "Taking Daatabase backup [" & FinancialYear._Object._DatabaseName & "]..." & vbCrLf)
                    rtbCloseYearLog.ResetText()
                    rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
                    rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
                    rtbCloseYearLog.ScrollToCaret()

                ElseIf i = 1 Then
                    Dim objBkUp As New clsBackup
                    If objBkUp.Is_DatabasePresent("arutiimages") Then
                        eZeeDatabase.change_database("arutiimages")
                        If Not System.IO.Directory.Exists(StrFilePath & "\" & "arutiimages") Then
                            System.IO.Directory.CreateDirectory(StrFilePath & "\" & "arutiimages")
                        End If
                        strFinalPath = StrFilePath & "\" & "arutiimages"
                        IO.File.AppendAllText(mstrCloseYearLogFileName, "Taking Daatabase backup [arutiimages]..." & vbCrLf)
                        rtbCloseYearLog.ResetText()
                        rtbCloseYearLog.LoadFile(mstrCloseYearLogFileName, RichTextBoxStreamType.PlainText)
                        rtbCloseYearLog.SelectionStart = rtbCloseYearLog.TextLength
                        rtbCloseYearLog.ScrollToCaret()
                    Else
                        strFinalPath = ""
                    End If
                    objBkUp = Nothing
                End If
                Me.Refresh()

                If strFinalPath <> "" Then

                    strFinalPath = objBackupDatabase.Backup(strFinalPath, General_Settings._Object._ServerName)

                    Dim objBackup As New clsBackup
                    If System.IO.File.Exists(strFinalPath) Then

                        objBackup._Backup_Date = DateAndTime.Now
                        objBackup._Backup_Path = strFinalPath
                        If i = 0 Then
                            objBackup._Companyunkid = -1
                            objBackup._Yearunkid = -1
                            objBackup._Isconfiguration = True
                        ElseIf i = 1 Then
                            objBackup._Companyunkid = Company._Object._Companyunkid
                            objBackup._Yearunkid = FinancialYear._Object._YearUnkid
                            objBackup._Isconfiguration = False
                        End If
                        objBackup._Userunkid = User._Object._Userunkid

                        Call objBackup.Insert()
                    End If

                End If
            Next

            objCloseYear._Datasource_Payroll = mdtPayroll
            objCloseYear._CurrYearUnkID = mintCurrYearUnkID
            objCloseYear._CurrYearStartDate = mdtCurrYearStartDate
            objCloseYear._CurrYearEndDate = mdtCurrYearEndDate
            objCloseYear._NewYearStartDate = mdtNewYearStartDate
            objCloseYear._NewYearEndDate = mdtNewYearEndDate
            objCloseYear._CopyLastPeriodEDSlab = chkCopyPreviousEDSlab.Checked
            objCloseYear._Leave_IDs = mstrLeave.Replace(" ", "")
            objCloseYear._LastPeriodUnkID = mintLastPeriodUnkID
            objCloseYear._LastPeriodStartDate = mdtLastPeriodStartDate
            objCloseYear._LastPeriodEndDate = mdtLastPeriodEndDate
            objCloseYear._LastPeriodTnAEndDate = mdtLastPeriodTnAEndDate
            objCloseYear._LeaveTypeIDs = mstrLeaveTypeIDs.Replace(" ", "")
            objCloseYear._Payroll_IDs = mstrPayroll.Replace(" ", "")
            objCloseYear._ProcessPendingLoan_IDs = mstrProcessPendingLoan.Replace(" ", "")
            objCloseYear._Loan_IDs = mstrLoan.Replace(" ", "")
            objCloseYear._Savings_IDs = mstrSavings.Replace(" ", "")
            objCloseYear._Applicants_IDs = mstrApplicants.Replace(" ", "")
            objCloseYear._Period_Code = txtCode.Text.Trim
            objCloseYear._Period_Name = txtName.Text.Trim
            objCloseYear._Period_EndDate = dtpPeriodEnddate.Value
            'Hemant (10 Nov 2023) -- Start
            objCloseYear._AssetDeclarationTemplate = ConfigParameter._Object._AssetDeclarationTemplate
            'Hemant (10 Nov 2023) -- End
            Dim settings As New clsApplicationSettings


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCloseYear._FormName = mstrModuleName
            objCloseYear._LoginEmployeeunkid = 0
            objCloseYear._ClientIP = getIP()
            objCloseYear._HostName = getHostName()
            objCloseYear._FromWeb = False
            objCloseYear._AuditUserId = User._Object._Userunkid
objCloseYear._CompanyUnkid = Company._Object._Companyunkid
            objCloseYear._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objCloseYear.Close_Year(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._UserAccessModeSetting, False, True, Company._Object._Code, settings._ApplicationPath, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._CurrentDateAndTime, False, "", mstrCloseYearLogFileName, objbgwCloseYear, rtbCloseYearLog)

            If blnFlag = False And objCloseYear._Message <> "" Then
                eZeeMsgBox.Show(objCloseYear._Message, enMsgBoxStyle.Information)
            End If
            If blnFlag = False Then
                eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                mblnProcessFailed = True
                Exit Try
            End If
            mblnCancel = False

            Dim intYearID As Integer = FinancialYear._Object._YearUnkid
            FinancialYear.Refresh()
            FinancialYear._Object._YearUnkid = intYearID

            blnFlag = CreateUserViewSynonymsForSharePointTRA()
            blnFlag = CreateUserViewSynonymsFor_IDMS()

        Catch ex As Exception
            mblnProcessFailed = True
            objbgwCloseYear.CancelAsync()
            DisplayError.Show("-1", ex.Message, "objbgwCloseYear_DoWork", mstrModuleName)
            eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()

            Me.ControlBox = True
            'Enabled = True
            wz_NewYearWizard.Enabled = True
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub objbgwCloseYear_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwCloseYear.RunWorkerCompleted
        Try
            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
                wz_NewYearWizard.SelectedPage = wpProgress
            Else
                'S.SANDEEP [28-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : PREVENTING NEXT PAGE NAVIGATION IF ERROR
                'wz_NewYearWizard.SelectedPage = wpFinish
                'wz_NewYearWizard.BackEnabled = False
                'wz_NewYearWizard.NextText = mstrNextTextCaption
                'wz_NewYearWizard.CancelText = "Finish"
                'rtbCloseYearLog.ResetText()
                'gfrmMDI.mnuPayrollView.Enabled = False
                'gfrmMDI.mnuReportView.PerformClick()
                'eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                If mblnProcessFailed = False Then
                wz_NewYearWizard.SelectedPage = wpFinish
                wz_NewYearWizard.BackEnabled = False
                wz_NewYearWizard.NextText = mstrNextTextCaption
                wz_NewYearWizard.CancelText = "Finish"
                rtbCloseYearLog.ResetText()
                gfrmMDI.mnuPayrollView.Enabled = False
                gfrmMDI.mnuReportView.PerformClick()
                eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            End If
                'S.SANDEEP [28-SEP-2017] -- END
            End If
            Me.ControlBox = True
            wz_NewYearWizard.Enabled = True
            RemoveHandler gfrmMDI.tmrReminder.Tick, New EventHandler(AddressOf gfrmMDI.tmrReminder_Tick)
            AddHandler gfrmMDI.tmrReminder.Tick, New EventHandler(AddressOf gfrmMDI.tmrReminder_Tick)
            clsApplicationIdleTimer.LastIdealTime.Start()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwCloseYear_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbYearInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbYearInformation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbTransferProgress.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTransferProgress.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.wz_NewYearWizard.CancelText = Language._Object.getCaption(Me.wz_NewYearWizard.Name & "_CancelText", Me.wz_NewYearWizard.CancelText)
            Me.wz_NewYearWizard.NextText = Language._Object.getCaption(Me.wz_NewYearWizard.Name & "_NextText", Me.wz_NewYearWizard.NextText)
            Me.wz_NewYearWizard.BackText = Language._Object.getCaption(Me.wz_NewYearWizard.Name & "_BackText", Me.wz_NewYearWizard.BackText)
            Me.wz_NewYearWizard.FinishText = Language._Object.getCaption(Me.wz_NewYearWizard.Name & "_FinishText", Me.wz_NewYearWizard.FinishText)
            Me.lblStep1Message.Text = Language._Object.getCaption(Me.lblStep1Message.Name, Me.lblStep1Message.Text)
            Me.gbYearInformation.Text = Language._Object.getCaption(Me.gbYearInformation.Name, Me.gbYearInformation.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.lblEnddate.Text = Language._Object.getCaption(Me.lblEnddate.Name, Me.lblEnddate.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.EZeeLine3.Text = Language._Object.getCaption(Me.EZeeLine3.Name, Me.EZeeLine3.Text)
            Me.lblNewEnddate.Text = Language._Object.getCaption(Me.lblNewEnddate.Name, Me.lblNewEnddate.Text)
            Me.lblNewStartdate.Text = Language._Object.getCaption(Me.lblNewStartdate.Name, Me.lblNewStartdate.Text)
            Me.EZeeLine2.Text = Language._Object.getCaption(Me.EZeeLine2.Name, Me.EZeeLine2.Text)
            Me.lblCurrEnddate.Text = Language._Object.getCaption(Me.lblCurrEnddate.Name, Me.lblCurrEnddate.Text)
            Me.lblCurrStartdate.Text = Language._Object.getCaption(Me.lblCurrStartdate.Name, Me.lblCurrStartdate.Text)
            Me.eZeeLine1.Text = Language._Object.getCaption(Me.eZeeLine1.Name, Me.eZeeLine1.Text)
            Me.gbTransferProgress.Text = Language._Object.getCaption(Me.gbTransferProgress.Name, Me.gbTransferProgress.Text)
            Me.lblStep7_Finish.Text = Language._Object.getCaption(Me.lblStep7_Finish.Name, Me.lblStep7_Finish.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

    Private Sub SetMessages()
        Try
            Language.setMessage(Me.mstrModuleName, 1, "Are you sure you want to Close this Year?", -1)
            Language.setMessage(Me.mstrModuleName, 2, "Please enter New Period Code.", -1)
            Language.setMessage(Me.mstrModuleName, 3, "Please enter New Period Name.", -1)
            Language.setMessage(Me.mstrModuleName, 4, "This Period Code is already defined. Please define new Period Code.", -1)
            Language.setMessage(Me.mstrModuleName, 5, "This Period Name is already defined. Please define new Period Name.", -1)
            Language.setMessage(Me.mstrModuleName, 6, "Database backup ", -1)
            Language.setMessage(Me.mstrModuleName, 7, "path has not been set. ", -1)
            Language.setMessage(Me.mstrModuleName, 8, "Copy Previous ED Slab is ticked. This will copy Previous Period Slab.", -1)
            Language.setMessage(Me.mstrModuleName, 9, "You can set the Database backup path from ", -1)
            Language.setMessage(Me.mstrModuleName, 10, "Aruti Configuration -> Option -> Path.", -1)
            Language.setMessage(Me.mstrModuleName, 11, "Backup of that database(s")
            Language.setMessage(Me.mstrModuleName, 12, "Do you want to continue?", -1)
            Language.setMessage(Me.mstrModuleName, 13, "Start", -1)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class