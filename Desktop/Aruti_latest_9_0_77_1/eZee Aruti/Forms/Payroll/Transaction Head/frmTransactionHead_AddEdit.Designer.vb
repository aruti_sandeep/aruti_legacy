﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransactionHead_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransactionHead_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbComputationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.pnlAddEditDelete = New System.Windows.Forms.Panel
        Me.lnkCopySlab = New System.Windows.Forms.LinkLabel
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.objbtnAddComputation = New eZee.Common.eZeeGradientButton
        Me.cboComputeOn = New System.Windows.Forms.ComboBox
        Me.lblComputeOn = New System.Windows.Forms.Label
        Me.lblSpecifiedFormula = New System.Windows.Forms.Label
        Me.pnlSimpleSlab = New System.Windows.Forms.Panel
        Me.objbtnAddSlabFormula = New eZee.Common.eZeeGradientButton
        Me.lblSimpleSlabPeriod = New System.Windows.Forms.Label
        Me.cboSimpleSlabPeriod = New System.Windows.Forms.ComboBox
        Me.lvTrnHeadSlabSimple = New eZee.Common.eZeeListView(Me.components)
        Me.colhTranHeadunkid = New System.Windows.Forms.ColumnHeader
        Me.colhSimpleAmtUpto = New System.Windows.Forms.ColumnHeader
        Me.colhsimpleSlabType = New System.Windows.Forms.ColumnHeader
        Me.colhSimpleValueBasis = New System.Windows.Forms.ColumnHeader
        Me.colhSimpleGUID = New System.Windows.Forms.ColumnHeader
        Me.colhSimplePeriod = New System.Windows.Forms.ColumnHeader
        Me.colhSimpleEndDate = New System.Windows.Forms.ColumnHeader
        Me.objSlabType = New System.Windows.Forms.ComboBox
        Me.cboSimpleSlabAmountType = New System.Windows.Forms.ComboBox
        Me.lblSimleSlabAmountType = New System.Windows.Forms.Label
        Me.txtSimpleSlabValueBasis = New eZee.TextBox.NumericTextBox
        Me.lblSimleSlabValueBasis = New System.Windows.Forms.Label
        Me.txtSimpleSlabAmtUpto = New eZee.TextBox.NumericTextBox
        Me.lblSimleSlabAmtUpto = New System.Windows.Forms.Label
        Me.txtSlabFormula = New System.Windows.Forms.TextBox
        Me.pnlTaxSlab = New System.Windows.Forms.Panel
        Me.lblTaxSlabPeriod = New System.Windows.Forms.Label
        Me.cboTaxSlabPeriod = New System.Windows.Forms.ComboBox
        Me.lvTrnHeadSlabInExcessof = New eZee.Common.eZeeListView(Me.components)
        Me.colhtrantaxslabunkid = New System.Windows.Forms.ColumnHeader
        Me.colhTaxAmountUpto = New System.Windows.Forms.ColumnHeader
        Me.colhTaxFixedAmount = New System.Windows.Forms.ColumnHeader
        Me.colhTaxRatePayable = New System.Windows.Forms.ColumnHeader
        Me.colhTaxInExcessOf = New System.Windows.Forms.ColumnHeader
        Me.colhTaxGUID = New System.Windows.Forms.ColumnHeader
        Me.colhTaxPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhTaxEndDate = New System.Windows.Forms.ColumnHeader
        Me.txtTaxSlabRatePayable = New eZee.TextBox.NumericTextBox
        Me.lblTaxSlabValueBasis = New System.Windows.Forms.Label
        Me.txtTaxSlabFixedAmount = New eZee.TextBox.NumericTextBox
        Me.lblTaxSlabFixedAmount = New System.Windows.Forms.Label
        Me.txtTaxSlabAmtUpto = New eZee.TextBox.NumericTextBox
        Me.lblTaxSlabAmountUpto = New System.Windows.Forms.Label
        Me.gbTrnHeadInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblPRemark = New System.Windows.Forms.Label
        Me.nudPriority = New System.Windows.Forms.NumericUpDown
        Me.lblPriority = New System.Windows.Forms.Label
        Me.cboRoundOffModeId = New System.Windows.Forms.ComboBox
        Me.cboRoundOffTypeId = New System.Windows.Forms.ComboBox
        Me.lblRoundOffTypeId = New System.Windows.Forms.Label
        Me.cboDefaultCostCenter = New System.Windows.Forms.ComboBox
        Me.lblDefaultCostCenter = New System.Windows.Forms.Label
        Me.chkProratedFlatRateHead = New System.Windows.Forms.CheckBox
        Me.chkIsPerformance = New System.Windows.Forms.CheckBox
        Me.chkBasicsalaryasotherearning = New System.Windows.Forms.CheckBox
        Me.chkMonetary = New System.Windows.Forms.CheckBox
        Me.chkNonCashBenefit = New System.Windows.Forms.CheckBox
        Me.chkCumulative = New System.Windows.Forms.CheckBox
        Me.chkTaxRelief = New System.Windows.Forms.CheckBox
        Me.chkTaxable = New System.Windows.Forms.CheckBox
        Me.lblTypeOf = New System.Windows.Forms.Label
        Me.cboTypeOf = New System.Windows.Forms.ComboBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.chkRecurrent = New System.Windows.Forms.CheckBox
        Me.chkAppearOnPayslip = New System.Windows.Forms.CheckBox
        Me.lnOtherInfo = New eZee.Common.eZeeLine
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.cboCalcType = New System.Windows.Forms.ComboBox
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.lblType = New System.Windows.Forms.Label
        Me.txtHeadName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtHeadCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.nudPeriodsAfterAssignment = New System.Windows.Forms.NumericUpDown
        Me.lblActiveOnly = New System.Windows.Forms.Label
        Me.lblPeriodsAfterAssignment = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbComputationInfo.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlAddEditDelete.SuspendLayout()
        Me.pnlSimpleSlab.SuspendLayout()
        Me.pnlTaxSlab.SuspendLayout()
        Me.gbTrnHeadInfo.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.nudPeriodsAfterAssignment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbComputationInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbTrnHeadInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(844, 607)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(844, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Add / Edit Transaction Heads"
        '
        'gbComputationInfo
        '
        Me.gbComputationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbComputationInfo.Checked = False
        Me.gbComputationInfo.CollapseAllExceptThis = False
        Me.gbComputationInfo.CollapsedHoverImage = Nothing
        Me.gbComputationInfo.CollapsedNormalImage = Nothing
        Me.gbComputationInfo.CollapsedPressedImage = Nothing
        Me.gbComputationInfo.CollapseOnLoad = False
        Me.gbComputationInfo.Controls.Add(Me.Panel2)
        Me.gbComputationInfo.ExpandedHoverImage = Nothing
        Me.gbComputationInfo.ExpandedNormalImage = Nothing
        Me.gbComputationInfo.ExpandedPressedImage = Nothing
        Me.gbComputationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbComputationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbComputationInfo.HeaderHeight = 25
        Me.gbComputationInfo.HeaderMessage = ""
        Me.gbComputationInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbComputationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbComputationInfo.HeightOnCollapse = 0
        Me.gbComputationInfo.LeftTextSpace = 0
        Me.gbComputationInfo.Location = New System.Drawing.Point(382, 66)
        Me.gbComputationInfo.Name = "gbComputationInfo"
        Me.gbComputationInfo.OpenHeight = 182
        Me.gbComputationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbComputationInfo.ShowBorder = True
        Me.gbComputationInfo.ShowCheckBox = False
        Me.gbComputationInfo.ShowCollapseButton = False
        Me.gbComputationInfo.ShowDefaultBorderColor = True
        Me.gbComputationInfo.ShowDownButton = False
        Me.gbComputationInfo.ShowHeader = True
        Me.gbComputationInfo.Size = New System.Drawing.Size(450, 480)
        Me.gbComputationInfo.TabIndex = 1
        Me.gbComputationInfo.Temp = 0
        Me.gbComputationInfo.Text = "Computation Information"
        Me.gbComputationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.pnlAddEditDelete)
        Me.Panel2.Controls.Add(Me.txtFormula)
        Me.Panel2.Controls.Add(Me.objbtnAddComputation)
        Me.Panel2.Controls.Add(Me.cboComputeOn)
        Me.Panel2.Controls.Add(Me.lblComputeOn)
        Me.Panel2.Controls.Add(Me.lblSpecifiedFormula)
        Me.Panel2.Controls.Add(Me.pnlSimpleSlab)
        Me.Panel2.Controls.Add(Me.pnlTaxSlab)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(2, 26)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(446, 454)
        Me.Panel2.TabIndex = 0
        '
        'pnlAddEditDelete
        '
        Me.pnlAddEditDelete.Controls.Add(Me.lnkCopySlab)
        Me.pnlAddEditDelete.Controls.Add(Me.btnEdit)
        Me.pnlAddEditDelete.Controls.Add(Me.btnDelete)
        Me.pnlAddEditDelete.Controls.Add(Me.btnAdd)
        Me.pnlAddEditDelete.Location = New System.Drawing.Point(9, 144)
        Me.pnlAddEditDelete.Name = "pnlAddEditDelete"
        Me.pnlAddEditDelete.Size = New System.Drawing.Size(429, 36)
        Me.pnlAddEditDelete.TabIndex = 302
        '
        'lnkCopySlab
        '
        Me.lnkCopySlab.Location = New System.Drawing.Point(11, 8)
        Me.lnkCopySlab.Name = "lnkCopySlab"
        Me.lnkCopySlab.Size = New System.Drawing.Size(115, 13)
        Me.lnkCopySlab.TabIndex = 108
        Me.lnkCopySlab.TabStop = True
        Me.lnkCopySlab.Text = "Copy Last Slab"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(228, 3)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 6
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(324, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 7
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(132, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'txtFormula
        '
        Me.txtFormula.Location = New System.Drawing.Point(125, 38)
        Me.txtFormula.Multiline = True
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.ReadOnly = True
        Me.txtFormula.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFormula.Size = New System.Drawing.Size(286, 50)
        Me.txtFormula.TabIndex = 1
        '
        'objbtnAddComputation
        '
        Me.objbtnAddComputation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddComputation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddComputation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddComputation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddComputation.BorderSelected = False
        Me.objbtnAddComputation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddComputation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddComputation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddComputation.Location = New System.Drawing.Point(417, 38)
        Me.objbtnAddComputation.Name = "objbtnAddComputation"
        Me.objbtnAddComputation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddComputation.TabIndex = 2
        '
        'cboComputeOn
        '
        Me.cboComputeOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComputeOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComputeOn.FormattingEnabled = True
        Me.cboComputeOn.Location = New System.Drawing.Point(125, 11)
        Me.cboComputeOn.Name = "cboComputeOn"
        Me.cboComputeOn.Size = New System.Drawing.Size(288, 21)
        Me.cboComputeOn.TabIndex = 0
        Me.cboComputeOn.Tag = ""
        '
        'lblComputeOn
        '
        Me.lblComputeOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComputeOn.Location = New System.Drawing.Point(14, 14)
        Me.lblComputeOn.Name = "lblComputeOn"
        Me.lblComputeOn.Size = New System.Drawing.Size(105, 14)
        Me.lblComputeOn.TabIndex = 10
        Me.lblComputeOn.Text = "Compute On"
        '
        'lblSpecifiedFormula
        '
        Me.lblSpecifiedFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecifiedFormula.Location = New System.Drawing.Point(14, 41)
        Me.lblSpecifiedFormula.Name = "lblSpecifiedFormula"
        Me.lblSpecifiedFormula.Size = New System.Drawing.Size(105, 14)
        Me.lblSpecifiedFormula.TabIndex = 11
        Me.lblSpecifiedFormula.Text = "Specified Formula"
        '
        'pnlSimpleSlab
        '
        Me.pnlSimpleSlab.Controls.Add(Me.objbtnAddSlabFormula)
        Me.pnlSimpleSlab.Controls.Add(Me.lblSimpleSlabPeriod)
        Me.pnlSimpleSlab.Controls.Add(Me.cboSimpleSlabPeriod)
        Me.pnlSimpleSlab.Controls.Add(Me.lvTrnHeadSlabSimple)
        Me.pnlSimpleSlab.Controls.Add(Me.objSlabType)
        Me.pnlSimpleSlab.Controls.Add(Me.cboSimpleSlabAmountType)
        Me.pnlSimpleSlab.Controls.Add(Me.lblSimleSlabAmountType)
        Me.pnlSimpleSlab.Controls.Add(Me.txtSimpleSlabValueBasis)
        Me.pnlSimpleSlab.Controls.Add(Me.lblSimleSlabValueBasis)
        Me.pnlSimpleSlab.Controls.Add(Me.txtSimpleSlabAmtUpto)
        Me.pnlSimpleSlab.Controls.Add(Me.lblSimleSlabAmtUpto)
        Me.pnlSimpleSlab.Controls.Add(Me.txtSlabFormula)
        Me.pnlSimpleSlab.Location = New System.Drawing.Point(16, 90)
        Me.pnlSimpleSlab.Name = "pnlSimpleSlab"
        Me.pnlSimpleSlab.Size = New System.Drawing.Size(421, 352)
        Me.pnlSimpleSlab.TabIndex = 301
        '
        'objbtnAddSlabFormula
        '
        Me.objbtnAddSlabFormula.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSlabFormula.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSlabFormula.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSlabFormula.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSlabFormula.BorderSelected = False
        Me.objbtnAddSlabFormula.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSlabFormula.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSlabFormula.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSlabFormula.Location = New System.Drawing.Point(400, 27)
        Me.objbtnAddSlabFormula.Name = "objbtnAddSlabFormula"
        Me.objbtnAddSlabFormula.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSlabFormula.TabIndex = 107
        '
        'lblSimpleSlabPeriod
        '
        Me.lblSimpleSlabPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSimpleSlabPeriod.Location = New System.Drawing.Point(3, 7)
        Me.lblSimpleSlabPeriod.Name = "lblSimpleSlabPeriod"
        Me.lblSimpleSlabPeriod.Size = New System.Drawing.Size(91, 14)
        Me.lblSimpleSlabPeriod.TabIndex = 106
        Me.lblSimpleSlabPeriod.Text = "Effective Period"
        Me.lblSimpleSlabPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboSimpleSlabPeriod
        '
        Me.cboSimpleSlabPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSimpleSlabPeriod.DropDownWidth = 215
        Me.cboSimpleSlabPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSimpleSlabPeriod.FormattingEnabled = True
        Me.cboSimpleSlabPeriod.Location = New System.Drawing.Point(3, 29)
        Me.cboSimpleSlabPeriod.Name = "cboSimpleSlabPeriod"
        Me.cboSimpleSlabPeriod.Size = New System.Drawing.Size(91, 21)
        Me.cboSimpleSlabPeriod.TabIndex = 105
        '
        'lvTrnHeadSlabSimple
        '
        Me.lvTrnHeadSlabSimple.BackColorOnChecked = True
        Me.lvTrnHeadSlabSimple.ColumnHeaders = Nothing
        Me.lvTrnHeadSlabSimple.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhTranHeadunkid, Me.colhSimpleAmtUpto, Me.colhsimpleSlabType, Me.colhSimpleValueBasis, Me.colhSimpleGUID, Me.colhSimplePeriod, Me.colhSimpleEndDate})
        Me.lvTrnHeadSlabSimple.CompulsoryColumns = ""
        Me.lvTrnHeadSlabSimple.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTrnHeadSlabSimple.FullRowSelect = True
        Me.lvTrnHeadSlabSimple.GridLines = True
        Me.lvTrnHeadSlabSimple.GroupingColumn = Nothing
        Me.lvTrnHeadSlabSimple.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvTrnHeadSlabSimple.HideSelection = False
        Me.lvTrnHeadSlabSimple.Location = New System.Drawing.Point(0, 96)
        Me.lvTrnHeadSlabSimple.MinColumnWidth = 50
        Me.lvTrnHeadSlabSimple.MultiSelect = False
        Me.lvTrnHeadSlabSimple.Name = "lvTrnHeadSlabSimple"
        Me.lvTrnHeadSlabSimple.OptionalColumns = ""
        Me.lvTrnHeadSlabSimple.ShowMoreItem = False
        Me.lvTrnHeadSlabSimple.ShowSaveItem = False
        Me.lvTrnHeadSlabSimple.ShowSelectAll = True
        Me.lvTrnHeadSlabSimple.ShowSizeAllColumnsToFit = True
        Me.lvTrnHeadSlabSimple.Size = New System.Drawing.Size(421, 253)
        Me.lvTrnHeadSlabSimple.Sortable = False
        Me.lvTrnHeadSlabSimple.TabIndex = 104
        Me.lvTrnHeadSlabSimple.UseCompatibleStateImageBehavior = False
        Me.lvTrnHeadSlabSimple.View = System.Windows.Forms.View.Details
        '
        'colhTranHeadunkid
        '
        Me.colhTranHeadunkid.Text = "tranheadunkid"
        Me.colhTranHeadunkid.Width = 0
        '
        'colhSimpleAmtUpto
        '
        Me.colhSimpleAmtUpto.Tag = "colhSimpleAmtUpto"
        Me.colhSimpleAmtUpto.Text = "Amount Upto"
        Me.colhSimpleAmtUpto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhSimpleAmtUpto.Width = 160
        '
        'colhsimpleSlabType
        '
        Me.colhsimpleSlabType.Tag = "colhsimpleSlabType"
        Me.colhsimpleSlabType.Text = "Slab Type"
        Me.colhsimpleSlabType.Width = 90
        '
        'colhSimpleValueBasis
        '
        Me.colhSimpleValueBasis.Tag = "colhSimpleValueBasis"
        Me.colhSimpleValueBasis.Text = "Value Basis"
        Me.colhSimpleValueBasis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhSimpleValueBasis.Width = 160
        '
        'colhSimpleGUID
        '
        Me.colhSimpleGUID.Text = "GUID"
        Me.colhSimpleGUID.Width = 0
        '
        'colhSimplePeriod
        '
        Me.colhSimplePeriod.Tag = "colhSimplePeriod"
        Me.colhSimplePeriod.Text = "Period"
        Me.colhSimplePeriod.Width = 0
        '
        'colhSimpleEndDate
        '
        Me.colhSimpleEndDate.Tag = "colhSimpleEndDate"
        Me.colhSimpleEndDate.Text = "End Date"
        Me.colhSimpleEndDate.Width = 0
        '
        'objSlabType
        '
        Me.objSlabType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objSlabType.DropDownWidth = 215
        Me.objSlabType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objSlabType.FormattingEnabled = True
        Me.objSlabType.Location = New System.Drawing.Point(263, 29)
        Me.objSlabType.Name = "objSlabType"
        Me.objSlabType.Size = New System.Drawing.Size(35, 21)
        Me.objSlabType.TabIndex = 103
        Me.objSlabType.Visible = False
        '
        'cboSimpleSlabAmountType
        '
        Me.cboSimpleSlabAmountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSimpleSlabAmountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSimpleSlabAmountType.FormattingEnabled = True
        Me.cboSimpleSlabAmountType.Items.AddRange(New Object() {"Select", "Percentage", "Value"})
        Me.cboSimpleSlabAmountType.Location = New System.Drawing.Point(220, 29)
        Me.cboSimpleSlabAmountType.Name = "cboSimpleSlabAmountType"
        Me.cboSimpleSlabAmountType.Size = New System.Drawing.Size(78, 21)
        Me.cboSimpleSlabAmountType.TabIndex = 1
        Me.cboSimpleSlabAmountType.Tag = "country"
        '
        'lblSimleSlabAmountType
        '
        Me.lblSimleSlabAmountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSimleSlabAmountType.Location = New System.Drawing.Point(220, 7)
        Me.lblSimleSlabAmountType.Name = "lblSimleSlabAmountType"
        Me.lblSimleSlabAmountType.Size = New System.Drawing.Size(78, 14)
        Me.lblSimleSlabAmountType.TabIndex = 102
        Me.lblSimleSlabAmountType.Text = "Slab Type"
        Me.lblSimleSlabAmountType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSimpleSlabValueBasis
        '
        Me.txtSimpleSlabValueBasis.AllowNegative = True
        Me.txtSimpleSlabValueBasis.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSimpleSlabValueBasis.DigitsInGroup = 0
        Me.txtSimpleSlabValueBasis.Flags = 0
        Me.txtSimpleSlabValueBasis.Location = New System.Drawing.Point(304, 27)
        Me.txtSimpleSlabValueBasis.MaxDecimalPlaces = 6
        Me.txtSimpleSlabValueBasis.MaxWholeDigits = 21
        Me.txtSimpleSlabValueBasis.Name = "txtSimpleSlabValueBasis"
        Me.txtSimpleSlabValueBasis.Prefix = ""
        Me.txtSimpleSlabValueBasis.RangeMax = 1.7976931348623157E+308
        Me.txtSimpleSlabValueBasis.RangeMin = -1.7976931348623157E+308
        Me.txtSimpleSlabValueBasis.Size = New System.Drawing.Size(92, 21)
        Me.txtSimpleSlabValueBasis.TabIndex = 2
        Me.txtSimpleSlabValueBasis.Text = "0"
        Me.txtSimpleSlabValueBasis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSimleSlabValueBasis
        '
        Me.lblSimleSlabValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSimleSlabValueBasis.Location = New System.Drawing.Point(313, 6)
        Me.lblSimleSlabValueBasis.Name = "lblSimleSlabValueBasis"
        Me.lblSimleSlabValueBasis.Size = New System.Drawing.Size(105, 13)
        Me.lblSimleSlabValueBasis.TabIndex = 100
        Me.lblSimleSlabValueBasis.Text = "Value Basis"
        Me.lblSimleSlabValueBasis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSimpleSlabAmtUpto
        '
        Me.txtSimpleSlabAmtUpto.AllowNegative = True
        Me.txtSimpleSlabAmtUpto.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSimpleSlabAmtUpto.DigitsInGroup = 0
        Me.txtSimpleSlabAmtUpto.Flags = 0
        Me.txtSimpleSlabAmtUpto.Location = New System.Drawing.Point(100, 29)
        Me.txtSimpleSlabAmtUpto.MaxDecimalPlaces = 6
        Me.txtSimpleSlabAmtUpto.MaxWholeDigits = 21
        Me.txtSimpleSlabAmtUpto.Name = "txtSimpleSlabAmtUpto"
        Me.txtSimpleSlabAmtUpto.Prefix = ""
        Me.txtSimpleSlabAmtUpto.RangeMax = 1.7976931348623157E+308
        Me.txtSimpleSlabAmtUpto.RangeMin = -1.7976931348623157E+308
        Me.txtSimpleSlabAmtUpto.Size = New System.Drawing.Size(114, 21)
        Me.txtSimpleSlabAmtUpto.TabIndex = 0
        Me.txtSimpleSlabAmtUpto.Text = "0"
        Me.txtSimpleSlabAmtUpto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSimleSlabAmtUpto
        '
        Me.lblSimleSlabAmtUpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSimleSlabAmtUpto.Location = New System.Drawing.Point(97, 6)
        Me.lblSimleSlabAmtUpto.Name = "lblSimleSlabAmtUpto"
        Me.lblSimleSlabAmtUpto.Size = New System.Drawing.Size(114, 14)
        Me.lblSimleSlabAmtUpto.TabIndex = 98
        Me.lblSimleSlabAmtUpto.Text = "Amount Upto"
        Me.lblSimleSlabAmtUpto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSlabFormula
        '
        Me.txtSlabFormula.Location = New System.Drawing.Point(304, 27)
        Me.txtSlabFormula.Name = "txtSlabFormula"
        Me.txtSlabFormula.ReadOnly = True
        Me.txtSlabFormula.Size = New System.Drawing.Size(92, 21)
        Me.txtSlabFormula.TabIndex = 108
        Me.txtSlabFormula.Visible = False
        '
        'pnlTaxSlab
        '
        Me.pnlTaxSlab.Controls.Add(Me.lblTaxSlabPeriod)
        Me.pnlTaxSlab.Controls.Add(Me.cboTaxSlabPeriod)
        Me.pnlTaxSlab.Controls.Add(Me.lvTrnHeadSlabInExcessof)
        Me.pnlTaxSlab.Controls.Add(Me.txtTaxSlabRatePayable)
        Me.pnlTaxSlab.Controls.Add(Me.lblTaxSlabValueBasis)
        Me.pnlTaxSlab.Controls.Add(Me.txtTaxSlabFixedAmount)
        Me.pnlTaxSlab.Controls.Add(Me.lblTaxSlabFixedAmount)
        Me.pnlTaxSlab.Controls.Add(Me.txtTaxSlabAmtUpto)
        Me.pnlTaxSlab.Controls.Add(Me.lblTaxSlabAmountUpto)
        Me.pnlTaxSlab.Location = New System.Drawing.Point(17, 90)
        Me.pnlTaxSlab.Name = "pnlTaxSlab"
        Me.pnlTaxSlab.Size = New System.Drawing.Size(421, 352)
        Me.pnlTaxSlab.TabIndex = 290
        '
        'lblTaxSlabPeriod
        '
        Me.lblTaxSlabPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxSlabPeriod.Location = New System.Drawing.Point(3, 5)
        Me.lblTaxSlabPeriod.Name = "lblTaxSlabPeriod"
        Me.lblTaxSlabPeriod.Size = New System.Drawing.Size(91, 14)
        Me.lblTaxSlabPeriod.TabIndex = 108
        Me.lblTaxSlabPeriod.Text = "Effective Period"
        Me.lblTaxSlabPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboTaxSlabPeriod
        '
        Me.cboTaxSlabPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTaxSlabPeriod.DropDownWidth = 215
        Me.cboTaxSlabPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxSlabPeriod.FormattingEnabled = True
        Me.cboTaxSlabPeriod.Location = New System.Drawing.Point(3, 29)
        Me.cboTaxSlabPeriod.Name = "cboTaxSlabPeriod"
        Me.cboTaxSlabPeriod.Size = New System.Drawing.Size(91, 21)
        Me.cboTaxSlabPeriod.TabIndex = 107
        '
        'lvTrnHeadSlabInExcessof
        '
        Me.lvTrnHeadSlabInExcessof.BackColorOnChecked = True
        Me.lvTrnHeadSlabInExcessof.ColumnHeaders = Nothing
        Me.lvTrnHeadSlabInExcessof.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhtrantaxslabunkid, Me.colhTaxAmountUpto, Me.colhTaxFixedAmount, Me.colhTaxRatePayable, Me.colhTaxInExcessOf, Me.colhTaxGUID, Me.colhTaxPeriod, Me.colhTaxEndDate})
        Me.lvTrnHeadSlabInExcessof.CompulsoryColumns = ""
        Me.lvTrnHeadSlabInExcessof.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTrnHeadSlabInExcessof.FullRowSelect = True
        Me.lvTrnHeadSlabInExcessof.GridLines = True
        Me.lvTrnHeadSlabInExcessof.GroupingColumn = Nothing
        Me.lvTrnHeadSlabInExcessof.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvTrnHeadSlabInExcessof.HideSelection = False
        Me.lvTrnHeadSlabInExcessof.Location = New System.Drawing.Point(0, 96)
        Me.lvTrnHeadSlabInExcessof.MinColumnWidth = 50
        Me.lvTrnHeadSlabInExcessof.MultiSelect = False
        Me.lvTrnHeadSlabInExcessof.Name = "lvTrnHeadSlabInExcessof"
        Me.lvTrnHeadSlabInExcessof.OptionalColumns = ""
        Me.lvTrnHeadSlabInExcessof.ShowMoreItem = False
        Me.lvTrnHeadSlabInExcessof.ShowSaveItem = False
        Me.lvTrnHeadSlabInExcessof.ShowSelectAll = True
        Me.lvTrnHeadSlabInExcessof.ShowSizeAllColumnsToFit = True
        Me.lvTrnHeadSlabInExcessof.Size = New System.Drawing.Size(421, 253)
        Me.lvTrnHeadSlabInExcessof.Sortable = False
        Me.lvTrnHeadSlabInExcessof.TabIndex = 103
        Me.lvTrnHeadSlabInExcessof.UseCompatibleStateImageBehavior = False
        Me.lvTrnHeadSlabInExcessof.View = System.Windows.Forms.View.Details
        '
        'colhtrantaxslabunkid
        '
        Me.colhtrantaxslabunkid.Text = "tranheadtaxslabunkid"
        Me.colhtrantaxslabunkid.Width = 0
        '
        'colhTaxAmountUpto
        '
        Me.colhTaxAmountUpto.DisplayIndex = 2
        Me.colhTaxAmountUpto.Tag = "colhTaxAmountUpto"
        Me.colhTaxAmountUpto.Text = "Amount Upto"
        Me.colhTaxAmountUpto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTaxAmountUpto.Width = 120
        '
        'colhTaxFixedAmount
        '
        Me.colhTaxFixedAmount.DisplayIndex = 3
        Me.colhTaxFixedAmount.Tag = "colhTaxFixedAmount"
        Me.colhTaxFixedAmount.Text = "Fixed Amount"
        Me.colhTaxFixedAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTaxFixedAmount.Width = 100
        '
        'colhTaxRatePayable
        '
        Me.colhTaxRatePayable.DisplayIndex = 4
        Me.colhTaxRatePayable.Tag = "colhTaxRatePayable"
        Me.colhTaxRatePayable.Text = "Rate Payable"
        Me.colhTaxRatePayable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTaxRatePayable.Width = 105
        '
        'colhTaxInExcessOf
        '
        Me.colhTaxInExcessOf.DisplayIndex = 1
        Me.colhTaxInExcessOf.Tag = "colhTaxInExcessOf"
        Me.colhTaxInExcessOf.Text = "In Excess Of"
        Me.colhTaxInExcessOf.Width = 90
        '
        'colhTaxGUID
        '
        Me.colhTaxGUID.Text = "GUID"
        Me.colhTaxGUID.Width = 0
        '
        'colhTaxPeriod
        '
        Me.colhTaxPeriod.Tag = "colhTaxPeriod"
        Me.colhTaxPeriod.Text = "Period"
        Me.colhTaxPeriod.Width = 0
        '
        'colhTaxEndDate
        '
        Me.colhTaxEndDate.Tag = "colhTaxEndDate"
        Me.colhTaxEndDate.Text = "End Date"
        Me.colhTaxEndDate.Width = 0
        '
        'txtTaxSlabRatePayable
        '
        Me.txtTaxSlabRatePayable.AllowNegative = True
        Me.txtTaxSlabRatePayable.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTaxSlabRatePayable.DigitsInGroup = 0
        Me.txtTaxSlabRatePayable.Flags = 0
        Me.txtTaxSlabRatePayable.Location = New System.Drawing.Point(334, 29)
        Me.txtTaxSlabRatePayable.MaxDecimalPlaces = 6
        Me.txtTaxSlabRatePayable.MaxWholeDigits = 21
        Me.txtTaxSlabRatePayable.Name = "txtTaxSlabRatePayable"
        Me.txtTaxSlabRatePayable.Prefix = ""
        Me.txtTaxSlabRatePayable.RangeMax = 1.7976931348623157E+308
        Me.txtTaxSlabRatePayable.RangeMin = -1.7976931348623157E+308
        Me.txtTaxSlabRatePayable.Size = New System.Drawing.Size(84, 21)
        Me.txtTaxSlabRatePayable.TabIndex = 3
        Me.txtTaxSlabRatePayable.Text = "0"
        Me.txtTaxSlabRatePayable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTaxSlabValueBasis
        '
        Me.lblTaxSlabValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxSlabValueBasis.Location = New System.Drawing.Point(334, 5)
        Me.lblTaxSlabValueBasis.Name = "lblTaxSlabValueBasis"
        Me.lblTaxSlabValueBasis.Size = New System.Drawing.Size(84, 13)
        Me.lblTaxSlabValueBasis.TabIndex = 102
        Me.lblTaxSlabValueBasis.Text = "Rate Pay. (%)"
        Me.lblTaxSlabValueBasis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTaxSlabFixedAmount
        '
        Me.txtTaxSlabFixedAmount.AllowNegative = True
        Me.txtTaxSlabFixedAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTaxSlabFixedAmount.DigitsInGroup = 0
        Me.txtTaxSlabFixedAmount.Flags = 0
        Me.txtTaxSlabFixedAmount.Location = New System.Drawing.Point(220, 29)
        Me.txtTaxSlabFixedAmount.MaxDecimalPlaces = 6
        Me.txtTaxSlabFixedAmount.MaxWholeDigits = 21
        Me.txtTaxSlabFixedAmount.Name = "txtTaxSlabFixedAmount"
        Me.txtTaxSlabFixedAmount.Prefix = ""
        Me.txtTaxSlabFixedAmount.RangeMax = 1.7976931348623157E+308
        Me.txtTaxSlabFixedAmount.RangeMin = -1.7976931348623157E+308
        Me.txtTaxSlabFixedAmount.Size = New System.Drawing.Size(108, 21)
        Me.txtTaxSlabFixedAmount.TabIndex = 2
        Me.txtTaxSlabFixedAmount.Text = "0"
        Me.txtTaxSlabFixedAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTaxSlabFixedAmount
        '
        Me.lblTaxSlabFixedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxSlabFixedAmount.Location = New System.Drawing.Point(220, 5)
        Me.lblTaxSlabFixedAmount.Name = "lblTaxSlabFixedAmount"
        Me.lblTaxSlabFixedAmount.Size = New System.Drawing.Size(108, 14)
        Me.lblTaxSlabFixedAmount.TabIndex = 100
        Me.lblTaxSlabFixedAmount.Text = "Fixed Amount"
        Me.lblTaxSlabFixedAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTaxSlabAmtUpto
        '
        Me.txtTaxSlabAmtUpto.AllowNegative = True
        Me.txtTaxSlabAmtUpto.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTaxSlabAmtUpto.DigitsInGroup = 0
        Me.txtTaxSlabAmtUpto.Flags = 0
        Me.txtTaxSlabAmtUpto.Location = New System.Drawing.Point(100, 29)
        Me.txtTaxSlabAmtUpto.MaxDecimalPlaces = 6
        Me.txtTaxSlabAmtUpto.MaxWholeDigits = 21
        Me.txtTaxSlabAmtUpto.Name = "txtTaxSlabAmtUpto"
        Me.txtTaxSlabAmtUpto.Prefix = ""
        Me.txtTaxSlabAmtUpto.RangeMax = 1.7976931348623157E+308
        Me.txtTaxSlabAmtUpto.RangeMin = -1.7976931348623157E+308
        Me.txtTaxSlabAmtUpto.Size = New System.Drawing.Size(114, 21)
        Me.txtTaxSlabAmtUpto.TabIndex = 1
        Me.txtTaxSlabAmtUpto.Text = "0"
        Me.txtTaxSlabAmtUpto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTaxSlabAmountUpto
        '
        Me.lblTaxSlabAmountUpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxSlabAmountUpto.Location = New System.Drawing.Point(100, 5)
        Me.lblTaxSlabAmountUpto.Name = "lblTaxSlabAmountUpto"
        Me.lblTaxSlabAmountUpto.Size = New System.Drawing.Size(111, 14)
        Me.lblTaxSlabAmountUpto.TabIndex = 98
        Me.lblTaxSlabAmountUpto.Text = "Amount Upto"
        Me.lblTaxSlabAmountUpto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbTrnHeadInfo
        '
        Me.gbTrnHeadInfo.AutoScroll = True
        Me.gbTrnHeadInfo.BorderColor = System.Drawing.Color.Black
        Me.gbTrnHeadInfo.Checked = False
        Me.gbTrnHeadInfo.CollapseAllExceptThis = False
        Me.gbTrnHeadInfo.CollapsedHoverImage = Nothing
        Me.gbTrnHeadInfo.CollapsedNormalImage = Nothing
        Me.gbTrnHeadInfo.CollapsedPressedImage = Nothing
        Me.gbTrnHeadInfo.CollapseOnLoad = False
        Me.gbTrnHeadInfo.Controls.Add(Me.Panel1)
        Me.gbTrnHeadInfo.ExpandedHoverImage = Nothing
        Me.gbTrnHeadInfo.ExpandedNormalImage = Nothing
        Me.gbTrnHeadInfo.ExpandedPressedImage = Nothing
        Me.gbTrnHeadInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrnHeadInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrnHeadInfo.HeaderHeight = 25
        Me.gbTrnHeadInfo.HeaderMessage = ""
        Me.gbTrnHeadInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTrnHeadInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTrnHeadInfo.HeightOnCollapse = 0
        Me.gbTrnHeadInfo.LeftTextSpace = 0
        Me.gbTrnHeadInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbTrnHeadInfo.Name = "gbTrnHeadInfo"
        Me.gbTrnHeadInfo.OpenHeight = 182
        Me.gbTrnHeadInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrnHeadInfo.ShowBorder = True
        Me.gbTrnHeadInfo.ShowCheckBox = False
        Me.gbTrnHeadInfo.ShowCollapseButton = False
        Me.gbTrnHeadInfo.ShowDefaultBorderColor = True
        Me.gbTrnHeadInfo.ShowDownButton = False
        Me.gbTrnHeadInfo.ShowHeader = True
        Me.gbTrnHeadInfo.Size = New System.Drawing.Size(362, 479)
        Me.gbTrnHeadInfo.TabIndex = 0
        Me.gbTrnHeadInfo.Temp = 0
        Me.gbTrnHeadInfo.Text = "Transaction Head Information"
        Me.gbTrnHeadInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.lblPeriodsAfterAssignment)
        Me.Panel1.Controls.Add(Me.nudPeriodsAfterAssignment)
        Me.Panel1.Controls.Add(Me.lblActiveOnly)
        Me.Panel1.Controls.Add(Me.lblPRemark)
        Me.Panel1.Controls.Add(Me.nudPriority)
        Me.Panel1.Controls.Add(Me.lblPriority)
        Me.Panel1.Controls.Add(Me.cboRoundOffModeId)
        Me.Panel1.Controls.Add(Me.cboRoundOffTypeId)
        Me.Panel1.Controls.Add(Me.lblRoundOffTypeId)
        Me.Panel1.Controls.Add(Me.cboDefaultCostCenter)
        Me.Panel1.Controls.Add(Me.lblDefaultCostCenter)
        Me.Panel1.Controls.Add(Me.chkProratedFlatRateHead)
        Me.Panel1.Controls.Add(Me.chkIsPerformance)
        Me.Panel1.Controls.Add(Me.chkBasicsalaryasotherearning)
        Me.Panel1.Controls.Add(Me.chkMonetary)
        Me.Panel1.Controls.Add(Me.chkNonCashBenefit)
        Me.Panel1.Controls.Add(Me.chkCumulative)
        Me.Panel1.Controls.Add(Me.chkTaxRelief)
        Me.Panel1.Controls.Add(Me.chkTaxable)
        Me.Panel1.Controls.Add(Me.lblTypeOf)
        Me.Panel1.Controls.Add(Me.cboTypeOf)
        Me.Panel1.Controls.Add(Me.objbtnOtherLanguage)
        Me.Panel1.Controls.Add(Me.chkRecurrent)
        Me.Panel1.Controls.Add(Me.chkAppearOnPayslip)
        Me.Panel1.Controls.Add(Me.lnOtherInfo)
        Me.Panel1.Controls.Add(Me.cboTrnHeadType)
        Me.Panel1.Controls.Add(Me.cboCalcType)
        Me.Panel1.Controls.Add(Me.lblCalcType)
        Me.Panel1.Controls.Add(Me.lblType)
        Me.Panel1.Controls.Add(Me.txtHeadName)
        Me.Panel1.Controls.Add(Me.lblName)
        Me.Panel1.Controls.Add(Me.txtHeadCode)
        Me.Panel1.Controls.Add(Me.lblCode)
        Me.Panel1.Location = New System.Drawing.Point(2, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(359, 510)
        Me.Panel1.TabIndex = 0
        '
        'lblPRemark
        '
        Me.lblPRemark.ForeColor = System.Drawing.Color.Red
        Me.lblPRemark.Location = New System.Drawing.Point(163, 449)
        Me.lblPRemark.Name = "lblPRemark"
        Me.lblPRemark.Size = New System.Drawing.Size(166, 13)
        Me.lblPRemark.TabIndex = 299
        Me.lblPRemark.Text = "Higher No. = Higher Priority"
        '
        'nudPriority
        '
        Me.nudPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPriority.Location = New System.Drawing.Point(110, 445)
        Me.nudPriority.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudPriority.Minimum = New Decimal(New Integer() {10, 0, 0, -2147483648})
        Me.nudPriority.Name = "nudPriority"
        Me.nudPriority.Size = New System.Drawing.Size(50, 21)
        Me.nudPriority.TabIndex = 297
        Me.nudPriority.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPriority
        '
        Me.lblPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPriority.Location = New System.Drawing.Point(3, 447)
        Me.lblPriority.Name = "lblPriority"
        Me.lblPriority.Size = New System.Drawing.Size(105, 15)
        Me.lblPriority.TabIndex = 298
        Me.lblPriority.Text = "Computation Priority"
        Me.lblPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRoundOffModeId
        '
        Me.cboRoundOffModeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRoundOffModeId.DropDownWidth = 215
        Me.cboRoundOffModeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRoundOffModeId.FormattingEnabled = True
        Me.cboRoundOffModeId.Location = New System.Drawing.Point(233, 173)
        Me.cboRoundOffModeId.Name = "cboRoundOffModeId"
        Me.cboRoundOffModeId.Size = New System.Drawing.Size(92, 21)
        Me.cboRoundOffModeId.TabIndex = 296
        '
        'cboRoundOffTypeId
        '
        Me.cboRoundOffTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRoundOffTypeId.DropDownWidth = 215
        Me.cboRoundOffTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRoundOffTypeId.FormattingEnabled = True
        Me.cboRoundOffTypeId.Location = New System.Drawing.Point(114, 173)
        Me.cboRoundOffTypeId.Name = "cboRoundOffTypeId"
        Me.cboRoundOffTypeId.Size = New System.Drawing.Size(113, 21)
        Me.cboRoundOffTypeId.TabIndex = 294
        '
        'lblRoundOffTypeId
        '
        Me.lblRoundOffTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoundOffTypeId.Location = New System.Drawing.Point(7, 176)
        Me.lblRoundOffTypeId.Name = "lblRoundOffTypeId"
        Me.lblRoundOffTypeId.Size = New System.Drawing.Size(108, 15)
        Me.lblRoundOffTypeId.TabIndex = 295
        Me.lblRoundOffTypeId.Text = "Round Off Type"
        Me.lblRoundOffTypeId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDefaultCostCenter
        '
        Me.cboDefaultCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDefaultCostCenter.DropDownWidth = 215
        Me.cboDefaultCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDefaultCostCenter.FormattingEnabled = True
        Me.cboDefaultCostCenter.Location = New System.Drawing.Point(115, 146)
        Me.cboDefaultCostCenter.Name = "cboDefaultCostCenter"
        Me.cboDefaultCostCenter.Size = New System.Drawing.Size(210, 21)
        Me.cboDefaultCostCenter.TabIndex = 292
        '
        'lblDefaultCostCenter
        '
        Me.lblDefaultCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefaultCostCenter.Location = New System.Drawing.Point(8, 149)
        Me.lblDefaultCostCenter.Name = "lblDefaultCostCenter"
        Me.lblDefaultCostCenter.Size = New System.Drawing.Size(108, 15)
        Me.lblDefaultCostCenter.TabIndex = 293
        Me.lblDefaultCostCenter.Text = "Default Cost Center"
        Me.lblDefaultCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkProratedFlatRateHead
        '
        Me.chkProratedFlatRateHead.AutoSize = True
        Me.chkProratedFlatRateHead.Location = New System.Drawing.Point(111, 425)
        Me.chkProratedFlatRateHead.Name = "chkProratedFlatRateHead"
        Me.chkProratedFlatRateHead.Size = New System.Drawing.Size(162, 17)
        Me.chkProratedFlatRateHead.TabIndex = 291
        Me.chkProratedFlatRateHead.Text = "Prorated Flat Rate Head"
        Me.chkProratedFlatRateHead.UseVisualStyleBackColor = True
        '
        'chkIsPerformance
        '
        Me.chkIsPerformance.AutoSize = True
        Me.chkIsPerformance.Location = New System.Drawing.Point(111, 379)
        Me.chkIsPerformance.Name = "chkIsPerformance"
        Me.chkIsPerformance.Size = New System.Drawing.Size(145, 17)
        Me.chkIsPerformance.TabIndex = 290
        Me.chkIsPerformance.Text = "Is Performance Head"
        Me.chkIsPerformance.UseVisualStyleBackColor = True
        '
        'chkBasicsalaryasotherearning
        '
        Me.chkBasicsalaryasotherearning.AutoSize = True
        Me.chkBasicsalaryasotherearning.Location = New System.Drawing.Point(111, 356)
        Me.chkBasicsalaryasotherearning.Name = "chkBasicsalaryasotherearning"
        Me.chkBasicsalaryasotherearning.Size = New System.Drawing.Size(191, 17)
        Me.chkBasicsalaryasotherearning.TabIndex = 11
        Me.chkBasicsalaryasotherearning.Text = "Basic Salary As Other Earning"
        Me.chkBasicsalaryasotherearning.UseVisualStyleBackColor = False
        '
        'chkMonetary
        '
        Me.chkMonetary.AutoSize = True
        Me.chkMonetary.Location = New System.Drawing.Point(111, 333)
        Me.chkMonetary.Name = "chkMonetary"
        Me.chkMonetary.Size = New System.Drawing.Size(183, 17)
        Me.chkMonetary.TabIndex = 10
        Me.chkMonetary.Text = "Monetary Transaction Head"
        Me.chkMonetary.UseVisualStyleBackColor = True
        '
        'chkNonCashBenefit
        '
        Me.chkNonCashBenefit.AutoSize = True
        Me.chkNonCashBenefit.Location = New System.Drawing.Point(112, 264)
        Me.chkNonCashBenefit.Name = "chkNonCashBenefit"
        Me.chkNonCashBenefit.Size = New System.Drawing.Size(122, 17)
        Me.chkNonCashBenefit.TabIndex = 7
        Me.chkNonCashBenefit.Text = "Non-Cash Benefit"
        Me.chkNonCashBenefit.UseVisualStyleBackColor = True
        '
        'chkCumulative
        '
        Me.chkCumulative.AutoSize = True
        Me.chkCumulative.Location = New System.Drawing.Point(111, 310)
        Me.chkCumulative.Name = "chkCumulative"
        Me.chkCumulative.Size = New System.Drawing.Size(90, 17)
        Me.chkCumulative.TabIndex = 9
        Me.chkCumulative.Text = "Cumulative"
        Me.chkCumulative.UseVisualStyleBackColor = True
        '
        'chkTaxRelief
        '
        Me.chkTaxRelief.AutoSize = True
        Me.chkTaxRelief.Location = New System.Drawing.Point(112, 287)
        Me.chkTaxRelief.Name = "chkTaxRelief"
        Me.chkTaxRelief.Size = New System.Drawing.Size(82, 17)
        Me.chkTaxRelief.TabIndex = 8
        Me.chkTaxRelief.Text = "Tax Relief"
        Me.chkTaxRelief.UseVisualStyleBackColor = True
        '
        'chkTaxable
        '
        Me.chkTaxable.AutoSize = True
        Me.chkTaxable.Location = New System.Drawing.Point(112, 241)
        Me.chkTaxable.Name = "chkTaxable"
        Me.chkTaxable.Size = New System.Drawing.Size(173, 17)
        Me.chkTaxable.TabIndex = 6
        Me.chkTaxable.Text = "Taxable Transaction Head"
        Me.chkTaxable.UseVisualStyleBackColor = True
        '
        'lblTypeOf
        '
        Me.lblTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOf.Location = New System.Drawing.Point(8, 94)
        Me.lblTypeOf.Name = "lblTypeOf"
        Me.lblTypeOf.Size = New System.Drawing.Size(101, 15)
        Me.lblTypeOf.TabIndex = 289
        Me.lblTypeOf.Text = "Type of"
        Me.lblTypeOf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTypeOf
        '
        Me.cboTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOf.DropDownWidth = 215
        Me.cboTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOf.FormattingEnabled = True
        Me.cboTypeOf.Location = New System.Drawing.Point(115, 92)
        Me.cboTypeOf.Name = "cboTypeOf"
        Me.cboTypeOf.Size = New System.Drawing.Size(210, 21)
        Me.cboTypeOf.TabIndex = 3
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(304, 38)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 287
        '
        'chkRecurrent
        '
        Me.chkRecurrent.AutoSize = True
        Me.chkRecurrent.Checked = True
        Me.chkRecurrent.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRecurrent.Location = New System.Drawing.Point(111, 402)
        Me.chkRecurrent.Name = "chkRecurrent"
        Me.chkRecurrent.Size = New System.Drawing.Size(83, 17)
        Me.chkRecurrent.TabIndex = 12
        Me.chkRecurrent.Text = "Recurrent"
        Me.chkRecurrent.UseVisualStyleBackColor = True
        '
        'chkAppearOnPayslip
        '
        Me.chkAppearOnPayslip.AutoSize = True
        Me.chkAppearOnPayslip.Checked = True
        Me.chkAppearOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearOnPayslip.Location = New System.Drawing.Point(112, 218)
        Me.chkAppearOnPayslip.Name = "chkAppearOnPayslip"
        Me.chkAppearOnPayslip.Size = New System.Drawing.Size(127, 17)
        Me.chkAppearOnPayslip.TabIndex = 5
        Me.chkAppearOnPayslip.Text = "Appear on Payslip"
        Me.chkAppearOnPayslip.UseVisualStyleBackColor = True
        '
        'lnOtherInfo
        '
        Me.lnOtherInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnOtherInfo.Location = New System.Drawing.Point(3, 199)
        Me.lnOtherInfo.Name = "lnOtherInfo"
        Me.lnOtherInfo.Size = New System.Drawing.Size(322, 16)
        Me.lnOtherInfo.TabIndex = 24
        Me.lnOtherInfo.Text = "Other Information"
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(115, 65)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(210, 21)
        Me.cboTrnHeadType.TabIndex = 2
        '
        'cboCalcType
        '
        Me.cboCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalcType.DropDownWidth = 215
        Me.cboCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcType.FormattingEnabled = True
        Me.cboCalcType.Location = New System.Drawing.Point(115, 119)
        Me.cboCalcType.Name = "cboCalcType"
        Me.cboCalcType.Size = New System.Drawing.Size(210, 21)
        Me.cboCalcType.TabIndex = 4
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(8, 122)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(101, 15)
        Me.lblCalcType.TabIndex = 12
        Me.lblCalcType.Text = "Calculation Type"
        Me.lblCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblType
        '
        Me.lblType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(8, 68)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(101, 15)
        Me.lblType.TabIndex = 9
        Me.lblType.Text = "Tran. Head Type"
        Me.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHeadName
        '
        Me.txtHeadName.Flags = 0
        Me.txtHeadName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeadName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHeadName.Location = New System.Drawing.Point(115, 38)
        Me.txtHeadName.MaxLength = 50
        Me.txtHeadName.Name = "txtHeadName"
        Me.txtHeadName.Size = New System.Drawing.Size(183, 21)
        Me.txtHeadName.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 41)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(101, 15)
        Me.lblName.TabIndex = 9
        Me.lblName.Text = "Tran. Head Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHeadCode
        '
        Me.txtHeadCode.Flags = 0
        Me.txtHeadCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeadCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHeadCode.Location = New System.Drawing.Point(115, 11)
        Me.txtHeadCode.MaxLength = 50
        Me.txtHeadCode.Name = "txtHeadCode"
        Me.txtHeadCode.Size = New System.Drawing.Size(183, 21)
        Me.txtHeadCode.TabIndex = 0
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 14)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(101, 15)
        Me.lblCode.TabIndex = 3
        Me.lblCode.Text = "Tran. Head Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 552)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(844, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(632, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(735, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'nudPeriodsAfterAssignment
        '
        Me.nudPeriodsAfterAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPeriodsAfterAssignment.Location = New System.Drawing.Point(111, 474)
        Me.nudPeriodsAfterAssignment.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudPeriodsAfterAssignment.Minimum = New Decimal(New Integer() {10, 0, 0, -2147483648})
        Me.nudPeriodsAfterAssignment.Name = "nudPeriodsAfterAssignment"
        Me.nudPeriodsAfterAssignment.Size = New System.Drawing.Size(50, 21)
        Me.nudPeriodsAfterAssignment.TabIndex = 300
        Me.nudPeriodsAfterAssignment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActiveOnly
        '
        Me.lblActiveOnly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActiveOnly.Location = New System.Drawing.Point(4, 476)
        Me.lblActiveOnly.Name = "lblActiveOnly"
        Me.lblActiveOnly.Size = New System.Drawing.Size(94, 15)
        Me.lblActiveOnly.TabIndex = 301
        Me.lblActiveOnly.Text = "Active Only"
        Me.lblActiveOnly.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPeriodsAfterAssignment
        '
        Me.lblPeriodsAfterAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodsAfterAssignment.Location = New System.Drawing.Point(168, 476)
        Me.lblPeriodsAfterAssignment.Name = "lblPeriodsAfterAssignment"
        Me.lblPeriodsAfterAssignment.Size = New System.Drawing.Size(146, 15)
        Me.lblPeriodsAfterAssignment.TabIndex = 302
        Me.lblPeriodsAfterAssignment.Text = " Periods after Assignment"
        Me.lblPeriodsAfterAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTransactionHead_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 607)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransactionHead_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Transaction Heads"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbComputationInfo.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlAddEditDelete.ResumeLayout(False)
        Me.pnlSimpleSlab.ResumeLayout(False)
        Me.pnlSimpleSlab.PerformLayout()
        Me.pnlTaxSlab.ResumeLayout(False)
        Me.pnlTaxSlab.PerformLayout()
        Me.gbTrnHeadInfo.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.nudPeriodsAfterAssignment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbComputationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objbtnAddComputation As eZee.Common.eZeeGradientButton
    Friend WithEvents cboComputeOn As System.Windows.Forms.ComboBox
    Friend WithEvents lblComputeOn As System.Windows.Forms.Label
    Friend WithEvents lblSpecifiedFormula As System.Windows.Forms.Label
    Friend WithEvents gbTrnHeadInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkAppearOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents lnOtherInfo As eZee.Common.eZeeLine
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents cboCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents txtHeadName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtHeadCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents chkRecurrent As System.Windows.Forms.CheckBox
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTypeOf As System.Windows.Forms.ComboBox
    Friend WithEvents lblTypeOf As System.Windows.Forms.Label
    Friend WithEvents pnlTaxSlab As System.Windows.Forms.Panel
    Friend WithEvents txtTaxSlabAmtUpto As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTaxSlabAmountUpto As System.Windows.Forms.Label
    Friend WithEvents txtTaxSlabFixedAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTaxSlabFixedAmount As System.Windows.Forms.Label
    Friend WithEvents txtTaxSlabRatePayable As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTaxSlabValueBasis As System.Windows.Forms.Label
    Friend WithEvents pnlSimpleSlab As System.Windows.Forms.Panel
    Friend WithEvents txtSimpleSlabAmtUpto As eZee.TextBox.NumericTextBox
    Friend WithEvents lblSimleSlabAmtUpto As System.Windows.Forms.Label
    Friend WithEvents txtSimpleSlabValueBasis As eZee.TextBox.NumericTextBox
    Friend WithEvents lblSimleSlabValueBasis As System.Windows.Forms.Label
    Friend WithEvents cboSimpleSlabAmountType As System.Windows.Forms.ComboBox
    Friend WithEvents lblSimleSlabAmountType As System.Windows.Forms.Label
    Friend WithEvents objSlabType As System.Windows.Forms.ComboBox
    Friend WithEvents pnlAddEditDelete As System.Windows.Forms.Panel
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents lvTrnHeadSlabSimple As eZee.Common.eZeeListView
    Friend WithEvents colhTranHeadunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSimpleAmtUpto As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhsimpleSlabType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSimpleValueBasis As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSimpleGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvTrnHeadSlabInExcessof As eZee.Common.eZeeListView
    Friend WithEvents colhtrantaxslabunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTaxAmountUpto As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTaxFixedAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTaxRatePayable As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTaxInExcessOf As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTaxGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkTaxable As System.Windows.Forms.CheckBox
    Friend WithEvents chkTaxRelief As System.Windows.Forms.CheckBox
    Friend WithEvents chkCumulative As System.Windows.Forms.CheckBox
    Friend WithEvents colhSimplePeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboSimpleSlabPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblSimpleSlabPeriod As System.Windows.Forms.Label
    Friend WithEvents lnkCopySlab As System.Windows.Forms.LinkLabel
    Friend WithEvents colhSimpleEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblTaxSlabPeriod As System.Windows.Forms.Label
    Friend WithEvents cboTaxSlabPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents colhTaxPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTaxEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkNonCashBenefit As System.Windows.Forms.CheckBox
    Friend WithEvents chkMonetary As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents objbtnAddSlabFormula As eZee.Common.eZeeGradientButton
    Friend WithEvents txtSlabFormula As System.Windows.Forms.TextBox
    Friend WithEvents chkBasicsalaryasotherearning As System.Windows.Forms.CheckBox
    Friend WithEvents chkIsPerformance As System.Windows.Forms.CheckBox
    Friend WithEvents chkProratedFlatRateHead As System.Windows.Forms.CheckBox
    Friend WithEvents cboDefaultCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblDefaultCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboRoundOffTypeId As System.Windows.Forms.ComboBox
    Friend WithEvents lblRoundOffTypeId As System.Windows.Forms.Label
    Friend WithEvents cboRoundOffModeId As System.Windows.Forms.ComboBox
    Friend WithEvents lblPRemark As System.Windows.Forms.Label
    Friend WithEvents nudPriority As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPriority As System.Windows.Forms.Label
    Friend WithEvents lblPeriodsAfterAssignment As System.Windows.Forms.Label
    Friend WithEvents nudPeriodsAfterAssignment As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblActiveOnly As System.Windows.Forms.Label
End Class
