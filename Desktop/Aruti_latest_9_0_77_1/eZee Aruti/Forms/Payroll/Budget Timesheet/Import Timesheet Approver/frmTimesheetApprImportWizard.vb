﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTimesheetApprImportWizard

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTimesheetApprImportWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim mdicEmpAdded As New Dictionary(Of String, Integer)

#End Region

#Region " From's Events "

    Private Sub frmTimesheetApprImportWizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetApprImportWizard_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub WizImportTimesheetApprover_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizImportTimesheetApprover.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizImportTimesheetApprover.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportTimesheetApprover_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub WizImportTimesheetApprover_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizImportTimesheetApprover.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizImportTimesheetApprover.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                    Call EnableDisableMappingControls()

                Case WizImportTimesheetApprover.Pages.IndexOf(wizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox AndAlso ctrl.Enabled = True Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case WizImportTimesheetApprover.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportTimesheetApprover_BeforeSwitchPages", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Dim objEmployee As New clsEmployee_Master
        Try
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("approver", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("levelname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("employee", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("approver_code", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                If chkImportExternalApprover.Checked Then
                    drNewRow.Item("approver") = dtRow.Item(cboUserName.Text).ToString.Trim
                Else
                    drNewRow.Item("approver") = dtRow.Item(cboApproverName.Text).ToString.Trim
                End If

                drNewRow.Item("levelname") = dtRow.Item(cboApproverLevel.Text).ToString.Trim
                drNewRow.Item("employee") = dtRow.Item(cboEmployeeName.Text).ToString.Trim

                If chkImportExternalApprover.Checked Then
                    drNewRow.Item("approver_code") = dtRow.Item(cboUserName.Text).ToString.Trim
                Else
                    drNewRow.Item("approver_code") = dtRow.Item(cboApproverCode.Text).ToString.Trim
                End If

                drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhLevel.DataPropertyName = "levelname"
                colhApprover.DataPropertyName = "approver"
                colhEmployee.DataPropertyName = "employee"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dvGriddata.Sort = "approver,levelname"
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizImportTimesheetApprover.BackEnabled = False
            WizImportTimesheetApprover.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    If TypeOf ctrl Is ComboBox Then
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
                    End If
                Next
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub EnableDisableMappingControls()
        Try
            objlblSign6.Enabled = chkImportExternalApprover.Checked
            lblUserName.Enabled = chkImportExternalApprover.Checked
            cboUserName.Enabled = chkImportExternalApprover.Checked

            objlblSign4.Enabled = Not chkImportExternalApprover.Checked
            lblApproverName.Enabled = Not chkImportExternalApprover.Checked
            cboApproverName.Enabled = Not chkImportExternalApprover.Checked

            objlblSign1.Enabled = Not chkImportExternalApprover.Checked
            lblApproverCode.Enabled = Not chkImportExternalApprover.Checked
            cboApproverCode.Enabled = Not chkImportExternalApprover.Checked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableDisableMappingControls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow

                If cboApproverCode.Enabled = True AndAlso .Item(cboApproverCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Approver Code cannot be blank. Please set Approver Code in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboApproverLevel.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Approver Level cannot be blank. Please set Approver Level in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboApproverName.Enabled = True AndAlso .Item(cboApproverName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Approver Name cannot be blank. Please set Approver Name in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Empolyee Code cannot be blank. Please set Employee Code in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Employee Name cannot be blank. Please set Employee Name in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboUserName.Enabled = True AndAlso .Item(cboUserName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "External Username cannot be blank. Please set External Username in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
            Dim mDicApprover As New Dictionary(Of String, Integer)
            For Each dtRow As DataRow In mdt_ImportData_Others.Rows

                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                Dim objLevel As New clstsapproverlevel_master
                Dim objEmployee As New clsEmployee_Master
                Dim objApproverTran As New clstsapprover_Tran
                Dim objApproverMaster As New clstsapprover_master
                Dim intLevelUnkid As Integer = 0
                Dim intApproverUnkid As Integer = 0
                Dim intMapUserId As Integer = 0
                Dim intEmployeeUnkid As Integer = 0
                Dim intApproverMstUnkid As Integer = 0
                Dim intApproverTrnUnkid As Integer = 0

                Dim objUserAddEdit As New clsUserAddEdit

                '---------- CHECK IF LEVEL EXISTS ------ START
                intLevelUnkid = objLevel.GetApproverLevelUnkId(CStr(dtRow.Item("levelname")))
                If intLevelUnkid <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Approver Level Not Found.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                '---------- CHECK IF LEVEL EXISTS ------ END

                If chkImportExternalApprover.Checked Then
                    intApproverUnkid = objUserAddEdit.Return_UserId(CStr(dtRow.Item("approver_code")).Trim, "hrmsConfiguration", enLoginMode.USER)
                    objUserAddEdit._Userunkid = intApproverUnkid
                    If objUserAddEdit._EmployeeCompanyUnkid = Company._Object._Companyunkid Then
                        intApproverUnkid = 0
                    End If
                    intMapUserId = intApproverUnkid
                    objUserAddEdit = Nothing
                Else
                    intApproverUnkid = objEmployee.GetEmployeeUnkid("", CStr(dtRow.Item("approver_code")))
                End If


                If intApproverUnkid <= 0 Then
                    dtRow.Item("image") = imgError
                    If chkImportExternalApprover.Checked Then
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Invalid Username.")
                    Else
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Invalid Approver Code.")
                    End If
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                '---------- CHECK IF APPROVER EXISTS ------ END

                '---------- CHECK IF EMPLOYEE EXISTS ------ START
                intEmployeeUnkid = objEmployee.GetEmployeeUnkid("", CStr(dtRow.Item("employeecode")))
                If intEmployeeUnkid <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Invalid Employee Code.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                '---------- CHECK IF EMPLOYEE EXISTS ------ END

                '---------- CHECK  & INSERT IF APPROVER MASTER EXISTS ------ START
                If objApproverMaster.isExist(intApproverUnkid, intLevelUnkid, intApproverMstUnkid, chkImportExternalApprover.Checked) = False Then
                    objApproverMaster._Tslevelunkid = intLevelUnkid
                    objApproverMaster._Employeeapproverunkid = intApproverUnkid
                    objApproverMaster._MapUserId = intMapUserId
                    objApproverMaster._Isvoid = False
                    objApproverMaster._Voiddatetime = Nothing
                    objApproverMaster._Userunkid = User._Object._Userunkid
                    objApproverMaster._Isactive = True
                    objApproverMaster._Isswap = False
                    objApproverMaster._Isexternalapprover = chkImportExternalApprover.Checked
                    intApproverMstUnkid = objApproverMaster.ImportApproverInsert()

                    If intApproverMstUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objApproverMaster._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    If mDicApprover.ContainsKey(intApproverUnkid.ToString & "|" & intLevelUnkid.ToString) = False Then
                        mDicApprover.Add(intApproverUnkid.ToString & "|" & intLevelUnkid.ToString, intApproverMstUnkid)
                    End If
                Else
                    intApproverMstUnkid = objApproverMaster._Tsapproverunkid
                    If mDicApprover.Keys.Count > 0 AndAlso intApproverMstUnkid <= 0 Then
                        intApproverMstUnkid = mDicApprover(intApproverUnkid.ToString & "|" & intLevelUnkid.ToString)
                    End If
                End If
                '---------- CHECK  & INSERT IF APPROVER MASTER EXISTS ------ END

                '---------- INSERT IF EMPLOYEE FOR THAT APPROVER EXISTS ------ START
                objApproverTran._Tsapproverunkid = intApproverMstUnkid
                objApproverTran._Userunkid = User._Object._Userunkid

                If objApproverTran.ImportInsertEmployee(intEmployeeUnkid.ToString, User._Object._Userunkid, intApproverTrnUnkid) = False Then
                    If intApproverTrnUnkid > 0 Then
                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Already Exists")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                        dtRow.Item("objStatus") = 0
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objApproverTran._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                Else
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                End If
                '---------- CHECK  & INSERT IF APPROVER MASTER EXISTS ------ END
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            If dvGriddata IsNot Nothing Then
                dvGriddata.RowFilter = "objStatus = 2"

                Dim savDialog As New SaveFileDialog
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("objstatus")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Leave Approver Wizard") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"
            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If
            objFileOpen = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.WizImportTimesheetApprover.CancelText = Language._Object.getCaption(Me.WizImportTimesheetApprover.Name & "_CancelText" , Me.WizImportTimesheetApprover.CancelText)
			Me.WizImportTimesheetApprover.NextText = Language._Object.getCaption(Me.WizImportTimesheetApprover.Name & "_NextText" , Me.WizImportTimesheetApprover.NextText)
			Me.WizImportTimesheetApprover.BackText = Language._Object.getCaption(Me.WizImportTimesheetApprover.Name & "_BackText" , Me.WizImportTimesheetApprover.BackText)
			Me.WizImportTimesheetApprover.FinishText = Language._Object.getCaption(Me.WizImportTimesheetApprover.Name & "_FinishText" , Me.WizImportTimesheetApprover.FinishText)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.lblApproverCode.Text = Language._Object.getCaption(Me.lblApproverCode.Name, Me.lblApproverCode.Text)
			Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblFirstname.Text = Language._Object.getCaption(Me.lblFirstname.Name, Me.lblFirstname.Text)
			Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.Name, Me.lblApproverName.Text)
			Me.colhApprover.HeaderText = Language._Object.getCaption(Me.colhApprover.Name, Me.colhApprover.HeaderText)
			Me.colhLevel.HeaderText = Language._Object.getCaption(Me.colhLevel.Name, Me.colhLevel.HeaderText)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.elImportType.Text = Language._Object.getCaption(Me.elImportType.Name, Me.elImportType.Text)
			Me.chkImportExternalApprover.Text = Language._Object.getCaption(Me.chkImportExternalApprover.Name, Me.chkImportExternalApprover.Text)
			Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from.")
			Language.setMessage(mstrModuleName, 2, "Please the select the correct field to Import Data.")
			Language.setMessage(mstrModuleName, 3, "Approver Code cannot be blank. Please set Approver Code in order to import.")
			Language.setMessage(mstrModuleName, 4, "Approver Level cannot be blank. Please set Approver Level in order to import.")
			Language.setMessage(mstrModuleName, 5, "Approver Name cannot be blank. Please set Approver Name in order to import.")
			Language.setMessage(mstrModuleName, 6, "Empolyee Code cannot be blank. Please set Employee Code in order to import.")
			Language.setMessage(mstrModuleName, 7, "Employee Name cannot be blank. Please set Employee Name in order to import.")
			Language.setMessage(mstrModuleName, 8, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 9, "Approver Level Not Found.")
			Language.setMessage(mstrModuleName, 10, "Fail")
			Language.setMessage(mstrModuleName, 11, "Invalid Approver Code.")
			Language.setMessage(mstrModuleName, 12, "Invalid Employee Code.")
			Language.setMessage(mstrModuleName, 13, "Success")
			Language.setMessage(mstrModuleName, 14, "Already Exists")
			Language.setMessage(mstrModuleName, 15, "External Username cannot be blank. Please set External Username in order to import.")
			Language.setMessage(mstrModuleName, 16, "Invalid Username.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class