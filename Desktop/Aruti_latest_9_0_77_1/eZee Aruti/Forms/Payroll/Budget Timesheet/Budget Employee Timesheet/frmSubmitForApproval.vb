﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO

Public Class frmSubmitForApproval

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmSubmitForApproval"
    Dim objBudgetTimesheet As clsBudgetEmp_timesheet

    Private mblnCancel As Boolean = True
    Private mintPeriodID As Integer = -1
    'Nilay (01 Apr 2017) -- Start
    'Private mblnIsFromPendingSubmitForApproval As Boolean = False
    'Nilay (01 Apr 2017) -- End
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mdtEmpTimesheet As DataTable = Nothing
    'Nilay (01 Apr 2017) -- Start
    Private menOperation As clsBudgetEmp_timesheet.enBudgetTimesheetStatus
    'Nilay (01 Apr 2017) -- End

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
    Dim mstrCompanyGrpName As String = ""
    'Pinkal (06-Jan-2023) -- End

#End Region

#Region " Display Dialog "
    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    'Public Function displayDialog(ByVal intPeriodID As Integer, ByVal blnIsFromPendingSubmitForApproval As Boolean) As Boolean
    Public Function displayDialog(ByVal intPeriodID As Integer, ByVal enOperation As clsBudgetEmp_timesheet.enBudgetTimesheetStatus) As Boolean
        'Nilay (01 Apr 2017) -- End
        Try
            mintPeriodID = intPeriodID
            'Nilay (01 Apr 2017) -- Start
            'mblnIsFromPendingSubmitForApproval = blnIsFromPendingSubmitForApproval
            menOperation = enOperation
            'Nilay (01 Apr 2017) -- End
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmSubmitForApproval_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objBudgetTimesheet = New clsBudgetEmp_timesheet
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            SetVisibility()
            Call SetControlVisibility()

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrCompanyGrpName = objGroup._Groupname.Trim
            objGroup = Nothing
            'Pinkal (06-Jan-2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSubmitForApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSubmitForApproval_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSubmitForApproval_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSubmitForApproval_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudgetEmp_timesheet.SetMessages()
            objfrm._Other_ModuleNames = "clsBudgetEmp_timesheet"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmSubmitForApproval_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date.Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = mintPeriodID
            End With
            objPeriod = Nothing
            dsList = Nothing

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If mblnIsFromPendingSubmitForApproval = False Then
            '    Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid NOT IN (4,6)").CopyToDataTable  'statusunkid = 4 = Rescheduled
            '    With cboStatus
            '        .ValueMember = "statusunkid"
            '        .DisplayMember = "name"
            '        .DataSource = dtTable
            '        .SelectedValue = 0
            '    End With
            'End If
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED

                    'Pinkal (03-Jan-2020) -- Start
                    'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
                    'Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable  'statusunkid = 4 = Rescheduled
                    Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", "", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable  'statusunkid = 4 = Rescheduled
                    'Pinkal (03-Jan-2020) -- End

                    With cboStatus
                        .ValueMember = "statusunkid"
                        .DisplayMember = "name"
                        .DataSource = dtTable
                        .SelectedValue = 0
                    End With
            End Select
            'Nilay (01 Apr 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim dsList As DataSet = Nothing
            Dim strSearch As String = ""

            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED Then
                If User._Object.Privilege._AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval = False Then Exit Sub
            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                If User._Object.Privilege._AllowToViewPendingEmpBudgetTimesheetSubmitForApproval = False Then Exit Sub
            End If
            'Pinkal (03-May-2017) -- End

            If dtpDate.Checked = True Then
                strSearch &= "AND ltbemployee_timesheet.activitydate='" & eZeeDate.convertDate(dtpDate.Value) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND ltbemployee_timesheet.employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
            End If

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If mblnIsFromPendingSubmitForApproval = False Then
            '    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 "
            '    If CInt(cboStatus.SelectedValue) > 0 Then
            '        strSearch &= "AND ltbemployee_timesheet.statusunkid=" & CInt(cboStatus.SelectedValue) & " "
            '    End If
            'Else
            '    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=0 "
            'End If
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=0 AND ltbemployee_timesheet.statusunkid=2 "

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 "
                    If CInt(cboStatus.SelectedValue) > 0 Then
                        strSearch &= "AND ltbemployee_timesheet.statusunkid=" & CInt(cboStatus.SelectedValue) & " "
                    End If

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 AND ltbemployee_timesheet.statusunkid=1 "

            End Select
            'Nilay (01 Apr 2017) -- End

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.

            'dsList = objBudgetTimesheet.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
            '                                    FinancialYear._Object._YearUnkid, FinancialYear._Object._Companyunkid, _
            '                                    mdtStartDate, mdtEndDate, ConfigParameter._Object._UserAccessModeSetting, _
            '                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, , True, True, _
            '                                    CInt(cboPeriod.SelectedValue), , strSearch, , True)

            dsList = objBudgetTimesheet.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, FinancialYear._Object._Companyunkid, _
                                                mdtStartDate, mdtEndDate, ConfigParameter._Object._UserAccessModeSetting, _
                                               True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._AllowOverTimeToEmpTimesheet, _
                                                CInt(cboEmployee.SelectedValue), True, True, CInt(cboPeriod.SelectedValue), , strSearch, , True)

            'Pinkal (28-Mar-2018) -- End



            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            If ConfigParameter._Object._AllowOverTimeToEmpTimesheet Then
                Dim objEmpHoliday As New clsemployee_holiday
                Dim dsHolidayList As DataSet = objEmpHoliday.GetList("Holiday", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                  , mdtStartDate, mdtEndDate, ConfigParameter._Object._UserAccessModeSetting, True _
                                                                                                  , ConfigParameter._Object._IsIncludeInactiveEmp, True, -1, Nothing, _
                                                                                                  , " AND  CONVERT(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(mdtStartDate) & "'  AND CONVERT(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "'")

                objEmpHoliday = Nothing

                For Each drRow As DataRow In dsHolidayList.Tables(0).Rows
                    Dim dRow() As DataRow = dsList.Tables(0).Select("ADate = '" & drRow("holidaydate").ToString() & "' AND employeeunkid = " & CInt(drRow("employeeunkid")))
                    If dRow.Length > 0 Then
                        For i As Integer = 0 To dRow.Length - 1
                            dRow(i)("isholiday") = True
                            dRow(i).AcceptChanges()
                        Next
                    End If
                Next
                dsList.AcceptChanges()
            End If
            'Pinkal (13-Aug-2018) -- End



            dgvEmpTimesheetList.AutoGenerateColumns = False

            dgcolhSelect.DataPropertyName = "IsChecked"
            dgcolhEmployee.DataPropertyName = "Particular" 'Nilay (21 Mar 2017) -- [Employee REPLACED BY Particular]
            dgcolhDate.DataPropertyName = "activitydate"
            dgcolhDonor.DataPropertyName = "fundname"
            dgcolhProject.DataPropertyName = "fundprojectname"
            dgcolhActivity.DataPropertyName = "activity_name"
            dgcolhHours.DataPropertyName = "activity_hrs"
            dgcolhApprovedActHrs.DataPropertyName = "approvedactivity_hrs"
            dgcolhDescription.DataPropertyName = "description"
            dgcolhStatus.DataPropertyName = "status"
            objdgcolhEmpTimesheetID.DataPropertyName = "emptimesheetunkid"
            objdgcolhPeriodID.DataPropertyName = "periodunkid"
            objdgcolhEmployeeID.DataPropertyName = "employeeunkid"
            objdgcolhFundSourceID.DataPropertyName = "fundsourceunkid"
            objdgcolhProjectID.DataPropertyName = "fundprojectcodeunkid"
            objdgcolhActivityID.DataPropertyName = "fundactivityunkid"
            objdgcolhStatusId.DataPropertyName = "statusunkid"
            objdgcolhIsGrp.DataPropertyName = "IsGrp" 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
            objdgcolhIsHoliday.DataPropertyName = "isholiday"
            objdgcolhIsDayOFF.DataPropertyName = "isDayOff"
            objdgcolhIsLeave.DataPropertyName = "isLeave"
            objdgcolhApprovedActivityHoursInMin.DataPropertyName = "ApprovedActivityHoursInMin"
            objdgcolhIsSubmitForApproval.DataPropertyName = "issubmit_approval"
            objdgcolhADate.DataPropertyName = "ADate" 'Nilay (21 Mar 2017) -- [adate REPLACED BY ADate]

            If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED AndAlso CInt(cboStatus.SelectedValue) > 0 Then
                dgvEmpTimesheetList.Columns(dgcolhApprovedActHrs.Index).Visible = True

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED Then
                dgvEmpTimesheetList.Columns(dgcolhApprovedActHrs.Index).Visible = True
            End If

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgcolhSubmissionRemark.DataPropertyName = "submission_remark"
            'Pinkal (28-Jul-2018) -- End


            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            dgcolhCostCenter.DataPropertyName = "costcentername"
            If mstrCompanyGrpName.Trim.ToUpper() = "PSI MALAWI" Then
                dgcolhCostCenter.Visible = True
            End If
            'Pinkal (06-Jan-2023) -- End

            mdtEmpTimesheet = dsList.Tables("List")
            dgvEmpTimesheetList.DataSource = mdtEmpTimesheet

            Call SetGridColor()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(CStr(dgvEmpTimesheetList.RowCount))
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dr = From p As DataGridViewRow In dgvEmpTimesheetList.Rows.Cast(Of DataGridViewRow)() Where CBool(p.Cells(objdgcolhIsGrp.Index).Value) = True Select p
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

            Dim drDays = From p As DataGridViewRow In dgvEmpTimesheetList.Rows.Cast(Of DataGridViewRow)() _
                                Where CBool(p.Cells(objdgcolhIsHoliday.Index).Value) = True OrElse CBool(p.Cells(objdgcolhIsDayOFF.Index).Value) = True OrElse CBool(p.Cells(objdgcolhIsLeave.Index).Value) = True Select p
            drDays.ToList.ForEach(Function(x) SetRowStyle(x, False))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal isHeader As Boolean) As Boolean
        Try
            If isHeader Then
                Dim dgvcsHeader As New DataGridViewCellStyle
                dgvcsHeader.ForeColor = Color.White
                dgvcsHeader.SelectionBackColor = Color.Gray
                dgvcsHeader.SelectionForeColor = Color.White
                dgvcsHeader.BackColor = Color.Gray
                xRow.DefaultCellStyle = dgvcsHeader
            Else
                xRow.DefaultCellStyle.ForeColor = Color.Red
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
        Return True
    End Function

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private Sub SetControlVisibility()
        Try
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL
                    Me.Text = Language.getMessage(mstrModuleName, 6, "Pending Submit For Approval")
                    btnSubmitForApproval.Visible = True
                    btnCancel.Visible = False
                    btnDelete.Visible = False
                    chkSelectAll.Visible = True
                    dgvEmpTimesheetList.Columns(dgcolhSelect.Index).Visible = True
                    dgvEmpTimesheetList.Columns(dgcolhStatus.Index).Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    'gbEmployeeTimesheet.Height = 60
                    'dgvEmpTimesheetList.Height = 355
                    'dgvEmpTimesheetList.Location = New Point(12, 78)
                    'chkSelectAll.Location = New Point(19, 83)
                    gbEmployeeTimesheet.Height = 112
                    dgvEmpTimesheetList.Height = 305
                    dgvEmpTimesheetList.Location = New Point(12, 128)
                    chkSelectAll.Location = New Point(19, 134)
                    LblSubmissionRemark.Visible = True
                    txtSubmissionRemark.Visible = True
                    'Pinkal (28-Jul-2018) -- End

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
                    Me.Text = Language.getMessage(mstrModuleName, 7, "Completed Submit For Approval")
                    btnSubmitForApproval.Visible = False
                    btnCancel.Visible = False
                    btnDelete.Visible = False
                    chkSelectAll.Visible = False
                    dgvEmpTimesheetList.Columns(dgcolhSelect.Index).Visible = False
                    dgvEmpTimesheetList.Columns(dgcolhStatus.Index).Visible = True
                    lblStatus.Visible = True
                    cboStatus.Visible = True
                    gbEmployeeTimesheet.Height = 90
                    dgvEmpTimesheetList.Height = 325
                    dgvEmpTimesheetList.Location = New Point(12, 108)
                    chkSelectAll.Location = New Point(19, 113)

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    LblSubmissionRemark.Visible = False
                    txtSubmissionRemark.Visible = False
                    'Pinkal (28-Jul-2018) -- End


                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    Me.Text = Language.getMessage(mstrModuleName, 8, "Global Cancel Timesheet")
                    btnCancel.Visible = True
                    btnSubmitForApproval.Visible = False
                    btnDelete.Visible = False
                    chkSelectAll.Visible = True
                    dgvEmpTimesheetList.Columns(dgcolhSelect.Index).Visible = True
                    dgvEmpTimesheetList.Columns(dgcolhStatus.Index).Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    gbEmployeeTimesheet.Height = 60
                    dgvEmpTimesheetList.Height = 355
                    dgvEmpTimesheetList.Location = New Point(12, 78)
                    chkSelectAll.Location = New Point(19, 83)

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    LblSubmissionRemark.Visible = False
                    txtSubmissionRemark.Visible = False
                    'Pinkal (28-Jul-2018) -- End

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    Me.Text = Language.getMessage(mstrModuleName, 9, "Global Delete Timesheet")
                    btnDelete.Visible = True
                    btnSubmitForApproval.Visible = False
                    btnCancel.Visible = False
                    chkSelectAll.Visible = True
                    dgvEmpTimesheetList.Columns(dgcolhSelect.Index).Visible = True
                    dgvEmpTimesheetList.Columns(dgcolhStatus.Index).Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    gbEmployeeTimesheet.Height = 60
                    dgvEmpTimesheetList.Height = 355
                    dgvEmpTimesheetList.Location = New Point(12, 78)
                    chkSelectAll.Location = New Point(19, 83)

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    LblSubmissionRemark.Visible = False
                    txtSubmissionRemark.Visible = False
                    'Pinkal (28-Jul-2018) -- End


            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetControlVisibility", mstrModuleName)
        End Try
    End Sub
    'Nilay (01 Apr 2017) -- End


    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
    Private Sub SetVisibility()
        Try
            btnSubmitForApproval.Enabled = User._Object.Privilege._AllowToSubmitForApprovalForPendingEmpBudgetTimesheet
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEmployeeBudgetTimesheet
            btnCancel.Enabled = User._Object.Privilege._AllowToCancelEmployeeBudgetTimesheet


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgcolhActivity.Visible = ConfigParameter._Object._ShowBgTimesheetActivity
            dgcolhProject.Visible = ConfigParameter._Object._ShowBgTimesheetActivityProject
            If menOperation <> clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                dgvEmpTimesheetList.Columns(dgcolhSubmissionRemark.Index).Visible = True
            Else
                dgvEmpTimesheetList.Columns(dgcolhSubmissionRemark.Index).Visible = False
            End If
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Pinkal (03-May-2017) -- End


#End Region

#Region " ComboBox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            dtpDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date

            If CInt(cboPeriod.SelectedValue) > 0 Then

                'Pinkal (05-Jun-2017) -- Start
                'Enhancement - Implemented TnA Period for PSI Malawi AS Per Suzan's Request .

                'Dim dRowView As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                'If dRowView IsNot Nothing Then
                '    mdtStartDate = eZeeDate.convertDate(dRowView("start_date").ToString()).Date
                '    mdtEndDate = eZeeDate.convertDate(dRowView("end_date").ToString()).Date
                '    dtpDate.MinDate = eZeeDate.convertDate(dRowView("start_date").ToString()).Date
                '    dtpDate.MaxDate = eZeeDate.convertDate(dRowView("end_date").ToString()).Date
                '    dtpDate.Value = dtpDate.MinDate
                '    dtpDate.Checked = False
                'End If

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtpDate.MinDate = objPeriod._TnA_StartDate.Date
                dtpDate.MaxDate = objPeriod._TnA_EndDate.Date
                mdtStartDate = objPeriod._TnA_StartDate.Date
                mdtEndDate = objPeriod._TnA_EndDate.Date
                    dtpDate.Value = dtpDate.MinDate
                    dtpDate.Checked = False
                objPeriod = Nothing

                'Pinkal (05-Jun-2017) -- End

                Dim objEmployee As New clsEmployee_Master

                'Pinkal (12-Oct-2017) -- Start
                'Enhancement - Working on CCK Leave Planner Changes & Budget Timesheet changes.
                'Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                '                                                    Company._Object._Companyunkid, dtpDate.MaxDate.Date, dtpDate.MaxDate.Date, _
                '                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                                    ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

                Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                                   Company._Object._Companyunkid, dtpDate.MinDate.Date, dtpDate.MaxDate.Date, _
                                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                    ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

                'Pinkal (12-Oct-2017) -- End

                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With
                objEmployee = Nothing
                dsList = Nothing
            Else
                dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpDate.Checked = False
                mdtStartDate = ConfigParameter._Object._CurrentDateAndTime.Date
                mdtEndDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click, _
                                                                                                             objbtnSearchEmployee.Click
        Try
            Dim objFrm As New frmCommonSearch

            With objFrm
                If CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper = objbtnSearchPeriod.Name.ToUpper Then
                    .ValueMember = cboPeriod.ValueMember
                    .DisplayMember = cboPeriod.DisplayMember
                    .CodeMember = "code"
                    .DataSource = CType(cboPeriod.DataSource, DataTable)

                ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper = objbtnSearchEmployee.Name.ToUpper Then
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                End If
            End With

            If objFrm.DisplayDialog Then
                If CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper = objbtnSearchPeriod.Name.ToUpper Then
                    cboPeriod.SelectedValue = objFrm.SelectedValue
                    cboPeriod.Focus()
                ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper = objbtnSearchEmployee.Name.ToUpper Then
                    cboEmployee.SelectedValue = objFrm.SelectedValue
                    cboEmployee.Focus()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpDate.Checked = False
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            chkSelectAll.Checked = False

            dgvEmpTimesheetList.DataSource = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information. Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSubmitForApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
        Try
            Dim blnFlag As Boolean = False
            objBudgetTimesheet = New clsBudgetEmp_timesheet
            Dim dtEmpTimeSheet As DataTable = New DataView(mdtEmpTimesheet, "IsChecked=1 AND IsGrp=0", "", DataViewRowState.CurrentRows).ToTable 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]


            If dtEmpTimeSheet.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dR As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0 AND issubmit_approval=1")
            If dR.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Some of the checked employee activity(s) is already sent for approval."), enMsgBoxStyle.Information)
                Exit Sub
            End If



            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

            If ConfigParameter._Object._RemarkMandatoryForTimesheetSubmission AndAlso txtSubmissionRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Submission Remark is compulsory information.Please enter submission remark."), enMsgBoxStyle.Information)
                txtSubmissionRemark.Select()
                Exit Sub
            End If

            dR = Nothing
            dR = dtEmpTimeSheet.Select("IsChecked=1 AND IsGrp=0 AND issubmit_approval = 0")
            If dR.Length > 0 Then
                For i As Integer = 0 To dR.Length - 1
                    dR(i)("submission_remark") = txtSubmissionRemark.Text.Trim()
                Next
                dtEmpTimeSheet.AcceptChanges()
            End If
            'Pinkal (28-Jul-2018) -- End


            objBudgetTimesheet._dtEmployeeActivity = dtEmpTimeSheet
            objBudgetTimesheet._IsSubmitForApproval = True
            objBudgetTimesheet._Periodunkid = CInt(cboPeriod.SelectedValue)
            objBudgetTimesheet._Userunkid = User._Object._Userunkid


            Dim objPeriod As New clscommom_period_Tran

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objPeriod._Periodunkid(ConfigParameter._Object._DatabaseUserName) = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Gajanan [23-SEP-2019] -- End


            If CBool(ConfigParameter._Object._NotAllowIncompleteTimesheet) = True Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBudgetTimesheet._FormName = mstrModuleName
                objBudgetTimesheet._LoginEmployeeunkid = 0
                objBudgetTimesheet._ClientIP = getIP()
                objBudgetTimesheet._HostName = getHostName()
                objBudgetTimesheet._FromWeb = False
                objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
                objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, CInt(objPeriod._Constant_Days), _
                '                                                       objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                '                                                        clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, CInt(objPeriod._Constant_Days), _
                                                                       objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                        clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        ConfigParameter._Object._AllowOverTimeToEmpTimesheet)
                'Pinkal (28-Jul-2018) -- End




                If objBudgetTimesheet._Message <> "" Then
                    eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot perform Submit For Approval. Reason : your timesheet is incomplete, please complete it and submit again."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBudgetTimesheet._FormName = mstrModuleName
            objBudgetTimesheet._LoginEmployeeunkid = 0
            objBudgetTimesheet._ClientIP = getIP()
            objBudgetTimesheet._HostName = getHostName()
            objBudgetTimesheet._FromWeb = False
            objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
            objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            'If objBudgetTimesheet.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
            '                             Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._IsIncludeInactiveEmp, True, , False) = False Then

            If objBudgetTimesheet.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
                                         Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._AllowOverTimeToEmpTimesheet, , False) = False Then

                'Pinkal (13-Aug-2018) -- End

                eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee Timesheet activity(s) submitted for approval successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Dim strEmployeeIDs As String = String.Join(",", dtEmpTimeSheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)

                Dim dtSelectedData As DataTable = New DataView(mdtEmpTimesheet, "IsChecked=1", "", DataViewRowState.CurrentRows).ToTable


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBudgetTimesheet._FormName = mstrModuleName
                objBudgetTimesheet._LoginEmployeeunkid = 0
                objBudgetTimesheet._ClientIP = getIP()
                objBudgetTimesheet._HostName = getHostName()
                objBudgetTimesheet._FromWeb = False
                objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
                objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objBudgetTimesheet.Send_Notification_Approver(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._IsIncludeInactiveEmp, strEmployeeIDs, CInt(cboPeriod.SelectedValue), _
                                                              -1, dtSelectedData, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, _
                                                              ConfigParameter._Object._ArutiSelfServiceURL, False)

                chkSelectAll.Checked = False

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                txtSubmissionRemark.Text = ""
                'Pinkal (28-Jul-2018) -- End

                FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSubmitForApproval_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If dgvEmpTimesheetList.RowCount <= 0 Then Exit Sub

            Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")
            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Are you sure you want to cancel this employee timesheet?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim mstrReason As String = ""
            Dim objFrm As New frmRemark
            objFrm.Text = Language.getMessage(mstrModuleName, 13, "Cancel Reason")
            objFrm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 13, "Cancel Reason")
            If objFrm.displayDialog(mstrReason, enArutiApplicatinType.Aruti_Payroll) = False Then
                Exit Sub
            End If

            Dim blnFlag As Boolean
            Dim objPeriod As New clscommom_period_Tran

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'objPeriod._Periodunkid(ConfigParameter._Object._DatabaseUserName) = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Pinkal (13-Oct-2017) -- End



            If CBool(ConfigParameter._Object._NotAllowIncompleteTimesheet) = True Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBudgetTimesheet._FormName = mstrModuleName
                objBudgetTimesheet._LoginEmployeeunkid = 0
                objBudgetTimesheet._ClientIP = getIP()
                objBudgetTimesheet._HostName = getHostName()
                objBudgetTimesheet._FromWeb = False
                objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
                objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Pinkal (02-Jul-2018) -- Start
                'Enhancement - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, CInt(objPeriod._Constant_Days), _
                '                                                          objPeriod._Start_Date.Date, objPeriod._End_Date.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, CInt(objPeriod._Constant_Days), _
                                                                        objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                          ConfigParameter._Object._AllowOverTimeToEmpTimesheet)

                'Pinkal (28-Jul-2018) -- End


                'Pinkal (02-Jul-2018) -- End

                

                If objBudgetTimesheet._Message <> "" Then
                    eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        'Pinkal (31-Oct-2017) -- Start
                        'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot perform Cancel operation. Reason : On Configuration --> Options --> Payroll Settings --> 'Don't allow incomplete timesheet to Submit For Apprval/Approve/Reject/Cancel' is alraeady set"), enMsgBoxStyle.Information)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot perform Cancel operation. Reason : your timesheet is incomplete, please complete it."), enMsgBoxStyle.Information)
                        'Pinkal (31-Oct-2017) -- End
                        Exit Sub
                    End If
                End If
            End If

            Dim strEmployeeIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False) _
            '                                               .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

            Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False AndAlso x.Field(Of Boolean)("ischecked") = True) _
                                                           .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)
            'Pinkal (13-Oct-2017) -- End

            

            objBudgetTimesheet._Userunkid = User._Object._Userunkid

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBudgetTimesheet._FormName = mstrModuleName
            objBudgetTimesheet._LoginEmployeeunkid = 0
            objBudgetTimesheet._ClientIP = getIP()
            objBudgetTimesheet._HostName = getHostName()
            objBudgetTimesheet._FromWeb = False
            objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
            objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.

            'If objBudgetTimesheet.CancelTimesheet(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
            '                                      Company._Object._Companyunkid, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._UserAccessModeSetting, _
            '                                      mstrReason, strEmpTimesheetIDs, Nothing) = False Then

            If objBudgetTimesheet.CancelTimesheet(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Userunkid, _
                                                  Company._Object._Companyunkid, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._UserAccessModeSetting, _
                                              mstrReason, strEmpTimesheetIDs, ConfigParameter._Object._AllowOverTimeToEmpTimesheet, Nothing) = False Then

                'Pinkal (28-Mar-2018) -- End



                eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Timesheet Cancelled Successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBudgetTimesheet._FormName = mstrModuleName
                objBudgetTimesheet._LoginEmployeeunkid = 0
                objBudgetTimesheet._ClientIP = getIP()
                objBudgetTimesheet._HostName = getHostName()
                objBudgetTimesheet._FromWeb = False
                objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
                objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objBudgetTimesheet.Send_Notification_Employee(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._IsIncludeInactiveEmp, strEmployeeIDs, strEmpTimesheetIDs, _
                                                              CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                              enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, ConfigParameter._Object._ArutiSelfServiceURL, _
                                                              mstrReason, , , False)
            End If

            chkSelectAll.Checked = False
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dgvEmpTimesheetList.RowCount <= 0 Then Exit Sub

            Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")
            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Are you sure you want to delete this employee timesheet?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim frm As New frmReasonSelection
            Dim mstrVoidReason As String = String.Empty
            frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
            If mstrVoidReason.Length <= 0 Then
                Exit Sub
            End If
            frm = Nothing

            For Each row As DataRow In mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")

                objBudgetTimesheet._Voiduserunkid = User._Object._Userunkid
                objBudgetTimesheet._LoginEmployeeunkid = -1
                objBudgetTimesheet._Voidreason = mstrVoidReason


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBudgetTimesheet._FormName = mstrModuleName
                objBudgetTimesheet._LoginEmployeeunkid = 0
                objBudgetTimesheet._ClientIP = getIP()
                objBudgetTimesheet._HostName = getHostName()
                objBudgetTimesheet._FromWeb = False
                objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
                objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objBudgetTimesheet.Delete(FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             CInt(row.Item("emptimesheetunkid"))) = False Then
                    eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Timesheet deleted Successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))

            Dim strEmployeeIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
            Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False) _
                                                           .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBudgetTimesheet._FormName = mstrModuleName
            objBudgetTimesheet._LoginEmployeeunkid = 0
            objBudgetTimesheet._ClientIP = getIP()
            objBudgetTimesheet._HostName = getHostName()
            objBudgetTimesheet._FromWeb = False
            objBudgetTimesheet._AuditUserId = User._Object._Userunkid
objBudgetTimesheet._CompanyUnkid = Company._Object._Companyunkid
            objBudgetTimesheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            objBudgetTimesheet.Send_Notification_Employee(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          ConfigParameter._Object._IsIncludeInactiveEmp, strEmployeeIDs, _
                                                          strEmpTimesheetIDs, CInt(cboPeriod.SelectedValue), _
                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED, _
                                                          enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, _
                                                          ConfigParameter._Object._ArutiSelfServiceURL, mstrVoidReason, , , False)

            chkSelectAll.Checked = False
            Call FillList()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (01 Apr 2017) -- End

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Private Sub dgvEmpTimesheetList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpTimesheetList.CellContentClick, _
                                                                                                                                                dgvEmpTimesheetList.CellContentDoubleClick
        Try
            If e.ColumnIndex = dgcolhSelect.Index Then
                If dgvEmpTimesheetList.IsCurrentCellDirty Then
                    dgvEmpTimesheetList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                mdtEmpTimesheet.AcceptChanges()

                Dim dgRow As DataRow() = mdtEmpTimesheet.Select("IsGrp=1 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND employeeunkid=" & _
                                                                CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value))
                'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

                Dim dRow0 As DataRow() = mdtEmpTimesheet.Select("IsChecked=0 AND IsGrp=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND employeeunkid=" & _
                                                                CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value))
                'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

                Dim dRow1 As DataRow() = mdtEmpTimesheet.Select("IsGrp=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND employeeunkid=" & _
                                                                CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value))
                'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

                If dRow0.Count = dRow1.Count Then
                    If dgRow.Length > 0 Then
                        dgRow(0).Item("IsChecked") = False
                    Else
                        dgRow(0).Item("IsChecked") = True
                    End If
                Else
                    dgRow(0).Item("IsChecked") = True
                End If
                dgRow(0).AcceptChanges()

                Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0") 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
                Dim dR As DataRow() = mdtEmpTimesheet.Select("IsGrp=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0") 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                If dRow.Length <= 0 Then
                    chkSelectAll.CheckState = CheckState.Unchecked
                ElseIf dRow.Length < dR.Length Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                ElseIf dRow.Length = dR.Length Then
                    chkSelectAll.CheckState = CheckState.Checked
                End If
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                Call SetGridColor()
            End If

            dgvEmpTimesheetList.FirstDisplayedScrollingRowIndex = e.RowIndex

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheetList_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheetList_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvEmpTimesheetList.CellPainting
        Try
            If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
                If CBool(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If e.ColumnIndex = dgvEmpTimesheetList.Columns(dgcolhSelect.Index).Index Then
                        e.PaintBackground(e.ClipBounds, True)
                        e.Handled = True
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheetList_CellPainting", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If mdtEmpTimesheet.Rows.Count > 0 Then

                'Pinkal (13-Aug-2018) -- Start
                'Enhancement - Changes For PACT  [Ref #249,252]

                Dim drRow() As DataRow = Nothing

                If ConfigParameter._Object._AllowOverTimeToEmpTimesheet Then
                    drRow = mdtEmpTimesheet.Select("isLeave=0 AND isDayOff=0")
                Else
                    drRow = mdtEmpTimesheet.Select("isholiday=0 AND isLeave=0 AND isDayOff=0")
                End If

                For Each dRow As DataRow In drRow
                    dRow.Item("IsChecked") = chkSelectAll.Checked
                Next

                'Pinkal (13-Aug-2018) -- End

            End If


            mdtEmpTimesheet.AcceptChanges()
            Call SetGridColor()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbEmployeeTimesheet.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeTimesheet.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSubmitForApproval.GradientBackColor = GUI._ButttonBackColor
            Me.btnSubmitForApproval.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSubmitForApproval.Text = Language._Object.getCaption(Me.btnSubmitForApproval.Name, Me.btnSubmitForApproval.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeTimesheet.Text = Language._Object.getCaption(Me.gbEmployeeTimesheet.Name, Me.gbEmployeeTimesheet.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.LblSubmissionRemark.Text = Language._Object.getCaption(Me.LblSubmissionRemark.Name, Me.LblSubmissionRemark.Text)
            Me.dgcolhSelect.HeaderText = Language._Object.getCaption(Me.dgcolhSelect.Name, Me.dgcolhSelect.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhDate.HeaderText = Language._Object.getCaption(Me.dgcolhDate.Name, Me.dgcolhDate.HeaderText)
            Me.dgcolhDonor.HeaderText = Language._Object.getCaption(Me.dgcolhDonor.Name, Me.dgcolhDonor.HeaderText)
            Me.dgcolhProject.HeaderText = Language._Object.getCaption(Me.dgcolhProject.Name, Me.dgcolhProject.HeaderText)
            Me.dgcolhActivity.HeaderText = Language._Object.getCaption(Me.dgcolhActivity.Name, Me.dgcolhActivity.HeaderText)
            Me.dgcolhCostCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenter.Name, Me.dgcolhCostCenter.HeaderText)
            Me.dgcolhHours.HeaderText = Language._Object.getCaption(Me.dgcolhHours.Name, Me.dgcolhHours.HeaderText)
            Me.dgcolhApprovedActHrs.HeaderText = Language._Object.getCaption(Me.dgcolhApprovedActHrs.Name, Me.dgcolhApprovedActHrs.HeaderText)
            Me.dgcolhDescription.HeaderText = Language._Object.getCaption(Me.dgcolhDescription.Name, Me.dgcolhDescription.HeaderText)
			Me.dgcolhSubmissionRemark.HeaderText = Language._Object.getCaption(Me.dgcolhSubmissionRemark.Name, Me.dgcolhSubmissionRemark.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
            Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
            Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
            Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is compulsory information. Please Select Period.")
            Language.setMessage(mstrModuleName, 2, "Please tick atleast one record for further process.")
            Language.setMessage(mstrModuleName, 3, "Some of the checked employee activity(s) is already sent for approval.")
            Language.setMessage(mstrModuleName, 5, "Employee Timesheet activity(s) submitted for approval successfully.")
            Language.setMessage(mstrModuleName, 6, "Pending Submit For Approval")
            Language.setMessage(mstrModuleName, 7, "Completed Submit For Approval")
            Language.setMessage(mstrModuleName, 8, "Global Cancel Timesheet")
            Language.setMessage(mstrModuleName, 9, "Global Delete Timesheet")
            Language.setMessage(mstrModuleName, 10, "Are you sure you want to delete this employee timesheet?")
            Language.setMessage(mstrModuleName, 11, "Timesheet deleted Successfully.")
            Language.setMessage(mstrModuleName, 12, "Are you sure you want to cancel this employee timesheet?")
            Language.setMessage(mstrModuleName, 13, "Cancel Reason")
            Language.setMessage(mstrModuleName, 14, "Timesheet Cancelled Successfully.")
            Language.setMessage(mstrModuleName, 16, "Sorry, you cannot perform Submit For Approval. Reason : your timesheet is incomplete, please complete it and submit again.")
            Language.setMessage(mstrModuleName, 17, "Sorry, you cannot perform Cancel operation. Reason : your timesheet is incomplete, please complete it.")
			Language.setMessage(mstrModuleName, 18, "Submission Remark is compulsory information.Please enter submission remark.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class