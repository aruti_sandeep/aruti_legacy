﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSubmitForApproval
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSubmitForApproval))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSubmitForApproval = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmployeeTimesheet = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtSubmissionRemark = New System.Windows.Forms.TextBox
        Me.LblSubmissionRemark = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblDate = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.dgvEmpTimesheetList = New System.Windows.Forms.DataGridView
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDonor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostCenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprovedActHrs = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSubmissionRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpTimesheetID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFundSourceID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProjectID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActivityID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsHoliday = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsDayOFF = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsLeave = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsSubmitForApproval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApprovedActivityHoursInMin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhADate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.gbEmployeeTimesheet.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEmpTimesheetList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Controls.Add(Me.btnSubmitForApproval)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 439)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(802, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(598, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(93, 30)
        Me.btnDelete.TabIndex = 4
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(598, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(93, 30)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSubmitForApproval
        '
        Me.btnSubmitForApproval.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSubmitForApproval.BackColor = System.Drawing.Color.White
        Me.btnSubmitForApproval.BackgroundImage = CType(resources.GetObject("btnSubmitForApproval.BackgroundImage"), System.Drawing.Image)
        Me.btnSubmitForApproval.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSubmitForApproval.BorderColor = System.Drawing.Color.Empty
        Me.btnSubmitForApproval.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSubmitForApproval.FlatAppearance.BorderSize = 0
        Me.btnSubmitForApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmitForApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmitForApproval.ForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSubmitForApproval.GradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForApproval.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.Location = New System.Drawing.Point(555, 13)
        Me.btnSubmitForApproval.Name = "btnSubmitForApproval"
        Me.btnSubmitForApproval.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForApproval.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForApproval.Size = New System.Drawing.Size(136, 30)
        Me.btnSubmitForApproval.TabIndex = 1
        Me.btnSubmitForApproval.Text = "&Submit For Approval"
        Me.btnSubmitForApproval.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(697, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbEmployeeTimesheet
        '
        Me.gbEmployeeTimesheet.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeTimesheet.Checked = False
        Me.gbEmployeeTimesheet.CollapseAllExceptThis = False
        Me.gbEmployeeTimesheet.CollapsedHoverImage = Nothing
        Me.gbEmployeeTimesheet.CollapsedNormalImage = Nothing
        Me.gbEmployeeTimesheet.CollapsedPressedImage = Nothing
        Me.gbEmployeeTimesheet.CollapseOnLoad = False
        Me.gbEmployeeTimesheet.Controls.Add(Me.txtSubmissionRemark)
        Me.gbEmployeeTimesheet.Controls.Add(Me.LblSubmissionRemark)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.lblStatus)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbEmployeeTimesheet.Controls.Add(Me.dtpDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.lblDate)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeTimesheet.Controls.Add(Me.cboPeriod)
        Me.gbEmployeeTimesheet.Controls.Add(Me.lblPeriod)
        Me.gbEmployeeTimesheet.ExpandedHoverImage = Nothing
        Me.gbEmployeeTimesheet.ExpandedNormalImage = Nothing
        Me.gbEmployeeTimesheet.ExpandedPressedImage = Nothing
        Me.gbEmployeeTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeTimesheet.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeTimesheet.HeaderHeight = 25
        Me.gbEmployeeTimesheet.HeaderMessage = ""
        Me.gbEmployeeTimesheet.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeTimesheet.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeTimesheet.HeightOnCollapse = 0
        Me.gbEmployeeTimesheet.LeftTextSpace = 0
        Me.gbEmployeeTimesheet.Location = New System.Drawing.Point(12, 12)
        Me.gbEmployeeTimesheet.Name = "gbEmployeeTimesheet"
        Me.gbEmployeeTimesheet.OpenHeight = 300
        Me.gbEmployeeTimesheet.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeTimesheet.ShowBorder = True
        Me.gbEmployeeTimesheet.ShowCheckBox = False
        Me.gbEmployeeTimesheet.ShowCollapseButton = False
        Me.gbEmployeeTimesheet.ShowDefaultBorderColor = True
        Me.gbEmployeeTimesheet.ShowDownButton = False
        Me.gbEmployeeTimesheet.ShowHeader = True
        Me.gbEmployeeTimesheet.Size = New System.Drawing.Size(778, 112)
        Me.gbEmployeeTimesheet.TabIndex = 3
        Me.gbEmployeeTimesheet.Temp = 0
        Me.gbEmployeeTimesheet.Text = "Filter Information"
        Me.gbEmployeeTimesheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSubmissionRemark
        '
        Me.txtSubmissionRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubmissionRemark.Location = New System.Drawing.Point(78, 58)
        Me.txtSubmissionRemark.Multiline = True
        Me.txtSubmissionRemark.Name = "txtSubmissionRemark"
        Me.txtSubmissionRemark.Size = New System.Drawing.Size(333, 47)
        Me.txtSubmissionRemark.TabIndex = 77
        '
        'LblSubmissionRemark
        '
        Me.LblSubmissionRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSubmissionRemark.Location = New System.Drawing.Point(4, 61)
        Me.LblSubmissionRemark.Name = "LblSubmissionRemark"
        Me.LblSubmissionRemark.Size = New System.Drawing.Size(67, 16)
        Me.LblSubmissionRemark.TabIndex = 76
        Me.LblSubmissionRemark.Text = "Remark"
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 400
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(78, 59)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(152, 21)
        Me.cboStatus.TabIndex = 74
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(4, 61)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(67, 16)
        Me.lblStatus.TabIndex = 73
        Me.lblStatus.Text = "Status"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(751, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 71
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(728, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 70
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchPeriod.Image = CType(resources.GetObject("objbtnSearchPeriod.Image"), System.Drawing.Image)
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(236, 33)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(18, 19)
        Me.objbtnSearchPeriod.TabIndex = 2
        '
        'dtpDate
        '
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(307, 32)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.ShowCheckBox = True
        Me.dtpDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate.TabIndex = 4
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(746, 32)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 7
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(261, 34)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(40, 16)
        Me.lblDate.TabIndex = 3
        Me.lblDate.Text = "Date"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(500, 32)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(240, 21)
        Me.cboEmployee.TabIndex = 6
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(427, 34)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(67, 16)
        Me.lblEmployee.TabIndex = 5
        Me.lblEmployee.Text = "Employee"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 400
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(78, 32)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(152, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(4, 34)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(67, 16)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Period"
        '
        'dgvEmpTimesheetList
        '
        Me.dgvEmpTimesheetList.AllowUserToAddRows = False
        Me.dgvEmpTimesheetList.AllowUserToDeleteRows = False
        Me.dgvEmpTimesheetList.AllowUserToResizeRows = False
        Me.dgvEmpTimesheetList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmpTimesheetList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmpTimesheetList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhSelect, Me.dgcolhEmployee, Me.dgcolhDate, Me.dgcolhDonor, Me.dgcolhProject, Me.dgcolhActivity, Me.dgcolhCostCenter, Me.dgcolhHours, Me.dgcolhApprovedActHrs, Me.dgcolhDescription, Me.dgcolhSubmissionRemark, Me.dgcolhStatus, Me.objdgcolhEmpTimesheetID, Me.objdgcolhPeriodID, Me.objdgcolhEmployeeID, Me.objdgcolhFundSourceID, Me.objdgcolhProjectID, Me.objdgcolhActivityID, Me.objdgcolhStatusId, Me.objdgcolhIsGrp, Me.objdgcolhIsHoliday, Me.objdgcolhIsDayOFF, Me.objdgcolhIsLeave, Me.objdgcolhIsSubmitForApproval, Me.objdgcolhApprovedActivityHoursInMin, Me.objdgcolhADate})
        Me.dgvEmpTimesheetList.Location = New System.Drawing.Point(12, 128)
        Me.dgvEmpTimesheetList.Name = "dgvEmpTimesheetList"
        Me.dgvEmpTimesheetList.RowHeadersVisible = False
        Me.dgvEmpTimesheetList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmpTimesheetList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpTimesheetList.Size = New System.Drawing.Size(778, 305)
        Me.dgvEmpTimesheetList.TabIndex = 5
        Me.dgvEmpTimesheetList.TabStop = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(19, 134)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 6
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 225
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Activity Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Donor/Grant"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Project Code"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 150
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Activity Code"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Cost Center"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Activity Hours"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Approved Activity Hrs"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 130
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 250
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Submission Remark"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 150
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "EmpTimesheetID"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "EmployeeID"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "FundSourceID"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "ProjectID"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "ActivityID"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Statusunkid"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "IsGrp"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "IsHoliday"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "IsDayOFF"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "IsLeave"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "IsSubmitForApproval"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "ApprovedActivityHoursInMin"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "ADate"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'dgcolhSelect
        '
        Me.dgcolhSelect.Frozen = True
        Me.dgcolhSelect.HeaderText = ""
        Me.dgcolhSelect.Name = "dgcolhSelect"
        Me.dgcolhSelect.Width = 25
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.Frozen = True
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 225
        '
        'dgcolhDate
        '
        Me.dgcolhDate.HeaderText = "Activity Date"
        Me.dgcolhDate.Name = "dgcolhDate"
        Me.dgcolhDate.ReadOnly = True
        Me.dgcolhDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDonor
        '
        Me.dgcolhDonor.HeaderText = "Donor/Grant"
        Me.dgcolhDonor.Name = "dgcolhDonor"
        Me.dgcolhDonor.ReadOnly = True
        Me.dgcolhDonor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDonor.Width = 150
        '
        'dgcolhProject
        '
        Me.dgcolhProject.HeaderText = "Project Code"
        Me.dgcolhProject.Name = "dgcolhProject"
        Me.dgcolhProject.ReadOnly = True
        Me.dgcolhProject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProject.Width = 150
        '
        'dgcolhActivity
        '
        Me.dgcolhActivity.HeaderText = "Activity Code"
        Me.dgcolhActivity.Name = "dgcolhActivity"
        Me.dgcolhActivity.ReadOnly = True
        Me.dgcolhActivity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhActivity.Width = 150
        '
        'dgcolhCostCenter
        '
        Me.dgcolhCostCenter.HeaderText = "Cost Center"
        Me.dgcolhCostCenter.Name = "dgcolhCostCenter"
        Me.dgcolhCostCenter.ReadOnly = True
        Me.dgcolhCostCenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCostCenter.Visible = False
        Me.dgcolhCostCenter.Width = 150
        '
        'dgcolhHours
        '
        Me.dgcolhHours.HeaderText = "Activity Hours"
        Me.dgcolhHours.Name = "dgcolhHours"
        Me.dgcolhHours.ReadOnly = True
        Me.dgcolhHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhApprovedActHrs
        '
        Me.dgcolhApprovedActHrs.HeaderText = "Approved Activity Hrs"
        Me.dgcolhApprovedActHrs.Name = "dgcolhApprovedActHrs"
        Me.dgcolhApprovedActHrs.ReadOnly = True
        Me.dgcolhApprovedActHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprovedActHrs.Visible = False
        Me.dgcolhApprovedActHrs.Width = 130
        '
        'dgcolhDescription
        '
        Me.dgcolhDescription.HeaderText = "Description"
        Me.dgcolhDescription.Name = "dgcolhDescription"
        Me.dgcolhDescription.ReadOnly = True
        Me.dgcolhDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDescription.Width = 250
        '
        'dgcolhSubmissionRemark
        '
        Me.dgcolhSubmissionRemark.HeaderText = "Submission Remark"
        Me.dgcolhSubmissionRemark.Name = "dgcolhSubmissionRemark"
        Me.dgcolhSubmissionRemark.ReadOnly = True
        Me.dgcolhSubmissionRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSubmissionRemark.Width = 150
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpTimesheetID
        '
        Me.objdgcolhEmpTimesheetID.HeaderText = "EmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.Name = "objdgcolhEmpTimesheetID"
        Me.objdgcolhEmpTimesheetID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpTimesheetID.Visible = False
        '
        'objdgcolhPeriodID
        '
        Me.objdgcolhPeriodID.HeaderText = "PeriodID"
        Me.objdgcolhPeriodID.Name = "objdgcolhPeriodID"
        Me.objdgcolhPeriodID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhPeriodID.Visible = False
        '
        'objdgcolhEmployeeID
        '
        Me.objdgcolhEmployeeID.HeaderText = "EmployeeID"
        Me.objdgcolhEmployeeID.Name = "objdgcolhEmployeeID"
        Me.objdgcolhEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeID.Visible = False
        '
        'objdgcolhFundSourceID
        '
        Me.objdgcolhFundSourceID.HeaderText = "FundSourceID"
        Me.objdgcolhFundSourceID.Name = "objdgcolhFundSourceID"
        Me.objdgcolhFundSourceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhFundSourceID.Visible = False
        '
        'objdgcolhProjectID
        '
        Me.objdgcolhProjectID.HeaderText = "ProjectID"
        Me.objdgcolhProjectID.Name = "objdgcolhProjectID"
        Me.objdgcolhProjectID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhProjectID.Visible = False
        '
        'objdgcolhActivityID
        '
        Me.objdgcolhActivityID.HeaderText = "ActivityID"
        Me.objdgcolhActivityID.Name = "objdgcolhActivityID"
        Me.objdgcolhActivityID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhActivityID.Visible = False
        '
        'objdgcolhStatusId
        '
        Me.objdgcolhStatusId.HeaderText = "Statusunkid"
        Me.objdgcolhStatusId.Name = "objdgcolhStatusId"
        Me.objdgcolhStatusId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhStatusId.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "IsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhIsHoliday
        '
        Me.objdgcolhIsHoliday.HeaderText = "IsHoliday"
        Me.objdgcolhIsHoliday.Name = "objdgcolhIsHoliday"
        Me.objdgcolhIsHoliday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsHoliday.Visible = False
        '
        'objdgcolhIsDayOFF
        '
        Me.objdgcolhIsDayOFF.HeaderText = "IsDayOFF"
        Me.objdgcolhIsDayOFF.Name = "objdgcolhIsDayOFF"
        Me.objdgcolhIsDayOFF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsDayOFF.Visible = False
        '
        'objdgcolhIsLeave
        '
        Me.objdgcolhIsLeave.HeaderText = "IsLeave"
        Me.objdgcolhIsLeave.Name = "objdgcolhIsLeave"
        Me.objdgcolhIsLeave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsLeave.Visible = False
        '
        'objdgcolhIsSubmitForApproval
        '
        Me.objdgcolhIsSubmitForApproval.HeaderText = "IsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.Name = "objdgcolhIsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.Visible = False
        '
        'objdgcolhApprovedActivityHoursInMin
        '
        Me.objdgcolhApprovedActivityHoursInMin.HeaderText = "ApprovedActivityHoursInMin"
        Me.objdgcolhApprovedActivityHoursInMin.Name = "objdgcolhApprovedActivityHoursInMin"
        Me.objdgcolhApprovedActivityHoursInMin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApprovedActivityHoursInMin.Visible = False
        '
        'objdgcolhADate
        '
        Me.objdgcolhADate.HeaderText = "ADate"
        Me.objdgcolhADate.Name = "objdgcolhADate"
        Me.objdgcolhADate.ReadOnly = True
        Me.objdgcolhADate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhADate.Visible = False
        '
        'frmSubmitForApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(802, 494)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.dgvEmpTimesheetList)
        Me.Controls.Add(Me.gbEmployeeTimesheet)
        Me.Controls.Add(Me.objFooter)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSubmitForApproval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Submit For Approval"
        Me.objFooter.ResumeLayout(False)
        Me.gbEmployeeTimesheet.ResumeLayout(False)
        Me.gbEmployeeTimesheet.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEmpTimesheetList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSubmitForApproval As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeTimesheet As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dgvEmpTimesheetList As System.Windows.Forms.DataGridView
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents txtSubmissionRemark As System.Windows.Forms.TextBox
    Friend WithEvents LblSubmissionRemark As System.Windows.Forms.Label
    Friend WithEvents dgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDonor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprovedActHrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSubmissionRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpTimesheetID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFundSourceID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProjectID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActivityID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsHoliday As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsDayOFF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsLeave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsSubmitForApproval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApprovedActivityHoursInMin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhADate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
