﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExchangeRateList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExchangeRateList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvExchangeRate = New eZee.Common.eZeeListView(Me.components)
        Me.colhCurrencyCountry = New System.Windows.Forms.ColumnHeader
        Me.colhCurrency = New System.Windows.Forms.ColumnHeader
        Me.colhCurrencySign = New System.Windows.Forms.ColumnHeader
        Me.colhExchangeRate = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnMakeBaseCurrency = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cpOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.objtsmiNew = New System.Windows.Forms.ToolStripMenuItem
        Me.objtsmiEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.objtsmiDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.colhExchangeDate = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cpOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvExchangeRate)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(682, 391)
        Me.pnlMainInfo.TabIndex = 4
        '
        'lvExchangeRate
        '
        Me.lvExchangeRate.BackColorOnChecked = True
        Me.lvExchangeRate.ColumnHeaders = Nothing
        Me.lvExchangeRate.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCurrencyCountry, Me.colhCurrency, Me.colhCurrencySign, Me.colhExchangeRate, Me.colhExchangeDate})
        Me.lvExchangeRate.CompulsoryColumns = ""
        Me.lvExchangeRate.FullRowSelect = True
        Me.lvExchangeRate.GridLines = True
        Me.lvExchangeRate.GroupingColumn = Nothing
        Me.lvExchangeRate.HideSelection = False
        Me.lvExchangeRate.Location = New System.Drawing.Point(13, 67)
        Me.lvExchangeRate.MinColumnWidth = 50
        Me.lvExchangeRate.MultiSelect = False
        Me.lvExchangeRate.Name = "lvExchangeRate"
        Me.lvExchangeRate.OptionalColumns = ""
        Me.lvExchangeRate.ShowMoreItem = False
        Me.lvExchangeRate.ShowSaveItem = False
        Me.lvExchangeRate.ShowSelectAll = True
        Me.lvExchangeRate.ShowSizeAllColumnsToFit = True
        Me.lvExchangeRate.Size = New System.Drawing.Size(657, 263)
        Me.lvExchangeRate.Sortable = True
        Me.lvExchangeRate.TabIndex = 6
        Me.lvExchangeRate.UseCompatibleStateImageBehavior = False
        Me.lvExchangeRate.View = System.Windows.Forms.View.Details
        '
        'colhCurrencyCountry
        '
        Me.colhCurrencyCountry.Tag = "colhCurrencyCountry"
        Me.colhCurrencyCountry.Text = "Country"
        Me.colhCurrencyCountry.Width = 140
        '
        'colhCurrency
        '
        Me.colhCurrency.Tag = "colhCurrency"
        Me.colhCurrency.Text = "Currency"
        Me.colhCurrency.Width = 130
        '
        'colhCurrencySign
        '
        Me.colhCurrencySign.Tag = "colhCurrencySign"
        Me.colhCurrencySign.Text = "Sign"
        Me.colhCurrencySign.Width = 100
        '
        'colhExchangeRate
        '
        Me.colhExchangeRate.Tag = "colhExchangeRate"
        Me.colhExchangeRate.Text = "Exchange Rate"
        Me.colhExchangeRate.Width = 190
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(682, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Exchange Rate List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnMakeBaseCurrency)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 336)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(682, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnMakeBaseCurrency
        '
        Me.btnMakeBaseCurrency.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMakeBaseCurrency.BackColor = System.Drawing.Color.White
        Me.btnMakeBaseCurrency.BackgroundImage = CType(resources.GetObject("btnMakeBaseCurrency.BackgroundImage"), System.Drawing.Image)
        Me.btnMakeBaseCurrency.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMakeBaseCurrency.BorderColor = System.Drawing.Color.Empty
        Me.btnMakeBaseCurrency.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMakeBaseCurrency.FlatAppearance.BorderSize = 0
        Me.btnMakeBaseCurrency.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMakeBaseCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMakeBaseCurrency.ForeColor = System.Drawing.Color.Black
        Me.btnMakeBaseCurrency.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMakeBaseCurrency.GradientForeColor = System.Drawing.Color.Black
        Me.btnMakeBaseCurrency.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMakeBaseCurrency.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMakeBaseCurrency.Location = New System.Drawing.Point(13, 13)
        Me.btnMakeBaseCurrency.Name = "btnMakeBaseCurrency"
        Me.btnMakeBaseCurrency.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMakeBaseCurrency.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMakeBaseCurrency.Size = New System.Drawing.Size(140, 30)
        Me.btnMakeBaseCurrency.TabIndex = 123
        Me.btnMakeBaseCurrency.Text = "&Make Base Currency"
        Me.btnMakeBaseCurrency.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(470, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(367, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(264, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(573, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cpOperation
        '
        Me.cpOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtsmiNew, Me.objtsmiEdit, Me.objtsmiDelete})
        Me.cpOperation.Name = "ContextMenuStrip1"
        Me.cpOperation.Size = New System.Drawing.Size(135, 70)
        '
        'objtsmiNew
        '
        Me.objtsmiNew.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.objtsmiNew.Name = "objtsmiNew"
        Me.objtsmiNew.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.objtsmiNew.Size = New System.Drawing.Size(134, 22)
        Me.objtsmiNew.Text = "&New"
        '
        'objtsmiEdit
        '
        Me.objtsmiEdit.Name = "objtsmiEdit"
        Me.objtsmiEdit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.objtsmiEdit.Size = New System.Drawing.Size(134, 22)
        Me.objtsmiEdit.Text = "&Edit"
        '
        'objtsmiDelete
        '
        Me.objtsmiDelete.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.objtsmiDelete.Name = "objtsmiDelete"
        Me.objtsmiDelete.ShortcutKeyDisplayString = ""
        Me.objtsmiDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.objtsmiDelete.Size = New System.Drawing.Size(134, 22)
        Me.objtsmiDelete.Text = "&Delete"
        '
        'colhExchangeDate
        '
        Me.colhExchangeDate.Tag = "colhExchangeDate"
        Me.colhExchangeDate.Text = "Date"
        Me.colhExchangeDate.Width = 90
        '
        'frmExchangeRateList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 391)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExchangeRateList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Exchange Rate List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.cpOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents lvExchangeRate As eZee.Common.eZeeListView
    Friend WithEvents colhCurrency As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCurrencySign As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCurrencyCountry As System.Windows.Forms.ColumnHeader
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents colhExchangeRate As System.Windows.Forms.ColumnHeader
    Friend WithEvents cpOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents objtsmiNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objtsmiEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objtsmiDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnMakeBaseCurrency As eZee.Common.eZeeLightButton
    Friend WithEvents colhExchangeDate As System.Windows.Forms.ColumnHeader
End Class
