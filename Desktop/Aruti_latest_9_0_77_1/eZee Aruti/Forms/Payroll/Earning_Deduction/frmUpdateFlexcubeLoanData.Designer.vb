﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUpdateFlexcubeLoanData
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUpdateFlexcubeLoanData))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboBankBranch = New System.Windows.Forms.ComboBox
        Me.lblBankBranch = New System.Windows.Forms.Label
        Me.cboBankGroup = New System.Windows.Forms.ComboBox
        Me.lblBankGroup = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.picExpand = New System.Windows.Forms.PictureBox
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgEcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhBankGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhBankBranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAccountNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgColhEmpBankTranUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkISelectAll = New System.Windows.Forms.CheckBox
        Me.dgvImportInfo = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemarks = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnGetData = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblLoanDataCount = New System.Windows.Forms.Label
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuShowSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuShowUnSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.btnHeadOperations = New eZee.Common.eZeeSplitButton
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeList.SuspendLayout()
        CType(Me.picExpand, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefFormFooter.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboBankBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblBankBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBankGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblBankGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(860, 65)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownHeight = 150
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.IntegralHeight = False
        Me.cboEmployee.Location = New System.Drawing.Point(273, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(131, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(191, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(76, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'cboBankBranch
        '
        Me.cboBankBranch.DropDownHeight = 150
        Me.cboBankBranch.DropDownWidth = 300
        Me.cboBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankBranch.FormattingEnabled = True
        Me.cboBankBranch.IntegralHeight = False
        Me.cboBankBranch.Location = New System.Drawing.Point(721, 34)
        Me.cboBankBranch.Name = "cboBankBranch"
        Me.cboBankBranch.Size = New System.Drawing.Size(131, 21)
        Me.cboBankBranch.TabIndex = 7
        '
        'lblBankBranch
        '
        Me.lblBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankBranch.Location = New System.Drawing.Point(639, 37)
        Me.lblBankBranch.Name = "lblBankBranch"
        Me.lblBankBranch.Size = New System.Drawing.Size(76, 15)
        Me.lblBankBranch.TabIndex = 6
        Me.lblBankBranch.Text = "Bank Branch"
        '
        'cboBankGroup
        '
        Me.cboBankGroup.DropDownHeight = 150
        Me.cboBankGroup.DropDownWidth = 300
        Me.cboBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankGroup.FormattingEnabled = True
        Me.cboBankGroup.IntegralHeight = False
        Me.cboBankGroup.Location = New System.Drawing.Point(497, 34)
        Me.cboBankGroup.Name = "cboBankGroup"
        Me.cboBankGroup.Size = New System.Drawing.Size(131, 21)
        Me.cboBankGroup.TabIndex = 5
        '
        'lblBankGroup
        '
        Me.lblBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankGroup.Location = New System.Drawing.Point(415, 37)
        Me.lblBankGroup.Name = "lblBankGroup"
        Me.lblBankGroup.Size = New System.Drawing.Size(76, 15)
        Me.lblBankGroup.TabIndex = 4
        Me.lblBankGroup.Text = "Bank Group"
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(689, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(106, 13)
        Me.lnkAllocation.TabIndex = 8
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Advance Filter"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(833, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(810, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownWidth = 150
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(67, 34)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(118, 21)
        Me.cboPayPeriod.TabIndex = 1
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(53, 16)
        Me.lblPayPeriod.TabIndex = 0
        Me.lblPayPeriod.Text = "Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.picExpand)
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 83)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(241, 478)
        Me.gbEmployeeList.TabIndex = 2
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'picExpand
        '
        Me.picExpand.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picExpand.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picExpand.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.picExpand.InitialImage = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.picExpand.Location = New System.Drawing.Point(220, 5)
        Me.picExpand.Name = "picExpand"
        Me.picExpand.Size = New System.Drawing.Size(16, 16)
        Me.picExpand.TabIndex = 164
        Me.picExpand.TabStop = False
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(115, 5)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblEmpCount.TabIndex = 0
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgEmployee)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchEmp)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(238, 449)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 2
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgEcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmployeeCode, Me.dgColhEmployee, Me.dgColhBankGroup, Me.dgColhBankBranch, Me.dgColhAccountNo, Me.objdgColhEmpBankTranUnkId})
        Me.dgEmployee.Location = New System.Drawing.Point(1, 28)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(234, 418)
        Me.dgEmployee.TabIndex = 0
        '
        'objdgEcolhCheck
        '
        Me.objdgEcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgEcolhCheck.HeaderText = ""
        Me.objdgEcolhCheck.Name = "objdgEcolhCheck"
        Me.objdgEcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgEcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmployeeCode
        '
        Me.dgColhEmployeeCode.HeaderText = "Emp. Code"
        Me.dgColhEmployeeCode.Name = "dgColhEmployeeCode"
        Me.dgColhEmployeeCode.ReadOnly = True
        Me.dgColhEmployeeCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmployeeCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        Me.dgColhEmployee.Width = 150
        '
        'dgColhBankGroup
        '
        Me.dgColhBankGroup.HeaderText = "Bank Group"
        Me.dgColhBankGroup.Name = "dgColhBankGroup"
        Me.dgColhBankGroup.ReadOnly = True
        '
        'dgColhBankBranch
        '
        Me.dgColhBankBranch.HeaderText = "Bank Branch"
        Me.dgColhBankBranch.Name = "dgColhBankBranch"
        Me.dgColhBankBranch.ReadOnly = True
        '
        'dgColhAccountNo
        '
        Me.dgColhAccountNo.HeaderText = "Account No."
        Me.dgColhAccountNo.Name = "dgColhAccountNo"
        Me.dgColhAccountNo.ReadOnly = True
        '
        'objdgColhEmpBankTranUnkId
        '
        Me.objdgColhEmpBankTranUnkId.HeaderText = "EmpBankTranUnkId"
        Me.objdgColhEmpBankTranUnkId.Name = "objdgColhEmpBankTranUnkId"
        Me.objdgColhEmpBankTranUnkId.ReadOnly = True
        Me.objdgColhEmpBankTranUnkId.Visible = False
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(234, 21)
        Me.txtSearchEmp.TabIndex = 0
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnHeadOperations)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Controls.Add(Me.btnImport)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 567)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(884, 55)
        Me.objefFormFooter.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(775, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(672, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkISelectAll)
        Me.Panel1.Controls.Add(Me.dgvImportInfo)
        Me.Panel1.Location = New System.Drawing.Point(259, 142)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(613, 419)
        Me.Panel1.TabIndex = 25
        '
        'objchkISelectAll
        '
        Me.objchkISelectAll.AutoSize = True
        Me.objchkISelectAll.Location = New System.Drawing.Point(8, 5)
        Me.objchkISelectAll.Name = "objchkISelectAll"
        Me.objchkISelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkISelectAll.TabIndex = 0
        Me.objchkISelectAll.UseVisualStyleBackColor = True
        '
        'dgvImportInfo
        '
        Me.dgvImportInfo.AllowUserToAddRows = False
        Me.dgvImportInfo.AllowUserToDeleteRows = False
        Me.dgvImportInfo.AllowUserToResizeRows = False
        Me.dgvImportInfo.BackgroundColor = System.Drawing.Color.White
        Me.dgvImportInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvImportInfo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhEmpCode, Me.dgcolhEmpName, Me.dgcolhTranHead, Me.dgcolhAmount, Me.dgcolhRemarks})
        Me.dgvImportInfo.Location = New System.Drawing.Point(0, 0)
        Me.dgvImportInfo.MultiSelect = False
        Me.dgvImportInfo.Name = "dgvImportInfo"
        Me.dgvImportInfo.RowHeadersVisible = False
        Me.dgvImportInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvImportInfo.Size = New System.Drawing.Size(613, 419)
        Me.dgvImportInfo.TabIndex = 1
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhEmpCode
        '
        Me.dgcolhEmpCode.HeaderText = "Emp Code"
        Me.dgcolhEmpCode.Name = "dgcolhEmpCode"
        Me.dgcolhEmpCode.ReadOnly = True
        Me.dgcolhEmpCode.Width = 70
        '
        'dgcolhEmpName
        '
        Me.dgcolhEmpName.HeaderText = "Emp. Name"
        Me.dgcolhEmpName.Name = "dgcolhEmpName"
        Me.dgcolhEmpName.ReadOnly = True
        Me.dgcolhEmpName.Width = 150
        '
        'dgcolhTranHead
        '
        Me.dgcolhTranHead.HeaderText = "Trans. Head Name"
        Me.dgcolhTranHead.Name = "dgcolhTranHead"
        Me.dgcolhTranHead.ReadOnly = True
        Me.dgcolhTranHead.Width = 120
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.Width = 120
        '
        'dgcolhRemarks
        '
        Me.dgcolhRemarks.HeaderText = "Remarks"
        Me.dgcolhRemarks.Name = "dgcolhRemarks"
        Me.dgcolhRemarks.ReadOnly = True
        Me.dgcolhRemarks.Width = 120
        '
        'btnGetData
        '
        Me.btnGetData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGetData.BackColor = System.Drawing.Color.White
        Me.btnGetData.BackgroundImage = CType(resources.GetObject("btnGetData.BackgroundImage"), System.Drawing.Image)
        Me.btnGetData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGetData.BorderColor = System.Drawing.Color.Empty
        Me.btnGetData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnGetData.FlatAppearance.BorderSize = 0
        Me.btnGetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGetData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGetData.ForeColor = System.Drawing.Color.Black
        Me.btnGetData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGetData.GradientForeColor = System.Drawing.Color.Black
        Me.btnGetData.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGetData.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGetData.Location = New System.Drawing.Point(469, 101)
        Me.btnGetData.Name = "btnGetData"
        Me.btnGetData.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGetData.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGetData.Size = New System.Drawing.Size(159, 30)
        Me.btnGetData.TabIndex = 4
        Me.btnGetData.Text = "&Get Flexcube Loan Data"
        Me.btnGetData.UseVisualStyleBackColor = False
        '
        'objlblLoanDataCount
        '
        Me.objlblLoanDataCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblLoanDataCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblLoanDataCount.Location = New System.Drawing.Point(306, 108)
        Me.objlblLoanDataCount.Name = "objlblLoanDataCount"
        Me.objlblLoanDataCount.Size = New System.Drawing.Size(110, 16)
        Me.objlblLoanDataCount.TabIndex = 5
        Me.objlblLoanDataCount.Text = "( 0 )"
        Me.objlblLoanDataCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuShowSuccessful, Me.mnuShowUnSuccessful})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(203, 70)
        '
        'mnuShowSuccessful
        '
        Me.mnuShowSuccessful.Name = "mnuShowSuccessful"
        Me.mnuShowSuccessful.Size = New System.Drawing.Size(227, 22)
        Me.mnuShowSuccessful.Text = "Show Successful Data"
        '
        'mnuShowUnSuccessful
        '
        Me.mnuShowUnSuccessful.Name = "mnuShowUnSuccessful"
        Me.mnuShowUnSuccessful.Size = New System.Drawing.Size(227, 22)
        Me.mnuShowUnSuccessful.Text = "Show Unsuccessful Data"
        '
        'btnHeadOperations
        '
        Me.btnHeadOperations.BorderColor = System.Drawing.Color.Black
        Me.btnHeadOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHeadOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnHeadOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnHeadOperations.Location = New System.Drawing.Point(14, 13)
        Me.btnHeadOperations.Name = "btnHeadOperations"
        Me.btnHeadOperations.ShowDefaultBorderColor = True
        Me.btnHeadOperations.Size = New System.Drawing.Size(110, 30)
        Me.btnHeadOperations.SplitButtonMenu = Me.cmnuOperations
        Me.btnHeadOperations.TabIndex = 101
        Me.btnHeadOperations.Text = "Operations"
        '
        'frmUpdateFlexcubeLoanData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(884, 622)
        Me.Controls.Add(Me.gbEmployeeList)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.objefFormFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objlblLoanDataCount)
        Me.Controls.Add(Me.btnGetData)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUpdateFlexcubeLoanData"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update Flexcube Loan Data"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeList.ResumeLayout(False)
        CType(Me.picExpand, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefFormFooter.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboBankBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankBranch As System.Windows.Forms.Label
    Friend WithEvents cboBankGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankGroup As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents picExpand As System.Windows.Forms.PictureBox
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgEcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhBankGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhBankBranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAccountNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgColhEmpBankTranUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkISelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvImportInfo As System.Windows.Forms.DataGridView
    Friend WithEvents btnGetData As eZee.Common.eZeeLightButton
    Friend WithEvents objlblLoanDataCount As System.Windows.Forms.Label
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemarks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuShowSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowUnSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnHeadOperations As eZee.Common.eZeeSplitButton
End Class
