﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO


Public Class frmEarningDeductionList

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmEarningDeductionList"
    Private objED As clsEarningDeduction

    Private mstrCurrencyFormat As String = "#0.00"
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (13 Jan 2012) -- End
    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private mstrSearchText As String = ""
    'Sohail (26 Oct 2016) -- End

    'S.SANDEEP [12-JUL-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
    Private mblnIsFromAppraisalScreen As Boolean = False
    Private mstrAppraisalEmployeeIds As String = String.Empty
    'S.SANDEEP [12-JUL-2018] -- END
    'Sohail (13 Jul 2019) -- Start
    'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
    Private mdtView As DataView
    'Sohail (13 Jul 2019) -- End

#End Region

    'S.SANDEEP [12-JUL-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
#Region " Properties "

    Public WriteOnly Property _FromAppraisalScreen() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromAppraisalScreen = value
        End Set
    End Property

    Public WriteOnly Property _AppraisalEmployeeIds() As String
        Set(ByVal value As String)
            mstrAppraisalEmployeeIds = value
        End Set
    End Property

#End Region
    'S.SANDEEP [12-JUL-2018] -- END

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'cboDepartment.BackColor = GUI.ColorOptional
            'cboGrade.BackColor = GUI.ColorOptional
            'cboSections.BackColor = GUI.ColorOptional
            'cboClass.BackColor = GUI.ColorOptional
            'cboCostCenter.BackColor = GUI.ColorOptional
            'cboJob.BackColor = GUI.ColorOptional
            'cboPayPoint.BackColor = GUI.ColorOptional
            'cboUnit.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- End
            'Sohail (14 Jun 2011) -- Start
            cboTranHead.BackColor = GUI.ColorOptional
            cboEDApprovalStatus.BackColor = GUI.ColorComp
            'Sohail (14 Jun 2011) -- End
            cboPeriod.BackColor = GUI.ColorComp 'Sohail (13 Jan 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillComBo()
        Dim objCommonMaster As New clsMasterData
        'Sohail (02 Jul 2014) -- Start
        'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objJob As New clsJobs
        'Dim objPayPoint As New clspaypoint_master
        'Dim objUnit As New clsUnits
        'Sohail (02 Jul 2014) -- End
        Dim dsList As DataSet
        Dim objEmployee As New clsEmployee_Master
        'Dim objTranHead As New clsTransactionHead 'Sohail (14 Jun 2011) 'Sohail (30 Oct 2019)
        Dim objPeriod As New clscommom_period_Tran 'Sohail (13 Jan 2012)

        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'dsList = objCommonMaster.getComboListForHeadType("HeadType")
            'With objTranHeadType
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables("HeadType")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With
            'Sohail (26 Aug 2016) -- End

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'End If

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            Dim strExtraFilter As String = String.Empty
            If mblnIsFromAppraisalScreen Then
                strExtraFilter = "hremployee_master.employeeunkid IN (" & mstrAppraisalEmployeeIds & ") "
            End If
            'S.SANDEEP [12-JUL-2018] -- END

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeList", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strExtraFilter)
            'S.SANDEEP [12-JUL-2018] -- START {Ref#223|ARUTI-239} (strExtraFilter) -- END

            'Anjan [10 June 2015] -- End

            'Sohail (06 Jan 2012) -- End

            With cboEmployee
                .BeginUpdate() 'Sohail (11 Sep 2010)
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate() 'Sohail (11 Sep 2010)
            End With
            'Sohail (26 Oct 2016) -- Start
            'Enhancement - Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Sohail (26 Oct 2016) -- End

            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'dsList = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsList.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objJob.getComboList("JobList", True)
            'With cboJob
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("JobList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "paypointunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("PayPointList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsList = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "unitunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("UnitList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With
            'Sohail (02 Jul 2014) -- End

            'Sohail (14 Jun 2011) -- Start
            'Changes : New filter Transaction Head added. New field 'IsApproved' added.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True)

            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
            ''S.SANDEEP [12-JUL-2018] -- START
            ''ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            ''dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True)
            'If mblnIsFromAppraisalScreen Then
            '    dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , , , , , , , 1)
            'Else
            '    dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , , , , , , , 0)
            'End If
            ''S.SANDEEP [12-JUL-2018] -- END

            ''Sohail (21 Aug 2015) -- End
            'With cboTranHead
            '    .BeginUpdate()
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables("TranHead")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            '    .EndUpdate()
            'End With
            ''Sohail (26 Oct 2016) -- Start
            ''Enhancement - Searchable Dropdown
            'Call SetDefaultSearchText(cboTranHead)
            ''Sohail (26 Oct 2016) -- End
            dsList = objCommonMaster.getComboListCalcType("List", , True)
            With cboCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                SetDefaultSearchText(cboCalcType)
            End With
            'Sohail (30 Oct 2019) -- End

            dsList = objCommonMaster.getComboListForEDHeadApprovalStatus("EdApproval", True)
            With cboEDApprovalStatus
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = New DataView(dsList.Tables("EdApproval"), "Id <> " & enEDHeadApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With
            'Sohail (14 Jun 2011) -- End

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Period")
                .EndUpdate()
            End With
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedValue = objCommonMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'cboPeriod.SelectedValue = objCommonMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
            cboPeriod.SelectedValue = objCommonMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (09 Nov 2013) -- End
            'Sohail (13 Jan 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillComBo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
            objEmployee = Nothing
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objJob = Nothing
            'objPayPoint = Nothing
            'objUnit = Nothing
            'Sohail (02 Jul 2014) -- End
            'objTranHead = Nothing 'Sohail (30 Oct 2019)
            objPeriod = Nothing 'Sohail (13 Jan 2012)
        End Try
    End Sub

    'Sohail (13 Jul 2019) -- Start
    'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
    'Private Sub FillList()
    '    Dim objTranHead As New clsTransactionHead
    '    'Dim objMaster As New clsMasterData
    '    'Dim objExRate As New clsExchangeRate
    '    'Dim objVendor As New clsvendor_master
    '    Dim dsList As DataSet
    '    ' Dim dsCalcType As DataSet = Nothing
    '    Dim lvItem As ListViewItem
    '    'Sohail (26 Aug 2016) -- Start
    '    'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    '    'Dim dsTranHead As DataSet
    '    'Dim objDic As New Dictionary(Of Integer, ArrayList)
    '    'Dim arrList As ArrayList
    '    'Sohail (26 Aug 2016) -- End

    '    Try

    '        If User._Object.Privilege._AllowToViewEmpEDList = True Then                'Pinkal (02-Jul-2012) -- Start

    '            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)
    '            'Sohail (11 Sep 2010) -- Start
    '            lvED.Items.Clear()
    '            lvED.BeginUpdate()
    '            Dim lvArray As New List(Of ListViewItem) 'Sohail (02 Aug 2011)
    '            'Sohail (11 Sep 2010) -- End
    '            'Sohail (13 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'dsList = objED.GetList("ED", CInt(cboEmployee.SelectedValue), _
    '            '                        CInt(cboDepartment.SelectedValue), _
    '            '                        CInt(cboSections.SelectedValue), _
    '            '                        CInt(cboUnit.SelectedValue), _
    '            '                        CInt(cboGrade.SelectedValue), _
    '            '                        0, _
    '            '                        CInt(cboClass.SelectedValue), _
    '            '                        CInt(cboCostCenter.SelectedValue), _
    '            '                        0, _
    '            '                        CInt(cboJob.SelectedValue), _
    '            '                        CInt(cboPayPoint.SelectedValue), _
    '            '                        "employeename, employeeunkid, trnheadtype_id, typeof_id, calctype_id DESC", _
    '            '                        , CInt(cboEDApprovalStatus.SelectedValue), _
    '            '                        CInt(cboTranHead.SelectedValue), _
    '            '                        chkIncludeInactiveEmployee.Checked) '[cboEDApprovalStatus, cboTranHead.SelectedValue - Sohail (14 Jun 2011)], 'Sohail (12 Oct 2011) - [chkIncludeInactiveEmployee.Checked]
    '            'Sohail (02 Jul 2014) -- Start
    '            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
    '            'dsList = objED.GetList("ED", cboEmployee.SelectedValue.ToString, _
    '            '                    CInt(cboDepartment.SelectedValue), _
    '            '                    CInt(cboSections.SelectedValue), _
    '            '                    CInt(cboUnit.SelectedValue), _
    '            '                    CInt(cboGrade.SelectedValue), _
    '            '                    0, _
    '            '                    CInt(cboClass.SelectedValue), _
    '            '                    CInt(cboCostCenter.SelectedValue), _
    '            '                    0, _
    '            '                    CInt(cboJob.SelectedValue), _
    '            '                    CInt(cboPayPoint.SelectedValue), _
    '            '                        "employeename, employeeunkid, end_date DESC, trnheadtype_id, typeof_id, calctype_id DESC", _
    '            '                    , CInt(cboEDApprovalStatus.SelectedValue), _
    '            '                    CInt(cboTranHead.SelectedValue), _
    '            '                    chkIncludeInactiveEmployee.Checked _
    '            '                    , mstrAdvanceFilter _
    '            '                    , mdtPeriodEndDate)
    '            'Sohail (21 Aug 2015) -- Start
    '            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '            'dsList = objED.GetList("ED", cboEmployee.SelectedValue.ToString, _
    '            '                    0, _
    '            '                    0, _
    '            '                   0, _
    '            '                   0, _
    '            '                   0, _
    '            '                   0, _
    '            '                   0, _
    '            '                   0, _
    '            '                   0, _
    '            '                   0, _
    '            '                        "employeename, employeeunkid, end_date DESC, trnheadtype_id, typeof_id, calctype_id DESC", _
    '            '                    , CInt(cboEDApprovalStatus.SelectedValue), _
    '            '                    CInt(cboTranHead.SelectedValue), _
    '            '                    chkIncludeInactiveEmployee.Checked _
    '            '                    , mstrAdvanceFilter _
    '            '                    , mdtPeriodEndDate)

    '            'S.SANDEEP [15 NOV 2016] -- START
    '            'ENHANCEMENT : QUERY OPTIMIZATION
    '            'dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, "ED", True, cboEmployee.SelectedValue.ToString, _
    '            '                        "employeename, employeeunkid, end_date DESC, trnheadtype_id, typeof_id, calctype_id DESC", _
    '            '                       , CInt(cboEDApprovalStatus.SelectedValue) _
    '            '                       , CInt(cboTranHead.SelectedValue) _
    '            '                    , mstrAdvanceFilter _
    '            '                    , mdtPeriodEndDate)

    '            'S.SANDEEP [12-JUL-2018] -- START
    '            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
    '            If mblnIsFromAppraisalScreen Then
    '                mstrAdvanceFilter = "prtranhead_master.isperformance = 1 AND hremployee_master.employeeunkid IN (" & mstrAppraisalEmployeeIds & ") "
    '            End If
    '            'S.SANDEEP [12-JUL-2018] -- END


    '            dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, "ED", True, Convert.ToInt32(cboEmployee.SelectedValue).ToString(), _
    '                                    "employeename, prearningdeduction_master.employeeunkid, cfcommon_period_tran.end_date DESC, prearningdeduction_master.trnheadtype_id, prearningdeduction_master.typeof_id, prearningdeduction_master.calctype_id DESC", _
                '                       , CInt(cboEDApprovalStatus.SelectedValue) _
                '                       , CInt(cboTranHead.SelectedValue) _
                '                    , mstrAdvanceFilter _
                '                    , mdtPeriodEndDate)
    '            'S.SANDEEP [15 NOV 2016] -- END



    '            'Sohail (21 Aug 2015) -- End
    '            'Sohail (02 Jul 2014) -- End
    '            'Sohail (13 Jan 2012) -- End

    '            'Sohail (21 Jun 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Dim dsRow As DataRow
    '            Dim intRowCount As Integer = dsList.Tables("ED").Rows.Count

    '            'Sohail (26 Aug 2016) -- Start
    '            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    '            'dsTranHead = objTranHead.GetList("TranHead", , , True)
    '            'Dim cnt As Integer = dsTranHead.Tables("TranHead").Rows.Count
    '            'For j As Integer = 0 To cnt - 1
    '            '    dsRow = dsTranHead.Tables("TranHead").Rows(j)

    '            '    arrList = New ArrayList
    '            '    arrList.Add(dsRow.Item("HeadType").ToString)
    '            '    arrList.Add(dsRow.Item("TypeOf").ToString)
    '            '    arrList.Add(dsRow.Item("calctype").ToString)

    '            '    objDic.Add(CInt(dsRow.Item("tranheadunkid")), arrList)
    '            'Next
    '            'Sohail (26 Aug 2016) -- End

    '            For i As Integer = 0 To intRowCount - 1
    '                'For Each dsRow As DataRow In dsList.Tables("ED").Rows
    '                dsRow = dsList.Tables("ED").Rows(i)
    '                'Sohail (21 Jun 2012) -- End
    '                lvItem = New ListViewItem
    '                'Sohail (14 Jun 2011) -- Start
    '                'Changes : Show Listview with CheckBox. So first column is checkbox column.
    '                'lvItem.Text = dsRow.Item("edunkid").ToString 'dsRow.Item("employeename").ToString
    '                lvItem.Text = ""
    '                'Sohail (14 Jun 2011) -- End
    '                lvItem.Tag = dsRow.Item("edunkid").ToString

    '                'Sohail (28 Jan 2012) -- Start
    '                'TRA - ENHANCEMENT

    '                'S.SANDEEP [ 20 MARCH 2012 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '                'lvItem.SubItems.Add(dsRow.Item("employeecode").ToString)
    '                lvItem.SubItems.Add(dsRow.Item("trnheadcode").ToString)
    '                'S.SANDEEP [ 20 MARCH 2012 ] -- END

    '                'Sohail (28 Jan 2012) -- End


    '                'S.SANDEEP [ 20 MARCH 2012 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '                'lvItem.SubItems.Add(dsRow.Item("employeename").ToString)
    '                lvItem.SubItems.Add(dsRow.Item("employeecode").ToString & " - " & dsRow.Item("employeename").ToString)
    '                'S.SANDEEP [ 20 MARCH 2012 ] -- END

    '                lvItem.SubItems(colhEmployee.Index).Tag = dsRow.Item("employeeunkid").ToString 'Sohail (08 Nov 2011)

    '                'Dim objTranHead As New clsTransactionHead 'Sohail (03 Nov 2010)
    '                'Sohail (18 Aug 2011) -- Start
    '                'objTranHead = New clsTransactionHead 'Sohail (14 Jun 2011)
    '                'objTranHead._Tranheadunkid = CInt(dsRow.Item("tranheadunkid"))
    '                'Sohail (18 Aug 2011) -- End

    '                ''Sohail (11 Sep 2010) -- Start
    '                ''lvItem.SubItems.Add(objTranHead._Trnheadname)
    '                lvItem.SubItems.Add(dsRow.Item("trnheadname").ToString)
    '                'Sohail (11 Sep 2010) -- End
    '                lvItem.SubItems(colhTrnHead.Index).Tag = dsRow.Item("tranheadunkid").ToString

    '                'Sohail (18 Aug 2011) -- Start
    '                'objTranHeadType.SelectedValue = objTranHead._Trnheadtype_Id
    '                'lvItem.SubItems.Add(objTranHeadType.Text)
    '                'lvItem.SubItems(colhTranHeadType.Index).Tag = objTranHead._Trnheadtype_Id.ToString

    '                'dsList = objMaster.getComboListTypeOf("TypeOf", objTranHead._Trnheadtype_Id)
    '                'With objTypeOf
    '                '    .ValueMember = "Id"
    '                '    .DisplayMember = "Name"
    '                '    .DataSource = dsList.Tables("TypeOf")
    '                '    If .Items.Count > 0 Then .SelectedIndex = 0
    '                'End With
    '                'objTypeOf.SelectedValue = objTranHead._Typeof_id
    '                'lvItem.SubItems.Add(objTypeOf.Text)
    '                'lvItem.SubItems(colhTypeOf.Index).Tag = objTranHead._Typeof_id.ToString

    '                'Select Case objTranHead._Typeof_id
    '                '    Case Is > 1
    '                '        dsCalcType = objMaster.getComboListCalcType("CalcType", 2)
    '                '    Case Else
    '                '        dsCalcType = objMaster.getComboListCalcType("CalcType", objTranHead._Typeof_id)
    '                'End Select
    '                'With objCalcType
    '                '    .ValueMember = "Id"
    '                '    .DisplayMember = "Name"
    '                '    .DataSource = dsCalcType.Tables("CalcType")
    '                '    If .Items.Count > 0 Then .SelectedIndex = 0
    '                'End With
    '                ''Sohail (07 Aug 2010) -- Start
    '                ''Sohail (11 Sep 2010) -- Start
    '                'objCalcType.SelectedValue = objTranHead._Calctype_Id
    '                ''If objTranHead._Calctype_Id = enCalcType.DEFINED_SALARY Or _
    '                ''    objTranHead._Calctype_Id = enCalcType.OnHourWorked Or _
    '                ''    objTranHead._Calctype_Id = enCalcType.OnAttendance Then

    '                ''    objCalcType.SelectedValue = enCalcType.OnAttendance
    '                ''Else
    '                ''objCalcType.SelectedValue = objTranHead._Calctype_Id
    '                ''End If
    '                ''Sohail (11 Sep 2010) -- End
    '                ''Sohail (07 Aug 2010) -- End
    '                'lvItem.SubItems.Add(objCalcType.Text)
    '                'objCalcType.SelectedValue = 0
    '                'lvItem.SubItems(colhCalcType.Index).Tag = objTranHead._Calctype_Id.ToString
    '                'Sohail (26 Nov 2011) -- Start
    '                'Sohail (26 Aug 2016) -- Start
    '                'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    '                'If objDic.ContainsKey(CInt(dsRow.Item("tranheadunkid"))) = False Then
    '                '    'Sohail (21 Jun 2012) -- Start
    '                '    'TRA - ENHANCEMENT
    '                '    'dsTranHead = objTranHead.GetList("TranHead", CInt(dsRow.Item("tranheadunkid")), , True)
    '                '    'If dsTranHead.Tables("TranHead").Rows.Count > 0 Then
    '                '    '    arrList = New ArrayList
    '                '    '    arrList.Add(dsTranHead.Tables("TranHead").Rows(0).Item("HeadType").ToString)
    '                '    '    arrList.Add(dsTranHead.Tables("TranHead").Rows(0).Item("TypeOf").ToString)
    '                '    '    arrList.Add(dsTranHead.Tables("TranHead").Rows(0).Item("calctype").ToString)

    '                '    '    objDic.Add(CInt(dsRow.Item("tranheadunkid")), arrList)

    '                '    '    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("HeadType").ToString)
    '                '    '    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("trnheadtype_id"))

    '                '    '    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("TypeOf").ToString)
    '                '    '    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("typeof_id"))

    '                '    '    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("calctype").ToString)
    '                '    '    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("calctype_id"))
    '                '    'Else
    '                '    '    lvItem.SubItems.Add("")
    '                '    '    lvItem.SubItems.Add("")
    '                '    '    lvItem.SubItems.Add("")
    '                '    'End If
    '                '    lvItem.SubItems.Add("")
    '                '    lvItem.SubItems.Add("")
    '                '    lvItem.SubItems.Add("")
    '                '    'End If
    '                '    lvItem.SubItems.Add("")
    '                '    lvItem.SubItems.Add("")
    '                '    lvItem.SubItems.Add("")
    '                '    'Sohail (21 Jun 2012) -- End
    '                'Else
    '                '    arrList = objDic.Item(CInt(dsRow.Item("tranheadunkid")))
    '                '    lvItem.SubItems.Add(arrList.Item(0).ToString)
    '                '    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsRow.Item("trnheadtype_id")) 'Tran Head Type Id 'Sohail (21 Jun 2012
    '                '    lvItem.SubItems.Add(arrList.Item(1).ToString)
    '                '    lvItem.SubItems(colhTypeOf.Index).Tag = CInt(dsRow.Item("typeof_id")) 'Type of Id 'Sohail (21 Jun 2012
    '                '    lvItem.SubItems.Add(arrList.Item(2).ToString)
    '                '    lvItem.SubItems(colhCalcType.Index).Tag = CInt(dsRow.Item("calctype_id")) 'Calc Type Id 'Sohail (21 Jun 2012
    '                'End If
    '                lvItem.SubItems.Add(dsRow.Item("trnheadtype_name").ToString)
    '                lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsRow.Item("trnheadtype_id"))
    '                lvItem.SubItems.Add(dsRow.Item("typeof_name").ToString)
    '                lvItem.SubItems(colhTypeOf.Index).Tag = CInt(dsRow.Item("typeof_id"))
    '                lvItem.SubItems.Add(dsRow.Item("calctype_name").ToString)
    '                lvItem.SubItems(colhCalcType.Index).Tag = CInt(dsRow.Item("calctype_id"))
    '                'Sohail (26 Aug 2016) -- End
                    '    'dsTranHead = objTranHead.GetList("TranHead", CInt(dsRow.Item("tranheadunkid")), , True)
                    '    'If dsTranHead.Tables("TranHead").Rows.Count > 0 Then
                    '    '    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("HeadType").ToString)
                    '    '    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("trnheadtype_id"))

                    '    '    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("TypeOf").ToString)
                    '    '    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("typeof_id"))

                    '    '    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("calctype").ToString)
                    '    '    lvItem.SubItems(colhTranHeadType.Index).Tag = CInt(dsTranHead.Tables("TranHead").Rows(0).Item("calctype_id"))
                    '    'Else
                    '    '    lvItem.SubItems.Add("")
                    '    '    lvItem.SubItems.Add("")
                    '    '    lvItem.SubItems.Add("")
                    '    'End If
    '                'Sohail (26 Nov 2011) -- End
    '                'Sohail (18 Aug 2011) -- End

    '                'Sohail (11 Sep 2010) -- Start
    '                'objVendor._Vendorunkid = CInt(dsRow.Item("vendorunkid").ToString)
    '                'lvItem.SubItems.Add(objVendor._Companyname)

    '                'Sohail (03 Nov 2010) -- Start
    '                'Dim objMembership As New clsmembership_master
    '                'objMembership._Membershipunkid = CInt(dsRow.Item("vendorunkid").ToString)
    '                'lvItem.SubItems.Add(objMembership._Membershipname)
    '                lvItem.SubItems.Add(dsRow.Item("membershipname").ToString)
    '                'Sohail (03 Nov 2010) -- End
    '                'Sohail (11 Sep 2010) -- End
    '                lvItem.SubItems(colhVendor.Index).Tag = dsRow.Item("vendorunkid").ToString

    '                '*** Currency is Removed *** (05 Aug 2010)
    '                'objExRate._ExchangeRateunkid = CInt(dsRow.Item("currencyunkid").ToString)
    '                'lvItem.SubItems.Add(objExRate._Currency_Name)
    '                'lvItem.SubItems(6).Tag = dsRow.Item("currencyunkid").ToString

    '                'Sohail (13 Jan 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                lvItem.SubItems.Add(dsRow.Item("period_name").ToString)
    '                lvItem.SubItems(colhPeriod.Index).Tag = CInt(dsRow.Item("periodunkid"))
    '                'Sohail (13 Jan 2012) -- End

    '                If CDec(dsRow.Item("amount").ToString) = 0 Then
                    '    lvItem.SubItems.Add("")
    '                Else
    '                    lvItem.SubItems.Add(Format(dsRow.Item("amount"), mstrCurrencyFormat))
    '                End If

    '                'Sohail (24 Jun 2011) -- Start
    '                If CBool(dsRow.Item("isapproved")) = True Then
    '                    lvItem.SubItems.Add("A")
    '                Else
    '                    lvItem.SubItems.Add("P")
    '                End If
    '                'Sohail (24 Jun 2011) -- End

    '                'S.SANDEEP [ 17 OCT 2012 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '                lvItem.SubItems.Add(dsRow.Item("membershiptranunkid").ToString)
    '                'S.SANDEEP [ 17 OCT 2012 ] -- END

    '                'Sohail (11 Sep 2010) -- Start
    '                'Sohail (02 Aug 2011) -- Start
    '                'RemoveHandler lvED.ItemChecked, AddressOf lvED_ItemChecked
    '                'lvED.Items.Add(lvItem)
    '                'AddHandler lvED.ItemChecked, AddressOf lvED_ItemChecked
    '                lvArray.Add(lvItem)
    '                'Sohail (02 Aug 2011) -- End

    '                'Sohail (03 Nov 2010) -- Start
    '                'objTranHead = Nothing
    '                'objMembership = Nothing
    '                'Sohail (03 Nov 2010) -- End

    '                'Sohail (11 Sep 2010) -- End
    '            Next
    '            'Sohail (11 Sep 2010) -- Start
    '            'Sohail (02 Aug 2011) -- Start
    '            RemoveHandler lvED.ItemChecked, AddressOf lvED_ItemChecked
    '            lvED.Items.AddRange(lvArray.ToArray)
    '            AddHandler lvED.ItemChecked, AddressOf lvED_ItemChecked
    '            lvArray = Nothing
    '            'Sohail (02 Aug 2011) -- End
    '            'Sohail (11 Sep 2010) -- End

    '            lvED.GroupingColumn = colhEmployee
    '            lvED.DisplayGroups(True)

    '            If lvED.Items.Count > 5 Then
    '                colhAmount.Width = 160 - 20 'Sohail (24 Jun 2011)
    '            Else
    '                colhAmount.Width = 160 'Sohail (24 Jun 2011)
    '            End If
    '            lvED.GridLines = False 'Sohail (16 Oct 2010)
    '            lvED.EndUpdate() 'Sohail (11 Sep 2010)
    '            Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)
    '            'Sohail (14 Jun 2011) -- Start
    '            'Changes : ED Approve/Disapprove option added.
    '            btnApprove.Visible = False
    '            btnDisApprove.Visible = False
    '            If CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Approved AndAlso btnDisApprove.Enabled = True Then
    '                btnDisApprove.Visible = True
    '            ElseIf CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Pending AndAlso btnApprove.Enabled = True Then
    '                btnApprove.Visible = True
    '            End If
    '            'Sohail (14 Jun 2011) -- End

    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '        'objMaster = Nothing
    '        'objExRate = Nothing
    '        'Sohail (26 Aug 2016) -- Start
    '        'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
    '        'objDic = Nothing
    '        'arrList = Nothing
    '        'dsTranHead = Nothing
    '        'Sohail (26 Aug 2016) -- End
    '        dsList = Nothing
    '        'objVendor = Nothing
    '        dsList = Nothing
    '        Call objbtnSearch.ShowResult(CStr(lvED.Items.Count)) 'Sohail (02 Jul 2014)
    '    End Try
    'End Sub
    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet

        Try

            'Sohail (13 Feb 2020) -- Start
            'POWERSOFT Enhancement # 0004519 : To see total amount for checked employees before approving flat rate earning deductions.
            objlblTotalAmt.Text = Format(0, GUI.fmtCurrency)
            'Sohail (13 Feb 2020) -- End

            If User._Object.Privilege._AllowToViewEmpEDList = True Then

                Cursor.Current = Cursors.WaitCursor

                'Sohail (30 Oct 2019) -- Start
                'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
                Dim strFilter As String = ""
                If CInt(cboCalcType.SelectedValue) > 0 Then
                    strFilter = " AND prtranhead_master.calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
                End If
                'Sohail (30 Oct 2019) -- End

                If mblnIsFromAppraisalScreen Then
                    mstrAdvanceFilter = "prtranhead_master.isperformance = 1 AND hremployee_master.employeeunkid IN (" & mstrAppraisalEmployeeIds & ") "
                End If

                dsList = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, "ED", True, Convert.ToInt32(cboEmployee.SelectedValue).ToString(), _
                                       "", _
                                       , CInt(cboEDApprovalStatus.SelectedValue) _
                                       , CInt(cboTranHead.SelectedValue) _
                                       , mstrAdvanceFilter _
                                       , mdtPeriodEndDate, , , True, strFilter)
                'Sohail (12 Oct 2021) - [Removed strSortField:employeename, prearningdeduction_master.employeeunkid, cfcommon_period_tran.end_date DESC, prearningdeduction_master.trnheadtype_id, prearningdeduction_master.typeof_id, prearningdeduction_master.calctype_id DESC]
                'Sohail (30 Oct 2019) - [strFilter]

                mdtView = dsList.Tables(0).DefaultView

                objdgcolhCheck.DataPropertyName = "IsChecked"
                dgcolhEmployee.DataPropertyName = "employeename"
                objdgcolhEmpCode.DataPropertyName = "employeecode"
                dgcolhTrnHeadCode.DataPropertyName = "trnheadcode"
                dgcolhTrnHead.DataPropertyName = "trnheadname"
                dgcolhTranHeadType.DataPropertyName = "trnheadtype_name"
                dgcolhTypeOf.DataPropertyName = "typeof_name"
                dgcolhCalcType.DataPropertyName = "calctype_name"
                dgcolhPeriod.DataPropertyName = "period_name"
                dgcolhAmount.DataPropertyName = "amount"
                dgcolhAmount.DefaultCellStyle.Format = mstrCurrencyFormat
                'Sohail (02 Sep 2019) -- Start
                dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                'Sohail (02 Sep 2019) -- End
                dgcolhApprovePending.DataPropertyName = "isapproved"
                objdgcolhIsGroup.DataPropertyName = "IsGrp"
                objdgcolhMembership.DataPropertyName = "membershipname"
                objdgcolhEDUnkid.DataPropertyName = "edunkid"
                objdgcolhEmpid.DataPropertyName = "employeeunkid"
                objdgcolhTranheadUnkId.DataPropertyName = "tranheadunkid"
                objdgcolhPeriodUnkid.DataPropertyName = "periodunkid"
                objdgcolhTranHeadTypeId.DataPropertyName = "trnheadtype_id"
                objdgcolhTypeOfId.DataPropertyName = "typeof_id"
                objdgcolhCalcTypeId.DataPropertyName = "calctype_id"
                objdgcolhMemTranUnkid.DataPropertyName = "membershiptranunkid"
                dgcolhEmployee.Visible = False
                objdgcolhEmpCode.Visible = False

                With dgvED
                    .AutoGenerateColumns = False

                    .DataSource = mdtView

                End With

                For Each dgvc As DataGridViewColumn In dgvED.Columns
                    dgvc.SortMode = DataGridViewColumnSortMode.NotSortable
                Next

                Cursor.Current = Cursors.Default
                btnApprove.Visible = False
                btnDisApprove.Visible = False

                If CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Approved AndAlso btnDisApprove.Enabled = True Then
                    'Sohail (06 Sep 2019) -- Start
                    'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
                    'btnDisApprove.Visible = True
                    'Sohail (06 Sep 2019) -- End
                ElseIf CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Pending AndAlso btnApprove.Enabled = True Then
                    btnApprove.Visible = True
                    'Sohail (06 Sep 2019) -- Start
                    'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
                    btnDisApprove.Visible = True
                    'Sohail (06 Sep 2019) -- End
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objTranHead = Nothing
            dsList = Nothing
            dsList = Nothing
            'Sohail (02 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
            '1. Set ED in pending status if user has not priviledge of ED approval
            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
            '3. Send notifications to ED approvers for all employees ED in one email
            'Call objbtnSearch.ShowResult(mdtView.ToTable.Select("IsGrp = 0").Length.ToString)
            If mdtView IsNot Nothing Then
            Call objbtnSearch.ShowResult(mdtView.ToTable.Select("IsGrp = 0").Length.ToString)
            Else
                Call objbtnSearch.ShowResult("0")
            End If
            'Sohail (02 Sep 2019) -- End
        End Try
    End Sub
    'Sohail (13 Jul 2019) -- End

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEarningDeduction
            btnEdit.Enabled = User._Object.Privilege._EditEarningDeduction
            'Sohail (14 Jun 2011) -- Start
            'Changes : those user can delete ed heads who has ED approval Privilege
            'btnDelete.Enabled = User._Object.Privilege._DeleteEarningDeduction
            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            'btnDelete.Enabled = User._Object.Privilege._AllowToApproveEarningDeduction
            btnDelete.Enabled = User._Object.Privilege._DeleteEarningDeduction
            'Sohail (06 Sep 2019) -- End
            'Sohail (14 Jun 2011) -- End
            mnuGlobalAssign.Enabled = User._Object.Privilege._AllowPerformGlobalAssignOnED
            mnuImportED.Enabled = User._Object.Privilege._AllowImportED
            mnuExportED.Enabled = User._Object.Privilege._AllowExportED
            mnuImporUpdatetFlatRateTran.Enabled = User._Object.Privilege._EditEarningDeduction 'Sohail (24 Jun 2011)
            'Sohail (14 Jun 2011) -- Start
            btnApprove.Enabled = User._Object.Privilege._AllowToApproveEarningDeduction
            btnDisApprove.Enabled = User._Object.Privilege._AllowToApproveEarningDeduction
            'Sohail (14 Jun 2011) -- End

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuBatchTransaction.Enabled = User._Object.Privilege._AddBatchTransaction
            mnuExemption.Enabled = User._Object.Privilege._AddEmployeeExemption
            'S.SANDEEP [ 29 OCT 2012 ] -- END
            mnuAssignMembershipHeads.Enabled = User._Object.Privilege._AddEarningDeduction 'Sohail (18 Nov 2016)

            'Sohail (21 Dec 2016) -- Start
            'ACB Enhancement - 64.1 - Loan data Integration with T24 ACB System.
            mnuUpdatetT24ACBLoanData.Enabled = User._Object.Privilege._EditEarningDeduction
            If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_ACB_T24WEB Then
                mnuUpdatetT24ACBLoanData.Visible = True
            Else
                mnuUpdatetT24ACBLoanData.Visible = False
            End If
            'Sohail (21 Dec 2016) -- End

            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube.
            mnuUpdatetFlexcubeLoanData.Enabled = User._Object.Privilege._EditEarningDeduction
            If ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
                mnuUpdatetFlexcubeLoanData.Visible = True
            Else
                mnuUpdatetFlexcubeLoanData.Visible = False
            End If
            'Hemant (10 Nov 2022) -- End

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            If mblnIsFromAppraisalScreen Then
                btnOperations.Enabled = False
                btnNew.Enabled = False
                btnEdit.Enabled = False
                btnApprove.Visible = False
                btnDisApprove.Visible = False
            End If
            'S.SANDEEP [12-JUL-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Sohail (13 Jul 2019) -- Start
    'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
    'Sohail (14 Jun 2011) -- Start
    'Private Sub CheckAllEdHeads(ByVal blnCheckAll As Boolean)
    '    Try
    '        For Each lvItem As ListViewItem In lvED.Items
    '            'If CInt(lvItem.SubItems(colhTypeOf.Index).Tag) = enTypeOf.Salary Then Continue For 'Sohail (13 Jan 2014)
    '            RemoveHandler lvED.ItemCheck, AddressOf lvED_ItemCheck
    '            RemoveHandler lvED.ItemChecked, AddressOf lvED_ItemChecked
    '            lvItem.Checked = blnCheckAll
    '            AddHandler lvED.ItemCheck, AddressOf lvED_ItemCheck
    '            AddHandler lvED.ItemChecked, AddressOf lvED_ItemChecked
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CheckAllEdHeads", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (14 Jun 2011) -- End
    'Sohail (13 Jul 2019) -- End

    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 22, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Oct 2016) -- End
#End Region

#Region " Form's Events "

    Private Sub frmEarningDeductionList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objED = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEarningDeductionList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEarningDeductionList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Close()
                Case Keys.Delete
                    btnDelete.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEarningDeductionList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEarningDeductionList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objED = New clsEarningDeduction
        'Dim objExRate As New clsExchangeRate 'Sohail (01 Dec 2010)
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            Call SetVisibility()
            Call SetColor()
            Call FillComBo()
            '*** Currency Format ***
            'Sohail (01 Dec 2010) -- Start
            'If objExRate.getComboList.Tables(0).Rows.Count > 0 Then
            '    objExRate._ExchangeRateunkid = 1
            '    mstrCurrencyFormat = objExRate._fmtCurrency
            'End If
            mstrCurrencyFormat = GUI.fmtCurrency
            'Sohail (01 Dec 2010) -- End
            'Call FillList()

            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'If lvED.Items.Count > 0 Then lvED.Items(0).Selected = True
            'lvED.Select()
            'Sohail (13 Jul 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEarningDeductionList_Load", mstrModuleName)
        Finally
            'objExRate = Nothing 'Sohail (01 Dec 2010)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEarningDeduction.SetMessages()
            objfrm._Other_ModuleNames = "clsEarningDeduction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objFrm As New frmEarningDeduction_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then

                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'Call FillList()
                'S.SANDEEP [ 20 MARCH 2012 ] -- END
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditEarningDeduction = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        'Sohail (13 Jul 2019) -- Start
        'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
        'If lvED.SelectedItems.Count < 1 Then
        If dgvED.SelectedRows.Count <= 0 OrElse CBool(dgvED.SelectedRows(0).Cells(objdgcolhIsGroup.Index).Value) = True Then
            'Sohail (13 Jul 2019) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Earning Deduction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'lvED.Select()
            'Sohail (13 Jul 2019) -- End
            Exit Sub
        End If


        'Sohail (13 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'objPeriod._Periodunkid = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
        'Sohail (13 Jul 2019) -- Start
        'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
        'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dgvED.SelectedRows(0).Cells(objdgcolhPeriodUnkid.Index).Value)
        'Sohail (13 Jul 2019) -- End
        'Sohail (21 Aug 2015) -- End
        If objPeriod._Statusid = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed."), enMsgBoxStyle.Information) '?1
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'lvED.Select()
            dgvED.Select()
            'Sohail (13 Jul 2019) -- End
            Exit Sub
        End If
        'Sohail (13 Jan 2012) -- End

        Dim frm As New frmEarningDeduction_AddEdit
        Dim objTranHead As New clsTransactionHead
        Try

            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvED.SelectedItems(0).Index
            'Sohail (13 Jul 2019) -- End

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTranHead._Tranheadunkid = CInt(lvED.SelectedItems(0).SubItems(colhTrnHead.Index).Tag)
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(lvED.SelectedItems(0).SubItems(colhTrnHead.Index).Tag)
            objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(dgvED.SelectedRows(0).Cells(objdgcolhTranheadUnkId.Index).Value)
            'Sohail (13 Jul 2019) -- End
            'Sohail (21 Aug 2015) -- End
            If objTranHead._Typeof_id = enTypeOf.Salary Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'If frm.displayDialog(CInt(lvED.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            If frm.displayDialog(CInt(dgvED.SelectedRows(0).Cells(objdgcolhEDUnkid.Index).Value), enAction.EDIT_ONE) Then
                'Sohail (13 Jul 2019) -- End
                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'Sohail (13 Jul 2019) -- Start
                'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
                'Call FillList()
                Call FillList()
                'Sohail (13 Jul 2019) -- End
                'S.SANDEEP [ 20 MARCH 2012 ] -- END                
            End If

            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'lvED.Items(intSelectedIndex).Selected = True
            'lvED.EnsureVisible(intSelectedIndex)
            'lvED.Select()
            'Sohail (13 Jul 2019) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            objTranHead = Nothing
            frm = Nothing
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (30 Oct 2019) -- Start
        'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
        If mdtView Is Nothing Then Exit Sub
        'Sohail (30 Oct 2019) -- End
        'Sohail (13 Jan 2014) -- Start
        'Enhancement - Multiple Delete on Earning Deduction
        'If lvED.SelectedItems.Count < 1 Then
        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Earning Deduction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
        'Sohail (13 Jul 2019) -- Start
        'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
        'If lvED.CheckedItems.Count <= 0 Then
        If mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
            'Sohail (13 Jul 2019) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Tick atleast one Earning Deduction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            'Sohail (13 Jan 2014) -- End
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'lvED.Select()
            'Sohail (13 Jul 2019) -- End
            Exit Sub
        End If
        'Sohail (13 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (17 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        'Sohail (17 Jan 2012) -- End
        'Sohail (13 Jan 2014) -- Start
        'Enhancement - Multiple Delete on Earning Deduction
        'Dim objPeriod As New clscommom_period_Tran
        'objPeriod._Periodunkid = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
        'If objPeriod._Statusid = enStatusType.Close Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed."), enMsgBoxStyle.Information) '?1
        '    lvED.Select()
        '    Exit Sub
        '    'Sohail (17 Jan 2012) -- Start
        '    'TRA - ENHANCEMENT
        'Else
        '    dtPeriodStart = objPeriod._Start_Date
        '    dtPeriodEnd = objPeriod._End_Date
        '    'Sohail (17 Jan 2012) -- End
        'End If
        'Sohail (13 Jan 2012) -- End
        'Sohail (11 Jan 2014) -- End

        Dim objTranHead As New clsTransactionHead
        'Sohail (24 Aug 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objMaster As New clsMasterData
        Dim objPayment As New clsPayment_tran
        Dim objTnALeaveTran As New clsTnALeaveTran
        Dim intOpenPeriod As Integer = 0
        Dim dtOpenPeriodEnd As DateTime = Nothing
        Dim dsList As DataSet
        'Sohail (24 Aug 2012) -- End
        'Sohail (13 Jan 2014) -- Start
        'Enhancement - Multiple Delete on Earning Deduction
        Dim objPeriod As New clscommom_period_Tran
        Dim strEmpList As String = ""
        'Sohail (13 Jan 2014) -- End

        Try

            'Sohail (13 Jan 2014) -- Start
            'Enhancement - Multiple Delete on Earning Deduction
            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvED.SelectedItems(0).Index
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'Dim allPeriods As List(Of ListViewItem) = (From p In lvED.CheckedItems.Cast(Of ListViewItem)() Select (p)).Distinct().ToList
            'For Each lvItem As ListViewItem In allPeriods
            '    objPeriod = New clscommom_period_Tran
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
            '    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
            '    'Sohail (21 Aug 2015) -- End
            '    If objPeriod._Statusid = enStatusType.Close Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! You can not Delete ticked transaction. Reason: Period is closed for some of the transactions."), enMsgBoxStyle.Information)
            '        lvED.Select()
            '        Exit Sub
            '    End If
            'Next
            Dim dr() As DataRow = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1")
            If dr.Length > 0 Then
                Dim dt As DataTable = New DataView(dr.CopyToDataTable).ToTable(True, "periodunkid")
                For Each dtRow As DataRow In dt.Rows
                objPeriod = New clscommom_period_Tran
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dtRow.Item("periodunkid"))
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! You can not Delete ticked transaction. Reason: Period is closed for some of the transactions."), enMsgBoxStyle.Information)
                        dgvED.Select()
                    Exit Sub
                End If
            Next
            End If
            'Sohail (13 Jul 2019) -- End
            'Sohail (13 Jan 2014) -- End

            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            Dim intCount As Integer = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsApproved")) = False) Select (p)).Count()
            If intCount > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, You can not Delete ticked Transaction Head(s). Reason : Some transaction heads are in pending status."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (06 Sep 2019) -- End

            'Sohail (24 Aug 2012) -- Start
            'TRA - ENHANCEMENT

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
            intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = intOpenPeriod
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intOpenPeriod
            'Sohail (21 Aug 2015) -- End
            dtOpenPeriodEnd = objPeriod._End_Date

            'Sohail (24 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, intOpenPeriod, clsPayment_tran.enPayTypeId.PAYMENT, CInt(lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag))
            'Sohail (13 Jan 2014) -- Start
            'Enhancement - Multiple Delete on Earning Deduction
            'dsList = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, intOpenPeriod, clsPayment_tran.enPayTypeId.PAYMENT, lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString)
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'Dim allEmp As List(Of String) = (From p In lvED.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhEmployee.Index).Tag.ToString)).Distinct().ToList
            Dim allEmp As List(Of String) = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).Distinct().ToList
            'Sohail (13 Jul 2019) -- End
            strEmpList = String.Join(",", allEmp.ToArray)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, intOpenPeriod, clsPayment_tran.enPayTypeId.PAYMENT, strEmpList)
            dsList = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, intOpenPeriod, clsPayment_tran.enPayTypeId.PAYMENT, strEmpList, False)
            'Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2014) -- End
            'Sohail (24 Sep 2012) -- End
            If dsList.Tables("Payment").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, You can not Delete ticked Transaction Head(s). Reason : Payment is already done for current open Period. Please Delete payment first to Delete ticked transaction head."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (24 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, CInt(lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag), dtOpenPeriodEnd) = True Then
            'Sohail (13 Jan 2014) -- Start
            'Enhancement - Multiple Delete on Earning Deduction
            'If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString, dtOpenPeriodEnd) = True Then
            If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, strEmpList, dtOpenPeriodEnd) = True Then
                'Sohail (13 Jan 2014) -- End
                'Sohail (24 Sep 2012) -- End
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, You can not Delete ticked Transaction Head. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete ticked transaction head."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, You can not Delete ticked Transaction Head. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete ticked transaction head.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 24, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If
            'Sohail (24 Aug 2012) -- End

            'Sohail (13 Jan 2014) -- Start
            'Enhancement - Multiple Delete on Earning Deduction
            'objTranHead._Tranheadunkid = CInt(lvED.SelectedItems(0).SubItems(colhTrnHead.Index).Tag)
            'If objTranHead._Typeof_id = enTypeOf.Salary Then
            '    'Sohail (17 Jan 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."), enMsgBoxStyle.Information)
            '    'Exit Try
            '    Dim ds As DataSet
            '    ds = objED.GetList("EDSalary", lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString, , , , , , , , , , , , , , , , , dtPeriodStart)
            '    If ds.Tables("EDSalary").Rows.Count <= 0 Then 'Only One period ED is aasigned so after deleting this salary head no salary head will remain on ED.
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."), enMsgBoxStyle.Information)
            'Exit Try
            '    End If
            '    ds = objED.GetList("EDSalary", lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString, , , , , , , , , , , , , , , , , dtPeriodEnd)
            '    If ds.Tables("EDSalary").Rows.Count > 1 Then 'Other Heads are asssigned besides Salary Heads. First delete other heads then finally Salary Heads.
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You can not Delete Salary Head when other Heads are assigned. Please delete other Heads First."), enMsgBoxStyle.Information)
            '        Exit Try
            '    End If
            '    'Sohail (17 Jan 2012) -- End
            'End If
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'Dim allSalaryHeads As List(Of ListViewItem) = (From p In lvED.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhTypeOf.Index).Tag) = enTypeOf.Salary) Select (p)).ToList
            'For Each lvItem As ListViewItem In allSalaryHeads
            '    objPeriod = New clscommom_period_Tran
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
            '    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
            '    'Sohail (21 Aug 2015) -- End
            '    dtPeriodStart = objPeriod._Start_Date
            '    dtPeriodEnd = objPeriod._End_Date

            '    Dim ds As DataSet
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'ds = objED.GetList("EDSalary", lvItem.SubItems(colhEmployee.Index).Tag.ToString, , , , , , , , , , , , , , , , , dtPeriodStart)
            '    ds = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, "EDSalary", False, lvItem.SubItems(colhEmployee.Index).Tag.ToString, , , , , , dtPeriodStart)
            '    'Sohail (21 Aug 2015) -- End
            '    If ds.Tables("EDSalary").Rows.Count <= 0 Then 'Only One period ED is aasigned so after deleting this salary head no salary head will remain on ED.
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, You can not Delete some of the ticked Salary Head(s). Please go to Employee Master to Delete ticked Salary Head for some of the employee(s)."), enMsgBoxStyle.Information)
            '        Exit Try
            '    End If

            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'ds = objED.GetList("EDSalary", lvItem.SubItems(colhEmployee.Index).Tag.ToString, , , , , , , , , , , , , , , , , dtPeriodEnd)
            '    ds = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, "EDSalary", False, lvItem.SubItems(colhEmployee.Index).Tag.ToString, , , , , , dtPeriodEnd)
            '    'Sohail (21 Aug 2015) -- End
            '    If ds.Tables("EDSalary").Rows.Count > 1 Then 'Other Heads are asssigned besides Salary Heads. First delete other heads then finally Salary Heads.
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, You can not Delete some of the ticked Salary Head(s) when other Heads are assigned. Please delete other Heads First."), enMsgBoxStyle.Information)
            '        Exit Try
            '    End If
            'Next
            Dim allSalaryHeads As List(Of DataRow) = (From p In mdtView.ToTable Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("typeof_id")) = enTypeOf.Salary) Select (p)).ToList
            For Each dRow As DataRow In allSalaryHeads
                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dRow.Item("periodunkid"))
                dtPeriodStart = objPeriod._Start_Date
                dtPeriodEnd = objPeriod._End_Date

                Dim ds As DataSet
                ds = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, "EDSalary", False, dRow.Item("employeeunkid").ToString, , , , , , dtPeriodStart)
                If ds.Tables("EDSalary").Rows.Count <= 0 Then 'Only One period ED is aasigned so after deleting this salary head no salary head will remain on ED.
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, You can not Delete some of the ticked Salary Head(s). Please go to Employee Master to Delete ticked Salary Head for some of the employee(s)."), enMsgBoxStyle.Information)
                    Exit Try
                End If

                ds = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, "EDSalary", False, dRow.Item("employeeunkid").ToString, , , , , , dtPeriodEnd)
                If ds.Tables("EDSalary").Rows.Count > 1 Then 'Other Heads are asssigned besides Salary Heads. First delete other heads then finally Salary Heads.
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, You can not Delete some of the ticked Salary Head(s) when other Heads are assigned. Please delete other Heads First."), enMsgBoxStyle.Information)
                    Exit Try
                End If
            Next
            'Sohail (13 Jul 2019) -- End
            'Sohail (13 Jan 2014) -- End

            'Sohail (08 Nov 2011) -- Start
            'Sohail (17 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            '*** Now allow to delete assogned Transaction Heads as now onlly open period ed can be delete.
            'If objED.isUsed(CInt(lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag), CInt(lvED.SelectedItems(0).SubItems(colhTrnHead.Index).Tag)) = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This Earning Deduction is in used."), enMsgBoxStyle.Information)
            '    Exit Try
            'End If
            'Sohail (17 Jan 2012) -- End
            'Sohail (08 Nov 2011) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete ticked Earning Deduction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, Now, "") 'Sohail (14 Oct 2010)
                Dim frm As New frmReasonSelection
                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 

                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                'S.SANDEEP [ 17 OCT 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason).
                'lvED.SelectedItems(0).Remove()
                'Sandeep [ 16 Oct 2010 ] -- End 

                'Sohail (13 Jan 2014) -- Start
                'Enhancement - Multiple Delete on Earning Deduction
                'If CInt(lvED.SelectedItems(0).SubItems(objcolhMemTranUnkid.Index).Text) > 0 Then
                '    Dim iCurrPeriodUnkid As Integer = -1 : Dim objMData As New clsMasterData
                '    iCurrPeriodUnkid = objMData.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
                '    Dim iEDUnkid As Integer = -1 : Dim objMTran As New clsMembershipTran
                '    iEDUnkid = objMTran.Get_Mem_EDUnkid(CInt(lvED.SelectedItems(0).Tag), _
                '                                                                      CInt(lvED.SelectedItems(0).SubItems(objcolhMemTranUnkid.Index).Text), _
                '                                                                      CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag), _
                '                                                                      CInt(lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag))
                '    If iCurrPeriodUnkid = CInt(lvED.SelectedItems(0).SubItems(colhPeriod.Index).Tag) Then
                '        If objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = True Then
                '            If objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = True Then
                '                objMTran.Void_Membership(CInt(lvED.SelectedItems(0).SubItems(objcolhMemTranUnkid.Index).Text), CInt(lvED.SelectedItems(0).SubItems(colhEmployee.Index).Tag))
                '            End If
                '        End If
                '    Else
                '        objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                '        objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                '    End If
                'Else
                '    objED.Void(CInt(lvED.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                'End If
                'If CInt(lvED.SelectedItems(0).SubItems(objcolhMemTranUnkid.Index).Text) > 0 Then
                '    Call FillList()
                'Else
                '    lvED.SelectedItems(0).Remove()
                'End If
                Cursor.Current = Cursors.WaitCursor

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objED._FormName = mstrModuleName
                objED._LoginEmployeeUnkid = 0
                objED._ClientIP = getIP()
                objED._HostName = getHostName()
                objED._FromWeb = False
                objED._AuditUserId = User._Object._Userunkid
objED._CompanyUnkid = Company._Object._Companyunkid
                objED._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

            'Sohail (13 Jul 2019) -- Start
                'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
                'For Each lvItem As ListViewItem In lvED.CheckedItems
                '    If CInt(lvItem.SubItems(objcolhMemTranUnkid.Index).Text) > 0 Then
                '        Dim iCurrPeriodUnkid As Integer = -1 : Dim objMData As New clsMasterData

                '        'S.SANDEEP [04 JUN 2015] -- START
                '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '        'iCurrPeriodUnkid = objMData.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
                '        iCurrPeriodUnkid = objMData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                '        'S.SANDEEP [04 JUN 2015] -- END

                '        Dim iEDUnkid As Integer = -1 : Dim objMTran As New clsMembershipTran
                '        iEDUnkid = objMTran.Get_Mem_EDUnkid(CInt(lvItem.Tag), _
                '                                              CInt(lvItem.SubItems(objcolhMemTranUnkid.Index).Text), _
                '                                              CInt(lvItem.SubItems(colhPeriod.Index).Tag), _
                '                                              CInt(lvItem.SubItems(colhEmployee.Index).Tag))
                '        If iCurrPeriodUnkid = CInt(lvItem.SubItems(colhPeriod.Index).Tag) Then
                '            'Sohail (21 Aug 2015) -- Start
                '            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '            'If objED.Void(CInt(lvItem.Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = True Then
                '            If objED.Void(CInt(lvItem.Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName) = True Then
                '                'Sohail (21 Aug 2015) -- End
                '                If iEDUnkid > 0 Then
                '                    'Sohail (21 Aug 2015) -- Start
                '                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '                    'If objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = True Then
                '                    If objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName) = True Then
                '                        'Sohail (21 Aug 2015) -- End
                '                        objMTran.Void_Membership(CInt(lvItem.SubItems(objcolhMemTranUnkid.Index).Text), CInt(lvItem.SubItems(colhEmployee.Index).Tag))
                '                    End If
                '                End If
                '            End If
                '        Else
                '            'Sohail (21 Aug 2015) -- Start
                '            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '            'objED.Void(CInt(lvItem.Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                '            'objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                '            objED.Void(CInt(lvItem.Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                '            objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                '            'Sohail (21 Aug 2015) -- End
                '        End If
                '    Else
                '        'Sohail (21 Aug 2015) -- Start
                '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '        'objED.Void(CInt(lvItem.Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                '        objED.Void(CInt(lvItem.Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                '        'Sohail (21 Aug 2015) -- End
                '    End If
                'Next
                'Sohail (06 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
                'For Each dRow As DataRow In mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 ")
                '    If CInt(dRow.Item("membershiptranunkid")) > 0 Then
                '        Dim iCurrPeriodUnkid As Integer = -1 : Dim objMData As New clsMasterData

                '        iCurrPeriodUnkid = objMData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)

                '        Dim iEDUnkid As Integer = -1 : Dim objMTran As New clsMembershipTran
                '        iEDUnkid = objMTran.Get_Mem_EDUnkid(CInt(dRow.Item("edunkid")), _
                '                                              CInt(dRow.Item("membershiptranunkid")), _
                '                                              CInt(dRow.Item("periodunkid")), _
                '                                              CInt(dRow.Item("employeeunkid")))
                '        If iCurrPeriodUnkid = CInt(dRow.Item("periodunkid")) Then
                '            If objED.Void(CInt(dRow.Item("edunkid")), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName) = True Then
                '                If iEDUnkid > 0 Then
                '                    If objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName) = True Then
                '                        objMTran.Void_Membership(CInt(dRow.Item("membershiptranunkid")), CInt(dRow.Item("employeeunkid")))
                '                    End If
                '                End If
                '            End If
                '        Else
                '            objED.Void(CInt(dRow.Item("edunkid")), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                '            objED.Void(iEDUnkid, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                '        End If
                '    Else
                '        objED.Void(CInt(dRow.Item("edunkid")), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                '    End If
                'Next
                Dim drRow() As DataRow = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 ")
                Dim dtTable As DataTable = Nothing
                If drRow.Length > 0 Then
                    dtTable = drRow.CopyToDataTable
                    If objED.VoidAll(Nothing, FinancialYear._Object._YearUnkid, dtTable, mstrVoidReason, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, getIP(), getHostName(), mstrModuleName, -1, False) = False Then
                        If objED._Message <> "" Then
                            eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                            Exit Try
                            End If
                        End If
                    End If
                'Sohail (06 Sep 2019) -- End
                'Sohail (13 Jul 2019) -- End
                Call FillList()
                'Sohail (13 Jan 2014) -- End
                'S.SANDEEP [ 17 OCT 2012 ] -- END

                'Sohail (13 Jan 2014) -- Start
                'Enhancement - Multiple Delete on Earning Deduction
                'If lvED.Items.Count <= 0 Then
                '    Exit Try
                'End If

                'If lvED.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvED.Items.Count - 1
                '    lvED.Items(intSelectedIndex).Selected = True
                '    lvED.EnsureVisible(intSelectedIndex)
                'ElseIf lvED.Items.Count <> 0 Then
                '    lvED.Items(intSelectedIndex).Selected = True
                '    lvED.EnsureVisible(intSelectedIndex)
                'End If
                'Sohail (13 Jan 2014) -- End
            End If
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'lvED.Select()
            dgvED.Select()
            'Sohail (13 Jul 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objTranHead = Nothing
            'Sohail (24 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            objPeriod = Nothing
            objPayment = Nothing
            objTnALeaveTran = Nothing
            'Sohail (24 Aug 2012) -- End
            Cursor.Current = Cursors.Default
        End Try

    End Sub

    Private Sub btnOperations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOperations.Click, mnuBatchTransaction.Click
        'Sohail (11 Sep 2010) -- Start
        'If lvED.SelectedItems.Count < 1 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Earning Deduction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
        '    lvED.Select()
        '    Exit Sub
        'End If
        'Sohail (11 Sep 2010) -- End

        Dim objBatchEntry As New frmBatchEntry
        Try
            'Sohail (11 Sep 2010) -- Start
            'If objBatchEntry.DisplayDialog(CInt(lvED.Items(lvED.SelectedItems(0).Index).SubItems(1).Tag)) = True Then
            If objBatchEntry.DisplayDialog(enAction.ADD_ONE) = True Then 'Sohail (03 Nov 2010)
                'If objBatchEntry.DisplayDialog() = True Then
                'Sohail (11 Sep 2010) -- End
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnBatchTransaction_Click", mstrModuleName)
        Finally
            objBatchEntry = Nothing
        End Try
    End Sub

    Private Sub mnuGlobalAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
        Dim Frm As New frmGlobalAssignEarningDeduction
        Try
            If Frm.DisplayDialog(enAction.ADD_ONE) = True Then 'Sohail (03 Nov 2010)
                'If Frm.DisplayDialog = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuExemption_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExemption.Click
        Dim objFrm As New frmEmployee_Exemption_List
        Try

            objFrm.ShowDialog()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuExemption_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (14 Jun 2011) -- Start
    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnDisApprove.Click
        Dim strMsg As String = ""
        Dim blnIsApprove As Boolean
        Dim btn As Button = CType(sender, Button)
        If btn.Name = btnApprove.Name Then
            blnIsApprove = True
            strMsg = Language.getMessage(mstrModuleName, 9, "Are you sure you want to Approve selected ED Heads?")
        ElseIf btn.Name = btnDisApprove.Name Then
            blnIsApprove = False
            strMsg = Language.getMessage(mstrModuleName, 10, "Are you sure you want to Dispprove selected ED Heads?")
        End If
        'Sohail (13 Jul 2019) -- Start
        'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
        'If lvED.CheckedItems.Count <= 0 Then
        'Sohail (13 Jul 2019) -- Start
        'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
        'If lvED.CheckedItems.Count <= 0 Then
        If mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
            'Sohail (13 Jul 2019) -- End
            'Sohail (13 Jul 2019) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Check atleast one ED Heads to Approve/Disapprove."), enMsgBoxStyle.Information)
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'lvED.Focus()
            dgvED.Focus()
            'Sohail (13 Jul 2019) -- End
            Exit Sub
            'Sohail (13 Jan 2014) -- Start
            'Enhancement - Multiple Delete on Earning Deduction
            'ElseIf (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
            '    Exit Sub
            'Sohail (13 Jan 2014) -- End
        End If

        'Sohail (13 Jan 2014) -- Start
        'Enhancement - Multiple Delete on Earning Deduction
        'Sohail (13 Jul 2019) -- Start
        'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
        'Dim intSalaryHeads As Integer = (From p In lvED.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhTypeOf.Index).Tag) = enTypeOf.Salary) Select p).ToList.Count
        Dim intSalaryHeads As Integer = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 AND typeof_id = " & enTypeOf.Salary & " ").Length
        'Sohail (13 Jul 2019) -- End
        If intSalaryHeads > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove Salary Head(s)."), enMsgBoxStyle.Information)
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'lvED.Focus()
            dgvED.Focus()
            'Sohail (13 Jul 2019) -- End
            Exit Sub
        End If

        If (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
            Exit Sub
        End If

        'Sohail (06 Sep 2019) -- Start
        'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
        Dim frm As New frmRemark
        Dim strRemarks As String = ""
        frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 26, "Comments")

        If frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll) = False Then
            Exit Sub
        End If
        'Sohail (06 Sep 2019) -- End
        'Sohail (13 Jan 2014) -- End


        Dim blnResult As Boolean
        Dim blnRefresh As Boolean = False
        Try
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'For Each lvItem As ListViewItem In lvED.CheckedItems
            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            'For Each dRow As DataRow In mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 ")
            '    'Sohail (13 Jul 2019) -- End

            '    objED = New clsEarningDeduction
            '    With objED
            '        'Sohail (21 Aug 2015) -- Start
            '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '        '._Edunkid = CInt(lvItem.Tag)
            '        'Sohail (13 Jul 2019) -- Start
            '        'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            '        '._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = CInt(lvItem.Tag)
            '        ._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = CInt(dRow.Item("edunkid"))
            '        'Sohail (13 Jul 2019) -- End
            '        'Sohail (21 Aug 2015) -- End
            '        ._Isapproved = blnIsApprove
            '        ._Approveruserunkid = User._Object._Userunkid

            '        'Sohail (21 Aug 2015) -- Start
            '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '        'blnResult = .Update(False)
            '        blnResult = .Update(FinancialYear._Object._DatabaseName, False, Nothing, ConfigParameter._Object._CurrentDateAndTime)
            '        'Sohail (21 Aug 2015) -- End
            '        If blnResult = False AndAlso objED._Message <> "" Then
            '            eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
            '            Exit Try
            '        End If
            '        blnRefresh = True
            '    End With
            'Next

            'If blnRefresh = True Then
            '    Call FillList()
            'End If
            Dim drRow() As DataRow = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 ")
            Dim dtTable As DataTable = Nothing
            If drRow.Length > 0 Then
                dtTable = drRow.CopyToDataTable

                If blnIsApprove = True Then
                    If objED.ApproveAll(Nothing, dtTable, blnIsApprove, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, getIP(), getHostName(), mstrModuleName, -1, False) = False Then
                        If objED._Message <> "" Then
                            eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    Else
                        blnResult = True
                    End If
                ElseIf blnIsApprove = False Then
                    If objED.VoidAll(Nothing, FinancialYear._Object._YearUnkid, dtTable, strRemarks, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, getIP(), getHostName(), mstrModuleName, -1, False) = False Then
                        If objED._Message <> "" Then
                            eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    Else
                        blnResult = True
                    End If
                End If

                If blnResult = True Then
                Call FillList()

                    Call objED.SendMailToInitiator(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, User._Object._Userunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, dtTable, blnIsApprove, strRemarks, GUI.fmtCurrency, enLogin_Mode.DESKTOP, mstrModuleName, 0)
            End If
            End If
            'Sohail (06 Sep 2019) -- End

        Catch ex As Exception
            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            'DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
            DisplayError.Show("-1", ex.Message, btn.Name & "_Click", mstrModuleName)
            'Sohail (06 Sep 2019) -- End
        End Try
    End Sub
    'Sohail (14 Jun 2011) -- End

    'Sohail (18 Nov 2016) -- Start
    'Enhancement -  64.1 - Insert Membership heads on Ed if not assigned.
    Private Sub mnuAssignMembershipHeads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssignMembershipHeads.Click
        Dim Frm As New frmAssignMembershipHeads
        Try
            If Frm.DisplayDialog(enAction.ADD_ONE) = True Then 'Sohail (03 Nov 2010)
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssignMembershipHeads_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Nov 2016) -- End

#End Region

    'Sohail (14 Jun 2011) -- Start
#Region " Listview's Events "

    'Sohail (13 Jul 2019) -- Start
    'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
    'Private Sub lvED_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)
    '    Try
    '        'Sohail (13 Jan 2014) -- Start
    '        'Enhancement - Multiple Delete on Earning Deduction
    '        'If CInt(lvED.Items(e.Index).SubItems(colhTypeOf.Index).Tag) = enTypeOf.Salary AndAlso e.NewValue = CheckState.Checked Then
    '        '    e.NewValue = CheckState.Unchecked
    '        'End If
    '        'Sohail (13 Jan 2014) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvED_ItemCheck", mstrModuleName)
    '    End Try
    'End Sub
    'Private Sub lvED_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        If lvED.CheckedItems.Count <= 0 Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvED.CheckedItems.Count < lvED.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvED.CheckedItems.Count = lvED.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Checked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvED_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (13 Jul 2019) -- End
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            'Sohail (13 Jul 2019) -- Start
            'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
            'Call CheckAllEdHeads(objchkSelectAll.Checked)
            If mdtView Is Nothing Then Exit Sub
            RemoveHandler dgvED.CellContentClick, AddressOf dgvED_CellContentClick
            For Each dr As DataRowView In mdtView
                dr.Item(objdgcolhCheck.DataPropertyName) = CBool(objchkSelectAll.CheckState)
            Next
            dgvED.Refresh()
            'Sohail (13 Feb 2020) -- Start
            'POWERSOFT Enhancement # 0004519 : To see total amount for checked employees before approving flat rate earning deductions.
            Dim decTotAmt As Decimal = (From p In mdtView.Table Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (CDec(p.Item("Amount")))).Sum
            objlblTotalAmt.Text = Format(decTotAmt, GUI.fmtCurrency)
            'Sohail (13 Feb 2020) -- End
            AddHandler dgvED.CellContentClick, AddressOf dgvED_CellContentClick
            'Sohail (13 Jul 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 Oct 2011) -- Start
    Private Sub chkIncludeInactiveEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeInactiveEmployee.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIncludeInactiveEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Oct 2011) -- End
#End Region
    'Sohail (14 Jun 2011) -- End

    'Sohail (13 Jul 2019) -- Start
    'Mainspring Resourcing Enhancement - Ref #  - 76.1 - 1.Global Approval for E &D  for all Add/Edit from MSS. 2. Set ED heads in pending status after importing update flat rate. 3. Notification for Approval with list of employees in one long email.
#Region " Datagridview's Events "

    Private Sub dgvED_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvED.CellContentClick
        Try
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If e.ColumnIndex = objdgcolhCheck.Index Then
                If Me.dgvED.IsCurrentCellDirty Then
                    Me.dgvED.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = Nothing
                If CBool(dgvED.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    drRow = mdtView.Table.Select(objdgcolhEmpid.DataPropertyName & " = '" & dgvED.Rows(e.RowIndex).Cells(objdgcolhEmpid.Index).Value.ToString() & "'", "")
                    If drRow.Length > 0 Then
                        For index As Integer = 0 To drRow.Length - 1
                            drRow(index)("isChecked") = CBool(dgvED.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                        Next
                    End If
                End If

                'Sohail (13 Feb 2020) -- Start
                'POWERSOFT Enhancement # 0004519 : To see total amount for checked employees before approving flat rate earning deductions.
                Dim decTotAmt As Decimal = (From p In mdtView.Table Where (CBool(p.Item("IsGrp")) = False AndAlso CBool(p.Item("IsChecked")) = True) Select (CDec(p.Item("Amount")))).Sum
                objlblTotalAmt.Text = Format(decTotAmt, GUI.fmtCurrency)
                'Sohail (13 Feb 2020) -- End

                drRow = mdtView.ToTable.Select("isChecked = true", "")

                If drRow.Length > 0 Then
                    If mdtView.ToTable.Rows.Count = drRow.Length Then
                        objchkSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                objchkSelectAll.CheckState = CheckState.Unchecked
                End If

            End If

                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvED_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvED_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvED.DataError

    End Sub

    Private Sub dgvED_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvED.CellFormatting
        Try
            If CBool(dgvED.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = False Then
                If e.ColumnIndex = dgcolhAmount.Index Then
                    'Hemant (20 Dec 2019) -- Start
                    'ISSUE (Mainspring Resourcing Ghana): On Earning & Deduction List Screen. Amount Column is showing nothing when amount is entered less than or equal to 0.5.
                    'If CInt(e.Value) = 0 Then
                    If CDec(e.Value) = 0 Then
                        'Hemant (20 Dec 2019) -- End
                        e.Value = ""
                        e.FormattingApplied = True
                    End If
                ElseIf e.ColumnIndex = dgcolhApprovePending.Index Then
                    If CBool(e.Value) = True Then
                        e.Value = "A"
                    Else
                        e.Value = "P"
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvED_CellFormatting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvED_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvED.CellPainting
        Try
            If e.RowIndex >= 0 AndAlso e.RowIndex < dgvED.RowCount - 1 AndAlso CBool(dgvED.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True AndAlso e.ColumnIndex > 0 Then
                If (e.ColumnIndex = objdgColhBlank.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim totWidth As Integer = 0
                    For i As Integer = 1 To dgvED.Columns.Count - 1
                        totWidth += dgvED.Columns(i).Width
                    Next
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, totWidth, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)

                    e.Graphics.DrawString(CType(dgvED.Rows(e.RowIndex).Cells(dgcolhEmployee.Index).Value.ToString & " : [" & dgvED.Rows(e.RowIndex).Cells(objdgcolhEmpCode.Index).Value.ToString & "]", String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)
               
                End If

                e.Handled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvED_CellPainting", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (13 Jul 2019) -- End

    'Sohail (13 Jan 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'mdtPeriodStartDate = Nothing
                'mdtPeriodEndDate = Nothing
                'Sohail (31 Mar 2016) -- Start
                'Issue - 58.1 - Assigned heads not coming for all periods when no period selected on ED list.
                'mdtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'mdtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriodStartDate = Nothing
                mdtPeriodEndDate = Nothing
                'Sohail (31 Mar 2016) -- End
                'Sohail (21 Aug 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            'If CInt(cboEmployee.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboEmployee)
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
            'Nilay (27-Oct-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboEmployee)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTranHead.KeyPress, cboCalcType.KeyPress
        'Sohail (30 Oct 2019) - [cboCalcType]
        Dim cbo As ComboBox = CType(sender, ComboBox) 'Sohail (30 Oct 2019)
        Try
            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
            'If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
            '    Dim frm As New frmCommonSearch
            '    With frm
            '        .ValueMember = cboTranHead.ValueMember
            '        .DisplayMember = cboTranHead.DisplayMember
            '        .DataSource = CType(cboTranHead.DataSource, DataTable)
            '        .CodeMember = "code"
            '    End With
            '    Dim c As Char = Convert.ToChar(e.KeyChar)
            '    frm.TypedText = c.ToString
            '    If frm.DisplayDialog Then
            '        cboTranHead.SelectedValue = frm.SelectedValue
            '        e.KeyChar = ChrW(Keys.ShiftKey)
            '    Else
            '        cboTranHead.Text = ""
            '    End If
            'End If
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboTranHead.Name Then
                    .CodeMember = "code"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
            'Sohail (30 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.SelectedIndexChanged, cboCalcType.SelectedIndexChanged
        'Sohail (30 Oct 2019) - [cboCalcType]
        'Sohail (30 Oct 2019) -- Start
        'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim cbo As ComboBox = CType(sender, ComboBox)
        'Sohail (30 Oct 2019) -- End
        Try
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            'If CInt(cboTranHead.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboTranHead)
            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
            'If CInt(cboTranHead.SelectedValue) < 0 Then Call SetDefaultSearchText(cboTranHead)
            If cbo.Name = cboCalcType.Name Then
                If mblnIsFromAppraisalScreen Then
                    dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , CInt(cboCalcType.SelectedValue), , , , , , , , 1, , , True)
                    'Hemant (03 Feb 2023) -- [blnAddLoanHeads:=True]
                Else
                    dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , CInt(cboCalcType.SelectedValue), , , , , , , , 0, , , True)
                    'Hemant (03 Feb 2023) -- [blnAddLoanHeads:=True]
                End If

                With cboTranHead
                    .BeginUpdate()
                    .ValueMember = "tranheadunkid"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("TranHead")
                    If .Items.Count > 0 Then .SelectedValue = 0
                    .EndUpdate()
                End With
                SetDefaultSearchText(cboTranHead)
            End If

            If CInt(cbo.SelectedValue) < 0 Then Call SetDefaultSearchText(cbo)
            'Sohail (30 Oct 2019) -- End
            'Nilay (27-Oct-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
        Finally
            objTranHead = Nothing
            'Sohail (30 Oct 2019) -- End
        End Try
    End Sub

    Private Sub cboTranHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.GotFocus, cboCalcType.GotFocus
        'Sohail (30 Oct 2019) - [cboCalcType]
        Dim cbo As ComboBox = CType(sender, ComboBox) 'Sohail (30 Oct 2019)
        Try
            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
            'With cboTranHead
            '    .ForeColor = Color.Black
            '    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

            '    If .Text = mstrSearchText Then
            '        .Text = ""
            '    End If
            'End With
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
            'Sohail (30 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.Leave, cboCalcType.Leave
        'Sohail (30 Oct 2019) - [cboCalcType]
        Dim cbo As ComboBox = CType(sender, ComboBox) 'Sohail (30 Oct 2019)
        Try
            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
            'If CInt(cboTranHead.SelectedValue) <= 0 Then
            '    Call SetDefaultSearchText(cboTranHead)
            'End If
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
            'Sohail (30 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Oct 2016) -- End
#End Region
    'Sohail (13 Jan 2012) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'End If
            'Sohail (06 Jan 2012) -- End



            With cboEmployee
                objfrm.DataSource = CType(.DataSource, DataTable) 'Anjan [10 June 2015] -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboTranHead.SelectedValue) <= 0 AndAlso CInt(cboEDApprovalStatus.SelectedValue) <> enEDHeadApprovalStatus.Pending Then
                '*** NOTE : This message is just for giving in language to show in Self Service
                'Sohail (11 Mar 2020) -- Start
                'Eko Supreme Enhancement # 0004596 : Allow to approve / reject all flat rate heads at a time on earning and deduction list screen in self service.
                'Dim strMsg As String = Language.getMessage(mstrModuleName, 27, "Please select Employee / Transaction head in order to do the futher operation on it.")
                Dim strMsg As String = Language.getMessage(mstrModuleName, 27, "Please select Employee / Transaction head / Calculation type in order to do the futher operation on it.")
                'Sohail (11 Mar 2020) -- End
            End If
            'Sohail (31 Dec 2019) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0
            'Sohail (02 Jul 2014) -- End
            'Sohail (14 Jun 2011) -- Start
            If cboTranHead.Items.Count > 0 Then cboTranHead.SelectedValue = 0
            If cboEDApprovalStatus.Items.Count > 0 Then cboEDApprovalStatus.SelectedIndex = 0
            'Sohail (14 Jun 2011) -- End
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (14 Jun 2011) -- Start
    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch

        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With cboTranHead
                objfrm.DataSource = CType(cboTranHead.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (14 Jun 2011) -- End

    'Sandeep [ 17 DEC 2010 ] -- Start
    'Sohail (14 Jun 2011) -- Start
    'Changes : 3 options (Approved, Pending, All) added for export.
    Private Sub mnuExportED_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApprovedEDHeads.Click, mnuPendingEDHeads.Click, mnuAllEDHeads.Click
        'Private Sub mnuExportED_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportED.Click
        Dim mnu As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        'Sohail (14 Jun 2011) -- End

        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsDataSet As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As FileInfo
        Dim dtTable As New DataTable
        Try
            If ConfigParameter._Object._ExportDataPath = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            dlgSaveFile.InitialDirectory = ConfigParameter._Object._ExportDataPath
            dlgSaveFile.Filter = "XML files (*.xml)|*.xml|Execl files(*.xlsx)|*.xlsx"
            dlgSaveFile.FilterIndex = 2 'Sohail (14 Jun 2011)
            path = ConfigParameter._Object._ExportDataPath & "\" & Company._Object._Code & "_"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(dlgSaveFile.FileName)

                'Sohail (18 Jul 2019) -- Start
                'Internal Enhancement - Ref #  - 76.1 - Path error if export data path does not exist.
                If IO.Directory.Exists(ConfigParameter._Object._ExportDataPath) = False Then
                    path = ObjFile.DirectoryName & "\"
                End If
                'Sohail (18 Jul 2019) -- End

                'Anjan (29 Jan 2010)-Start
                'Issue : This to save the file on path other than set from configuration.
                'If dlgSaveFile.InitialDirectory <> dlgSaveFile.FileName.Substring(0, dlgSaveFile.FileName.LastIndexOf("\")) Then
                '    strFilePath = dlgSaveFile.FileName.Substring(0, dlgSaveFile.FileName.LastIndexOf("\")) & "\" & ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'Else
                'Sohail (18 Jul 2019) -- Start
                'Internal Enhancement - Ref #  - 76.1 - Path error if export data path does not exist.
                'strFilePath = path & ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath = path & ObjFile.Name.Substring(0, ObjFile.Name.Length - 5) & "_" & eZeeDate.convertDate(Now)
                'Sohail (18 Jul 2019) -- End
                'End If
                'Anjan (29 Jan 2010)-End

                strFilePath &= ObjFile.Extension
                'Sohail (14 Jun 2011) -- Start
                'dsDataSet = objED.GetList("ED")
                If mnu.Name = mnuApprovedEDHeads.Name Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsDataSet = objED.GetList("ED", , , , , , , , , , , , , , enEDHeadApprovalStatus.Approved)
                    'Sohail (31 Mar 2016) -- Start
                    'Issue - 58.1 - Assigned heads not coming for all periods when no period selected on ED list.
                    'dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.Approved, , , mdtPeriodEndDate)
                    'Sohail (29 Mar 2017) -- Start
                    'Issue - 65.2 - no heads were exported.
                    'dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.Approved, , , )
                    dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.Approved, , , mdtPeriodEndDate)
                    'Sohail (29 Mar 2017) -- End
                    'Sohail (31 Mar 2016) -- End
                    'Sohail (21 Aug 2015) -- End
                ElseIf mnu.Name = mnuPendingEDHeads.Name Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsDataSet = objED.GetList("ED", , , , , , , , , , , , , , enEDHeadApprovalStatus.Pending)
                    'Sohail (31 Mar 2016) -- Start
                    'Issue - 58.1 - Assigned heads not coming for all periods when no period selected on ED list.
                    'dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.Pending, , , mdtPeriodEndDate)
                    'Sohail (29 Mar 2017) -- Start
                    'Issue - 65.2 - no heads were exported.
                    'dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.Pending, , , )
                    dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.Pending, , , mdtPeriodEndDate)
                    'Sohail (29 Mar 2017) -- End
                    'Sohail (31 Mar 2016) -- End
                    'Sohail (21 Aug 2015) -- End
                Else 'All
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsDataSet = objED.GetList("ED", , , , , , , , , , , , , , enEDHeadApprovalStatus.All)
                    'Sohail (31 Mar 2016) -- Start
                    'Issue - 58.1 - Assigned heads not coming for all periods when no period selected on ED list.
                    'dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.All, , , mdtPeriodEndDate)
                    'Sohail (29 Mar 2017) -- Start
                    'Issue - 65.2 - no heads were exported.
                    'dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.All, , , )
                    dsDataSet = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "ED", True, , , , enEDHeadApprovalStatus.All, , , mdtPeriodEndDate)
                    'Sohail (29 Mar 2017) -- End
                    'Sohail (31 Mar 2016) -- End
                    'Sohail (21 Aug 2015) -- End
                End If

                'dtTable = New DataView(dsDataSet.Tables(0), "calctype_id = 7", "", DataViewRowState.CurrentRows).ToTable
                dtTable = New DataView(dsDataSet.Tables(0), "calctype_id = " & enCalcType.FlatRate_Others & "", "", DataViewRowState.CurrentRows).ToTable
                'Sohail (14 Jun 2011) -- End
                dsDataSet.Tables.Clear()
                dsDataSet.Tables.Add(dtTable)
                If dsDataSet.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no data to Export."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Select Case dlgSaveFile.FilterIndex
                    'Case 1   'CSV
                    '    Dim objCsvExport As New clsExportData
                    '    ObjFile = New FileInfo(strFilePath)
                    '    objCsvExport.ExportToCSV(dsDataSet.Tables(0), strFilePath)
                    Case 1 'XML
                        dsDataSet.WriteXml(strFilePath)
                    Case 2  'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsDataSet)
                        OpenXML_Export(strFilePath, dsDataSet)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "File Exported Successfully to Export Data Path."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportED_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportED_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportED.Click
        Dim frm As New frmImportExportHeads
        Try
            If frm.displayDialog() = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "File Imported Successfully."), enMsgBoxStyle.Information)
                ' FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportED_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 

    'Sohail (23 Apr 2011) -- Start
    Private Sub mnuImporUpdatetFlatRateTran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImporUpdatetFlatRateTran.Click
        Dim frm As New frmImportUpdateFlatRateHeads
        Try
            If frm.displayDialog() = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "File Imported Successfully."), enMsgBoxStyle.Information)
                'FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImporUpdatetFlatRateTran_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Apr 2011) -- End

    'Sohail (21 Dec 2016) -- Start
    'ACB Enhancement - 64.1 - Loan data Integration with T24 ACB System.
    Private Sub mnuUpdatetT24ACBLoanData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdatetT24ACBLoanData.Click
        Dim frm As New frmUpdateEDT24ACBLoanData
        Try
            If frm.displayDialog() = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "T24 Loan Data Imported Successfully."), enMsgBoxStyle.Information)
                'FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUpdatetT24ACBLoanData", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Dec 2016) -- End

    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 


    'Hemant (10 Nov 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube.
    Private Sub mnuUpdatetFlexcubeLoanData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdatetFlexcubeLoanData.Click
        Dim frm As New frmUpdateFlexcubeLoanData
        Try
            If frm.displayDialog() = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1023, "Flexcube Loan Data Imported Successfully."), enMsgBoxStyle.Information)
                'FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUpdatetFlexcubeLoanData", mstrModuleName)
        End Try
    End Sub
    'Hemant (10 Nov 2022) -- End

    'Pinkal (14-Dec-2022) -- Start
    'NMB Loan Module Enhancement.
    Private Sub mnuExportFlexcubeLoanData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportFlexcubeLoanData.Click
        Dim frm As New frmExportFlexcubeLoanDetailData
        Try
            frm.displayDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportFlexcubeLoanData_Click", mstrModuleName)
        End Try
    End Sub
    'Pinkal (14-Dec-2022) -- End

    

#End Region

#Region " Message List "
    '1, "Please select Earning Deduction from the list to perform further operation on it."
    '2, "Are you sure you want to delete this Earning Deduction?"
    '3, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."
    '4, "File Exported Successfully to Export Data Path."
    '5, "File Imported Successfully."
    '6, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."
    '7, "There is no data to Export."
    '8, "Please Check atleast one ED Heads to Approve/Disapprove."
    '9, "Are you sure you want to Approve selected ED Heads?"
    '10, "Are you sure you want to Dispprove selected ED Heads?"
    '11, "Sorry, This Earning Deduction is in used."
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEDList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEDList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnDisApprove.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDisApprove.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprove.GradientBackColor = GUI._ButttonBackColor 
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuBatchTransaction.Text = Language._Object.getCaption(Me.mnuBatchTransaction.Name, Me.mnuBatchTransaction.Text)
            Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)
            Me.mnuExemption.Text = Language._Object.getCaption(Me.mnuExemption.Name, Me.mnuExemption.Text)
            Me.mnuExportED.Text = Language._Object.getCaption(Me.mnuExportED.Name, Me.mnuExportED.Text)
            Me.mnuImportED.Text = Language._Object.getCaption(Me.mnuImportED.Name, Me.mnuImportED.Text)
            Me.mnuImporUpdatetFlatRateTran.Text = Language._Object.getCaption(Me.mnuImporUpdatetFlatRateTran.Name, Me.mnuImporUpdatetFlatRateTran.Text)
            Me.lblApprovalStatus.Text = Language._Object.getCaption(Me.lblApprovalStatus.Name, Me.lblApprovalStatus.Text)
            Me.btnDisApprove.Text = Language._Object.getCaption(Me.btnDisApprove.Name, Me.btnDisApprove.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
            Me.mnuApprovedEDHeads.Text = Language._Object.getCaption(Me.mnuApprovedEDHeads.Name, Me.mnuApprovedEDHeads.Text)
            Me.mnuPendingEDHeads.Text = Language._Object.getCaption(Me.mnuPendingEDHeads.Name, Me.mnuPendingEDHeads.Text)
            Me.mnuAllEDHeads.Text = Language._Object.getCaption(Me.mnuAllEDHeads.Name, Me.mnuAllEDHeads.Text)
            Me.gbEDList.Text = Language._Object.getCaption(Me.gbEDList.Name, Me.gbEDList.Text)
            Me.chkIncludeInactiveEmployee.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmployee.Name, Me.chkIncludeInactiveEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.ToolStripMenuItem1.Text = Language._Object.getCaption(Me.ToolStripMenuItem1.Name, Me.ToolStripMenuItem1.Text)
            Me.mnuAssignMembershipHeads.Text = Language._Object.getCaption(Me.mnuAssignMembershipHeads.Name, Me.mnuAssignMembershipHeads.Text)
            Me.mnuUpdatetT24ACBLoanData.Text = Language._Object.getCaption(Me.mnuUpdatetT24ACBLoanData.Name, Me.mnuUpdatetT24ACBLoanData.Text)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
			Me.dgcolhTrnHeadCode.HeaderText = Language._Object.getCaption(Me.dgcolhTrnHeadCode.Name, Me.dgcolhTrnHeadCode.HeaderText)
			Me.dgcolhTrnHead.HeaderText = Language._Object.getCaption(Me.dgcolhTrnHead.Name, Me.dgcolhTrnHead.HeaderText)
			Me.dgcolhTranHeadType.HeaderText = Language._Object.getCaption(Me.dgcolhTranHeadType.Name, Me.dgcolhTranHeadType.HeaderText)
			Me.dgcolhTypeOf.HeaderText = Language._Object.getCaption(Me.dgcolhTypeOf.Name, Me.dgcolhTypeOf.HeaderText)
			Me.dgcolhCalcType.HeaderText = Language._Object.getCaption(Me.dgcolhCalcType.Name, Me.dgcolhCalcType.HeaderText)
			Me.dgcolhPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhPeriod.Name, Me.dgcolhPeriod.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhApprovePending.HeaderText = Language._Object.getCaption(Me.dgcolhApprovePending.Name, Me.dgcolhApprovePending.HeaderText)
			Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
			Me.lblTotalAmt.Text = Language._Object.getCaption(Me.lblTotalAmt.Name, Me.lblTotalAmt.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Earning Deduction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete ticked Earning Deduction?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee.")
            Language.setMessage(mstrModuleName, 4, "File Exported Successfully to Export Data Path.")
            Language.setMessage(mstrModuleName, 5, "File Imported Successfully.")
            Language.setMessage(mstrModuleName, 6, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 7, "There is no data to Export.")
            Language.setMessage(mstrModuleName, 8, "Please Check atleast one ED Heads to Approve/Disapprove.")
            Language.setMessage(mstrModuleName, 9, "Are you sure you want to Approve selected ED Heads?")
            Language.setMessage(mstrModuleName, 10, "Are you sure you want to Dispprove selected ED Heads?")
            Language.setMessage(mstrModuleName, 11, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed.")
            Language.setMessage(mstrModuleName, 15, "Please Tick atleast one Earning Deduction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 16, "Sorry! You can not Delete ticked transaction. Reason: Period is closed for some of the transactions.")
            Language.setMessage(mstrModuleName, 17, "Sorry, You can not Delete ticked Transaction Head(s). Reason : Payment is already done for current open Period. Please Delete payment first to Delete ticked transaction head.")
            Language.setMessage(mstrModuleName, 18, "Sorry, You can not Delete ticked Transaction Head. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete ticked transaction head.")
            Language.setMessage(mstrModuleName, 19, "Sorry, You can not Delete some of the ticked Salary Head(s). Please go to Employee Master to Delete ticked Salary Head for some of the employee(s).")
            Language.setMessage(mstrModuleName, 20, "Sorry, You can not Delete some of the ticked Salary Head(s) when other Heads are assigned. Please delete other Heads First.")
            Language.setMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove Salary Head(s).")
            Language.setMessage(mstrModuleName, 22, "Type to Search")
            Language.setMessage(mstrModuleName, 23, "T24 Loan Data Imported Successfully.")
			Language.setMessage(mstrModuleName, 24, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 25, "Sorry, You can not Delete ticked Transaction Head(s). Reason : Some transaction heads are in pending status.")
			Language.setMessage(mstrModuleName, 26, "Comments")
            Language.setMessage(mstrModuleName, 27, "Please select Employee / Transaction head / Calculation type in order to do the futher operation on it.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

  
End Class