﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Net

#End Region

Public Class frmExportFlexcubeLoanDetailData

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmExportFlexcubeLoanDetailData"
    Private mblnCancel As Boolean = True
    Private mdtTable As New DataTable
    Private mdtFilteredTable As DataTable
    Private m_Dataview As DataView
    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    Private mintCount As Integer = 0
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mstrSearchEmpText As String = ""
    Private mstrSearchText As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objBankGrp As New clspayrollgroup_master
        Dim objHead As New clsTransactionHead
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable
        Try
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open)
            dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable

            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboPayPeriod)

            dsCombo = objBankGrp.getListForCombo(enGroupType.BankGroup, "BankGrp", True)
            With cboBankGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("BankGrp")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboBankGroup)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objBankGrp = Nothing
            objHead = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As New DataTable

        objlblEmpCount.Text = "( 0 / 0 )"
        mintCount = 0

        Try
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then Exit Try

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND premployee_bank_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboBankGroup.SelectedValue) > 0 Then
                StrSearching &= "AND cfpayrollgroup_master.groupmasterunkid = " & CInt(cboBankGroup.SelectedValue) & " "
            End If

            If CInt(cboBankBranch.SelectedValue) > 0 Then
                StrSearching &= "AND premployee_bank_tran.branchunkid = " & CInt(cboBankBranch.SelectedValue) & " "
            End If

            If mstrAdvanceFilter.Length > 0 Then
                mstrAdvanceFilter &= "AND " & mstrAdvanceFilter
            End If

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , mstrAdvanceFilter, , mdtPayPeriodEndDate, "EmpName, end_date DESC, BankGrp, BranchName", StrSearching)
            dtTable = New DataView(dsList.Tables(0)).ToTable

            mintTotalEmployee = dtTable.Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvEmployee = dtTable.DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgEcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmployeeCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "EmpName"
            dgColhBankGroup.DataPropertyName = "BankGrp"
            dgColhBankBranch.DataPropertyName = "BranchName"
            dgColhAccountNo.DataPropertyName = "accountno"
            objdgColhEmpBankTranUnkId.DataPropertyName = "empbanktranunkid"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, EmpName "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       mdtPayPeriodStartDate, _
                                       mdtPayPeriodEndDate, _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, False, "Employee", True, , , , , , , , , , , , , , True, mstrAdvanceFilter)

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
            Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 2, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpComboText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = Language.getMessage(mstrModuleName, 1, "Type to Search Emp. Code, Emp. Name, Bank, Branch, A/C No.")
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Try
            dgvExportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhEmpCode.DataPropertyName = "employeecode"
            dgcolhEmpName.DataPropertyName = "employeename"
            dgcolhTranHead.DataPropertyName = "trnheadname"
            dgcolhLnAcNo.DataPropertyName = "LoanAcNo"
            dgcolhLoanSchemeCode.DataPropertyName = "Productcode"
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhRemarks.DataPropertyName = "rowtype_remark"

            dgvExportInfo.DataSource = m_Dataview
            dgvExportInfo.Refresh()

            objlblLoanDataCount.Text = "( 0 / " & m_Dataview.Count & " )"

            RemoveHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            objchkISelectAll.Checked = False
            AddHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkISelectAll.Checked
            Next

            Dim intCnt As Integer = m_Dataview.ToTable.Select("IsChecked = 1").Length

            objlblLoanDataCount.Text = "( " & intCnt.ToString & " / " & m_Dataview.Count & " )"

            dgvExportInfo.DataSource = m_Dataview
            dgvExportInfo.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Period."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            ElseIf dvEmployee.Table.Select("IsChecked = 1 ").Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one employee."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidated", mstrModuleName)
        End Try
    End Function

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try

            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExportFlexcubeLoanDetailData_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            picExpand.Tag = "Collapsed"

            Call FillCombo()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExportFlexcubeLoanDetailData_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons Events "

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click
        Dim objED As New clsEarningDeduction
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objTransactionHead As New clsTransactionHead
        Dim objloanAdvance As New clsLoan_Advance
        Dim dtTable As DataTable
        Dim dtLoanScheme As DataTable
        Dim dsTransactionHead As New DataSet
        Dim intCnt As Integer = 0
        Try

            If IsValidated() = False Then Exit Sub

            dtLoanScheme = New DataView(objLoanScheme.GetList("List", True, True).Tables(0), "ispostingtoflexcube = 1", "", DataViewRowState.CurrentRows).ToTable
            dsTransactionHead = objTransactionHead.GetList("TranHead", -1, -1, , True, CInt(enTranHeadActiveInActive.ACTIVE), , , , True, True, 0, , , , True)
            dtTable = dvEmployee.Table.Select("IsChecked = 1 ").CopyToDataTable.DefaultView.ToTable(True, "employeeunkid", "employeecode", "EmpName", "accountno")

            mdtTable = objED._DataSource

            If mdtTable IsNot Nothing Then
                Dim dcColumn As DataColumn
                If mdtTable.Columns.Contains("Productcode") = False Then
                    dcColumn = New DataColumn("Productcode")
                    dcColumn.DataType = Type.GetType("System.String")
                    dcColumn.DefaultValue = ""
                    mdtTable.Columns.Add(dcColumn)
                End If
                dcColumn = Nothing

                If mdtTable.Columns.Contains("LoanAcNo") = False Then
                    dcColumn = New DataColumn("LoanAcNo")
                    dcColumn.DataType = Type.GetType("System.String")
                    dcColumn.DefaultValue = ""
                    mdtTable.Columns.Add(dcColumn)
                End If
                dcColumn = Nothing
            End If

            If m_Dataview IsNot Nothing Then m_Dataview.Table.Clear()

            objlblLoanDataCount.Text = "( 0 / " & (mintCount * 2).ToString & " )"
            Application.DoEvents()

            For Each dtRow As DataRow In dtTable.Rows

                Dim dsFlexcubeData As DataSet = objloanAdvance.GetFlexcubeLoanData(ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword, dtRow.Item("accountno").ToString, , , True)
                'Hemant (01 Sep 2023) -- [blnUseOldView := True]
                For Each drFlexLoanData As DataRow In dsFlexcubeData.Tables(0).Rows
                    Dim dr As DataRow = mdtTable.NewRow
                    Dim dr2 As DataRow = mdtTable.NewRow

                    dr.Item("edunkid") = 0
                    dr.Item("employeeunkid") = dtRow.Item("employeeunkid")
                    dr.Item("employeecode") = dtRow.Item("employeecode")
                    dr.Item("employeename") = dtRow.Item("EmpName")
                    dr.Item("Productcode") = drFlexLoanData.Item("PRODUCT_CODE")
                    dr.Item("LoanAcNo") = drFlexLoanData.Item("ACCOUNT_NUMBER")
                    dr.Item("currencyId") = 0
                    dr.Item("isvoid") = 0
                    dr.Item("voiduserunkId") = -1
                    dr.Item("voiddatetime") = DBNull.Value
                    dr.Item("voidreason") = ""
                    dr.Item("formula") = ""
                    dr.Item("formulaid") = ""
                    dr.Item("periodunkid") = CInt(cboPayPeriod.SelectedValue)
                    dr.Item("period_name") = cboPayPeriod.Text
                    dr.Item("start_date") = CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString
                    dr.Item("end_date") = CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString

                    dr.Item("rowtypeid") = 0

                    dr2.ItemArray = dr.ItemArray

                    Dim intHeadID As Integer = 0
                    Dim strHeadName As String = String.Empty
                    Dim intHeadTypeId As Integer
                    Dim intHeadTypeOfId As Integer
                    Dim intHeadCalcTypeId As Integer
                    Dim intHeadComputeOnId As Integer
                    For i As Integer = 0 To 1
                        intCnt += 1
                        objlblLoanDataCount.Text = "( 0 / " & intCnt.ToString & " )"
                        Application.DoEvents()

                        If i = 0 Then
                            intHeadID = 0
                            strHeadName = ""
                            intHeadTypeId = 0
                            intHeadTypeOfId = 0
                            Dim drLoanScheme() As DataRow = dtLoanScheme.Select("code ='" & drFlexLoanData.Item("PRODUCT_CODE").ToString & "'")
                            If drLoanScheme.Length > 0 Then
                                Dim drTransactionHead() As DataRow = dsTransactionHead.Tables(0).Select("loanschemeunkid =" & CInt(drLoanScheme(0).Item("loanschemeunkid").ToString) & " AND calctype_id = " & CInt(enCalcType.LOAN_INSTALLMENT) & " ")
                                If drTransactionHead.Length <= 0 Then
                                    dr.Item("rowtypeid") = 3 'Failure
                                    dr.Item("rowtype_remark") = drFlexLoanData.Item("PRODUCT_CODE").ToString & " " & Language.getMessage(mstrModuleName, 10, "Installment Head is not assigned to this Loan Scheme.")
                                Else
                                    intHeadID = CInt(drTransactionHead(0).Item("tranheadunkid"))
                                    strHeadName = drTransactionHead(0).Item("trnheadname").ToString
                                    intHeadTypeId = CInt(drTransactionHead(0).Item("trnheadtype_id"))
                                    intHeadTypeOfId = CInt(drTransactionHead(0).Item("typeof_id"))
                                    intHeadCalcTypeId = CInt(drTransactionHead(0).Item("calctype_id"))
                                    intHeadComputeOnId = CInt(drTransactionHead(0).Item("computeon_id"))
                                End If
                            Else
                                dr.Item("rowtypeid") = 3 'Failure
                                dr.Item("rowtype_remark") = drFlexLoanData.Item("PRODUCT_CODE").ToString & " " & Language.getMessage(mstrModuleName, 5, "LOAN SCHEME CODE is not available to this PRODUCT CODE.")
                            End If

                            dr.Item("amount") = Format(CDec(IIf(IsDBNull(drFlexLoanData.Item("INSTALMENT_AMOUNT")) OrElse drFlexLoanData.Item("INSTALMENT_AMOUNT").ToString().Trim.Length <= 0, 0, drFlexLoanData.Item("INSTALMENT_AMOUNT"))), GUI.fmtCurrency)
                            dr.Item("tranheadunkid") = intHeadID
                            dr.Item("trnheadname") = strHeadName
                            dr.Item("trnheadtype_id") = intHeadTypeId
                            dr.Item("trnheadtype") = strHeadName
                            dr.Item("typeof_id") = intHeadTypeOfId
                            dr.Item("calctype_id") = intHeadCalcTypeId
                            dr.Item("computeon_id") = intHeadComputeOnId
                            If intHeadTypeId = enTranHeadType.EarningForEmployees Then
                                dr.Item("isdeduct") = 0
                            ElseIf intHeadTypeId = enTranHeadType.DeductionForEmployee OrElse intHeadTypeId = enTranHeadType.EmployeesStatutoryDeductions Then
                                dr.Item("isdeduct") = 1
                            Else
                                dr.Item("isdeduct") = 0
                            End If

                        Else
                            intHeadID = 0
                            strHeadName = ""
                            intHeadTypeId = 0
                            intHeadTypeOfId = 0
                            Dim drLoanScheme() As DataRow = dtLoanScheme.Select("code ='" & drFlexLoanData.Item("PRODUCT_CODE").ToString & "'")
                            If drLoanScheme.Length > 0 Then
                                Dim drTransactionHead() As DataRow = dsTransactionHead.Tables(0).Select("loanschemeunkid =" & CInt(drLoanScheme(0).Item("loanschemeunkid").ToString) & " AND calctype_id = " & CInt(enCalcType.LOAN_BALANCE) & " ")
                                If drTransactionHead.Length <= 0 Then
                                    dr2.Item("rowtypeid") = 3 'Failure
                                    dr2.Item("rowtype_remark") = drFlexLoanData.Item("PRODUCT_CODE").ToString & " " & Language.getMessage(mstrModuleName, 11, "Loan Balance Head is not assigned to this Loan Scheme.")
                                Else
                                    intHeadID = CInt(drTransactionHead(0).Item("tranheadunkid"))
                                    strHeadName = drTransactionHead(0).Item("trnheadname").ToString
                                    intHeadTypeId = CInt(drTransactionHead(0).Item("trnheadtype_id"))
                                    intHeadTypeOfId = CInt(drTransactionHead(0).Item("typeof_id"))
                                    intHeadCalcTypeId = CInt(drTransactionHead(0).Item("calctype_id"))
                                    intHeadComputeOnId = CInt(drTransactionHead(0).Item("computeon_id"))
                                End If
                            Else
                                dr2.Item("rowtypeid") = 3 'Failure
                                dr2.Item("rowtype_remark") = drFlexLoanData.Item("PRODUCT_CODE").ToString & " " & Language.getMessage(mstrModuleName, 5, "LOAN SCHEME CODE is not available to this PRODUCT CODE.")
                            End If

                            dr2.Item("amount") = Format(CDec(IIf(IsDBNull(drFlexLoanData.Item("PRINCIPALOUTSTANDING")) OrElse drFlexLoanData.Item("PRINCIPALOUTSTANDING").ToString().Trim.Length <= 0, 0, drFlexLoanData.Item("PRINCIPALOUTSTANDING"))), GUI.fmtCurrency)
                            dr2.Item("tranheadunkid") = intHeadID
                            dr2.Item("trnheadname") = strHeadName
                            dr2.Item("trnheadtype_id") = intHeadTypeId
                            dr2.Item("trnheadtype") = ""
                            dr2.Item("typeof_id") = intHeadTypeOfId
                            dr2.Item("calctype_id") = intHeadCalcTypeId
                            dr2.Item("computeon_id") = intHeadComputeOnId
                            If intHeadTypeId = enTranHeadType.EarningForEmployees Then
                                dr2.Item("isdeduct") = 0
                            ElseIf intHeadTypeId = enTranHeadType.DeductionForEmployee OrElse intHeadTypeId = enTranHeadType.EmployeesStatutoryDeductions Then
                                dr2.Item("isdeduct") = 1
                            Else
                                dr2.Item("isdeduct") = 0
                            End If
                        End If

                        If i = 0 Then
                            mdtTable.Rows.Add(dr)
                        Else
                            mdtTable.Rows.Add(dr2)
                        End If

                    Next
                Next


            Next

            m_Dataview = New DataView(mdtTable)
            m_Dataview.Sort = "rowtypeid, employeename "
            m_Dataview.RowFilter = "rowtypeid <> 0 AND rowtypeid <> 2 "

            If m_Dataview.Count <= 0 Then
                m_Dataview.RowFilter = "rowtypeid = 0"
            End If

            Call FillGirdView()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGetData_Click", mstrModuleName)
        Finally
            objED = Nothing
            objLoanScheme = Nothing
            objTransactionHead = Nothing
            objloanAdvance = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If mdtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot export data. Reason :There is no transaction to export."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            m_Dataview.RowFilter = "rowtypeid = 0 "
            mdtFilteredTable = m_Dataview.ToTable

            If mdtFilteredTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot export this file. Reason :Some transaction heads are not assigned to selected employees."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If mdtFilteredTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Tick atleast one transaction from list to export."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            If dvEmployee Is Nothing Then Exit Try

            If dvEmployee.Table.Select("IsChecked = 1 ").Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are you sure, you want to Export Loan Data to selected Employee(s)?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.None Then
                Exit Try
            End If

            Dim csv As String = ""

            ' START FOR DATAGRID HEADER
            For Each column As DataGridViewColumn In dgvExportInfo.Columns
                If column.CellType.FullName = "System.Windows.Forms.DataGridViewCheckBoxCell" OrElse column.Visible = False Then Continue For
                csv &= column.HeaderText & ","c
            Next
            ' END FOR DATAGRID HEADER

            'ADDING NEW LINE
            csv &= vbCr & vbLf

            ' START FOR DATAGRID ROW
            For Each row As DataGridViewRow In dgvExportInfo.Rows
                For Each cell As DataGridViewCell In row.Cells
                    If dgvExportInfo.Columns(cell.ColumnIndex).CellType.FullName = "System.Windows.Forms.DataGridViewCheckBoxCell" OrElse cell.Visible = False Then Continue For
                    csv &= cell.Value.ToString().Replace(",", ";") & ","c
                Next
                csv &= vbCr & vbLf  'ADDING NEW LINE
            Next
            ' END FOR DATAGRID ROW


            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "CSV files(*.csv)|*.csv"

            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim tw As New System.IO.StreamWriter(savDialog.FileName)
                tw.Write(csv)
                tw.Close()
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "All selected transaction(s) exported successfully."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))
            End If

            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click ", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Combobox Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            If CInt(cboPayPeriod.SelectedValue) < 0 Then Call SetDefaultSearchText(cboPayPeriod)

            Call FillEmployeeCombo()

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus _
                                                                                               , cboBankGroup.GotFocus _
                                                                                               , cboBankBranch.GotFocus _
                                                                                               , cboPayPeriod.GotFocus
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If

            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress _
                                                                                                                      , cboBankGroup.KeyPress _
                                                                                                                      , cboBankBranch.KeyPress _
                                                                                                                      , cboPayPeriod.KeyPress
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave _
                                                                                             , cboBankGroup.Leave _
                                                                                             , cboBankBranch.Leave _
                                                                                             , cboPayPeriod.Leave
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkISelectAll.CheckedChanged
        Try
            Call CheckAll(objchkISelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim objBranch As New clsbankbranch_master
        Dim dsCombos As DataSet
        Try
            If CInt(cboBankGroup.SelectedValue) > 0 Then
                dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            Else
                dsCombos = objBranch.getListForCombo("Branch", True)
            End If
            With cboBankBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
                .SelectedValue = 0
            End With
            If CInt(cboBankGroup.SelectedValue) < 0 Then Call SetDefaultSearchText(cboBankGroup)
            Call SetDefaultSearchText(cboBankBranch)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objBranch = Nothing
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgEcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExportInfo.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                Dim intCnt As Integer = m_Dataview.ToTable.Select("IsChecked = 1").Length

                objlblLoanDataCount.Text = "( " & intCnt.ToString & " / " & m_Dataview.Count & " )"

                RemoveHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                If intCnt <= 0 Then
                    objchkISelectAll.CheckState = CheckState.Unchecked
                ElseIf intCnt < dgvExportInfo.Rows.Count Then
                    objchkISelectAll.CheckState = CheckState.Indeterminate
                ElseIf intCnt = dgvExportInfo.Rows.Count Then
                    objchkISelectAll.CheckState = CheckState.Checked
                End If

                AddHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvExportInfo.CurrentCellDirtyStateChanged
        Try
            If dgvExportInfo.IsCurrentCellDirty Then
                dgvExportInfo.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other control's Events "

    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR EmpName LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR BankGrp LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR BranchName LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR accountno LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            If cboPayPeriod.Items.Count > 0 Then cboPayPeriod.SelectedValue = 0
            If cboBankGroup.Items.Count > 0 Then cboBankGroup.SelectedValue = 0
            If cboBankBranch.Items.Count > 0 Then cboBankBranch.SelectedValue = 0
            If txtSearchEmp.Text.Length > 0 Then txtSearchEmp.Text = ""

            mstrAdvanceFilter = ""
            dvEmployee = Nothing
            m_Dataview = Nothing
            dgEmployee.DataSource = Nothing
            dgvExportInfo.DataSource = Nothing
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub picExpand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picExpand.Click
        Try
            If picExpand.Tag.ToString = "Collapsed" Then
                picExpand.Image = My.Resources.left_arrow_16
                gbEmployeeList.Width = 591
                picExpand.Tag = "Expanded"

            Else
                picExpand.Image = My.Resources.right_arrow_16
                gbEmployeeList.Width = 241
                picExpand.Tag = "Collapsed"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "picExpand_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub mnuShowUnSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowUnSuccessful.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid <> 0 AND rowtypeid <> 2  "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowUnSuccessful_Click", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnGetData.GradientBackColor = GUI._ButttonBackColor
            Me.btnGetData.GradientForeColor = GUI._ButttonFontColor

            Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblBankBranch.Text = Language._Object.getCaption(Me.lblBankBranch.Name, Me.lblBankBranch.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.dgColhEmployeeCode.HeaderText = Language._Object.getCaption(Me.dgColhEmployeeCode.Name, Me.dgColhEmployeeCode.HeaderText)
            Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
            Me.dgColhBankGroup.HeaderText = Language._Object.getCaption(Me.dgColhBankGroup.Name, Me.dgColhBankGroup.HeaderText)
            Me.dgColhBankBranch.HeaderText = Language._Object.getCaption(Me.dgColhBankBranch.Name, Me.dgColhBankBranch.HeaderText)
            Me.dgColhAccountNo.HeaderText = Language._Object.getCaption(Me.dgColhAccountNo.Name, Me.dgColhAccountNo.HeaderText)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnGetData.Text = Language._Object.getCaption(Me.btnGetData.Name, Me.btnGetData.Text)
            Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
            Me.mnuShowUnSuccessful.Text = Language._Object.getCaption(Me.mnuShowUnSuccessful.Name, Me.mnuShowUnSuccessful.Text)
            Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
            Me.dgcolhEmpName.HeaderText = Language._Object.getCaption(Me.dgcolhEmpName.Name, Me.dgcolhEmpName.HeaderText)
            Me.dgcolhLnAcNo.HeaderText = Language._Object.getCaption(Me.dgcolhLnAcNo.Name, Me.dgcolhLnAcNo.HeaderText)
            Me.dgcolhLoanSchemeCode.HeaderText = Language._Object.getCaption(Me.dgcolhLoanSchemeCode.Name, Me.dgcolhLoanSchemeCode.HeaderText)
            Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhRemarks.HeaderText = Language._Object.getCaption(Me.dgcolhRemarks.Name, Me.dgcolhRemarks.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Type to Search Emp. Code, Emp. Name, Bank, Branch, A/C No.")
            Language.setMessage(mstrModuleName, 2, "Type to Search")
            Language.setMessage(mstrModuleName, 3, "Please select Period.")
            Language.setMessage(mstrModuleName, 4, "Please select atleast one employee.")
            Language.setMessage(mstrModuleName, 5, "LOAN SCHEME CODE is not available to this PRODUCT CODE.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot export data. Reason :There is no transaction to export.")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot export this file. Reason :Some transaction heads are not assigned to selected employees.")
            Language.setMessage(mstrModuleName, 8, "Please Tick atleast one transaction from list to export.")
            Language.setMessage(mstrModuleName, 9, "Are you sure, you want to Export Loan Data to selected Employee(s)?")
            Language.setMessage(mstrModuleName, 10, "Installment Head is not assigned to this Loan Scheme.")
            Language.setMessage(mstrModuleName, 11, "Loan Balance Head is not assigned to this Loan Scheme.")
            Language.setMessage(mstrModuleName, 12, "All selected transaction(s) exported successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

