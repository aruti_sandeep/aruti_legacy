﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmAssignMembershipHeads

#Region " Private Variables "
    'Hemant (07 Jan 2019) -- Start
    'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
    'Private ReadOnly mstrModuleName As String = "frmProcessPayroll"
    Private ReadOnly mstrModuleName As String = "frmAssignMembershipHeads"
    'Hemant (07 Jan 2019) -- End
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    Private mstrSearchText As String = ""
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    Private mintCheckedEmployee As Integer = 0

    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPayPeriod.BackColor = GUI.ColorComp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable

        Try
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open)
            'Sohail (27 Jul 2020) -- Start
            'NMB Enhancement # : Allow to do unassigned membership before xx days of start date for future period if Allow Period Closer Before XX Days setting is ticked.
            'dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            If ConfigParameter._Object._AllowToClosePeriodBefore Then
                dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today.AddDays(CInt(ConfigParameter._Object._ClosePeriodDaysBefore) + 1)) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            Else
            dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            End If
            'Sohail (27 Jul 2020) -- End

            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            Call SetDefaultSearchText(cboPayPeriod)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            dtTable = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objED As New clsEarningDeduction
        Dim dsList As DataSet
        objlblEmpCount.Text = "( 0 / 0 )"
        mintCount = 0
        Try
            dgEmployee.DataSource = Nothing
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                Exit Try
            End If

            'Sohail (20 Jul 2018) -- Start
            'Support Issue # 0002383 : VOLTAMP: Unable to assign Transaction heads using batch entry in Payroll transactions (Warning message of Membership heads not assigned)in 73.1.
            'dsList = objED.GetList_UnAssigned_MembershipHeads(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "List", CInt(cboPayPeriod.SelectedValue), Nothing)
            dsList = objED.GetList_UnAssigned_MembershipHeads(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "List", CInt(cboPayPeriod.SelectedValue), Nothing, "")
            'Sohail (20 Jul 2018) -- End

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            mintTotalEmployee = dsList.Tables(0).Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            dvEmployee = dsList.Tables(0).DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"
            objdgcolhMembershiptranunkid.DataPropertyName = "membershiptranunkid"
            dgcolhMemCategoryName.DataPropertyName = "MemCategoryName"
            objdgcolhMembershipunkid.DataPropertyName = "membershipunkid"
            dgcolhMembershipname.DataPropertyName = "membershipname"
            objdgcolhEmptranheadunkid.DataPropertyName = "emptranheadunkid"
            dgcolhEmpheadname.DataPropertyName = "empheadname"
            objdgcolhCotranheadunkid.DataPropertyName = "cotranheadunkid"
            dgcolhCotranheadname.DataPropertyName = "coheadname"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, employeename "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    'RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    'RemoveHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                    'AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    'AddHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpComboText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnAssign.Enabled = User._Object.Privilege._AddEarningDeduction
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmAssignMembershipHeads_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignMembershipHeads_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignMembershipHeads_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessPayroll_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignMembershipHeads_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()

            mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to Search")

            Call FillCombo()

            Call SetDefaultSearchEmpText()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessPayroll_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignMembershipHeads_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollProcessTran.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollProcessTran"
            objfrm.displayDialog(Me)

            Call setLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmAssignMembershipHeads_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)


            End If

            If CInt(cboPayPeriod.SelectedValue) < 0 Then Call SetDefaultSearchText(cboPayPeriod)
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPayPeriod.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboPayPeriod.ValueMember
                    .DisplayMember = cboPayPeriod.DisplayMember
                    .DataSource = CType(cboPayPeriod.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboPayPeriod.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboPayPeriod.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPayPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.GotFocus
        Try
            With cboPayPeriod
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPayPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.Leave
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboPayPeriod)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_Leave", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim objED As New clsEarningDeduction
        Try
            If dvEmployee Is Nothing Then Exit Try

            mintCheckedEmployee = dvEmployee.Table.Select("IsChecked = 1 ").Length

            If mintCheckedEmployee = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select atleast one employee to assign membership."), enMsgBoxStyle.Information)
                Exit Try
            ElseIf CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Exit Try
            End If

            With objED
                ._FormName = mstrModuleName
                ._LoginEmployeeUnkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If objED.Insert_MembershipHeads(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, CInt(cboPayPeriod.SelectedValue), User._Object.Privilege._AllowToApproveEarningDeduction, dvEmployee.Table.Select("IsChecked = 1 ").CopyToDataTable, ConfigParameter._Object._CurrentDateAndTime, Nothing, True) = True Then
                mblnCancel = False
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Membership heads assigned successfully!"), enMsgBoxStyle.Information)

                If menAction = enAction.ADD_CONTINUE Then
                    Call FillList()
                Else
                    Me.Close()
                End If
            Else
                If objED._Message <> "" Then
                    eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
            objED = Nothing
        End Try
    End Sub
#End Region

#Region " GridView Events "

    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.SelectionChanged
        Try
            If dgEmployee.SelectedRows.Count = 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Exit Try
            End If
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrAdvanceFilter = ""

            dgEmployee.DataSource = Nothing
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            txtSearchEmp.Clear()

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
            Call dgEmployee_SelectionChanged(dgEmployee, New System.EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                RemoveHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR MemCategoryName LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR membershipname LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR empheadname LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR coheadname LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
                AddHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgcolhMemCategoryName.HeaderText = Language._Object.getCaption(Me.dgcolhMemCategoryName.Name, Me.dgcolhMemCategoryName.HeaderText)
			Me.dgcolhMembershipname.HeaderText = Language._Object.getCaption(Me.dgcolhMembershipname.Name, Me.dgcolhMembershipname.HeaderText)
			Me.dgcolhEmpheadname.HeaderText = Language._Object.getCaption(Me.dgcolhEmpheadname.Name, Me.dgcolhEmpheadname.HeaderText)
			Me.dgcolhCotranheadname.HeaderText = Language._Object.getCaption(Me.dgcolhCotranheadname.Name, Me.dgcolhCotranheadname.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Type to Search")
			Language.setMessage(mstrModuleName, 2, "Please select atleast one employee to assign membership.")
			Language.setMessage(mstrModuleName, 3, "Please select Period. Period is mandatory information.")
			Language.setMessage(mstrModuleName, 4, "Membership heads assigned successfully!")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class