﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaySlipMsgList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPaySlipMsgList))
        Me.pnlPayslipMsgList = New System.Windows.Forms.Panel
        Me.gbPayslipMsgList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblAsOnPeriod = New System.Windows.Forms.Label
        Me.lvPayslipMsgList = New eZee.Common.eZeeListView(Me.components)
        Me.colhUnkID = New System.Windows.Forms.ColumnHeader
        Me.colhAnalysisIndex = New System.Windows.Forms.ColumnHeader
        Me.colhAnalysisName = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.colhMessage1 = New System.Windows.Forms.ColumnHeader
        Me.colhMessage2 = New System.Windows.Forms.ColumnHeader
        Me.colhMessage3 = New System.Windows.Forms.ColumnHeader
        Me.colhMessage4 = New System.Windows.Forms.ColumnHeader
        Me.colhStatusid = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.cboPayPoint = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.rabClass = New System.Windows.Forms.RadioButton
        Me.rabJob = New System.Windows.Forms.RadioButton
        Me.rabSection = New System.Windows.Forms.RadioButton
        Me.rabDepartment = New System.Windows.Forms.RadioButton
        Me.rabPayPoint = New System.Windows.Forms.RadioButton
        Me.rabCostCenter = New System.Windows.Forms.RadioButton
        Me.rabGrade = New System.Windows.Forms.RadioButton
        Me.rabEmployee = New System.Windows.Forms.RadioButton
        Me.rabAllGroups = New System.Windows.Forms.RadioButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlPayslipMsgList.SuspendLayout()
        Me.gbPayslipMsgList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlPayslipMsgList
        '
        Me.pnlPayslipMsgList.Controls.Add(Me.gbPayslipMsgList)
        Me.pnlPayslipMsgList.Controls.Add(Me.eZeeHeader)
        Me.pnlPayslipMsgList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlPayslipMsgList.Controls.Add(Me.objFooter)
        Me.pnlPayslipMsgList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPayslipMsgList.Location = New System.Drawing.Point(0, 0)
        Me.pnlPayslipMsgList.Name = "pnlPayslipMsgList"
        Me.pnlPayslipMsgList.Size = New System.Drawing.Size(898, 539)
        Me.pnlPayslipMsgList.TabIndex = 0
        '
        'gbPayslipMsgList
        '
        Me.gbPayslipMsgList.BorderColor = System.Drawing.Color.Black
        Me.gbPayslipMsgList.Checked = False
        Me.gbPayslipMsgList.CollapseAllExceptThis = False
        Me.gbPayslipMsgList.CollapsedHoverImage = Nothing
        Me.gbPayslipMsgList.CollapsedNormalImage = Nothing
        Me.gbPayslipMsgList.CollapsedPressedImage = Nothing
        Me.gbPayslipMsgList.CollapseOnLoad = False
        Me.gbPayslipMsgList.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbPayslipMsgList.Controls.Add(Me.cboPeriod)
        Me.gbPayslipMsgList.Controls.Add(Me.lblAsOnPeriod)
        Me.gbPayslipMsgList.Controls.Add(Me.lvPayslipMsgList)
        Me.gbPayslipMsgList.ExpandedHoverImage = Nothing
        Me.gbPayslipMsgList.ExpandedNormalImage = Nothing
        Me.gbPayslipMsgList.ExpandedPressedImage = Nothing
        Me.gbPayslipMsgList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPayslipMsgList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPayslipMsgList.HeaderHeight = 25
        Me.gbPayslipMsgList.HeaderMessage = ""
        Me.gbPayslipMsgList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPayslipMsgList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPayslipMsgList.HeightOnCollapse = 0
        Me.gbPayslipMsgList.LeftTextSpace = 0
        Me.gbPayslipMsgList.Location = New System.Drawing.Point(12, 189)
        Me.gbPayslipMsgList.Name = "gbPayslipMsgList"
        Me.gbPayslipMsgList.OpenHeight = 90
        Me.gbPayslipMsgList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPayslipMsgList.ShowBorder = True
        Me.gbPayslipMsgList.ShowCheckBox = False
        Me.gbPayslipMsgList.ShowCollapseButton = False
        Me.gbPayslipMsgList.ShowDefaultBorderColor = True
        Me.gbPayslipMsgList.ShowDownButton = False
        Me.gbPayslipMsgList.ShowHeader = True
        Me.gbPayslipMsgList.Size = New System.Drawing.Size(874, 289)
        Me.gbPayslipMsgList.TabIndex = 4
        Me.gbPayslipMsgList.Temp = 0
        Me.gbPayslipMsgList.Text = "Payslip Messages List"
        Me.gbPayslipMsgList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(507, 2)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 275
        '
        'cboPeriod
        '
        Me.cboPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(382, 2)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(119, 21)
        Me.cboPeriod.TabIndex = 274
        '
        'lblAsOnPeriod
        '
        Me.lblAsOnPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblAsOnPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnPeriod.Location = New System.Drawing.Point(281, 6)
        Me.lblAsOnPeriod.Name = "lblAsOnPeriod"
        Me.lblAsOnPeriod.Size = New System.Drawing.Size(87, 15)
        Me.lblAsOnPeriod.TabIndex = 272
        Me.lblAsOnPeriod.Text = "As On Period"
        '
        'lvPayslipMsgList
        '
        Me.lvPayslipMsgList.BackColorOnChecked = True
        Me.lvPayslipMsgList.ColumnHeaders = Nothing
        Me.lvPayslipMsgList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhUnkID, Me.colhAnalysisIndex, Me.colhAnalysisName, Me.colhPeriodName, Me.colhMessage1, Me.colhMessage2, Me.colhMessage3, Me.colhMessage4, Me.colhStatusid})
        Me.lvPayslipMsgList.CompulsoryColumns = ""
        Me.lvPayslipMsgList.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lvPayslipMsgList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPayslipMsgList.FullRowSelect = True
        Me.lvPayslipMsgList.GridLines = True
        Me.lvPayslipMsgList.GroupingColumn = Nothing
        Me.lvPayslipMsgList.HideSelection = False
        Me.lvPayslipMsgList.Location = New System.Drawing.Point(0, 25)
        Me.lvPayslipMsgList.MinColumnWidth = 50
        Me.lvPayslipMsgList.MultiSelect = False
        Me.lvPayslipMsgList.Name = "lvPayslipMsgList"
        Me.lvPayslipMsgList.OptionalColumns = ""
        Me.lvPayslipMsgList.ShowMoreItem = False
        Me.lvPayslipMsgList.ShowSaveItem = False
        Me.lvPayslipMsgList.ShowSelectAll = True
        Me.lvPayslipMsgList.ShowSizeAllColumnsToFit = True
        Me.lvPayslipMsgList.Size = New System.Drawing.Size(874, 264)
        Me.lvPayslipMsgList.Sortable = False
        Me.lvPayslipMsgList.TabIndex = 1
        Me.lvPayslipMsgList.UseCompatibleStateImageBehavior = False
        Me.lvPayslipMsgList.View = System.Windows.Forms.View.Details
        '
        'colhUnkID
        '
        Me.colhUnkID.Tag = "colhUnkID"
        Me.colhUnkID.Text = "ID"
        Me.colhUnkID.Width = 0
        '
        'colhAnalysisIndex
        '
        Me.colhAnalysisIndex.Tag = "colhAnalysisIndex"
        Me.colhAnalysisIndex.Text = "Analysis Index"
        Me.colhAnalysisIndex.Width = 0
        '
        'colhAnalysisName
        '
        Me.colhAnalysisName.Tag = "colhAnalysisName"
        Me.colhAnalysisName.Text = "Analysis Name"
        Me.colhAnalysisName.Width = 220
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Effective Period"
        Me.colhPeriodName.Width = 100
        '
        'colhMessage1
        '
        Me.colhMessage1.Tag = "colhMessage1"
        Me.colhMessage1.Text = "Message 1"
        Me.colhMessage1.Width = 350
        '
        'colhMessage2
        '
        Me.colhMessage2.Tag = "colhMessage2"
        Me.colhMessage2.Text = "Message 2"
        Me.colhMessage2.Width = 150
        '
        'colhMessage3
        '
        Me.colhMessage3.Tag = "colhMessage3"
        Me.colhMessage3.Text = "Message 3"
        Me.colhMessage3.Width = 150
        '
        'colhMessage4
        '
        Me.colhMessage4.Tag = "colhMessage4"
        Me.colhMessage4.Text = "Message 4"
        Me.colhMessage4.Width = 150
        '
        'colhStatusid
        '
        Me.colhStatusid.Tag = "colhStatusid"
        Me.colhStatusid.Text = "Statusid"
        Me.colhStatusid.Width = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(898, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Payslip Messages List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboSection)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPoint)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.rabClass)
        Me.gbFilterCriteria.Controls.Add(Me.rabJob)
        Me.gbFilterCriteria.Controls.Add(Me.rabSection)
        Me.gbFilterCriteria.Controls.Add(Me.rabDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.rabPayPoint)
        Me.gbFilterCriteria.Controls.Add(Me.rabCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.rabGrade)
        Me.gbFilterCriteria.Controls.Add(Me.rabEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.rabAllGroups)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(874, 117)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboClass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(735, 88)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(119, 21)
        Me.cboClass.TabIndex = 97
        '
        'cboSection
        '
        Me.cboSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(735, 61)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(119, 21)
        Me.cboSection.TabIndex = 96
        '
        'cboDepartment
        '
        Me.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(735, 34)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(119, 21)
        Me.cboDepartment.TabIndex = 95
        '
        'cboGrade
        '
        Me.cboGrade.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboGrade.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(98, 88)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(119, 21)
        Me.cboGrade.TabIndex = 94
        '
        'cboPayPoint
        '
        Me.cboPayPoint.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayPoint.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPoint.FormattingEnabled = True
        Me.cboPayPoint.Location = New System.Drawing.Point(382, 61)
        Me.cboPayPoint.Name = "cboPayPoint"
        Me.cboPayPoint.Size = New System.Drawing.Size(119, 21)
        Me.cboPayPoint.TabIndex = 93
        '
        'cboJob
        '
        Me.cboJob.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboJob.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(382, 88)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(119, 21)
        Me.cboJob.TabIndex = 92
        '
        'cboCostCenter
        '
        Me.cboCostCenter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCostCenter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(382, 34)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(119, 21)
        Me.cboCostCenter.TabIndex = 91
        '
        'rabClass
        '
        Me.rabClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabClass.Location = New System.Drawing.Point(531, 90)
        Me.rabClass.Name = "rabClass"
        Me.rabClass.Size = New System.Drawing.Size(198, 17)
        Me.rabClass.TabIndex = 9
        Me.rabClass.Text = "C&lass in Class Group"
        Me.rabClass.UseVisualStyleBackColor = True
        '
        'rabJob
        '
        Me.rabJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabJob.Location = New System.Drawing.Point(263, 90)
        Me.rabJob.Name = "rabJob"
        Me.rabJob.Size = New System.Drawing.Size(150, 17)
        Me.rabJob.TabIndex = 8
        Me.rabJob.Text = "&Job in Job Group"
        Me.rabJob.UseVisualStyleBackColor = True
        '
        'rabSection
        '
        Me.rabSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabSection.Location = New System.Drawing.Point(531, 63)
        Me.rabSection.Name = "rabSection"
        Me.rabSection.Size = New System.Drawing.Size(198, 17)
        Me.rabSection.TabIndex = 7
        Me.rabSection.Text = "&Section in Department"
        Me.rabSection.UseVisualStyleBackColor = True
        '
        'rabDepartment
        '
        Me.rabDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabDepartment.Location = New System.Drawing.Point(531, 36)
        Me.rabDepartment.Name = "rabDepartment"
        Me.rabDepartment.Size = New System.Drawing.Size(198, 17)
        Me.rabDepartment.TabIndex = 6
        Me.rabDepartment.Text = "&Department in Department Group"
        Me.rabDepartment.UseVisualStyleBackColor = True
        '
        'rabPayPoint
        '
        Me.rabPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabPayPoint.Location = New System.Drawing.Point(263, 63)
        Me.rabPayPoint.Name = "rabPayPoint"
        Me.rabPayPoint.Size = New System.Drawing.Size(198, 17)
        Me.rabPayPoint.TabIndex = 5
        Me.rabPayPoint.Text = "&Pay Point"
        Me.rabPayPoint.UseVisualStyleBackColor = True
        '
        'rabCostCenter
        '
        Me.rabCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabCostCenter.Location = New System.Drawing.Point(263, 36)
        Me.rabCostCenter.Name = "rabCostCenter"
        Me.rabCostCenter.Size = New System.Drawing.Size(113, 17)
        Me.rabCostCenter.TabIndex = 4
        Me.rabCostCenter.Text = "&Cost Center"
        Me.rabCostCenter.UseVisualStyleBackColor = True
        '
        'rabGrade
        '
        Me.rabGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabGrade.Location = New System.Drawing.Point(8, 90)
        Me.rabGrade.Name = "rabGrade"
        Me.rabGrade.Size = New System.Drawing.Size(84, 17)
        Me.rabGrade.TabIndex = 3
        Me.rabGrade.Text = "&Grade"
        Me.rabGrade.UseVisualStyleBackColor = True
        '
        'rabEmployee
        '
        Me.rabEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabEmployee.Location = New System.Drawing.Point(8, 63)
        Me.rabEmployee.Name = "rabEmployee"
        Me.rabEmployee.Size = New System.Drawing.Size(84, 17)
        Me.rabEmployee.TabIndex = 1
        Me.rabEmployee.Text = "&Employee"
        Me.rabEmployee.UseVisualStyleBackColor = True
        '
        'rabAllGroups
        '
        Me.rabAllGroups.Checked = True
        Me.rabAllGroups.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabAllGroups.Location = New System.Drawing.Point(8, 36)
        Me.rabAllGroups.Name = "rabAllGroups"
        Me.rabAllGroups.Size = New System.Drawing.Size(84, 17)
        Me.rabAllGroups.TabIndex = 0
        Me.rabAllGroups.TabStop = True
        Me.rabAllGroups.Text = "&All Groups"
        Me.rabAllGroups.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(223, 63)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 89
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(98, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(119, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(847, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(817, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 484)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(898, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(697, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(793, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(600, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(504, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'frmPaySlipMsgList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(898, 539)
        Me.Controls.Add(Me.pnlPayslipMsgList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPaySlipMsgList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payslip Messages List"
        Me.pnlPayslipMsgList.ResumeLayout(False)
        Me.gbPayslipMsgList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlPayslipMsgList As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents rabAllGroups As System.Windows.Forms.RadioButton
    Friend WithEvents rabEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents rabPayPoint As System.Windows.Forms.RadioButton
    Friend WithEvents rabCostCenter As System.Windows.Forms.RadioButton
    Friend WithEvents rabGrade As System.Windows.Forms.RadioButton
    Friend WithEvents rabJob As System.Windows.Forms.RadioButton
    Friend WithEvents rabSection As System.Windows.Forms.RadioButton
    Friend WithEvents rabDepartment As System.Windows.Forms.RadioButton
    Friend WithEvents rabClass As System.Windows.Forms.RadioButton
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayPoint As System.Windows.Forms.ComboBox
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents gbPayslipMsgList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAsOnPeriod As System.Windows.Forms.Label
    Friend WithEvents lvPayslipMsgList As eZee.Common.eZeeListView
    Friend WithEvents colhUnkID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAnalysisIndex As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAnalysisName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMessage1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents colhMessage2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMessage3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMessage4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatusid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
End Class
