﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaySlipMsg
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPaySlipMsg))
        Me.pnlPayslipMsg = New System.Windows.Forms.Panel
        Me.objchkSelectallList = New System.Windows.Forms.CheckBox
        Me.lblList = New System.Windows.Forms.Label
        Me.objchkSelectallGroup = New System.Windows.Forms.CheckBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbPayslipMsg = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblEffPeriod = New System.Windows.Forms.Label
        Me.txtMsg4 = New eZee.TextBox.AlphanumericTextBox
        Me.txtMsg3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtMsg1 = New eZee.TextBox.AlphanumericTextBox
        Me.txtMsg2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblMsg2 = New System.Windows.Forms.Label
        Me.lblMsg4 = New System.Windows.Forms.Label
        Me.lblMsg3 = New System.Windows.Forms.Label
        Me.lblMsg1 = New System.Windows.Forms.Label
        Me.lstList = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAnalysis = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAnalysis = New System.Windows.Forms.Panel
        Me.rabPayPoint = New System.Windows.Forms.RadioButton
        Me.rabCostCenter = New System.Windows.Forms.RadioButton
        Me.rabSection = New System.Windows.Forms.RadioButton
        Me.rabClassnClassGrp = New System.Windows.Forms.RadioButton
        Me.rabGrade = New System.Windows.Forms.RadioButton
        Me.rabJobnJobGrp = New System.Windows.Forms.RadioButton
        Me.rabEmployee = New System.Windows.Forms.RadioButton
        Me.rabDeptGroup = New System.Windows.Forms.RadioButton
        Me.rabSectionDept = New System.Windows.Forms.RadioButton
        Me.lstGroup = New System.Windows.Forms.ListView
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.pnlPayslipMsg.SuspendLayout()
        Me.gbPayslipMsg.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbAnalysis.SuspendLayout()
        Me.pnlAnalysis.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlPayslipMsg
        '
        Me.pnlPayslipMsg.Controls.Add(Me.objchkSelectallList)
        Me.pnlPayslipMsg.Controls.Add(Me.lblList)
        Me.pnlPayslipMsg.Controls.Add(Me.objchkSelectallGroup)
        Me.pnlPayslipMsg.Controls.Add(Me.lblGroup)
        Me.pnlPayslipMsg.Controls.Add(Me.eZeeHeader)
        Me.pnlPayslipMsg.Controls.Add(Me.gbPayslipMsg)
        Me.pnlPayslipMsg.Controls.Add(Me.lstList)
        Me.pnlPayslipMsg.Controls.Add(Me.objFooter)
        Me.pnlPayslipMsg.Controls.Add(Me.gbAnalysis)
        Me.pnlPayslipMsg.Controls.Add(Me.lstGroup)
        Me.pnlPayslipMsg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPayslipMsg.Location = New System.Drawing.Point(0, 0)
        Me.pnlPayslipMsg.Name = "pnlPayslipMsg"
        Me.pnlPayslipMsg.Size = New System.Drawing.Size(607, 520)
        Me.pnlPayslipMsg.TabIndex = 0
        '
        'objchkSelectallList
        '
        Me.objchkSelectallList.AutoSize = True
        Me.objchkSelectallList.Location = New System.Drawing.Point(580, 75)
        Me.objchkSelectallList.Name = "objchkSelectallList"
        Me.objchkSelectallList.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectallList.TabIndex = 4
        Me.objchkSelectallList.UseVisualStyleBackColor = True
        '
        'lblList
        '
        Me.lblList.Location = New System.Drawing.Point(429, 74)
        Me.lblList.Name = "lblList"
        Me.lblList.Size = New System.Drawing.Size(63, 13)
        Me.lblList.TabIndex = 33
        Me.lblList.Text = "List"
        Me.lblList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkSelectallGroup
        '
        Me.objchkSelectallGroup.AutoSize = True
        Me.objchkSelectallGroup.Location = New System.Drawing.Point(236, 72)
        Me.objchkSelectallGroup.Name = "objchkSelectallGroup"
        Me.objchkSelectallGroup.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectallGroup.TabIndex = 2
        Me.objchkSelectallGroup.UseVisualStyleBackColor = True
        '
        'lblGroup
        '
        Me.lblGroup.Location = New System.Drawing.Point(265, 73)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(63, 13)
        Me.lblGroup.TabIndex = 31
        Me.lblGroup.Text = "Group"
        Me.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(607, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Add / Edit Payslip Messages"
        '
        'gbPayslipMsg
        '
        Me.gbPayslipMsg.BorderColor = System.Drawing.Color.Black
        Me.gbPayslipMsg.Checked = False
        Me.gbPayslipMsg.CollapseAllExceptThis = False
        Me.gbPayslipMsg.CollapsedHoverImage = Nothing
        Me.gbPayslipMsg.CollapsedNormalImage = Nothing
        Me.gbPayslipMsg.CollapsedPressedImage = Nothing
        Me.gbPayslipMsg.CollapseOnLoad = False
        Me.gbPayslipMsg.Controls.Add(Me.chkOverwrite)
        Me.gbPayslipMsg.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbPayslipMsg.Controls.Add(Me.cboPeriod)
        Me.gbPayslipMsg.Controls.Add(Me.lblEffPeriod)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg4)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg3)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg1)
        Me.gbPayslipMsg.Controls.Add(Me.txtMsg2)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg2)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg4)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg3)
        Me.gbPayslipMsg.Controls.Add(Me.lblMsg1)
        Me.gbPayslipMsg.ExpandedHoverImage = Nothing
        Me.gbPayslipMsg.ExpandedNormalImage = Nothing
        Me.gbPayslipMsg.ExpandedPressedImage = Nothing
        Me.gbPayslipMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPayslipMsg.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPayslipMsg.HeaderHeight = 25
        Me.gbPayslipMsg.HeaderMessage = ""
        Me.gbPayslipMsg.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPayslipMsg.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPayslipMsg.HeightOnCollapse = 0
        Me.gbPayslipMsg.LeftTextSpace = 0
        Me.gbPayslipMsg.Location = New System.Drawing.Point(14, 338)
        Me.gbPayslipMsg.Name = "gbPayslipMsg"
        Me.gbPayslipMsg.OpenHeight = 300
        Me.gbPayslipMsg.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPayslipMsg.ShowBorder = True
        Me.gbPayslipMsg.ShowCheckBox = False
        Me.gbPayslipMsg.ShowCollapseButton = False
        Me.gbPayslipMsg.ShowDefaultBorderColor = True
        Me.gbPayslipMsg.ShowDownButton = False
        Me.gbPayslipMsg.ShowHeader = True
        Me.gbPayslipMsg.Size = New System.Drawing.Size(581, 121)
        Me.gbPayslipMsg.TabIndex = 5
        Me.gbPayslipMsg.Temp = 0
        Me.gbPayslipMsg.Text = "Payslip Messages"
        Me.gbPayslipMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(263, 34)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 273
        '
        'cboPeriod
        '
        Me.cboPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(101, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(156, 21)
        Me.cboPeriod.TabIndex = 272
        '
        'lblEffPeriod
        '
        Me.lblEffPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblEffPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffPeriod.Location = New System.Drawing.Point(8, 37)
        Me.lblEffPeriod.Name = "lblEffPeriod"
        Me.lblEffPeriod.Size = New System.Drawing.Size(87, 15)
        Me.lblEffPeriod.TabIndex = 271
        Me.lblEffPeriod.Text = "Effective Period"
        '
        'txtMsg4
        '
        Me.txtMsg4.Flags = 0
        Me.txtMsg4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg4.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg4.Location = New System.Drawing.Point(385, 88)
        Me.txtMsg4.Name = "txtMsg4"
        Me.txtMsg4.Size = New System.Drawing.Size(183, 21)
        Me.txtMsg4.TabIndex = 3
        '
        'txtMsg3
        '
        Me.txtMsg3.Flags = 0
        Me.txtMsg3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg3.Location = New System.Drawing.Point(385, 61)
        Me.txtMsg3.Name = "txtMsg3"
        Me.txtMsg3.Size = New System.Drawing.Size(183, 21)
        Me.txtMsg3.TabIndex = 2
        '
        'txtMsg1
        '
        Me.txtMsg1.Flags = 0
        Me.txtMsg1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg1.Location = New System.Drawing.Point(101, 61)
        Me.txtMsg1.Name = "txtMsg1"
        Me.txtMsg1.Size = New System.Drawing.Size(183, 21)
        Me.txtMsg1.TabIndex = 0
        '
        'txtMsg2
        '
        Me.txtMsg2.Flags = 0
        Me.txtMsg2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMsg2.Location = New System.Drawing.Point(101, 88)
        Me.txtMsg2.Name = "txtMsg2"
        Me.txtMsg2.Size = New System.Drawing.Size(183, 21)
        Me.txtMsg2.TabIndex = 1
        '
        'lblMsg2
        '
        Me.lblMsg2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg2.Location = New System.Drawing.Point(8, 89)
        Me.lblMsg2.Name = "lblMsg2"
        Me.lblMsg2.Size = New System.Drawing.Size(87, 15)
        Me.lblMsg2.TabIndex = 269
        Me.lblMsg2.Text = "Message 2:"
        '
        'lblMsg4
        '
        Me.lblMsg4.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg4.Location = New System.Drawing.Point(293, 91)
        Me.lblMsg4.Name = "lblMsg4"
        Me.lblMsg4.Size = New System.Drawing.Size(87, 15)
        Me.lblMsg4.TabIndex = 265
        Me.lblMsg4.Text = "Message 4:"
        '
        'lblMsg3
        '
        Me.lblMsg3.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg3.Location = New System.Drawing.Point(293, 64)
        Me.lblMsg3.Name = "lblMsg3"
        Me.lblMsg3.Size = New System.Drawing.Size(87, 15)
        Me.lblMsg3.TabIndex = 263
        Me.lblMsg3.Text = "Message 3:"
        '
        'lblMsg1
        '
        Me.lblMsg1.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg1.Location = New System.Drawing.Point(8, 64)
        Me.lblMsg1.Name = "lblMsg1"
        Me.lblMsg1.Size = New System.Drawing.Size(87, 15)
        Me.lblMsg1.TabIndex = 230
        Me.lblMsg1.Text = "Message 1:"
        '
        'lstList
        '
        Me.lstList.CheckBoxes = True
        Me.lstList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lstList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lstList.Location = New System.Drawing.Point(412, 94)
        Me.lstList.Name = "lstList"
        Me.lstList.ShowItemToolTips = True
        Me.lstList.Size = New System.Drawing.Size(183, 238)
        Me.lstList.TabIndex = 3
        Me.lstList.UseCompatibleStateImageBehavior = False
        Me.lstList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 170
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 465)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(607, 55)
        Me.objFooter.TabIndex = 6
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(395, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(498, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbAnalysis
        '
        Me.gbAnalysis.BorderColor = System.Drawing.Color.Black
        Me.gbAnalysis.Checked = False
        Me.gbAnalysis.CollapseAllExceptThis = False
        Me.gbAnalysis.CollapsedHoverImage = Nothing
        Me.gbAnalysis.CollapsedNormalImage = Nothing
        Me.gbAnalysis.CollapsedPressedImage = Nothing
        Me.gbAnalysis.CollapseOnLoad = False
        Me.gbAnalysis.Controls.Add(Me.pnlAnalysis)
        Me.gbAnalysis.ExpandedHoverImage = Nothing
        Me.gbAnalysis.ExpandedNormalImage = Nothing
        Me.gbAnalysis.ExpandedPressedImage = Nothing
        Me.gbAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAnalysis.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAnalysis.HeaderHeight = 25
        Me.gbAnalysis.HeaderMessage = ""
        Me.gbAnalysis.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAnalysis.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAnalysis.HeightOnCollapse = 0
        Me.gbAnalysis.LeftTextSpace = 0
        Me.gbAnalysis.Location = New System.Drawing.Point(12, 66)
        Me.gbAnalysis.Name = "gbAnalysis"
        Me.gbAnalysis.OpenHeight = 182
        Me.gbAnalysis.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAnalysis.ShowBorder = True
        Me.gbAnalysis.ShowCheckBox = False
        Me.gbAnalysis.ShowCollapseButton = False
        Me.gbAnalysis.ShowDefaultBorderColor = True
        Me.gbAnalysis.ShowDownButton = False
        Me.gbAnalysis.ShowHeader = True
        Me.gbAnalysis.Size = New System.Drawing.Size(210, 266)
        Me.gbAnalysis.TabIndex = 0
        Me.gbAnalysis.Temp = 0
        Me.gbAnalysis.Text = "Analysis"
        Me.gbAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAnalysis
        '
        Me.pnlAnalysis.AutoScroll = True
        Me.pnlAnalysis.Controls.Add(Me.rabPayPoint)
        Me.pnlAnalysis.Controls.Add(Me.rabCostCenter)
        Me.pnlAnalysis.Controls.Add(Me.rabSection)
        Me.pnlAnalysis.Controls.Add(Me.rabClassnClassGrp)
        Me.pnlAnalysis.Controls.Add(Me.rabGrade)
        Me.pnlAnalysis.Controls.Add(Me.rabJobnJobGrp)
        Me.pnlAnalysis.Controls.Add(Me.rabEmployee)
        Me.pnlAnalysis.Controls.Add(Me.rabDeptGroup)
        Me.pnlAnalysis.Controls.Add(Me.rabSectionDept)
        Me.pnlAnalysis.Location = New System.Drawing.Point(2, 26)
        Me.pnlAnalysis.Name = "pnlAnalysis"
        Me.pnlAnalysis.Size = New System.Drawing.Size(206, 238)
        Me.pnlAnalysis.TabIndex = 124
        '
        'rabPayPoint
        '
        Me.rabPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabPayPoint.Location = New System.Drawing.Point(8, 78)
        Me.rabPayPoint.Name = "rabPayPoint"
        Me.rabPayPoint.Size = New System.Drawing.Size(193, 17)
        Me.rabPayPoint.TabIndex = 3
        Me.rabPayPoint.TabStop = True
        Me.rabPayPoint.Text = "Pay Point"
        Me.rabPayPoint.UseVisualStyleBackColor = True
        '
        'rabCostCenter
        '
        Me.rabCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabCostCenter.Location = New System.Drawing.Point(8, 55)
        Me.rabCostCenter.Name = "rabCostCenter"
        Me.rabCostCenter.Size = New System.Drawing.Size(193, 17)
        Me.rabCostCenter.TabIndex = 2
        Me.rabCostCenter.TabStop = True
        Me.rabCostCenter.Text = "Cost Center"
        Me.rabCostCenter.UseVisualStyleBackColor = True
        '
        'rabSection
        '
        Me.rabSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabSection.Location = New System.Drawing.Point(8, 193)
        Me.rabSection.Name = "rabSection"
        Me.rabSection.Size = New System.Drawing.Size(193, 17)
        Me.rabSection.TabIndex = 8
        Me.rabSection.TabStop = True
        Me.rabSection.Text = "Section"
        Me.rabSection.UseVisualStyleBackColor = True
        Me.rabSection.Visible = False
        '
        'rabClassnClassGrp
        '
        Me.rabClassnClassGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabClassnClassGrp.Location = New System.Drawing.Point(8, 170)
        Me.rabClassnClassGrp.Name = "rabClassnClassGrp"
        Me.rabClassnClassGrp.Size = New System.Drawing.Size(193, 17)
        Me.rabClassnClassGrp.TabIndex = 7
        Me.rabClassnClassGrp.TabStop = True
        Me.rabClassnClassGrp.Text = "Class in Class Group"
        Me.rabClassnClassGrp.UseVisualStyleBackColor = True
        '
        'rabGrade
        '
        Me.rabGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabGrade.Location = New System.Drawing.Point(8, 32)
        Me.rabGrade.Name = "rabGrade"
        Me.rabGrade.Size = New System.Drawing.Size(193, 17)
        Me.rabGrade.TabIndex = 1
        Me.rabGrade.TabStop = True
        Me.rabGrade.Text = "Grade"
        Me.rabGrade.UseVisualStyleBackColor = True
        '
        'rabJobnJobGrp
        '
        Me.rabJobnJobGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabJobnJobGrp.Location = New System.Drawing.Point(8, 147)
        Me.rabJobnJobGrp.Name = "rabJobnJobGrp"
        Me.rabJobnJobGrp.Size = New System.Drawing.Size(193, 17)
        Me.rabJobnJobGrp.TabIndex = 6
        Me.rabJobnJobGrp.TabStop = True
        Me.rabJobnJobGrp.Text = "Job in Job Group"
        Me.rabJobnJobGrp.UseVisualStyleBackColor = True
        '
        'rabEmployee
        '
        Me.rabEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabEmployee.Location = New System.Drawing.Point(8, 9)
        Me.rabEmployee.Name = "rabEmployee"
        Me.rabEmployee.Size = New System.Drawing.Size(193, 17)
        Me.rabEmployee.TabIndex = 0
        Me.rabEmployee.TabStop = True
        Me.rabEmployee.Text = "Employee"
        Me.rabEmployee.UseVisualStyleBackColor = True
        '
        'rabDeptGroup
        '
        Me.rabDeptGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabDeptGroup.Location = New System.Drawing.Point(8, 101)
        Me.rabDeptGroup.Name = "rabDeptGroup"
        Me.rabDeptGroup.Size = New System.Drawing.Size(193, 17)
        Me.rabDeptGroup.TabIndex = 4
        Me.rabDeptGroup.TabStop = True
        Me.rabDeptGroup.Text = "Department in Department Group"
        Me.rabDeptGroup.UseVisualStyleBackColor = True
        '
        'rabSectionDept
        '
        Me.rabSectionDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabSectionDept.Location = New System.Drawing.Point(8, 124)
        Me.rabSectionDept.Name = "rabSectionDept"
        Me.rabSectionDept.Size = New System.Drawing.Size(193, 17)
        Me.rabSectionDept.TabIndex = 5
        Me.rabSectionDept.TabStop = True
        Me.rabSectionDept.Text = "Section in Department"
        Me.rabSectionDept.UseVisualStyleBackColor = True
        '
        'lstGroup
        '
        Me.lstGroup.CheckBoxes = True
        Me.lstGroup.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lstGroup.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lstGroup.Location = New System.Drawing.Point(228, 92)
        Me.lstGroup.Name = "lstGroup"
        Me.lstGroup.Size = New System.Drawing.Size(178, 240)
        Me.lstGroup.TabIndex = 1
        Me.lstGroup.UseCompatibleStateImageBehavior = False
        Me.lstGroup.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Width = 160
        '
        'chkOverwrite
        '
        Me.chkOverwrite.AutoSize = True
        Me.chkOverwrite.Location = New System.Drawing.Point(299, 37)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(135, 17)
        Me.chkOverwrite.TabIndex = 275
        Me.chkOverwrite.Text = "Overwrite Message"
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'frmPaySlipMsg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(607, 520)
        Me.Controls.Add(Me.pnlPayslipMsg)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPaySlipMsg"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Payslip Messages"
        Me.pnlPayslipMsg.ResumeLayout(False)
        Me.pnlPayslipMsg.PerformLayout()
        Me.gbPayslipMsg.ResumeLayout(False)
        Me.gbPayslipMsg.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.gbAnalysis.ResumeLayout(False)
        Me.pnlAnalysis.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlPayslipMsg As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbPayslipMsg As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtMsg4 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMsg3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMsg1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMsg2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMsg2 As System.Windows.Forms.Label
    Friend WithEvents lblMsg4 As System.Windows.Forms.Label
    Friend WithEvents lblMsg3 As System.Windows.Forms.Label
    Friend WithEvents lblMsg1 As System.Windows.Forms.Label
    Friend WithEvents lstList As System.Windows.Forms.ListView
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbAnalysis As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAnalysis As System.Windows.Forms.Panel
    Friend WithEvents rabPayPoint As System.Windows.Forms.RadioButton
    Friend WithEvents rabCostCenter As System.Windows.Forms.RadioButton
    Friend WithEvents rabSection As System.Windows.Forms.RadioButton
    Friend WithEvents rabClassnClassGrp As System.Windows.Forms.RadioButton
    Friend WithEvents rabGrade As System.Windows.Forms.RadioButton
    Friend WithEvents rabJobnJobGrp As System.Windows.Forms.RadioButton
    Friend WithEvents rabEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents rabDeptGroup As System.Windows.Forms.RadioButton
    Friend WithEvents rabSectionDept As System.Windows.Forms.RadioButton
    Friend WithEvents lstGroup As System.Windows.Forms.ListView
    Friend WithEvents objchkSelectallGroup As System.Windows.Forms.CheckBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents objchkSelectallList As System.Windows.Forms.CheckBox
    Friend WithEvents lblList As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblEffPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents chkOverwrite As System.Windows.Forms.CheckBox
End Class
