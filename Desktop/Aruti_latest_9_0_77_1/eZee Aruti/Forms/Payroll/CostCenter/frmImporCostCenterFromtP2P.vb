﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Xml
Imports System.Net
Imports System.IO

#End Region

Public Class frmImporCostCenterFromtP2P

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImporCostCenterFromtP2P"
    Dim dvGriddata As DataView = Nothing
    Private mdt_ImportData As New DataTable
    Private m_dsImportData As DataTable
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
#End Region

#Region " From's Events "

    Private Sub frmImporCostCenterFromtP2P_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            m_dsImportData = GetCostCenterJson()
            If m_dsImportData IsNot Nothing Then
                CreateDataTable()
            End If
            pnlInfo.Enabled = False
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmImporCostCenterFromtP2P_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objCostCenterGrp As New clspayrollgroup_master
            Dim dsList As DataSet = objCostCenterGrp.getListForCombo(enPayrollGroupType.CostCenter, "List", True)
            cboCostCenterGrp.ValueMember = "groupmasterunkid"
            cboCostCenterGrp.DisplayMember = "name"
            cboCostCenterGrp.DataSource = dsList.Tables(0)
            objCostCenterGrp = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function GetCostCenterJson() As DataTable
        Dim dtTable As DataTable = Nothing
        Try
            'Pinkal below is commented purposely to test jason.
            'Dim mstrCCJson As String = "[{""costCenter"": ""704"", ""costCenterName"": ""Nachingwea""},{""costCenter"": ""705"", ""costCenterName"": ""Masasi""},{""costCenter"":""706"",""costCenterName"": ""Mtwara""},{""costCenter"": ""707"",""costCenterName"": ""Ndanda""},{""costCenter"": ""708"",""costCenterName"": ""Newala""}}]"

            Dim mstrCCJson As String = GetP2PResponse(ConfigParameter._Object._ImportCostCenterP2PServiceURL.Trim()) 'TO GET COST CENTER FROM P2P INTEGRATION
            If mstrCCJson.Trim.Length > 0 Then
                dtTable = JsonStringToDataTable(mstrCCJson)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please the check Import Cost Center P2P service URL, Either URL is invalid or URL is blank."), enMsgBoxStyle.Information)
                Return Nothing
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "GetCostCenterJson", mstrModuleName)
        End Try
        Return dtTable
    End Function

    Private Sub CreateDataTable()
        Try

            mdt_ImportData = m_dsImportData.Clone()

            mdt_ImportData.Rows.Clear() : objTotal.Text = "0"
            mdt_ImportData.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdt_ImportData.Columns.Add("image", System.Type.GetType("System.Object")).DefaultValue = New Drawing.Bitmap(1, 1).Clone
            mdt_ImportData.Columns.Add("message", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData.Columns.Add("status", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData.Columns.Add("objStatus", System.Type.GetType("System.String")).DefaultValue = "0"
            mdt_ImportData.Columns.Add("objIsProcess", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdt_ImportData.Columns.Add("IsExists", System.Type.GetType("System.Boolean")).DefaultValue = False

            Dim objCostCenter As New clscostcenter_master
            For Each dtRow As DataRow In m_dsImportData.Rows
                mdt_ImportData.ImportRow(dtRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
                Dim RowIndex As Integer = m_dsImportData.Rows.IndexOf(dtRow)
                If objCostCenter.isExist(mdt_ImportData.Rows(RowIndex)("costCenter").ToString(), "", -1) Then
                    mdt_ImportData.Rows(RowIndex)("image") = imgError
                    mdt_ImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 7, "This CostCenter Code is already defined.")
                    mdt_ImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    mdt_ImportData.Rows(RowIndex)("objStatus") = 2
                    mdt_ImportData.Rows(RowIndex)("objIsProcess") = False
                    mdt_ImportData.Rows(RowIndex)("IsExists") = True
                    objError.Text = CStr(Val(objError.Text) + 1)

                ElseIf objCostCenter.isExist("", mdt_ImportData.Rows(RowIndex)("costCenterName").ToString(), -1) Then
                    mdt_ImportData.Rows(RowIndex)("image") = imgError
                    mdt_ImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 8, "This CostCenter Name is already defined.")
                    mdt_ImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    mdt_ImportData.Rows(RowIndex)("objStatus") = 2
                    mdt_ImportData.Rows(RowIndex)("objIsProcess") = False
                    mdt_ImportData.Rows(RowIndex)("IsExists") = True
                    objError.Text = CStr(Val(objError.Text) + 1)

                Else
                    mdt_ImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 10, "This CostCenter does not exist.")
                    mdt_ImportData.Rows(RowIndex)("objStatus") = 0
                    mdt_ImportData.Rows(RowIndex)("objIsProcess") = False
                    mdt_ImportData.Rows(RowIndex)("IsExists") = False
                End If

            Next
            mdt_ImportData.AcceptChanges()
            objCostCenter = Nothing

            dgData.AutoGenerateColumns = False
            objdgcolhIsCheck.DataPropertyName = "ischeck"
            colhCostcenterCode.DataPropertyName = "costCenter"
            colhCostcenterName.DataPropertyName = "costCenterName"
            colhMessage.DataPropertyName = "message"
            objcolhImage.DataPropertyName = "image"
            'colhStatus.DataPropertyName = "status"
            objcolhstatus.DataPropertyName = "objStatus"
            dvGriddata = mdt_ImportData.DefaultView
            dvGriddata.RowFilter = "IsExists = 1"

            If dvGriddata.ToTable().Rows.Count <= 0 Then
                dvGriddata.RowFilter = "IsExists = 0"
                btnImport.Enabled = True
            Else
                btnImport.Enabled = False
            End If

            dgData.DataSource = mdt_ImportData


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        End Try
    End Sub

    Private Function Savefile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New IO.FileStream(fpath, IO.FileMode.Create, IO.FileAccess.Write)
        Dim strWriter As New IO.StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, IO.SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", "modGlobal")
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    Private Function Export_ErrorList(ByVal StrPath As String, ByVal strSourceName As String) As Boolean
        Dim strBuilder As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            strBuilder.Append(" <HTML> " & vbCrLf)
            strBuilder.Append(" <TITLE>" & strSourceName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=100%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            For j As Integer = 0 To dgData.ColumnCount - 1
                If dgData.Columns(j).Name.ToString.ToUpper.StartsWith("OBJ") = False Then
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dgData.Columns(j).HeaderText & "</B></FONT></TD>" & vbCrLf)
                End If
            Next

            For i As Integer = 0 To dgData.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To dgData.ColumnCount - 1
                    If dgData.Columns(k).Name.ToString.ToUpper.StartsWith("OBJ") = False Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CStr(dgData.Rows(i).Cells(k).Value) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </BODY> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)

            If Savefile(StrPath, strBuilder) Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Export_ErrorList", mstrModuleName)
        End Try
    End Function

    Private Function GetP2PResponse(ByVal strServiceURL As String) As String
        Dim strResponseData As String = ""
        Try

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            Dim mstrP2PToken As String = GetP2PToken()
            'Pinkal (24-Oct-2019) -- End

            Dim xmlDoc As New XmlDocument
            Dim request As HttpWebRequest = CType(WebRequest.Create(strServiceURL), HttpWebRequest)
            request.ContentType = "text/json"
            request.Method = "GET"

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            If mstrP2PToken.Trim.Length > 0 Then
                request.Headers.Add("Authorization", "Bearer " & mstrP2PToken)
            End If
            'Pinkal (24-Oct-2019) -- End

            Using responseReader As New StreamReader(request.GetResponse().GetResponseStream())
                Dim result As String = responseReader.ReadToEnd()
                strResponseData = result.ToString()
            End Using
            'Dim bytes() As Byte
            'bytes = System.Text.Encoding.ASCII.GetBytes(xmlDoc.InnerXml)
            'request.ContentType = "text/json"
            'request.ContentLength = bytes.Length
            'request.Method = "GET"
            'Dim requestStream As Stream = request.GetRequestStream()
            'requestStream.Write(bytes, 0, bytes.Length)
            'requestStream.Close()

        Catch ex As WebException
            Dim err As String = ""
            If ex.Response IsNot Nothing Then
                Using response As WebResponse = ex.Response
                    Dim httpResponse As HttpWebResponse = CType(response, HttpWebResponse)
                    Using data As Stream = response.GetResponseStream()
                        Using reader = New StreamReader(data)
                            err = reader.ReadToEnd()
                        End Using
                    End Using
                End Using
            End If
            WriteToFile("GetP2PResponse " & Date.Now & " : " & err)
        Catch ex As Exception
            WriteToFile("GetP2PResponse " & Date.Now & " : " & ex.Message)
            DisplayError.Show("-1", ex.Message, "GetP2PResponse", mstrModuleName)
        End Try
        Return strResponseData
    End Function

    Public Sub WriteToFile(ByVal Message As String)
        Dim m_strLogFile As String = ""
        Dim m_strFileName As String = "ArutiP2PImportCostCenter_LOG_" & Date.Now.Date.ToString("yyyyMMdd")
        Dim file As System.IO.StreamWriter

        'Pinkal (24-Oct-2019) -- Start
        'Enhancement NMB - Working On OT Enhancement for NMB.
        'm_strLogFile = System.IO.Path.Combine(Environment.CurrentDirectory & "\", m_strFileName & ".txt")

        'Pinkal (10-Jan-2020) -- Start
        'Enhancements -  Working on OT Requisistion for NMB.
        'm_strLogFile = System.IO.Path.Combine(Application.ExecutablePath & "\", m_strFileName & ".txt")
        m_strLogFile = System.IO.Path.Combine(Directory.GetParent(Application.ExecutablePath).FullName & "\", m_strFileName & ".txt")
        'Pinkal (10-Jan-2020) -- End

        'Pinkal (24-Oct-2019) -- End

        file = New System.IO.StreamWriter(m_strLogFile, True)
        file.BaseStream.Seek(0, SeekOrigin.End)
        file.WriteLine(Message)
        file.Close()
    End Sub

    Private Sub CheckItems()
        Try
            If dvGriddata Is Nothing Then Exit Sub

            Dim xTotalCount As Integer = dvGriddata.ToTable().AsEnumerable().Count
            Dim xCheckedCount As Integer = dvGriddata.ToTable().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If xCheckedCount <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf xCheckedCount < xTotalCount Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf xTotalCount = xCheckedCount Then
                chkSelectAll.CheckState = CheckState.Checked
            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckItems", mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Private Function GetP2PToken() As String
        Dim mstrToken As String = ""
        Dim mstrError As String = ""
        Dim mstrGetData As String = ""
        Try
            If ConfigParameter._Object._GetTokenP2PServiceURL.Trim.Length > 0 Then
                Dim objMstData As New clsMasterData
                Dim objCRUser As New clsCRP2PUserDetail
                Dim mstrUserNamePwd As String = objCRUser.username
                If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._GetTokenP2PServiceURL.Trim, True, True, mstrUserNamePwd, mstrGetData, mstrError, objCRUser, "", "") = False Then
                    eZeeMsgBox.Show(mstrError, enMsgBoxStyle.Information)
                    Return mstrToken
                End If

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mstrToken = dtTable.Rows(0)("token").ToString()
                    End If
                End If
                objMstData = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetP2PToken", mstrModuleName)
        End Try
        Return mstrToken
    End Function

    'Pinkal (24-Oct-2019) -- End

#End Region

#Region " Button's Events "

    Private Sub objSearchCostCenterGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchCostCenterGrp.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            objfrmCommonsearch.DataSource = CType(cboCostCenterGrp.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboCostCenterGrp.ValueMember
            objfrmCommonsearch.DisplayMember = cboCostCenterGrp.DisplayMember
            objfrmCommonsearch.CodeMember = "Code"
            If objfrmCommonsearch.DisplayDialog() Then
                cboCostCenterGrp.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboCostCenterGrp.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchCostCenterGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try
            If CInt(cboCostCenterGrp.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Cost Center Group is compulsory information.Please Select Cost Center Group."), enMsgBoxStyle.Information)
                cboCostCenterGrp.Select()
                Exit Sub
            End If

            If mdt_ImportData Is Nothing OrElse dvGriddata Is Nothing Then Exit Sub

            Dim xCount As Integer = dvGriddata.ToTable().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True AndAlso x.Field(Of Boolean)("isexists") = False AndAlso x.Field(Of Boolean)("objIsProcess") = False).Count

            Dim drRow As DataRow() = Nothing
            If xCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select atleast one cost center to do further operation on it."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                drRow = mdt_ImportData.Select("ischeck = 1 AND isexists = 0 AND objIsProcess = 0")
            End If

            btnImport.Enabled = False : btnFilter.Enabled = False
            ezWait.Active = True
            Dim objCostCenter As clscostcenter_master = Nothing
            Dim iCostCenterId As Integer = 0

            'objError.Text = "0"
            'objPending.Text = "0"
            'objSuccess.Text = "0"

            For Each dtRow As DataRow In drRow
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData.Rows.IndexOf(dtRow)
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                If objCostCenter IsNot Nothing Then objCostCenter = Nothing

                objCostCenter = New clscostcenter_master

                iCostCenterId = 0
                'iCostCenterId = objCostCenter.GetCostCenterUnkId(dtRow.Item("costCenterName").ToString())

                'If iCostCenterId > 0 Then objCostCenter._Costcenterunkid = iCostCenterId
                objCostCenter._Costcentergroupmasterunkid = CInt(cboCostCenterGrp.SelectedValue)
                objCostCenter._Costcentercode = dtRow.Item("costCenter").ToString()
                objCostCenter._Costcentername = dtRow.Item("costCenterName").ToString()
                objCostCenter._Customcode = dtRow.Item("costCenter").ToString()
                objCostCenter._Isactive = True

                If objCostCenter._Costcenterunkid <= 0 Then
                    objCostCenter._Costcentername1 = dtRow.Item("costCenterName").ToString()
                    objCostCenter._Costcentername2 = dtRow.Item("costCenterName").ToString()
                End If


                'If iCostCenterId > 0 Then
                'If objCostCenter.Update() = False Then
                '    dtRow.Item("image") = imgError
                '    dtRow.Item("message") = objCostCenter._Message
                '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                '    dtRow.Item("objStatus") = 2
                '    dtRow.Item("objIsProcess") = False
                '    objError.Text = CStr(Val(objError.Text) + 1)
                'Else
                '    dtRow.Item("image") = imgAccept
                '    dtRow.Item("message") = ""
                '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 4, "Successfully Updated.")
                '    dtRow.Item("objStatus") = 1
                '    dtRow.Item("objIsProcess") = True
                '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                '    objPending.Text = CStr(Val(objTotal.Text) - Val(objSuccess.Text))
                'End If
                'Else

                If objCostCenter.Insert() = False Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objCostCenter._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    dtRow.Item("objStatus") = 2
                    dtRow.Item("objIsProcess") = False
                    dtRow.Item("IsExists") = True
                    objError.Text = CStr(Val(objError.Text) + 1)
                    'objPending.Text = CStr(Val(objTotal.Text) - Val(objSuccess.Text))
                Else
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Success.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Success.")
                    'dtRow.Item("status") = Language.getMessage(mstrModuleName, 3, "Successfully inserted.")
                    dtRow.Item("objStatus") = 1
                    dtRow.Item("objIsProcess") = True
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                End If
                'End If
            Next


            dvGriddata.RowFilter = "IsExists = 0 AND objIsProcess = 0" 'FOR PENDING TRANSACTION
            CheckItems()
            If dvGriddata.ToTable().Rows.Count <= 0 Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.Checked = False
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
            btnImport.Enabled = True : btnFilter.Enabled = True : ezWait.Active = False
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Private Sub dgData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgData.CellContentClick, dgData.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhIsCheck.Index Then
                If dgData.IsCurrentCellDirty Then
                    dgData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                CheckItems()
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "dgData_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Context Menu's Events "

    Private Sub tsmNotExistsItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmNotExistsItems.Click
        Try
            dvGriddata.RowFilter = "IsExists = 0 AND objIsProcess = 0"
            If dvGriddata.ToTable().Rows.Count > 0 Then btnImport.Enabled = True
            CheckItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmNotExistsItems_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExistsItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExistsItems.Click
        Try
            dvGriddata.RowFilter = "IsExists = 1"
            btnImport.Enabled = False
            CheckItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExistsItems_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
            btnImport.Enabled = False
            CheckItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
            CheckItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        dvGriddata.RowFilter = ""
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                Dim savDialog As New SaveFileDialog
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If Export_ErrorList(savDialog.FileName, "Importing Cost Center") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub



#End Region

#Region "CheckBox Events"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dvGriddata IsNot Nothing Then
                For Each dr As DataRow In dvGriddata.Table().Rows
                    dr("ischeck") = chkSelectAll.Checked
                Next
                dvGriddata.Table().AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region



    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbImportEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbImportEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbImportEmployee.Text = Language._Object.getCaption(Me.gbImportEmployee.Name, Me.gbImportEmployee.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.LblCostCenterGrp.Text = Language._Object.getCaption(Me.LblCostCenterGrp.Name, Me.LblCostCenterGrp.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.tsmNotExistsItems.Text = Language._Object.getCaption(Me.tsmNotExistsItems.Name, Me.tsmNotExistsItems.Text)
			Me.tsmExistsItems.Text = Language._Object.getCaption(Me.tsmExistsItems.Name, Me.tsmExistsItems.Text)
			Me.colhCostcenterCode.HeaderText = Language._Object.getCaption(Me.colhCostcenterCode.Name, Me.colhCostcenterCode.HeaderText)
			Me.colhCostcenterName.HeaderText = Language._Object.getCaption(Me.colhCostcenterName.Name, Me.colhCostcenterName.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Cost Center Group is compulsory information.Please Select Cost Center Group.")
			Language.setMessage(mstrModuleName, 2, "Fail")
			Language.setMessage(mstrModuleName, 5, "Please select atleast one cost center to do further operation on it.")
			Language.setMessage(mstrModuleName, 6, "Please the check Import Cost Center P2P service URL, Either URL is invalid or URL is blank.")
			Language.setMessage(mstrModuleName, 7, "This CostCenter Code is already defined.")
			Language.setMessage(mstrModuleName, 8, "This CostCenter Name is already defined.")
			Language.setMessage(mstrModuleName, 9, "Success.")
			Language.setMessage(mstrModuleName, 10, "This CostCenter does not exist.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class