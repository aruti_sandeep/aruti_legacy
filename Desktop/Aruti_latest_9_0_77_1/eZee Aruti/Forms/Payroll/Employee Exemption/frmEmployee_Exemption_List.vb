﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmEmployee_Exemption_List

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployee_Exemption_List"
    Private objEmployeeExemption As clsemployee_exemption_Tran
    Private mblnCancel As Boolean = True
    Private mintYearID As Integer = 0
    Private mintPeriodID As Integer = 0
    Private mintEmployeeID As Integer = 0
    Private mintTranHeadID As Integer = 0
#End Region

#Region " Display Dialog "

    Public Function displayDialog(Optional ByVal intYearID As Integer = 0, Optional ByVal intPeriodID As Integer = 0, Optional ByVal intEmpID As Integer = 0, Optional ByVal intTranHeadID As Integer = 0) As Boolean
        Try

            mintYearID = intYearID
            mintPeriodID = intPeriodID
            mintEmployeeID = intEmpID
            mintTranHeadID = intTranHeadID

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboTransactionHead.BackColor = GUI.ColorOptional
            cboPayYear.BackColor = GUI.ColorOptional
            cboPayPeriod.BackColor = GUI.ColorOptional
            cboExempApprovStatus.BackColor = GUI.ColorOptional 'Sohail (26 Nov 2011)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewEmpExemptionList = True Then                'Pinkal (02-Jul-2012) -- Start


                'Sohail (26 Nov 2011) -- Start
                'Enhancement : TRA Changes
                btnApprove.Visible = False
                btnDisApprove.Visible = False
                'Sohail (26 Nov 2011) -- End

                'Sohail (09 Oct 2010) -- Start
                Cursor.Current = Cursors.WaitCursor 'Sohail (10 Feb 2012)
                lvEmpExemption.BeginUpdate()
                Dim lvArray As New List(Of ListViewItem) 'Sohail (02 Aug 2011)
                'Sohail (09 Oct 2010) -- End

                'Sohail (26 Nov 2011) -- Start
                'dsList = objEmployeeExemption.GetList("Exemption")

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching = "AND employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
                'End If

                'If CInt(cboTransactionHead.SelectedValue) > 0 Then
                '    StrSearching &= "AND tranheadunkid = " & CInt(cboTransactionHead.SelectedValue) & " "
                'End If

                'If CInt(cboPayYear.SelectedValue) > 0 Then
                '    StrSearching &= "AND yearunkid = " & CInt(cboPayYear.SelectedValue) & " "
                'End If

                'If CInt(cboPayPeriod.SelectedValue) > 0 Then
                '    StrSearching &= "AND periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
                'End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objEmployeeExemption.GetList("Exemption", True, CInt(cboEmployee.SelectedValue), CInt(cboTransactionHead.SelectedValue), CInt(cboPayPeriod.SelectedValue), CInt(cboExempApprovStatus.SelectedValue))
                dsList = objEmployeeExemption.GetList(FinancialYear._Object._DatabaseName, _
                                                      User._Object._Userunkid, _
                                                      FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      ConfigParameter._Object._UserAccessModeSetting, True, _
                                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                      "Exemption", True, , , CInt(cboTransactionHead.SelectedValue), CInt(cboPayPeriod.SelectedValue), _
                                                      CInt(cboExempApprovStatus.SelectedValue))
                'S.SANDEEP [04 JUN 2015] -- END


                'Sohail (26 Nov 2011) -- End

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    'Sohail (15 Oct 2010) -- Start
                    'dtTable = New DataView(dsList.Tables("Exemption"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                    dtTable = New DataView(dsList.Tables("Exemption"), StrSearching, "periodunkid", DataViewRowState.CurrentRows).ToTable
                    'Sohail (15 Oct 2010) -- End
                Else
                    'Sohail (15 Oct 2010) -- Start
                    'dtTable = dsList.Tables("Exemption")
                    dtTable = New DataView(dsList.Tables("Exemption"), "", "periodunkid", DataViewRowState.CurrentRows).ToTable
                    'Sohail (15 Oct 2010) -- End

                End If
                lvEmpExemption.Items.Clear()
                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    'Sandeep [ 01 MARCH 2011 ] -- Start
                    'lvItem.Text = dtRow("year").ToString
                    lvItem.Text = ""
                    lvItem.SubItems.Add(dtRow("year").ToString)
                    'Sandeep [ 01 MARCH 2011 ] -- End 


                    lvItem.Tag = CInt(dtRow("exemptionunkid"))
                    lvItem.SubItems.Add(dtRow("period").ToString())
                    'Sohail (12 Aug 2010) -- Start
                    lvItem.SubItems(colhPeriod.Index).Tag = dtRow("periodunkid").ToString
                    'Sohail (12 Aug 2010) -- End

                    'Sohail (28 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)
                    'Sohail (28 Jan 2012) -- End

                    lvItem.SubItems.Add(dtRow("employeename").ToString())
                    lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow("employeeunkid")) 'Sohail (18 Oct 2012)
                    lvItem.SubItems.Add(dtRow("trnheadname").ToString())

                    'Sohail (26 Nov 2011) -- Start
                    If CBool(dtRow.Item("isapproved")) = True Then
                        lvItem.SubItems.Add("A")
                    Else
                        lvItem.SubItems.Add("P")
                    End If
                    'Sohail (26 Nov 2011) -- End

                    'Sohail (09 Oct 2010) -- Start
                    'Sohail (02 Aug 2011) -- Start
                    'RemoveHandler lvEmpExemption.ItemChecked, AddressOf lvEmpExemption_ItemChecked
                    'lvEmpExemption.Items.Add(lvItem)
                    'AddHandler lvEmpExemption.ItemChecked, AddressOf lvEmpExemption_ItemChecked
                    lvArray.Add(lvItem)
                    'Sohail (02 Aug 2011) -- End
                    'Sohail (09 Oct 2010) -- End
                Next

                'Sohail (09 Oct 2010) -- Start
                'Sohail (02 Aug 2011) -- Start
                RemoveHandler lvEmpExemption.ItemChecked, AddressOf lvEmpExemption_ItemChecked
                lvEmpExemption.Items.AddRange(lvArray.ToArray)
                AddHandler lvEmpExemption.ItemChecked, AddressOf lvEmpExemption_ItemChecked
                lvArray = Nothing
                'Sohail (02 Aug 2011) -- End


                lvEmpExemption.GroupingColumn = colhPeriod
                lvEmpExemption.DisplayGroups(True)
                'Sohail (09 Oct 2010) -- End

                'Sohail (26 Nov 2011) -- Start
                'Enhancement : TRA Changes
                If CInt(cboExempApprovStatus.SelectedValue) = enExempHeadApprovalStatus.Approved AndAlso btnDisApprove.Enabled = True Then
                    btnDisApprove.Visible = True
                ElseIf CInt(cboExempApprovStatus.SelectedValue) = enExempHeadApprovalStatus.Pending AndAlso btnApprove.Enabled = True Then
                    btnApprove.Visible = True
                End If
                'Sohail (26 Nov 2011) -- End

                If lvEmpExemption.Items.Count > 5 Then
                    colhTransactionhead.Width = 250 - 18 'Sohail (09 Oct 2010)
                Else
                    colhTransactionhead.Width = 250 'Sohail (09 Oct 2010)
                End If
                lvEmpExemption.GridLines = False 'Sohail (16 Oct 2010)
                lvEmpExemption.EndUpdate() 'Sohail (09 Oct 2010)

                Cursor.Current = Cursors.WaitCursor 'Sohail (10 Feb 2012)


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objTranhead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsFill As New DataSet
        Try
            'FOR EMPLOYEE
            Dim objEmployee As New clsEmployee_Master

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsFill = objEmployee.GetEmployeeList("Employee", True, True)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True)
            'End If
            'Sohail (06 Jan 2012) -- End

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'Anjan [10 June 2015] -- End

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables("Employee")

            'FOR TRANSACTION HEAD
            dsFill = Nothing

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objTranhead.getComboList("TranHead", True)

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            'dsFill = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True)
            dsFill = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , , , , , , , 0)
            'S.SANDEEP [12-JUL-2018] -- END

            'Sohail (21 Aug 2015) -- End
            cboTransactionHead.ValueMember = "tranheadunkid"
            cboTransactionHead.DisplayMember = "name"
            cboTransactionHead.DataSource = dsFill.Tables("TranHead")

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objMaster.getComboListPAYYEAR("Year", True)
            dsFill = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPayYear
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsFill.Tables("Year")
            End With

            'Sohail (26 Nov 2011) -- Start
            dsFill = objMaster.getComboListForExemptHeadApprovalStatus("ExempApproval", True)
            With cboExempApprovStatus
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = New DataView(dsFill.Tables("ExempApproval"), "Id <> " & enExempHeadApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With
            'Sohail (26 Nov 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objTranhead = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeExemption
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeExemption
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeExemption

            'Sohail (26 Nov 2011) -- Start
            'Enhancement : TRA Changes
            btnApprove.Enabled = User._Object.Privilege._AllowToApproveEmpExemtion
            btnDisApprove.Enabled = User._Object.Privilege._AllowToApproveEmpExemtion
            'Sohail (26 Nov 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try


    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployee_Exemption_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployeeExemption = New clsemployee_exemption_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            Call SetVisibility()

            Call SetColor()
            FillCombo()

            cboEmployee.SelectedValue = mintEmployeeID
            cboTransactionHead.SelectedValue = mintTranHeadID
            cboPayYear.SelectedValue = mintYearID
            cboPayPeriod.SelectedValue = mintPeriodID

            'FillList()
            If lvEmpExemption.Items.Count > 0 Then lvEmpExemption.Items(0).Selected = True
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Exemption_List_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Exemption_List_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete And lvEmpExemption.Focused = True Then
                'Sohail (24 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (24 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Exemption_List_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Exemption_List_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Exemption_List_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Exemption_List_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmployeeExemption = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_exemption_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_exemption_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrm As New frmEmployee_Exemption_AddEdit
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            If objfrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillList()
                mblnCancel = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (16 Oct 2010) -- Start
    'Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvEmpExemption.DoubleClick
    '    If lvEmpExemption.SelectedItems.Count < 1 Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exemption from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
    '        lvEmpExemption.Select()
    '        Exit Sub
    '    End If
    '    Dim objfrmExemption_AddEdit As New frmEmployee_Exemption_AddEdit
    '    Dim objPeriod As New clscommom_period_Tran
    '    Try
    '        'Sohail (12 Aug 2010) -- Start
    '        objPeriod._Periodunkid = CInt(lvEmpExemption.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
    '        If objPeriod._Statusid = enStatusType.Close Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You can not Edit/Delete this entry. Reason: Period is closed."), enMsgBoxStyle.Information)
    '            lvEmpExemption.Select()
    '            Exit Try
    '        End If
    '        'Sohail (12 Aug 2010) -- End
    '        Dim intSelectedIndex As Integer
    '        intSelectedIndex = lvEmpExemption.SelectedItems(0).Index
    '        If objfrmExemption_AddEdit.displayDialog(CInt(lvEmpExemption.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
    '            FillList()
    '            mblnCancel = False
    '        End If
    '        objfrmExemption_AddEdit = Nothing

    '        lvEmpExemption.Items(intSelectedIndex).Selected = True
    '        lvEmpExemption.EnsureVisible(intSelectedIndex)
    '        lvEmpExemption.Select()
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
    '    Finally
    '        If objfrmExemption_AddEdit IsNot Nothing Then objfrmExemption_AddEdit.Dispose()
    '        objPeriod = Nothing
    '    End Try
    'End Sub
    'Sohail (16 Oct 2010) -- End



    'Sandeep [ 01 MARCH 2011 ] -- Start
    'Isseu: For multiple delete of exemption ,before only one exemption was deleted.
    'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

    '    If lvEmpExemption.SelectedItems.Count < 1 Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Exemption from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
    '        lvEmpExemption.Select()
    '        Exit Sub
    '    End If
    '    'If objEmployeeExemption.isUsed(CInt(lvEmpExemption.SelectedItems(0).Tag)) Then
    '    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Exemption. Reason: This Exemption is in use."), enMsgBoxStyle.Information) '?2
    '    '    lvEmpExemption.Select()
    '    '    Exit Sub
    '    'End If
    '    Dim objPeriod As New clscommom_period_Tran
    '    Try
    '        Dim intSelectedIndex As Integer
    '        intSelectedIndex = lvEmpExemption.SelectedItems(0).Index
    '        'Sohail (12 Aug 2010) -- Start
    '        objPeriod._Periodunkid = CInt(lvEmpExemption.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
    '        If objPeriod._Statusid = enStatusType.Close Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You can not Edit/Delete this entry. Reason: Period is closed."), enMsgBoxStyle.Information)
    '            lvEmpExemption.Select()
    '            Exit Try
    '        End If
    '        'Sohail (12 Aug 2010) -- End

    '        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Exemption?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

    '            'Sandeep [ 16 Oct 2010 ] -- Start
    '            'objEmployeeExemption._Voidreason = "VOID REASON"
    '            Dim frm As New frmReasonSelection
    '            Dim mstrVoidReason As String = String.Empty
    '            frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
    '            If mstrVoidReason.Length <= 0 Then
    '                Exit Sub
    '            Else
    '                objEmployeeExemption._Voidreason = mstrVoidReason
    '            End If
    '            frm = Nothing
    '            objEmployeeExemption._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            'Sandeep [ 16 Oct 2010 ] -- End 

    '            objEmployeeExemption._Voiduserunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)

    '            objEmployeeExemption.Delete(CInt(lvEmpExemption.SelectedItems(0).Tag))
    '            lvEmpExemption.SelectedItems(0).Remove()

    '            mblnCancel = False
    '            If lvEmpExemption.Items.Count <= 0 Then
    '                Exit Try
    '            End If

    '            If lvEmpExemption.Items.Count = intSelectedIndex Then
    '                intSelectedIndex = lvEmpExemption.Items.Count - 1
    '                lvEmpExemption.Items(intSelectedIndex).Selected = True
    '                lvEmpExemption.EnsureVisible(intSelectedIndex)
    '            ElseIf lvEmpExemption.Items.Count <> 0 Then
    '                lvEmpExemption.Items(intSelectedIndex).Selected = True
    '                lvEmpExemption.EnsureVisible(intSelectedIndex)
    '            End If
    '        End If
    '        lvEmpExemption.Select()
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
    '    Finally
    '        objPeriod = Nothing
    '    End Try
    'End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        If lvEmpExemption.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Check Exemption from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvEmpExemption.Select()
            Exit Sub
        End If

        Dim objPeriod As New clscommom_period_Tran
        Dim StrIds As String = String.Empty
        Dim blnIsUsed As Boolean = False
        'Sohail (18 Oct 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objTnA As New clsTnALeaveTran
        Dim intFirstOpenPeriod As Integer
        Dim dtFirstOpenPeriodEndDate As DateTime = Nothing
        Dim strEmpIDs As String = ""
        'Sohail (18 Oct 2012) -- End
        Try
            'Sohail (18 Oct 2012) -- Start
            'TRA - ENHANCEMENT

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intFirstOpenPeriod = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
            intFirstOpenPeriod = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (18 Oct 2012) -- End
            For Each lvItem As ListViewItem In lvEmpExemption.CheckedItems
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    blnIsUsed = True
                    Exit For
                End If
                StrIds &= "," & lvItem.Tag.ToString
                'Sohail (18 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                If intFirstOpenPeriod = CInt(lvItem.SubItems(colhPeriod.Index).Tag) Then
                    dtFirstOpenPeriodEndDate = objPeriod._End_Date
                    If strEmpIDs.Trim = "" Then
                        strEmpIDs = lvItem.SubItems(colhEmployee.Index).Tag.ToString
                    Else
                        strEmpIDs &= ", " & lvItem.SubItems(colhEmployee.Index).Tag.ToString
                    End If
                End If
                'Sohail (18 Oct 2012) -- End
            Next

            If blnIsUsed = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot Edit/Delete cheked entry(s). Reason: Period is closed for some checked entries."), enMsgBoxStyle.Information)
                lvEmpExemption.Select()
                Exit Try
            End If

            'Sohail (18 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If dtFirstOpenPeriodEndDate <> Nothing Then
                If objTnA.IsPayrollProcessDone(intFirstOpenPeriod, strEmpIDs, dtFirstOpenPeriodEndDate) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to delete exempted head."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to delete exempted head.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 8, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Try
                End If
            End If
            'Sohail (18 Oct 2012) -- End

            If StrIds.Length > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to delete this Exemption?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim frm As New frmReasonSelection
                    'Anjan (02 Sep 2011)-Start
                    'Issue : Including Language Settings.
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    'Anjan (02 Sep 2011)-End 

                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objEmployeeExemption._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing
                    objEmployeeExemption._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                    objEmployeeExemption._Voiduserunkid = User._Object._Userunkid

                    StrIds = Mid(StrIds, 2)

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objEmployeeExemption._FormName = mstrModuleName
                    objEmployeeExemption._LoginEmployeeUnkid = 0
                    objEmployeeExemption._ClientIP = getIP()
                    objEmployeeExemption._HostName = getHostName()
                    objEmployeeExemption._FromWeb = False
                    objEmployeeExemption._AuditUserId = User._Object._Userunkid
objEmployeeExemption._CompanyUnkid = Company._Object._Companyunkid
                    objEmployeeExemption._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployeeExemption.Delete(StrIds)
                    objEmployeeExemption.Delete(StrIds, ConfigParameter._Object._CurrentDateAndTime)
                    'S.SANDEEP [04 JUN 2015] -- END


                    mblnCancel = False
                    If lvEmpExemption.Items.Count <= 0 Then
                        Exit Try
                    End If
                End If
            End If
            Call FillList()
            lvEmpExemption.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Sandeep [ 01 MARCH 2011 ] -- End 

    'Sohail (26 Nov 2011) -- Start
    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnDisApprove.Click
        Dim strMsg As String = ""
        Dim blnIsApprove As Boolean
        Dim btn As Button = CType(sender, Button)
        If btn.Name = btnApprove.Name Then
            blnIsApprove = True
            strMsg = Language.getMessage(mstrModuleName, 5, "Are you sure you want to Approve selected ED Heads?")
        ElseIf btn.Name = btnDisApprove.Name Then
            blnIsApprove = False
            strMsg = Language.getMessage(mstrModuleName, 6, "Are you sure you want to Dispprove selected ED Heads?")
        End If
        If lvEmpExemption.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one ED Heads to Approve/Disapprove."), enMsgBoxStyle.Information)
            lvEmpExemption.Focus()
            Exit Sub
        ElseIf (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
            Exit Sub
        End If

        Dim blnResult As Boolean
        Dim blnRefresh As Boolean = False
        Try
            For Each lvItem As ListViewItem In lvEmpExemption.CheckedItems

                objEmployeeExemption = New clsemployee_exemption_Tran
                With objEmployeeExemption
                    ._Exemptionunkid = CInt(lvItem.Tag)
                    ._Isapproved = blnIsApprove
                    ._Approveruserunkid = User._Object._Userunkid
                    ._FormName = mstrModuleName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'blnResult = .Update()
                    blnResult = .Update(ConfigParameter._Object._CurrentDateAndTime)
                    'S.SANDEEP [04 JUN 2015] -- END

                    If blnResult = False AndAlso objEmployeeExemption._Message <> "" Then
                        eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    blnRefresh = True
                End With
            Next

            If blnRefresh = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Nov 2011) -- End

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboTransactionHead.SelectedIndex = 0
            cboPayYear.SelectedValue = 0
            cboPayPeriod.SelectedValue = 0
            If cboExempApprovStatus.Items.Count > 0 Then cboExempApprovStatus.SelectedIndex = 0 'Sohail (26 Nov 2011)
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'End If
            'Sohail (06 Jan 2012) -- End

            With cboEmployee
                objfrm.DataSource = CType(.DataSource, DataTable) 'Anjan [10 June 2015] -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub
#End Region

#Region " ComboBox's Events "
    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
#End Region

#Region " Contols "

#End Region


    'Sandeep [ 01 MARCH 2011 ] -- Start
    Private Sub Do_Operation(ByVal blnOperation As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmpExemption.Items
                lvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvEmpExemption.ItemChecked, AddressOf lvEmpExemption_ItemChecked
            Call Do_Operation(CBool(objChkAll.CheckState))
            AddHandler lvEmpExemption.ItemChecked, AddressOf lvEmpExemption_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEmpExemption_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmpExemption.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvEmpExemption.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvEmpExemption.CheckedItems.Count < lvEmpExemption.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvEmpExemption.CheckedItems.Count = lvEmpExemption.Items.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmpExemption_ItemChecked", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 01 MARCH 2011 ] -- End 


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbEmployeeExempetion.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeExempetion.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisApprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeExempetion.Text = Language._Object.getCaption(Me.gbEmployeeExempetion.Name, Me.gbEmployeeExempetion.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhYear.Text = Language._Object.getCaption(CStr(Me.colhYear.Tag), Me.colhYear.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhTransactionhead.Text = Language._Object.getCaption(CStr(Me.colhTransactionhead.Tag), Me.colhTransactionhead.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.lblAccess.Text = Language._Object.getCaption(Me.lblAccess.Name, Me.lblAccess.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnDisApprove.Text = Language._Object.getCaption(Me.btnDisApprove.Name, Me.btnDisApprove.Text)
            Me.lblApprovalStatus.Text = Language._Object.getCaption(Me.lblApprovalStatus.Name, Me.lblApprovalStatus.Text)
            Me.colhApprovePending.Text = Language._Object.getCaption(CStr(Me.colhApprovePending.Tag), Me.colhApprovePending.Text)
            Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Are you sure you want to delete this Exemption?")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot Edit/Delete cheked entry(s). Reason: Period is closed for some checked entries.")
            Language.setMessage(mstrModuleName, 3, "Please Check Exemption from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 4, "Please Check atleast one ED Heads to Approve/Disapprove.")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to Approve selected ED Heads?")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to Dispprove selected ED Heads?")
            Language.setMessage(mstrModuleName, 7, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to delete exempted head.")
			Language.setMessage(mstrModuleName, 8, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


