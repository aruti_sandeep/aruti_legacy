﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLoanParameter

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmLoanParameter"
    Private mblnCancel As Boolean = True
    Private mintTranUnkid As Integer = -1
    Private mintParameterId As enParameterMode
    Private mintLoanAdvanceUnkid As Integer = 0
    Private objlnInterest As clslnloan_interest_tran
    Private objlnEMI As clslnloan_emitenure_tran
    Private objlnTopUp As clslnloan_topup_tran
    Private mdtLoanEffectiveDate As Date = Nothing
    Private mintCalcTypeId As enLoanCalcId
    Private mActionMode As enAction  
    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    Private mintCountryUnkid As Integer = 0
    'Nilay (15-Dec-2015) -- Start
    'Private dsLoanInfo As New DataSet
    Private dsLoanInfo As DataSet
    'Nilay (15-Dec-2015) -- End
    Private mblnIsFormLoad As Boolean = False
    Private mdtBalanceEndDate As Date = Nothing
    Private mintEmployeeId As Integer = 0
    Private mdecInstrAmount As Decimal = 0
    Private mdecNetAmount As Decimal = 0
    Private mintInterestCalcTypeId As enLoanInterestCalcType 'Sohail (15 Dec 2015)
    'Nilay (25-Mar-2016) -- Start
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    'Nilay (25-Mar-2016) -- End

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    'Private mblnIsAllowChangePeriod As Boolean = False
    'Private mblnIsFromApprovedOp As Boolean = False
    Private objlnOpApprovaltran As clsloanotherop_approval_tran
    Private mblnAllowChangeStatus As Boolean = False
    Private mintLoanSchemeunkid As Integer = -1
    Private mintlnOtherOptranunkid As Integer = -1
    Private mintNoOfInstl As Integer = 1
    Private mdecInterestRate As Decimal = 0
    'Nilay (01-Apr-2016) -- End

    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private mintAppPriority As Integer = -1
    'Shani (21-Jul-2016) -- End


#End Region

#Region " Display Dialog "

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    'Public Function displayDialog(ByRef intUnkId As Integer, _
    '                              ByVal xLoanAdvUnkid As Integer, _
    '                              ByVal xMode As enAction, _
    '                              ByVal blnIsFromApprovedOp As Boolean, _
    '                              ByVal blnAllowChangeStatus As Boolean, _
    '                              Optional ByVal xParamId As Integer = 0, _
    '                              Optional ByVal blnAllowChangePeriod As Boolean = True) As Boolean

    Public Function displayDialog(ByRef intUnkId As Integer, _
                                  ByVal xLoanAdvUnkid As Integer, _
                                  ByVal xMode As enAction, _
                              ByVal blnAllowChangeStatus As Boolean, _
                              Optional ByVal xParamId As Integer = 0) As Boolean
        Try
            mintTranUnkid = intUnkId
            If xParamId > 0 Then
                mintParameterId = CType(xParamId, enParameterMode)
            End If
            mintLoanAdvanceUnkid = xLoanAdvUnkid

            Dim objLnAdv As New clsLoan_Advance
            objLnAdv._Loanadvancetranunkid = xLoanAdvUnkid
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            mintLoanSchemeunkid = objLnAdv._Loanschemeunkid
            mblnAllowChangeStatus = blnAllowChangeStatus
            'Nilay (01-Apr-2016) -- End
            mintCalcTypeId = CType(objLnAdv._Calctype_Id, enLoanCalcId)
            mintInterestCalcTypeId = CType(objLnAdv._Interest_Calctype_Id, enLoanInterestCalcType) 'Sohail (15 Dec 2015)
            mintCountryUnkid = objLnAdv._CountryUnkid
            mdtLoanEffectiveDate = objLnAdv._Effective_Date
            mintEmployeeId = objLnAdv._Employeeunkid

            'If mintCalcTypeId = enLoanCalcId.Simple_Interest Or mintCalcTypeId = enLoanCalcId.No_Interest Then
            '    radInterestRate.Enabled = False
            'End If
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If mintCalcTypeId = enLoanCalcId.No_Interest Then
            If mintCalcTypeId = enLoanCalcId.No_Interest OrElse mintCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head Then
                'Sohail (29 Apr 2019) -- End
                radInterestRate.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If mintCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head Then
                radInstallment.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End
            objLnAdv = Nothing
            mActionMode = xMode

            txtInstallmentAmt.ReadOnly = False
            radInterestRate.Checked = False : radInstallment.Checked = False : radTopup.Checked = False

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                txtPrincipalAmt.ReadOnly = False
            Else
                txtPrincipalAmt.ReadOnly = True
            End If
            'Sohail (15 Dec 2015) -- End

            Call SetFormControls()

            If mActionMode <> enAction.EDIT_ONE Then
                pnlControls.Enabled = False
            End If

            Me.ShowDialog()

            intUnkId = mintTranUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetFormControls()
        Try
            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    'Nilay (15-Dec-2015) -- Start
                    'txtInstallmentAmt.Enabled = False
                    lblEMIAmount.Visible = True
                    txtInstallmentAmt.Visible = True
                    txtInstallmentAmt.Enabled = True
                    txtInstallmentAmt.ReadOnly = True
                    lblTopupAmt.Visible = False
                    txtTopupAmt.Visible = False
                    'Nilay (15-Dec-2015) -- End

                    txtTopupAmt.Enabled = False
                    txtLoanInterest.Enabled = True
                    nudDuration.Enabled = False
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'dtpEffectiveDate.Enabled = True

                    'Nilay (13-Sept-2016) -- Start
                    'Enhancement : Enable Default Parameter Edit & other fixes
                    'If mintInterestCalcTypeId = enLoanInterestCalcType.DAILY Then
                    '    dtpEffectiveDate.Enabled = True
                    'Else
                    '    dtpEffectiveDate.Enabled = False
                    'End If
                    If mActionMode <> enAction.EDIT_ONE Then
                    If mintInterestCalcTypeId = enLoanInterestCalcType.DAILY Then
                    dtpEffectiveDate.Enabled = True
                    Else
                        dtpEffectiveDate.Enabled = False
                    End If
                    ElseIf mActionMode = enAction.EDIT_ONE Then
                        If mblnAllowChangeStatus = True Then
                            dtpEffectiveDate.Enabled = True
                        Else
                            dtpEffectiveDate.Enabled = False
                        End If
                    End If
                    'Nilay (13-Sept-2016) -- End
                    txtPrincipalAmt.Enabled = False
                    txtInterestAmt.Enabled = True 'Nilay (15-Dec-2015) -- txtInterestAmt.Enabled = False
                    'Sohail (15 Dec 2015) -- End

                Case enParameterMode.LN_EMI
                    'Nilay (15-Dec-2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'nudDuration.Enabled = True
                    If mintCalcTypeId = enLoanCalcId.No_Interest Then
                        txtInstallmentAmt.ReadOnly = False
                        nudDuration.Enabled = False
                    Else
                        txtInstallmentAmt.ReadOnly = True
                        nudDuration.Enabled = True
                    End If

                    'Nilay (15-Dec-2015) -- End
                    txtInstallmentAmt.Enabled = True
                    txtTopupAmt.Enabled = False
                    txtLoanInterest.Enabled = False
                    lblEMIAmount.Visible = True
                    txtInstallmentAmt.Visible = True
                    txtTopupAmt.Visible = False
                    lblTopupAmt.Visible = False
                    dtpEffectiveDate.Enabled = False
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    txtPrincipalAmt.Enabled = True
                    txtInterestAmt.Enabled = False 'Nilay (15-Dec-2015) -- txtInterestAmt.Enabled = True 
                    'Sohail (15 Dec 2015) -- End

                Case enParameterMode.LN_TOPUP
                    txtInstallmentAmt.Enabled = False
                    txtTopupAmt.Enabled = True
                    nudDuration.Enabled = False
                    txtLoanInterest.Enabled = False
                    lblEMIAmount.Visible = False
                    txtInstallmentAmt.Visible = False
                    txtTopupAmt.Visible = True
                    lblTopupAmt.Visible = True
                    dtpEffectiveDate.Enabled = False
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    txtPrincipalAmt.Enabled = False
                    txtInterestAmt.Enabled = False
                    'Sohail (15 Dec 2015) -- End
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFormControls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtInstallmentAmt.BackColor = GUI.ColorComp
            txtTopupAmt.BackColor = GUI.ColorComp
            txtLoanInterest.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objPrd As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim mdtTable As DataTable = Nothing
        Try
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombos = objPrd.getListForCombo(enModuleReference.Payroll, _
                                              FinancialYear._Object._YearUnkid, _
                                              FinancialYear._Object._DatabaseName, _
                                              FinancialYear._Object._Database_Start_Date, _
                                              "List", True, enStatusType.Open)
            'Nilay (10-Oct-2015) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            If intFirstPeriodId > 0 Then
                mdtTable = New DataView(dsCombos.Tables("List"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsCombos.Tables("List"), "1=2", "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = mdtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            Dim objLoan As New clsProcess_pending_loan

            'Nilay (09-Aug-2016) -- Start
            'dsCombos = objLoan.GetLoan_Status("Status", False, False)
            dsCombos = objLoan.GetLoan_Status("Status", True, False)
            Dim dtView As DataView = New DataView(dsCombos.Tables("Status"), "Id <> 1", "", DataViewRowState.CurrentRows)
            'Nilay (09-Aug-2016) -- End
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                'Nilay (09-Aug-2016) -- Start
                '.DataSource = dsCombos.Tables("Status")
                .DataSource = dtView
                .SelectedValue = 0
                'Nilay (09-Aug-2016) -- End
            End With
            objLoan = Nothing
            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPrd = Nothing : objMaster = Nothing
        End Try
    End Sub

    Private Function Valid_Data() As Boolean
        Try
            If mintCalcTypeId = enLoanCalcId.No_Interest Then
                If radInstallment.Checked = False AndAlso _
                   radTopup.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please select atleast one operation type to continue."), enMsgBoxStyle.Information)
                    Return False 'Sohail (29 Apr 2019)
                End If
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf mintCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head Then
                If radTopup.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please select atleast one operation type to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (29 Apr 2019) -- End
            Else
                If radInterestRate.Checked = False AndAlso _
                   radInstallment.Checked = False AndAlso _
                   radTopup.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please select atleast one operation type to continue."), enMsgBoxStyle.Information)
                    Return False 'Sohail (29 Apr 2019)
                End If
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            'Nilay (09-Aug-2016) -- Start
            If mblnAllowChangeStatus Then
                If CInt(cboStatus.SelectedValue) = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select atleast one Status from the list to perform further operation."), enMsgBoxStyle.Information)
                    cboStatus.Focus()
                    Return False
                End If
            End If
            'Nilay (09-Aug-2016) -- End

            Dim objTnaLeaveTran As New clsTnALeaveTran
            Dim objPeriod As New clscommom_period_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Nilay (10-Oct-2015) -- End

            If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), mintEmployeeId.ToString, objPeriod._End_Date) Then
                Select Case mintParameterId
                    Case enParameterMode.LN_RATE
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add this Interest Rate Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add this Interest Rate Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                    Case enParameterMode.LN_EMI
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot add this Installment Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot add this Installment Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                    Case enParameterMode.LN_TOPUP
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot add this Topup Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot add this Topup Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                End Select
                objPeriod = Nothing
                objTnaLeaveTran = Nothing
                Return False
            End If
            objTnaLeaveTran = Nothing : objPeriod = Nothing

            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    If txtLoanInterest.Decimal <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Interest Rate is mandatory information. Please set Interest Rate to continue."), enMsgBoxStyle.Information)
                        txtLoanInterest.Focus()
                        Return False
                    End If

                    'Nilay (13-Sept-2016) -- Start
                    'Enhancement : Enable Default Parameter Edit & other fixes
                    'Dim strMsg As String = ""
                    'strMsg = objlnInterest.IsValidInterestRate(mintLoanAdvanceUnkid, dtpEffectiveDate.Value.Date, txtLoanInterest.Decimal)
                    'If strMsg <> "" Then
                    '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    '    txtLoanInterest.Focus()
                    '    Return False
                    'End If
                    If mActionMode <> enAction.EDIT_ONE Then
                    Dim strMsg As String = ""
                    strMsg = objlnInterest.IsValidInterestRate(mintLoanAdvanceUnkid, dtpEffectiveDate.Value.Date, txtLoanInterest.Decimal)
                    If strMsg <> "" Then
                        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                        txtLoanInterest.Focus()
                        Return False
                    End If
                    End If
                    'Nilay (13-Sept-2016) -- End

                Case enParameterMode.LN_EMI
                    If mintCalcTypeId = enLoanCalcId.No_Interest Then
                        If txtInstallmentAmt.Decimal <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Installment amount is mandatory information. Please set Installment amount to continue."), enMsgBoxStyle.Information)
                            txtInstallmentAmt.Focus()
                            Return False
                        End If
                    End If
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                        If txtPrincipalAmt.Decimal <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Principal amount is mandatory information. Please set Principal amount to continue."), enMsgBoxStyle.Information)
                            txtPrincipalAmt.Focus()
                            Return False
                        End If
                    End If
                    'Sohail (15 Dec 2015) -- End
                    'Sohail (26 Mar 2018) -- Start
                    'Le Grand Casino and Plam Beach Hotel and Casino Issue - Support Issue Id # 0002139 - 70.2 - Unable to change loan installment for no interest loan due to loan end date doesnt change when topups were added in previous periods.
                    'If DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1).Date <= mdtBalanceEndDate.Date Then
                    If Not (mintCalcTypeId = CInt(enLoanCalcId.No_Interest) OrElse mintCalcTypeId = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI)) AndAlso DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1).Date <= mdtBalanceEndDate.Date Then
                        'Sohail (26 Mar 2018) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Loan Stop Date should be greater than") & " " & mdtBalanceEndDate.ToShortDateString, enMsgBoxStyle.Information)
                        txtInstallmentAmt.Focus()
                        Return False
                    End If
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = mintLoanSchemeunkid
                    If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(nudDuration.Value) > objLoanScheme._MaxNoOfInstallment Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 18, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 19, " Scheme."), enMsgBoxStyle.Information)
                        nudDuration.Focus()
                        Exit Function
                    End If
                    'Varsha (25 Nov 2017) -- End
                Case enParameterMode.LN_TOPUP
                    If txtTopupAmt.Decimal <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Topup amount is mandatory information. Please set Totup amount to continue."), enMsgBoxStyle.Information)
                        txtTopupAmt.Focus()
                        Return False
                    End If
            End Select

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemark.Text.Trim.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Remark cannot be blank. Remark is compulsory information."), enMsgBoxStyle.Information)
            '    txtRemark.Focus()
            '    Return False
            'End If
            If mblnAllowChangeStatus = True Then
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemark.Text.Trim.Length <= 0 Then
                If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED AndAlso txtRemark.Text.Trim.Length <= 0 Then
                    'Nilay (20-Sept-2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Remark cannot be blank. Remark is compulsory information."), enMsgBoxStyle.Information)
                txtRemark.Focus()
                Return False
            End If
            End If
            'Nilay (13-Sept-2016) -- End
            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Valid_Data", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Function IsSaveAllowed(ByVal xColumnToMatch As String, _
                                   ByVal xColumnUnkid As Integer) As Boolean
        Try
            Dim objLnAdv As New clsLoan_Advance
            Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceUnkid)
            objLnAdv = Nothing

            If mActionMode <> enAction.EDIT_ONE Then
                If ds.Tables("List").Rows.Count > 0 Then
                    If ds.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(dtpEffectiveDate.Value.Date) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Effective date should not less than last transactoin date") & " " & Format(eZeeDate.convertDate(ds.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & ".", enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            Else
                Dim strMsg As String = String.Empty
                If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item(xColumnToMatch)) = xColumnUnkid) Then
                    If ds.Tables("List").Rows.Count > 0 Then
                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = Language.getMessage("clslnloan_interest_tran", 3, " Process Payroll.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                            strMsg = Language.getMessage("clslnloan_interest_tran", 4, " Loan Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                            strMsg = Language.getMessage("clslnloan_interest_tran", 5, " Receipt Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = Language.getMessage("clslnloan_interest_tran", 6, " Installment Change.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = Language.getMessage("clslnloan_interest_tran", 7, " Topup Added.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = Language.getMessage("clslnloan_interest_tran", 8, " Rate Change.")
                        End If
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot save this transaction. Please delete last transaction from the screen of") & strMsg, enMsgBoxStyle.Information)
                    Return False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsSaveAllowed", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    'Private Sub GetValue()
    '    Try
    '        Select Case mintParameterId
    '            Case enParameterMode.LN_RATE

    '                txtLoanInterest.Decimal = CDec(objlnInterest._Interest_Rate)
    '                radInterestRate.Checked = True
    '                If objlnInterest._Effectivedate <> Nothing Then
    '                    'Nilay (28-Aug-2015) -- Start
    '                    'dtpEffectiveDate.Value = objlnInterest._Effectivedate
    '                    dtpEffectiveDate.Value = CDate(objlnInterest._Effectivedate).Date
    '                    'Nilay (28-Aug-2015) -- End
    '                End If
    '            Case enParameterMode.LN_EMI

    '                nudDuration.Value = objlnEMI._Emi_Tenure
    '                txtInstallmentAmt.Tag = objlnEMI._Emi_Amount
    '                txtInstallmentAmt.Decimal = CDec(Format(objlnEMI._Emi_Amount, GUI.fmtCurrency))
    '                radInstallment.Checked = True
    '                If objlnEMI._Effectivedate <> Nothing Then
    '                    'Nilay (28-Aug-2015) -- Start
    '                    'dtpEffectiveDate.Value = objlnEMI._Effectivedate
    '                    dtpEffectiveDate.Value = CDate(objlnEMI._Effectivedate).Date
    '                    'Nilay (28-Aug-2015) -- End
    '                End If
    '            Case enParameterMode.LN_TOPUP
                   
    '                txtTopupAmt.Tag = objlnTopUp._Topup_Amount
    '                txtTopupAmt.Decimal = CDec(Format(objlnTopUp._Topup_Amount, GUI.fmtCurrency))
    '                radTopup.Checked = True
    '                If objlnTopUp._Effectivedate <> Nothing Then
    '                    'Nilay (28-Aug-2015) -- Start
    '                    'dtpEffectiveDate.Value = objlnTopUp._Effectivedate
    '                    dtpEffectiveDate.Value = CDate(objlnTopUp._Effectivedate).Date
    '                    'Nilay (28-Aug-2015) -- End
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (01-Apr-2016) -- End
                    
                    

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub SetValue()
        Try
            Dim strGuidValue As String

            If mblnAllowChangeStatus = True Then
                objlnOpApprovaltran._LoanOtherOptranunkid = mintTranUnkid
            Else
                strGuidValue = Guid.NewGuid.ToString()
                objlnOpApprovaltran._Identify_Guid = strGuidValue
            End If

            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    objlnOpApprovaltran._LoanAdvancetranunkid = mintLoanAdvanceUnkid
                    objlnOpApprovaltran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objlnOpApprovaltran._LoanOperationTypeId = enParameterMode.LN_RATE
                    objlnOpApprovaltran._EffectiveDate = dtpEffectiveDate.Value
                    objlnOpApprovaltran._InterestRate = txtLoanInterest.Decimal

                    objlnOpApprovaltran._Userunkid = User._Object._Userunkid
                    objlnOpApprovaltran._Isvoid = False
                    objlnOpApprovaltran._Voiduserunkid = -1
                    objlnOpApprovaltran._VoidDateTime = Nothing
                    objlnOpApprovaltran._VoidReason = ""
                    objlnOpApprovaltran._FinalStatus = 1 'Pending

                    'objlnOpApprovaltran._Identify_Guid = strGuidValue
                    'If mActionMode = enAction.EDIT_ONE Then
                    '    objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                    'End If

                Case enParameterMode.LN_EMI
                    objlnOpApprovaltran._LoanAdvancetranunkid = mintLoanAdvanceUnkid
                    objlnOpApprovaltran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objlnOpApprovaltran._LoanOperationTypeId = enParameterMode.LN_EMI
                    objlnOpApprovaltran._EffectiveDate = dtpEffectiveDate.Value
                    objlnOpApprovaltran._EmiTenure = CInt(nudDuration.Value)
                    objlnOpApprovaltran._EmiAmount = txtInstallmentAmt.Decimal
                    objlnOpApprovaltran._LoanDuration = CInt(nudDuration.Value)
                    'Hemant (01 Dec 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-525(Aflife) - No. of Installments not changing after installment amount change.
                    'objlnOpApprovaltran._EndDate = DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1)
                    objlnOpApprovaltran._EndDate = DateAdd(DateInterval.Month, nudDuration.Value, dtpEffectiveDate.Value).AddDays(-1)
                    'Hemant (01 Dec 2021) -- End
                    objlnOpApprovaltran._PrincipalAmount = txtPrincipalAmt.Decimal
                    objlnOpApprovaltran._InterestAmount = txtInterestAmt.Decimal
                    objlnOpApprovaltran._ExchangeRate = mdecPaidExRate
                    objlnOpApprovaltran._BaseCurrencyAmount = txtInstallmentAmt.Decimal / mdecPaidExRate

                    objlnOpApprovaltran._Userunkid = User._Object._Userunkid
                    objlnOpApprovaltran._Isvoid = False
                    objlnOpApprovaltran._Voiduserunkid = -1
                    objlnOpApprovaltran._VoidDateTime = Nothing
                    objlnOpApprovaltran._VoidReason = ""
                    objlnOpApprovaltran._FinalStatus = 1 'Pending

                    'objlnOpApprovaltran._Identify_Guid = strGuidValue
                    'If mActionMode = enAction.EDIT_ONE Then
                    '    objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                    'End If

                Case enParameterMode.LN_TOPUP
                    objlnOpApprovaltran._LoanAdvancetranunkid = mintLoanAdvanceUnkid
                    objlnOpApprovaltran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objlnOpApprovaltran._LoanOperationTypeId = enParameterMode.LN_TOPUP
                    objlnOpApprovaltran._EffectiveDate = dtpEffectiveDate.Value
                    objlnOpApprovaltran._TopupAmount = txtTopupAmt.Decimal
                    objlnOpApprovaltran._ExchangeRate = mdecPaidExRate

                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                    'objlnOpApprovaltran._BaseCurrencyAmount = txtTopupAmt.Decimal / mdecPaidExRate
                    objlnOpApprovaltran._BaseCurrencyAmount = txtTopupAmt.Decimal / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                    'Shani (21-Jul-2016) -- End



                    objlnOpApprovaltran._Userunkid = User._Object._Userunkid
                    objlnOpApprovaltran._Isvoid = False
                    objlnOpApprovaltran._Voiduserunkid = -1
                    objlnOpApprovaltran._VoidDateTime = Nothing
                    objlnOpApprovaltran._VoidReason = ""
                    objlnOpApprovaltran._FinalStatus = 1 'Pending

                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    objlnOpApprovaltran._Isfromess = False
                    ' Varsha Rana (12-Sept-2017) -- End


                    'objlnOpApprovaltran._Identify_Guid = strGuidValue
                    'If mActionMode = enAction.EDIT_ONE Then
                    '    objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                    'End If

            End Select

            If mblnAllowChangeStatus = True Then
                'objlnOpApprovaltran._LoanOtherOptranunkid = mintTranUnkid
                objlnOpApprovaltran._Employeeunkid = mintEmployeeId
                objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                objlnOpApprovaltran._Statusunkid = CInt(cboStatus.SelectedValue)
                objlnOpApprovaltran._Remark = txtRemark.Text
                'objlnOpApprovaltran._FinalStatus = CInt(cboStatus.SelectedValue)
            Else
                'objlnOpApprovaltran._FinalStatus = 1 'Pending
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub SetValue()
    '    Try
    '        Select Case mintParameterId
    '            Case enParameterMode.LN_RATE
    '                'Nilay (28-Aug-2015) -- Start
    '                objlnInterest._Effectivedate = dtpEffectiveDate.Value
    '                'objlnInterest._Effectivedate = CType(dtpEffectiveDate.Value.Date & " " & Format(Now, "hh:mm:ss tt"), DateTime)
    '                'Nilay (28-Aug-2015) -- End
    '                objlnInterest._Interest_Rate = txtLoanInterest.Decimal
    '                objlnInterest._Isvoid = False
    '                objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
    '                objlnInterest._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                objlnInterest._Userunkid = User._Object._Userunkid
    '                objlnInterest._Voiddatetime = Nothing
    '                objlnInterest._Voidreason = ""
    '                objlnInterest._Voiduserunkid = -1

    '            Case enParameterMode.LN_EMI
    '                'Nilay (28-Aug-2015) -- Start
    '                objlnEMI._Effectivedate = dtpEffectiveDate.Value
    '                'objlnEMI._Effectivedate = CType(dtpEffectiveDate.Value.Date & " " & Format(Now, "hh:mm:ss tt"), DateTime)
    '                'Nilay (28-Aug-2015) -- End
    '                objlnEMI._Emi_Tenure = CInt(nudDuration.Value)
    '                objlnEMI._End_Date = DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1)
    '                objlnEMI._Emi_Amount = txtInstallmentAmt.Decimal
    '                objlnEMI._Exchange_rate = mdecPaidExRate
    '                objlnEMI._Basecurrency_amount = txtInstallmentAmt.Decimal / mdecPaidExRate
    '                objlnEMI._Loan_Duration = CInt(nudDuration.Value)
    '                objlnEMI._Isvoid = False
    '                objlnEMI._Voiddatetime = Nothing
    '                objlnEMI._Voidreason = ""
    '                objlnEMI._Voiduserunkid = -1
    '                objlnEMI._Userunkid = User._Object._Userunkid
    '                objlnEMI._Loanadvancetranunkid = mintLoanAdvanceUnkid
    '                objlnEMI._Periodunkid = CInt(cboPeriod.SelectedValue)

    '            Case enParameterMode.LN_TOPUP
    '                'Nilay (28-Aug-2015) -- Start
    '                objlnTopUp._Effectivedate = dtpEffectiveDate.Value
    '                'objlnTopUp._Effectivedate = CType(dtpEffectiveDate.Value.Date & " " & Format(Now, "hh:mm:ss tt"), DateTime)
    '                'Nilay (28-Aug-2015) -- End
    '                objlnTopUp._Exchange_rate = mdecPaidExRate
    '                objlnTopUp._Isvoid = False
    '                objlnTopUp._Loanadvancetranunkid = mintLoanAdvanceUnkid
    '                objlnTopUp._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                objlnTopUp._Topup_Amount = txtTopupAmt.Decimal
    '                objlnTopUp._Userunkid = User._Object._Userunkid
    '                objlnTopUp._Voiddatetime = Nothing
    '                objlnTopUp._Voidreason = ""
    '                objlnTopUp._Voiduserunkid = -1
    '                objlnTopUp._Basecurrency_amount = txtTopupAmt.Decimal / mdecPaidExRate

    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (01-Apr-2016) -- End

    Private Sub SetDateValue()
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                dtpEffectiveDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
                dtpEffectiveDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date
                Dim dtEndDate As Date = Nothing

                If mintParameterId = enParameterMode.LN_RATE Then
                    Dim dsBal As New DataSet
                    Dim objLoanAdvance As New clsLoan_Advance
                    'Nilay (19-Dec-2016) -- Start
                    objLoanAdvance._Loanadvancetranunkid = mintLoanAdvanceUnkid
                    'Nilay (19-Dec-2016) -- End
                    dsBal = objLoanAdvance.GetLastLoanBalance("List", mintLoanAdvanceUnkid)
                    'Nilay (19-Dec-2016) -- Start
                    'objLoanAdvance = Nothing
                    'If dsBal.Tables(0).Rows.Count > 0 Then
                    '    dtEndDate = eZeeDate.convertDate(dsBal.Tables(0).Rows(0).Item("end_date").ToString).Date.AddDays(1)
                    'End If
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        If CDate(objLoanAdvance._Effective_Date).Date = eZeeDate.convertDate(dsBal.Tables(0).Rows(0).Item("end_date").ToString).Date Then
                            dtEndDate = eZeeDate.convertDate(dsBal.Tables(0).Rows(0).Item("end_date").ToString).Date
                        Else
                        dtEndDate = eZeeDate.convertDate(dsBal.Tables(0).Rows(0).Item("end_date").ToString).Date.AddDays(1)
                    End If
                End If
                    objLoanAdvance = Nothing
                    'Nilay (19-Dec-2016) -- End
                End If

                Dim objPeriod As New clscommom_period_Tran
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Nilay (25-Mar-2016) -- Start
                mdtStartDate = objPeriod._Start_Date.Date
                mdtEndDate = objPeriod._End_Date.Date
                'Nilay (25-Mar-2016) -- End
                'Nilay (10-Oct-2015) -- End

                If dtEndDate <> Nothing Then
                    dtpEffectiveDate.MinDate = dtEndDate
                    dtpEffectiveDate.Value = dtEndDate
                Else
                    dtpEffectiveDate.MinDate = objPeriod._Start_Date.Date
                    dtpEffectiveDate.Value = objPeriod._Start_Date.Date
                End If
                dtpEffectiveDate.MaxDate = objPeriod._End_Date
                objPeriod = Nothing
            End If

            Dim objExRate As New clsExchangeRate
            Dim dsList As New DataSet
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , mintCountryUnkid, True, dtpEffectiveDate.Value.Date, True)
            If dsList.Tables("ExRate").Rows.Count > 0 Then
                mdecBaseExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
            objExRate = Nothing

            Dim objLnAdv As New clsLoan_Advance

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.

            'Nilay (25-Mar-2016) -- Start
            'dsLoanInfo = objLnAdv.Calculate_LoanBalanceInfo("List", dtpEffectiveDate.Value, "", mintLoanAdvanceUnkid, , , True)
            dsLoanInfo = objLnAdv.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                                                            User._Object._Userunkid, _
                                                            FinancialYear._Object._YearUnkid, _
                                                            Company._Object._Companyunkid, _
                                                            mdtStartDate, _
                                                            mdtEndDate, _
                                                            ConfigParameter._Object._UserAccessModeSetting, _
                                                            True, "List", dtpEffectiveDate.Value, "", mintLoanAdvanceUnkid, , , True)
            'Nilay (25-Mar-2016) -- End

            'If mdtLoanEffectiveDate.Date <> dtpEffectiveDate.Value.Date Then
            '    dsLoanInfo = objLnAdv.Calculate_LoanBalanceInfo("List", dtpEffectiveDate.Value, "", mintLoanAdvanceUnkid, , , True)
            'Else
            '    dsLoanInfo = objLnAdv.Calculate_LoanBalanceInfo("List", dtpEffectiveDate.Value.Date.AddDays(1), "", mintLoanAdvanceUnkid, , , True)
            'End If
            'Nilay (15-Dec-2015) -- End

            dsList = New DataSet
            dsList = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceUnkid)
            If dsList.Tables(0).Rows.Count > 0 Then
                mdtBalanceEndDate = CDate(eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("end_date").ToString))
            End If
            dsList.Dispose()
            objLnAdv = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDateValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Calculate_Loan_Installment_Amount(ByVal eLnCalcType As enLoanCalcId _
                                                  , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                  , Optional ByVal blnIsDurationChange As Boolean = False _
                                                  )
        'Sohail (15 Dec 2015) - [eIntRateCalcTypeID]
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            mblnIsFormLoad = False
            If dsLoanInfo IsNot Nothing AndAlso dsLoanInfo.Tables(0).Rows.Count > 0 Then

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'If eLnCalcType <> enLoanCalcId.No_Interest Then
                If eLnCalcType <> enLoanCalcId.No_Interest AndAlso eLnCalcType <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                    'Sohail (29 Apr 2019) -- End
                    Dim mdecInstallmentAmount As Decimal = 0
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    Dim mdecTotInstallmentAmount As Decimal = 0
                    Dim mdecTotIntrstAmount As Decimal = 0
                    'Sohail (15 Dec 2015) -- End

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, dtpEffectiveDate.Value.Date)
                    'objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(Format(CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate), GUI.fmtCurrency)), CInt(DateDiff(DateInterval.Day, dtpEffectiveDate.Value.Date, dtEndDate.Date)), CDec(dsLoanInfo.Tables(0).Rows(0).Item("interest_rate")), eLnCalcType, CInt(nudDuration.Value), mdecInstrAmount, mdecInstallmentAmount)
                    Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate.Date).AddDays(-1)
                    Dim dtFPeriodStart As Date = dtpEffectiveDate.Value.Date
                    Dim dtFPeriodEnd As Date = dtEndDate
                    If CInt(cboPeriod.SelectedValue) > 0 Then
                        Dim objPeriod As New clscommom_period_Tran
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                        'Nilay (25-Mar-2016) -- Start
                        'dtFPeriodStart = objPeriod._Start_Date
                        dtFPeriodStart = objPeriod._Start_Date
                        'Nilay (25-Mar-2016) -- End
                        dtFPeriodEnd = objPeriod._End_Date
                    End If
                    Dim decOrigInstAmt As Decimal = CDec(dsLoanInfo.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                    If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                        decOrigInstAmt = txtPrincipalAmt.Decimal
                    End If

                    'Nilay (15-Dec-2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.

                    'Nilay (25-Mar-2016) -- Start
                    'Dim dsLoan As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo("List", dtpEffectiveDate.Value.Date, mintEmployeeId.ToString, mintLoanAdvanceUnkid)
                    Dim dsLoan As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                                                                                      User._Object._Userunkid, _
                                                                                      FinancialYear._Object._YearUnkid, _
                                                                                      Company._Object._Companyunkid, _
                                                                                      dtFPeriodStart, dtFPeriodEnd, _
                                                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                                                      True, "List", _
                                                                                      dtpEffectiveDate.Value.Date, mintEmployeeId.ToString, mintLoanAdvanceUnkid)
                    'Nilay (25-Mar-2016) -- End

                    Dim decDueBalanceAmt As Decimal = CDec(dsLoanInfo.Tables("List").Rows(0).Item("BalanceAmount"))
                    If dsLoan.Tables("List").Rows.Count > 0 Then
                        decDueBalanceAmt = CDec(dsLoan.Tables("List").Rows(0).Item("LastProjectedBalance"))
                    End If
                    'Sohail / Varsha (01 Nov 2017) -- Start
                    'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                    'objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(Format(CDec(decDueBalanceAmt * mdecPaidExRate), GUI.fmtCurrency)) _
                    '                                                , CInt(DateDiff(DateInterval.Day, dtpEffectiveDate.Value.Date, dtEndDate.Date.AddDays(1))) _
                    '                                                , txtLoanInterest.Decimal _
                    '                                                , eLnCalcType _
                    '                                                , eIntRateCalcTypeID _
                    '                                                , CInt(nudDuration.Value) _
                    '                                                , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                    '                                                , decOrigInstAmt _
                    '                                                , mdecInstrAmount _
                    '                                                , mdecInstallmentAmount _
                    '                                                , mdecTotIntrstAmount _
                    '                                                , mdecTotInstallmentAmount _
                    '                                                )

                    objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(Format(CDec(decDueBalanceAmt * mdecPaidExRate), GUI.fmtCurrency)) _
                                                                    , CInt(DateDiff(DateInterval.Day, dtpEffectiveDate.Value.Date, dtEndDate.Date.AddDays(1))) _
                                                                    , txtLoanInterest.Decimal _
                                                                    , eLnCalcType _
                                                                    , eIntRateCalcTypeID _
                                                                    , CInt(DateDiff(DateInterval.Month, dtFPeriodStart.Date, dtEndDate.Date.AddDays(1))) _
                                                                    , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                                    , decOrigInstAmt _
                                                                    , mdecInstrAmount _
                                                                    , mdecInstallmentAmount _
                                                                    , mdecTotIntrstAmount _
                                                                    , mdecTotInstallmentAmount _
                                                                    )                    
                    'Sohail / Varsha (01 Nov 2017) -- End
                    'Nilay (15-Dec-2015) -- End
                    'Sohail (15 Dec 2015) -- End

                    mdecNetAmount = CDec(Format(CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate), GUI.fmtCurrency)) + mdecInstrAmount

                    RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                    txtInstallmentAmt.Tag = mdecInstallmentAmount
                    txtInstallmentAmt.Decimal = CDec(Format(mdecInstallmentAmount, GUI.fmtCurrency))
                    AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    txtPrincipalAmt.Text = Format(mdecInstallmentAmount - mdecInstrAmount, GUI.fmtCurrency)
                    txtPrincipalAmt.Tag = mdecInstallmentAmount - mdecInstrAmount
                    txtInterestAmt.Text = Format(mdecInstrAmount, GUI.fmtCurrency)
                    txtInterestAmt.Tag = mdecInstrAmount
                    'Sohail (15 Dec 2015) -- End

                Else
                    'Nilay (15-Dec-2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.

                    'Dim intNoOfEMI As Integer = 1
                    'If blnIsDurationChange = False Then
                    '    If txtInstallmentAmt.Decimal > 0 Then
                    '        intNoOfEMI = CInt(IIf(txtInstallmentAmt.Decimal = 0, 0, CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) / txtInstallmentAmt.Decimal))
                    '    Else
                    '        intNoOfEMI = 1
                    '    End If

                    '    'Sohail (15 Dec 2015) -- Start
                    '    'Error : Devide by Zero if txtInstallmentAmt.Decimal = 0 [Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.]
                    '    'If CInt(CDec(IIf(txtInstallmentAmt.Decimal = 0, 0, CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) / txtInstallmentAmt.Decimal))) > intNoOfEMI Then
                    '    If txtInstallmentAmt.Decimal <> 0 AndAlso CInt(CDec(IIf(txtInstallmentAmt.Decimal = 0, 0, CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) / txtInstallmentAmt.Decimal))) > intNoOfEMI Then
                    '        'Sohail (15 Dec 2015) -- End
                    '        intNoOfEMI += 1
                    '    End If

                    '    'Sohail (15 Dec 2015) -- Start
                    '    'Error : Value cannot be less than Minimum value. [Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.]
                    '    If intNoOfEMI <= 0 Then
                    '        intNoOfEMI = 1
                    '    End If
                    '    'Sohail (15 Dec 2015) -- End

                    '    RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    '    nudDuration.Value = intNoOfEMI
                    '    'Sohail (15 Dec 2015) -- Start
                    '    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    '    If mintParameterId = enParameterMode.LN_EMI Then
                    '        objlblEndDate.Text = Language.getMessage(mstrModuleName, 7, "Loan Stop Date :") & " " & DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1)
                    '    Else
                    '        objlblEndDate.Text = ""
                    '    End If
                    '    'Sohail (15 Dec 2015) -- End
                    '    AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    'Else
                    '    Dim decInstallmentAmt As Decimal
                    '    If intNoOfEMI > 0 Then
                    '        decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) / nudDuration.Value))
                    '    Else
                    '        decInstallmentAmt = 0
                    '    End If

                    '    RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                    '    txtInstallmentAmt.Tag = decInstallmentAmt
                    '    txtInstallmentAmt.Decimal = CDec(Format(decInstallmentAmt, GUI.fmtCurrency))
                    '    AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged

                    '    'Sohail (15 Dec 2015) -- Start
                    '    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    '    txtPrincipalAmt.Text = Format(decInstallmentAmt, GUI.fmtCurrency)
                    '    txtPrincipalAmt.Tag = decInstallmentAmt
                    '    txtInterestAmt.Text = Format(0, GUI.fmtCurrency)
                    '    txtInterestAmt.Tag = 0
                    '    'Sohail (15 Dec 2015) -- End
                    'End If

                    If blnIsDurationChange = False Then
                        'RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                        'txtInstallmentAmt.Tag = CDec(dsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"))
                        'txtInstallmentAmt.Decimal = CDec(Format(dsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"), GUI.fmtCurrency))
                        'AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                        txtPrincipalAmt.Text = txtInstallmentAmt.Text
                        txtPrincipalAmt.Tag = txtInstallmentAmt.Tag
                        'txtInterestAmt.Text = Format(0, GUI.fmtCurrency)
                        'txtInterestAmt.Tag = 0
                        Else
                        RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                        txtInstallmentAmt.Tag = CDec(dsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"))
                        txtInstallmentAmt.Decimal = CDec(Format(dsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"), GUI.fmtCurrency))
                        AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                        txtPrincipalAmt.Text = Format(CDec(dsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency")), GUI.fmtCurrency)
                        txtPrincipalAmt.Tag = CDec(dsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"))
                        txtInterestAmt.Text = Format(0, GUI.fmtCurrency)
                        txtInterestAmt.Tag = 0
                    End If
                    'Nilay (15-Dec-2015) -- End
                End If
            End If
            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_Loan_Installment_Amount", mstrModuleName)
        Finally
            mblnIsFormLoad = True
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub ResetParameters()
        Try
            nudDuration.Value = mintNoOfInstl
            txtLoanInterest.Decimal = mdecInterestRate
            txtTopupAmt.Decimal = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetParameters", mstrModuleName)
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
#Region " Approved Parameter Methods "
    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    'Private Sub GetValue()
    Private Sub GetValueApprovedParameter()
        'Nilay (01-Apr-2016) -- End
        Try
            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    txtLoanInterest.Decimal = CDec(objlnInterest._Interest_Rate)
                    radInterestRate.Checked = True
                    If objlnInterest._Effectivedate <> Nothing Then
                        'Nilay (28-Aug-2015) -- Start
                        'dtpEffectiveDate.Value = objlnInterest._Effectivedate
                        dtpEffectiveDate.Value = CDate(objlnInterest._Effectivedate).Date
                        'Nilay (28-Aug-2015) -- End
                    End If
                Case enParameterMode.LN_EMI
                    nudDuration.Value = objlnEMI._Emi_Tenure
                    txtInstallmentAmt.Tag = objlnEMI._Emi_Amount
                    txtInstallmentAmt.Decimal = CDec(Format(objlnEMI._Emi_Amount, GUI.fmtCurrency))
                    radInstallment.Checked = True
                    If objlnEMI._Effectivedate <> Nothing Then
                        'Nilay (28-Aug-2015) -- Start
                        'dtpEffectiveDate.Value = objlnEMI._Effectivedate
                        dtpEffectiveDate.Value = CDate(objlnEMI._Effectivedate).Date
                        'Nilay (28-Aug-2015) -- End
                    End If
                Case enParameterMode.LN_TOPUP
                    txtTopupAmt.Tag = objlnTopUp._Topup_Amount
                    txtTopupAmt.Decimal = CDec(Format(objlnTopUp._Topup_Amount, GUI.fmtCurrency))
                    radTopup.Checked = True
                    If objlnTopUp._Effectivedate <> Nothing Then
                        'Nilay (28-Aug-2015) -- Start
                        'dtpEffectiveDate.Value = objlnTopUp._Effectivedate
                        dtpEffectiveDate.Value = CDate(objlnTopUp._Effectivedate).Date
                        'Nilay (28-Aug-2015) -- End
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValueApprovedParameter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Pending Parameter Methods "

    Private Sub GetValuePendingParameter()
        Dim dsList As DataSet = Nothing
        Try
            Select Case mintParameterId
                Case enParameterMode.LN_RATE

                    radInterestRate.Checked = True
                    txtLoanInterest.Decimal = CDec(Format(objlnOpApprovaltran._InterestRate, GUI.fmtCurrency))
                    nudDuration.Value = mintNoOfInstl
                    'txtPrincipalAmt.Decimal = CDec(Format(objlnEMI._Principal_amount, GUI.fmtCurrency))
                    'txtInterestAmt.Decimal = CDec(Format(objlnEMI._Interest_amount, GUI.fmtCurrency))
                    'txtInstallmentAmt.Decimal = CDec(Format(objlnEMI._Emi_Amount, GUI.fmtCurrency))
                    'txtTopupAmt.Decimal = CDec(Format(objlnTopUp._Topup_Amount, GUI.fmtCurrency))

                    If objlnOpApprovaltran._EffectiveDate <> Nothing Then
                        'Nilay (28-Aug-2015) -- Start
                        'dtpEffectiveDate.Value = objlnInterest._Effectivedate
                        dtpEffectiveDate.Value = CDate(objlnOpApprovaltran._EffectiveDate).Date
                        'Nilay (28-Aug-2015) -- End
                    End If
                Case enParameterMode.LN_EMI

                    radInstallment.Checked = True
                    txtLoanInterest.Decimal = mdecInterestRate
                    nudDuration.Value = objlnOpApprovaltran._EmiTenure
                    txtInstallmentAmt.Tag = objlnOpApprovaltran._EmiAmount
                    txtInstallmentAmt.Decimal = CDec(Format(objlnOpApprovaltran._EmiAmount, GUI.fmtCurrency))
                    txtPrincipalAmt.Decimal = CDec(Format(objlnOpApprovaltran._PrincipalAmount, GUI.fmtCurrency))
                    txtInterestAmt.Decimal = CDec(Format(objlnOpApprovaltran._InterestAmount, GUI.fmtCurrency))
                    'txtTopupAmt.Decimal = CDec(Format(objlnTopUp._Topup_Amount, GUI.fmtCurrency))

                    If objlnOpApprovaltran._EffectiveDate <> Nothing Then
                        'Nilay (28-Aug-2015) -- Start
                        'dtpEffectiveDate.Value = objlnEMI._Effectivedate
                        dtpEffectiveDate.Value = CDate(objlnOpApprovaltran._EffectiveDate).Date
                        'Nilay (28-Aug-2015) -- End
                    End If
                Case enParameterMode.LN_TOPUP

                    radTopup.Checked = True
                    txtLoanInterest.Decimal = mdecInterestRate
                    nudDuration.Value = mintNoOfInstl
                    'txtPrincipalAmt.Decimal = CDec(Format(objlnEMI._Principal_amount, GUI.fmtCurrency))
                    'txtInterestAmt.Decimal = CDec(Format(objlnEMI._Interest_amount, GUI.fmtCurrency))
                    'txtInstallmentAmt.Decimal = CDec(Format(objlnEMI._Emi_Amount, GUI.fmtCurrency))
                    txtTopupAmt.Tag = objlnOpApprovaltran._TopupAmount
                    txtTopupAmt.Decimal = CDec(Format(objlnOpApprovaltran._TopupAmount, GUI.fmtCurrency))

                    If objlnOpApprovaltran._EffectiveDate <> Nothing Then
                        'Nilay (28-Aug-2015) -- Start
                        'dtpEffectiveDate.Value = objlnTopUp._Effectivedate
                        dtpEffectiveDate.Value = CDate(objlnOpApprovaltran._EffectiveDate).Date
                        'Nilay (28-Aug-2015) -- End
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValuePendingParameter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'Nilay (01-Apr-2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmLoanParameter_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objlnInterest = Nothing : objlnEMI = Nothing : objlnTopUp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanParameter_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmLoanParameter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objlnInterest = New clslnloan_interest_tran : objlnEMI = New clslnloan_emitenure_tran
        objlnTopUp = New clslnloan_topup_tran
        'Nilay (01-Apr-2016) -- Start
        'ENHANCEMENT - Approval Process in Loan Other Operations
        objlnOpApprovaltran = New clsloanotherop_approval_tran
        'Nilay (01-Apr-2016) -- End
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
  
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'If mActionMode = enAction.EDIT_ONE Then
            '    Select Case mintParameterId
            '        Case enParameterMode.LN_RATE
            '            objlnInterest._Lninteresttranunkid = mintTranUnkid
            '        Case enParameterMode.LN_EMI
            '            objlnEMI._Lnemitranunkid = mintTranUnkid
            '        Case enParameterMode.LN_TOPUP
            '            objlnTopUp._Lntopuptranunkid = mintTranUnkid
            '    End Select
            '    pnlradOpr.Enabled = False
            'End If
            'Call GetValue()
            'cboPeriod.Enabled = mblnDoNotAllowChangePeriod
            'objbtnPeriodSearch.Enabled = mblnDoNotAllowChangePeriod
            'dtpEffectiveDate.Enabled = mblnDoNotAllowChangePeriod
            cboPeriod.Enabled = True
            objbtnPeriodSearch.Enabled = True
            dtpEffectiveDate.Enabled = True
            'Nilay (01-Apr-2016) -- End
            Select Case mintParameterId
                Case enParameterMode.LN_EMI, enParameterMode.LN_TOPUP
                    dtpEffectiveDate.Enabled = False
            End Select
            mblnIsFormLoad = True

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If mActionMode <> enAction.EDIT_ONE Then
                Dim objLnAdv As New clsLoan_Advance
                Dim dsList As DataSet = objLnAdv.GetList(FinancialYear._Object._DatabaseName, _
                                                         User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                         Company._Object._Companyunkid, True, _
                                                         mdtLoanEffectiveDate, _
                                                         FinancialYear._Object._Database_End_Date, _
                                                         FinancialYear._Object._Database_End_Date, _
                                                         ConfigParameter._Object._UserAccessModeSetting, _
                                                         "List", "LN.loanadvancetranunkid = " & mintLoanAdvanceUnkid & "", _
                                                         mintEmployeeId)
                If dsList.Tables("List").Rows.Count > 0 Then
                    txtLoanInterest.Decimal = CDec(Format(CDec(dsList.Tables("List").Rows(0).Item("interest_rate")), GUI.fmtCurrency))
                    nudDuration.Value = CInt(dsList.Tables("List").Rows(0).Item("installments"))
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                mdecInterestRate = CDec(Format(CDec(dsList.Tables("List").Rows(0).Item("interest_rate")), GUI.fmtCurrency))
                mintNoOfInstl = CInt(dsList.Tables("List").Rows(0).Item("installments"))
                'Nilay (01-Apr-2016) -- End
                End If
            'End If
            'Nilay (15-Dec-2015) -- End


            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            If mActionMode = enAction.EDIT_ONE Then
                Select Case mintParameterId
                    Case enParameterMode.LN_RATE
                        objlnInterest._Lninteresttranunkid = mintTranUnkid
                        radInterestRate.Checked = True
                    Case enParameterMode.LN_EMI
                        objlnEMI._Lnemitranunkid = mintTranUnkid
                        radInstallment.Checked = True
                    Case enParameterMode.LN_TOPUP
                        objlnTopUp._Lntopuptranunkid = mintTranUnkid
                        radTopup.Checked = True
                End Select
            End If
            'Nilay (13-Sept-2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'Call GetValue()
            If mblnAllowChangeStatus = True Then
                pnlStatus.Visible = True
                Me.Size = CType(New Point(398, 368), Drawing.Size)

                objlnOpApprovaltran._LoanOtherOptranunkid = mintTranUnkid
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                mintAppPriority = objlnOpApprovaltran._Priority
                'Shani (21-Jul-2016) -- End
                pnlradOpr.Enabled = False
                Call GetValuePendingParameter()
            Else
                pnlStatus.Visible = False
                Me.Size = CType(New Point(394, 283), Drawing.Size)
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'pnlradOpr.Enabled = True
                If mActionMode = enAction.EDIT_ONE Then
                    dtpEffectiveDate.Enabled = False
                    pnlradOpr.Enabled = False
                    Call GetValueApprovedParameter()
                Else
                pnlradOpr.Enabled = True
                End If
                'Nilay (13-Sept-2016) -- End
            End If
            'Nilay (01-Apr-2016) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then Call txtPrincipalAmt_Validated(txtPrincipalAmt, New System.EventArgs)
            'Sohail (15 Dec 2015) -- End

            'Sohail (26 Mar 2018) -- Start
            'Le Grand Casino and Plam Beach Hotel and Casino Issue - Support Issue Id # 0002139 - 70.2 - Unable to change loan installment for no interest loan due to loan end date doesnt change when topups were added in previous periods.
            If CInt(nudDuration.Value) > 0 Then
                objlblEndDate.Text = Language.getMessage(mstrModuleName, 7, "Loan Stop Date :") & " " & DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1)
            Else
                objlblEndDate.Text = ""
            End If
            'Sohail (26 Mar 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanParameter_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnPeriodSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnPeriodSearch.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                'Nilay (15-Dec-2015) -- Start
                .CodeMember = "code"
                'Nilay (15-Dec-2015) -- End
                .DataSource = CType(cboPeriod.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPeriod.SelectedValue = .SelectedValue
                    cboPeriod.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnPeriodSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Valid_Data() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objlnOpApprovaltran._FormName = mstrModuleName
            objlnOpApprovaltran._LoginEmployeeunkid = 0
            objlnOpApprovaltran._ClientIP = getIP()
            objlnOpApprovaltran._HostName = getHostName()
            objlnOpApprovaltran._FromWeb = False
            objlnOpApprovaltran._AuditUserId = User._Object._Userunkid
objlnOpApprovaltran._CompanyUnkid = Company._Object._Companyunkid
            objlnOpApprovaltran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.WaitCursor
            'Shani (21-Jul-2016) -- End
            If mActionMode = enAction.ADD_ONE Then

                blnFlag = objlnOpApprovaltran.InsertApprovalTran(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                                 mdtStartDate, mdtEndDate, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                 mintEmployeeId, _
                                                                 mintLoanSchemeunkid, _
                                                                 ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                 ConfigParameter._Object._CurrentDateAndTime)
                'Nilay (08-Dec-2016) -- [mdtStartDate,mdtEndDate,ConfigParameter._Object._UserAccessModeSetting, True,ConfigParameter._Object._CurrentDateAndTime]

                If blnFlag = False AndAlso objlnOpApprovaltran._Message <> "" Then
                    eZeeMsgBox.Show(objlnOpApprovaltran._Message, enMsgBoxStyle.Information)
                            Exit Sub
                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                Else
                    'Nilay (10-Dec-2016) -- Start
                    'Issue #26: Setting to be included on configuration for Loan flow Approval process
                    If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then
                    Dim objProcessLoan As New clsProcess_pending_loan

                        With objProcessLoan
                            ._FormName = mstrModuleName
                            ._Loginemployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        'Nilay (27-Dec-2016) -- Start
                        objlnOpApprovaltran._LoanOtherOptranunkid = mintTranUnkid

                        If objlnOpApprovaltran._FinalStatus = 1 Then 'Pending

                    Select Case mintParameterId
                        Case enParameterMode.LN_RATE
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                                  mintLoanSchemeunkid, mintEmployeeId, _
                                    '                                                  CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                    '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                    '                                                  ConfigParameter._Object._ArutiSelfServiceURL, False, _
                                    '                                                  enLogin_Mode.DESKTOP, 0, 0)
                            objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                              mintLoanSchemeunkid, mintEmployeeId, _
                                                                              CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                                                      False, objlnOpApprovaltran._Identify_Guid, _
                                                                                    ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, False, _
                                                                              enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                        Case enParameterMode.LN_EMI
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                                  mintLoanSchemeunkid, mintEmployeeId, _
                                    '                                                  CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                    '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                    '                                                  ConfigParameter._Object._ArutiSelfServiceURL, False, _
                                    '                                                  enLogin_Mode.DESKTOP, 0, 0)
                            objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                              mintLoanSchemeunkid, mintEmployeeId, _
                                                                              CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                                                      False, objlnOpApprovaltran._Identify_Guid, _
                                                                                      ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, False, _
                                                                              enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                        Case enParameterMode.LN_TOPUP
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                                  mintLoanSchemeunkid, mintEmployeeId, _
                                    '                                                  CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                    '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                    '                                                  ConfigParameter._Object._ArutiSelfServiceURL, False, _
                                    '                                                  enLogin_Mode.DESKTOP, 0, 0)
                            objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                              mintLoanSchemeunkid, mintEmployeeId, _
                                                                              CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                                                      False, objlnOpApprovaltran._Identify_Guid, _
                                                                                      ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, False, _
                                                                              enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                            End Select

                        ElseIf objlnOpApprovaltran._FinalStatus = 2 Then 'Approved

                            Select Case mintParameterId
                                Case enParameterMode.LN_RATE
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.DESKTOP, 0, 0)
                                    objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                              enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_EMI
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.DESKTOP, 0, 0)
                                    objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                              enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_TOPUP
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.DESKTOP, 0, 0)
                                    objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                              enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                    End Select
                    End If
                        'Nilay (27-Dec-2016) -- End

                    End If
                    'Nilay (10-Dec-2016) -- End
                    'Shani (21-Jul-2016) -- End
                        End If

            ElseIf mActionMode = enAction.EDIT_ONE Then

                If mblnAllowChangeStatus = True Then

                    With objlnOpApprovaltran
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    blnFlag = objlnOpApprovaltran.UpdatePendingOpChangeStatus(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                                              mdtStartDate, mdtEndDate, _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                                              True, mintLoanSchemeunkid, _
                                                                              ConfigParameter._Object._IsLoanApprover_ForLoanScheme)
                    If blnFlag = False And objlnOpApprovaltran._Message <> "" Then
                        eZeeMsgBox.Show(objlnOpApprovaltran._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        'Shani (21-Jul-2016) -- Start
                        'Enhancement - Create New Loan Notification 
                    Else

                        'Nilay (10-Dec-2016) -- Start
                        'Issue #26: Setting to be included on configuration for Loan flow Approval process
                        If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then
                        Dim objProcessLoan As New clsProcess_pending_loan

                            With objProcessLoan
                                ._FormName = mstrModuleName
                                ._Loginemployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'If CInt(cboStatus.SelectedValue) = 2 Then
                        If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                            'Nilay (20-Sept-2016) -- End
                            objlnOpApprovaltran._LoanOtherOptranunkid = mintTranUnkid
                                If objlnOpApprovaltran._FinalStatus = 1 Then 'Pending
                                Select Case mintParameterId
                                    Case enParameterMode.LN_RATE
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                            '                                              mintLoanSchemeunkid, mintEmployeeId, mintAppPriority, _
                                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                            '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                            '                                              ConfigParameter._Object._ArutiSelfServiceURL, False, _
                                            '                                              enLogin_Mode.DESKTOP, 0, 0)
                                        objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                                      mintLoanSchemeunkid, mintEmployeeId, mintAppPriority, _
                                                                                  clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                                                                  False, objlnOpApprovaltran._Identify_Guid, _
                                                                                         ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, False, _
                                                                                      enLogin_Mode.DESKTOP, 0, 0)
                                            'Sohail (30 Nov 2017) -- End
                                    Case enParameterMode.LN_EMI
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                            '                                              mintLoanSchemeunkid, mintEmployeeId, mintAppPriority, _
                                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                            '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                            '                                              ConfigParameter._Object._ArutiSelfServiceURL, False, _
                                            '                                              enLogin_Mode.DESKTOP, 0, 0)
                                        objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                                      mintLoanSchemeunkid, mintEmployeeId, mintAppPriority, _
                                                                                  clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                                                                  False, objlnOpApprovaltran._Identify_Guid, _
                                                                                         ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, False, _
                                                                                      enLogin_Mode.DESKTOP, 0, 0)
                                            'Sohail (30 Nov 2017) -- End
                                    Case enParameterMode.LN_TOPUP
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                            '                                              mintLoanSchemeunkid, mintEmployeeId, mintAppPriority, _
                                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                            '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                            '                                              ConfigParameter._Object._ArutiSelfServiceURL, False, _
                                            '                                              enLogin_Mode.DESKTOP, 0, 0)
                                        objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                                      mintLoanSchemeunkid, mintEmployeeId, mintAppPriority, _
                                                                                  clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                                                                  False, objlnOpApprovaltran._Identify_Guid, _
                                                                                          ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, False, _
                                                                                      enLogin_Mode.DESKTOP, 0, 0)
                                            'Sohail (30 Nov 2017) -- End
                                End Select
                            ElseIf objlnOpApprovaltran._FinalStatus = 2 Then
                                Select Case mintParameterId
                                    Case enParameterMode.LN_RATE
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , enLogin_Mode.DESKTOP, 0, 0)
                                            objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , enLogin_Mode.DESKTOP, 0, 0)
                                            'Sohail (30 Nov 2017) -- End
                                    Case enParameterMode.LN_EMI
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , enLogin_Mode.DESKTOP, 0, 0)
                                            objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , enLogin_Mode.DESKTOP, 0, 0)
                                            'Sohail (30 Nov 2017) -- End
                                    Case enParameterMode.LN_TOPUP
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , enLogin_Mode.DESKTOP, 0, 0)
                                            objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , enLogin_Mode.DESKTOP, 0, 0)
                                            'Sohail (30 Nov 2017) -- End
                                End Select
                            End If
                            'Nilay (20-Sept-2016) -- Start
                            'Enhancement : Cancel feature for approved but not assigned loan application
                            'ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                        ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED Then
                            'Nilay (20-Sept-2016) -- End
                            Select Case mintParameterId
                                Case enParameterMode.LN_RATE
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                        objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                        'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_EMI
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                        objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                        'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_TOPUP
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                        objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceUnkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                        'Sohail (30 Nov 2017) -- End
                            End Select
                        End If
                        'Shani (21-Jul-2016) -- End
                    End If
                        'Nilay (10-Dec-2016) -- End
                    End If
                    'Nilay (13-Sept-2016) -- Start
                    'Enhancement : Enable Default Parameter Edit & other fixes
                ElseIf mblnAllowChangeStatus = False Then

                    Select Case mintParameterId
                        Case enParameterMode.LN_RATE
                            If IsSaveAllowed("lninteresttranunkid", mintTranUnkid) = False Then Exit Sub

                            Dim objLoan_Advance As New clsLoan_Advance
                            objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
                            objLoan_Advance._Interest_Rate = txtLoanInterest.Decimal
                            objLoan_Advance._Interest_Amount = txtInterestAmt.Decimal
                            objLoan_Advance._Emi_Amount = CDec(txtInstallmentAmt.Tag) / mdecPaidExRate
                            objLoan_Advance._Userunkid = User._Object._Userunkid
                            With objLoan_Advance
                                ._FormName = mstrModuleName
                                ._LoginEmployeeUnkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnFlag = objLoan_Advance.Update()

                            If blnFlag Then
                                objlnInterest._Interest_Rate = txtLoanInterest.Decimal
                                objlnInterest._Userunkid = User._Object._Userunkid


                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objlnInterest._FormName = mstrModuleName
                                objlnInterest._LoginEmployeeunkid = 0
                                objlnInterest._ClientIP = getIP()
                                objlnInterest._HostName = getHostName()
                                objlnInterest._FromWeb = False
                                objlnInterest._AuditUserId = User._Object._Userunkid
objlnInterest._CompanyUnkid = Company._Object._Companyunkid
                                objlnInterest._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                'S.SANDEEP [28-May-2018] -- END

                                blnFlag = objlnInterest.Update()

                                If blnFlag = False AndAlso objlnInterest._Message <> "" Then
                                    eZeeMsgBox.Show(objlnInterest._Message, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            Else
                                If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                                    eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                                    objLoan_Advance = Nothing
                                    Exit Sub
                                End If
                            End If
                            objLoan_Advance = Nothing

                        Case enParameterMode.LN_EMI
                            If IsSaveAllowed("lnemitranunkid", mintTranUnkid) = False Then Exit Sub

                            Dim objLoan_Advance As New clsLoan_Advance
                            objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
                            objLoan_Advance._Loan_Duration = CInt(nudDuration.Value)
                            objLoan_Advance._Emi_Tenure = CInt(nudDuration.Value)
                            objLoan_Advance._Emi_Amount = CDec(txtInstallmentAmt.Tag) / mdecPaidExRate
                            objLoan_Advance._Userunkid = User._Object._Userunkid
                            With objLoan_Advance
                                ._FormName = mstrModuleName
                                ._LoginEmployeeUnkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnFlag = objLoan_Advance.Update()

                            If blnFlag Then
                                objlnEMI._Emi_Tenure = CInt(nudDuration.Value)
                                objlnEMI._Emi_Amount = CDec(txtInstallmentAmt.Tag) / mdecPaidExRate
                                objlnEMI._Loan_Duration = CInt(nudDuration.Value)
                                objlnEMI._Userunkid = User._Object._Userunkid
                                With objlnEMI
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With
                                blnFlag = objlnEMI.Update()

                                If blnFlag = False AndAlso objlnEMI._Message <> "" Then
                                    eZeeMsgBox.Show(objlnEMI._Message, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            Else
                                If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                                    eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                                    objLoan_Advance = Nothing
                                    Exit Sub
                                End If
                    End If

                        Case enParameterMode.LN_TOPUP
                            If IsSaveAllowed("lntopuptranunkid", mintTranUnkid) = False Then Exit Sub

                            objlnTopUp._Topup_Amount = CDec(txtTopupAmt.Text) / mdecPaidExRate
                            With objlnTopUp
                                ._FormName = mstrModuleName
                                ._Loginemployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnFlag = objlnTopUp.Update()
                            If blnFlag = False AndAlso objlnTopUp._Message <> "" Then
                                eZeeMsgBox.Show(objlnTopUp._Message, enMsgBoxStyle.Information)
                                Exit Sub
                        End If
                    End Select
                    'Nilay (13-Sept-2016) -- End

            End If
            End If

            If blnFlag = True Then
                mblnCancel = False
                Call btnClose_Click(sender, e)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.Default
            'Shani (21-Jul-2016) -- End
        End Try
    End Sub
    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Dim blnFlag As Boolean = False
    '    Try
    '        If Valid_Data() = False Then Exit Sub
    '        Call SetValue()
    '        If mActionMode <> enAction.EDIT_ONE Then
    '            Select Case mintParameterId
    '                Case enParameterMode.LN_RATE
    '                    If IsSaveAllowed("lninteresttranunkid", mintTranUnkid) = False Then Exit Sub

    '                    'S.SANDEEP [04 JUN 2015] -- START
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'blnFlag = objlnInterest.Insert(True)

    '                    'Nilay (25-Mar-2016) -- Start
    '                    'blnFlag = objlnInterest.Insert(True, FinancialYear._Object._YearUnkid)
    '                    blnFlag = objlnInterest.Insert(FinancialYear._Object._DatabaseName, _
    '                                                   User._Object._Userunkid, _
    '                                                   FinancialYear._Object._YearUnkid, _
    '                                                   Company._Object._Companyunkid, _
    '                                                   mdtStartDate, _
    '                                                   mdtEndDate, _
    '                                                   ConfigParameter._Object._UserAccessModeSetting, _
    '                                                   True, True)
    '                    'Nilay (25-Mar-2016) -- End

    '                    'S.SANDEEP [04 JUN 2015] -- END

    '                    If blnFlag = False AndAlso objlnInterest._Message <> "" Then
    '                        eZeeMsgBox.Show(objlnInterest._Message, enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                Case enParameterMode.LN_EMI
    '                    If IsSaveAllowed("lnemitranunkid", mintTranUnkid) = False Then Exit Sub

    '                    'S.SANDEEP [04 JUN 2015] -- START
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'blnFlag = objlnEMI.Insert(True)

    '                    'Nilay (25-Mar-2016) -- Start
    '                    'blnFlag = objlnEMI.Insert(True, FinancialYear._Object._YearUnkid)
    '                    blnFlag = objlnEMI.Insert(FinancialYear._Object._DatabaseName, _
    '                                              User._Object._Userunkid, _
    '                                              FinancialYear._Object._YearUnkid, _
    '                                              Company._Object._Companyunkid, _
    '                                              mdtStartDate, _
    '                                              mdtEndDate, _
    '                                              ConfigParameter._Object._UserAccessModeSetting, _
    '                                              True, True)
    '                    'Nilay (25-Mar-2016) -- End


    '                    'S.SANDEEP [04 JUN 2015] -- END

    '                    If blnFlag = False AndAlso objlnEMI._Message <> "" Then
    '                        eZeeMsgBox.Show(objlnEMI._Message, enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                Case enParameterMode.LN_TOPUP
    '                    If IsSaveAllowed("lntopuptranunkid", mintTranUnkid) = False Then Exit Sub

    '                    'S.SANDEEP [04 JUN 2015] -- START
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'blnFlag = objlnTopUp.Insert(True)

    '                    'Nilay (25-Mar-2016) -- Start
    '                    'blnFlag = objlnTopUp.Insert(True, FinancialYear._Object._YearUnkid)
    '                    blnFlag = objlnTopUp.Insert(FinancialYear._Object._DatabaseName, _
    '                                                User._Object._Userunkid, _
    '                                                FinancialYear._Object._YearUnkid, _
    '                                                Company._Object._Companyunkid, _
    '                                                mdtStartDate, _
    '                                                mdtEndDate, _
    '                                                ConfigParameter._Object._UserAccessModeSetting, _
    '                                                True, True)
    '                    'Nilay (25-Mar-2016) -- End

    '                    'S.SANDEEP [04 JUN 2015] -- END

    '                    If blnFlag = False AndAlso objlnTopUp._Message <> "" Then
    '                        eZeeMsgBox.Show(objlnTopUp._Message, enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '            End Select
    '        Else
    '            Select Case mintParameterId
    '                Case enParameterMode.LN_RATE
    '                    If IsSaveAllowed("lninteresttranunkid", mintTranUnkid) = False Then Exit Sub

    '                    Dim objLoan_Advance As New clsLoan_Advance
    '                    objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
    '                    objLoan_Advance._Interest_Rate = txtLoanInterest.Decimal
    '                    objLoan_Advance._Userunkid = User._Object._Userunkid
    '                    blnFlag = objLoan_Advance.Update(User._Object._Userunkid)

    '                    If blnFlag Then
    '                        blnFlag = objlnInterest.Update()
    '                        If blnFlag = False AndAlso objlnInterest._Message <> "" Then
    '                            eZeeMsgBox.Show(objlnInterest._Message, enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If
    '                    Else
    '                        If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
    '                            eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
    '                            objLoan_Advance = Nothing
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    objLoan_Advance = Nothing

    '                Case enParameterMode.LN_EMI
    '                    If IsSaveAllowed("lnemitranunkid", mintTranUnkid) = False Then Exit Sub

    '                    Dim objLoan_Advance As New clsLoan_Advance
    '                    objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
    '                    objLoan_Advance._Loan_Duration = CInt(nudDuration.Value)
    '                    objLoan_Advance._Emi_Tenure = CInt(nudDuration.Value)
    '                    objLoan_Advance._Emi_Amount = CDec(txtInstallmentAmt.Tag) / mdecPaidExRate
    '                    objLoan_Advance._Userunkid = User._Object._Userunkid
    '                    blnFlag = objLoan_Advance.Update(User._Object._Userunkid)

    '                    If blnFlag Then
    '                        blnFlag = objlnEMI.Update()
    '                        If blnFlag = False AndAlso objlnEMI._Message <> "" Then
    '                            eZeeMsgBox.Show(objlnEMI._Message, enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If
    '                    Else
    '                        If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
    '                            eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
    '                            objLoan_Advance = Nothing
    '                            Exit Sub
    '                        End If
    '                    End If

    '                Case enParameterMode.LN_TOPUP
    '                    If IsSaveAllowed("lntopuptranunkid", mintTranUnkid) = False Then Exit Sub
    '                    blnFlag = objlnTopUp.Update()
    '                    If blnFlag = False AndAlso objlnTopUp._Message <> "" Then
    '                        eZeeMsgBox.Show(objlnTopUp._Message, enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '            End Select
    '        End If

    '        If blnFlag = True Then
    '            mblnCancel = False
    '            Call btnClose_Click(sender, e)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (01-Apr-2016) -- End
#End Region

#Region " Numeric Up Down Event "

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub nudDuration_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles nudDuration.Validating
        Try
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate.Date).AddDays(-1)
            'Sohail (26 Mar 2018) -- Start
            'Le Grand Casino and Plam Beach Hotel and Casino Issue - Support Issue Id # 0002139 - 70.2 - Unable to change loan installment for no interest loan due to loan end date doesnt change when topups were added in previous periods.
            'If dtEndDate < eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date Then
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If Not (mintCalcTypeId = CInt(enLoanCalcId.No_Interest) OrElse mintCalcTypeId = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI)) AndAlso dtEndDate < eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date Then
            If Not (mintCalcTypeId = CInt(enLoanCalcId.No_Interest) OrElse mintCalcTypeId = CInt(enLoanCalcId.No_Interest_With_Mapped_Head) OrElse mintCalcTypeId = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI)) AndAlso dtEndDate < eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date Then
                'Sohail (29 Apr 2019) -- End
                'Sohail (26 Mar 2018) -- End
                e.Cancel = True
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Loan stop date cannot be less than current open period end date") & " " & Format(eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date, "dd-MMM-yyyy"), enMsgBoxStyle.Information)
                Exit Try
            End If
        Catch ex As Exception

        End Try
    End Sub
    'Sohail (15 Dec 2015) -- End

    Private Sub nudDuration_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDuration.ValueChanged
        Try
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If mintParameterId = enParameterMode.LN_EMI Then
            '    objlblEndDate.Text = Language.getMessage(mstrModuleName, 7, "Loan Stop Date :") & " " & DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1)
            'Else
            '    objlblEndDate.Text = ""
            'End If
            If CInt(nudDuration.Value) > 0 Then
                objlblEndDate.Text = Language.getMessage(mstrModuleName, 7, "Loan Stop Date :") & " " & DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate).AddDays(-1)
            Else
                objlblEndDate.Text = ""
            End If
            'Nilay (15-Dec-2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            If mdtLoanEffectiveDate = Nothing Then
                Exit Try
            End If

            Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, mdtLoanEffectiveDate.Date).AddDays(-1)
            If dtEndDate < eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date Then

                Exit Try
            End If
            'Sohail (15 Dec 2015) -- End

            If mblnIsFormLoad = True Then
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'Call Calculate_Loan_Installment_Amount(mintCalcTypeId, True)
                Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId, True)
                'Sohail (15 Dec 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudDuration_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            If mblnIsFormLoad = True Then
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'Call Calculate_Loan_Installment_Amount(mintCalcTypeId)
                Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
                'Sohail (15 Dec 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub txtPrincipalAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrincipalAmt.TextChanged
        Try
            If mblnIsFormLoad = True And txtPrincipalAmt.ReadOnly = False Then
                'Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPrincipalAmt_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtPrincipalAmt_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrincipalAmt.Validated
        Try
            If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                If txtPrincipalAmt.Decimal = 0 Then
                    Dim objEmi As New clslnloan_emitenure_tran
                    Dim dsEmi As DataSet = objEmi.GetLastEMI("List", mintLoanAdvanceUnkid, dtpEffectiveDate.Value.Date)
                    If dsEmi IsNot Nothing AndAlso dsEmi.Tables("List").Rows.Count > 0 Then
                        txtPrincipalAmt.Text = Format(CDec(dsEmi.Tables("List").Rows(0).Item("principal_amount")), GUI.fmtCurrency)
                    End If
                End If
                Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPrincipalAmt_Validated", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Dec 2015) -- End

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub txtLoanInterest_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanInterest.Validated
        Try
            If radInterestRate.Checked = True Then
                Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLoanInterest_Validated", mstrModuleName)
        End Try
    End Sub
    'Nilay (15-Dec-2015) -- End

    'Private Sub txtLoanInterest_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanInterest.TextChanged
    '    Try
    '        If mblnIsFormLoad = True Then
    '            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

    '            Dim objLoan_Advance As New clsLoan_Advance
    '            objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
    '            RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
    '            nudDuration.Value = objLoan_Advance._Loan_Duration
    '            AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
    '            Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, dtpEffectiveDate.Value.Date)

    '            Dim mdecInstallmentAmount As Decimal = 0
    '            objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(Format(CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate), GUI.fmtCurrency)), _
    '                                                            CInt(DateDiff(DateInterval.Day, dtpEffectiveDate.Value.Date, dtEndDate.Date)), _
    '                                                            txtLoanInterest.Decimal, mintCalcTypeId, _
    '                                                            CInt(nudDuration.Value), mdecInstrAmount, mdecInstallmentAmount)

    '            mdecNetAmount = CDec(CDec(dsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) + mdecInstrAmount

    '            txtInstallmentAmt.Tag = mdecInstallmentAmount
    '            txtInstallmentAmt.Decimal = CDec(Format(mdecInstallmentAmount, GUI.fmtCurrency))

    '            objLoan_Advance = Nothing

    '            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtLoanInterest_TextChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub


#End Region

#Region " Radio Event "

    Private Sub radInterestRate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radInterestRate.CheckedChanged, _
        radInstallment.CheckedChanged, _
        radTopup.CheckedChanged
        Try
            'Nilay (15-Dec-2015) -- Start
            'Select Case CType(sender, RadioButton).Name.ToUpper
            '    Case radInterestRate.Name.ToUpper
            '        If CType(sender, RadioButton).Checked = True Then
            '            mintParameterId = enParameterMode.LN_RATE
            '            If pnlControls.Enabled = False Then pnlControls.Enabled = True
            '            Call SetDateValue()
            '        End If
            '    Case radInstallment.Name.ToUpper
            '        If CType(sender, RadioButton).Checked = True Then
            '            mintParameterId = enParameterMode.LN_EMI
            '            If pnlControls.Enabled = False Then pnlControls.Enabled = True
            '            Call SetDateValue()
            '        End If
            '    Case radTopup.Name.ToUpper
            '        If CType(sender, RadioButton).Checked = True Then
            '            mintParameterId = enParameterMode.LN_TOPUP
            '            If pnlControls.Enabled = False Then pnlControls.Enabled = True
            '            Call SetDateValue()
            '        End If
            'End Select
            'Call SetFormControls()

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            Call ResetParameters()
            'Nilay (01-Apr-2016) -- End

            Select Case CType(sender, RadioButton).Name.ToUpper
                Case radInterestRate.Name.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        mintParameterId = enParameterMode.LN_RATE
                    End If
                Case radInstallment.Name.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        mintParameterId = enParameterMode.LN_EMI
                    End If
                Case radTopup.Name.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        mintParameterId = enParameterMode.LN_TOPUP
                    End If
            End Select

                        If pnlControls.Enabled = False Then pnlControls.Enabled = True
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'Call SetDateValue()
            If mActionMode <> enAction.EDIT_ONE Then
                        Call SetDateValue()
            End If
            'Nilay (13-Sept-2016) -- End
            Call SetFormControls()
            'Call GetValue()
            'Nilay (15-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radInterestRate_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Date Control "
    'Nilay (15-Dec-2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub dtpEffectiveDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpEffectiveDate.ValueChanged
        Try
            Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpEffectiveDate_ValueChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (15-Dec-2015) -- End
#End Region

#Region " Combobox Event "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Call SetDateValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Old Code "
'Public Class frmLoanParameter

'#Region " Private Variable "

'    Private ReadOnly mstrModuleName As String = "frmLoanParameter"
'    Private mblnCancel As Boolean = True
'    Private mintTranUnkid As Integer = -1
'    Private mintParameterId As enParameterMode
'    Private mintLoanAdvanceUnkid As Integer = 0
'    Private mintLoanSchemeUnkid As Integer = 0

'    Private mdecTopupAmt As Decimal = 0
'    Private mdecInstallmentAmt As Decimal = 0
'    Private mintNoOfInstallment As Integer = 0
'    Private mdecInterestRate As Decimal = 0
'    Private mintPeriodUnkid As Integer = 0
'    Private mdtEffectiveDate As Date = Nothing
'    Private mintCalcTypeId As enLoanCalcId
'    Private mstrPeriodName As String = String.Empty
'    Private mActionMode As enAction
'    Private mintDuration As Integer = 1
'    Private mblnDoNotAllowChangePeriod As Boolean = False
'    Private mdtTran As DataTable

'    Private objlnInterest As clslnloan_interest_tran
'    Private objlnEMI As clslnloan_emitenure_tran
'    Private objlnTopUp As clslnloan_topup_tran

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByRef intUnkId As Integer, _
'                                  ByVal xParamId As enParameterMode, _
'                                  ByVal xLoanAdvUnkid As Integer, _
'                                  ByVal xMode As enAction, _
'                                  Optional ByVal blnAllowChangePeriod As Boolean = True) As Boolean
'        mintTranUnkid = intUnkId
'        mintParameterId = xParamId
'        mintLoanAdvanceUnkid = xLoanAdvUnkid
'        Dim objLnAdv As New clsLoan_Advance
'        objLnAdv._Loanadvancetranunkid = xLoanAdvUnkid
'        mintCalcTypeId = CType(objLnAdv._Calctype_Id, enLoanCalcId)
'        mintLoanSchemeUnkid = objLnAdv._Loanschemeunkid
'        objLnAdv = Nothing
'        mActionMode = xMode
'        mblnDoNotAllowChangePeriod = blnAllowChangePeriod
'        txtInstallmentAmt.ReadOnly = False

'        Call SetFormControls()

'        Me.ShowDialog()

'        intUnkId = mintTranUnkid
'        Return Not mblnCancel
'    End Function

'#End Region

'#Region " Private Methods "

'    Private Sub SetFormControls()
'        Try
'            Select Case mintParameterId
'                Case enParameterMode.LN_RATE
'                    txtEMIInstallments.Enabled = False
'                    txtInstallmentAmt.Enabled = False
'                    txtTopupAmt.Enabled = False
'                    txtLoanInterest.Enabled = True
'                    nudDuration.Enabled = False

'                Case enParameterMode.LN_EMI
'                    txtEMIInstallments.Enabled = True
'                    txtInstallmentAmt.Enabled = True
'                    txtTopupAmt.Enabled = False
'                    txtLoanInterest.Enabled = False
'                    nudDuration.Enabled = True

'                    If mintCalcTypeId = enLoanCalcId.No_Interest Then
'                        nudDuration.Enabled = False
'                    End If

'                    lblEMIAmount.Visible = True
'                    txtEMIInstallments.Visible = True
'                    txtInstallmentAmt.Visible = True
'                    txtTopupAmt.Visible = False
'                    lblTopupAmt.Visible = False
'                    dtpEffectiveDate.Enabled = False

'                Case enParameterMode.LN_TOPUP
'                    txtEMIInstallments.Enabled = False
'                    txtInstallmentAmt.Enabled = False
'                    txtTopupAmt.Enabled = True
'                    nudDuration.Enabled = False
'                    txtLoanInterest.Enabled = False

'                    lblEMIAmount.Visible = False
'                    txtInstallmentAmt.Visible = False
'                    txtTopupAmt.Visible = True
'                    lblTopupAmt.Visible = True
'                    dtpEffectiveDate.Enabled = False
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetFormControls", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub SetColor()
'        Try
'            txtEMIInstallments.BackColor = GUI.ColorComp
'            txtInstallmentAmt.BackColor = GUI.ColorComp
'            txtTopupAmt.BackColor = GUI.ColorComp
'            txtLoanInterest.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim dsCombos As New DataSet
'        Dim objPrd As New clscommom_period_Tran
'        Dim objMaster As New clsMasterData
'        Dim mdtTable As DataTable = Nothing
'        Try
'            dsCombos = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)

'            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
'            If intFirstPeriodId > 0 Then
'                mdtTable = New DataView(dsCombos.Tables("List"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
'            Else
'                mdtTable = New DataView(dsCombos.Tables("List"), "1=2", "", DataViewRowState.CurrentRows).ToTable
'            End If
'            With cboPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = mdtTable
'                If .Items.Count > 0 Then .SelectedIndex = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            objPrd = Nothing : objMaster = Nothing
'        End Try
'    End Sub

'    Private Function Valid_Data() As Boolean
'        Try
'            If CInt(cboPeriod.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
'                cboPeriod.Focus()
'                Return False
'            End If

'            Select Case mintParameterId
'                Case enParameterMode.LN_RATE
'                    If txtLoanInterest.Decimal <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Interest Rate is mandatory information. Please set Interest Rate to continue."), enMsgBoxStyle.Information)
'                        txtLoanInterest.Focus()
'                        Return False
'                    End If
'                Case enParameterMode.LN_EMI
'                    If txtEMIInstallments.Decimal <= 0 Or txtInstallmentAmt.Decimal <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Please give atleast No Of Installment and Installment Amount to continue."), enMsgBoxStyle.Information)
'                        txtEMIInstallments.Focus()
'                        Return False
'                    End If
'                Case enParameterMode.LN_TOPUP
'                    If txtTopupAmt.Decimal <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Topup amount is mandatory information. Please set Totup amount to continue."), enMsgBoxStyle.Information)
'                        txtTopupAmt.Focus()
'                        Return False
'                    End If
'            End Select

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Valid_Data", mstrModuleName)
'        Finally
'        End Try
'        Return True
'    End Function

'    Private Function IsSaveAllowed(ByVal xColumnToMatch As String, _
'                                   ByVal xColumnUnkid As Integer) As Boolean
'        Try
'            Dim objLnAdv As New clsLoan_Advance
'            Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceUnkid)
'            objLnAdv = Nothing

'            If mActionMode <> enAction.EDIT_ONE Then
'                If ds.Tables("List").Rows.Count > 0 Then
'                    If ds.Tables("List").Rows(0).Item("end_date").ToString > eZeeDate.convertDate(dtpEffectiveDate.Value.Date) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Payment date should not less than last transactoin date") & " " & Format(eZeeDate.convertDate(ds.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & ".", enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'            Else
'                Dim strMsg As String = String.Empty
'                If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item(xColumnToMatch)) = xColumnUnkid) Then
'                    If ds.Tables("List").Rows.Count > 0 Then
'                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
'                            strMsg = Language.getMessage("clslnloan_interest_tran", 1, " Process Payroll.")
'                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
'                            strMsg = Language.getMessage("clslnloan_interest_tran", 2, " Loan Payment List.")
'                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
'                            strMsg = Language.getMessage("clslnloan_interest_tran", 3, " Receipt Payment List.")
'                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
'                            strMsg = Language.getMessage("clslnloan_interest_tran", 4, " Loan Operation (EMI).")
'                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
'                            strMsg = Language.getMessage("clslnloan_interest_tran", 5, " Loan Operation (Topup).")
'                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
'                            strMsg = Language.getMessage("clslnloan_interest_tran", 6, " Loan Operation (Rate).")
'                        End If
'                    End If
'                End If
'                If strMsg.Trim.Length > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot save this transaction. Please delete last transaction from the screen of") & strMsg, enMsgBoxStyle.Information)
'                    Return False
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsSaveAllowed", mstrModuleName)
'        Finally
'        End Try
'        Return True
'    End Function

'    Private Sub Fill_Value()
'        Try
'            Select Case mintParameterId
'                Case enParameterMode.LN_RATE
'                    objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
'                    mdtTran = New DataView(objlnInterest._DataTable.Copy, "lninteresttranunkid = '" & mintTranUnkid & "'", "", DataViewRowState.CurrentRows).ToTable
'                    txtLoanInterest.Decimal = CDec(mdtTran.Rows(0).Item("interest_rate"))
'                Case enParameterMode.LN_EMI

'                Case enParameterMode.LN_TOPUP
'            End Select
'            'txtTopupAmt.Decimal = mdecTopupAmt
'            'txtInstallmentAmt.Decimal = mdecInstallmentAmt
'            'txtEMIInstallments.Decimal = mintNoOfInstallment
'            'txtLoanInterest.Decimal = mdecInterestRate
'            'cboPeriod.SelectedValue = mintPeriodUnkid
'            'nudDuration.Value = mintDuration
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_Value", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Function Insert_LnAdv_Parameter_Value() As Boolean
'        Dim blnFlag As Boolean = False
'        Try
'            Select Case mintParameterId
'                Case enParameterMode.LN_RATE

'                    objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
'                    mdtTran = objlnInterest._DataTable : mdtTran.Rows.Clear()

'                    Dim dRow As DataRow = mdtTran.NewRow()
'                    dRow.Item("lninteresttranunkid") = -1
'                    dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
'                    dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                    dRow.Item("effectivedate") = dtpEffectiveDate.Value
'                    dRow.Item("interest_rate") = txtLoanInterest.Decimal
'                    dRow.Item("userunkid") = User._Object._Userunkid
'                    dRow.Item("isvoid") = False
'                    dRow.Item("voiduserunkid") = -1
'                    dRow.Item("voiddatetime") = DBNull.Value
'                    dRow.Item("voidreason") = ""
'                    dRow.Item("AUD") = "A"
'                    dRow.Item("GUID") = Guid.NewGuid.ToString()
'                    dRow.Item("dperiod") = cboPeriod.Text
'                    dRow.Item("ddate") = dtpEffectiveDate.Value.Date.ToShortDateString
'                    dRow.Item("pstatusid") = enStatusType.Open
'                    mdtTran.Rows.Add(dRow)

'                    blnFlag = objlnInterest.InsertUpdateDelete_InerestRate_Tran(, True, User._Object._Userunkid)

'                Case enParameterMode.LN_EMI

'                    objlnEMI._Loanadvancetranunkid = mintLoanAdvanceUnkid
'                    mdtTran = objlnEMI._DataTable : mdtTran.Rows.Clear()

'                    Dim dRow As DataRow = mdtTran.NewRow()
'                    dRow.Item("lnemitranunkid") = -1
'                    dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
'                    dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                    dRow.Item("effectivedate") = dtpEffectiveDate.Value
'                    dRow.Item("emi_tenure") = txtEMIInstallments.Decimal
'                    dRow.Item("emi_amount") = txtInstallmentAmt.Decimal
'                    dRow.Item("loan_duration") = nudDuration.Value
'                    dRow.Item("userunkid") = User._Object._Userunkid
'                    dRow.Item("isvoid") = False
'                    dRow.Item("voiduserunkid") = -1
'                    dRow.Item("voiddatetime") = DBNull.Value
'                    dRow.Item("voidreason") = ""
'                    dRow.Item("AUD") = "A"
'                    dRow.Item("GUID") = Guid.NewGuid.ToString()
'                    dRow.Item("dperiod") = cboPeriod.Text
'                    dRow.Item("ddate") = dtpEffectiveDate.Value.Date.ToShortDateString
'                    dRow.Item("pstatusid") = enStatusType.Open
'                    mdtTran.Rows.Add(dRow)

'                    blnFlag = objlnEMI.InsertUpdateDelete_EMI_Tran()

'                Case enParameterMode.LN_TOPUP

'                    objlnTopUp._Loanadvancetranunkid = mintLoanAdvanceUnkid
'                    mdtTran = objlnTopUp._DataTable : mdtTran.Rows.Clear()

'                    Dim dRow As DataRow = mdtTran.NewRow()
'                    dRow.Item("lntopuptranunkid") = -1
'                    dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
'                    dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                    dRow.Item("effectivedate") = dtpEffectiveDate.Value
'                    dRow.Item("topup_amount") = txtTopupAmt.Decimal
'                    dRow.Item("interest_amount") = 0
'                    dRow.Item("userunkid") = User._Object._Userunkid
'                    dRow.Item("isvoid") = False
'                    dRow.Item("voiduserunkid") = -1
'                    dRow.Item("voiddatetime") = DBNull.Value
'                    dRow.Item("voidreason") = ""
'                    dRow.Item("AUD") = "A"
'                    dRow.Item("GUID") = Guid.NewGuid.ToString()
'                    dRow.Item("dperiod") = cboPeriod.Text
'                    dRow.Item("ddate") = dtpEffectiveDate.Value.Date.ToShortDateString
'                    dRow.Item("pstatusid") = enStatusType.Open
'                    mdtTran.Rows.Add(dRow)

'                    blnFlag = objlnTopUp.InsertUpdateDelete_Topup_Tran()

'            End Select

'            Return blnFlag

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Insert_LnAdv_Parameter_Value", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function Update_LnAdv_Parameter_Value() As Boolean
'        Dim blnFlag As Boolean = False
'        Try
'            Select Case mintParameterId
'                Case enParameterMode.LN_RATE
'                    objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
'                    mdtTran = New DataView(objlnInterest._DataTable.Copy, "lninteresttranunkid = '" & mintTranUnkid & "'", "", DataViewRowState.CurrentRows).ToTable

'                    mdtTran.Rows(0).Item("lninteresttranunkid") = mintTranUnkid
'                    mdtTran.Rows(0).Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
'                    mdtTran.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                    mdtTran.Rows(0).Item("effectivedate") = dtpEffectiveDate.Value
'                    mdtTran.Rows(0).Item("interest_rate") = txtLoanInterest.Decimal
'                    mdtTran.Rows(0).Item("userunkid") = User._Object._Userunkid
'                    mdtTran.Rows(0).Item("isvoid") = False
'                    mdtTran.Rows(0).Item("voiduserunkid") = -1
'                    mdtTran.Rows(0).Item("voiddatetime") = DBNull.Value
'                    mdtTran.Rows(0).Item("voidreason") = ""
'                    mdtTran.Rows(0).Item("AUD") = "U"
'                    mdtTran.Rows(0).Item("GUID") = Guid.NewGuid.ToString()
'                    mdtTran.Rows(0).Item("dperiod") = cboPeriod.Text
'                    mdtTran.Rows(0).Item("ddate") = dtpEffectiveDate.Value.Date.ToShortDateString
'                    mdtTran.Rows(0).Item("pstatusid") = enStatusType.Open

'                    mdtTran.AcceptChanges()

'                    objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
'                    objlnInterest._DataTable = mdtTran

'                    blnFlag = objlnInterest.InsertUpdateDelete_InerestRate_Tran(, True, User._Object._Userunkid)

'                Case enParameterMode.LN_EMI

'                Case enParameterMode.LN_TOPUP

'            End Select

'            Return blnFlag

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Update_LnAdv_Parameter_Value", mstrModuleName)
'        Finally
'        End Try
'    End Function

'#End Region

'#Region " Form's Events "

'    Private Sub frmLoanParameter_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objlnInterest = Nothing
'        objlnEMI = Nothing
'        objlnTopUp = Nothing
'    End Sub

'    Private Sub frmLoanParameter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objlnInterest = New clslnloan_interest_tran
'        objlnEMI = New clslnloan_emitenure_tran
'        objlnTopUp = New clslnloan_topup_tran

'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            Call FillCombo()
'            If mActionMode = enAction.EDIT_ONE Then
'                Call Fill_Value()
'            End If
'            cboPeriod.Enabled = mblnDoNotAllowChangePeriod
'            objbtnPeriodSearch.Enabled = mblnDoNotAllowChangePeriod
'            dtpEffectiveDate.Enabled = mblnDoNotAllowChangePeriod
'            Select Case mintParameterId
'                Case enParameterMode.LN_EMI, enParameterMode.LN_TOPUP
'                    dtpEffectiveDate.Enabled = False
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmLoanParameter_Load", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Combobox Event "

'    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
'        Try
'            If CInt(cboPeriod.SelectedValue) > 0 Then
'                mintPeriodUnkid = CInt(cboPeriod.SelectedValue)
'                dtpEffectiveDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
'                dtpEffectiveDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date

'                Dim dtEndDate As Date = Nothing

'                If mintParameterId = enParameterMode.LN_RATE Then
'                    Dim dsBal As New DataSet
'                    Dim objLoanAdvance As New clsLoan_Advance
'                    dsBal = objLoanAdvance.GetLastLoanBalance("List", mintLoanAdvanceUnkid)
'                    objLoanAdvance = Nothing
'                    If dsBal.Tables(0).Rows.Count > 0 Then
'                        dtEndDate = eZeeDate.convertDate(dsBal.Tables(0).Rows(0).Item("end_date").ToString).Date.AddDays(1)
'                    End If
'                End If

'                Dim objPeriod As New clscommom_period_Tran
'                objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
'                If dtEndDate <> Nothing Then
'                    dtpEffectiveDate.MinDate = dtEndDate
'                    dtpEffectiveDate.Value = dtEndDate
'                Else
'                    dtpEffectiveDate.MinDate = objPeriod._Start_Date
'                    dtpEffectiveDate.Value = objPeriod._Start_Date
'                End If
'                dtpEffectiveDate.MaxDate = objPeriod._End_Date
'                objPeriod = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Button's Event(s) "

'    Private Sub objbtnPeriodSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnPeriodSearch.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            With frm
'                .ValueMember = cboPeriod.ValueMember
'                .DisplayMember = cboPeriod.DisplayMember
'                .DataSource = CType(cboPeriod.DataSource, DataTable)
'                If .DisplayDialog Then
'                    cboPeriod.SelectedValue = .SelectedValue
'                    cboPeriod.Focus()
'                End If
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnPeriodSearch_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If Valid_Data() = False Then Exit Sub
'            If mActionMode <> enAction.EDIT_ONE Then
'                Select Case mintParameterId
'                    Case enParameterMode.LN_RATE
'                        If IsSaveAllowed("lninteresttranunkid", mintTranUnkid) = False Then Exit Sub
'                        blnFlag = Insert_LnAdv_Parameter_Value()
'                    Case enParameterMode.LN_EMI
'                        If IsSaveAllowed("lnemitranunkid", mintTranUnkid) = False Then Exit Sub

'                    Case enParameterMode.LN_TOPUP
'                        If IsSaveAllowed("lntopuptranunkid", mintTranUnkid) = False Then Exit Sub

'                End Select
'            Else
'                Select Case mintParameterId
'                    Case enParameterMode.LN_RATE
'                        blnFlag = Update_LnAdv_Parameter_Value()
'                    Case enParameterMode.LN_EMI

'                    Case enParameterMode.LN_TOPUP

'                End Select
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region
#End Region
	
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.Name, Me.lblDuration.Text)
			Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
			Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.Name, Me.lblEMIAmount.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblTopupAmt.Text = Language._Object.getCaption(Me.lblTopupAmt.Name, Me.lblTopupAmt.Text)
			Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.elOperation.Text = Language._Object.getCaption(Me.elOperation.Name, Me.elOperation.Text)
			Me.radTopup.Text = Language._Object.getCaption(Me.radTopup.Name, Me.radTopup.Text)
			Me.radInstallment.Text = Language._Object.getCaption(Me.radInstallment.Name, Me.radInstallment.Text)
			Me.radInterestRate.Text = Language._Object.getCaption(Me.radInterestRate.Name, Me.radInterestRate.Text)
			Me.lblPrincipalAmt.Text = Language._Object.getCaption(Me.lblPrincipalAmt.Name, Me.lblPrincipalAmt.Text)
			Me.lblInterestAmt.Text = Language._Object.getCaption(Me.lblInterestAmt.Name, Me.lblInterestAmt.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
                        Language.setMessage("clslnloan_interest_tran", 3, " Process Payroll.")
                        Language.setMessage("clslnloan_interest_tran", 4, " Loan Payment List.")
                        Language.setMessage("clslnloan_interest_tran", 5, " Receipt Payment List.")
                        Language.setMessage("clslnloan_interest_tran", 6, " Installment Change.")
                        Language.setMessage("clslnloan_interest_tran", 7, " Topup Added.")
                        Language.setMessage("clslnloan_interest_tran", 8, " Rate Change.")
			Language.setMessage(mstrModuleName, 1, "Sorry, Please select atleast one operation type to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 3, "Sorry, Interest Rate is mandatory information. Please set Interest Rate to continue.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Installment amount is mandatory information. Please set Installment amount to continue.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Loan Stop Date should be greater than")
			Language.setMessage(mstrModuleName, 6, "Sorry, Topup amount is mandatory information. Please set Totup amount to continue.")
                        Language.setMessage(mstrModuleName, 7, "Loan Stop Date :")
                        Language.setMessage(mstrModuleName, 8, "Please select atleast one Status from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 9, "Sorry, You cannot save this transaction. Please delete last transaction from the screen of")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot add this Interest Rate Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period.")
			Language.setMessage(mstrModuleName, 11, "Sorry, you cannot add this Installment Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period.")
			Language.setMessage(mstrModuleName, 12, "Sorry, you cannot add this Topup Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period.")
			Language.setMessage(mstrModuleName, 13, "Sorry, Effective date should not less than last transactoin date")
			Language.setMessage(mstrModuleName, 14, "Sorry, Principal amount is mandatory information. Please set Principal amount to continue.")
			Language.setMessage(mstrModuleName, 15, "Loan stop date cannot be less than current open period end date")
                        Language.setMessage(mstrModuleName, 16, "Remark cannot be blank. Remark is compulsory information.")
                        Language.setMessage(mstrModuleName, 17, "Installment months cannot be greater than")
			Language.setMessage(mstrModuleName, 18, " for")
			Language.setMessage(mstrModuleName, 19, " Scheme.")
Language.setMessage(mstrModuleName, 20, "Do you want to void Payroll?")
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


