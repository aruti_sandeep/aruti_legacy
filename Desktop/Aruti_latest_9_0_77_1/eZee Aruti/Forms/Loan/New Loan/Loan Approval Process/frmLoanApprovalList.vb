﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math

#End Region

Public Class frmLoanApprovalList

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmLoanApprovalList"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrAdvanceFilter As String = ""
    Private mintLowerPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private objApprovaltran As clsloanapproval_process_Tran
    Dim objLoanApprover As clsLoanApprover_master
    Private mintApproverID As Integer = -1
    Private mintApproverEmpID As Integer = -1
    Private mstrEmployeeIDs As String = ""
    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private mstrSearchText As String = String.Empty
    'Nilay (27-Oct-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Public Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMasterData As New clsMasterData
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objPeriod As New clscommom_period_Tran
        Dim objLoanApplication As New clsProcess_pending_loan
        Dim objLoanApprover As New clsLoanApprover_master

        Try

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objLoanApprover.GetEmployeeFromUser(User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate.ToString, ConfigParameter._Object._IsIncludeInactiveEmp)
            dsCombos = objLoanApprover.GetEmployeeFromUser(FinancialYear._Object._DatabaseName, _
                                                           User._Object._Userunkid, _
                                                           FinancialYear._Object._YearUnkid, _
                                                           Company._Object._Companyunkid, _
                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                           True, _
                                                           ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                           "List")
            'Nilay (10-Oct-2015) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Nilay (27-Oct-2016) -- End

            Dim lstIDs As List(Of String) = (From p In dsCombos.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            dsCombos = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboLoanScheme)
            'Nilay (27-Oct-2016) -- End

            dsCombos = objLoanApplication.GetLoan_Status("Status", True, False)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Status")
                .SelectedValue = 1
            End With

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 2, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 3, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCombos = objMasterData.GetCondition(False, True)
            dsCombos = objMasterData.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsCombos.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            cboAmountCondition.ValueMember = "id"
            cboAmountCondition.DisplayMember = "Name"
            cboAmountCondition.DataSource = dtCondition
            cboAmountCondition.SelectedIndex = 0

            Dim objLevel As New clslnapproverlevel_master
            Dim dsList As DataSet = objLevel.GetLevelFromUserLogin(User._Object._Userunkid)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                txtApprover.Text = dsList.Tables(0).Rows(0)("ApproverName").ToString()
                txtApprover.Tag = CInt(dsList.Tables(0).Rows(0)("lnapproverunkid"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges in payroll module.
    Private Sub SetVisibility()
        Try
            btnChangeStatus.Enabled = User._Object.Privilege._AllowToChangeLoanStatus
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

   

    Private Sub FillList()
        Dim mstrSearch As String = ""
        Dim dsList As DataSet = Nothing
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in loan module.
            If User._Object.Privilege._AllowToViewLoanApprovalList = True Then

                'Varsha Rana (17-Oct-2017) -- End

            If txtApplicationNo.Text.Trim <> "" Then
                mstrSearch &= "AND lnloan_process_pending_loan.Application_No like '" & txtApplicationNo.Text.Trim & "%' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If dtpFromDate.Checked Then
                mstrSearch &= "AND lnloan_process_pending_loan.application_date >='" & eZeeDate.convertDate(dtpFromDate.Value) & "' "
            End If

            If dtpToDate.Checked Then
                mstrSearch &= "AND lnloan_process_pending_loan.application_date <='" & eZeeDate.convertDate(dtpToDate.Value) & "' "
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                mstrSearch &= "AND isloan = " & CInt(IIf(CInt(cboLoanAdvance.SelectedIndex) = 1, 1, 0)) & " "
            End If

            If txtLoanAmount.Text.Trim <> "" AndAlso txtLoanAmount.Decimal > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.loan_amount  " & cboAmountCondition.Text & " " & txtLoanAmount.Decimal & " "
            End If
            'Nilay (21-Oct-2015) -- AND lnloanapproval_process_tran.loan_amount

            If CInt(cboStatus.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            'Nilay (07-Feb-2016) -- Start
            'If chkMyApprovals.Checked Then
            '    mstrSearch &= "AND lnloan_approver_mapping.userunkid = " & User._Object._Userunkid & " "
            'Else
            '    RemoveHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
            '    chkMyApprovals.Checked = False
            '    AddHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
            '    If mstrEmployeeIDs.Trim.Length > 0 Then
            '        mstrSearch &= "AND lnloan_process_pending_loan.employeeunkid  in (" & mstrEmployeeIDs & ")" & " "
            '    End If
            'End If
            'mstrSearch &= " AND lnloanapproval_process_tran.employeeunkid in (" & mstrEmployeeIDs & ")" & " "
            'Nilay (07-Feb-2016) -- End

            mstrSearch &= "AND lnloanapproval_process_tran.visibleid <> -1 " & " "


            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    mstrSearch &= " AND " & mstrAdvanceFilter
            'End If
            'Nilay (01-Mar-2016) -- End

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Substring(3)
            End If

            'Nilay (15-Dec-2015) -- Start
            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            'Nilay (15-Dec-2015) -- End

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objApprovaltran.GetList(User._Object._Userunkid, mstrSearch)
            dsList = objApprovaltran.GetList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, _
                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             "List", mstrSearch, mstrAdvanceFilter)
            'Nilay (01-Mar-2016) -- [mstrAdvanceFilter]
            'Nilay (10-Oct-2015) -- End

            Dim mintProcesspendingloanunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""

            If dsList.Tables(0) IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each drRow As DataRow In dsList.Tables(0).Rows

                    If CInt(drRow("pendingloantranunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If mintProcesspendingloanunkid <> CInt(drRow("processpendingloanunkid")) Then
                        dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND processpendingloanunkid = " & CInt(drRow("processpendingloanunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintProcesspendingloanunkid = CInt(drRow("processpendingloanunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStaus = Language.getMessage(mstrModuleName, 8, "Approved By :-  ") & drRow("Approver").ToString()
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 1 Then

                                    If CInt(dr(i)("statusunkid")) = 2 Then
                                        mstrStaus = Language.getMessage(mstrModuleName, 8, "Approved By :-  ") & dr(i)("Approver").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                        mstrStaus = Language.getMessage(mstrModuleName, 9, "Rejected By :-  ") & dr(i)("Approver").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStaus = Language.getMessage(mstrModuleName, 9, "Rejected By :-  ") & drRow("Approver").ToString()
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If

                Next

            End If

            'Nilay (07-Feb-2016) -- Start
            Dim dtTable As New DataTable

            If chkMyApprovals.Checked Then
                dtTable = New DataView(dsList.Tables("List"), " MappedUserId =  " & User._Object._Userunkid & "  OR MappedUserId <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                If mstrEmployeeIDs.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), "employeeunkid  in (" & mstrEmployeeIDs & ") OR MappedUserId <= 0", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            'Nilay (07-Feb-2016) -- End


            'Pinkal (20-Sep-2017) -- Start
            'Bug : Issue-On Loan Approval list ,Grouping on application no. is not display

            Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "processpendingloanunkid", "employeeunkid")

            Dim dtRow = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("processpendingloanunkid") Equals drTemp.Field(Of Integer)("processpendingloanunkid") And _
                         drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable


            If dtRow.Count > 0 Then
                dtRow.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
            End If

            'Pinkal (20-Sep-2017) -- End


            dgLoanApproval.AutoGenerateColumns = False
            dgcolhApplicationNo.DataPropertyName = "application_no"

            dgcolhApplicationDate.DataPropertyName = "application_date"
            dgcolhApplicationDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern

            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhApprover.DataPropertyName = "ApproverName"
            dgcolhLoanScheme.DataPropertyName = "Scheme"
            'Nilay (28-Aug-2015) -- Start
            'Add Column Loan/Advance
            dgcolhloan_advance.DataPropertyName = "loan_advance"
            'Nilay (28-Aug-2015) -- End

            'Nilay (15-Dec-2015) -- Start
            'dgcolhLoanAmount.DataPropertyName = "loan_amount"
            'dgcolhLoanAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhLoanAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhLoanAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhLnAdvAmount.DataPropertyName = "loan_amount"
            dgcolhLnAdvAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhLnAdvAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhLnAdvAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            'Nilay (15-Dec-2015) -- End

            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            dgcolhApprovedAmount.DataPropertyName = "approved_amount"
            dgcolhApprovedAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhApprovedAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhApprovedAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            'Nilay (21-Oct-2015) -- End

            dgcolhStatus.DataPropertyName = "status"
            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            dgcolhRemarks.DataPropertyName = "remark"
            'Nilay (21-Oct-2015) -- End
            objdgcolhPriority.DataPropertyName = "priority"
            objdgcolhIsGrp.DataPropertyName = "Isgrp"
            objdgcolhEmployeeID.DataPropertyName = "employeeunkid"
            objdgcolhUserID.DataPropertyName = "MappedUserId"
            objdgcolhApproverunkid.DataPropertyName = "Approverunkid"
            objdgcolhApproverempunkid.DataPropertyName = "approverempunkid"
            objdgcolhStatusunkid.DataPropertyName = "statusunkid"
            objdgcolhLoanStatusID.DataPropertyName = "loan_statusunkid"
            objdgcolhprocesspendingloanunkid.DataPropertyName = "processpendingloanunkid"
            objdgcolhpendingloantranunkid.DataPropertyName = "pendingloantranunkid"
            'Nilay (07-Feb-2016) -- Start
            'dgLoanApproval.DataSource = dsList.Tables(0)
            dgLoanApproval.DataSource = dtTable
            'Nilay (07-Feb-2016) -- End
            End If 'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Nilay (15-Dec-2015) -- Start
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
            'Nilay (15-Dec-2015) -- End
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            'Dim dgvcsChild As New DataGridViewCellStyle
            'dgvcsChild.ForeColor = Color.Black
            'dgvcsChild.SelectionForeColor = Color.Black
            'dgvcsChild.SelectionBackColor = Color.White
            'dgvcsChild.BackColor = Color.White


            For i As Integer = 0 To dgLoanApproval.RowCount - 1
                If CBool(dgLoanApproval.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgLoanApproval.Rows(i).DefaultCellStyle = dgvcsHeader
                    'Else
                    '    dgLoanApproval.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 10, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

    'Pinkal (20-Sep-2017) -- Start
    'Bug : Issue-On Loan Approval list ,Grouping on application no. is not display

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("processpendingloanunkid = " & CInt(dr("processpendingloanunkid").ToString()) & " AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DeleteRow", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (20-Sep-2017) -- End


#End Region

#Region " Form's Events "

    Private Sub frmLoanApprovalList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            objApprovaltran = New clsloanapproval_process_Tran
            objLoanApprover = New clsLoanApprover_master
            Call FillCombo()
            Call SetVisibility()
            objLoanApprover._lnApproverunkid = CInt(txtApprover.Tag)
            RemoveHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
            chkMyApprovals.Checked = True
            AddHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApprovalList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsloanapproval_process_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsloanapproval_process_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objLoanSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchScheme.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboLoanScheme.ValueMember
                .DisplayMember = cboLoanScheme.DisplayMember
                .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objFrm.DisplayDialog Then
                cboLoanScheme.SelectedValue = objFrm.SelectedValue
                cboLoanScheme.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objLoanSchemeSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboLoanScheme.SelectedValue = 0
            'Nilay (16-Nov-2016) -- Start
            Call SetDefaultSearchText(cboEmployee)
            Call SetDefaultSearchText(cboLoanScheme)
            lblEmployee.Focus()
            'Nilay (16-Nov-2016) -- End
            txtApplicationNo.Text = ""
            cboLoanAdvance.SelectedValue = 0
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpFromDate.Checked = False
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Checked = False
            txtLoanAmount.Decimal = 0
            cboAmountCondition.SelectedIndex = 0
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'cboStatus.SelectedValue = 1
            cboStatus.SelectedValue = enLoanApplicationStatus.PENDING
            'Nilay (20-Sept-2016) -- End
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Try
            If dgLoanApproval.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Loan Application approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgLoanApproval.Select()
                Exit Sub
            End If

            If CBool(dgLoanApproval.SelectedRows(0).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Exit Sub
            End If

            If User._Object._Userunkid <> CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhUserID.Index).Value) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Loan detail. Reason: You are logged in into another user account."), enMsgBoxStyle.Information) '?1
                dgLoanApproval.Select()
                Exit Sub
            End If
            'Nilay (01-Apr-2016) -- Start
            If CDate(dgLoanApproval.SelectedRows(0).Cells(dgcolhApplicationDate.Index).Value).Date < ConfigParameter._Object._CurrentDateAndTime.Date Then

                If CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) = 2 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Loan detail. Reason: This Loan is already approved."), enMsgBoxStyle.Information) '?1
                    dgLoanApproval.Select()
                    Exit Sub
                ElseIf CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) = 3 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), enMsgBoxStyle.Information) '?1
                    dgLoanApproval.Select()
                    Exit Sub
                End If
            Else
                If CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhLoanStatusID.Index).Value) = 3 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), enMsgBoxStyle.Information) '?1
                    dgLoanApproval.Select()
                    Exit Sub
                End If
            End If
            'Nilay (01-Apr-2016) -- End
            Dim objapproverlevel As New clslnapproverlevel_master
            objapproverlevel._lnLevelunkid = objLoanApprover._lnLevelunkid

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objApprovaltran.GetApprovalTranList(CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value) _
            '                                                            , CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value) _
            '                                                            , "lnloanapproval_process_tran.approverempunkid <> " & CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhApproverempunkid.Index).Value))
            Dim dsList As DataSet = objApprovaltran.GetApprovalTranList(FinancialYear._Object._DatabaseName, _
                                                                        User._Object._Userunkid, _
                                                                        FinancialYear._Object._YearUnkid, _
                                                                        Company._Object._Companyunkid, _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        ConfigParameter._Object._UserAccessModeSetting, _
                                                                        True, _
                                                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                        "List", _
                                                                        CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value), _
                                                                        CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), _
                                                                        "lnloanapproval_process_tran.approverempunkid <> " & CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhApproverempunkid.Index).Value))
            'Nilay (10-Oct-2015) -- End

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If objapproverlevel._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                        Dim dList As DataTable = New DataView(dsList.Tables(0), "lnlevelunkid = " & CInt(dsList.Tables(0).Rows(i)("lnlevelunkid")) & " AND statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                        If dList.Rows.Count > 0 Then Continue For

                        If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Loan detail. Reason: This Loan is already approved."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                    ElseIf objapproverlevel._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                        If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Loan detail. Reason: This Loan is already approved."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                    End If

                Next

            End If

            Dim mintPendingloantranunkid As Integer = -1
            Dim objFrm As New frmLoanApproval

            If CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhpendingloantranunkid.Index).Value) > 0 Then
                mintPendingloantranunkid = CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhpendingloantranunkid.Index).Value)
                If objFrm.displayDialog(mintPendingloantranunkid, enAction.EDIT_ONE, CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhApproverunkid.Index).Value) _
                                             , CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value)) Then
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                    'FillList()
                    If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = False Then
                    FillList()
                    Else
                        dgLoanApproval.DataSource = Nothing
                    End If 'Hemant (30 Aug 2019) 
                End If
            Else
                mintPendingloantranunkid = objApprovaltran.GetLastApproverApprovalID(CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value), objapproverlevel._Priority)
                If objFrm.displayDialog(mintPendingloantranunkid, enAction.ADD_ONE, CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhApproverunkid.Index).Value) _
                                             , CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhprocesspendingloanunkid.Index).Value)) Then
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                    'FillList()
                    If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = False Then
                    FillList()
                    Else
                        dgLoanApproval.DataSource = Nothing
                    End If
                    'Hemant (30 Aug 2019) -- End
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnLoanAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If dgLoanApproval.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Loan Application approver from the list to perform further operation."), enMsgBoxStyle.Information)
                dgLoanApproval.Select()
                Exit Sub
            End If

            Dim objAssignLoanAdvance As New frmNewLoanAdvance_AddEdit
            objAssignLoanAdvance._PendingApprovalTranID = CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhpendingloantranunkid.Index).Value)
            objAssignLoanAdvance._Employeeid = CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value)
            objAssignLoanAdvance.displayDialog(-1, enAction.ADD_ONE, -1)
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLoanAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (03 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - View employee diary on loan approval screen. This should help the management on decision to approve/reject the loan application in 71.1.
    Private Sub btnViewDiary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDiary.Click
        Dim frm As New frmEmployeeDiary
        Try
            If dgLoanApproval.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Loan Application approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgLoanApproval.Select()
                Exit Sub
            End If
            frm.displayDialog(CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhEmployeeID.Index).Value))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnViewDiary_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (03 Apr 2018) -- End

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'frm._Hr_EmployeeTable_Alias = "Emp"
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            'Nilay (01-Mar-2016) -- End

            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            Call FillList() 'Nilay (01-Mar-2016)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkMyApprovals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            FillList()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMyApprovals_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Events"

    Private Sub dgLoanApproval_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgLoanApproval.DataBindingComplete
        Try
            Call SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLoanApproval_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgLoanApproval_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgLoanApproval.SelectionChanged
        Try
            If dgLoanApproval.SelectedRows.Count > 0 Then

                Dim objLoanMapping As New clsloan_approver_mapping
                objLoanMapping.GetData(CInt(dgLoanApproval.SelectedRows(0).Cells(objdgcolhApproverunkid.Index).Value), CInt(User._Object._Userunkid))

                If objLoanMapping._Approvertranunkid > 0 Then
                    objLoanApprover._lnApproverunkid = objLoanMapping._Approvertranunkid
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLoanApproval_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
#Region " ComboBox's Events "
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboEmployee)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_Leave", mstrModuleName)
        End Try
    End Sub

#End Region
    'Nilay (27-Oct-2016) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblApplicationNo.Text = Language._Object.getCaption(Me.lblApplicationNo.Name, Me.lblApplicationNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblLoanAmount.Text = Language._Object.getCaption(Me.lblLoanAmount.Name, Me.lblLoanAmount.Text)
            Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.Name, Me.chkMyApprovals.Text)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.dgcolhApplicationNo.HeaderText = Language._Object.getCaption(Me.dgcolhApplicationNo.Name, Me.dgcolhApplicationNo.HeaderText)
            Me.dgcolhApplicationDate.HeaderText = Language._Object.getCaption(Me.dgcolhApplicationDate.Name, Me.dgcolhApplicationDate.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhLoanScheme.HeaderText = Language._Object.getCaption(Me.dgcolhLoanScheme.Name, Me.dgcolhLoanScheme.HeaderText)
            Me.dgcolhloan_advance.HeaderText = Language._Object.getCaption(Me.dgcolhloan_advance.Name, Me.dgcolhloan_advance.HeaderText)
            Me.dgcolhLnAdvAmount.HeaderText = Language._Object.getCaption(Me.dgcolhLnAdvAmount.Name, Me.dgcolhLnAdvAmount.HeaderText)
            Me.dgcolhApprovedAmount.HeaderText = Language._Object.getCaption(Me.dgcolhApprovedAmount.Name, Me.dgcolhApprovedAmount.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhRemarks.HeaderText = Language._Object.getCaption(Me.dgcolhRemarks.Name, Me.dgcolhRemarks.HeaderText)
			Me.btnViewDiary.Text = Language._Object.getCaption(Me.btnViewDiary.Name, Me.btnViewDiary.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "Advance")
            Language.setMessage(mstrModuleName, 4, "Please select Loan Application approver from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 5, "You can't Edit this Loan detail. Reason: You are logged in into another user account.")
            Language.setMessage(mstrModuleName, 6, "You can't Edit this Loan detail. Reason: This Loan is already approved.")
            Language.setMessage(mstrModuleName, 7, "You can't Edit this Loan detail. Reason: This Loan is already rejected.")
            Language.setMessage(mstrModuleName, 8, "Approved By :-")
            Language.setMessage(mstrModuleName, 9, "Rejected By :-")
            Language.setMessage(mstrModuleName, 10, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class