﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanApprovalList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanApprovalList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.dgLoanApproval = New System.Windows.Forms.DataGridView
        Me.dgcolhApplicationNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApplicationDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhloan_advance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLnAdvAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprovedAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemarks = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUserID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApproverunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApproverempunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLoanStatusID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhprocesspendingloanunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhpendingloantranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnChangeStatus = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtApprover = New eZee.TextBox.AlphanumericTextBox
        Me.cboAmountCondition = New System.Windows.Forms.ComboBox
        Me.chkMyApprovals = New System.Windows.Forms.CheckBox
        Me.lblLoanAmount = New System.Windows.Forms.Label
        Me.cboLoanAdvance = New System.Windows.Forms.ComboBox
        Me.txtLoanAmount = New eZee.TextBox.NumericTextBox
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.objbtnSearchScheme = New eZee.Common.eZeeGradientButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.lblApprover = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblToDate = New System.Windows.Forms.Label
        Me.txtApplicationNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.lblApplicationNo = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.btnViewDiary = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        CType(Me.dgLoanApproval, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.dgLoanApproval)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(846, 482)
        Me.pnlMain.TabIndex = 0
        '
        'dgLoanApproval
        '
        Me.dgLoanApproval.AllowUserToAddRows = False
        Me.dgLoanApproval.AllowUserToDeleteRows = False
        Me.dgLoanApproval.AllowUserToResizeRows = False
        Me.dgLoanApproval.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgLoanApproval.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgLoanApproval.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhApplicationNo, Me.dgcolhApplicationDate, Me.dgcolhEmployee, Me.dgcolhApprover, Me.dgcolhLoanScheme, Me.dgcolhloan_advance, Me.dgcolhLnAdvAmount, Me.dgcolhApprovedAmount, Me.dgcolhStatus, Me.dgcolhRemarks, Me.objdgcolhPriority, Me.objdgcolhIsGrp, Me.objdgcolhEmployeeID, Me.objdgcolhUserID, Me.objdgcolhApproverunkid, Me.objdgcolhApproverempunkid, Me.objdgcolhStatusunkid, Me.objdgcolhLoanStatusID, Me.objdgcolhprocesspendingloanunkid, Me.objdgcolhpendingloantranunkid})
        Me.dgLoanApproval.Location = New System.Drawing.Point(12, 124)
        Me.dgLoanApproval.MultiSelect = False
        Me.dgLoanApproval.Name = "dgLoanApproval"
        Me.dgLoanApproval.RowHeadersVisible = False
        Me.dgLoanApproval.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgLoanApproval.Size = New System.Drawing.Size(822, 295)
        Me.dgLoanApproval.TabIndex = 19
        '
        'dgcolhApplicationNo
        '
        Me.dgcolhApplicationNo.Frozen = True
        Me.dgcolhApplicationNo.HeaderText = "Application No"
        Me.dgcolhApplicationNo.Name = "dgcolhApplicationNo"
        Me.dgcolhApplicationNo.ReadOnly = True
        Me.dgcolhApplicationNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhApplicationDate
        '
        Me.dgcolhApplicationDate.HeaderText = "Application Date"
        Me.dgcolhApplicationDate.Name = "dgcolhApplicationDate"
        Me.dgcolhApplicationDate.ReadOnly = True
        Me.dgcolhApplicationDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApplicationDate.Width = 125
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 250
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.ReadOnly = True
        Me.dgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprover.Width = 250
        '
        'dgcolhLoanScheme
        '
        Me.dgcolhLoanScheme.HeaderText = "Loan Scheme"
        Me.dgcolhLoanScheme.Name = "dgcolhLoanScheme"
        Me.dgcolhLoanScheme.ReadOnly = True
        Me.dgcolhLoanScheme.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLoanScheme.Width = 200
        '
        'dgcolhloan_advance
        '
        Me.dgcolhloan_advance.HeaderText = "Loan / Advance"
        Me.dgcolhloan_advance.Name = "dgcolhloan_advance"
        '
        'dgcolhLnAdvAmount
        '
        Me.dgcolhLnAdvAmount.HeaderText = "Applied Amount"
        Me.dgcolhLnAdvAmount.Name = "dgcolhLnAdvAmount"
        Me.dgcolhLnAdvAmount.ReadOnly = True
        Me.dgcolhLnAdvAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhApprovedAmount
        '
        Me.dgcolhApprovedAmount.HeaderText = "Approved Amount"
        Me.dgcolhApprovedAmount.Name = "dgcolhApprovedAmount"
        Me.dgcolhApprovedAmount.ReadOnly = True
        Me.dgcolhApprovedAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStatus.Width = 250
        '
        'dgcolhRemarks
        '
        Me.dgcolhRemarks.HeaderText = "Remarks"
        Me.dgcolhRemarks.Name = "dgcolhRemarks"
        Me.dgcolhRemarks.ReadOnly = True
        Me.dgcolhRemarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRemarks.Width = 150
        '
        'objdgcolhPriority
        '
        Me.objdgcolhPriority.HeaderText = "Priority"
        Me.objdgcolhPriority.Name = "objdgcolhPriority"
        Me.objdgcolhPriority.ReadOnly = True
        Me.objdgcolhPriority.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "IsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhEmployeeID
        '
        Me.objdgcolhEmployeeID.HeaderText = "EmployeeID"
        Me.objdgcolhEmployeeID.Name = "objdgcolhEmployeeID"
        Me.objdgcolhEmployeeID.ReadOnly = True
        Me.objdgcolhEmployeeID.Visible = False
        '
        'objdgcolhUserID
        '
        Me.objdgcolhUserID.HeaderText = "UserID"
        Me.objdgcolhUserID.Name = "objdgcolhUserID"
        Me.objdgcolhUserID.ReadOnly = True
        Me.objdgcolhUserID.Visible = False
        '
        'objdgcolhApproverunkid
        '
        Me.objdgcolhApproverunkid.HeaderText = "Approverunkid"
        Me.objdgcolhApproverunkid.Name = "objdgcolhApproverunkid"
        Me.objdgcolhApproverunkid.ReadOnly = True
        Me.objdgcolhApproverunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApproverunkid.Visible = False
        '
        'objdgcolhApproverempunkid
        '
        Me.objdgcolhApproverempunkid.HeaderText = "Approverempunkid"
        Me.objdgcolhApproverempunkid.Name = "objdgcolhApproverempunkid"
        Me.objdgcolhApproverempunkid.ReadOnly = True
        Me.objdgcolhApproverempunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApproverempunkid.Visible = False
        '
        'objdgcolhStatusunkid
        '
        Me.objdgcolhStatusunkid.HeaderText = "Statusunkid"
        Me.objdgcolhStatusunkid.Name = "objdgcolhStatusunkid"
        Me.objdgcolhStatusunkid.ReadOnly = True
        Me.objdgcolhStatusunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhStatusunkid.Visible = False
        '
        'objdgcolhLoanStatusID
        '
        Me.objdgcolhLoanStatusID.HeaderText = "LoanStatusID"
        Me.objdgcolhLoanStatusID.Name = "objdgcolhLoanStatusID"
        Me.objdgcolhLoanStatusID.ReadOnly = True
        Me.objdgcolhLoanStatusID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhLoanStatusID.Visible = False
        '
        'objdgcolhprocesspendingloanunkid
        '
        Me.objdgcolhprocesspendingloanunkid.HeaderText = "processpendingloanunkid"
        Me.objdgcolhprocesspendingloanunkid.Name = "objdgcolhprocesspendingloanunkid"
        Me.objdgcolhprocesspendingloanunkid.ReadOnly = True
        Me.objdgcolhprocesspendingloanunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhprocesspendingloanunkid.Visible = False
        '
        'objdgcolhpendingloantranunkid
        '
        Me.objdgcolhpendingloantranunkid.HeaderText = "pendingloantranunkid"
        Me.objdgcolhpendingloantranunkid.Name = "objdgcolhpendingloantranunkid"
        Me.objdgcolhpendingloantranunkid.ReadOnly = True
        Me.objdgcolhpendingloantranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhpendingloantranunkid.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnViewDiary)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnChangeStatus)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 427)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(846, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(741, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnChangeStatus
        '
        Me.btnChangeStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnChangeStatus.BackColor = System.Drawing.Color.White
        Me.btnChangeStatus.BackgroundImage = CType(resources.GetObject("btnChangeStatus.BackgroundImage"), System.Drawing.Image)
        Me.btnChangeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnChangeStatus.BorderColor = System.Drawing.Color.Empty
        Me.btnChangeStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnChangeStatus.FlatAppearance.BorderSize = 0
        Me.btnChangeStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeStatus.ForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnChangeStatus.GradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Location = New System.Drawing.Point(616, 13)
        Me.btnChangeStatus.Name = "btnChangeStatus"
        Me.btnChangeStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Size = New System.Drawing.Size(119, 30)
        Me.btnChangeStatus.TabIndex = 0
        Me.btnChangeStatus.Text = "Change &Status"
        Me.btnChangeStatus.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtApprover)
        Me.gbFilterCriteria.Controls.Add(Me.cboAmountCondition)
        Me.gbFilterCriteria.Controls.Add(Me.chkMyApprovals)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanAmount)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.txtLoanAmount)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchScheme)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprover)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.txtApplicationNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblApplicationNo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(822, 115)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApprover
        '
        Me.txtApprover.BackColor = System.Drawing.Color.White
        Me.txtApprover.Flags = 0
        Me.txtApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprover.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApprover.Location = New System.Drawing.Point(78, 87)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.ReadOnly = True
        Me.txtApprover.Size = New System.Drawing.Size(170, 21)
        Me.txtApprover.TabIndex = 3
        '
        'cboAmountCondition
        '
        Me.cboAmountCondition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAmountCondition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAmountCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAmountCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAmountCondition.FormattingEnabled = True
        Me.cboAmountCondition.Location = New System.Drawing.Point(626, 87)
        Me.cboAmountCondition.Name = "cboAmountCondition"
        Me.cboAmountCondition.Size = New System.Drawing.Size(42, 21)
        Me.cboAmountCondition.TabIndex = 10
        '
        'chkMyApprovals
        '
        Me.chkMyApprovals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMyApprovals.Location = New System.Drawing.Point(266, 89)
        Me.chkMyApprovals.Name = "chkMyApprovals"
        Me.chkMyApprovals.Size = New System.Drawing.Size(182, 17)
        Me.chkMyApprovals.TabIndex = 6
        Me.chkMyApprovals.Text = "My Approvals"
        Me.chkMyApprovals.UseVisualStyleBackColor = True
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAmount.Location = New System.Drawing.Point(455, 90)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(67, 15)
        Me.lblLoanAmount.TabIndex = 212
        Me.lblLoanAmount.Text = "Loan Amt"
        Me.lblLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanAdvance
        '
        Me.cboLoanAdvance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAdvance.FormattingEnabled = True
        Me.cboLoanAdvance.Location = New System.Drawing.Point(351, 60)
        Me.cboLoanAdvance.Name = "cboLoanAdvance"
        Me.cboLoanAdvance.Size = New System.Drawing.Size(97, 21)
        Me.cboLoanAdvance.TabIndex = 5
        '
        'txtLoanAmount
        '
        Me.txtLoanAmount.AllowNegative = True
        Me.txtLoanAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmount.DigitsInGroup = 0
        Me.txtLoanAmount.Flags = 0
        Me.txtLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmount.Location = New System.Drawing.Point(523, 87)
        Me.txtLoanAmount.MaxDecimalPlaces = 6
        Me.txtLoanAmount.MaxWholeDigits = 21
        Me.txtLoanAmount.Name = "txtLoanAmount"
        Me.txtLoanAmount.Prefix = ""
        Me.txtLoanAmount.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmount.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmount.Size = New System.Drawing.Size(97, 21)
        Me.txtLoanAmount.TabIndex = 9
        Me.txtLoanAmount.Text = "0"
        Me.txtLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(263, 63)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(82, 15)
        Me.lblLoanAdvance.TabIndex = 245
        Me.lblLoanAdvance.Text = "Loan/Advance"
        Me.lblLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(627, 36)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(63, 15)
        Me.lblStatus.TabIndex = 244
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(696, 33)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(120, 21)
        Me.cboStatus.TabIndex = 11
        '
        'objbtnSearchScheme
        '
        Me.objbtnSearchScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchScheme.BorderSelected = False
        Me.objbtnSearchScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchScheme.Location = New System.Drawing.Point(227, 60)
        Me.objbtnSearchScheme.Name = "objbtnSearchScheme"
        Me.objbtnSearchScheme.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchScheme.TabIndex = 241
        Me.objbtnSearchScheme.Visible = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(685, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 0
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(5, 90)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(67, 15)
        Me.lblApprover.TabIndex = 234
        Me.lblApprover.Text = "Approver"
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownWidth = 300
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(78, 60)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(170, 21)
        Me.cboLoanScheme.TabIndex = 2
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(5, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(70, 15)
        Me.lblLoanScheme.TabIndex = 231
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'dtpFromDate
        '
        Me.dtpFromDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(524, 33)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpFromDate.TabIndex = 7
        '
        'dtpToDate
        '
        Me.dtpToDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(524, 60)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpToDate.TabIndex = 8
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(455, 63)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(67, 15)
        Me.lblToDate.TabIndex = 223
        Me.lblToDate.Text = "To Date"
        '
        'txtApplicationNo
        '
        Me.txtApplicationNo.Flags = 0
        Me.txtApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicationNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicationNo.Location = New System.Drawing.Point(351, 33)
        Me.txtApplicationNo.Name = "txtApplicationNo"
        Me.txtApplicationNo.Size = New System.Drawing.Size(97, 21)
        Me.txtApplicationNo.TabIndex = 4
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(455, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(67, 15)
        Me.lblFromDate.TabIndex = 221
        Me.lblFromDate.Text = "From Date"
        '
        'lblApplicationNo
        '
        Me.lblApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationNo.Location = New System.Drawing.Point(263, 36)
        Me.lblApplicationNo.Name = "lblApplicationNo"
        Me.lblApplicationNo.Size = New System.Drawing.Size(82, 15)
        Me.lblApplicationNo.TabIndex = 217
        Me.lblApplicationNo.Text = "Application No"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(795, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(254, 24)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(6, 88)
        Me.objLine1.TabIndex = 88
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(772, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(227, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchEmployee.TabIndex = 86
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(78, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(170, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(5, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(67, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'btnViewDiary
        '
        Me.btnViewDiary.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewDiary.BackColor = System.Drawing.Color.White
        Me.btnViewDiary.BackgroundImage = CType(resources.GetObject("btnViewDiary.BackgroundImage"), System.Drawing.Image)
        Me.btnViewDiary.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewDiary.BorderColor = System.Drawing.Color.Empty
        Me.btnViewDiary.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewDiary.FlatAppearance.BorderSize = 0
        Me.btnViewDiary.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewDiary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewDiary.ForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewDiary.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDiary.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.Location = New System.Drawing.Point(20, 13)
        Me.btnViewDiary.Name = "btnViewDiary"
        Me.btnViewDiary.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDiary.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.Size = New System.Drawing.Size(90, 30)
        Me.btnViewDiary.TabIndex = 78
        Me.btnViewDiary.Text = "&View Diary"
        Me.btnViewDiary.UseVisualStyleBackColor = True
        '
        'frmLoanApprovalList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 482)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanApprovalList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan Approval List"
        Me.pnlMain.ResumeLayout(False)
        CType(Me.dgLoanApproval, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents txtApplicationNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApplicationNo As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnChangeStatus As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents dgLoanApproval As System.Windows.Forms.DataGridView
    Friend WithEvents cboLoanAdvance As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents txtLoanAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtApprover As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkMyApprovals As System.Windows.Forms.CheckBox
    Friend WithEvents cboAmountCondition As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApplicationNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApplicationDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanScheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhloan_advance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLnAdvAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprovedAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemarks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUserID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApproverunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApproverempunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLoanStatusID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhprocesspendingloanunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhpendingloantranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnViewDiary As eZee.Common.eZeeLightButton
End Class
