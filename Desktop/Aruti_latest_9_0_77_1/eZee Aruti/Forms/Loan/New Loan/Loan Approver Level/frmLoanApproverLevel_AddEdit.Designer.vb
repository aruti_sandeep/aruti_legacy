﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanApproverLevel_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanApproverLevel_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbApproverLevelInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPRemark = New System.Windows.Forms.Label
        Me.nudPriority = New System.Windows.Forms.NumericUpDown
        Me.lblPriority = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtLevelName = New eZee.TextBox.AlphanumericTextBox
        Me.lblLevelName = New System.Windows.Forms.Label
        Me.LblMinRange = New System.Windows.Forms.Label
        Me.nudMinimumRange = New System.Windows.Forms.NumericUpDown
        Me.nudMaximumRange = New System.Windows.Forms.NumericUpDown
        Me.LblMaxRange = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbApproverLevelInfo.SuspendLayout()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMinimumRange, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMaximumRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbApproverLevelInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(456, 157)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 101)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(456, 56)
        Me.objFooter.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(244, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(347, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbApproverLevelInfo
        '
        Me.gbApproverLevelInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApproverLevelInfo.Checked = False
        Me.gbApproverLevelInfo.CollapseAllExceptThis = False
        Me.gbApproverLevelInfo.CollapsedHoverImage = Nothing
        Me.gbApproverLevelInfo.CollapsedNormalImage = Nothing
        Me.gbApproverLevelInfo.CollapsedPressedImage = Nothing
        Me.gbApproverLevelInfo.CollapseOnLoad = False
        Me.gbApproverLevelInfo.Controls.Add(Me.LblMaxRange)
        Me.gbApproverLevelInfo.Controls.Add(Me.nudMaximumRange)
        Me.gbApproverLevelInfo.Controls.Add(Me.nudMinimumRange)
        Me.gbApproverLevelInfo.Controls.Add(Me.LblMinRange)
        Me.gbApproverLevelInfo.Controls.Add(Me.lblPRemark)
        Me.gbApproverLevelInfo.Controls.Add(Me.nudPriority)
        Me.gbApproverLevelInfo.Controls.Add(Me.lblPriority)
        Me.gbApproverLevelInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbApproverLevelInfo.Controls.Add(Me.txtLevelName)
        Me.gbApproverLevelInfo.Controls.Add(Me.lblLevelName)
        Me.gbApproverLevelInfo.ExpandedHoverImage = Nothing
        Me.gbApproverLevelInfo.ExpandedNormalImage = Nothing
        Me.gbApproverLevelInfo.ExpandedPressedImage = Nothing
        Me.gbApproverLevelInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproverLevelInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproverLevelInfo.HeaderHeight = 25
        Me.gbApproverLevelInfo.HeaderMessage = ""
        Me.gbApproverLevelInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApproverLevelInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproverLevelInfo.HeightOnCollapse = 0
        Me.gbApproverLevelInfo.LeftTextSpace = 0
        Me.gbApproverLevelInfo.Location = New System.Drawing.Point(8, 7)
        Me.gbApproverLevelInfo.Name = "gbApproverLevelInfo"
        Me.gbApproverLevelInfo.OpenHeight = 300
        Me.gbApproverLevelInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproverLevelInfo.ShowBorder = True
        Me.gbApproverLevelInfo.ShowCheckBox = False
        Me.gbApproverLevelInfo.ShowCollapseButton = False
        Me.gbApproverLevelInfo.ShowDefaultBorderColor = True
        Me.gbApproverLevelInfo.ShowDownButton = False
        Me.gbApproverLevelInfo.ShowHeader = True
        Me.gbApproverLevelInfo.Size = New System.Drawing.Size(439, 90)
        Me.gbApproverLevelInfo.TabIndex = 1
        Me.gbApproverLevelInfo.Temp = 0
        Me.gbApproverLevelInfo.Text = "Loan Approver Level Info"
        Me.gbApproverLevelInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPRemark
        '
        Me.lblPRemark.ForeColor = System.Drawing.Color.Red
        Me.lblPRemark.Location = New System.Drawing.Point(205, 65)
        Me.lblPRemark.Name = "lblPRemark"
        Me.lblPRemark.Size = New System.Drawing.Size(226, 13)
        Me.lblPRemark.TabIndex = 17
        Me.lblPRemark.Text = "Higher Priority means High Level."
        '
        'nudPriority
        '
        Me.nudPriority.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPriority.Location = New System.Drawing.Point(88, 62)
        Me.nudPriority.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudPriority.Name = "nudPriority"
        Me.nudPriority.Size = New System.Drawing.Size(111, 21)
        Me.nudPriority.TabIndex = 16
        Me.nudPriority.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudPriority.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPriority
        '
        Me.lblPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPriority.Location = New System.Drawing.Point(9, 65)
        Me.lblPriority.Name = "lblPriority"
        Me.lblPriority.Size = New System.Drawing.Size(62, 13)
        Me.lblPriority.TabIndex = 15
        Me.lblPriority.Text = "Priority"
        Me.lblPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(409, 34)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(22, 22)
        Me.objbtnOtherLanguage.TabIndex = 13
        '
        'txtLevelName
        '
        Me.txtLevelName.Flags = 0
        Me.txtLevelName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLevelName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLevelName.Location = New System.Drawing.Point(88, 35)
        Me.txtLevelName.Name = "txtLevelName"
        Me.txtLevelName.Size = New System.Drawing.Size(315, 21)
        Me.txtLevelName.TabIndex = 12
        '
        'lblLevelName
        '
        Me.lblLevelName.AutoSize = True
        Me.lblLevelName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelName.Location = New System.Drawing.Point(9, 38)
        Me.lblLevelName.Name = "lblLevelName"
        Me.lblLevelName.Size = New System.Drawing.Size(62, 13)
        Me.lblLevelName.TabIndex = 11
        Me.lblLevelName.Text = "Level Name"
        Me.lblLevelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMinRange
        '
        Me.LblMinRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMinRange.Location = New System.Drawing.Point(9, 92)
        Me.LblMinRange.Name = "LblMinRange"
        Me.LblMinRange.Size = New System.Drawing.Size(62, 15)
        Me.LblMinRange.TabIndex = 19
        Me.LblMinRange.Text = "Min. Range"
        '
        'nudMinimumRange
        '
        Me.nudMinimumRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudMinimumRange.DecimalPlaces = 2
        Me.nudMinimumRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinimumRange.Location = New System.Drawing.Point(88, 89)
        Me.nudMinimumRange.Maximum = New Decimal(New Integer() {1569325055, 23283064, 0, 0})
        Me.nudMinimumRange.Name = "nudMinimumRange"
        Me.nudMinimumRange.Size = New System.Drawing.Size(111, 21)
        Me.nudMinimumRange.TabIndex = 20
        Me.nudMinimumRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nudMaximumRange
        '
        Me.nudMaximumRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudMaximumRange.DecimalPlaces = 2
        Me.nudMaximumRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaximumRange.Location = New System.Drawing.Point(320, 89)
        Me.nudMaximumRange.Maximum = New Decimal(New Integer() {1569325055, 23283064, 0, 0})
        Me.nudMaximumRange.Name = "nudMaximumRange"
        Me.nudMaximumRange.Size = New System.Drawing.Size(111, 21)
        Me.nudMaximumRange.TabIndex = 21
        Me.nudMaximumRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMaximumRange.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LblMaxRange
        '
        Me.LblMaxRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMaxRange.Location = New System.Drawing.Point(243, 92)
        Me.LblMaxRange.Name = "LblMaxRange"
        Me.LblMaxRange.Size = New System.Drawing.Size(62, 15)
        Me.LblMaxRange.TabIndex = 22
        Me.LblMaxRange.Text = "Max.Range"
        '
        'frmLoanApproverLevel_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(456, 157)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanApproverLevel_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Loan Approver Level"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbApproverLevelInfo.ResumeLayout(False)
        Me.gbApproverLevelInfo.PerformLayout()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMinimumRange, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMaximumRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbApproverLevelInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblLevelName As System.Windows.Forms.Label
    Friend WithEvents txtLevelName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPriority As System.Windows.Forms.Label
    Friend WithEvents nudPriority As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPRemark As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents LblMinRange As System.Windows.Forms.Label
    Friend WithEvents nudMaximumRange As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudMinimumRange As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblMaxRange As System.Windows.Forms.Label
End Class
