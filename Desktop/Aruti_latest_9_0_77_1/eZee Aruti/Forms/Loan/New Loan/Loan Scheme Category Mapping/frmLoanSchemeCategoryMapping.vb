﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Public Class frmLoanSchemeCategoryMapping
    Private ReadOnly mstrModuleName As String = "frmLoanSchemeCategoryMapping"
    Private objLoanCategoryMapping As clsLoanCategoryMapping
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMappingUnkid As Integer = -1
    Private mblnCancel As Boolean = True

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMappingUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "
    Private Sub frmLoanSchemeCategoryMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoanCategoryMapping = Nothing
    End Sub

    Private Sub frmLoanSchemeCategoryMapping_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmLoanSchemeCategoryMapping_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanSchemeCategoryMapping_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvMasterListInfo.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmLoanSchemeCategoryMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objLoanCategoryMapping = New clsLoanCategoryMapping
            Call FillCombo()

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanSchemeCategoryMapping_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim objLoan_Advance As New clsLoan_Advance
        Dim objCMaster As New clsCommon_Master
        Dim objChStatus As New clsDiscipline_Status
        Dim dsList As New DataSet
        Try
            dsList = objLoan_Advance.GetLoan_Scheme_Categories("List", True)
            With cboLoanSchemeCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdentityType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objChStatus.getComboList("List", True, clsDiscipline_Status.enDisciplineStatusType.ALL)
            With cboChargesStatus
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboOtherDocument
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Hemant (27 Oct 2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            With cboCRBDocument
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With
            'Pinkal (23-Nov-2022) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objLoan_Advance = Nothing
            objCMaster = Nothing
            objChStatus = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try
            dsList = objLoanCategoryMapping.GetList("List")
            lvMasterListInfo.Items.Clear()

            For Each drRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                If IsDBNull(drRow("schemecategory")) Then
                    lvItem.SubItems.Add("")
                Else

                    lvItem.Text = drRow("schemecategory").ToString
                End If
                If IsDBNull(drRow("identitytype")) Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("identitytype").ToString)
                End If
                If IsDBNull(drRow("chargestatus")) Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("chargestatus").ToString)
                End If

                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                If IsDBNull(drRow("otherdocument")) Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("otherdocument").ToString)
                End If
                'Hemant (27 Oct 2022) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                If IsDBNull(drRow("crbdocument")) Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("crbdocument").ToString)
                End If
                'Pinkal (23-Nov-2022) -- End


                lvItem.Tag = drRow("mappingunkid")
                lvMasterListInfo.Items.Add(lvItem)
                lvItem = Nothing
            Next

            lvMasterListInfo.GridLines = False
            lvMasterListInfo.GroupingColumn = colhLoanSchemeCateogry
            lvMasterListInfo.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLoanCategoryMapping._LoanschemeCategoryId = CInt(cboLoanSchemeCategory.SelectedValue)
            objLoanCategoryMapping._IdentityTypeUnkid = CInt(cboIdentityType.SelectedValue)
            objLoanCategoryMapping._ChargeStatusunkid = CInt(cboChargesStatus.SelectedValue)
            objLoanCategoryMapping._Userunkid = User._Object._Userunkid
            objLoanCategoryMapping._WebFormName = mstrModuleName
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            objLoanCategoryMapping._OtherDocumentunkid = CInt(cboOtherDocument.SelectedValue)
            'Hemant (27 Oct 2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objLoanCategoryMapping._CRBDocumentunkid = CInt(cboCRBDocument.SelectedValue)
            'Pinkal (23-Nov-2022) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboLoanSchemeCategory.SelectedValue = objLoanCategoryMapping._LoanschemeCategoryId
            cboIdentityType.SelectedValue = objLoanCategoryMapping._IdentityTypeUnkid
            cboChargesStatus.SelectedValue = objLoanCategoryMapping._ChargeStatusunkid
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            cboOtherDocument.SelectedValue = objLoanCategoryMapping._OtherDocumentunkid
            'Hemant (27 Oct 2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            cboCRBDocument.SelectedValue = objLoanCategoryMapping._CRBDocumentunkid
            'Pinkal (23-Nov-2022) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboLoanSchemeCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Scheme Category cannot be blank. Loan Scheme Category is required information."), enMsgBoxStyle.Information) '?1
                cboLoanSchemeCategory.Focus()
                Exit Sub
            End If


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objLoanCategoryMapping.Update()
            Else
                blnFlag = objLoanCategoryMapping.Insert()
            End If

            If blnFlag = False And objLoanCategoryMapping._Message <> "" Then
                eZeeMsgBox.Show(objLoanCategoryMapping._Message, enMsgBoxStyle.Information)
            End If


            If blnFlag Then

                If menAction = enAction.ADD_CONTINUE Then
                    objLoanCategoryMapping = Nothing
                    objLoanCategoryMapping = New clsLoanCategoryMapping
                    Call GetValue()
                    cboLoanSchemeCategory.Focus()
                Else
                    mintMappingUnkid = objLoanCategoryMapping._Mappingunkid
                End If
            End If

            Call FillList()

            If blnFlag = True Then
                cboLoanSchemeCategory.Enabled = True
                cboLoanSchemeCategory.SelectedIndex = 0
                cboIdentityType.SelectedIndex = 0
                cboChargesStatus.SelectedIndex = 0
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                cboOtherDocument.SelectedIndex = 0
                'Hemant (27 Oct 2022) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                cboCRBDocument.SelectedIndex = 0
                'Pinkal (23-Nov-2022) -- End

                menAction = enAction.ADD_CONTINUE
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            If lvMasterListInfo.SelectedItems.Count <= 0 Then
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Record?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objLoanCategoryMapping._Isvoid = True
                objLoanCategoryMapping._Voiduserunkid = User._Object._Userunkid
                objLoanCategoryMapping._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objLoanCategoryMapping._Voidreason = ""
                objLoanCategoryMapping._WebFormName = mstrModuleName
                objLoanCategoryMapping._Userunkid = User._Object._Userunkid
                objLoanCategoryMapping.Delete(mintMappingUnkid)
                If objLoanCategoryMapping._Message <> "" Then
                    eZeeMsgBox.Show(objLoanCategoryMapping._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    objLoanCategoryMapping = Nothing
                    objLoanCategoryMapping = New clsLoanCategoryMapping
                    Call FillList()
                End If

            End If

            cboLoanSchemeCategory.Enabled = True
            cboLoanSchemeCategory.SelectedIndex = 0
            cboIdentityType.SelectedIndex = 0
            cboChargesStatus.SelectedIndex = 0
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            cboOtherDocument.SelectedIndex = 0
            'Hemant (27 Oct 2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            cboCRBDocument.SelectedIndex = 0
            'Pinkal (23-Nov-2022) -- End


            menAction = enAction.ADD_CONTINUE
            lvMasterListInfo.Select()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control Events "
    Private Sub lvMasterListInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvMasterListInfo.SelectedIndexChanged
        Try
            If lvMasterListInfo.SelectedItems.Count > 0 Then
                lvMasterListInfo.Enabled = True
                menAction = enAction.EDIT_ONE
                mintMappingUnkid = CInt(lvMasterListInfo.SelectedItems(0).Tag)
                objLoanCategoryMapping._Mappingunkid = mintMappingUnkid
                Call GetValue()
                cboLoanSchemeCategory.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasterListInfo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

End Class