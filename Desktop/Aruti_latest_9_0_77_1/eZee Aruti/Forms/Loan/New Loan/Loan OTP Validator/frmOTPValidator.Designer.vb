﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOTPValidator
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOTPValidator))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.LblMessage = New System.Windows.Forms.Label
        Me.btnVerify = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbTOTP = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtVerifyOTP = New eZee.TextBox.AlphanumericTextBox
        Me.LblSeconds = New System.Windows.Forms.Label
        Me.LnkSendCodeAgain = New System.Windows.Forms.LinkLabel
        Me.LblOTP = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.gbTOTP.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbTOTP)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(349, 123)
        Me.pnlMain.TabIndex = 0
        '
        'LblMessage
        '
        Me.LblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMessage.ForeColor = System.Drawing.Color.Red
        Me.LblMessage.Location = New System.Drawing.Point(9, 86)
        Me.LblMessage.Name = "LblMessage"
        Me.LblMessage.Size = New System.Drawing.Size(94, 19)
        Me.LblMessage.TabIndex = 426
        '
        'btnVerify
        '
        Me.btnVerify.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVerify.BackColor = System.Drawing.Color.White
        Me.btnVerify.BackgroundImage = CType(resources.GetObject("btnVerify.BackgroundImage"), System.Drawing.Image)
        Me.btnVerify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVerify.BorderColor = System.Drawing.Color.Empty
        Me.btnVerify.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVerify.FlatAppearance.BorderSize = 0
        Me.btnVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVerify.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVerify.ForeColor = System.Drawing.Color.Black
        Me.btnVerify.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVerify.GradientForeColor = System.Drawing.Color.Black
        Me.btnVerify.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVerify.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVerify.Location = New System.Drawing.Point(129, 80)
        Me.btnVerify.Name = "btnVerify"
        Me.btnVerify.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVerify.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVerify.Size = New System.Drawing.Size(97, 30)
        Me.btnVerify.TabIndex = 1
        Me.btnVerify.Text = "&Verify"
        Me.btnVerify.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(232, 80)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbTOTP
        '
        Me.gbTOTP.BorderColor = System.Drawing.Color.Black
        Me.gbTOTP.Checked = False
        Me.gbTOTP.CollapseAllExceptThis = False
        Me.gbTOTP.CollapsedHoverImage = Nothing
        Me.gbTOTP.CollapsedNormalImage = Nothing
        Me.gbTOTP.CollapsedPressedImage = Nothing
        Me.gbTOTP.CollapseOnLoad = False
        Me.gbTOTP.Controls.Add(Me.LblMessage)
        Me.gbTOTP.Controls.Add(Me.txtVerifyOTP)
        Me.gbTOTP.Controls.Add(Me.btnVerify)
        Me.gbTOTP.Controls.Add(Me.LblSeconds)
        Me.gbTOTP.Controls.Add(Me.btnClose)
        Me.gbTOTP.Controls.Add(Me.LnkSendCodeAgain)
        Me.gbTOTP.Controls.Add(Me.LblOTP)
        Me.gbTOTP.ExpandedHoverImage = Nothing
        Me.gbTOTP.ExpandedNormalImage = Nothing
        Me.gbTOTP.ExpandedPressedImage = Nothing
        Me.gbTOTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTOTP.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTOTP.HeaderHeight = 25
        Me.gbTOTP.HeaderMessage = ""
        Me.gbTOTP.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTOTP.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTOTP.HeightOnCollapse = 0
        Me.gbTOTP.LeftTextSpace = 0
        Me.gbTOTP.Location = New System.Drawing.Point(3, 3)
        Me.gbTOTP.Name = "gbTOTP"
        Me.gbTOTP.OpenHeight = 300
        Me.gbTOTP.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTOTP.ShowBorder = True
        Me.gbTOTP.ShowCheckBox = False
        Me.gbTOTP.ShowCollapseButton = False
        Me.gbTOTP.ShowDefaultBorderColor = True
        Me.gbTOTP.ShowDownButton = False
        Me.gbTOTP.ShowHeader = True
        Me.gbTOTP.Size = New System.Drawing.Size(343, 117)
        Me.gbTOTP.TabIndex = 31
        Me.gbTOTP.Temp = 0
        Me.gbTOTP.Text = "Verify OTP"
        Me.gbTOTP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVerifyOTP
        '
        Me.txtVerifyOTP.Flags = 0
        Me.txtVerifyOTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVerifyOTP.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVerifyOTP.Location = New System.Drawing.Point(115, 30)
        Me.txtVerifyOTP.MaxLength = 6
        Me.txtVerifyOTP.Name = "txtVerifyOTP"
        Me.txtVerifyOTP.Size = New System.Drawing.Size(214, 21)
        Me.txtVerifyOTP.TabIndex = 360
        '
        'LblSeconds
        '
        Me.LblSeconds.ForeColor = System.Drawing.Color.Red
        Me.LblSeconds.Location = New System.Drawing.Point(115, 56)
        Me.LblSeconds.Name = "LblSeconds"
        Me.LblSeconds.Size = New System.Drawing.Size(94, 19)
        Me.LblSeconds.TabIndex = 424
        '
        'LnkSendCodeAgain
        '
        Me.LnkSendCodeAgain.AutoSize = True
        Me.LnkSendCodeAgain.Enabled = False
        Me.LnkSendCodeAgain.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LnkSendCodeAgain.Location = New System.Drawing.Point(248, 58)
        Me.LnkSendCodeAgain.Name = "LnkSendCodeAgain"
        Me.LnkSendCodeAgain.Size = New System.Drawing.Size(74, 13)
        Me.LnkSendCodeAgain.TabIndex = 423
        Me.LnkSendCodeAgain.TabStop = True
        Me.LnkSendCodeAgain.Text = "Resend OTP"
        '
        'LblOTP
        '
        Me.LblOTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblOTP.Location = New System.Drawing.Point(6, 32)
        Me.LblOTP.Name = "LblOTP"
        Me.LblOTP.Size = New System.Drawing.Size(96, 17)
        Me.LblOTP.TabIndex = 418
        Me.LblOTP.Text = "Enter OTP"
        Me.LblOTP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmOTPValidator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(349, 123)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOTPValidator"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Verify OTP"
        Me.pnlMain.ResumeLayout(False)
        Me.gbTOTP.ResumeLayout(False)
        Me.gbTOTP.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbTOTP As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtVerifyOTP As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblSeconds As System.Windows.Forms.Label
    Friend WithEvents LnkSendCodeAgain As System.Windows.Forms.LinkLabel
    Friend WithEvents LblOTP As System.Windows.Forms.Label
    Friend WithEvents btnVerify As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents LblMessage As System.Windows.Forms.Label
End Class
