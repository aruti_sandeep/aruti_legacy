﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmLoanApprover_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmLoanApprover_AddEdit"

    Private objLoanApprover As clsLoan_Approver

    Private mintLoanApproverUnkid As Integer = -1
    Dim objEmployeeData As clsEmployee_Master
    Dim strApproverUnkid As String = String.Empty


    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Dim mdicValue As New Dictionary(Of String, Integer)
    'Pinkal (12-Oct-2011) -- End

#End Region

#Region " Form's Events "

    Private Sub frmLoanApprover_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoanApprover = Nothing
    End Sub

    Private Sub frmLoanApprover_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmLoanApprover_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanApprover_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            Call ApproverFillList()
            Call EmployeeFillList()


            'S.SANDEEP [ 08 AUG 2011 ] -- START
            'ISSUE : GENERAL CHANGES
            'lnkDeSelectAll.Visible = False
            'lnkSelectToAll.Visible = True
            'S.SANDEEP [ 08 AUG 2011 ] -- END 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApprover_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Approver.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Approver"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Dim blnFlag As Boolean = False
    '    Try
    '        'S.SANDEEP [ 08 AUG 2011 ] -- START
    '        'ISSUE : GENERAL CHANGES
    '        'objLoanApprover.Delete()
    '        'For i As Integer = 0 To lvApprover.Items.Count - 1
    '        '    objLoanApprover._Approverunkid = CInt(lvApprover.Items(i).Tag)
    '        '    objLoanApprover.Insert()
    '        'Next
    '        'Me.Close()
'
    '        If StrRemovedIds.Length > 0 Then
    '            For Each StrId As String In StrRemovedIds.Split(CChar(","))
    '                objLoanApprover.Delete(CInt(StrId))
    '                objLoanApprover.DeleteMapping(CInt(StrId))
    '        Next
    '        End If
'
    '        Dim mintNewUnkid As Integer = -1
    '        For Each LVI As ListViewItem In lvApprover.Items
    '            If CInt(LVI.SubItems(objcolhAppTranId.Index).Text) > 0 Then
    '                objLoanApprover.Delete(CInt(LVI.SubItems(objcolhAppTranId.Index).Text))
    '            End If
    '            objLoanApprover._Approverunkid = CInt(LVI.Tag)
    '            mintNewUnkid = objLoanApprover.Insert()
    '            If CInt(LVI.SubItems(objcolhAppTranId.Index).Text) > 0 Then
    '                objLoanApprover.UpdateMapping(CInt(LVI.SubItems(objcolhAppTranId.Index).Text), mintNewUnkid)
    '            End If
    '            mintNewUnkid = -1
    '        Next
    '        Call ApproverFillList()
    '        'S.SANDEEP [ 08 AUG 2011 ] -- END 
'
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            Dim dctApprover As New Dictionary(Of Integer, String)
            For Each LVI As ListViewItem In lvApprover.Items

                If dctApprover.ContainsKey(CInt(LVI.SubItems(objcolhAppTranId.Index).Text)) Then
                    dctApprover(CInt(LVI.SubItems(objcolhAppTranId.Index).Text)) &= "," & LVI.Tag.ToString()
                Else
                    dctApprover.Add(CInt(LVI.SubItems(objcolhAppTranId.Index).Text), LVI.Tag.ToString())
                End If
            Next

            If dctApprover IsNot Nothing AndAlso dctApprover.Keys.Count > 0 Then
                blnFlag = objLoanApprover.Insert(mdicValue, dctApprover)
            End If

            If blnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Infomation saved successfully."), enMsgBoxStyle.Information)
            End If

            Call ApproverFillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Private Method "

    Private Sub EmployeeFillList()
        objLoanApprover = New clsLoan_Approver
        objEmployeeData = New clsEmployee_Master
        Dim lvItem As ListViewItem
        Dim dsList As New DataSet
        Dim dtTable As New DataTable
        Try
            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            RemoveHandler objChkECheck.CheckedChanged, AddressOf objChkECheck_CheckedChanged

            lvEmployee.Items.Clear()
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployeeData.GetEmployeeList("Employee List")

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployeeData.GetEmployeeList("Employee List", , , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            dsList = objEmployeeData.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, False, "Employee List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            If strApproverUnkid.Length > 0 Then
                dtTable = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strApproverUnkid & " )", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables(0)
            End If

            'S.SANDEEP [ 08 AUG 2011 ] -- START
            'ISSUE : GENERAL CHANGES
            'For Each drRow As DataRow In dtTable.Rows
            '    lvItem = New ListViewItem

            '    If IsDBNull(drRow("employeename")) Then
            '        lvItem.SubItems.Add("")
            '    Else
            '        lvItem.Text = drRow("employeename").ToString
            '    End If
            '    lvItem.Tag = drRow("employeeunkid")
            '    lvEmployee.Items.Add(lvItem)
            '    lvItem = Nothing

            'Next

            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                'S.SANDEEP [ 27 JAN 2014 ] -- START
                'lvItem.Text = ""
                'lvItem.SubItems.Add(drRow.Item("employeecode").ToString)
                'lvItem.SubItems.Add(drRow.Item("employeename").ToString)
                lvItem.Text = drRow.Item("employeecode").ToString & " - " & drRow.Item("employeename").ToString
                'S.SANDEEP [ 27 JAN 2014 ] -- END

                lvItem.Tag = drRow.Item("employeeunkid")
                lvEmployee.Items.Add(lvItem)
            Next

            If lvEmployee.Items.Count > 15 Then
                'S.SANDEEP [ 27 JAN 2014 ] -- START
                '    colhFromEmployee.Width = 195 - 20
                'Else
                '    colhFromEmployee.Width = 195
                colhCode.Width = 300 - 20
                objChkECheck.Location = New Point(285 - 20, 62)
            Else
                colhCode.Width = 300
                objChkECheck.Location = New Point(285, 62)
                'S.SANDEEP [ 27 JAN 2014 ] -- END
            End If

            'S.SANDEEP [ 08 AUG 2011 ] -- END 

            AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            AddHandler objChkECheck.CheckedChanged, AddressOf objChkECheck_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EmployeeFillList", mstrModuleName)
        Finally
            If dsList IsNot Nothing Then
                dsList.Dispose()
            End If
            dsList = Nothing
            lvItem = Nothing
        End Try
    End Sub

    Private Sub ApproverFillList()

        objLoanApprover = New clsLoan_Approver
        objEmployeeData = New clsEmployee_Master

        Dim lvItem As ListViewItem
        Dim dsList As New DataSet

        Try
            RemoveHandler lvApprover.ItemChecked, AddressOf lvApprover_ItemChecked
            RemoveHandler objChkACheck.CheckedChanged, AddressOf objChkACheck_CheckedChanged

            lvApprover.Items.Clear()
            dsList = objLoanApprover.GetList("ApproverList")

            'S.SANDEEP [ 08 AUG 2011 ] -- START
            'ISSUE : GENERAL CHANGES
            'For Each drRow As DataRow In dsList.Tables(0).Rows
            '    lvItem = New ListViewItem

            '    If IsDBNull(drRow("approvername")) Then
            '        lvItem.SubItems.Add("")
            '    Else
            '        lvItem.Text = drRow("approvername").ToString
            '    End If
            '    lvItem.Tag = drRow("approverunkid")
            '    If strApproverUnkid.Length <= 0 Then
            '        strApproverUnkid = CStr(lvItem.Tag)
            '    Else
            '        strApproverUnkid &= " ," & CStr(lvItem.Tag)
            '    End If
            '    lvApprover.Items.Add(lvItem)
            '    lvItem = Nothing
            'Next

            For Each drRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                'S.SANDEEP [ 27 JAN 2014 ] -- START
                'lvItem.Text = ""
                'lvItem.SubItems.Add(drRow.Item("Code").ToString)
                'lvItem.SubItems.Add(drRow.Item("approvername").ToString)
                lvItem.Text = drRow.Item("Code").ToString & " - " & drRow.Item("approvername").ToString
                'S.SANDEEP [ 27 JAN 2014 ] -- END

                lvItem.SubItems.Add(drRow.Item("approvertranunkid").ToString)
                lvItem.Tag = drRow.Item("approverunkid")
                If strApproverUnkid.Length <= 0 Then
                    strApproverUnkid = CStr(lvItem.Tag)
                Else
                    strApproverUnkid &= " ," & CStr(lvItem.Tag)
                End If
                lvApprover.Items.Add(lvItem)
            Next

            If lvApprover.Items.Count > 15 Then
                'S.SANDEEP [ 27 JAN 2014 ] -- START
                '    colhToApprover.Width = 195 - 20
                'Else
                '    colhToApprover.Width = 195
                colhACode.Width = 300 - 20
                objChkACheck.Location = New Point(644 - 20, 62)
            Else
                colhACode.Width = 300
                objChkACheck.Location = New Point(644, 62)
                'S.SANDEEP [ 27 JAN 2014 ] -- END
            End If

            'S.SANDEEP [ 08 AUG 2011 ] -- END 

            AddHandler lvApprover.ItemChecked, AddressOf lvApprover_ItemChecked
            AddHandler objChkACheck.CheckedChanged, AddressOf objChkACheck_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ApproverFillList", mstrModuleName)
        Finally
            If dsList IsNot Nothing Then
                dsList.Dispose()
            End If
            dsList = Nothing
            lvItem = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnSave.Enabled = User._Object.Privilege._AllowLoanApproverCreation
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Button's Events "
    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try
            For Each lvItem As ListViewItem In lvEmployee.CheckedItems
                Dim lvAppItem As New ListViewItem

                lvAppItem.Text = lvItem.Text
                'S.SANDEEP [ 27 JAN 2014 ] -- START
                'lvAppItem.SubItems.Add(lvItem.SubItems(colhCode.Index).Text)
                'lvAppItem.SubItems.Add(lvItem.SubItems(colhFromEmployee.Index).Text)
                'S.SANDEEP [ 27 JAN 2014 ] -- END

                'Pinkal (12-Oct-2011) -- Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

                If mdicValue.ContainsKey(lvItem.SubItems(colhCode.Index).Text) = True Then
                    lvAppItem.SubItems.Add(mdicValue(lvItem.SubItems(colhCode.Index).Text).ToString)
                    mdicValue.Remove(lvItem.SubItems(colhCode.Index).Text)
                Else
                    lvAppItem.SubItems.Add("-1")
                End If

                'Pinkal (12-Oct-2011) -- End

                lvAppItem.Tag = lvItem.Tag
                lvEmployee.Items.Remove(lvItem)
                lvApprover.Items.Add(lvAppItem)
                lvApprover.Sorting = SortOrder.Ascending
                lvAppItem.Checked = False
                lvAppItem = Nothing
            Next

            'S.SANDEEP [ 27 JAN 2014 ] -- START
            If lvApprover.Items.Count > 15 Then
                colhACode.Width = 300 - 20
                objChkACheck.Location = New Point(644 - 20, 62)
            Else
                colhACode.Width = 300
                objChkACheck.Location = New Point(644, 62)
            End If
            'S.SANDEEP [ 27 JAN 2014 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnUnassign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnassign.Click
        Dim blnFlag As Boolean = False
        Try
            For Each lvItem As ListViewItem In lvApprover.CheckedItems
                If objLoanApprover.CanDeleteApprover(CInt(lvItem.Tag)) = True Then
                    blnFlag = True
                    Continue For
                End If
                Dim lvEmpItems As New ListViewItem
                lvEmpItems = lvItem
                lvApprover.Items.Remove(lvItem)
                lvEmployee.Items.Add(lvEmpItems)
                lvEmployee.Sorting = SortOrder.Ascending
                lvEmpItems.Checked = False
                lvEmpItems = Nothing

                'Pinkal (12-Oct-2011) -- Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

                If mdicValue.ContainsKey(lvItem.SubItems(colhCode.Index).Text) = False Then
                    'Sohail (04 Aug 2012) -- Start
                    'TRA - ENHANCEMENT
                    'mdicValue.Add(lvItem.SubItems(colhCode.Index).Text, CInt(lvItem.SubItems(objcolhAppTranId.Index).Text))
                    mdicValue.Add(lvItem.SubItems(colhCode.Index).Text, CInt(lvItem.Tag))
                    'Sohail (04 Aug 2012) -- End
                End If
                'Pinkal (12-Oct-2011) -- End

            Next


            'S.SANDEEP [ 27 JAN 2014 ] -- START
            If lvEmployee.Items.Count > 15 Then
                colhCode.Width = 300 - 20
                objChkECheck.Location = New Point(285 - 20, 62)
            Else
                colhCode.Width = 300
                objChkECheck.Location = New Point(285, 62)
            End If
            'S.SANDEEP [ 27 JAN 2014 ] -- END


            'If StrRemovedIds.Length > 0 Then
            '    'StrRemovedIds = Mid(StrRemovedIds, 2)
            '    StrRemovedIds = StrRemovedIds.Substring(0, StrRemovedIds.Length - 1)
            'End If

            'S.SANDEEP [ 08 AUG 2011 ] -- START
            'ISSUE : GENERAL CHANGES
            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "You cannot remove Approver(s). Reason : Approver(s) must be linked with some transactions."), enMsgBoxStyle.Information)
            End If
            'S.SANDEEP [ 08 AUG 2011 ] -- END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnassign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objChkACheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkACheck.CheckedChanged
        Try
            RemoveHandler lvApprover.ItemChecked, AddressOf lvApprover_ItemChecked
            For Each LVI As ListViewItem In lvApprover.Items
                LVI.Checked = CBool(objChkACheck.CheckState)
            Next
            AddHandler lvApprover.ItemChecked, AddressOf lvApprover_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkACheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvApprover_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvApprover.ItemChecked
        Try
            RemoveHandler objChkACheck.CheckedChanged, AddressOf objChkACheck_CheckedChanged
            If lvApprover.CheckedItems.Count <= 0 Then
                objChkACheck.CheckState = CheckState.Unchecked
            ElseIf lvApprover.CheckedItems.Count < lvApprover.Items.Count Then
                objChkACheck.CheckState = CheckState.Indeterminate
            ElseIf lvApprover.CheckedItems.Count = lvApprover.Items.Count Then
                objChkACheck.CheckState = CheckState.Checked
            End If
            AddHandler objChkACheck.CheckedChanged, AddressOf objChkACheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApprover_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objChkECheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkECheck.CheckedChanged
        Try
            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            For Each LVI As ListViewItem In lvEmployee.Items
                LVI.Checked = CBool(objChkECheck.CheckState)
            Next
            AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkECheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvEmployee_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployee.ItemChecked
        Try
            RemoveHandler objChkECheck.CheckedChanged, AddressOf objChkECheck_CheckedChanged
            If lvEmployee.CheckedItems.Count <= 0 Then
                objChkECheck.CheckState = CheckState.Unchecked
            ElseIf lvEmployee.CheckedItems.Count < lvEmployee.Items.Count Then
                objChkECheck.CheckState = CheckState.Indeterminate
            ElseIf lvEmployee.CheckedItems.Count = lvEmployee.Items.Count Then
                objChkECheck.CheckState = CheckState.Checked
            End If
            AddHandler objChkECheck.CheckedChanged, AddressOf objChkECheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployee_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub lnkSelectToAll_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
    '    Try

    '        'S.SANDEEP [ 08 AUG 2011 ] -- START
    '        'ISSUE : GENERAL CHANGES
    '        'lnkDeSelectAll.Visible = True
    '        'lnkSelectToAll.Visible = False
    '        'S.SANDEEP [ 08 AUG 2011 ] -- END 
    '        For i As Integer = 0 To lvApprover.Items.Count - 1
    '            lvApprover.Items(i).Checked = True
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkSelectToAll_LinkClicked", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lnkDeSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        'S.SANDEEP [ 08 AUG 2011 ] -- START
    '        'ISSUE : GENERAL CHANGES
    '        'lnkSelectToAll.Visible = True
    '        'S.SANDEEP [ 08 AUG 2011 ] -- END 
    '        For i As Integer = 0 To lvApprover.Items.Count - 1
    '            lvApprover.Items(i).Checked = False
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkDeSelectAll_Click", mstrModuleName)
    '    End Try
    'End Sub
    'S.SANDEEP [ 08 AUG 2011 ] -- END 

#End Region

    'S.SANDEEP [ 27 JAN 2014 ] -- START
#Region " Controls Events "

    Private Sub txtSearchApprover_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchApprover.TextChanged, txtSearchEmp.TextChanged
        Try
            Select Case CType(sender, TextBox).Name.ToString.ToUpper
                Case txtSearchApprover.Name.ToUpper
                    If lvApprover.Items.Count <= 0 Then Exit Sub
                    lvApprover.SelectedIndices.Clear()
                    Dim list = From item In lvApprover.Items.Cast(Of ListViewItem)() Where item.Text.ToUpper.Contains(txtSearchApprover.Text.ToUpper) Select item.Text
                    If list IsNot Nothing AndAlso list.Count > 0 Then
                        Dim lvFound As ListViewItem = lvApprover.FindItemWithText(list.ElementAt(0))
                        If lvFound IsNot Nothing Then
                            lvFound.EnsureVisible()
                            lvApprover.TopItem = lvFound
                            lvFound.Selected = True
                        End If
                    End If

                Case txtSearchEmp.Name.ToUpper
                    If lvEmployee.Items.Count <= 0 Then Exit Sub
                    lvEmployee.SelectedIndices.Clear()
                    Dim list = From item In lvEmployee.Items.Cast(Of ListViewItem)() Where item.Text.ToUpper.Contains(txtSearchEmp.Text.ToUpper) Select item.Text
                    If list IsNot Nothing AndAlso list.Count > 0 Then
                        Dim lvFound As ListViewItem = lvEmployee.FindItemWithText(list.ElementAt(0))
                        If lvFound IsNot Nothing Then
                            lvFound.EnsureVisible()
                            lvEmployee.TopItem = lvFound
                            lvFound.Selected = True
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchApprover_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 27 JAN 2014 ] -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbApproversInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApproversInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUnassign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUnassign.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbApproversInfo.Text = Language._Object.getCaption(Me.gbApproversInfo.Name, Me.gbApproversInfo.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhACode.Text = Language._Object.getCaption(CStr(Me.colhACode.Tag), Me.colhACode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "You cannot remove Approver(s). Reason : Approver(s) must be linked with some transactions.")
            Language.setMessage(mstrModuleName, 2, "Infomation saved successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class