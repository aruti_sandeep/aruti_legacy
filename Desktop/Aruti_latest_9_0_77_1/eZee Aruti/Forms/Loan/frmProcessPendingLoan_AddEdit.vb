﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math

#End Region

Public Class frmProcessPendingLoan_AddEdit

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmProcessPendingLoan_AddEdit"
    Private objProcesspendingloan As clsProcess_pending_loan
    Private mintProcessPendingLoanUnkid As Integer = -1
    Private mintEmployeeSelectedId As Integer = -1
    Private mintApproverSelectedId As Integer = -1
    Private mdecLoanAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mdecApprovedAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objStatusTran As clsProcess_pending_loan
    Private mstrStatusData As String = String.Empty
   
    Private mintLoanAdvaceunkid As Integer
    Private mdecInterestAmount As Decimal 'Sohail (11 May 2011)
    Private mdecNetAmount As Decimal 'Sohail (11 May 2011)
    Private mblnIsAdvance As Boolean


    Private mdecOldAmount As Decimal = 0 'Sohail (11 May 2011)

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private mintNewStatusId As Integer = 0
    'Sandeep [ 21 Aug 2010 ] -- End 


#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intNewStatusId As Integer = 0) As Boolean
        Try
            mintProcessPendingLoanUnkid = intUnkId
            menAction = eAction
            mintNewStatusId = intNewStatusId

            Me.ShowDialog()

            intUnkId = mintProcessPendingLoanUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Private Methods"

    Private Sub SetColor()
        Try
            txtLoanAmt.BackColor = GUI.ColorComp
            cboApprovedBy.BackColor = GUI.ColorComp
            cboEmpName.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            cboLoanScheme.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorComp
            txtApplicationNo.BackColor = GUI.ColorComp
            txtExternalEntity.BackColor = GUI.ColorComp
            'S.SANDEEP [ 18 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtEmployeeRemark.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 18 APRIL 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Public Sub SetValue()

        Try
            If radLoan.Checked Then
                objProcesspendingloan._Isloan = True
            Else
                objProcesspendingloan._Isloan = False
            End If
            objProcesspendingloan._Application_No = txtApplicationNo.Text
            objProcesspendingloan._Application_Date = dtpApplicationDate.Value
            objProcesspendingloan._Loan_Amount = txtLoanAmt.Decimal 'Sohail (11 May 2011)
            objProcesspendingloan._Approved_Amount = txtApprovedAmt.Decimal 'Sohail (11 May 2011)
            objProcesspendingloan._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objProcesspendingloan._Approverunkid = CInt(cboApprovedBy.SelectedValue)
            objProcesspendingloan._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            objProcesspendingloan._Loan_Statusunkid = CInt(cboStatus.SelectedValue)
            objProcesspendingloan._Isvoid = False
            objProcesspendingloan._Voiddatetime = Nothing
            objProcesspendingloan._Voiduserunkid = -1
            objProcesspendingloan._Remark = txtRemarks.Text
            'Sandeep [ 21 Aug 2010 ] -- Start
            objProcesspendingloan._Isexternal_Entity = CBool(chkExternalEntity.CheckState)
            objProcesspendingloan._External_Entity_Name = txtExternalEntity.Text
            'Sandeep [ 21 Aug 2010 ] -- End 


            'Anjan (03 Aug 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Rutta's Request
            objProcesspendingloan._Userunkid = User._Object._Userunkid
            'Anjan (03 Aug 2012)-End 


            'objProcesspendingloan._LoanAdvanceId = mintLoanAdvaceunkid
            'objProcesspendingloan._Loan_Amount = txtApprovedAmt.Decimal
            'objProcesspendingloan._Interest_Amount = mdblInterestAmount
            'objProcesspendingloan._Net_Amount = mdblNetAmount
            'objProcesspendingloan._IsAdvance = mblnIsAdvance

            'mstrStatusData = objStatusTran._Isvoid & "|" & _
            '                       objStatusTran._Voiddatetime & "|" & _
            '                       objStatusTran._Voiduserunkid & "|"

            'S.SANDEEP [ 18 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objProcesspendingloan._Emp_Remark = txtEmployeeRemark.Text
            'S.SANDEEP [ 18 APRIL 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try

    End Sub

    Public Sub GetValue()

        Try
            If menAction = enAction.EDIT_ONE Then 'Anjan (18 Feb 2011) - Start  - Issue : Is External Entity has been made default as per Rutta's Comment after release
                If objProcesspendingloan._Isloan = True Then
                    radLoan.Checked = True
                Else
                    radAdvance.Checked = True
                End If

                'Sandeep [ 21 Aug 2010 ] -- Start
                chkExternalEntity.Checked = objProcesspendingloan._Isexternal_Entity
                txtExternalEntity.Text = objProcesspendingloan._External_Entity_Name
                'Sandeep [ 21 Aug 2010 ] -- End 
            Else
                radLoan.Checked = True
                chkExternalEntity.CheckState = CheckState.Checked
            End If
            If Not (objProcesspendingloan._Application_Date = Nothing) Then
                dtpApplicationDate.Value = objProcesspendingloan._Application_Date
            End If 'Anjan (18 Feb 2011) - End

            txtApplicationNo.Text = objProcesspendingloan._Application_No
            cboEmpName.SelectedValue = CStr(objProcesspendingloan._Employeeunkid)
            txtLoanAmt.Text = Format(objProcesspendingloan._Loan_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            txtApprovedAmt.Text = Format(objProcesspendingloan._Approved_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            cboLoanScheme.SelectedValue = CInt(objProcesspendingloan._Loanschemeunkid)
            'Sandeep [ 21 Aug 2010 ] -- Start
            'If mintProcessPendingLoanUnkid = -1 Then
            '    cboStatus.SelectedValue = 1
            '    txtRemarks.Text = objProcesspendingloan._Remark
            'Else
            '    cboStatus.SelectedValue = CInt(objProcesspendingloan._Loan_Statusunkid)
            'End If
            If mintNewStatusId > 0 Then
                Select Case mintNewStatusId
                    Case 1
                        cboStatus.SelectedValue = mintNewStatusId
                        cboApprovedBy.SelectedValue = 0
                    Case Else
                        cboStatus.SelectedValue = mintNewStatusId
                End Select
            Else
                If mintProcessPendingLoanUnkid = -1 Then
                    cboStatus.SelectedValue = 1
                    txtRemarks.Text = objProcesspendingloan._Remark
                Else
                    cboStatus.SelectedValue = CInt(objProcesspendingloan._Loan_Statusunkid)
                End If
            End If
            'Sandeep [ 21 Aug 2010 ] -- End
            cboApprovedBy.SelectedValue = CInt(objProcesspendingloan._Approverunkid)
            objProcesspendingloan._Isvoid = objProcesspendingloan._Isvoid
            objProcesspendingloan._Voiddatetime = objProcesspendingloan._Voiddatetime
            objProcesspendingloan._Voiduserunkid = objProcesspendingloan._Voiduserunkid

            mdecOldAmount = objProcesspendingloan._Approved_Amount

            'S.SANDEEP [ 18 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objProcesspendingloan._Emp_Remark.Trim.Length > 0 Then
                txtEmployeeRemark.ReadOnly = True
            End If
            txtEmployeeRemark.Text = objProcesspendingloan._Emp_Remark
            'S.SANDEEP [ 18 APRIL 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMasterData As New clsMasterData
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objApprover As New clsLoan_Approver
        Dim objProcesspendingloan As New clsProcess_pending_loan

        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Employee", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (06 Jan 2012) -- End
            With cboEmpName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With

            'Pinkal (15-Jun-2011) --Start

            'dsCombos = objApprover.GetList("Approver", True, True)
            dsCombos = objApprover.GetList("Approver", True, True, True)

            'Pinkal (15-Jun-2011) --End
            With cboApprovedBy
                .ValueMember = "approverunkid"
                .DisplayMember = "approvername"
                .DataSource = dsCombos.Tables("Approver")
                .SelectedValue = 0
            End With

            dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With

            dsCombos = objProcesspendingloan.GetLoan_Status("Status")
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Status")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboEmpName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmpName.Focus()
                Return False
            End If

            If radLoan.Checked Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), enMsgBoxStyle.Information)
                    cboLoanScheme.Focus()
                    Return False
                End If
            End If

            If txtLoanAmt.Decimal = 0 Then 'Sohail (11 May 2011)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information."), enMsgBoxStyle.Information)
                txtLoanAmt.Focus()
                Return False
            End If

            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
                'If CInt(cboApprovedBy.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
                '    cboApprovedBy.Focus()
                '    Return False
                'End If



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
                Select Case CInt(cboStatus.SelectedValue)
                    Case 2, 3
                        If CInt(cboApprovedBy.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
                            cboApprovedBy.Focus()
                            Return False
                        End If
                    Case Else
                        cboApprovedBy.SelectedValue = 0
                End Select
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            'Pinkal (25-APR-2012) -- End



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    'Sandeep [ 21 Aug 2010 ] -- Start
            '    'If CInt(cboApprovedBy.SelectedValue) <= 0 Then
            '    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
            '    '    cboApprovedBy.Focus()
            '    '    Return False
            '    'End If

            '    Select Case CInt(cboStatus.SelectedValue)
            '        Case 2, 3
            '            If CInt(cboApprovedBy.SelectedValue) <= 0 Then
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
            '                cboApprovedBy.Focus()
            '                Return False
            '            End If
            '        Case Else
            '            cboApprovedBy.SelectedValue = 0
            '    End Select
            '    'Sandeep [ 21 Aug 2010 ] -- End 
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END




            If CInt(cboApprovedBy.SelectedValue) = CInt(cboEmpName.SelectedValue) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Approver cannot approve Loan/Advance to him/herself. Please select different Approver/Employee."), enMsgBoxStyle.Information)
                cboApprovedBy.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Status is compulsory information. Please select Status to continue."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If

            If txtLoanAmt.Decimal < txtApprovedAmt.Decimal Then 'Sohail (11 May 2011)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Approved Amount cannot be greater than the loan amount."), enMsgBoxStyle.Information)
                txtApprovedAmt.Focus()
                Return False
            End If

            If txtApprovedAmt.Decimal <= 0 Then 'Sohail (11 May 2011)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Approved Amount must greater than zero."), enMsgBoxStyle.Information)
                txtApprovedAmt.Focus()
                Return False
            End If



            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            'If txtApplicationNo.Text = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Application No cannot be blank. Application No is compulsory information."), enMsgBoxStyle.Information)
            '    txtApplicationNo.Focus()
            '    Return False
            'End If
            If ConfigParameter._Object._LoanApplicationNoType = 0 Then
                If txtApplicationNo.Text = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Application No cannot be blank. Application No is compulsory information."), enMsgBoxStyle.Information)
                    txtApplicationNo.Focus()
                    Return False
                End If
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            If chkExternalEntity.CheckState = CheckState.Checked Then
                If txtExternalEntity.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "External Entity cannot be blank. External Entity is compulsory information."), enMsgBoxStyle.Information)
                    txtExternalEntity.Focus()
                    Return False
                End If
            End If


            'Anjan (20 May 2011)-Start
            If dtpApplicationDate.Value.Date > FinancialYear._Object._Database_End_Date Or dtpApplicationDate.Value.Date < FinancialYear._Object._Database_Start_Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Application date should be between current financial year."), enMsgBoxStyle.Information)
                dtpApplicationDate.Focus()
                Return False
            End If
            'Anjan (20 May 2011)-End 


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    Private Sub SetVisibility()
        Try

            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            If ConfigParameter._Object._LoanApplicationNoType = 1 Then
                txtApplicationNo.Enabled = False
                txtApplicationNo.Text = ConfigParameter._Object._LoanApplicationPrifix & ConfigParameter._Object._NextLoanApplicationNo
            Else
                txtApplicationNo.Enabled = True
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            If mintProcessPendingLoanUnkid = -1 Then
                'Anjan (04 Apr 2011)-Start
                'Issue : Rutta wanted to add any status while making new loan application form.
                'cboStatus.Enabled = False
                'cboApprovedBy.Enabled = False
                'objbtnSearchLoanApprover.Enabled = False
                'Anjan (04 Apr 2011)-End
            Else
                cboStatus.Enabled = True
                'S.SANDEEP [ 13 JUNE 2011 ] -- START
                'ISSUE : EMPLOYEE & LOAN PRIVILEGE
                'cboApprovedBy.Enabled = True
                'objbtnSearchLoanApprover.Enabled = True
                'S.SANDEEP [ 13 JUNE 2011 ] -- END
                objbtnAddLoanScheme.Enabled = User._Object.Privilege._AddLoanScheme
            End If

            'S.SANDEEP [ 13 JUNE 2011 ] -- START
            'ISSUE : EMPLOYEE & LOAN PRIVILEGE
            If radLoan.Checked = True Then
                cboApprovedBy.Enabled = User._Object.Privilege._AllowtoApproveLoan
                objbtnSearchLoanApprover.Enabled = User._Object.Privilege._AllowtoApproveLoan
            ElseIf radAdvance.Checked = True Then
                cboApprovedBy.Enabled = User._Object.Privilege._AllowtoApproveAdvance
                objbtnSearchLoanApprover.Enabled = User._Object.Privilege._AllowtoApproveAdvance
            End If

            If cboApprovedBy.Enabled = False Then
                cboStatus.SelectedValue = 1
                cboStatus.Enabled = False
            End If
            'S.SANDEEP [ 13 JUNE 2011 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function IsAmount_Changed(ByVal decNewAmount As Decimal) As Boolean
        Try
            If mdecOldAmount <> decNewAmount Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmProcessPendingLoan_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objProcesspendingloan = Nothing
    End Sub

    Private Sub frmProcessPendingLoan_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control = True And e.KeyCode = Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmProcessPendingLoan_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmProcessPendingLoan_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProcesspendingloan = New clsProcess_pending_loan

        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetColor()
            Call FillCombo()
            Call SetVisibility()

            'Sandeep | 04 JAN 2010 | -- Start
            'Issue : to be checked when inserting only as loan process are c/f , so date's not to be checked for previous year on edit.
            If menAction <> enAction.EDIT_ONE Then
                dtpApplicationDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                If ConfigParameter._Object._CurrentDateAndTime.Date > FinancialYear._Object._Database_End_Date.Date Then
                    dtpApplicationDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                End If
            End If
            'Sandeep | 04 JAN 2010 | -- END 

            cboEmpName.Focus()

            If menAction = enAction.EDIT_ONE Then
                objProcesspendingloan._Processpendingloanunkid = mintProcessPendingLoanUnkid
                If objProcesspendingloan._Loan_Statusunkid = 2 Then
                    radLoan.Enabled = False
                    radAdvance.Enabled = False
                End If
            End If

            Call GetValue()
            cboEmpName.Focus()

            Call chkExternalEntity_CheckedChanged(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessPendingLoan_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim btnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            'If mintProcessPendingLoanUnkid > 0 Then
            '    If objProcesspendingloan.Get_Id_Used_In_LoanAdvance(mintProcessPendingLoanUnkid) = True Then
            '        If IsAmount_Changed(txtApprovedAmt.Decimal) = True Then
            '            Dim objLoanAdvance As New clsLoan_Advance
            '            Call objProcesspendingloan.Get_LoanAdvanceunkid(mintProcessPendingLoanUnkid, mintLoanAdvaceunkid)
            '            objLoanAdvance._Loanadvancetranunkid = mintLoanAdvaceunkid

            '            If objLoanAdvance._Isloan = True Then
            '                Select Case objLoanAdvance._Calctype_Id
            '                    Case 1
            '                        mdblInterestAmount = (txtApprovedAmt.Decimal * objLoanAdvance._Interest_Rate * (objLoanAdvance._Loan_Duration / 12)) / 100
            '                    Case 2
            '                        mdblInterestAmount = txtApprovedAmt.Decimal * Pow((1 + (objLoanAdvance._Interest_Rate / 100)), (objLoanAdvance._Loan_Duration / 12))
            '                    Case 3
            '                End Select
            '                mdblNetAmount = mdblInterestAmount + txtApprovedAmt.Decimal
            '                objProcesspendingloan._Loan_Amount = txtApprovedAmt.Decimal
            '                objProcesspendingloan._Interest_Amount = mdblInterestAmount
            '                objProcesspendingloan._Net_Amount = mdblNetAmount
            '                objProcesspendingloan._IsAdvance = False
            '                objProcesspendingloan._LoanAdvanceId = mintLoanAdvaceunkid
            '            Else
            '                objLoanAdvance._Advance_Amount = txtApprovedAmt.Decimal
            '                objProcesspendingloan._Loan_Amount = 0
            '                objProcesspendingloan._Interest_Amount = 0
            '                objProcesspendingloan._Net_Amount = 0
            '                objProcesspendingloan._IsAdvance = True
            '                objProcesspendingloan._LoanAdvanceId = mintLoanAdvaceunkid
            '            End If
            '        End If
            '    End If

            'End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                btnFlag = objProcesspendingloan.Update()
            Else
                btnFlag = objProcesspendingloan.Insert()
            End If

            If btnFlag = False And objProcesspendingloan._Message <> "" Then
                eZeeMsgBox.Show(objProcesspendingloan._Message, enMsgBoxStyle.Information)
            End If

            If btnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objProcesspendingloan = Nothing
                    objProcesspendingloan = New clsProcess_pending_loan
                    Call GetValue()
                    cboEmpName.Focus()
                Else
                    mintProcessPendingLoanUnkid = objProcesspendingloan._Processpendingloanunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Controls"
    Private Sub txtLoanAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
        txtApprovedAmt.Text = txtLoanAmt.Text
    End Sub

    Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        If radLoan.Checked Then
            cboLoanScheme.Enabled = True
            objbtnAddLoanScheme.Enabled = True
        Else
            cboLoanScheme.Enabled = False
            objbtnAddLoanScheme.Enabled = False
            cboLoanScheme.SelectedValue = 0
        End If
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            With objFrm
                .ValueMember = cboEmpName.ValueMember
                .DisplayMember = cboEmpName.DisplayMember
                .DataSource = CType(cboEmpName.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmpName.SelectedValue = objFrm.SelectedValue
                cboEmpName.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddLoanScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLoanScheme.Click
        Dim frm As New frmLoanScheme_AddEdit
        Dim intLoan_SchemeId As Integer = -1
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(intLoan_SchemeId, enAction.ADD_ONE) Then
                Dim dsCombo As New DataSet
                Dim objLoan As New clsLoan_Scheme
                dsCombo = objLoan.getComboList(True, "LoanScheme")
                With cboLoanScheme
                    .ValueMember = "loanschemeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("LoanScheme")
                    .SelectedValue = intLoan_SchemeId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLoanScheme_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchLoanApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLoanApprover.Click
        Dim objFrm As New frmCommonSearch
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            With objFrm
                .ValueMember = cboApprovedBy.ValueMember
                .DisplayMember = cboApprovedBy.DisplayMember
                .DataSource = CType(cboApprovedBy.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboApprovedBy.SelectedValue = objFrm.SelectedValue
                cboApprovedBy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLoanApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged

        Select Case CInt(cboStatus.SelectedValue)
            Case 1
                txtApprovedAmt.ReadOnly = True
                'Anjan (04 Apr 2011)-Start
                'txtApprovedAmt.Text = "0"
                'Anjan (04 Apr 2011)-End
                txtRemarks.Enabled = False
                txtRemarks.Text = ""
                cboApprovedBy.SelectedValue = 0
            Case 2
                txtApprovedAmt.ReadOnly = False
                txtRemarks.Enabled = False
                txtRemarks.Text = ""
            Case 3
                txtApprovedAmt.ReadOnly = True
                txtRemarks.Enabled = True
        End Select



    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub chkExternalEntity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExternalEntity.CheckedChanged
        Try
            If chkExternalEntity.CheckState = CheckState.Checked Then
                txtExternalEntity.Enabled = True
            Else
                txtExternalEntity.Text = ""
                txtExternalEntity.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExternalEntity_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 

    'Anjan (30 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub objLoanSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLoanSchemeSearch.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboLoanScheme.ValueMember
                .DisplayMember = cboLoanScheme.DisplayMember
                .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objFrm.DisplayDialog Then
                cboLoanScheme.SelectedValue = objFrm.SelectedValue
                cboLoanScheme.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objLoanSchemeSearch_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (30 Jan 2012)-End 
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbProcessPendingLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbProcessPendingLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbProcessPendingLoanInfo.Text = Language._Object.getCaption(Me.gbProcessPendingLoanInfo.Name, Me.gbProcessPendingLoanInfo.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
			Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
			Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblAmt.Text = Language._Object.getCaption(Me.lblAmt.Name, Me.lblAmt.Text)
			Me.lblApprovedBy.Text = Language._Object.getCaption(Me.lblApprovedBy.Name, Me.lblApprovedBy.Text)
			Me.lblApprovedAmount.Text = Language._Object.getCaption(Me.lblApprovedAmount.Name, Me.lblApprovedAmount.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.lblApplicationDate.Text = Language._Object.getCaption(Me.lblApplicationDate.Name, Me.lblApplicationDate.Text)
			Me.lblApplicationNo.Text = Language._Object.getCaption(Me.lblApplicationNo.Name, Me.lblApplicationNo.Text)
			Me.chkExternalEntity.Text = Language._Object.getCaption(Me.chkExternalEntity.Name, Me.chkExternalEntity.Text)
			Me.lblEmpRemark.Text = Language._Object.getCaption(Me.lblEmpRemark.Name, Me.lblEmpRemark.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
			Language.setMessage(mstrModuleName, 3, "Approver is compulsory information. Please select Approver to continue.")
			Language.setMessage(mstrModuleName, 4, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 5, "Approver cannot approve Loan/Advance to him/herself. Please select different Approver/Employee.")
			Language.setMessage(mstrModuleName, 6, "Status is compulsory information. Please select Status to continue.")
			Language.setMessage(mstrModuleName, 7, "Approved Amount cannot be greater than the loan amount.")
			Language.setMessage(mstrModuleName, 8, "Approved Amount must greater than zero.")
			Language.setMessage(mstrModuleName, 9, "Application No cannot be blank. Application No is compulsory information.")
			Language.setMessage(mstrModuleName, 10, "External Entity cannot be blank. External Entity is compulsory information.")
			Language.setMessage(mstrModuleName, 11, "Application date should be between current financial year.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class