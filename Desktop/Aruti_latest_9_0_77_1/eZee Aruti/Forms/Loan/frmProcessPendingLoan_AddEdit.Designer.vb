﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProcessPendingLoan_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProcessPendingLoan_AddEdit))
        Me.gbProcessPendingLoanInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objLoanSchemeSearch = New eZee.Common.eZeeGradientButton
        Me.txtExternalEntity = New eZee.TextBox.AlphanumericTextBox
        Me.chkExternalEntity = New System.Windows.Forms.CheckBox
        Me.dtpApplicationDate = New System.Windows.Forms.DateTimePicker
        Me.txtApplicationNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblApplicationDate = New System.Windows.Forms.Label
        Me.lblApplicationNo = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtApprovedAmt = New eZee.TextBox.NumericTextBox
        Me.objbtnSearchLoanApprover = New eZee.Common.eZeeGradientButton
        Me.cboApprovedBy = New System.Windows.Forms.ComboBox
        Me.lblApprovedBy = New System.Windows.Forms.Label
        Me.lblAmt = New System.Windows.Forms.Label
        Me.txtLoanAmt = New eZee.TextBox.NumericTextBox
        Me.objbtnAddLoanScheme = New eZee.Common.eZeeGradientButton
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.cboEmpName = New System.Windows.Forms.ComboBox
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblApprovedAmount = New System.Windows.Forms.Label
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtEmployeeRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmpRemark = New System.Windows.Forms.Label
        Me.gbProcessPendingLoanInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbProcessPendingLoanInfo
        '
        Me.gbProcessPendingLoanInfo.BorderColor = System.Drawing.Color.Black
        Me.gbProcessPendingLoanInfo.Checked = False
        Me.gbProcessPendingLoanInfo.CollapseAllExceptThis = False
        Me.gbProcessPendingLoanInfo.CollapsedHoverImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapsedNormalImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapsedPressedImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapseOnLoad = False
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblEmpRemark)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtEmployeeRemark)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objLoanSchemeSearch)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtExternalEntity)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.chkExternalEntity)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.dtpApplicationDate)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtApplicationNo)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApplicationDate)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApplicationNo)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtRemarks)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboStatus)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblStatus)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtApprovedAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objbtnSearchLoanApprover)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboApprovedBy)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApprovedBy)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtLoanAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objbtnAddLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.radAdvance)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.radLoan)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblLoanAdvance)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboEmpName)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblEmpName)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApprovedAmount)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblRemarks)
        Me.gbProcessPendingLoanInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbProcessPendingLoanInfo.ExpandedHoverImage = Nothing
        Me.gbProcessPendingLoanInfo.ExpandedNormalImage = Nothing
        Me.gbProcessPendingLoanInfo.ExpandedPressedImage = Nothing
        Me.gbProcessPendingLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProcessPendingLoanInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProcessPendingLoanInfo.HeaderHeight = 25
        Me.gbProcessPendingLoanInfo.HeaderMessage = ""
        Me.gbProcessPendingLoanInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbProcessPendingLoanInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProcessPendingLoanInfo.HeightOnCollapse = 0
        Me.gbProcessPendingLoanInfo.LeftTextSpace = 0
        Me.gbProcessPendingLoanInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbProcessPendingLoanInfo.Name = "gbProcessPendingLoanInfo"
        Me.gbProcessPendingLoanInfo.OpenHeight = 300
        Me.gbProcessPendingLoanInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProcessPendingLoanInfo.ShowBorder = True
        Me.gbProcessPendingLoanInfo.ShowCheckBox = False
        Me.gbProcessPendingLoanInfo.ShowCollapseButton = False
        Me.gbProcessPendingLoanInfo.ShowDefaultBorderColor = True
        Me.gbProcessPendingLoanInfo.ShowDownButton = False
        Me.gbProcessPendingLoanInfo.ShowHeader = True
        Me.gbProcessPendingLoanInfo.Size = New System.Drawing.Size(474, 409)
        Me.gbProcessPendingLoanInfo.TabIndex = 1
        Me.gbProcessPendingLoanInfo.Temp = 0
        Me.gbProcessPendingLoanInfo.Text = "Process Pending Loan Info"
        Me.gbProcessPendingLoanInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLoanSchemeSearch
        '
        Me.objLoanSchemeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objLoanSchemeSearch.BorderSelected = False
        Me.objLoanSchemeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objLoanSchemeSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objLoanSchemeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objLoanSchemeSearch.Location = New System.Drawing.Point(417, 137)
        Me.objLoanSchemeSearch.Name = "objLoanSchemeSearch"
        Me.objLoanSchemeSearch.Size = New System.Drawing.Size(21, 21)
        Me.objLoanSchemeSearch.TabIndex = 24
        '
        'txtExternalEntity
        '
        Me.txtExternalEntity.Flags = 0
        Me.txtExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExternalEntity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtExternalEntity.Location = New System.Drawing.Point(127, 110)
        Me.txtExternalEntity.Name = "txtExternalEntity"
        Me.txtExternalEntity.Size = New System.Drawing.Size(284, 21)
        Me.txtExternalEntity.TabIndex = 10
        '
        'chkExternalEntity
        '
        Me.chkExternalEntity.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExternalEntity.Location = New System.Drawing.Point(8, 112)
        Me.chkExternalEntity.Name = "chkExternalEntity"
        Me.chkExternalEntity.Size = New System.Drawing.Size(112, 17)
        Me.chkExternalEntity.TabIndex = 9
        Me.chkExternalEntity.Text = "Is External Entity"
        Me.chkExternalEntity.UseVisualStyleBackColor = True
        '
        'dtpApplicationDate
        '
        Me.dtpApplicationDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplicationDate.Checked = False
        Me.dtpApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplicationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplicationDate.Location = New System.Drawing.Point(315, 33)
        Me.dtpApplicationDate.Name = "dtpApplicationDate"
        Me.dtpApplicationDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpApplicationDate.TabIndex = 3
        '
        'txtApplicationNo
        '
        Me.txtApplicationNo.Flags = 0
        Me.txtApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicationNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicationNo.Location = New System.Drawing.Point(99, 33)
        Me.txtApplicationNo.Name = "txtApplicationNo"
        Me.txtApplicationNo.Size = New System.Drawing.Size(107, 21)
        Me.txtApplicationNo.TabIndex = 1
        '
        'lblApplicationDate
        '
        Me.lblApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationDate.Location = New System.Drawing.Point(257, 37)
        Me.lblApplicationDate.Name = "lblApplicationDate"
        Me.lblApplicationDate.Size = New System.Drawing.Size(52, 15)
        Me.lblApplicationDate.TabIndex = 2
        Me.lblApplicationDate.Text = "Date"
        '
        'lblApplicationNo
        '
        Me.lblApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationNo.Location = New System.Drawing.Point(8, 37)
        Me.lblApplicationNo.Name = "lblApplicationNo"
        Me.lblApplicationNo.Size = New System.Drawing.Size(85, 15)
        Me.lblApplicationNo.TabIndex = 0
        Me.lblApplicationNo.Text = "Application No."
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemarks.Location = New System.Drawing.Point(99, 300)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemarks.Size = New System.Drawing.Size(312, 49)
        Me.txtRemarks.TabIndex = 22
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(99, 273)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(107, 21)
        Me.cboStatus.TabIndex = 18
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 276)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(85, 15)
        Me.lblStatus.TabIndex = 17
        Me.lblStatus.Text = "Loan Status"
        '
        'txtApprovedAmt
        '
        Me.txtApprovedAmt.AllowNegative = True
        Me.txtApprovedAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtApprovedAmt.DigitsInGroup = 0
        Me.txtApprovedAmt.Flags = 0
        Me.txtApprovedAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprovedAmt.Location = New System.Drawing.Point(315, 273)
        Me.txtApprovedAmt.MaxDecimalPlaces = 6
        Me.txtApprovedAmt.MaxWholeDigits = 21
        Me.txtApprovedAmt.Name = "txtApprovedAmt"
        Me.txtApprovedAmt.Prefix = ""
        Me.txtApprovedAmt.RangeMax = 1.7976931348623157E+308
        Me.txtApprovedAmt.RangeMin = -1.7976931348623157E+308
        Me.txtApprovedAmt.ReadOnly = True
        Me.txtApprovedAmt.Size = New System.Drawing.Size(96, 21)
        Me.txtApprovedAmt.TabIndex = 20
        Me.txtApprovedAmt.Text = "0"
        Me.txtApprovedAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchLoanApprover
        '
        Me.objbtnSearchLoanApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLoanApprover.BorderSelected = False
        Me.objbtnSearchLoanApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLoanApprover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLoanApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLoanApprover.Location = New System.Drawing.Point(417, 246)
        Me.objbtnSearchLoanApprover.Name = "objbtnSearchLoanApprover"
        Me.objbtnSearchLoanApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLoanApprover.TabIndex = 26
        '
        'cboApprovedBy
        '
        Me.cboApprovedBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprovedBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprovedBy.FormattingEnabled = True
        Me.cboApprovedBy.Items.AddRange(New Object() {"Select"})
        Me.cboApprovedBy.Location = New System.Drawing.Point(99, 246)
        Me.cboApprovedBy.Name = "cboApprovedBy"
        Me.cboApprovedBy.Size = New System.Drawing.Size(312, 21)
        Me.cboApprovedBy.TabIndex = 16
        '
        'lblApprovedBy
        '
        Me.lblApprovedBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedBy.Location = New System.Drawing.Point(8, 249)
        Me.lblApprovedBy.Name = "lblApprovedBy"
        Me.lblApprovedBy.Size = New System.Drawing.Size(85, 15)
        Me.lblApprovedBy.TabIndex = 15
        Me.lblApprovedBy.Text = "Approved By"
        '
        'lblAmt
        '
        Me.lblAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmt.Location = New System.Drawing.Point(8, 167)
        Me.lblAmt.Name = "lblAmt"
        Me.lblAmt.Size = New System.Drawing.Size(85, 15)
        Me.lblAmt.TabIndex = 13
        Me.lblAmt.Text = "Amount"
        Me.lblAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanAmt
        '
        Me.txtLoanAmt.AllowNegative = True
        Me.txtLoanAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmt.DigitsInGroup = 0
        Me.txtLoanAmt.Flags = 0
        Me.txtLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmt.Location = New System.Drawing.Point(99, 164)
        Me.txtLoanAmt.MaxDecimalPlaces = 6
        Me.txtLoanAmt.MaxWholeDigits = 21
        Me.txtLoanAmt.Name = "txtLoanAmt"
        Me.txtLoanAmt.Prefix = ""
        Me.txtLoanAmt.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmt.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmt.Size = New System.Drawing.Size(107, 21)
        Me.txtLoanAmt.TabIndex = 14
        Me.txtLoanAmt.Text = "0"
        Me.txtLoanAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnAddLoanScheme
        '
        Me.objbtnAddLoanScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLoanScheme.BorderSelected = False
        Me.objbtnAddLoanScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLoanScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLoanScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLoanScheme.Location = New System.Drawing.Point(444, 137)
        Me.objbtnAddLoanScheme.Name = "objbtnAddLoanScheme"
        Me.objbtnAddLoanScheme.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLoanScheme.TabIndex = 25
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Items.AddRange(New Object() {"Select"})
        Me.cboLoanScheme.Location = New System.Drawing.Point(99, 137)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(312, 21)
        Me.cboLoanScheme.TabIndex = 12
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 140)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(85, 15)
        Me.lblLoanScheme.TabIndex = 11
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'radAdvance
        '
        Me.radAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdvance.Location = New System.Drawing.Point(201, 87)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(95, 17)
        Me.radAdvance.TabIndex = 8
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = True
        '
        'radLoan
        '
        Me.radLoan.Checked = True
        Me.radLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLoan.Location = New System.Drawing.Point(100, 87)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(95, 17)
        Me.radLoan.TabIndex = 7
        Me.radLoan.TabStop = True
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = True
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(8, 87)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(85, 15)
        Me.lblLoanAdvance.TabIndex = 6
        Me.lblLoanAdvance.Text = "Select"
        '
        'cboEmpName
        '
        Me.cboEmpName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpName.FormattingEnabled = True
        Me.cboEmpName.Items.AddRange(New Object() {"Select"})
        Me.cboEmpName.Location = New System.Drawing.Point(99, 60)
        Me.cboEmpName.Name = "cboEmpName"
        Me.cboEmpName.Size = New System.Drawing.Size(312, 21)
        Me.cboEmpName.TabIndex = 5
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 63)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(85, 15)
        Me.lblEmpName.TabIndex = 4
        Me.lblEmpName.Text = "Employee"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(417, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 23
        '
        'lblApprovedAmount
        '
        Me.lblApprovedAmount.BackColor = System.Drawing.Color.Transparent
        Me.lblApprovedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedAmount.Location = New System.Drawing.Point(212, 276)
        Me.lblApprovedAmount.Name = "lblApprovedAmount"
        Me.lblApprovedAmount.Size = New System.Drawing.Size(97, 15)
        Me.lblApprovedAmount.TabIndex = 19
        Me.lblApprovedAmount.Text = "Approved Amt."
        Me.lblApprovedAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(8, 304)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(85, 15)
        Me.lblRemarks.TabIndex = 21
        Me.lblRemarks.Text = "Remarks"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 354)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(474, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(262, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(365, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtEmployeeRemark
        '
        Me.txtEmployeeRemark.Flags = 0
        Me.txtEmployeeRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtEmployeeRemark.Location = New System.Drawing.Point(99, 191)
        Me.txtEmployeeRemark.Multiline = True
        Me.txtEmployeeRemark.Name = "txtEmployeeRemark"
        Me.txtEmployeeRemark.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtEmployeeRemark.Size = New System.Drawing.Size(312, 49)
        Me.txtEmployeeRemark.TabIndex = 28
        '
        'lblEmpRemark
        '
        Me.lblEmpRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpRemark.Location = New System.Drawing.Point(8, 194)
        Me.lblEmpRemark.Name = "lblEmpRemark"
        Me.lblEmpRemark.Size = New System.Drawing.Size(85, 26)
        Me.lblEmpRemark.TabIndex = 29
        Me.lblEmpRemark.Text = "Employee Remarks"
        '
        'frmProcessPendingLoan_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(474, 409)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbProcessPendingLoanInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProcessPendingLoan_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add / Edit Process Pending Loan"
        Me.gbProcessPendingLoanInfo.ResumeLayout(False)
        Me.gbProcessPendingLoanInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbProcessPendingLoanInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmpName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents objbtnAddLoanScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents lblAmt As System.Windows.Forms.Label
    Friend WithEvents txtLoanAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchLoanApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApprovedBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovedBy As System.Windows.Forms.Label
    Friend WithEvents lblApprovedAmount As System.Windows.Forms.Label
    Friend WithEvents txtApprovedAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpApplicationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtApplicationNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApplicationDate As System.Windows.Forms.Label
    Friend WithEvents lblApplicationNo As System.Windows.Forms.Label
    Friend WithEvents chkExternalEntity As System.Windows.Forms.CheckBox
    Friend WithEvents txtExternalEntity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objLoanSchemeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmpRemark As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeRemark As eZee.TextBox.AlphanumericTextBox
End Class
