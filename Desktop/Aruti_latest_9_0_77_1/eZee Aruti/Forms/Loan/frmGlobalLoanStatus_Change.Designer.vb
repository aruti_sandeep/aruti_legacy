﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalLoanStatus_Change
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalLoanStatus_Change))
        Me.cboLoanAdvance = New System.Windows.Forms.ComboBox
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker
        Me.lblFrmDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblToDate = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbOperation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.cboOperation = New System.Windows.Forms.ComboBox
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblOperation = New System.Windows.Forms.Label
        Me.lvLoanAdvance = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhVocNo = New System.Windows.Forms.ColumnHeader
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhLoanScheme = New System.Windows.Forms.ColumnHeader
        Me.colhLoan_Advance = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsLoan = New System.Windows.Forms.ColumnHeader
        Me.objcolhemployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboLoanAdvance
        '
        Me.cboLoanAdvance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAdvance.DropDownWidth = 300
        Me.cboLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAdvance.FormattingEnabled = True
        Me.cboLoanAdvance.Location = New System.Drawing.Point(99, 34)
        Me.cboLoanAdvance.Name = "cboLoanAdvance"
        Me.cboLoanAdvance.Size = New System.Drawing.Size(151, 21)
        Me.cboLoanAdvance.TabIndex = 17
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(8, 36)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(85, 16)
        Me.lblLoanAdvance.TabIndex = 16
        Me.lblLoanAdvance.Text = "Loan/Advance"
        Me.lblLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 300
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(99, 88)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(151, 21)
        Me.cboStatus.TabIndex = 19
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 90)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(85, 16)
        Me.lblStatus.TabIndex = 18
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(85, 16)
        Me.lblLoanScheme.TabIndex = 14
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.DropDownWidth = 300
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(99, 61)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(151, 21)
        Me.cboLoanScheme.TabIndex = 15
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.DropDownWidth = 300
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(374, 34)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(115, 21)
        Me.cboPayPeriod.TabIndex = 21
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(283, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(85, 16)
        Me.lblPayPeriod.TabIndex = 20
        Me.lblPayPeriod.Text = "Assigned Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateFrom.Checked = False
        Me.dtpDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateFrom.Location = New System.Drawing.Point(374, 61)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.ShowCheckBox = True
        Me.dtpDateFrom.Size = New System.Drawing.Size(115, 21)
        Me.dtpDateFrom.TabIndex = 22
        '
        'lblFrmDate
        '
        Me.lblFrmDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFrmDate.Location = New System.Drawing.Point(283, 64)
        Me.lblFrmDate.Name = "lblFrmDate"
        Me.lblFrmDate.Size = New System.Drawing.Size(85, 16)
        Me.lblFrmDate.TabIndex = 23
        Me.lblFrmDate.Text = "Date From"
        '
        'dtpToDate
        '
        Me.dtpToDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(374, 88)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(115, 21)
        Me.dtpToDate.TabIndex = 24
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(283, 91)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(85, 16)
        Me.lblToDate.TabIndex = 25
        Me.lblToDate.Text = "Date To"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.lblFrmDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanScheme)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(499, 120)
        Me.gbFilterCriteria.TabIndex = 26
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(256, 61)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 94
        Me.objbtnSearchEmployee.Visible = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(474, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(449, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 414)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(834, 55)
        Me.objFooter.TabIndex = 27
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(622, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(725, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbOperation
        '
        Me.gbOperation.BorderColor = System.Drawing.Color.Black
        Me.gbOperation.Checked = False
        Me.gbOperation.CollapseAllExceptThis = False
        Me.gbOperation.CollapsedHoverImage = Nothing
        Me.gbOperation.CollapsedNormalImage = Nothing
        Me.gbOperation.CollapsedPressedImage = Nothing
        Me.gbOperation.CollapseOnLoad = False
        Me.gbOperation.Controls.Add(Me.lblRemarks)
        Me.gbOperation.Controls.Add(Me.cboOperation)
        Me.gbOperation.Controls.Add(Me.txtRemarks)
        Me.gbOperation.Controls.Add(Me.lblOperation)
        Me.gbOperation.ExpandedHoverImage = Nothing
        Me.gbOperation.ExpandedNormalImage = Nothing
        Me.gbOperation.ExpandedPressedImage = Nothing
        Me.gbOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOperation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOperation.HeaderHeight = 25
        Me.gbOperation.HeaderMessage = ""
        Me.gbOperation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOperation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOperation.HeightOnCollapse = 0
        Me.gbOperation.LeftTextSpace = 0
        Me.gbOperation.Location = New System.Drawing.Point(502, 0)
        Me.gbOperation.Name = "gbOperation"
        Me.gbOperation.OpenHeight = 300
        Me.gbOperation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOperation.ShowBorder = True
        Me.gbOperation.ShowCheckBox = False
        Me.gbOperation.ShowCollapseButton = False
        Me.gbOperation.ShowDefaultBorderColor = True
        Me.gbOperation.ShowDownButton = False
        Me.gbOperation.ShowHeader = True
        Me.gbOperation.Size = New System.Drawing.Size(332, 120)
        Me.gbOperation.TabIndex = 28
        Me.gbOperation.Temp = 0
        Me.gbOperation.Text = "Operation"
        Me.gbOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(11, 66)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(84, 16)
        Me.lblRemarks.TabIndex = 239
        Me.lblRemarks.Text = "Remark"
        '
        'cboOperation
        '
        Me.cboOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOperation.DropDownWidth = 300
        Me.cboOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOperation.FormattingEnabled = True
        Me.cboOperation.Location = New System.Drawing.Point(101, 34)
        Me.cboOperation.Name = "cboOperation"
        Me.cboOperation.Size = New System.Drawing.Size(219, 21)
        Me.cboOperation.TabIndex = 72
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemarks.Location = New System.Drawing.Point(101, 61)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemarks.Size = New System.Drawing.Size(219, 48)
        Me.txtRemarks.TabIndex = 31
        '
        'lblOperation
        '
        Me.lblOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperation.Location = New System.Drawing.Point(11, 36)
        Me.lblOperation.Name = "lblOperation"
        Me.lblOperation.Size = New System.Drawing.Size(84, 16)
        Me.lblOperation.TabIndex = 71
        Me.lblOperation.Text = "Change Status"
        Me.lblOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvLoanAdvance
        '
        Me.lvLoanAdvance.BackColorOnChecked = False
        Me.lvLoanAdvance.CheckBoxes = True
        Me.lvLoanAdvance.ColumnHeaders = Nothing
        Me.lvLoanAdvance.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhVocNo, Me.colhEmpCode, Me.colhEmployee, Me.colhDate, Me.colhLoanScheme, Me.colhLoan_Advance, Me.colhPeriod, Me.objcolhIsLoan, Me.objcolhemployeeunkid})
        Me.lvLoanAdvance.CompulsoryColumns = ""
        Me.lvLoanAdvance.FullRowSelect = True
        Me.lvLoanAdvance.GridLines = True
        Me.lvLoanAdvance.GroupingColumn = Nothing
        Me.lvLoanAdvance.HideSelection = False
        Me.lvLoanAdvance.Location = New System.Drawing.Point(0, 121)
        Me.lvLoanAdvance.MinColumnWidth = 50
        Me.lvLoanAdvance.MultiSelect = False
        Me.lvLoanAdvance.Name = "lvLoanAdvance"
        Me.lvLoanAdvance.OptionalColumns = ""
        Me.lvLoanAdvance.ShowMoreItem = False
        Me.lvLoanAdvance.ShowSaveItem = False
        Me.lvLoanAdvance.ShowSelectAll = True
        Me.lvLoanAdvance.ShowSizeAllColumnsToFit = True
        Me.lvLoanAdvance.Size = New System.Drawing.Size(834, 293)
        Me.lvLoanAdvance.Sortable = True
        Me.lvLoanAdvance.TabIndex = 29
        Me.lvLoanAdvance.UseCompatibleStateImageBehavior = False
        Me.lvLoanAdvance.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.objcolhCheck.Width = 25
        '
        'colhVocNo
        '
        Me.colhVocNo.Tag = "colhVocNo"
        Me.colhVocNo.Text = "Voc #"
        Me.colhVocNo.Width = 100
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Text = "Code"
        Me.colhEmpCode.Width = 90
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 220
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 90
        '
        'colhLoanScheme
        '
        Me.colhLoanScheme.Tag = "colhLoanScheme"
        Me.colhLoanScheme.Text = "Loan Scheme"
        Me.colhLoanScheme.Width = 110
        '
        'colhLoan_Advance
        '
        Me.colhLoan_Advance.Tag = "colhLoan_Advance"
        Me.colhLoan_Advance.Text = "Loan / Advance"
        Me.colhLoan_Advance.Width = 95
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 95
        '
        'objcolhIsLoan
        '
        Me.objcolhIsLoan.Tag = "objcolhIsLoan"
        Me.objcolhIsLoan.Text = ""
        Me.objcolhIsLoan.Width = 0
        '
        'objcolhemployeeunkid
        '
        Me.objcolhemployeeunkid.Tag = "objcolhemployeeunkid"
        Me.objcolhemployeeunkid.Text = ""
        Me.objcolhemployeeunkid.Width = 0
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(8, 124)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 30
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'frmGlobalLoanStatus_Change
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 469)
        Me.Controls.Add(Me.objchkAll)
        Me.Controls.Add(Me.lvLoanAdvance)
        Me.Controls.Add(Me.gbOperation)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalLoanStatus_Change"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Loan Status Change"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbOperation.ResumeLayout(False)
        Me.gbOperation.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboLoanAdvance As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFrmDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbOperation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOperation As System.Windows.Forms.ComboBox
    Friend WithEvents lblOperation As System.Windows.Forms.Label
    Friend WithEvents lvLoanAdvance As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVocNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanScheme As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoan_Advance As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsLoan As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhemployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
End Class
