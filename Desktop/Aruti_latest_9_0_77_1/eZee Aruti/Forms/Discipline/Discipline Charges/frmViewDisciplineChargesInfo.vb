﻿Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmViewDisciplineChargesInfo
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineFiling"
    Private mblnCancel As Boolean = True
    Private mintDisciplineFileUnkid As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private objDiscChargeMaster As clsDiscipline_file_master
    Private objDiscChargeTran As clsDiscipline_file_tran
    Private mdtCharges As DataTable
    Private mintSeverity As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intEmpUnkId As Integer, Optional ByRef intUnkid As Integer = -1) As Boolean
        Try
            mintEmployeeunkid = intEmpUnkId

            Me.ShowDialog()

            If mblnCancel = False Then
                intUnkid = mintDisciplineFileUnkid
            End If

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineFiling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDiscChargeMaster = New clsDiscipline_file_master
        objDiscChargeTran = New clsDiscipline_file_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()
            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFiling_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmDisciplineAnalysis_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDiscChargeMaster = Nothing
        objDiscChargeTran = Nothing
    End Sub

    'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        clsDiscipline_file_master.SetMessages()
    '        objfrm._Other_ModuleNames = "clsDiscipline_file_master"
    '        objfrm.displayDialog(Me)

    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objDiscFileMaster As New clsDiscipline_file_master
        Try
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
            End With


            'Gajanan [02-June-2020] -- Start
            'dsCombo = objCommon.getDesciplineRefNoComboList(mintEmployeeunkid, "refnoList")
            dsCombo = objDiscFileMaster.GetComboList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._IsIncludeInactiveEmp, "refnoList", , , mintEmployeeunkid)
            'Gajanan [02-June-2020] -- End


            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = dsCombo.Tables("refnoList")
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objEmp = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objDiscChargeMaster._Disciplinefileunkid = cboReferenceNo.SelectedValue

            If objDiscChargeMaster._Chargedate <> Nothing Then
                txtchargeDate.Text = objDiscChargeMaster._Chargedate.ToShortDateString()
            End If

            If objDiscChargeMaster._Interdictiondate <> Nothing Then
                txtinterdictionDate.Text = objDiscChargeMaster._Interdictiondate.ToShortDateString()
            End If

            txtChargeDescription.Text = objDiscChargeMaster._Charge_Description
            cboReferenceNo.Text = objDiscChargeMaster._Reference_No.ToString()
            cboEmployee.SelectedValue = objDiscChargeMaster._Involved_Employeeunkid

            objDiscChargeTran._Disciplinefileunkid = cboReferenceNo.SelectedValue
            mintDisciplineFileUnkid = cboReferenceNo.SelectedValue
            mdtCharges = objDiscChargeTran._ChargesTable
            Call FillCharges_Tran()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCharges_Tran()
        Try
            dgvData.AutoGenerateColumns = False
            dgcolhIncident.DataPropertyName = "incident_description"
            dgcolhOffCategory.DataPropertyName = "charge_category"
            dgcolhOffence.DataPropertyName = "charge_descr"
            objdgcolhoffencecategoryunkid.DataPropertyName = "offencecategoryunkid"
            objdgcolhoffenceunkid.DataPropertyName = "offenceunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            objdgcolhfileunkid.DataPropertyName = "disciplinefileunkid"
            objdgcolhfiletraunkid.DataPropertyName = "disciplinefiletranunkid"
            dgcolhResponseDate.DataPropertyName = "response_date"
            dgcolhResponseDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhResponseText.DataPropertyName = "response_remark"
            dgcolhResponseType.DataPropertyName = "response_type"

            dgvData.DataSource = mdtCharges

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCharges_Tran", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                Dim objDept As New clsDepartment
                Dim objJobs As New clsJobs
                objDept._Departmentunkid = objEmp._Departmentunkid
                objJobs._Jobunkid = objEmp._Jobunkid
                txtDepartment.Text = objDept._Name
                txtJobTitle.Text = objJobs._Job_Name
                objDept = Nothing : objJobs = Nothing : objEmp = Nothing
            Else
                txtDepartment.Text = "" : txtJobTitle.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged
        Try
            mintDisciplineFileUnkid = CInt(cboReferenceNo.SelectedValue)
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons Event(s) "
    Private Sub objbtnSearchReference_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReference.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = CType(cboReferenceNo.DataSource, DataTable)
                If .DisplayDialog Then
                    cboReferenceNo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            frm.Dispose()
        End Try
    End Sub
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClosePopup.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " DataGrid Event(s) "
    Private Sub dgvData_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgvData.RowPostPaint
        Try
            Dim rect As Rectangle = dgvData.GetCellDisplayRectangle(dgcolhCount.Index, e.RowIndex, True)
            Dim strRowNumber As String = (e.RowIndex + 1).ToString()
            Dim size As SizeF = e.Graphics.MeasureString(strRowNumber, Me.Font)
            If dgvData.RowHeadersWidth < CInt(size.Width + 20) Then
                dgvData.RowHeadersWidth = CInt(size.Width + 20)
            End If
            Dim b As Brush = SystemBrushes.ControlText
            e.Graphics.DrawString(strRowNumber, Me.Font, b, rect.Location.X + 15, rect.Location.Y + ((rect.Height - size.Height) / 2))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_RowPostPaint", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbDiscipilneFiling.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDiscipilneFiling.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnClosePopup.GradientBackColor = GUI._ButttonBackColor
            Me.btnClosePopup.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbDiscipilneFiling.Text = Language._Object.getCaption(Me.gbDiscipilneFiling.Name, Me.gbDiscipilneFiling.Text)
            Me.lblGeneralChargeDescr.Text = Language._Object.getCaption(Me.lblGeneralChargeDescr.Name, Me.lblGeneralChargeDescr.Text)
            Me.elLine1.Text = Language._Object.getCaption(Me.elLine1.Name, Me.elLine1.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.Name, Me.lblJobTitle.Text)
            Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.DataGridViewImageColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewImageColumn1.Name, Me.DataGridViewImageColumn1.HeaderText)
            Me.DataGridViewImageColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewImageColumn2.Name, Me.DataGridViewImageColumn2.HeaderText)
            Me.DataGridViewImageColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewImageColumn3.Name, Me.DataGridViewImageColumn3.HeaderText)
            Me.dgcolhCount.HeaderText = Language._Object.getCaption(Me.dgcolhCount.Name, Me.dgcolhCount.HeaderText)
            Me.dgcolhOffCategory.HeaderText = Language._Object.getCaption(Me.dgcolhOffCategory.Name, Me.dgcolhOffCategory.HeaderText)
            Me.dgcolhOffence.HeaderText = Language._Object.getCaption(Me.dgcolhOffence.Name, Me.dgcolhOffence.HeaderText)
            Me.dgcolhIncident.HeaderText = Language._Object.getCaption(Me.dgcolhIncident.Name, Me.dgcolhIncident.HeaderText)
            Me.dgcolhResponseType.HeaderText = Language._Object.getCaption(Me.dgcolhResponseType.Name, Me.dgcolhResponseType.HeaderText)
            Me.dgcolhResponseDate.HeaderText = Language._Object.getCaption(Me.dgcolhResponseDate.Name, Me.dgcolhResponseDate.HeaderText)
            Me.dgcolhResponseText.HeaderText = Language._Object.getCaption(Me.dgcolhResponseText.Name, Me.dgcolhResponseText.HeaderText)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnClosePopup.Text = Language._Object.getCaption(Me.btnClosePopup.Name, Me.btnClosePopup.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class