﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmDisciplineType_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmDisciplineType_AddEdit"
    Private mblnCancel As Boolean = True
    Private objDisciplineType As clsDisciplineType
    Private menAction As enAction = enAction.ADD_ONE
    Private mintDisciplineType As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintDisciplineType = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintDisciplineType

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            nudSeverity.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            cboOffenceCategory.BackColor = GUI.ColorComp
            'S.SANDEEP [ 20 MARCH 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objDisciplineType._Code
        txtName.Text = objDisciplineType._Name
        txtDescription.Text = objDisciplineType._Description
        nudSeverity.Value = CInt(IIf(CInt(objDisciplineType._Severity) = 0, 1, CInt(objDisciplineType._Severity)))
        'S.SANDEEP [ 20 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
        cboOffenceCategory.SelectedValue = objDisciplineType._Offencecategoryunkid
        'S.SANDEEP [ 20 MARCH 2012 ] -- END
    End Sub

    Private Sub SetValue()
        objDisciplineType._Code = txtCode.Text
        objDisciplineType._Name = txtName.Text
        objDisciplineType._Description = txtDescription.Text
        objDisciplineType._Severity = CInt(nudSeverity.Value)
        'S.SANDEEP [ 20 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
        objDisciplineType._Offencecategoryunkid = CInt(cboOffenceCategory.SelectedValue)
        'S.SANDEEP [ 20 MARCH 2012 ] -- END
    End Sub

    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "List")
            With cboOffenceCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objCMaster = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

#End Region

#Region " Form's Events "
    Private Sub frmDisciplineType_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDisciplineType = Nothing
    End Sub

    Private Sub frmDisciplineType_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineType_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineType_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineType_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineType_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDisciplineType = New clsDisciplineType
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            Call FillCombo()
            'S.SANDEEP [ 20 MARCH 2012 ] -- END


            If menAction = enAction.EDIT_ONE Then
                objDisciplineType._Disciplinetypeunkid = mintDisciplineType
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            'S.SANDEEP [ 07 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'DisplayError.Show("-1", ex.Message, "frmAirline_AddEdit_Load", mstrModuleName)
            DisplayError.Show("-1", ex.Message, "frmDisciplineType_AddEdit_Load", mstrModuleName)
            'S.SANDEEP [ 07 MAY 2012 ] -- END
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDisciplineType.SetMessages()
            objfrm._Other_ModuleNames = "clsDisciplineType"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Discipline Type cannot be blank. Discipline Type is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            If CInt(cboOffenceCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Offence Category is mandatory information. Please select Offence Category to continue."), enMsgBoxStyle.Information)
                cboOffenceCategory.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objDisciplineType._FormName = mstrModuleName
            objDisciplineType._LoginEmployeeunkid = 0
            objDisciplineType._ClientIP = getIP()
            objDisciplineType._HostName = getHostName()
            objDisciplineType._FromWeb = False
            objDisciplineType._AuditUserId = User._Object._Userunkid
objDisciplineType._CompanyUnkid = Company._Object._Companyunkid
            objDisciplineType._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objDisciplineType.Update()
            Else
                blnFlag = objDisciplineType.Insert()
            End If

            If blnFlag = False And objDisciplineType._Message <> "" Then
                eZeeMsgBox.Show(objDisciplineType._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDisciplineType = Nothing
                    objDisciplineType = New clsDisciplineType
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintDisciplineType = objDisciplineType._Disciplinetypeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call objFrm.displayDialog(txtName.Text, objDisciplineType._Name1, objDisciplineType._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboOffenceCategory.ValueMember
                .DisplayMember = cboOffenceCategory.DisplayMember
                .DataSource = CType(cboOffenceCategory.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboOffenceCategory.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAddOffenceCatagory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOffenceCatagory.Click
        Dim frm As New frmCommonMaster
        Try
            Dim intRefId As Integer = -1

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objCMaster As New clsCommon_Master
                Dim dsList As New DataSet
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "List")
                With cboOffenceCategory
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddOffenceCatagory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbAccessInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAccessInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAccessInfo.Text = Language._Object.getCaption(Me.gbAccessInfo.Name, Me.gbAccessInfo.Text)
			Me.lblAction.Text = Language._Object.getCaption(Me.lblAction.Name, Me.lblAction.Text)
			Me.lblAccCode.Text = Language._Object.getCaption(Me.lblAccCode.Name, Me.lblAccCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblSeverity.Text = Language._Object.getCaption(Me.lblSeverity.Name, Me.lblSeverity.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblOffencecategory.Text = Language._Object.getCaption(Me.lblOffencecategory.Name, Me.lblOffencecategory.Text)
			Me.btnAddOffenceCatagory.Text = Language._Object.getCaption(Me.btnAddOffenceCatagory.Name, Me.btnAddOffenceCatagory.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Discipline Type cannot be blank. Discipline Type is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Offence Category is mandatory information. Please select Offence Category to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class