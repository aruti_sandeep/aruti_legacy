Option Strict On

#Region " Imports "
Imports System
Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO
#End Region

Public Class frmLetterTemplate

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmLetterTemplate"
    Private objLetterType As clsLetterType
    Private mstrNodeTag As String = ""
    Private mintNodeId As Integer = 0
    Private strLetterData As String = ""
    Private mblnInsertbtn As Boolean = False
    Private mblnOkbtn As Boolean = False
    'S.SANDEEP [ 02 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnCancel As Boolean = True
    'S.SANDEEP [ 02 MAY 2012 ] -- END
#End Region

#Region " Property "
    Public ReadOnly Property _Letter_Content() As String
        Get
            Return strLetterData
        End Get
    End Property

    Public ReadOnly Property _Letter_Unkid() As Integer
        Get
            Return mintNodeId
        End Get
    End Property
#End Region

#Region " Display Dialog "

    Public Function displayDialog(Optional ByVal blnInsertbtn As Boolean = False, Optional ByVal blnIsOkbtn As Boolean = False) As Boolean
        Try
            mblnInsertbtn = blnInsertbtn
            mblnOkbtn = blnIsOkbtn
            Me.ShowDialog()
            'S.SANDEEP [ 02 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Return Not mblnCancel
            'S.SANDEEP [ 02 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "
    Private Sub CreateTreeView()
        Try

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowtoViewLetterTemplate = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 

            Dim dsList As New DataSet
            dsList = objLetterType.getList(clsCommon_Master.enCommonMaster.LETTER_TYPE)
            Dim intLastId As Integer = 0
            Dim intGrpName As Integer = 0
            Dim nGroupNode As New TreeNode
            Dim nGroupItemNode As New TreeNode
            Dim nCurrentNode As New TreeNode
            tvLetterType.Nodes.Clear()

            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If intLastId <> CInt(dsList.Tables(0).Rows(i).Item("lettermasterunkid")) Then
                        nGroupNode = New TreeNode
                        nGroupNode = tvLetterType.Nodes.Add(CStr(dsList.Tables(0).Rows(i).Item("lettermasterunkid")), CStr(dsList.Tables(0).Rows(i).Item("name")))
                        nGroupNode.Tag = "Group"
                        nGroupNode.ForeColor = Color.Blue
                        intLastId = CInt(dsList.Tables(0).Rows(i).Item("lettermasterunkid"))
                    End If
                    nCurrentNode = nGroupNode.Nodes.Add(CStr(dsList.Tables(0).Rows(i).Item("lettertypeunkid")), CStr(dsList.Tables(0).Rows(i).Item("lettertypename")))
                Next
            End If
            tvLetterType.ExpandAll()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateTreeView", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmLetterTemplate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Call btnCancel_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmLetterTemplate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLetterType = New clsLetterType
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Anjan (20 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            btnNew.Enabled = User._Object.Privilege._AllowAddLetterTemplate
            btnEdit.Enabled = User._Object.Privilege._AllowEditLetterTemplate
            btnDelete.Enabled = User._Object.Privilege._AllowDeleteLetterTemplate
            'Anjan (20 Jan 2012)-End 


            btnInsert.Visible = mblnInsertbtn
            btnOk.Visible = mblnOkbtn

            If mblnInsertbtn = True Or mblnOkbtn = True Then
                btnEdit.Visible = False
                btnDelete.Visible = False
            End If

            Call CreateTreeView()

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "frmLetterTemplate_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objfrm As New frmLetterType_AddEdit
        Try
            If objfrm.displayDialog(-1, enAction.ADD_CONTINUE) = True Then
                Call CreateTreeView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If mintNodeId = 0 Then
            Exit Sub
        ElseIf mstrNodeTag = "Group" Then
            Exit Sub
        End If
        Dim objfrm As New frmLetterType_AddEdit
        Try
            If objfrm.displayDialog(mintNodeId, enAction.EDIT_ONE) = True Then
                objrtbLetterPreview.Text = ""
                Call CreateTreeView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If mintNodeId = 0 Then
            Exit Sub
        ElseIf mstrNodeTag = "Group" Then
            Exit Sub
        End If
        Try
            If mintNodeId > 0 Then
                If objLetterType.isUsed(mintNodeId) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, selected Letter Type cannot be deleted. Reason : Selected Letter Type is already used for some transaction."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete selected Letter Type."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then '?2

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objLetterType._FormName = mstrModuleName
                    objLetterType._LoginEmployeeUnkid = 0
                    objLetterType._ClientIP = getIP()
                    objLetterType._HostName = getHostName()
                    objLetterType._FromWeb = False
                    objLetterType._AuditUserId = User._Object._Userunkid
objLetterType._CompanyUnkid = Company._Object._Companyunkid
                    objLetterType._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objLetterType.Delete(mintNodeId) Then
                        Call CreateTreeView()
                        objrtbLetterPreview.Text = ""
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        If mintNodeId = 0 Then
            Exit Sub
        ElseIf mstrNodeTag = "Group" Then
            Exit Sub
        End If
        Try
            objLetterType._LettertypeUnkId = mintNodeId
            objrtbLetterPreview.Rtf = objLetterType._Lettercontent
            strLetterData = objLetterType._Lettercontent

            'S.SANDEEP [ 02 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnCancel = False
            'S.SANDEEP [ 02 MAY 2012 ] -- END

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnInsert_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
        'S.SANDEEP [ 02 MAY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        mblnCancel = True
        'S.SANDEEP [ 02 MAY 2012 ] -- END
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            Call btnInsert_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End
#End Region

#Region " Controls "
    Private Sub tvLetterType_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvLetterType.AfterSelect

        If tvLetterType.Nodes.Count <= 0 Then Exit Sub

        mstrNodeTag = CStr(e.Node.Tag)
        mintNodeId = CInt(Val(e.Node.Name))

    End Sub

    Private Sub tvLetterType_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvLetterType.DoubleClick
        If mintNodeId = 0 Then
            Exit Sub
        ElseIf mstrNodeTag = "Group" Then
            Exit Sub
        End If
        Try
            objLetterType._LettertypeUnkId = mintNodeId
            objrtbLetterPreview.Rtf = objLetterType._Lettercontent
            strLetterData = objLetterType._Lettercontent

        Catch ex As Exception
            Call DisplayError.Show(CStr(-1), ex.Message, "tvLetterType_DoubleClick", mstrModuleName)
        End Try
    End Sub
#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnInsert.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInsert.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnInsert.Text = Language._Object.getCaption(Me.btnInsert.Name, Me.btnInsert.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, selected Letter Type cannot be deleted. Reason : Selected Letter Type is already used for some transaction.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete selected Letter Type.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

