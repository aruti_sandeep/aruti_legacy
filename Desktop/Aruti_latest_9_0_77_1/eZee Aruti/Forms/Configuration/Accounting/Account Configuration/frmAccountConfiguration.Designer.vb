﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountConfiguration
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountConfiguration))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbTranHeadList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblHeadCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgTransactionHead = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchTranHead = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchHeadType = New eZee.Common.eZeeGradientButton
        Me.lblHeadType = New System.Windows.Forms.Label
        Me.cboHeadType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTrantype = New eZee.Common.eZeeGradientButton
        Me.lblTransactionType = New System.Windows.Forms.Label
        Me.cboTransactionType = New System.Windows.Forms.ComboBox
        Me.gbAccountConfigInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.lblDistribution = New System.Windows.Forms.Label
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objbtnKeywordsSN2 = New eZee.Common.eZeeGradientButton
        Me.objbtnKeywordsSN1 = New eZee.Common.eZeeGradientButton
        Me.objbtnKeywords = New eZee.Common.eZeeGradientButton
        Me.lblShortName3 = New System.Windows.Forms.Label
        Me.txtShortName3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblShortName2 = New System.Windows.Forms.Label
        Me.txtShortName2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblShortName = New System.Windows.Forms.Label
        Me.txtShortName = New eZee.TextBox.AlphanumericTextBox
        Me.cboMapRefName = New System.Windows.Forms.ComboBox
        Me.lblMapRefName = New System.Windows.Forms.Label
        Me.cboMapRefCode = New System.Windows.Forms.ComboBox
        Me.lblMapRefCode = New System.Windows.Forms.Label
        Me.chkIsExist = New System.Windows.Forms.CheckBox
        Me.objbtnAddAccount = New eZee.Common.eZeeGradientButton
        Me.cboAccount = New System.Windows.Forms.ComboBox
        Me.lblAccount = New System.Windows.Forms.Label
        Me.cboAccountGroup = New System.Windows.Forms.ComboBox
        Me.lblAccountGroup = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblSearchTranhead = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbTranHeadList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgTransactionHead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbAccountConfigInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbTranHeadList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.gbAccountConfigInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(685, 572)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbTranHeadList
        '
        Me.gbTranHeadList.BorderColor = System.Drawing.Color.Black
        Me.gbTranHeadList.Checked = False
        Me.gbTranHeadList.CollapseAllExceptThis = False
        Me.gbTranHeadList.CollapsedHoverImage = Nothing
        Me.gbTranHeadList.CollapsedNormalImage = Nothing
        Me.gbTranHeadList.CollapsedPressedImage = Nothing
        Me.gbTranHeadList.CollapseOnLoad = False
        Me.gbTranHeadList.Controls.Add(Me.objlblHeadCount)
        Me.gbTranHeadList.Controls.Add(Me.pnlEmployeeList)
        Me.gbTranHeadList.ExpandedHoverImage = Nothing
        Me.gbTranHeadList.ExpandedNormalImage = Nothing
        Me.gbTranHeadList.ExpandedPressedImage = Nothing
        Me.gbTranHeadList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTranHeadList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTranHeadList.HeaderHeight = 25
        Me.gbTranHeadList.HeaderMessage = ""
        Me.gbTranHeadList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTranHeadList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTranHeadList.HeightOnCollapse = 0
        Me.gbTranHeadList.LeftTextSpace = 0
        Me.gbTranHeadList.Location = New System.Drawing.Point(12, 66)
        Me.gbTranHeadList.Name = "gbTranHeadList"
        Me.gbTranHeadList.OpenHeight = 300
        Me.gbTranHeadList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTranHeadList.ShowBorder = True
        Me.gbTranHeadList.ShowCheckBox = False
        Me.gbTranHeadList.ShowCollapseButton = False
        Me.gbTranHeadList.ShowDefaultBorderColor = True
        Me.gbTranHeadList.ShowDownButton = False
        Me.gbTranHeadList.ShowHeader = True
        Me.gbTranHeadList.Size = New System.Drawing.Size(280, 445)
        Me.gbTranHeadList.TabIndex = 4
        Me.gbTranHeadList.Temp = 0
        Me.gbTranHeadList.Text = "Transaction Head List"
        Me.gbTranHeadList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblHeadCount
        '
        Me.objlblHeadCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblHeadCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblHeadCount.Location = New System.Drawing.Point(122, 5)
        Me.objlblHeadCount.Name = "objlblHeadCount"
        Me.objlblHeadCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblHeadCount.TabIndex = 160
        Me.objlblHeadCount.Text = "( 0 )"
        Me.objlblHeadCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgTransactionHead)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchTranHead)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(277, 416)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgTransactionHead
        '
        Me.dgTransactionHead.AllowUserToAddRows = False
        Me.dgTransactionHead.AllowUserToDeleteRows = False
        Me.dgTransactionHead.AllowUserToResizeRows = False
        Me.dgTransactionHead.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgTransactionHead.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgTransactionHead.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgTransactionHead.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgTransactionHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgTransactionHead.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhUnkid, Me.dgColhCode, Me.dgColhName})
        Me.dgTransactionHead.Location = New System.Drawing.Point(1, 28)
        Me.dgTransactionHead.Name = "dgTransactionHead"
        Me.dgTransactionHead.RowHeadersVisible = False
        Me.dgTransactionHead.RowHeadersWidth = 5
        Me.dgTransactionHead.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgTransactionHead.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgTransactionHead.Size = New System.Drawing.Size(274, 388)
        Me.dgTransactionHead.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhUnkid
        '
        Me.objdgcolhUnkid.HeaderText = "unkid"
        Me.objdgcolhUnkid.Name = "objdgcolhUnkid"
        Me.objdgcolhUnkid.ReadOnly = True
        Me.objdgcolhUnkid.Visible = False
        '
        'dgColhCode
        '
        Me.dgColhCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhCode.HeaderText = "Code"
        Me.dgColhCode.Name = "dgColhCode"
        Me.dgColhCode.ReadOnly = True
        Me.dgColhCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhCode.Width = 70
        '
        'dgColhName
        '
        Me.dgColhName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhName.HeaderText = "Name"
        Me.dgColhName.Name = "dgColhName"
        Me.dgColhName.ReadOnly = True
        '
        'txtSearchTranHead
        '
        Me.txtSearchTranHead.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchTranHead.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchTranHead.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchTranHead.Name = "txtSearchTranHead"
        Me.txtSearchTranHead.Size = New System.Drawing.Size(273, 21)
        Me.txtSearchTranHead.TabIndex = 12
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.lblHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.cboHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTrantype)
        Me.gbFilterCriteria.Controls.Add(Me.lblTransactionType)
        Me.gbFilterCriteria.Controls.Add(Me.cboTransactionType)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(300, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(375, 92)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Transaction Head Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchHeadType
        '
        Me.objbtnSearchHeadType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchHeadType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchHeadType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchHeadType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchHeadType.BorderSelected = False
        Me.objbtnSearchHeadType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchHeadType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchHeadType.Location = New System.Drawing.Point(340, 61)
        Me.objbtnSearchHeadType.Name = "objbtnSearchHeadType"
        Me.objbtnSearchHeadType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchHeadType.TabIndex = 145
        '
        'lblHeadType
        '
        Me.lblHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadType.Location = New System.Drawing.Point(15, 64)
        Me.lblHeadType.Name = "lblHeadType"
        Me.lblHeadType.Size = New System.Drawing.Size(111, 15)
        Me.lblHeadType.TabIndex = 146
        Me.lblHeadType.Text = "Head Type"
        '
        'cboHeadType
        '
        Me.cboHeadType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboHeadType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadType.FormattingEnabled = True
        Me.cboHeadType.Location = New System.Drawing.Point(132, 61)
        Me.cboHeadType.Name = "cboHeadType"
        Me.cboHeadType.Size = New System.Drawing.Size(202, 21)
        Me.cboHeadType.TabIndex = 1
        '
        'objbtnSearchTrantype
        '
        Me.objbtnSearchTrantype.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTrantype.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrantype.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrantype.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTrantype.BorderSelected = False
        Me.objbtnSearchTrantype.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTrantype.Image = CType(resources.GetObject("objbtnSearchTrantype.Image"), System.Drawing.Image)
        Me.objbtnSearchTrantype.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTrantype.Location = New System.Drawing.Point(340, 34)
        Me.objbtnSearchTrantype.Name = "objbtnSearchTrantype"
        Me.objbtnSearchTrantype.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTrantype.TabIndex = 88
        '
        'lblTransactionType
        '
        Me.lblTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionType.Location = New System.Drawing.Point(15, 37)
        Me.lblTransactionType.Name = "lblTransactionType"
        Me.lblTransactionType.Size = New System.Drawing.Size(111, 15)
        Me.lblTransactionType.TabIndex = 142
        Me.lblTransactionType.Text = "Transaction Type"
        '
        'cboTransactionType
        '
        Me.cboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTransactionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionType.FormattingEnabled = True
        Me.cboTransactionType.Location = New System.Drawing.Point(132, 34)
        Me.cboTransactionType.Name = "cboTransactionType"
        Me.cboTransactionType.Size = New System.Drawing.Size(202, 21)
        Me.cboTransactionType.TabIndex = 0
        '
        'gbAccountConfigInfo
        '
        Me.gbAccountConfigInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAccountConfigInfo.Checked = False
        Me.gbAccountConfigInfo.CollapseAllExceptThis = False
        Me.gbAccountConfigInfo.CollapsedHoverImage = Nothing
        Me.gbAccountConfigInfo.CollapsedNormalImage = Nothing
        Me.gbAccountConfigInfo.CollapsedPressedImage = Nothing
        Me.gbAccountConfigInfo.CollapseOnLoad = False
        Me.gbAccountConfigInfo.Controls.Add(Me.lblPercentage)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblDistribution)
        Me.gbAccountConfigInfo.Controls.Add(Me.txtPercentage)
        Me.gbAccountConfigInfo.Controls.Add(Me.cboPeriod)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblPeriod)
        Me.gbAccountConfigInfo.Controls.Add(Me.objbtnKeywordsSN2)
        Me.gbAccountConfigInfo.Controls.Add(Me.objbtnKeywordsSN1)
        Me.gbAccountConfigInfo.Controls.Add(Me.objbtnKeywords)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblShortName3)
        Me.gbAccountConfigInfo.Controls.Add(Me.txtShortName3)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblShortName2)
        Me.gbAccountConfigInfo.Controls.Add(Me.txtShortName2)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblShortName)
        Me.gbAccountConfigInfo.Controls.Add(Me.txtShortName)
        Me.gbAccountConfigInfo.Controls.Add(Me.cboMapRefName)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblMapRefName)
        Me.gbAccountConfigInfo.Controls.Add(Me.cboMapRefCode)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblMapRefCode)
        Me.gbAccountConfigInfo.Controls.Add(Me.chkIsExist)
        Me.gbAccountConfigInfo.Controls.Add(Me.objbtnAddAccount)
        Me.gbAccountConfigInfo.Controls.Add(Me.cboAccount)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblAccount)
        Me.gbAccountConfigInfo.Controls.Add(Me.cboAccountGroup)
        Me.gbAccountConfigInfo.Controls.Add(Me.lblAccountGroup)
        Me.gbAccountConfigInfo.ExpandedHoverImage = Nothing
        Me.gbAccountConfigInfo.ExpandedNormalImage = Nothing
        Me.gbAccountConfigInfo.ExpandedPressedImage = Nothing
        Me.gbAccountConfigInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAccountConfigInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAccountConfigInfo.HeaderHeight = 25
        Me.gbAccountConfigInfo.HeaderMessage = ""
        Me.gbAccountConfigInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAccountConfigInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAccountConfigInfo.HeightOnCollapse = 0
        Me.gbAccountConfigInfo.LeftTextSpace = 0
        Me.gbAccountConfigInfo.Location = New System.Drawing.Point(298, 164)
        Me.gbAccountConfigInfo.Name = "gbAccountConfigInfo"
        Me.gbAccountConfigInfo.OpenHeight = 300
        Me.gbAccountConfigInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAccountConfigInfo.ShowBorder = True
        Me.gbAccountConfigInfo.ShowCheckBox = False
        Me.gbAccountConfigInfo.ShowCollapseButton = False
        Me.gbAccountConfigInfo.ShowDefaultBorderColor = True
        Me.gbAccountConfigInfo.ShowDownButton = False
        Me.gbAccountConfigInfo.ShowHeader = True
        Me.gbAccountConfigInfo.Size = New System.Drawing.Size(377, 347)
        Me.gbAccountConfigInfo.TabIndex = 1
        Me.gbAccountConfigInfo.Temp = 0
        Me.gbAccountConfigInfo.Text = "Account Configuration Information"
        Me.gbAccountConfigInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(225, 275)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(32, 15)
        Me.lblPercentage.TabIndex = 262
        Me.lblPercentage.Text = "(%)"
        Me.lblPercentage.Visible = False
        '
        'lblDistribution
        '
        Me.lblDistribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDistribution.Location = New System.Drawing.Point(8, 275)
        Me.lblDistribution.Name = "lblDistribution"
        Me.lblDistribution.Size = New System.Drawing.Size(102, 15)
        Me.lblDistribution.TabIndex = 261
        Me.lblDistribution.Text = "Distribution"
        Me.lblDistribution.Visible = False
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = False
        Me.txtPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 65536
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(134, 272)
        Me.txtPercentage.MaxDecimalPlaces = 6
        Me.txtPercentage.MaxWholeDigits = 21
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(85, 21)
        Me.txtPercentage.TabIndex = 260
        Me.txtPercentage.Text = "0"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPercentage.Visible = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(134, 32)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(202, 21)
        Me.cboPeriod.TabIndex = 185
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(13, 35)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(116, 18)
        Me.lblPeriod.TabIndex = 186
        Me.lblPeriod.Text = "Effective Period"
        '
        'objbtnKeywordsSN2
        '
        Me.objbtnKeywordsSN2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSN2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSN2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSN2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsSN2.BorderSelected = False
        Me.objbtnKeywordsSN2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsSN2.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsSN2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsSN2.Location = New System.Drawing.Point(342, 194)
        Me.objbtnKeywordsSN2.Name = "objbtnKeywordsSN2"
        Me.objbtnKeywordsSN2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsSN2.TabIndex = 163
        '
        'objbtnKeywordsSN1
        '
        Me.objbtnKeywordsSN1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSN1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSN1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSN1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsSN1.BorderSelected = False
        Me.objbtnKeywordsSN1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsSN1.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsSN1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsSN1.Location = New System.Drawing.Point(342, 167)
        Me.objbtnKeywordsSN1.Name = "objbtnKeywordsSN1"
        Me.objbtnKeywordsSN1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsSN1.TabIndex = 162
        '
        'objbtnKeywords
        '
        Me.objbtnKeywords.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywords.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywords.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywords.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywords.BorderSelected = False
        Me.objbtnKeywords.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywords.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objbtnKeywords.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywords.Location = New System.Drawing.Point(342, 221)
        Me.objbtnKeywords.Name = "objbtnKeywords"
        Me.objbtnKeywords.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywords.TabIndex = 160
        '
        'lblShortName3
        '
        Me.lblShortName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShortName3.Location = New System.Drawing.Point(8, 224)
        Me.lblShortName3.Name = "lblShortName3"
        Me.lblShortName3.Size = New System.Drawing.Size(120, 15)
        Me.lblShortName3.TabIndex = 158
        Me.lblShortName3.Text = "Short Name3"
        '
        'txtShortName3
        '
        Me.txtShortName3.Flags = 0
        Me.txtShortName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShortName3.HideSelection = False
        Me.txtShortName3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtShortName3.Location = New System.Drawing.Point(134, 221)
        Me.txtShortName3.Name = "txtShortName3"
        Me.txtShortName3.Size = New System.Drawing.Size(202, 21)
        Me.txtShortName3.TabIndex = 157
        '
        'lblShortName2
        '
        Me.lblShortName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShortName2.Location = New System.Drawing.Point(8, 197)
        Me.lblShortName2.Name = "lblShortName2"
        Me.lblShortName2.Size = New System.Drawing.Size(120, 15)
        Me.lblShortName2.TabIndex = 155
        Me.lblShortName2.Text = "Short Name2"
        '
        'txtShortName2
        '
        Me.txtShortName2.Flags = 0
        Me.txtShortName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShortName2.HideSelection = False
        Me.txtShortName2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtShortName2.Location = New System.Drawing.Point(134, 194)
        Me.txtShortName2.Name = "txtShortName2"
        Me.txtShortName2.Size = New System.Drawing.Size(202, 21)
        Me.txtShortName2.TabIndex = 5
        '
        'lblShortName
        '
        Me.lblShortName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShortName.Location = New System.Drawing.Point(8, 170)
        Me.lblShortName.Name = "lblShortName"
        Me.lblShortName.Size = New System.Drawing.Size(120, 15)
        Me.lblShortName.TabIndex = 152
        Me.lblShortName.Text = "Short Name"
        '
        'txtShortName
        '
        Me.txtShortName.Flags = 0
        Me.txtShortName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShortName.HideSelection = False
        Me.txtShortName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtShortName.Location = New System.Drawing.Point(134, 167)
        Me.txtShortName.Name = "txtShortName"
        Me.txtShortName.Size = New System.Drawing.Size(202, 21)
        Me.txtShortName.TabIndex = 4
        '
        'cboMapRefName
        '
        Me.cboMapRefName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMapRefName.DropDownWidth = 250
        Me.cboMapRefName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMapRefName.FormattingEnabled = True
        Me.cboMapRefName.Location = New System.Drawing.Point(134, 140)
        Me.cboMapRefName.Name = "cboMapRefName"
        Me.cboMapRefName.Size = New System.Drawing.Size(202, 21)
        Me.cboMapRefName.TabIndex = 3
        '
        'lblMapRefName
        '
        Me.lblMapRefName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMapRefName.Location = New System.Drawing.Point(8, 143)
        Me.lblMapRefName.Name = "lblMapRefName"
        Me.lblMapRefName.Size = New System.Drawing.Size(120, 15)
        Me.lblMapRefName.TabIndex = 149
        Me.lblMapRefName.Text = "Map Reference Name"
        '
        'cboMapRefCode
        '
        Me.cboMapRefCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMapRefCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMapRefCode.FormattingEnabled = True
        Me.cboMapRefCode.Location = New System.Drawing.Point(134, 113)
        Me.cboMapRefCode.Name = "cboMapRefCode"
        Me.cboMapRefCode.Size = New System.Drawing.Size(202, 21)
        Me.cboMapRefCode.TabIndex = 2
        '
        'lblMapRefCode
        '
        Me.lblMapRefCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMapRefCode.Location = New System.Drawing.Point(8, 116)
        Me.lblMapRefCode.Name = "lblMapRefCode"
        Me.lblMapRefCode.Size = New System.Drawing.Size(120, 15)
        Me.lblMapRefCode.TabIndex = 147
        Me.lblMapRefCode.Text = "Map Reference Code"
        '
        'chkIsExist
        '
        Me.chkIsExist.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsExist.Location = New System.Drawing.Point(134, 248)
        Me.chkIsExist.Name = "chkIsExist"
        Me.chkIsExist.Size = New System.Drawing.Size(202, 18)
        Me.chkIsExist.TabIndex = 6
        Me.chkIsExist.Text = "Overwrite if exist"
        Me.chkIsExist.UseVisualStyleBackColor = True
        '
        'objbtnAddAccount
        '
        Me.objbtnAddAccount.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAccount.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAccount.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAccount.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAccount.BorderSelected = False
        Me.objbtnAddAccount.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAccount.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddAccount.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAccount.Location = New System.Drawing.Point(342, 86)
        Me.objbtnAddAccount.Name = "objbtnAddAccount"
        Me.objbtnAddAccount.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAccount.TabIndex = 144
        '
        'cboAccount
        '
        Me.cboAccount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccount.FormattingEnabled = True
        Me.cboAccount.Location = New System.Drawing.Point(134, 86)
        Me.cboAccount.Name = "cboAccount"
        Me.cboAccount.Size = New System.Drawing.Size(202, 21)
        Me.cboAccount.TabIndex = 1
        '
        'lblAccount
        '
        Me.lblAccount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccount.Location = New System.Drawing.Point(8, 89)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Size = New System.Drawing.Size(120, 15)
        Me.lblAccount.TabIndex = 138
        Me.lblAccount.Text = "Account Name"
        '
        'cboAccountGroup
        '
        Me.cboAccountGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountGroup.FormattingEnabled = True
        Me.cboAccountGroup.Location = New System.Drawing.Point(134, 59)
        Me.cboAccountGroup.Name = "cboAccountGroup"
        Me.cboAccountGroup.Size = New System.Drawing.Size(202, 21)
        Me.cboAccountGroup.TabIndex = 0
        '
        'lblAccountGroup
        '
        Me.lblAccountGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountGroup.Location = New System.Drawing.Point(8, 62)
        Me.lblAccountGroup.Name = "lblAccountGroup"
        Me.lblAccountGroup.Size = New System.Drawing.Size(120, 15)
        Me.lblAccountGroup.TabIndex = 137
        Me.lblAccountGroup.Text = "Account Group"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblSearchTranhead)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(685, 55)
        Me.objFooter.TabIndex = 3
        '
        'lblSearchTranhead
        '
        Me.lblSearchTranhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchTranhead.Location = New System.Drawing.Point(10, 21)
        Me.lblSearchTranhead.Name = "lblSearchTranhead"
        Me.lblSearchTranhead.Size = New System.Drawing.Size(112, 15)
        Me.lblSearchTranhead.TabIndex = 154
        Me.lblSearchTranhead.Text = "Search Tran. Head"
        Me.lblSearchTranhead.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(484, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(580, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(685, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Add / Edit Account Configuration"
        '
        'frmAccountConfiguration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 572)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAccountConfiguration"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Account Configuration"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbTranHeadList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgTransactionHead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbAccountConfigInfo.ResumeLayout(False)
        Me.gbAccountConfigInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbAccountConfigInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents cboAccount As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccount As System.Windows.Forms.Label
    Friend WithEvents cboAccountGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountGroup As System.Windows.Forms.Label
    Friend WithEvents lblTransactionType As System.Windows.Forms.Label
    Friend WithEvents cboTransactionType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddAccount As eZee.Common.eZeeGradientButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchTrantype As eZee.Common.eZeeGradientButton
    Friend WithEvents chkIsExist As System.Windows.Forms.CheckBox
    Friend WithEvents cboMapRefCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblMapRefCode As System.Windows.Forms.Label
    Friend WithEvents cboMapRefName As System.Windows.Forms.ComboBox
    Friend WithEvents lblMapRefName As System.Windows.Forms.Label
    Friend WithEvents lblShortName As System.Windows.Forms.Label
    Friend WithEvents txtShortName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSearchTranhead As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchHeadType As eZee.Common.eZeeGradientButton
    Friend WithEvents lblHeadType As System.Windows.Forms.Label
    Friend WithEvents cboHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblShortName2 As System.Windows.Forms.Label
    Friend WithEvents txtShortName2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbTranHeadList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblHeadCount As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgTransactionHead As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchTranHead As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblShortName3 As System.Windows.Forms.Label
    Friend WithEvents txtShortName3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnKeywords As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnKeywordsSN2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnKeywordsSN1 As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents lblDistribution As System.Windows.Forms.Label
    Friend WithEvents txtPercentage As eZee.TextBox.NumericTextBox
End Class
