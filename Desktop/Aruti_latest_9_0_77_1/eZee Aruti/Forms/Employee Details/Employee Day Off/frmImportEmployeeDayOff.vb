﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region


Public Class frmImportEmployeeDayOff

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportEmployeeDayOff"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Dim objEmpDayoff As clsemployee_dayoff_Tran
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmImportEmployeeDayOff_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objEmpDayoff = New clsemployee_dayoff_Tran
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportEmployeeDayOff_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillGirdView()
        Try
            If dsList IsNot Nothing Then

                dsList.Tables(0).Columns.Add("isChecked", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("image", Type.GetType("System.Object"))
                dsList.Tables(0).Columns.Add("employeeunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("error", Type.GetType("System.String"))

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim objEmployee As New clsEmployee_Master

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        dsList.Tables(0).Rows(i)("isChecked") = False
                        dsList.Tables(0).Rows(i)("error") = ""
                        dsList.Tables(0).Rows(i)("image") = New Drawing.Bitmap(1, 1).Clone
                        dsList.Tables(0).Rows(i)("employeeunkid") = objEmployee.GetEmployeeUnkid("", dsList.Tables(0).Rows(i)("Code").ToString().Trim())
                        If IsDBNull(dsList.Tables(0).Rows(i)("Offdate")) = False AndAlso dsList.Tables(0).Rows(i)("Offdate").ToString().Trim().Length > 0 Then
                            dsList.Tables(0).Rows(i)("Offdate") = CDate(dsList.Tables(0).Rows(i)("Offdate")).ToShortDateString()
                        End If
                    Next

                End If

            End If

            dgEmployees.AutoGenerateColumns = False

            dgEmployees.DataSource = dsList.Tables(0)

            colhImage.DataPropertyName = "image"
            colhEmployeeCode.DataPropertyName = "Code"
            colhEmployee.DataPropertyName = "EmployeeName"
            colhDayOffDate.DataPropertyName = "Offdate"
            objcolhIscheck.DataPropertyName = "isChecked"
            colhError.DataPropertyName = "error"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then

                Cursor = Cursors.WaitCursor
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                dsList = OpenXML_Import(txtFilePath.Text)

                Dim strQuery As String = ""
                For i As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    dsList.Tables(0).Columns(i).ColumnName = dsList.Tables(0).Columns(i).ColumnName.Replace(" ", "_")
                    strQuery &= " AND " & dsList.Tables(0).Columns(i).ColumnName & " IS NULL "
                Next

                If strQuery.Trim.Length > 0 Then
                    strQuery = strQuery.Trim.Substring(4, strQuery.Trim.Length - 4)
                End If

                Dim drRow As DataRow() = dsList.Tables(0).Select(strQuery)

                If drRow.Length > 0 Then

                    For j As Integer = 0 To drRow.Length - 1
                        dsList.Tables(0).Rows.Remove(drRow(j))
                    Next
                    dsList.Tables(0).AcceptChanges()

                End If
                FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try
            Dim blnError As Boolean = False

            If dgEmployees.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to import Employee(s) Dayoff."), enMsgBoxStyle.Information)
                Exit Sub
            Else

                Me.Cursor = Cursors.WaitCursor

                btnImportData.Enabled = False
                btnExportError.Enabled = False

                Dim objShortList As New clsshortlist_finalapplicant
                Dim xCountFinalApplicants As Integer = 0
                Dim intIndex As Integer = 0

                For Each dr As DataRow In dsList.Tables(0).Rows

                    xCountFinalApplicants += 1

                    lnkFinalApplicantsProcess.Text = Language.getMessage(mstrModuleName, 11, "Employees Dayoff Processed : ") & xCountFinalApplicants & "/" & dsList.Tables(0).Rows.Count
                    Try
                        dgEmployees.FirstDisplayedScrollingRowIndex = dsList.Tables(0).Rows.IndexOf(dr) - 16
                        Application.DoEvents()
                    Catch ex As Exception
                    End Try

                    If CInt(dr("employeeunkid")) <= 0 Then
                        dgEmployees.Rows(intIndex).Cells(colhEmployeeCode.Index).ErrorText = Language.getMessage(mstrModuleName, 3, "Invalid Employee Code.")
                        dgEmployees.Rows(intIndex).Cells(colhEmployeeCode.Index).ToolTipText = dgEmployees.Rows(intIndex).Cells(colhEmployeeCode.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 5, "Invalid Employee Code.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf IsDBNull(dr("offdate")) Then
                        dgEmployees.Rows(intIndex).Cells(colhDayOffDate.Index).ErrorText = Language.getMessage(mstrModuleName, 4, "Off Date is compulsory information.")
                        dgEmployees.Rows(intIndex).Cells(colhDayOffDate.Index).ToolTipText = dgEmployees.Rows(intIndex).Cells(colhDayOffDate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 2, "Off Date is compulsory information.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    End If

                    dr("isChecked") = True


                    If objEmpDayoff.isExist(CDate(dr("offdate")).Date, CInt(dr("employeeunkid")), -1, Nothing) = False Then
                        objEmpDayoff._Employeeunkid = CInt(dr("employeeunkid"))
                        objEmpDayoff._Dayoffdate = CDate(dr("offdate")).Date
                        objEmpDayoff._Userunkid = User._Object._Userunkid

                        If objEmpDayoff.Insert() = False Then
                            dgEmployees.Rows(intIndex).Cells(colhEmployee.Index).ErrorText = objEmpDayoff._Message
                            dgEmployees.Rows(intIndex).Cells(colhEmployee.Index).ToolTipText = dgEmployees.Rows(intIndex).Cells(colhEmployeeCode.Index).ErrorText
                            dr("error") = Language.getMessage(mstrModuleName, 7, "This Employee's dayoff already assigned for this date.Please assign new date.")
                            dr("Image") = imgError
                            blnError = True
                            intIndex += 1
                            Continue For
                        End If
                    Else
                        dgEmployees.Rows(intIndex).Cells(colhEmployeeCode.Index).ErrorText = Language.getMessage(mstrModuleName, 7, "This Employee's dayoff already assigned for this date.Please assign new date.")
                        dgEmployees.Rows(intIndex).Cells(colhEmployee.Index).ToolTipText = dgEmployees.Rows(intIndex).Cells(colhEmployeeCode.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 7, "This Employee's dayoff already assigned for this date.Please assign new date.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For
                    End If


                    dr("Image") = imgAccept
                    intIndex += 1

                    GC.Collect(0, GCCollectionMode.Forced)
                Next

                btnImportData.Enabled = True
                btnExportError.Enabled = True

                Dim drError As DataRow() = dsList.Tables(0).Select("error <> '' ")

                If drError.Length > 0 Then
                    If drError.Length < dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Some Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        btnExportError_Click(btnExportError, New EventArgs())
                        Exit Sub

                    ElseIf drError.Length = dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        btnExportError_Click(btnExportError, New EventArgs())
                        Exit Sub

                    End If
                End If

                If blnError = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Data successfully imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList.Dispose()
                    dsList = Nothing
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnImportData_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetFileFormat.Click
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim dsList As DataSet = objEmpDayoff.GetFileStrctureForEmployeeDayOff()
                OpenXML_Export(objSave.FileName, dsList)
            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnGetFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportError.Click
        Try

            If dgEmployees.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "There is no data to show error(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dsList IsNot Nothing Then
                Dim dvGriddata As DataView = dsList.Tables(0).DefaultView
                dvGriddata.RowFilter = "error <>''"
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("isChecked")
                        dtTable.Columns.Remove("employeeunkid")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Employee(s) Dayoff ") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.lnkFinalApplicantsProcess.Text = Language._Object.getCaption(Me.lnkFinalApplicantsProcess.Name, Me.lnkFinalApplicantsProcess.Text)
            Me.colhImage.HeaderText = Language._Object.getCaption(Me.colhImage.Name, Me.colhImage.HeaderText)
            Me.colhEmployeeCode.HeaderText = Language._Object.getCaption(Me.colhEmployeeCode.Name, Me.colhEmployeeCode.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhDayOffDate.HeaderText = Language._Object.getCaption(Me.colhDayOffDate.Name, Me.colhDayOffDate.HeaderText)
            Me.colhError.HeaderText = Language._Object.getCaption(Me.colhError.Name, Me.colhError.HeaderText)
            Me.btnGetFileFormat.Text = Language._Object.getCaption(Me.btnGetFileFormat.Name, Me.btnGetFileFormat.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no data to import Employee(s) Dayoff.")
            Language.setMessage(mstrModuleName, 2, "Off Date is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Invalid Employee Code.")
            Language.setMessage(mstrModuleName, 4, "Off Date is compulsory information.")
            Language.setMessage(mstrModuleName, 5, "Invalid Employee Code.")
            Language.setMessage(mstrModuleName, 6, "There is no data to show error(s).")
            Language.setMessage(mstrModuleName, 7, "This Employee's dayoff already assigned for this date.Please assign new date.")
            Language.setMessage(mstrModuleName, 8, "Some Data did not import due to error.")
            Language.setMessage(mstrModuleName, 9, "Data did not import due to error.")
            Language.setMessage(mstrModuleName, 10, "Data successfully imported.")
            Language.setMessage(mstrModuleName, 11, "Employees Dayoff Processed :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class