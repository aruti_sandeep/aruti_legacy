﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeReferee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeReferee))
        Me.pnlEmployeeReferee = New System.Windows.Forms.Panel
        Me.gbRefereeInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPosition = New System.Windows.Forms.Label
        Me.objbtnAddRefereeType = New eZee.Common.eZeeGradientButton
        Me.cboRefereeType = New System.Windows.Forms.ComboBox
        Me.lblRefereeType = New System.Windows.Forms.Label
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.lnRefreeInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboTown = New System.Windows.Forms.ComboBox
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.txtMobileNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmailAddress = New eZee.TextBox.AlphanumericTextBox
        Me.txtTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtPosition = New eZee.TextBox.AlphanumericTextBox
        Me.txtCompany = New eZee.TextBox.AlphanumericTextBox
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.txtAddress = New eZee.TextBox.AlphanumericTextBox
        Me.txtRefreeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lblGender = New System.Windows.Forms.Label
        Me.lblMobile = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblTelNo = New System.Windows.Forms.Label
        Me.lblIDNo = New System.Windows.Forms.Label
        Me.lblPostTown = New System.Windows.Forms.Label
        Me.lblPostCountry = New System.Windows.Forms.Label
        Me.lblRefereeName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSaveInfo = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlEmployeeReferee.SuspendLayout()
        Me.gbRefereeInformation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlEmployeeReferee
        '
        Me.pnlEmployeeReferee.Controls.Add(Me.gbRefereeInformation)
        Me.pnlEmployeeReferee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmployeeReferee.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmployeeReferee.Name = "pnlEmployeeReferee"
        Me.pnlEmployeeReferee.Size = New System.Drawing.Size(458, 400)
        Me.pnlEmployeeReferee.TabIndex = 0
        '
        'gbRefereeInformation
        '
        Me.gbRefereeInformation.BorderColor = System.Drawing.Color.Black
        Me.gbRefereeInformation.Checked = False
        Me.gbRefereeInformation.CollapseAllExceptThis = False
        Me.gbRefereeInformation.CollapsedHoverImage = Nothing
        Me.gbRefereeInformation.CollapsedNormalImage = Nothing
        Me.gbRefereeInformation.CollapsedPressedImage = Nothing
        Me.gbRefereeInformation.CollapseOnLoad = False
        Me.gbRefereeInformation.Controls.Add(Me.lblPosition)
        Me.gbRefereeInformation.Controls.Add(Me.objbtnAddRefereeType)
        Me.gbRefereeInformation.Controls.Add(Me.cboRefereeType)
        Me.gbRefereeInformation.Controls.Add(Me.lblRefereeType)
        Me.gbRefereeInformation.Controls.Add(Me.cboState)
        Me.gbRefereeInformation.Controls.Add(Me.lblState)
        Me.gbRefereeInformation.Controls.Add(Me.lnRefreeInfo)
        Me.gbRefereeInformation.Controls.Add(Me.lnEmployeeName)
        Me.gbRefereeInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbRefereeInformation.Controls.Add(Me.cboEmployee)
        Me.gbRefereeInformation.Controls.Add(Me.lblEmployee)
        Me.gbRefereeInformation.Controls.Add(Me.cboTown)
        Me.gbRefereeInformation.Controls.Add(Me.cboGender)
        Me.gbRefereeInformation.Controls.Add(Me.txtMobileNo)
        Me.gbRefereeInformation.Controls.Add(Me.txtEmailAddress)
        Me.gbRefereeInformation.Controls.Add(Me.txtTelNo)
        Me.gbRefereeInformation.Controls.Add(Me.txtPosition)
        Me.gbRefereeInformation.Controls.Add(Me.txtCompany)
        Me.gbRefereeInformation.Controls.Add(Me.cboCountry)
        Me.gbRefereeInformation.Controls.Add(Me.txtAddress)
        Me.gbRefereeInformation.Controls.Add(Me.txtRefreeName)
        Me.gbRefereeInformation.Controls.Add(Me.lblAddress)
        Me.gbRefereeInformation.Controls.Add(Me.lblGender)
        Me.gbRefereeInformation.Controls.Add(Me.lblMobile)
        Me.gbRefereeInformation.Controls.Add(Me.lblEmail)
        Me.gbRefereeInformation.Controls.Add(Me.lblTelNo)
        Me.gbRefereeInformation.Controls.Add(Me.lblIDNo)
        Me.gbRefereeInformation.Controls.Add(Me.lblPostTown)
        Me.gbRefereeInformation.Controls.Add(Me.lblPostCountry)
        Me.gbRefereeInformation.Controls.Add(Me.lblRefereeName)
        Me.gbRefereeInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbRefereeInformation.ExpandedHoverImage = Nothing
        Me.gbRefereeInformation.ExpandedNormalImage = Nothing
        Me.gbRefereeInformation.ExpandedPressedImage = Nothing
        Me.gbRefereeInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRefereeInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRefereeInformation.HeaderHeight = 25
        Me.gbRefereeInformation.HeaderMessage = ""
        Me.gbRefereeInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRefereeInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRefereeInformation.HeightOnCollapse = 0
        Me.gbRefereeInformation.LeftTextSpace = 0
        Me.gbRefereeInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbRefereeInformation.Name = "gbRefereeInformation"
        Me.gbRefereeInformation.OpenHeight = 270
        Me.gbRefereeInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRefereeInformation.ShowBorder = True
        Me.gbRefereeInformation.ShowCheckBox = False
        Me.gbRefereeInformation.ShowCollapseButton = False
        Me.gbRefereeInformation.ShowDefaultBorderColor = True
        Me.gbRefereeInformation.ShowDownButton = False
        Me.gbRefereeInformation.ShowHeader = True
        Me.gbRefereeInformation.Size = New System.Drawing.Size(458, 400)
        Me.gbRefereeInformation.TabIndex = 0
        Me.gbRefereeInformation.Temp = 0
        Me.gbRefereeInformation.Text = "Reference Information"
        Me.gbRefereeInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition
        '
        Me.lblPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPosition.Location = New System.Drawing.Point(18, 325)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(79, 15)
        Me.lblPosition.TabIndex = 30
        Me.lblPosition.Text = "Position"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddRefereeType
        '
        Me.objbtnAddRefereeType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddRefereeType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddRefereeType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddRefereeType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddRefereeType.BorderSelected = False
        Me.objbtnAddRefereeType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddRefereeType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddRefereeType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddRefereeType.Location = New System.Drawing.Point(425, 133)
        Me.objbtnAddRefereeType.Name = "objbtnAddRefereeType"
        Me.objbtnAddRefereeType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddRefereeType.TabIndex = 28
        '
        'cboRefereeType
        '
        Me.cboRefereeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRefereeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRefereeType.FormattingEnabled = True
        Me.cboRefereeType.Location = New System.Drawing.Point(103, 133)
        Me.cboRefereeType.Name = "cboRefereeType"
        Me.cboRefereeType.Size = New System.Drawing.Size(316, 21)
        Me.cboRefereeType.TabIndex = 27
        '
        'lblRefereeType
        '
        Me.lblRefereeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefereeType.Location = New System.Drawing.Point(18, 136)
        Me.lblRefereeType.Name = "lblRefereeType"
        Me.lblRefereeType.Size = New System.Drawing.Size(84, 15)
        Me.lblRefereeType.TabIndex = 26
        Me.lblRefereeType.Text = "Reference Type"
        Me.lblRefereeType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(301, 187)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(118, 21)
        Me.cboState.TabIndex = 12
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(234, 190)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(61, 15)
        Me.lblState.TabIndex = 11
        Me.lblState.Text = "State"
        '
        'lnRefreeInfo
        '
        Me.lnRefreeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnRefreeInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnRefreeInfo.Location = New System.Drawing.Point(6, 82)
        Me.lnRefreeInfo.Name = "lnRefreeInfo"
        Me.lnRefreeInfo.Size = New System.Drawing.Size(413, 21)
        Me.lnRefreeInfo.TabIndex = 4
        Me.lnRefreeInfo.Text = "Reference Info"
        Me.lnRefreeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(6, 32)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(413, 21)
        Me.lnEmployeeName.TabIndex = 0
        Me.lnEmployeeName.Text = "Employee Info"
        Me.lnEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(425, 58)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(103, 58)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(316, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(18, 61)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(79, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTown
        '
        Me.cboTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTown.FormattingEnabled = True
        Me.cboTown.Location = New System.Drawing.Point(103, 214)
        Me.cboTown.Name = "cboTown"
        Me.cboTown.Size = New System.Drawing.Size(125, 21)
        Me.cboTown.TabIndex = 14
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(301, 214)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(118, 21)
        Me.cboGender.TabIndex = 16
        '
        'txtMobileNo
        '
        Me.txtMobileNo.Flags = 0
        Me.txtMobileNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobileNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMobileNo.Location = New System.Drawing.Point(301, 241)
        Me.txtMobileNo.Name = "txtMobileNo"
        Me.txtMobileNo.Size = New System.Drawing.Size(118, 21)
        Me.txtMobileNo.TabIndex = 22
        '
        'txtEmailAddress
        '
        Me.txtEmailAddress.Flags = 0
        Me.txtEmailAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmailAddress.Location = New System.Drawing.Point(103, 268)
        Me.txtEmailAddress.Name = "txtEmailAddress"
        Me.txtEmailAddress.Size = New System.Drawing.Size(316, 21)
        Me.txtEmailAddress.TabIndex = 24
        '
        'txtTelNo
        '
        Me.txtTelNo.Flags = 0
        Me.txtTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTelNo.Location = New System.Drawing.Point(103, 241)
        Me.txtTelNo.Name = "txtTelNo"
        Me.txtTelNo.Size = New System.Drawing.Size(125, 21)
        Me.txtTelNo.TabIndex = 20
        '
        'txtPosition
        '
        Me.txtPosition.Flags = 0
        Me.txtPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPosition.Location = New System.Drawing.Point(103, 322)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.Size = New System.Drawing.Size(316, 21)
        Me.txtPosition.TabIndex = 18
        '
        'txtCompany
        '
        Me.txtCompany.Flags = 0
        Me.txtCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompany.Location = New System.Drawing.Point(103, 295)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(316, 21)
        Me.txtCompany.TabIndex = 18
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(103, 187)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(125, 21)
        Me.cboCountry.TabIndex = 10
        '
        'txtAddress
        '
        Me.txtAddress.Flags = 0
        Me.txtAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress.Location = New System.Drawing.Point(103, 160)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(316, 21)
        Me.txtAddress.TabIndex = 8
        '
        'txtRefreeName
        '
        Me.txtRefreeName.Flags = 0
        Me.txtRefreeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRefreeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRefreeName.Location = New System.Drawing.Point(103, 106)
        Me.txtRefreeName.Name = "txtRefreeName"
        Me.txtRefreeName.Size = New System.Drawing.Size(316, 21)
        Me.txtRefreeName.TabIndex = 6
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(18, 163)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(79, 15)
        Me.lblAddress.TabIndex = 7
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(234, 217)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(61, 15)
        Me.lblGender.TabIndex = 15
        Me.lblGender.Text = "Gender"
        '
        'lblMobile
        '
        Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMobile.Location = New System.Drawing.Point(234, 244)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(61, 15)
        Me.lblMobile.TabIndex = 21
        Me.lblMobile.Text = "Mobile"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(18, 271)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(79, 15)
        Me.lblEmail.TabIndex = 23
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTelNo
        '
        Me.lblTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelNo.Location = New System.Drawing.Point(18, 244)
        Me.lblTelNo.Name = "lblTelNo"
        Me.lblTelNo.Size = New System.Drawing.Size(79, 15)
        Me.lblTelNo.TabIndex = 19
        Me.lblTelNo.Text = "Tel. No."
        '
        'lblIDNo
        '
        Me.lblIDNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDNo.Location = New System.Drawing.Point(18, 298)
        Me.lblIDNo.Name = "lblIDNo"
        Me.lblIDNo.Size = New System.Drawing.Size(79, 15)
        Me.lblIDNo.TabIndex = 17
        Me.lblIDNo.Text = "Company"
        Me.lblIDNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostTown
        '
        Me.lblPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostTown.Location = New System.Drawing.Point(18, 217)
        Me.lblPostTown.Name = "lblPostTown"
        Me.lblPostTown.Size = New System.Drawing.Size(79, 15)
        Me.lblPostTown.TabIndex = 13
        Me.lblPostTown.Text = "Town"
        Me.lblPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostCountry
        '
        Me.lblPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostCountry.Location = New System.Drawing.Point(18, 190)
        Me.lblPostCountry.Name = "lblPostCountry"
        Me.lblPostCountry.Size = New System.Drawing.Size(79, 15)
        Me.lblPostCountry.TabIndex = 9
        Me.lblPostCountry.Text = "Country"
        Me.lblPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRefereeName
        '
        Me.lblRefereeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefereeName.Location = New System.Drawing.Point(18, 109)
        Me.lblRefereeName.Name = "lblRefereeName"
        Me.lblRefereeName.Size = New System.Drawing.Size(79, 15)
        Me.lblRefereeName.TabIndex = 5
        Me.lblRefereeName.Text = "Name"
        Me.lblRefereeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSaveInfo)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 350)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(458, 50)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(353, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 27
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSaveInfo
        '
        Me.btnSaveInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveInfo.BackColor = System.Drawing.Color.White
        Me.btnSaveInfo.BackgroundImage = CType(resources.GetObject("btnSaveInfo.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveInfo.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveInfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveInfo.FlatAppearance.BorderSize = 0
        Me.btnSaveInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveInfo.ForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveInfo.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Location = New System.Drawing.Point(254, 9)
        Me.btnSaveInfo.Name = "btnSaveInfo"
        Me.btnSaveInfo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Size = New System.Drawing.Size(93, 30)
        Me.btnSaveInfo.TabIndex = 26
        Me.btnSaveInfo.Text = "&Save"
        Me.btnSaveInfo.UseVisualStyleBackColor = True
        '
        'frmEmployeeReferee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(458, 400)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlEmployeeReferee)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeReferee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Reference"
        Me.pnlEmployeeReferee.ResumeLayout(False)
        Me.gbRefereeInformation.ResumeLayout(False)
        Me.gbRefereeInformation.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlEmployeeReferee As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbRefereeInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSaveInfo As eZee.Common.eZeeLightButton
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblTelNo As System.Windows.Forms.Label
    Friend WithEvents lblIDNo As System.Windows.Forms.Label
    Friend WithEvents lblPostTown As System.Windows.Forms.Label
    Friend WithEvents lblPostCountry As System.Windows.Forms.Label
    Friend WithEvents lblRefereeName As System.Windows.Forms.Label
    Friend WithEvents txtRefreeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtMobileNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmailAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents cboTown As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lnRefreeInfo As eZee.Common.eZeeLine
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents objbtnAddRefereeType As eZee.Common.eZeeGradientButton
    Friend WithEvents cboRefereeType As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefereeType As System.Windows.Forms.Label
    Friend WithEvents txtPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPosition As System.Windows.Forms.Label
End Class
