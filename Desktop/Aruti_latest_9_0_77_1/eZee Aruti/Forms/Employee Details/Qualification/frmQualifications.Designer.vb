﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQualifications
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQualifications))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlCertificates = New System.Windows.Forms.Panel
        Me.gbEmployeeQualification = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnAddAttachment = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvQualification = New System.Windows.Forms.DataGridView
        Me.objcohDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSize = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboDocumentType = New System.Windows.Forms.ComboBox
        Me.lblDocumentType = New System.Windows.Forms.Label
        Me.lnkOtherQualification = New System.Windows.Forms.LinkLabel
        Me.pnlOtherQualification = New System.Windows.Forms.Panel
        Me.lblOtherResultCode = New System.Windows.Forms.Label
        Me.lblOtherQualificationGrp = New System.Windows.Forms.Label
        Me.txtOtherResultCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherQualificationGrp = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherQualification = New eZee.TextBox.AlphanumericTextBox
        Me.lblOtherQualification = New System.Windows.Forms.Label
        Me.objbtnSearchQualification = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchProvider = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchQGrp = New eZee.Common.eZeeGradientButton
        Me.nudLevel = New System.Windows.Forms.NumericUpDown
        Me.lblGPAcode = New System.Windows.Forms.Label
        Me.cboResultCode = New System.Windows.Forms.ComboBox
        Me.lblResultCode = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnAddInstitution = New eZee.Common.eZeeGradientButton
        Me.cboInstitution = New System.Windows.Forms.ComboBox
        Me.objstLine2 = New eZee.Common.eZeeStraightLine
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnAddQulification = New eZee.Common.eZeeGradientButton
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.lnQualificationInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.txtAwardNo = New eZee.TextBox.AlphanumericTextBox
        Me.dtpCertificateDate = New System.Windows.Forms.DateTimePicker
        Me.cboCertificates = New System.Windows.Forms.ComboBox
        Me.cboCertificateGroup = New System.Windows.Forms.ComboBox
        Me.lblQualification = New System.Windows.Forms.Label
        Me.lblAwardDate = New System.Windows.Forms.Label
        Me.lblQualificationGroup = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblInstitution = New System.Windows.Forms.Label
        Me.lblReferenceNo = New System.Windows.Forms.Label
        Me.txtOtherInstitute = New System.Windows.Forms.TextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ofdAttachment = New System.Windows.Forms.OpenFileDialog
        Me.sfdAttachment = New System.Windows.Forms.SaveFileDialog
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlCertificates.SuspendLayout()
        Me.gbEmployeeQualification.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvQualification, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOtherQualification.SuspendLayout()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCertificates
        '
        Me.pnlCertificates.Controls.Add(Me.gbEmployeeQualification)
        Me.pnlCertificates.Controls.Add(Me.objFooter)
        Me.pnlCertificates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCertificates.Location = New System.Drawing.Point(0, 0)
        Me.pnlCertificates.Name = "pnlCertificates"
        Me.pnlCertificates.Size = New System.Drawing.Size(704, 501)
        Me.pnlCertificates.TabIndex = 0
        '
        'gbEmployeeQualification
        '
        Me.gbEmployeeQualification.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeQualification.Checked = False
        Me.gbEmployeeQualification.CollapseAllExceptThis = False
        Me.gbEmployeeQualification.CollapsedHoverImage = Nothing
        Me.gbEmployeeQualification.CollapsedNormalImage = Nothing
        Me.gbEmployeeQualification.CollapsedPressedImage = Nothing
        Me.gbEmployeeQualification.CollapseOnLoad = False
        Me.gbEmployeeQualification.Controls.Add(Me.btnAddAttachment)
        Me.gbEmployeeQualification.Controls.Add(Me.Panel1)
        Me.gbEmployeeQualification.Controls.Add(Me.cboDocumentType)
        Me.gbEmployeeQualification.Controls.Add(Me.lblDocumentType)
        Me.gbEmployeeQualification.Controls.Add(Me.lnkOtherQualification)
        Me.gbEmployeeQualification.Controls.Add(Me.pnlOtherQualification)
        Me.gbEmployeeQualification.Controls.Add(Me.objbtnSearchQualification)
        Me.gbEmployeeQualification.Controls.Add(Me.objbtnSearchProvider)
        Me.gbEmployeeQualification.Controls.Add(Me.objbtnSearchQGrp)
        Me.gbEmployeeQualification.Controls.Add(Me.nudLevel)
        Me.gbEmployeeQualification.Controls.Add(Me.lblGPAcode)
        Me.gbEmployeeQualification.Controls.Add(Me.cboResultCode)
        Me.gbEmployeeQualification.Controls.Add(Me.lblResultCode)
        Me.gbEmployeeQualification.Controls.Add(Me.lblEndDate)
        Me.gbEmployeeQualification.Controls.Add(Me.dtpEndDate)
        Me.gbEmployeeQualification.Controls.Add(Me.dtpStartDate)
        Me.gbEmployeeQualification.Controls.Add(Me.objbtnAddInstitution)
        Me.gbEmployeeQualification.Controls.Add(Me.cboInstitution)
        Me.gbEmployeeQualification.Controls.Add(Me.objstLine2)
        Me.gbEmployeeQualification.Controls.Add(Me.objbtnAddGroup)
        Me.gbEmployeeQualification.Controls.Add(Me.objbtnAddQulification)
        Me.gbEmployeeQualification.Controls.Add(Me.objStLine1)
        Me.gbEmployeeQualification.Controls.Add(Me.lnQualificationInfo)
        Me.gbEmployeeQualification.Controls.Add(Me.lnEmployeeName)
        Me.gbEmployeeQualification.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeQualification.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeQualification.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeQualification.Controls.Add(Me.txtRemark)
        Me.gbEmployeeQualification.Controls.Add(Me.txtAwardNo)
        Me.gbEmployeeQualification.Controls.Add(Me.dtpCertificateDate)
        Me.gbEmployeeQualification.Controls.Add(Me.cboCertificates)
        Me.gbEmployeeQualification.Controls.Add(Me.cboCertificateGroup)
        Me.gbEmployeeQualification.Controls.Add(Me.lblQualification)
        Me.gbEmployeeQualification.Controls.Add(Me.lblAwardDate)
        Me.gbEmployeeQualification.Controls.Add(Me.lblQualificationGroup)
        Me.gbEmployeeQualification.Controls.Add(Me.lblStartDate)
        Me.gbEmployeeQualification.Controls.Add(Me.lblRemark)
        Me.gbEmployeeQualification.Controls.Add(Me.lblInstitution)
        Me.gbEmployeeQualification.Controls.Add(Me.lblReferenceNo)
        Me.gbEmployeeQualification.Controls.Add(Me.txtOtherInstitute)
        Me.gbEmployeeQualification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbEmployeeQualification.ExpandedHoverImage = Nothing
        Me.gbEmployeeQualification.ExpandedNormalImage = Nothing
        Me.gbEmployeeQualification.ExpandedPressedImage = Nothing
        Me.gbEmployeeQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeQualification.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeQualification.HeaderHeight = 25
        Me.gbEmployeeQualification.HeaderMessage = ""
        Me.gbEmployeeQualification.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeQualification.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeQualification.HeightOnCollapse = 0
        Me.gbEmployeeQualification.LeftTextSpace = 0
        Me.gbEmployeeQualification.Location = New System.Drawing.Point(0, 0)
        Me.gbEmployeeQualification.Name = "gbEmployeeQualification"
        Me.gbEmployeeQualification.OpenHeight = 253
        Me.gbEmployeeQualification.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeQualification.ShowBorder = True
        Me.gbEmployeeQualification.ShowCheckBox = False
        Me.gbEmployeeQualification.ShowCollapseButton = False
        Me.gbEmployeeQualification.ShowDefaultBorderColor = True
        Me.gbEmployeeQualification.ShowDownButton = False
        Me.gbEmployeeQualification.ShowHeader = True
        Me.gbEmployeeQualification.Size = New System.Drawing.Size(704, 446)
        Me.gbEmployeeQualification.TabIndex = 0
        Me.gbEmployeeQualification.Temp = 0
        Me.gbEmployeeQualification.Text = "Qualification Information"
        Me.gbEmployeeQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAddAttachment
        '
        Me.btnAddAttachment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddAttachment.BackColor = System.Drawing.Color.White
        Me.btnAddAttachment.BackgroundImage = CType(resources.GetObject("btnAddAttachment.BackgroundImage"), System.Drawing.Image)
        Me.btnAddAttachment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddAttachment.BorderColor = System.Drawing.Color.Empty
        Me.btnAddAttachment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddAttachment.FlatAppearance.BorderSize = 0
        Me.btnAddAttachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddAttachment.ForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddAttachment.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Location = New System.Drawing.Point(350, 293)
        Me.btnAddAttachment.Name = "btnAddAttachment"
        Me.btnAddAttachment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Size = New System.Drawing.Size(119, 29)
        Me.btnAddAttachment.TabIndex = 14
        Me.btnAddAttachment.Text = "&Browse"
        Me.btnAddAttachment.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvQualification)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(12, 328)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(458, 109)
        Me.Panel1.TabIndex = 229
        '
        'dgvQualification
        '
        Me.dgvQualification.AllowUserToAddRows = False
        Me.dgvQualification.AllowUserToDeleteRows = False
        Me.dgvQualification.AllowUserToResizeRows = False
        Me.dgvQualification.BackgroundColor = System.Drawing.Color.White
        Me.dgvQualification.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvQualification.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvQualification.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohDelete, Me.colhName, Me.colhSize, Me.objcolhDownload, Me.objcolhGUID, Me.objcolhScanUnkId})
        Me.dgvQualification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvQualification.Location = New System.Drawing.Point(0, 0)
        Me.dgvQualification.Name = "dgvQualification"
        Me.dgvQualification.RowHeadersVisible = False
        Me.dgvQualification.Size = New System.Drawing.Size(458, 109)
        Me.dgvQualification.TabIndex = 228
        '
        'objcohDelete
        '
        Me.objcohDelete.HeaderText = ""
        Me.objcohDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objcohDelete.Name = "objcohDelete"
        Me.objcohDelete.ReadOnly = True
        Me.objcohDelete.Width = 25
        '
        'colhName
        '
        Me.colhName.HeaderText = "File Name"
        Me.colhName.Name = "colhName"
        Me.colhName.ReadOnly = True
        Me.colhName.Width = 180
        '
        'colhSize
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhSize.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhSize.HeaderText = "File Size"
        Me.colhSize.Name = "colhSize"
        Me.colhSize.ReadOnly = True
        Me.colhSize.Width = 150
        '
        'objcolhDownload
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhDownload.DefaultCellStyle = DataGridViewCellStyle4
        Me.objcolhDownload.HeaderText = "Download"
        Me.objcolhDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhDownload.Name = "objcolhDownload"
        Me.objcolhDownload.ReadOnly = True
        Me.objcolhDownload.Text = "Download"
        Me.objcolhDownload.UseColumnTextForLinkValue = True
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "objcolhGUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.Visible = False
        '
        'objcolhScanUnkId
        '
        Me.objcolhScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Name = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Visible = False
        '
        'cboDocumentType
        '
        Me.cboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDocumentType.FormattingEnabled = True
        Me.cboDocumentType.Location = New System.Drawing.Point(162, 293)
        Me.cboDocumentType.Name = "cboDocumentType"
        Me.cboDocumentType.Size = New System.Drawing.Size(161, 21)
        Me.cboDocumentType.TabIndex = 226
        '
        'lblDocumentType
        '
        Me.lblDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentType.Location = New System.Drawing.Point(17, 295)
        Me.lblDocumentType.Name = "lblDocumentType"
        Me.lblDocumentType.Size = New System.Drawing.Size(139, 16)
        Me.lblDocumentType.TabIndex = 227
        Me.lblDocumentType.Text = "Document Type"
        Me.lblDocumentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkOtherQualification
        '
        Me.lnkOtherQualification.BackColor = System.Drawing.Color.Transparent
        Me.lnkOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkOtherQualification.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkOtherQualification.Location = New System.Drawing.Point(466, 7)
        Me.lnkOtherQualification.Name = "lnkOtherQualification"
        Me.lnkOtherQualification.Size = New System.Drawing.Size(233, 13)
        Me.lnkOtherQualification.TabIndex = 224
        Me.lnkOtherQualification.TabStop = True
        Me.lnkOtherQualification.Text = "Other Qualification /Short Courses"
        Me.lnkOtherQualification.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlOtherQualification
        '
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherResultCode)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherQualificationGrp)
        Me.pnlOtherQualification.Controls.Add(Me.txtOtherResultCode)
        Me.pnlOtherQualification.Controls.Add(Me.txtOtherQualificationGrp)
        Me.pnlOtherQualification.Controls.Add(Me.txtOtherQualification)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherQualification)
        Me.pnlOtherQualification.Location = New System.Drawing.Point(13, 101)
        Me.pnlOtherQualification.Name = "pnlOtherQualification"
        Me.pnlOtherQualification.Size = New System.Drawing.Size(457, 84)
        Me.pnlOtherQualification.TabIndex = 14
        Me.pnlOtherQualification.Visible = False
        '
        'lblOtherResultCode
        '
        Me.lblOtherResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherResultCode.Location = New System.Drawing.Point(5, 59)
        Me.lblOtherResultCode.Name = "lblOtherResultCode"
        Me.lblOtherResultCode.Size = New System.Drawing.Size(122, 16)
        Me.lblOtherResultCode.TabIndex = 223
        Me.lblOtherResultCode.Text = "Other Result Code"
        Me.lblOtherResultCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherQualificationGrp
        '
        Me.lblOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualificationGrp.Location = New System.Drawing.Point(5, 5)
        Me.lblOtherQualificationGrp.Name = "lblOtherQualificationGrp"
        Me.lblOtherQualificationGrp.Size = New System.Drawing.Size(122, 16)
        Me.lblOtherQualificationGrp.TabIndex = 223
        Me.lblOtherQualificationGrp.Text = "Oth. Qualif. Group"
        Me.lblOtherQualificationGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherResultCode
        '
        Me.txtOtherResultCode.Flags = 0
        Me.txtOtherResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherResultCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherResultCode.Location = New System.Drawing.Point(150, 59)
        Me.txtOtherResultCode.Name = "txtOtherResultCode"
        Me.txtOtherResultCode.Size = New System.Drawing.Size(253, 21)
        Me.txtOtherResultCode.TabIndex = 3
        '
        'txtOtherQualificationGrp
        '
        Me.txtOtherQualificationGrp.Flags = 0
        Me.txtOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualificationGrp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualificationGrp.Location = New System.Drawing.Point(150, 5)
        Me.txtOtherQualificationGrp.Name = "txtOtherQualificationGrp"
        Me.txtOtherQualificationGrp.Size = New System.Drawing.Size(253, 21)
        Me.txtOtherQualificationGrp.TabIndex = 1
        '
        'txtOtherQualification
        '
        Me.txtOtherQualification.Flags = 0
        Me.txtOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualification.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualification.Location = New System.Drawing.Point(150, 32)
        Me.txtOtherQualification.Name = "txtOtherQualification"
        Me.txtOtherQualification.Size = New System.Drawing.Size(253, 21)
        Me.txtOtherQualification.TabIndex = 2
        '
        'lblOtherQualification
        '
        Me.lblOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualification.Location = New System.Drawing.Point(5, 32)
        Me.lblOtherQualification.Name = "lblOtherQualification"
        Me.lblOtherQualification.Size = New System.Drawing.Size(122, 16)
        Me.lblOtherQualification.TabIndex = 211
        Me.lblOtherQualification.Text = "Oth. Qualif."
        Me.lblOtherQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchQualification
        '
        Me.objbtnSearchQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQualification.BorderSelected = False
        Me.objbtnSearchQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQualification.Location = New System.Drawing.Point(448, 132)
        Me.objbtnSearchQualification.Name = "objbtnSearchQualification"
        Me.objbtnSearchQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQualification.TabIndex = 224
        '
        'objbtnSearchProvider
        '
        Me.objbtnSearchProvider.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProvider.BorderSelected = False
        Me.objbtnSearchProvider.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProvider.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProvider.Location = New System.Drawing.Point(448, 266)
        Me.objbtnSearchProvider.Name = "objbtnSearchProvider"
        Me.objbtnSearchProvider.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProvider.TabIndex = 224
        '
        'objbtnSearchQGrp
        '
        Me.objbtnSearchQGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQGrp.BorderSelected = False
        Me.objbtnSearchQGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQGrp.Location = New System.Drawing.Point(448, 105)
        Me.objbtnSearchQGrp.Name = "objbtnSearchQGrp"
        Me.objbtnSearchQGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQGrp.TabIndex = 224
        '
        'nudLevel
        '
        Me.nudLevel.DecimalPlaces = 2
        Me.nudLevel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLevel.Location = New System.Drawing.Point(162, 186)
        Me.nudLevel.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.nudLevel.Name = "nudLevel"
        Me.nudLevel.Size = New System.Drawing.Size(95, 20)
        Me.nudLevel.TabIndex = 222
        Me.nudLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGPAcode
        '
        Me.lblGPAcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGPAcode.Location = New System.Drawing.Point(17, 189)
        Me.lblGPAcode.Name = "lblGPAcode"
        Me.lblGPAcode.Size = New System.Drawing.Size(60, 16)
        Me.lblGPAcode.TabIndex = 37
        Me.lblGPAcode.Text = "GPA/Points"
        Me.lblGPAcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResultCode
        '
        Me.cboResultCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultCode.DropDownWidth = 200
        Me.cboResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultCode.FormattingEnabled = True
        Me.cboResultCode.Location = New System.Drawing.Point(162, 159)
        Me.cboResultCode.Name = "cboResultCode"
        Me.cboResultCode.Size = New System.Drawing.Size(253, 21)
        Me.cboResultCode.TabIndex = 4
        '
        'lblResultCode
        '
        Me.lblResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultCode.Location = New System.Drawing.Point(17, 161)
        Me.lblResultCode.Name = "lblResultCode"
        Me.lblResultCode.Size = New System.Drawing.Size(139, 16)
        Me.lblResultCode.TabIndex = 35
        Me.lblResultCode.Text = "Result Code"
        Me.lblResultCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(271, 241)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(64, 16)
        Me.lblEndDate.TabIndex = 33
        Me.lblEndDate.Text = "Award Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(338, 239)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpEndDate.TabIndex = 8
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(162, 239)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpStartDate.TabIndex = 7
        '
        'objbtnAddInstitution
        '
        Me.objbtnAddInstitution.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddInstitution.BorderSelected = False
        Me.objbtnAddInstitution.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddInstitution.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddInstitution.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddInstitution.Location = New System.Drawing.Point(421, 266)
        Me.objbtnAddInstitution.Name = "objbtnAddInstitution"
        Me.objbtnAddInstitution.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddInstitution.TabIndex = 30
        '
        'cboInstitution
        '
        Me.cboInstitution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitution.FormattingEnabled = True
        Me.cboInstitution.Location = New System.Drawing.Point(162, 266)
        Me.cboInstitution.Name = "cboInstitution"
        Me.cboInstitution.Size = New System.Drawing.Size(253, 21)
        Me.cboInstitution.TabIndex = 9
        '
        'objstLine2
        '
        Me.objstLine2.BackColor = System.Drawing.Color.Transparent
        Me.objstLine2.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objstLine2.Location = New System.Drawing.Point(496, 84)
        Me.objstLine2.Name = "objstLine2"
        Me.objstLine2.Size = New System.Drawing.Size(198, 16)
        Me.objstLine2.TabIndex = 25
        Me.objstLine2.Text = "EZeeStraightLine1"
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(421, 105)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 6
        '
        'objbtnAddQulification
        '
        Me.objbtnAddQulification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddQulification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddQulification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddQulification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddQulification.BorderSelected = False
        Me.objbtnAddQulification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddQulification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddQulification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddQulification.Location = New System.Drawing.Point(421, 132)
        Me.objbtnAddQulification.Name = "objbtnAddQulification"
        Me.objbtnAddQulification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddQulification.TabIndex = 9
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(475, 36)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(12, 224)
        Me.objStLine1.TabIndex = 22
        Me.objStLine1.Text = "EZeeStraightLine1"
        '
        'lnQualificationInfo
        '
        Me.lnQualificationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnQualificationInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnQualificationInfo.Location = New System.Drawing.Point(8, 84)
        Me.lnQualificationInfo.Name = "lnQualificationInfo"
        Me.lnQualificationInfo.Size = New System.Drawing.Size(239, 17)
        Me.lnQualificationInfo.TabIndex = 3
        Me.lnQualificationInfo.Text = "Qualification Info"
        Me.lnQualificationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(239, 17)
        Me.lnEmployeeName.TabIndex = 0
        Me.lnEmployeeName.Text = "Employee Info"
        Me.lnEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(421, 61)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(162, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(253, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(17, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(139, 16)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(496, 132)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(198, 128)
        Me.txtRemark.TabIndex = 11
        '
        'txtAwardNo
        '
        Me.txtAwardNo.Flags = 0
        Me.txtAwardNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAwardNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAwardNo.Location = New System.Drawing.Point(162, 212)
        Me.txtAwardNo.Name = "txtAwardNo"
        Me.txtAwardNo.Size = New System.Drawing.Size(253, 21)
        Me.txtAwardNo.TabIndex = 6
        '
        'dtpCertificateDate
        '
        Me.dtpCertificateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCertificateDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCertificateDate.Location = New System.Drawing.Point(578, 59)
        Me.dtpCertificateDate.Name = "dtpCertificateDate"
        Me.dtpCertificateDate.Size = New System.Drawing.Size(114, 21)
        Me.dtpCertificateDate.TabIndex = 10
        '
        'cboCertificates
        '
        Me.cboCertificates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCertificates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCertificates.FormattingEnabled = True
        Me.cboCertificates.Location = New System.Drawing.Point(162, 132)
        Me.cboCertificates.Name = "cboCertificates"
        Me.cboCertificates.Size = New System.Drawing.Size(253, 21)
        Me.cboCertificates.TabIndex = 3
        '
        'cboCertificateGroup
        '
        Me.cboCertificateGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCertificateGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCertificateGroup.FormattingEnabled = True
        Me.cboCertificateGroup.Location = New System.Drawing.Point(162, 105)
        Me.cboCertificateGroup.Name = "cboCertificateGroup"
        Me.cboCertificateGroup.Size = New System.Drawing.Size(253, 21)
        Me.cboCertificateGroup.TabIndex = 2
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(17, 134)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(139, 16)
        Me.lblQualification.TabIndex = 7
        Me.lblQualification.Text = "Qualification/Award"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAwardDate
        '
        Me.lblAwardDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAwardDate.Location = New System.Drawing.Point(493, 63)
        Me.lblAwardDate.Name = "lblAwardDate"
        Me.lblAwardDate.Size = New System.Drawing.Size(79, 16)
        Me.lblAwardDate.TabIndex = 23
        Me.lblAwardDate.Text = "Date"
        Me.lblAwardDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQualificationGroup
        '
        Me.lblQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGroup.Location = New System.Drawing.Point(17, 107)
        Me.lblQualificationGroup.Name = "lblQualificationGroup"
        Me.lblQualificationGroup.Size = New System.Drawing.Size(139, 16)
        Me.lblQualificationGroup.TabIndex = 4
        Me.lblQualificationGroup.Text = "Qualification/Award Group"
        Me.lblQualificationGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(17, 241)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(139, 16)
        Me.lblStartDate.TabIndex = 10
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(493, 107)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(113, 16)
        Me.lblRemark.TabIndex = 26
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInstitution
        '
        Me.lblInstitution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstitution.Location = New System.Drawing.Point(17, 268)
        Me.lblInstitution.Name = "lblInstitution"
        Me.lblInstitution.Size = New System.Drawing.Size(139, 16)
        Me.lblInstitution.TabIndex = 14
        Me.lblInstitution.Text = "Provider Name"
        Me.lblInstitution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReferenceNo
        '
        Me.lblReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferenceNo.Location = New System.Drawing.Point(17, 214)
        Me.lblReferenceNo.Name = "lblReferenceNo"
        Me.lblReferenceNo.Size = New System.Drawing.Size(139, 16)
        Me.lblReferenceNo.TabIndex = 12
        Me.lblReferenceNo.Text = "Reference No"
        Me.lblReferenceNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherInstitute
        '
        Me.txtOtherInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherInstitute.Location = New System.Drawing.Point(162, 266)
        Me.txtOtherInstitute.Name = "txtOtherInstitute"
        Me.txtOtherInstitute.Size = New System.Drawing.Size(253, 21)
        Me.txtOtherInstitute.TabIndex = 231
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 446)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(704, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(492, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 12
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(595, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 13
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ofdAttachment
        '
        Me.ofdAttachment.FileName = "OpenFileDialog1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "File Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 180
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objcolhGUID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objcolhScanUnkId"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'frmQualifications
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 501)
        Me.Controls.Add(Me.pnlCertificates)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQualifications"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Qualification"
        Me.pnlCertificates.ResumeLayout(False)
        Me.gbEmployeeQualification.ResumeLayout(False)
        Me.gbEmployeeQualification.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvQualification, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOtherQualification.ResumeLayout(False)
        Me.pnlOtherQualification.PerformLayout()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlCertificates As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbEmployeeQualification As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents lblQualificationGroup As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblInstitution As System.Windows.Forms.Label
    Friend WithEvents lblReferenceNo As System.Windows.Forms.Label
    Friend WithEvents lblAwardDate As System.Windows.Forms.Label
    Friend WithEvents dtpCertificateDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboCertificates As System.Windows.Forms.ComboBox
    Friend WithEvents cboCertificateGroup As System.Windows.Forms.ComboBox
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAwardNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lnQualificationInfo As eZee.Common.eZeeLine
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnAddQulification As eZee.Common.eZeeGradientButton
    Friend WithEvents objstLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnAddInstitution As eZee.Common.eZeeGradientButton
    Friend WithEvents cboInstitution As System.Windows.Forms.ComboBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblGPAcode As System.Windows.Forms.Label
    Friend WithEvents cboResultCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultCode As System.Windows.Forms.Label
    Friend WithEvents nudLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents objbtnSearchQGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchProvider As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlOtherQualification As System.Windows.Forms.Panel
    Friend WithEvents lblOtherResultCode As System.Windows.Forms.Label
    Friend WithEvents lblOtherQualificationGrp As System.Windows.Forms.Label
    Friend WithEvents txtOtherResultCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherQualificationGrp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherQualification As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOtherQualification As System.Windows.Forms.Label
    Friend WithEvents lnkOtherQualification As System.Windows.Forms.LinkLabel
    Friend WithEvents cboDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocumentType As System.Windows.Forms.Label
    Friend WithEvents dgvQualification As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnAddAttachment As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ofdAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sfdAttachment As System.Windows.Forms.SaveFileDialog
    Friend WithEvents objcohDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtOtherInstitute As System.Windows.Forms.TextBox
End Class
