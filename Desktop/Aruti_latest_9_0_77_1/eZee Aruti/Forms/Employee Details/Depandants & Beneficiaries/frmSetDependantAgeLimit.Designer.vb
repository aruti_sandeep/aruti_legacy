﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetDependantAgeLimit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetDependantAgeLimit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvAgeLimit = New System.Windows.Forms.DataGridView
        Me.objeZeeFooter = New eZee.Common.eZeeFooter
        Me.objlblNote = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRelationName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAgeLimit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhmedicalbenefit = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhCRBenefit = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhMinAgeForCRBenefit = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhMaxCRCount = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objdgcolhRelationId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvAgeLimit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objeZeeFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvAgeLimit)
        Me.pnlMainInfo.Controls.Add(Me.objeZeeFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(732, 275)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgvAgeLimit
        '
        Me.dgvAgeLimit.AllowUserToAddRows = False
        Me.dgvAgeLimit.AllowUserToDeleteRows = False
        Me.dgvAgeLimit.AllowUserToResizeColumns = False
        Me.dgvAgeLimit.AllowUserToResizeRows = False
        Me.dgvAgeLimit.BackgroundColor = System.Drawing.Color.White
        Me.dgvAgeLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAgeLimit.ColumnHeadersHeight = 22
        Me.dgvAgeLimit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAgeLimit.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhRelationName, Me.dgcolhAgeLimit, Me.dgColhmedicalbenefit, Me.dgcolhCRBenefit, Me.dgcolhMinAgeForCRBenefit, Me.dgcolhMaxCRCount, Me.objdgcolhRelationId})
        Me.dgvAgeLimit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAgeLimit.Location = New System.Drawing.Point(0, 0)
        Me.dgvAgeLimit.Name = "dgvAgeLimit"
        Me.dgvAgeLimit.RowHeadersVisible = False
        Me.dgvAgeLimit.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAgeLimit.Size = New System.Drawing.Size(732, 220)
        Me.dgvAgeLimit.TabIndex = 0
        '
        'objeZeeFooter
        '
        Me.objeZeeFooter.BorderColor = System.Drawing.Color.Silver
        Me.objeZeeFooter.Controls.Add(Me.objlblNote)
        Me.objeZeeFooter.Controls.Add(Me.btnSave)
        Me.objeZeeFooter.Controls.Add(Me.btnClose)
        Me.objeZeeFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objeZeeFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objeZeeFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objeZeeFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.Location = New System.Drawing.Point(0, 220)
        Me.objeZeeFooter.Name = "objeZeeFooter"
        Me.objeZeeFooter.Size = New System.Drawing.Size(732, 55)
        Me.objeZeeFooter.TabIndex = 122
        '
        'objlblNote
        '
        Me.objlblNote.BackColor = System.Drawing.Color.Transparent
        Me.objlblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblNote.ForeColor = System.Drawing.Color.Maroon
        Me.objlblNote.Location = New System.Drawing.Point(7, 13)
        Me.objlblNote.Name = "objlblNote"
        Me.objlblNote.Size = New System.Drawing.Size(253, 28)
        Me.objlblNote.TabIndex = 119
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(526, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 118
        Me.btnSave.Text = "&Save && Close"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(626, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 117
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Relation"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.FillWeight = 120.0!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Age Limit"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhRelationId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'dgcolhRelationName
        '
        Me.dgcolhRelationName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhRelationName.HeaderText = "Relation"
        Me.dgcolhRelationName.Name = "dgcolhRelationName"
        Me.dgcolhRelationName.ReadOnly = True
        Me.dgcolhRelationName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAgeLimit
        '
        Me.dgcolhAgeLimit.FillWeight = 120.0!
        Me.dgcolhAgeLimit.HeaderText = "Age Limit"
        Me.dgcolhAgeLimit.Name = "dgcolhAgeLimit"
        Me.dgcolhAgeLimit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAgeLimit.Width = 90
        '
        'dgColhmedicalbenefit
        '
        Me.dgColhmedicalbenefit.HeaderText = "Medical Benefit"
        Me.dgColhmedicalbenefit.Name = "dgColhmedicalbenefit"
        '
        'dgcolhCRBenefit
        '
        Me.dgcolhCRBenefit.HeaderText = "CR Benefit"
        Me.dgcolhCRBenefit.Name = "dgcolhCRBenefit"
        Me.dgcolhCRBenefit.Width = 80
        '
        'dgcolhMinAgeForCRBenefit
        '
        Me.dgcolhMinAgeForCRBenefit.AllowNegative = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.dgcolhMinAgeForCRBenefit.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhMinAgeForCRBenefit.HeaderText = "Min. Age Limit for CR Benefit"
        Me.dgcolhMinAgeForCRBenefit.Name = "dgcolhMinAgeForCRBenefit"
        Me.dgcolhMinAgeForCRBenefit.ReadOnly = True
        Me.dgcolhMinAgeForCRBenefit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMinAgeForCRBenefit.Width = 150
        '
        'dgcolhMaxCRCount
        '
        Me.dgcolhMaxCRCount.AllowNegative = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F0"
        Me.dgcolhMaxCRCount.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhMaxCRCount.HeaderText = "Max. CR Dependants Count"
        Me.dgcolhMaxCRCount.Name = "dgcolhMaxCRCount"
        Me.dgcolhMaxCRCount.ReadOnly = True
        Me.dgcolhMaxCRCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMaxCRCount.Width = 150
        '
        'objdgcolhRelationId
        '
        Me.objdgcolhRelationId.HeaderText = "objdgcolhRelationId"
        Me.objdgcolhRelationId.Name = "objdgcolhRelationId"
        Me.objdgcolhRelationId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhRelationId.Visible = False
        '
        'frmSetDependantAgeLimit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(732, 275)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSetDependantAgeLimit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Dependants Rule"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvAgeLimit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objeZeeFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents dgvAgeLimit As System.Windows.Forms.DataGridView
    Friend WithEvents objeZeeFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objlblNote As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRelationName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAgeLimit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhmedicalbenefit As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhCRBenefit As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhMinAgeForCRBenefit As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhMaxCRCount As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objdgcolhRelationId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
