﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDependantsAndBeneficiaries
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDependantsAndBeneficiaries))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlDependantsAndBeneficiaries = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbDBInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.objlblAge = New System.Windows.Forms.Label
        Me.lblGender = New System.Windows.Forms.Label
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.txtAge = New eZee.TextBox.NumericTextBox
        Me.txtAgeLimit = New eZee.TextBox.NumericTextBox
        Me.lblLimit = New System.Windows.Forms.Label
        Me.lblInfo = New System.Windows.Forms.Label
        Me.txtTotBeneficiaries = New eZee.TextBox.NumericTextBox
        Me.objbtnAddRelation = New eZee.Common.eZeeGradientButton
        Me.chkBeneficiaries = New System.Windows.Forms.CheckBox
        Me.lnkCopyAddress = New System.Windows.Forms.LinkLabel
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objStLine = New eZee.Common.eZeeStraightLine
        Me.imgDependants = New eZee.Common.eZeeImageControl
        Me.tabcOtherDetails = New System.Windows.Forms.TabControl
        Me.tabpMembership = New System.Windows.Forms.TabPage
        Me.gbAddMembership = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objAddMedMem = New eZee.Common.eZeeGradientButton
        Me.objbtnAddMemCategory = New eZee.Common.eZeeGradientButton
        Me.cboMemCategory = New System.Windows.Forms.ComboBox
        Me.lblMembershipCategory = New System.Windows.Forms.Label
        Me.lvMembershipInfo = New eZee.Common.eZeeListView(Me.components)
        Me.colhMembershipType = New System.Windows.Forms.ColumnHeader
        Me.colhMembership = New System.Windows.Forms.ColumnHeader
        Me.colhMembershipNo = New System.Windows.Forms.ColumnHeader
        Me.objcolhMembershipCatId = New System.Windows.Forms.ColumnHeader
        Me.objcolhMembershipUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhMGUID = New System.Windows.Forms.ColumnHeader
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.cboMedicalNo = New System.Windows.Forms.ComboBox
        Me.btnEditMembership = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblDBMedicalMembershipNo = New System.Windows.Forms.Label
        Me.btnAddMembership = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtMembershipNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblMemberNo = New System.Windows.Forms.Label
        Me.tabpBenefitInfo = New System.Windows.Forms.TabPage
        Me.gbBenefitInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRemainingPercent = New eZee.TextBox.NumericTextBox
        Me.lblTotalRemainingPercent = New System.Windows.Forms.Label
        Me.lblTotalPercent = New System.Windows.Forms.Label
        Me.txtTotalPercentage = New eZee.TextBox.NumericTextBox
        Me.objbtnAddBenefitType = New eZee.Common.eZeeGradientButton
        Me.objbtnAddBenefitGroup = New eZee.Common.eZeeGradientButton
        Me.lvBenefit = New eZee.Common.eZeeListView(Me.components)
        Me.colhBenefitGroup = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitType = New System.Windows.Forms.ColumnHeader
        Me.colhValueBasis = New System.Windows.Forms.ColumnHeader
        Me.colhPercent = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.objcolhBenefitGrpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhBenefitId = New System.Windows.Forms.ColumnHeader
        Me.objcolhValueBasisId = New System.Windows.Forms.ColumnHeader
        Me.objcolhBGUID = New System.Windows.Forms.ColumnHeader
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.objstLine2 = New eZee.Common.eZeeStraightLine
        Me.btnDeleteBenefit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditBenefit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddBenefit = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboValueBasis = New System.Windows.Forms.ComboBox
        Me.lblValueBasis = New System.Windows.Forms.Label
        Me.lblBenefitGroup = New System.Windows.Forms.Label
        Me.cboBenefitGroup = New System.Windows.Forms.ComboBox
        Me.cboBenefitType = New System.Windows.Forms.ComboBox
        Me.lblBenefitsInPercent = New System.Windows.Forms.Label
        Me.txtBenefitPercent = New eZee.TextBox.NumericTextBox
        Me.lblDBBenefitInAmount = New System.Windows.Forms.Label
        Me.txtBenefitAmount = New eZee.TextBox.NumericTextBox
        Me.lblDBBenefitType = New System.Windows.Forms.Label
        Me.tabAttachment = New System.Windows.Forms.TabPage
        Me.dbDocumentAttachment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAttachmentDate = New System.Windows.Forms.Label
        Me.dtpAttachmentDate = New System.Windows.Forms.DateTimePicker
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.dgvDepedantAttachment = New System.Windows.Forms.DataGridView
        Me.objcohDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSize = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnAddAttachment = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboDocumentType = New System.Windows.Forms.ComboBox
        Me.lblDocumentType = New System.Windows.Forms.Label
        Me.tapbBankDetails = New System.Windows.Forms.TabPage
        Me.gbBankDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDown = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblMode = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.lblPerc = New System.Windows.Forms.Label
        Me.lblSalaryDistrib = New System.Windows.Forms.Label
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.objbtnAddAccType = New eZee.Common.eZeeGradientButton
        Me.txtAccountNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.lblAccountType = New System.Windows.Forms.Label
        Me.cboAccountType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchBankBranch = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchBankGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnAddBranch = New eZee.Common.eZeeGradientButton
        Me.objbtnAddBankGroup = New eZee.Common.eZeeGradientButton
        Me.cboBankBranch = New System.Windows.Forms.ComboBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.btnDeleteEmpDetails = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEditEmpDetails = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddEmpDetails = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.lvEmpBankList = New eZee.Common.eZeeListView(Me.components)
        Me.colhID = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhBankGrp = New System.Windows.Forms.ColumnHeader
        Me.colhBankBranch = New System.Windows.Forms.ColumnHeader
        Me.colhAccType = New System.Windows.Forms.ColumnHeader
        Me.colhAccNo = New System.Windows.Forms.ColumnHeader
        Me.colhPerc = New System.Windows.Forms.ColumnHeader
        Me.colhGUID = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhModeID = New System.Windows.Forms.ColumnHeader
        Me.lblBank = New System.Windows.Forms.Label
        Me.cboBankGroup = New System.Windows.Forms.ComboBox
        Me.lblBankGroup = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnAddReminder = New eZee.Common.eZeeGradientButton
        Me.txtDBFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.lblDBFirstName = New System.Windows.Forms.Label
        Me.lblDBMiddleName = New System.Windows.Forms.Label
        Me.lblDBLastName = New System.Windows.Forms.Label
        Me.txtDBMiddleName = New eZee.TextBox.AlphanumericTextBox
        Me.txtDBLastName = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddressLine1 = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboDBRelation = New System.Windows.Forms.ComboBox
        Me.lblDBBirthDate = New System.Windows.Forms.Label
        Me.lblDBRelation = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblDBAddress = New System.Windows.Forms.Label
        Me.dtpDBBirthDate = New System.Windows.Forms.DateTimePicker
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboDBPostCountry = New System.Windows.Forms.ComboBox
        Me.txtDBTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.cboPostState = New System.Windows.Forms.ComboBox
        Me.cboDBPostTown = New System.Windows.Forms.ComboBox
        Me.lblDBNationality = New System.Windows.Forms.Label
        Me.lblDBPostCode = New System.Windows.Forms.Label
        Me.lblState = New System.Windows.Forms.Label
        Me.lblDBResidentialNo = New System.Windows.Forms.Label
        Me.cboPostCode = New System.Windows.Forms.ComboBox
        Me.lblDBEmail = New System.Windows.Forms.Label
        Me.lblDBPostCountry = New System.Windows.Forms.Label
        Me.lblDBPostBox = New System.Windows.Forms.Label
        Me.txtDBIdNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblDBIdNo = New System.Windows.Forms.Label
        Me.txtDBPostBox = New eZee.TextBox.AlphanumericTextBox
        Me.lblDBMobileNo = New System.Windows.Forms.Label
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblDBPostTown = New System.Windows.Forms.Label
        Me.cboDBNationality = New System.Windows.Forms.ComboBox
        Me.txtDBPersonalNo = New eZee.TextBox.AlphanumericTextBox
        Me.objeZeeFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ofdAttachment = New System.Windows.Forms.OpenFileDialog
        Me.sfdAttachment = New System.Windows.Forms.SaveFileDialog
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboDBLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblDBLoanScheme = New System.Windows.Forms.Label
        Me.cboDBTranHead = New System.Windows.Forms.ComboBox
        Me.lblDBTranHead = New System.Windows.Forms.Label
        Me.pnlDependantsAndBeneficiaries.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbDBInfo.SuspendLayout()
        Me.tabcOtherDetails.SuspendLayout()
        Me.tabpMembership.SuspendLayout()
        Me.gbAddMembership.SuspendLayout()
        Me.tabpBenefitInfo.SuspendLayout()
        Me.gbBenefitInfo.SuspendLayout()
        Me.tabAttachment.SuspendLayout()
        Me.dbDocumentAttachment.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvDepedantAttachment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tapbBankDetails.SuspendLayout()
        Me.gbBankDetails.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.objeZeeFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlDependantsAndBeneficiaries
        '
        Me.pnlDependantsAndBeneficiaries.Controls.Add(Me.Panel1)
        Me.pnlDependantsAndBeneficiaries.Controls.Add(Me.objeZeeFooter)
        Me.pnlDependantsAndBeneficiaries.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDependantsAndBeneficiaries.Location = New System.Drawing.Point(0, 0)
        Me.pnlDependantsAndBeneficiaries.Name = "pnlDependantsAndBeneficiaries"
        Me.pnlDependantsAndBeneficiaries.Size = New System.Drawing.Size(811, 570)
        Me.pnlDependantsAndBeneficiaries.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbDBInfo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(811, 515)
        Me.Panel1.TabIndex = 2
        '
        'gbDBInfo
        '
        Me.gbDBInfo.BorderColor = System.Drawing.Color.Black
        Me.gbDBInfo.Checked = False
        Me.gbDBInfo.CollapseAllExceptThis = False
        Me.gbDBInfo.CollapsedHoverImage = Nothing
        Me.gbDBInfo.CollapsedNormalImage = Nothing
        Me.gbDBInfo.CollapsedPressedImage = Nothing
        Me.gbDBInfo.CollapseOnLoad = False
        Me.gbDBInfo.Controls.Add(Me.cboDBTranHead)
        Me.gbDBInfo.Controls.Add(Me.lblDBTranHead)
        Me.gbDBInfo.Controls.Add(Me.cboDBLoanScheme)
        Me.gbDBInfo.Controls.Add(Me.lblDBLoanScheme)
        Me.gbDBInfo.Controls.Add(Me.lblEffectiveDate)
        Me.gbDBInfo.Controls.Add(Me.dtpEffectiveDate)
        Me.gbDBInfo.Controls.Add(Me.objlblAge)
        Me.gbDBInfo.Controls.Add(Me.lblGender)
        Me.gbDBInfo.Controls.Add(Me.cboGender)
        Me.gbDBInfo.Controls.Add(Me.txtAge)
        Me.gbDBInfo.Controls.Add(Me.txtAgeLimit)
        Me.gbDBInfo.Controls.Add(Me.lblLimit)
        Me.gbDBInfo.Controls.Add(Me.lblInfo)
        Me.gbDBInfo.Controls.Add(Me.txtTotBeneficiaries)
        Me.gbDBInfo.Controls.Add(Me.objbtnAddRelation)
        Me.gbDBInfo.Controls.Add(Me.chkBeneficiaries)
        Me.gbDBInfo.Controls.Add(Me.lnkCopyAddress)
        Me.gbDBInfo.Controls.Add(Me.objelLine1)
        Me.gbDBInfo.Controls.Add(Me.objStLine)
        Me.gbDBInfo.Controls.Add(Me.imgDependants)
        Me.gbDBInfo.Controls.Add(Me.tabcOtherDetails)
        Me.gbDBInfo.Controls.Add(Me.objbtnAddReminder)
        Me.gbDBInfo.Controls.Add(Me.txtDBFirstName)
        Me.gbDBInfo.Controls.Add(Me.lblDBFirstName)
        Me.gbDBInfo.Controls.Add(Me.lblDBMiddleName)
        Me.gbDBInfo.Controls.Add(Me.lblDBLastName)
        Me.gbDBInfo.Controls.Add(Me.txtDBMiddleName)
        Me.gbDBInfo.Controls.Add(Me.txtDBLastName)
        Me.gbDBInfo.Controls.Add(Me.txtAddressLine1)
        Me.gbDBInfo.Controls.Add(Me.cboEmployee)
        Me.gbDBInfo.Controls.Add(Me.cboDBRelation)
        Me.gbDBInfo.Controls.Add(Me.lblDBBirthDate)
        Me.gbDBInfo.Controls.Add(Me.lblDBRelation)
        Me.gbDBInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbDBInfo.Controls.Add(Me.lblDBAddress)
        Me.gbDBInfo.Controls.Add(Me.dtpDBBirthDate)
        Me.gbDBInfo.Controls.Add(Me.lblEmployee)
        Me.gbDBInfo.Controls.Add(Me.cboDBPostCountry)
        Me.gbDBInfo.Controls.Add(Me.txtDBTelNo)
        Me.gbDBInfo.Controls.Add(Me.cboPostState)
        Me.gbDBInfo.Controls.Add(Me.cboDBPostTown)
        Me.gbDBInfo.Controls.Add(Me.lblDBNationality)
        Me.gbDBInfo.Controls.Add(Me.lblDBPostCode)
        Me.gbDBInfo.Controls.Add(Me.lblState)
        Me.gbDBInfo.Controls.Add(Me.lblDBResidentialNo)
        Me.gbDBInfo.Controls.Add(Me.cboPostCode)
        Me.gbDBInfo.Controls.Add(Me.lblDBEmail)
        Me.gbDBInfo.Controls.Add(Me.lblDBPostCountry)
        Me.gbDBInfo.Controls.Add(Me.lblDBPostBox)
        Me.gbDBInfo.Controls.Add(Me.txtDBIdNo)
        Me.gbDBInfo.Controls.Add(Me.lblDBIdNo)
        Me.gbDBInfo.Controls.Add(Me.txtDBPostBox)
        Me.gbDBInfo.Controls.Add(Me.lblDBMobileNo)
        Me.gbDBInfo.Controls.Add(Me.txtEmail)
        Me.gbDBInfo.Controls.Add(Me.lblDBPostTown)
        Me.gbDBInfo.Controls.Add(Me.cboDBNationality)
        Me.gbDBInfo.Controls.Add(Me.txtDBPersonalNo)
        Me.gbDBInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbDBInfo.ExpandedHoverImage = Nothing
        Me.gbDBInfo.ExpandedNormalImage = Nothing
        Me.gbDBInfo.ExpandedPressedImage = Nothing
        Me.gbDBInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDBInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDBInfo.HeaderHeight = 25
        Me.gbDBInfo.HeaderMessage = ""
        Me.gbDBInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDBInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDBInfo.HeightOnCollapse = 0
        Me.gbDBInfo.LeftTextSpace = 0
        Me.gbDBInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbDBInfo.Name = "gbDBInfo"
        Me.gbDBInfo.OpenHeight = 460
        Me.gbDBInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDBInfo.ShowBorder = True
        Me.gbDBInfo.ShowCheckBox = False
        Me.gbDBInfo.ShowCollapseButton = False
        Me.gbDBInfo.ShowDefaultBorderColor = True
        Me.gbDBInfo.ShowDownButton = False
        Me.gbDBInfo.ShowHeader = True
        Me.gbDBInfo.Size = New System.Drawing.Size(811, 515)
        Me.gbDBInfo.TabIndex = 1
        Me.gbDBInfo.Temp = 0
        Me.gbDBInfo.Text = "Dependants And Beneficiaries"
        Me.gbDBInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(623, 234)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(62, 15)
        Me.lblEffectiveDate.TabIndex = 61
        Me.lblEffectiveDate.Text = "Eff. Date"
        Me.lblEffectiveDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(691, 231)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpEffectiveDate.TabIndex = 60
        '
        'objlblAge
        '
        Me.objlblAge.BackColor = System.Drawing.Color.Transparent
        Me.objlblAge.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAge.Location = New System.Drawing.Point(544, 5)
        Me.objlblAge.Name = "objlblAge"
        Me.objlblAge.Size = New System.Drawing.Size(255, 15)
        Me.objlblAge.TabIndex = 58
        Me.objlblAge.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(8, 182)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(73, 15)
        Me.lblGender.TabIndex = 56
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(87, 180)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(176, 21)
        Me.cboGender.TabIndex = 55
        '
        'txtAge
        '
        Me.txtAge.AllowNegative = True
        Me.txtAge.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAge.DigitsInGroup = 0
        Me.txtAge.Flags = 0
        Me.txtAge.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAge.Location = New System.Drawing.Point(226, 150)
        Me.txtAge.MaxDecimalPlaces = 0
        Me.txtAge.MaxWholeDigits = 21
        Me.txtAge.Name = "txtAge"
        Me.txtAge.Prefix = ""
        Me.txtAge.RangeMax = 1.7976931348623157E+308
        Me.txtAge.RangeMin = -1.7976931348623157E+308
        Me.txtAge.ReadOnly = True
        Me.txtAge.Size = New System.Drawing.Size(37, 21)
        Me.txtAge.TabIndex = 53
        Me.txtAge.Text = "0"
        Me.txtAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtAge.Visible = False
        '
        'txtAgeLimit
        '
        Me.txtAgeLimit.AllowNegative = True
        Me.txtAgeLimit.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAgeLimit.DigitsInGroup = 0
        Me.txtAgeLimit.Flags = 0
        Me.txtAgeLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgeLimit.Location = New System.Drawing.Point(349, 205)
        Me.txtAgeLimit.MaxDecimalPlaces = 0
        Me.txtAgeLimit.MaxWholeDigits = 21
        Me.txtAgeLimit.Name = "txtAgeLimit"
        Me.txtAgeLimit.Prefix = ""
        Me.txtAgeLimit.RangeMax = 1.7976931348623157E+308
        Me.txtAgeLimit.RangeMin = -1.7976931348623157E+308
        Me.txtAgeLimit.ReadOnly = True
        Me.txtAgeLimit.Size = New System.Drawing.Size(39, 21)
        Me.txtAgeLimit.TabIndex = 51
        Me.txtAgeLimit.Text = "0"
        Me.txtAgeLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblLimit
        '
        Me.lblLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLimit.Location = New System.Drawing.Point(274, 208)
        Me.lblLimit.Name = "lblLimit"
        Me.lblLimit.Size = New System.Drawing.Size(62, 15)
        Me.lblLimit.TabIndex = 50
        Me.lblLimit.Text = "Limit"
        Me.lblLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInfo
        '
        Me.lblInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.Location = New System.Drawing.Point(427, 233)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(101, 16)
        Me.lblInfo.TabIndex = 43
        Me.lblInfo.Text = "Total Beneficiaries"
        Me.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblInfo.Visible = False
        '
        'txtTotBeneficiaries
        '
        Me.txtTotBeneficiaries.AllowNegative = True
        Me.txtTotBeneficiaries.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotBeneficiaries.DigitsInGroup = 0
        Me.txtTotBeneficiaries.Flags = 0
        Me.txtTotBeneficiaries.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotBeneficiaries.Location = New System.Drawing.Point(582, 231)
        Me.txtTotBeneficiaries.MaxDecimalPlaces = 0
        Me.txtTotBeneficiaries.MaxWholeDigits = 21
        Me.txtTotBeneficiaries.Name = "txtTotBeneficiaries"
        Me.txtTotBeneficiaries.Prefix = ""
        Me.txtTotBeneficiaries.RangeMax = 1.7976931348623157E+308
        Me.txtTotBeneficiaries.RangeMin = -1.7976931348623157E+308
        Me.txtTotBeneficiaries.ReadOnly = True
        Me.txtTotBeneficiaries.Size = New System.Drawing.Size(32, 21)
        Me.txtTotBeneficiaries.TabIndex = 44
        Me.txtTotBeneficiaries.Text = "0"
        Me.txtTotBeneficiaries.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtTotBeneficiaries.Visible = False
        '
        'objbtnAddRelation
        '
        Me.objbtnAddRelation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddRelation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddRelation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddRelation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddRelation.BorderSelected = False
        Me.objbtnAddRelation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddRelation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddRelation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddRelation.Location = New System.Drawing.Point(242, 208)
        Me.objbtnAddRelation.Name = "objbtnAddRelation"
        Me.objbtnAddRelation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddRelation.TabIndex = 16
        '
        'chkBeneficiaries
        '
        Me.chkBeneficiaries.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBeneficiaries.Location = New System.Drawing.Point(623, 36)
        Me.chkBeneficiaries.Name = "chkBeneficiaries"
        Me.chkBeneficiaries.Size = New System.Drawing.Size(176, 16)
        Me.chkBeneficiaries.TabIndex = 20
        Me.chkBeneficiaries.Text = "Treat as Beneficiaries"
        Me.chkBeneficiaries.UseVisualStyleBackColor = True
        '
        'lnkCopyAddress
        '
        Me.lnkCopyAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCopyAddress.Location = New System.Drawing.Point(427, 36)
        Me.lnkCopyAddress.Name = "lnkCopyAddress"
        Me.lnkCopyAddress.Size = New System.Drawing.Size(187, 16)
        Me.lnkCopyAddress.TabIndex = 19
        Me.lnkCopyAddress.TabStop = True
        Me.lnkCopyAddress.Text = "Copy Address from Employee"
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 57)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(396, 12)
        Me.objelLine1.TabIndex = 4
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine
        '
        Me.objStLine.BackColor = System.Drawing.Color.Transparent
        Me.objStLine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine.ForeColor = System.Drawing.Color.DarkGray
        Me.objStLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine.Location = New System.Drawing.Point(416, 33)
        Me.objStLine.Name = "objStLine"
        Me.objStLine.Size = New System.Drawing.Size(5, 183)
        Me.objStLine.TabIndex = 18
        '
        'imgDependants
        '
        Me.imgDependants._FileMode = False
        Me.imgDependants._Image = Nothing
        Me.imgDependants._ShowAddButton = True
        Me.imgDependants._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.imgDependants._ShowDeleteButton = True
        Me.imgDependants.BackColor = System.Drawing.Color.Transparent
        Me.imgDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgDependants.Location = New System.Drawing.Point(272, 72)
        Me.imgDependants.Name = "imgDependants"
        Me.imgDependants.Size = New System.Drawing.Size(142, 117)
        Me.imgDependants.TabIndex = 17
        '
        'tabcOtherDetails
        '
        Me.tabcOtherDetails.Controls.Add(Me.tabpMembership)
        Me.tabcOtherDetails.Controls.Add(Me.tabpBenefitInfo)
        Me.tabcOtherDetails.Controls.Add(Me.tabAttachment)
        Me.tabcOtherDetails.Controls.Add(Me.tapbBankDetails)
        Me.tabcOtherDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcOtherDetails.Location = New System.Drawing.Point(3, 262)
        Me.tabcOtherDetails.Name = "tabcOtherDetails"
        Me.tabcOtherDetails.SelectedIndex = 0
        Me.tabcOtherDetails.Size = New System.Drawing.Size(808, 251)
        Me.tabcOtherDetails.TabIndex = 0
        '
        'tabpMembership
        '
        Me.tabpMembership.Controls.Add(Me.gbAddMembership)
        Me.tabpMembership.Location = New System.Drawing.Point(4, 22)
        Me.tabpMembership.Name = "tabpMembership"
        Me.tabpMembership.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpMembership.Size = New System.Drawing.Size(800, 235)
        Me.tabpMembership.TabIndex = 0
        Me.tabpMembership.Text = "Membership Info"
        Me.tabpMembership.UseVisualStyleBackColor = True
        '
        'gbAddMembership
        '
        Me.gbAddMembership.BorderColor = System.Drawing.Color.Black
        Me.gbAddMembership.Checked = False
        Me.gbAddMembership.CollapseAllExceptThis = False
        Me.gbAddMembership.CollapsedHoverImage = Nothing
        Me.gbAddMembership.CollapsedNormalImage = Nothing
        Me.gbAddMembership.CollapsedPressedImage = Nothing
        Me.gbAddMembership.CollapseOnLoad = False
        Me.gbAddMembership.Controls.Add(Me.objAddMedMem)
        Me.gbAddMembership.Controls.Add(Me.objbtnAddMemCategory)
        Me.gbAddMembership.Controls.Add(Me.cboMemCategory)
        Me.gbAddMembership.Controls.Add(Me.lblMembershipCategory)
        Me.gbAddMembership.Controls.Add(Me.lvMembershipInfo)
        Me.gbAddMembership.Controls.Add(Me.objStLine1)
        Me.gbAddMembership.Controls.Add(Me.btnDelete)
        Me.gbAddMembership.Controls.Add(Me.objelLine3)
        Me.gbAddMembership.Controls.Add(Me.cboMedicalNo)
        Me.gbAddMembership.Controls.Add(Me.btnEditMembership)
        Me.gbAddMembership.Controls.Add(Me.lblDBMedicalMembershipNo)
        Me.gbAddMembership.Controls.Add(Me.btnAddMembership)
        Me.gbAddMembership.Controls.Add(Me.txtMembershipNo)
        Me.gbAddMembership.Controls.Add(Me.lblMemberNo)
        Me.gbAddMembership.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAddMembership.ExpandedHoverImage = Nothing
        Me.gbAddMembership.ExpandedNormalImage = Nothing
        Me.gbAddMembership.ExpandedPressedImage = Nothing
        Me.gbAddMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAddMembership.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAddMembership.HeaderHeight = 25
        Me.gbAddMembership.HeaderMessage = ""
        Me.gbAddMembership.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAddMembership.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAddMembership.HeightOnCollapse = 0
        Me.gbAddMembership.LeftTextSpace = 0
        Me.gbAddMembership.Location = New System.Drawing.Point(3, 3)
        Me.gbAddMembership.Name = "gbAddMembership"
        Me.gbAddMembership.OpenHeight = 300
        Me.gbAddMembership.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAddMembership.ShowBorder = True
        Me.gbAddMembership.ShowCheckBox = False
        Me.gbAddMembership.ShowCollapseButton = False
        Me.gbAddMembership.ShowDefaultBorderColor = True
        Me.gbAddMembership.ShowDownButton = False
        Me.gbAddMembership.ShowHeader = True
        Me.gbAddMembership.Size = New System.Drawing.Size(794, 229)
        Me.gbAddMembership.TabIndex = 0
        Me.gbAddMembership.Temp = 0
        Me.gbAddMembership.Text = "Memberships"
        Me.gbAddMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objAddMedMem
        '
        Me.objAddMedMem.BackColor = System.Drawing.Color.Transparent
        Me.objAddMedMem.BackColor1 = System.Drawing.Color.Transparent
        Me.objAddMedMem.BackColor2 = System.Drawing.Color.Transparent
        Me.objAddMedMem.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAddMedMem.BorderSelected = False
        Me.objAddMedMem.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAddMedMem.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objAddMedMem.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAddMedMem.Location = New System.Drawing.Point(244, 60)
        Me.objAddMedMem.Name = "objAddMedMem"
        Me.objAddMedMem.Size = New System.Drawing.Size(21, 21)
        Me.objAddMedMem.TabIndex = 6
        '
        'objbtnAddMemCategory
        '
        Me.objbtnAddMemCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddMemCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddMemCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddMemCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddMemCategory.BorderSelected = False
        Me.objbtnAddMemCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddMemCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddMemCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddMemCategory.Location = New System.Drawing.Point(244, 33)
        Me.objbtnAddMemCategory.Name = "objbtnAddMemCategory"
        Me.objbtnAddMemCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddMemCategory.TabIndex = 3
        '
        'cboMemCategory
        '
        Me.cboMemCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMemCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMemCategory.FormattingEnabled = True
        Me.cboMemCategory.Location = New System.Drawing.Point(107, 33)
        Me.cboMemCategory.Name = "cboMemCategory"
        Me.cboMemCategory.Size = New System.Drawing.Size(131, 21)
        Me.cboMemCategory.TabIndex = 2
        '
        'lblMembershipCategory
        '
        Me.lblMembershipCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipCategory.Location = New System.Drawing.Point(8, 36)
        Me.lblMembershipCategory.Name = "lblMembershipCategory"
        Me.lblMembershipCategory.Size = New System.Drawing.Size(93, 15)
        Me.lblMembershipCategory.TabIndex = 1
        Me.lblMembershipCategory.Text = "Mem. Category"
        Me.lblMembershipCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvMembershipInfo
        '
        Me.lvMembershipInfo.BackColorOnChecked = True
        Me.lvMembershipInfo.ColumnHeaders = Nothing
        Me.lvMembershipInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhMembershipType, Me.colhMembership, Me.colhMembershipNo, Me.objcolhMembershipCatId, Me.objcolhMembershipUnkid, Me.objcolhMGUID})
        Me.lvMembershipInfo.CompulsoryColumns = ""
        Me.lvMembershipInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvMembershipInfo.FullRowSelect = True
        Me.lvMembershipInfo.GridLines = True
        Me.lvMembershipInfo.GroupingColumn = Nothing
        Me.lvMembershipInfo.HideSelection = False
        Me.lvMembershipInfo.Location = New System.Drawing.Point(11, 95)
        Me.lvMembershipInfo.MinColumnWidth = 50
        Me.lvMembershipInfo.MultiSelect = False
        Me.lvMembershipInfo.Name = "lvMembershipInfo"
        Me.lvMembershipInfo.OptionalColumns = ""
        Me.lvMembershipInfo.ShowMoreItem = False
        Me.lvMembershipInfo.ShowSaveItem = False
        Me.lvMembershipInfo.ShowSelectAll = True
        Me.lvMembershipInfo.ShowSizeAllColumnsToFit = True
        Me.lvMembershipInfo.Size = New System.Drawing.Size(772, 118)
        Me.lvMembershipInfo.Sortable = True
        Me.lvMembershipInfo.TabIndex = 0
        Me.lvMembershipInfo.UseCompatibleStateImageBehavior = False
        Me.lvMembershipInfo.View = System.Windows.Forms.View.Details
        '
        'colhMembershipType
        '
        Me.colhMembershipType.Tag = "colhMembershipType"
        Me.colhMembershipType.Text = "Membership Type"
        Me.colhMembershipType.Width = 200
        '
        'colhMembership
        '
        Me.colhMembership.Tag = "colhMembership"
        Me.colhMembership.Text = "Membership"
        Me.colhMembership.Width = 200
        '
        'colhMembershipNo
        '
        Me.colhMembershipNo.Tag = "colhMembershipNo"
        Me.colhMembershipNo.Text = "Membership No"
        Me.colhMembershipNo.Width = 365
        '
        'objcolhMembershipCatId
        '
        Me.objcolhMembershipCatId.Tag = "objcolhMembershipCatId"
        Me.objcolhMembershipCatId.Text = "objcolhMembershipCatId"
        Me.objcolhMembershipCatId.Width = 0
        '
        'objcolhMembershipUnkid
        '
        Me.objcolhMembershipUnkid.Tag = "objcolhMembershipUnkid"
        Me.objcolhMembershipUnkid.Text = "objcolhMembershipUnkid"
        Me.objcolhMembershipUnkid.Width = 0
        '
        'objcolhMGUID
        '
        Me.objcolhMGUID.Tag = "objcolhMGUID"
        Me.objcolhMGUID.Text = "objcolhMGUID"
        Me.objcolhMGUID.Width = 0
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine1.ForeColor = System.Drawing.Color.DarkGray
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(482, 33)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(10, 48)
        Me.objStLine1.TabIndex = 9
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(692, 51)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(91, 30)
        Me.btnDelete.TabIndex = 12
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(11, 84)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(772, 8)
        Me.objelLine3.TabIndex = 13
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMedicalNo
        '
        Me.cboMedicalNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedicalNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedicalNo.FormattingEnabled = True
        Me.cboMedicalNo.Location = New System.Drawing.Point(107, 60)
        Me.cboMedicalNo.Name = "cboMedicalNo"
        Me.cboMedicalNo.Size = New System.Drawing.Size(131, 21)
        Me.cboMedicalNo.TabIndex = 5
        '
        'btnEditMembership
        '
        Me.btnEditMembership.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditMembership.BackColor = System.Drawing.Color.White
        Me.btnEditMembership.BackgroundImage = CType(resources.GetObject("btnEditMembership.BackgroundImage"), System.Drawing.Image)
        Me.btnEditMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditMembership.BorderColor = System.Drawing.Color.Empty
        Me.btnEditMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditMembership.FlatAppearance.BorderSize = 0
        Me.btnEditMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditMembership.ForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditMembership.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditMembership.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.Location = New System.Drawing.Point(595, 51)
        Me.btnEditMembership.Name = "btnEditMembership"
        Me.btnEditMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditMembership.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.Size = New System.Drawing.Size(91, 30)
        Me.btnEditMembership.TabIndex = 11
        Me.btnEditMembership.Text = "&Edit"
        Me.btnEditMembership.UseVisualStyleBackColor = True
        '
        'lblDBMedicalMembershipNo
        '
        Me.lblDBMedicalMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBMedicalMembershipNo.Location = New System.Drawing.Point(8, 63)
        Me.lblDBMedicalMembershipNo.Name = "lblDBMedicalMembershipNo"
        Me.lblDBMedicalMembershipNo.Size = New System.Drawing.Size(93, 15)
        Me.lblDBMedicalMembershipNo.TabIndex = 4
        Me.lblDBMedicalMembershipNo.Text = "Medical Mem."
        Me.lblDBMedicalMembershipNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAddMembership
        '
        Me.btnAddMembership.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddMembership.BackColor = System.Drawing.Color.White
        Me.btnAddMembership.BackgroundImage = CType(resources.GetObject("btnAddMembership.BackgroundImage"), System.Drawing.Image)
        Me.btnAddMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddMembership.BorderColor = System.Drawing.Color.Empty
        Me.btnAddMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddMembership.FlatAppearance.BorderSize = 0
        Me.btnAddMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddMembership.ForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddMembership.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddMembership.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.Location = New System.Drawing.Point(498, 51)
        Me.btnAddMembership.Name = "btnAddMembership"
        Me.btnAddMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddMembership.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.Size = New System.Drawing.Size(91, 30)
        Me.btnAddMembership.TabIndex = 10
        Me.btnAddMembership.Text = "&Add"
        Me.btnAddMembership.UseVisualStyleBackColor = True
        '
        'txtMembershipNo
        '
        Me.txtMembershipNo.BackColor = System.Drawing.Color.White
        Me.txtMembershipNo.Flags = 0
        Me.txtMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMembershipNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMembershipNo.Location = New System.Drawing.Point(342, 33)
        Me.txtMembershipNo.Name = "txtMembershipNo"
        Me.txtMembershipNo.Size = New System.Drawing.Size(134, 21)
        Me.txtMembershipNo.TabIndex = 8
        '
        'lblMemberNo
        '
        Me.lblMemberNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemberNo.Location = New System.Drawing.Point(271, 35)
        Me.lblMemberNo.Name = "lblMemberNo"
        Me.lblMemberNo.Size = New System.Drawing.Size(65, 15)
        Me.lblMemberNo.TabIndex = 7
        Me.lblMemberNo.Text = "Mem. No."
        Me.lblMemberNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpBenefitInfo
        '
        Me.tabpBenefitInfo.Controls.Add(Me.gbBenefitInfo)
        Me.tabpBenefitInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpBenefitInfo.Name = "tabpBenefitInfo"
        Me.tabpBenefitInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpBenefitInfo.Size = New System.Drawing.Size(800, 225)
        Me.tabpBenefitInfo.TabIndex = 1
        Me.tabpBenefitInfo.Text = "Benefit Info"
        Me.tabpBenefitInfo.UseVisualStyleBackColor = True
        '
        'gbBenefitInfo
        '
        Me.gbBenefitInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBenefitInfo.Checked = False
        Me.gbBenefitInfo.CollapseAllExceptThis = False
        Me.gbBenefitInfo.CollapsedHoverImage = Nothing
        Me.gbBenefitInfo.CollapsedNormalImage = Nothing
        Me.gbBenefitInfo.CollapsedPressedImage = Nothing
        Me.gbBenefitInfo.CollapseOnLoad = False
        Me.gbBenefitInfo.Controls.Add(Me.txtRemainingPercent)
        Me.gbBenefitInfo.Controls.Add(Me.lblTotalRemainingPercent)
        Me.gbBenefitInfo.Controls.Add(Me.lblTotalPercent)
        Me.gbBenefitInfo.Controls.Add(Me.txtTotalPercentage)
        Me.gbBenefitInfo.Controls.Add(Me.objbtnAddBenefitType)
        Me.gbBenefitInfo.Controls.Add(Me.objbtnAddBenefitGroup)
        Me.gbBenefitInfo.Controls.Add(Me.lvBenefit)
        Me.gbBenefitInfo.Controls.Add(Me.objelLine2)
        Me.gbBenefitInfo.Controls.Add(Me.objstLine2)
        Me.gbBenefitInfo.Controls.Add(Me.btnDeleteBenefit)
        Me.gbBenefitInfo.Controls.Add(Me.btnEditBenefit)
        Me.gbBenefitInfo.Controls.Add(Me.btnAddBenefit)
        Me.gbBenefitInfo.Controls.Add(Me.cboValueBasis)
        Me.gbBenefitInfo.Controls.Add(Me.lblValueBasis)
        Me.gbBenefitInfo.Controls.Add(Me.lblBenefitGroup)
        Me.gbBenefitInfo.Controls.Add(Me.cboBenefitGroup)
        Me.gbBenefitInfo.Controls.Add(Me.cboBenefitType)
        Me.gbBenefitInfo.Controls.Add(Me.lblBenefitsInPercent)
        Me.gbBenefitInfo.Controls.Add(Me.txtBenefitPercent)
        Me.gbBenefitInfo.Controls.Add(Me.lblDBBenefitInAmount)
        Me.gbBenefitInfo.Controls.Add(Me.txtBenefitAmount)
        Me.gbBenefitInfo.Controls.Add(Me.lblDBBenefitType)
        Me.gbBenefitInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbBenefitInfo.ExpandedHoverImage = Nothing
        Me.gbBenefitInfo.ExpandedNormalImage = Nothing
        Me.gbBenefitInfo.ExpandedPressedImage = Nothing
        Me.gbBenefitInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBenefitInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBenefitInfo.HeaderHeight = 25
        Me.gbBenefitInfo.HeaderMessage = ""
        Me.gbBenefitInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBenefitInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBenefitInfo.HeightOnCollapse = 0
        Me.gbBenefitInfo.LeftTextSpace = 0
        Me.gbBenefitInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbBenefitInfo.Name = "gbBenefitInfo"
        Me.gbBenefitInfo.OpenHeight = 300
        Me.gbBenefitInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBenefitInfo.ShowBorder = True
        Me.gbBenefitInfo.ShowCheckBox = False
        Me.gbBenefitInfo.ShowCollapseButton = False
        Me.gbBenefitInfo.ShowDefaultBorderColor = True
        Me.gbBenefitInfo.ShowDownButton = False
        Me.gbBenefitInfo.ShowHeader = True
        Me.gbBenefitInfo.Size = New System.Drawing.Size(794, 219)
        Me.gbBenefitInfo.TabIndex = 0
        Me.gbBenefitInfo.Temp = 0
        Me.gbBenefitInfo.Text = "Benefit Info"
        Me.gbBenefitInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemainingPercent
        '
        Me.txtRemainingPercent.AllowNegative = True
        Me.txtRemainingPercent.BackColor = System.Drawing.SystemColors.Control
        Me.txtRemainingPercent.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtRemainingPercent.DigitsInGroup = 0
        Me.txtRemainingPercent.Flags = 0
        Me.txtRemainingPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemainingPercent.Location = New System.Drawing.Point(708, 33)
        Me.txtRemainingPercent.MaxDecimalPlaces = 2
        Me.txtRemainingPercent.MaxWholeDigits = 21
        Me.txtRemainingPercent.Name = "txtRemainingPercent"
        Me.txtRemainingPercent.Prefix = ""
        Me.txtRemainingPercent.RangeMax = 1.7976931348623157E+308
        Me.txtRemainingPercent.RangeMin = -1.7976931348623157E+308
        Me.txtRemainingPercent.ReadOnly = True
        Me.txtRemainingPercent.Size = New System.Drawing.Size(44, 21)
        Me.txtRemainingPercent.TabIndex = 52
        Me.txtRemainingPercent.Text = "0.00"
        Me.txtRemainingPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalRemainingPercent
        '
        Me.lblTotalRemainingPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalRemainingPercent.Location = New System.Drawing.Point(626, 35)
        Me.lblTotalRemainingPercent.Name = "lblTotalRemainingPercent"
        Me.lblTotalRemainingPercent.Size = New System.Drawing.Size(78, 16)
        Me.lblTotalRemainingPercent.TabIndex = 51
        Me.lblTotalRemainingPercent.Text = "Remaining (%)"
        Me.lblTotalRemainingPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalPercent
        '
        Me.lblTotalPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPercent.Location = New System.Drawing.Point(497, 35)
        Me.lblTotalPercent.Name = "lblTotalPercent"
        Me.lblTotalPercent.Size = New System.Drawing.Size(75, 16)
        Me.lblTotalPercent.TabIndex = 49
        Me.lblTotalPercent.Text = "Allocated (%)"
        Me.lblTotalPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTotalPercentage
        '
        Me.txtTotalPercentage.AllowNegative = True
        Me.txtTotalPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtTotalPercentage.DigitsInGroup = 0
        Me.txtTotalPercentage.Flags = 0
        Me.txtTotalPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPercentage.Location = New System.Drawing.Point(576, 33)
        Me.txtTotalPercentage.MaxDecimalPlaces = 2
        Me.txtTotalPercentage.MaxWholeDigits = 21
        Me.txtTotalPercentage.Name = "txtTotalPercentage"
        Me.txtTotalPercentage.Prefix = ""
        Me.txtTotalPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPercentage.ReadOnly = True
        Me.txtTotalPercentage.Size = New System.Drawing.Size(44, 21)
        Me.txtTotalPercentage.TabIndex = 50
        Me.txtTotalPercentage.Text = "0.00"
        Me.txtTotalPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnAddBenefitType
        '
        Me.objbtnAddBenefitType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBenefitType.BorderSelected = False
        Me.objbtnAddBenefitType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBenefitType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddBenefitType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBenefitType.Location = New System.Drawing.Point(212, 60)
        Me.objbtnAddBenefitType.Name = "objbtnAddBenefitType"
        Me.objbtnAddBenefitType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBenefitType.TabIndex = 5
        '
        'objbtnAddBenefitGroup
        '
        Me.objbtnAddBenefitGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBenefitGroup.BorderSelected = False
        Me.objbtnAddBenefitGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBenefitGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddBenefitGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBenefitGroup.Location = New System.Drawing.Point(212, 33)
        Me.objbtnAddBenefitGroup.Name = "objbtnAddBenefitGroup"
        Me.objbtnAddBenefitGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBenefitGroup.TabIndex = 2
        '
        'lvBenefit
        '
        Me.lvBenefit.BackColorOnChecked = True
        Me.lvBenefit.ColumnHeaders = Nothing
        Me.lvBenefit.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhBenefitGroup, Me.colhBenefitType, Me.colhValueBasis, Me.colhPercent, Me.colhAmount, Me.objcolhBenefitGrpId, Me.objcolhBenefitId, Me.objcolhValueBasisId, Me.objcolhBGUID})
        Me.lvBenefit.CompulsoryColumns = ""
        Me.lvBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvBenefit.FullRowSelect = True
        Me.lvBenefit.GridLines = True
        Me.lvBenefit.GroupingColumn = Nothing
        Me.lvBenefit.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvBenefit.HideSelection = False
        Me.lvBenefit.Location = New System.Drawing.Point(11, 102)
        Me.lvBenefit.MinColumnWidth = 50
        Me.lvBenefit.MultiSelect = False
        Me.lvBenefit.Name = "lvBenefit"
        Me.lvBenefit.OptionalColumns = ""
        Me.lvBenefit.ShowMoreItem = False
        Me.lvBenefit.ShowSaveItem = False
        Me.lvBenefit.ShowSelectAll = True
        Me.lvBenefit.ShowSizeAllColumnsToFit = True
        Me.lvBenefit.Size = New System.Drawing.Size(769, 111)
        Me.lvBenefit.Sortable = True
        Me.lvBenefit.TabIndex = 17
        Me.lvBenefit.UseCompatibleStateImageBehavior = False
        Me.lvBenefit.View = System.Windows.Forms.View.Details
        '
        'colhBenefitGroup
        '
        Me.colhBenefitGroup.Tag = "colhBenefitGroup"
        Me.colhBenefitGroup.Text = "Benefit Group"
        Me.colhBenefitGroup.Width = 210
        '
        'colhBenefitType
        '
        Me.colhBenefitType.Tag = "colhBenefitType"
        Me.colhBenefitType.Text = "Benefit Type"
        Me.colhBenefitType.Width = 200
        '
        'colhValueBasis
        '
        Me.colhValueBasis.Tag = "colhValueBasis"
        Me.colhValueBasis.Text = "Value Basis"
        Me.colhValueBasis.Width = 120
        '
        'colhPercent
        '
        Me.colhPercent.Tag = "colhPercent"
        Me.colhPercent.Text = "Percent(%)"
        Me.colhPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhPercent.Width = 85
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 150
        '
        'objcolhBenefitGrpId
        '
        Me.objcolhBenefitGrpId.Tag = "objcolhBenefitGrpId"
        Me.objcolhBenefitGrpId.Text = "objcolhBenefitGrpId"
        Me.objcolhBenefitGrpId.Width = 0
        '
        'objcolhBenefitId
        '
        Me.objcolhBenefitId.Tag = "objcolhBenefitId"
        Me.objcolhBenefitId.Text = "objcolhBenefitId"
        Me.objcolhBenefitId.Width = 0
        '
        'objcolhValueBasisId
        '
        Me.objcolhValueBasisId.Tag = "objcolhValueBasisId"
        Me.objcolhValueBasisId.Text = "objcolhValueBasisId"
        Me.objcolhValueBasisId.Width = 0
        '
        'objcolhBGUID
        '
        Me.objcolhBGUID.Tag = "objcolhBGUID"
        Me.objcolhBGUID.Text = "objcolhBGUID"
        Me.objcolhBGUID.Width = 0
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(11, 95)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(770, 3)
        Me.objelLine2.TabIndex = 16
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objstLine2
        '
        Me.objstLine2.BackColor = System.Drawing.Color.Transparent
        Me.objstLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objstLine2.ForeColor = System.Drawing.Color.DarkGray
        Me.objstLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine2.Location = New System.Drawing.Point(483, 33)
        Me.objstLine2.Name = "objstLine2"
        Me.objstLine2.Size = New System.Drawing.Size(10, 60)
        Me.objstLine2.TabIndex = 12
        '
        'btnDeleteBenefit
        '
        Me.btnDeleteBenefit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteBenefit.BackColor = System.Drawing.Color.White
        Me.btnDeleteBenefit.BackgroundImage = CType(resources.GetObject("btnDeleteBenefit.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteBenefit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteBenefit.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteBenefit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteBenefit.FlatAppearance.BorderSize = 0
        Me.btnDeleteBenefit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteBenefit.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteBenefit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteBenefit.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteBenefit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteBenefit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteBenefit.Location = New System.Drawing.Point(690, 60)
        Me.btnDeleteBenefit.Name = "btnDeleteBenefit"
        Me.btnDeleteBenefit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteBenefit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteBenefit.Size = New System.Drawing.Size(91, 30)
        Me.btnDeleteBenefit.TabIndex = 15
        Me.btnDeleteBenefit.Text = "&Delete"
        Me.btnDeleteBenefit.UseVisualStyleBackColor = True
        '
        'btnEditBenefit
        '
        Me.btnEditBenefit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditBenefit.BackColor = System.Drawing.Color.White
        Me.btnEditBenefit.BackgroundImage = CType(resources.GetObject("btnEditBenefit.BackgroundImage"), System.Drawing.Image)
        Me.btnEditBenefit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditBenefit.BorderColor = System.Drawing.Color.Empty
        Me.btnEditBenefit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditBenefit.FlatAppearance.BorderSize = 0
        Me.btnEditBenefit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditBenefit.ForeColor = System.Drawing.Color.Black
        Me.btnEditBenefit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditBenefit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditBenefit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditBenefit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditBenefit.Location = New System.Drawing.Point(595, 60)
        Me.btnEditBenefit.Name = "btnEditBenefit"
        Me.btnEditBenefit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditBenefit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditBenefit.Size = New System.Drawing.Size(91, 30)
        Me.btnEditBenefit.TabIndex = 14
        Me.btnEditBenefit.Text = "&Edit"
        Me.btnEditBenefit.UseVisualStyleBackColor = True
        '
        'btnAddBenefit
        '
        Me.btnAddBenefit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddBenefit.BackColor = System.Drawing.Color.White
        Me.btnAddBenefit.BackgroundImage = CType(resources.GetObject("btnAddBenefit.BackgroundImage"), System.Drawing.Image)
        Me.btnAddBenefit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddBenefit.BorderColor = System.Drawing.Color.Empty
        Me.btnAddBenefit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddBenefit.FlatAppearance.BorderSize = 0
        Me.btnAddBenefit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddBenefit.ForeColor = System.Drawing.Color.Black
        Me.btnAddBenefit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddBenefit.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddBenefit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddBenefit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddBenefit.Location = New System.Drawing.Point(499, 60)
        Me.btnAddBenefit.Name = "btnAddBenefit"
        Me.btnAddBenefit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddBenefit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddBenefit.Size = New System.Drawing.Size(91, 30)
        Me.btnAddBenefit.TabIndex = 13
        Me.btnAddBenefit.Text = "&Add"
        Me.btnAddBenefit.UseVisualStyleBackColor = True
        '
        'cboValueBasis
        '
        Me.cboValueBasis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboValueBasis.FormattingEnabled = True
        Me.cboValueBasis.Location = New System.Drawing.Point(304, 33)
        Me.cboValueBasis.Name = "cboValueBasis"
        Me.cboValueBasis.Size = New System.Drawing.Size(173, 21)
        Me.cboValueBasis.TabIndex = 7
        '
        'lblValueBasis
        '
        Me.lblValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValueBasis.Location = New System.Drawing.Point(236, 36)
        Me.lblValueBasis.Name = "lblValueBasis"
        Me.lblValueBasis.Size = New System.Drawing.Size(62, 15)
        Me.lblValueBasis.TabIndex = 6
        Me.lblValueBasis.Text = "Benefit In"
        Me.lblValueBasis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBenefitGroup
        '
        Me.lblBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblBenefitGroup.Name = "lblBenefitGroup"
        Me.lblBenefitGroup.Size = New System.Drawing.Size(74, 15)
        Me.lblBenefitGroup.TabIndex = 0
        Me.lblBenefitGroup.Text = "Benefit Group"
        Me.lblBenefitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBenefitGroup
        '
        Me.cboBenefitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitGroup.FormattingEnabled = True
        Me.cboBenefitGroup.Location = New System.Drawing.Point(88, 33)
        Me.cboBenefitGroup.Name = "cboBenefitGroup"
        Me.cboBenefitGroup.Size = New System.Drawing.Size(118, 21)
        Me.cboBenefitGroup.TabIndex = 1
        '
        'cboBenefitType
        '
        Me.cboBenefitType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitType.FormattingEnabled = True
        Me.cboBenefitType.Location = New System.Drawing.Point(88, 60)
        Me.cboBenefitType.Name = "cboBenefitType"
        Me.cboBenefitType.Size = New System.Drawing.Size(118, 21)
        Me.cboBenefitType.TabIndex = 4
        '
        'lblBenefitsInPercent
        '
        Me.lblBenefitsInPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitsInPercent.Location = New System.Drawing.Point(236, 62)
        Me.lblBenefitsInPercent.Name = "lblBenefitsInPercent"
        Me.lblBenefitsInPercent.Size = New System.Drawing.Size(62, 15)
        Me.lblBenefitsInPercent.TabIndex = 8
        Me.lblBenefitsInPercent.Text = "Perc. (%)"
        Me.lblBenefitsInPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBenefitPercent
        '
        Me.txtBenefitPercent.AllowNegative = True
        Me.txtBenefitPercent.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtBenefitPercent.DigitsInGroup = 0
        Me.txtBenefitPercent.Flags = 0
        Me.txtBenefitPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenefitPercent.Location = New System.Drawing.Point(304, 60)
        Me.txtBenefitPercent.MaxDecimalPlaces = 2
        Me.txtBenefitPercent.MaxWholeDigits = 6
        Me.txtBenefitPercent.Name = "txtBenefitPercent"
        Me.txtBenefitPercent.Prefix = ""
        Me.txtBenefitPercent.RangeMax = 1.7976931348623157E+308
        Me.txtBenefitPercent.RangeMin = -1.7976931348623157E+308
        Me.txtBenefitPercent.Size = New System.Drawing.Size(44, 21)
        Me.txtBenefitPercent.TabIndex = 9
        Me.txtBenefitPercent.Text = "0.00"
        Me.txtBenefitPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDBBenefitInAmount
        '
        Me.lblDBBenefitInAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBBenefitInAmount.Location = New System.Drawing.Point(352, 63)
        Me.lblDBBenefitInAmount.Name = "lblDBBenefitInAmount"
        Me.lblDBBenefitInAmount.Size = New System.Drawing.Size(53, 15)
        Me.lblDBBenefitInAmount.TabIndex = 10
        Me.lblDBBenefitInAmount.Text = "Amount"
        Me.lblDBBenefitInAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBenefitAmount
        '
        Me.txtBenefitAmount.AllowNegative = True
        Me.txtBenefitAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 196608})
        Me.txtBenefitAmount.DigitsInGroup = 0
        Me.txtBenefitAmount.Flags = 0
        Me.txtBenefitAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenefitAmount.Location = New System.Drawing.Point(411, 60)
        Me.txtBenefitAmount.MaxDecimalPlaces = 6
        Me.txtBenefitAmount.MaxWholeDigits = 21
        Me.txtBenefitAmount.Name = "txtBenefitAmount"
        Me.txtBenefitAmount.Prefix = ""
        Me.txtBenefitAmount.RangeMax = 1.7976931348623157E+308
        Me.txtBenefitAmount.RangeMin = -1.7976931348623157E+308
        Me.txtBenefitAmount.Size = New System.Drawing.Size(66, 21)
        Me.txtBenefitAmount.TabIndex = 11
        Me.txtBenefitAmount.Text = "0.000"
        Me.txtBenefitAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDBBenefitType
        '
        Me.lblDBBenefitType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBBenefitType.Location = New System.Drawing.Point(8, 63)
        Me.lblDBBenefitType.Name = "lblDBBenefitType"
        Me.lblDBBenefitType.Size = New System.Drawing.Size(74, 15)
        Me.lblDBBenefitType.TabIndex = 3
        Me.lblDBBenefitType.Text = "Benefit Type"
        Me.lblDBBenefitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabAttachment
        '
        Me.tabAttachment.Controls.Add(Me.dbDocumentAttachment)
        Me.tabAttachment.Location = New System.Drawing.Point(4, 22)
        Me.tabAttachment.Name = "tabAttachment"
        Me.tabAttachment.Padding = New System.Windows.Forms.Padding(3)
        Me.tabAttachment.Size = New System.Drawing.Size(800, 235)
        Me.tabAttachment.TabIndex = 2
        Me.tabAttachment.Text = "Attachment"
        Me.tabAttachment.UseVisualStyleBackColor = True
        '
        'dbDocumentAttachment
        '
        Me.dbDocumentAttachment.BorderColor = System.Drawing.Color.Black
        Me.dbDocumentAttachment.Checked = False
        Me.dbDocumentAttachment.CollapseAllExceptThis = False
        Me.dbDocumentAttachment.CollapsedHoverImage = Nothing
        Me.dbDocumentAttachment.CollapsedNormalImage = Nothing
        Me.dbDocumentAttachment.CollapsedPressedImage = Nothing
        Me.dbDocumentAttachment.CollapseOnLoad = False
        Me.dbDocumentAttachment.Controls.Add(Me.lblAttachmentDate)
        Me.dbDocumentAttachment.Controls.Add(Me.dtpAttachmentDate)
        Me.dbDocumentAttachment.Controls.Add(Me.Panel2)
        Me.dbDocumentAttachment.Controls.Add(Me.btnAddAttachment)
        Me.dbDocumentAttachment.Controls.Add(Me.cboDocumentType)
        Me.dbDocumentAttachment.Controls.Add(Me.lblDocumentType)
        Me.dbDocumentAttachment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dbDocumentAttachment.ExpandedHoverImage = Nothing
        Me.dbDocumentAttachment.ExpandedNormalImage = Nothing
        Me.dbDocumentAttachment.ExpandedPressedImage = Nothing
        Me.dbDocumentAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dbDocumentAttachment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.dbDocumentAttachment.HeaderHeight = 25
        Me.dbDocumentAttachment.HeaderMessage = ""
        Me.dbDocumentAttachment.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.dbDocumentAttachment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.dbDocumentAttachment.HeightOnCollapse = 0
        Me.dbDocumentAttachment.LeftTextSpace = 0
        Me.dbDocumentAttachment.Location = New System.Drawing.Point(3, 3)
        Me.dbDocumentAttachment.Name = "dbDocumentAttachment"
        Me.dbDocumentAttachment.OpenHeight = 300
        Me.dbDocumentAttachment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.dbDocumentAttachment.ShowBorder = True
        Me.dbDocumentAttachment.ShowCheckBox = False
        Me.dbDocumentAttachment.ShowCollapseButton = False
        Me.dbDocumentAttachment.ShowDefaultBorderColor = True
        Me.dbDocumentAttachment.ShowDownButton = False
        Me.dbDocumentAttachment.ShowHeader = True
        Me.dbDocumentAttachment.Size = New System.Drawing.Size(794, 229)
        Me.dbDocumentAttachment.TabIndex = 234
        Me.dbDocumentAttachment.Temp = 0
        Me.dbDocumentAttachment.Text = "Document Attachment Info"
        Me.dbDocumentAttachment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAttachmentDate
        '
        Me.lblAttachmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttachmentDate.Location = New System.Drawing.Point(470, 65)
        Me.lblAttachmentDate.Name = "lblAttachmentDate"
        Me.lblAttachmentDate.Size = New System.Drawing.Size(139, 16)
        Me.lblAttachmentDate.TabIndex = 235
        Me.lblAttachmentDate.Text = "Attachment Date"
        Me.lblAttachmentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAttachmentDate
        '
        Me.dtpAttachmentDate.Checked = False
        Me.dtpAttachmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAttachmentDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAttachmentDate.Location = New System.Drawing.Point(615, 62)
        Me.dtpAttachmentDate.Name = "dtpAttachmentDate"
        Me.dtpAttachmentDate.Size = New System.Drawing.Size(88, 21)
        Me.dtpAttachmentDate.TabIndex = 236
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvDepedantAttachment)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(4, 27)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(460, 185)
        Me.Panel2.TabIndex = 233
        '
        'dgvDepedantAttachment
        '
        Me.dgvDepedantAttachment.AllowUserToAddRows = False
        Me.dgvDepedantAttachment.AllowUserToDeleteRows = False
        Me.dgvDepedantAttachment.AllowUserToResizeRows = False
        Me.dgvDepedantAttachment.BackgroundColor = System.Drawing.Color.White
        Me.dgvDepedantAttachment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDepedantAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDepedantAttachment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohDelete, Me.colhName, Me.colhSize, Me.objcolhDownload, Me.objcolhGUID, Me.objcolhScanUnkId})
        Me.dgvDepedantAttachment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDepedantAttachment.Location = New System.Drawing.Point(0, 0)
        Me.dgvDepedantAttachment.Name = "dgvDepedantAttachment"
        Me.dgvDepedantAttachment.RowHeadersVisible = False
        Me.dgvDepedantAttachment.Size = New System.Drawing.Size(460, 185)
        Me.dgvDepedantAttachment.TabIndex = 228
        '
        'objcohDelete
        '
        Me.objcohDelete.HeaderText = ""
        Me.objcohDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objcohDelete.Name = "objcohDelete"
        Me.objcohDelete.ReadOnly = True
        Me.objcohDelete.Width = 25
        '
        'colhName
        '
        Me.colhName.HeaderText = "File Name"
        Me.colhName.Name = "colhName"
        Me.colhName.ReadOnly = True
        Me.colhName.Width = 180
        '
        'colhSize
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhSize.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhSize.HeaderText = "File Size"
        Me.colhSize.Name = "colhSize"
        Me.colhSize.ReadOnly = True
        Me.colhSize.Width = 150
        '
        'objcolhDownload
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhDownload.DefaultCellStyle = DataGridViewCellStyle2
        Me.objcolhDownload.HeaderText = "Download"
        Me.objcolhDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhDownload.Name = "objcolhDownload"
        Me.objcolhDownload.ReadOnly = True
        Me.objcolhDownload.Text = "Download"
        Me.objcolhDownload.UseColumnTextForLinkValue = True
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "objcolhGUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.Visible = False
        '
        'objcolhScanUnkId
        '
        Me.objcolhScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Name = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Visible = False
        '
        'btnAddAttachment
        '
        Me.btnAddAttachment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddAttachment.BackColor = System.Drawing.Color.White
        Me.btnAddAttachment.BackgroundImage = CType(resources.GetObject("btnAddAttachment.BackgroundImage"), System.Drawing.Image)
        Me.btnAddAttachment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddAttachment.BorderColor = System.Drawing.Color.Empty
        Me.btnAddAttachment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddAttachment.FlatAppearance.BorderSize = 0
        Me.btnAddAttachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddAttachment.ForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddAttachment.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Location = New System.Drawing.Point(615, 89)
        Me.btnAddAttachment.Name = "btnAddAttachment"
        Me.btnAddAttachment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Size = New System.Drawing.Size(119, 29)
        Me.btnAddAttachment.TabIndex = 230
        Me.btnAddAttachment.Text = "&Browse"
        Me.btnAddAttachment.UseVisualStyleBackColor = True
        '
        'cboDocumentType
        '
        Me.cboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDocumentType.FormattingEnabled = True
        Me.cboDocumentType.Location = New System.Drawing.Point(615, 35)
        Me.cboDocumentType.Name = "cboDocumentType"
        Me.cboDocumentType.Size = New System.Drawing.Size(161, 21)
        Me.cboDocumentType.TabIndex = 231
        '
        'lblDocumentType
        '
        Me.lblDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentType.Location = New System.Drawing.Point(470, 36)
        Me.lblDocumentType.Name = "lblDocumentType"
        Me.lblDocumentType.Size = New System.Drawing.Size(139, 16)
        Me.lblDocumentType.TabIndex = 232
        Me.lblDocumentType.Text = "Document Type"
        Me.lblDocumentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tapbBankDetails
        '
        Me.tapbBankDetails.Controls.Add(Me.gbBankDetails)
        Me.tapbBankDetails.Location = New System.Drawing.Point(4, 22)
        Me.tapbBankDetails.Name = "tapbBankDetails"
        Me.tapbBankDetails.Size = New System.Drawing.Size(800, 225)
        Me.tapbBankDetails.TabIndex = 3
        Me.tapbBankDetails.Text = "Bank Details"
        Me.tapbBankDetails.UseVisualStyleBackColor = True
        '
        'gbBankDetails
        '
        Me.gbBankDetails.BorderColor = System.Drawing.Color.Black
        Me.gbBankDetails.Checked = False
        Me.gbBankDetails.CollapseAllExceptThis = False
        Me.gbBankDetails.CollapsedHoverImage = Nothing
        Me.gbBankDetails.CollapsedNormalImage = Nothing
        Me.gbBankDetails.CollapsedPressedImage = Nothing
        Me.gbBankDetails.CollapseOnLoad = False
        Me.gbBankDetails.Controls.Add(Me.objbtnUp)
        Me.gbBankDetails.Controls.Add(Me.objbtnDown)
        Me.gbBankDetails.Controls.Add(Me.lblMode)
        Me.gbBankDetails.Controls.Add(Me.cboMode)
        Me.gbBankDetails.Controls.Add(Me.lblPerc)
        Me.gbBankDetails.Controls.Add(Me.lblSalaryDistrib)
        Me.gbBankDetails.Controls.Add(Me.txtPercentage)
        Me.gbBankDetails.Controls.Add(Me.objbtnAddAccType)
        Me.gbBankDetails.Controls.Add(Me.txtAccountNo)
        Me.gbBankDetails.Controls.Add(Me.lblAccountNo)
        Me.gbBankDetails.Controls.Add(Me.lblAccountType)
        Me.gbBankDetails.Controls.Add(Me.cboAccountType)
        Me.gbBankDetails.Controls.Add(Me.objbtnSearchBankBranch)
        Me.gbBankDetails.Controls.Add(Me.objbtnSearchBankGroup)
        Me.gbBankDetails.Controls.Add(Me.objbtnAddBranch)
        Me.gbBankDetails.Controls.Add(Me.objbtnAddBankGroup)
        Me.gbBankDetails.Controls.Add(Me.cboBankBranch)
        Me.gbBankDetails.Controls.Add(Me.EZeeLine1)
        Me.gbBankDetails.Controls.Add(Me.btnDeleteEmpDetails)
        Me.gbBankDetails.Controls.Add(Me.btnEditEmpDetails)
        Me.gbBankDetails.Controls.Add(Me.btnAddEmpDetails)
        Me.gbBankDetails.Controls.Add(Me.Panel4)
        Me.gbBankDetails.Controls.Add(Me.lblBank)
        Me.gbBankDetails.Controls.Add(Me.cboBankGroup)
        Me.gbBankDetails.Controls.Add(Me.lblBankGroup)
        Me.gbBankDetails.Controls.Add(Me.lblPeriod)
        Me.gbBankDetails.Controls.Add(Me.cboPeriod)
        Me.gbBankDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbBankDetails.ExpandedHoverImage = Nothing
        Me.gbBankDetails.ExpandedNormalImage = Nothing
        Me.gbBankDetails.ExpandedPressedImage = Nothing
        Me.gbBankDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBankDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBankDetails.HeaderHeight = 25
        Me.gbBankDetails.HeaderMessage = ""
        Me.gbBankDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBankDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBankDetails.HeightOnCollapse = 0
        Me.gbBankDetails.LeftTextSpace = 0
        Me.gbBankDetails.Location = New System.Drawing.Point(0, 0)
        Me.gbBankDetails.Name = "gbBankDetails"
        Me.gbBankDetails.OpenHeight = 351
        Me.gbBankDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBankDetails.ShowBorder = True
        Me.gbBankDetails.ShowCheckBox = False
        Me.gbBankDetails.ShowCollapseButton = False
        Me.gbBankDetails.ShowDefaultBorderColor = True
        Me.gbBankDetails.ShowDownButton = False
        Me.gbBankDetails.ShowHeader = True
        Me.gbBankDetails.Size = New System.Drawing.Size(800, 225)
        Me.gbBankDetails.TabIndex = 2
        Me.gbBankDetails.Temp = 0
        Me.gbBankDetails.Text = "Bank Details"
        Me.gbBankDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.Aruti.Main.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(798, 159)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(32, 30)
        Me.objbtnUp.TabIndex = 151
        Me.objbtnUp.UseVisualStyleBackColor = False
        '
        'objbtnDown
        '
        Me.objbtnDown.BackColor = System.Drawing.Color.White
        Me.objbtnDown.BackgroundImage = CType(resources.GetObject("objbtnDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDown.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDown.FlatAppearance.BorderSize = 0
        Me.objbtnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Image = Global.Aruti.Main.My.Resources.Resources.MoveDown_16
        Me.objbtnDown.Location = New System.Drawing.Point(798, 188)
        Me.objbtnDown.Name = "objbtnDown"
        Me.objbtnDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Size = New System.Drawing.Size(32, 30)
        Me.objbtnDown.TabIndex = 152
        Me.objbtnDown.UseVisualStyleBackColor = False
        '
        'lblMode
        '
        Me.lblMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMode.Location = New System.Drawing.Point(289, 87)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(98, 15)
        Me.lblMode.TabIndex = 260
        Me.lblMode.Text = "Distribution Mode"
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(393, 84)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(127, 21)
        Me.cboMode.TabIndex = 253
        '
        'lblPerc
        '
        Me.lblPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerc.Location = New System.Drawing.Point(761, 33)
        Me.lblPerc.Name = "lblPerc"
        Me.lblPerc.Size = New System.Drawing.Size(32, 15)
        Me.lblPerc.TabIndex = 259
        Me.lblPerc.Text = "(%)"
        '
        'lblSalaryDistrib
        '
        Me.lblSalaryDistrib.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryDistrib.Location = New System.Drawing.Point(554, 33)
        Me.lblSalaryDistrib.Name = "lblSalaryDistrib"
        Me.lblSalaryDistrib.Size = New System.Drawing.Size(102, 15)
        Me.lblSalaryDistrib.TabIndex = 258
        Me.lblSalaryDistrib.Text = "Salary Distribution"
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = False
        Me.txtPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 65536
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(670, 30)
        Me.txtPercentage.MaxDecimalPlaces = 6
        Me.txtPercentage.MaxWholeDigits = 21
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(85, 21)
        Me.txtPercentage.TabIndex = 254
        Me.txtPercentage.Text = "0"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnAddAccType
        '
        Me.objbtnAddAccType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAccType.BorderSelected = False
        Me.objbtnAddAccType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAccType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAccType.Location = New System.Drawing.Point(541, 30)
        Me.objbtnAddAccType.Name = "objbtnAddAccType"
        Me.objbtnAddAccType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAccType.TabIndex = 257
        '
        'txtAccountNo
        '
        Me.txtAccountNo.Flags = 0
        Me.txtAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAccountNo.Location = New System.Drawing.Point(393, 57)
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.Size = New System.Drawing.Size(154, 21)
        Me.txtAccountNo.TabIndex = 252
        '
        'lblAccountNo
        '
        Me.lblAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountNo.Location = New System.Drawing.Point(289, 60)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.Size = New System.Drawing.Size(98, 15)
        Me.lblAccountNo.TabIndex = 256
        Me.lblAccountNo.Text = "Account No."
        '
        'lblAccountType
        '
        Me.lblAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountType.Location = New System.Drawing.Point(289, 33)
        Me.lblAccountType.Name = "lblAccountType"
        Me.lblAccountType.Size = New System.Drawing.Size(98, 15)
        Me.lblAccountType.TabIndex = 255
        Me.lblAccountType.Text = "Account Type"
        '
        'cboAccountType
        '
        Me.cboAccountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountType.FormattingEnabled = True
        Me.cboAccountType.Location = New System.Drawing.Point(393, 30)
        Me.cboAccountType.Name = "cboAccountType"
        Me.cboAccountType.Size = New System.Drawing.Size(127, 21)
        Me.cboAccountType.TabIndex = 251
        '
        'objbtnSearchBankBranch
        '
        Me.objbtnSearchBankBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankBranch.BorderSelected = False
        Me.objbtnSearchBankBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankBranch.Location = New System.Drawing.Point(249, 84)
        Me.objbtnSearchBankBranch.Name = "objbtnSearchBankBranch"
        Me.objbtnSearchBankBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankBranch.TabIndex = 250
        '
        'objbtnSearchBankGroup
        '
        Me.objbtnSearchBankGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankGroup.BorderSelected = False
        Me.objbtnSearchBankGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankGroup.Location = New System.Drawing.Point(249, 57)
        Me.objbtnSearchBankGroup.Name = "objbtnSearchBankGroup"
        Me.objbtnSearchBankGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankGroup.TabIndex = 249
        '
        'objbtnAddBranch
        '
        Me.objbtnAddBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBranch.BorderSelected = False
        Me.objbtnAddBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBranch.Location = New System.Drawing.Point(276, 84)
        Me.objbtnAddBranch.Name = "objbtnAddBranch"
        Me.objbtnAddBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBranch.TabIndex = 248
        '
        'objbtnAddBankGroup
        '
        Me.objbtnAddBankGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBankGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBankGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBankGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBankGroup.BorderSelected = False
        Me.objbtnAddBankGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBankGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBankGroup.Location = New System.Drawing.Point(276, 57)
        Me.objbtnAddBankGroup.Name = "objbtnAddBankGroup"
        Me.objbtnAddBankGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBankGroup.TabIndex = 247
        '
        'cboBankBranch
        '
        Me.cboBankBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankBranch.FormattingEnabled = True
        Me.cboBankBranch.Location = New System.Drawing.Point(89, 84)
        Me.cboBankBranch.Name = "cboBankBranch"
        Me.cboBankBranch.Size = New System.Drawing.Size(154, 21)
        Me.cboBankBranch.TabIndex = 57
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(11, 110)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(528, 7)
        Me.EZeeLine1.TabIndex = 51
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDeleteEmpDetails
        '
        Me.btnDeleteEmpDetails.BackColor = System.Drawing.Color.White
        Me.btnDeleteEmpDetails.BackgroundImage = CType(resources.GetObject("btnDeleteEmpDetails.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteEmpDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteEmpDetails.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteEmpDetails.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteEmpDetails.FlatAppearance.BorderSize = 0
        Me.btnDeleteEmpDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteEmpDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteEmpDetails.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteEmpDetails.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteEmpDetails.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEmpDetails.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteEmpDetails.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEmpDetails.Location = New System.Drawing.Point(715, 87)
        Me.btnDeleteEmpDetails.Name = "btnDeleteEmpDetails"
        Me.btnDeleteEmpDetails.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteEmpDetails.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEmpDetails.Size = New System.Drawing.Size(77, 30)
        Me.btnDeleteEmpDetails.TabIndex = 19
        Me.btnDeleteEmpDetails.Text = "&Delete"
        Me.btnDeleteEmpDetails.UseVisualStyleBackColor = True
        '
        'btnEditEmpDetails
        '
        Me.btnEditEmpDetails.BackColor = System.Drawing.Color.White
        Me.btnEditEmpDetails.BackgroundImage = CType(resources.GetObject("btnEditEmpDetails.BackgroundImage"), System.Drawing.Image)
        Me.btnEditEmpDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditEmpDetails.BorderColor = System.Drawing.Color.Empty
        Me.btnEditEmpDetails.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditEmpDetails.FlatAppearance.BorderSize = 0
        Me.btnEditEmpDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditEmpDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditEmpDetails.ForeColor = System.Drawing.Color.Black
        Me.btnEditEmpDetails.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditEmpDetails.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditEmpDetails.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditEmpDetails.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditEmpDetails.Location = New System.Drawing.Point(632, 87)
        Me.btnEditEmpDetails.Name = "btnEditEmpDetails"
        Me.btnEditEmpDetails.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditEmpDetails.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditEmpDetails.Size = New System.Drawing.Size(77, 30)
        Me.btnEditEmpDetails.TabIndex = 18
        Me.btnEditEmpDetails.Text = "&Edit"
        Me.btnEditEmpDetails.UseVisualStyleBackColor = True
        '
        'btnAddEmpDetails
        '
        Me.btnAddEmpDetails.BackColor = System.Drawing.Color.White
        Me.btnAddEmpDetails.BackgroundImage = CType(resources.GetObject("btnAddEmpDetails.BackgroundImage"), System.Drawing.Image)
        Me.btnAddEmpDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddEmpDetails.BorderColor = System.Drawing.Color.Empty
        Me.btnAddEmpDetails.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddEmpDetails.FlatAppearance.BorderSize = 0
        Me.btnAddEmpDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddEmpDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddEmpDetails.ForeColor = System.Drawing.Color.Black
        Me.btnAddEmpDetails.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddEmpDetails.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddEmpDetails.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddEmpDetails.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddEmpDetails.Location = New System.Drawing.Point(549, 87)
        Me.btnAddEmpDetails.Name = "btnAddEmpDetails"
        Me.btnAddEmpDetails.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddEmpDetails.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddEmpDetails.Size = New System.Drawing.Size(77, 30)
        Me.btnAddEmpDetails.TabIndex = 17
        Me.btnAddEmpDetails.Text = "&Add"
        Me.btnAddEmpDetails.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lvEmpBankList)
        Me.Panel4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel4.Location = New System.Drawing.Point(11, 123)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(781, 95)
        Me.Panel4.TabIndex = 47
        '
        'lvEmpBankList
        '
        Me.lvEmpBankList.BackColorOnChecked = True
        Me.lvEmpBankList.ColumnHeaders = Nothing
        Me.lvEmpBankList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhID, Me.colhEmployee, Me.colhBankGrp, Me.colhBankBranch, Me.colhAccType, Me.colhAccNo, Me.colhPerc, Me.colhGUID, Me.colhPeriod, Me.colhModeID})
        Me.lvEmpBankList.CompulsoryColumns = ""
        Me.lvEmpBankList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmpBankList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEmpBankList.FullRowSelect = True
        Me.lvEmpBankList.GridLines = True
        Me.lvEmpBankList.GroupingColumn = Nothing
        Me.lvEmpBankList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEmpBankList.HideSelection = False
        Me.lvEmpBankList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmpBankList.MinColumnWidth = 50
        Me.lvEmpBankList.MultiSelect = False
        Me.lvEmpBankList.Name = "lvEmpBankList"
        Me.lvEmpBankList.OptionalColumns = ""
        Me.lvEmpBankList.ShowMoreItem = False
        Me.lvEmpBankList.ShowSaveItem = False
        Me.lvEmpBankList.ShowSelectAll = True
        Me.lvEmpBankList.ShowSizeAllColumnsToFit = True
        Me.lvEmpBankList.Size = New System.Drawing.Size(781, 95)
        Me.lvEmpBankList.Sortable = False
        Me.lvEmpBankList.TabIndex = 1
        Me.lvEmpBankList.UseCompatibleStateImageBehavior = False
        Me.lvEmpBankList.View = System.Windows.Forms.View.Details
        '
        'colhID
        '
        Me.colhID.Text = "Priority"
        Me.colhID.Width = 50
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhBankGrp
        '
        Me.colhBankGrp.Tag = "colhBankGrp"
        Me.colhBankGrp.Text = "Bank Group"
        Me.colhBankGrp.Width = 175
        '
        'colhBankBranch
        '
        Me.colhBankBranch.Tag = "colhBankBranch"
        Me.colhBankBranch.Text = "Bank Branch"
        Me.colhBankBranch.Width = 175
        '
        'colhAccType
        '
        Me.colhAccType.Tag = "colhAccType"
        Me.colhAccType.Text = "Account Type"
        Me.colhAccType.Width = 130
        '
        'colhAccNo
        '
        Me.colhAccNo.Tag = "colhAccNo"
        Me.colhAccNo.Text = "Account No"
        Me.colhAccNo.Width = 130
        '
        'colhPerc
        '
        Me.colhPerc.Tag = "colhPerc"
        Me.colhPerc.Text = "(%)"
        Me.colhPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhPerc.Width = 100
        '
        'colhGUID
        '
        Me.colhGUID.Text = "GUID"
        Me.colhGUID.Width = 0
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = ""
        Me.colhPeriod.Width = 0
        '
        'colhModeID
        '
        Me.colhModeID.Tag = "colhModeID"
        Me.colhModeID.Text = "Mode"
        Me.colhModeID.Width = 0
        '
        'lblBank
        '
        Me.lblBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBank.Location = New System.Drawing.Point(8, 87)
        Me.lblBank.Name = "lblBank"
        Me.lblBank.Size = New System.Drawing.Size(78, 15)
        Me.lblBank.TabIndex = 4
        Me.lblBank.Text = "Branch"
        Me.lblBank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBankGroup
        '
        Me.cboBankGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankGroup.DropDownWidth = 180
        Me.cboBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankGroup.FormattingEnabled = True
        Me.cboBankGroup.Location = New System.Drawing.Point(89, 57)
        Me.cboBankGroup.Name = "cboBankGroup"
        Me.cboBankGroup.Size = New System.Drawing.Size(154, 21)
        Me.cboBankGroup.TabIndex = 5
        '
        'lblBankGroup
        '
        Me.lblBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankGroup.Location = New System.Drawing.Point(8, 60)
        Me.lblBankGroup.Name = "lblBankGroup"
        Me.lblBankGroup.Size = New System.Drawing.Size(78, 15)
        Me.lblBankGroup.TabIndex = 2
        Me.lblBankGroup.Text = "Bank Group"
        Me.lblBankGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 33)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(78, 15)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Effec. Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(89, 30)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(154, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'objbtnAddReminder
        '
        Me.objbtnAddReminder.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReminder.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReminder.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReminder.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReminder.BorderSelected = False
        Me.objbtnAddReminder.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReminder.Image = Global.Aruti.Main.My.Resources.Resources.Remind
        Me.objbtnAddReminder.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReminder.Location = New System.Drawing.Point(199, 153)
        Me.objbtnAddReminder.Name = "objbtnAddReminder"
        Me.objbtnAddReminder.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReminder.TabIndex = 13
        '
        'txtDBFirstName
        '
        Me.txtDBFirstName.BackColor = System.Drawing.Color.White
        Me.txtDBFirstName.Flags = 0
        Me.txtDBFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBFirstName.Location = New System.Drawing.Point(87, 72)
        Me.txtDBFirstName.Name = "txtDBFirstName"
        Me.txtDBFirstName.Size = New System.Drawing.Size(176, 21)
        Me.txtDBFirstName.TabIndex = 6
        '
        'lblDBFirstName
        '
        Me.lblDBFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBFirstName.Location = New System.Drawing.Point(8, 75)
        Me.lblDBFirstName.Name = "lblDBFirstName"
        Me.lblDBFirstName.Size = New System.Drawing.Size(73, 15)
        Me.lblDBFirstName.TabIndex = 5
        Me.lblDBFirstName.Text = "Firstname"
        Me.lblDBFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBMiddleName
        '
        Me.lblDBMiddleName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBMiddleName.Location = New System.Drawing.Point(8, 102)
        Me.lblDBMiddleName.Name = "lblDBMiddleName"
        Me.lblDBMiddleName.Size = New System.Drawing.Size(73, 15)
        Me.lblDBMiddleName.TabIndex = 7
        Me.lblDBMiddleName.Text = "Middlename"
        Me.lblDBMiddleName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBLastName
        '
        Me.lblDBLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBLastName.Location = New System.Drawing.Point(8, 129)
        Me.lblDBLastName.Name = "lblDBLastName"
        Me.lblDBLastName.Size = New System.Drawing.Size(73, 15)
        Me.lblDBLastName.TabIndex = 9
        Me.lblDBLastName.Text = "Lastname"
        Me.lblDBLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDBMiddleName
        '
        Me.txtDBMiddleName.Flags = 0
        Me.txtDBMiddleName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBMiddleName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBMiddleName.Location = New System.Drawing.Point(87, 99)
        Me.txtDBMiddleName.Name = "txtDBMiddleName"
        Me.txtDBMiddleName.Size = New System.Drawing.Size(176, 21)
        Me.txtDBMiddleName.TabIndex = 8
        '
        'txtDBLastName
        '
        Me.txtDBLastName.Flags = 0
        Me.txtDBLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBLastName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBLastName.Location = New System.Drawing.Point(87, 126)
        Me.txtDBLastName.Name = "txtDBLastName"
        Me.txtDBLastName.Size = New System.Drawing.Size(176, 21)
        Me.txtDBLastName.TabIndex = 10
        '
        'txtAddressLine1
        '
        Me.txtAddressLine1.Flags = 0
        Me.txtAddressLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddressLine1.Location = New System.Drawing.Point(506, 60)
        Me.txtAddressLine1.Name = "txtAddressLine1"
        Me.txtAddressLine1.Size = New System.Drawing.Size(293, 21)
        Me.txtAddressLine1.TabIndex = 22
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(87, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(301, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'cboDBRelation
        '
        Me.cboDBRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBRelation.FormattingEnabled = True
        Me.cboDBRelation.Location = New System.Drawing.Point(87, 207)
        Me.cboDBRelation.Name = "cboDBRelation"
        Me.cboDBRelation.Size = New System.Drawing.Size(149, 21)
        Me.cboDBRelation.TabIndex = 15
        '
        'lblDBBirthDate
        '
        Me.lblDBBirthDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBBirthDate.Location = New System.Drawing.Point(8, 156)
        Me.lblDBBirthDate.Name = "lblDBBirthDate"
        Me.lblDBBirthDate.Size = New System.Drawing.Size(73, 15)
        Me.lblDBBirthDate.TabIndex = 11
        Me.lblDBBirthDate.Text = "Birth"
        Me.lblDBBirthDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBRelation
        '
        Me.lblDBRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBRelation.Location = New System.Drawing.Point(8, 210)
        Me.lblDBRelation.Name = "lblDBRelation"
        Me.lblDBRelation.Size = New System.Drawing.Size(73, 15)
        Me.lblDBRelation.TabIndex = 14
        Me.lblDBRelation.Text = "Relation"
        Me.lblDBRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(393, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'lblDBAddress
        '
        Me.lblDBAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBAddress.Location = New System.Drawing.Point(427, 63)
        Me.lblDBAddress.Name = "lblDBAddress"
        Me.lblDBAddress.Size = New System.Drawing.Size(73, 15)
        Me.lblDBAddress.TabIndex = 21
        Me.lblDBAddress.Text = "Address"
        Me.lblDBAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDBBirthDate
        '
        Me.dtpDBBirthDate.Checked = False
        Me.dtpDBBirthDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDBBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDBBirthDate.Location = New System.Drawing.Point(87, 153)
        Me.dtpDBBirthDate.Name = "dtpDBBirthDate"
        Me.dtpDBBirthDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpDBBirthDate.TabIndex = 12
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(73, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        '
        'cboDBPostCountry
        '
        Me.cboDBPostCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBPostCountry.FormattingEnabled = True
        Me.cboDBPostCountry.Location = New System.Drawing.Point(506, 87)
        Me.cboDBPostCountry.Name = "cboDBPostCountry"
        Me.cboDBPostCountry.Size = New System.Drawing.Size(108, 21)
        Me.cboDBPostCountry.TabIndex = 24
        '
        'txtDBTelNo
        '
        Me.txtDBTelNo.Flags = 0
        Me.txtDBTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBTelNo.Location = New System.Drawing.Point(506, 195)
        Me.txtDBTelNo.Name = "txtDBTelNo"
        Me.txtDBTelNo.Size = New System.Drawing.Size(108, 21)
        Me.txtDBTelNo.TabIndex = 40
        '
        'cboPostState
        '
        Me.cboPostState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPostState.FormattingEnabled = True
        Me.cboPostState.Location = New System.Drawing.Point(691, 87)
        Me.cboPostState.Name = "cboPostState"
        Me.cboPostState.Size = New System.Drawing.Size(108, 21)
        Me.cboPostState.TabIndex = 26
        '
        'cboDBPostTown
        '
        Me.cboDBPostTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBPostTown.FormattingEnabled = True
        Me.cboDBPostTown.Location = New System.Drawing.Point(506, 114)
        Me.cboDBPostTown.Name = "cboDBPostTown"
        Me.cboDBPostTown.Size = New System.Drawing.Size(108, 21)
        Me.cboDBPostTown.TabIndex = 28
        '
        'lblDBNationality
        '
        Me.lblDBNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBNationality.Location = New System.Drawing.Point(427, 144)
        Me.lblDBNationality.Name = "lblDBNationality"
        Me.lblDBNationality.Size = New System.Drawing.Size(73, 15)
        Me.lblDBNationality.TabIndex = 31
        Me.lblDBNationality.Text = "Nationality"
        Me.lblDBNationality.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBPostCode
        '
        Me.lblDBPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBPostCode.Location = New System.Drawing.Point(620, 117)
        Me.lblDBPostCode.Name = "lblDBPostCode"
        Me.lblDBPostCode.Size = New System.Drawing.Size(65, 15)
        Me.lblDBPostCode.TabIndex = 29
        Me.lblDBPostCode.Text = "Post Code"
        Me.lblDBPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(620, 90)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(65, 15)
        Me.lblState.TabIndex = 25
        Me.lblState.Text = "Post State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBResidentialNo
        '
        Me.lblDBResidentialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBResidentialNo.Location = New System.Drawing.Point(427, 198)
        Me.lblDBResidentialNo.Name = "lblDBResidentialNo"
        Me.lblDBResidentialNo.Size = New System.Drawing.Size(73, 15)
        Me.lblDBResidentialNo.TabIndex = 39
        Me.lblDBResidentialNo.Text = "Res. Tel No."
        Me.lblDBResidentialNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPostCode
        '
        Me.cboPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPostCode.FormattingEnabled = True
        Me.cboPostCode.Location = New System.Drawing.Point(691, 114)
        Me.cboPostCode.Name = "cboPostCode"
        Me.cboPostCode.Size = New System.Drawing.Size(108, 21)
        Me.cboPostCode.TabIndex = 30
        '
        'lblDBEmail
        '
        Me.lblDBEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBEmail.Location = New System.Drawing.Point(620, 144)
        Me.lblDBEmail.Name = "lblDBEmail"
        Me.lblDBEmail.Size = New System.Drawing.Size(65, 15)
        Me.lblDBEmail.TabIndex = 33
        Me.lblDBEmail.Text = "Email"
        Me.lblDBEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBPostCountry
        '
        Me.lblDBPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBPostCountry.Location = New System.Drawing.Point(427, 90)
        Me.lblDBPostCountry.Name = "lblDBPostCountry"
        Me.lblDBPostCountry.Size = New System.Drawing.Size(73, 15)
        Me.lblDBPostCountry.TabIndex = 23
        Me.lblDBPostCountry.Text = "Post Country"
        Me.lblDBPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBPostBox
        '
        Me.lblDBPostBox.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBPostBox.Location = New System.Drawing.Point(427, 171)
        Me.lblDBPostBox.Name = "lblDBPostBox"
        Me.lblDBPostBox.Size = New System.Drawing.Size(73, 15)
        Me.lblDBPostBox.TabIndex = 35
        Me.lblDBPostBox.Text = "Post Box"
        Me.lblDBPostBox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDBIdNo
        '
        Me.txtDBIdNo.Flags = 0
        Me.txtDBIdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBIdNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBIdNo.Location = New System.Drawing.Point(691, 168)
        Me.txtDBIdNo.Name = "txtDBIdNo"
        Me.txtDBIdNo.Size = New System.Drawing.Size(108, 21)
        Me.txtDBIdNo.TabIndex = 38
        '
        'lblDBIdNo
        '
        Me.lblDBIdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBIdNo.Location = New System.Drawing.Point(620, 171)
        Me.lblDBIdNo.Name = "lblDBIdNo"
        Me.lblDBIdNo.Size = New System.Drawing.Size(65, 15)
        Me.lblDBIdNo.TabIndex = 37
        Me.lblDBIdNo.Text = "ID No."
        Me.lblDBIdNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDBPostBox
        '
        Me.txtDBPostBox.Flags = 0
        Me.txtDBPostBox.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBPostBox.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBPostBox.Location = New System.Drawing.Point(506, 168)
        Me.txtDBPostBox.Name = "txtDBPostBox"
        Me.txtDBPostBox.Size = New System.Drawing.Size(108, 21)
        Me.txtDBPostBox.TabIndex = 36
        '
        'lblDBMobileNo
        '
        Me.lblDBMobileNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBMobileNo.Location = New System.Drawing.Point(620, 198)
        Me.lblDBMobileNo.Name = "lblDBMobileNo"
        Me.lblDBMobileNo.Size = New System.Drawing.Size(65, 15)
        Me.lblDBMobileNo.TabIndex = 41
        Me.lblDBMobileNo.Text = "Mobile No"
        Me.lblDBMobileNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(691, 141)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(108, 21)
        Me.txtEmail.TabIndex = 34
        '
        'lblDBPostTown
        '
        Me.lblDBPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBPostTown.Location = New System.Drawing.Point(427, 117)
        Me.lblDBPostTown.Name = "lblDBPostTown"
        Me.lblDBPostTown.Size = New System.Drawing.Size(73, 15)
        Me.lblDBPostTown.TabIndex = 27
        Me.lblDBPostTown.Text = "Post Town"
        Me.lblDBPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDBNationality
        '
        Me.cboDBNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBNationality.FormattingEnabled = True
        Me.cboDBNationality.Location = New System.Drawing.Point(506, 141)
        Me.cboDBNationality.Name = "cboDBNationality"
        Me.cboDBNationality.Size = New System.Drawing.Size(108, 21)
        Me.cboDBNationality.TabIndex = 32
        '
        'txtDBPersonalNo
        '
        Me.txtDBPersonalNo.Flags = 0
        Me.txtDBPersonalNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBPersonalNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBPersonalNo.Location = New System.Drawing.Point(691, 195)
        Me.txtDBPersonalNo.Name = "txtDBPersonalNo"
        Me.txtDBPersonalNo.Size = New System.Drawing.Size(108, 21)
        Me.txtDBPersonalNo.TabIndex = 42
        '
        'objeZeeFooter
        '
        Me.objeZeeFooter.BorderColor = System.Drawing.Color.Silver
        Me.objeZeeFooter.Controls.Add(Me.btnSave)
        Me.objeZeeFooter.Controls.Add(Me.btnClose)
        Me.objeZeeFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objeZeeFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objeZeeFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objeZeeFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.Location = New System.Drawing.Point(0, 515)
        Me.objeZeeFooter.Name = "objeZeeFooter"
        Me.objeZeeFooter.Size = New System.Drawing.Size(811, 55)
        Me.objeZeeFooter.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(600, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(702, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ofdAttachment
        '
        Me.ofdAttachment.FileName = "OpenFileDialog1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "File Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 180
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn2.HeaderText = "File Size"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objcolhGUID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objcolhScanUnkId"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'cboDBLoanScheme
        '
        Me.cboDBLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBLoanScheme.FormattingEnabled = True
        Me.cboDBLoanScheme.Location = New System.Drawing.Point(87, 234)
        Me.cboDBLoanScheme.Name = "cboDBLoanScheme"
        Me.cboDBLoanScheme.Size = New System.Drawing.Size(149, 21)
        Me.cboDBLoanScheme.TabIndex = 64
        '
        'lblDBLoanScheme
        '
        Me.lblDBLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBLoanScheme.Location = New System.Drawing.Point(8, 237)
        Me.lblDBLoanScheme.Name = "lblDBLoanScheme"
        Me.lblDBLoanScheme.Size = New System.Drawing.Size(73, 15)
        Me.lblDBLoanScheme.TabIndex = 63
        Me.lblDBLoanScheme.Text = "Loan Scheme"
        Me.lblDBLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDBTranHead
        '
        Me.cboDBTranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBTranHead.FormattingEnabled = True
        Me.cboDBTranHead.Location = New System.Drawing.Point(310, 235)
        Me.cboDBTranHead.Name = "cboDBTranHead"
        Me.cboDBTranHead.Size = New System.Drawing.Size(111, 21)
        Me.cboDBTranHead.TabIndex = 66
        '
        'lblDBTranHead
        '
        Me.lblDBTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBTranHead.Location = New System.Drawing.Point(246, 237)
        Me.lblDBTranHead.Name = "lblDBTranHead"
        Me.lblDBTranHead.Size = New System.Drawing.Size(64, 15)
        Me.lblDBTranHead.TabIndex = 65
        Me.lblDBTranHead.Text = "Tran. Head"
        Me.lblDBTranHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmDependantsAndBeneficiaries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(811, 570)
        Me.Controls.Add(Me.pnlDependantsAndBeneficiaries)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDependantsAndBeneficiaries"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Dependants And Beneficiaries"
        Me.pnlDependantsAndBeneficiaries.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.gbDBInfo.ResumeLayout(False)
        Me.gbDBInfo.PerformLayout()
        Me.tabcOtherDetails.ResumeLayout(False)
        Me.tabpMembership.ResumeLayout(False)
        Me.gbAddMembership.ResumeLayout(False)
        Me.gbAddMembership.PerformLayout()
        Me.tabpBenefitInfo.ResumeLayout(False)
        Me.gbBenefitInfo.ResumeLayout(False)
        Me.gbBenefitInfo.PerformLayout()
        Me.tabAttachment.ResumeLayout(False)
        Me.dbDocumentAttachment.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvDepedantAttachment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tapbBankDetails.ResumeLayout(False)
        Me.gbBankDetails.ResumeLayout(False)
        Me.gbBankDetails.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.objeZeeFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlDependantsAndBeneficiaries As System.Windows.Forms.Panel
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objeZeeFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbDBInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDBTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDBPostTown As System.Windows.Forms.ComboBox
    Friend WithEvents txtDBPersonalNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDBResidentialNo As System.Windows.Forms.Label
    Friend WithEvents lblDBMobileNo As System.Windows.Forms.Label
    Friend WithEvents lblDBAddress As System.Windows.Forms.Label
    Friend WithEvents lblDBIdNo As System.Windows.Forms.Label
    Friend WithEvents txtDBIdNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDBEmail As System.Windows.Forms.Label
    Friend WithEvents cboDBPostCountry As System.Windows.Forms.ComboBox
    Friend WithEvents cboDBNationality As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBPostCountry As System.Windows.Forms.Label
    Friend WithEvents lblDBNationality As System.Windows.Forms.Label
    Friend WithEvents lblDBPostCode As System.Windows.Forms.Label
    Friend WithEvents lblDBPostTown As System.Windows.Forms.Label
    Friend WithEvents txtDBPostBox As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDBPostBox As System.Windows.Forms.Label
    Friend WithEvents cboPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents txtAddressLine1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPostState As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents imgDependants As eZee.Common.eZeeImageControl
    Friend WithEvents cboBenefitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitGroup As System.Windows.Forms.Label
    Friend WithEvents cboBenefitType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBBenefitType As System.Windows.Forms.Label
    Friend WithEvents lblBenefitsInPercent As System.Windows.Forms.Label
    Friend WithEvents txtBenefitAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDBBenefitInAmount As System.Windows.Forms.Label
    Friend WithEvents txtBenefitPercent As eZee.TextBox.NumericTextBox
    Friend WithEvents txtDBFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDBFirstName As System.Windows.Forms.Label
    Friend WithEvents lblDBMiddleName As System.Windows.Forms.Label
    Friend WithEvents lblDBLastName As System.Windows.Forms.Label
    Friend WithEvents txtDBMiddleName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDBLastName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboDBRelation As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBRelation As System.Windows.Forms.Label
    Friend WithEvents lblDBBirthDate As System.Windows.Forms.Label
    Friend WithEvents dtpDBBirthDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnAddReminder As eZee.Common.eZeeGradientButton
    Friend WithEvents tabcOtherDetails As System.Windows.Forms.TabControl
    Friend WithEvents tabpMembership As System.Windows.Forms.TabPage
    Friend WithEvents tabpBenefitInfo As System.Windows.Forms.TabPage
    Friend WithEvents lblMemberNo As System.Windows.Forms.Label
    Friend WithEvents txtMembershipNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboMedicalNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBMedicalMembershipNo As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objStLine As eZee.Common.eZeeStraightLine
    Friend WithEvents chkBeneficiaries As System.Windows.Forms.CheckBox
    Friend WithEvents lnkCopyAddress As System.Windows.Forms.LinkLabel
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditMembership As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddMembership As eZee.Common.eZeeLightButton
    Friend WithEvents gbAddMembership As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvMembershipInfo As eZee.Common.eZeeListView
    Friend WithEvents colhMembership As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMembershipNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMembershipUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbBenefitInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboValueBasis As System.Windows.Forms.ComboBox
    Friend WithEvents lblValueBasis As System.Windows.Forms.Label
    Friend WithEvents btnDeleteBenefit As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditBenefit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddBenefit As eZee.Common.eZeeLightButton
    Friend WithEvents objstLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents lvBenefit As eZee.Common.eZeeListView
    Friend WithEvents colhBenefitGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhValueBasis As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPercent As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhBenefitGrpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhBenefitId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhValueBasisId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhBGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboMemCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembershipCategory As System.Windows.Forms.Label
    Friend WithEvents colhMembershipType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMembershipCatId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddRelation As eZee.Common.eZeeGradientButton
    Friend WithEvents objAddMedMem As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddMemCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBenefitType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBenefitGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents txtTotBeneficiaries As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtAgeLimit As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLimit As System.Windows.Forms.Label
    Friend WithEvents txtAge As eZee.TextBox.NumericTextBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents tabAttachment As System.Windows.Forms.TabPage
    Friend WithEvents dbDocumentAttachment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvDepedantAttachment As System.Windows.Forms.DataGridView
    Friend WithEvents objcohDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents btnAddAttachment As eZee.Common.eZeeLightButton
    Friend WithEvents lblDocumentType As System.Windows.Forms.Label
    Friend WithEvents lblAttachmentDate As System.Windows.Forms.Label
    Friend WithEvents dtpAttachmentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents ofdAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents sfdAttachment As System.Windows.Forms.SaveFileDialog
    Friend WithEvents objlblAge As System.Windows.Forms.Label
    Friend WithEvents txtRemainingPercent As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalRemainingPercent As System.Windows.Forms.Label
    Friend WithEvents lblTotalPercent As System.Windows.Forms.Label
    Friend WithEvents txtTotalPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents tapbBankDetails As System.Windows.Forms.TabPage
    Friend WithEvents gbBankDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDown As eZee.Common.eZeeLightButton
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblPerc As System.Windows.Forms.Label
    Friend WithEvents lblSalaryDistrib As System.Windows.Forms.Label
    Friend WithEvents txtPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnAddAccType As eZee.Common.eZeeGradientButton
    Friend WithEvents txtAccountNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAccountNo As System.Windows.Forms.Label
    Friend WithEvents lblAccountType As System.Windows.Forms.Label
    Friend WithEvents cboAccountType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchBankBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBankGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBankGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboBankBranch As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnDeleteEmpDetails As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditEmpDetails As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddEmpDetails As eZee.Common.eZeeLightButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lvEmpBankList As eZee.Common.eZeeListView
    Friend WithEvents colhID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBankGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBankBranch As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPerc As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhModeID As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblBank As System.Windows.Forms.Label
    Friend WithEvents cboBankGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankGroup As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboDBTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBTranHead As System.Windows.Forms.Label
    Friend WithEvents cboDBLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblDBLoanScheme As System.Windows.Forms.Label
End Class
