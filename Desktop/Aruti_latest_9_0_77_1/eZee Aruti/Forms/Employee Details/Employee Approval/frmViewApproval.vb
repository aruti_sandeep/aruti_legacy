﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmViewApproval

#Region " Private Varaibles "

    Private objEmpApprovalTran As New clsemployeeapproval_Tran
    Private ReadOnly mstrModuleName As String = "frmViewApproval"
    Private mintUserId As Integer
    Private mintPriority As Integer

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUserId As Integer, ByVal intPriority As Integer) As Boolean
        Try
            mintUserId = intUserId
            mintPriority = intPriority
            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillGrid()
        Try
            Dim dtTable As New DataTable
            dtTable = objEmpApprovalTran.GetEmployeeApprovalData(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, 344, mintUserId, mintPriority, False)

            dgvData.AutoGenerateColumns = False

            objchkCheck.DataPropertyName = "icheck"
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhDepartment.DataPropertyName = "Department"
            dgcolhJob.DataPropertyName = "Job"
            dgcolhClassGrp.DataPropertyName = "ClassGroup"
            dgcolhClass.DataPropertyName = "Class"
            dgcolhApprover.DataPropertyName = "username"
            dgcolhLevel.DataPropertyName = "Level"
            dgcolhPriority.DataPropertyName = "priority"
            dgcolhStatus.DataPropertyName = "Status"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolhApprUsrId.DataPropertyName = "userunkid"
            objdgcolhMappingId.DataPropertyName = "mappingid"
            objdgcolhiRead.DataPropertyName = "iRead"

            dgvData.DataSource = dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmViewApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpApprovalTran = New clsemployeeapproval_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmViewApproval_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmpApprovalTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployeeapproval_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployeeapproval_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhClassGrp.HeaderText = Language._Object.getCaption(Me.dgcolhClassGrp.Name, Me.dgcolhClassGrp.HeaderText)
            Me.dgcolhClass.HeaderText = Language._Object.getCaption(Me.dgcolhClass.Name, Me.dgcolhClass.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhLevel.HeaderText = Language._Object.getCaption(Me.dgcolhLevel.Name, Me.dgcolhLevel.HeaderText)
            Me.dgcolhPriority.HeaderText = Language._Object.getCaption(Me.dgcolhPriority.Name, Me.dgcolhPriority.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class