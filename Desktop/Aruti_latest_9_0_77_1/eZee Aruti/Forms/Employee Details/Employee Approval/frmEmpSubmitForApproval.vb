﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System.Text

#End Region

Public Class frmEmpSubmitForApproval

#Region " Private Varaibles "

    Private objEmpApprovalTran As New clsemployeeapproval_Tran
    Private ReadOnly mstrModuleName As String = "frmEmpSubmitForApproval"
    Private dtEmpView As DataView = Nothing

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            cboDisplayType.Items.Clear()
            cboDisplayType.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboDisplayType.Items.Add(Language.getMessage(mstrModuleName, 2, "Completed On Board Check List"))
            cboDisplayType.Items.Add(Language.getMessage(mstrModuleName, 3, "Incompleted On Board Check List"))
            'S.SANDEEP [19-NOV-2018] -- START
            cboDisplayType.Items.Add(Language.getMessage(mstrModuleName, 4, "Submitted For Approval List"))
            'S.SANDEEP [19-NOV-2018] -- END
            cboDisplayType.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dtTable As DataTable = Nothing
            If CInt(cboDisplayType.SelectedIndex) = 1 Then  'Completed On Board List
                dtTable = objEmployee.GetEmpOnBoardList(Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Languageunkid, ConfigParameter._Object._PendingEmployeeScreenIDs.Trim, ConfigParameter._Object._UserAccessModeSetting, User._Object._Userunkid, False)
            ElseIf CInt(cboDisplayType.SelectedIndex) = 2 Then 'InCompleted On Board List
                dtTable = objEmployee.GetEmpOnBoardList(Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Languageunkid, ConfigParameter._Object._PendingEmployeeScreenIDs.Trim, ConfigParameter._Object._UserAccessModeSetting, User._Object._Userunkid, True)
                'S.SANDEEP [19-NOV-2018] -- START
            ElseIf CInt(cboDisplayType.SelectedIndex) = 3 Then 'Submitted For Approval List
                dtTable = objEmployee.GetEmpOnBoardList(Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, User._Object._Languageunkid, ConfigParameter._Object._PendingEmployeeScreenIDs.Trim, ConfigParameter._Object._UserAccessModeSetting, User._Object._Userunkid, False, True)
                'S.SANDEEP [19-NOV-2018] -- END
            End If
            objEmployee = Nothing

            If dtTable Is Nothing Then Exit Sub

            dgEmployeeList.AutoGenerateColumns = False
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhDepartment.DataPropertyName = "Department"
            dgcolhJob.DataPropertyName = "Job"
            dgcolhClassGrp.DataPropertyName = "ClassGroup"
            dgcolhClass.DataPropertyName = "Class"
            dgcolhScreenName.DataPropertyName = "ScreenName"

            objdgcolhEmployeeID.DataPropertyName = "employeeunkid"
            objdgcolhDepartmentID.DataPropertyName = "departmentunkid"
            objdgcolhJobId.DataPropertyName = "jobunkid"
            objdgcolhClassGrpID.DataPropertyName = "classgroupunkid"
            objdgcolhClassID.DataPropertyName = "classunkid"


            If dtTable IsNot Nothing Then
                dtEmpView = dtTable.DefaultView
            Else
                dtEmpView = Nothing
            End If

            dgEmployeeList.DataSource = dtEmpView

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try
            objEmpApprovalTran._Tranguid = Guid.NewGuid().ToString()
            objEmpApprovalTran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            objEmpApprovalTran._Mappingunkid = -1
            objEmpApprovalTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
            objEmpApprovalTran._Isfinal = False
            objEmpApprovalTran._Isnotified = False
            objEmpApprovalTran._Remark = ""
            objEmpApprovalTran._Isvoid = False
            objEmpApprovalTran._Audittype = 1
            objEmpApprovalTran._Audituserunkid = User._Object._Userunkid
            objEmpApprovalTran._Form_Name = mstrModuleName
            objEmpApprovalTran._Hostname = getHostName()
            objEmpApprovalTran._Ip = getIP()
            objEmpApprovalTran._Isweb = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub ResetControls()
        Try
            cboDisplayType.SelectedIndex = 0
            FillEmployeeList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ResetControls", mstrModuleName)
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Function UpdateGUID(ByVal x As DataRow, ByVal strGUID As String) As Boolean
        If x.Item("tranguid").ToString() = "" Then
            x.Item("tranguid") = strGUID
        End If
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmpSubmitForApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpApprovalTran = New clsemployeeapproval_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpSubmitForApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpSubmitForApproval_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmpApprovalTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployeeapproval_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployeeapproval_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboDisplayType.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select display type to do further operation on it."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboDisplayType.Select()
                Exit Sub
            End If
            FillEmployeeList()
            If dtEmpView IsNot Nothing Then
                objbtnSearch.ShowResult(dtEmpView.ToTable.Rows.Count.ToString())
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            ResetControls()
            If dtEmpView IsNot Nothing Then
                objbtnSearch.ShowResult(dtEmpView.ToTable.Rows.Count.ToString())
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSubmitForApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
        Try
            If dgEmployeeList.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry,There are no record(s) to submit for approval."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            Dim dtTable As DataTable = CType(dgEmployeeList.DataSource, DataView).ToTable()
            If dtTable IsNot Nothing Then
                Dim strEmpUnkids As String = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
                Dim dtAppr As DataTable = objEmpApprovalTran.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, strEmpUnkids, ConfigParameter._Object._UserAccessModeSetting, 344)
                If dtAppr.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, No approvers having access of allocation in access mode setting defined in configuration."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If dtAppr.AsEnumerable().Where(Function(x) x.Field(Of String)("email").ToString().Trim() = "").Count > 0 Then
                    Dim strUser As String = String.Join(",", dtAppr.AsEnumerable().Where(Function(x) x.Field(Of String)("email").ToString().Trim() = "").Select(Function(x) x.Field(Of String)("username")).Distinct().ToArray())
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Some of the approver(s) does not have email address assigned to them. Please assign them emails to continue. Below are the list of approver(s)") & _
                                    vbCrLf & "[" & strUser & "]", enMsgBoxStyle.Information)
                    Exit Sub
                End If

                SetValue()

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEmpApprovalTran.Insert(dtTable, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) = False Then
                'Sohail (27 Nov 2018) -- Start
                'NMB Issue - Email is sent to employee everytime whenver employee is updated and send company details to newly approved employee option is ticked in 75.1.
                'If objEmpApprovalTran.Insert(dtTable, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) = False Then
                If objEmpApprovalTran.Insert(dtTable, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, ConfigParameter._Object._SendDetailToEmployee, ConfigParameter._Object._SMSCompanyDetailToNewEmp, getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP, False, Nothing, "", "", Company._Object._Senderaddress, ConfigParameter._Object._SMSGatewayEmail, ConfigParameter._Object._SMSGatewayEmailType) = False Then
                    'Sohail (27 Nov 2018) -- End
                    'Pinkal (18-Aug-2018) -- End
                    eZeeMsgBox.Show(objEmpApprovalTran._Message)
                Else
                    If dtTable IsNot Nothing Then
                        dtTable.AsEnumerable().ToList.ForEach(Function(x) UpdateGUID(x, objEmpApprovalTran._Tranguid))
                        Dim iDictionary As Dictionary(Of Integer, String) = dtTable.AsEnumerable().ToDictionary(Function(x) x.Field(Of Integer)("employeeunkid"), Function(y) y.Field(Of String)("tranguid"))
                        Call objEmpApprovalTran.SendSubmitNotification(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, strEmpUnkids, ConfigParameter._Object._UserAccessModeSetting, 344, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Email, ConfigParameter._Object._ArutiSelfServiceURL, iDictionary)
                    End If
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Submit for approval done successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    FillEmployeeList()
                End If
            End If



            
            dtTable = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSubmitForApproval_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If CType(dgEmployeeList.DataSource, DataView) IsNot Nothing Then
                Dim dsData As New DataSet
                Dim dtTable As DataTable = CType(dgEmployeeList.DataSource, DataView).ToTable()
                dtTable.Columns.Remove("employeeunkid")
                dtTable.Columns.Remove("departmentunkid")
                dtTable.Columns.Remove("jobunkid")
                dtTable.Columns.Remove("classgroupunkid")
                dtTable.Columns.Remove("classunkid")
                dtTable.Columns.Remove("Employeecode")
                dtTable.Columns.Remove("FirstName")
                dtTable.Columns.Remove("OtherName")
                dtTable.Columns.Remove("Surname")
                'S.SANDEEP [19-NOV-2018] -- START
                If dtTable.Columns.Contains("isfinal") = True Then
                    dtTable.Columns.Remove("isfinal")
                End If
                If dtTable.Columns.Contains("tranguid") = True Then
                    dtTable.Columns.Remove("tranguid")
                End If
                'S.SANDEEP [19-NOV-2018] -- END
                dsData.Tables.Add(dtTable)
                Dim svfile As New SaveFileDialog
                svfile.Filter = "Excel Files (.xlsx)|*.xlsx"
                If svfile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Aruti.Data.modGlobal.OpenXML_Export(svfile.FileName, dsData)
                End If
                If svfile.FileName.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No file name specified for export operation."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "File exported successfully to the selected path."), enMsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If dtEmpView IsNot Nothing Then
                Dim str As String = ""
                If (txtSearch.Text.Length > 0) Then
                    str = dgcolhEmployee.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                          dgcolhEmployee.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                          dgcolhDepartment.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                          dgcolhJob.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                          dgcolhClassGrp.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                          dgcolhClass.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' "
                    If dgcolhScreenName.Visible = True Then
                        str &= " OR " & dgcolhScreenName.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' "
                    End If
                End If
                dtEmpView.RowFilter = str
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagrid Event(s) "

    Private Sub dgEmployeeList_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployeeList.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhPreview.Index
                    Dim strData As String = ""
                    Dim objListing As New ArutiReports.clsEmployee_Listing(User._Object._Languageunkid, Company._Object._Companyunkid)
                    Dim blnIsFinYear As Boolean = False
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        blnIsFinYear = True
                    End If

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'strData = objListing.ViewEmployeeDetails(CInt(dgEmployeeList.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value), _
                    '                                         blnIsFinYear, GUI.fmtCurrency, ConfigParameter._Object._ShowFirstAppointmentDate, _
                    '                                         User._Object.Privilege._AllowTo_View_Scale, _
                    '                                         ConfigParameter._Object._EmpMandatoryFieldsIDs, ConfigParameter._Object._PendingEmployeeScreenIDs, _
                    '                                         True, True, True, True)

                    strData = objListing.ViewEmployeeDetails(CInt(dgEmployeeList.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value), _
                                                             blnIsFinYear, GUI.fmtCurrency, ConfigParameter._Object._ShowFirstAppointmentDate, _
                                                             User._Object.Privilege._AllowTo_View_Scale, _
                                                             ConfigParameter._Object._EmpMandatoryFieldsIDs, ConfigParameter._Object._PendingEmployeeScreenIDs, _
                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, True, True, True, True)
                    'Pinkal (18-Aug-2018) -- End


                    If strData.Trim.Length > 0 Then
                        Dim frm As New frmHTMLReportView
                        frm.displayDialog(strData, False)
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployeeList_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Private Sub cboDisplayType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDisplayType.SelectedIndexChanged
        Try
            'S.SANDEEP [19-NOV-2018] -- START
            'If CInt(cboDisplayType.SelectedIndex) = 0 OrElse CInt(cboDisplayType.SelectedIndex) = 2 Then
            If CInt(cboDisplayType.SelectedIndex) = 0 OrElse CInt(cboDisplayType.SelectedIndex) = 2 OrElse CInt(cboDisplayType.SelectedIndex) = 3 Then
                'S.SANDEEP [19-NOV-2018] -- END
                btnSubmitForApproval.Enabled = False
                If CInt(cboDisplayType.SelectedIndex) = 0 Then btnExport.Enabled = False Else btnExport.Enabled = True
                dgcolhScreenName.Visible = True
            ElseIf CInt(cboDisplayType.SelectedIndex) = 1 Then
                btnExport.Enabled = False
                btnSubmitForApproval.Enabled = True
                dgcolhScreenName.Visible = False
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboDisplayType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSubmitForApproval.GradientBackColor = GUI._ButttonBackColor
            Me.btnSubmitForApproval.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.btnSubmitForApproval.Text = Language._Object.getCaption(Me.btnSubmitForApproval.Name, Me.btnSubmitForApproval.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.LblDisplayType.Text = Language._Object.getCaption(Me.LblDisplayType.Name, Me.LblDisplayType.Text)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhClassGrp.HeaderText = Language._Object.getCaption(Me.dgcolhClassGrp.Name, Me.dgcolhClassGrp.HeaderText)
            Me.dgcolhClass.HeaderText = Language._Object.getCaption(Me.dgcolhClass.Name, Me.dgcolhClass.HeaderText)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Complete On Board Check List")
            Language.setMessage(mstrModuleName, 3, "Incomplete On Board Check List")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class