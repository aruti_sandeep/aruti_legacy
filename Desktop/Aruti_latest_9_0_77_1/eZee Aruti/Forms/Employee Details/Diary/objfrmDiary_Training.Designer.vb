﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Training
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.elTrininEnrolment = New eZee.Common.eZeeLine
        Me.lvEnrollList = New System.Windows.Forms.ListView
        Me.colhTrainingtitle = New System.Windows.Forms.ColumnHeader
        Me.colhEnrollDate = New System.Windows.Forms.ColumnHeader
        Me.colhStDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elTrininEnrolment
        '
        Me.elTrininEnrolment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elTrininEnrolment.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elTrininEnrolment.Location = New System.Drawing.Point(2, 6)
        Me.elTrininEnrolment.Name = "elTrininEnrolment"
        Me.elTrininEnrolment.Size = New System.Drawing.Size(575, 17)
        Me.elTrininEnrolment.TabIndex = 309
        Me.elTrininEnrolment.Text = "Training Enrolled"
        Me.elTrininEnrolment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvEnrollList
        '
        Me.lvEnrollList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhTrainingtitle, Me.colhEnrollDate, Me.colhStDate, Me.colhEndDate, Me.colhStatus})
        Me.lvEnrollList.FullRowSelect = True
        Me.lvEnrollList.Location = New System.Drawing.Point(5, 26)
        Me.lvEnrollList.Name = "lvEnrollList"
        Me.lvEnrollList.Size = New System.Drawing.Size(572, 380)
        Me.lvEnrollList.TabIndex = 310
        Me.lvEnrollList.UseCompatibleStateImageBehavior = False
        Me.lvEnrollList.View = System.Windows.Forms.View.Details
        '
        'colhTrainingtitle
        '
        Me.colhTrainingtitle.Tag = "colhTrainingtitle"
        Me.colhTrainingtitle.Text = "Title"
        Me.colhTrainingtitle.Width = 218
        '
        'colhEnrollDate
        '
        Me.colhEnrollDate.Tag = "colhEnrollDate"
        Me.colhEnrollDate.Text = "Enroll Date"
        Me.colhEnrollDate.Width = 85
        '
        'colhStDate
        '
        Me.colhStDate.Tag = "colhStDate"
        Me.colhStDate.Text = "Start Date"
        Me.colhStDate.Width = 85
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 85
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 95
        '
        'objfrmDiary_Training
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvEnrollList)
        Me.Controls.Add(Me.elTrininEnrolment)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Training"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elTrininEnrolment As eZee.Common.eZeeLine
    Friend WithEvents lvEnrollList As System.Windows.Forms.ListView
    Friend WithEvents colhTrainingtitle As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEnrollDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
End Class
