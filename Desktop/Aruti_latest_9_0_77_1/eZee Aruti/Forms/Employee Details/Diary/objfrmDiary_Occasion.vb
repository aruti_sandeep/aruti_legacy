﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Occasion

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = 0

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Occasion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            Call Fill_Info()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Occasion_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function's & Procedure "

    Private Sub Fill_Info()
        Try
            Dim dsList As New DataSet : Dim mobjQTran As New clsEmp_Qualification_Tran

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = mobjQTran.GetList("Qualif")
            'Dim dtTable = New DataView(dsList.Tables(0), "EmpId = '" & mintEmployeeId & "'", "", DataViewRowState.CurrentRows).ToTable
            dsList = mobjQTran.GetList(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                       ConfigParameter._Object._IsIncludeInactiveEmp, "Qualif", , "hremployee_master.employeeunkid = '" & mintEmployeeId & "'")
            Dim dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            lvQualification.Items.Clear()
            lvQualification.BeginUpdate()
            'S.SANDEEP [04 JUN 2015] -- END

            For Each dtRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item("QGrp").ToString
                lvItem.SubItems.Add(dtRow.Item("Qualify").ToString)
                lvItem.SubItems.Add(dtRow.Item("RefNo").ToString)
                If dtRow.Item("StDate").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("StDate").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("EnDate").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EnDate").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                'S.SANDEEP [ 18 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(dtRow.Item("QGrpId")) <= 0 Then
                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 1, "Other Qualification / Short Course"))
                Else
                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 2, "Regular Qualification"))
                End If
                'S.SANDEEP [ 18 AUG 2012 ] -- END

                lvQualification.Items.Add(lvItem)
            Next

            lvQualification.GridLines = False
            lvQualification.GroupingColumn = objcolhGrp
            lvQualification.DisplayGroups(True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            lvQualification.EndUpdate()
            'S.SANDEEP [04 JUN 2015] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvQualification_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvQualification.ItemSelectionChanged
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvQualification_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elAchievement.Text = Language._Object.getCaption(Me.elAchievement.Name, Me.elAchievement.Text)
			Me.colhQGrp.Text = Language._Object.getCaption(CStr(Me.colhQGrp.Tag), Me.colhQGrp.Text)
			Me.colhQualification.Text = Language._Object.getCaption(CStr(Me.colhQualification.Tag), Me.colhQualification.Text)
			Me.colhRefNo.Text = Language._Object.getCaption(CStr(Me.colhRefNo.Tag), Me.colhRefNo.Text)
			Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Other Qualification / Short Course")
			Language.setMessage(mstrModuleName, 2, "Regular Qualification")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class