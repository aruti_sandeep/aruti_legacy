﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Discipline
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elDiscipline = New eZee.Common.eZeeLine
        Me.gbIncident = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlData = New System.Windows.Forms.Panel
        Me.rtbIncident = New System.Windows.Forms.RichTextBox
        Me.lblClose = New System.Windows.Forms.Label
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStep = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActionTaken = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppSatatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShow = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIncident = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbIncident.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'elDiscipline
        '
        Me.elDiscipline.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.elDiscipline.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elDiscipline.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elDiscipline.Location = New System.Drawing.Point(2, 6)
        Me.elDiscipline.Name = "elDiscipline"
        Me.elDiscipline.Size = New System.Drawing.Size(575, 17)
        Me.elDiscipline.TabIndex = 310
        Me.elDiscipline.Text = "Discipline"
        Me.elDiscipline.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbIncident
        '
        Me.gbIncident.BorderColor = System.Drawing.Color.Black
        Me.gbIncident.Checked = False
        Me.gbIncident.CollapseAllExceptThis = False
        Me.gbIncident.CollapsedHoverImage = Nothing
        Me.gbIncident.CollapsedNormalImage = Nothing
        Me.gbIncident.CollapsedPressedImage = Nothing
        Me.gbIncident.CollapseOnLoad = False
        Me.gbIncident.Controls.Add(Me.pnlData)
        Me.gbIncident.Controls.Add(Me.lblClose)
        Me.gbIncident.ExpandedHoverImage = Nothing
        Me.gbIncident.ExpandedNormalImage = Nothing
        Me.gbIncident.ExpandedPressedImage = Nothing
        Me.gbIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbIncident.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbIncident.HeaderHeight = 25
        Me.gbIncident.HeaderMessage = ""
        Me.gbIncident.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbIncident.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbIncident.HeightOnCollapse = 0
        Me.gbIncident.LeftTextSpace = 0
        Me.gbIncident.Location = New System.Drawing.Point(141, 60)
        Me.gbIncident.Name = "gbIncident"
        Me.gbIncident.OpenHeight = 300
        Me.gbIncident.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbIncident.ShowBorder = True
        Me.gbIncident.ShowCheckBox = False
        Me.gbIncident.ShowCollapseButton = False
        Me.gbIncident.ShowDefaultBorderColor = True
        Me.gbIncident.ShowDownButton = False
        Me.gbIncident.ShowHeader = True
        Me.gbIncident.Size = New System.Drawing.Size(293, 297)
        Me.gbIncident.TabIndex = 311
        Me.gbIncident.Temp = 0
        Me.gbIncident.Text = "Incident"
        Me.gbIncident.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlData.Controls.Add(Me.rtbIncident)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(2, 26)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(289, 269)
        Me.pnlData.TabIndex = 2
        '
        'rtbIncident
        '
        Me.rtbIncident.BackColor = System.Drawing.Color.White
        Me.rtbIncident.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbIncident.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtbIncident.Location = New System.Drawing.Point(0, 0)
        Me.rtbIncident.Name = "rtbIncident"
        Me.rtbIncident.ReadOnly = True
        Me.rtbIncident.Size = New System.Drawing.Size(287, 267)
        Me.rtbIncident.TabIndex = 2
        Me.rtbIncident.Text = ""
        '
        'lblClose
        '
        Me.lblClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblClose.AutoSize = True
        Me.lblClose.BackColor = System.Drawing.Color.Transparent
        Me.lblClose.Location = New System.Drawing.Point(264, 5)
        Me.lblClose.Name = "lblClose"
        Me.lblClose.Size = New System.Drawing.Size(24, 13)
        Me.lblClose.TabIndex = 1
        Me.lblClose.Text = "[X]"
        Me.lblClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhStep, Me.dgcolhActionTaken, Me.dgcolhAStartDate, Me.dgcolhAEndDate, Me.dgcolhStatus, Me.dgcolhAppSatatus, Me.dgcolhAppDate, Me.dgcolhShow, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhIncident})
        Me.dgvData.Location = New System.Drawing.Point(5, 26)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(572, 380)
        Me.dgvData.TabIndex = 312
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.Width = 25
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Resolution Steps"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 220
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Action Taken"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Start Date"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "End Date"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 80
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Approved Status"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Approval Date"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 80
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "dgcolhGrpId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "dgcolhIncident"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.ReadOnly = True
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhStep
        '
        Me.dgcolhStep.HeaderText = "Resolution Steps"
        Me.dgcolhStep.Name = "dgcolhStep"
        Me.dgcolhStep.ReadOnly = True
        Me.dgcolhStep.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStep.Width = 220
        '
        'dgcolhActionTaken
        '
        Me.dgcolhActionTaken.HeaderText = "Action Taken"
        Me.dgcolhActionTaken.Name = "dgcolhActionTaken"
        Me.dgcolhActionTaken.ReadOnly = True
        Me.dgcolhActionTaken.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAStartDate
        '
        Me.dgcolhAStartDate.HeaderText = "Start Date"
        Me.dgcolhAStartDate.Name = "dgcolhAStartDate"
        Me.dgcolhAStartDate.ReadOnly = True
        Me.dgcolhAStartDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAStartDate.Width = 80
        '
        'dgcolhAEndDate
        '
        Me.dgcolhAEndDate.HeaderText = "End Date"
        Me.dgcolhAEndDate.Name = "dgcolhAEndDate"
        Me.dgcolhAEndDate.ReadOnly = True
        Me.dgcolhAEndDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAEndDate.Width = 80
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Step Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAppSatatus
        '
        Me.dgcolhAppSatatus.HeaderText = "Approved Status"
        Me.dgcolhAppSatatus.Name = "dgcolhAppSatatus"
        Me.dgcolhAppSatatus.ReadOnly = True
        Me.dgcolhAppSatatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAppDate
        '
        Me.dgcolhAppDate.HeaderText = "Approval Date"
        Me.dgcolhAppDate.Name = "dgcolhAppDate"
        Me.dgcolhAppDate.ReadOnly = True
        Me.dgcolhAppDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAppDate.Width = 80
        '
        'dgcolhShow
        '
        Me.dgcolhShow.ActiveLinkColor = System.Drawing.Color.White
        Me.dgcolhShow.HeaderText = "Show"
        Me.dgcolhShow.LinkColor = System.Drawing.Color.White
        Me.dgcolhShow.Name = "dgcolhShow"
        Me.dgcolhShow.ReadOnly = True
        Me.dgcolhShow.VisitedLinkColor = System.Drawing.Color.White
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "dgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "dgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhIncident
        '
        Me.objdgcolhIncident.HeaderText = "dgcolhIncident"
        Me.objdgcolhIncident.Name = "objdgcolhIncident"
        Me.objdgcolhIncident.ReadOnly = True
        Me.objdgcolhIncident.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIncident.Visible = False
        '
        'objfrmDiary_Discipline
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.gbIncident)
        Me.Controls.Add(Me.elDiscipline)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Discipline"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.gbIncident.ResumeLayout(False)
        Me.gbIncident.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elDiscipline As eZee.Common.eZeeLine
    Friend WithEvents gbIncident As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblClose As System.Windows.Forms.Label
    Friend WithEvents rtbIncident As System.Windows.Forms.RichTextBox
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStep As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActionTaken As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppSatatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShow As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIncident As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
