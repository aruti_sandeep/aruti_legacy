﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Depandants

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Depandants_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            lvDepandants_Benefice.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Depandants_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Dim objDependants_Benefice As New clsDependants_Beneficiary_tran
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objDependants_Benefice.GetList("List")

            'If CInt(mintEmployeeId) > 0 Then
            '    StrSearching &= "AND EmpId = " & mintEmployeeId & " "
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables("List"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables("List"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'End If

            dsList = objDependants_Benefice.GetList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    ConfigParameter._Object._IsIncludeInactiveEmp, "List", , mintEmployeeId, , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Sohail (18 May 2019) - [eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)]

            dtTable = dsList.Tables(0)

            'S.SANDEEP [04 JUN 2015] -- END

            lvDepandants_Benefice.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(dtRow.Item("DpndtBefName").ToString)
                lvItem.SubItems.Add(dtRow.Item("Relation").ToString)
                If dtRow.Item("birthdate").ToString.Trim = "" Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("birthdate").ToString).ToShortDateString)
                End If
                lvItem.SubItems.Add(dtRow.Item("IdNo").ToString)
                lvItem.SubItems.Add(dtRow.Item("Gender").ToString)

                lvItem.Tag = dtRow.Item("DpndtTranId")

                lvDepandants_Benefice.Items.Add(lvItem)
                lvItem = Nothing
            Next

            lvDepandants_Benefice.GroupingColumn = colhEmployee
            lvDepandants_Benefice.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elEmployeeDepandents.Text = Language._Object.getCaption(Me.elEmployeeDepandents.Name, Me.elEmployeeDepandents.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDBName.Text = Language._Object.getCaption(CStr(Me.colhDBName.Tag), Me.colhDBName.Text)
			Me.colhRelation.Text = Language._Object.getCaption(CStr(Me.colhRelation.Tag), Me.colhRelation.Text)
			Me.colhCountry.Text = Language._Object.getCaption(CStr(Me.colhCountry.Tag), Me.colhCountry.Text)
			Me.colhIdNo.Text = Language._Object.getCaption(CStr(Me.colhIdNo.Tag), Me.colhIdNo.Text)
			Me.colhGender.Text = Language._Object.getCaption(CStr(Me.colhGender.Tag), Me.colhGender.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class