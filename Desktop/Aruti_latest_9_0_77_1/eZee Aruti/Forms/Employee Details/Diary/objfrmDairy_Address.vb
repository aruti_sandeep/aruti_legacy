﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Address

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mobjEmpMaster As clsEmployee_Master
    Private mobjState As clsstate_master
    Private mobjCity As clscity_master
    Private mobjZipcode As clszipcode_master
    Private mobjMaster As clsMasterData

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mobjEmpMaster = New clsEmployee_Master
        mobjEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
        mobjState = New clsstate_master
        mobjCity = New clscity_master
        mobjZipcode = New clszipcode_master
        mobjMaster = New clsMasterData
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDairy_Address_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            Call Fill_Info()
            Call lnkClose_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDairy_Address_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    

#End Region

#Region " Private Methods & Fucntions "

    Private Sub Fill_Info()
        Try
            Dim dsCountry As New DataSet

            '*********** PRESENT ADDRESS ***********'
            txtAddress1.Text = mobjEmpMaster._Present_Address1
            txtAddress2.Text = mobjEmpMaster._Present_Address2
            txtAlternativeNo.Text = mobjEmpMaster._Present_Alternateno
            'S.SANDEEP [05 SEP 2016] -- START
            'dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Present_Countryunkid)
            'If dsCountry.Tables(0).Rows.Count > 0 Then
            '    txtPresentCountry.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            'End If
            If mobjEmpMaster._Present_Countryunkid > 0 Then
            dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Present_Countryunkid)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                txtPresentCountry.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            End If
            End If
            'S.SANDEEP [05 SEP 2016] -- START
            txtEmail.Text = mobjEmpMaster._Present_Email
            txtEstate.Text = mobjEmpMaster._Present_Estate
            txtFax.Text = mobjEmpMaster._Present_Fax
            txtMobile.Text = mobjEmpMaster._Present_Mobile
            txtPlotNo.Text = mobjEmpMaster._Present_Plotno

            mobjCity._Cityunkid = mobjEmpMaster._Present_Post_Townunkid
            txtPresentCity.Text = mobjCity._Name

            mobjZipcode._Zipcodeunkid = mobjEmpMaster._Present_Postcodeunkid
            txtPresentZipCode.Text = mobjZipcode._Zipcode_No

            txtProvince.Text = mobjEmpMaster._Present_Provicnce
            txtRoad.Text = mobjEmpMaster._Present_Road

            mobjState._Stateunkid = mobjEmpMaster._Present_Stateunkid
            txtPresentState.Text = mobjState._Name

            txtTelNo.Text = mobjEmpMaster._Present_Tel_No
            '*********** PRESENT ADDRESS ***********'

            '*********** DOMICILE ADDRESS ***********'
            txtDomicileAddress1.Text = mobjEmpMaster._Domicile_Address1
            txtDomicileAddress2.Text = mobjEmpMaster._Domicile_Address2
            txtDomicileAltNo.Text = mobjEmpMaster._Domicile_Alternateno


            'S.SANDEEP [05 SEP 2016] -- START
            'dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Present_Countryunkid)
            'dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Domicile_Countryunkid)
            'If dsCountry.Tables(0).Rows.Count > 0 Then
            '    txtDCountry.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            'End If
            If mobjEmpMaster._Domicile_Countryunkid > 0 Then
            dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Domicile_Countryunkid)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                txtDCountry.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            End If
            End If
            'S.SANDEEP [05 SEP 2016] -- START

            

            txtDomicileEmail.Text = mobjEmpMaster._Domicile_Email
            txtDomicileEstate.Text = mobjEmpMaster._Domicile_Estate
            txtDomicileFax.Text = mobjEmpMaster._Domicile_Fax
            txtDomicileMobile.Text = mobjEmpMaster._Domicile_Mobile
            txtDomicilePlotNo.Text = mobjEmpMaster._Domicile_Plotno

            mobjCity._Cityunkid = mobjEmpMaster._Domicile_Post_Townunkid
            txtDCity.Text = mobjCity._Name

            mobjZipcode._Zipcodeunkid = mobjEmpMaster._Domicile_Postcodeunkid
            txtDZipCode.Text = mobjZipcode._Zipcode_No


            txtDomicileProvince.Text = mobjEmpMaster._Domicile_Provicnce
            txtDomicileRoad.Text = mobjEmpMaster._Domicile_Road

            mobjState._Stateunkid = mobjEmpMaster._Domicile_Stateunkid
            txtDState.Text = mobjState._Name

            txtDomicileTelNo.Text = mobjEmpMaster._Domicile_Tel_No
            '*********** DOMICILE ADDRESS ***********'

            '*********** EMERGENCY ADDRESS ***********'
            txtEmgFax.Text = mobjEmpMaster._Emer_Con_Fax
            txtEmgAddress.Text = mobjEmpMaster._Emer_Con_Address
            txtEmgAltNo.Text = mobjEmpMaster._Emer_Con_Alternateno


            'S.SANDEEP [05 SEP 2016] -- START
            'dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Emer_Con_Countryunkid)
            'If dsCountry.Tables(0).Rows.Count > 0 Then
            '    txtEmg1Country.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            'End If
            If mobjEmpMaster._Emer_Con_Countryunkid > 0 Then
            dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Emer_Con_Countryunkid)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                txtEmg1Country.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            End If
            End If
            'S.SANDEEP [05 SEP 2016] -- START

            

            txtEmgEmail.Text = mobjEmpMaster._Emer_Con_Email
            txtEmgEstate.Text = mobjEmpMaster._Emer_Con_Estate
            txtEmgFirstName.Text = mobjEmpMaster._Emer_Con_Firstname
            txtEmgLastName.Text = mobjEmpMaster._Emer_Con_Lastname
            txtEmgMobile.Text = mobjEmpMaster._Emer_Con_Mobile
            txtEmgPlotNo.Text = mobjEmpMaster._Emer_Con_Plotno

            mobjCity._Cityunkid = mobjEmpMaster._Emer_Con_Post_Townunkid
            txtEmg1City.Text = mobjCity._Name

            mobjZipcode._Zipcodeunkid = mobjEmpMaster._Emer_Con_Postcodeunkid
            txtEmg1ZipCode.Text = mobjZipcode._Zipcode_No

            mobjState._Stateunkid = mobjEmpMaster._Emer_Con_State
            txtEmg1State.Text = mobjState._Name

            txtEmgProvince.Text = mobjEmpMaster._Emer_Con_Provicnce
            txtEmgRoad.Text = mobjEmpMaster._Emer_Con_Road
            txtEmgTelNo.Text = mobjEmpMaster._Emer_Con_Tel_No

            txtEmgAddress2.Text = mobjEmpMaster._Emer_Con_Address2
            txtEmgAltNo2.Text = mobjEmpMaster._Emer_Con_Alternateno2
            txtEmgEmail2.Text = mobjEmpMaster._Emer_Con_Email2
            txtEmgEstate2.Text = mobjEmpMaster._Emer_Con_Estate2
            txtEmgFax2.Text = mobjEmpMaster._Emer_Con_Fax2
            txtEmgFirstName2.Text = mobjEmpMaster._Emer_Con_Firstname2
            txtEmgLastName2.Text = mobjEmpMaster._Emer_Con_Lastname2
            txtEmgMobile2.Text = mobjEmpMaster._Emer_Con_Mobile2
            txtEmgPlotNo2.Text = mobjEmpMaster._Emer_Con_Plotno2
            txtEmgProvince2.Text = mobjEmpMaster._Emer_Con_Provicnce2
            txtEmgRoad2.Text = mobjEmpMaster._Emer_Con_Road2
            txtEmgTelNo2.Text = mobjEmpMaster._Emer_Con_Tel_No2


            'S.SANDEEP [05 SEP 2016] -- START
            'dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Emer_Con_Countryunkid2)
            'If dsCountry.Tables(0).Rows.Count > 0 Then
            '    txtEmg2Country.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            'End If
            If mobjEmpMaster._Emer_Con_Countryunkid2 > 0 Then
            dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Emer_Con_Countryunkid2)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                txtEmg2Country.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            End If
            End If
            'S.SANDEEP [05 SEP 2016] -- START
           

            mobjCity._Cityunkid = mobjEmpMaster._Emer_Con_Post_Townunkid2
            txtEmg2City.Text = mobjCity._Name

            mobjZipcode._Zipcodeunkid = mobjEmpMaster._Emer_Con_Postcodeunkid2
            txtEmg2ZipCode.Text = mobjZipcode._Zipcode_No

            mobjState._Stateunkid = mobjEmpMaster._Emer_Con_State2
            txtEmg2State.Text = mobjState._Name

            txtEmgAddress3.Text = mobjEmpMaster._Emer_Con_Address3
            txtEmgAltNo3.Text = mobjEmpMaster._Emer_Con_Alternateno3
            txtEmgEmail3.Text = mobjEmpMaster._Emer_Con_Email3
            txtEmgEstate3.Text = mobjEmpMaster._Emer_Con_Estate3
            txtEmgFax3.Text = mobjEmpMaster._Emer_Con_Fax3
            txtEmgFirstName3.Text = mobjEmpMaster._Emer_Con_Firstname3
            txtEmgLastName3.Text = mobjEmpMaster._Emer_Con_Lastname3
            txtEmgMobile3.Text = mobjEmpMaster._Emer_Con_Mobile3
            txtEmgPlotNo3.Text = mobjEmpMaster._Emer_Con_Plotno3
            txtEmgProvince3.Text = mobjEmpMaster._Emer_Con_Provicnce3
            txtEmgRoad3.Text = mobjEmpMaster._Emer_Con_Road3
            txtEmgTelNo3.Text = mobjEmpMaster._Emer_Con_Tel_No3

            'S.SANDEEP [05 SEP 2016] -- START
            'dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Emer_Con_Countryunkid3)
            'If dsCountry.Tables(0).Rows.Count > 0 Then
            '    txtEmg3Country.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            'End If
            If mobjEmpMaster._Emer_Con_Countryunkid3 > 0 Then
            dsCountry = mobjMaster.getCountryList("List", False, mobjEmpMaster._Emer_Con_Countryunkid3)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                txtEmg3Country.Text = CStr(dsCountry.Tables(0).Rows(0)("country_name"))
            End If
            End If
            'S.SANDEEP [05 SEP 2016] -- START

            mobjCity._Cityunkid = mobjEmpMaster._Emer_Con_Post_Townunkid3
            txtEmg3City.Text = mobjCity._Name

            mobjZipcode._Zipcodeunkid = mobjEmpMaster._Emer_Con_Postcodeunkid3
            txtEmg3ZipCode.Text = mobjZipcode._Zipcode_No

            mobjState._Stateunkid = mobjEmpMaster._Emer_Con_State3
            txtEmg3State.Text = mobjState._Name
            '*********** EMERGENCY ADDRESS ***********'
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub lnkEmergencyContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmergencyContact.Click
        Try
            pnlPersonalAddress.Visible = False
            pnlEmergencyContact.Visible = True
            lnkEmergencyContact.Visible = False
            pnlEmergencyContact.Location = New Point(2, 27)
            pnlEmergencyContact.Size = CType(New Point(575, 390), Drawing.Size)
            pnlEmergencyContact.BringToFront()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEmergencyContact_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClose.Click
        Try
            pnlEmergencyContact.Visible = False
            pnlPersonalAddress.Visible = True
            pnlEmergencyContact.Location = New Point(2, 27)
            pnlPersonalAddress.Size = CType(New Point(575, 390), Drawing.Size)
            pnlPersonalAddress.BringToFront()
            lnkEmergencyContact.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbAddress.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAddress.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.gbAddress.Text = Language._Object.getCaption(Me.gbAddress.Name, Me.gbAddress.Text)
			Me.lblDomicileState.Text = Language._Object.getCaption(Me.lblDomicileState.Name, Me.lblDomicileState.Text)
			Me.lblDomicileAddress2.Text = Language._Object.getCaption(Me.lblDomicileAddress2.Name, Me.lblDomicileAddress2.Text)
			Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)
			Me.lblPresentState.Text = Language._Object.getCaption(Me.lblPresentState.Name, Me.lblPresentState.Text)
			Me.lblPostTown.Text = Language._Object.getCaption(Me.lblPostTown.Name, Me.lblPostTown.Text)
			Me.lblDomicileAltNo.Text = Language._Object.getCaption(Me.lblDomicileAltNo.Name, Me.lblDomicileAltNo.Text)
			Me.lblRoad.Text = Language._Object.getCaption(Me.lblRoad.Name, Me.lblRoad.Text)
			Me.lblDomicilePlotNo.Text = Language._Object.getCaption(Me.lblDomicilePlotNo.Name, Me.lblDomicilePlotNo.Text)
			Me.lblDomicileProvince.Text = Language._Object.getCaption(Me.lblDomicileProvince.Name, Me.lblDomicileProvince.Text)
			Me.lblDomicilePostCode.Text = Language._Object.getCaption(Me.lblDomicilePostCode.Name, Me.lblDomicilePostCode.Text)
			Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
			Me.lblEstate.Text = Language._Object.getCaption(Me.lblEstate.Name, Me.lblEstate.Text)
			Me.lblProvince.Text = Language._Object.getCaption(Me.lblProvince.Name, Me.lblProvince.Text)
			Me.lblDomicileFax.Text = Language._Object.getCaption(Me.lblDomicileFax.Name, Me.lblDomicileFax.Text)
			Me.lblDomicileMobileNo.Text = Language._Object.getCaption(Me.lblDomicileMobileNo.Name, Me.lblDomicileMobileNo.Text)
			Me.lblDomicileTelNo.Text = Language._Object.getCaption(Me.lblDomicileTelNo.Name, Me.lblDomicileTelNo.Text)
			Me.lblDomicileRoad.Text = Language._Object.getCaption(Me.lblDomicileRoad.Name, Me.lblDomicileRoad.Text)
			Me.lblDomicileEState.Text = Language._Object.getCaption(Me.lblDomicileEState.Name, Me.lblDomicileEState.Text)
			Me.lblDomicileEmail.Text = Language._Object.getCaption(Me.lblDomicileEmail.Name, Me.lblDomicileEmail.Text)
			Me.lblDomicilePostTown.Text = Language._Object.getCaption(Me.lblDomicilePostTown.Name, Me.lblDomicilePostTown.Text)
			Me.lblDomicilePostCountry.Text = Language._Object.getCaption(Me.lblDomicilePostCountry.Name, Me.lblDomicilePostCountry.Text)
			Me.lblDomicileAddress1.Text = Language._Object.getCaption(Me.lblDomicileAddress1.Name, Me.lblDomicileAddress1.Text)
			Me.lnDomicileAddress.Text = Language._Object.getCaption(Me.lnDomicileAddress.Name, Me.lnDomicileAddress.Text)
			Me.lblAlternativeNo.Text = Language._Object.getCaption(Me.lblAlternativeNo.Name, Me.lblAlternativeNo.Text)
			Me.lblPloteNo.Text = Language._Object.getCaption(Me.lblPloteNo.Name, Me.lblPloteNo.Text)
			Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblTelNo.Text = Language._Object.getCaption(Me.lblTelNo.Name, Me.lblTelNo.Text)
			Me.lblPostCountry.Text = Language._Object.getCaption(Me.lblPostCountry.Name, Me.lblPostCountry.Text)
			Me.lblPostcode.Text = Language._Object.getCaption(Me.lblPostcode.Name, Me.lblPostcode.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lnPresentAddress.Text = Language._Object.getCaption(Me.lnPresentAddress.Name, Me.lnPresentAddress.Text)
			Me.lnkEmergencyContact.Text = Language._Object.getCaption(Me.lnkEmergencyContact.Name, Me.lnkEmergencyContact.Text)
			Me.lblEmgEmail3.Text = Language._Object.getCaption(Me.lblEmgEmail3.Name, Me.lblEmgEmail3.Text)
			Me.lblEmgFax3.Text = Language._Object.getCaption(Me.lblEmgFax3.Name, Me.lblEmgFax3.Text)
			Me.lblEmgTelNo3.Text = Language._Object.getCaption(Me.lblEmgTelNo3.Name, Me.lblEmgTelNo3.Text)
			Me.lblEmgAltNo3.Text = Language._Object.getCaption(Me.lblEmgAltNo3.Name, Me.lblEmgAltNo3.Text)
			Me.lblEmgFirstName3.Text = Language._Object.getCaption(Me.lblEmgFirstName3.Name, Me.lblEmgFirstName3.Text)
			Me.lblEmgMobile3.Text = Language._Object.getCaption(Me.lblEmgMobile3.Name, Me.lblEmgMobile3.Text)
			Me.lblEmgLastName3.Text = Language._Object.getCaption(Me.lblEmgLastName3.Name, Me.lblEmgLastName3.Text)
			Me.lblEmgPlotNo3.Text = Language._Object.getCaption(Me.lblEmgPlotNo3.Name, Me.lblEmgPlotNo3.Text)
			Me.lblEmgProvince3.Text = Language._Object.getCaption(Me.lblEmgProvince3.Name, Me.lblEmgProvince3.Text)
			Me.lblEmgAddress3.Text = Language._Object.getCaption(Me.lblEmgAddress3.Name, Me.lblEmgAddress3.Text)
			Me.lblEmgPostCountry3.Text = Language._Object.getCaption(Me.lblEmgPostCountry3.Name, Me.lblEmgPostCountry3.Text)
			Me.lblEmgEstate3.Text = Language._Object.getCaption(Me.lblEmgEstate3.Name, Me.lblEmgEstate3.Text)
			Me.lblEmgPostTown3.Text = Language._Object.getCaption(Me.lblEmgPostTown3.Name, Me.lblEmgPostTown3.Text)
			Me.lblEmgState3.Text = Language._Object.getCaption(Me.lblEmgState3.Name, Me.lblEmgState3.Text)
			Me.lblEmgPostCode3.Text = Language._Object.getCaption(Me.lblEmgPostCode3.Name, Me.lblEmgPostCode3.Text)
			Me.lblEmgRoad3.Text = Language._Object.getCaption(Me.lblEmgRoad3.Name, Me.lblEmgRoad3.Text)
			Me.lnEmergencyContact3.Text = Language._Object.getCaption(Me.lnEmergencyContact3.Name, Me.lnEmergencyContact3.Text)
			Me.lblEmgEmail2.Text = Language._Object.getCaption(Me.lblEmgEmail2.Name, Me.lblEmgEmail2.Text)
			Me.lblEmgFax2.Text = Language._Object.getCaption(Me.lblEmgFax2.Name, Me.lblEmgFax2.Text)
			Me.lblEmgTelNo2.Text = Language._Object.getCaption(Me.lblEmgTelNo2.Name, Me.lblEmgTelNo2.Text)
			Me.lblEmgAltNo2.Text = Language._Object.getCaption(Me.lblEmgAltNo2.Name, Me.lblEmgAltNo2.Text)
			Me.lblEmgFirstName2.Text = Language._Object.getCaption(Me.lblEmgFirstName2.Name, Me.lblEmgFirstName2.Text)
			Me.lblEmgMobile2.Text = Language._Object.getCaption(Me.lblEmgMobile2.Name, Me.lblEmgMobile2.Text)
			Me.lblEmgLastName2.Text = Language._Object.getCaption(Me.lblEmgLastName2.Name, Me.lblEmgLastName2.Text)
			Me.lblEmgPlotNo2.Text = Language._Object.getCaption(Me.lblEmgPlotNo2.Name, Me.lblEmgPlotNo2.Text)
			Me.lblEmgProvince2.Text = Language._Object.getCaption(Me.lblEmgProvince2.Name, Me.lblEmgProvince2.Text)
			Me.lblEmgAddress2.Text = Language._Object.getCaption(Me.lblEmgAddress2.Name, Me.lblEmgAddress2.Text)
			Me.lblEmgPostCountry2.Text = Language._Object.getCaption(Me.lblEmgPostCountry2.Name, Me.lblEmgPostCountry2.Text)
			Me.lblEmgEstate2.Text = Language._Object.getCaption(Me.lblEmgEstate2.Name, Me.lblEmgEstate2.Text)
			Me.lblEmgPostTown2.Text = Language._Object.getCaption(Me.lblEmgPostTown2.Name, Me.lblEmgPostTown2.Text)
			Me.lblEmgState2.Text = Language._Object.getCaption(Me.lblEmgState2.Name, Me.lblEmgState2.Text)
			Me.lblEmgPostCode2.Text = Language._Object.getCaption(Me.lblEmgPostCode2.Name, Me.lblEmgPostCode2.Text)
			Me.lblEmgRoad2.Text = Language._Object.getCaption(Me.lblEmgRoad2.Name, Me.lblEmgRoad2.Text)
			Me.lnEmergencyContact2.Text = Language._Object.getCaption(Me.lnEmergencyContact2.Name, Me.lnEmergencyContact2.Text)
			Me.lnEmergencyContact1.Text = Language._Object.getCaption(Me.lnEmergencyContact1.Name, Me.lnEmergencyContact1.Text)
			Me.lblEmgEmail.Text = Language._Object.getCaption(Me.lblEmgEmail.Name, Me.lblEmgEmail.Text)
			Me.lblEmgFax.Text = Language._Object.getCaption(Me.lblEmgFax.Name, Me.lblEmgFax.Text)
			Me.lblEmgTelNo.Text = Language._Object.getCaption(Me.lblEmgTelNo.Name, Me.lblEmgTelNo.Text)
			Me.lblEmgAltNo.Text = Language._Object.getCaption(Me.lblEmgAltNo.Name, Me.lblEmgAltNo.Text)
			Me.lblEmgFirstName.Text = Language._Object.getCaption(Me.lblEmgFirstName.Name, Me.lblEmgFirstName.Text)
			Me.lblEmgMobile.Text = Language._Object.getCaption(Me.lblEmgMobile.Name, Me.lblEmgMobile.Text)
			Me.lblEmgLastName.Text = Language._Object.getCaption(Me.lblEmgLastName.Name, Me.lblEmgLastName.Text)
			Me.lblEmgPlotNo.Text = Language._Object.getCaption(Me.lblEmgPlotNo.Name, Me.lblEmgPlotNo.Text)
			Me.lblEmgProvince.Text = Language._Object.getCaption(Me.lblEmgProvince.Name, Me.lblEmgProvince.Text)
			Me.lblEmgAddress.Text = Language._Object.getCaption(Me.lblEmgAddress.Name, Me.lblEmgAddress.Text)
			Me.lblEmgPostCountry.Text = Language._Object.getCaption(Me.lblEmgPostCountry.Name, Me.lblEmgPostCountry.Text)
			Me.lblEmgEstate.Text = Language._Object.getCaption(Me.lblEmgEstate.Name, Me.lblEmgEstate.Text)
			Me.lblEmgPostTown.Text = Language._Object.getCaption(Me.lblEmgPostTown.Name, Me.lblEmgPostTown.Text)
			Me.lblEmgState.Text = Language._Object.getCaption(Me.lblEmgState.Name, Me.lblEmgState.Text)
			Me.lblEmgPostCode.Text = Language._Object.getCaption(Me.lblEmgPostCode.Name, Me.lblEmgPostCode.Text)
			Me.lblEmgRoad.Text = Language._Object.getCaption(Me.lblEmgRoad.Name, Me.lblEmgRoad.Text)
			Me.lnkClose.Text = Language._Object.getCaption(Me.lnkClose.Name, Me.lnkClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class