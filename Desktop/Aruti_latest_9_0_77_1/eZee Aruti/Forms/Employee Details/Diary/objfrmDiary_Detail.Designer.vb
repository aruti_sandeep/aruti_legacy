﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Detail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.elPersonalInfo = New eZee.Common.eZeeLine
        Me.imgImageControl = New eZee.Common.eZeeImageControl
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.txttitle = New eZee.TextBox.AlphanumericTextBox
        Me.lblTitle = New System.Windows.Forms.Label
        Me.txtFirstname = New eZee.TextBox.AlphanumericTextBox
        Me.txtSurname = New eZee.TextBox.AlphanumericTextBox
        Me.txtMobile = New eZee.TextBox.AlphanumericTextBox
        Me.lblMobile = New System.Windows.Forms.Label
        Me.txtTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblTelNo = New System.Windows.Forms.Label
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.lblSurname = New System.Windows.Forms.Label
        Me.txtAppointedDate = New eZee.TextBox.AlphanumericTextBox
        Me.lblAppointeddate = New System.Windows.Forms.Label
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmail = New System.Windows.Forms.Label
        Me.elOfficialInfo = New eZee.Common.eZeeLine
        Me.txtSection = New eZee.TextBox.AlphanumericTextBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.txtDeptGroup = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.txtBranch = New eZee.TextBox.AlphanumericTextBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.txtUnit = New eZee.TextBox.AlphanumericTextBox
        Me.txtJobGroup = New eZee.TextBox.AlphanumericTextBox
        Me.lblUnit = New System.Windows.Forms.Label
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.txtJob = New eZee.TextBox.AlphanumericTextBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.txtGradeGroup = New eZee.TextBox.AlphanumericTextBox
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.txtGrade = New eZee.TextBox.AlphanumericTextBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.txtGradeLevel = New eZee.TextBox.AlphanumericTextBox
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.txtShift = New eZee.TextBox.AlphanumericTextBox
        Me.lblShift = New System.Windows.Forms.Label
        Me.txtEmployeeType = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeeType = New System.Windows.Forms.Label
        Me.txtClass = New eZee.TextBox.AlphanumericTextBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.txtClassGroup = New eZee.TextBox.AlphanumericTextBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.txtScale = New eZee.TextBox.AlphanumericTextBox
        Me.lblScale = New System.Windows.Forms.Label
        Me.lnkShowSignature = New System.Windows.Forms.LinkLabel
        Me.lnkShowImage = New System.Windows.Forms.LinkLabel
        Me.SuspendLayout()
        '
        'elPersonalInfo
        '
        Me.elPersonalInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elPersonalInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elPersonalInfo.Location = New System.Drawing.Point(8, 6)
        Me.elPersonalInfo.Name = "elPersonalInfo"
        Me.elPersonalInfo.Size = New System.Drawing.Size(561, 19)
        Me.elPersonalInfo.TabIndex = 306
        Me.elPersonalInfo.Text = "Personal Information"
        Me.elPersonalInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'imgImageControl
        '
        Me.imgImageControl._FileMode = True
        Me.imgImageControl._Image = Nothing
        Me.imgImageControl._ShowAddButton = True
        Me.imgImageControl._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.imgImageControl._ShowDeleteButton = True
        Me.imgImageControl._ShowPreview = False
        Me.imgImageControl.BackColor = System.Drawing.Color.Transparent
        Me.imgImageControl.Enabled = False
        Me.imgImageControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgImageControl.Location = New System.Drawing.Point(425, 33)
        Me.imgImageControl.Name = "imgImageControl"
        Me.imgImageControl.Size = New System.Drawing.Size(152, 129)
        Me.imgImageControl.TabIndex = 375
        '
        'txtCode
        '
        Me.txtCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtCode.Flags = 0
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(112, 33)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.ReadOnly = True
        Me.txtCode.Size = New System.Drawing.Size(111, 21)
        Me.txtCode.TabIndex = 377
        '
        'lblCode
        '
        Me.lblCode.Location = New System.Drawing.Point(16, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(92, 15)
        Me.lblCode.TabIndex = 376
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txttitle
        '
        Me.txttitle.BackColor = System.Drawing.Color.White
        Me.txttitle.Flags = 0
        Me.txttitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txttitle.Location = New System.Drawing.Point(308, 33)
        Me.txttitle.Name = "txttitle"
        Me.txttitle.ReadOnly = True
        Me.txttitle.Size = New System.Drawing.Size(111, 21)
        Me.txttitle.TabIndex = 379
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(229, 36)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(73, 15)
        Me.lblTitle.TabIndex = 378
        Me.lblTitle.Text = "Title"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFirstname
        '
        Me.txtFirstname.BackColor = System.Drawing.Color.White
        Me.txtFirstname.Flags = 0
        Me.txtFirstname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFirstname.Location = New System.Drawing.Point(112, 60)
        Me.txtFirstname.Name = "txtFirstname"
        Me.txtFirstname.ReadOnly = True
        Me.txtFirstname.Size = New System.Drawing.Size(307, 21)
        Me.txtFirstname.TabIndex = 380
        '
        'txtSurname
        '
        Me.txtSurname.BackColor = System.Drawing.Color.White
        Me.txtSurname.Flags = 0
        Me.txtSurname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSurname.Location = New System.Drawing.Point(112, 87)
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.ReadOnly = True
        Me.txtSurname.Size = New System.Drawing.Size(307, 21)
        Me.txtSurname.TabIndex = 381
        '
        'txtMobile
        '
        Me.txtMobile.BackColor = System.Drawing.Color.White
        Me.txtMobile.Flags = 0
        Me.txtMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMobile.Location = New System.Drawing.Point(112, 114)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.ReadOnly = True
        Me.txtMobile.Size = New System.Drawing.Size(111, 21)
        Me.txtMobile.TabIndex = 383
        '
        'lblMobile
        '
        Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMobile.Location = New System.Drawing.Point(16, 117)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(92, 15)
        Me.lblMobile.TabIndex = 382
        Me.lblMobile.Text = "Mobile"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTelNo
        '
        Me.txtTelNo.BackColor = System.Drawing.Color.White
        Me.txtTelNo.Flags = 0
        Me.txtTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTelNo.Location = New System.Drawing.Point(308, 114)
        Me.txtTelNo.Name = "txtTelNo"
        Me.txtTelNo.ReadOnly = True
        Me.txtTelNo.Size = New System.Drawing.Size(111, 21)
        Me.txtTelNo.TabIndex = 385
        '
        'lblTelNo
        '
        Me.lblTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelNo.Location = New System.Drawing.Point(229, 117)
        Me.lblTelNo.Name = "lblTelNo"
        Me.lblTelNo.Size = New System.Drawing.Size(76, 15)
        Me.lblTelNo.TabIndex = 384
        Me.lblTelNo.Text = "Tel. No"
        Me.lblTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFirstName
        '
        Me.lblFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(16, 63)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(92, 15)
        Me.lblFirstName.TabIndex = 387
        Me.lblFirstName.Text = "Firstname"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSurname
        '
        Me.lblSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurname.Location = New System.Drawing.Point(16, 90)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(92, 15)
        Me.lblSurname.TabIndex = 386
        Me.lblSurname.Text = "Surname"
        Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAppointedDate
        '
        Me.txtAppointedDate.BackColor = System.Drawing.Color.White
        Me.txtAppointedDate.Flags = 0
        Me.txtAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppointedDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAppointedDate.Location = New System.Drawing.Point(112, 141)
        Me.txtAppointedDate.Name = "txtAppointedDate"
        Me.txtAppointedDate.ReadOnly = True
        Me.txtAppointedDate.Size = New System.Drawing.Size(111, 21)
        Me.txtAppointedDate.TabIndex = 391
        '
        'lblAppointeddate
        '
        Me.lblAppointeddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointeddate.Location = New System.Drawing.Point(16, 147)
        Me.lblAppointeddate.Name = "lblAppointeddate"
        Me.lblAppointeddate.Size = New System.Drawing.Size(92, 15)
        Me.lblAppointeddate.TabIndex = 390
        Me.lblAppointeddate.Text = "Appointed Date"
        Me.lblAppointeddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.White
        Me.txtEmail.Flags = 0
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(308, 141)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.ReadOnly = True
        Me.txtEmail.Size = New System.Drawing.Size(111, 21)
        Me.txtEmail.TabIndex = 389
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(229, 144)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(76, 15)
        Me.lblEmail.TabIndex = 388
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elOfficialInfo
        '
        Me.elOfficialInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elOfficialInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elOfficialInfo.Location = New System.Drawing.Point(5, 182)
        Me.elOfficialInfo.Name = "elOfficialInfo"
        Me.elOfficialInfo.Size = New System.Drawing.Size(565, 23)
        Me.elOfficialInfo.TabIndex = 392
        Me.elOfficialInfo.Text = "Official Information"
        Me.elOfficialInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSection
        '
        Me.txtSection.BackColor = System.Drawing.Color.White
        Me.txtSection.Flags = 0
        Me.txtSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSection.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSection.Location = New System.Drawing.Point(388, 241)
        Me.txtSection.Name = "txtSection"
        Me.txtSection.ReadOnly = True
        Me.txtSection.Size = New System.Drawing.Size(182, 21)
        Me.txtSection.TabIndex = 400
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(300, 244)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(81, 15)
        Me.lblSection.TabIndex = 399
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.Color.White
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(112, 241)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(182, 21)
        Me.txtDepartment.TabIndex = 398
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(16, 244)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(91, 15)
        Me.lblDepartment.TabIndex = 397
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDeptGroup
        '
        Me.txtDeptGroup.BackColor = System.Drawing.Color.White
        Me.txtDeptGroup.Flags = 0
        Me.txtDeptGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDeptGroup.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDeptGroup.Location = New System.Drawing.Point(388, 214)
        Me.txtDeptGroup.Name = "txtDeptGroup"
        Me.txtDeptGroup.ReadOnly = True
        Me.txtDeptGroup.Size = New System.Drawing.Size(182, 21)
        Me.txtDeptGroup.TabIndex = 396
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(300, 217)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(81, 15)
        Me.lblDepartmentGroup.TabIndex = 395
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBranch
        '
        Me.txtBranch.BackColor = System.Drawing.Color.White
        Me.txtBranch.Flags = 0
        Me.txtBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBranch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBranch.Location = New System.Drawing.Point(112, 214)
        Me.txtBranch.Name = "txtBranch"
        Me.txtBranch.ReadOnly = True
        Me.txtBranch.Size = New System.Drawing.Size(182, 21)
        Me.txtBranch.TabIndex = 394
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(16, 217)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(91, 15)
        Me.lblBranch.TabIndex = 393
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUnit
        '
        Me.txtUnit.BackColor = System.Drawing.Color.White
        Me.txtUnit.Flags = 0
        Me.txtUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnit.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtUnit.Location = New System.Drawing.Point(112, 268)
        Me.txtUnit.Name = "txtUnit"
        Me.txtUnit.ReadOnly = True
        Me.txtUnit.Size = New System.Drawing.Size(182, 21)
        Me.txtUnit.TabIndex = 401
        '
        'txtJobGroup
        '
        Me.txtJobGroup.BackColor = System.Drawing.Color.White
        Me.txtJobGroup.Flags = 0
        Me.txtJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobGroup.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobGroup.Location = New System.Drawing.Point(388, 268)
        Me.txtJobGroup.Name = "txtJobGroup"
        Me.txtJobGroup.ReadOnly = True
        Me.txtJobGroup.Size = New System.Drawing.Size(182, 21)
        Me.txtJobGroup.TabIndex = 402
        '
        'lblUnit
        '
        Me.lblUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnit.Location = New System.Drawing.Point(16, 271)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(91, 15)
        Me.lblUnit.TabIndex = 403
        Me.lblUnit.Text = "Unit"
        Me.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(300, 271)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(81, 15)
        Me.lblJobGroup.TabIndex = 404
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJob
        '
        Me.txtJob.BackColor = System.Drawing.Color.White
        Me.txtJob.Flags = 0
        Me.txtJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJob.Location = New System.Drawing.Point(112, 295)
        Me.txtJob.Name = "txtJob"
        Me.txtJob.ReadOnly = True
        Me.txtJob.Size = New System.Drawing.Size(182, 21)
        Me.txtJob.TabIndex = 406
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(16, 298)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(91, 15)
        Me.lblJob.TabIndex = 405
        Me.lblJob.Text = "Job "
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGradeGroup
        '
        Me.txtGradeGroup.BackColor = System.Drawing.Color.White
        Me.txtGradeGroup.Flags = 0
        Me.txtGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGradeGroup.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGradeGroup.Location = New System.Drawing.Point(388, 295)
        Me.txtGradeGroup.Name = "txtGradeGroup"
        Me.txtGradeGroup.ReadOnly = True
        Me.txtGradeGroup.Size = New System.Drawing.Size(182, 21)
        Me.txtGradeGroup.TabIndex = 408
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(300, 298)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(81, 15)
        Me.lblGradeGroup.TabIndex = 407
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGrade
        '
        Me.txtGrade.BackColor = System.Drawing.Color.White
        Me.txtGrade.Flags = 0
        Me.txtGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrade.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGrade.Location = New System.Drawing.Point(388, 322)
        Me.txtGrade.Name = "txtGrade"
        Me.txtGrade.ReadOnly = True
        Me.txtGrade.Size = New System.Drawing.Size(182, 21)
        Me.txtGrade.TabIndex = 410
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(300, 325)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(81, 15)
        Me.lblGrade.TabIndex = 409
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGradeLevel
        '
        Me.txtGradeLevel.BackColor = System.Drawing.Color.White
        Me.txtGradeLevel.Flags = 0
        Me.txtGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGradeLevel.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGradeLevel.Location = New System.Drawing.Point(388, 349)
        Me.txtGradeLevel.Name = "txtGradeLevel"
        Me.txtGradeLevel.ReadOnly = True
        Me.txtGradeLevel.Size = New System.Drawing.Size(182, 21)
        Me.txtGradeLevel.TabIndex = 412
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(300, 352)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(81, 15)
        Me.lblGradeLevel.TabIndex = 411
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShift
        '
        Me.txtShift.BackColor = System.Drawing.Color.White
        Me.txtShift.Flags = 0
        Me.txtShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShift.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtShift.Location = New System.Drawing.Point(388, 376)
        Me.txtShift.Name = "txtShift"
        Me.txtShift.ReadOnly = True
        Me.txtShift.Size = New System.Drawing.Size(182, 21)
        Me.txtShift.TabIndex = 414
        Me.txtShift.Visible = False
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(298, 379)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(84, 15)
        Me.lblShift.TabIndex = 413
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblShift.Visible = False
        '
        'txtEmployeeType
        '
        Me.txtEmployeeType.BackColor = System.Drawing.Color.White
        Me.txtEmployeeType.Flags = 0
        Me.txtEmployeeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeType.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeType.Location = New System.Drawing.Point(112, 322)
        Me.txtEmployeeType.Name = "txtEmployeeType"
        Me.txtEmployeeType.ReadOnly = True
        Me.txtEmployeeType.Size = New System.Drawing.Size(182, 21)
        Me.txtEmployeeType.TabIndex = 415
        '
        'lblEmployeeType
        '
        Me.lblEmployeeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeType.Location = New System.Drawing.Point(16, 324)
        Me.lblEmployeeType.Name = "lblEmployeeType"
        Me.lblEmployeeType.Size = New System.Drawing.Size(91, 15)
        Me.lblEmployeeType.TabIndex = 416
        Me.lblEmployeeType.Text = "Employee Type"
        Me.lblEmployeeType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClass
        '
        Me.txtClass.BackColor = System.Drawing.Color.White
        Me.txtClass.Flags = 0
        Me.txtClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClass.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClass.Location = New System.Drawing.Point(112, 376)
        Me.txtClass.Name = "txtClass"
        Me.txtClass.ReadOnly = True
        Me.txtClass.Size = New System.Drawing.Size(182, 21)
        Me.txtClass.TabIndex = 420
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(16, 379)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(91, 15)
        Me.lblClass.TabIndex = 419
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClassGroup
        '
        Me.txtClassGroup.BackColor = System.Drawing.Color.White
        Me.txtClassGroup.Flags = 0
        Me.txtClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClassGroup.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClassGroup.Location = New System.Drawing.Point(112, 349)
        Me.txtClassGroup.Name = "txtClassGroup"
        Me.txtClassGroup.ReadOnly = True
        Me.txtClassGroup.Size = New System.Drawing.Size(182, 21)
        Me.txtClassGroup.TabIndex = 418
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(16, 352)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(91, 15)
        Me.lblClassGroup.TabIndex = 417
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtScale
        '
        Me.txtScale.BackColor = System.Drawing.Color.White
        Me.txtScale.Flags = 0
        Me.txtScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScale.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtScale.Location = New System.Drawing.Point(388, 376)
        Me.txtScale.Name = "txtScale"
        Me.txtScale.ReadOnly = True
        Me.txtScale.Size = New System.Drawing.Size(182, 21)
        Me.txtScale.TabIndex = 422
        Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblScale
        '
        Me.lblScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScale.Location = New System.Drawing.Point(300, 379)
        Me.lblScale.Name = "lblScale"
        Me.lblScale.Size = New System.Drawing.Size(81, 15)
        Me.lblScale.TabIndex = 421
        Me.lblScale.Text = "Scale"
        Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkShowSignature
        '
        Me.lnkShowSignature.ActiveLinkColor = System.Drawing.SystemColors.Highlight
        Me.lnkShowSignature.BackColor = System.Drawing.Color.Transparent
        Me.lnkShowSignature.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShowSignature.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkShowSignature.LinkColor = System.Drawing.SystemColors.Highlight
        Me.lnkShowSignature.Location = New System.Drawing.Point(425, 167)
        Me.lnkShowSignature.Name = "lnkShowSignature"
        Me.lnkShowSignature.Size = New System.Drawing.Size(124, 14)
        Me.lnkShowSignature.TabIndex = 423
        Me.lnkShowSignature.TabStop = True
        Me.lnkShowSignature.Text = "Show Signature"
        Me.lnkShowSignature.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lnkShowSignature.VisitedLinkColor = System.Drawing.SystemColors.Highlight
        '
        'lnkShowImage
        '
        Me.lnkShowImage.ActiveLinkColor = System.Drawing.SystemColors.Highlight
        Me.lnkShowImage.BackColor = System.Drawing.Color.Transparent
        Me.lnkShowImage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShowImage.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkShowImage.LinkColor = System.Drawing.SystemColors.Highlight
        Me.lnkShowImage.Location = New System.Drawing.Point(425, 167)
        Me.lnkShowImage.Name = "lnkShowImage"
        Me.lnkShowImage.Size = New System.Drawing.Size(124, 14)
        Me.lnkShowImage.TabIndex = 424
        Me.lnkShowImage.TabStop = True
        Me.lnkShowImage.Text = "Show Image"
        Me.lnkShowImage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lnkShowImage.VisitedLinkColor = System.Drawing.SystemColors.Highlight
        '
        'objfrmDiary_Detail
        '
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lnkShowImage)
        Me.Controls.Add(Me.lnkShowSignature)
        Me.Controls.Add(Me.txtScale)
        Me.Controls.Add(Me.lblScale)
        Me.Controls.Add(Me.txtClass)
        Me.Controls.Add(Me.lblClass)
        Me.Controls.Add(Me.txtClassGroup)
        Me.Controls.Add(Me.lblClassGroup)
        Me.Controls.Add(Me.lblEmployeeType)
        Me.Controls.Add(Me.txtEmployeeType)
        Me.Controls.Add(Me.txtGradeLevel)
        Me.Controls.Add(Me.lblGradeLevel)
        Me.Controls.Add(Me.txtGrade)
        Me.Controls.Add(Me.lblGrade)
        Me.Controls.Add(Me.txtGradeGroup)
        Me.Controls.Add(Me.lblGradeGroup)
        Me.Controls.Add(Me.txtJob)
        Me.Controls.Add(Me.lblJob)
        Me.Controls.Add(Me.lblJobGroup)
        Me.Controls.Add(Me.lblUnit)
        Me.Controls.Add(Me.txtJobGroup)
        Me.Controls.Add(Me.txtUnit)
        Me.Controls.Add(Me.txtSection)
        Me.Controls.Add(Me.lblSection)
        Me.Controls.Add(Me.txtDepartment)
        Me.Controls.Add(Me.lblDepartment)
        Me.Controls.Add(Me.txtDeptGroup)
        Me.Controls.Add(Me.lblDepartmentGroup)
        Me.Controls.Add(Me.txtBranch)
        Me.Controls.Add(Me.lblBranch)
        Me.Controls.Add(Me.elOfficialInfo)
        Me.Controls.Add(Me.txtAppointedDate)
        Me.Controls.Add(Me.lblAppointeddate)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.lblFirstName)
        Me.Controls.Add(Me.lblSurname)
        Me.Controls.Add(Me.txtTelNo)
        Me.Controls.Add(Me.lblTelNo)
        Me.Controls.Add(Me.txtMobile)
        Me.Controls.Add(Me.lblMobile)
        Me.Controls.Add(Me.txtSurname)
        Me.Controls.Add(Me.txtFirstname)
        Me.Controls.Add(Me.txttitle)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.txtCode)
        Me.Controls.Add(Me.lblCode)
        Me.Controls.Add(Me.imgImageControl)
        Me.Controls.Add(Me.elPersonalInfo)
        Me.Controls.Add(Me.txtShift)
        Me.Controls.Add(Me.lblShift)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Detail"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents elPersonalInfo As eZee.Common.eZeeLine
    Friend WithEvents imgImageControl As eZee.Common.eZeeImageControl
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txttitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents txtFirstname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSurname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents txtTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblTelNo As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents lblSurname As System.Windows.Forms.Label
    Friend WithEvents txtAppointedDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAppointeddate As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents elOfficialInfo As eZee.Common.eZeeLine
    Friend WithEvents txtSection As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents txtDeptGroup As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents txtBranch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents txtUnit As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJobGroup As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents txtJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents txtGradeGroup As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents txtGrade As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents txtGradeLevel As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents txtShift As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeType As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeType As System.Windows.Forms.Label
    Friend WithEvents txtClass As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents txtClassGroup As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents txtScale As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblScale As System.Windows.Forms.Label
    Friend WithEvents lnkShowSignature As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkShowImage As System.Windows.Forms.LinkLabel
End Class
