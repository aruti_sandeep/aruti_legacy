﻿Option Strict On
#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.IO
Imports System.Text.RegularExpressions

#End Region

Public Class frmOrbitRequest

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOrbitRequest"
    Private objRequest As clsOrbit_Request_Tran
    Private mdtEmployee As DataTable
    Private mvwEmployee As DataView
    Private mstrAdvanceFilter As String = ""
    Private mblnCancelWorker As Boolean = False
    Private dtExiststCust As DataTable

    Private mintEffectivePeriodId As Integer = 0
    Private mintBankGroupId As Integer
    Private mintBranchId As Integer = 0
    Private mintAccountTypeId As Integer = 0
    Private mintDistributionModeId As Integer = 0
    Private mdecDistributionValue As Decimal = 0
    Private mblnIsErrorOnRequest As Boolean = False 'S.SANDEEP |30-MAY-2020| -- START -- END
    Private iErList As New List(Of String) 'S.SANDEEP |30-MAY-2020| -- START -- END

#End Region

#Region " Form's Events "

    Private Sub frmOrbitRequest_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objRequest = Nothing
    End Sub

    Private Sub frmOrbitRequest_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsOrbit_Request_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsOrbit_Request_Tran"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOrbitRequest_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmOrbitRequest_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objRequest = New clsOrbit_Request_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOrbitRequest_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub CreateDatatable()
        Try
            Dim oCol As DataColumn = Nothing
            oCol = New DataColumn
            With oCol
                .ColumnName = "firstname"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            dtExiststCust.Columns.Add(oCol)

            With oCol
                .ColumnName = "lastname"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            dtExiststCust.Columns.Add(oCol)

            With oCol
                .ColumnName = "custnumber"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            dtExiststCust.Columns.Add(oCol)

            With oCol
                .ColumnName = "employeecode"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            dtExiststCust.Columns.Add(oCol)

            With oCol
                .ColumnName = "employeename"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            dtExiststCust.Columns.Add(oCol)

            With oCol
                .ColumnName = "isGrp"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dtExiststCust.Columns.Add(oCol)

            With oCol
                .ColumnName = "grpid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            dtExiststCust.Columns.Add(oCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDatatable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            Dim iCboItems() As ComboBoxValue = {New ComboBoxValue(Language.getMessage(mstrModuleName, 1, "Select"), 0), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 2, "New Request"), clsOrbit_Request_Tran.enViewType.VW_NEW), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 3, "Failed"), clsOrbit_Request_Tran.enViewType.VW_FAILED), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 4, "Success"), clsOrbit_Request_Tran.enViewType.VW_SUCCESS)}
            With cboRequestMode
                .Items.Clear()
                .Items.AddRange(iCboItems)
                .SelectedIndex = 0
                .SelectedValue = CType(cboRequestMode.SelectedItem, ComboBoxValue).Value
            End With

            Dim strSrvType As String = ""
            strSrvType = CInt(enOrbitServiceInfo.ORB_CREATECUSTOMER).ToString() & "," & CInt(enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT)

            dsList = objMaster.GetOrbitServiceList("List", True, strSrvType)
            Dim dr As DataRow = dsList.Tables(0).NewRow
            dr("Id") = "999"
            dr("Name") = Language.getMessage(mstrModuleName, 5, "Process Account Number")
            dsList.Tables(0).Rows.Add(dr)

            With cboRequestType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objMaster = Nothing
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim strFilter As String = ""
            If pnlDate.Enabled = True Then
                If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                    strFilter &= "AND CONVERT(CHAR(8),requestdate,112) BETWEEN '" & eZeeDate.convertDate(dtpStartDate.Value).ToString() & _
                                 "' AND '" & eZeeDate.convertDate(dtpEndDate.Value).ToString() & "' "
                End If
                If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = False Then
                    strFilter &= "AND CONVERT(CHAR(8),requestdate,112) = '" & eZeeDate.convertDate(dtpEndDate.Value).ToString() & "' "
                End If
                If dtpStartDate.Checked = False AndAlso dtpEndDate.Checked = True Then
                    strFilter &= "AND CONVERT(CHAR(8),requestdate,112) = '" & eZeeDate.convertDate(dtpEndDate.Value).ToString() & "' "
                End If
            End If
            If mstrAdvanceFilter.Trim.Length > 0 Then
                strFilter &= "AND " & mstrAdvanceFilter
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)


            mdtEmployee = objRequest.GetDisplayList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    CType(CType(cboRequestMode.SelectedItem, ComboBoxValue).Value, clsOrbit_Request_Tran.enViewType), _
                                                    CType(cboRequestType.SelectedValue, enOrbitServiceInfo), strFilter)
            If mdtEmployee IsNot Nothing Then
                mvwEmployee = mdtEmployee.DefaultView
                dgvAEmployee.AutoGenerateColumns = False

                objdgcolhiCheck.DataPropertyName = "iCheck"
                dgcolhemployee.DataPropertyName = "employee"
                dgcolhrequestdate.DataPropertyName = "requestdate"
                dgcolhreferenceno.DataPropertyName = "referenceno"
                dgcolhcustomer_number.DataPropertyName = "customer_number"
                dgcolherror_description.DataPropertyName = "error_description"
                objdgcolhemployeeunkid.DataPropertyName = "employeeunkid"
                dgcolhprimaryactnumber.DataPropertyName = "primaryactnumber"

                dgvAEmployee.DataSource = mvwEmployee
                txtSearchEmp.Enabled = True
            Else
                txtSearchEmp.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ShowCount()
        Try
            Call objbtnSearch.ShowResult(CStr(dgvAEmployee.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub PerformOperation()
        Try
            Dim iList As List(Of DataGridViewRow) = dgvAEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhiCheck.Index).Value) = True).ToList()
            pbProgress_Report.Maximum = iList.Count
            For Each gRow In iList
                If mblnCancelWorker = True Then Exit For
                Dim xMsg As String = ""
                Select Case CInt(cboRequestType.SelectedValue)
                    Case enOrbitServiceInfo.ORB_CREATECUSTOMER
                        objRequest.CreateCustomer(Company._Object._Companyunkid, CInt(gRow.Cells(objdgcolhemployeeunkid.Index).Value), ConfigParameter._Object._IsImgInDataBase, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._PhotoPath, xMsg, mblnIsErrorOnRequest) 'S.SANDEEP |30-MAY-2020| -- START {mblnIsErrorOnRequest} -- END
                        gRow.Cells(dgcolherror_description.Index).Value = xMsg
                        If xMsg.Trim.Length > 0 Then
                            gRow.DefaultCellStyle.ForeColor = Color.Red
                        End If
                    Case enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT
                        objRequest.CreateDepositAccount(Company._Object._Companyunkid, gRow.Cells(dgcolhemployee.Index).Value.ToString(), gRow.Cells(dgcolhcustomer_number.Index).Value.ToString(), eZeeDate.convertDate(Now.Date), CInt(gRow.Cells(objdgcolhemployeeunkid.Index).Value), xMsg, mblnIsErrorOnRequest) 'S.SANDEEP |30-MAY-2020| -- START {mblnIsErrorOnRequest} -- END
                        gRow.Cells(dgcolherror_description.Index).Value = xMsg
                        If xMsg.Trim.Length > 0 Then
                            gRow.DefaultCellStyle.ForeColor = Color.Red
                        End If
                    Case 999 'UPDATING BANK ACCOUNT NUMNER 
                        objRequest.AssignBankAccount(CInt(gRow.Cells(objdgcolhemployeeunkid.Index).Value), mintEffectivePeriodId, mintBankGroupId, mintBranchId, mintAccountTypeId, mintDistributionModeId, mdecDistributionValue, gRow.Cells(dgcolhprimaryactnumber.Index).Value.ToString, xMsg, mblnIsErrorOnRequest) 'S.SANDEEP |30-MAY-2020| -- START {mblnIsErrorOnRequest} -- END
                        gRow.Cells(dgcolherror_description.Index).Value = xMsg
                        If xMsg.Trim.Length > 0 Then
                            gRow.DefaultCellStyle.ForeColor = Color.Red
                        End If
                End Select
                objlblValue.Text = gRow.Cells(dgcolhemployee.Index).Value.ToString
                'S.SANDEEP |30-MAY-2020| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                If mblnIsErrorOnRequest Then
                    iErList.Add(objlblValue.Text)
                End If
                'S.SANDEEP |30-MAY-2020| -- END                
                objbgWorker.ReportProgress(gRow.Index + 1)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "PerformOperation", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillGrid()
            ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboRequestMode.SelectedIndex = 0
            mstrAdvanceFilter = ""
            dgvAEmployee.DataSource = Nothing
            ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Try
            If dgvAEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhiCheck.Index).Value) = True).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Please check atleast one employee in order to perform selected operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(cboRequestType.SelectedValue) = 999 Then
                Dim frm As New frmOrbitBankAcctMapping
                frm.ShowDialog()
                If frm._IsMapped Then
                    mintEffectivePeriodId = frm._EffectivePeriodId
                    mintBankGroupId = frm._BankGroupId
                    mintBranchId = frm._BranchId
                    mintAccountTypeId = frm._AccountTypeId
                    mintDistributionModeId = frm._DistributionModeId
                    mdecDistributionValue = frm._DistributionValue
                Else
                    Exit Sub
                End If
                frm.Dispose()
            End If
            tblStatus.Visible = True
            gbFilterCriteria.Enabled = False
            gbEmployee.Enabled = False
            btnPost.Enabled = False
            btnClose.Enabled = False
            btnExport.Visible = False
            btnStopProcess.Visible = True
            btnStopProcess.Enabled = True

            objbgWorker.RunWorkerAsync()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPost_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If dgvAEmployee.RowCount <= 0 Then Exit Sub

            Dim StrFilter As String = String.Empty
            StrFilter &= "," & Language.getMessage(mstrModuleName, 500, "Total Employee") & " : " & dgvAEmployee.RowCount.ToString

            If cboRequestMode.SelectedIndex > 0 Then
                StrFilter &= "," & lblRequestMode.Text & " : " & cboRequestMode.Text
            End If

            If cboRequestType.SelectedIndex > 0 Then
                StrFilter &= "," & lblRequestType.Text & " : " & cboRequestType.Text
            End If

            If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                StrFilter &= "," & lblReqDateFrom.Text & " : " & dtpStartDate.Value.Date.ToShortDateString
                StrFilter &= "," & lblReqDateTo.Text & " : " & dtpEndDate.Value.Date.ToShortDateString
            End If
            Dim oView As DataView = mvwEmployee

            oView.Table.Columns.Remove("employeeunkid")
            oView.Table.Columns.Remove("iCheck")
            Dim ds As New DataSet : ds.Tables.Add(oView.ToTable)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, ds)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnStopProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStopProcess.Click
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Are you sure want stop current posting process?"), CType(enMsgBoxStyle.OkCancel + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.OK Then
                mblnCancelWorker = True
                objbgWorker.CancelAsync()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnStopProcess_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "EM"
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " ComboBox Event(s) "

    Private Sub cboRequestMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRequestMode.SelectedIndexChanged
        Try
            Select Case CType(cboRequestMode.SelectedItem, ComboBoxValue).Value
                Case 0, clsOrbit_Request_Tran.enViewType.VW_NEW
                    pnlDate.Enabled = False
                    btnExport.Visible = False : btnStopProcess.Visible = False
                Case clsOrbit_Request_Tran.enViewType.VW_FAILED
                    pnlDate.Enabled = True
                    btnExport.Visible = True : btnStopProcess.Visible = False
            End Select
            cboRequestType.SelectedValue = 0
            mblnIsErrorOnRequest = False 'S.SANDEEP |30-MAY-2020| -- START {mblnIsErrorOnRequest} -- END
            iErList = New List(Of String) 'S.SANDEEP |30-MAY-2020| -- START {iList} -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboRequestMode_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboRequestType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRequestType.SelectedIndexChanged
        Try
            dgvAEmployee.DataSource = Nothing : ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In mvwEmployee
                dr.Item("iCheck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If mvwEmployee IsNot Nothing Then
                Dim strSearch As String = ""
                If txtSearchEmp.Text.Trim.Length > 0 Then
                    strSearch = dgcolhemployee.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolhreferenceno.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolhcustomer_number.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolherror_description.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' "
                End If
                mvwEmployee.RowFilter = strSearch
                ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhiCheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = mvwEmployee.ToTable.Select("iCheck = true", "")
                If drRow.Length > 0 Then
                    If mvwEmployee.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Background Worker Event(s) "

    Private Sub objbgWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgWorker.DoWork
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            Call PerformOperation()
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgWorker_DoWork", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbgWorker_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgWorker.ProgressChanged
        Try
            Application.DoEvents()
            If e.ProgressPercentage > pbProgress_Report.Maximum Then
                pbProgress_Report.Value = pbProgress_Report.Maximum
            Else
                pbProgress_Report.Value = e.ProgressPercentage
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgWorker_ProgressChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbgWorker_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgWorker.RunWorkerCompleted
        Try
            tblStatus.Visible = False
            objlblValue.Text = ""
            gbFilterCriteria.Enabled = True
            gbEmployee.Enabled = True
            btnPost.Enabled = True
            btnClose.Enabled = True
            pbProgress_Report.Value = 0
            pbProgress_Report.Visible = False
            btnExport.Visible = False
            btnStopProcess.Visible = False
            Application.DoEvents()
            'S.SANDEEP |30-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
            If iErList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Problem in processing of selected reqiest for some of the selected employee(s). Please refer to failed request for more information."), enMsgBoxStyle.Information)
            Else
                If dgvAEmployee.RowCount > 0 Then
                    If dgvAEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(r) r.DefaultCellStyle.ForeColor = Color.Red).Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, we cannot process for some of the selected employee(s), as there are some mandatory information missing on them. Please refer to the message on the list for more information."), enMsgBoxStyle.Exclamation)
                    Else
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selected Operation processed successfully."), enMsgBoxStyle.Information)
            End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selected Operation processed successfully."), enMsgBoxStyle.Information)
                End If
            End If
            'S.SANDEEP |30-MAY-2020| -- END            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgWorker_RunWorkerCompleted", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnPost.GradientBackColor = GUI._ButttonBackColor
            Me.btnPost.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnStopProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnStopProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.pbProgress_Report.Text = Language._Object.getCaption(Me.pbProgress_Report.Name, Me.pbProgress_Report.Text)
            Me.btnPost.Text = Language._Object.getCaption(Me.btnPost.Name, Me.btnPost.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnStopProcess.Text = Language._Object.getCaption(Me.btnStopProcess.Name, Me.btnStopProcess.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblRequestType.Text = Language._Object.getCaption(Me.lblRequestType.Name, Me.lblRequestType.Text)
            Me.lblRequestMode.Text = Language._Object.getCaption(Me.lblRequestMode.Name, Me.lblRequestMode.Text)
            Me.lblReqDateTo.Text = Language._Object.getCaption(Me.lblReqDateTo.Name, Me.lblReqDateTo.Text)
            Me.lblReqDateFrom.Text = Language._Object.getCaption(Me.lblReqDateFrom.Name, Me.lblReqDateFrom.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.lblProcessing.Text = Language._Object.getCaption(Me.lblProcessing.Name, Me.lblProcessing.Text)
            Me.dgcolhemployee.HeaderText = Language._Object.getCaption(Me.dgcolhemployee.Name, Me.dgcolhemployee.HeaderText)
            Me.dgcolhrequestdate.HeaderText = Language._Object.getCaption(Me.dgcolhrequestdate.Name, Me.dgcolhrequestdate.HeaderText)
            Me.dgcolhreferenceno.HeaderText = Language._Object.getCaption(Me.dgcolhreferenceno.Name, Me.dgcolhreferenceno.HeaderText)
            Me.dgcolhcustomer_number.HeaderText = Language._Object.getCaption(Me.dgcolhcustomer_number.Name, Me.dgcolhcustomer_number.HeaderText)
            Me.dgcolhprimaryactnumber.HeaderText = Language._Object.getCaption(Me.dgcolhprimaryactnumber.Name, Me.dgcolhprimaryactnumber.HeaderText)
            Me.dgcolherror_description.HeaderText = Language._Object.getCaption(Me.dgcolherror_description.Name, Me.dgcolherror_description.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "New Request")
            Language.setMessage(mstrModuleName, 3, "Failed")
            Language.setMessage(mstrModuleName, 4, "Success")
            Language.setMessage(mstrModuleName, 5, "Process Account Number")
            Language.setMessage(mstrModuleName, 6, "Sorry, Please check atleast one employee in order to perform selected operation.")
            Language.setMessage(mstrModuleName, 7, "Selected Operation processed successfully.")
            Language.setMessage(mstrModuleName, 8, "Problem in processing of selected reqiest for some of the selected employee(s). Please refer to failed request for more information.")
            Language.setMessage(mstrModuleName, 9, "Exported Successfully.")
            Language.setMessage(mstrModuleName, 10, "Are you sure want stop current posting process?")
            Language.setMessage(mstrModuleName, 11, "Sorry, we cannot process for some of the selected employee(s), as there are some mandatory information missing on them. Please refer to the message on the list for more information.")
            Language.setMessage(mstrModuleName, 500, "Total Employee")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class