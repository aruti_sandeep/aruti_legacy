﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeExperience
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeExperience))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlEmployeeExperience = New System.Windows.Forms.Panel
        Me.tabcJobHistory = New System.Windows.Forms.TabControl
        Me.tabpJobInfo = New System.Windows.Forms.TabPage
        Me.gbEmployeeExperience = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.txtSign = New eZee.TextBox.AlphanumericTextBox
        Me.tabcBenefits = New System.Windows.Forms.TabControl
        Me.tabpBenefit = New System.Windows.Forms.TabPage
        Me.objbtnAddBenefit = New eZee.Common.eZeeGradientButton
        Me.lvBenefitHoldings = New System.Windows.Forms.ListView
        Me.objColhBenefit = New System.Windows.Forms.ColumnHeader
        Me.tabpOtherBenefit = New System.Windows.Forms.TabPage
        Me.txtOtherBenefits = New eZee.TextBox.AlphanumericTextBox
        Me.txtOldJob = New eZee.TextBox.AlphanumericTextBox
        Me.txtContactNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtContactPerson = New eZee.TextBox.AlphanumericTextBox
        Me.lblContactNo = New System.Windows.Forms.Label
        Me.lblContacPerson = New System.Windows.Forms.Label
        Me.objLine = New eZee.Common.eZeeStraightLine
        Me.chkCanContatEmployee = New System.Windows.Forms.CheckBox
        Me.txtGrossPay = New eZee.TextBox.NumericTextBox
        Me.lblLastGrossPay = New System.Windows.Forms.Label
        Me.txtAddress = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lnJobInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.txtNote = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblRemark = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.lblSupervisor = New System.Windows.Forms.Label
        Me.txtSupervisor = New eZee.TextBox.AlphanumericTextBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.txtCompany = New eZee.TextBox.AlphanumericTextBox
        Me.lblInstitution = New System.Windows.Forms.Label
        Me.tabpOtherDetails = New System.Windows.Forms.TabPage
        Me.gbLeavingReason = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlLeavingReason = New System.Windows.Forms.Panel
        Me.txtLeaveReason = New eZee.TextBox.AlphanumericTextBox
        Me.gbOtherDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlMemo = New System.Windows.Forms.Panel
        Me.txtMemoField = New eZee.TextBox.AlphanumericTextBox
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.btnAddAttachment = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboDocumentType = New System.Windows.Forms.ComboBox
        Me.lblDocumentType = New System.Windows.Forms.Label
        Me.dgvExperience = New System.Windows.Forms.DataGridView
        Me.objcohDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSize = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ofdAttachment = New System.Windows.Forms.OpenFileDialog
        Me.sfdAttachment = New System.Windows.Forms.SaveFileDialog
        Me.pnlEmployeeExperience.SuspendLayout()
        Me.tabcJobHistory.SuspendLayout()
        Me.tabpJobInfo.SuspendLayout()
        Me.gbEmployeeExperience.SuspendLayout()
        Me.tabcBenefits.SuspendLayout()
        Me.tabpBenefit.SuspendLayout()
        Me.tabpOtherBenefit.SuspendLayout()
        Me.tabpOtherDetails.SuspendLayout()
        Me.gbLeavingReason.SuspendLayout()
        Me.pnlLeavingReason.SuspendLayout()
        Me.gbOtherDetails.SuspendLayout()
        Me.pnlMemo.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvExperience, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlEmployeeExperience
        '
        Me.pnlEmployeeExperience.Controls.Add(Me.tabcJobHistory)
        Me.pnlEmployeeExperience.Controls.Add(Me.eZeeHeader)
        Me.pnlEmployeeExperience.Controls.Add(Me.objFooter)
        Me.pnlEmployeeExperience.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmployeeExperience.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmployeeExperience.Name = "pnlEmployeeExperience"
        Me.pnlEmployeeExperience.Size = New System.Drawing.Size(692, 513)
        Me.pnlEmployeeExperience.TabIndex = 0
        '
        'tabcJobHistory
        '
        Me.tabcJobHistory.Controls.Add(Me.tabpJobInfo)
        Me.tabcJobHistory.Controls.Add(Me.tabpOtherDetails)
        Me.tabcJobHistory.Controls.Add(Me.TabPage1)
        Me.tabcJobHistory.Location = New System.Drawing.Point(12, 64)
        Me.tabcJobHistory.Name = "tabcJobHistory"
        Me.tabcJobHistory.SelectedIndex = 0
        Me.tabcJobHistory.Size = New System.Drawing.Size(669, 389)
        Me.tabcJobHistory.TabIndex = 0
        '
        'tabpJobInfo
        '
        Me.tabpJobInfo.Controls.Add(Me.gbEmployeeExperience)
        Me.tabpJobInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpJobInfo.Name = "tabpJobInfo"
        Me.tabpJobInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpJobInfo.Size = New System.Drawing.Size(661, 363)
        Me.tabpJobInfo.TabIndex = 0
        Me.tabpJobInfo.Text = "Job Information"
        Me.tabpJobInfo.UseVisualStyleBackColor = True
        '
        'gbEmployeeExperience
        '
        Me.gbEmployeeExperience.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeExperience.Checked = False
        Me.gbEmployeeExperience.CollapseAllExceptThis = False
        Me.gbEmployeeExperience.CollapsedHoverImage = Nothing
        Me.gbEmployeeExperience.CollapsedNormalImage = Nothing
        Me.gbEmployeeExperience.CollapsedPressedImage = Nothing
        Me.gbEmployeeExperience.CollapseOnLoad = False
        Me.gbEmployeeExperience.Controls.Add(Me.lblCurrency)
        Me.gbEmployeeExperience.Controls.Add(Me.txtSign)
        Me.gbEmployeeExperience.Controls.Add(Me.tabcBenefits)
        Me.gbEmployeeExperience.Controls.Add(Me.txtOldJob)
        Me.gbEmployeeExperience.Controls.Add(Me.txtContactNo)
        Me.gbEmployeeExperience.Controls.Add(Me.txtContactPerson)
        Me.gbEmployeeExperience.Controls.Add(Me.lblContactNo)
        Me.gbEmployeeExperience.Controls.Add(Me.lblContacPerson)
        Me.gbEmployeeExperience.Controls.Add(Me.objLine)
        Me.gbEmployeeExperience.Controls.Add(Me.chkCanContatEmployee)
        Me.gbEmployeeExperience.Controls.Add(Me.txtGrossPay)
        Me.gbEmployeeExperience.Controls.Add(Me.lblLastGrossPay)
        Me.gbEmployeeExperience.Controls.Add(Me.txtAddress)
        Me.gbEmployeeExperience.Controls.Add(Me.lblAddress)
        Me.gbEmployeeExperience.Controls.Add(Me.lnJobInfo)
        Me.gbEmployeeExperience.Controls.Add(Me.lnEmployeeName)
        Me.gbEmployeeExperience.Controls.Add(Me.txtNote)
        Me.gbEmployeeExperience.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeExperience.Controls.Add(Me.lblRemark)
        Me.gbEmployeeExperience.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeExperience.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeExperience.Controls.Add(Me.dtpEndDate)
        Me.gbEmployeeExperience.Controls.Add(Me.dtpStartDate)
        Me.gbEmployeeExperience.Controls.Add(Me.lblEndDate)
        Me.gbEmployeeExperience.Controls.Add(Me.lblStartDate)
        Me.gbEmployeeExperience.Controls.Add(Me.lblSupervisor)
        Me.gbEmployeeExperience.Controls.Add(Me.txtSupervisor)
        Me.gbEmployeeExperience.Controls.Add(Me.lblJob)
        Me.gbEmployeeExperience.Controls.Add(Me.txtCompany)
        Me.gbEmployeeExperience.Controls.Add(Me.lblInstitution)
        Me.gbEmployeeExperience.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbEmployeeExperience.ExpandedHoverImage = Nothing
        Me.gbEmployeeExperience.ExpandedNormalImage = Nothing
        Me.gbEmployeeExperience.ExpandedPressedImage = Nothing
        Me.gbEmployeeExperience.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeExperience.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeExperience.HeaderHeight = 25
        Me.gbEmployeeExperience.HeaderMessage = ""
        Me.gbEmployeeExperience.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeExperience.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeExperience.HeightOnCollapse = 0
        Me.gbEmployeeExperience.LeftTextSpace = 0
        Me.gbEmployeeExperience.Location = New System.Drawing.Point(3, 3)
        Me.gbEmployeeExperience.Name = "gbEmployeeExperience"
        Me.gbEmployeeExperience.OpenHeight = 262
        Me.gbEmployeeExperience.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeExperience.ShowBorder = True
        Me.gbEmployeeExperience.ShowCheckBox = False
        Me.gbEmployeeExperience.ShowCollapseButton = False
        Me.gbEmployeeExperience.ShowDefaultBorderColor = True
        Me.gbEmployeeExperience.ShowDownButton = False
        Me.gbEmployeeExperience.ShowHeader = True
        Me.gbEmployeeExperience.Size = New System.Drawing.Size(655, 357)
        Me.gbEmployeeExperience.TabIndex = 0
        Me.gbEmployeeExperience.Temp = 0
        Me.gbEmployeeExperience.Text = "Job History"
        Me.gbEmployeeExperience.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(574, 188)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(75, 14)
        Me.lblCurrency.TabIndex = 38
        Me.lblCurrency.Text = "Currency Sign"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSign
        '
        Me.txtSign.Flags = 0
        Me.txtSign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSign.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSign.Location = New System.Drawing.Point(577, 208)
        Me.txtSign.Name = "txtSign"
        Me.txtSign.Size = New System.Drawing.Size(71, 21)
        Me.txtSign.TabIndex = 36
        '
        'tabcBenefits
        '
        Me.tabcBenefits.Controls.Add(Me.tabpBenefit)
        Me.tabcBenefits.Controls.Add(Me.tabpOtherBenefit)
        Me.tabcBenefits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcBenefits.Location = New System.Drawing.Point(421, 36)
        Me.tabcBenefits.Name = "tabcBenefits"
        Me.tabcBenefits.SelectedIndex = 0
        Me.tabcBenefits.Size = New System.Drawing.Size(231, 144)
        Me.tabcBenefits.TabIndex = 33
        '
        'tabpBenefit
        '
        Me.tabpBenefit.Controls.Add(Me.objbtnAddBenefit)
        Me.tabpBenefit.Controls.Add(Me.lvBenefitHoldings)
        Me.tabpBenefit.Location = New System.Drawing.Point(4, 22)
        Me.tabpBenefit.Name = "tabpBenefit"
        Me.tabpBenefit.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpBenefit.Size = New System.Drawing.Size(223, 118)
        Me.tabpBenefit.TabIndex = 0
        Me.tabpBenefit.Text = "Benefits"
        Me.tabpBenefit.UseVisualStyleBackColor = True
        '
        'objbtnAddBenefit
        '
        Me.objbtnAddBenefit.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBenefit.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefit.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefit.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBenefit.BorderSelected = False
        Me.objbtnAddBenefit.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBenefit.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddBenefit.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBenefit.Location = New System.Drawing.Point(199, 6)
        Me.objbtnAddBenefit.Name = "objbtnAddBenefit"
        Me.objbtnAddBenefit.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBenefit.TabIndex = 30
        '
        'lvBenefitHoldings
        '
        Me.lvBenefitHoldings.CheckBoxes = True
        Me.lvBenefitHoldings.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhBenefit})
        Me.lvBenefitHoldings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvBenefitHoldings.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvBenefitHoldings.Location = New System.Drawing.Point(4, 28)
        Me.lvBenefitHoldings.Name = "lvBenefitHoldings"
        Me.lvBenefitHoldings.Size = New System.Drawing.Size(214, 86)
        Me.lvBenefitHoldings.TabIndex = 21
        Me.lvBenefitHoldings.UseCompatibleStateImageBehavior = False
        Me.lvBenefitHoldings.View = System.Windows.Forms.View.Details
        '
        'objColhBenefit
        '
        Me.objColhBenefit.Width = 195
        '
        'tabpOtherBenefit
        '
        Me.tabpOtherBenefit.Controls.Add(Me.txtOtherBenefits)
        Me.tabpOtherBenefit.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherBenefit.Name = "tabpOtherBenefit"
        Me.tabpOtherBenefit.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOtherBenefit.Size = New System.Drawing.Size(223, 118)
        Me.tabpOtherBenefit.TabIndex = 1
        Me.tabpOtherBenefit.Text = "Other Benefit"
        Me.tabpOtherBenefit.UseVisualStyleBackColor = True
        '
        'txtOtherBenefits
        '
        Me.txtOtherBenefits.BackColor = System.Drawing.SystemColors.Window
        Me.txtOtherBenefits.Flags = 0
        Me.txtOtherBenefits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherBenefits.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherBenefits.Location = New System.Drawing.Point(3, 3)
        Me.txtOtherBenefits.Multiline = True
        Me.txtOtherBenefits.Name = "txtOtherBenefits"
        Me.txtOtherBenefits.Size = New System.Drawing.Size(217, 113)
        Me.txtOtherBenefits.TabIndex = 33
        '
        'txtOldJob
        '
        Me.txtOldJob.Flags = 0
        Me.txtOldJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOldJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOldJob.Location = New System.Drawing.Point(109, 215)
        Me.txtOldJob.Name = "txtOldJob"
        Me.txtOldJob.Size = New System.Drawing.Size(288, 21)
        Me.txtOldJob.TabIndex = 32
        '
        'txtContactNo
        '
        Me.txtContactNo.Flags = 0
        Me.txtContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtContactNo.Location = New System.Drawing.Point(421, 306)
        Me.txtContactNo.Multiline = True
        Me.txtContactNo.Name = "txtContactNo"
        Me.txtContactNo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtContactNo.Size = New System.Drawing.Size(227, 43)
        Me.txtContactNo.TabIndex = 28
        '
        'txtContactPerson
        '
        Me.txtContactPerson.Flags = 0
        Me.txtContactPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactPerson.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtContactPerson.Location = New System.Drawing.Point(515, 257)
        Me.txtContactPerson.Name = "txtContactPerson"
        Me.txtContactPerson.Size = New System.Drawing.Size(133, 21)
        Me.txtContactPerson.TabIndex = 26
        '
        'lblContactNo
        '
        Me.lblContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNo.Location = New System.Drawing.Point(418, 281)
        Me.lblContactNo.Name = "lblContactNo"
        Me.lblContactNo.Size = New System.Drawing.Size(110, 14)
        Me.lblContactNo.TabIndex = 27
        Me.lblContactNo.Text = "Contact Address"
        Me.lblContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContacPerson
        '
        Me.lblContacPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContacPerson.Location = New System.Drawing.Point(418, 260)
        Me.lblContacPerson.Name = "lblContacPerson"
        Me.lblContacPerson.Size = New System.Drawing.Size(93, 14)
        Me.lblContacPerson.TabIndex = 25
        Me.lblContacPerson.Text = "Contact Person"
        Me.lblContacPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine
        '
        Me.objLine.BackColor = System.Drawing.Color.Transparent
        Me.objLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine.Location = New System.Drawing.Point(401, 110)
        Me.objLine.Name = "objLine"
        Me.objLine.Size = New System.Drawing.Size(14, 239)
        Me.objLine.TabIndex = 19
        '
        'chkCanContatEmployee
        '
        Me.chkCanContatEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCanContatEmployee.Location = New System.Drawing.Point(421, 235)
        Me.chkCanContatEmployee.Name = "chkCanContatEmployee"
        Me.chkCanContatEmployee.Size = New System.Drawing.Size(215, 16)
        Me.chkCanContatEmployee.TabIndex = 24
        Me.chkCanContatEmployee.Text = "Can Contact Previous Employer"
        Me.chkCanContatEmployee.UseVisualStyleBackColor = True
        '
        'txtGrossPay
        '
        Me.txtGrossPay.AllowNegative = False
        Me.txtGrossPay.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtGrossPay.DigitsInGroup = 0
        Me.txtGrossPay.Flags = 65536
        Me.txtGrossPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrossPay.Location = New System.Drawing.Point(421, 208)
        Me.txtGrossPay.MaxDecimalPlaces = 6
        Me.txtGrossPay.MaxWholeDigits = 21
        Me.txtGrossPay.Name = "txtGrossPay"
        Me.txtGrossPay.Prefix = ""
        Me.txtGrossPay.RangeMax = 1.7976931348623157E+308
        Me.txtGrossPay.RangeMin = -1.7976931348623157E+308
        Me.txtGrossPay.Size = New System.Drawing.Size(150, 21)
        Me.txtGrossPay.TabIndex = 23
        Me.txtGrossPay.Text = "0"
        Me.txtGrossPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLastGrossPay
        '
        Me.lblLastGrossPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastGrossPay.Location = New System.Drawing.Point(418, 188)
        Me.lblLastGrossPay.Name = "lblLastGrossPay"
        Me.lblLastGrossPay.Size = New System.Drawing.Size(80, 14)
        Me.lblLastGrossPay.TabIndex = 22
        Me.lblLastGrossPay.Text = "Last Gross Pay"
        Me.lblLastGrossPay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.Flags = 0
        Me.txtAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress.Location = New System.Drawing.Point(109, 137)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAddress.Size = New System.Drawing.Size(288, 45)
        Me.txtAddress.TabIndex = 8
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(17, 137)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(86, 16)
        Me.lblAddress.TabIndex = 7
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnJobInfo
        '
        Me.lnJobInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnJobInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnJobInfo.Location = New System.Drawing.Point(8, 86)
        Me.lnJobInfo.Name = "lnJobInfo"
        Me.lnJobInfo.Size = New System.Drawing.Size(236, 21)
        Me.lnJobInfo.TabIndex = 4
        Me.lnJobInfo.Text = "Job Info"
        Me.lnJobInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(236, 21)
        Me.lnEmployeeName.TabIndex = 0
        Me.lnEmployeeName.Text = "Employee Info"
        Me.lnEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNote
        '
        Me.txtNote.Flags = 0
        Me.txtNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNote.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNote.Location = New System.Drawing.Point(109, 269)
        Me.txtNote.Multiline = True
        Me.txtNote.Name = "txtNote"
        Me.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNote.Size = New System.Drawing.Size(288, 80)
        Me.txtNote.TabIndex = 18
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(398, 62)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(25, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(17, 272)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(86, 16)
        Me.lblRemark.TabIndex = 17
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(109, 62)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(287, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(20, 65)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(83, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(295, 188)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(102, 21)
        Me.dtpEndDate.TabIndex = 11
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(109, 188)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpStartDate.TabIndex = 9
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(218, 190)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(74, 16)
        Me.lblEndDate.TabIndex = 10
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(17, 190)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(86, 16)
        Me.lblStartDate.TabIndex = 8
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSupervisor
        '
        Me.lblSupervisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupervisor.Location = New System.Drawing.Point(17, 244)
        Me.lblSupervisor.Name = "lblSupervisor"
        Me.lblSupervisor.Size = New System.Drawing.Size(86, 16)
        Me.lblSupervisor.TabIndex = 15
        Me.lblSupervisor.Text = "Supervisor"
        Me.lblSupervisor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSupervisor
        '
        Me.txtSupervisor.Flags = 0
        Me.txtSupervisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSupervisor.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSupervisor.Location = New System.Drawing.Point(109, 242)
        Me.txtSupervisor.Name = "txtSupervisor"
        Me.txtSupervisor.Size = New System.Drawing.Size(288, 21)
        Me.txtSupervisor.TabIndex = 16
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(17, 217)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(86, 16)
        Me.lblJob.TabIndex = 12
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompany
        '
        Me.txtCompany.Flags = 0
        Me.txtCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompany.Location = New System.Drawing.Point(109, 110)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(287, 21)
        Me.txtCompany.TabIndex = 6
        '
        'lblInstitution
        '
        Me.lblInstitution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstitution.Location = New System.Drawing.Point(17, 112)
        Me.lblInstitution.Name = "lblInstitution"
        Me.lblInstitution.Size = New System.Drawing.Size(86, 16)
        Me.lblInstitution.TabIndex = 5
        Me.lblInstitution.Text = "Company"
        Me.lblInstitution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpOtherDetails
        '
        Me.tabpOtherDetails.Controls.Add(Me.gbLeavingReason)
        Me.tabpOtherDetails.Controls.Add(Me.gbOtherDetails)
        Me.tabpOtherDetails.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherDetails.Name = "tabpOtherDetails"
        Me.tabpOtherDetails.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOtherDetails.Size = New System.Drawing.Size(661, 363)
        Me.tabpOtherDetails.TabIndex = 1
        Me.tabpOtherDetails.Text = "Other Details"
        Me.tabpOtherDetails.UseVisualStyleBackColor = True
        '
        'gbLeavingReason
        '
        Me.gbLeavingReason.BorderColor = System.Drawing.Color.Black
        Me.gbLeavingReason.Checked = False
        Me.gbLeavingReason.CollapseAllExceptThis = False
        Me.gbLeavingReason.CollapsedHoverImage = Nothing
        Me.gbLeavingReason.CollapsedNormalImage = Nothing
        Me.gbLeavingReason.CollapsedPressedImage = Nothing
        Me.gbLeavingReason.CollapseOnLoad = False
        Me.gbLeavingReason.Controls.Add(Me.pnlLeavingReason)
        Me.gbLeavingReason.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbLeavingReason.ExpandedHoverImage = Nothing
        Me.gbLeavingReason.ExpandedNormalImage = Nothing
        Me.gbLeavingReason.ExpandedPressedImage = Nothing
        Me.gbLeavingReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeavingReason.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeavingReason.HeaderHeight = 25
        Me.gbLeavingReason.HeaderMessage = ""
        Me.gbLeavingReason.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeavingReason.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeavingReason.HeightOnCollapse = 0
        Me.gbLeavingReason.LeftTextSpace = 0
        Me.gbLeavingReason.Location = New System.Drawing.Point(3, 251)
        Me.gbLeavingReason.Name = "gbLeavingReason"
        Me.gbLeavingReason.OpenHeight = 100
        Me.gbLeavingReason.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeavingReason.ShowBorder = True
        Me.gbLeavingReason.ShowCheckBox = False
        Me.gbLeavingReason.ShowCollapseButton = False
        Me.gbLeavingReason.ShowDefaultBorderColor = True
        Me.gbLeavingReason.ShowDownButton = False
        Me.gbLeavingReason.ShowHeader = True
        Me.gbLeavingReason.Size = New System.Drawing.Size(655, 109)
        Me.gbLeavingReason.TabIndex = 1
        Me.gbLeavingReason.Temp = 0
        Me.gbLeavingReason.Text = "Leaving Reason"
        Me.gbLeavingReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLeavingReason
        '
        Me.pnlLeavingReason.Controls.Add(Me.txtLeaveReason)
        Me.pnlLeavingReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLeavingReason.Location = New System.Drawing.Point(1, 25)
        Me.pnlLeavingReason.Name = "pnlLeavingReason"
        Me.pnlLeavingReason.Size = New System.Drawing.Size(652, 82)
        Me.pnlLeavingReason.TabIndex = 145
        '
        'txtLeaveReason
        '
        Me.txtLeaveReason.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLeaveReason.Flags = 0
        Me.txtLeaveReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeaveReason.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLeaveReason.Location = New System.Drawing.Point(0, 0)
        Me.txtLeaveReason.Multiline = True
        Me.txtLeaveReason.Name = "txtLeaveReason"
        Me.txtLeaveReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLeaveReason.Size = New System.Drawing.Size(652, 82)
        Me.txtLeaveReason.TabIndex = 0
        '
        'gbOtherDetails
        '
        Me.gbOtherDetails.BorderColor = System.Drawing.Color.Black
        Me.gbOtherDetails.Checked = False
        Me.gbOtherDetails.CollapseAllExceptThis = False
        Me.gbOtherDetails.CollapsedHoverImage = Nothing
        Me.gbOtherDetails.CollapsedNormalImage = Nothing
        Me.gbOtherDetails.CollapsedPressedImage = Nothing
        Me.gbOtherDetails.CollapseOnLoad = False
        Me.gbOtherDetails.Controls.Add(Me.pnlMemo)
        Me.gbOtherDetails.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbOtherDetails.ExpandedHoverImage = Nothing
        Me.gbOtherDetails.ExpandedNormalImage = Nothing
        Me.gbOtherDetails.ExpandedPressedImage = Nothing
        Me.gbOtherDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtherDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOtherDetails.HeaderHeight = 25
        Me.gbOtherDetails.HeaderMessage = ""
        Me.gbOtherDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOtherDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOtherDetails.HeightOnCollapse = 0
        Me.gbOtherDetails.LeftTextSpace = 0
        Me.gbOtherDetails.Location = New System.Drawing.Point(3, 3)
        Me.gbOtherDetails.Name = "gbOtherDetails"
        Me.gbOtherDetails.OpenHeight = 100
        Me.gbOtherDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOtherDetails.ShowBorder = True
        Me.gbOtherDetails.ShowCheckBox = False
        Me.gbOtherDetails.ShowCollapseButton = False
        Me.gbOtherDetails.ShowDefaultBorderColor = True
        Me.gbOtherDetails.ShowDownButton = False
        Me.gbOtherDetails.ShowHeader = True
        Me.gbOtherDetails.Size = New System.Drawing.Size(655, 223)
        Me.gbOtherDetails.TabIndex = 0
        Me.gbOtherDetails.Temp = 0
        Me.gbOtherDetails.Text = "Memo"
        Me.gbOtherDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMemo
        '
        Me.pnlMemo.Controls.Add(Me.txtMemoField)
        Me.pnlMemo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMemo.Location = New System.Drawing.Point(1, 25)
        Me.pnlMemo.Name = "pnlMemo"
        Me.pnlMemo.Size = New System.Drawing.Size(652, 197)
        Me.pnlMemo.TabIndex = 1
        '
        'txtMemoField
        '
        Me.txtMemoField.BackColor = System.Drawing.Color.White
        Me.txtMemoField.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtMemoField.Flags = 0
        Me.txtMemoField.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMemoField.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMemoField.Location = New System.Drawing.Point(0, 0)
        Me.txtMemoField.Multiline = True
        Me.txtMemoField.Name = "txtMemoField"
        Me.txtMemoField.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMemoField.Size = New System.Drawing.Size(652, 197)
        Me.txtMemoField.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnAddAttachment)
        Me.TabPage1.Controls.Add(Me.cboDocumentType)
        Me.TabPage1.Controls.Add(Me.lblDocumentType)
        Me.TabPage1.Controls.Add(Me.dgvExperience)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(661, 363)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Attachment"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnAddAttachment
        '
        Me.btnAddAttachment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddAttachment.BackColor = System.Drawing.Color.White
        Me.btnAddAttachment.BackgroundImage = CType(resources.GetObject("btnAddAttachment.BackgroundImage"), System.Drawing.Image)
        Me.btnAddAttachment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddAttachment.BorderColor = System.Drawing.Color.Empty
        Me.btnAddAttachment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddAttachment.FlatAppearance.BorderSize = 0
        Me.btnAddAttachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddAttachment.ForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddAttachment.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Location = New System.Drawing.Point(286, 15)
        Me.btnAddAttachment.Name = "btnAddAttachment"
        Me.btnAddAttachment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Size = New System.Drawing.Size(119, 29)
        Me.btnAddAttachment.TabIndex = 229
        Me.btnAddAttachment.Text = "&Browse"
        Me.btnAddAttachment.UseVisualStyleBackColor = True
        '
        'cboDocumentType
        '
        Me.cboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDocumentType.FormattingEnabled = True
        Me.cboDocumentType.Location = New System.Drawing.Point(110, 20)
        Me.cboDocumentType.Name = "cboDocumentType"
        Me.cboDocumentType.Size = New System.Drawing.Size(161, 21)
        Me.cboDocumentType.TabIndex = 230
        '
        'lblDocumentType
        '
        Me.lblDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentType.Location = New System.Drawing.Point(3, 21)
        Me.lblDocumentType.Name = "lblDocumentType"
        Me.lblDocumentType.Size = New System.Drawing.Size(98, 16)
        Me.lblDocumentType.TabIndex = 231
        Me.lblDocumentType.Text = "Document Type"
        Me.lblDocumentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvExperience
        '
        Me.dgvExperience.AllowUserToAddRows = False
        Me.dgvExperience.AllowUserToDeleteRows = False
        Me.dgvExperience.AllowUserToResizeRows = False
        Me.dgvExperience.BackgroundColor = System.Drawing.Color.White
        Me.dgvExperience.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvExperience.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvExperience.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohDelete, Me.colhName, Me.colhSize, Me.objcolhDownload, Me.objcolhGUID, Me.objcolhScanUnkId})
        Me.dgvExperience.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvExperience.Location = New System.Drawing.Point(3, 61)
        Me.dgvExperience.Name = "dgvExperience"
        Me.dgvExperience.RowHeadersVisible = False
        Me.dgvExperience.Size = New System.Drawing.Size(655, 299)
        Me.dgvExperience.TabIndex = 232
        '
        'objcohDelete
        '
        Me.objcohDelete.HeaderText = ""
        Me.objcohDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objcohDelete.Name = "objcohDelete"
        Me.objcohDelete.ReadOnly = True
        Me.objcohDelete.Width = 25
        '
        'colhName
        '
        Me.colhName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhName.HeaderText = "File Name"
        Me.colhName.Name = "colhName"
        Me.colhName.ReadOnly = True
        '
        'colhSize
        '
        Me.colhSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhSize.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhSize.HeaderText = "File Size"
        Me.colhSize.Name = "colhSize"
        Me.colhSize.ReadOnly = True
        '
        'objcolhDownload
        '
        Me.objcolhDownload.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhDownload.DefaultCellStyle = DataGridViewCellStyle4
        Me.objcolhDownload.HeaderText = "Download"
        Me.objcolhDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhDownload.Name = "objcolhDownload"
        Me.objcolhDownload.ReadOnly = True
        Me.objcolhDownload.Text = "Download"
        Me.objcolhDownload.UseColumnTextForLinkValue = True
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "objcolhGUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.Visible = False
        '
        'objcolhScanUnkId
        '
        Me.objcolhScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Name = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(692, 58)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Job History And Experience"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 458)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(692, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(480, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 124
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(583, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 123
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ofdAttachment
        '
        Me.ofdAttachment.FileName = "OpenFileDialog1"
        '
        'frmEmployeeExperience
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(692, 513)
        Me.Controls.Add(Me.pnlEmployeeExperience)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeExperience"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Job History And Experience"
        Me.pnlEmployeeExperience.ResumeLayout(False)
        Me.tabcJobHistory.ResumeLayout(False)
        Me.tabpJobInfo.ResumeLayout(False)
        Me.gbEmployeeExperience.ResumeLayout(False)
        Me.gbEmployeeExperience.PerformLayout()
        Me.tabcBenefits.ResumeLayout(False)
        Me.tabpBenefit.ResumeLayout(False)
        Me.tabpOtherBenefit.ResumeLayout(False)
        Me.tabpOtherBenefit.PerformLayout()
        Me.tabpOtherDetails.ResumeLayout(False)
        Me.gbLeavingReason.ResumeLayout(False)
        Me.pnlLeavingReason.ResumeLayout(False)
        Me.pnlLeavingReason.PerformLayout()
        Me.gbOtherDetails.ResumeLayout(False)
        Me.pnlMemo.ResumeLayout(False)
        Me.pnlMemo.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dgvExperience, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlEmployeeExperience As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbEmployeeExperience As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblInstitution As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblSupervisor As System.Windows.Forms.Label
    Friend WithEvents txtSupervisor As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtNote As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lnJobInfo As eZee.Common.eZeeLine
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents tabcJobHistory As System.Windows.Forms.TabControl
    Friend WithEvents tabpJobInfo As System.Windows.Forms.TabPage
    Friend WithEvents txtAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents tabpOtherDetails As System.Windows.Forms.TabPage
    Friend WithEvents gbOtherDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtLeaveReason As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvBenefitHoldings As System.Windows.Forms.ListView
    Friend WithEvents txtGrossPay As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLastGrossPay As System.Windows.Forms.Label
    Friend WithEvents objLine As eZee.Common.eZeeStraightLine
    Friend WithEvents chkCanContatEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents txtContactNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtContactPerson As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblContactNo As System.Windows.Forms.Label
    Friend WithEvents lblContacPerson As System.Windows.Forms.Label
    Friend WithEvents txtMemoField As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbLeavingReason As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlLeavingReason As System.Windows.Forms.Panel
    Friend WithEvents pnlMemo As System.Windows.Forms.Panel
    Friend WithEvents objColhBenefit As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddBenefit As eZee.Common.eZeeGradientButton
    Friend WithEvents txtOldJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabcBenefits As System.Windows.Forms.TabControl
    Friend WithEvents tabpBenefit As System.Windows.Forms.TabPage
    Friend WithEvents tabpOtherBenefit As System.Windows.Forms.TabPage
    Friend WithEvents txtOtherBenefits As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSign As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnAddAttachment As eZee.Common.eZeeLightButton
    Friend WithEvents cboDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocumentType As System.Windows.Forms.Label
    Friend WithEvents dgvExperience As System.Windows.Forms.DataGridView
    Friend WithEvents objcohDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ofdAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents sfdAttachment As System.Windows.Forms.SaveFileDialog
End Class
