﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMembershipHead

#Region " Private Variables "

    Private mstrModuleName As String = "frmMembershipHead"
    Private mintEmpHeadId As Integer = -1
    Private mintCoHeadId As Integer = -1
    Private mintEffPeriodId As Integer = -1
    Private mblnCancel As Boolean = True
    Private mstrMembershipName As String = String.Empty
    'S.SANDEEP [ 11 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnCopyPerviousED As Boolean = False
    Private mblnOverwritePreviousED As Boolean = False
    Private mblnOverWriteHead As Boolean = False
    'S.SANDEEP [ 11 DEC 2012 ] -- END
#End Region

    'S.SANDEEP [ 11 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Properties "

    Public ReadOnly Property _CopyPreviousED_Slab() As Boolean
        Get
            Return mblnCopyPerviousED
        End Get
    End Property

    Public ReadOnly Property _OverwritePreviousED() As Boolean
        Get
            Return mblnOverwritePreviousED
        End Get
    End Property

    Public ReadOnly Property _OverwriteHead() As Boolean
        Get
            Return mblnOverWriteHead
        End Get
    End Property

#End Region
    'S.SANDEEP [ 11 DEC 2012 ] -- END

#Region " Display Dialog "

    'S.SANDEEP [ 17 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function displayDialog(ByRef intEffPeriodId As Integer, _
    '                       ByVal intEmpHeadId As Integer, _
    '                       ByVal intCoHeadId As Integer, _
    '                       ByVal strMembershipName As String) As Boolean
    Public Function displayDialog(ByRef intEffPeriodId As Integer, _
                                  ByVal intEmpHeadId As Integer, _
                                  ByVal intCoHeadId As Integer, _
                           ByVal strMembershipName As String, _
                           ByVal intEmpId As Integer) As Boolean
        'S.SANDEEP [ 17 OCT 2012 ] -- END
        Try
            mintEmpHeadId = intEmpHeadId
            mintCoHeadId = intCoHeadId
            mintEffPeriodId = intEffPeriodId
            mstrMembershipName = strMembershipName

            'S.SANDEEP [ 11 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'pnlData.Visible = False
            'If intEmpId <= 0 Then
            '    'pnlData.Visible = False
            '    cboEffectivePeriod.Enabled = False
            'Else
            '    'pnlData.Visible = True
            '    cboEffectivePeriod.Enabled = True
            'End If

            If intEmpId <= 0 Then
                pnlData.Visible = False
                cboEffectivePeriod.Enabled = False
            Else
                pnlData.Visible = True
                chkOverwritePrevEDSlabHeads.Checked = False : chkOverwritePrevEDSlabHeads.Enabled = False
                chkOverwrite.Checked = False
                cboEffectivePeriod.Enabled = True
            End If
            'S.SANDEEP [ 11 DEC 2012 ] -- END

            Me.ShowDialog()

            intEffPeriodId = mintEffPeriodId

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms Events "

    Private Sub frmMembershipHead_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Set_Logo(Me, gApplicationType)
            Call Fill_Info()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMembershipHead_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_Info()
        Dim objTHead As New clsTransactionHead
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            txtMembership.Text = mstrMembershipName

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTHead._Tranheadunkid = mintEmpHeadId
            objTHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = mintEmpHeadId
            'Sohail (21 Aug 2015) -- End
            txtEmpHead.Text = objTHead._Trnheadname

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objTHead._Tranheadunkid = mintCoHeadId
            objTHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = mintCoHeadId
            'Sohail (21 Aug 2015) -- End
            txtCoHead.Text = objTHead._Trnheadname

            objTHead = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboEffectivePeriod.SelectedValue) <= 0 Then
            If CInt(cboEffectivePeriod.SelectedValue) <= 0 AndAlso cboEffectivePeriod.Enabled = True Then
                'S.SANDEEP [ 17 OCT 2012 ] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Effective Period is mandatory information. Please select Effective Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            mintEffPeriodId = CInt(cboEffectivePeriod.SelectedValue)

            'S.SANDEEP [ 11 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If pnlData.Visible = True AndAlso chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If
            mblnCopyPerviousED = chkCopyPreviousEDSlab.Checked
            mblnOverwritePreviousED = chkOverwritePrevEDSlabHeads.Checked
            mblnOverWriteHead = chkOverwrite.Checked
            'S.SANDEEP [ 11 DEC 2012 ] -- END

            Call btnClose_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 11 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Controls Events "

    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            'S.SANDEEP [ 28 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If chkCopyPreviousEDSlab.Checked = True Then
            '    chkOverwritePrevEDSlabHeads.Enabled = True
            '    chkOverwrite.Enabled = True
            'Else
            '    chkOverwritePrevEDSlabHeads.Enabled = False
            '    chkOverwrite.Enabled = False
            'End If
            'S.SANDEEP [ 28 MAR 2013 ] -- END
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 11 DEC 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbHeadInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbHeadInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.gbHeadInformation.Text = Language._Object.getCaption(Me.gbHeadInformation.Name, Me.gbHeadInformation.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblCoHead.Text = Language._Object.getCaption(Me.lblCoHead.Name, Me.lblCoHead.Text)
			Me.lblEmpHead.Text = Language._Object.getCaption(Me.lblEmpHead.Name, Me.lblEmpHead.Text)
			Me.lblEffectivePeriod.Text = Language._Object.getCaption(Me.lblEffectivePeriod.Name, Me.lblEffectivePeriod.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
			Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
			Me.chkOverwrite.Text = Language._Object.getCaption(Me.chkOverwrite.Name, Me.chkOverwrite.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Effective Period is mandatory information. Please select Effective Period to continue.")
			Language.setMessage(mstrModuleName, 2, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class