﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeList))
        Me.pnlMainEmployeeList = New System.Windows.Forms.Panel
        Me.gbColumns = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.pnlFiledSerach = New System.Windows.Forms.Panel
        Me.chkAllChecked = New System.Windows.Forms.CheckBox
        Me.dgvchkEmpfields = New System.Windows.Forms.DataGridView
        Me.objcohchecked = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.cohFiled = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFieldClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.dgEmployeeList = New System.Windows.Forms.DataGridView
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowActiveEmployee = New System.Windows.Forms.CheckBox
        Me.cboEmploymentType = New System.Windows.Forms.ComboBox
        Me.lblEmploymentType = New System.Windows.Forms.Label
        Me.lnkAddRemoveField = New System.Windows.Forms.LinkLabel
        Me.cboEmployeeStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.radTerminated = New System.Windows.Forms.RadioButton
        Me.radProbated = New System.Windows.Forms.RadioButton
        Me.radSuspended = New System.Windows.Forms.RadioButton
        Me.objstLine3 = New eZee.Common.eZeeStraightLine
        Me.dtpDateTo = New System.Windows.Forms.DateTimePicker
        Me.lblDateTo = New System.Windows.Forms.Label
        Me.objstLine2 = New eZee.Common.eZeeStraightLine
        Me.objstLine1 = New eZee.Common.eZeeStraightLine
        Me.cboShiftInfo = New System.Windows.Forms.ComboBox
        Me.lblShift = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker
        Me.lblAppointDate = New System.Windows.Forms.Label
        Me.txtOtherName = New eZee.TextBox.AlphanumericTextBox
        Me.lblOthername = New System.Windows.Forms.Label
        Me.txtEmployeeCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuEnrollCard = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuOrbitRequest = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFlexcubeData = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSubmitForApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployeebiodata = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployeeDependents = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployeeQualifications = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployeeExperiences = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployeeReferences = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployeeSkills = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployeeBenefits = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAssignedCompanyAssets = New System.Windows.Forms.ToolStripMenuItem
        Me.cmnuEmployeeMovement = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuApproverLevel = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuMovementApprover = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuApproveRejectMovments = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTransfers = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRecategorize = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSubstantivePost = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuProbation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuConfirmation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSuspension = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTermination = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReHire = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRetirements = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWorkPermit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuResidentPermit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCostCenter = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExemption = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep0 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuScanDocuments = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuEnrollNewCard = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDeleteEnrolledCard = New System.Windows.Forms.ToolStripMenuItem
        Me.objSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.mnuImport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportEligibleApplicant = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportImages = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportToDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPhotoFile = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportMembership = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportEmpIdentities = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportAddress = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImpPPLSoftData = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportBirthInformation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportRehireEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetMembershipTemplate = New System.Windows.Forms.ToolStripMenuItem
        Me.objSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuEnrollFP = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDeleteFP = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEditImportedEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalVoidMembership = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewDiary = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuReportTo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuList = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetReportingTo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportReportingTo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetFileFormat_ReportTO = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuShift_Policy = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAssign_Shift_Policy = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAssigndayoff = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVAssigned_Shift = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVAssigned_Policy = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportEmpShift = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpdateImpDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpdateEmployeeMovement = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep3 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuAssignSalaryAnniversaryMonth = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep4 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuMissingEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.btnExportList = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objefemailFooter = New eZee.Common.eZeeFooter
        Me.btnMailClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtSurname = New eZee.TextBox.AlphanumericTextBox
        Me.lblSurname = New System.Windows.Forms.Label
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.txtFirstname = New eZee.TextBox.AlphanumericTextBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainEmployeeList.SuspendLayout()
        Me.gbColumns.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnlFiledSerach.SuspendLayout()
        CType(Me.dgvchkEmpfields, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgEmployeeList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuEnrollCard.SuspendLayout()
        Me.objefemailFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainEmployeeList
        '
        Me.pnlMainEmployeeList.Controls.Add(Me.gbColumns)
        Me.pnlMainEmployeeList.Controls.Add(Me.objChkAll)
        Me.pnlMainEmployeeList.Controls.Add(Me.dgEmployeeList)
        Me.pnlMainEmployeeList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainEmployeeList.Controls.Add(Me.objFooter)
        Me.pnlMainEmployeeList.Controls.Add(Me.objefemailFooter)
        Me.pnlMainEmployeeList.Controls.Add(Me.txtSurname)
        Me.pnlMainEmployeeList.Controls.Add(Me.lblSurname)
        Me.pnlMainEmployeeList.Controls.Add(Me.lblFirstName)
        Me.pnlMainEmployeeList.Controls.Add(Me.txtFirstname)
        Me.pnlMainEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainEmployeeList.Name = "pnlMainEmployeeList"
        Me.pnlMainEmployeeList.Size = New System.Drawing.Size(868, 522)
        Me.pnlMainEmployeeList.TabIndex = 0
        '
        'gbColumns
        '
        Me.gbColumns.BorderColor = System.Drawing.Color.Black
        Me.gbColumns.Checked = False
        Me.gbColumns.CollapseAllExceptThis = False
        Me.gbColumns.CollapsedHoverImage = Nothing
        Me.gbColumns.CollapsedNormalImage = Nothing
        Me.gbColumns.CollapsedPressedImage = Nothing
        Me.gbColumns.CollapseOnLoad = False
        Me.gbColumns.Controls.Add(Me.TableLayoutPanel1)
        Me.gbColumns.Controls.Add(Me.btnSave)
        Me.gbColumns.Controls.Add(Me.btnFieldClose)
        Me.gbColumns.ExpandedHoverImage = Nothing
        Me.gbColumns.ExpandedNormalImage = Nothing
        Me.gbColumns.ExpandedPressedImage = Nothing
        Me.gbColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbColumns.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbColumns.HeaderHeight = 25
        Me.gbColumns.HeaderMessage = ""
        Me.gbColumns.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbColumns.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbColumns.HeightOnCollapse = 0
        Me.gbColumns.LeftTextSpace = 0
        Me.gbColumns.Location = New System.Drawing.Point(286, 133)
        Me.gbColumns.Name = "gbColumns"
        Me.gbColumns.OpenHeight = 300
        Me.gbColumns.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbColumns.ShowBorder = True
        Me.gbColumns.ShowCheckBox = False
        Me.gbColumns.ShowCollapseButton = False
        Me.gbColumns.ShowDefaultBorderColor = True
        Me.gbColumns.ShowDownButton = False
        Me.gbColumns.ShowHeader = True
        Me.gbColumns.Size = New System.Drawing.Size(256, 323)
        Me.gbColumns.TabIndex = 19
        Me.gbColumns.Temp = 0
        Me.gbColumns.Text = "Add/Remove Field(s)"
        Me.gbColumns.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbColumns.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.pnlFiledSerach, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtSearch, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(2, 26)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(252, 257)
        Me.TableLayoutPanel1.TabIndex = 83
        '
        'pnlFiledSerach
        '
        Me.pnlFiledSerach.Controls.Add(Me.chkAllChecked)
        Me.pnlFiledSerach.Controls.Add(Me.dgvchkEmpfields)
        Me.pnlFiledSerach.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFiledSerach.Location = New System.Drawing.Point(3, 28)
        Me.pnlFiledSerach.Name = "pnlFiledSerach"
        Me.pnlFiledSerach.Size = New System.Drawing.Size(246, 226)
        Me.pnlFiledSerach.TabIndex = 10
        '
        'chkAllChecked
        '
        Me.chkAllChecked.AutoSize = True
        Me.chkAllChecked.Location = New System.Drawing.Point(6, 4)
        Me.chkAllChecked.Name = "chkAllChecked"
        Me.chkAllChecked.Size = New System.Drawing.Size(15, 14)
        Me.chkAllChecked.TabIndex = 20
        Me.chkAllChecked.UseVisualStyleBackColor = True
        '
        'dgvchkEmpfields
        '
        Me.dgvchkEmpfields.AllowUserToAddRows = False
        Me.dgvchkEmpfields.AllowUserToDeleteRows = False
        Me.dgvchkEmpfields.AllowUserToResizeRows = False
        Me.dgvchkEmpfields.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvchkEmpfields.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvchkEmpfields.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvchkEmpfields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvchkEmpfields.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohchecked, Me.cohFiled})
        Me.dgvchkEmpfields.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvchkEmpfields.Location = New System.Drawing.Point(0, 0)
        Me.dgvchkEmpfields.Name = "dgvchkEmpfields"
        Me.dgvchkEmpfields.RowHeadersVisible = False
        Me.dgvchkEmpfields.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvchkEmpfields.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvchkEmpfields.Size = New System.Drawing.Size(246, 226)
        Me.dgvchkEmpfields.TabIndex = 9
        '
        'objcohchecked
        '
        Me.objcohchecked.Frozen = True
        Me.objcohchecked.HeaderText = ""
        Me.objcohchecked.Name = "objcohchecked"
        Me.objcohchecked.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcohchecked.Width = 25
        '
        'cohFiled
        '
        Me.cohFiled.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cohFiled.HeaderText = ""
        Me.cohFiled.Name = "cohFiled"
        Me.cohFiled.ReadOnly = True
        Me.cohFiled.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(246, 21)
        Me.txtSearch.TabIndex = 8
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(61, 286)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(93, 30)
        Me.btnSave.TabIndex = 81
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnFieldClose
        '
        Me.btnFieldClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFieldClose.BackColor = System.Drawing.Color.White
        Me.btnFieldClose.BackgroundImage = CType(resources.GetObject("btnFieldClose.BackgroundImage"), System.Drawing.Image)
        Me.btnFieldClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFieldClose.BorderColor = System.Drawing.Color.Empty
        Me.btnFieldClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFieldClose.FlatAppearance.BorderSize = 0
        Me.btnFieldClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFieldClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFieldClose.ForeColor = System.Drawing.Color.Black
        Me.btnFieldClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFieldClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnFieldClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFieldClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFieldClose.Location = New System.Drawing.Point(160, 286)
        Me.btnFieldClose.Name = "btnFieldClose"
        Me.btnFieldClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFieldClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFieldClose.Size = New System.Drawing.Size(93, 30)
        Me.btnFieldClose.TabIndex = 77
        Me.btnFieldClose.Text = "Close"
        Me.btnFieldClose.UseVisualStyleBackColor = True
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(19, 223)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 8
        Me.objChkAll.UseVisualStyleBackColor = True
        Me.objChkAll.Visible = False
        '
        'dgEmployeeList
        '
        Me.dgEmployeeList.AllowUserToAddRows = False
        Me.dgEmployeeList.AllowUserToDeleteRows = False
        Me.dgEmployeeList.AllowUserToResizeRows = False
        Me.dgEmployeeList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgEmployeeList.BackgroundColor = System.Drawing.Color.White
        Me.dgEmployeeList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployeeList.Location = New System.Drawing.Point(12, 215)
        Me.dgEmployeeList.MultiSelect = False
        Me.dgEmployeeList.Name = "dgEmployeeList"
        Me.dgEmployeeList.RowHeadersVisible = False
        Me.dgEmployeeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployeeList.Size = New System.Drawing.Size(844, 246)
        Me.dgEmployeeList.TabIndex = 20
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowActiveEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmploymentType)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmploymentType)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAddRemoveField)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployeeStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeStraightLine1)
        Me.gbFilterCriteria.Controls.Add(Me.radTerminated)
        Me.gbFilterCriteria.Controls.Add(Me.radProbated)
        Me.gbFilterCriteria.Controls.Add(Me.radSuspended)
        Me.gbFilterCriteria.Controls.Add(Me.objstLine3)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.objstLine2)
        Me.gbFilterCriteria.Controls.Add(Me.objstLine1)
        Me.gbFilterCriteria.Controls.Add(Me.cboShiftInfo)
        Me.gbFilterCriteria.Controls.Add(Me.lblShift)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblAppointDate)
        Me.gbFilterCriteria.Controls.Add(Me.txtOtherName)
        Me.gbFilterCriteria.Controls.Add(Me.lblOthername)
        Me.gbFilterCriteria.Controls.Add(Me.txtEmployeeCode)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployeeCode)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 100
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(844, 143)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowActiveEmployee
        '
        Me.chkShowActiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowActiveEmployee.Location = New System.Drawing.Point(265, 115)
        Me.chkShowActiveEmployee.Name = "chkShowActiveEmployee"
        Me.chkShowActiveEmployee.Size = New System.Drawing.Size(244, 17)
        Me.chkShowActiveEmployee.TabIndex = 110
        Me.chkShowActiveEmployee.Text = "Show Only Active Employee List"
        Me.chkShowActiveEmployee.UseVisualStyleBackColor = True
        '
        'cboEmploymentType
        '
        Me.cboEmploymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmploymentType.FormattingEnabled = True
        Me.cboEmploymentType.Location = New System.Drawing.Point(364, 87)
        Me.cboEmploymentType.Name = "cboEmploymentType"
        Me.cboEmploymentType.Size = New System.Drawing.Size(145, 21)
        Me.cboEmploymentType.TabIndex = 16
        '
        'lblEmploymentType
        '
        Me.lblEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmploymentType.Location = New System.Drawing.Point(262, 90)
        Me.lblEmploymentType.Name = "lblEmploymentType"
        Me.lblEmploymentType.Size = New System.Drawing.Size(96, 15)
        Me.lblEmploymentType.TabIndex = 15
        Me.lblEmploymentType.Text = "Employment Type"
        Me.lblEmploymentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAddRemoveField
        '
        Me.lnkAddRemoveField.BackColor = System.Drawing.Color.Transparent
        Me.lnkAddRemoveField.Location = New System.Drawing.Point(549, 6)
        Me.lnkAddRemoveField.Name = "lnkAddRemoveField"
        Me.lnkAddRemoveField.Size = New System.Drawing.Size(146, 13)
        Me.lnkAddRemoveField.TabIndex = 104
        Me.lnkAddRemoveField.TabStop = True
        Me.lnkAddRemoveField.Text = "Add/Remove Field(s)"
        '
        'cboEmployeeStatus
        '
        Me.cboEmployeeStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployeeStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployeeStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeStatus.FormattingEnabled = True
        Me.cboEmployeeStatus.Location = New System.Drawing.Point(594, 97)
        Me.cboEmployeeStatus.Name = "cboEmployeeStatus"
        Me.cboEmployeeStatus.Size = New System.Drawing.Size(242, 21)
        Me.cboEmployeeStatus.TabIndex = 108
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(527, 100)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(62, 15)
        Me.lblStatus.TabIndex = 107
        Me.lblStatus.Text = "Status"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(488, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 105
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(93, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(389, 21)
        Me.cboEmployee.TabIndex = 104
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 35)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(79, 15)
        Me.lblEmployee.TabIndex = 103
        Me.lblEmployee.Text = "Employee"
        '
        'cboBranch
        '
        Me.cboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(594, 32)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(242, 21)
        Me.cboBranch.TabIndex = 101
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(527, 36)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(62, 15)
        Me.lblBranch.TabIndex = 100
        Me.lblBranch.Text = "Branch"
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(522, 59)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(321, 5)
        Me.EZeeStraightLine1.TabIndex = 99
        Me.EZeeStraightLine1.Text = "EZeeStraightLine3"
        '
        'radTerminated
        '
        Me.radTerminated.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radTerminated.Location = New System.Drawing.Point(530, 121)
        Me.radTerminated.Name = "radTerminated"
        Me.radTerminated.Size = New System.Drawing.Size(90, 16)
        Me.radTerminated.TabIndex = 26
        Me.radTerminated.Text = "Terminated"
        Me.radTerminated.UseVisualStyleBackColor = True
        '
        'radProbated
        '
        Me.radProbated.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radProbated.Location = New System.Drawing.Point(733, 121)
        Me.radProbated.Name = "radProbated"
        Me.radProbated.Size = New System.Drawing.Size(100, 16)
        Me.radProbated.TabIndex = 25
        Me.radProbated.Text = "Probated"
        Me.radProbated.UseVisualStyleBackColor = True
        '
        'radSuspended
        '
        Me.radSuspended.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSuspended.Location = New System.Drawing.Point(629, 121)
        Me.radSuspended.Name = "radSuspended"
        Me.radSuspended.Size = New System.Drawing.Size(98, 16)
        Me.radSuspended.TabIndex = 24
        Me.radSuspended.Text = "Suspended"
        Me.radSuspended.UseVisualStyleBackColor = True
        '
        'objstLine3
        '
        Me.objstLine3.BackColor = System.Drawing.Color.Transparent
        Me.objstLine3.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objstLine3.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objstLine3.Location = New System.Drawing.Point(522, 90)
        Me.objstLine3.Name = "objstLine3"
        Me.objstLine3.Size = New System.Drawing.Size(321, 5)
        Me.objstLine3.TabIndex = 22
        Me.objstLine3.Text = "EZeeStraightLine3"
        '
        'dtpDateTo
        '
        Me.dtpDateTo.Checked = False
        Me.dtpDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateTo.Location = New System.Drawing.Point(731, 67)
        Me.dtpDateTo.Name = "dtpDateTo"
        Me.dtpDateTo.ShowCheckBox = True
        Me.dtpDateTo.Size = New System.Drawing.Size(105, 21)
        Me.dtpDateTo.TabIndex = 21
        '
        'lblDateTo
        '
        Me.lblDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTo.Location = New System.Drawing.Point(704, 70)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(25, 15)
        Me.lblDateTo.TabIndex = 20
        Me.lblDateTo.Text = "To"
        Me.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objstLine2
        '
        Me.objstLine2.BackColor = System.Drawing.Color.Transparent
        Me.objstLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objstLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine2.Location = New System.Drawing.Point(515, 24)
        Me.objstLine2.Name = "objstLine2"
        Me.objstLine2.Size = New System.Drawing.Size(12, 118)
        Me.objstLine2.TabIndex = 17
        Me.objstLine2.Text = "EZeeStraightLine1"
        '
        'objstLine1
        '
        Me.objstLine1.BackColor = System.Drawing.Color.Transparent
        Me.objstLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objstLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine1.Location = New System.Drawing.Point(244, 59)
        Me.objstLine1.Name = "objstLine1"
        Me.objstLine1.Size = New System.Drawing.Size(12, 84)
        Me.objstLine1.TabIndex = 8
        '
        'cboShiftInfo
        '
        Me.cboShiftInfo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShiftInfo.FormattingEnabled = True
        Me.cboShiftInfo.Location = New System.Drawing.Point(364, 87)
        Me.cboShiftInfo.Name = "cboShiftInfo"
        Me.cboShiftInfo.Size = New System.Drawing.Size(145, 21)
        Me.cboShiftInfo.TabIndex = 14
        Me.cboShiftInfo.Visible = False
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(262, 89)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(96, 15)
        Me.lblShift.TabIndex = 13
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblShift.Visible = False
        '
        'objbtnReset
        '
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(818, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 95
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(794, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 94
        Me.objbtnSearch.TabStop = False
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(364, 60)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(145, 21)
        Me.cboJob.TabIndex = 12
        '
        'cboDepartment
        '
        Me.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(93, 60)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(145, 21)
        Me.cboDepartment.TabIndex = 10
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(262, 63)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(96, 15)
        Me.lblJob.TabIndex = 11
        Me.lblJob.Text = "Job"
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 63)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(79, 15)
        Me.lblDepartment.TabIndex = 9
        Me.lblDepartment.Text = "Department"
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.Checked = False
        Me.dtpDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateFrom.Location = New System.Drawing.Point(594, 67)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.ShowCheckBox = True
        Me.dtpDateFrom.Size = New System.Drawing.Size(105, 21)
        Me.dtpDateFrom.TabIndex = 19
        '
        'lblAppointDate
        '
        Me.lblAppointDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointDate.Location = New System.Drawing.Point(527, 70)
        Me.lblAppointDate.Name = "lblAppointDate"
        Me.lblAppointDate.Size = New System.Drawing.Size(61, 15)
        Me.lblAppointDate.TabIndex = 18
        Me.lblAppointDate.Text = "Date From"
        Me.lblAppointDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherName
        '
        Me.txtOtherName.Flags = 0
        Me.txtOtherName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherName.Location = New System.Drawing.Point(93, 87)
        Me.txtOtherName.Name = "txtOtherName"
        Me.txtOtherName.Size = New System.Drawing.Size(145, 21)
        Me.txtOtherName.TabIndex = 5
        '
        'lblOthername
        '
        Me.lblOthername.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOthername.Location = New System.Drawing.Point(8, 90)
        Me.lblOthername.Name = "lblOthername"
        Me.lblOthername.Size = New System.Drawing.Size(79, 15)
        Me.lblOthername.TabIndex = 4
        Me.lblOthername.Text = "Other Name"
        Me.lblOthername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeeCode
        '
        Me.txtEmployeeCode.Flags = 0
        Me.txtEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeCode.Location = New System.Drawing.Point(93, 114)
        Me.txtEmployeeCode.Name = "txtEmployeeCode"
        Me.txtEmployeeCode.Size = New System.Drawing.Size(145, 21)
        Me.txtEmployeeCode.TabIndex = 7
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(8, 117)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(79, 15)
        Me.lblEmployeeCode.TabIndex = 6
        Me.lblEmployeeCode.Text = "Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(705, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 103
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnExportList)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 412)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(868, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(103, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuEnrollCard
        Me.btnOperations.TabIndex = 86
        Me.btnOperations.Text = "&Operations"
        '
        'cmnuEnrollCard
        '
        Me.cmnuEnrollCard.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.cmnuEnrollCard.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOrbitRequest, Me.mnuFlexcubeData, Me.mnuSubmitForApproval, Me.mnuEmployeebiodata, Me.cmnuEmployeeMovement, Me.objSep0, Me.mnuScanDocuments, Me.mnuPreview, Me.ToolStripSeparator1, Me.mnuEnrollNewCard, Me.mnuDeleteEnrolledCard, Me.objSeparator, Me.mnuImport, Me.mnuGetMembershipTemplate, Me.objSeparator1, Me.mnuEnrollFP, Me.mnuDeleteFP, Me.mnuEditImportedEmployee, Me.mnuGlobalVoidMembership, Me.mnuViewDiary, Me.objSep1, Me.mnuReportTo, Me.objSep2, Me.mnuShift_Policy, Me.mnuUpdateImpDetails, Me.mnuUpdateEmployeeMovement, Me.objSep3, Me.mnuAssignSalaryAnniversaryMonth, Me.objSep4, Me.mnuMissingEmployee})
        Me.cmnuEnrollCard.Name = "cmnuEnrollCard"
        Me.cmnuEnrollCard.Size = New System.Drawing.Size(248, 536)
        '
        'mnuOrbitRequest
        '
        Me.mnuOrbitRequest.Name = "mnuOrbitRequest"
        Me.mnuOrbitRequest.Size = New System.Drawing.Size(247, 22)
        Me.mnuOrbitRequest.Tag = "mnuOrbitRequest"
        Me.mnuOrbitRequest.Text = "Orbit Request"
        '
        'mnuFlexcubeData
        '
        Me.mnuFlexcubeData.Name = "mnuFlexcubeData"
        Me.mnuFlexcubeData.Size = New System.Drawing.Size(247, 22)
        Me.mnuFlexcubeData.Tag = "mnuFlexcubeData"
        Me.mnuFlexcubeData.Text = "Flexcube Data"
        '
        'mnuSubmitForApproval
        '
        Me.mnuSubmitForApproval.Name = "mnuSubmitForApproval"
        Me.mnuSubmitForApproval.Size = New System.Drawing.Size(247, 22)
        Me.mnuSubmitForApproval.Text = "Submit For Approval"
        '
        'mnuEmployeebiodata
        '
        Me.mnuEmployeebiodata.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEmployeeDependents, Me.mnuEmployeeQualifications, Me.mnuEmployeeExperiences, Me.mnuEmployeeReferences, Me.mnuEmployeeSkills, Me.mnuEmployeeBenefits, Me.mnuAssignedCompanyAssets})
        Me.mnuEmployeebiodata.Name = "mnuEmployeebiodata"
        Me.mnuEmployeebiodata.Size = New System.Drawing.Size(247, 22)
        Me.mnuEmployeebiodata.Text = "Employee &Biodata"
        '
        'mnuEmployeeDependents
        '
        Me.mnuEmployeeDependents.Name = "mnuEmployeeDependents"
        Me.mnuEmployeeDependents.Size = New System.Drawing.Size(213, 22)
        Me.mnuEmployeeDependents.Text = "Employee &Dependants"
        '
        'mnuEmployeeQualifications
        '
        Me.mnuEmployeeQualifications.Name = "mnuEmployeeQualifications"
        Me.mnuEmployeeQualifications.Size = New System.Drawing.Size(213, 22)
        Me.mnuEmployeeQualifications.Text = "Employee &Qualifications"
        '
        'mnuEmployeeExperiences
        '
        Me.mnuEmployeeExperiences.Name = "mnuEmployeeExperiences"
        Me.mnuEmployeeExperiences.Size = New System.Drawing.Size(213, 22)
        Me.mnuEmployeeExperiences.Text = "Employee &Experiences"
        '
        'mnuEmployeeReferences
        '
        Me.mnuEmployeeReferences.Name = "mnuEmployeeReferences"
        Me.mnuEmployeeReferences.Size = New System.Drawing.Size(213, 22)
        Me.mnuEmployeeReferences.Text = "Employee &References"
        '
        'mnuEmployeeSkills
        '
        Me.mnuEmployeeSkills.Name = "mnuEmployeeSkills"
        Me.mnuEmployeeSkills.Size = New System.Drawing.Size(213, 22)
        Me.mnuEmployeeSkills.Text = "Employee S&kills"
        '
        'mnuEmployeeBenefits
        '
        Me.mnuEmployeeBenefits.Name = "mnuEmployeeBenefits"
        Me.mnuEmployeeBenefits.Size = New System.Drawing.Size(213, 22)
        Me.mnuEmployeeBenefits.Text = "Employee Be&nefits"
        '
        'mnuAssignedCompanyAssets
        '
        Me.mnuAssignedCompanyAssets.Name = "mnuAssignedCompanyAssets"
        Me.mnuAssignedCompanyAssets.Size = New System.Drawing.Size(213, 22)
        Me.mnuAssignedCompanyAssets.Text = "&Assigned Company Assets"
        '
        'cmnuEmployeeMovement
        '
        Me.cmnuEmployeeMovement.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuApproverLevel, Me.mnuMovementApprover, Me.mnuApproveRejectMovments, Me.mnuTransfers, Me.mnuRecategorize, Me.mnuSubstantivePost, Me.mnuProbation, Me.mnuConfirmation, Me.mnuSuspension, Me.mnuTermination, Me.mnuReHire, Me.mnuRetirements, Me.mnuWorkPermit, Me.mnuResidentPermit, Me.mnuCostCenter, Me.mnuExemption})
        Me.cmnuEmployeeMovement.Name = "cmnuEmployeeMovement"
        Me.cmnuEmployeeMovement.Size = New System.Drawing.Size(247, 22)
        Me.cmnuEmployeeMovement.Tag = "cmnuEmployeeMovement"
        Me.cmnuEmployeeMovement.Text = "Employee Movement"
        '
        'mnuApproverLevel
        '
        Me.mnuApproverLevel.Name = "mnuApproverLevel"
        Me.mnuApproverLevel.Size = New System.Drawing.Size(269, 22)
        Me.mnuApproverLevel.Tag = "mnuApproverLevel"
        Me.mnuApproverLevel.Text = "Approver Level"
        '
        'mnuMovementApprover
        '
        Me.mnuMovementApprover.Name = "mnuMovementApprover"
        Me.mnuMovementApprover.Size = New System.Drawing.Size(269, 22)
        Me.mnuMovementApprover.Tag = "mnuMovementApprover"
        Me.mnuMovementApprover.Text = "Add/Edit Approver "
        '
        'mnuApproveRejectMovments
        '
        Me.mnuApproveRejectMovments.Name = "mnuApproveRejectMovments"
        Me.mnuApproveRejectMovments.Size = New System.Drawing.Size(269, 22)
        Me.mnuApproveRejectMovments.Tag = "mnuApproveRejectMovments"
        Me.mnuApproveRejectMovments.Text = "Approve/Reject Pending Movements"
        '
        'mnuTransfers
        '
        Me.mnuTransfers.Name = "mnuTransfers"
        Me.mnuTransfers.Size = New System.Drawing.Size(269, 22)
        Me.mnuTransfers.Tag = "mnuTransfers"
        Me.mnuTransfers.Text = "Transfers"
        '
        'mnuRecategorize
        '
        Me.mnuRecategorize.Name = "mnuRecategorize"
        Me.mnuRecategorize.Size = New System.Drawing.Size(269, 22)
        Me.mnuRecategorize.Tag = "mnuRecategorize"
        Me.mnuRecategorize.Text = "Recategorize"
        '
        'mnuSubstantivePost
        '
        Me.mnuSubstantivePost.Name = "mnuSubstantivePost"
        Me.mnuSubstantivePost.Size = New System.Drawing.Size(269, 22)
        Me.mnuSubstantivePost.Text = "Substantive Post"
        '
        'mnuProbation
        '
        Me.mnuProbation.Name = "mnuProbation"
        Me.mnuProbation.Size = New System.Drawing.Size(269, 22)
        Me.mnuProbation.Tag = "mnuProbation"
        Me.mnuProbation.Text = "Probation"
        '
        'mnuConfirmation
        '
        Me.mnuConfirmation.Name = "mnuConfirmation"
        Me.mnuConfirmation.Size = New System.Drawing.Size(269, 22)
        Me.mnuConfirmation.Tag = "mnuConfirmation"
        Me.mnuConfirmation.Text = "Confirmation"
        '
        'mnuSuspension
        '
        Me.mnuSuspension.Name = "mnuSuspension"
        Me.mnuSuspension.Size = New System.Drawing.Size(269, 22)
        Me.mnuSuspension.Tag = "mnuSuspension"
        Me.mnuSuspension.Text = "Suspension"
        '
        'mnuTermination
        '
        Me.mnuTermination.Name = "mnuTermination"
        Me.mnuTermination.Size = New System.Drawing.Size(269, 22)
        Me.mnuTermination.Tag = "mnuTermination"
        Me.mnuTermination.Text = "Termination"
        '
        'mnuReHire
        '
        Me.mnuReHire.Name = "mnuReHire"
        Me.mnuReHire.Size = New System.Drawing.Size(269, 22)
        Me.mnuReHire.Tag = "mnuReHire"
        Me.mnuReHire.Text = "Re-Hire"
        '
        'mnuRetirements
        '
        Me.mnuRetirements.Name = "mnuRetirements"
        Me.mnuRetirements.Size = New System.Drawing.Size(269, 22)
        Me.mnuRetirements.Tag = "mnuRetirements"
        Me.mnuRetirements.Text = "Retirements"
        '
        'mnuWorkPermit
        '
        Me.mnuWorkPermit.Name = "mnuWorkPermit"
        Me.mnuWorkPermit.Size = New System.Drawing.Size(269, 22)
        Me.mnuWorkPermit.Tag = "mnuWorkPermit"
        Me.mnuWorkPermit.Text = "Work Permit"
        '
        'mnuResidentPermit
        '
        Me.mnuResidentPermit.Name = "mnuResidentPermit"
        Me.mnuResidentPermit.Size = New System.Drawing.Size(269, 22)
        Me.mnuResidentPermit.Tag = "mnuResidentPermit"
        Me.mnuResidentPermit.Text = "Resident Permit"
        '
        'mnuCostCenter
        '
        Me.mnuCostCenter.Name = "mnuCostCenter"
        Me.mnuCostCenter.Size = New System.Drawing.Size(269, 22)
        Me.mnuCostCenter.Tag = "mnuCostCenter"
        Me.mnuCostCenter.Text = "Cost Center"
        '
        'mnuExemption
        '
        Me.mnuExemption.Name = "mnuExemption"
        Me.mnuExemption.Size = New System.Drawing.Size(269, 22)
        Me.mnuExemption.Tag = "mnuExemption"
        Me.mnuExemption.Text = "Exemption"
        '
        'objSep0
        '
        Me.objSep0.Name = "objSep0"
        Me.objSep0.Size = New System.Drawing.Size(244, 6)
        '
        'mnuScanDocuments
        '
        Me.mnuScanDocuments.Name = "mnuScanDocuments"
        Me.mnuScanDocuments.Size = New System.Drawing.Size(247, 22)
        Me.mnuScanDocuments.Text = "Browse"
        '
        'mnuPreview
        '
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(247, 22)
        Me.mnuPreview.Text = "Preview"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(244, 6)
        '
        'mnuEnrollNewCard
        '
        Me.mnuEnrollNewCard.Name = "mnuEnrollNewCard"
        Me.mnuEnrollNewCard.Size = New System.Drawing.Size(247, 22)
        Me.mnuEnrollNewCard.Text = "Enroll &New Card"
        Me.mnuEnrollNewCard.Visible = False
        '
        'mnuDeleteEnrolledCard
        '
        Me.mnuDeleteEnrolledCard.Name = "mnuDeleteEnrolledCard"
        Me.mnuDeleteEnrolledCard.Size = New System.Drawing.Size(247, 22)
        Me.mnuDeleteEnrolledCard.Text = "&Delete Enrolled Card"
        Me.mnuDeleteEnrolledCard.Visible = False
        '
        'objSeparator
        '
        Me.objSeparator.Name = "objSeparator"
        Me.objSeparator.Size = New System.Drawing.Size(244, 6)
        Me.objSeparator.Visible = False
        '
        'mnuImport
        '
        Me.mnuImport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportEligibleApplicant, Me.mnuImportEmployee, Me.mnuImportImages, Me.mnuImportMembership, Me.mnuImportEmpIdentities, Me.mnuImportAddress, Me.mnuImpPPLSoftData, Me.mnuImportBirthInformation, Me.mnuImportRehireEmployee})
        Me.mnuImport.Name = "mnuImport"
        Me.mnuImport.Size = New System.Drawing.Size(247, 22)
        Me.mnuImport.Text = "Import"
        '
        'mnuImportEligibleApplicant
        '
        Me.mnuImportEligibleApplicant.Name = "mnuImportEligibleApplicant"
        Me.mnuImportEligibleApplicant.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportEligibleApplicant.Text = "&Import Eligible Applicant"
        '
        'mnuImportEmployee
        '
        Me.mnuImportEmployee.Name = "mnuImportEmployee"
        Me.mnuImportEmployee.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportEmployee.Text = "Im&port Employee"
        '
        'mnuImportImages
        '
        Me.mnuImportImages.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportToDatabase, Me.mnuPhotoFile})
        Me.mnuImportImages.Name = "mnuImportImages"
        Me.mnuImportImages.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportImages.Text = "Import Images"
        '
        'mnuImportToDatabase
        '
        Me.mnuImportToDatabase.Name = "mnuImportToDatabase"
        Me.mnuImportToDatabase.Size = New System.Drawing.Size(175, 22)
        Me.mnuImportToDatabase.Text = "Import to &Database"
        '
        'mnuPhotoFile
        '
        Me.mnuPhotoFile.Name = "mnuPhotoFile"
        Me.mnuPhotoFile.Size = New System.Drawing.Size(175, 22)
        Me.mnuPhotoFile.Text = "Get File Format"
        Me.mnuPhotoFile.Visible = False
        '
        'mnuImportMembership
        '
        Me.mnuImportMembership.Name = "mnuImportMembership"
        Me.mnuImportMembership.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportMembership.Text = "Import Memberships"
        '
        'mnuImportEmpIdentities
        '
        Me.mnuImportEmpIdentities.Name = "mnuImportEmpIdentities"
        Me.mnuImportEmpIdentities.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportEmpIdentities.Text = "Import Indentity"
        '
        'mnuImportAddress
        '
        Me.mnuImportAddress.Name = "mnuImportAddress"
        Me.mnuImportAddress.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportAddress.Text = "Import Address"
        '
        'mnuImpPPLSoftData
        '
        Me.mnuImpPPLSoftData.Name = "mnuImpPPLSoftData"
        Me.mnuImpPPLSoftData.Size = New System.Drawing.Size(205, 22)
        Me.mnuImpPPLSoftData.Text = "Import Peoplesoft Data"
        '
        'mnuImportBirthInformation
        '
        Me.mnuImportBirthInformation.Name = "mnuImportBirthInformation"
        Me.mnuImportBirthInformation.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportBirthInformation.Text = "Import Birth Information"
        '
        'mnuImportRehireEmployee
        '
        Me.mnuImportRehireEmployee.Name = "mnuImportRehireEmployee"
        Me.mnuImportRehireEmployee.Size = New System.Drawing.Size(205, 22)
        Me.mnuImportRehireEmployee.Text = "Import Rehire Employee"
        '
        'mnuGetMembershipTemplate
        '
        Me.mnuGetMembershipTemplate.Name = "mnuGetMembershipTemplate"
        Me.mnuGetMembershipTemplate.Size = New System.Drawing.Size(247, 22)
        Me.mnuGetMembershipTemplate.Text = "Export Membership"
        '
        'objSeparator1
        '
        Me.objSeparator1.Name = "objSeparator1"
        Me.objSeparator1.Size = New System.Drawing.Size(244, 6)
        '
        'mnuEnrollFP
        '
        Me.mnuEnrollFP.Name = "mnuEnrollFP"
        Me.mnuEnrollFP.Size = New System.Drawing.Size(247, 22)
        Me.mnuEnrollFP.Text = "Enroll &Finger Print"
        '
        'mnuDeleteFP
        '
        Me.mnuDeleteFP.Name = "mnuDeleteFP"
        Me.mnuDeleteFP.Size = New System.Drawing.Size(247, 22)
        Me.mnuDeleteFP.Text = "De&lete Finger Print"
        '
        'mnuEditImportedEmployee
        '
        Me.mnuEditImportedEmployee.Name = "mnuEditImportedEmployee"
        Me.mnuEditImportedEmployee.Size = New System.Drawing.Size(247, 22)
        Me.mnuEditImportedEmployee.Text = "Edit Imported Employee"
        '
        'mnuGlobalVoidMembership
        '
        Me.mnuGlobalVoidMembership.Name = "mnuGlobalVoidMembership"
        Me.mnuGlobalVoidMembership.Size = New System.Drawing.Size(247, 22)
        Me.mnuGlobalVoidMembership.Text = "Global Void Membership"
        '
        'mnuViewDiary
        '
        Me.mnuViewDiary.Name = "mnuViewDiary"
        Me.mnuViewDiary.Size = New System.Drawing.Size(247, 22)
        Me.mnuViewDiary.Text = "&View Diary"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(244, 6)
        '
        'mnuReportTo
        '
        Me.mnuReportTo.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuList, Me.mnuSetReportingTo, Me.mnuImportReportingTo, Me.mnuGetFileFormat_ReportTO})
        Me.mnuReportTo.Name = "mnuReportTo"
        Me.mnuReportTo.Size = New System.Drawing.Size(247, 22)
        Me.mnuReportTo.Text = "Employee Reporting"
        '
        'mnuList
        '
        Me.mnuList.Name = "mnuList"
        Me.mnuList.Size = New System.Drawing.Size(223, 22)
        Me.mnuList.Tag = "mnuList"
        Me.mnuList.Text = "List Unreported Employee(s)"
        '
        'mnuSetReportingTo
        '
        Me.mnuSetReportingTo.Name = "mnuSetReportingTo"
        Me.mnuSetReportingTo.Size = New System.Drawing.Size(223, 22)
        Me.mnuSetReportingTo.Tag = "mnuSetReportingTo"
        Me.mnuSetReportingTo.Text = "Set Reporting To"
        '
        'mnuImportReportingTo
        '
        Me.mnuImportReportingTo.Name = "mnuImportReportingTo"
        Me.mnuImportReportingTo.Size = New System.Drawing.Size(223, 22)
        Me.mnuImportReportingTo.Tag = "mnuImportReportingTo"
        Me.mnuImportReportingTo.Text = "Import Reporting To"
        '
        'mnuGetFileFormat_ReportTO
        '
        Me.mnuGetFileFormat_ReportTO.Name = "mnuGetFileFormat_ReportTO"
        Me.mnuGetFileFormat_ReportTO.Size = New System.Drawing.Size(223, 22)
        Me.mnuGetFileFormat_ReportTO.Tag = "mnuGetFileFormat_ReportTO"
        Me.mnuGetFileFormat_ReportTO.Text = "Get File Format"
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(244, 6)
        Me.objSep2.Tag = "objSep2"
        '
        'mnuShift_Policy
        '
        Me.mnuShift_Policy.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAssign_Shift_Policy, Me.mnuAssigndayoff, Me.mnuVAssigned_Shift, Me.mnuVAssigned_Policy, Me.mnuImportEmpShift})
        Me.mnuShift_Policy.Name = "mnuShift_Policy"
        Me.mnuShift_Policy.Size = New System.Drawing.Size(247, 22)
        Me.mnuShift_Policy.Tag = "mnuShift_Policy"
        Me.mnuShift_Policy.Text = "Shift/Policy"
        '
        'mnuAssign_Shift_Policy
        '
        Me.mnuAssign_Shift_Policy.Name = "mnuAssign_Shift_Policy"
        Me.mnuAssign_Shift_Policy.Size = New System.Drawing.Size(192, 22)
        Me.mnuAssign_Shift_Policy.Tag = "mnuAssign_Shift_Policy"
        Me.mnuAssign_Shift_Policy.Text = "Assign Shift/Policy"
        '
        'mnuAssigndayoff
        '
        Me.mnuAssigndayoff.Name = "mnuAssigndayoff"
        Me.mnuAssigndayoff.Size = New System.Drawing.Size(192, 22)
        Me.mnuAssigndayoff.Text = "Assign Day Off"
        '
        'mnuVAssigned_Shift
        '
        Me.mnuVAssigned_Shift.Name = "mnuVAssigned_Shift"
        Me.mnuVAssigned_Shift.Size = New System.Drawing.Size(192, 22)
        Me.mnuVAssigned_Shift.Tag = "mnuVAssigned_Shift"
        Me.mnuVAssigned_Shift.Text = "View Assigned Shift"
        '
        'mnuVAssigned_Policy
        '
        Me.mnuVAssigned_Policy.Name = "mnuVAssigned_Policy"
        Me.mnuVAssigned_Policy.Size = New System.Drawing.Size(192, 22)
        Me.mnuVAssigned_Policy.Tag = "mnuVAssigned_Policy"
        Me.mnuVAssigned_Policy.Text = "View Assigned Policy"
        '
        'mnuImportEmpShift
        '
        Me.mnuImportEmpShift.Name = "mnuImportEmpShift"
        Me.mnuImportEmpShift.Size = New System.Drawing.Size(192, 22)
        Me.mnuImportEmpShift.Text = "Import Employee Shift"
        '
        'mnuUpdateImpDetails
        '
        Me.mnuUpdateImpDetails.Name = "mnuUpdateImpDetails"
        Me.mnuUpdateImpDetails.Size = New System.Drawing.Size(247, 22)
        Me.mnuUpdateImpDetails.Tag = "mnuUpdateImpDetails"
        Me.mnuUpdateImpDetails.Text = "&Update Employee Details"
        '
        'mnuUpdateEmployeeMovement
        '
        Me.mnuUpdateEmployeeMovement.Name = "mnuUpdateEmployeeMovement"
        Me.mnuUpdateEmployeeMovement.Size = New System.Drawing.Size(247, 22)
        Me.mnuUpdateEmployeeMovement.Tag = "mnuUpdateEmployeeMovement"
        Me.mnuUpdateEmployeeMovement.Text = "Update Employee &Movements"
        '
        'objSep3
        '
        Me.objSep3.Name = "objSep3"
        Me.objSep3.Size = New System.Drawing.Size(244, 6)
        '
        'mnuAssignSalaryAnniversaryMonth
        '
        Me.mnuAssignSalaryAnniversaryMonth.Name = "mnuAssignSalaryAnniversaryMonth"
        Me.mnuAssignSalaryAnniversaryMonth.Size = New System.Drawing.Size(247, 22)
        Me.mnuAssignSalaryAnniversaryMonth.Text = "Assign Salary Anniversary Month"
        '
        'objSep4
        '
        Me.objSep4.Name = "objSep4"
        Me.objSep4.Size = New System.Drawing.Size(244, 6)
        Me.objSep4.Tag = "objSep4"
        '
        'mnuMissingEmployee
        '
        Me.mnuMissingEmployee.Name = "mnuMissingEmployee"
        Me.mnuMissingEmployee.Size = New System.Drawing.Size(247, 22)
        Me.mnuMissingEmployee.Tag = "mnuMissingEmployee"
        Me.mnuMissingEmployee.Text = "Find Missing Employee"
        '
        'btnExportList
        '
        Me.btnExportList.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExportList.BackColor = System.Drawing.Color.White
        Me.btnExportList.BackgroundImage = CType(resources.GetObject("btnExportList.BackgroundImage"), System.Drawing.Image)
        Me.btnExportList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExportList.BorderColor = System.Drawing.Color.Empty
        Me.btnExportList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExportList.FlatAppearance.BorderSize = 0
        Me.btnExportList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExportList.ForeColor = System.Drawing.Color.Black
        Me.btnExportList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExportList.GradientForeColor = System.Drawing.Color.Black
        Me.btnExportList.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExportList.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExportList.Location = New System.Drawing.Point(121, 13)
        Me.btnExportList.Name = "btnExportList"
        Me.btnExportList.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExportList.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExportList.Size = New System.Drawing.Size(94, 30)
        Me.btnExportList.TabIndex = 85
        Me.btnExportList.Text = "E&xport List"
        Me.btnExportList.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(663, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(94, 30)
        Me.btnDelete.TabIndex = 84
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(563, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(94, 30)
        Me.btnEdit.TabIndex = 83
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(463, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(94, 30)
        Me.btnNew.TabIndex = 82
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(763, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 81
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objefemailFooter
        '
        Me.objefemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefemailFooter.Controls.Add(Me.btnMailClose)
        Me.objefemailFooter.Controls.Add(Me.btnOk)
        Me.objefemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.Location = New System.Drawing.Point(0, 467)
        Me.objefemailFooter.Name = "objefemailFooter"
        Me.objefemailFooter.Size = New System.Drawing.Size(868, 55)
        Me.objefemailFooter.TabIndex = 5
        '
        'btnMailClose
        '
        Me.btnMailClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMailClose.BackColor = System.Drawing.Color.White
        Me.btnMailClose.BackgroundImage = CType(resources.GetObject("btnMailClose.BackgroundImage"), System.Drawing.Image)
        Me.btnMailClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMailClose.BorderColor = System.Drawing.Color.Empty
        Me.btnMailClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMailClose.FlatAppearance.BorderSize = 0
        Me.btnMailClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMailClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMailClose.ForeColor = System.Drawing.Color.Black
        Me.btnMailClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMailClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnMailClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMailClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMailClose.Location = New System.Drawing.Point(762, 12)
        Me.btnMailClose.Name = "btnMailClose"
        Me.btnMailClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMailClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMailClose.Size = New System.Drawing.Size(94, 30)
        Me.btnMailClose.TabIndex = 85
        Me.btnMailClose.Text = "&Close"
        Me.btnMailClose.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(663, 12)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(94, 30)
        Me.btnOk.TabIndex = 84
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'txtSurname
        '
        Me.txtSurname.Flags = 0
        Me.txtSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSurname.Location = New System.Drawing.Point(105, 128)
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.Size = New System.Drawing.Size(145, 21)
        Me.txtSurname.TabIndex = 1
        '
        'lblSurname
        '
        Me.lblSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurname.Location = New System.Drawing.Point(20, 131)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(79, 15)
        Me.lblSurname.TabIndex = 0
        Me.lblSurname.Text = "Surname"
        Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFirstName
        '
        Me.lblFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(20, 158)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(79, 15)
        Me.lblFirstName.TabIndex = 2
        Me.lblFirstName.Text = "First Name"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFirstname
        '
        Me.txtFirstname.Flags = 0
        Me.txtFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFirstname.Location = New System.Drawing.Point(105, 155)
        Me.txtFirstname.Name = "txtFirstname"
        Me.txtFirstname.Size = New System.Drawing.Size(145, 21)
        Me.txtFirstname.TabIndex = 3
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(868, 58)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Employee List"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmEmployeeList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(868, 522)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlMainEmployeeList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee List"
        Me.pnlMainEmployeeList.ResumeLayout(False)
        Me.pnlMainEmployeeList.PerformLayout()
        Me.gbColumns.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.pnlFiledSerach.ResumeLayout(False)
        Me.pnlFiledSerach.PerformLayout()
        CType(Me.dgvchkEmpfields, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgEmployeeList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuEnrollCard.ResumeLayout(False)
        Me.objefemailFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtEmployeeCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents txtOtherName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFirstname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSurname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOthername As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents lblSurname As System.Windows.Forms.Label
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAppointDate As System.Windows.Forms.Label
    Friend WithEvents cboEmploymentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmploymentType As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboShiftInfo As System.Windows.Forms.ComboBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents objstLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents dtpDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents objstLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objstLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents radProbated As System.Windows.Forms.RadioButton
    Friend WithEvents radSuspended As System.Windows.Forms.RadioButton
    Friend WithEvents radTerminated As System.Windows.Forms.RadioButton
    Friend WithEvents btnExportList As eZee.Common.eZeeLightButton
    Friend WithEvents cmnuEnrollCard As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuEnrollNewCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDeleteEnrolledCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuScanDocuments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objefemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnMailClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents objSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuEnrollFP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDeleteFP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditImportedEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGetMembershipTemplate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents mnuViewDiary As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents cboEmployeeStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents mnuEmployeebiodata As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployeeDependents As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployeeQualifications As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployeeExperiences As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployeeReferences As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployeeSkills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployeeBenefits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssignedCompanyAssets As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep0 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents gbColumns As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnFieldClose As eZee.Common.eZeeLightButton
    Friend WithEvents lnkAddRemoveField As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuReportTo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetReportingTo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportReportingTo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGetFileFormat_ReportTO As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuShift_Policy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssign_Shift_Policy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuVAssigned_Shift As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuVAssigned_Policy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents mnuAssigndayoff As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpdateImpDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmnuEmployeeMovement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTransfers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecategorize As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkShowActiveEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents mnuProbation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuConfirmation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSuspension As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTermination As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReHire As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRetirements As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWorkPermit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuCostCenter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlFiledSerach As System.Windows.Forms.Panel
    Friend WithEvents chkAllChecked As System.Windows.Forms.CheckBox
    Friend WithEvents dgvchkEmpfields As System.Windows.Forms.DataGridView
    Friend WithEvents objcohchecked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cohFiled As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgEmployeeList As System.Windows.Forms.DataGridView
    Friend WithEvents mnuUpdateEmployeeMovement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalVoidMembership As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssignSalaryAnniversaryMonth As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportEligibleApplicant As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportImages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportMembership As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportEmpIdentities As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportAddress As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportToDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPhotoFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImpPPLSoftData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportBirthInformation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportEmpShift As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuMissingEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuResidentPermit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSubmitForApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuApproverLevel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMovementApprover As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuApproveRejectMovments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFlexcubeData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExemption As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOrbitRequest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportRehireEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSubstantivePost As System.Windows.Forms.ToolStripMenuItem
End Class
