﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

'Last Message Index = 6

Public Class frmImportEmployeeShift

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportEmployeeShift"
    Private mblnCancel As Boolean = True
    Private mds_ImportData As DataSet
    Private mdtImportData As New DataTable
    Dim dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmImportEmployeeShift_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmImportEmployeeShift_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub CreateDataTable()
        Try
            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next

            Dim lstcbo = gbFileInfo.Controls.OfType(Of ComboBox)().ToList()

            For Each iCbo In lstcbo
                Dim cbo = iCbo
                Select Case iCbo.Name.ToUpper
                    Case cboEffectiveDate.Name.ToUpper
                        If mds_ImportData.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)(cbo.Text) = "").Count() > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Effective Date cannot be blank. Please set Effective Date in order to import."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If mds_ImportData.Tables(0).AsEnumerable().Where(Function(x) IsDate(x.Field(Of String)(cbo.Text)) = False).Count > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Invalid Effective Date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Case cboEmployeeCode.Name.ToUpper
                        If mds_ImportData.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)(cbo.Text) = "").Count() > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Empolyee Code cannot be blank. Please set Employee Code in order to import."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Case cboEmployeeName.Name.ToUpper
                        If mds_ImportData.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)(cbo.Text) = "").Count() > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee Name cannot be blank. Please set Employee Name in order to import."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Case cboShift.Name.ToUpper
                        If mds_ImportData.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)(cbo.Text) = "").Count() > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Shift cannot be blank. Please set Shift in order to import."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                End Select
            Next

            mdtImportData = mds_ImportData.Tables(0).DefaultView.ToTable(False, cboEffectiveDate.Text, cboEmployeeCode.Text, cboEmployeeName.Text, cboShift.Text)

            For Each iCbo In lstcbo
                Dim cbo = iCbo
                Select Case iCbo.Name.ToUpper
                    Case cboEffectiveDate.Name.ToUpper
                        mdtImportData.Columns(cbo.Text).ColumnName = "effectivedate"
                    Case cboEmployeeCode.Name.ToUpper
                        mdtImportData.Columns(cbo.Text).ColumnName = "employeecode"
                    Case cboEmployeeName.Name.ToUpper
                        mdtImportData.Columns(cbo.Text).ColumnName = "employee"
                    Case cboShift.Name.ToUpper
                        mdtImportData.Columns(cbo.Text).ColumnName = "shift"
                End Select
            Next
            Dim dCol As DataColumn
            dCol = New DataColumn
            With dCol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "image"
                .DataType = GetType(System.Object)
                .DefaultValue = New Drawing.Bitmap(1, 1).Clone
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "message"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "status"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "objStatus"
                .DataType = GetType(System.String)
                .DefaultValue = 0
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "objIsProcess"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "IsExists"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "EmpId"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtImportData.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ShiftId"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtImportData.Columns.Add(dCol)



            'mdtImportData.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            'mdtImportData.Columns.Add("image", System.Type.GetType("System.Object")).DefaultValue = New Drawing.Bitmap(1, 1).Clone
            'mdtImportData.Columns.Add("message", System.Type.GetType("System.String")).DefaultValue = ""
            'mdtImportData.Columns.Add("status", System.Type.GetType("System.String")).DefaultValue = ""
            'mdtImportData.Columns.Add("objStatus", System.Type.GetType("System.String")).DefaultValue = "0"
            'mdtImportData.Columns.Add("objIsProcess", System.Type.GetType("System.Boolean")).DefaultValue = False
            'mdtImportData.Columns.Add("IsExists", System.Type.GetType("System.Boolean")).DefaultValue = False

            'mdtImportData.Columns.Add("EmpId", GetType(System.Int32)).DefaultValue = 0
            'mdtImportData.Columns.Add("ShiftId", GetType(System.Int32)).DefaultValue = 0


            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim objEmployee As New clsEmployee_Master
            Dim objShift As New clsNewshift_master
            For Each dtRow As DataRow In mdtImportData.Rows
                'dtRow.Item("EmployeeName") = dtRow.Item("EmployeeCode").ToString() & " - " & dtRow.Item("EmployeeName").ToString()
                'mdtImportData.ImportRow(dtRow) : mdtImportData.AcceptChanges()
                Dim RowIndex As Integer = mdtImportData.Rows.IndexOf(dtRow)

                Dim xEmployeeId As Integer = objEmployee.GetEmployeeUnkidFromEmpCode(dtRow("employeecode").ToString())
                mdtImportData.Rows(RowIndex)("EmpId") = xEmployeeId

                Dim xShiftID As Integer = objShift.GetShiftUnkId(dtRow.Item("shift").ToString())
                mdtImportData.Rows(RowIndex)("ShiftId") = xShiftID

                If IsDate(dtRow("effectivedate")) = False Then
                    mdtImportData.Rows(RowIndex)("image") = imgError
                    mdtImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 21, "Invalid Effective Date.")
                    mdtImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    mdtImportData.Rows(RowIndex)("objStatus") = -1
                    mdtImportData.Rows(RowIndex)("objIsProcess") = False
                    mdtImportData.Rows(RowIndex)("IsExists") = False
                    Continue For
                End If

                If xEmployeeId <= 0 Then
                    mdtImportData.Rows(RowIndex)("image") = imgError
                    mdtImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 1, "Invalid Employee Code.")
                    mdtImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    mdtImportData.Rows(RowIndex)("objStatus") = -1
                    mdtImportData.Rows(RowIndex)("objIsProcess") = False
                    mdtImportData.Rows(RowIndex)("IsExists") = False
                    Continue For

                ElseIf xShiftID <= 0 Then
                    mdtImportData.Rows(RowIndex)("image") = imgError
                    mdtImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 24, "Invalid Shift Name.")
                    mdtImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                    mdtImportData.Rows(RowIndex)("objStatus") = -1
                    mdtImportData.Rows(RowIndex)("objIsProcess") = False
                    mdtImportData.Rows(RowIndex)("IsExists") = False
                    Continue For
                End If

                Dim intOldShiftId As Integer = objEmpShift.GetEmployee_Old_ShiftId(CDate(dtRow("effectivedate")).Date, xEmployeeId)

                If xShiftID = intOldShiftId Then
                    mdtImportData.Rows(RowIndex)("image") = imgWarring
                    mdtImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 25, "This shift already assign to this employee.")
                    mdtImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 27, "Warning")
                    mdtImportData.Rows(RowIndex)("objStatus") = 2
                    mdtImportData.Rows(RowIndex)("objIsProcess") = False
                    mdtImportData.Rows(RowIndex)("IsExists") = True
                    Continue For
                End If

                Dim iCurrShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dtRow.Item("effectivedate")).Date, xEmployeeId)

                If iCurrShiftId = xShiftID Then
                    mdtImportData.Rows(RowIndex)("image") = imgWarring
                    mdtImportData.Rows(RowIndex)("message") = Language.getMessage(mstrModuleName, 23, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for previous date.")
                    mdtImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 27, "Warning")
                    mdtImportData.Rows(RowIndex)("objStatus") = 2
                    mdtImportData.Rows(RowIndex)("objIsProcess") = False
                    mdtImportData.Rows(RowIndex)("IsExists") = True
                    Continue For
                End If

                If iCurrShiftId <> xShiftID Then
                    Dim intFutureShiftId, intPreviousShiftId As Integer
                    intFutureShiftId = 0 : intPreviousShiftId = 0
                    Dim dtOldEffDate As DateTime = Nothing
                    Dim dttab As DataTable = Nothing
                    Dim strMessage As String = ""
                    dttab = objEmpShift.GetEmployeePreviousFutureShift(CDate(dtRow.Item("effectivedate")), xEmployeeId, False)
                    If dttab.Rows.Count > 0 Then
                        intPreviousShiftId = CInt(dttab.Rows(0)("shiftunkid"))
                        dtOldEffDate = CDate(dttab.Rows(0)("effectivedate"))
                    End If
                    If intPreviousShiftId = xShiftID Then
                        mdtImportData.Rows(RowIndex)("image") = imgWarring
                        strMessage = Language.getMessage(mstrModuleName, 14, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for date :")
                        If dtOldEffDate <> Nothing Then
                            strMessage &= " " & dtOldEffDate.ToShortDateString()
                        End If
                        mdtImportData.Rows(RowIndex)("message") = strMessage
                        mdtImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 27, "Warning")
                        mdtImportData.Rows(RowIndex)("objStatus") = 2
                        mdtImportData.Rows(RowIndex)("objIsProcess") = False
                        mdtImportData.Rows(RowIndex)("IsExists") = True
                        Continue For
                    End If
                    strMessage = ""
                    dtOldEffDate = Nothing
                    If dttab.Rows.Count > 0 Then
                        intFutureShiftId = CInt(dttab.Rows(0)("shiftunkid"))
                        dtOldEffDate = CDate(dttab.Rows(0)("effectivedate"))
                    End If
                    dttab = objEmpShift.GetEmployeePreviousFutureShift(CDate(dtRow.Item("effectivedate")), xEmployeeId, True)
                    If intFutureShiftId = xShiftID Then
                        mdtImportData.Rows(RowIndex)("image") = imgError
                        strMessage = Language.getMessage(mstrModuleName, 14, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for date :")
                        If dtOldEffDate <> Nothing Then
                            strMessage &= " " & dtOldEffDate.ToShortDateString()
                        End If
                        mdtImportData.Rows(RowIndex)("message") = strMessage
                        mdtImportData.Rows(RowIndex)("status") = Language.getMessage(mstrModuleName, 2, "Fail")
                        mdtImportData.Rows(RowIndex)("objStatus") = 2
                        mdtImportData.Rows(RowIndex)("objIsProcess") = False
                        mdtImportData.Rows(RowIndex)("IsExists") = True
                        Continue For
                    End If
                End If

            Next

            mdtImportData.AcceptChanges()
            objEmployee = Nothing
            objEmpShift = Nothing

            dgData.AutoGenerateColumns = False
            objdgcolhIsCheck.DataPropertyName = "ischeck"
            colhEffectiveDate.DataPropertyName = "effectivedate"
            dgcolhEcode.DataPropertyName = "employeecode"
            colhEmployee.DataPropertyName = "employee"
            colhShift.DataPropertyName = "shift"
            colhMessage.DataPropertyName = "message"
            objcolhImage.DataPropertyName = "image"
            colhStatus.DataPropertyName = "status"
            objcolhstatus.DataPropertyName = "objStatus"

            dvGriddata = mdtImportData.DefaultView

            If mdtImportData.AsEnumerable().Where(Function(x) x.Field(Of String)("status") = "").Count() > 0 Then
                btnImport.Enabled = True
            Else
                btnImport.Enabled = False
            End If

            dgData.DataSource = mdtImportData

            Dim dr = From dgdr As DataGridViewRow In dgData.Rows.Cast(Of DataGridViewRow)() Where CInt(dgdr.Cells(objcolhstatus.Index).Value) = -1 Select dgdr
            dr.ToList.ForEach(Function(x) SetRowStyle(x))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Function SetRowStyle(ByVal xRow As DataGridViewRow) As Boolean
        xRow.Cells(objdgcolhIsCheck.Index).ReadOnly = True
        Return True
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False

            Dim drList = mdtImportData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("EmpId") > 0 AndAlso x.Field(Of Boolean)("ischeck") = True)
            If drList.Count <= 0 Then Exit Sub
            Dim iRowIndex As Integer = 0
            For Each dr In drList
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdtImportData.Rows.IndexOf(dr) - 5
                    Application.DoEvents()
                Catch ex As Exception
                End Try

                iRowIndex = mdtImportData.Rows.IndexOf(dr)

                Dim objEmpShift As New clsEmployee_Shift_Tran
                Dim objEmployee As New clsEmployee_Master
                Dim objShift As New clsNewshift_master
                Dim xEmployeeID As Integer = -1
                Dim xShiftID As Integer = -1
                Dim xEmpShiftTranID As Integer = -1

                xEmployeeID = CInt(dr.Item("EmpId"))
                xShiftID = CInt(dr.Item("ShiftId"))

                If chkOverWriteifExist.Checked = False Then
                    Dim iCurrShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr.Item("effectivedate")).Date, xEmployeeID)

                    If iCurrShiftId = xShiftID Then
                        mdtImportData.Rows(iRowIndex)("image") = imgError
                        mdtImportData.Rows(iRowIndex)("message") = Language.getMessage(mstrModuleName, 23, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for previous date.")
                        mdtImportData.Rows(iRowIndex)("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        mdtImportData.Rows(iRowIndex)("objStatus") = 2
                        Continue For

                    ElseIf iCurrShiftId <> xShiftID Then

                        Dim iPreviousShiftId As Integer = 0
                        Dim dtTable As DataTable = objEmpShift.GetEmployeePreviousFutureShift(CDate(dr.Item("effectivedate")).Date, xEmployeeID, False)
                        If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                            iPreviousShiftId = CInt(dtTable.Rows(0)("shiftunkid"))
                        End If

                        If iPreviousShiftId = xShiftID Then  'CHECK FOR PREVIOUS DATE SHIFT ID
                            mdtImportData.Rows(iRowIndex)("image") = imgError
                            mdtImportData.Rows(iRowIndex)("message") = Language.getMessage(mstrModuleName, 13, "This Employee's Shift already exists for previous date. Please define new Employee's Shift.")
                            mdtImportData.Rows(iRowIndex)("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            mdtImportData.Rows(iRowIndex)("objStatus") = 2
                        End If

                        If iPreviousShiftId <> xShiftID Then  'CHECK FOR PREVIOUS DATE SHIFT ID

                            Dim iFutureShiftId As Integer = 0
                            dtTable = objEmpShift.GetEmployeePreviousFutureShift(CDate(dr.Item("effectivedate")).Date, xEmployeeID, True)
                            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                                iFutureShiftId = CInt(dtTable.Rows(0)("shiftunkid"))
                            End If

                            If iFutureShiftId = xShiftID Then    'CHECK FOR FUTURE DATE SHIFT ID
                                mdtImportData.Rows(iRowIndex)("image") = imgError
                                mdtImportData.Rows(iRowIndex)("message") = Language.getMessage(mstrModuleName, 19, "This Employee's Shift already exists for future date. Please define new Employee's Shift.")
                                mdtImportData.Rows(iRowIndex)("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                                mdtImportData.Rows(iRowIndex)("objStatus") = 2
                            End If

                            If iFutureShiftId <> xShiftID Then    'CHECK FOR FUTURE DATE SHIFT ID
                                objEmpShift._effectivedate = CDate(dr.Item("effectivedate")).Date
                                objEmpShift._EmployeeUnkid = xEmployeeID
                                objEmpShift._ShiftUnkid = xShiftID
                                objEmpShift._Userunkid = User._Object._Userunkid
                                objEmpShift._Isvoid = False

                                Dim iShiftId As Integer = objEmpShift.GetEmployee_Old_ShiftId(CDate(dr.Item("effectivedate")).Date, xEmployeeID)  'CHECK FOR CURRENT DATE SHIFT ID

                                If iShiftId <= 0 Then

                                    If objEmpShift.ImportInsertEmployeeShift() = False Then
                                        mdtImportData.Rows(iRowIndex)("image") = imgError
                                        mdtImportData.Rows(iRowIndex)("message") = objEmpShift._Message
                                        mdtImportData.Rows(iRowIndex)("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                                        mdtImportData.Rows(iRowIndex)("objStatus") = 2
                                    Else
                                        mdtImportData.Rows(iRowIndex)("image") = imgAccept
                                        mdtImportData.Rows(iRowIndex)("message") = ""
                                        mdtImportData.Rows(iRowIndex)("status") = Language.getMessage(mstrModuleName, 11, "Success")
                                        mdtImportData.Rows(iRowIndex)("objStatus") = 1
                                    End If

                                ElseIf iShiftId > 0 Then

                                End If
                            End If
                        End If
                    End If
                Else
                    Dim xShiftTranId As Integer = 0
                    Dim dtShift As DataTable = objEmpShift.GetEmployeeShift(CDate(dr.Item("effectivedate")).Date, xEmployeeID)
                    If dtShift IsNot Nothing AndAlso dtShift.Rows.Count > 0 Then
                        xShiftTranId = CInt(dtShift.Rows(0)("shifttranunkid"))
                    End If

                    objEmpShift._EmployeeUnkid = xEmployeeID
                    objEmpShift._effectivedate = CDate(dr.Item("effectivedate")).Date
                    objEmpShift._Isvoid = False
                    objEmpShift._ShiftUnkid = xShiftID
                    objEmpShift._Userunkid = User._Object._Userunkid

                    If objEmpShift.ImportUpdateEmployeeShift(xShiftTranId) = False Then
                        mdtImportData.Rows(iRowIndex)("image") = imgError
                        mdtImportData.Rows(iRowIndex)("message") = objEmpShift._Message
                        mdtImportData.Rows(iRowIndex)("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        mdtImportData.Rows(iRowIndex)("objStatus") = 2
                    Else
                        mdtImportData.Rows(iRowIndex)("image") = imgAccept
                        mdtImportData.Rows(iRowIndex)("message") = ""
                        mdtImportData.Rows(iRowIndex)("status") = Language.getMessage(mstrModuleName, 11, "Success")
                        mdtImportData.Rows(iRowIndex)("objStatus") = 1
                    End If
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            Dim listcols = mds_ImportData.Tables(0).Columns.Cast(Of DataColumn).AsEnumerable().Select(Function(x) x.ColumnName).ToList()
            For Each cr As Control In gbFileInfo.Controls
                If TypeOf cr Is ComboBox Then
                    ClearCombo(CType(cr, ComboBox))
                    CType(cr, ComboBox).Items.AddRange(listcols.ToArray())
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then

                Cursor = Cursors.WaitCursor
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                mds_ImportData = OpenXML_Import(txtFilePath.Text)

                Dim strQuery As String = ""
                For i As Integer = 0 To mds_ImportData.Tables(0).Columns.Count - 1
                    mds_ImportData.Tables(0).Columns(i).ColumnName = mds_ImportData.Tables(0).Columns(i).ColumnName.Replace(" ", "_")
                    strQuery &= " AND " & mds_ImportData.Tables(0).Columns(i).ColumnName & " IS NULL "
                Next

                If strQuery.Trim.Length > 0 Then
                    strQuery = strQuery.Trim.Substring(4, strQuery.Trim.Length - 4)
                End If

                Dim drRow As DataRow() = mds_ImportData.Tables(0).Select(strQuery)

                If drRow.Length > 0 Then

                    For j As Integer = 0 To drRow.Length - 1
                        mds_ImportData.Tables(0).Rows.Remove(drRow(j))
                    Next
                    mds_ImportData.Tables(0).AcceptChanges()

                End If
                Call SetDataCombo()
                'CreateDataTable()
                Cursor = Cursors.Default
            End If
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            If dvGriddata IsNot Nothing Then
                dvGriddata.RowFilter = "objStatus IN (-1,2)"

                Dim savDialog As New SaveFileDialog
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image") : dtTable.Columns.Remove("objstatus")
                        dtTable.Columns.Remove("objIsProcess") : dtTable.Columns.Remove("IsExists")
                        dtTable.Columns.Remove("ischeck") : dtTable.Columns.Remove("EmpId")
                        dtTable.Columns.Remove("ShiftId")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Employee Shift") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 3"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try
            mdtImportData.AcceptChanges()
            If mdtImportData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, Please check atleast one of the information from the list in order to import or update."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Import_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkButton Events "

    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim dtExTable As New DataTable("DataList")
            With dtExTable
                .Columns.Add(Language.getMessage(mstrModuleName, 15, "EmployeeCode"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 16, "EffectiveDate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 17, "EmployeeName"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 18, "Shift"), System.Type.GetType("System.String"))
            End With
            Dim dsList As New DataSet
            dsList.Tables.Add(dtExTable.Copy)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFileInfo.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFileInfo.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkFill_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkFill.LinkClicked
        Try
            For Each ctrl As Control In gbFileInfo.Controls
                If TypeOf ctrl Is ComboBox AndAlso ctrl.Enabled = True Then
                    If CType(ctrl, ComboBox).Text = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit For
                    End If
                End If
            Next
            dgData.DataSource = Nothing
            Application.DoEvents()
            CreateDataTable()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkFill_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Checkbox Event(s)"

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler dgData.CellContentClick, AddressOf dgData_CellContentClick
            RemoveHandler dgData.CellContentDoubleClick, AddressOf dgData_CellContentClick
            For Each dr As DataRow In mdtImportData.Rows
                dr("ischeck") = CBool(objchkAll.Checked)
            Next
            AddHandler dgData.CellContentClick, AddressOf dgData_CellContentClick
            AddHandler dgData.CellContentDoubleClick, AddressOf dgData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFileInfo.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGriview Event(s)"

    Private Sub dgData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgData.CellContentClick, dgData.CellContentDoubleClick
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If e.ColumnIndex = objdgcolhIsCheck.Index Then
                If Me.dgData.IsCurrentCellDirty Then
                    Me.dgData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                mdtImportData.AcceptChanges()
                Dim drRow As DataRow() = mdtImportData.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If mdtImportData.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.chkOverWriteifExist.Text = Language._Object.getCaption(Me.chkOverWriteifExist.Name, Me.chkOverWriteifExist.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
            Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblFirstname.Text = Language._Object.getCaption(Me.lblFirstname.Name, Me.lblFirstname.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
            Me.elMapping.Text = Language._Object.getCaption(Me.elMapping.Name, Me.elMapping.Text)
            Me.lnkFill.Text = Language._Object.getCaption(Me.lnkFill.Name, Me.lnkFill.Text)
            Me.colhEffectiveDate.HeaderText = Language._Object.getCaption(Me.colhEffectiveDate.Name, Me.colhEffectiveDate.HeaderText)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhShift.HeaderText = Language._Object.getCaption(Me.colhShift.Name, Me.colhShift.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Invalid Employee Code.")
            Language.setMessage(mstrModuleName, 2, "Fail")
            Language.setMessage(mstrModuleName, 3, "Effective Date cannot be blank. Please set Effective Date in order to import.")
            Language.setMessage(mstrModuleName, 4, "Empolyee Code cannot be blank. Please set Employee Code in order to import.")
            Language.setMessage(mstrModuleName, 5, "Employee Name cannot be blank. Please set Employee Name in order to import.")
            Language.setMessage(mstrModuleName, 6, "Shift cannot be blank. Please set Shift in order to import.")
            Language.setMessage(mstrModuleName, 7, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 8, "Fail")
            Language.setMessage(mstrModuleName, 11, "Success")
            Language.setMessage(mstrModuleName, 13, "This Employee's Shift already exists for previous date. Please define new Employee's Shift.")
            Language.setMessage(mstrModuleName, 14, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for date :")
            Language.setMessage(mstrModuleName, 15, "EmployeeCode")
            Language.setMessage(mstrModuleName, 16, "EffectiveDate")
            Language.setMessage(mstrModuleName, 17, "EmployeeName")
            Language.setMessage(mstrModuleName, 18, "Shift")
            Language.setMessage(mstrModuleName, 19, "This Employee's Shift already exists for future date. Please define new Employee's Shift.")
            Language.setMessage(mstrModuleName, 20, "Template Exported Successfully.")
            Language.setMessage(mstrModuleName, 21, "Invalid Effective Date.")
            Language.setMessage(mstrModuleName, 22, "Sorry, Please check atleast one of the information from the list in order to import or update.")
            Language.setMessage(mstrModuleName, 23, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for previous date.")
            Language.setMessage(mstrModuleName, 24, "Invalid Shift Name.")
            Language.setMessage(mstrModuleName, 25, "This shift already assign to this employee.")
            Language.setMessage(mstrModuleName, 26, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 27, "Warning")
            Language.setMessage(mstrModuleName, 41, " Fields From File.")
            Language.setMessage(mstrModuleName, 42, "Please select different Field.")
            Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 50, "is already Mapped with")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class