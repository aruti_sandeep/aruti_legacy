﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportReportTo

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportReportTo"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Form's Events "

    Private Sub frmImportReportTo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportReportTo_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizReportTo_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizReportTo.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizReportTo.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizReportTo_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizReportTo_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizReportTo.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizReportTo.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizReportTo.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case eZeeWizReportTo.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizReportTo_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboDefaultReportTo.Items.Add(dtColumns.ColumnName)
                cboReportingCode.Items.Add(dtColumns.ColumnName)
                cboEmployeeName.Items.Add(dtColumns.ColumnName)
                cboReportToEmp.Items.Add(dtColumns.ColumnName)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known
                If dtColumns.ColumnName = Language.getMessage("clsReportingToEmployee", 4, "EmployeeCode") Then
                    dtColumns.Caption = "EmployeeCode"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsReportingToEmployee", 5, "EmployeeName") Then
                    dtColumns.Caption = "EmployeeName"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsReportingToEmployee", 6, "ReportingCode") Then
                    dtColumns.Caption = "ReportingCode"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsReportingToEmployee", 7, "ReportToEmp") Then
                    dtColumns.Caption = "ReportToEmp"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsReportingToEmployee", 8, "DefaultReportTo") Then
                    dtColumns.Caption = "DefaultReportTo"
                End If
                'Gajanan (24 Nov 2018) -- End

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("ename", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("rcode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("rname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("isdefault", System.Type.GetType("System.Int32"))

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))


            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                drNewRow.Item("ename") = dtRow.Item(cboEmployeeName.Text).ToString.Trim
                drNewRow.Item("rcode") = dtRow.Item(cboReportingCode.Text).ToString.Trim
                drNewRow.Item("rname") = dtRow.Item(cboReportToEmp.Text).ToString.Trim
                drNewRow.Item("isdefault") = dtRow.Item(cboDefaultReportTo.Text).ToString.Trim

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ename"
                colhReportingTo.DataPropertyName = "rname"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizReportTo.BackEnabled = False
            eZeeWizReportTo.CancelText = "Finish"
        Catch ex As System.ArgumentException
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please check the selected file column '") & cboDefaultReportTo.Text & Language.getMessage(mstrModuleName, 9, "' should only be in '0' Or '1' Format. Please modify it and try to import again."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Name is mandatory information. Please select Employee Name to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboReportingCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Reporting Code is mandatory information. Please select Reporting Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboReportToEmp.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Reporting Employee is mandatory information. Please select Reporting Employee to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboDefaultReportTo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Default Reporting is mandatory information. Please select Default Reporting to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim dtTable As DataTable
            Dim objEMaster As New clsEmployee_Master
            Dim objReportTo As clsReportingToEmployee

            Dim intEEmpUnkid As Integer = -1
            Dim intREmpUnkid As Integer = -1

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Application.DoEvents()
                intEEmpUnkid = 0 : intREmpUnkid = 0

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEEmpUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEEmpUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF REPORT TO EMPLOYEE PRESENT.
                If dtRow.Item("rcode").ToString.Trim.Length > 0 Then
                    intREmpUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("rcode").ToString.Trim)
                    If intREmpUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Report To Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                objReportTo = New clsReportingToEmployee

                If objReportTo.Is_Present(intEEmpUnkid, intREmpUnkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = True Then
                    'Hemant (03 Feb 2023) -- [eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)]
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Data Already Exists.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 14, "Warning")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                End If

                'S.SANDEEP [ 14 JUN 2014 ] -- START
                Dim mdtInsertedData As DataTable = Nothing

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'objReportTo._EmployeeUnkid = intEEmpUnkid
                objReportTo._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEEmpUnkid
                'Pinkal (18-Aug-2018) -- End

                mdtInsertedData = objReportTo._RDataTable
                Dim isHierarchy As Boolean = False
                Dim dtmp() As DataRow = mdtInsertedData.Select("employeeunkid = '" & intEEmpUnkid & "' AND ishierarchy = True")
                If dtmp.Length > 0 Then
                    isHierarchy = True
                End If
                'S.SANDEEP [ 14 JUN 2014 ] -- END


                dtTable = objReportTo._RDataTable
                'S.SANDEEP [ 14 JUN 2014 ] -- START
                dtTable.Rows.Clear()
                'S.SANDEEP [ 14 JUN 2014 ] -- END
                Dim dRow As DataRow
                dRow = dtTable.NewRow

                With dRow
                    .Item("reporttounkid") = -1
                    .Item("employeeunkid") = intEEmpUnkid
                    .Item("reporttoemployeeunkid") = intREmpUnkid
                    'S.SANDEEP [ 14 JUN 2014 ] -- START
                    '.Item("ishierarchy") = CBool(dtRow.Item("isdefault"))
                    If isHierarchy = True Then
                        .Item("ishierarchy") = False
                    Else
                        .Item("ishierarchy") = CBool(dtRow.Item("isdefault"))
                    End If
                    'S.SANDEEP [ 14 JUN 2014 ] -- END
                    .Item("userunkid") = User._Object._Userunkid
                    .Item("isvoid") = False
                    .Item("voiduserunkid") = -1
                    .Item("voiddatetime") = DBNull.Value
                    .Item("voidreason") = ""
                    .Item("AUD") = "A"
                    .Item("GUID") = Guid.NewGuid.ToString
                    'Hemant (03 Feb 2023) -- Start
                    'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                    .Item("effectivedate") = ConfigParameter._Object._EmployeeAsOnDate
                    'Hemant (03 Feb 2023) -- End

                End With
                dtTable.Rows.Add(dRow)

                objReportTo._RDataTable = dtTable

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objReportTo._FormName = mstrModuleName
                objReportTo._LoginEmployeeunkid = 0
                objReportTo._ClientIP = getIP()
                objReportTo._HostName = getHostName()
                objReportTo._FromWeb = False
                objReportTo._AuditUserId = User._Object._Userunkid
objReportTo._CompanyUnkid = Company._Object._Companyunkid
                objReportTo._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objReportTo.InsertUpdateDelete() = True Then

                'Pinkal (20-Mar-2023) -- Start
                'A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube.
                'If objReportTo.InsertUpdateDelete(False) = True Then
                If objReportTo.InsertUpdateDelete(False, Company._Object._Companyunkid) = True Then
                    'Pinkal (20-Mar-2023) -- End

                    'Pinkal (18-Aug-2018) -- End
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 15, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Insert Data Failed.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END
#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizReportTo.CancelText = Language._Object.getCaption(Me.eZeeWizReportTo.Name & "_CancelText", Me.eZeeWizReportTo.CancelText)
            Me.eZeeWizReportTo.NextText = Language._Object.getCaption(Me.eZeeWizReportTo.Name & "_NextText", Me.eZeeWizReportTo.NextText)
            Me.eZeeWizReportTo.BackText = Language._Object.getCaption(Me.eZeeWizReportTo.Name & "_BackText", Me.eZeeWizReportTo.BackText)
            Me.eZeeWizReportTo.FinishText = Language._Object.getCaption(Me.eZeeWizReportTo.Name & "_FinishText", Me.eZeeWizReportTo.FinishText)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblIsDefault.Text = Language._Object.getCaption(Me.lblIsDefault.Name, Me.lblIsDefault.Text)
            Me.lblReportToCode.Text = Language._Object.getCaption(Me.lblReportToCode.Name, Me.lblReportToCode.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lblReportingEmpName.Text = Language._Object.getCaption(Me.lblReportingEmpName.Name, Me.lblReportingEmpName.Text)
            Me.Label3.Text = Language._Object.getCaption(Me.Label3.Name, Me.Label3.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
            Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhReportingTo.HeaderText = Language._Object.getCaption(Me.colhReportingTo.Name, Me.colhReportingTo.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue.")
			Language.setMessage("clsReportingToEmployee", 4, "EmployeeCode")
			Language.setMessage("clsReportingToEmployee", 5, "EmployeeName")
			Language.setMessage("clsReportingToEmployee", 6, "ReportingCode")
			Language.setMessage("clsReportingToEmployee", 7, "ReportToEmp")
			Language.setMessage("clsReportingToEmployee", 8, "DefaultReportTo")
            Language.setMessage(mstrModuleName, 9, "' should only be in '0' Or '1' Format. Please modify it and try to import again.")
            Language.setMessage(mstrModuleName, 10, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 11, "Fail")
            Language.setMessage(mstrModuleName, 12, "Report To Employee Not Found.")
            Language.setMessage(mstrModuleName, 13, "Data Already Exists.")
            Language.setMessage(mstrModuleName, 14, "Warning")
            Language.setMessage(mstrModuleName, 15, "Success")
            Language.setMessage(mstrModuleName, 16, "Insert Data Failed.")
            Language.setMessage(mstrModuleName, 17, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 4, "Employee Name is mandatory information. Please select Employee Name to continue.")
			Language.setMessage(mstrModuleName, 5, "Reporting Code is mandatory information. Please select Reporting Code to continue.")
			Language.setMessage(mstrModuleName, 6, "Reporting Employee is mandatory information. Please select Reporting Employee to continue.")
			Language.setMessage(mstrModuleName, 7, "Default Reporting is mandatory information. Please select Default Reporting to continue.")
			Language.setMessage(mstrModuleName, 8, "Please check the selected file column '")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class