﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportRehireEmployee

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportRehireEmployee"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " From's Events "

    Private Sub frmImportMemberships_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportMemberships_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub WizImportRehireEmployee_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizImportRehireEmployee.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizImportRehireEmployee.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportRehireEmployee_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub WizImportRehireEmployee_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizImportRehireEmployee.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizImportRehireEmployee.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()

                Case WizImportRehireEmployee.Pages.IndexOf(wizPageMapping)

                    If cboEmployeeCode.Text.Trim.Length <= 0 Then
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Employee Code cannot be blank. Please set the Employee Code to import rehire employee(s)."), enMsgBoxStyle.Information)
                        cboEmployeeCode.Focus()
                        Exit Sub

                    ElseIf cboEffectiveDate.Text.Trim.Length <= 0 Then
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Effective Date cannot be blank. Please set the Effective Date to import rehire employee(s)."), enMsgBoxStyle.Information)
                        cboEffectiveDate.Focus()
                        Exit Sub

                    ElseIf cboRehireDate.Text.Trim.Length <= 0 Then
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Rehire Date cannot be blank. Please set the Rehire Date to import rehire employee(s)."), enMsgBoxStyle.Information)
                        cboRehireDate.Focus()
                        Exit Sub

                    ElseIf cboReason.Text.Trim.Length <= 0 Then
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Reason cannot be blank. Please set the Reason to import rehire employee(s)."), enMsgBoxStyle.Information)
                        cboReason.Focus()
                        Exit Sub
                    End If

                Case WizImportRehireEmployee.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportRehireEmployee_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Dim objEmployee As New clsEmployee_Master
        Try
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("effectivedate", System.Type.GetType("System.DateTime"))
            mdt_ImportData_Others.Columns.Add("rehiredate", System.Type.GetType("System.DateTime"))
            mdt_ImportData_Others.Columns.Add("eocdate", System.Type.GetType("System.DateTime"))
            mdt_ImportData_Others.Columns.Add("reason", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))


            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            Dim intEmpId As Integer = -1

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows

                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                If cboEmployeeCode.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                Else
                    drNewRow.Item("employeecode") = ""
                End If

                If cboEffectiveDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("effectivedate") = IIf(dtRow.Item(cboEffectiveDate.Text).ToString.Trim.Length > 0, dtRow.Item(cboEffectiveDate.Text).ToString.Trim, DBNull.Value)
                Else
                    drNewRow.Item("effectivedate") = DBNull.Value
                End If

                If cboRehireDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("rehiredate") = IIf(dtRow.Item(cboRehireDate.Text).ToString.Trim.Length > 0, dtRow.Item(cboRehireDate.Text).ToString.Trim, DBNull.Value)
                Else
                    drNewRow.Item("rehiredate") = DBNull.Value
                End If

                If cboReason.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("reason") = dtRow.Item(cboReason.Text).ToString.Trim
                Else
                    drNewRow.Item("reason") = ""
                End If

                If cboEOCDate.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("eocdate") = IIf(dtRow.Item(cboEOCDate.Text).ToString.Trim.Length > 0, dtRow.Item(cboEOCDate.Text).ToString.Trim, DBNull.Value)
                Else
                    drNewRow.Item("eocdate") = DBNull.Value
                End If

                objTotal.Text = CStr(Val(objTotal.Text) + 1)
                intEmpId = -1
                mdt_ImportData_Others.Rows.Add(drNewRow)
            Next

            If mdt_ImportData_Others.Rows.Count > 0 Then
                mdt_ImportData_Others.DefaultView.Sort = "employeecode ASC"
                mdt_ImportData_Others = mdt_ImportData_Others.DefaultView.ToTable()
            End If


            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "employeecode"
                colhEffectiveDate.DataPropertyName = "effectivedate"
                colhRehireDate.DataPropertyName = "rehiredate"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizImportRehireEmployee.BackEnabled = False
            WizImportRehireEmployee.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
            btnFilter.Enabled = True
            objEmployee = Nothing
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Employee Code cannot be blank. Please set the Employee Code to import rehire employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEffectiveDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Effective Date cannot be blank. Please set the Effective Date to import rehire employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboRehireDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Rehire Date cannot be blank. Please set the Rehire Date to import rehire employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboReason.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Reason cannot be blank. Please set the Reason to import rehire employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objEmp As New clsEmployee_Master
            Dim objEmpRehire As New clsemployee_rehire_tran
            Dim objCommon As New clsCommon_Master

            Dim iEmpId As Integer = -1
            Dim xChangeReasonID As Integer = -1

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                iEmpId = 0
                xChangeReasonID = 0
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 4
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("employeecode").ToString.Trim.Length > 0 Then
                    iEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("employeecode").ToString.Trim)
                    If iEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If


                If IsDBNull(dtRow.Item("effectivedate")) OrElse dtRow.Item("effectivedate") Is Nothing Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Effective Date Not Found.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                If IsDBNull(dtRow.Item("rehiredate")) OrElse dtRow.Item("rehiredate") Is Nothing Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Rehire Date Not Found.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If


                If IsDBNull(dtRow.Item("reason")) OrElse dtRow.Item("reason") Is Nothing OrElse dtRow.Item("reason").ToString().Trim().Length <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Rehire Reason Not Found.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                If CDate(dtRow.Item("effectivedate")).Date > CDate(dtRow.Item("rehiredate")).Date Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Effective Date must be less than or equal to Rehire date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEmpId

                If (IsDBNull(objEmp._Empl_Enddate) = False AndAlso objEmp._Empl_Enddate.Date >= CDate(dtRow.Item("effectivedate")).Date) Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Effective date must be greater than EOC Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For

                ElseIf (IsDBNull(objEmp._Termination_From_Date) = False AndAlso objEmp._Termination_From_Date.Date >= CDate(dtRow.Item("effectivedate")).Date) Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Effective date must be greater than Termination / Leaving Date..")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For

                ElseIf (IsDBNull(objEmp._Termination_To_Date) = False AndAlso objEmp._Termination_To_Date.Date <= CDate(dtRow.Item("effectivedate")).Date) Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Effective date must be greater than Retirement Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                If dtRow.Item("reason").ToString.Trim.Length > 0 Then
                    xChangeReasonID = objCommon.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RE_HIRE, dtRow.Item("reason").ToString.Trim)
                    If xChangeReasonID <= 0 Then

                        If dtRow.Item("reason").ToString.Length > 5 Then
                            objCommon._Alias = dtRow.Item("reason").ToString.PadRight(5, CChar(" ")).Substring(0, 4)
                            objCommon._Code = dtRow.Item("reason").ToString.Trim.ToUpper
                        Else
                            objCommon._Alias = dtRow.Item("reason").ToString.Trim.ToUpper
                            objCommon._Code = dtRow.Item("reason").ToString.Trim.ToUpper
                        End If

                        objCommon._Mastertype = clsCommon_Master.enCommonMaster.RE_HIRE
                        objCommon._Name = dtRow.Item("reason").ToString.Trim
                        objCommon._Name1 = dtRow.Item("reason").ToString.Trim
                        objCommon._Name2 = dtRow.Item("reason").ToString.Trim
                        objCommon._Isactive = True
                        objCommon._Userunkid = User._Object._Userunkid
                        If objCommon.Insert(Nothing, Nothing) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCommon._Code & "/" & objCommon._Name & ":" & objCommon._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            xChangeReasonID = objCommon._Masterunkid
                        End If
                    End If
                End If

                objEmpRehire._Effectivedate = CDate(dtRow.Item("effectivedate")).Date
                objEmpRehire._Employeeunkid = iEmpId
                objEmpRehire._Reinstatment_Date = CDate(dtRow.Item("rehiredate")).Date
                objEmpRehire._Changereasonunkid = xChangeReasonID
                objEmpRehire._ActualDate = Nothing

                If IsDBNull(dtRow.Item("eocdate")) = False AndAlso dtRow.Item("eocdate") IsNot Nothing Then
                    objEmpRehire._EOCDate = CDate(dtRow.Item("eocdate")).Date
                    objEmpRehire._LeavingDate = CDate(dtRow.Item("eocdate")).Date
                Else
                    objEmpRehire._EOCDate = Nothing
                    objEmpRehire._LeavingDate = Nothing
                End If
                objEmpRehire._Isvoid = False
                objEmpRehire._Statusunkid = 0
                objEmpRehire._Userunkid = User._Object._Userunkid
                objEmpRehire._Voiddatetime = Nothing
                objEmpRehire._Voidreason = ""
                objEmpRehire._Voiduserunkid = -1

                If objEmpRehire.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                , ConfigParameter._Object._UserAccessModeSetting, True, True, Nothing, False, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst _
                                                , Nothing, "", True, "", eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._IsArutiDemo.ToString, Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees _
                                                 , ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserMustchangePwdOnNextLogon, True) = False Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objEmpRehire._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                Else
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 26, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    If TypeOf ctrl Is ComboBox Then
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
                    End If
                Next

                If dtColumns.ColumnName = Language.getMessage(mstrModuleName, 1, "EmployeeCode") Then
                    dtColumns.Caption = "EmployeeCode"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 2, "EffectiveDate") Then
                    dtColumns.Caption = "EffectiveDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 3, "RehireDate") Then
                    dtColumns.Caption = "RehireDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 4, "EOCDate") Then
                    dtColumns.Caption = "EOCDate"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 5, "Reason") Then
                    dtColumns.Caption = "Reason"
                End If
            Next
            mds_ImportData.AcceptChanges()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Rehire Employee Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkImportFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkImportFormat.LinkClicked
        Try
            Dim dtExTable As New DataTable("DataList")
            With dtExTable
                .Columns.Add(Language.getMessage(mstrModuleName, 1, "EmployeeCode"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 2, "EffectiveDate"), System.Type.GetType("System.DateTime"))
                .Columns.Add(Language.getMessage(mstrModuleName, 3, "RehireDate"), System.Type.GetType("System.DateTime"))
                .Columns.Add(Language.getMessage(mstrModuleName, 4, "EOCDate"), System.Type.GetType("System.DateTime"))
                .Columns.Add(Language.getMessage(mstrModuleName, 5, "Reason"), System.Type.GetType("System.String"))
            End With
            Dim dsList As New DataSet
            dsList.Tables.Add(dtExTable.Copy)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, dsList)
            End If
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Template Exported Successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 23, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 24, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 25, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.WizImportRehireEmployee.CancelText = Language._Object.getCaption(Me.WizImportRehireEmployee.Name & "_CancelText", Me.WizImportRehireEmployee.CancelText)
            Me.WizImportRehireEmployee.NextText = Language._Object.getCaption(Me.WizImportRehireEmployee.Name & "_NextText", Me.WizImportRehireEmployee.NextText)
            Me.WizImportRehireEmployee.BackText = Language._Object.getCaption(Me.WizImportRehireEmployee.Name & "_BackText", Me.WizImportRehireEmployee.BackText)
            Me.WizImportRehireEmployee.FinishText = Language._Object.getCaption(Me.WizImportRehireEmployee.Name & "_FinishText", Me.WizImportRehireEmployee.FinishText)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblRehireDate.Text = Language._Object.getCaption(Me.lblRehireDate.Name, Me.lblRehireDate.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.LblEOCDate.Text = Language._Object.getCaption(Me.LblEOCDate.Name, Me.LblEOCDate.Text)
            Me.lnkImportFormat.Text = Language._Object.getCaption(Me.lnkImportFormat.Name, Me.lnkImportFormat.Text)
            Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhEffectiveDate.HeaderText = Language._Object.getCaption(Me.colhEffectiveDate.Name, Me.colhEffectiveDate.HeaderText)
            Me.colhRehireDate.HeaderText = Language._Object.getCaption(Me.colhRehireDate.Name, Me.colhRehireDate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "EmployeeCode")
            Language.setMessage(mstrModuleName, 2, "EffectiveDate")
            Language.setMessage(mstrModuleName, 3, "RehireDate")
            Language.setMessage(mstrModuleName, 4, "EOCDate")
            Language.setMessage(mstrModuleName, 5, "Reason")
            Language.setMessage(mstrModuleName, 6, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 7, "Employee Code cannot be blank. Please set the Employee Code to import rehire employee(s).")
            Language.setMessage(mstrModuleName, 8, "Effective Date cannot be blank. Please set the Effective Date to import rehire employee(s).")
            Language.setMessage(mstrModuleName, 9, "Rehire Date cannot be blank. Please set the Rehire Date to import rehire employee(s).")
            Language.setMessage(mstrModuleName, 10, "Reason cannot be blank. Please set the Reason to import rehire employee(s).")
            Language.setMessage(mstrModuleName, 11, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 12, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 13, "Fail")
            Language.setMessage(mstrModuleName, 14, "Effective Date Not Found.")
            Language.setMessage(mstrModuleName, 15, "Rehire Date Not Found.")
            Language.setMessage(mstrModuleName, 16, "Rehire Reason Not Found.")
            Language.setMessage(mstrModuleName, 17, "Effective Date must be less than or equal to Rehire date.")
            Language.setMessage(mstrModuleName, 18, "Effective date must be greater than EOC Date.")
            Language.setMessage(mstrModuleName, 19, "Effective date must be greater than Termination / Leaving Date..")
            Language.setMessage(mstrModuleName, 20, "Effective date must be greater than Retirement Date.")
            Language.setMessage(mstrModuleName, 21, "Template Exported Successfully.")
            Language.setMessage(mstrModuleName, 22, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 23, "is already Mapped with")
            Language.setMessage(mstrModuleName, 24, " Fields From File.")
            Language.setMessage(mstrModuleName, 25, "Please select different Field.")
            Language.setMessage(mstrModuleName, 26, "Success")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class