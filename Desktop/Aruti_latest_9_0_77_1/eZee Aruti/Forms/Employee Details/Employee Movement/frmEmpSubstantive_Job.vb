﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System

#End Region

Public Class frmEmpSubstantive_Job

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmpSubstantive_Job"
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private imgBlank As Drawing.Bitmap = CType(New Drawing.Bitmap(1, 1).Clone, Bitmap)
    Private xMasterType As Integer = 0
    Private imgInfo As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.information)
    Private mintSubstantivetranunkid As Integer = 0
    Private imgView As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.doc_view)
    Private mstrEmployeeCode As String = String.Empty
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private objEmpSubstantive As clsempsubstantive_job_tran

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombos As New DataSet
        Try
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , "", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
            End With

            dsCombos = (New clsJobs).getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose() : objEmployee = Nothing
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try
            dsData = objEmpSubstantive.GetList("List", CInt(cboEmployee.SelectedValue), True)

            dgvHistory.AutoGenerateColumns = False

            dgcolhEffectiveDate.DataPropertyName = "effectivedate"
            dgcolhEffectiveDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhStartDate.DataPropertyName = "startdate"
            dgcolhStartDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhEndDate.DataPropertyName = "enddate"
            dgcolhEndDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhSubstantivePost.DataPropertyName = "Job"
            dgcolhRemark.DataPropertyName = "remark"
            objdgcolhsubstantivetranunkid.DataPropertyName = "substantivetranunkid"
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            objdgcolhJobId.DataPropertyName = "jobunkid"
            objdgcolhrehiretranunkid.DataPropertyName = "rehiretranunkid"
            dgvHistory.DataSource = dsData.Tables(0)

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If dtpStartDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Start Date is mandatory information. Please set Start Date to continue."), enMsgBoxStyle.Information)
                dtpStartDate.Focus()
                Return False
            End If

            If dtpEffectiveDate.Value.Date > dtpStartDate.Value.Date Then
                eZeeMsgBox.Show(objlblStartFrom.Text & " " & Language.getMessage(mstrModuleName, 4, "cannot be less then effective date."), enMsgBoxStyle.Information)
                dtpStartDate.Focus()
                Return False
            End If

            If dtpEndDate.Checked = True AndAlso (dtpEndDate.Value.Date < dtpStartDate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, end date cannot be less then start date."), enMsgBoxStyle.Information)
                dtpEndDate.Focus()
                Return False
            End If

            If CInt(cboJob.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Job is mandatory information. Please select atleast one Job to continue."), enMsgBoxStyle.Information)
                cboJob.Focus()
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try
            If mAction = enAction.EDIT_ONE Then
                objEmpSubstantive._Substantivetranunkid = mintSubstantivetranunkid
            End If

            objEmpSubstantive._Effectivedate = dtpEffectiveDate.Value
            objEmpSubstantive._Startdate = dtpStartDate.Value.Date

            If dtpEndDate.Checked = True Then
                objEmpSubstantive._Enddate = dtpEndDate.Value.Date
            Else
                objEmpSubstantive._Enddate = Nothing
            End If
            objEmpSubstantive._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmpSubstantive._Jobunkid = CInt(cboJob.SelectedValue)
            objEmpSubstantive._Remark = txtRemark.Text.Trim
            objEmpSubstantive._Userunkid = User._Object._Userunkid
            objEmpSubstantive._Isvoid = False
            objEmpSubstantive._Voiddatetime = Nothing
            objEmpSubstantive._Voidreason = ""
            objEmpSubstantive._Voiduserunkid = -1
            objEmpSubstantive._WebFormName = mstrModuleName


            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.
            objEmpSubstantive._Expiry_reminder = Nothing
            objEmpSubstantive._Ntfsendremindertoemp = False
            objEmpSubstantive._Ntfsendremindertousers = False
            'Pinkal (16-Oct-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetEditValue()
        Try
            objEmpSubstantive._Substantivetranunkid = mintSubstantivetranunkid
            dtpEffectiveDate.Value = objEmpSubstantive._Effectivedate
            dtpStartDate.Value = objEmpSubstantive._Startdate.Date

            If IsDBNull(objEmpSubstantive._Enddate) = False AndAlso objEmpSubstantive._Enddate <> Nothing Then
                dtpEndDate.Value = objEmpSubstantive._Enddate.Date
            End If
            cboEmployee.SelectedValue = objEmpSubstantive._Employeeunkid
            cboJob.SelectedValue = objEmpSubstantive._Jobunkid
            txtRemark.Text = objEmpSubstantive._Remark
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetEditValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            mintSubstantivetranunkid = 0
            dtpEffectiveDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEffectiveDate.Checked = False
            dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpStartDate.Checked = False
            dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEndDate.Checked = False
            cboEmployee.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboEmployee.Text = ""
            cboJob.Text = ""
            txtRemark.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmEmpSubstantive_Job_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpSubstantive = New clsempsubstantive_job_tran
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpSubstantive_Job_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsempsubstantive_job_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsempsubstantive_job_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchJob.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objbtnSearchJob.Name.ToUpper
                    xCbo = cboJob
            End Select

            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Validation() = False Then Exit Sub

            SetValue()

            Dim mblnFlag As Boolean = False
            If mAction = enAction.ADD_CONTINUE OrElse mAction = enAction.ADD_ONE Then
                mblnFlag = objEmpSubstantive.Insert(Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, Nothing)
            Else
                mblnFlag = objEmpSubstantive.Update(Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, Nothing)
            End If

            If mblnFlag = False Then
                eZeeMsgBox.Show(objEmpSubstantive._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
            Call Fill_Grid()
            ClearControls()
            If objEmpSubstantive._Substantivetranunkid > 0 Then objEmpSubstantive._Substantivetranunkid = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress, cboJob.KeyPress
        Try
            Dim cbo As ComboBox = Nothing

            Select Case CType(sender, Windows.Forms.ComboBox).Name.ToUpper
                Case cboEmployee.Name.ToUpper
                    cbo = cboEmployee
                Case cboJob.Name.ToUpper
                    cbo = cboJob
            End Select

            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If CType(sender, Windows.Forms.ComboBox).Name.ToUpper = cboEmployee.Name.ToUpper Then
                        .CodeMember = "employeecode"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim xtmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                If xtmp.Length > 0 Then
                    mstrEmployeeCode = CStr(xtmp(0).Item("employeecode"))
                    mintEmployeeUnkid = CInt(cboEmployee.SelectedValue)
                    mstrEmployeeName = cboEmployee.Text
                End If

                If btnSave.Enabled = False Then btnSave.Enabled = True
                If objdgcolhEdit.Visible = False Then objdgcolhEdit.Visible = True
                If objdgcolhDelete.Visible = False Then objdgcolhDelete.Visible = True

                If mAction = enAction.EDIT_ONE Then
                    If objEmpSubstantive._Substantivetranunkid > 0 Then objEmpSubstantive._Substantivetranunkid = 0
                    mAction = enAction.ADD_CONTINUE
                End If

                Call Fill_Grid()
            Else
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Datagrid Events"

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub

            Select Case e.ColumnIndex

                Case objdgcolhEdit.Index

                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgView Then
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhsubstantivetranunkid.Index).Value) > 0 Then
                            Dim dr As DataRow() = CType(dgvHistory.DataSource, DataTable).Select(objdgcolhsubstantivetranunkid.DataPropertyName & " = " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhsubstantivetranunkid.Index).Value))
                            If dr.Length > 0 Then
                                Dim index As Integer = CType(dgvHistory.DataSource, DataTable).Rows.IndexOf(dr(0))
                                dgvHistory.Rows(index).Selected = True
                                dgvHistory.FirstDisplayedScrollingRowIndex = index
                            End If
                        End If
                        Exit Sub
                    End If

                    mAction = enAction.EDIT_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub

                    objEmpSubstantive._Substantivetranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhsubstantivetranunkid.Index).Value)
                    mintSubstantivetranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhsubstantivetranunkid.Index).Value)

                    SetEditValue()

                Case objdgcolhDelete.Index
                    mAction = enAction.ADD_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objEmpSubstantive._Isvoid = True
                    objEmpSubstantive._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEmpSubstantive._Voidreason = xStrVoidReason
                    objEmpSubstantive._Voiduserunkid = User._Object._Userunkid
                    objEmpSubstantive._WebFormName = mstrModuleName

                    If objEmpSubstantive.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhsubstantivetranunkid.Index).Value), Nothing) = False Then
                        If objEmpSubstantive._Message <> "" Then
                            eZeeMsgBox.Show(objEmpSubstantive._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
            End Select
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.objgbSubstantiveInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbSubstantiveInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.dgcolhEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffectiveDate.Name, Me.dgcolhEffectiveDate.HeaderText)
            Me.dgcolhStartDate.HeaderText = Language._Object.getCaption(Me.dgcolhStartDate.Name, Me.dgcolhStartDate.HeaderText)
            Me.dgcolhEndDate.HeaderText = Language._Object.getCaption(Me.dgcolhEndDate.Name, Me.dgcolhEndDate.HeaderText)
            Me.dgcolhSubstantivePost.HeaderText = Language._Object.getCaption(Me.dgcolhSubstantivePost.Name, Me.dgcolhSubstantivePost.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Start Date is mandatory information. Please set Start Date to continue.")
            Language.setMessage(mstrModuleName, 4, "cannot be less then effective date.")
            Language.setMessage(mstrModuleName, 5, "Sorry, end date cannot be less then start date.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Job is mandatory information. Please select atleast one Job to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class