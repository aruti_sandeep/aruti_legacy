﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmWorkPermitTransaction

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmWorkPermitTransaction"
    Private objEWPermit As clsemployee_workpermit_tran
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mdtAppointmentDate As Date = Nothing
    Private mstrEmployeeCode As String = String.Empty
    'S.SANDEEP [04-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 120
    Private mblnIsResidentPermit As Boolean = False
    'S.SANDEEP [04-Jan-2018] -- END

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private objAPermit As clsPermit_Approval_Tran
    'S.SANDEEP [20-JUN-2018] -- END

    'S.SANDEEP [09-AUG-2018] -- START
    Private imgInfo As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.information)
    'S.SANDEEP [09-AUG-2018] -- END


    'S.SANDEEP [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'S.SANDEEP [15-NOV-2018] -- END


    'S.SANDEEP |17-JAN-2019| -- START
    Private imgView As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.doc_view)
    'S.SANDEEP |17-JAN-2019| -- END


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mintEmployeeID As Integer = -1
    'Gajanan [11-Dec-2019] -- End

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End

#End Region


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
#Region "Property"
    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property
#End Region
    'Gajanan [11-Dec-2019] -- End


#Region " Display Dialog "

    Public Function displayDialog(ByVal blnIsResidentPermit As Boolean) As Boolean
        Try
            mblnIsResidentPermit = blnIsResidentPermit
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objCountry As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, True)
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                        User._Object._Userunkid, _
            '                                        FinancialYear._Object._YearUnkid, _
            '                                        Company._Object._Companyunkid, _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        ConfigParameter._Object._UserAccessModeSetting, _
            '                                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''S.SANDEEP [04 JUN 2015] -- END
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Emp")
            '    .SelectedValue = 0
            '    .Text = ""
            'End With

            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End



            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.WORK_PERMIT, True, "List")
            If mblnIsResidentPermit Then
                dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RESIDENT_PERMIT, True, "List")
            Else
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.WORK_PERMIT, True, "List")
            End If
            'S.SANDEEP [04-Jan-2018] -- END
            With cboChangeReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objCountry.getCountryList("List", True)
            With cboIssueCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            objEmployee = Nothing : objCMaster = Nothing : objCountry = Nothing
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            dsData = objEWPermit.GetList("List", mblnIsResidentPermit, CInt(cboEmployee.SelectedValue)) 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {mblnIsResidentPermit} -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)

            'S.SANDEEP |17-JAN-2019| -- START
            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.Int32)
                .ColumnName = "operationtypeid"
                .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
            End With
            dsData.Tables(0).Columns.Add(dcol)

            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "OperationType"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)
            'S.SANDEEP |17-JAN-2019| -- END

            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim dsPending As New DataSet
                dsPending = objAPermit.GetList("List", mblnIsResidentPermit, CInt(cboEmployee.SelectedValue))
                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsData.Tables(0).ImportRow(row)
                    Next
                End If
            End If
            'S.SANDEEP [20-JUN-2018] -- END

            dgvHistory.AutoGenerateColumns = False

            dgcolhChangeDate.DataPropertyName = "EffDate"
            dgcolhPermitNo.DataPropertyName = "work_permit_no"
            dgcolhCountryName.DataPropertyName = "Country"
            dgcolhIssuePlace.DataPropertyName = "issue_place"
            dgcolhIssueDate.DataPropertyName = "IDate"
            dgcolhExpiryDate.DataPropertyName = "ExDate"
            dgcolhReason.DataPropertyName = "CReason"
            objdgcolhpermittranunkid.DataPropertyName = "workpermittranunkid"
            objdgcolhFromEmp.DataPropertyName = "isfromemployee"
            objdgcolhAppointdate.DataPropertyName = "adate"
            'S.SANDEEP [14 APR 2015] -- START
            objdgcolhrehiretranunkid.DataPropertyName = "rehiretranunkid"
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objdgcolhtranguid.DataPropertyName = "tranguid"
            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            objdgcolhOperationTypeId.DataPropertyName = "operationtypeid"
            objdgcolhOperationType.DataPropertyName = "OperationType"
            'S.SANDEEP |17-JAN-2019| -- END
            dgvHistory.DataSource = dsData.Tables(0)



            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_WorkPermit()
        Try
            Dim dsWorkPermit As New DataSet
            dsWorkPermit = objEWPermit.Get_Current_WorkPermit(Now.Date, mblnIsResidentPermit, CInt(cboEmployee.SelectedValue)) 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {mblnIsResidentPermit} -- END
            If dsWorkPermit.Tables(0).Rows.Count > 0 Then
                txtWorkPermitNo.Text = dsWorkPermit.Tables(0).Rows(0).Item("work_permit_no").ToString
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("issue_date")) = False Then
                    dtpIssueDate.Value = CDate(dsWorkPermit.Tables(0).Rows(0).Item("issue_date"))
                End If
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date")) = False Then
                    dtpExpiryDate.Value = CDate(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date"))
                End If
                cboIssueCountry.SelectedValue = dsWorkPermit.Tables(0).Rows(0).Item("workcountryunkid")
                txtPlaceofIssue.Text = CStr(dsWorkPermit.Tables(0).Rows(0).Item("issue_place"))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_WorkPermit", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Valid_WorkPermit() As Boolean
        Try
            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If txtWorkPermitNo.Text.Trim.Length <= 0 Then
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Work Permit No. is mandatory information. Please enter work permit no."), enMsgBoxStyle.Information)
                If mblnIsResidentPermit = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Work Permit No. is mandatory information. Please enter work permit no."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, Work Permit No. is mandatory information. Please enter work permit no."), enMsgBoxStyle.Information)
                End If
                'S.SANDEEP [04-Jan-2018] -- END
                txtWorkPermitNo.Focus()
                Return False
            End If

            If CInt(cboIssueCountry.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Issue Country is mandatory information. Please select issue country."), enMsgBoxStyle.Information)
                cboIssueCountry.Focus()
                Return False
            End If

            If (dtpIssueDate.Checked = True And dtpExpiryDate.Checked = False) Or (dtpIssueDate.Checked = False And dtpExpiryDate.Checked = True) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, issue date and expiry date are madatory information."), enMsgBoxStyle.Information)
                dtpExpiryDate.Focus()
                Return False
            End If

            If dtpIssueDate.Checked = True AndAlso dtpExpiryDate.Checked = True Then
                If dtpExpiryDate.Value.Date <= dtpIssueDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, expiry date cannot be less or equal to issue date."), enMsgBoxStyle.Information)
                    dtpExpiryDate.Focus()
                    Return False
                End If
            End If

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue"), enMsgBoxStyle.Information)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtpEffectiveDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim objPMovement As New clsEmployeeMovmentApproval
                Dim intPrivilegeId As Integer = 0
                Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                If mblnIsResidentPermit Then
                    intPrivilegeId = 1200
                    eMovement = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT
                Else
                    intPrivilegeId = 1199
                    eMovement = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                End If
                Dim strMsg As String = String.Empty
                strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, CInt(cboEmployee.SelectedValue))
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing


                'S.SANDEEP |16-JAN-2019| -- START
                If objEWPermit.isExist(mblnIsResidentPermit, dtpEffectiveDate.Value.Date, , CInt(cboEmployee.SelectedValue), mintTransactionId) Then 'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    If mblnIsResidentPermit Then
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 4, "Sorry, resident permit information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 1, "Sorry, work permit information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    End If
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objEWPermit.Get_Current_WorkPermit(dtpEffectiveDate.Value.Date, mblnIsResidentPermit, CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If dsList.Tables(0).Rows(0)("work_permit_no").ToString().Trim = txtWorkPermitNo.Text AndAlso CInt(dsList.Tables(0).Rows(0)("workcountryunkid")) = CInt(cboIssueCountry.SelectedValue) Then
                        If mblnIsResidentPermit Then
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 5, "Sorry, resident permit no is already present for the selected employee."), enMsgBoxStyle.Information)
                        Else
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 2, "Sorry, work permit no is already present for the selected employee."), enMsgBoxStyle.Information)
                        End If
                        Return False
                    End If
                End If
                dsList = Nothing

                If objEWPermit.isExist(mblnIsResidentPermit, Nothing, txtWorkPermitNo.Text, CInt(cboEmployee.SelectedValue), mintTransactionId) Then 'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    If mblnIsResidentPermit Then
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 2, "Sorry, resident permit no is already present for the selected employee."), enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 2, "Sorry, work permit no is already present for the selected employee."), enMsgBoxStyle.Information)
            End If
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If


            'S.SANDEEP [20-JUN-2018] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Valid_WorkPermit", mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objEWPermit._Effectivedate = dtpEffectiveDate.Value
            'objEWPermit._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'If dtpExpiryDate.Checked = False Then
            '    objEWPermit._Expiry_Date = Nothing
            'Else
            '    objEWPermit._Expiry_Date = dtpExpiryDate.Value
            'End If
            'objEWPermit._Isfromemployee = False
            'If dtpIssueDate.Checked = False Then
            '    objEWPermit._Issue_Date = Nothing
            'Else
            '    objEWPermit._Issue_Date = dtpIssueDate.Value
            'End If
            'objEWPermit._Issue_Place = txtPlaceofIssue.Text
            'objEWPermit._Isvoid = False
            'objEWPermit._Userunkid = User._Object._Userunkid
            'objEWPermit._Voiddatetime = Nothing
            'objEWPermit._Voidreason = ""
            'objEWPermit._Voiduserunkid = -1
            'objEWPermit._Work_Permit_No = txtWorkPermitNo.Text
            'objEWPermit._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
            'objEWPermit._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            ''S.SANDEEP [04-Jan-2018] -- START
            ''ISSUE/ENHANCEMENT : REF-ID # 120
            'objEWPermit._IsResidentPermit = mblnIsResidentPermit
            ''S.SANDEEP [04-Jan-2018] -- END

            'S.SANDEEP [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
            'S.SANDEEP |17-JAN-2019| -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                'S.SANDEEP |17-JAN-2019| -- END
                'S.SANDEEP [15-NOV-2018] -- END

            objEWPermit._Effectivedate = dtpEffectiveDate.Value
            objEWPermit._Employeeunkid = CInt(cboEmployee.SelectedValue)
            If dtpExpiryDate.Checked = False Then
                objEWPermit._Expiry_Date = Nothing
            Else
                objEWPermit._Expiry_Date = dtpExpiryDate.Value
            End If
            objEWPermit._Isfromemployee = False
            If dtpIssueDate.Checked = False Then
                objEWPermit._Issue_Date = Nothing
            Else
                objEWPermit._Issue_Date = dtpIssueDate.Value
            End If
            objEWPermit._Issue_Place = txtPlaceofIssue.Text
            objEWPermit._Isvoid = False
            objEWPermit._Userunkid = User._Object._Userunkid
            objEWPermit._Voiddatetime = Nothing
            objEWPermit._Voidreason = ""
            objEWPermit._Voiduserunkid = -1
            objEWPermit._Work_Permit_No = txtWorkPermitNo.Text
            objEWPermit._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
            objEWPermit._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            objEWPermit._IsResidentPermit = mblnIsResidentPermit
            'S.SANDEEP [04-Jan-2018] -- END

            Else

                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                objAPermit = New clsPermit_Approval_Tran
                'Gajanan [11-Dec-2019] -- End

                objAPermit._Audittype = enAuditType.ADD
                objAPermit._Audituserunkid = User._Object._Userunkid

                objAPermit._Effectivedate = dtpEffectiveDate.Value
                objAPermit._Employeeunkid = CInt(cboEmployee.SelectedValue)
                If dtpExpiryDate.Checked = False Then
                    objAPermit._Expiry_Date = Nothing
                Else
                    objAPermit._Expiry_Date = dtpExpiryDate.Value
                End If

                If dtpIssueDate.Checked = False Then
                    objAPermit._Issue_Date = Nothing
                Else
                    objAPermit._Issue_Date = dtpIssueDate.Value
                End If

                objAPermit._Issue_Place = txtPlaceofIssue.Text
                objAPermit._Isvoid = False
                objAPermit._Voidreason = ""
                objAPermit._Voiduserunkid = -1
                objAPermit._Work_Permit_No = txtWorkPermitNo.Text
                objAPermit._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
                objAPermit._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                objAPermit._Isresidentpermit = mblnIsResidentPermit
                'S.SANDEEP [04-Jan-2018] -- END
                objAPermit._Isvoid = False
                objAPermit._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objAPermit._Voidreason = ""
                objAPermit._Voiduserunkid = -1
                objAPermit._Tranguid = Guid.NewGuid.ToString()
                objAPermit._Transactiondate = Now
                objAPermit._Remark = ""
                objAPermit._Rehiretranunkid = 0
                objAPermit._Mappingunkid = 0
                objAPermit._Isweb = False
                objAPermit._Isfinal = False
                objAPermit._Ip = getIP()
                objAPermit._Hostname = getHostName()
                objAPermit._Form_Name = mstrModuleName

                'S.SANDEEP |17-JAN-2019| -- START
                If mAction = enAction.EDIT_ONE Then
                    objAPermit._Workpermittranunkid = mintTransactionId
                    objAPermit._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objAPermit._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetEditValue()
        Try
            dtpEffectiveDate.Value = objEWPermit._Effectivedate
            cboEmployee.SelectedValue = objEWPermit._Employeeunkid
            If objEWPermit._Expiry_Date <> Nothing Then
                dtpExpiryDate.Value = objEWPermit._Expiry_Date
            End If
            If objEWPermit._Issue_Date <> Nothing Then
                dtpIssueDate.Value = objEWPermit._Issue_Date
            End If
            txtPlaceofIssue.Text = objEWPermit._Issue_Place
            txtWorkPermitNo.Text = objEWPermit._Work_Permit_No
            cboIssueCountry.SelectedValue = objEWPermit._Workcountryunkid
            cboChangeReason.SelectedValue = objEWPermit._Changereasonunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetEditValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEffectiveDate.Checked = False
            dtpIssueDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpIssueDate.Checked = False
            dtpExpiryDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpExpiryDate.Checked = False
            txtPlaceofIssue.Text = ""
            txtWorkPermitNo.Text = ""
            cboChangeReason.SelectedValue = 0
            cboIssueCountry.SelectedValue = 0
            txtDate.Text = ""
            pnlData.Visible = False
            mdtAppointmentDate = Nothing

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            mintTransactionId = 0
            'Gajanan [11-Dec-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowToChangeEmpWorkPermit
            objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditWorkPermitEmployeeDetails
            objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteWorkPermitEmployeeDetails

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- End

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master

        Try
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
            End With

    
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region " Form's Events "

    Private Sub frmWorkPermitTransaction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEWPermit = New clsemployee_workpermit_tran

        'S.SANDEEP [20-JUN-2018] -- START
        'ISSUE/ENHANCEMENT : {Ref#244}
        objAPermit = New clsPermit_Approval_Tran
        'S.SANDEEP [20-JUN-2018] -- END

        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If mintEmployeeID > 0 Then
                cboEmployee.SelectedValue = mintEmployeeID
            End If
            'Gajanan [11-Dec-2019] -- End

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            If mblnIsResidentPermit = True Then
                Me.Text = Language.getMessage(mstrModuleName, 100, "Resident Permit")
                gbWPInformation.Text = Language.getMessage(mstrModuleName, 101, "Resident Permit Information")
                lblWorkPermitNo.Text = Language.getMessage(mstrModuleName, 102, "Permit No")
            End If
            'S.SANDEEP [04-Jan-2018] -- END


            pnlData.Visible = False
            'S.SANDEEP [14 APR 2015] -- START
            objlblCaption.Visible = False
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            lblPendingData.Visible = False
            'S.SANDEEP [20-JUN-2018] -- END

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            'btnSave.Enabled = User._Object.Privilege._AllowToChangeEmpWorkPermit
            'Shani (08-Dec-2016) -- End
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWorkPermitTransaction_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_workpermit_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_workpermit_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Valid_WorkPermit() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objEWPermit._FormName = mstrModuleName
            objEWPermit._LoginEmployeeunkid = 0
            objEWPermit._ClientIP = getIP()
            objEWPermit._HostName = getHostName()
            objEWPermit._FromWeb = False
            objEWPermit._AuditUserId = User._Object._Userunkid
objEWPermit._CompanyUnkid = Company._Object._Companyunkid
            objEWPermit._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If mAction = enAction.EDIT_ONE Then
                'blnFlag = objEWPermit.Update()

                'S.SANDEEP |17-JAN-2019| -- START
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    blnFlag = objEWPermit.Update(Nothing)
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    'Gajanan [9-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                    blnFlag = objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing)
                    'Gajanan [9-July-2019] -- End

                End If

                'Gajanan [9-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                'blnFlag = objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing)
                'Gajanan [9-July-2019] -- End

                'S.SANDEEP |17-JAN-2019| -- END

                If blnFlag = False AndAlso objAPermit._Message <> "" Then
                    eZeeMsgBox.Show(objAPermit._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'blnFlag = objEWPermit.Insert()
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                blnFlag = objEWPermit.Insert()
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmEmployeeBenefitCoverage
                        objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
                    End If
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED, Nothing)

                    If blnFlag = False AndAlso objAPermit._Message <> "" Then
                        eZeeMsgBox.Show(objAPermit._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If
                'S.SANDEEP [20-JUN-2018] -- END


            End If
            If blnFlag = False AndAlso objEWPermit._Message <> "" Then
                eZeeMsgBox.Show(objEWPermit._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                Call Fill_Grid()
                Call Set_WorkPermit()
                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'S.SANDEEP [15-NOV-2018] -- START
                'S.SANDEEP |17-JAN-2019| -- START
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
                    'S.SANDEEP |17-JAN-2019| -- END
                    'S.SANDEEP [15-NOV-2018] -- END
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    Dim intPrivilegeId As Integer = 0
                    Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                    If mblnIsResidentPermit Then
                        intPrivilegeId = 1200
                        eMovement = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT
                    Else
                        intPrivilegeId = 1199
                        eMovement = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                    End If
                    'S.SANDEEP |17-JAN-2019| -- START
                    'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, mblnIsResidentPermit, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If mAction = enAction.EDIT_ONE Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, eOperType, mblnIsResidentPermit, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    'S.SANDEEP |17-JAN-2019| -- END

                    objPMovement = Nothing
                End If
                'S.SANDEEP [20-JUN-2018] -- END
            End If
            Call ClearControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'S.SANDEEP [15-NOV-2018] -- START
            'S.SANDEEP |17-JAN-2019| -- START
            'mintTransactionId = 0
            'S.SANDEEP |17-JAN-2019| -- END
            'S.SANDEEP [15-NOV-2018] -- END
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objSearchReason.Click, objbtnSearchCountry.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objbtnSearchCountry.Name.ToUpper
                    xCbo = cboIssueCountry
                Case objSearchReason.Name.ToUpper
                    xCbo = cboChangeReason
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.WORK_PERMIT, enAction.ADD_ONE)
            If mblnIsResidentPermit Then
                frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.RESIDENT_PERMIT, enAction.ADD_ONE)
            Else
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.WORK_PERMIT, enAction.ADD_ONE)
            End If
            'S.SANDEEP [04-Jan-2018] -- END
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                'dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.WORK_PERMIT, True, "List")
                If mblnIsResidentPermit Then
                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RESIDENT_PERMIT, True, "List")
                Else
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.WORK_PERMIT, True, "List")
                End If
                'S.SANDEEP [04-Jan-2018] -- END
                With cboChangeReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString

            FillEmployeeCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region " Combobox Event(s) "


    'Pinkal (09-Apr-2015) -- Start
    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

    'Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboEmployee.KeyDown
    '    Try
    '        If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboEmployee.ValueMember
    '                .DisplayMember = cboEmployee.DisplayMember
    '                .DataSource = CType(cboEmployee.DataSource, DataTable)
    '                .CodeMember = "employeecode"
    '                .SelectedText = cboEmployee.Text
    '            End With
    '            If frm.DisplayDialog Then
    '                cboEmployee.SelectedValue = frm.SelectedValue
    '                If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
    '            Else
    '                cboEmployee.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (09-Apr-2015) -- End

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim xtmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                If xtmp.Length > 0 Then
                    mstrEmployeeCode = CStr(xtmp(0).Item("employeecode"))
                End If
                'S.SANDEEP [14 APR 2015] -- START
                objlblCaption.Visible = False
                'S.SANDEEP [14 APR 2015] -- END

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If btnSave.Enabled = False Then btnSave.Enabled = True
                If objdgcolhEdit.Visible = False Then objdgcolhEdit.Visible = True
                If objdgcolhDelete.Visible = False Then objdgcolhDelete.Visible = True
                'S.SANDEEP [04-AUG-2017] -- END

                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                If mAction = enAction.EDIT_ONE Then
                    If objEWPermit._Workpermittranunkid > 0 Then objEWPermit._Workpermittranunkid = 0
                    mAction = enAction.ADD_CONTINUE
                End If
                'S.SANDEEP [07-Feb-2018] -- END

                Call ClearControls()
                Call Fill_Grid()
                Call Set_WorkPermit()

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If ConfigParameter._Object._IsIncludeInactiveEmp Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date) = CInt(cboEmployee.SelectedValue)
                    If objEmp._Empl_Enddate <> Nothing Then
                        If objEmp._Empl_Enddate <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_From_Date <> Nothing Then
                        If objEmp._Termination_From_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_To_Date <> Nothing Then
                        If objEmp._Termination_To_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    objEmp = Nothing
                End If
                'S.SANDEEP [04-AUG-2017] -- END

            Else
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            mdtAppointmentDate = Nothing
            txtDate.Text = ""
            pnlData.Visible = False
            Select Case e.ColumnIndex
                'S.SANDEEP [09-AUG-2018] -- START
                Case objdgcolhViewPending.Index
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhViewPending.Index).Value Is imgInfo Then
                        Dim frm As New frmViewMovementApproval
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim intMovementId As Integer = 0
                        Dim intPrivilegeId As Integer = 0
                        If mblnIsResidentPermit Then
                            intMovementId = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT
                            intPrivilegeId = 1200
                        Else
                            intMovementId = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                            intPrivilegeId = 1199
                        End If

                        'S.SANDEEP |17-JAN-2019| -- START
                        'frm.displayDialog(User._Object._Userunkid, 0, intPrivilegeId, CType(intMovementId, clsEmployeeMovmentApproval.enMovementType), Nothing, mblnIsResidentPermit, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                        If mAction = enAction.EDIT_ONE Then
                            eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                        Else
                            eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                        End If
                        frm.displayDialog(User._Object._Userunkid, 0, intPrivilegeId, CType(intMovementId, clsEmployeeMovmentApproval.enMovementType), Nothing, mblnIsResidentPermit, eOperType, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        'S.SANDEEP |17-JAN-2019| -- END
                        If frm IsNot Nothing Then frm.Dispose()
                        Exit Sub
                    End If
                    'S.SANDEEP [09-AUG-2018] -- END

                Case objdgcolhEdit.Index
                    'S.SANDEEP |17-JAN-2019| -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgView Then
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhpermittranunkid.Index).Value) > 0 Then
                            Dim dr As DataRow() = CType(dgvHistory.DataSource, DataTable).Select(objdgcolhpermittranunkid.DataPropertyName & " = " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhpermittranunkid.Index).Value) & " AND " & objdgcolhtranguid.DataPropertyName & "= ''")
                            If dr.Length > 0 Then
                                Dim index As Integer = CType(dgvHistory.DataSource, DataTable).Rows.IndexOf(dr(0))
                                dgvHistory.Rows(index).Selected = True
                                dgvHistory.FirstDisplayedScrollingRowIndex = index
                            End If
                        End If
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                    mAction = enAction.EDIT_ONE
                    'S.SANDEEP [14 APR 2015] -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub
                    'S.SANDEEP [14 APR 2015] -- END

                    objEWPermit._Workpermittranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhpermittranunkid.Index).Value)

                    'S.SANDEEP [15-NOV-2018] -- START
                    mintTransactionId = objEWPermit._Workpermittranunkid
                    'S.SANDEEP [15-NOV-2018] -- END


                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, "", objAPermit._Employeeunkid, "", Nothing, True, mintTransactionId)
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    Call SetEditValue()
                    If CBool(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhFromEmp.Index).Value) = True Then
                        mdtAppointmentDate = eZeeDate.convertDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAppointdate.Index).Value.ToString)
                        txtDate.Text = mdtAppointmentDate.Date.ToShortDateString
                        pnlData.Visible = True
                    End If
                Case objdgcolhDelete.Index
                    mAction = enAction.ADD_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, "", objAPermit._Employeeunkid, "", Nothing, True, CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhpermittranunkid.Index).Value))
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objEWPermit._Isvoid = True
                    objEWPermit._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEWPermit._Voidreason = xStrVoidReason
                    objEWPermit._Voiduserunkid = User._Object._Userunkid
                    objEWPermit._Userunkid = User._Object._Userunkid
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        objAPermit._Isvoid = True
                        objAPermit._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objAPermit._Voidreason = xStrVoidReason
                        objAPermit._Voiduserunkid = -1
                        objAPermit._Audituserunkid = User._Object._Userunkid
                        objAPermit._Isweb = False
                        objAPermit._Ip = getIP()
                        objAPermit._Hostname = getHostName()
                        objAPermit._Form_Name = mstrModuleName
                        objAPermit._Audituserunkid = User._Object._Userunkid

                        'Gajanan [9-July-2019] -- Start      
                        'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                        objAPermit._Isresidentpermit = mblnIsResidentPermit
                        'Gajanan [9-July-2019] -- End   


                        If objAPermit.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhpermittranunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED, Company._Object._Companyunkid, Nothing) = False Then
                            If objAPermit._Message <> "" Then
                                eZeeMsgBox.Show(objAPermit._Message, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If
                    Else
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objEWPermit._FormName = mstrModuleName
                    objEWPermit._LoginEmployeeunkid = 0
                    objEWPermit._ClientIP = getIP()
                    objEWPermit._HostName = getHostName()
                    objEWPermit._FromWeb = False
                    objEWPermit._AuditUserId = User._Object._Userunkid
                    objEWPermit._CompanyUnkid = Company._Object._Companyunkid
                    objEWPermit._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objEWPermit.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhpermittranunkid.Index).Value)) = False Then
                        If objEWPermit._Message <> "" Then
                            eZeeMsgBox.Show(objEWPermit._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                    End If


                    Call Fill_Grid()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHistory.DataBindingComplete
        Try
            For Each xdgvr As DataGridViewRow In dgvHistory.Rows
                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                'xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                'End If
                If CStr(xdgvr.Cells(objdgcolhtranguid.Index).Value).Trim.Length > 0 Then
                    xdgvr.DefaultCellStyle.BackColor = Color.PowderBlue
                    xdgvr.DefaultCellStyle.ForeColor = Color.Black
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    'S.SANDEEP [09-AUG-2018] -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).Value = imgInfo
                    'S.SANDEEP [09-AUG-2018] -- END
                    lblPendingData.Visible = True


                    'Gajanan [9-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                    xdgvr.Cells(objdgcolhViewPending.Index).ToolTipText = xdgvr.Cells(objdgcolhOperationType.Index).Value.ToString
                    If CInt(xdgvr.Cells(objdgcolhOperationTypeId.Index).Value) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        xdgvr.Cells(objdgcolhEdit.Index).Value = imgView
                    End If
                    'Gajanan [9-July-2019] -- End
                Else
                If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                End If
                End If
                'S.SANDEEP [20-JUN-2018] -- END

                'S.SANDEEP [14 APR 2015] -- START
                If CInt(xdgvr.Cells(objdgcolhrehiretranunkid.Index).Value) > 0 Then
                    For xCellIdx As Integer = 0 To xdgvr.Cells.Count - 1
                        If xCellIdx > 1 Then
                            xdgvr.Cells(xCellIdx).Style.BackColor = Color.Orange
                            xdgvr.Cells(xCellIdx).Style.ForeColor = Color.Black
                        End If
                    Next
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    objlblCaption.Visible = True
                End If
                'S.SANDEEP [14 APR 2015] -- END

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbWPInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbWPInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbWPInformation.Text = Language._Object.getCaption(Me.gbWPInformation.Name, Me.gbWPInformation.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblPlaceOfIssue.Text = Language._Object.getCaption(Me.lblPlaceOfIssue.Name, Me.lblPlaceOfIssue.Text)
            Me.lblExpiryDate.Text = Language._Object.getCaption(Me.lblExpiryDate.Name, Me.lblExpiryDate.Text)
            Me.lblIssueDate.Text = Language._Object.getCaption(Me.lblIssueDate.Name, Me.lblIssueDate.Text)
            Me.lblIssueCountry.Text = Language._Object.getCaption(Me.lblIssueCountry.Name, Me.lblIssueCountry.Text)
            Me.lblWorkPermitNo.Text = Language._Object.getCaption(Me.lblWorkPermitNo.Name, Me.lblWorkPermitNo.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.Name, Me.lblAppointmentdate.Text)
            Me.dgcolhChangeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChangeDate.Name, Me.dgcolhChangeDate.HeaderText)
            Me.dgcolhPermitNo.HeaderText = Language._Object.getCaption(Me.dgcolhPermitNo.Name, Me.dgcolhPermitNo.HeaderText)
            Me.dgcolhCountryName.HeaderText = Language._Object.getCaption(Me.dgcolhCountryName.Name, Me.dgcolhCountryName.HeaderText)
            Me.dgcolhIssuePlace.HeaderText = Language._Object.getCaption(Me.dgcolhIssuePlace.Name, Me.dgcolhIssuePlace.HeaderText)
            Me.dgcolhIssueDate.HeaderText = Language._Object.getCaption(Me.dgcolhIssueDate.Name, Me.dgcolhIssueDate.HeaderText)
            Me.dgcolhExpiryDate.HeaderText = Language._Object.getCaption(Me.dgcolhExpiryDate.Name, Me.dgcolhExpiryDate.HeaderText)
            Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Work Permit No. is mandatory information. Please enter work permit no.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Issue Country is mandatory information. Please select issue country.")
            Language.setMessage(mstrModuleName, 5, "Sorry, issue date and expiry date are madatory information.")
            Language.setMessage(mstrModuleName, 6, "Sorry, expiry date cannot be less or equal to issue date.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue")
            Language.setMessage(mstrModuleName, 8, "Sorry, This is system generate entry. Please set effective date as employee appointment date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class