﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeAbsents
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeAbsents))
        Me.pnlEmployeeabsent = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnInsert = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmployeeAbsent = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblPeriod = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblPayrollPeriod = New System.Windows.Forms.Label
        Me.cboPayrollPeriod = New System.Windows.Forms.ComboBox
        Me.objbgwProcessEmpAbsent = New System.ComponentModel.BackgroundWorker
        Me.btnUnProcessedEmployees = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlEmployeeabsent.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbEmployeeAbsent.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlEmployeeabsent
        '
        Me.pnlEmployeeabsent.Controls.Add(Me.objFooter)
        Me.pnlEmployeeabsent.Controls.Add(Me.gbEmployeeAbsent)
        Me.pnlEmployeeabsent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmployeeabsent.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmployeeabsent.Name = "pnlEmployeeabsent"
        Me.pnlEmployeeabsent.Size = New System.Drawing.Size(399, 160)
        Me.pnlEmployeeabsent.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnInsert)
        Me.objFooter.Controls.Add(Me.btnUnProcessedEmployees)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 105)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(399, 55)
        Me.objFooter.TabIndex = 10
        '
        'objlblProgress
        '
        Me.objlblProgress.BackColor = System.Drawing.Color.Transparent
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.ForeColor = System.Drawing.Color.Blue
        Me.objlblProgress.Location = New System.Drawing.Point(210, 4)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(169, 18)
        Me.objlblProgress.TabIndex = 15
        Me.objlblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(298, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnInsert
        '
        Me.btnInsert.BackColor = System.Drawing.Color.White
        Me.btnInsert.BackgroundImage = CType(resources.GetObject("btnInsert.BackgroundImage"), System.Drawing.Image)
        Me.btnInsert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInsert.BorderColor = System.Drawing.Color.Empty
        Me.btnInsert.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInsert.FlatAppearance.BorderSize = 0
        Me.btnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInsert.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInsert.ForeColor = System.Drawing.Color.Black
        Me.btnInsert.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInsert.GradientForeColor = System.Drawing.Color.Black
        Me.btnInsert.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInsert.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInsert.Location = New System.Drawing.Point(202, 13)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInsert.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInsert.Size = New System.Drawing.Size(90, 30)
        Me.btnInsert.TabIndex = 2
        Me.btnInsert.Text = "&Insert"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'gbEmployeeAbsent
        '
        Me.gbEmployeeAbsent.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeAbsent.Checked = False
        Me.gbEmployeeAbsent.CollapseAllExceptThis = False
        Me.gbEmployeeAbsent.CollapsedHoverImage = Nothing
        Me.gbEmployeeAbsent.CollapsedNormalImage = Nothing
        Me.gbEmployeeAbsent.CollapsedPressedImage = Nothing
        Me.gbEmployeeAbsent.CollapseOnLoad = False
        Me.gbEmployeeAbsent.Controls.Add(Me.objlblPeriod)
        Me.gbEmployeeAbsent.Controls.Add(Me.lblPeriod)
        Me.gbEmployeeAbsent.Controls.Add(Me.objlblProgress)
        Me.gbEmployeeAbsent.Controls.Add(Me.lblPayrollPeriod)
        Me.gbEmployeeAbsent.Controls.Add(Me.cboPayrollPeriod)
        Me.gbEmployeeAbsent.ExpandedHoverImage = Nothing
        Me.gbEmployeeAbsent.ExpandedNormalImage = Nothing
        Me.gbEmployeeAbsent.ExpandedPressedImage = Nothing
        Me.gbEmployeeAbsent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeAbsent.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeAbsent.HeaderHeight = 25
        Me.gbEmployeeAbsent.HeaderMessage = ""
        Me.gbEmployeeAbsent.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeAbsent.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeAbsent.HeightOnCollapse = 0
        Me.gbEmployeeAbsent.LeftTextSpace = 0
        Me.gbEmployeeAbsent.Location = New System.Drawing.Point(7, 7)
        Me.gbEmployeeAbsent.Name = "gbEmployeeAbsent"
        Me.gbEmployeeAbsent.OpenHeight = 300
        Me.gbEmployeeAbsent.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeAbsent.ShowBorder = True
        Me.gbEmployeeAbsent.ShowCheckBox = False
        Me.gbEmployeeAbsent.ShowCollapseButton = False
        Me.gbEmployeeAbsent.ShowDefaultBorderColor = True
        Me.gbEmployeeAbsent.ShowDownButton = False
        Me.gbEmployeeAbsent.ShowHeader = True
        Me.gbEmployeeAbsent.Size = New System.Drawing.Size(383, 93)
        Me.gbEmployeeAbsent.TabIndex = 4
        Me.gbEmployeeAbsent.Temp = 0
        Me.gbEmployeeAbsent.Text = "Employee Absents"
        Me.gbEmployeeAbsent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblPeriod
        '
        Me.objlblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblPeriod.Location = New System.Drawing.Point(103, 64)
        Me.objlblPeriod.Name = "objlblPeriod"
        Me.objlblPeriod.Size = New System.Drawing.Size(263, 18)
        Me.objlblPeriod.TabIndex = 36
        Me.objlblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(11, 64)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(86, 18)
        Me.lblPeriod.TabIndex = 35
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayrollPeriod
        '
        Me.lblPayrollPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayrollPeriod.Location = New System.Drawing.Point(11, 37)
        Me.lblPayrollPeriod.Name = "lblPayrollPeriod"
        Me.lblPayrollPeriod.Size = New System.Drawing.Size(86, 18)
        Me.lblPayrollPeriod.TabIndex = 32
        Me.lblPayrollPeriod.Text = "Payroll Period"
        Me.lblPayrollPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayrollPeriod
        '
        Me.cboPayrollPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayrollPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayrollPeriod.FormattingEnabled = True
        Me.cboPayrollPeriod.Location = New System.Drawing.Point(103, 36)
        Me.cboPayrollPeriod.Name = "cboPayrollPeriod"
        Me.cboPayrollPeriod.Size = New System.Drawing.Size(131, 21)
        Me.cboPayrollPeriod.TabIndex = 1
        '
        'objbgwProcessEmpAbsent
        '
        Me.objbgwProcessEmpAbsent.WorkerReportsProgress = True
        Me.objbgwProcessEmpAbsent.WorkerSupportsCancellation = True
        '
        'btnUnProcessedEmployees
        '
        Me.btnUnProcessedEmployees.BackColor = System.Drawing.Color.White
        Me.btnUnProcessedEmployees.BackgroundImage = CType(resources.GetObject("btnUnProcessedEmployees.BackgroundImage"), System.Drawing.Image)
        Me.btnUnProcessedEmployees.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnProcessedEmployees.BorderColor = System.Drawing.Color.Empty
        Me.btnUnProcessedEmployees.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnProcessedEmployees.FlatAppearance.BorderSize = 0
        Me.btnUnProcessedEmployees.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnProcessedEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnProcessedEmployees.ForeColor = System.Drawing.Color.Black
        Me.btnUnProcessedEmployees.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnProcessedEmployees.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnProcessedEmployees.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnProcessedEmployees.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnProcessedEmployees.Location = New System.Drawing.Point(7, 13)
        Me.btnUnProcessedEmployees.Name = "btnUnProcessedEmployees"
        Me.btnUnProcessedEmployees.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnProcessedEmployees.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnProcessedEmployees.Size = New System.Drawing.Size(164, 30)
        Me.btnUnProcessedEmployees.TabIndex = 16
        Me.btnUnProcessedEmployees.Text = "Unprocessed Employee(s)"
        Me.btnUnProcessedEmployees.UseVisualStyleBackColor = True
        '
        'frmEmployeeAbsents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 160)
        Me.Controls.Add(Me.pnlEmployeeabsent)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeAbsents"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Absents"
        Me.pnlEmployeeabsent.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbEmployeeAbsent.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlEmployeeabsent As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnInsert As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeAbsent As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPayrollPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPayrollPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objlblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbgwProcessEmpAbsent As System.ComponentModel.BackgroundWorker
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents btnUnProcessedEmployees As eZee.Common.eZeeLightButton
End Class
