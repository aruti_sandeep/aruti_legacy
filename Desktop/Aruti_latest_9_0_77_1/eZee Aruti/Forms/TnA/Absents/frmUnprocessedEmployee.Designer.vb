﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUnprocessedEmployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUnprocessedEmployee))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.dgUnprocessedEmployee = New System.Windows.Forms.DataGridView
        Me.objlblPeriodduration = New System.Windows.Forms.Label
        Me.lblPeriodDuration = New System.Windows.Forms.Label
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.objLblPeriodName = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        CType(Me.dgUnprocessedEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 349)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(360, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(262, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnProcess
        '
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(166, 13)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(90, 30)
        Me.btnProcess.TabIndex = 2
        Me.btnProcess.Text = "Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.dgUnprocessedEmployee)
        Me.pnlMain.Location = New System.Drawing.Point(6, 58)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(346, 285)
        Me.pnlMain.TabIndex = 12
        '
        'dgUnprocessedEmployee
        '
        Me.dgUnprocessedEmployee.AllowUserToAddRows = False
        Me.dgUnprocessedEmployee.AllowUserToDeleteRows = False
        Me.dgUnprocessedEmployee.AllowUserToResizeColumns = False
        Me.dgUnprocessedEmployee.AllowUserToResizeRows = False
        Me.dgUnprocessedEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgUnprocessedEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgUnprocessedEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhEmpCode, Me.dgcolhEmployee, Me.objdgcolhEmployeeId})
        Me.dgUnprocessedEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgUnprocessedEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgUnprocessedEmployee.Name = "dgUnprocessedEmployee"
        Me.dgUnprocessedEmployee.ReadOnly = True
        Me.dgUnprocessedEmployee.RowHeadersVisible = False
        Me.dgUnprocessedEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgUnprocessedEmployee.Size = New System.Drawing.Size(346, 285)
        Me.dgUnprocessedEmployee.TabIndex = 0
        '
        'objlblPeriodduration
        '
        Me.objlblPeriodduration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblPeriodduration.Location = New System.Drawing.Point(97, 34)
        Me.objlblPeriodduration.Name = "objlblPeriodduration"
        Me.objlblPeriodduration.Size = New System.Drawing.Size(255, 18)
        Me.objlblPeriodduration.TabIndex = 38
        Me.objlblPeriodduration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriodDuration
        '
        Me.lblPeriodDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodDuration.Location = New System.Drawing.Point(5, 34)
        Me.lblPeriodDuration.Name = "lblPeriodDuration"
        Me.lblPeriodDuration.Size = New System.Drawing.Size(84, 18)
        Me.lblPeriodDuration.TabIndex = 37
        Me.lblPeriodDuration.Text = "Period"
        Me.lblPeriodDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblPeriod
        '
        Me.LblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(5, 9)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(84, 18)
        Me.LblPeriod.TabIndex = 39
        Me.LblPeriod.Text = "Period Name"
        Me.LblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLblPeriodName
        '
        Me.objLblPeriodName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLblPeriodName.Location = New System.Drawing.Point(97, 9)
        Me.objLblPeriodName.Name = "objLblPeriodName"
        Me.objLblPeriodName.Size = New System.Drawing.Size(255, 18)
        Me.objLblPeriodName.TabIndex = 40
        Me.objLblPeriodName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 125
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 240
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "EmployeeId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'dgcolhEmpCode
        '
        Me.dgcolhEmpCode.HeaderText = "Employee Code"
        Me.dgcolhEmpCode.Name = "dgcolhEmpCode"
        Me.dgcolhEmpCode.ReadOnly = True
        Me.dgcolhEmpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpCode.Width = 125
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "EmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeId.Visible = False
        '
        'frmUnprocessedEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(360, 404)
        Me.Controls.Add(Me.objLblPeriodName)
        Me.Controls.Add(Me.LblPeriod)
        Me.Controls.Add(Me.objlblPeriodduration)
        Me.Controls.Add(Me.lblPeriodDuration)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.objFooter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUnprocessedEmployee"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Unprocessed Employee List"
        Me.objFooter.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        CType(Me.dgUnprocessedEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents dgUnprocessedEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objlblPeriodduration As System.Windows.Forms.Label
    Friend WithEvents lblPeriodDuration As System.Windows.Forms.Label
    Friend WithEvents LblPeriod As System.Windows.Forms.Label
    Friend WithEvents objLblPeriodName As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
