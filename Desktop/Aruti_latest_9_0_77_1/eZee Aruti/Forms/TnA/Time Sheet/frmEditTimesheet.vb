﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEditTimesheet

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmEditTimesheet"
    Private mblnCancel As Boolean = True
    Private objTimeSheet As clslogin_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTimesheetUnkid As Integer = -1
    Private mintEmployeeID As Integer = -1
    Private mintShiftID As Integer = -1
    Private mintPolicyId As Integer = -1
    Private mblnIsRoundOff As Boolean = False
    Private mdtLoginDate As DateTime = Nothing
    Private mdtDays As DataTable = Nothing
    Private mblnIsChange As Boolean = False
    Private mblnIsProperRoundOff As Boolean = True
    Private mstrRoundOffMsg As String = ""
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal intEmployeeId As Integer, ByVal intShiftId As Integer, ByVal _dLoginDate As DateTime, _
                                             ByVal intPolicyId As Integer, ByVal mstrFormName As String, Optional ByVal blIsRoundOff As Boolean = False) As Boolean
        Try
            mintTimesheetUnkid = intUnkId
            mintEmployeeID = intEmployeeId
            mintShiftID = intShiftId
            mintPolicyId = intPolicyId
            mblnIsRoundOff = blIsRoundOff
            mdtLoginDate = _dLoginDate
            Me.Text = mstrFormName
            Me.ShowDialog()
            intUnkId = mintTimesheetUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmEditTimesheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTimeSheet = New clslogin_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = mintEmployeeID
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintEmployeeID
            'S.SANDEEP [04 JUN 2015] -- END

            LblEmpCodeValue.Text = objEmployee._Employeecode
            LblEmpValue.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            Dim objShift As New clsNewshift_master
            objShift._Shiftunkid = mintShiftID
            LblShiftValue.Text = objShift._Shiftname

            If ConfigParameter._Object._PolicyManagementTNA Then
                Dim objPolicy As New clspolicy_master
                objPolicy._Policyunkid = mintPolicyId
                LblPolicyValue.Text = objPolicy._Policyname
            Else
                LblPolicy.Visible = False
                LblPolicyValue.Visible = False
            End If

            objTimeSheet._Employeeunkid = mintEmployeeID
            objTimeSheet._Logindate = mdtLoginDate.Date

            If mblnIsRoundOff Then
                FillGridForRoundOff()
            Else
                FillGridForEdit()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEditTimesheet_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEditTimesheet_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEditTimesheet_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            objfrm.displayDialog(Me)

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmEditTimesheet_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objTimeSheet = Nothing
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If dgvTimeSheet.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no record(s) for editing."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If mdtDays Is Nothing Then Exit Sub

            ' START USED WHEN USER CAN'T EDIT OR ROUNDOFF ANYTHING 
            If mblnIsChange = False Then
                mblnCancel = False
                Me.Close()
                Exit Sub
            End If
            ' END USED WHEN USER CAN'T EDIT OR ROUNDOFF ANYTHING 


            For i As Integer = 0 To mdtDays.Rows.Count - 1
                If IsValid(i) = False Then
                    Exit Sub
                End If
            Next

            objTimeSheet._Employeeunkid = mintEmployeeID
            objTimeSheet._Userunkid = User._Object._Userunkid
            If mblnIsRoundOff = False Then
                objTimeSheet._OperationType = enTnAOperation.Edit
            ElseIf mblnIsRoundOff Then
                objTimeSheet._OperationType = enTnAOperation.RoundOff
            End If

            objTimeSheet._Shiftunkid = mintShiftID
            objTimeSheet._PolicyId = mintPolicyId

            Dim blnFlag As Boolean = False
            If mblnIsRoundOff Then

                If mblnIsProperRoundOff = False AndAlso mstrRoundOffMsg.ToString().Trim <> "" Then
                    eZeeMsgBox.Show(mstrRoundOffMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                'blnFlag = objTimeSheet.UpdateForRoundOffOperation(mdtDays, enTnAOperation.RoundOff, FinancialYear._Object._DatabaseName _
                '                                                                              , User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                                              , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                '                                                                              , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                '                                                  ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, "", "")

'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTimeSheet._FormName = mstrModuleName
                objTimeSheet._LoginEmployeeUnkid = 0
                objTimeSheet._ClientIP = getIP()
                objTimeSheet._HostName = getHostName()
                objTimeSheet._FromWeb = False
                objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                blnFlag = objTimeSheet.UpdateForRoundOffOperation(mdtDays, enTnAOperation.RoundOff, FinancialYear._Object._DatabaseName _
                                                                                              , User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                             , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                             , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                             , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                             , ConfigParameter._Object._PolicyManagementTNA _
                                                                                             , ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                                             , ConfigParameter._Object._IsHolidayConsiderOnWeekend, ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                                             , ConfigParameter._Object._IsHolidayConsiderOnDayoff, False, -1, "", "")

                'Pinkal (11-AUG-2017) -- End


            Else


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                'blnFlag = objTimeSheet.UpdateForEditOperation(mdtDays, enTnAOperation.Edit, FinancialYear._Object._DatabaseName _
                '                                                                        , User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                                        , Company._Object._Companyunkid, mdtLoginDate, mdtLoginDate _
                '                                                                        , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                '                                                                        , ConfigParameter._Object._PolicyManagementTNA, _
                '                                              ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, "", "")

'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTimeSheet._FormName = mstrModuleName
                objTimeSheet._LoginEmployeeUnkid = 0
                objTimeSheet._ClientIP = getIP()
                objTimeSheet._HostName = getHostName()
                objTimeSheet._FromWeb = False
                objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                blnFlag = objTimeSheet.UpdateForEditOperation(mdtDays, enTnAOperation.Edit, FinancialYear._Object._DatabaseName _
                                                                                        , User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                        , Company._Object._Companyunkid, mdtLoginDate, mdtLoginDate _
                                                                                        , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                        , ConfigParameter._Object._PolicyManagementTNA _
                                                                                        , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                                        , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                                        , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                                        , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                                        , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                                        , False, -1, "", "")

                'Pinkal (11-AUG-2017) -- End

            End If

            If blnFlag = False AndAlso objTimeSheet._Message <> "" Then
                eZeeMsgBox.Show(objTimeSheet._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                mblnCancel = False
            End If

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillGridForEdit()
        Try
            dgvTimeSheet.Rows.Clear()

            Dim dsList As DataSet = objTimeSheet.GetList("List", True)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("AUD") = False Then
                dsList.Tables(0).Columns.Add("AUD", Type.GetType("System.String"))
                dsList.Tables(0).Columns("AUD").DefaultValue = ""
            End If


            For Each dtrow As DataRow In dsList.Tables(0).Rows
                Dim dgvRow As New DataGridViewRow
                dgvRow.CreateCells(dgvTimeSheet)
                dgvRow.Cells(objcolhLoginunkid.Index).Value = dtrow.Item("loginunkid")

                If Not dtrow.Item("checkintime") Is DBNull.Value Then
                    dgvRow.Cells(dgcolhStarttime.Index).Value = (CDate(dtrow.Item("checkintime"))).ToShortTimeString
                    dgvRow.Cells(dgcolhStarttime.Index).Tag = CDate(dtrow.Item("checkintime")).Date 'Sohail (10 Dec 2013)
                End If
                If Not dtrow.Item("checkouttime") Is DBNull.Value Then
                    dgvRow.Cells(dgcolhEndtime.Index).Value = (CDate(dtrow.Item("checkouttime"))).ToShortTimeString
                    dgvRow.Cells(dgcolhEndtime.Index).Tag = CDate(dtrow.Item("checkouttime")).Date 'Sohail (10 Dec 2013)
                End If

                dgvRow.Cells(dgcolhLoginDate.Index).Value = CDate(dtrow.Item("logindate")).ToShortDateString
                dgvRow.Cells(dgcolhWorkingHours.Index).Value = CalculateTime(True, CInt(dtrow.Item("workhour"))).ToString("#0.00")
                dgvTimeSheet.Rows.Add(dgvRow)
            Next

            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                objTotalHours.Text = CalculateTime(True, CInt(dsList.Tables(0).Compute("sum(workhour)", "isvoid = 0"))).ToString("#0.00")
                objBreak.Text = CStr(CalculateTime(False, CInt(dsList.Tables(0).Compute("sum(breakhr)", "isvoid = 0")))) & " Mins"


                'Pinkal (28-Jan-2014) -- Start
                'Enhancement : Oman Changes
                objTeaBreak.Text = CStr(CalculateTime(False, CInt(dsList.Tables(0).Compute("sum(teahr)", "isvoid = 0")))) & " Mins"
                'Pinkal (28-Jan-2014) -- End

            Else
                objTotalHours.Text = "0"
                objBreak.Text = "0 Mins"


                'Pinkal (28-Jan-2014) -- Start
                'Enhancement : Oman Changes
                objTeaBreak.Text = "0 Mins"
                'Pinkal (28-Jan-2014) -- End

            End If

            mdtDays = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGridForEdit", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGridForRoundOff()
        Try
            dgvTimeSheet.Rows.Clear()

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dtList As DataTable = objTimeSheet.GetEmployeeLoginData(mintEmployeeID, mdtLoginDate.Date, mdtLoginDate.Date)
            Dim dtList As DataTable = objTimeSheet.GetEmployeeLoginData(mintEmployeeID, mdtLoginDate.Date, mdtLoginDate.Date, , , ConfigParameter._Object._PolicyManagementTNA)
            'S.SANDEEP [04 JUN 2015] -- END



            For Each dtrow As DataRow In dtList.Rows
                Dim dgvRow As New DataGridViewRow
                dgvRow.CreateCells(dgvTimeSheet)
                dgvRow.Cells(objcolhLoginunkid.Index).Value = dtrow.Item("loginunkid")

                If Not dtrow.Item("checkintime") Is DBNull.Value Then
                    dgvRow.Cells(dgcolhStarttime.Index).Value = (CDate(dtrow.Item("checkintime"))).ToShortTimeString
                    dgvRow.Cells(objcolhInTime.Index).Value = (CDate(dtrow.Item("checkintime"))).ToShortTimeString
                    dgvRow.Cells(dgcolhStarttime.Index).Tag = CDate(dtrow.Item("checkintime")).Date 'Sohail (10 Dec 2013)
                End If
                If Not dtrow.Item("checkouttime") Is DBNull.Value Then
                    dgvRow.Cells(dgcolhEndtime.Index).Value = (CDate(dtrow.Item("checkouttime"))).ToShortTimeString
                    dgvRow.Cells(objColhOuttime.Index).Value = (CDate(dtrow.Item("checkouttime"))).ToShortTimeString
                    dgvRow.Cells(dgcolhEndtime.Index).Tag = CDate(dtrow.Item("checkouttime")).Date 'Sohail (10 Dec 2013)
                End If
                dgvRow.Cells(dgcolhLoginDate.Index).Value = CDate(dtrow.Item("logindate")).ToShortDateString
                dgvRow.Cells(dgcolhWorkingHours.Index).Value = CalculateTime(True, CInt(dtrow.Item("workhour"))).ToString("#0.00")
                dgvTimeSheet.Rows.Add(dgvRow)
            Next

            If Not dtList Is Nothing And dtList.Rows.Count > 0 Then
                objTotalHours.Text = CalculateTime(True, CInt(dtList.Compute("sum(workhour)", "1=1"))).ToString("#0.00")
                objBreak.Text = CStr(CalculateTime(False, CInt(dtList.Compute("sum(breakhr)", "1=1")))) & " Mins"
            Else
                objTotalHours.Text = "0"
                objBreak.Text = "0 Mins"
            End If

            mdtDays = dtList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGridForRoundOff", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid(ByVal intRowIndex As Integer) As Boolean
        Try
            If IsDate(dgvTimeSheet.Rows(intRowIndex).Cells(dgcolhStarttime.Index).Value) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Invalid Checkin time.Please Enter Proper Checkin Time."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(intRowIndex).Cells(dgcolhStarttime.Index)
                dgvTimeSheet.BeginEdit(True)
                Return False

            ElseIf IsDate(dgvTimeSheet.Rows(intRowIndex).Cells(dgcolhEndtime.Index).Value) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Invalid Checkout time.Please Enter Proper Checkout Time."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(intRowIndex).Cells(dgcolhEndtime.Index)
                dgvTimeSheet.BeginEdit(True)
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

#Region "Datagrid Event"

    Private Sub dgvTimeSheet_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvTimeSheet.CellValidating
        Try

            If e.ColumnIndex = dgcolhStarttime.Index OrElse e.ColumnIndex = dgcolhEndtime.Index Then
                If e.FormattedValue.ToString().Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Invalid time.Please Enter Valid time."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgvTimeSheet.CancelEdit()
                    dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    dgvTimeSheet.BeginEdit(True)
                    e.Cancel = True
                    Exit Sub
                ElseIf IsDate(e.FormattedValue.ToString().Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Invalid time.Please Enter Valid time."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgvTimeSheet.CancelEdit()
                    dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    dgvTimeSheet.BeginEdit(True)
                    e.Cancel = True
                    Exit Sub
                End If
            End If

            If mblnIsRoundOff Then

                Dim objPolicyTran As New clsPolicy_tran
                objPolicyTran.GetPolicyTran(mintPolicyId)
                Dim drPolicy() As DataRow = objPolicyTran._DataList.Select("dayid = " & Weekday(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value)))

                If drPolicy.Length > 0 Then
                    Dim mdtDuetime As DateTime = Nothing

                    If e.ColumnIndex = dgcolhStarttime.Index Then

                        'Sohail (10 Dec 2013) -- Start
                        'Enhancement - OMAN
                        'Dim mdtStarttime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(objcolhInTime.Index).Value).ToString("hh:mm tt"))
                        'Dim mdtFStarttime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & CDate(e.FormattedValue).ToString("hh:mm tt"))
                        Dim mdtStarttime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhStarttime.Index).Tag).Date & " " & CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(objcolhInTime.Index).Value).ToString("hh:mm tt"))
                        Dim mdtFStarttime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhStarttime.Index).Tag).Date & " " & CDate(e.FormattedValue).ToString("hh:mm tt"))
                        'Sohail (10 Dec 2013) -- End

                        If mdtFStarttime < mdtStarttime Then

                            'Sohail (10 Dec 2013) -- Start
                            'Enhancement - OMAN
                            'mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & mdtStarttime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("ltcome_graceinsec")) * -1)
                            mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhStarttime.Index).Tag).Date & " " & mdtStarttime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("ltcome_graceinsec")) * -1)
                            'Sohail (10 Dec 2013) -- End

                            If mdtDuetime > mdtFStarttime Then
                                mblnIsProperRoundOff = False
                                mstrRoundOffMsg = Language.getMessage(mstrModuleName, 4, "You can't round off the start time.Reason: Given gracing is greater than late coming gracing as per the assigned policy setting.")
                                dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhStarttime.Index)
                                dgvTimeSheet.BeginEdit(True)
                                eZeeMsgBox.Show(mstrRoundOffMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Exit Sub
                            Else
                                mblnIsProperRoundOff = True
                                mstrRoundOffMsg = ""
                            End If

                        ElseIf mdtFStarttime > mdtStarttime Then

                            'Sohail (10 Dec 2013) -- Start
                            'Enhancement - OMAN
                            'mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & mdtStarttime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("elrcome_graceinsec")))
                            mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhStarttime.Index).Tag).Date & " " & mdtStarttime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("elrcome_graceinsec")))
                            'Sohail (10 Dec 2013) -- End

                            If mdtDuetime < mdtFStarttime Then
                                mblnIsProperRoundOff = False
                                mstrRoundOffMsg = Language.getMessage(mstrModuleName, 5, "You can't round off the start time.Reason: Given gracing is greater than early coming gracing as per the assigned policy setting.")
                                dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhStarttime.Index)
                                dgvTimeSheet.BeginEdit(True)
                                eZeeMsgBox.Show(mstrRoundOffMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Exit Sub
                            Else
                                mblnIsProperRoundOff = True
                                mstrRoundOffMsg = ""
                            End If

                        End If

                    ElseIf e.ColumnIndex = dgcolhEndtime.Index Then

                        'Sohail (10 Dec 2013) -- Start
                        'Enhancement - OMAN
                        'Dim mdtFEndtime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & CDate(e.FormattedValue).ToString("hh:mm tt"))
                        'Dim mdtEndtime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(objColhOuttime.Index).Value).ToString("hh:mm tt"))
                        Dim mdtFEndtime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhEndtime.Index).Tag).Date & " " & CDate(e.FormattedValue).ToString("hh:mm tt"))
                        Dim mdtEndtime As DateTime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhEndtime.Index).Tag).Date & " " & CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(objColhOuttime.Index).Value).ToString("hh:mm tt"))
                        'Sohail (10 Dec 2013) -- End

                        If mdtFEndtime < mdtEndtime Then

                            'Sohail (10 Dec 2013) -- Start
                            'Enhancement - OMAN
                            'mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & mdtEndtime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("ltgoing_graceinsec")) * -1)
                            mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhEndtime.Index).Tag).Date & " " & mdtEndtime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("ltgoing_graceinsec")) * -1)
                            'Sohail (10 Dec 2013) -- End

                            If mdtDuetime > mdtFEndtime Then
                                mblnIsProperRoundOff = False
                                mstrRoundOffMsg = Language.getMessage(mstrModuleName, 6, "You can't round off the end time.Reason: Given gracing is greater than late going gracing as per the assigned policy setting.")
                                dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhEndtime.Index)
                                dgvTimeSheet.BeginEdit(True)
                                eZeeMsgBox.Show(mstrRoundOffMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Exit Sub
                            Else
                                mblnIsProperRoundOff = True
                                mstrRoundOffMsg = ""
                            End If

                        ElseIf CDate(e.FormattedValue) > CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhEndtime.Index).Value) Then

                            'Sohail (10 Dec 2013) -- Start
                            'Enhancement - OMAN
                            'mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhLoginDate.Index).Value).Date & " " & mdtEndtime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("elrgoing_graceinsec")))
                            mdtDuetime = CDate(CDate(dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhEndtime.Index).Tag).Date & " " & mdtEndtime.ToString("hh:mm tt")).AddSeconds(CDbl(drPolicy(0)("elrgoing_graceinsec")))
                            'Sohail (10 Dec 2013) -- End

                            If mdtDuetime < mdtFEndtime Then
                                mblnIsProperRoundOff = False
                                mstrRoundOffMsg = Language.getMessage(mstrModuleName, 7, "You can't round off the end time.Reason: Given gracing is greater than early going gracing as per the assigned policy setting.")
                                dgvTimeSheet.CurrentCell = dgvTimeSheet.Rows(e.RowIndex).Cells(dgcolhEndtime.Index)
                                dgvTimeSheet.BeginEdit(True)
                                eZeeMsgBox.Show(mstrRoundOffMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Exit Sub
                            Else
                                mblnIsProperRoundOff = True
                                mstrRoundOffMsg = ""
                            End If
                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimeSheet_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTimeSheet_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTimeSheet.CellValidated
        Try
            If dgvTimeSheet.CurrentCell Is Nothing Then Exit Sub

            Dim wkMins As Double = 0

            If dgvTimeSheet.CurrentCell.ColumnIndex = dgcolhStarttime.Index Or dgvTimeSheet.CurrentCell.ColumnIndex = dgcolhEndtime.Index Then

                If IsDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value) = False Then Exit Sub
                If IsDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value) = False Then Exit Sub

                If CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value) = Nothing OrElse CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value) = Nothing Then Exit Sub

                'Sohail (10 Dec 2013) -- Start
                'Enhancement - OMAN
                'Dim dtStartTime As DateTime = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value))
                'Dim dtEndTime As DateTime = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value))

                'If CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value) > CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value) Then
                '    wkMins = DateDiff(DateInterval.Minute, dtStartTime, dtEndTime)
                '    dtEndTime = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value)).AddDays(1)
                '    dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value = dtEndTime
                'Else
                '    wkMins = DateDiff(DateInterval.Minute, CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value), CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value))
                'End If
                Dim dtStartTime As DateTime = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Tag) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value))
                Dim dtEndTime As DateTime = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Tag) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value))

                If CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value) > CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value) Then
                    dtEndTime = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value)).AddDays(1)
                    wkMins = DateDiff(DateInterval.Minute, dtStartTime, dtEndTime)
                    dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value = dtEndTime
                Else
                    'wkMins = DateDiff(DateInterval.Minute, CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value), CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value))
                    wkMins = DateDiff(DateInterval.Minute, dtStartTime, dtEndTime)
                End If

                'Sohail (10 Dec 2013) -- End

                dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value = CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value).ToString("hh:mm tt")
                dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value = CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value).ToString("hh:mm tt")

                If mblnIsRoundOff = False Then
                    If (dtStartTime <> Nothing OrElse dtEndTime <> Nothing) AndAlso _
                  (CDate(mdtDays.Rows(e.RowIndex)("checkintime")) <> dtStartTime OrElse CDate(mdtDays.Rows(e.RowIndex)("checkouttime")) <> dtEndTime) Then
                        mdtDays.Rows(e.RowIndex)("AUD") = "U"
                        mblnIsChange = True
                    End If
                End If


                If mblnIsRoundOff = False Then   'ONLY USED FOR EDIT

                    'Sohail (10 Dec 2013) -- Start
                    'Enhancement - OMAN
                    'mdtDays.Rows(e.RowIndex)("checkintime") = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value))
                    'mdtDays.Rows(e.RowIndex)("checkouttime") = CDate(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value))
                    mdtDays.Rows(e.RowIndex)("checkintime") = dtStartTime
                    mdtDays.Rows(e.RowIndex)("checkouttime") = dtEndTime
                    'Sohail (10 Dec 2013) -- End
                
                    Dim calMinute As Integer = CInt(wkMins) Mod 60
                    Dim calHour As Double = wkMins / 60

                    dgvTimeSheet.CurrentRow.Cells(dgcolhWorkingHours.Index).Value = CDec(CDec(Int(calHour) + (calMinute / 100))).ToString("#0.00")
                    mdtDays.Rows(e.RowIndex)("workhour") = (wkMins * 60)


                    'Pinkal (20-Jan-2014) -- Start
                    'Enhancement : Oman Changes

                    ' START FOR CALCULATING BREAK
                    If ConfigParameter._Object._FirstCheckInLastCheckOut = False Then

                        For i As Integer = 0 To mdtDays.Rows.Count - 1
                            If i = 0 Then Continue For
                            objTimeSheet._Loginunkid = CInt(mdtDays.Rows(i - 1)("loginunkid"))
                            mdtDays.Rows(i)("breakhr") = DateDiff(DateInterval.Second, CDate(mdtDays.Rows(i - 1)("checkouttime")), CDate(mdtDays.Rows(i)("checkintime")))
                        Next

                    Else
                        Dim objPolicyTran As New clsPolicy_tran
                        objPolicyTran.GetPolicyTran(mintPolicyId)

                        Dim drPolicy() As DataRow = objPolicyTran._DataList.Select("dayid = " & GetWeekDayNumber(CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value).DayOfWeek.ToString()))
                        If drPolicy.Length > 0 Then
                            mdtDays.Rows(e.RowIndex)("workhour") = CInt(mdtDays.Rows(e.RowIndex)("workhour")) - CInt(drPolicy(0)("breaktimeinsec"))
                            mdtDays.Rows(e.RowIndex)("breakhr") = CInt(drPolicy(0)("breaktimeinsec"))
                            dgvTimeSheet.CurrentRow.Cells(dgcolhWorkingHours.Index).Value = CalculateTime(True, CInt(mdtDays.Rows(e.RowIndex)("workhour"))).ToString("#0.00")
                        End If
                    End If
                    ' END FOR CALCULATING BREAK

                    'Pinkal (20-Jan-2014) -- End


                    If Not mdtDays Is Nothing And mdtDays.Rows.Count > 0 Then
                        objTotalHours.Text = CalculateTime(True, CInt(mdtDays.Compute("sum(workhour)", "isvoid = 0"))).ToString("#0.00")
                        objBreak.Text = CStr(CalculateTime(False, CInt(mdtDays.Compute("sum(breakhr)", "isvoid = 0")))) & " Mins"
                    Else
                        objTotalHours.Text = "0"
                        objBreak.Text = "0 Mins"
                    End If

                ElseIf mblnIsRoundOff Then   'ONLY USED FOR ROUND OFF

                    If (dtStartTime <> Nothing OrElse dtEndTime <> Nothing) AndAlso _
                 (CDate(mdtDays.Rows(e.RowIndex)("checkintime")) <> dtStartTime OrElse CDate(mdtDays.Rows(e.RowIndex)("checkouttime")) <> dtEndTime) Then
                        mblnIsChange = True
                    End If

                    'Sohail (10 Dec 2013) -- Start
                    'Enhancement - OMAN
                    'mdtDays.Rows(e.RowIndex)("checkintime") = CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value)
                    'mdtDays.Rows(e.RowIndex)("checkouttime") = CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhLoginDate.Index).Value) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value)
                    mdtDays.Rows(e.RowIndex)("checkintime") = CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Tag) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhStarttime.Index).Value)
                    mdtDays.Rows(e.RowIndex)("checkouttime") = CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Tag) & " " & CDate(dgvTimeSheet.CurrentRow.Cells(dgcolhEndtime.Index).Value)
                    'Sohail (10 Dec 2013) -- End

                    mdtDays.Rows(e.RowIndex)("workhour") = (wkMins * 60) - CInt(mdtDays.Rows(e.RowIndex)("breakhr"))
                    dgvTimeSheet.CurrentRow.Cells(dgcolhWorkingHours.Index).Value = CDec(CalculateTime(True, CInt(mdtDays.Rows(e.RowIndex)("workhour")))).ToString("#0.00")
                    If Not mdtDays Is Nothing And mdtDays.Rows.Count > 0 Then
                        objTotalHours.Text = CalculateTime(True, CInt(mdtDays.Compute("sum(workhour)", "1 = 1"))).ToString("#0.00")
                        objBreak.Text = CStr(CalculateTime(False, CInt(mdtDays.Compute("sum(breakhr)", "1 = 1")))) & " Mins"
                    Else
                        objTotalHours.Text = "0"
                        objBreak.Text = "0 Mins"
                    End If

                End If


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTimeSheet_CellValidated", mstrModuleName)
        End Try
    End Sub

#End Region

  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblTotalHours.Text = Language._Object.getCaption(Me.lblTotalHours.Name, Me.lblTotalHours.Text)
			Me.lblBreak.Text = Language._Object.getCaption(Me.lblBreak.Name, Me.lblBreak.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.LblEmpValue.Text = Language._Object.getCaption(Me.LblEmpValue.Name, Me.LblEmpValue.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblPolicyValue.Text = Language._Object.getCaption(Me.LblPolicyValue.Name, Me.LblPolicyValue.Text)
			Me.LblPolicy.Text = Language._Object.getCaption(Me.LblPolicy.Name, Me.LblPolicy.Text)
			Me.LblShiftValue.Text = Language._Object.getCaption(Me.LblShiftValue.Name, Me.LblShiftValue.Text)
			Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
			Me.LblEmpCodeValue.Text = Language._Object.getCaption(Me.LblEmpCodeValue.Name, Me.LblEmpCodeValue.Text)
			Me.LblEmpCode.Text = Language._Object.getCaption(Me.LblEmpCode.Name, Me.LblEmpCode.Text)
			Me.dgcolhLoginDate.HeaderText = Language._Object.getCaption(Me.dgcolhLoginDate.Name, Me.dgcolhLoginDate.HeaderText)
			Me.dgcolhStarttime.HeaderText = Language._Object.getCaption(Me.dgcolhStarttime.Name, Me.dgcolhStarttime.HeaderText)
			Me.dgcolhEndtime.HeaderText = Language._Object.getCaption(Me.dgcolhEndtime.Name, Me.dgcolhEndtime.HeaderText)
			Me.dgcolhWorkingHours.HeaderText = Language._Object.getCaption(Me.dgcolhWorkingHours.Name, Me.dgcolhWorkingHours.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "There is no record(s) for editing.")
			Language.setMessage(mstrModuleName, 2, "Invalid Checkin time.Please Enter Proper Checkin Time.")
			Language.setMessage(mstrModuleName, 3, "Invalid Checkout time.Please Enter Proper Checkout Time.")
			Language.setMessage(mstrModuleName, 4, "You can't round off the start time.Reason: Given gracing is greater than late coming gracing as per the assigned policy setting.")
			Language.setMessage(mstrModuleName, 5, "You can't round off the start time.Reason: Given gracing is greater than early coming gracing as per the assigned policy setting.")
			Language.setMessage(mstrModuleName, 6, "You can't round off the end time.Reason: Given gracing is greater than late going gracing as per the assigned policy setting.")
			Language.setMessage(mstrModuleName, 7, "You can't round off the end time.Reason: Given gracing is greater than early going gracing as per the assigned policy setting.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class