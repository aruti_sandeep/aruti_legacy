﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimesheetDetail
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimesheetDetail))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.dgTimeSheet = New System.Windows.Forms.DataGridView
        Me.gbShiftinfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnSearchPolicy = New eZee.Common.eZeeGradientButton
        Me.CboPolicy = New System.Windows.Forms.ComboBox
        Me.LblPolicy = New System.Windows.Forms.Label
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.LblShift = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.LblToDate = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromdate = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cmnuTimesheet = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuEditTimeSheet = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRoundingoff = New System.Windows.Forms.ToolStripMenuItem
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShift = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPolicy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhIntime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhoutTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhWorkhrs = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBreakhrs = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBaseHrs = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOT1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOT2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhOT3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhOT4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTotalOverTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTotalShortHrs = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInException = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOutException = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemarks = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhLoginunkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhEmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhShiftId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPolicyId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgTimeSheet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbShiftinfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuTimesheet.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.dgTimeSheet)
        Me.pnlMain.Controls.Add(Me.gbShiftinfo)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(854, 570)
        Me.pnlMain.TabIndex = 0
        '
        'dgTimeSheet
        '
        Me.dgTimeSheet.AllowUserToAddRows = False
        Me.dgTimeSheet.AllowUserToDeleteRows = False
        Me.dgTimeSheet.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgTimeSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgTimeSheet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhDate, Me.dgcolhShift, Me.dgcolhPolicy, Me.dgColhIntime, Me.dgColhoutTime, Me.dgcolhWorkhrs, Me.dgcolhBreakhrs, Me.dgcolhBaseHrs, Me.dgcolhOT1, Me.dgcolhOT2, Me.dgColhOT3, Me.dgColhOT4, Me.dgcolhTotalOverTime, Me.dgcolhTotalShortHrs, Me.dgcolhInException, Me.dgcolhOutException, Me.dgcolhRemarks, Me.objcolhLoginunkId, Me.objcolhEmployeeID, Me.objcolhShiftId, Me.objcolhPolicyId})
        Me.dgTimeSheet.Location = New System.Drawing.Point(9, 105)
        Me.dgTimeSheet.Name = "dgTimeSheet"
        Me.dgTimeSheet.RowHeadersVisible = False
        Me.dgTimeSheet.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgTimeSheet.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgTimeSheet.Size = New System.Drawing.Size(835, 402)
        Me.dgTimeSheet.TabIndex = 6
        '
        'gbShiftinfo
        '
        Me.gbShiftinfo.BorderColor = System.Drawing.Color.Black
        Me.gbShiftinfo.Checked = False
        Me.gbShiftinfo.CollapseAllExceptThis = False
        Me.gbShiftinfo.CollapsedHoverImage = Nothing
        Me.gbShiftinfo.CollapsedNormalImage = Nothing
        Me.gbShiftinfo.CollapsedPressedImage = Nothing
        Me.gbShiftinfo.CollapseOnLoad = False
        Me.gbShiftinfo.Controls.Add(Me.objLine1)
        Me.gbShiftinfo.Controls.Add(Me.objbtnSearchPolicy)
        Me.gbShiftinfo.Controls.Add(Me.CboPolicy)
        Me.gbShiftinfo.Controls.Add(Me.LblPolicy)
        Me.gbShiftinfo.Controls.Add(Me.objbtnSearchShift)
        Me.gbShiftinfo.Controls.Add(Me.cboShift)
        Me.gbShiftinfo.Controls.Add(Me.LblShift)
        Me.gbShiftinfo.Controls.Add(Me.objbtnReset)
        Me.gbShiftinfo.Controls.Add(Me.objbtnSearch)
        Me.gbShiftinfo.Controls.Add(Me.dtpToDate)
        Me.gbShiftinfo.Controls.Add(Me.LblToDate)
        Me.gbShiftinfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbShiftinfo.Controls.Add(Me.lnkAllocation)
        Me.gbShiftinfo.Controls.Add(Me.dtpFromDate)
        Me.gbShiftinfo.Controls.Add(Me.lblFromdate)
        Me.gbShiftinfo.Controls.Add(Me.cboEmployee)
        Me.gbShiftinfo.Controls.Add(Me.lblEmployee)
        Me.gbShiftinfo.ExpandedHoverImage = Nothing
        Me.gbShiftinfo.ExpandedNormalImage = Nothing
        Me.gbShiftinfo.ExpandedPressedImage = Nothing
        Me.gbShiftinfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShiftinfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShiftinfo.HeaderHeight = 25
        Me.gbShiftinfo.HeaderMessage = ""
        Me.gbShiftinfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbShiftinfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShiftinfo.HeightOnCollapse = 0
        Me.gbShiftinfo.LeftTextSpace = 0
        Me.gbShiftinfo.Location = New System.Drawing.Point(9, 8)
        Me.gbShiftinfo.Name = "gbShiftinfo"
        Me.gbShiftinfo.OpenHeight = 300
        Me.gbShiftinfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShiftinfo.ShowBorder = True
        Me.gbShiftinfo.ShowCheckBox = False
        Me.gbShiftinfo.ShowCollapseButton = False
        Me.gbShiftinfo.ShowDefaultBorderColor = True
        Me.gbShiftinfo.ShowDownButton = False
        Me.gbShiftinfo.ShowHeader = True
        Me.gbShiftinfo.Size = New System.Drawing.Size(835, 89)
        Me.gbShiftinfo.TabIndex = 2
        Me.gbShiftinfo.Temp = 0
        Me.gbShiftinfo.Text = "Filter Creteria"
        Me.gbShiftinfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(394, 25)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(8, 62)
        Me.objLine1.TabIndex = 89
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'objbtnSearchPolicy
        '
        Me.objbtnSearchPolicy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPolicy.BorderSelected = False
        Me.objbtnSearchPolicy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPolicy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPolicy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPolicy.Location = New System.Drawing.Point(802, 59)
        Me.objbtnSearchPolicy.Name = "objbtnSearchPolicy"
        Me.objbtnSearchPolicy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPolicy.TabIndex = 118
        '
        'CboPolicy
        '
        Me.CboPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboPolicy.DropDownWidth = 350
        Me.CboPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboPolicy.FormattingEnabled = True
        Me.CboPolicy.Location = New System.Drawing.Point(494, 59)
        Me.CboPolicy.Name = "CboPolicy"
        Me.CboPolicy.Size = New System.Drawing.Size(305, 21)
        Me.CboPolicy.TabIndex = 5
        '
        'LblPolicy
        '
        Me.LblPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPolicy.Location = New System.Drawing.Point(424, 61)
        Me.LblPolicy.Name = "LblPolicy"
        Me.LblPolicy.Size = New System.Drawing.Size(66, 17)
        Me.LblPolicy.TabIndex = 117
        Me.LblPolicy.Text = "Policy"
        Me.LblPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(356, 59)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 115
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 350
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(76, 59)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(274, 21)
        Me.cboShift.TabIndex = 4
        '
        'LblShift
        '
        Me.LblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShift.Location = New System.Drawing.Point(6, 61)
        Me.LblShift.Name = "LblShift"
        Me.LblShift.Size = New System.Drawing.Size(66, 17)
        Me.LblShift.TabIndex = 114
        Me.LblShift.Text = "Shift"
        Me.LblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(809, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 111
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(786, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 110
        Me.objbtnSearch.TabStop = False
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(251, 32)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(99, 21)
        Me.dtpToDate.TabIndex = 2
        '
        'LblToDate
        '
        Me.LblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToDate.Location = New System.Drawing.Point(187, 34)
        Me.LblToDate.Name = "LblToDate"
        Me.LblToDate.Size = New System.Drawing.Size(58, 17)
        Me.LblToDate.TabIndex = 109
        Me.LblToDate.Text = "To Date"
        Me.LblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(802, 32)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 106
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(705, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(76, 17)
        Me.lnkAllocation.TabIndex = 104
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(76, 32)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(99, 21)
        Me.dtpFromDate.TabIndex = 1
        '
        'lblFromdate
        '
        Me.lblFromdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromdate.Location = New System.Drawing.Point(8, 34)
        Me.lblFromdate.Name = "lblFromdate"
        Me.lblFromdate.Size = New System.Drawing.Size(64, 17)
        Me.lblFromdate.TabIndex = 30
        Me.lblFromdate.Text = "From Date"
        Me.lblFromdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(494, 32)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(305, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(424, 34)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(66, 17)
        Me.lblEmployee.TabIndex = 5
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmnuTimesheet
        '
        Me.cmnuTimesheet.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEditTimeSheet, Me.mnuRoundingoff})
        Me.cmnuTimesheet.Name = "cmnuTimesheet"
        Me.cmnuTimesheet.Size = New System.Drawing.Size(156, 48)
        '
        'mnuEditTimeSheet
        '
        Me.mnuEditTimeSheet.Name = "mnuEditTimeSheet"
        Me.mnuEditTimeSheet.Size = New System.Drawing.Size(155, 22)
        Me.mnuEditTimeSheet.Text = "Edit Timesheet"
        '
        'mnuRoundingoff
        '
        Me.mnuRoundingoff.Name = "mnuRoundingoff"
        Me.mnuRoundingoff.Size = New System.Drawing.Size(155, 22)
        Me.mnuRoundingoff.Text = "Roundig Off"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 515)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(854, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(749, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 125
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "InTime"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "OutTime"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Work Hrs"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total Hrs"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "OT-1"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 60
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "OT-2"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 60
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "OT-3"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 60
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "In Exception"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 60
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Out Exception"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 60
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Remarks"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Width = 60
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Remarks"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Width = 90
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Out Exception"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        Me.DataGridViewTextBoxColumn13.Width = 90
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Remarks"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        Me.DataGridViewTextBoxColumn14.Width = 90
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Remarks"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        Me.DataGridViewTextBoxColumn15.Width = 90
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Loginunkid"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        Me.DataGridViewTextBoxColumn16.Width = 90
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "EmployeeID"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "ShiftId"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "PolicyId"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "PolicyId"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "PolicyId"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'dgcolhDate
        '
        Me.dgcolhDate.Frozen = True
        Me.dgcolhDate.HeaderText = "Date"
        Me.dgcolhDate.Name = "dgcolhDate"
        Me.dgcolhDate.ReadOnly = True
        Me.dgcolhDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDate.Width = 110
        '
        'dgcolhShift
        '
        Me.dgcolhShift.Frozen = True
        Me.dgcolhShift.HeaderText = "Shift"
        Me.dgcolhShift.Name = "dgcolhShift"
        Me.dgcolhShift.ReadOnly = True
        Me.dgcolhShift.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhShift.Width = 150
        '
        'dgcolhPolicy
        '
        Me.dgcolhPolicy.Frozen = True
        Me.dgcolhPolicy.HeaderText = "Policy"
        Me.dgcolhPolicy.Name = "dgcolhPolicy"
        Me.dgcolhPolicy.ReadOnly = True
        Me.dgcolhPolicy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgColhIntime
        '
        Me.dgColhIntime.HeaderText = "InTime"
        Me.dgColhIntime.Name = "dgColhIntime"
        Me.dgColhIntime.ReadOnly = True
        Me.dgColhIntime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgColhIntime.Width = 150
        '
        'dgColhoutTime
        '
        Me.dgColhoutTime.HeaderText = "OutTime"
        Me.dgColhoutTime.Name = "dgColhoutTime"
        Me.dgColhoutTime.ReadOnly = True
        Me.dgColhoutTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgColhoutTime.Width = 150
        '
        'dgcolhWorkhrs
        '
        Me.dgcolhWorkhrs.HeaderText = "Work Hrs"
        Me.dgcolhWorkhrs.Name = "dgcolhWorkhrs"
        Me.dgcolhWorkhrs.ReadOnly = True
        Me.dgcolhWorkhrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhWorkhrs.Width = 60
        '
        'dgcolhBreakhrs
        '
        Me.dgcolhBreakhrs.HeaderText = "Break Hrs"
        Me.dgcolhBreakhrs.Name = "dgcolhBreakhrs"
        Me.dgcolhBreakhrs.ReadOnly = True
        Me.dgcolhBreakhrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBreakhrs.Width = 70
        '
        'dgcolhBaseHrs
        '
        Me.dgcolhBaseHrs.HeaderText = "Base Hrs"
        Me.dgcolhBaseHrs.Name = "dgcolhBaseHrs"
        Me.dgcolhBaseHrs.ReadOnly = True
        Me.dgcolhBaseHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBaseHrs.Width = 60
        '
        'dgcolhOT1
        '
        Me.dgcolhOT1.HeaderText = "OT-1"
        Me.dgcolhOT1.Name = "dgcolhOT1"
        Me.dgcolhOT1.ReadOnly = True
        Me.dgcolhOT1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOT1.Width = 60
        '
        'dgcolhOT2
        '
        Me.dgcolhOT2.HeaderText = "OT-2"
        Me.dgcolhOT2.Name = "dgcolhOT2"
        Me.dgcolhOT2.ReadOnly = True
        Me.dgcolhOT2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOT2.Width = 60
        '
        'dgColhOT3
        '
        Me.dgColhOT3.HeaderText = "OT-3"
        Me.dgColhOT3.Name = "dgColhOT3"
        Me.dgColhOT3.ReadOnly = True
        Me.dgColhOT3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgColhOT3.Width = 60
        '
        'dgColhOT4
        '
        Me.dgColhOT4.HeaderText = "OT-4"
        Me.dgColhOT4.Name = "dgColhOT4"
        Me.dgColhOT4.ReadOnly = True
        Me.dgColhOT4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgColhOT4.Width = 60
        '
        'dgcolhTotalOverTime
        '
        Me.dgcolhTotalOverTime.HeaderText = "Total OverTime"
        Me.dgcolhTotalOverTime.Name = "dgcolhTotalOverTime"
        Me.dgcolhTotalOverTime.ReadOnly = True
        Me.dgcolhTotalOverTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTotalOverTime.Visible = False
        '
        'dgcolhTotalShortHrs
        '
        Me.dgcolhTotalShortHrs.HeaderText = "Total Short Hrs"
        Me.dgcolhTotalShortHrs.Name = "dgcolhTotalShortHrs"
        Me.dgcolhTotalShortHrs.ReadOnly = True
        Me.dgcolhTotalShortHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTotalShortHrs.Visible = False
        '
        'dgcolhInException
        '
        Me.dgcolhInException.HeaderText = "In Exception"
        Me.dgcolhInException.Name = "dgcolhInException"
        Me.dgcolhInException.ReadOnly = True
        Me.dgcolhInException.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInException.Width = 90
        '
        'dgcolhOutException
        '
        Me.dgcolhOutException.HeaderText = "Out Exception"
        Me.dgcolhOutException.Name = "dgcolhOutException"
        Me.dgcolhOutException.ReadOnly = True
        Me.dgcolhOutException.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOutException.Width = 90
        '
        'dgcolhRemarks
        '
        Me.dgcolhRemarks.HeaderText = "Remarks"
        Me.dgcolhRemarks.Name = "dgcolhRemarks"
        Me.dgcolhRemarks.ReadOnly = True
        Me.dgcolhRemarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRemarks.Visible = False
        '
        'objcolhLoginunkId
        '
        Me.objcolhLoginunkId.HeaderText = "Loginunkid"
        Me.objcolhLoginunkId.Name = "objcolhLoginunkId"
        Me.objcolhLoginunkId.ReadOnly = True
        Me.objcolhLoginunkId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhLoginunkId.Visible = False
        '
        'objcolhEmployeeID
        '
        Me.objcolhEmployeeID.HeaderText = "EmployeeID"
        Me.objcolhEmployeeID.Name = "objcolhEmployeeID"
        Me.objcolhEmployeeID.ReadOnly = True
        Me.objcolhEmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhEmployeeID.Visible = False
        '
        'objcolhShiftId
        '
        Me.objcolhShiftId.HeaderText = "ShiftId"
        Me.objcolhShiftId.Name = "objcolhShiftId"
        Me.objcolhShiftId.ReadOnly = True
        Me.objcolhShiftId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhShiftId.Visible = False
        '
        'objcolhPolicyId
        '
        Me.objcolhPolicyId.HeaderText = "PolicyId"
        Me.objcolhPolicyId.Name = "objcolhPolicyId"
        Me.objcolhPolicyId.ReadOnly = True
        Me.objcolhPolicyId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhPolicyId.Visible = False
        '
        'frmTimesheetDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(854, 570)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTimesheetDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Timesheet Detail"
        Me.pnlMain.ResumeLayout(False)
        CType(Me.dgTimeSheet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbShiftinfo.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuTimesheet.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbShiftinfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromdate As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblToDate As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgTimeSheet As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmnuTimesheet As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuEditTimeSheet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRoundingoff As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchPolicy As eZee.Common.eZeeGradientButton
    Friend WithEvents CboPolicy As System.Windows.Forms.ComboBox
    Friend WithEvents LblPolicy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents LblShift As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents dgcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPolicy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhIntime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhoutTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhWorkhrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBreakhrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBaseHrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOT1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhOT3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhOT4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTotalOverTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTotalShortHrs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInException As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOutException As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemarks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhLoginunkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhEmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhShiftId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPolicyId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
