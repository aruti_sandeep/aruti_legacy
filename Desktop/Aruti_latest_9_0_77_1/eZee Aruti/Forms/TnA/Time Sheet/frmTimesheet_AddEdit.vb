﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 1

Public Class frmTimesheet_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmTimesheet_AddEdit"
    Private mblnCancel As Boolean = True
    Private objTimeSheet As clslogin_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTimesheetUnkid As Integer = -1
    Private mintEmployeeID As Integer = -1
    Private mintShiftID As Integer = -1
    Friend mblnIsLogin As Boolean = False
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intEmployeeId As Integer, ByVal intShiftId As Integer, ByVal dtlogindate As DateTime, Optional ByVal blIsLogin As Boolean = False) As Boolean
        Try
            mintTimesheetUnkid = intUnkId
            menAction = eAction
            mintEmployeeID = intEmployeeId
            mintShiftID = intShiftId
            dtpCheckIntime.Value = dtlogindate


            'Pinkal (03-Nov-2012) -- Start
            'Enhancement : TRA Changes

            dtpCheckIntime.MinDate = dtlogindate.Date
            dtpCheckouttime.MinDate = dtlogindate.Date

            'Pinkal (03-Nov-2012) -- End

            mblnIsLogin = blIsLogin
            Me.ShowDialog()
            intUnkId = mintTimesheetUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmTimesheet_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTimeSheet = New clslogin_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            'Pinkal (28-oct-2010) -- START


            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes

            'dtpCheckIndate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpCheckIndate.MaxDate = FinancialYear._Object._Database_End_Date.AddDays(1)

            'dtpCheckIntime.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpCheckIntime.MaxDate = FinancialYear._Object._Database_End_Date.AddDays(1)

            'dtpCheckoutdate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpCheckoutdate.MaxDate = FinancialYear._Object._Database_End_Date.AddDays(1)

            'dtpCheckouttime.MinDate = FinancialYear._Object._Database_Start_Date.Date
            'dtpCheckouttime.MaxDate = FinancialYear._Object._Database_End_Date.AddDays(1)

            'Pinkal (25-Oct-2012) -- End

            'Pinkal (28-oct-2010) -- END



            'Pinkal (25-Feb-2015) -- Start
            'Enhancement - CHANGING FOR VOLTAMP NET BF ISSUE WHEN CLOSED PERIOD.
            If ConfigParameter._Object._PolicyManagementTNA Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim dsList As DataSet = objPeriod.GetList("List", enModuleReference.Payroll, True, enStatusType.Open)
                Dim dsList As DataSet = objPeriod.GetList("List", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
                'Sohail (21 Aug 2015) -- End
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(dsList.Tables(0).Rows(0)("periodunkid"))
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables(0).Rows(0)("periodunkid"))
                    'Sohail (21 Aug 2015) -- End
                    dtpCheckIntime.MinDate = objPeriod._TnA_StartDate.Date
                    'dtpCheckIntime.MaxDate = objPeriod._TnA_EndDate.Date
                    dtpCheckouttime.MinDate = objPeriod._TnA_StartDate.Date
                    'dtpCheckouttime.MaxDate = objPeriod._TnA_EndDate.Date
                End If
            Else
                dtpCheckIntime.MinDate = FinancialYear._Object._Database_Start_Date.Date
                dtpCheckIntime.MaxDate = FinancialYear._Object._Database_End_Date.Date
                dtpCheckouttime.MinDate = FinancialYear._Object._Database_Start_Date.Date
                dtpCheckouttime.MaxDate = FinancialYear._Object._Database_End_Date.Date
            End If
            ''Pinkal (25-Feb-2015) -- End


            If menAction = enAction.EDIT_ONE Then
                objTimeSheet._Loginunkid = mintTimesheetUnkid
            End If
            If mblnIsLogin = False Then
                btnLogout.Visible = False
            Else
                btnLogout.Visible = True
            End If
            setColor()
            GetValue()
            dtpCheckIntime.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheet_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheet_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheet_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objTimeSheet = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clslogin_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clslogin_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        Try
            objTimeSheet._InOutType = 1
            btnSave_Click(sender, e)
            mblnIsLogin = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLogout_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() Then
                SetValue()


                'Pinkal (21-Oct-2016) -- Start
                'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
                'objTimeSheet._Holdunkid = objTimeSheet.CheckforHoldEmployee(mintEmployeeID, dtpCheckIntime.Value.Date)
                'Pinkal (21-Oct-2016) -- End


                If menAction = enAction.EDIT_ONE Then

                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'blnFlag = objTimeSheet.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                    '                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                    '                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                    '                            , ConfigParameter._Object._DonotAttendanceinSeconds _
                    '                            , ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "")



                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objTimeSheet._FormName = mstrModuleName
                    objTimeSheet._LoginEmployeeUnkid = 0
                    objTimeSheet._ClientIP = getIP()
                    objTimeSheet._HostName = getHostName()
                    objTimeSheet._FromWeb = False
                    objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                    objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    blnFlag = objTimeSheet.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                  , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                  , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                  , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                  , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                  , False, -1, Nothing, "", "")

                    'Pinkal (11-AUG-2017) -- End
                Else


                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'blnFlag = objTimeSheet.Insert(FinancialYear._Object._DatabaseName, _
                    '                              User._Object._Userunkid, _
                    '                              FinancialYear._Object._YearUnkid, _
                    '                              Company._Object._Companyunkid, _
                    '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                              ConfigParameter._Object._UserAccessModeSetting, _
                    '                              True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                    '                              ConfigParameter._Object._PolicyManagementTNA, _
                    '                              ConfigParameter._Object._DonotAttendanceinSeconds, _
                    '                              ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "")

'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objTimeSheet._FormName = mstrModuleName
                    objTimeSheet._LoginEmployeeUnkid = 0
                    objTimeSheet._ClientIP = getIP()
                    objTimeSheet._HostName = getHostName()
                    objTimeSheet._FromWeb = False
                    objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                    objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    blnFlag = objTimeSheet.Insert(FinancialYear._Object._DatabaseName _
                                                               , User._Object._Userunkid _
                                                               , FinancialYear._Object._YearUnkid _
                                                               , Company._Object._Companyunkid _
                                                               , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                               , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                               , ConfigParameter._Object._UserAccessModeSetting _
                                                               , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                               , ConfigParameter._Object._PolicyManagementTNA _
                                                               , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                               , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                               , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                               , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                               , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                               , False, -1, Nothing, "", "")


                    'Pinkal (11-AUG-2017) -- End

                End If

                ' CalculateForBreak()

                If blnFlag = False And objTimeSheet._Message <> "" Then
                    eZeeMsgBox.Show(objTimeSheet._Message, enMsgBoxStyle.Information)
                End If
                If blnFlag Then
                    mblnCancel = False
                    If menAction = enAction.ADD_CONTINUE Then
                        objTimeSheet = Nothing
                        objTimeSheet = New clslogin_Tran
                        Call GetValue()
                        dtpCheckIntime.Select()
                    Else
                        mintTimesheetUnkid = objTimeSheet._Loginunkid
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtRemark.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objTimeSheet._Loginunkid > 0 Then


                'Pinkal (13-Jan-2014) -- Start
                'Enhancement : Oman Changes

                If objTimeSheet._Roundoff_Intime <> Nothing Then
                    dtpCheckIntime.Value = objTimeSheet._Roundoff_Intime
                Else
                    dtpCheckIntime.Value = objTimeSheet._checkintime
                End If

                'If objTimeSheet._Checkouttime <> Nothing Then
                '    dtpCheckouttime.Value = objTimeSheet._Checkouttime
                'Else
                '    dtpCheckouttime.Value = Now
                'End If

                If objTimeSheet._Roundoff_Outtime <> Nothing Then
                    dtpCheckouttime.Value = objTimeSheet._Roundoff_Outtime
                Else
                    If objTimeSheet._Checkouttime <> Nothing Then
                        dtpCheckouttime.Value = objTimeSheet._Checkouttime
                    Else
                        dtpCheckouttime.Value = Now
                    End If
                End If

                'Pinkal (13-Jan-2014) -- End

            End If
            txtRemark.Text = objTimeSheet._Remark
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes
            'objTimeSheet._Logindate = dtpCheckIndate.Value.Date
            objTimeSheet._Logindate = dtpCheckIntime.Value.Date
            objTimeSheet._checkintime = CDate(dtpCheckIntime.Value.Date & " " & dtpCheckIntime.Value.ToShortTimeString)

            If dtpCheckouttime.Visible Then
                objTimeSheet._Checkouttime = CDate(dtpCheckouttime.Value.Date & " " & dtpCheckouttime.Value.ToShortTimeString)
                Dim wkmins As Double

                'Pinkal (20-Sep-2013) -- Start
                'Enhancement : TRA Changes
                'wkmins = DateDiff(DateInterval.Minute, dtpCheckIntime.Value, dtpCheckouttime.Value)
                wkmins = DateDiff(DateInterval.Minute, objTimeSheet._checkintime, objTimeSheet._Checkouttime)
                'Pinkal (20-Sep-2013) -- End


                objTimeSheet._Workhour = CInt(wkmins * 60)
                objTimeSheet._InOutType = 1
            Else
                objTimeSheet._Checkouttime = Nothing
                objTimeSheet._InOutType = 0
            End If

            'Pinkal (25-Oct-2012) -- End


            'Pinkal (15-Dec-2013) -- Start
            'Enhancement : Oman Changes

            If menAction <> enAction.EDIT_ONE AndAlso mintTimesheetUnkid <= 0 Then
                objTimeSheet._Original_InTime = objTimeSheet._checkintime
                objTimeSheet._Original_OutTime = objTimeSheet._Checkouttime
            End If

            'Pinkal (15-Dec-2013) -- End


            objTimeSheet._Remark = txtRemark.Text.Trim
            objTimeSheet._Employeeunkid = mintEmployeeID
            objTimeSheet._Shiftunkid = mintShiftID
            objTimeSheet._SourceType = enInOutSource.Manual
            objTimeSheet._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function CalculateForBreak() As Double
        Dim calcBreaktime As Double = 0
        Try
            Dim dsTime As DataSet = Nothing


            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes

'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTimeSheet._FormName = mstrModuleName
            objTimeSheet._LoginEmployeeUnkid = 0
            objTimeSheet._ClientIP = getIP()
            objTimeSheet._HostName = getHostName()
            objTimeSheet._FromWeb = False
            objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
            objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objTimeSheet.CheckforEmployee(mintEmployeeID, dtpCheckIntime.Value.Date) > 1 Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTimeSheet._FormName = mstrModuleName
                objTimeSheet._LoginEmployeeUnkid = 0
                objTimeSheet._ClientIP = getIP()
                objTimeSheet._HostName = getHostName()
                objTimeSheet._FromWeb = False
                objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                dsTime = objTimeSheet.GetList("Timeinfo", True)
                For i As Integer = 0 To dsTime.Tables(0).Rows.Count - 1
                    If i = 0 Then Continue For
                    objTimeSheet._Loginunkid = CInt(dsTime.Tables(0).Rows(i - 1)("loginunkid"))
                    Dim Mins As Long = DateDiff(DateInterval.Minute, CDate(dsTime.Tables(0).Rows(i - 1)("checkouttime")), CDate(dsTime.Tables(0).Rows(i)("checkintime")))
                    objTimeSheet._Breakhr = CInt(Mins * 60)


                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'objTimeSheet.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                                                , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                    '                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                    '                                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                    '                    ConfigParameter._Object._DonotAttendanceinSeconds, _
                    '                    ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "")

 'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objTimeSheet._FormName = mstrModuleName
                    objTimeSheet._LoginEmployeeUnkid = 0
                    objTimeSheet._ClientIP = getIP()
                    objTimeSheet._HostName = getHostName()
                    objTimeSheet._FromWeb = False
                    objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                    objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    objTimeSheet.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                   , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                   , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                   , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                   , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                   , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                   , False, -1, Nothing, "", "")

                    'Pinkal (11-AUG-2017) -- End

                Next
            End If

            'Pinkal (25-Oct-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CalculateForBreak", mstrModuleName)
        End Try
        Return calcBreaktime
    End Function

    Private Function Validation() As Boolean
        Try
            If dtpCheckouttime.Visible = False Then
                dtpCheckouttime.Value = dtpCheckIntime.Value
            End If

            If dtpCheckIntime.Value > dtpCheckouttime.Value Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check your In Time, it seems incorrect. In Time should be less than Out Time."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dtpCheckIntime.Select()
                Return False
            End If



            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.

            Dim objShift As New clsNewshift_master
            objShift._Shiftunkid = mintShiftID

            If objShift._IsOpenShift = False Then

            Dim objShifttran As New clsshift_tran
            objShifttran.GetShiftTran(mintShiftID)
            Dim drRow() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(dtpCheckIntime.Value.Date.DayOfWeek.ToString()))
            If drRow.Length > 0 Then

                If CDate(dtpCheckIntime.Value.Date & " " & dtpCheckIntime.Value.ToShortTimeString) >= CDate(dtpCheckIntime.Value.Date & " " & CDate(drRow(0)("starttime")).ToShortTimeString()) Then

                    If CDate(drRow(0)("endtime")).ToShortTimeString.Contains("AM") Then
                        drRow(0)("endtime") = dtpCheckouttime.Value.Date.AddDays(1) & " " & CDate(drRow(0)("endtime")).ToShortTimeString
                    Else
                        drRow(0)("endtime") = dtpCheckouttime.Value.Date & " " & CDate(drRow(0)("endtime")).ToShortTimeString
                    End If

                    If CDate(dtpCheckIntime.Value.Date & " " & dtpCheckIntime.Value.ToShortTimeString) <= CDate(drRow(0)("endtime")) Then
                        objTimeSheet._Shiftunkid = mintShiftID
                    Else

                        Dim objEmpShift As New clsEmployee_Shift_Tran
                        objEmpShift._EmployeeUnkid = mintEmployeeID
                        mintShiftID = objEmpShift.GetEmployee_Current_ShiftId(dtpCheckIntime.Value.Date, mintEmployeeID)

                        objShifttran.GetShiftTran(mintShiftID)
                                    Dim drcheckShift() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(dtpCheckIntime.Value.Date.DayOfWeek.ToString()))
                                    If drRow.Length > 0 Then

                                        If CDate(dtpCheckIntime.Value.Date & " " & dtpCheckIntime.Value.ToShortTimeString) >= CDate(dtpCheckIntime.Value.Date & " " & CDate(drcheckShift(0)("starttime")).ToShortTimeString()) Then

                                            If CDate(drcheckShift(0)("endtime")).ToShortTimeString.Contains("AM") Then
                                                drcheckShift(0)("endtime") = dtpCheckouttime.Value.Date.AddDays(1) & " " & CDate(drcheckShift(0)("endtime")).ToShortTimeString
                                            Else
                                                drcheckShift(0)("endtime") = dtpCheckouttime.Value.Date & " " & CDate(drcheckShift(0)("endtime")).ToShortTimeString
                                            End If

                                            If CDate(dtpCheckIntime.Value.Date & " " & dtpCheckIntime.Value.ToShortTimeString) <= CDate(drcheckShift(0)("endtime")) Then
                                                objTimeSheet._Shiftunkid = mintShiftID
                                            End If

                                        End If

                                    End If

                        End If

                        End If

                    End If

                End If

            'Pinkal (09-Jun-2015) -- End

            'Pinkal (30-Jul-2015) -- Start
            'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)

            'Pinkal (29-Jul-2016) -- Start
            'Enhancement - IMPLEMENTING TnA Valiadation For Timesheet Entry.
            'If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment = False Then
            If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment Then
                'Pinkal (29-Jul-2016) -- End

            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                Dim objPaymentTran As New clsPayment_tran
                Dim objPeriod As New clsMasterData
                Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpCheckIntime.Value.Date)

                Dim mdtTnAStartdate As Date = Nothing
                If intPeriodId > 0 Then
                    Dim objTnAPeriod As New clscommom_period_Tran
                    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                End If

                If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintEmployeeID, mdtTnAStartdate, intPeriodId, dtpCheckIntime.Value.Date) > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to add/edit timesheet data ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Return False
                    End If
                End If
            End If

            'Pinkal (13-Jan-2015) -- End

            End If

            'Pinkal (30-Jul-2015) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
    End Function

#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbInOutinfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInOutinfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbRemark.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbRemark.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnLogout.GradientBackColor = GUI._ButttonBackColor 
			Me.btnLogout.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbInOutinfo.Text = Language._Object.getCaption(Me.gbInOutinfo.Name, Me.gbInOutinfo.Text)
			Me.lblCheckIntime.Text = Language._Object.getCaption(Me.lblCheckIntime.Name, Me.lblCheckIntime.Text)
			Me.lblCheckout.Text = Language._Object.getCaption(Me.lblCheckout.Name, Me.lblCheckout.Text)
			Me.lblcheckin.Text = Language._Object.getCaption(Me.lblcheckin.Name, Me.lblcheckin.Text)
			Me.lblCheckouttime.Text = Language._Object.getCaption(Me.lblCheckouttime.Name, Me.lblCheckouttime.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbRemark.Text = Language._Object.getCaption(Me.gbRemark.Name, Me.gbRemark.Text)
			Me.btnLogout.Text = Language._Object.getCaption(Me.btnLogout.Name, Me.btnLogout.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check your In Time, it seems incorrect. In Time should be less than Out Time.")
			Language.setMessage(mstrModuleName, 5, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to add/edit timesheet data ?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class