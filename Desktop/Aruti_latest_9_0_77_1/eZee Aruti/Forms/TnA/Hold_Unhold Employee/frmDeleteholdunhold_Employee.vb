﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmDeleteholdunhold_Employee

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmDeleteholdunhold_Employee"
    Private mblnCancel As Boolean = True
    Private objHoldEmployee As clsholdemployee_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintHoldMasterUnkid As Integer = -1
    Dim wkMins As Double
    Private mstrAdvanceFilter As String = ""
    Dim mdvEmployee As DataView = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintHoldMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintHoldMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"


    Private Sub frmDeleteholdunhold_Employee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objHoldEmployee = New clsholdemployee_master
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objHoldEmployee._Holdunkid = mintHoldMasterUnkid
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDeleteholdunhold_Employee_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDeleteholdunhold_Employee_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDeleteholdunhold_Employee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDeleteholdunhold_Employee_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objHoldEmployee = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsholdemployee_master.SetMessages()
            objfrm._Other_ModuleNames = "clsholdemployee_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            cboOperationType.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboOperationType.Items.Add(Language.getMessage(mstrModuleName, 2, "Hold"))
            cboOperationType.Items.Add(Language.getMessage(mstrModuleName, 3, "Unhold"))
            cboOperationType.SelectedIndex = 0


            Dim objEmployee As New clsEmployee_Master
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , , , , , True, True, True)

            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables(0).Copy
            cboEmployee.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Try

            strSearching &= "AND CONVERT(CHAR(8),tnaholdemployee_master.effectivedate,112) >= '" & eZeeDate.convertDate(dtpFromDate.Value.Date) & "' AND CONVERT(CHAR(8),tnaholdemployee_master.effectivedate,112) <=  '" & eZeeDate.convertDate(dtpToDate.Value.Date) & "'"

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearching &= "AND tnaholdemployee_master.employeeunkid =  " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboOperationType.SelectedIndex) > 0 Then
                strSearching &= "AND tnaholdemployee_master.operationtype =  " & CInt(cboOperationType.SelectedIndex) & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If

            Dim dsList As DataSet = objHoldEmployee.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                      , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                      , dtpFromDate.Value.Date, dtpToDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                                      , True, ConfigParameter._Object._IsIncludeInactiveEmp, strSearching)

            dgvData.AutoGenerateColumns = False

            objdgcolhIscheck.DataPropertyName = "ischeck"
            objdgcolhIsGrp.DataPropertyName = "isGrp"
            objdgcolhOperation.DataPropertyName = "operationtypeid"
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            objdgcolhholdunkid.DataPropertyName = "holdunkid"
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhTransactionDate.DataPropertyName = "transactiondate"
            dgcolhEffectiveDate.DataPropertyName = "effectivedate"
            dgcolhOperation.DataPropertyName = "operationtype"
            dgcolhRemark.DataPropertyName = "Remark"

            dgvData.DataSource = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = CType(dgvData.DataSource, DataTable).Select("ischeck = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objHoldEmployee As New frmHoldemployee
            If objHoldEmployee.displayDialog(-1, enAction.ADD_ONE) Then

            End If
            objHoldEmployee = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If CType(dgvData.DataSource, DataTable) Is Nothing Then Exit Sub
            Dim mblnFlag As Boolean = False

            Dim xRow As List(Of DataGridViewRow) = (From p In dgvData.Rows.Cast(Of DataGridViewRow)() Where CBool(p.Cells(objdgcolhIscheck.Index).Value) = True AndAlso _
                                                                          CBool(p.Cells(objdgcolhIsGrp.Index).Value) = False Select p).ToList
            If xRow.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one transaction to delete."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            Else
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete this Hold/Unhold information ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.TNA, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objHoldEmployee._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                For Each dr In xRow


                    'Pinkal (27-Apr-2019) -- Start
                    'Enhancement - Audit Trail changes.
                    dgvData.Refresh()
                    'Pinkal (27-Apr-2019) -- End


                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 

                    objHoldEmployee._LoginEmployeeunkid = 0





                    'S.SANDEEP [28-May-2018] -- END

                    If objHoldEmployee.isOperationExist(CDate(dr.Cells(dgcolhEffectiveDate.Index).Value).Date, CInt(dr.Cells(objdgcolhEmployeeId.Index).Value)) = False Then
                        objHoldEmployee._Voiduserunkid = User._Object._Userunkid

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 

                        objHoldEmployee._LoginEmployeeunkid = 0





                        'S.SANDEEP [28-May-2018] -- END

                        With objHoldEmployee
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objHoldEmployee.Delete(CInt(dr.Cells(objdgcolhholdunkid.Index).Value)) = False Then
                            eZeeMsgBox.Show(objHoldEmployee._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        dgvData.Rows(dr.Index).DefaultCellStyle.ForeColor = Color.Red
                        mblnFlag = True
                        Continue For
                    End If
                Next
            End If

            If mblnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the data cannot be delete. Reason, the same operation (Hold/Unhold) will be there for different effective date."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            chkSelectAll.Checked = False
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            chkSelectAll.Checked = False
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboOperationType.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            mstrAdvanceFilter = ""
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " LinkButton's Event(s) "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If CType(dgvData.DataSource, DataTable) Is Nothing Then Exit Sub

            For Each dr As DataRow In CType(dgvData.DataSource, DataTable).Rows
                dr("ischeck") = chkSelectAll.Checked
                dr.AcceptChanges()
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


#Region "Datagrid Event"

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            'RemoveHandler objChkSelectAll.CheckedChanged, AddressOf objChkSelectAll_Checked

            If e.ColumnIndex = objdgcolhIscheck.Index Then
                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    CType(dgvData.DataSource, DataTable).AcceptChanges()
                End If

                If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    Dim xRow As List(Of DataRow) = (From p In CType(dgvData.DataSource, DataTable) Where CInt(p.Item("employeeunkid")) = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmployeeId.Index).Value) Select (p)).ToList
                    For Each dr As DataRow In xRow
                        dr("ischeck") = CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIscheck.Index).Value)
                        dr.AcceptChanges()
                    Next

                ElseIf CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = False Then

                    Dim xRow As List(Of DataRow) = (From p In CType(dgvData.DataSource, DataTable) Where CInt(p.Item("employeeunkid")) = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmployeeId.Index).Value) Select (p)).ToList
                    Dim xRowSelected As List(Of DataRow) = (From p In CType(dgvData.DataSource, DataTable) Where CInt(p.Item("employeeunkid")) = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmployeeId.Index).Value) AndAlso _
                                                                                 CBool(p.Item("ischeck")) = True AndAlso CBool(p.Item("isGrp")) = False Select (p)).ToList
                    Dim xRowIndex As Integer = (From p In dgvData.Rows.Cast(Of DataGridViewRow)() Where CInt(p.Cells(objdgcolhEmployeeId.Index).Value) = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmployeeId.Index).Value) AndAlso _
                                                             CBool(p.Cells(objdgcolhIsGrp.Index).Value) = True Select CInt((p).Index)).First



                    If xRow.Count - 1 = xRowSelected.Count Then
                        dgvData.Rows(xRowIndex).Cells(objdgcolhIscheck.Index).Value = True
                    Else
                        dgvData.Rows(xRowIndex).Cells(objdgcolhIscheck.Index).Value = False
                    End If

                End If

                SetCheckBoxValue()
            End If

            'AddHandler objChkSelectAll.CheckedChanged, AddressOf objChkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvData.CellPainting
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If e.ColumnIndex = dgcolhTransactionDate.Index OrElse e.ColumnIndex = dgcolhEffectiveDate.Index Then
                If dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value IsNot Nothing AndAlso IsDBNull(dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False AndAlso dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString().Trim.Length > 0 AndAlso IsDate(dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = eZeeDate.convertDate(dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()).ToShortDateString
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvData.SelectionChanged
        Try
            If dgvData.SelectedRows.Count <= 0 Then Exit Sub

            btnDelete.Enabled = Not CBool(dgvData.SelectedRows(0).Cells(objdgcolhIsGrp.Index).Value)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region








    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterEmployee.Text = Language._Object.getCaption(Me.gbFilterEmployee.Name, Me.gbFilterEmployee.Text)
            Me.LblOperationType.Text = Language._Object.getCaption(Me.LblOperationType.Name, Me.LblOperationType.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhTransactionDate.HeaderText = Language._Object.getCaption(Me.dgcolhTransactionDate.Name, Me.dgcolhTransactionDate.HeaderText)
            Me.dgcolhEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffectiveDate.Name, Me.dgcolhEffectiveDate.HeaderText)
            Me.dgcolhOperation.HeaderText = Language._Object.getCaption(Me.dgcolhOperation.Name, Me.dgcolhOperation.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Hold")
            Language.setMessage(mstrModuleName, 3, "Unhold")
            Language.setMessage(mstrModuleName, 4, "Please select atleast one transaction to delete.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class