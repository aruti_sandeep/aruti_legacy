﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCandidateFeedback
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCandidateFeedback))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.tbApplicantsEmail = New System.Windows.Forms.TabControl
        Me.tbpApplicantListSendEmail = New System.Windows.Forms.TabPage
        Me.pnlSendEmailApplicants = New System.Windows.Forms.Panel
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.btnSendEmails = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbRecruitmentTemplate = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tlsOperations = New System.Windows.Forms.ToolStrip
        Me.objtlbbtnCut = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnCopy = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnPaste = New System.Windows.Forms.ToolStripButton
        Me.objtoolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.objcboFont = New System.Windows.Forms.ToolStripComboBox
        Me.objcboFontSize = New System.Windows.Forms.ToolStripComboBox
        Me.objToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnColor = New System.Windows.Forms.ToolStripDropDownButton
        Me.objToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnBold = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnItalic = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnUnderline = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnLeftAlign = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnCenterAlign = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRightAlign = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnUndo = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRedo = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnBulletSimple = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnLeftIndent = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRightIndent = New System.Windows.Forms.ToolStripButton
        Me.objbtnSearchLetterGroup = New eZee.Common.eZeeGradientButton
        Me.cboLetterGroup = New System.Windows.Forms.ComboBox
        Me.LblLetterGroup = New System.Windows.Forms.Label
        Me.txtEmailSubject = New eZee.TextBox.AlphanumericTextBox
        Me.rtbLetterPreview = New System.Windows.Forms.RichTextBox
        Me.LblEmailSubject = New System.Windows.Forms.Label
        Me.objbtnSearchLetterTemplate = New eZee.Common.eZeeGradientButton
        Me.cboLetterTemplate = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.pnlColor = New System.Windows.Forms.Panel
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.btnMoreColor = New System.Windows.Forms.Button
        Me.objbtn47 = New System.Windows.Forms.Button
        Me.objbtn34 = New System.Windows.Forms.Button
        Me.objbtn18 = New System.Windows.Forms.Button
        Me.objbtn46 = New System.Windows.Forms.Button
        Me.objbtn32 = New System.Windows.Forms.Button
        Me.objbtn33 = New System.Windows.Forms.Button
        Me.objbtn45 = New System.Windows.Forms.Button
        Me.objbtn19 = New System.Windows.Forms.Button
        Me.objbtn2 = New System.Windows.Forms.Button
        Me.objbtn44 = New System.Windows.Forms.Button
        Me.objbtn31 = New System.Windows.Forms.Button
        Me.objbtn42 = New System.Windows.Forms.Button
        Me.objbtn20 = New System.Windows.Forms.Button
        Me.objbtn43 = New System.Windows.Forms.Button
        Me.objbtn3 = New System.Windows.Forms.Button
        Me.objbtn15 = New System.Windows.Forms.Button
        Me.objbtn26 = New System.Windows.Forms.Button
        Me.objbtn16 = New System.Windows.Forms.Button
        Me.objbtn35 = New System.Windows.Forms.Button
        Me.objbtn41 = New System.Windows.Forms.Button
        Me.objbtn21 = New System.Windows.Forms.Button
        Me.objbtn8 = New System.Windows.Forms.Button
        Me.objbtn4 = New System.Windows.Forms.Button
        Me.objbtn23 = New System.Windows.Forms.Button
        Me.objbtn27 = New System.Windows.Forms.Button
        Me.objbtn1 = New System.Windows.Forms.Button
        Me.objbtn36 = New System.Windows.Forms.Button
        Me.objbtn7 = New System.Windows.Forms.Button
        Me.objbtn22 = New System.Windows.Forms.Button
        Me.objbtn24 = New System.Windows.Forms.Button
        Me.objbtn5 = New System.Windows.Forms.Button
        Me.objbtn12 = New System.Windows.Forms.Button
        Me.objbtn28 = New System.Windows.Forms.Button
        Me.objbtn17 = New System.Windows.Forms.Button
        Me.objbtn37 = New System.Windows.Forms.Button
        Me.objbtn40 = New System.Windows.Forms.Button
        Me.objbtn9 = New System.Windows.Forms.Button
        Me.objbtn25 = New System.Windows.Forms.Button
        Me.objbtn6 = New System.Windows.Forms.Button
        Me.objbtn13 = New System.Windows.Forms.Button
        Me.objbtn29 = New System.Windows.Forms.Button
        Me.objbtn11 = New System.Windows.Forms.Button
        Me.objbtn38 = New System.Windows.Forms.Button
        Me.objbtn39 = New System.Windows.Forms.Button
        Me.objbtn10 = New System.Windows.Forms.Button
        Me.objbtn30 = New System.Windows.Forms.Button
        Me.objbtn14 = New System.Windows.Forms.Button
        Me.objbtn48 = New System.Windows.Forms.Button
        Me.gbApplicantList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnApplicantReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchApplicant = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgApplicants = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhApplicantunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAppCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhApplicant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchApplicant = New eZee.TextBox.AlphanumericTextBox
        Me.tbpApplicantEmailLogs = New System.Windows.Forms.TabPage
        Me.txtELSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlMain = New System.Windows.Forms.Panel
        Me.objwbContent = New System.Windows.Forms.WebBrowser
        Me.dgEmailLogs = New System.Windows.Forms.DataGridView
        Me.dgcolhELApplicant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVacancy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSubject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSendDateTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBody = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbDatesFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnResetEmailLogs = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmailLogs = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.LblToDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.LblFromDate = New System.Windows.Forms.Label
        Me.gbVacancyFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboApplicantType = New System.Windows.Forms.ComboBox
        Me.LblApplicantType = New System.Windows.Forms.Label
        Me.cboViewType = New System.Windows.Forms.ComboBox
        Me.LblViewType = New System.Windows.Forms.Label
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.LblVacancy = New System.Windows.Forms.Label
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.LblVacanyType = New System.Windows.Forms.Label
        Me.bgwSendMail = New System.ComponentModel.BackgroundWorker
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.tbApplicantsEmail.SuspendLayout()
        Me.tbpApplicantListSendEmail.SuspendLayout()
        Me.pnlSendEmailApplicants.SuspendLayout()
        Me.gbRecruitmentTemplate.SuspendLayout()
        Me.tlsOperations.SuspendLayout()
        Me.pnlColor.SuspendLayout()
        Me.gbApplicantList.SuspendLayout()
        CType(Me.objbtnApplicantReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearchApplicant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgApplicants, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpApplicantEmailLogs.SuspendLayout()
        Me.objpnlMain.SuspendLayout()
        CType(Me.dgEmailLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbDatesFilterCriteria.SuspendLayout()
        CType(Me.objbtnResetEmailLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearchEmailLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbVacancyFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.tbApplicantsEmail)
        Me.pnlMain.Controls.Add(Me.gbVacancyFilterCriteria)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(900, 708)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 653)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(900, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(791, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'tbApplicantsEmail
        '
        Me.tbApplicantsEmail.Controls.Add(Me.tbpApplicantListSendEmail)
        Me.tbApplicantsEmail.Controls.Add(Me.tbpApplicantEmailLogs)
        Me.tbApplicantsEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbApplicantsEmail.Location = New System.Drawing.Point(3, 100)
        Me.tbApplicantsEmail.Name = "tbApplicantsEmail"
        Me.tbApplicantsEmail.SelectedIndex = 0
        Me.tbApplicantsEmail.Size = New System.Drawing.Size(893, 553)
        Me.tbApplicantsEmail.TabIndex = 14
        '
        'tbpApplicantListSendEmail
        '
        Me.tbpApplicantListSendEmail.Controls.Add(Me.pnlSendEmailApplicants)
        Me.tbpApplicantListSendEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpApplicantListSendEmail.Location = New System.Drawing.Point(4, 22)
        Me.tbpApplicantListSendEmail.Name = "tbpApplicantListSendEmail"
        Me.tbpApplicantListSendEmail.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpApplicantListSendEmail.Size = New System.Drawing.Size(885, 527)
        Me.tbpApplicantListSendEmail.TabIndex = 0
        Me.tbpApplicantListSendEmail.Text = "Send Emails To Applicant(s)"
        Me.tbpApplicantListSendEmail.UseVisualStyleBackColor = True
        '
        'pnlSendEmailApplicants
        '
        Me.pnlSendEmailApplicants.Controls.Add(Me.pbProgress)
        Me.pnlSendEmailApplicants.Controls.Add(Me.btnSendEmails)
        Me.pnlSendEmailApplicants.Controls.Add(Me.gbRecruitmentTemplate)
        Me.pnlSendEmailApplicants.Controls.Add(Me.gbApplicantList)
        Me.pnlSendEmailApplicants.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSendEmailApplicants.Location = New System.Drawing.Point(3, 3)
        Me.pnlSendEmailApplicants.Name = "pnlSendEmailApplicants"
        Me.pnlSendEmailApplicants.Size = New System.Drawing.Size(879, 521)
        Me.pnlSendEmailApplicants.TabIndex = 0
        '
        'pbProgress
        '
        Me.pbProgress.Location = New System.Drawing.Point(368, 492)
        Me.pbProgress.Margin = New System.Windows.Forms.Padding(0)
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(398, 19)
        Me.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress.TabIndex = 11
        Me.pbProgress.Visible = False
        '
        'btnSendEmails
        '
        Me.btnSendEmails.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSendEmails.BackColor = System.Drawing.Color.White
        Me.btnSendEmails.BackgroundImage = CType(resources.GetObject("btnSendEmails.BackgroundImage"), System.Drawing.Image)
        Me.btnSendEmails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSendEmails.BorderColor = System.Drawing.Color.Empty
        Me.btnSendEmails.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSendEmails.FlatAppearance.BorderSize = 0
        Me.btnSendEmails.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSendEmails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSendEmails.ForeColor = System.Drawing.Color.Black
        Me.btnSendEmails.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSendEmails.GradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmails.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendEmails.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmails.Location = New System.Drawing.Point(775, 487)
        Me.btnSendEmails.Name = "btnSendEmails"
        Me.btnSendEmails.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendEmails.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmails.Size = New System.Drawing.Size(97, 30)
        Me.btnSendEmails.TabIndex = 2
        Me.btnSendEmails.Text = "Send &Emails"
        Me.btnSendEmails.UseVisualStyleBackColor = True
        '
        'gbRecruitmentTemplate
        '
        Me.gbRecruitmentTemplate.BorderColor = System.Drawing.Color.Black
        Me.gbRecruitmentTemplate.Checked = False
        Me.gbRecruitmentTemplate.CollapseAllExceptThis = False
        Me.gbRecruitmentTemplate.CollapsedHoverImage = Nothing
        Me.gbRecruitmentTemplate.CollapsedNormalImage = Nothing
        Me.gbRecruitmentTemplate.CollapsedPressedImage = Nothing
        Me.gbRecruitmentTemplate.CollapseOnLoad = False
        Me.gbRecruitmentTemplate.Controls.Add(Me.tlsOperations)
        Me.gbRecruitmentTemplate.Controls.Add(Me.objbtnSearchLetterGroup)
        Me.gbRecruitmentTemplate.Controls.Add(Me.cboLetterGroup)
        Me.gbRecruitmentTemplate.Controls.Add(Me.LblLetterGroup)
        Me.gbRecruitmentTemplate.Controls.Add(Me.txtEmailSubject)
        Me.gbRecruitmentTemplate.Controls.Add(Me.rtbLetterPreview)
        Me.gbRecruitmentTemplate.Controls.Add(Me.LblEmailSubject)
        Me.gbRecruitmentTemplate.Controls.Add(Me.objbtnSearchLetterTemplate)
        Me.gbRecruitmentTemplate.Controls.Add(Me.cboLetterTemplate)
        Me.gbRecruitmentTemplate.Controls.Add(Me.Label3)
        Me.gbRecruitmentTemplate.Controls.Add(Me.pnlColor)
        Me.gbRecruitmentTemplate.ExpandedHoverImage = Nothing
        Me.gbRecruitmentTemplate.ExpandedNormalImage = Nothing
        Me.gbRecruitmentTemplate.ExpandedPressedImage = Nothing
        Me.gbRecruitmentTemplate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRecruitmentTemplate.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRecruitmentTemplate.HeaderHeight = 25
        Me.gbRecruitmentTemplate.HeaderMessage = ""
        Me.gbRecruitmentTemplate.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRecruitmentTemplate.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRecruitmentTemplate.HeightOnCollapse = 0
        Me.gbRecruitmentTemplate.LeftTextSpace = 0
        Me.gbRecruitmentTemplate.Location = New System.Drawing.Point(359, 5)
        Me.gbRecruitmentTemplate.Name = "gbRecruitmentTemplate"
        Me.gbRecruitmentTemplate.OpenHeight = 91
        Me.gbRecruitmentTemplate.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRecruitmentTemplate.ShowBorder = True
        Me.gbRecruitmentTemplate.ShowCheckBox = False
        Me.gbRecruitmentTemplate.ShowCollapseButton = False
        Me.gbRecruitmentTemplate.ShowDefaultBorderColor = True
        Me.gbRecruitmentTemplate.ShowDownButton = False
        Me.gbRecruitmentTemplate.ShowHeader = True
        Me.gbRecruitmentTemplate.Size = New System.Drawing.Size(513, 475)
        Me.gbRecruitmentTemplate.TabIndex = 241
        Me.gbRecruitmentTemplate.Temp = 0
        Me.gbRecruitmentTemplate.Text = "Letter Template"
        Me.gbRecruitmentTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tlsOperations
        '
        Me.tlsOperations.AutoSize = False
        Me.tlsOperations.BackColor = System.Drawing.Color.Transparent
        Me.tlsOperations.Dock = System.Windows.Forms.DockStyle.None
        Me.tlsOperations.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtlbbtnCut, Me.objtlbbtnCopy, Me.objtlbbtnPaste, Me.objtoolStripSeparator1, Me.objcboFont, Me.objcboFontSize, Me.objToolStripSeparator2, Me.objtlbbtnColor, Me.objToolStripSeparator7, Me.objtlbbtnBold, Me.objtlbbtnItalic, Me.objtlbbtnUnderline, Me.objToolStripSeparator4, Me.objtlbbtnLeftAlign, Me.objtlbbtnCenterAlign, Me.objtlbbtnRightAlign, Me.objToolStripSeparator5, Me.objtlbbtnUndo, Me.objtlbbtnRedo, Me.objToolStripSeparator3, Me.objtlbbtnBulletSimple, Me.objToolStripSeparator8, Me.objtlbbtnLeftIndent, Me.objtlbbtnRightIndent})
        Me.tlsOperations.Location = New System.Drawing.Point(10, 114)
        Me.tlsOperations.Name = "tlsOperations"
        Me.tlsOperations.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tlsOperations.Size = New System.Drawing.Size(494, 30)
        Me.tlsOperations.TabIndex = 250
        '
        'objtlbbtnCut
        '
        Me.objtlbbtnCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCut.Image = CType(resources.GetObject("objtlbbtnCut.Image"), System.Drawing.Image)
        Me.objtlbbtnCut.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCut.Name = "objtlbbtnCut"
        Me.objtlbbtnCut.Size = New System.Drawing.Size(23, 27)
        '
        'objtlbbtnCopy
        '
        Me.objtlbbtnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCopy.Image = CType(resources.GetObject("objtlbbtnCopy.Image"), System.Drawing.Image)
        Me.objtlbbtnCopy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCopy.Name = "objtlbbtnCopy"
        Me.objtlbbtnCopy.Size = New System.Drawing.Size(23, 27)
        '
        'objtlbbtnPaste
        '
        Me.objtlbbtnPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnPaste.Image = CType(resources.GetObject("objtlbbtnPaste.Image"), System.Drawing.Image)
        Me.objtlbbtnPaste.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnPaste.Name = "objtlbbtnPaste"
        Me.objtlbbtnPaste.Size = New System.Drawing.Size(23, 27)
        '
        'objtoolStripSeparator1
        '
        Me.objtoolStripSeparator1.Name = "objtoolStripSeparator1"
        Me.objtoolStripSeparator1.Size = New System.Drawing.Size(6, 30)
        '
        'objcboFont
        '
        Me.objcboFont.DropDownHeight = 150
        Me.objcboFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objcboFont.DropDownWidth = 250
        Me.objcboFont.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objcboFont.IntegralHeight = False
        Me.objcboFont.Name = "objcboFont"
        Me.objcboFont.Size = New System.Drawing.Size(125, 30)
        Me.objcboFont.ToolTipText = "Select Font"
        '
        'objcboFontSize
        '
        Me.objcboFontSize.AutoSize = False
        Me.objcboFontSize.DropDownWidth = 75
        Me.objcboFontSize.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objcboFontSize.Name = "objcboFontSize"
        Me.objcboFontSize.Size = New System.Drawing.Size(40, 23)
        Me.objcboFontSize.ToolTipText = "Font Size"
        '
        'objToolStripSeparator2
        '
        Me.objToolStripSeparator2.Name = "objToolStripSeparator2"
        Me.objToolStripSeparator2.Size = New System.Drawing.Size(6, 30)
        '
        'objtlbbtnColor
        '
        Me.objtlbbtnColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnColor.Image = CType(resources.GetObject("objtlbbtnColor.Image"), System.Drawing.Image)
        Me.objtlbbtnColor.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnColor.Name = "objtlbbtnColor"
        Me.objtlbbtnColor.Size = New System.Drawing.Size(29, 27)
        Me.objtlbbtnColor.ToolTipText = "Color"
        '
        'objToolStripSeparator7
        '
        Me.objToolStripSeparator7.Name = "objToolStripSeparator7"
        Me.objToolStripSeparator7.Size = New System.Drawing.Size(6, 30)
        '
        'objtlbbtnBold
        '
        Me.objtlbbtnBold.CheckOnClick = True
        Me.objtlbbtnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnBold.Image = CType(resources.GetObject("objtlbbtnBold.Image"), System.Drawing.Image)
        Me.objtlbbtnBold.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnBold.Name = "objtlbbtnBold"
        Me.objtlbbtnBold.Size = New System.Drawing.Size(23, 27)
        Me.objtlbbtnBold.ToolTipText = "Bold"
        '
        'objtlbbtnItalic
        '
        Me.objtlbbtnItalic.CheckOnClick = True
        Me.objtlbbtnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnItalic.Image = CType(resources.GetObject("objtlbbtnItalic.Image"), System.Drawing.Image)
        Me.objtlbbtnItalic.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnItalic.Name = "objtlbbtnItalic"
        Me.objtlbbtnItalic.Size = New System.Drawing.Size(23, 27)
        Me.objtlbbtnItalic.ToolTipText = "Italic"
        '
        'objtlbbtnUnderline
        '
        Me.objtlbbtnUnderline.CheckOnClick = True
        Me.objtlbbtnUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnUnderline.Image = CType(resources.GetObject("objtlbbtnUnderline.Image"), System.Drawing.Image)
        Me.objtlbbtnUnderline.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnUnderline.Name = "objtlbbtnUnderline"
        Me.objtlbbtnUnderline.Size = New System.Drawing.Size(23, 27)
        Me.objtlbbtnUnderline.ToolTipText = "Underline"
        '
        'objToolStripSeparator4
        '
        Me.objToolStripSeparator4.Name = "objToolStripSeparator4"
        Me.objToolStripSeparator4.Size = New System.Drawing.Size(6, 30)
        '
        'objtlbbtnLeftAlign
        '
        Me.objtlbbtnLeftAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnLeftAlign.Image = CType(resources.GetObject("objtlbbtnLeftAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnLeftAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnLeftAlign.Name = "objtlbbtnLeftAlign"
        Me.objtlbbtnLeftAlign.Size = New System.Drawing.Size(23, 27)
        Me.objtlbbtnLeftAlign.ToolTipText = "Align Left"
        '
        'objtlbbtnCenterAlign
        '
        Me.objtlbbtnCenterAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCenterAlign.Image = CType(resources.GetObject("objtlbbtnCenterAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnCenterAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCenterAlign.Name = "objtlbbtnCenterAlign"
        Me.objtlbbtnCenterAlign.Size = New System.Drawing.Size(23, 27)
        Me.objtlbbtnCenterAlign.ToolTipText = "Center"
        '
        'objtlbbtnRightAlign
        '
        Me.objtlbbtnRightAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRightAlign.Image = CType(resources.GetObject("objtlbbtnRightAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnRightAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRightAlign.Name = "objtlbbtnRightAlign"
        Me.objtlbbtnRightAlign.Size = New System.Drawing.Size(23, 27)
        Me.objtlbbtnRightAlign.ToolTipText = "Align Right"
        '
        'objToolStripSeparator5
        '
        Me.objToolStripSeparator5.Name = "objToolStripSeparator5"
        Me.objToolStripSeparator5.Size = New System.Drawing.Size(6, 30)
        '
        'objtlbbtnUndo
        '
        Me.objtlbbtnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnUndo.Image = CType(resources.GetObject("objtlbbtnUndo.Image"), System.Drawing.Image)
        Me.objtlbbtnUndo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnUndo.Name = "objtlbbtnUndo"
        Me.objtlbbtnUndo.Size = New System.Drawing.Size(23, 27)
        Me.objtlbbtnUndo.ToolTipText = "Undo"
        '
        'objtlbbtnRedo
        '
        Me.objtlbbtnRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRedo.Image = CType(resources.GetObject("objtlbbtnRedo.Image"), System.Drawing.Image)
        Me.objtlbbtnRedo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRedo.Name = "objtlbbtnRedo"
        Me.objtlbbtnRedo.Size = New System.Drawing.Size(23, 20)
        Me.objtlbbtnRedo.ToolTipText = "Redo"
        '
        'objToolStripSeparator3
        '
        Me.objToolStripSeparator3.Name = "objToolStripSeparator3"
        Me.objToolStripSeparator3.Size = New System.Drawing.Size(6, 475)
        '
        'objtlbbtnBulletSimple
        '
        Me.objtlbbtnBulletSimple.CheckOnClick = True
        Me.objtlbbtnBulletSimple.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnBulletSimple.Image = CType(resources.GetObject("objtlbbtnBulletSimple.Image"), System.Drawing.Image)
        Me.objtlbbtnBulletSimple.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnBulletSimple.Name = "objtlbbtnBulletSimple"
        Me.objtlbbtnBulletSimple.Size = New System.Drawing.Size(23, 20)
        Me.objtlbbtnBulletSimple.ToolTipText = "Insert Bullet"
        '
        'objToolStripSeparator8
        '
        Me.objToolStripSeparator8.Name = "objToolStripSeparator8"
        Me.objToolStripSeparator8.Size = New System.Drawing.Size(6, 32)
        '
        'objtlbbtnLeftIndent
        '
        Me.objtlbbtnLeftIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnLeftIndent.Image = CType(resources.GetObject("objtlbbtnLeftIndent.Image"), System.Drawing.Image)
        Me.objtlbbtnLeftIndent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnLeftIndent.Name = "objtlbbtnLeftIndent"
        Me.objtlbbtnLeftIndent.Size = New System.Drawing.Size(23, 20)
        Me.objtlbbtnLeftIndent.ToolTipText = "Left Indent"
        '
        'objtlbbtnRightIndent
        '
        Me.objtlbbtnRightIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRightIndent.Image = CType(resources.GetObject("objtlbbtnRightIndent.Image"), System.Drawing.Image)
        Me.objtlbbtnRightIndent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRightIndent.Name = "objtlbbtnRightIndent"
        Me.objtlbbtnRightIndent.Size = New System.Drawing.Size(23, 20)
        Me.objtlbbtnRightIndent.ToolTipText = "Right Indent"
        '
        'objbtnSearchLetterGroup
        '
        Me.objbtnSearchLetterGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLetterGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLetterGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLetterGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLetterGroup.BorderSelected = False
        Me.objbtnSearchLetterGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLetterGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLetterGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLetterGroup.Location = New System.Drawing.Point(483, 31)
        Me.objbtnSearchLetterGroup.Name = "objbtnSearchLetterGroup"
        Me.objbtnSearchLetterGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLetterGroup.TabIndex = 248
        '
        'cboLetterGroup
        '
        Me.cboLetterGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLetterGroup.DropDownWidth = 400
        Me.cboLetterGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLetterGroup.FormattingEnabled = True
        Me.cboLetterGroup.Location = New System.Drawing.Point(126, 31)
        Me.cboLetterGroup.Name = "cboLetterGroup"
        Me.cboLetterGroup.Size = New System.Drawing.Size(351, 21)
        Me.cboLetterGroup.TabIndex = 247
        '
        'LblLetterGroup
        '
        Me.LblLetterGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLetterGroup.Location = New System.Drawing.Point(10, 33)
        Me.LblLetterGroup.Name = "LblLetterGroup"
        Me.LblLetterGroup.Size = New System.Drawing.Size(109, 17)
        Me.LblLetterGroup.TabIndex = 246
        Me.LblLetterGroup.Text = "Letter Group"
        '
        'txtEmailSubject
        '
        Me.txtEmailSubject.Flags = 0
        Me.txtEmailSubject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailSubject.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmailSubject.Location = New System.Drawing.Point(126, 84)
        Me.txtEmailSubject.Name = "txtEmailSubject"
        Me.txtEmailSubject.Size = New System.Drawing.Size(378, 21)
        Me.txtEmailSubject.TabIndex = 244
        '
        'rtbLetterPreview
        '
        Me.rtbLetterPreview.BackColor = System.Drawing.Color.White
        Me.rtbLetterPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rtbLetterPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.rtbLetterPreview.Location = New System.Drawing.Point(6, 147)
        Me.rtbLetterPreview.Name = "rtbLetterPreview"
        Me.rtbLetterPreview.ReadOnly = True
        Me.rtbLetterPreview.Size = New System.Drawing.Size(503, 323)
        Me.rtbLetterPreview.TabIndex = 242
        Me.rtbLetterPreview.Text = ""
        '
        'LblEmailSubject
        '
        Me.LblEmailSubject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.LblEmailSubject.Location = New System.Drawing.Point(10, 86)
        Me.LblEmailSubject.Name = "LblEmailSubject"
        Me.LblEmailSubject.Size = New System.Drawing.Size(109, 17)
        Me.LblEmailSubject.TabIndex = 243
        Me.LblEmailSubject.Text = "Email Subject"
        Me.LblEmailSubject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchLetterTemplate
        '
        Me.objbtnSearchLetterTemplate.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLetterTemplate.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLetterTemplate.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLetterTemplate.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLetterTemplate.BorderSelected = False
        Me.objbtnSearchLetterTemplate.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLetterTemplate.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLetterTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLetterTemplate.Location = New System.Drawing.Point(483, 58)
        Me.objbtnSearchLetterTemplate.Name = "objbtnSearchLetterTemplate"
        Me.objbtnSearchLetterTemplate.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLetterTemplate.TabIndex = 241
        '
        'cboLetterTemplate
        '
        Me.cboLetterTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLetterTemplate.DropDownWidth = 400
        Me.cboLetterTemplate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLetterTemplate.FormattingEnabled = True
        Me.cboLetterTemplate.Location = New System.Drawing.Point(126, 58)
        Me.cboLetterTemplate.Name = "cboLetterTemplate"
        Me.cboLetterTemplate.Size = New System.Drawing.Size(351, 21)
        Me.cboLetterTemplate.TabIndex = 87
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 17)
        Me.Label3.TabIndex = 85
        Me.Label3.Text = "Letter Template"
        '
        'pnlColor
        '
        Me.pnlColor.BackColor = System.Drawing.Color.White
        Me.pnlColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlColor.Controls.Add(Me.EZeeStraightLine1)
        Me.pnlColor.Controls.Add(Me.btnMoreColor)
        Me.pnlColor.Controls.Add(Me.objbtn47)
        Me.pnlColor.Controls.Add(Me.objbtn34)
        Me.pnlColor.Controls.Add(Me.objbtn18)
        Me.pnlColor.Controls.Add(Me.objbtn46)
        Me.pnlColor.Controls.Add(Me.objbtn32)
        Me.pnlColor.Controls.Add(Me.objbtn33)
        Me.pnlColor.Controls.Add(Me.objbtn45)
        Me.pnlColor.Controls.Add(Me.objbtn19)
        Me.pnlColor.Controls.Add(Me.objbtn2)
        Me.pnlColor.Controls.Add(Me.objbtn44)
        Me.pnlColor.Controls.Add(Me.objbtn31)
        Me.pnlColor.Controls.Add(Me.objbtn42)
        Me.pnlColor.Controls.Add(Me.objbtn20)
        Me.pnlColor.Controls.Add(Me.objbtn43)
        Me.pnlColor.Controls.Add(Me.objbtn3)
        Me.pnlColor.Controls.Add(Me.objbtn15)
        Me.pnlColor.Controls.Add(Me.objbtn26)
        Me.pnlColor.Controls.Add(Me.objbtn16)
        Me.pnlColor.Controls.Add(Me.objbtn35)
        Me.pnlColor.Controls.Add(Me.objbtn41)
        Me.pnlColor.Controls.Add(Me.objbtn21)
        Me.pnlColor.Controls.Add(Me.objbtn8)
        Me.pnlColor.Controls.Add(Me.objbtn4)
        Me.pnlColor.Controls.Add(Me.objbtn23)
        Me.pnlColor.Controls.Add(Me.objbtn27)
        Me.pnlColor.Controls.Add(Me.objbtn1)
        Me.pnlColor.Controls.Add(Me.objbtn36)
        Me.pnlColor.Controls.Add(Me.objbtn7)
        Me.pnlColor.Controls.Add(Me.objbtn22)
        Me.pnlColor.Controls.Add(Me.objbtn24)
        Me.pnlColor.Controls.Add(Me.objbtn5)
        Me.pnlColor.Controls.Add(Me.objbtn12)
        Me.pnlColor.Controls.Add(Me.objbtn28)
        Me.pnlColor.Controls.Add(Me.objbtn17)
        Me.pnlColor.Controls.Add(Me.objbtn37)
        Me.pnlColor.Controls.Add(Me.objbtn40)
        Me.pnlColor.Controls.Add(Me.objbtn9)
        Me.pnlColor.Controls.Add(Me.objbtn25)
        Me.pnlColor.Controls.Add(Me.objbtn6)
        Me.pnlColor.Controls.Add(Me.objbtn13)
        Me.pnlColor.Controls.Add(Me.objbtn29)
        Me.pnlColor.Controls.Add(Me.objbtn11)
        Me.pnlColor.Controls.Add(Me.objbtn38)
        Me.pnlColor.Controls.Add(Me.objbtn39)
        Me.pnlColor.Controls.Add(Me.objbtn10)
        Me.pnlColor.Controls.Add(Me.objbtn30)
        Me.pnlColor.Controls.Add(Me.objbtn14)
        Me.pnlColor.Controls.Add(Me.objbtn48)
        Me.pnlColor.Location = New System.Drawing.Point(254, 147)
        Me.pnlColor.Name = "pnlColor"
        Me.pnlColor.Size = New System.Drawing.Size(214, 197)
        Me.pnlColor.TabIndex = 252
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(5, 161)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(202, 3)
        Me.EZeeStraightLine1.TabIndex = 3
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'btnMoreColor
        '
        Me.btnMoreColor.BackColor = System.Drawing.Color.White
        Me.btnMoreColor.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnMoreColor.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnMoreColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnMoreColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMoreColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoreColor.Location = New System.Drawing.Point(5, 164)
        Me.btnMoreColor.Name = "btnMoreColor"
        Me.btnMoreColor.Size = New System.Drawing.Size(202, 27)
        Me.btnMoreColor.TabIndex = 68
        Me.btnMoreColor.Text = "More Color"
        Me.btnMoreColor.UseVisualStyleBackColor = False
        '
        'objbtn47
        '
        Me.objbtn47.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn47.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn47.Location = New System.Drawing.Point(161, 135)
        Me.objbtn47.Name = "objbtn47"
        Me.objbtn47.Size = New System.Drawing.Size(20, 20)
        Me.objbtn47.TabIndex = 75
        Me.objbtn47.UseVisualStyleBackColor = False
        '
        'objbtn34
        '
        Me.objbtn34.BackColor = System.Drawing.Color.Maroon
        Me.objbtn34.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn34.Location = New System.Drawing.Point(31, 109)
        Me.objbtn34.Name = "objbtn34"
        Me.objbtn34.Size = New System.Drawing.Size(20, 20)
        Me.objbtn34.TabIndex = 61
        Me.objbtn34.UseVisualStyleBackColor = False
        '
        'objbtn18
        '
        Me.objbtn18.BackColor = System.Drawing.Color.Red
        Me.objbtn18.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn18.Location = New System.Drawing.Point(31, 57)
        Me.objbtn18.Name = "objbtn18"
        Me.objbtn18.Size = New System.Drawing.Size(20, 20)
        Me.objbtn18.TabIndex = 37
        Me.objbtn18.UseVisualStyleBackColor = False
        '
        'objbtn46
        '
        Me.objbtn46.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn46.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn46.Location = New System.Drawing.Point(135, 135)
        Me.objbtn46.Name = "objbtn46"
        Me.objbtn46.Size = New System.Drawing.Size(20, 20)
        Me.objbtn46.TabIndex = 74
        Me.objbtn46.UseVisualStyleBackColor = False
        '
        'objbtn32
        '
        Me.objbtn32.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn32.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn32.Location = New System.Drawing.Point(187, 83)
        Me.objbtn32.Name = "objbtn32"
        Me.objbtn32.Size = New System.Drawing.Size(20, 20)
        Me.objbtn32.TabIndex = 59
        Me.objbtn32.UseVisualStyleBackColor = False
        '
        'objbtn33
        '
        Me.objbtn33.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn33.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn33.Location = New System.Drawing.Point(5, 109)
        Me.objbtn33.Name = "objbtn33"
        Me.objbtn33.Size = New System.Drawing.Size(20, 20)
        Me.objbtn33.TabIndex = 60
        Me.objbtn33.UseVisualStyleBackColor = False
        '
        'objbtn45
        '
        Me.objbtn45.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn45.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn45.Location = New System.Drawing.Point(109, 135)
        Me.objbtn45.Name = "objbtn45"
        Me.objbtn45.Size = New System.Drawing.Size(20, 20)
        Me.objbtn45.TabIndex = 73
        Me.objbtn45.UseVisualStyleBackColor = False
        '
        'objbtn19
        '
        Me.objbtn19.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn19.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn19.Location = New System.Drawing.Point(57, 57)
        Me.objbtn19.Name = "objbtn19"
        Me.objbtn19.Size = New System.Drawing.Size(20, 20)
        Me.objbtn19.TabIndex = 38
        Me.objbtn19.UseVisualStyleBackColor = False
        '
        'objbtn2
        '
        Me.objbtn2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn2.Location = New System.Drawing.Point(31, 5)
        Me.objbtn2.Name = "objbtn2"
        Me.objbtn2.Size = New System.Drawing.Size(20, 20)
        Me.objbtn2.TabIndex = 36
        Me.objbtn2.UseVisualStyleBackColor = False
        '
        'objbtn44
        '
        Me.objbtn44.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn44.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn44.Location = New System.Drawing.Point(83, 135)
        Me.objbtn44.Name = "objbtn44"
        Me.objbtn44.Size = New System.Drawing.Size(20, 20)
        Me.objbtn44.TabIndex = 72
        Me.objbtn44.UseVisualStyleBackColor = False
        '
        'objbtn31
        '
        Me.objbtn31.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn31.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn31.Location = New System.Drawing.Point(161, 83)
        Me.objbtn31.Name = "objbtn31"
        Me.objbtn31.Size = New System.Drawing.Size(20, 20)
        Me.objbtn31.TabIndex = 58
        Me.objbtn31.UseVisualStyleBackColor = False
        '
        'objbtn42
        '
        Me.objbtn42.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn42.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn42.Location = New System.Drawing.Point(31, 135)
        Me.objbtn42.Name = "objbtn42"
        Me.objbtn42.Size = New System.Drawing.Size(20, 20)
        Me.objbtn42.TabIndex = 70
        Me.objbtn42.UseVisualStyleBackColor = False
        '
        'objbtn20
        '
        Me.objbtn20.BackColor = System.Drawing.Color.Yellow
        Me.objbtn20.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn20.Location = New System.Drawing.Point(83, 57)
        Me.objbtn20.Name = "objbtn20"
        Me.objbtn20.Size = New System.Drawing.Size(20, 20)
        Me.objbtn20.TabIndex = 39
        Me.objbtn20.UseVisualStyleBackColor = False
        '
        'objbtn43
        '
        Me.objbtn43.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn43.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn43.Location = New System.Drawing.Point(57, 135)
        Me.objbtn43.Name = "objbtn43"
        Me.objbtn43.Size = New System.Drawing.Size(20, 20)
        Me.objbtn43.TabIndex = 71
        Me.objbtn43.UseVisualStyleBackColor = False
        '
        'objbtn3
        '
        Me.objbtn3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn3.Location = New System.Drawing.Point(57, 5)
        Me.objbtn3.Name = "objbtn3"
        Me.objbtn3.Size = New System.Drawing.Size(20, 20)
        Me.objbtn3.TabIndex = 35
        Me.objbtn3.UseVisualStyleBackColor = False
        '
        'objbtn15
        '
        Me.objbtn15.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn15.Location = New System.Drawing.Point(161, 31)
        Me.objbtn15.Name = "objbtn15"
        Me.objbtn15.Size = New System.Drawing.Size(20, 20)
        Me.objbtn15.TabIndex = 48
        Me.objbtn15.UseVisualStyleBackColor = False
        '
        'objbtn26
        '
        Me.objbtn26.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn26.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn26.Location = New System.Drawing.Point(31, 83)
        Me.objbtn26.Name = "objbtn26"
        Me.objbtn26.Size = New System.Drawing.Size(20, 20)
        Me.objbtn26.TabIndex = 57
        Me.objbtn26.UseVisualStyleBackColor = False
        '
        'objbtn16
        '
        Me.objbtn16.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn16.Location = New System.Drawing.Point(187, 31)
        Me.objbtn16.Name = "objbtn16"
        Me.objbtn16.Size = New System.Drawing.Size(20, 20)
        Me.objbtn16.TabIndex = 49
        Me.objbtn16.UseVisualStyleBackColor = False
        '
        'objbtn35
        '
        Me.objbtn35.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn35.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn35.Location = New System.Drawing.Point(57, 109)
        Me.objbtn35.Name = "objbtn35"
        Me.objbtn35.Size = New System.Drawing.Size(20, 20)
        Me.objbtn35.TabIndex = 62
        Me.objbtn35.UseVisualStyleBackColor = False
        '
        'objbtn41
        '
        Me.objbtn41.BackColor = System.Drawing.Color.Black
        Me.objbtn41.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn41.Location = New System.Drawing.Point(5, 135)
        Me.objbtn41.Name = "objbtn41"
        Me.objbtn41.Size = New System.Drawing.Size(20, 20)
        Me.objbtn41.TabIndex = 69
        Me.objbtn41.UseVisualStyleBackColor = False
        '
        'objbtn21
        '
        Me.objbtn21.BackColor = System.Drawing.Color.Lime
        Me.objbtn21.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn21.Location = New System.Drawing.Point(109, 57)
        Me.objbtn21.Name = "objbtn21"
        Me.objbtn21.Size = New System.Drawing.Size(20, 20)
        Me.objbtn21.TabIndex = 40
        Me.objbtn21.UseVisualStyleBackColor = False
        '
        'objbtn8
        '
        Me.objbtn8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn8.Location = New System.Drawing.Point(187, 5)
        Me.objbtn8.Name = "objbtn8"
        Me.objbtn8.Size = New System.Drawing.Size(20, 20)
        Me.objbtn8.TabIndex = 47
        Me.objbtn8.UseVisualStyleBackColor = False
        '
        'objbtn4
        '
        Me.objbtn4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn4.Location = New System.Drawing.Point(83, 5)
        Me.objbtn4.Name = "objbtn4"
        Me.objbtn4.Size = New System.Drawing.Size(20, 20)
        Me.objbtn4.TabIndex = 34
        Me.objbtn4.UseVisualStyleBackColor = False
        '
        'objbtn23
        '
        Me.objbtn23.BackColor = System.Drawing.Color.Blue
        Me.objbtn23.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn23.Location = New System.Drawing.Point(161, 57)
        Me.objbtn23.Name = "objbtn23"
        Me.objbtn23.Size = New System.Drawing.Size(20, 20)
        Me.objbtn23.TabIndex = 50
        Me.objbtn23.UseVisualStyleBackColor = False
        '
        'objbtn27
        '
        Me.objbtn27.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn27.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn27.Location = New System.Drawing.Point(57, 83)
        Me.objbtn27.Name = "objbtn27"
        Me.objbtn27.Size = New System.Drawing.Size(20, 20)
        Me.objbtn27.TabIndex = 56
        Me.objbtn27.UseVisualStyleBackColor = False
        '
        'objbtn1
        '
        Me.objbtn1.BackColor = System.Drawing.Color.White
        Me.objbtn1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn1.Location = New System.Drawing.Point(5, 5)
        Me.objbtn1.Name = "objbtn1"
        Me.objbtn1.Size = New System.Drawing.Size(20, 20)
        Me.objbtn1.TabIndex = 28
        Me.objbtn1.UseVisualStyleBackColor = False
        '
        'objbtn36
        '
        Me.objbtn36.BackColor = System.Drawing.Color.Olive
        Me.objbtn36.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn36.Location = New System.Drawing.Point(83, 109)
        Me.objbtn36.Name = "objbtn36"
        Me.objbtn36.Size = New System.Drawing.Size(20, 20)
        Me.objbtn36.TabIndex = 63
        Me.objbtn36.UseVisualStyleBackColor = False
        '
        'objbtn7
        '
        Me.objbtn7.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn7.Location = New System.Drawing.Point(161, 5)
        Me.objbtn7.Name = "objbtn7"
        Me.objbtn7.Size = New System.Drawing.Size(20, 20)
        Me.objbtn7.TabIndex = 46
        Me.objbtn7.UseVisualStyleBackColor = False
        '
        'objbtn22
        '
        Me.objbtn22.BackColor = System.Drawing.Color.Cyan
        Me.objbtn22.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn22.Location = New System.Drawing.Point(135, 57)
        Me.objbtn22.Name = "objbtn22"
        Me.objbtn22.Size = New System.Drawing.Size(20, 20)
        Me.objbtn22.TabIndex = 41
        Me.objbtn22.UseVisualStyleBackColor = False
        '
        'objbtn24
        '
        Me.objbtn24.BackColor = System.Drawing.Color.Fuchsia
        Me.objbtn24.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn24.Location = New System.Drawing.Point(187, 57)
        Me.objbtn24.Name = "objbtn24"
        Me.objbtn24.Size = New System.Drawing.Size(20, 20)
        Me.objbtn24.TabIndex = 51
        Me.objbtn24.UseVisualStyleBackColor = False
        '
        'objbtn5
        '
        Me.objbtn5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn5.Location = New System.Drawing.Point(109, 5)
        Me.objbtn5.Name = "objbtn5"
        Me.objbtn5.Size = New System.Drawing.Size(20, 20)
        Me.objbtn5.TabIndex = 33
        Me.objbtn5.UseVisualStyleBackColor = False
        '
        'objbtn12
        '
        Me.objbtn12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn12.Location = New System.Drawing.Point(83, 31)
        Me.objbtn12.Name = "objbtn12"
        Me.objbtn12.Size = New System.Drawing.Size(20, 20)
        Me.objbtn12.TabIndex = 29
        Me.objbtn12.UseVisualStyleBackColor = False
        '
        'objbtn28
        '
        Me.objbtn28.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn28.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn28.Location = New System.Drawing.Point(83, 83)
        Me.objbtn28.Name = "objbtn28"
        Me.objbtn28.Size = New System.Drawing.Size(20, 20)
        Me.objbtn28.TabIndex = 55
        Me.objbtn28.UseVisualStyleBackColor = False
        '
        'objbtn17
        '
        Me.objbtn17.BackColor = System.Drawing.Color.Silver
        Me.objbtn17.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn17.Location = New System.Drawing.Point(5, 57)
        Me.objbtn17.Name = "objbtn17"
        Me.objbtn17.Size = New System.Drawing.Size(20, 20)
        Me.objbtn17.TabIndex = 45
        Me.objbtn17.UseVisualStyleBackColor = False
        '
        'objbtn37
        '
        Me.objbtn37.BackColor = System.Drawing.Color.Green
        Me.objbtn37.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn37.Location = New System.Drawing.Point(109, 109)
        Me.objbtn37.Name = "objbtn37"
        Me.objbtn37.Size = New System.Drawing.Size(20, 20)
        Me.objbtn37.TabIndex = 64
        Me.objbtn37.UseVisualStyleBackColor = False
        '
        'objbtn40
        '
        Me.objbtn40.BackColor = System.Drawing.Color.Purple
        Me.objbtn40.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn40.Location = New System.Drawing.Point(187, 109)
        Me.objbtn40.Name = "objbtn40"
        Me.objbtn40.Size = New System.Drawing.Size(20, 20)
        Me.objbtn40.TabIndex = 67
        Me.objbtn40.UseVisualStyleBackColor = False
        '
        'objbtn9
        '
        Me.objbtn9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.objbtn9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn9.Location = New System.Drawing.Point(5, 31)
        Me.objbtn9.Name = "objbtn9"
        Me.objbtn9.Size = New System.Drawing.Size(20, 20)
        Me.objbtn9.TabIndex = 42
        Me.objbtn9.UseVisualStyleBackColor = False
        '
        'objbtn25
        '
        Me.objbtn25.BackColor = System.Drawing.Color.Gray
        Me.objbtn25.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn25.Location = New System.Drawing.Point(5, 83)
        Me.objbtn25.Name = "objbtn25"
        Me.objbtn25.Size = New System.Drawing.Size(20, 20)
        Me.objbtn25.TabIndex = 52
        Me.objbtn25.UseVisualStyleBackColor = False
        '
        'objbtn6
        '
        Me.objbtn6.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn6.Location = New System.Drawing.Point(135, 5)
        Me.objbtn6.Name = "objbtn6"
        Me.objbtn6.Size = New System.Drawing.Size(20, 20)
        Me.objbtn6.TabIndex = 32
        Me.objbtn6.UseVisualStyleBackColor = False
        '
        'objbtn13
        '
        Me.objbtn13.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn13.Location = New System.Drawing.Point(109, 31)
        Me.objbtn13.Name = "objbtn13"
        Me.objbtn13.Size = New System.Drawing.Size(20, 20)
        Me.objbtn13.TabIndex = 30
        Me.objbtn13.UseVisualStyleBackColor = False
        '
        'objbtn29
        '
        Me.objbtn29.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn29.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn29.Location = New System.Drawing.Point(109, 83)
        Me.objbtn29.Name = "objbtn29"
        Me.objbtn29.Size = New System.Drawing.Size(20, 20)
        Me.objbtn29.TabIndex = 54
        Me.objbtn29.UseVisualStyleBackColor = False
        '
        'objbtn11
        '
        Me.objbtn11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn11.Location = New System.Drawing.Point(57, 31)
        Me.objbtn11.Name = "objbtn11"
        Me.objbtn11.Size = New System.Drawing.Size(20, 20)
        Me.objbtn11.TabIndex = 44
        Me.objbtn11.UseVisualStyleBackColor = False
        '
        'objbtn38
        '
        Me.objbtn38.BackColor = System.Drawing.Color.Teal
        Me.objbtn38.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn38.Location = New System.Drawing.Point(135, 109)
        Me.objbtn38.Name = "objbtn38"
        Me.objbtn38.Size = New System.Drawing.Size(20, 20)
        Me.objbtn38.TabIndex = 65
        Me.objbtn38.UseVisualStyleBackColor = False
        '
        'objbtn39
        '
        Me.objbtn39.BackColor = System.Drawing.Color.Navy
        Me.objbtn39.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn39.Location = New System.Drawing.Point(161, 109)
        Me.objbtn39.Name = "objbtn39"
        Me.objbtn39.Size = New System.Drawing.Size(20, 20)
        Me.objbtn39.TabIndex = 66
        Me.objbtn39.UseVisualStyleBackColor = False
        '
        'objbtn10
        '
        Me.objbtn10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn10.Location = New System.Drawing.Point(31, 31)
        Me.objbtn10.Name = "objbtn10"
        Me.objbtn10.Size = New System.Drawing.Size(20, 20)
        Me.objbtn10.TabIndex = 43
        Me.objbtn10.UseVisualStyleBackColor = False
        '
        'objbtn30
        '
        Me.objbtn30.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn30.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn30.Location = New System.Drawing.Point(135, 83)
        Me.objbtn30.Name = "objbtn30"
        Me.objbtn30.Size = New System.Drawing.Size(20, 20)
        Me.objbtn30.TabIndex = 53
        Me.objbtn30.UseVisualStyleBackColor = False
        '
        'objbtn14
        '
        Me.objbtn14.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn14.Location = New System.Drawing.Point(135, 31)
        Me.objbtn14.Name = "objbtn14"
        Me.objbtn14.Size = New System.Drawing.Size(20, 20)
        Me.objbtn14.TabIndex = 31
        Me.objbtn14.UseVisualStyleBackColor = False
        '
        'objbtn48
        '
        Me.objbtn48.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn48.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn48.Location = New System.Drawing.Point(187, 135)
        Me.objbtn48.Name = "objbtn48"
        Me.objbtn48.Size = New System.Drawing.Size(20, 20)
        Me.objbtn48.TabIndex = 76
        Me.objbtn48.UseVisualStyleBackColor = False
        '
        'gbApplicantList
        '
        Me.gbApplicantList.BorderColor = System.Drawing.Color.Black
        Me.gbApplicantList.Checked = False
        Me.gbApplicantList.CollapseAllExceptThis = False
        Me.gbApplicantList.CollapsedHoverImage = Nothing
        Me.gbApplicantList.CollapsedNormalImage = Nothing
        Me.gbApplicantList.CollapsedPressedImage = Nothing
        Me.gbApplicantList.CollapseOnLoad = False
        Me.gbApplicantList.Controls.Add(Me.objbtnApplicantReset)
        Me.gbApplicantList.Controls.Add(Me.objbtnSearchApplicant)
        Me.gbApplicantList.Controls.Add(Me.pnlEmployeeList)
        Me.gbApplicantList.ExpandedHoverImage = Nothing
        Me.gbApplicantList.ExpandedNormalImage = Nothing
        Me.gbApplicantList.ExpandedPressedImage = Nothing
        Me.gbApplicantList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplicantList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApplicantList.HeaderHeight = 25
        Me.gbApplicantList.HeaderMessage = ""
        Me.gbApplicantList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplicantList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApplicantList.HeightOnCollapse = 0
        Me.gbApplicantList.LeftTextSpace = 0
        Me.gbApplicantList.Location = New System.Drawing.Point(4, 4)
        Me.gbApplicantList.Name = "gbApplicantList"
        Me.gbApplicantList.OpenHeight = 300
        Me.gbApplicantList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApplicantList.ShowBorder = True
        Me.gbApplicantList.ShowCheckBox = False
        Me.gbApplicantList.ShowCollapseButton = False
        Me.gbApplicantList.ShowDefaultBorderColor = True
        Me.gbApplicantList.ShowDownButton = False
        Me.gbApplicantList.ShowHeader = True
        Me.gbApplicantList.Size = New System.Drawing.Size(349, 513)
        Me.gbApplicantList.TabIndex = 3
        Me.gbApplicantList.Temp = 0
        Me.gbApplicantList.Text = "Applicant(s)"
        Me.gbApplicantList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnApplicantReset
        '
        Me.objbtnApplicantReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnApplicantReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnApplicantReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnApplicantReset.Image = CType(resources.GetObject("objbtnApplicantReset.Image"), System.Drawing.Image)
        Me.objbtnApplicantReset.Location = New System.Drawing.Point(325, 0)
        Me.objbtnApplicantReset.Name = "objbtnApplicantReset"
        Me.objbtnApplicantReset.ResultMessage = ""
        Me.objbtnApplicantReset.SearchMessage = ""
        Me.objbtnApplicantReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnApplicantReset.TabIndex = 243
        Me.objbtnApplicantReset.TabStop = False
        '
        'objbtnSearchApplicant
        '
        Me.objbtnSearchApplicant.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearchApplicant.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearchApplicant.Image = CType(resources.GetObject("objbtnSearchApplicant.Image"), System.Drawing.Image)
        Me.objbtnSearchApplicant.Location = New System.Drawing.Point(300, 0)
        Me.objbtnSearchApplicant.Name = "objbtnSearchApplicant"
        Me.objbtnSearchApplicant.ResultMessage = ""
        Me.objbtnSearchApplicant.SearchMessage = ""
        Me.objbtnSearchApplicant.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearchApplicant.TabIndex = 162
        Me.objbtnSearchApplicant.TabStop = False
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgApplicants)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchApplicant)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(346, 484)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 34)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgApplicants
        '
        Me.dgApplicants.AllowUserToAddRows = False
        Me.dgApplicants.AllowUserToDeleteRows = False
        Me.dgApplicants.AllowUserToResizeRows = False
        Me.dgApplicants.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgApplicants.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgApplicants.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgApplicants.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgApplicants.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgApplicants.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhApplicantunkid, Me.dgColhAppCode, Me.dgColhApplicant})
        Me.dgApplicants.Location = New System.Drawing.Point(1, 28)
        Me.dgApplicants.Name = "dgApplicants"
        Me.dgApplicants.RowHeadersVisible = False
        Me.dgApplicants.RowHeadersWidth = 5
        Me.dgApplicants.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgApplicants.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgApplicants.Size = New System.Drawing.Size(343, 456)
        Me.dgApplicants.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhApplicantunkid
        '
        Me.objdgcolhApplicantunkid.HeaderText = "Applicantunkid"
        Me.objdgcolhApplicantunkid.Name = "objdgcolhApplicantunkid"
        Me.objdgcolhApplicantunkid.ReadOnly = True
        Me.objdgcolhApplicantunkid.Visible = False
        '
        'dgColhAppCode
        '
        Me.dgColhAppCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhAppCode.HeaderText = "App. Code"
        Me.dgColhAppCode.Name = "dgColhAppCode"
        Me.dgColhAppCode.ReadOnly = True
        Me.dgColhAppCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhAppCode.Width = 70
        '
        'dgColhApplicant
        '
        Me.dgColhApplicant.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhApplicant.HeaderText = "Applicant Name"
        Me.dgColhApplicant.Name = "dgColhApplicant"
        Me.dgColhApplicant.ReadOnly = True
        '
        'txtSearchApplicant
        '
        Me.txtSearchApplicant.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchApplicant.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchApplicant.Flags = 0
        Me.txtSearchApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchApplicant.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchApplicant.Location = New System.Drawing.Point(2, 4)
        Me.txtSearchApplicant.Name = "txtSearchApplicant"
        Me.txtSearchApplicant.Size = New System.Drawing.Size(342, 21)
        Me.txtSearchApplicant.TabIndex = 12
        '
        'tbpApplicantEmailLogs
        '
        Me.tbpApplicantEmailLogs.Controls.Add(Me.txtELSearch)
        Me.tbpApplicantEmailLogs.Controls.Add(Me.objpnlMain)
        Me.tbpApplicantEmailLogs.Controls.Add(Me.dgEmailLogs)
        Me.tbpApplicantEmailLogs.Controls.Add(Me.gbDatesFilterCriteria)
        Me.tbpApplicantEmailLogs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpApplicantEmailLogs.Location = New System.Drawing.Point(4, 22)
        Me.tbpApplicantEmailLogs.Name = "tbpApplicantEmailLogs"
        Me.tbpApplicantEmailLogs.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpApplicantEmailLogs.Size = New System.Drawing.Size(885, 527)
        Me.tbpApplicantEmailLogs.TabIndex = 1
        Me.tbpApplicantEmailLogs.Text = "Sent Email Logs"
        Me.tbpApplicantEmailLogs.UseVisualStyleBackColor = True
        '
        'txtELSearch
        '
        Me.txtELSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtELSearch.BackColor = System.Drawing.SystemColors.Window
        Me.txtELSearch.Flags = 0
        Me.txtELSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtELSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtELSearch.Location = New System.Drawing.Point(6, 67)
        Me.txtELSearch.Name = "txtELSearch"
        Me.txtELSearch.Size = New System.Drawing.Size(872, 21)
        Me.txtELSearch.TabIndex = 291
        '
        'objpnlMain
        '
        Me.objpnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objpnlMain.Controls.Add(Me.objwbContent)
        Me.objpnlMain.Location = New System.Drawing.Point(6, 307)
        Me.objpnlMain.Name = "objpnlMain"
        Me.objpnlMain.Size = New System.Drawing.Size(872, 214)
        Me.objpnlMain.TabIndex = 290
        '
        'objwbContent
        '
        Me.objwbContent.AllowNavigation = False
        Me.objwbContent.AllowWebBrowserDrop = False
        Me.objwbContent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objwbContent.IsWebBrowserContextMenuEnabled = False
        Me.objwbContent.Location = New System.Drawing.Point(0, 0)
        Me.objwbContent.MinimumSize = New System.Drawing.Size(20, 20)
        Me.objwbContent.Name = "objwbContent"
        Me.objwbContent.Size = New System.Drawing.Size(870, 212)
        Me.objwbContent.TabIndex = 289
        Me.objwbContent.WebBrowserShortcutsEnabled = False
        '
        'dgEmailLogs
        '
        Me.dgEmailLogs.AllowUserToAddRows = False
        Me.dgEmailLogs.AllowUserToDeleteRows = False
        Me.dgEmailLogs.AllowUserToResizeRows = False
        Me.dgEmailLogs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmailLogs.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmailLogs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmailLogs.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmailLogs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmailLogs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhELApplicant, Me.dgcolhEmail, Me.dgcolhVacancy, Me.dgcolhSubject, Me.dgcolhSendDateTime, Me.objdgcolhBody})
        Me.dgEmailLogs.Location = New System.Drawing.Point(6, 90)
        Me.dgEmailLogs.Name = "dgEmailLogs"
        Me.dgEmailLogs.ReadOnly = True
        Me.dgEmailLogs.RowHeadersVisible = False
        Me.dgEmailLogs.RowHeadersWidth = 5
        Me.dgEmailLogs.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmailLogs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmailLogs.Size = New System.Drawing.Size(872, 212)
        Me.dgEmailLogs.TabIndex = 288
        '
        'dgcolhELApplicant
        '
        Me.dgcolhELApplicant.Frozen = True
        Me.dgcolhELApplicant.HeaderText = "Applicant"
        Me.dgcolhELApplicant.Name = "dgcolhELApplicant"
        Me.dgcolhELApplicant.ReadOnly = True
        Me.dgcolhELApplicant.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhELApplicant.Width = 200
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.Frozen = True
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmail.Width = 200
        '
        'dgcolhVacancy
        '
        Me.dgcolhVacancy.HeaderText = "Vacancy"
        Me.dgcolhVacancy.Name = "dgcolhVacancy"
        Me.dgcolhVacancy.ReadOnly = True
        Me.dgcolhVacancy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhVacancy.Width = 300
        '
        'dgcolhSubject
        '
        Me.dgcolhSubject.HeaderText = "Subject"
        Me.dgcolhSubject.Name = "dgcolhSubject"
        Me.dgcolhSubject.ReadOnly = True
        Me.dgcolhSubject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSubject.Width = 220
        '
        'dgcolhSendDateTime
        '
        Me.dgcolhSendDateTime.HeaderText = "Send Date Time"
        Me.dgcolhSendDateTime.Name = "dgcolhSendDateTime"
        Me.dgcolhSendDateTime.ReadOnly = True
        Me.dgcolhSendDateTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSendDateTime.Width = 150
        '
        'objdgcolhBody
        '
        Me.objdgcolhBody.HeaderText = "Body"
        Me.objdgcolhBody.Name = "objdgcolhBody"
        Me.objdgcolhBody.ReadOnly = True
        Me.objdgcolhBody.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBody.Visible = False
        Me.objdgcolhBody.Width = 275
        '
        'gbDatesFilterCriteria
        '
        Me.gbDatesFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbDatesFilterCriteria.Checked = False
        Me.gbDatesFilterCriteria.CollapseAllExceptThis = False
        Me.gbDatesFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbDatesFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbDatesFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbDatesFilterCriteria.CollapseOnLoad = False
        Me.gbDatesFilterCriteria.Controls.Add(Me.objbtnResetEmailLogs)
        Me.gbDatesFilterCriteria.Controls.Add(Me.objbtnSearchEmailLogs)
        Me.gbDatesFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbDatesFilterCriteria.Controls.Add(Me.LblToDate)
        Me.gbDatesFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbDatesFilterCriteria.Controls.Add(Me.LblFromDate)
        Me.gbDatesFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbDatesFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbDatesFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbDatesFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDatesFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDatesFilterCriteria.HeaderHeight = 25
        Me.gbDatesFilterCriteria.HeaderMessage = ""
        Me.gbDatesFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDatesFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDatesFilterCriteria.HeightOnCollapse = 0
        Me.gbDatesFilterCriteria.LeftTextSpace = 0
        Me.gbDatesFilterCriteria.Location = New System.Drawing.Point(7, 6)
        Me.gbDatesFilterCriteria.Name = "gbDatesFilterCriteria"
        Me.gbDatesFilterCriteria.OpenHeight = 91
        Me.gbDatesFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDatesFilterCriteria.ShowBorder = True
        Me.gbDatesFilterCriteria.ShowCheckBox = False
        Me.gbDatesFilterCriteria.ShowCollapseButton = False
        Me.gbDatesFilterCriteria.ShowDefaultBorderColor = True
        Me.gbDatesFilterCriteria.ShowDownButton = False
        Me.gbDatesFilterCriteria.ShowHeader = True
        Me.gbDatesFilterCriteria.Size = New System.Drawing.Size(871, 59)
        Me.gbDatesFilterCriteria.TabIndex = 244
        Me.gbDatesFilterCriteria.Temp = 0
        Me.gbDatesFilterCriteria.Text = "Filter Criteria"
        Me.gbDatesFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnResetEmailLogs
        '
        Me.objbtnResetEmailLogs.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnResetEmailLogs.BackColor = System.Drawing.Color.Transparent
        Me.objbtnResetEmailLogs.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnResetEmailLogs.Image = CType(resources.GetObject("objbtnResetEmailLogs.Image"), System.Drawing.Image)
        Me.objbtnResetEmailLogs.Location = New System.Drawing.Point(845, 1)
        Me.objbtnResetEmailLogs.Name = "objbtnResetEmailLogs"
        Me.objbtnResetEmailLogs.ResultMessage = ""
        Me.objbtnResetEmailLogs.SearchMessage = ""
        Me.objbtnResetEmailLogs.Size = New System.Drawing.Size(24, 24)
        Me.objbtnResetEmailLogs.TabIndex = 249
        Me.objbtnResetEmailLogs.TabStop = False
        '
        'objbtnSearchEmailLogs
        '
        Me.objbtnSearchEmailLogs.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearchEmailLogs.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmailLogs.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearchEmailLogs.Image = CType(resources.GetObject("objbtnSearchEmailLogs.Image"), System.Drawing.Image)
        Me.objbtnSearchEmailLogs.Location = New System.Drawing.Point(820, 1)
        Me.objbtnSearchEmailLogs.Name = "objbtnSearchEmailLogs"
        Me.objbtnSearchEmailLogs.ResultMessage = ""
        Me.objbtnSearchEmailLogs.SearchMessage = ""
        Me.objbtnSearchEmailLogs.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearchEmailLogs.TabIndex = 248
        Me.objbtnSearchEmailLogs.TabStop = False
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(302, 31)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(125, 21)
        Me.dtpToDate.TabIndex = 246
        '
        'LblToDate
        '
        Me.LblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToDate.Location = New System.Drawing.Point(226, 33)
        Me.LblToDate.Name = "LblToDate"
        Me.LblToDate.Size = New System.Drawing.Size(72, 17)
        Me.LblToDate.TabIndex = 247
        Me.LblToDate.Text = "To Date"
        Me.LblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(86, 31)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(125, 21)
        Me.dtpFromDate.TabIndex = 244
        '
        'LblFromDate
        '
        Me.LblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFromDate.Location = New System.Drawing.Point(7, 33)
        Me.LblFromDate.Name = "LblFromDate"
        Me.LblFromDate.Size = New System.Drawing.Size(73, 17)
        Me.LblFromDate.TabIndex = 245
        Me.LblFromDate.Text = "From Date"
        Me.LblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbVacancyFilterCriteria
        '
        Me.gbVacancyFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbVacancyFilterCriteria.Checked = False
        Me.gbVacancyFilterCriteria.CollapseAllExceptThis = False
        Me.gbVacancyFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbVacancyFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbVacancyFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbVacancyFilterCriteria.CollapseOnLoad = False
        Me.gbVacancyFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.cboApplicantType)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.LblApplicantType)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.cboViewType)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.LblViewType)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.LblVacancy)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbVacancyFilterCriteria.Controls.Add(Me.LblVacanyType)
        Me.gbVacancyFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbVacancyFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbVacancyFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbVacancyFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbVacancyFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbVacancyFilterCriteria.HeaderHeight = 25
        Me.gbVacancyFilterCriteria.HeaderMessage = ""
        Me.gbVacancyFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbVacancyFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbVacancyFilterCriteria.HeightOnCollapse = 0
        Me.gbVacancyFilterCriteria.LeftTextSpace = 0
        Me.gbVacancyFilterCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbVacancyFilterCriteria.Name = "gbVacancyFilterCriteria"
        Me.gbVacancyFilterCriteria.OpenHeight = 91
        Me.gbVacancyFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbVacancyFilterCriteria.ShowBorder = True
        Me.gbVacancyFilterCriteria.ShowCheckBox = False
        Me.gbVacancyFilterCriteria.ShowCollapseButton = False
        Me.gbVacancyFilterCriteria.ShowDefaultBorderColor = True
        Me.gbVacancyFilterCriteria.ShowDownButton = False
        Me.gbVacancyFilterCriteria.ShowHeader = True
        Me.gbVacancyFilterCriteria.Size = New System.Drawing.Size(894, 90)
        Me.gbVacancyFilterCriteria.TabIndex = 13
        Me.gbVacancyFilterCriteria.Temp = 0
        Me.gbVacancyFilterCriteria.Text = "Filter Criteria"
        Me.gbVacancyFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(867, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 242
        Me.objbtnReset.TabStop = False
        '
        'cboApplicantType
        '
        Me.cboApplicantType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicantType.DropDownWidth = 400
        Me.cboApplicantType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicantType.FormattingEnabled = True
        Me.cboApplicantType.Location = New System.Drawing.Point(101, 60)
        Me.cboApplicantType.Name = "cboApplicantType"
        Me.cboApplicantType.Size = New System.Drawing.Size(158, 21)
        Me.cboApplicantType.TabIndex = 245
        '
        'LblApplicantType
        '
        Me.LblApplicantType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblApplicantType.Location = New System.Drawing.Point(8, 62)
        Me.LblApplicantType.Name = "LblApplicantType"
        Me.LblApplicantType.Size = New System.Drawing.Size(89, 17)
        Me.LblApplicantType.TabIndex = 244
        Me.LblApplicantType.Text = "Applicant Type"
        '
        'cboViewType
        '
        Me.cboViewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewType.DropDownWidth = 200
        Me.cboViewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewType.FormattingEnabled = True
        Me.cboViewType.Location = New System.Drawing.Point(670, 33)
        Me.cboViewType.Name = "cboViewType"
        Me.cboViewType.Size = New System.Drawing.Size(216, 21)
        Me.cboViewType.TabIndex = 239
        '
        'LblViewType
        '
        Me.LblViewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblViewType.Location = New System.Drawing.Point(599, 35)
        Me.LblViewType.Name = "LblViewType"
        Me.LblViewType.Size = New System.Drawing.Size(65, 17)
        Me.LblViewType.TabIndex = 238
        Me.LblViewType.Text = "View Type"
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(566, 33)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 86
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 600
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(344, 33)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(216, 21)
        Me.cboVacancy.TabIndex = 237
        '
        'LblVacancy
        '
        Me.LblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVacancy.Location = New System.Drawing.Point(273, 35)
        Me.LblVacancy.Name = "LblVacancy"
        Me.LblVacancy.Size = New System.Drawing.Size(67, 17)
        Me.LblVacancy.TabIndex = 236
        Me.LblVacancy.Text = "Vacancy"
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 400
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(101, 33)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(158, 21)
        Me.cboVacancyType.TabIndex = 87
        '
        'LblVacanyType
        '
        Me.LblVacanyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVacanyType.Location = New System.Drawing.Point(8, 35)
        Me.LblVacanyType.Name = "LblVacanyType"
        Me.LblVacanyType.Size = New System.Drawing.Size(89, 17)
        Me.LblVacanyType.TabIndex = 85
        Me.LblVacanyType.Text = "Vacancy Type"
        '
        'bgwSendMail
        '
        Me.bgwSendMail.WorkerReportsProgress = True
        Me.bgwSendMail.WorkerSupportsCancellation = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Applicantunkid"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.HeaderText = "App. Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "Vacancy"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn6.HeaderText = "Subject"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 220
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn7.HeaderText = "Send Date Time"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Body"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 275
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Body"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        Me.DataGridViewTextBoxColumn9.Width = 275
        '
        'frmCandidateFeedback
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 708)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCandidateFeedback"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Send Candidate Feedback"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.tbApplicantsEmail.ResumeLayout(False)
        Me.tbpApplicantListSendEmail.ResumeLayout(False)
        Me.pnlSendEmailApplicants.ResumeLayout(False)
        Me.gbRecruitmentTemplate.ResumeLayout(False)
        Me.gbRecruitmentTemplate.PerformLayout()
        Me.tlsOperations.ResumeLayout(False)
        Me.tlsOperations.PerformLayout()
        Me.pnlColor.ResumeLayout(False)
        Me.gbApplicantList.ResumeLayout(False)
        CType(Me.objbtnApplicantReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearchApplicant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgApplicants, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpApplicantEmailLogs.ResumeLayout(False)
        Me.tbpApplicantEmailLogs.PerformLayout()
        Me.objpnlMain.ResumeLayout(False)
        CType(Me.dgEmailLogs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbDatesFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnResetEmailLogs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearchEmailLogs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbVacancyFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents gbVacancyFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents LblVacancy As System.Windows.Forms.Label
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents LblVacanyType As System.Windows.Forms.Label
    Friend WithEvents cboViewType As System.Windows.Forms.ComboBox
    Friend WithEvents LblViewType As System.Windows.Forms.Label
    Friend WithEvents tbApplicantsEmail As System.Windows.Forms.TabControl
    Friend WithEvents tbpApplicantListSendEmail As System.Windows.Forms.TabPage
    Friend WithEvents tbpApplicantEmailLogs As System.Windows.Forms.TabPage
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlSendEmailApplicants As System.Windows.Forms.Panel
    Friend WithEvents gbApplicantList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgApplicants As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhApplicantunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAppCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhApplicant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbRecruitmentTemplate As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLetterTemplate As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchLetterTemplate As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchApplicant As eZee.Common.eZeeSearchResetButton
    Friend WithEvents rtbLetterPreview As System.Windows.Forms.RichTextBox
    Friend WithEvents btnSendEmails As eZee.Common.eZeeLightButton
    Friend WithEvents txtEmailSubject As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblEmailSubject As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnApplicantReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents bgwSendMail As System.ComponentModel.BackgroundWorker
    Friend WithEvents objbtnSearchLetterGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLetterGroup As System.Windows.Forms.ComboBox
    Friend WithEvents LblLetterGroup As System.Windows.Forms.Label
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents dgEmailLogs As System.Windows.Forms.DataGridView
    Friend WithEvents gbDatesFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnResetEmailLogs As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmailLogs As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblFromDate As System.Windows.Forms.Label
    Friend WithEvents objwbContent As System.Windows.Forms.WebBrowser
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objpnlMain As System.Windows.Forms.Panel
    Friend WithEvents cboApplicantType As System.Windows.Forms.ComboBox
    Friend WithEvents LblApplicantType As System.Windows.Forms.Label
    Friend WithEvents tlsOperations As System.Windows.Forms.ToolStrip
    Friend WithEvents objtlbbtnCut As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnCopy As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnPaste As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtoolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objcboFont As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents objcboFontSize As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents objToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnColor As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents objToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnBold As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnItalic As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnUnderline As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnLeftAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnCenterAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRightAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnUndo As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRedo As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnBulletSimple As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnLeftIndent As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRightIndent As System.Windows.Forms.ToolStripButton
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents pnlColor As System.Windows.Forms.Panel
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents btnMoreColor As System.Windows.Forms.Button
    Friend WithEvents objbtn47 As System.Windows.Forms.Button
    Friend WithEvents objbtn34 As System.Windows.Forms.Button
    Friend WithEvents objbtn18 As System.Windows.Forms.Button
    Friend WithEvents objbtn46 As System.Windows.Forms.Button
    Friend WithEvents objbtn32 As System.Windows.Forms.Button
    Friend WithEvents objbtn33 As System.Windows.Forms.Button
    Friend WithEvents objbtn45 As System.Windows.Forms.Button
    Friend WithEvents objbtn19 As System.Windows.Forms.Button
    Friend WithEvents objbtn2 As System.Windows.Forms.Button
    Friend WithEvents objbtn44 As System.Windows.Forms.Button
    Friend WithEvents objbtn31 As System.Windows.Forms.Button
    Friend WithEvents objbtn42 As System.Windows.Forms.Button
    Friend WithEvents objbtn20 As System.Windows.Forms.Button
    Friend WithEvents objbtn43 As System.Windows.Forms.Button
    Friend WithEvents objbtn3 As System.Windows.Forms.Button
    Friend WithEvents objbtn15 As System.Windows.Forms.Button
    Friend WithEvents objbtn26 As System.Windows.Forms.Button
    Friend WithEvents objbtn16 As System.Windows.Forms.Button
    Friend WithEvents objbtn35 As System.Windows.Forms.Button
    Friend WithEvents objbtn41 As System.Windows.Forms.Button
    Friend WithEvents objbtn21 As System.Windows.Forms.Button
    Friend WithEvents objbtn8 As System.Windows.Forms.Button
    Friend WithEvents objbtn4 As System.Windows.Forms.Button
    Friend WithEvents objbtn23 As System.Windows.Forms.Button
    Friend WithEvents objbtn27 As System.Windows.Forms.Button
    Friend WithEvents objbtn1 As System.Windows.Forms.Button
    Friend WithEvents objbtn36 As System.Windows.Forms.Button
    Friend WithEvents objbtn7 As System.Windows.Forms.Button
    Friend WithEvents objbtn22 As System.Windows.Forms.Button
    Friend WithEvents objbtn24 As System.Windows.Forms.Button
    Friend WithEvents objbtn5 As System.Windows.Forms.Button
    Friend WithEvents objbtn12 As System.Windows.Forms.Button
    Friend WithEvents objbtn28 As System.Windows.Forms.Button
    Friend WithEvents objbtn17 As System.Windows.Forms.Button
    Friend WithEvents objbtn37 As System.Windows.Forms.Button
    Friend WithEvents objbtn40 As System.Windows.Forms.Button
    Friend WithEvents objbtn9 As System.Windows.Forms.Button
    Friend WithEvents objbtn25 As System.Windows.Forms.Button
    Friend WithEvents objbtn6 As System.Windows.Forms.Button
    Friend WithEvents objbtn13 As System.Windows.Forms.Button
    Friend WithEvents objbtn29 As System.Windows.Forms.Button
    Friend WithEvents objbtn11 As System.Windows.Forms.Button
    Friend WithEvents objbtn38 As System.Windows.Forms.Button
    Friend WithEvents objbtn39 As System.Windows.Forms.Button
    Friend WithEvents objbtn10 As System.Windows.Forms.Button
    Friend WithEvents objbtn30 As System.Windows.Forms.Button
    Friend WithEvents objbtn14 As System.Windows.Forms.Button
    Friend WithEvents objbtn48 As System.Windows.Forms.Button
    Friend WithEvents dgcolhELApplicant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVacancy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSubject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSendDateTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBody As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents txtSearchApplicant As eZee.TextBox.AlphanumericTextBox
    Private WithEvents txtELSearch As eZee.TextBox.AlphanumericTextBox
End Class
