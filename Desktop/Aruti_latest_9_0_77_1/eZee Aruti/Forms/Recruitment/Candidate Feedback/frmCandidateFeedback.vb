﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Collections.Specialized

Public Class frmCandidateFeedback

    <DllImport("gdiplus.dll")> _
Private Shared Function GdipEmfToWmfBits(ByVal _hEmf As IntPtr, ByVal _bufferSize As UInteger, ByVal _buffer() As Byte, ByVal _mappingMode As Integer, ByVal _flags As EmfToWmfBitsFlags) As UInteger
    End Function

    Private Enum EmfToWmfBitsFlags
        EmfToWmfBitsFlagsDefault = &H0
        EmfToWmfBitsFlagsEmbedEmf = &H1
        EmfToWmfBitsFlagsIncludePlaceable = &H2
        EmfToWmfBitsFlagsNoXORClip = &H4
    End Enum

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmCandidateFeedback"
    Private dvApplicant As DataView
    Private mstrReceipentId As String
    Private blnIsMailSending As Boolean = False
    Dim drRow As DataRow() = Nothing

    'Pinkal (14-Apr-2023) -- Start
    'NMB Enhancements - Adding Formating and Letter Template to be Editable .
    Private rtbTemp As New RichTextBox
    Public Shared ReadOnly FontSizes As List(Of Single) = New List(Of Single)()
    'Pinkal (14-Apr-2023) -- End

    'Pinkal (15-May-2023) -- Start
    '(A1X-903) NMB - Candidate Feedback Screen: Filters for search by candidate email ID and vacancy name on the AT log window.
    Private dvEmailLogList As DataView
    'Pinkal (15-May-2023) -- End

#End Region

#Region " Display Dialog "


#End Region

#Region " Form's Events "

    Private Sub frmCandidateFeedback_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Pinkal (14-Apr-2023) -- Start
            'NMB Enhancements - Adding Formating and Letter Template to be Editable .
            If FontSizes.Count <= 0 Then
                InitializeFontSizes()
            End If
            'Pinkal (14-Apr-2023) -- End

            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCandidateFeedback_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCandidateFeedback_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            mblnIsSendDisposed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCandidateFeedback_FormClosing", mstrModuleName)
        End Try
    End Sub

    'Pinkal (14-Apr-2023) -- Start
    '(A1X-820) NMB - As a user, i want to import employees with same email ID while importing employee on employee master.

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (14-Apr-2023) -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As DataSet = Nothing
        Dim objVacancy As New clsVacancy
        Dim objCommon As New clsCommon_Master
        Try

            dsCombo = objVacancy.getVacancyType(True)
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

            dsCombo = Nothing
            dsCombo = objVacancy.getVacancyType(False)
            With cboApplicantType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

            cboViewType.Items.Clear()
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 2, "Non ShortListed Applicant"))
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 3, "ShortListed Applicant"))
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 4, "Final ShortListed Applicant"))
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 5, "Non Interviewed Applicant"))
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 6, "Interviewed Applicant"))
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 7, "Non Eligible Applicant"))
            cboViewType.Items.Add(Language.getMessage(mstrModuleName, 8, "Eligible Applicant"))
            cboViewType.SelectedIndex = 0

            dsCombo = Nothing
            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.LETTER_TYPE, True, "Letter")
            With cboLetterGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing

            'Pinkal (14-Apr-2023) -- Start
            'NMB Enhancements - Adding Formating and Letter Template to be Editable .
            objcboFont.Items.Clear()
            For Each obj As FontFamily In FontFamily.Families()
                objcboFont.Items.Add(obj.Name)
            Next

            objcboFontSize.Items.Clear()
            For i As Integer = 0 To FontSizes.Count - 1
                objcboFontSize.Items.Add(FontSizes.Item(i).ToString)
            Next
            RemoveHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
            objcboFont.SelectedIndex = objcboFont.FindStringExact("Arial")
            objcboFontSize.SelectedIndex = objcboFontSize.FindStringExact("8")
            AddHandler objcboFont.SelectedIndexChanged, AddressOf cboFont_SelectedIndexChanged
            'Pinkal (14-Apr-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCommon = Nothing
            objVacancy = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dsApplicantList As DataSet = Nothing
        Try


            If CInt(cboVacancy.SelectedValue) > 0 Then
                strSearching &= "AND ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) = " & CInt(cboVacancy.SelectedValue) & " "
            End If

            Select Case cboViewType.SelectedIndex

                Case 1 '  Non ShortListed Applicant

                    Select Case CInt(cboVacancyType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            strSearching &= " AND (rcvacancy_master.isexternalvacancy = 1 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                        Case enVacancyType.INTERNAL_VACANCY
                            strSearching &= " AND (rcvacancy_master.isexternalvacancy = 0 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            strSearching &= " AND ISNULL(rcvacancy_master.isbothintext, 0) = 1 "
                    End Select


                    Select Case CInt(cboApplicantType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                        Case enVacancyType.INTERNAL_VACANCY
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                    End Select


                    Dim objNonShortListApplicant As New clsshortlist_finalapplicant
                    dsApplicantList = objNonShortListApplicant.GetShortListedOnlyApplicantList(True, CInt(cboVacancy.SelectedValue), strSearching)
                    objNonShortListApplicant = Nothing

                    dgColhAppCode.DataPropertyName = "applicant_code"
                    dgColhApplicant.DataPropertyName = "applicantname"


                Case 2 ' ShortListed Applicant

                    Select Case CInt(cboVacancyType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            strSearching &= " AND (rcvacancy_master.isexternalvacancy = 1 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                        Case enVacancyType.INTERNAL_VACANCY
                            strSearching &= " AND (rcvacancy_master.isexternalvacancy = 0 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            strSearching &= " AND ISNULL(rcvacancy_master.isbothintext, 0) = 1 "
                    End Select


                    Select Case CInt(cboApplicantType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                        Case enVacancyType.INTERNAL_VACANCY
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                    End Select

                    Dim objShortListApplicant As New clsshortlist_finalapplicant
                    dsApplicantList = objShortListApplicant.GetShortListedOnlyApplicantList(False, CInt(cboVacancy.SelectedValue), strSearching)
                    objShortListApplicant = Nothing

                    dgColhAppCode.DataPropertyName = "applicant_code"
                    dgColhApplicant.DataPropertyName = "applicantname"


                Case 3 ' Final ShortListed Applicant

                    Select Case CInt(cboVacancyType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            strSearching &= " AND (rcvacancy_master.isexternalvacancy = 1 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                        Case enVacancyType.INTERNAL_VACANCY
                            strSearching &= " AND (rcvacancy_master.isexternalvacancy = 0 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            strSearching &= " AND ISNULL(rcvacancy_master.isbothintext, 0) = 1 "
                    End Select

                    Select Case CInt(cboApplicantType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                        Case enVacancyType.INTERNAL_VACANCY
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                    End Select

                    Dim objShortListApplicant As New clsshortlist_finalapplicant
                    dsApplicantList = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), True, strSearching)
                    objShortListApplicant = Nothing

                    dgColhAppCode.DataPropertyName = "applicant_code"
                    dgColhApplicant.DataPropertyName = "applicantname"

                Case 4 ' Non Interviewed Applicant

                    Dim objInterviewed As New clsInterviewAnalysis_master
                    dsApplicantList = objInterviewed.GetNonInterviewedApplicantList(CInt(cboVacancyType.SelectedValue), CInt(cboVacancy.SelectedValue))
                    objInterviewed = Nothing

                    dgColhAppCode.DataPropertyName = "applicant_code"
                    dgColhApplicant.DataPropertyName = "applicantname"

                Case 5 ' Interviewed Applicant

                    Dim objInterviewed As New clsInterviewAnalysis_master
                    dsApplicantList = objInterviewed.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedValue), enApplicant_Eligibility.AE_NOTELIGIBLE, CInt(cboVacancy.SelectedValue), enShortListing_Status.SC_NONE, False)
                    objInterviewed = Nothing

                    dgColhAppCode.DataPropertyName = "applicant_code"
                    dgColhApplicant.DataPropertyName = "applicantname"

                Case 6 ' Non Eligible Applicant

                    Dim objNonEligible As New clsInterviewAnalysis_master
                    dsApplicantList = objNonEligible.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedValue), enApplicant_Eligibility.AE_NOTELIGIBLE, CInt(cboVacancy.SelectedValue), enShortListing_Status.SC_NONE, True)
                    objNonEligible = Nothing

                    dgColhAppCode.DataPropertyName = "applicant_code"
                    dgColhApplicant.DataPropertyName = "applicantname"

                Case 7 ' Eligible Applicant

                    Dim objEligible As New clsInterviewAnalysis_master
                    dsApplicantList = objEligible.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedValue), enApplicant_Eligibility.AE_ELIGIBLE_APPROVED, CInt(cboVacancy.SelectedValue), enShortListing_Status.SC_NONE, True)
                    objEligible = Nothing

                    dgColhAppCode.DataPropertyName = "applicant_code"
                    dgColhApplicant.DataPropertyName = "applicantname"

            End Select

            If dsApplicantList IsNot Nothing AndAlso dsApplicantList.Tables(0).Columns.Contains("isChecked") = False Then
                Dim dcColumn As New DataColumn("isChecked")
                dcColumn.DataType = Type.GetType("System.Boolean")
                dcColumn.DefaultValue = False
                dsApplicantList.Tables(0).Columns.Add(dcColumn)
            End If

            dgApplicants.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "isChecked"

            If dsApplicantList IsNot Nothing Then
                dvApplicant = dsApplicantList.Tables(0).DefaultView
                dgApplicants.DataSource = dvApplicant
            Else
                dgApplicants.DataSource = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmailLogList()
        Dim objMail As New clsSendMail
        Dim dTable As DataTable = Nothing
        Dim mstrSearch As String = ""
        Try

            If CInt(cboVacancyType.SelectedValue) > 0 Then
                mstrSearch &= "AND ISNULL(atrecruit_email_ntf_tran.vacancytypeunkid, 0) = " & CInt(cboVacancyType.SelectedValue) & " "
            End If

            If CInt(cboVacancy.SelectedValue) > 0 Then
                mstrSearch &= "AND ISNULL(atrecruit_email_ntf_tran.vacancyunkid, 0) = " & CInt(cboVacancy.SelectedValue) & " "
            End If

            If CInt(cboViewType.SelectedIndex) > 0 Then
                mstrSearch &= "AND ISNULL(atrecruit_email_ntf_tran.viewtypeunkid, 0) = " & CInt(cboViewType.SelectedIndex) & " "
            End If

            Select Case CInt(cboApplicantType.SelectedValue)
                Case enVacancyType.EXTERNAL_VACANCY
                    mstrSearch &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                Case enVacancyType.INTERNAL_VACANCY
                    mstrSearch &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
            End Select

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If

            dTable = objMail.Generate_Log_List(clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, dtpFromDate.Value.Date, dtpToDate.Value.Date, 0, 0, Me.Text, "", "", "", mstrSearch)

            dgEmailLogs.AutoGenerateColumns = False

            dgcolhEmail.DataPropertyName = "Recipient"
            dgcolhVacancy.DataPropertyName = "vacancy"
            dgcolhSubject.DataPropertyName = "iSub"
            objdgcolhBody.DataPropertyName = "iCon"
            dgcolhSendDateTime.DataPropertyName = "Send Date"

            'Pinkal (15-May-2023) -- Start
            '(A1X-903) NMB - Candidate Feedback Screen: Filters for search by candidate email ID and vacancy name on the AT log window.
            dgcolhELApplicant.DataPropertyName = "Applicant"

            If dTable IsNot Nothing Then
                dvEmailLogList = dTable.DefaultView
                dgEmailLogs.DataSource = dvEmailLogList
            Else
                dgEmailLogs.DataSource = Nothing
            End If
            'Pinkal (15-May-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmailLogList", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Vacancy Type is compulsory information.Please select Vancancy Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboVacancyType.Select()
                mblnFlag = False
            ElseIf CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Vacancy is compulsory information.Please select Vancancy."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboVacancy.Select()
                mblnFlag = False
            ElseIf CInt(cboViewType.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "View Type is compulsory information.Please select View Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboViewType.Select()
                mblnFlag = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function

    Private Sub CheckAllApplicant(ByVal blnCheckAll As Boolean)
        Try
            If dvApplicant IsNot Nothing Then
                For Each dr As DataRowView In dvApplicant
                    dr("isChecked") = blnCheckAll
                Next
                dvApplicant.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllApplicant", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxApplicant()
        Try
            Dim dRow As DataRow() = dvApplicant.ToTable.Select("isChecked = true")

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            If dRow.Length <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf dRow.Length < dvApplicant.Table.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf dRow.Length = dvApplicant.Table.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If
            dvApplicant.Table.AcceptChanges()
            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxApplicant", mstrModuleName)
        End Try
    End Sub

    Private Sub SendEmails()
        Dim strTextData As String
        Dim strFailGusetName As String = ""
        Dim strMessage As String = ""
        Dim blnSend As Boolean = False
        Dim mblnResult As Boolean = False
        Dim objEmailData As New clsEmail_Master
        Dim strEmail As String = ""
        Try
            rtbLetterPreview.Enabled = False
            Control.CheckForIllegalCrossThreadCalls = False

            Dim objSendMail As New clsSendMail
            strTextData = rtbLetterPreview.Rtf

            blnIsMailSending = True

            Dim mstrEmail As String = ""
            Dim mstrRefViewMergeData As String = ""

            For i As Integer = 0 To drRow.Count - 1

                mstrRefViewMergeData = ""
                strEmail = drRow(i)("email").ToString.Trim().Replace("'", "''")

                If strEmail = "" Then Continue For

                If mstrEmail.Trim() = strEmail.Trim Then
                    Continue For
                End If

                ViewMergeData(i, strTextData, mstrRefViewMergeData)

                objSendMail._ToEmail = strEmail
                objSendMail._Subject = txtEmailSubject.Text

                Dim htmlOutput = "Document.html"
                Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                Dim htmlResult = RtfToHtmlConverter.RtfToHtml(mstrRefViewMergeData, contentUriPrefix)
                objSendMail._ImageContents = htmlResult._Content
                htmlResult.WriteToFile(htmlOutput)
                Dim oHtml As String = htmlResult._HTML.Replace("  ", "&nbsp;&nbsp;").Replace(vbTab, "&#9;")
                objSendMail._Message = oHtml

                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                objSendMail._AttachedFiles = ""
                objSendMail._UserUnkid = User._Object._Userunkid
                objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                objSendMail._LogEmployeeUnkid = 0
                objSendMail._VacancyTypeId = CInt(cboVacancyType.SelectedValue)
                objSendMail._VacancyId = CInt(cboVacancy.SelectedValue)
                objSendMail._VacancyViewId = CInt(cboViewType.SelectedIndex)

                strMessage = objSendMail.SendMail(Company._Object._Companyunkid)

                If strMessage <> "" Then
                    strFailGusetName &= ", " & drRow(i)("email").ToString.Trim().Replace("'", "''")
                Else
                    blnSend = True
                End If

                'If mstrReceipentId = "" Then
                '    mstrReceipentId = CStr(drRow(i).Item("applicantunkid"))
                'Else
                '    mstrReceipentId &= " , " & CStr(drRow(i).Item("applicantunkid"))
                'End If
                'rtbLetterPreview.Rtf = strTextData
                mstrEmail = strEmail
                bgwSendMail.ReportProgress(i)
            Next

            'objEmailData._dicDisciplineFiles = Nothing
            'objEmailData._EmailDateTime = ConfigParameter._Object._CurrentDateAndTime
            'objEmailData._Istype_of = CChar("O")
            'objEmailData._LettertypeUnkId = CInt(cboLetterTemplate.SelectedValue)
            'If CInt(cboLetterTemplate.SelectedValue) > 0 Then
            '    Dim objLetterType As New clsLetterType
            '    objLetterType._LettertypeUnkId = CInt(cboLetterTemplate.SelectedValue)
            '    objEmailData._Message = objLetterType._Lettercontent
            '    objLetterType = Nothing
            'Else
            '    objEmailData._Message = rtbLetterPreview.Rtf
            'End If
            'objEmailData._Subject = txtEmailSubject.Text
            'objEmailData._UserUnkId = User._Object._Userunkid
            'objEmailData._Isapplicant = True

            'If mstrReceipentId.Length > 0 Then
            '    mblnResult = objEmailData.Insert(mstrReceipentId)
            'End If


            If blnSend And strFailGusetName.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Mail(s) sent successfully!"), enMsgBoxStyle.Information)
            ElseIf blnSend And strFailGusetName.Length > 2 Then
                strFailGusetName = strFailGusetName.Substring(2)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Failure sending some mails!") & vbCrLf & """" & strFailGusetName & """", enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Failure sending mails!"), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            If ex.Message.Contains("Fail") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 14, "Please check your Internet settings."), enMsgBoxStyle.Information)
            ElseIf ex.Message.Contains("secure") Then
                eZeeMsgBox.Show(ex.Message & " " & Language.getMessage(mstrModuleName, 15, "Please Uncheck Login using SSL setting."), enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "SendEmails", mstrModuleName)
            End If
        Finally
            blnIsMailSending = False
            objEmailData = Nothing
        End Try
    End Sub

    Private Function embedImage(ByVal img As Image) As String
        Dim rtf As New StringBuilder()
        Try
            rtf.Append("{\rtf1\ansi\ansicpg1252\deff0\deflang1033")
            rtf.Append(GetFontTable(Me.Font))
            rtf.Append(GetImagePrefix(img))
            rtf.Append(getRtfImage(img))
            rtf.Append("}")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "embedImage", mstrModuleName)
        Finally
        End Try
        Return rtf.ToString()
    End Function

    Private Function GetFontTable(ByVal font As Font) As String
        Dim fontTable = New StringBuilder()
        ' Append table control string
        fontTable.Append("{\fonttbl{\f0")
        fontTable.Append("\")
        Dim rtfFontFamily = New HybridDictionary()
        rtfFontFamily.Add(FontFamily.GenericMonospace.Name, "\fmodern")
        rtfFontFamily.Add(FontFamily.GenericSansSerif, "\fswiss")
        rtfFontFamily.Add(FontFamily.GenericSerif, "\froman")
        rtfFontFamily.Add("UNKNOWN", "\fnil")

        ' If the font's family corresponds to an RTF family, append the
        ' RTF family name, else, append the RTF for unknown font family.
        fontTable.Append(If(rtfFontFamily.Contains(font.FontFamily.Name), rtfFontFamily(font.FontFamily.Name), rtfFontFamily("UNKNOWN")))
        ' \fcharset specifies the character set of a font in the font table.
        ' 0 is for ANSI.
        fontTable.Append("\fcharset0 ")
        ' Append the name of the font
        fontTable.Append(font.Name)
        ' Close control string
        fontTable.Append(";}}")
        Return fontTable.ToString()
    End Function

    Private Function GetImagePrefix(ByVal _image As Image) As String
        Dim xDpi, yDpi As Single
        Dim rtf = New StringBuilder()
        Using graphics As Graphics = CreateGraphics()
            xDpi = graphics.DpiX
            yDpi = graphics.DpiY
        End Using
        ' Calculate the current width of the image in (0.01)mm
        Dim picw = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 2540)))
        ' Calculate the current height of the image in (0.01)mm
        Dim pich = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 2540)))
        ' Calculate the target width of the image in twips
        Dim picwgoal = CInt(Math.Truncate(Math.Round((_image.Width / xDpi) * 1440)))
        ' Calculate the target height of the image in twips
        Dim pichgoal = CInt(Math.Truncate(Math.Round((_image.Height / yDpi) * 1440)))
        ' Append values to RTF string
        rtf.Append("{\pict\wmetafile8")
        rtf.Append("\picw")
        rtf.Append(picw)
        rtf.Append("\pich")
        rtf.Append(pich)
        rtf.Append("\picwgoal")
        rtf.Append(picwgoal)
        rtf.Append("\pichgoal")
        rtf.Append(pichgoal)
        rtf.Append(" ")

        Return rtf.ToString()
    End Function

    Private Function getRtfImage(ByVal image As Image) As String
        ' Used to store the enhanced metafile
        Dim stream As MemoryStream = Nothing
        ' Used to create the metafile and draw the image
        Dim graphics As Graphics = Nothing
        ' The enhanced metafile
        Dim metaFile As Imaging.Metafile = Nothing
        Try
            Dim rtf = New StringBuilder()
            stream = New MemoryStream()
            ' Get a graphics context from the RichTextBox
            graphics = CreateGraphics()
            Using graphics
                ' Get the device context from the graphics context
                Dim hdc As IntPtr = graphics.GetHdc()
                ' Create a new Enhanced Metafile from the device context
                metaFile = New Imaging.Metafile(stream, hdc)
                ' Release the device context
                graphics.ReleaseHdc(hdc)
            End Using

            ' Get a graphics context from the Enhanced Metafile
            graphics = graphics.FromImage(metaFile)
            Using graphics
                ' Draw the image on the Enhanced Metafile
                graphics.DrawImage(image, New Rectangle(0, 0, image.Width, image.Height))
            End Using

            ' Get the handle of the Enhanced Metafile
            Dim hEmf As IntPtr = metaFile.GetHenhmetafile()
            ' A call to EmfToWmfBits with a null buffer return the size of the
            ' buffer need to store the WMF bits.  Use this to get the buffer
            ' size.
            Dim bufferSize As UInteger = GdipEmfToWmfBits(hEmf, 0, Nothing, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Create an array to hold the bits
            Dim buffer = New Byte(CInt(bufferSize - 1)) {}
            ' A call to EmfToWmfBits with a valid buffer copies the bits into the
            ' buffer an returns the number of bits in the WMF.  
            Dim _convertedSize As UInteger = GdipEmfToWmfBits(hEmf, bufferSize, buffer, 8, EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)
            ' Append the bits to the RTF string
            For Each t As Byte In buffer
                rtf.Append(String.Format("{0:X2}", t))
            Next t
            Return rtf.ToString()
        Finally
            If graphics IsNot Nothing Then
                graphics.Dispose()
            End If
            If metaFile IsNot Nothing Then
                metaFile.Dispose()
            End If
            If stream IsNot Nothing Then
                stream.Close()
            End If
        End Try
    End Function

    Private Sub ViewMergeData(ByVal intNumber As Integer, ByVal strDataName As String, ByRef mstrRefViewMergeData As String)
        Try
            GC.Collect()
            For i As Integer = intNumber To intNumber
                Dim StrCol As String = ""
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    For j As Integer = 0 To drRow(intNumber).Table.Columns.Count - 1
                        GC.Collect()
                        StrCol = drRow(intNumber).Table.Columns(j).ColumnName
                        If strDataName.ToLower.Contains("#" & StrCol.ToLower & "#") Then
                            rtbLetterPreview.Focus()

                            If StrCol.Trim.ToUpper = "PASSWORD" Then
                                If blnIsMailSending = False Then
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(strDataName, "#" & StrCol & "#", drRow(intNumber).Item(StrCol).ToString, , , CompareMethod.Text)
                                ElseIf blnIsMailSending = True Then
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(strDataName, "#" & StrCol & "#", drRow(intNumber).Item("Original_Password").ToString, , , CompareMethod.Text)
                                End If

                            ElseIf StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                If IsDBNull(drRow(intNumber).Item(StrCol)) = False Then
                                    Dim data As Byte() = CType(drRow(intNumber).Item(StrCol), Byte())
                                    Dim ms As MemoryStream = New MemoryStream(data)
                                    Dim img As Image = Image.FromStream(ms)
                                    Dim strValue As String = embedImage(img)
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(strDataName, "#" & StrCol & "#", strValue, , , CompareMethod.Text)
                                Else
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(strDataName, "#" & StrCol & "#", "", , , CompareMethod.Text)
                                End If

                            ElseIf StrCol.Trim.ToUpper = "KEY DUTIES AND RESPONSIBILITIES" Then
                                strDataName = Microsoft.VisualBasic.Strings.Replace(strDataName, "#" & StrCol & "#", drRow(intNumber).Item(StrCol).ToString.Replace("\r\n", "\line"), , , CompareMethod.Text)
                            Else
                                If TypeOf drRow(intNumber).Item(StrCol) Is Decimal Then
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(strDataName, "#" & StrCol & "#", Format(drRow(intNumber).Item(StrCol), GUI.fmtCurrency), , , CompareMethod.Text)
                                Else
                                    strDataName = Microsoft.VisualBasic.Strings.Replace(strDataName, "#" & StrCol & "#", drRow(intNumber).Item(StrCol).ToString, , , CompareMethod.Text)
                                End If
                            End If
                        End If
                    Next
                End If

                Dim arr As MatchCollection = Regex.Matches(strDataName, "<calc[^>]*>(?<TEXT>[^<]*)<\/calc>", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim dt As New DataTable
                    Dim ans As Object = Nothing
                    Try
                        ans = dt.Compute(s.Groups("TEXT").ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(strDataName, "<calc>" & s.Groups("TEXT").ToString & "</calc>", Format(ans, "###,###,###,##0.00"), , , CompareMethod.Text)
                Next

                'arr = Nothing
                'arr = Regex.Matches(rtbLetterPreview.Rtf, "<hcode[^>]*>(?<TEXT>[^<]*)<\/hcode>", RegexOptions.IgnoreCase)
                'If arr IsNot Nothing AndAlso arr.Count > 0 Then
                '    Dim objPR As New clsPayrollProcessTran
                '    Dim objMD As New clsMasterData
                '    Dim objPD As New clscommom_period_Tran
                '    Dim objCS As New clsComputeScore_master
                '    Dim intPeriodId As Integer = -1
                '    For Each s As RegularExpressions.Match In arr
                '        intPeriodId = objMD.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, False, False)
                '        '*********************************** PAYROLL AMOUNT START
                '        Dim amt As Decimal = 0
                '        If intPeriodId > 0 Then
                '            objPD._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                '            Dim dsAmt As New DataSet
                '            dsAmt = objPR.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPD._Start_Date, objPD._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(dtRow(0)("EmpId")), intPeriodId, 0, False, "prtranhead_master.trnheadcode = '" & s.Groups("TEXT").ToString() & "' ", "", False, "")
                '            If dsAmt IsNot Nothing AndAlso dsAmt.Tables.Count > 0 Then
                '                If dsAmt.Tables(0).Rows.Count > 0 Then
                '                    amt = CDec(dsAmt.Tables(0).Rows(0)("amount"))
                '                End If
                '            End If
                '        End If
                '        Dim sreplace As String = Microsoft.VisualBasic.Strings.Replace(rtbLetterPreview.Rtf, "<hcode>" & s.Groups("TEXT").ToString & "</hcode>", Format(amt, "###,###,###,##0.00"), , , CompareMethod.Text)
                '        rtbLetterPreview.Rtf = sreplace
                '        '*********************************** PAYROLL AMOUNT END

                '        '*********************************** PERFORMANCE RATING START
                '        intPeriodId = -1
                '        Dim strRating As String = ""
                '        intPeriodId = objMD.getFirstPeriodID(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, 1, True, False)
                '        If intPeriodId > 0 Then
                '            strRating = objCS.GetRating(CInt(dtRow(0)("EmpId")), intPeriodId, ConfigParameter._Object._IsCalibrationSettingActive)
                '        End If
                '        sreplace = Microsoft.VisualBasic.Strings.Replace(rtbLetterPreview.Rtf, "<rating>", strRating, , , CompareMethod.Text)
                '        rtbLetterPreview.Rtf = sreplace
                '        '*********************************** PERFORMANCE RATING END
                '    Next
                '    objPR = Nothing
                '    objMD = Nothing
                '    objPD = Nothing
                '    objCS = Nothing
                'End If
            Next
            mstrRefViewMergeData = strDataName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub ApplyStyle(ByVal mStyle As FontStyle, ByVal mAddStyle As Boolean)
        Try
            Dim intStart As Integer = rtbLetterPreview.SelectionStart
            Dim intEnd As Integer = rtbLetterPreview.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And rtbLetterPreview.SelectionFont IsNot Nothing Then
                If mAddStyle Then
                    rtbLetterPreview.SelectionFont = New Font(rtbLetterPreview.SelectionFont, rtbLetterPreview.SelectionFont.Style Or mStyle)
                Else
                    rtbLetterPreview.SelectionFont = New Font(rtbLetterPreview.SelectionFont, rtbLetterPreview.SelectionFont.Style And (Not mStyle))
                End If
                Return
            End If

            rtbTemp.Rtf = rtbLetterPreview.SelectedRtf

            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                If mAddStyle Then
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style Or mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont, rtbTemp.SelectionFont.Style And (Not mStyle))
                End If
            Next

            rtbTemp.Select(rtbTempStart, intEnd)
            rtbLetterPreview.SelectedRtf = rtbTemp.SelectedRtf
            rtbLetterPreview.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "ApplyStyle", mstrModuleName)
        End Try
    End Sub

    Private Sub ChangeFont(ByVal mFontName As String)
        Try
            Dim intStart As Integer = rtbLetterPreview.SelectionStart
            Dim intEnd As Integer = rtbLetterPreview.SelectionLength
            Dim rtbTempStart As Integer = 0
            Dim mStyle As New FontStyle
            mStyle = GetDefaultStyle(mFontName)

            If intEnd <= 1 And rtbLetterPreview.SelectionFont IsNot Nothing Then
                rtbLetterPreview.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbLetterPreview.SelectionFont.Size <> Nothing, rtbLetterPreview.SelectionFont.Size, 8)), mStyle)
                Return
            End If

            rtbTemp.Rtf = rtbLetterPreview.SelectedRtf
            For i As Integer = 0 To intEnd - 1
                rtbTemp.Select(rtbTempStart + i, 1)
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(mFontName, CInt(objcboFontSize.Text), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(CStr(mFontName), CInt(IIf(rtbTemp.SelectionFont.Size <> Nothing, rtbTemp.SelectionFont.Size, 8)), mStyle)
                End If
            Next
            rtbTemp.Select(rtbTempStart, intEnd)
            rtbLetterPreview.SelectedRtf = rtbTemp.SelectedRtf
            rtbLetterPreview.Select(intStart, intEnd)
            Return
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "ChangeFont", mstrModuleName)
        End Try
    End Sub

    Private Sub ChangeFontSize(ByVal mFSize As Double)
        Try
            If mFSize <= 0 Then
                Exit Sub
            End If

            Dim intStart As Integer = rtbLetterPreview.SelectionStart
            Dim intEnd As Integer = rtbLetterPreview.SelectionLength
            Dim rtbTempStart As Integer = 0

            If intEnd <= 1 And rtbLetterPreview.SelectionFont IsNot Nothing Then
                rtbLetterPreview.SelectionFont = New Font(rtbLetterPreview.SelectionFont.FontFamily, CInt(mFSize), rtbLetterPreview.SelectionFont.Style)
                Return
            End If

            rtbTemp.Rtf = rtbLetterPreview.SelectedRtf
            For i As Integer = 0 To intEnd
                rtbTemp.Select(rtbTempStart + i, 1)
                Dim mStyle As New FontStyle
                If rtbLetterPreview.SelectionLength > 0 Then
                    mStyle = GetDefaultStyle(rtbTemp.SelectionFont.FontFamily.Name)
                Else
                    mStyle = GetDefaultStyle(objcboFont.Text)
                End If
                If IsNothing(rtbTemp.SelectionFont) Then
                    rtbTemp.SelectionFont = New Font(objcboFont.Text, CInt(mFSize), mStyle)
                Else
                    rtbTemp.SelectionFont = New Font(rtbTemp.SelectionFont.FontFamily, CInt(mFSize), mStyle)
                End If
            Next

            rtbTemp.Select(rtbTempStart, intEnd)
            rtbLetterPreview.SelectedRtf = rtbTemp.SelectedRtf
            rtbLetterPreview.Select(intStart, intEnd)

            Return
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "ChangeFontSize", mstrModuleName)
        End Try
    End Sub

    Private Function GetDefaultStyle(ByVal mFontName As String) As FontStyle
        For Each fntName As FontFamily In FontFamily.Families
            If fntName.Name.Equals(mFontName) Then
                If fntName.IsStyleAvailable(FontStyle.Regular) Then
                    Return FontStyle.Regular
                ElseIf fntName.IsStyleAvailable(FontStyle.Bold) Then
                    Return FontStyle.Bold
                ElseIf fntName.IsStyleAvailable(FontStyle.Italic) Then
                    Return FontStyle.Italic
                ElseIf fntName.IsStyleAvailable(FontStyle.Underline) Then
                    Return FontStyle.Underline
                End If
            End If
        Next
    End Function

    Private Sub RegainOldStyle()
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RegainOldStyle", mstrModuleName)
        End Try
    End Sub


    'Pinkal (14-Apr-2023) -- Start
    'NMB Enhancements - Adding Formating and Letter Template to be Editable .
    Private Sub InitializeFontSizes()
        FontSizes.AddRange(New Single() {8, 9, 10, 10.5F, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72})
    End Sub
    'Pinkal (14-Apr-2023) -- End


#End Region

#Region "Dropdown Events"

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim dsVacList As New DataSet
            Dim objVac As New clsVacancy
            dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsVacList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLetterGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLetterGroup.SelectedIndexChanged
        Try
            Dim objLetter As New clsLetterType
            Dim dsCombo As DataSet = objLetter.getComboList(True, CInt(cboLetterGroup.SelectedValue), Company._Object._Companyunkid)
            With cboLetterTemplate
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With
            dsCombo.Clear()
            dsCombo = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLetterGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLetterTemplate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLetterTemplate.SelectedIndexChanged
        Dim objLetterType As New clsLetterType
        Try
            txtSearchApplicant.Text = ""
            objLetterType._LettertypeUnkId = CInt(cboLetterTemplate.SelectedValue)
            rtbLetterPreview.Rtf = objLetterType._Lettercontent
            If cboLetterTemplate.SelectedIndex > 0 Then rtbLetterPreview.ReadOnly = False Else rtbLetterPreview.ReadOnly = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLetterTemplate_SelectedIndexChanged", mstrModuleName)
        Finally
            objLetterType = Nothing
        End Try
    End Sub

#End Region

#Region "Textbox Events"

    Private Sub txtSearchApplicant_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchApplicant.TextChanged
        Try
            If dvApplicant IsNot Nothing Then
                dvApplicant.RowFilter = dgColhAppCode.DataPropertyName & " LIKE '%" & txtSearchApplicant.Text.Replace("'", "''") & "%'  OR " & dgColhApplicant.DataPropertyName & " LIKE '%" & txtSearchApplicant.Text.Replace("'", "''") & "%'"
                dgApplicants.Refresh()
                SetCheckBoxApplicant()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchApplicant_TextChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (15-May-2023) -- Start
    '(A1X-903) NMB - Candidate Feedback Screen: Filters for search by candidate email ID and vacancy name on the AT log window.
    Private Sub txtELSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtELSearch.TextChanged
        Try
            If dvEmailLogList IsNot Nothing Then
                dvEmailLogList.RowFilter = dgcolhELApplicant.DataPropertyName & " LIKE '%" & txtELSearch.Text.Replace("'", "''") & "%'  OR " & dgcolhEmail.DataPropertyName & " LIKE '%" & txtELSearch.Text.Replace("'", "''") & "%' OR " & dgcolhSubject.DataPropertyName & " LIKE '%" & txtELSearch.Text.Replace("'", "''") & "%'"
                dgEmailLogs.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtELSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (15-May-2023) -- End

#End Region

#Region "Checkbox Events"

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            CheckAllApplicant(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (28-Apr-2023) -- Start
    '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid   loan,mortgage loan    
    'Private Sub objbtnSearchViewType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmCommonSearch
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If
    '        With frm
    '            .ValueMember = cboVacancyType.ValueMember
    '            .DisplayMember = cboVacancyType.DisplayMember
    '            .DataSource = CType(cboVacancyType.DataSource, DataTable)
    '        End With

    '        If frm.DisplayDialog Then
    '            cboVacancyType.SelectedValue = frm.SelectedValue
    '            cboVacancyType.Focus()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchViewType_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub
    'Pinkal (28-Apr-2023) -- End

    Private Sub objbtnSearchLetterGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLetterGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboLetterGroup.ValueMember
                .DisplayMember = cboLetterGroup.DisplayMember
                .DataSource = CType(cboLetterGroup.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboLetterGroup.SelectedValue = frm.SelectedValue
                cboLetterGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLetterGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLetterTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLetterTemplate.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboLetterTemplate.ValueMember
                .DisplayMember = cboLetterTemplate.DisplayMember
                .DataSource = CType(cboLetterTemplate.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboLetterTemplate.SelectedValue = frm.SelectedValue
                cboLetterTemplate.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLetterTemplate_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboVacancyType.SelectedIndex = 0
            cboApplicantType.SelectedIndex = 0
            cboVacancy.SelectedIndex = 0
            cboViewType.SelectedIndex = 0
            cboLetterGroup.SelectedIndex = 0
            cboLetterTemplate.SelectedIndex = 0
            txtSearchApplicant.Text = ""
            'Pinkal (14-Apr-2023) -- Start
            'NMB Enhancements - Adding Formating and Letter Template to be Editable .
            txtEmailSubject.Text = ""
            objchkSelectAll.Checked = False
            objcboFont.SelectedIndex = 0
            objcboFontSize.SelectedIndex = 0
            'objwbContent.Navigate("about:blank")
            'objwbContent.Document.OpenNew(False)
            'objwbContent.Refresh()
            'Pinkal (14-Apr-2023) -- End
            FillList()
            FillEmailLogList()
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            Call objbtnSearchApplicant.ShowResult(CStr(dgApplicants.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Try
            If Validation() = False Then Exit Sub
            FillList()
            Call objbtnSearchApplicant.ShowResult(CStr(dgApplicants.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnApplicantReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnApplicantReset.Click
        Try
            cboApplicantType.SelectedIndex = 0
            txtSearchApplicant.Text = ""
            objchkSelectAll.Checked = False
            FillList()
            Call objbtnSearchApplicant.ShowResult(CStr(dgApplicants.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnApplicantReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmailLogs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmailLogs.Click
        Try
            If Validation() = False Then Exit Sub
            objwbContent.Navigate("about:blank")
            objwbContent.Document.OpenNew(False)
            objwbContent.Refresh()
            FillEmailLogList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmailLogs_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnResetEmailLogs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnResetEmailLogs.Click
        Try
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (15-May-2023) -- Start
            '(A1X-903) NMB - Candidate Feedback Screen: Filters for search by candidate email ID and vacancy name on the AT log window.
            txtELSearch.Text = ""
            dvEmailLogList = Nothing
            'Pinkal (15-May-2023) -- End
            FillEmailLogList()
            objwbContent.Navigate("about:blank")
            objwbContent.Document.OpenNew(False)
            objwbContent.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnResetEmailLogs_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSendEmails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendEmails.Click
        Try
            If txtEmailSubject.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Email Subject is compulsory information.Please Enter Email Subject."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                txtEmailSubject.Focus()
                Exit Sub
            End If

            Dim mstrApplicationIds As String = String.Join(",", dvApplicant.ToTable().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isChecked") = True).Select(Function(x) x.Field(Of Integer)("applicantunkid").ToString()).ToArray())

            If mstrApplicationIds.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Select atleast one applicant to send email."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                Exit Sub
            End If

            Dim objLetterFields As New clsLetterFields
            objLetterFields._VacancyUnkid = CStr(cboVacancy.SelectedValue)
            Dim dsList As DataSet = objLetterFields.GetEmployeeData(mstrApplicationIds, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True)
            drRow = dtTable.Select("1=1", "EmpCode")

            pbProgress.Visible = True
            btnSendEmails.Enabled = False
            mblnIsSendingMail = True
            bgwSendMail.RunWorkerAsync()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSendEmails_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Events"

    Private Sub dgApplicants_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgApplicants.CellContentClick, dgApplicants.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                If dgApplicants.IsCurrentCellDirty Then
                    dgApplicants.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                SetCheckBoxApplicant()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgApplicants_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmailLogs_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgEmailLogs.SelectionChanged
        Try
            If dgEmailLogs.SelectedRows.Count > 0 Then
                objwbContent.Navigate("about:blank") : objwbContent.Document.OpenNew(False)
                objwbContent.Document.Write(dgEmailLogs.Rows(dgEmailLogs.SelectedRows(0).Index).Cells(objdgcolhBody.Name).Value.ToString())
                objwbContent.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmailLogs_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Background Worker Event(s) "

    Private Sub bgwSendMail_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwSendMail.DoWork
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            Me.Enabled = False
            Call SendEmails()
            Me.Enabled = True
            objbtnReset_Click(objbtnReset, New EventArgs())
            pbProgress.Value = 0
            pbProgress.Visible = False
            btnSendEmails.Enabled = True
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Catch ex As Exception
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Finally
            mblnIsSendingMail = False
        End Try
    End Sub

    Private Sub bgwSendMail_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwSendMail.ProgressChanged
        Application.DoEvents()
        Dim canvas As Graphics = Me.pbProgress.CreateGraphics
        If e.ProgressPercentage = 0 Then
            pbProgress.Value = CInt(((1 * 100) / drRow.Count))
        Else
            pbProgress.Value = CInt(((e.ProgressPercentage * 100) / drRow.Count))
        End If
        canvas.DrawString(pbProgress.Value.ToString & "%", New Font("Verdana", 9, FontStyle.Bold), New SolidBrush(Color.Black), 180, 5)
        canvas.Dispose()
    End Sub

    Private Sub bgwSendMail_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwSendMail.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            mblnIsSendingMail = False
            mblnIsSendDisposed = True
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Catch ex As IO.IOException
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgwSendMail_RunWorkerCompleted", mstrModuleName)
        Finally
            mblnIsSendingPayslip = False
            mblnIsSendingMail = False
        End Try
    End Sub

#End Region

    'Pinkal (14-Apr-2023) -- Start
    'NMB Enhancements - Adding Formating and Letter Template to be Editable .

#Region "ToolStrip Events "

#Region " Font And Size "

    Private Sub cboFontSize_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles objcboFontSize.KeyPress
        Try
            If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
                If (Asc(e.KeyChar) = Asc(GUI.DecimalSeparator) And InStr(Trim(objcboFontSize.Text), GUI.DecimalSeparator) = 0) Or Asc(e.KeyChar) = 8 Then Exit Sub
                e.KeyChar = CChar("")
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFontSize_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFontSize.TextChanged
        Try
            If objcboFontSize.Text = "" Or objcboFontSize.Text = "." Then
                Exit Sub
            Else
                Call ChangeFontSize(CDec(objcboFontSize.Text))
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFontSize_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFont_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objcboFont.SelectedIndexChanged
        Try
            Call ChangeFont(objcboFont.Text)
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboFont_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Alignments "
    Private Sub tlbbtnLeftAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftAlign.Click
        Try
            Dim strFormat As New StringFormat(StringFormatFlags.NoClip)
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = True
            If rtbLetterPreview.SelectionAlignment <> HorizontalAlignment.Left Then
                rtbLetterPreview.SelectionAlignment = HorizontalAlignment.Left
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnLeftAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnCenterAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCenterAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = True
            objtlbbtnRightAlign.Checked = False
            objtlbbtnLeftAlign.Checked = False
            If rtbLetterPreview.SelectionAlignment <> HorizontalAlignment.Center Then
                rtbLetterPreview.SelectionAlignment = HorizontalAlignment.Center
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnCenterAlign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tlbbtnRightAlign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightAlign.Click
        Try
            objtlbbtnCenterAlign.Checked = False
            objtlbbtnRightAlign.Checked = True
            objtlbbtnLeftAlign.Checked = False
            If rtbLetterPreview.SelectionAlignment <> HorizontalAlignment.Right Then
                rtbLetterPreview.SelectionAlignment = HorizontalAlignment.Right
                Call RegainOldStyle()
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "tlbbtnRightAlign_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Styles "

    Private Sub objtlbbtnBold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBold.Click
        Try
            If objtlbbtnBold.Checked = True Then
                Call ApplyStyle(FontStyle.Bold, True)
            Else
                Call ApplyStyle(FontStyle.Bold, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBold_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnItalic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnItalic.Click
        Try
            If objtlbbtnItalic.Checked = True Then
                Call ApplyStyle(FontStyle.Italic, True)
            Else
                Call ApplyStyle(FontStyle.Italic, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnItalic_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnUnderline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUnderline.Click
        Try
            If objtlbbtnUnderline.Checked = True Then
                Call ApplyStyle(FontStyle.Underline, True)
            Else
                Call ApplyStyle(FontStyle.Underline, False)
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUnderline_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Operations "

    Private Sub objtlbbtnUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnUndo.Click
        Try
            rtbLetterPreview.Undo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnUndo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRedo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRedo.Click
        Try
            rtbLetterPreview.Redo()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRedo_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objtlbbtnBulletSimple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnBulletSimple.Click
        Try
            If objtlbbtnBulletSimple.Checked = True Then
                rtbLetterPreview.SelectionBullet = True
            Else
                rtbLetterPreview.SelectionBullet = False
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnBulletSimple_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnLeftIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnLeftIndent.Click
        Try
            If rtbLetterPreview.SelectionIndent = 0 Then
                rtbLetterPreview.SelectionIndent = 5
            Else
                rtbLetterPreview.SelectionIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnLeftIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnRightIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnRightIndent.Click
        Try
            If rtbLetterPreview.SelectionRightIndent = 0 Then
                rtbLetterPreview.SelectionRightIndent = 5
            Else
                rtbLetterPreview.SelectionRightIndent += 10
            End If
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objtlbbtnRightIndent_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCut.Click
        Try
            If rtbLetterPreview.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(rtbLetterPreview.SelectedText)
                rtbLetterPreview.SelectedText = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objtlbbtnCut_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnCopy.Click
        Try
            If rtbLetterPreview.SelectedText.Length > 0 Then
                My.Computer.Clipboard.SetText(rtbLetterPreview.SelectedText)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objtlbbtnCopy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnPaste.Click
        Try
            If My.Computer.Clipboard.ContainsText Then
                rtbLetterPreview.AppendText(My.Computer.Clipboard.GetText())
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objtlbbtnPaste_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objtlbbtnColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objtlbbtnColor.Click
        Try
            If pnlColor.Visible = True Then
                pnlColor.Hide()
                pnlColor.SendToBack()
            End If
            pnlColor.BringToFront()
            pnlColor.Show()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objtlbbtnColor_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objbtn1.MouseClick, _
                                objbtn2.MouseClick, objbtn3.MouseClick, objbtn4.MouseClick, objbtn5.MouseClick, objbtn6.MouseClick, objbtn7.MouseClick, _
                                objbtn8.MouseClick, objbtn9.MouseClick, objbtn10.MouseClick, objbtn11.MouseClick, objbtn12.MouseClick, objbtn13.MouseClick, objbtn14.MouseClick, _
                                 objbtn15.MouseClick, objbtn16.MouseClick, objbtn17.MouseClick, objbtn18.MouseClick, objbtn19.MouseClick, _
                                objbtn20.MouseClick, objbtn21.MouseClick, objbtn22.MouseClick, objbtn23.MouseClick, objbtn24.MouseClick, objbtn25.MouseClick, _
                                objbtn26.MouseClick, objbtn27.MouseClick, objbtn28.MouseClick, objbtn29.MouseClick, objbtn30.MouseClick, objbtn31.MouseClick, _
                                objbtn32.MouseClick, objbtn33.MouseClick, objbtn34.MouseClick, objbtn35.MouseClick, objbtn36.MouseClick, objbtn37.MouseClick, _
                                objbtn38.MouseClick, objbtn39.MouseClick, objbtn40.MouseClick, objbtn41.MouseClick, objbtn42.MouseClick, objbtn43.MouseClick, _
                                 objbtn44.MouseClick, objbtn45.MouseClick, objbtn46.MouseClick, objbtn47.MouseClick, objbtn48.MouseClick
        Try
            rtbLetterPreview.SelectionColor = CType(sender, Button).BackColor
            Call RegainOldStyle()
            pnlColor.Hide()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMoreColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoreColor.Click
        Try
            ColorDialog1.ShowDialog()
            rtbLetterPreview.SelectionColor = ColorDialog1.Color
            pnlColor.Hide()
            Call RegainOldStyle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMoreColor_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#End Region

    'Pinkal (14-Apr-2023) -- End







    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbVacancyFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbVacancyFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbApplicantList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApplicantList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbRecruitmentTemplate.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRecruitmentTemplate.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbDatesFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDatesFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSendEmails.GradientBackColor = GUI._ButttonBackColor
            Me.btnSendEmails.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbVacancyFilterCriteria.Text = Language._Object.getCaption(Me.gbVacancyFilterCriteria.Name, Me.gbVacancyFilterCriteria.Text)
            Me.LblVacancy.Text = Language._Object.getCaption(Me.LblVacancy.Name, Me.LblVacancy.Text)
            Me.LblVacanyType.Text = Language._Object.getCaption(Me.LblVacanyType.Name, Me.LblVacanyType.Text)
            Me.LblViewType.Text = Language._Object.getCaption(Me.LblViewType.Name, Me.LblViewType.Text)
            Me.tbpApplicantListSendEmail.Text = Language._Object.getCaption(Me.tbpApplicantListSendEmail.Name, Me.tbpApplicantListSendEmail.Text)
            Me.tbpApplicantEmailLogs.Text = Language._Object.getCaption(Me.tbpApplicantEmailLogs.Name, Me.tbpApplicantEmailLogs.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbApplicantList.Text = Language._Object.getCaption(Me.gbApplicantList.Name, Me.gbApplicantList.Text)
            Me.dgColhAppCode.HeaderText = Language._Object.getCaption(Me.dgColhAppCode.Name, Me.dgColhAppCode.HeaderText)
            Me.dgColhApplicant.HeaderText = Language._Object.getCaption(Me.dgColhApplicant.Name, Me.dgColhApplicant.HeaderText)
            Me.gbRecruitmentTemplate.Text = Language._Object.getCaption(Me.gbRecruitmentTemplate.Name, Me.gbRecruitmentTemplate.Text)
            Me.Label3.Text = Language._Object.getCaption(Me.Label3.Name, Me.Label3.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.btnSendEmails.Text = Language._Object.getCaption(Me.btnSendEmails.Name, Me.btnSendEmails.Text)
            Me.LblEmailSubject.Text = Language._Object.getCaption(Me.LblEmailSubject.Name, Me.LblEmailSubject.Text)
            Me.LblLetterGroup.Text = Language._Object.getCaption(Me.LblLetterGroup.Name, Me.LblLetterGroup.Text)
            Me.pbProgress.Text = Language._Object.getCaption(Me.pbProgress.Name, Me.pbProgress.Text)
            Me.gbDatesFilterCriteria.Text = Language._Object.getCaption(Me.gbDatesFilterCriteria.Name, Me.gbDatesFilterCriteria.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.LblApplicantType.Text = Language._Object.getCaption(Me.LblApplicantType.Name, Me.LblApplicantType.Text)
            Me.btnMoreColor.Text = Language._Object.getCaption(Me.btnMoreColor.Name, Me.btnMoreColor.Text)
            Me.dgcolhELApplicant.HeaderText = Language._Object.getCaption(Me.dgcolhELApplicant.Name, Me.dgcolhELApplicant.HeaderText)
            Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)
            Me.dgcolhVacancy.HeaderText = Language._Object.getCaption(Me.dgcolhVacancy.Name, Me.dgcolhVacancy.HeaderText)
            Me.dgcolhSubject.HeaderText = Language._Object.getCaption(Me.dgcolhSubject.Name, Me.dgcolhSubject.HeaderText)
            Me.dgcolhSendDateTime.HeaderText = Language._Object.getCaption(Me.dgcolhSendDateTime.Name, Me.dgcolhSendDateTime.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Non ShortListed Applicant")
            Language.setMessage(mstrModuleName, 3, "ShortListed Applicant")
            Language.setMessage(mstrModuleName, 4, "Final ShortListed Applicant")
            Language.setMessage(mstrModuleName, 5, "Non Interviewed Applicant")
            Language.setMessage(mstrModuleName, 6, "Interviewed Applicant")
            Language.setMessage(mstrModuleName, 7, "Non Eligible Applicant")
            Language.setMessage(mstrModuleName, 8, "Eligible Applicant")
            Language.setMessage(mstrModuleName, 9, "Email Subject is compulsory information.Please Enter Email Subject.")
            Language.setMessage(mstrModuleName, 10, "Select atleast one applicant to send email.")
            Language.setMessage(mstrModuleName, 11, "Mail(s) sent successfully!")
            Language.setMessage(mstrModuleName, 12, "Failure sending some mails!")
            Language.setMessage(mstrModuleName, 13, "Failure sending mails!")
            Language.setMessage(mstrModuleName, 14, "Please check your Internet settings.")
            Language.setMessage(mstrModuleName, 15, "Please Uncheck Login using SSL setting.")
            Language.setMessage(mstrModuleName, 16, "Vacancy Type is compulsory information.Please select Vancancy Type.")
            Language.setMessage(mstrModuleName, 17, "Vacancy is compulsory information.Please select Vancancy.")
            Language.setMessage(mstrModuleName, 18, "View Type is compulsory information.Please select View Type.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class