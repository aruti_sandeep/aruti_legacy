﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Collections.Specialized

Public Class frmDownloadAptitudeResult

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmDownloadAptitudeResult"
    Private objAppVacancy As clsApplicant_Vacancy_Mapping = Nothing
    Private dtTable As DataTable
    Private mstrSuccessText As String = ""
    Private mstrErrorText As String = ""

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Display Dialog "


#End Region

#Region " Form's Events "

    Private Sub frmDownloadAptitudeResult_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            objAppVacancy = New clsApplicant_Vacancy_Mapping
            mstrSuccessText = btnShowSuccessful.Text
            mstrErrorText = btnShowError.Text
            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDownloadAptitudeResult_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As DataSet = Nothing
        Dim objVacancy As New clsVacancy
        Try

            dsCombo = objVacancy.getVacancyType(True)
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objVacancy = Nothing
            dsCombo = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown Events"

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim dsVacList As New DataSet
            Dim objVac As New clsVacancy
            dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsVacList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


#Region " Button's Events "

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResult.Click
        Try
            If ConfigParameter._Object._AptitudeDatabaseServer.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Aptitude Database Server is compulsory information.Please configure it on configuration Aptitude mysql database connection."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub

            ElseIf ConfigParameter._Object._AptitudeDatabaseName.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Aptitude Database Name is compulsory information.Please configure it on configuration Aptitude mysql database connection."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub

            ElseIf ConfigParameter._Object._AptitudePortNo.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Aptitude Database Port No is compulsory information.Please configure it on configuration Aptitude mysql database connection."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub

            ElseIf ConfigParameter._Object._AptitudeUserName.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Aptitude Database User Name is compulsory information.Please configure it on configuration Aptitude mysql database connection."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub

            ElseIf ConfigParameter._Object._AptitudeUserPwd.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Aptitude Database User Password is compulsory information.Please configure it on configuration Aptitude mysql database connection."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub

            ElseIf CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Vacancy is compulsory information.Please Select Vacancy."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboVacancy.Select()
                Exit Sub
            End If

            dtTable = objAppVacancy.GetAptitudeResult(ConfigParameter._Object._AptitudeDatabaseServer, ConfigParameter._Object._AptitudeDatabaseName _
                                                                                                      , ConfigParameter._Object._AptitudePortNo, ConfigParameter._Object._AptitudeUserName, ConfigParameter._Object._AptitudeUserPwd _
                                                                                                      , CInt(cboVacancy.SelectedValue))

            If dtTable IsNot Nothing AndAlso dtTable.Columns.Contains("vacancy") = False Then
                Dim dcColum As New DataColumn("vacancy", Type.GetType("System.String"))
                dcColum.DefaultValue = cboVacancy.Text
                dtTable.Columns.Add(dcColum)
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Columns.Contains("image") = False Then
                Dim dcColum As New DataColumn("image", Type.GetType("System.Object"))
                dcColum.DefaultValue = New Drawing.Bitmap(1, 1).Clone
                dtTable.Columns.Add(dcColum)
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Columns.Contains("error") = False Then
                Dim dcColum As New DataColumn("error", Type.GetType("System.String"))
                dcColum.DefaultValue = ""
                dtTable.Columns.Add(dcColum)
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Columns.Contains("iserror") = False Then
                Dim dcColum As New DataColumn("iserror", Type.GetType("System.Boolean"))
                dcColum.DefaultValue = False
                dtTable.Columns.Add(dcColum)
            End If



            dgAptitudeResult.AutoGenerateColumns = False
            dgcolhImage.DataPropertyName = "image"
            dgcolhFirstName.DataPropertyName = "firstname"
            dgcolhSurname.DataPropertyName = "lastname"
            dgcolhEmail.DataPropertyName = "username"
            dgcolhFinalGrade.DataPropertyName = "finalgrade"
            dgcolhErrorMsg.DataPropertyName = "error"

            dgAptitudeResult.DataSource = dtTable

            Call objbtnSearchResult.ShowResult(CStr(dgAptitudeResult.RowCount))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResult_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboVacancyType.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            If dtTable IsNot Nothing Then dtTable.Rows.Clear()
            dgAptitudeResult.Refresh()
            btnShowSuccessful.Text = mstrSuccessText
            btnShowError.Text = mstrErrorText
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try
            Dim blnError As Boolean = False

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                Dim objApplicant As New clsApplicant_master

                Dim objAppVacancy As New clsApplicant_Vacancy_Mapping

                Dim intIndex As Integer = 0

                For Each dr As DataRow In dtTable.Rows

                    Dim xApplicantVacancyMappingId As Integer = -1

                    Dim xApplicantID As Integer = objApplicant.GetApplicantIDByEmail(dr("username").ToString())

                    If xApplicantID <= 0 Then
                        dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText = Language.getMessage(mstrModuleName, 7, "Applicant not found for this vacancy.")
                        dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ToolTipText = dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 7, "Applicant not found for this vacancy.")
                        dr("Image") = imgError
                        dr("iserror") = True
                        intIndex += 1
                        blnError = True
                        Continue For
                    End If

                    If objAppVacancy.isExist(xApplicantID, CInt(cboVacancy.SelectedValue), -1, xApplicantVacancyMappingId, False) Then

                        If xApplicantVacancyMappingId > 0 Then

                            objAppVacancy._Appvacancytranunkid = xApplicantVacancyMappingId
                            objAppVacancy._AptitudeResult = CDec(dr("finalgrade"))
                            objAppVacancy._Userunkid = User._Object._Userunkid

                            If objAppVacancy.Update() = False Then
                                dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText = objAppVacancy._Message
                                dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ToolTipText = dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText
                                dr("error") = objAppVacancy._Message
                                dr("Image") = imgError
                                dr("iserror") = True
                                intIndex += 1
                                blnError = True
                                Continue For
                            End If

                        Else
                            dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText = Language.getMessage(mstrModuleName, 8, "Applicant with this vacancy not found.")
                            dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ToolTipText = dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText
                            dr("error") = Language.getMessage(mstrModuleName, 8, "Applicant with this vacancy not found.")
                            dr("Image") = imgError
                            dr("iserror") = True
                            intIndex += 1
                            blnError = True
                            Continue For
                        End If

                        dr("Image") = imgAccept
                        intIndex += 1

                    Else
                        dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText = Language.getMessage(mstrModuleName, 8, "Applicant with this vacancy not found.")
                        dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ToolTipText = dgAptitudeResult.Rows(intIndex).Cells(dgcolhErrorMsg.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 8, "Applicant with this vacancy not found.")
                        dr("Image") = imgError
                        dr("iserror") = True
                        intIndex += 1
                        blnError = True
                        Continue For
                    End If

                Next

                objAppVacancy = Nothing
                objApplicant = Nothing

                If blnError = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Data successfully imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dtTable.Rows.Clear()
                End If

                dgAptitudeResult.Refresh()

                Dim xSuccessCount = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iserror") = False).Count
                Dim xFailCount = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iserror") = True).Count

                btnShowSuccessful.Text = mstrSuccessText & " (" & xSuccessCount.ToString() & ")"
                btnShowError.Text = mstrErrorText & " (" & xFailCount.ToString() & ")"

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImportData_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowSuccessful.Click
        Try
            If dgAptitudeResult.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "There is no data to show successful(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dtTable IsNot Nothing Then
                Dim dvGriddata As DataView = dtTable.DefaultView
                dvGriddata.RowFilter = "iserror = 0 "
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub btnShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowError.Click
        Try
            If dgAptitudeResult.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no data to show error(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dtTable IsNot Nothing Then
                Dim dvGriddata As DataView = dtTable.DefaultView
                dvGriddata.RowFilter = "iserror = 1"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportError.Click
        Try

            If dgAptitudeResult.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no data to show error(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dtTable IsNot Nothing Then
                Dim dvGriddata As DataView = dtTable.DefaultView
                dvGriddata.RowFilter = "iserror = 1"
                Dim dtError As DataTable = dvGriddata.ToTable
                If dtError.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtError, "Import Aptitude Result") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbVacancyFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbVacancyFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbVacancyFilterCriteria.Text = Language._Object.getCaption(Me.gbVacancyFilterCriteria.Name, Me.gbVacancyFilterCriteria.Text)
            Me.LblVacancy.Text = Language._Object.getCaption(Me.LblVacancy.Name, Me.LblVacancy.Text)
            Me.LblVacanyType.Text = Language._Object.getCaption(Me.LblVacanyType.Name, Me.LblVacanyType.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
            Me.btnShowError.Text = Language._Object.getCaption(Me.btnShowError.Name, Me.btnShowError.Text)
            Me.dgcolhImage.HeaderText = Language._Object.getCaption(Me.dgcolhImage.Name, Me.dgcolhImage.HeaderText)
            Me.dgcolhFirstName.HeaderText = Language._Object.getCaption(Me.dgcolhFirstName.Name, Me.dgcolhFirstName.HeaderText)
            Me.dgcolhSurname.HeaderText = Language._Object.getCaption(Me.dgcolhSurname.Name, Me.dgcolhSurname.HeaderText)
            Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)
            Me.dgcolhFinalGrade.HeaderText = Language._Object.getCaption(Me.dgcolhFinalGrade.Name, Me.dgcolhFinalGrade.HeaderText)
            Me.dgcolhErrorMsg.HeaderText = Language._Object.getCaption(Me.dgcolhErrorMsg.Name, Me.dgcolhErrorMsg.HeaderText)
            Me.btnShowSuccessful.Text = Language._Object.getCaption(Me.btnShowSuccessful.Name, Me.btnShowSuccessful.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Aptitude Database Server is compulsory information.Please configure it on configuration Aptitude mysql database connection.")
            Language.setMessage(mstrModuleName, 2, "Aptitude Database Name is compulsory information.Please configure it on configuration Aptitude mysql database connection.")
            Language.setMessage(mstrModuleName, 3, "Aptitude Database Port No is compulsory information.Please configure it on configuration Aptitude mysql database connection.")
            Language.setMessage(mstrModuleName, 4, "Aptitude Database User Name is compulsory information.Please configure it on configuration Aptitude mysql database connection.")
            Language.setMessage(mstrModuleName, 5, "Aptitude Database User Password is compulsory information.Please configure it on configuration Aptitude mysql database connection.")
            Language.setMessage(mstrModuleName, 6, "Vacancy is compulsory information.Please Select Vacancy.")
            Language.setMessage(mstrModuleName, 7, "Applicant not found for this vacancy.")
            Language.setMessage(mstrModuleName, 8, "Applicant with this vacancy not found.")
            Language.setMessage(mstrModuleName, 9, "Data successfully imported.")
            Language.setMessage(mstrModuleName, 10, "There is no data to show error(s).")
            Language.setMessage(mstrModuleName, 11, "There is no data to show successful(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class