﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmCancelRemark

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmCancelRemark"
    Private mblnCancel As Boolean = True
    Private objAppBatchScheduleData As clsApplicant_Batchschedule_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAppBatchScheduleTranId As Integer = -1
    Private mblnIsAction_Reasom As Boolean = False
    Private mstrBatchName As String = ""
    Private mstrApplicantName As String = ""
#End Region


#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal strBatchName As String, ByVal strApplicantName As String) As Boolean
        Try
            mintAppBatchScheduleTranId = intUnkId
            mstrBatchName = strBatchName
            mstrApplicantName = strApplicantName
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintAppBatchScheduleTranId

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtBatchName.BackColor = GUI.ColorOptional
            txtCancelRemark.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    'Private Sub GetValue()
    '    txtBatchName.Text = objAppBatchScheduleData._Code
    '    txtCancelRemark.Text = objAppBatchScheduleData._Reason_Action
    'End Sub

    Private Sub SetValue()
        objAppBatchScheduleData._Appbatchscheduletranunkid = mintAppBatchScheduleTranId
        objAppBatchScheduleData._Canceldatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
        objAppBatchScheduleData._Iscancel = True
        objAppBatchScheduleData._Cancelremark = txtCancelRemark.Text
     
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmAction_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAppBatchScheduleData = Nothing
    End Sub

    Private Sub frmAction_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAction_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCancelRemark_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAppBatchScheduleData = New clsApplicant_Batchschedule_Tran
        'Call Language.setLanguage(Me.Name)
        'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

        Try

            Call Set_Logo(Me, gApplicationType)
            'Call OtherSettings()
            Call setColor()

            If menAction = enAction.ADD_ONE Then
                objAppBatchScheduleData._Appbatchscheduletranunkid = mintAppBatchScheduleTranId
            End If

            txtBatchName.Text = mstrBatchName
            txtApplicantName.Text = mstrApplicantName

            txtCancelRemark.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCancelRemark_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsApplicant_Batchschedule_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicant_Batchschedule_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

    'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._RightToLeft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        clsAirLine.SetMessages()
    '        objfrm._Other_ModuleNames = "clsAirLine"
    '        objfrm.displayDialog(Me)


    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCancelRemark.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Cancel Remark cannot be blank. Cancel Remark is required information."), enMsgBoxStyle.Information)
                txtCancelRemark.Focus()
                Exit Sub
            End If

            Call SetValue()
            With objAppBatchScheduleData
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            blnFlag = objAppBatchScheduleData.Cancel_Applicant(mintAppBatchScheduleTranId)

            If blnFlag = False And objAppBatchScheduleData._Message <> "" Then
                eZeeMsgBox.Show(objAppBatchScheduleData._Message, enMsgBoxStyle.Information)
            End If


            'mintActionReasonUnkid = objAction_Reason._Actionreasonunkid
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbAccessInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAccessInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAccessInfo.Text = Language._Object.getCaption(Me.gbAccessInfo.Name, Me.gbAccessInfo.Text)
			Me.lblCancelRemark.Text = Language._Object.getCaption(Me.lblCancelRemark.Name, Me.lblCancelRemark.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblApplicantName.Text = Language._Object.getCaption(Me.lblApplicantName.Name, Me.lblApplicantName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Cancel Remark cannot be blank. Cancel Remark is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class