﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

Public Class frmApplicantList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApplicantList"
    Private objApplicantList As clsApplicant_master
    Private mstrApplicant() As String
    Private dsList As New DataSet
    Private mblnIsFromMail As Boolean = False
    Private mblnExport_Print As Boolean = False
    Private mstrApplicantId As String
    Private mdtApplicant As DataTable = Nothing

#End Region

#Region " Properties "

    Public ReadOnly Property EmailList() As String()
        Get
            Return mstrApplicant
        End Get
    End Property

    Public Property _DataView() As DataSet
        Get
            Return dsList
        End Get
        Set(ByVal value As DataSet)
            dsList = value
        End Set
    End Property

    Public WriteOnly Property _IsFromMail() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property

    Public WriteOnly Property IsExport_Print() As Boolean
        Set(ByVal value As Boolean)
            mblnExport_Print = value
        End Set
    End Property

    Public ReadOnly Property EmployeeIDs() As String
        Get
            Return mstrApplicantId
        End Get
    End Property

#End Region

#Region " Private Function "

    Private Sub SetColor()
        Try
            txtReferenceNo.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtMobile.BackColor = GUI.ColorOptional
            txtTelNo.BackColor = GUI.ColorOptional
            cboApplicant.BackColor = GUI.ColorOptional
            cboVacancy.BackColor = GUI.ColorOptional
            cboVacancyType.BackColor = GUI.ColorOptional
            cboApplicantType.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strSearching As String = ""
        Dim objShortListApplicant As New clsshortlist_finalapplicant
        Try
            If User._Object.Privilege._AllowToViewApplicantMasterList = True Then
                If CInt(cboApplicant.SelectedValue) > 0 Then
                    strSearching &= "AND rcapplicant_master.applicantunkid = '" & CInt(cboApplicant.SelectedValue) & "' "
                End If

                If txtEmail.Text.Trim.Length > 0 Then
                    strSearching &= "AND rcapplicant_master.email LIKE '%" & txtEmail.Text & "%'" & " "
                End If

                If txtMobile.Text.Trim.Length > 0 Then
                    strSearching &= "AND rcapplicant_master.present_mobileno LIKE '%" & txtMobile.Text & "%' "
                End If

                If txtReferenceNo.Text.Trim.Length > 0 Then
                    strSearching &= "AND rcapplicant_master.referenceno LIKE '%" & clsCrypto.Encrypt(txtReferenceNo.Text).ToString & "%'" & " "
                End If

                If txtTelNo.Text.Trim.Length > 0 Then
                    strSearching &= "AND rcapplicant_master.present_tel_no LIKE '%" & txtTelNo.Text & "%' "
                End If

                If CInt(cboVacancy.SelectedValue) > 0 Then
                    strSearching &= "AND ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) = " & CInt(cboVacancy.SelectedValue)
                End If

                If CInt(cboApplicantType.SelectedValue) > 0 Then
                    Select Case CInt(cboApplicantType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND rcvacancy_master.isexternalvacancy = 1 "
                            'Sohail (25 Sep 2020) -- Start
                            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                            'strSearching &= "AND (rcvacancy_master.isexternalvacancy = 1 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                            'Sohail (25 Sep 2020) -- End
                            'Sohail (18 Apr 2020) -- End
                        Case enVacancyType.INTERNAL_VACANCY
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND rcvacancy_master.isexternalvacancy = 0 "
                            'Sohail (25 Sep 2020) -- Start
                            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                            'strSearching &= "AND (rcvacancy_master.isexternalvacancy = 0 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                            strSearching &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                            'Sohail (25 Sep 2020) -- End
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            'strSearching &= "AND ISNULL(rcvacancy_master.isbothintext, 0) = 1 " 'Sohail (25 Sep 2020)
                            'Sohail (18 Apr 2020) -- End
                    End Select
                End If

                If CInt(cboVacancyType.SelectedValue) > 0 Then
                    Select Case CInt(cboVacancyType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND rcvacancy_master.isexternalvacancy = 1 "
                            strSearching &= "AND (rcvacancy_master.isexternalvacancy = 1 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                            'Sohail (18 Apr 2020) -- End
                        Case enVacancyType.INTERNAL_VACANCY
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND rcvacancy_master.isexternalvacancy = 0 "
                            strSearching &= "AND (rcvacancy_master.isexternalvacancy = 0 AND ISNULL(rcvacancy_master.isbothintext, 0) = 0) "
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            strSearching &= "AND ISNULL(rcvacancy_master.isbothintext, 0) = 1 "
                            'Sohail (18 Apr 2020) -- End
                    End Select
                End If

                If CInt(cboShortListType.SelectedIndex) = 1 Then
                    dsList = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), True, strSearching)
                ElseIf CInt(cboShortListType.SelectedIndex) = 2 Then
                    dsList = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), False, strSearching)
                Else
                    dsList = objApplicantList.GetList("List", True, , strSearching)
                End If
                mdtApplicant = New DataView(dsList.Tables(0), "", "applicantname", DataViewRowState.CurrentRows).ToTable

                If mdtApplicant.Columns.Contains("isChecked") = False Then
                    Dim dCol As New DataColumn
                    With dCol
                        .DataType = GetType(System.Boolean)
                        .DefaultValue = False
                        .ColumnName = "isChecked"
                    End With
                    mdtApplicant.Columns.Add(dCol)
                End If

                dgvApplicantList.AutoGenerateColumns = False

                objdgcolhCheck.DataPropertyName = "isChecked"
                dgcolhApplicantName.DataPropertyName = "applicantname"
                dgcolhemail.DataPropertyName = "email"
                dgcolhVacancy.DataPropertyName = "vacancy"
                dgcolhMobile.DataPropertyName = "present_mobileno"
                dgcolhTelNo.DataPropertyName = "present_tel_no"
                dgcolhRefNo.DataPropertyName = "referenceno"
                objdgcolhempcode.DataPropertyName = "employeecode"
                objdgcolhVacId.DataPropertyName = "mapvacancyunkid"
                objdgcolhApplicantId.DataPropertyName = "applicantunkid"
                'Sohail (09 Oct 2018) -- Start
                'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                objdgcolhIsfromonline.DataPropertyName = "isfromonline"
                'Sohail (09 Oct 2018) -- End

                dgvApplicantList.DataSource = mdtApplicant

                objdgcolhCheck.Visible = mblnIsFromMail
                objChkAll.Visible = mblnIsFromMail

                dgcolhRefNo.Visible = User._Object.Privilege._AllowToViewApplicantReferenceNo

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objVacancy As New clsVacancy
        Try
            dsCombo = objApplicantList.GetApplicantList("list", True, , True)
            With cboApplicant
                .ValueMember = "applicantunkid"
                .DisplayMember = "applicantname"
                .DataSource = dsCombo.Tables("list")
                .SelectedValue = 0
            End With

            dsCombo = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            'dsCombo = objVacancy.getVacancyType
            dsCombo = objVacancy.getVacancyType(False)
            'Sohail (25 Sep 2020) -- End
            With cboApplicantType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objVacancy = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddApplicant
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditApplicant
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteApplicant
            mnuScan_Documents.Enabled = User._Object.Privilege._AllowtoAttachApplicantDocuments
            mnuMapVacancy.Enabled = User._Object.Privilege._AllowtoMapVacancy
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetApplicantEmail()
        Try
            Dim strNumber As String = ""
            Dim objLetterFields As New clsLetterFields
            mstrApplicant = New String() {}
            Dim blnFlag As Boolean = False
            Dim intCnt As Integer = 0
            Dim drRow() As DataRow = Nothing
            drRow = mdtApplicant.Select("isChecked = True")
            If drRow.Length > 0 Then
                ReDim mstrApplicant(drRow.Length + 1)
                For i As Integer = 0 To drRow.Length - 1
                    If drRow(i)("email").ToString <> "" Then
                        mstrApplicant(intCnt) = drRow(i)("applicantname").ToString & " " & "<" & drRow(i)("email").ToString & ">"
                        If strNumber = "" Then
                            strNumber = drRow(i)("applicantunkid").ToString
                        Else
                            strNumber = strNumber & " , " & drRow(i)("applicantunkid").ToString
                        End If
                        intCnt += 1
                    Else
                        If blnFlag = False Then
                            Dim strMsg As String = Language.getMessage(mstrModuleName, 2, "Some of the email address(s) are blank. Do you want to continue?")
                            If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                blnFlag = True
                                Continue For
                            Else
                                blnFlag = False
                                Exit Sub
                            End If
                        End If
                    End If
                Next
                If strNumber <> "" Then
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 18 : On letter template, applicant fields, provide parameter to pick employee reporting-to. (For internal vacancies))
                    'objLetterFields._VacancyUnkid = CInt(cboVacancy.SelectedValue)
                    objLetterFields._VacancyUnkid = CStr(cboVacancy.SelectedValue)
                    'Hemant (07 Oct 2019) -- End
                    'S.SANDEEP |09-APR-2019| -- START
                    'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                    dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                    'S.SANDEEP |09-APR-2019| -- END

                End If
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantEmail", mstrModuleName)
        End Try
    End Sub

    Private Sub GetApplicantPrint_Export_Letter()
        Try
            Dim strNumber As String = ""
            Dim objLetterFields As New clsLetterFields
            mstrApplicant = New String() {}
            Dim blnFlag As Boolean = False
            Dim intCnt As Integer = 0
            Dim drRow() As DataRow = Nothing
            drRow = mdtApplicant.Select("isChecked = True")
            If drRow.Length > 0 Then
                ReDim mstrApplicant(drRow.Length + 1)
                For i As Integer = 0 To drRow.Length - 1
                    If drRow(i)("applicantunkid").ToString <> "" Then
                        mstrApplicant(intCnt) = drRow(i)("applicantname").ToString
                        If strNumber = "" Then
                            strNumber = drRow(i)("applicantunkid").ToString
                            mstrApplicantId = drRow(i)("applicantunkid").ToString
                        Else
                            strNumber = strNumber & " , " & drRow(i)("applicantunkid").ToString
                            mstrApplicantId = mstrApplicantId & " , " & drRow(i)("applicantunkid").ToString
                        End If
                        intCnt += 1
                    End If
                Next
                If strNumber <> "" Then
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 18 : On letter template, applicant fields, provide parameter to pick employee reporting-to. (For internal vacancies))
                    'objLetterFields._VacancyUnkid = CInt(cboVacancy.SelectedValue)
                    objLetterFields._VacancyUnkid = CStr(cboVacancy.SelectedValue)
                    'Hemant (07 Oct 2019) -- End
                    'S.SANDEEP |09-APR-2019| -- START
                    'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                    dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                    'S.SANDEEP |09-APR-2019| -- END

                End If
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantPrint_Export_Letter", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmApplicantList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objApplicantList = Nothing
    End Sub

    Private Sub frmApplicantList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmApplicantList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objApplicantList = New clsApplicant_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings() 'Sohail (09 Jul 2021)
            Call SetColor()
            Call FillCombo()

            If mblnIsFromMail = True Then
                objChkAll.BringToFront()
                lblShortListType.Visible = True
                cboShortListType.Visible = True
                cboShortListType.BackColor = GUI.ColorOptional
                cboShortListType.Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
                cboShortListType.Items.Add(Language.getMessage(mstrModuleName, 5, "Final Short Listed Applicant"))
                cboShortListType.Items.Add(Language.getMessage(mstrModuleName, 6, "Non Short Listed Applicant"))
                cboShortListType.SelectedIndex = 0
            End If

            objdgcolhCheck.Visible = mblnIsFromMail
            objChkAll.Visible = mblnIsFromMail

            chkIncludeInternalApplicant.Checked = False
            objpnlInternalApplicantColor.Visible = False

            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            'Call SetMessages()
            clsApplicant_master.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicant_master"
            objfrm.displayDialog(Me)

            'Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            txtEmail.Text = ""
            txtMobile.Text = ""
            txtReferenceNo.Text = ""
            txtTelNo.Text = ""
            cboApplicant.SelectedValue = 0
            cboApplicantType.SelectedValue = 0
            objpnlInternalApplicantColor.Visible = True
            If mblnIsFromMail Then
                cboShortListType.SelectedIndex = 0
            End If
            cboVacancyType.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            dgvApplicantList.DataSource = Nothing
            Call objbtnReset.ShowResult(CStr(dgvApplicantList.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            objChkAll.Checked = False
            'Sandeep(02-May-2017)-Start
            'Issue: Removed this checkpoint because for new applicants registered from aruti desktop were not appearing in this list for mapping vacancy to them.
            'If CInt(cboVacancy.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Vacancy to continue."), enMsgBoxStyle.Information)
            '    cboVacancy.Focus()
            '    Exit Sub
            'End If
            'Sandeep(02-May-2017)-End
            Call FillList()

            Call objbtnSearch.ShowResult(CStr(dgvApplicantList.RowCount))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnMailClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMailClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMailClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If mblnExport_Print = False Then
                GetApplicantEmail()
            Else
                GetApplicantPrint_Export_Letter()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmApplicantMaster
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvApplicantList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation."), enMsgBoxStyle.Information)
            dgvApplicantList.Focus()
            Exit Sub
        End If
        Dim frm As New frmApplicantMaster
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = dgvApplicantList.SelectedRows(0).Index

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Sohail (02 May 2022) -- Start
            'Enhancement :  AC2-312 / AC2-326 : ZRA - Newly added skills should not populate on old job applications for closed vacancies on applicant master.
            Dim dtVacancyEndDate As Date = Nothing
            If CType(dgvApplicantList.SelectedRows(0).DataBoundItem, DataRowView).Item("CDate").ToString <> "" Then
                dtVacancyEndDate = eZeeDate.convertDate(CType(dgvApplicantList.SelectedRows(0).DataBoundItem, DataRowView).Item("CDate").ToString)
            End If
            'Sohail (02 May 2022) -- End
            If frm.displayDialog(CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value), enAction.EDIT_ONE, CInt(cboVacancy.SelectedValue), dtVacancyEndDate) Then
                'Sohail (02 May 2022) - [dtVacancyEndDate]
                Call FillList()
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvApplicantList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation."), enMsgBoxStyle.Information)
            dgvApplicantList.Focus()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = dgvApplicantList.SelectedRows(0).Index


            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Are you sure you want to delete this Applicant?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objApplicantList._FormName = mstrModuleName
                objApplicantList._LoginEmployeeUnkid = 0
                objApplicantList._ClientIP = getIP()
                objApplicantList._HostName = getHostName()
                objApplicantList._FromWeb = False
                objApplicantList._AuditUserId = User._Object._Userunkid
objApplicantList._CompanyUnkid = Company._Object._Companyunkid
                objApplicantList._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                'If objApplicantList.Delete(CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value)) Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.APPLICANT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                If objApplicantList.Delete(CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value), True, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) Then
                    'Sohail (25 Sep 2020) -- End
                    If objApplicantList._Message <> "" Then
                        eZeeMsgBox.Show(objApplicantList._Message, enMsgBoxStyle.Information)
                    Else
                        Call FillList()
                    End If
                Else
                    If objApplicantList._Message <> "" Then
                        eZeeMsgBox.Show(objApplicantList._Message, enMsgBoxStyle.Information)
                    End If
                End If
            End If

            dgvApplicantList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboApplicant.ValueMember
                .DisplayMember = cboApplicant.DisplayMember
                .CodeMember = "applicant_code"
                .DataSource = CType(cboApplicant.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboApplicant.SelectedValue = frm.SelectedValue
                cboApplicant.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Hemant (30 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : On applicant master, they want to have a smart search on the vacancy drop down. Currently its taking a lot of time to scroll through all vacancies.
    Private Sub EZeeGradientButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboVacancy.SelectedValue = objFrm.SelectedValue
                cboVacancy.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EZeeGradientButton1_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (30 Mar 2022) -- End


#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvApplicantList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvApplicantList.CellContentClick, dgvApplicantList.CellContentDoubleClick
        Try
            If dgvApplicantList.RowCount <= 0 Then Exit Sub

            If Me.dgvApplicantList.IsCurrentCellDirty Then
                Me.dgvApplicantList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            mdtApplicant.AcceptChanges()
            Dim dtmp() As DataRow = mdtApplicant.Select("isChecked = True")
            If dtmp.Length > 0 Then
                If dtmp.Length < mdtApplicant.Rows.Count Then
                    objChkAll.CheckState = CheckState.Indeterminate
                ElseIf dtmp.Length = mdtApplicant.Rows.Count Then
                    objChkAll.CheckState = CheckState.Checked
                End If
            Else
                objChkAll.CheckState = CheckState.Unchecked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvApplicantList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvApplicantList_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvApplicantList.CellPainting
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If CStr(dgvApplicantList.Rows(e.RowIndex).Cells(objdgcolhempcode.Index).Value).Trim.Length > 0 Then
                dgvApplicantList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = objpnlInternalApplicantColor.BackColor
            End If


            If dgcolhRefNo.Visible = True Then
                If e.ColumnIndex = dgcolhRefNo.Index Then
                    If dgvApplicantList.Columns(e.ColumnIndex).DataPropertyName = dgcolhRefNo.DataPropertyName Then
                        Try
                            dgvApplicantList.Rows(e.RowIndex).Cells(dgcolhRefNo.Index).Value = clsCrypto.Dicrypt(dgvApplicantList.Rows(e.RowIndex).Cells(dgcolhRefNo.Index).Value.ToString)
                        Catch ex As Exception
                        End Try
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvApplicantList_CellPainting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvApplicantList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvApplicantList.DataError

    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                Dim dsVacList As New DataSet
                Dim objVac As New clsVacancy
                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
                With cboVacancy
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsVacList.Tables("List")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboApplicantType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApplicantType.SelectedIndexChanged
        Try
            If CInt(cboApplicantType.SelectedValue) > 0 Then
                Select Case CInt(cboApplicantType.SelectedValue)
                    Case enVacancyType.EXTERNAL_VACANCY
                        objpnlInternalApplicantColor.Visible = False
                    Case enVacancyType.INTERNAL_VACANCY
                        objpnlInternalApplicantColor.Visible = True
                    Case Else
                        objpnlInternalApplicantColor.Visible = True
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboApplicantType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Menu Event(s) "

    Private Sub mnuScan_Documents_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScan_Documents.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If dgvApplicantList.SelectedRows.Count <= 0 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'S.SANDEEP |26-APR-2019| -- START
            'frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Applicant"), enImg_Email_RefId.Applicant_Module, enAction.ADD_ONE, "", mstrModuleName)
            frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Applicant"), enImg_Email_RefId.Applicant_Module, enAction.ADD_ONE, "", mstrModuleName, False)
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnScan_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuMapVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMapVacancy.Click
        Dim frm As New frmVacancyMapping
        Try
            If dgvApplicantList.SelectedRows.Count <= 0 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value), dgvApplicantList.SelectedRows(0).Cells(dgcolhApplicantName.Index).Value.ToString) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMapVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuPreviewAttachments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreviewAttachments.Click
        Dim frm As New frmScanAttachmentList
        Try
            If dgvApplicantList.SelectedRows.Count <= 0 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Gajanan [11-NOV-2019] -- Start   
            GC.Collect()
            'Gajanan [11-NOV-2019] -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            '  frm.displayDialog(enScanAttactRefId.QUALIFICATIONS, CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value), True)

            'Pinkal (18-Aug-2023) -- Start
            'Recruitment Issue - Not Matching Applicant Documents Count with Applicant Master List Applicants Count . 
            'frm.displayDialog(enScanAttactRefId.QUALIFICATIONS, CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value), True, False, True, CInt(cboVacancy.SelectedValue))
            frm.displayDialog(enScanAttactRefId.QUALIFICATIONS, CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value), True, False, True, CInt(cboVacancyType.SelectedValue), CInt(cboVacancy.SelectedValue))
            'Pinkal (18-Aug-2023) -- End

            'Pinkal (04-Aug-2023) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreviewAttachments_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (15-May-2023) -- Start
    '(A1X-899) Ifakara - Employee Timesheet submission report fixes.
    Private Sub mnuDownloadAttachments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDownloadAttachments.Click
        Dim objDocument As clsScan_Attach_Documents
        Try
            If dgvApplicantList.SelectedRows.Count > 0 Then
                objDocument = New clsScan_Attach_Documents
                Dim mblnIsSelfServiceInstall As Boolean = IsSelfServiceExist()

                Dim mstrFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", CInt(enScanAttactRefId.CURRICULAM_VITAE)).Tables(0).Rows(0)("Name").ToString()

                Dim dsList As DataSet = objDocument.GetList(ConfigParameter._Object._Document_Path, "List", "", -1, False, Nothing, Nothing, "", False, -1, CInt(enScanAttactRefId.CURRICULAM_VITAE), CInt(dgvApplicantList.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value), True)
                Dim mdtTran As DataTable = dsList.Tables(0).Copy()


                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no attached document(s) for this applicant."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim strError As String = ""
                Dim strLocalPath As String = String.Empty
                Dim fileToAdd As New List(Of String)
                For Each xRow As DataRow In mdtTran.Rows
                    If IsDBNull(xRow("file_data")) = False Then
                        strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                        Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                        Dim fs As New FileStream(strLocalPath, FileMode.Create)
                        ms.WriteTo(fs)
                        ms.Close()
                        fs.Close()
                        fs.Dispose()
                        If strLocalPath <> "" Then
                            fileToAdd.Add(strLocalPath)
                        End If
                    ElseIf IsDBNull(xRow("file_data")) = True Then
                        If IsSelfServiceExist() Then
                            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)
                            If imagebyte IsNot Nothing Then
                                strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                                Dim ms As New MemoryStream(imagebyte)
                                Dim fs As New FileStream(strLocalPath, FileMode.Create)
                                ms.WriteTo(fs)
                                ms.Close()
                                fs.Close()
                                fs.Dispose()
                            End If
                            fileToAdd.Add(strLocalPath)
                        Else
                            strLocalPath = xRow("filepath").ToString()
                            If IO.File.Exists(strLocalPath) = True Then
                                fileToAdd.Add(strLocalPath)
                            End If
                        End If
                    End If
                Next

                If fileToAdd.Count > 0 Then
                    Dim fbd As New FolderBrowserDialog()
                    fbd.ShowNewFolderButton = True
                    If (fbd.ShowDialog() = DialogResult.OK) Then
                        Dim fl As New System.IO.FileInfo(fbd.SelectedPath)
                        For Each item As String In fileToAdd
                            If item.ToString.Trim.Length <= 0 Then Continue For
                            Dim destFilename As String = ".\" & Path.GetFileName(item)
                            System.IO.File.Copy(item.ToString, fl.FullName.ToString & "\" & Path.GetFileName(item))
                        Next
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                    End If

                    For Each fl As String In fileToAdd
                        Try
                            System.IO.File.Delete(fl)
                        Catch ex As Exception
                        End Try
                    Next
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "There is no attached document(s) for this applicant."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "There is no attached document(s) for this applicant."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDownloadAttachments_Click", mstrModuleName)
        Finally
            objDocument = Nothing
        End Try
    End Sub
    'Pinkal (15-May-2023) -- End


#End Region

#Region " Checkbox Event(s) "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler dgvApplicantList.CellContentClick, AddressOf dgvApplicantList_CellContentClick
            For Each row As DataRow In mdtApplicant.Rows
                row.Item("isChecked") = objChkAll.Checked
            Next
            mdtApplicant.AcceptChanges()
            AddHandler dgvApplicantList.CellContentClick, AddressOf dgvApplicantList_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Old Class "
'********************************* S.SANDEEP [20-APR-2017] -- ISSUE/ENHANCEMENT : OPTIMIZING RECRUITMENT MODULE *********************************

'Public Class frmApplicantList

'    Private ReadOnly mstrModuleName As String = "frmApplicantList"
'    Private objApplicantList As clsApplicant_master
'    Private mstrApplicant() As String
'    Private dsList As New DataSet

'    'Pinkal (12-Oct-2011) -- Start
'    Private mblnIsFromMail As Boolean = False
'    Private mblnExport_Print As Boolean = False
'    Private mstrApplicantId As String
'    'Pinkal (12-Oct-2011) -- End

'#Region " Properties "
'    Public ReadOnly Property EmailList() As String()
'        Get
'            Return mstrApplicant
'        End Get
'    End Property

'    Public Property _DataView() As DataSet
'        Get
'            Return dsList
'        End Get
'        Set(ByVal value As DataSet)
'            dsList = value
'        End Set
'    End Property

'    'Pinkal (12-Oct-2011) -- Start

'    Public WriteOnly Property _IsFromMail() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsFromMail = value
'        End Set
'    End Property

'    Public WriteOnly Property IsExport_Print() As Boolean
'        Set(ByVal value As Boolean)
'            mblnExport_Print = value
'        End Set
'    End Property

'    Public ReadOnly Property EmployeeIDs() As String
'        Get
'            Return mstrApplicantId
'        End Get
'    End Property

'    'Pinkal (12-Oct-2011) -- End

'#End Region

'#Region " Private Function "

'    Private Sub SetColor()
'        Try
'            txtSurname.BackColor = GUI.ColorOptional
'            txtFirstname.BackColor = GUI.ColorOptional
'            txtReferenceNo.BackColor = GUI.ColorOptional
'            txtEmail.BackColor = GUI.ColorOptional
'            txtMobile.BackColor = GUI.ColorOptional
'            txtTelNo.BackColor = GUI.ColorOptional
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            cboApplicant.BackColor = GUI.ColorOptional
'            cboVacancy.BackColor = GUI.ColorOptional
'            cboVacancyType.BackColor = GUI.ColorOptional
'            'S.SANDEEP [ 25 DEC 2011 ] -- END

'            'Anjan (09 Jan 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS
'            cboApplicantType.BackColor = GUI.ColorOptional
'            'Anjan (09 Jan 2012)-End 


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillList()
'        Dim dsList As New DataSet
'        Dim strSearching As String = ""
'        Dim dtTable As New DataTable
'        Dim objShortListApplicant As New clsshortlist_finalapplicant
'        Try

'            If User._Object.Privilege._AllowToViewApplicantMasterList = True Then                'Pinkal (02-Jul-2012) -- Start

'                'Sohail (05 Dec 2016) -- Start
'                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                'If CInt(cboShortListType.SelectedIndex) = 1 Then
'                '    dsList = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), True)
'                'ElseIf CInt(cboShortListType.SelectedIndex) = 2 Then
'                '    dsList = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), False)
'                'Else
'                '    dsList = objApplicantList.GetList("List", True)
'                'End If
'                'Sohail (05 Dec 2016) -- End

'                If CInt(cboApplicant.SelectedValue) > 0 Then
'                    'Sohail (05 Dec 2016) -- Start
'                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                    'strSearching &= "AND applicantunkid = '" & CInt(cboApplicant.SelectedValue) & "' "
'                    strSearching &= "AND rcapplicant_master.applicantunkid = '" & CInt(cboApplicant.SelectedValue) & "' "
'                    'Sohail (05 Dec 2016) -- End
'                End If


'                If txtEmail.Text.Trim.Length > 0 Then
'                    'Sohail (05 Dec 2016) -- Start
'                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                    'strSearching &= "AND email LIKE '%" & txtEmail.Text & "%'" & " "
'                    strSearching &= "AND rcapplicant_master.email LIKE '%" & txtEmail.Text & "%'" & " "
'                    'Sohail (05 Dec 2016) -- End
'                End If

'                If txtMobile.Text.Trim.Length > 0 Then
'                    'Sohail (05 Dec 2016) -- Start
'                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                    'strSearching &= "AND present_mobileno LIKE '%" & txtMobile.Text & "%' "
'                    strSearching &= "AND rcapplicant_master.present_mobileno LIKE '%" & txtMobile.Text & "%' "
'                    'Sohail (05 Dec 2016) -- End
'                End If


'                If txtReferenceNo.Text.Trim.Length > 0 Then
'                    'Sohail (05 Dec 2016) -- Start
'                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                    'strSearching &= "AND referenceno LIKE '%" & clsCrypto.Encrypt(txtReferenceNo.Text).ToString & "%'" & " "
'                    strSearching &= "AND rcapplicant_master.referenceno LIKE '%" & clsCrypto.Encrypt(txtReferenceNo.Text).ToString & "%'" & " "
'                    'Sohail (05 Dec 2016) -- End
'                End If

'                If txtTelNo.Text.Trim.Length > 0 Then
'                    'Sohail (05 Dec 2016) -- Start
'                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                    'strSearching &= "AND present_tel_no LIKE '%" & txtTelNo.Text & "%' "
'                    strSearching &= "AND rcapplicant_master.present_tel_no LIKE '%" & txtTelNo.Text & "%' "
'                    'Sohail (05 Dec 2016) -- End
'                End If

'                If CInt(cboVacancy.SelectedValue) > 0 Then
'                    'Sohail (05 Dec 2016) -- Start
'                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                    'strSearching &= "AND mapvacancyunkid = " & CInt(cboVacancy.SelectedValue)
'                    strSearching &= "AND ISNULL(rcapp_vacancy_mapping.vacancyunkid, 0) = " & CInt(cboVacancy.SelectedValue)
'                    'Sohail (05 Dec 2016) -- End
'                End If

'                'S.SANDEEP [ 07 NOV 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If CInt(cboApplicantType.SelectedValue) > 0 Then
'                '    Select Case CInt(cboApplicantType.SelectedValue)
'                '        Case enVacancyType.EXTERNAL_VACANCY
'                '            strSearching &= "AND employeecode = '' "
'                '        Case enVacancyType.INTERNAL_VACANCY
'                '            strSearching &= "AND employeecode <> '' "
'                '    End Select
'                'End If

'                If CInt(cboApplicantType.SelectedValue) > 0 Then
'                    Select Case CInt(cboApplicantType.SelectedValue)
'                        Case enVacancyType.EXTERNAL_VACANCY
'                            'Sohail (05 Dec 2016) -- Start
'                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                            'strSearching &= "AND isexternalvacancy = 1 "
'                            strSearching &= "AND rcvacancy_master.isexternalvacancy = 1 "
'                            'Sohail (05 Dec 2016) -- End
'                        Case enVacancyType.INTERNAL_VACANCY
'                            'Sohail (05 Dec 2016) -- Start
'                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                            'strSearching &= "AND isexternalvacancy = 0 "
'                            strSearching &= "AND rcvacancy_master.isexternalvacancy = 0 "
'                            'Sohail (05 Dec 2016) -- End
'                    End Select
'                End If
'                'S.SANDEEP [ 07 NOV 2012 ] -- END

'                'S.SANDEEP [ 06 NOV 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If CInt(cboVacancyType.SelectedValue) > 0 Then
'                    Select Case CInt(cboVacancyType.SelectedValue)
'                        Case enVacancyType.EXTERNAL_VACANCY
'                            'Sohail (05 Dec 2016) -- Start
'                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                            'strSearching &= "AND isexternalvacancy = 1 "
'                            strSearching &= "AND rcvacancy_master.isexternalvacancy = 1 "
'                            'Sohail (05 Dec 2016) -- End
'                        Case enVacancyType.INTERNAL_VACANCY
'                            'Sohail (05 Dec 2016) -- Start
'                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                            'strSearching &= "AND isexternalvacancy = 0 "
'                            strSearching &= "AND rcvacancy_master.isexternalvacancy = 0 "
'                            'Sohail (05 Dec 2016) -- End
'                    End Select
'                End If
'                'S.SANDEEP [ 06 NOV 2012 ] -- END

'                'Sohail (05 Dec 2016) -- Start
'                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                'If strSearching.Trim.Length > 0 Then
'                '    strSearching = strSearching.Substring(3)
'                '    'S.SANDEEP [ 04 JULY 2012 ] -- START
'                '    'ENHANCEMENT : TRA CHANGES
'                '    'dtTable = New DataView(dsList.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
'                '    dtTable = New DataView(dsList.Tables(0), strSearching, "applicantname", DataViewRowState.CurrentRows).ToTable
'                '    'S.SANDEEP [ 04 JULY 2012 ] -- END
'                'Else
'                '    'S.SANDEEP [ 04 JULY 2012 ] -- START
'                '    'ENHANCEMENT : TRA CHANGES
'                '    'dtTable = dsList.Tables(0)
'                '    dtTable = New DataView(dsList.Tables(0), "", "applicantname", DataViewRowState.CurrentRows).ToTable
'                '    'S.SANDEEP [ 04 JULY 2012 ] -- END
'                'End If
'                If CInt(cboShortListType.SelectedIndex) = 1 Then
'                    dsList = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), True, strSearching)
'                ElseIf CInt(cboShortListType.SelectedIndex) = 2 Then
'                    dsList = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), False, strSearching)
'                Else
'                    dsList = objApplicantList.GetList("List", True, , strSearching)
'                End If
'                dtTable = New DataView(dsList.Tables(0), "", "applicantname", DataViewRowState.CurrentRows).ToTable
'                'Sohail (05 Dec 2016) -- End


'                RemoveHandler lvApplicantList.ItemChecked, AddressOf lvApplicantList_ItemChecked
'                RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

'                Dim lvItem As ListViewItem

'                lvApplicantList.Items.Clear()

'                For Each dtRow As DataRow In dtTable.Rows
'                    lvItem = New ListViewItem
'                    lvItem.Text = ""
'                    lvItem.SubItems.Add(dtRow.Item("applicantname").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("email").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("present_mobileno").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("present_tel_no").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("vacancy").ToString)
'                    If dtRow.Item("referenceno").ToString <> "" Then
'                        lvItem.SubItems.Add(clsCrypto.Dicrypt(dtRow.Item("referenceno").ToString))
'                    Else
'                        lvItem.SubItems.Add("")
'                    End If
'                    If dtRow.Item("employeecode").ToString.Trim.Length > 0 Then
'                        lvItem.ForeColor = objpnlInternalApplicantColor.BackColor
'                    End If

'                    'S.SANDEEP [ 02 MAY 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    lvItem.SubItems.Add(dtRow.Item("mapvacancyunkid").ToString)
'                    'S.SANDEEP [ 02 MAY 2012 ] -- END

'                    lvItem.Tag = dtRow.Item("applicantunkid")
'                    lvApplicantList.Items.Add(lvItem)

'                Next

'                If lvApplicantList.Items.Count > 16 Then
'                    colhApplicantName.Width = 170 - 20
'                Else
'                    colhApplicantName.Width = 190
'                End If

'                'S.SANDEEP [ 02 MAY 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'lvApplicantList.GroupingColumn = colhApplicantName
'                'lvApplicantList.DisplayGroups(True)

'                If mblnIsFromMail = False Then
'                    lvApplicantList.GroupingColumn = colhApplicantName
'                    lvApplicantList.DisplayGroups(True)
'                End If
'                'S.SANDEEP [ 02 MAY 2012 ] -- END

'                AddHandler lvApplicantList.ItemChecked, AddressOf lvApplicantList_ItemChecked
'                AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub GetCheckedData()
'        Try
'            If Me._DataView IsNot Nothing Then
'                If dsList.Tables.Count > 0 Then
'                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
'                        For j As Integer = 0 To lvApplicantList.Items.Count - 1
'                            If CInt(dsList.Tables(0).Rows(i)("EmpId")) = CInt(lvApplicantList.Items(j).Tag) Then
'                                lvApplicantList.Items(j).Checked = True

'                                'Pinkal (12-Oct-2011) -- Start

'                                'lvApplicantList.CheckItem(lvApplicantList.Items(j))
'                                'lvApplicantList.Invalidate(lvApplicantList.Items(j).Bounds)

'                                'Pinkal (12-Oct-2011) -- End

'                            End If
'                        Next
'                    Next
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetCheckedData", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (12-Oct-2011) -- Start

'    Private Sub GetApplicantEmail()
'        Try
'            Dim strNumber As String = ""
'            Dim objLetterFields As New clsLetterFields
'            mstrApplicant = New String() {}
'            Dim blnFlag As Boolean = False
'            Dim intCnt As Integer = 0
'            If lvApplicantList.CheckedItems.Count > 0 Then
'                ReDim mstrApplicant(lvApplicantList.CheckedItems.Count + 1)
'                For i As Integer = 0 To lvApplicantList.CheckedItems.Count - 1
'                    With lvApplicantList.CheckedItems(i)
'                        If .SubItems(colhemail.Index).Text <> "" Then
'                            mstrApplicant(intCnt) = .SubItems(colhApplicantName.Index).Text & " " & "<" & .SubItems(colhemail.Index).Text & ">"
'                            If strNumber = "" Then
'                                strNumber = .Tag.ToString
'                            Else
'                                strNumber = strNumber & " , " & .Tag.ToString
'                            End If
'                            intCnt += 1
'                        Else
'                            If blnFlag = False Then
'                                Dim strMsg As String = Language.getMessage(mstrModuleName, 2, "Some of the email address(s) are blank. Do you want to continue?")
'                                If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                                    blnFlag = True
'                                    Continue For
'                                Else
'                                    blnFlag = False
'                                    Exit Sub
'                                End If
'                            End If
'                        End If
'                    End With
'                Next
'                If strNumber <> "" Then
'                    'S.SANDEEP [ 02 MAY 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    objLetterFields._VacancyUnkid = CInt(lvApplicantList.CheckedItems(0).SubItems(objcolhVacId.Index).Text)
'                    'S.SANDEEP [ 02 MAY 2012 ] -- END

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module)
'                    dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                End If
'                Me.Close()
'                'Else
'                '    If lvApplicantList.SelectedIndices.Count > 0 Then
'                '        dsList = objLetterFields.GetEmployeeData(CStr(lvApplicantList.LVItems(lvApplicantList.SelectedIndices.Item(0)).Tag), enImg_Email_RefId.Employee_Module)
'                '        Me.Close()
'                '    End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetApplicantEmail", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetApplicantPrint_Export_Letter()
'        Try
'            Dim strNumber As String = ""
'            Dim objLetterFields As New clsLetterFields
'            mstrApplicant = New String() {}
'            Dim blnFlag As Boolean = False
'            Dim intCnt As Integer = 0
'            If lvApplicantList.CheckedItems.Count > 0 Then
'                ReDim mstrApplicant(lvApplicantList.CheckedItems.Count + 1)
'                For i As Integer = 0 To lvApplicantList.CheckedItems.Count - 1
'                    With lvApplicantList.CheckedItems(i)
'                        If .Tag.ToString() <> "" Then
'                            mstrApplicant(intCnt) = .SubItems(colhApplicantName.Index).Text
'                            If strNumber = "" Then
'                                strNumber = .Tag.ToString
'                                mstrApplicantId = .Tag.ToString()
'                            Else
'                                strNumber = strNumber & " , " & .Tag.ToString
'                                mstrApplicantId = mstrApplicantId & " , " & .Tag.ToString
'                            End If
'                            intCnt += 1
'                        End If
'                    End With
'                Next
'                If strNumber <> "" Then
'                    'S.SANDEEP [ 02 MAY 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    objLetterFields._VacancyUnkid = CInt(lvApplicantList.CheckedItems(0).SubItems(objcolhVacId.Index).Text)
'                    'S.SANDEEP [ 02 MAY 2012 ] -- END

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module)
'                    dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                End If
'                Me.Close()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetApplicantPrint_Export_Letter", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (12-Oct-2011) -- End

'    'S.SANDEEP [ 25 DEC 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub FillCombo()
'        Dim dsCombo As New DataSet
'        Dim objVacancy As New clsVacancy
'        Try
'            dsCombo = objApplicantList.GetApplicantList("list", True, , True)
'            With cboApplicant
'                .ValueMember = "applicantunkid"
'                .DisplayMember = "applicantname"
'                .DataSource = dsCombo.Tables("list")
'                .SelectedValue = 0
'            End With

'            dsCombo = objVacancy.getVacancyType
'            With cboVacancyType
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables(0)
'                .SelectedValue = 0
'            End With

'            dsCombo = objVacancy.getVacancyType
'            With cboApplicantType
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables(0)
'                .SelectedValue = 0
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            dsCombo.Dispose() : objVacancy = Nothing
'        End Try
'    End Sub

'    Private Sub SetVisibility()
'        Try
'            btnNew.Enabled = User._Object.Privilege._AllowtoAddApplicant
'            btnEdit.Enabled = User._Object.Privilege._AllowtoEditApplicant
'            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteApplicant
'            'S.SANDEEP [ 28 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'btnScan.Enabled = User._Object.Privilege._AllowtoAttachApplicantDocuments
'            mnuScan_Documents.Enabled = User._Object.Privilege._AllowtoAttachApplicantDocuments
'            'S.SANDEEP [ 28 FEB 2012 ] -- END


'            'Anjan (25 Oct 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
'            mnuMapVacancy.Enabled = User._Object.Privilege._AllowtoMapVacancy
'            'Anjan (25 Oct 2012)-End 

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 25 DEC 2011 ] -- END



'    'Pinkal (07-Jan-2012) -- Start
'    'Enhancement : TRA Changes



'    'S.SANDEEP [ 02 MAY 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Private Sub ShortListedApplicant()
'    '    Try
'    '        Dim dsApplicant As DataSet = Nothing
'    '        Dim strSearching As String = ""
'    '        Dim dtTable As DataTable = Nothing

'    '        Dim objShortListApplicant As New clsshortlist_finalapplicant
'    '        If CInt(cboShortListType.SelectedIndex) = 1 Then
'    '            dsApplicant = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), True)
'    '        ElseIf CInt(cboShortListType.SelectedIndex) = 2 Then
'    '            dsApplicant = objShortListApplicant.GetShortListApplicantList(CInt(cboVacancy.SelectedValue), False)
'    '        End If

'    '        If CInt(cboApplicant.SelectedValue) > 0 Then
'    '            strSearching &= "AND applicantunkid = '" & CInt(cboApplicant.SelectedValue) & "' "
'    '        End If


'    '        If txtEmail.Text.Trim.Length > 0 Then
'    '            strSearching &= "AND email LIKE '%" & txtEmail.Text & "%'" & " "
'    '        End If

'    '        If txtMobile.Text.Trim.Length > 0 Then
'    '            strSearching &= "AND present_mobileno LIKE '%" & txtMobile.Text & "%' "
'    '        End If

'    '        If txtReferenceNo.Text.Trim.Length > 0 Then
'    '            strSearching &= "AND othername LIKE '%" & txtReferenceNo.Text & "%'" & " "
'    '        End If

'    '        If txtTelNo.Text.Trim.Length > 0 Then
'    '            strSearching &= "AND present_tel_no LIKE '%" & txtTelNo.Text & "%' "
'    '        End If

'    '        If CInt(cboVacancy.SelectedValue) > 0 Then
'    '            strSearching &= "AND vacancyunkid = " & CInt(cboVacancy.SelectedValue)
'    '        End If

'    '        If chkIncludeInternalApplicant.CheckState = CheckState.Unchecked Then
'    '            strSearching &= "AND employeecode = '' "
'    '        End If

'    '        If strSearching.Trim.Length > 0 Then
'    '            strSearching = strSearching.Substring(3)
'    '            dtTable = New DataView(dsApplicant.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
'    '        Else
'    '            dtTable = dsApplicant.Tables(0)
'    '        End If


'    '        RemoveHandler lvApplicantList.ItemChecked, AddressOf lvApplicantList_ItemChecked
'    '        RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

'    '        Dim lvItem As ListViewItem

'    '        lvApplicantList.Items.Clear()

'    '        For Each dtRow As DataRow In dtTable.Rows
'    '            lvItem = New ListViewItem
'    '            lvItem.Text = ""
'    '            lvItem.SubItems.Add(dtRow.Item("applicantname").ToString)
'    '            lvItem.SubItems.Add(dtRow.Item("email").ToString)
'    '            lvItem.SubItems.Add(dtRow.Item("present_mobileno").ToString)
'    '            lvItem.SubItems.Add(dtRow.Item("present_tel_no").ToString)
'    '            lvItem.Tag = dtRow.Item("applicantunkid")
'    '            lvItem.SubItems.Add(dtRow.Item("vacancy").ToString)

'    '            If chkIncludeInternalApplicant.CheckState = CheckState.Checked Then
'    '                If dtRow.Item("employeecode").ToString.Trim.Length > 0 Then
'    '                    lvItem.ForeColor = objpnlInternalApplicantColor.BackColor
'    '                End If
'    '            End If
'    '            lvApplicantList.Items.Add(lvItem)
'    '        Next

'    '        If lvApplicantList.Items.Count > 16 Then
'    '            colhApplicantName.Width = 170 - 18
'    '        Else
'    '            colhApplicantName.Width = 170
'    '        End If

'    '        AddHandler lvApplicantList.ItemChecked, AddressOf lvApplicantList_ItemChecked
'    '        AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "ShortListedApplicant", mstrModuleName)
'    '    End Try
'    'End Sub
'    'S.SANDEEP [ 02 MAY 2012 ] -- END




'    'Pinkal (07-Jan-2012) -- End


'#End Region

'#Region " Form's Event "
'    Private Sub frmApplicantList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objApplicantList = Nothing
'    End Sub

'    Private Sub frmApplicantList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        If Asc(e.KeyChar) = 27 Then
'            Me.Close()
'        ElseIf Asc(e.KeyChar) = 13 Then
'            SendKeys.Send("{TAB}")
'            e.Handled = True
'        End If
'    End Sub

'    Private Sub frmApplicantList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
'        If e.KeyCode = Keys.Delete And lvApplicantList.Focused = True Then
'            Call btnDelete.PerformClick()
'        End If
'    End Sub

'    Private Sub frmApplicantList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objApplicantList = New clsApplicant_master
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Call Language.setLanguage(Me.Name)
'            'Sohail (11 Sep 2010) -- Start
'            Call SetColor()
'            'Sohail (11 Sep 2010) -- End

'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Call FillCombo()
'            'S.SANDEEP [ 25 DEC 2011 ] -- END


'            'Anjan (15 Oct 2012)-Start
'            'Issue : need to ask sandeep for this comment that now on operation button map vacancy and scan/attachment menu are there so what is the reason of below commnet now.
'            'If ConfigParameter._Object._WebClientCode > 0 And ConfigParameter._Object._AuthenticationCode <> "" Then
'            '    btnOperation.Visible = True
'            'Else
'            '    btnOperation.Visible = False
'            'End If
'            'Anjan (15 Oct 2012)-End 



'            'Pinkal (12-Oct-2011) -- Start

'            Dim p As New Point()

'            If mblnIsFromMail = True Then
'                lvApplicantList.CheckBoxes = True
'                objcolhCheck.Width = 25
'                objChkAll.Visible = True


'                'Pinkal (07-Jan-2012) -- Start
'                'Enhancement : TRA Changes

'                objChkAll.BringToFront()

'                lblShortListType.Visible = True
'                cboShortListType.Visible = True
'                cboShortListType.BackColor = GUI.ColorOptional
'                cboShortListType.Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
'                cboShortListType.Items.Add(Language.getMessage(mstrModuleName, 5, "Final Short Listed Applicant"))
'                cboShortListType.Items.Add(Language.getMessage(mstrModuleName, 6, "Non Short Listed Applicant"))
'                cboShortListType.SelectedIndex = 0

'                p.X = 750
'                p.Y = 141
'                gbFilterCriteria.Size = CType(p, Size)

'                p.X = 751
'                p.Y = 273
'                lvApplicantList.Size = CType(p, Size)

'                p.X = 14
'                p.Y = 211

'                lvApplicantList.Location = p
'                objChkAll.Location = New Point(23, 215)
'                'Pinkal (07-Jan-2012) -- End

'            Else

'                'Pinkal (07-Jan-2012) -- Start
'                'Enhancement : TRA Changes


'                p.X = 750
'                p.Y = 115
'                gbFilterCriteria.Size = CType(p, Size)

'                p.X = 751
'                p.Y = 297
'                lvApplicantList.Size = CType(p, Size)

'                'Pinkal (07-Jan-2012) -- End


'                lvApplicantList.CheckBoxes = False
'                objChkAll.Visible = False
'                objcolhCheck.Width = 0
'                colhApplicantName.Width = colhApplicantName.Width + 25
'            End If
'            'Pinkal (12-Oct-2011) -- End

'            If Me._DataView.Tables.Count <> 0 Then
'                Call GetCheckedData()
'            End If

'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            chkIncludeInternalApplicant.Checked = False
'            objpnlInternalApplicantColor.Visible = False
'            'S.SANDEEP [ 25 DEC 2011 ] -- END

'            If lvApplicantList.Items.Count > 0 Then lvApplicantList.Items(0).Selected = True
'            lvApplicantList.Select()


'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Call SetVisibility()
'            'S.SANDEEP [ 25 DEC 2011 ] -- END

'            'S.SANDEEP [ 17 AUG 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If User._Object.Privilege._AllowToViewApplicantReferenceNo = False Then
'                colhVacancy.Width = 120 + 110
'                colhRefNo.Width = 0
'            Else
'                colhVacancy.Width = 120
'                colhRefNo.Width = 110
'            End If
'            'S.SANDEEP [ 17 AUG 2012 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmApplicantList_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()
'            clsApplicant_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsApplicant_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Button Events "
'    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click

'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Dim frm As New frmApplicantMaster
'        Try
'            'If User._Object._RightToLeft = True Then
'            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'            '    frm.RightToLeftLayout = True
'            '    Call Language.ctlRightToLeftlayOut(frm)
'            'End If
'            If frm.displayDialog(-1, enAction.ADD_ONE) Then
'                Call FillList()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

'        'Pinkal (12-Oct-2011) -- Start

'        If dgvApplicantList.SelectedRows.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation."), enMsgBoxStyle.Information)
'            lvApplicantList.Select()
'            Exit Sub
'        End If
'        Dim frm As New frmApplicantMaster
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvApplicantList.SelectedItems(0).Index

'            'If User._Object._RightToLeft = True Then
'            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'            '    frm.RightToLeftLayout = True
'            '    Call Language.ctlRightToLeftlayOut(frm)
'            'End If

'            'If frm.displayDialog(CInt(lvApplicantList.LVItems(intSelectedIndex).Tag), enAction.EDIT_ONE) Then
'            '    Call FillList()
'            'End If

'            'S.SANDEEP [ 06 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If frm.displayDialog(CInt(lvApplicantList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
'            '    Call FillList()
'            'End If

'            If frm.displayDialog(CInt(lvApplicantList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvApplicantList.SelectedItems(0).SubItems(objcolhVacId.Index).Text)) Then
'                Call FillList()
'            End If
'            'S.SANDEEP [ 06 NOV 2012 ] -- END

'            frm = Nothing

'            lvApplicantList.Items(intSelectedIndex).Selected = True
'            lvApplicantList.EnsureVisible(intSelectedIndex)
'            lvApplicantList.Select()

'            'Pinkal (12-Oct-2011) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

'        'Pinkal (12-Oct-2011) -- Start

'        If dgvApplicantList.SelectedRows.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation."), enMsgBoxStyle.Information)
'            lvApplicantList.Select()
'            Exit Sub
'        End If
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvApplicantList.SelectedItems(0).Index


'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Are you sure you want to delete this Applicant?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'                'objApplicantList.Delete(CInt(lvApplicantList.LVItems(intSelectedIndex).Tag))
'                If objApplicantList.Delete(CInt(lvApplicantList.SelectedItems(0).Tag)) Then
'                    lvApplicantList.SelectedItems(0).Remove()

'                    If objApplicantList._Message <> "" Then
'                        eZeeMsgBox.Show(objApplicantList._Message, enMsgBoxStyle.Information)
'                    Else
'                        Call FillList()
'                    End If

'                    If lvApplicantList.Items.Count <= 0 Then
'                        Exit Try
'                    End If

'                    If lvApplicantList.Items.Count = intSelectedIndex Then
'                        intSelectedIndex = lvApplicantList.Items.Count - 1
'                        lvApplicantList.Items(intSelectedIndex).Selected = True
'                        lvApplicantList.EnsureVisible(intSelectedIndex)
'                    ElseIf lvApplicantList.Items.Count <> 0 Then
'                        lvApplicantList.Items(intSelectedIndex).Selected = True
'                        lvApplicantList.EnsureVisible(intSelectedIndex)
'                    End If

'                Else
'                    If objApplicantList._Message <> "" Then
'                        eZeeMsgBox.Show(objApplicantList._Message, enMsgBoxStyle.Information)
'                    End If

'                End If

'            End If

'            lvApplicantList.Select()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'        'Pinkal (12-Oct-2011) -- End
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click

'        Try
'            txtEmail.Text = ""
'            txtFirstname.Text = ""
'            txtMobile.Text = ""
'            txtReferenceNo.Text = ""
'            txtSurname.Text = ""
'            txtTelNo.Text = ""
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            cboApplicant.SelectedValue = 0
'            'chkIncludeInternalApplicant.CheckState = CheckState.Unchecked
'            cboApplicantType.SelectedValue = 0
'            objpnlInternalApplicantColor.Visible = True
'            'S.SANDEEP [ 25 DEC 2011 ] -- END


'            'Pinkal (07-Jan-2012) -- Start
'            'Enhancement : TRA Changes
'            If mblnIsFromMail Then
'                cboShortListType.SelectedIndex = 0
'            End If
'            'Pinkal (07-Jan-2012) -- End

'            'Anjan (02 Mar 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
'            cboVacancyType.SelectedValue = 0
'            cboVacancy.SelectedValue = 0
'            'Anjan (02 Mar 2012)-End 



'            'S.SANDEEP [ 02 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mblnIsFromMail = False Then
'                Call FillList()
'            Else
'                lvApplicantList.Items.Clear()
'            End If
'            'S.SANDEEP [ 02 MAY 2012 ] -- END


'            'Pinkal (18-Jul-2012) -- Start
'            'Enhancement : TRA Changes
'            Call objbtnSearch.ShowResult(CStr(lvApplicantList.Items.Count))
'            'Pinkal (18-Jul-2012) -- End


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try

'            'Pinkal (07-Jan-2012) -- Start
'            'Enhancement : TRA Changes

'            objChkAll.Checked = False

'            If mblnIsFromMail Then
'                'S.SANDEEP [ 02 MAY 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If CInt(cboShortListType.SelectedIndex) = 0 Then
'                '    FillList()
'                'ElseIf CInt(cboShortListType.SelectedIndex) = 1 Or CInt(cboShortListType.SelectedIndex) = 2 Then
'                '    ShortListedApplicant()
'                'End If

'                If CInt(cboVacancy.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Vacancy to continue."), enMsgBoxStyle.Information)
'                    cboVacancy.Focus()
'                    Exit Sub
'                End If

'                FillList()
'                'S.SANDEEP [ 02 MAY 2012 ] -- END
'            Else
'                Call FillList()
'            End If

'            'Pinkal (07-Jan-2012) -- End


'            'Pinkal (18-Jul-2012) -- Start
'            'Enhancement : TRA Changes
'            Call objbtnSearch.ShowResult(CStr(lvApplicantList.Items.Count))
'            'Pinkal (18-Jul-2012) -- End


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnMailClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMailClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnMailClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (12-Oct-2011) -- Start

'    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
'        Try
'            If mblnExport_Print = False Then
'                GetApplicantEmail()
'            Else
'                GetApplicantPrint_Export_Letter()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Pinkal (12-Oct-2011) -- End



'    'S.SANDEEP [ 25 DEC 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            With frm
'                .ValueMember = cboApplicant.ValueMember
'                .DisplayMember = cboApplicant.DisplayMember
'                .CodeMember = "applicant_code"
'                .DataSource = CType(cboApplicant.DataSource, DataTable)
'            End With

'            If frm.DisplayDialog Then
'                cboApplicant.SelectedValue = frm.SelectedValue
'                cboApplicant.Focus()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'S.SANDEEP [ 25 DEC 2011 ] -- END


'#End Region

'#Region " Controls "

'    'S.SANDEEP [ 18 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES

'    'Private Sub mnuScanCV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanCV.Click

'    '    'Pinkal (12-Oct-2011) -- Start

'    '    If dgvApplicantList.SelectedRows.Count <= 0 Then
'    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation on it."), enMsgBoxStyle.Information)
'    '        lvApplicantList.Select()
'    '        Exit Sub
'    '    End If

'    '    Try
'    '        Dim ObjFrm As New frmScanOrAttachmentInfo


'    '        ObjFrm._ApplicantUnkid = CInt(lvApplicantList.SelectedItems(0).Tag)
'    '        ObjFrm._ApplicantName = lvApplicantList.SelectedItems(0).SubItems(colhApplicantName.Index).Text
'    '        ObjFrm._ActionTag = CInt(lvApplicantList.SelectedItems(0).Tag)

'    '        'ObjFrm._ApplicantUnkid = CInt(lvApplicantList.LVItems(intSelectedIndex).Tag)
'    '        'ObjFrm._ApplicantName = lvApplicantList.LVItems(intSelectedIndex).SubItems(colhApplicantName.Index).Text
'    '        'ObjFrm._ActionTag = CInt(lvApplicantList.LVItems(intSelectedIndex).Tag)

'    '        ObjFrm._IsComboVisible = False
'    '        ObjFrm._RefId = enScanAttactRefId.APPLICANT_CV
'    '        ObjFrm._TextValue = Language.getMessage(mstrModuleName, 3, "Applicant CV")
'    '        ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 4, "Curriculum vitae")

'    '        ObjFrm.ShowDialog()

'    '        'Pinkal (12-Oct-2011) -- End

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuScanCV_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub mnuScanQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanQualification.Click

'    '    'Pinkal (12-Oct-2011) -- Start

'    '    If dgvApplicantList.SelectedRows.Count <= 0 Then
'    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation on it."), enMsgBoxStyle.Information)
'    '        lvApplicantList.Select()
'    '        Exit Sub
'    '    End If

'    '    Dim ObjFrm As New frmScanOrAttachmentInfo
'    '    Dim dsList As New DataSet
'    '    Try

'    '        'dsList = objApplicantList.GetApplicantQualification(CInt(lvApplicantList.LVItems(intSelectedIndex).Tag), False, "Qualification")
'    '        dsList = objApplicantList.GetApplicantQualification(CInt(lvApplicantList.SelectedItems(0).Tag), False, "Qualification")

'    '        If dsList.Tables(0).Rows.Count > 0 Then
'    '            ObjFrm._Dataset = dsList
'    '            ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 6, "Select Qualification")
'    '            ObjFrm._RefId = enScanAttactRefId.APPLICANT_QUALIFICATION
'    '            ObjFrm._ApplicantUnkid = CInt(lvApplicantList.SelectedItems(0).Tag)
'    '            ObjFrm._ApplicantName = lvApplicantList.SelectedItems(0).SubItems(colhApplicantName.Index).Text

'    '            'ObjFrm._ApplicantUnkid = CInt(lvApplicantList.LVItems(intSelectedIndex).Tag)
'    '            'ObjFrm._ApplicantName = lvApplicantList.LVItems(intSelectedIndex).SubItems(colhApplicantName.Index).Text

'    '            ObjFrm._IsComboVisible = True
'    '            ObjFrm.ShowDialog()
'    '        End If

'    '        'Pinkal (12-Oct-2011) -- End

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuScanQualification_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub mnuScanJobDocuments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanJobDocuments.Click

'    '    'Pinkal (12-Oct-2011) -- Start

'    '    If dgvApplicantList.SelectedRows.Count <= 0 Then
'    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation on it."), enMsgBoxStyle.Information)
'    '        lvApplicantList.Select()
'    '        Exit Sub
'    '    End If
'    '    'Sandeep [ 23 Oct 2010 ] -- End 

'    '    Dim ObjFrm As New frmScanOrAttachmentInfo
'    '    Dim dsList As New DataSet

'    '    Try


'    '        dsList = objApplicantList.GetApplicantJobHistory(CInt(lvApplicantList.SelectedItems(0).Tag), False, "EXP")

'    '        If dsList.Tables(0).Rows.Count > 0 Then
'    '            ObjFrm._Dataset = dsList
'    '            ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 7, "Select Company")
'    '            ObjFrm._RefId = enScanAttactRefId.APPLICANT_JOBHISTORY

'    '            ObjFrm._ApplicantUnkid = CInt(lvApplicantList.SelectedItems(0).Tag)
'    '            ObjFrm._ApplicantName = lvApplicantList.SelectedItems(0).SubItems(colhApplicantName.Index).Text

'    '            'ObjFrm._ApplicantUnkid = CInt(lvApplicantList.LVItems(intSelectedIndex).Tag)
'    '            'ObjFrm._ApplicantName = lvApplicantList.LVItems(intSelectedIndex).SubItems(colhApplicantName.Index).Text
'    '            ObjFrm._IsComboVisible = True
'    '            ObjFrm.ShowDialog()
'    '        End If

'    '        'Pinkal (12-Oct-2011) -- End

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuScanJobDocuments_Click", mstrModuleName)
'    '    End Try
'    'End Sub
'    'S.SANDEEP [ 18 FEB 2012 ] -- END







'    'S.SANDEEP [ 25 DEC 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
'        Try
'            If CInt(cboVacancyType.SelectedValue) >= 0 Then
'                Dim dsVacList As New DataSet
'                Dim objVac As New clsVacancy

'                'Shani(24-Aug-2015) -- Start
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
'                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
'                'Shani(24-Aug-2015) -- End

'                With cboVacancy
'                    .ValueMember = "id"
'                    .DisplayMember = "name"
'                    .DataSource = dsVacList.Tables("List")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub cboApplicantType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApplicantType.SelectedIndexChanged
'        Try
'            If CInt(cboApplicantType.SelectedValue) > 0 Then
'                Select Case CInt(cboApplicantType.SelectedValue)
'                    Case enVacancyType.EXTERNAL_VACANCY
'                        objpnlInternalApplicantColor.Visible = False
'                    Case enVacancyType.INTERNAL_VACANCY
'                        objpnlInternalApplicantColor.Visible = True
'                    Case Else
'                        objpnlInternalApplicantColor.Visible = True
'                End Select
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboApplicantType_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 25 DEC 2011 ] -- END



'    'S.SANDEEP [ 28 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub mnuScan_Documents_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScan_Documents.Click
'        Dim frm As New frmScanOrAttachmentInfo
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Applicant"), enImg_Email_RefId.Applicant_Module, enAction.ADD_ONE, "", mstrModuleName) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnScan_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub mnuMapVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMapVacancy.Click
'        Dim frm As New frmVacancyMapping
'        Try
'            If dgvApplicantList.SelectedRows.Count <= 0 Then Exit Sub

'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If frm.displayDialog(CInt(lvApplicantList.SelectedItems(0).Tag), lvApplicantList.SelectedItems(0).SubItems(colhApplicantName.Index).Text) Then
'                Call FillList()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuMapVacancy_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'S.SANDEEP [ 28 FEB 2012 ] -- END


'    'Sohail (29 May 2015) -- Start
'    'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
'    Private Sub mnuPreviewAttachments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreviewAttachments.Click
'        Dim frm As New frmScanAttachmentList
'        Try
'            If dgvApplicantList.SelectedRows.Count <= 0 Then Exit Sub

'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.displayDialog(enScanAttactRefId.QUALIFICATIONS, CInt(lvApplicantList.SelectedItems(0).Tag), True)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuPreviewAttachments_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'Sohail (29 May 2015) -- End


'#End Region

'    'Pinkal (12-Oct-2011) -- Start

'#Region "CheckBox Event"

'    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
'        Try
'            RemoveHandler lvApplicantList.ItemChecked, AddressOf lvApplicantList_ItemChecked
'            For Each lvItem As ListViewItem In lvApplicantList.Items
'                lvItem.Checked = CBool(objChkAll.CheckState)
'            Next
'            AddHandler lvApplicantList.ItemChecked, AddressOf lvApplicantList_ItemChecked
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [ 25 DEC 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub chkIncludeInternalApplicant_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeInternalApplicant.CheckedChanged
'        Try
'            If chkIncludeInternalApplicant.CheckState = CheckState.Checked Then
'                objpnlInternalApplicantColor.Visible = True
'            Else
'                objpnlInternalApplicantColor.Visible = False
'            End If
'            'Call FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkIncludeInternalApplicant_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 25 DEC 2011 ] -- END

'#End Region

'#Region "ContextMenu Event"

'    Private Sub cmuExportWeb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Try
'            Dim startdate As DateTime = Now
'            'Call WebConnection(False)
'            If CBool(eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You are about to Export data to Web,this process will take some time depending on Internet bandwidth. Do you want to continue?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes) Then

'                'Shani(24-Aug-2015) -- Start
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'objApplicantList.Synchronize_Data(False)
'                'Sohail (05 Dec 2016) -- Start
'                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'                'objApplicantList.Synchronize_Data(False, ConfigParameter._Object._ApplicantCodeNotype, _
'                '                                  ConfigParameter._Object._ApplicantCodePrifix, _
'                '                                  ConfigParameter._Object._DatabaseServer, _
'                '                                  ConfigParameter._Object._DatabaseServerSetting, _
'                '                                  ConfigParameter._Object._AuthenticationCode, _
'                '                                  ConfigParameter._Object._DatabaseName, _
'                '                                  ConfigParameter._Object._DatabaseUserName, _
'                '                                  ConfigParameter._Object._DatabaseUserPassword, _
'                '                                  ConfigParameter._Object._DatabaseOwner, _
'                '                                  ConfigParameter._Object._WebClientCode, _
'                '                                  Company._Object._ConfigDatabaseName, _
'                '                                  ConfigParameter._Object._IsImgInDataBase, _
'                '                                  ConfigParameter._Object._PhotoPath, _
'                '                                  ConfigParameter._Object._Document_Path, _
'                '                                  User._Object._Userunkid, _
'                '                                  ConfigParameter._Object._ArutiSelfServiceURL, _
'                '                                  ConfigParameter._Object._ApplicantDeclaration, _
'                '                                  ConfigParameter._Object._TimeZoneMinuteDifference, _
'                '                                  ConfigParameter._Object._QualificationCertificateAttachmentMandatory, _
'                '                                  ConfigParameter._Object._CurrentDateAndTime, _
'                '                                  Company._Object._Code, _
'                '                                  Company._Object._Companyunkid)
'                objApplicantList.Synchronize_Data(False, ConfigParameter._Object._ApplicantCodeNotype, _
'                                                  ConfigParameter._Object._ApplicantCodePrifix, _
'                                                  ConfigParameter._Object._DatabaseServer, _
'                                                  ConfigParameter._Object._DatabaseServerSetting, _
'                                                  ConfigParameter._Object._AuthenticationCode, _
'                                                  ConfigParameter._Object._DatabaseName, _
'                                                  ConfigParameter._Object._DatabaseUserName, _
'                                                  ConfigParameter._Object._DatabaseUserPassword, _
'                                                  ConfigParameter._Object._DatabaseOwner, _
'                                                  ConfigParameter._Object._WebClientCode, _
'                                                  Company._Object._ConfigDatabaseName, _
'                                                  ConfigParameter._Object._IsImgInDataBase, _
'                                                  ConfigParameter._Object._PhotoPath, _
'                                                  ConfigParameter._Object._Document_Path, _
'                                                  User._Object._Userunkid, _
'                                                  ConfigParameter._Object._ArutiSelfServiceURL, _
'                                                  ConfigParameter._Object._ApplicantDeclaration, _
'                                                  ConfigParameter._Object._TimeZoneMinuteDifference, _
'                                                  ConfigParameter._Object._QualificationCertificateAttachmentMandatory, _
'                                                  ConfigParameter._Object._CurrentDateAndTime, _
'                                                  Company._Object._Code, _
'                                                  Company._Object._Companyunkid, _
'                                                  ConfigParameter._Object._CompanyDateFormat, _
'                                                  ConfigParameter._Object._CompanyDateSeparator, _
'                                                  ConfigParameter._Object._AdminEmail, _
'                                                  ConfigParameter._Object._MiddleNameMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._GenderMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._BirthDateMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._Address1MandatoryInRecruitment, _
'                                                  ConfigParameter._Object._Address2MandatoryInRecruitment, _
'                                                  ConfigParameter._Object._CountryMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._StateMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._CityMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._PostCodeMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._MaritalStatusMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._Language1MandatoryInRecruitment, _
'                                                  ConfigParameter._Object._NationalityMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._OneSkillMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._OneQualificationMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._OneJobExperienceMandatoryInRecruitment, _
'                                                  ConfigParameter._Object._OneReferenceMandatoryInRecruitment _
'                                                  )
'                'Sohail (05 Dec 2016) -- End
'                'Shani(24-Aug-2015) -- End

'                Me.Text = DateDiff(DateInterval.Second, Now, startdate).ToString
'                eZeeMsgBox.Show(objApplicantList._Message, enMsgBoxStyle.Information)
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cmuExportWeb_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Sohail (05 Dec 2016) -- Start
'    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
'    'Private Sub cmuImportWeb_Click(ByVal sender As Object, ByVal e As System.EventArgs)
'    '    Try
'    '        'Call WebConnection(True)
'    '        If CBool(eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You are about to Import data from Web,this process will take some time depending upon Internet bandwidth. Do you want to continue?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes) Then
'    '            Dim starttime As DateTime = Now

'    '            'Shani(24-Aug-2015) -- Start
'    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '            'objApplicantList.Synchronize_Data(True)
'    '            objApplicantList.Synchronize_Data(True, ConfigParameter._Object._ApplicantCodeNotype, _
'    '                                              ConfigParameter._Object._ApplicantCodePrifix, _
'    '                                              ConfigParameter._Object._DatabaseServer, _
'    '                                              ConfigParameter._Object._DatabaseServerSetting, _
'    '                                              ConfigParameter._Object._AuthenticationCode, _
'    '                                              ConfigParameter._Object._DatabaseName, _
'    '                                              ConfigParameter._Object._DatabaseUserName, _
'    '                                              ConfigParameter._Object._DatabaseUserPassword, _
'    '                                              ConfigParameter._Object._DatabaseOwner, _
'    '                                              ConfigParameter._Object._WebClientCode, _
'    '                                              Company._Object._ConfigDatabaseName, _
'    '                                              ConfigParameter._Object._IsImgInDataBase, _
'    '                                              ConfigParameter._Object._PhotoPath, _
'    '                                              ConfigParameter._Object._Document_Path, _
'    '                                              User._Object._Userunkid, _
'    '                                              ConfigParameter._Object._ArutiSelfServiceURL, _
'    '                                              ConfigParameter._Object._ApplicantDeclaration, _
'    '                                              ConfigParameter._Object._TimeZoneMinuteDifference, _
'    '                                              ConfigParameter._Object._QualificationCertificateAttachmentMandatory, _
'    '                                              ConfigParameter._Object._CurrentDateAndTime, _
'    '                                              Company._Object._Code, _
'    '                                              Company._Object._Companyunkid)
'    '            'Shani(24-Aug-2015) -- End

'    '            eZeeMsgBox.Show(objApplicantList._Message, enMsgBoxStyle.Information, (Now - starttime).TotalSeconds.ToString)
'    '            Call FillList()
'    '        End If

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cmuImportWeb_Click", mstrModuleName)
'    '    End Try
'    'End Sub
'    'Sohail (05 Dec 2016) -- End
'#End Region

'#Region "ListView Event"

'    Private Sub lvApplicantList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvApplicantList.ItemChecked
'        Try
'            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
'            If lvApplicantList.CheckedItems.Count <= 0 Then
'                objChkAll.CheckState = CheckState.Unchecked
'            ElseIf lvApplicantList.CheckedItems.Count < lvApplicantList.Items.Count Then
'                objChkAll.CheckState = CheckState.Indeterminate
'            ElseIf lvApplicantList.CheckedItems.Count = lvApplicantList.Items.Count Then
'                objChkAll.CheckState = CheckState.Checked
'            End If
'            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvApplicantList_ItemChecked", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'    'Pinkal (12-Oct-2011) -- End


'    'Private Sub WebConnection(ByVal IsImport As Boolean)
'    '    Dim ds As DataSet
'    '    Dim objDataoperation As New clsDataOperation
'    '    Dim msql As String
'    '    Dim mlinkweb As String = ""
'    '    Dim mLink As String = ""
'    '    'Dim mdatabaseserver As String, mdatabasename As String, mdatabaseuser As String, mdatabasepassword As String, mDatabaseOwner As String,
'    '    Dim mdatabaseweb As String, mdatabaseserverweb As String, mdatabaseuserweb As String, mdatabasepasswordweb As String, mDatabaseOwnerweb As String
'    '    Try
'    '        Dim objNetCon As New clsNetConnectivity
'    '        If objNetCon._Conected = False Then
'    '            eZeeMsgBox.Show("Internet Connection : " & objNetCon._ConnectionStatus, enMsgBoxStyle.Critical)
'    '    Exit Sub
'    'End If

'    '        'msql = "select * from sys.servers where is_linked = 1  "
'    '        'ds = objDataoperation.ExecQuery(msql, "servers")

'    '        'If ds.Tables(0).Rows.Count <= 0 Then
'    '        '    eZeeMsgBox.Show("Web server is not connected with your database.")
'    '        '    Exit Sub
'    '        'End If


'    '        mdatabaseserverweb = "Anjan\APAYROLL"
'    '        mdatabaseweb = "Web_tran"
'    '        mdatabaseuserweb = "anjan"
'    '        mdatabasepasswordweb = "anjan123"
'    '        mDatabaseOwnerweb = "dbo"

'    '        mlinkweb = "[" & mdatabaseserverweb & "]" & ".[" & mdatabaseweb & "].[" & mDatabaseOwnerweb & "]."

'    '        Dim StrCode As String = Company._Object._Code
'    '        Dim intClientCode As Integer = ConfigParameter._Object._WebClientCode
'    '        If IsImport = False Then

'    '            If ExportToWeb(mlinkweb, StrCode, intClientCode) = False Then
'    '                eZeeMsgBox.Show("Data Exported process failed.", enMsgBoxStyle.Information)
'    '            Else
'    '                eZeeMsgBox.Show("Data Exported Successfully.", enMsgBoxStyle.Information)
'    '            End If
'    '        Else
'    '            If ImportFromWeb(mlinkweb, StrCode, intClientCode, mdatabaseserverweb) = False Then
'    '                eZeeMsgBox.Show("Data Imported process failed.", enMsgBoxStyle.Information)
'    '            Else
'    '                eZeeMsgBox.Show("Data Imported Successfully.", enMsgBoxStyle.Information)
'    '            End If
'    '        End If

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "WebConnection", mstrModuleName)
'    '    End Try
'    'End Sub
'    'Private Function ExportToWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer) As Boolean

'    '    Dim objDataoperation As New clsDataOperation
'    '    Dim msql As String
'    '    Dim mLinkDesk As String = Company._Object._ConfigDatabaseName & ".."

'    '    Try

'    '        ' Insert country master
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 1,'AFG', 'Afghanistan', 'Af', 1 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 1) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ALB', 'Albania', 'L', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 2) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'DZA', 'Algeria', 'DA', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 3) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ASM', 'American Samoa', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 4) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'AND', 'Andorra', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 5) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'AGO', 'Angola', 'Kz', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 6) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'AIA', 'Anguilla', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 7) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ATA', 'Antarctica', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 8) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ATG', 'Antigua And Barbuda', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 9) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ARG', 'Argentina', 'A', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 10) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ARM', 'Armenia', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 11) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ABW', 'Aruba', 'Afl', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 12) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'AUS', 'Australia', 'A$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 13) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'AUT', 'Austria', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 14) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'AZE', 'Azerbaijan', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 15) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BHS', 'Bahamas, The', 'B$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 16) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BHR', 'Bahrain', 'BD', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 17) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BGD', 'Bangladesh', 'Tk', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 18) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BRB', 'Barbados', 'Bds$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 19) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BLR', 'Belarus', 'BR', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 20) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BEL', 'Belgium', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 21) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BLZ', 'Belize', 'BZ$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 22) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BEN', 'Benin', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 23) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BMU', 'Bermuda', 'Bd$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 24) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BTN', 'Bhutan', 'Nu', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 25) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BOL', 'Bolivia', 'Bs', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 26) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BIH', 'Bosnia and Herzegovina', 'KM', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 27) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BWA', 'Botswana', 'P', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 28) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BVT', 'Bouvet Island', 'NKr', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 29) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BRA', 'Brazil', 'R$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 30) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'IOT', 'British Indian Ocean Territory', 'GBP', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 31) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BRN', 'Brunei', 'B$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 32) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BGR', 'Bulgaria', 'Lv', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 33) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BFA', 'Burkina Faso', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 34) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'BDI', 'Burundi', 'Fbu', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 35) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KHM', 'Cambodia', 'CR', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 36) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CMR', 'Cameroon', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 37) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CAN', 'Canada', 'Can$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 38) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CPV', 'Cape Verde', 'C.V.Esc', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 39) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CYM', 'Cayman Islands', 'CI$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 40) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CAF', 'Central African Republic', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 41) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TCD', 'Chad', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 42) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CHL', 'Chile', 'Ch$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 43) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CHN', 'China', 'Y', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 44) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'HKG', 'China (Hong Kong S.A.R.)', 'HK$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 45) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MAC', 'China (Macau S.A.R.)', 'P', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 46) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CXR', 'Christmas Island', 'A$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 47) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CCK', 'Cocos (Keeling) Islands', 'A$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 48) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'COL', 'Colombia', 'Col$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 49) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'COM', 'Comoros', 'CF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 50) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'COG', 'Congo', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 51) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'COD', 'Congo Democractic Republic of the', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 52) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'COK', 'Cook Islands', 'NZ$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 53) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CRI', 'Costa Rica', '/C', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 54) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CIV', 'Cote D''Ivoire (Ivory Coast)', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 55) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'HRV', 'Croatia (Hrvatska)', 'HRK', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 56) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CUB', 'Cuba', 'Cu$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 57) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CYP', 'Cyprus', '£C', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 58) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CZE', 'Czech Republic', 'Kc', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 59) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'DNK', 'Denmark', 'Dkr', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 60) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'DJI', 'Djibouti', 'DF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 61) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'DMA', 'Dominica', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 62) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'DOM', 'Dominican Republic', 'RD$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 63) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TLS', 'East Timor', 'Rp', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 64) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ECU', 'Ecuador', 'S/', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 65) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'EGY', 'Egypt', '£E', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 66) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SLV', 'El Salvador', '¢', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 67) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GNQ', 'Equatorial Guinea', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 68) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ERI', 'Eritrea', 'Nfa', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 69) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'EST', 'Estonia', 'KR', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 70) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ETH', 'Ethiopia', 'Br', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 71) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'FLK', 'Falkland Islands (Islas Malvinas)', '£F', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 72) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'FRO', 'Faroe Islands', 'Dkr', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 73) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'FJI', 'Fiji Islands', 'F$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 74) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'FIN', 'Finland', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 75) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'FRA', 'France', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 76) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GUF', 'French Guiana', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 77) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PYF', 'French Polynesia', 'CFPF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 78) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ATF', 'French Southern Territories', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 79) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GAB', 'Gabon', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 80) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GMB', 'Gambia, The', 'D', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 81) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GEO', 'Georgia', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 82) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'DEU', 'Germany', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 83) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GHA', 'Ghana', '¢', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 84) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GIB', 'Gibraltar', '£G', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 85) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GRC', 'Greece', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 86) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GRL', 'Greenland', 'Dkr', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 87) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GRD', 'Grenada', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 88) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GLP', 'Guadeloupe', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 89) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GUM', 'Guam', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 90) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GTM', 'Guatemala', 'Q', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 91) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GIN', 'Guinea', 'PG', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 92) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GNB', 'Guinea-Bissau', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 93) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GUY', 'Guyana', 'G$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 94) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'HTI', 'Haiti', 'G$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 95) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'HMD', 'Heard and McDonald Islands', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 96) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'HND', 'Honduras', 'L', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 97) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'HUN', 'Hungary', 'Ft', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 98) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ISL', 'Iceland', 'Ikr', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 99) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'IND', 'India', 'Rs', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 100) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'IDN', 'Indonesia', 'Rp', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 101) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'IRN', 'Iran', 'Rls', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 102) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'IRQ', 'Iraq', 'ID', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 103) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'IRL', 'Ireland', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 104) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'VGB', 'Islands (British)', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 105) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ISR', 'Israel', 'NIS', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 106) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ITA', 'Italy', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 107) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'JAM', 'Jamaica', 'J$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 108) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'JPN', 'Japan', '¥', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 109) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'JOR', 'Jordan', 'JD', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 110) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KAZ', 'Kazakhstan', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 111) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KEN', 'Kenya', 'K Sh', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 112) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KIR', 'Kiribati', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 113) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PRK', 'Korea', 'W', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 114) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KOR', 'Korea, North', 'Wn', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 115) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KWT', 'Kuwait', 'KD', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 116) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KGZ', 'Kyrgyzstan', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 117) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LAO', 'Laos', 'KN', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 118) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LVA', 'Latvia', 'Ls', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 119) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LBN', 'Lebanon', 'L.L.', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 120) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LSO', 'Lesotho', 'L, lp, M', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 121) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LBR', 'Liberia', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 122) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LBY', 'Libya', 'LD', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 123) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LIE', 'Liechtenstein', 'SwF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 124) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LTU', 'Lithuania', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 125) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LUX', 'Luxembourg', 'LuxF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 126) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MKD', 'Macedonia, Former Yugoslav Republic of', 'MKD', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 127) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MDG', 'Madagascar', 'FMG', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 128) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MWI', 'Malawi', 'MK', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 129) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MYS', 'Malaysia', 'RM', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 130) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MDV', 'Maldives', 'Rf', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 131) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MLI', 'Mali', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 132) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MLT', 'Malta', 'Lm', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 133) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MHL', 'Marshall Islands', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 134) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MTQ', 'Martinique', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 135) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MRT', 'Mauritania', 'UM', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 136) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MUS', 'Mauritius', 'Mau Rs', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 137) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MYT', 'Mayotte', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 138) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MEX', 'Mexico', 'Mex$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 139) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'FSM', 'Micronesia', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 140) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MDA', 'Moldova', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 141) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MCO', 'Monaco', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 142) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MNG', 'Mongolia', 'Tug', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 143) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MSR', 'Montserrat', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 144) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MAR', 'Morocco', 'DH', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 145) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MOZ', 'Mozambique', 'Mt', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 146) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MMR', 'Myanmar', 'K', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 147) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NAM', 'Namibia', 'N$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 148) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NRU', 'Nauru', 'A$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 149) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NPL', 'Nepal', 'NRs', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 150) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ANT', 'Netherlands Antilles', 'Ant.f. .', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 151) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NLD', 'Netherlands, The', 'f.', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 152) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NCL', 'New Caledonia', 'CFPF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 153) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NZL', 'New Zealand', 'NZ$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 154) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NIC', 'Nicaragua', 'C$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 155) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NER', 'Niger', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 156) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NGA', 'Nigeria', ' N', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 157) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NIU', 'Niue', 'NZ$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 158) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NFK', 'Norfolk Island', 'A$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 159) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MNP', 'Northern Mariana Islands', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 160) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'NOR', 'Norway', 'NKr', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 161) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'OMN', 'Oman', 'RO', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 162) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PAK', 'Pakistan', 'Rs', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 163) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PLW', 'Palau', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 164) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PAN', 'Panama', 'B', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 165) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PNG', 'Papua new Guinea', 'K', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 166) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PRY', 'Paraguay', '/G', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 167) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PER', 'Peru', 'S/', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 168) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PHL', 'Philippines', 'P', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 169) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PCN', 'Pitcairn Island', 'NZ$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 170) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'POL', 'Poland', 'z - 1', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 171) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PRT', 'Portugal', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 172) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PRI', 'Puerto Rico', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 173) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'QAT', 'Qatar', 'QR', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 174) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'REU', 'Reunion', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 175) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ROU', 'Romania', 'L', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 176) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'RUS', 'Russia', 'R', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 177) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'RWA', 'Rwanda', 'RF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 178) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SHN', 'Saint Helena', '£S', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 179) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'KNA', 'Saint Kitts And Nevis', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 180) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LCA', 'Saint Lucia', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 181) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SPM', 'Saint Pierre and Miquelon', '€', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 182) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'VCT', 'Saint Vincent And The Grenadines', 'EC$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 183) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'WSM', 'Samoa', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 184) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SMR', 'San Marino', 'Lit', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 185) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'STP', 'Sao Tome and Principe', 'Db', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 186) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SAU', 'Saudi Arabia', 'SR1s', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 187) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SEN', 'Senegal', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 188) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SYC', 'Seychelles', 'SR', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 189) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SLE', 'Sierra Leone', 'Le', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 190) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SGP', 'Singapore', 'S$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 191) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SVK', 'Slovakia', 'Sk', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 192) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SVN', 'Slovenia', 'S1T', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 193) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SLB', 'Solomon Islands', 'SI$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 194) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SOM', 'Somalia', 'So. Sh', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 195) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ZAF', 'South Africa', 'R', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 196) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ESP', 'Spain', 'Ptas', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 197) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'LKA', 'Sri Lanka', 'SLRs', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 198) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SDN', 'Sudan', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 199) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SUR', 'Suriname', '$Sur', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 200) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SJM', 'Svalbard And Jan Mayen Islands', 'NKr', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 201) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SWZ', 'Swaziland', 'L, pl. , E', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 202) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SWE', 'Sweden', 'kr or Sk', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 203) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'CHE', 'Switzerland', 'SwF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 204) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SYR', 'Syria', '£S', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 205) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TWN', 'Taiwan', 'NT$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 206) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TJK', 'Tajikistan', '100 dirams', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 207) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TZA', 'Tanzania', 'TSh', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 208) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'THA', 'Thailand', 'Bht or bt', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 209) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TGO', 'Togo', 'CFAF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 210) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TKL', 'Tokelau', 'NZ$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 211) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TON', 'Tonga', 'PT or T$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 212) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TTO', 'Trinidad And Tobago', 'TT$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 213) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TUN', 'Tunisia', 'TD', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 214) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TUR', 'Turkey', 'TL', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 215) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TKM', 'Turkmenistan', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 216) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TCA', 'Turks And Caicos Islands', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 217) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'TUV', 'Tuvalu', 'A$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 218) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'UGA', 'Uganda', 'Ush', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 219) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'UKR', 'Ukraine', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 220) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ARE', 'United Arab Emirates', 'Dh', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 221) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'GBR', 'United Kingdom', '£', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 222) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'USA', 'United States', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 223) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'USA', 'United States Minor Outlying Islands', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 224) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'URY', 'Uruguay', 'Nur$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 225) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'UZB', 'Uzbekistan', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 226) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'VUT', 'Vanuatu', 'VT', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 227) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'VAT', 'Vatican City State (Holy See)', 'Lit', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 228) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'VEN', 'Venezuela', 'Bs', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 229) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'VNM', 'Vietnam', '', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 230) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'VIR', 'Virgin Islands (US)', '$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 231) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'WLF', 'Wallis And Futuna Islands', 'CFPF', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 232) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ESH', 'Western Sahara', 'Ptas', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 233) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'YEM', 'Yemen', 'YR1s', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 234) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'YOG', 'Yugoslavia', 'Din', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 235) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ZMB', 'Zambia', 'ZK', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 236) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'ZWE', 'Zimbabwe', 'Z$', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 237) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'MONT', 'Montenegro', 'Euro', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 238) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'PAL', 'Palestine', '£', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 239) ")
'    '        'objDataoperation.ExecNonQuery("INSERT INTO " & mlinkweb & "cfcountry_master(countryunkid,alias, country_name, Currency, isrighttoleft) SELECT 'SRB', 'Serbia', 'Dinar', 0 WHERE NOT EXISTS (SELECT * FROM " & mlinkweb & "cfcountry_master WHERE countryunkid = 240) ")

'    '        '<TODO to be removed later.>
'    '        If objDataoperation.RecordCount("SELECT * FROM " & mLinkDesk & "cfcountry_master") <> objDataoperation.RecordCount("SELECT * FROM " & mlinkweb & "cfcountry_master") Then
'    '            objDataoperation.ExecNonQuery("delete from " & mlinkweb & "cfcountry_master")
'    '            msql = "INSERT INTO " & mlinkweb & "cfcountry_master ( countryunkid , alias,country_name ,currency ,isrighttoleft ,country_name1 ,country_name2 ) " & _
'    '                    "Select countryunkid, alias ,country_name ,currency,isrighttoleft ,country_name1,country_name2 FROM " & mLinkDesk & "cfcountry_master "

'    '            objDataoperation.ExecNonQuery(msql)
'    '            If objDataoperation.ErrorMessage <> "" Then
'    '                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '            End If
'    '        End If




'    '        'Insert cfcity_master 
'    '        msql = "delete from " & mlinkweb & "cfcity_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "cfcity_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        msql = "insert into " & mlinkweb & "cfcity_master(cityunkid,countryunkid,stateunkid,code,name,isactive,name1,name2,Comp_Code,companyunkid) select cityunkid,countryunkid,stateunkid,code,name,isactive,name1,name2,'" & StrCode & "'," & intClientCode & " from  " & mLinkDesk & "cfcity_master WHERE isactive = 1 "

'    '        objDataoperation.ExecNonQuery(msql)

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If


'    '        'Insert cfstate_master

'    '        msql = "delete from " & mlinkweb & "cfstate_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "cfstate_master")


'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        msql = "insert into " & mlinkweb & "cfstate_master(stateunkid,countryunkid,code,name,isactive,name1,name2,Comp_Code,companyunkid)select stateunkid,countryunkid,code,name,isactive,name1,name2,'" & StrCode & "'," & intClientCode & " from  " & mLinkDesk & "cfstate_master WHERE isactive = 1 "

'    '        objDataoperation.ExecQuery(msql, "cfstate_master")


'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Insert cfzipcode_master

'    '        msql = "delete from " & mlinkweb & "cfzipcode_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "cfzipcode_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If


'    '        msql = "insert into " & mlinkweb & "cfzipcode_master(zipcodeunkid,countryunkid,stateunkid,cityunkid,zipcode_code,zipcode_no,isactive,Comp_Code,companyunkid)select zipcodeunkid,countryunkid,stateunkid,cityunkid,zipcode_code,zipcode_no,isactive,'" & StrCode & "'," & intClientCode & " from  " & mLinkDesk & "cfzipcode_master WHERE isactive = 1 "

'    '        objDataoperation.ExecQuery(msql, "cfzipcode_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Delete Common master

'    '        msql = "delete from " & mlinkweb & "cfcommon_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "cfcommon_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Delete  Skill master

'    '        msql = "delete from " & mlinkweb & "hrskill_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "hrskill_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Delete institute master

'    '        msql = "delete from " & mlinkweb & "hrinstitute_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "hrinstitute_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Delete Qaulification master

'    '        msql = "delete from " & mlinkweb & "hrqualification_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "hrqualification_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Delete vacancy master

'    '        msql = "delete from " & mlinkweb & "rcvacancy_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

'    '        objDataoperation.ExecQuery(msql, "rcvacancy_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Change_Database
'    '        ' mdbname = FinancialYear._Object._DatabaseName


'    '        'mLink = "[" & mdatabaseserver & "]" & ".[" & mdbname & "].[" & mDatabaseOwner & "]."

'    '        'cfcommon_master

'    '        msql = "insert into " & mlinkweb & "cfcommon_master(Comp_Code,companyunkid,masterunkid,code,alias,name,name1,name2,mastertype,isactive)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid, * from cfcommon_master WHERE isactive = 1 "

'    '        objDataoperation.ExecQuery(msql, "cfcommon_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If


'    '        'hrskill_master

'    '        msql = "insert into " & mlinkweb & "hrskill_master(Comp_Code,companyunkid,skillunkid,skillcode,skillname,skillcategoryunkid,description,isactive,skillname1,skillname2)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,* from hrskill_master WHERE isactive = 1 "

'    '        objDataoperation.ExecQuery(msql, "hrskill_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        ' hrinstitute_master

'    '        msql = "insert into " & mlinkweb & "hrinstitute_master(Comp_Code,companyunkid,instituteunkid,institute_code,institute_name,institute_address,telephoneno,institute_fax,institute_email,description,contact_person,stateunkid,cityunkid,countryunkid,ishospital,isactive)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,* from hrinstitute_master WHERE isactive = 1 "

'    '        objDataoperation.ExecQuery(msql, "hrinstitute_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'hrqualification_master

'    '        msql = "insert into " & mlinkweb & "hrqualification_master(Comp_Code,companyunkid,qualificationunkid,qualificationcode,qualificationname,qualificationgroupunkid,description,isactive,qualificationname1,qualificationname2)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,* from hrqualification_master WHERE isactive = 1 "

'    '        objDataoperation.ExecQuery(msql, "hrqualification_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'rcvacancy_master

'    '        msql = "insert into " & mlinkweb & "rcvacancy_master(Comp_Code,vacancyunkid,vacancytitle,openingdate,closingdate,interview_startdate,interview_closedate,noofposition,experience,responsibilities_duties,remark,isvoid,companyunkid) " & _
'    '                "select '" & StrCode & "' as Comp_Code,vacancyunkid,vacancytitle,openingdate,closingdate,interview_startdate,interview_closedate,noofposition,experience,responsibilities_duties,remark,isvoid ," & intClientCode & " as companyunkid from rcvacancy_master WHERE ISNULL(isvoid,0) = 0 "

'    '        objDataoperation.ExecQuery(msql, "rcvacancy_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        Return True

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "ExportToWeb", mstrModuleName)
'    '    End Try
'    'End Function

'    'Private Function ImportFromWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer, ByVal mdatabaseserverweb As String) As Boolean
'    '    Dim objDataoperation As New clsDataOperation
'    '    Dim msql As String
'    '    Dim mLinkDesk As String = Company._Object._ConfigDatabaseName & ".."
'    '    Dim dsList As DataSet
'    '    Dim mintCurrApplicantUnkID As Integer
'    '    Dim mintNewApplicantUnkID As Integer
'    '    Dim objApplicant As clsApplicant_master
'    '    Dim blnResult As Boolean

'    '    Try

'    '        If ConfigParameter._Object._PhotoPath = "" Then
'    '            eZeeMsgBox.Show("Please select Image path from configuration -> Option -> Photo Path.", enMsgBoxStyle.Information)
'    '            Exit Function
'    '        End If

'    '        msql = " select applicantunkid, applicant_code,titleunkid,firstname,surname,othername,gender,email,present_address1,present_address2,present_countryunkid,present_stateunkid,present_province,present_post_townunkid,present_zipcode,present_road,present_estate,present_plotno,present_mobileno,present_alternateno,present_tel_no,present_fax,perm_address1,perm_address2,perm_countryunkid,perm_stateunkid,perm_province,perm_post_townunkid,perm_zipcode,perm_road,perm_estate,perm_plotno,perm_mobileno,perm_alternateno,perm_tel_no,perm_fax,birth_date,marital_statusunkid,anniversary_date,language1unkid,language2unkid,language3unkid,language4unkid,nationality,userunkid,vacancyunkid,isimport "
'    '        msql &= " from  " & mlinkweb & "rcapplicant_master where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' " & vbCrLf
'    '        dsList = objDataoperation.ExecQuery(msql, "rcapplicant_master")

'    '        For Each drRow As DataRow In dsList.Tables("rcapplicant_master").Rows
'    '            mintCurrApplicantUnkID = CInt(drRow.Item("applicantunkid").ToString)
'    '            objApplicant = New clsApplicant_master
'    '            With objApplicant
'    '                ._Applicant_Code = "" 'TODO : Make Applicant code Auto generate from Configuration and make disabled.

'    '                ._Email = drRow.Item("email").ToString
'    '                ._Firstname = drRow.Item("firstname").ToString
'    '                ._Surname = drRow.Item("surname").ToString
'    '                ._Othername = drRow.Item("othername").ToString
'    '                ._Titleunkid = CInt(drRow.Item("titleunkid"))
'    '                ._Gender = CInt(drRow.Item("gender"))
'    '                ._Vacancyunkid = CInt(drRow.Item("vacancyunkid"))
'    '                '._ImagePath = drRow.Item("").ToString 'imgImageControl._FileName
'    '                'imgImageControl.SaveImage()

'    '                'Personal Start
'    '                ._Present_Address1 = drRow.Item("present_address1").ToString
'    '                ._Present_Address2 = drRow.Item("present_address2").ToString
'    '                ._Present_Alternateno = drRow.Item("present_alternateno").ToString
'    '                ._Present_Countryunkid = CInt(drRow.Item("present_countryunkid"))
'    '                ._Present_Estate = drRow.Item("present_estate").ToString
'    '                ._Present_Fax = drRow.Item("present_fax").ToString
'    '                ._Present_Mobileno = drRow.Item("present_mobileno").ToString
'    '                ._Present_Plotno = drRow.Item("present_plotno").ToString
'    '                ._Present_Post_Townunkid = CInt(drRow.Item("present_post_townunkid"))
'    '                ._Present_Province = drRow.Item("present_province").ToString
'    '                ._Present_Road = drRow.Item("present_road").ToString
'    '                ._Present_Stateunkid = CInt(drRow.Item("present_stateunkid"))
'    '                ._Present_Tel_No = drRow.Item("present_tel_no").ToString
'    '                ._Present_ZipCode = CInt(drRow.Item("present_zipcode"))

'    '                ._Perm_Address1 = drRow.Item("perm_address1").ToString
'    '                ._Perm_Address2 = drRow.Item("perm_address2").ToString
'    '                ._Perm_Alternateno = drRow.Item("perm_alternateno").ToString
'    '                ._Perm_Countryunkid = CInt(drRow.Item("perm_countryunkid"))
'    '                ._Perm_Estate = drRow.Item("perm_estate").ToString
'    '                ._Perm_Fax = drRow.Item("perm_fax").ToString
'    '                ._Perm_Mobileno = drRow.Item("perm_mobileno").ToString
'    '                ._Perm_Plotno = drRow.Item("perm_plotno").ToString
'    '                ._Perm_Post_Townunkid = CInt(drRow.Item("perm_post_townunkid"))
'    '                ._Perm_Province = drRow.Item("perm_province").ToString
'    '                ._Perm_Road = drRow.Item("perm_road").ToString
'    '                ._Perm_Stateunkid = CInt(drRow.Item("perm_stateunkid"))
'    '                ._Perm_Tel_No = drRow.Item("perm_tel_no").ToString
'    '                ._Perm_ZipCode = CInt(drRow.Item("perm_zipcode"))
'    '                'Personal End

'    '                'Additional Start

'    '                objApplicant._Anniversary_Date = CDate(drRow.Item("anniversary_date"))

'    '                objApplicant._Birth_Date = CDate(drRow.Item("birth_date"))

'    '                ._Marital_Statusunkid = CInt(drRow.Item("marital_statusunkid"))
'    '                ._Nationality = CInt(drRow.Item("nationality"))
'    '                ._Language1unkid = CInt(drRow.Item("language1unkid"))
'    '                ._Language2unkid = CInt(drRow.Item("language2unkid"))
'    '                ._Language3unkid = CInt(drRow.Item("language3unkid"))
'    '                ._Language4unkid = CInt(drRow.Item("language4unkid"))
'    '                'Additional End

'    '                ._Userunkid = CInt(drRow.Item("userunkid"))

'    '                blnResult = .Insert()
'    '                If blnResult = False And objApplicant._Message <> "" Then
'    '                    Throw New Exception(objDataoperation.ErrorNumber & " : " & objApplicant._Message)
'    '                End If


'    '                mintNewApplicantUnkID = objApplicant._Applicantunkid

'    '            End With


'    '            ' rcjobhistory

'    '            msql = "insert into rcjobhistory(applicantunkid,employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason) " & vbCrLf

'    '            msql &= "select " & mintNewApplicantUnkID & ",employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason from  " & mlinkweb & "rcjobhistory where Comp_Code='" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
'    '            msql &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

'    '            objDataoperation.ExecQuery(msql, "rcjobhistory")

'    '            If objDataoperation.ErrorMessage <> "" Then
'    '                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '            End If


'    '            ' rcapplicantskill_tran
'    '            msql = "insert into  rcapplicantskill_tran(applicantunkid,skillcategoryunkid,skillunkid,remark)" & vbCrLf
'    '            'Sohail (05 Jul 2011) -- Start
'    '            'Purpose :  Replace applicatunkid with new generated applicatunkid
'    '            msql &= "select " & mintNewApplicantUnkID & ",skillcategoryunkid,skillunkid,remark from  " & mlinkweb & "rcapplicantskill_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' '" & vbCrLf
'    '            msql &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "
'    '            'Sohail (05 Jul 2011) -- End
'    '            objDataoperation.ExecQuery(msql, "rcapplicantskill_tran")

'    '            If objDataoperation.ErrorMessage <> "" Then
'    '                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '            End If


'    '            ' rcapplicantqualification_tran
'    '            msql = "insert into rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark)" & vbCrLf
'    '            'Sohail (05 Jul 2011) -- Start
'    '            'Purpose :  Replace applicatunkid with new generated applicatunkid
'    '            msql &= "select " & mintNewApplicantUnkID & ",qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark from  " & mlinkweb & "rcapplicantqualification_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
'    '            msql &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "
'    '            'Sohail (05 Jul 2011) -- End
'    '            objDataoperation.ExecQuery(msql, "rcapplicantqualification_tran")

'    '            If objDataoperation.ErrorMessage <> "" Then
'    '                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '            End If

'    '            ' hr_images_tran
'    '            msql = "insert into hr_images_tran(employeeunkid,imagename,transactionid,referenceid,isapplicant) " & vbCrLf
'    '            'Sohail (05 Jul 2011) -- Start
'    '            'Purpose :  Replace applicatunkid with new generated applicatunkid
'    '            msql &= "select " & mintNewApplicantUnkID & ",imagename," & mintNewApplicantUnkID & ",referenceid,isapplicant from " & mlinkweb & "hr_images_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
'    '            msql &= " AND employeeunkid = " & mintCurrApplicantUnkID & " "
'    '            'Sohail (05 Jul 2011) -- End
'    '            objDataoperation.ExecQuery(msql, "hr_images_tran")

'    '            If objDataoperation.ErrorMessage <> "" Then
'    '                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '            End If

'    '        Next

'    '        ' Dim objDataoperation As New eZeeCommonLib.clsDataOperation("payrollweb")

'    '        msql = "update   " & mlinkweb & "rcapplicant_master  set Syncdatetime= getdate()  where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' "
'    '        objDataoperation.ExecQuery(msql, "rcapplicant_master")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        msql = "update " & mlinkweb & "rcjobhistory  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and   isnull(Syncdatetime,' ')=' ' "
'    '        objDataoperation.ExecQuery(msql, "rcjobhistory")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        msql = "update " & mlinkweb & "rcapplicantskill_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' '  "
'    '        objDataoperation.ExecQuery(msql, "rcapplicantskill_tran")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        msql = "update " & mlinkweb & "rcapplicantqualification_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' "
'    '        objDataoperation.ExecQuery(msql, "rcapplicantqualification_tran")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Sohail (05 Jul 2011) -- Start
'    '        Dim imgServerPath As String = "http://" & Split(mdatabaseserverweb, "\")(0)
'    '        Dim imgLocalPath As String = ConfigParameter._Object._PhotoPath & "\"
'    '        Dim objWebRequest As System.Net.WebRequest
'    '        Dim objWebResponse As System.Net.WebResponse
'    '        Dim reader As IO.Stream
'    '        Dim writer As IO.Stream
'    '        Dim buffer(1023) As Byte
'    '        Dim bytesRead As Integer
'    '        Dim lngTotalBytes As Long
'    '        Dim lngMaxBytes As Long
'    '        Dim intCnt, i As Integer
'    '        Dim dsImages As DataSet
'    '        Dim imgWebpath As String = "/Aruti/UploadImage/"  '<TODO - give webserver folder path for image.>

'    '        msql = "select imagename from " & mlinkweb & "hr_images_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND  ISNULL(imagename,' ') <> ' ' "
'    '        dsImages = objDataoperation.ExecQuery(msql, "hr_images_tran")
'    '        intCnt = dsImages.Tables("hr_images_tran").Rows.Count
'    '        'If intCnt > 0 Then ProgressBar1.Visible = True
'    '        i = 0
'    '        Cursor.Current = Cursors.WaitCursor
'    '        For Each dsRow As DataRow In dsImages.Tables("hr_images_tran").Rows
'    '            i += 1

'    '            objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dsRow.Item("imagename")))
'    '            objWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

'    '            If WebFileExist(objWebRequest) = True Then
'    '                objWebResponse = objWebRequest.GetResponse

'    '                lngMaxBytes = CLng(objWebResponse.ContentLength)
'    '                'ProgressBar1.Maximum = lngMaxBytes

'    '                reader = objWebResponse.GetResponseStream
'    '                writer = IO.File.Create(imgLocalPath & CStr(dsRow.Item("imagename")))
'    '                bytesRead = 0
'    '                lngTotalBytes = 0
'    '                While True
'    '                    bytesRead = reader.Read(buffer, 0, buffer.Length)
'    '                    If bytesRead <= 0 Then Exit While

'    '                    writer.Write(buffer, 0, bytesRead)
'    '                    lngTotalBytes += bytesRead

'    '                    'If lngTotalBytes <= ProgressBar1.Maximum Then ProgressBar1.Value = lngTotalBytes
'    '                End While
'    '                reader.Close()
'    '                writer.Close()
'    '            End If
'    '            'lblCount.Text = "(" & i & "/" & intCnt.ToString & ")"
'    '            'lblCount.Refresh()
'    '        Next
'    '        'ProgressBar1.Visible = False
'    '        'lblCount.Text = ""
'    '        'lblCount.Refresh()
'    '        'Sohail (05 Jul 2011) -- End

'    '        msql = "update " & mlinkweb & "hr_images_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' "
'    '        objDataoperation.ExecQuery(msql, "hr_images_tran")

'    '        If objDataoperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
'    '        End If

'    '        'Next i

'    '        Return True

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "ImportFromWeb", mstrModuleName)
'    '    End Try
'    'End Function

'    'Public Function WebFileExist(ByRef objRequest As System.Net.WebRequest) As Boolean
'    '    Dim objResponse As System.Net.HttpWebResponse
'    '    Try
'    '        objResponse = CType(objRequest.GetResponse, Net.HttpWebResponse)

'    '        Return True
'    '    Catch ex As System.Net.WebException
'    '        Dim r As System.Net.HttpWebResponse = CType(ex.Response, Net.HttpWebResponse)
'    '        If r.StatusCode = Net.HttpStatusCode.NotFound Then
'    '            Return False
'    '        Else
'    '            MessageBox.Show(ex.ToString)
'    '        End If
'    '    Finally
'    '        objResponse = Nothing
'    '    End Try

'    'End Function


'    'Private Sub cboApplicantType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApplicantType.SelectedIndexChanged
'    '    Try
'    '        If CInt(cboApplicantType.SelectedValue) > 0 Then
'    '            Select Case CInt(cboApplicantType.SelectedValue)
'    '                Case enVacancyType.EXTERNAL_VACANCY
'    '                    objpnlInternalApplicantColor.Visible = False
'    '                Case enVacancyType.INTERNAL_VACANCY
'    '                    objpnlInternalApplicantColor.Visible = True
'    '                Case Else
'    '                    objpnlInternalApplicantColor.Visible = True
'    '            End Select
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboApplicantType_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub
#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 



			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnMailClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnMailClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblOthername.Text = Language._Object.getCaption(Me.lblOthername.Name, Me.lblOthername.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
			Me.lblTelNo.Text = Language._Object.getCaption(Me.lblTelNo.Name, Me.lblTelNo.Text)
			Me.btnMailClose.Text = Language._Object.getCaption(Me.btnMailClose.Name, Me.btnMailClose.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
			Me.chkIncludeInternalApplicant.Text = Language._Object.getCaption(Me.chkIncludeInternalApplicant.Name, Me.chkIncludeInternalApplicant.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
			Me.lblApplicantType.Text = Language._Object.getCaption(Me.lblApplicantType.Name, Me.lblApplicantType.Text)
			Me.lblShortListType.Text = Language._Object.getCaption(Me.lblShortListType.Name, Me.lblShortListType.Text)
			Me.mnuMapVacancy.Text = Language._Object.getCaption(Me.mnuMapVacancy.Name, Me.mnuMapVacancy.Text)
			Me.mnuScan_Documents.Text = Language._Object.getCaption(Me.mnuScan_Documents.Name, Me.mnuScan_Documents.Text)
			Me.mnuPreviewAttachments.Text = Language._Object.getCaption(Me.mnuPreviewAttachments.Name, Me.mnuPreviewAttachments.Text)
			Me.dgcolhApplicantName.HeaderText = Language._Object.getCaption(Me.dgcolhApplicantName.Name, Me.dgcolhApplicantName.HeaderText)
			Me.dgcolhemail.HeaderText = Language._Object.getCaption(Me.dgcolhemail.Name, Me.dgcolhemail.HeaderText)
			Me.dgcolhMobile.HeaderText = Language._Object.getCaption(Me.dgcolhMobile.Name, Me.dgcolhMobile.HeaderText)
			Me.dgcolhTelNo.HeaderText = Language._Object.getCaption(Me.dgcolhTelNo.Name, Me.dgcolhTelNo.HeaderText)
			Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
			Me.dgcolhVacancy.HeaderText = Language._Object.getCaption(Me.dgcolhVacancy.Name, Me.dgcolhVacancy.HeaderText)
            Me.mnuDownloadAttachments.Text = Language._Object.getCaption(Me.mnuDownloadAttachments.Name, Me.mnuDownloadAttachments.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub


	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Applicant from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Some of the email address(s) are blank. Do you want to continue?")
			Language.setMessage(mstrModuleName, 3, "Select Applicant")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Final Short Listed Applicant")
			Language.setMessage(mstrModuleName, 6, "Non Short Listed Applicant")
            Language.setMessage(mstrModuleName, 7, "There is no attached document(s) for this applicant.")
            Language.setMessage(mstrModuleName, 8, "File(s) downloaded successfully to the selected location.")
            Language.setMessage(mstrModuleName, 9, "There is no attached document(s) for this applicant.")
			Language.setMessage(mstrModuleName, 10, "Are you sure you want to delete this Applicant?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

