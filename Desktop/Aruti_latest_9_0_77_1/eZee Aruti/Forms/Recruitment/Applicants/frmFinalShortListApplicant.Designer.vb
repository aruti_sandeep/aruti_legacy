﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinalShortListApplicant
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFinalShortListApplicant))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbApplicantInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboApplicantType = New System.Windows.Forms.ComboBox
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblApplicantType = New System.Windows.Forms.Label
        Me.lblFinalApplicantType = New System.Windows.Forms.Label
        Me.cboFinalApplicantType = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.objbtnSearchApplicant = New eZee.Common.eZeeGradientButton
        Me.lblApplicant = New System.Windows.Forms.Label
        Me.cboApplicant = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnPrintList = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSendMail = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnGenerateLetter = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlEligibleApplicants = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.lvFinalAppList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhApplicantName = New System.Windows.Forms.ColumnHeader
        Me.colhAddress = New System.Windows.Forms.ColumnHeader
        Me.colhPhone = New System.Windows.Forms.ColumnHeader
        Me.colhEmail = New System.Windows.Forms.ColumnHeader
        Me.colhVacancy = New System.Windows.Forms.ColumnHeader
        Me.objcolhVacancyunkid = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbApplicantInfo.SuspendLayout()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.pnlEligibleApplicants.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbApplicantInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.pnlEligibleApplicants)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(784, 477)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbApplicantInfo
        '
        Me.gbApplicantInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.Checked = False
        Me.gbApplicantInfo.CollapseAllExceptThis = False
        Me.gbApplicantInfo.CollapsedHoverImage = Nothing
        Me.gbApplicantInfo.CollapsedNormalImage = Nothing
        Me.gbApplicantInfo.CollapsedPressedImage = Nothing
        Me.gbApplicantInfo.CollapseOnLoad = False
        Me.gbApplicantInfo.Controls.Add(Me.objbtnSearch)
        Me.gbApplicantInfo.Controls.Add(Me.objbtnReset)
        Me.gbApplicantInfo.Controls.Add(Me.cboApplicantType)
        Me.gbApplicantInfo.Controls.Add(Me.cboVacancyType)
        Me.gbApplicantInfo.Controls.Add(Me.lblApplicantType)
        Me.gbApplicantInfo.Controls.Add(Me.lblFinalApplicantType)
        Me.gbApplicantInfo.Controls.Add(Me.cboFinalApplicantType)
        Me.gbApplicantInfo.Controls.Add(Me.lblVacancyType)
        Me.gbApplicantInfo.Controls.Add(Me.objbtnSearchApplicant)
        Me.gbApplicantInfo.Controls.Add(Me.lblApplicant)
        Me.gbApplicantInfo.Controls.Add(Me.cboApplicant)
        Me.gbApplicantInfo.Controls.Add(Me.lblJob)
        Me.gbApplicantInfo.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbApplicantInfo.Controls.Add(Me.cboVacancy)
        Me.gbApplicantInfo.ExpandedHoverImage = Nothing
        Me.gbApplicantInfo.ExpandedNormalImage = Nothing
        Me.gbApplicantInfo.ExpandedPressedImage = Nothing
        Me.gbApplicantInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplicantInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApplicantInfo.HeaderHeight = 25
        Me.gbApplicantInfo.HeaderMessage = ""
        Me.gbApplicantInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApplicantInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.HeightOnCollapse = 0
        Me.gbApplicantInfo.LeftTextSpace = 0
        Me.gbApplicantInfo.Location = New System.Drawing.Point(12, 64)
        Me.gbApplicantInfo.Name = "gbApplicantInfo"
        Me.gbApplicantInfo.OpenHeight = 261
        Me.gbApplicantInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApplicantInfo.ShowBorder = True
        Me.gbApplicantInfo.ShowCheckBox = False
        Me.gbApplicantInfo.ShowCollapseButton = False
        Me.gbApplicantInfo.ShowDefaultBorderColor = True
        Me.gbApplicantInfo.ShowDownButton = False
        Me.gbApplicantInfo.ShowHeader = True
        Me.gbApplicantInfo.Size = New System.Drawing.Size(760, 90)
        Me.gbApplicantInfo.TabIndex = 2
        Me.gbApplicantInfo.Temp = 0
        Me.gbApplicantInfo.Text = "Applicant Info"
        Me.gbApplicantInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(711, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 98
        Me.objbtnSearch.TabStop = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(735, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 99
        Me.objbtnReset.TabStop = False
        '
        'cboApplicantType
        '
        Me.cboApplicantType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicantType.DropDownWidth = 250
        Me.cboApplicantType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicantType.FormattingEnabled = True
        Me.cboApplicantType.Location = New System.Drawing.Point(93, 33)
        Me.cboApplicantType.Name = "cboApplicantType"
        Me.cboApplicantType.Size = New System.Drawing.Size(107, 21)
        Me.cboApplicantType.TabIndex = 108
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 250
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(93, 60)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(107, 21)
        Me.cboVacancyType.TabIndex = 102
        '
        'lblApplicantType
        '
        Me.lblApplicantType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicantType.Location = New System.Drawing.Point(8, 36)
        Me.lblApplicantType.Name = "lblApplicantType"
        Me.lblApplicantType.Size = New System.Drawing.Size(79, 15)
        Me.lblApplicantType.TabIndex = 107
        Me.lblApplicantType.Text = "Applicant Type"
        Me.lblApplicantType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFinalApplicantType
        '
        Me.lblFinalApplicantType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinalApplicantType.Location = New System.Drawing.Point(462, 36)
        Me.lblFinalApplicantType.Name = "lblFinalApplicantType"
        Me.lblFinalApplicantType.Size = New System.Drawing.Size(107, 14)
        Me.lblFinalApplicantType.TabIndex = 104
        Me.lblFinalApplicantType.Text = "Final Applicant Type"
        Me.lblFinalApplicantType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFinalApplicantType
        '
        Me.cboFinalApplicantType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFinalApplicantType.DropDownWidth = 200
        Me.cboFinalApplicantType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFinalApplicantType.FormattingEnabled = True
        Me.cboFinalApplicantType.Location = New System.Drawing.Point(575, 33)
        Me.cboFinalApplicantType.Name = "cboFinalApplicantType"
        Me.cboFinalApplicantType.Size = New System.Drawing.Size(147, 21)
        Me.cboFinalApplicantType.TabIndex = 105
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(10, 63)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(77, 15)
        Me.lblVacancyType.TabIndex = 101
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchApplicant
        '
        Me.objbtnSearchApplicant.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApplicant.BorderSelected = False
        Me.objbtnSearchApplicant.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApplicant.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApplicant.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApplicant.Location = New System.Drawing.Point(435, 33)
        Me.objbtnSearchApplicant.Name = "objbtnSearchApplicant"
        Me.objbtnSearchApplicant.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApplicant.TabIndex = 93
        '
        'lblApplicant
        '
        Me.lblApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicant.Location = New System.Drawing.Point(206, 36)
        Me.lblApplicant.Name = "lblApplicant"
        Me.lblApplicant.Size = New System.Drawing.Size(71, 14)
        Me.lblApplicant.TabIndex = 91
        Me.lblApplicant.Text = "Applicant"
        Me.lblApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApplicant
        '
        Me.cboApplicant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicant.DropDownWidth = 300
        Me.cboApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicant.FormattingEnabled = True
        Me.cboApplicant.Location = New System.Drawing.Point(283, 33)
        Me.cboApplicant.Name = "cboApplicant"
        Me.cboApplicant.Size = New System.Drawing.Size(146, 21)
        Me.cboApplicant.TabIndex = 92
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(206, 63)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(71, 15)
        Me.lblJob.TabIndex = 1
        Me.lblJob.Text = "Vacancy"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(728, 60)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 87
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 500
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(283, 60)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(439, 21)
        Me.cboVacancy.TabIndex = 2
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnPrintList)
        Me.objFooter.Controls.Add(Me.btnSendMail)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnGenerateLetter)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 422)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(784, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnPrintList
        '
        Me.btnPrintList.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrintList.BackColor = System.Drawing.Color.White
        Me.btnPrintList.BackgroundImage = CType(resources.GetObject("btnPrintList.BackgroundImage"), System.Drawing.Image)
        Me.btnPrintList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPrintList.BorderColor = System.Drawing.Color.Empty
        Me.btnPrintList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPrintList.FlatAppearance.BorderSize = 0
        Me.btnPrintList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrintList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintList.ForeColor = System.Drawing.Color.Black
        Me.btnPrintList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrintList.GradientForeColor = System.Drawing.Color.Black
        Me.btnPrintList.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrintList.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPrintList.Location = New System.Drawing.Point(12, 13)
        Me.btnPrintList.Name = "btnPrintList"
        Me.btnPrintList.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrintList.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPrintList.Size = New System.Drawing.Size(97, 30)
        Me.btnPrintList.TabIndex = 132
        Me.btnPrintList.Text = "&Print List"
        Me.btnPrintList.UseVisualStyleBackColor = True
        '
        'btnSendMail
        '
        Me.btnSendMail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSendMail.BackColor = System.Drawing.Color.White
        Me.btnSendMail.BackgroundImage = CType(resources.GetObject("btnSendMail.BackgroundImage"), System.Drawing.Image)
        Me.btnSendMail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSendMail.BorderColor = System.Drawing.Color.Empty
        Me.btnSendMail.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSendMail.FlatAppearance.BorderSize = 0
        Me.btnSendMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSendMail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSendMail.ForeColor = System.Drawing.Color.Black
        Me.btnSendMail.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSendMail.GradientForeColor = System.Drawing.Color.Black
        Me.btnSendMail.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendMail.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSendMail.Location = New System.Drawing.Point(455, 13)
        Me.btnSendMail.Name = "btnSendMail"
        Me.btnSendMail.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendMail.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSendMail.Size = New System.Drawing.Size(97, 30)
        Me.btnSendMail.TabIndex = 131
        Me.btnSendMail.Text = "&Send Mail"
        Me.btnSendMail.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(675, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 129
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnGenerateLetter
        '
        Me.btnGenerateLetter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGenerateLetter.BackColor = System.Drawing.Color.White
        Me.btnGenerateLetter.BackgroundImage = CType(resources.GetObject("btnGenerateLetter.BackgroundImage"), System.Drawing.Image)
        Me.btnGenerateLetter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGenerateLetter.BorderColor = System.Drawing.Color.Empty
        Me.btnGenerateLetter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnGenerateLetter.FlatAppearance.BorderSize = 0
        Me.btnGenerateLetter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerateLetter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerateLetter.ForeColor = System.Drawing.Color.Black
        Me.btnGenerateLetter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGenerateLetter.GradientForeColor = System.Drawing.Color.Black
        Me.btnGenerateLetter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGenerateLetter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGenerateLetter.Location = New System.Drawing.Point(558, 13)
        Me.btnGenerateLetter.Name = "btnGenerateLetter"
        Me.btnGenerateLetter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGenerateLetter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGenerateLetter.Size = New System.Drawing.Size(111, 30)
        Me.btnGenerateLetter.TabIndex = 130
        Me.btnGenerateLetter.Text = "&Generate Letter"
        Me.btnGenerateLetter.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(784, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Final Applicant List"
        '
        'pnlEligibleApplicants
        '
        Me.pnlEligibleApplicants.Controls.Add(Me.objChkAll)
        Me.pnlEligibleApplicants.Controls.Add(Me.lvFinalAppList)
        Me.pnlEligibleApplicants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEligibleApplicants.Location = New System.Drawing.Point(12, 160)
        Me.pnlEligibleApplicants.Name = "pnlEligibleApplicants"
        Me.pnlEligibleApplicants.Size = New System.Drawing.Size(760, 256)
        Me.pnlEligibleApplicants.TabIndex = 88
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(8, 5)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 1
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'lvFinalAppList
        '
        Me.lvFinalAppList.BackColorOnChecked = True
        Me.lvFinalAppList.CheckBoxes = True
        Me.lvFinalAppList.ColumnHeaders = Nothing
        Me.lvFinalAppList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhApplicantName, Me.colhAddress, Me.colhPhone, Me.colhEmail, Me.colhVacancy, Me.objcolhVacancyunkid, Me.colhStatus})
        Me.lvFinalAppList.CompulsoryColumns = ""
        Me.lvFinalAppList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvFinalAppList.FullRowSelect = True
        Me.lvFinalAppList.GridLines = True
        Me.lvFinalAppList.GroupingColumn = Nothing
        Me.lvFinalAppList.HideSelection = False
        Me.lvFinalAppList.Location = New System.Drawing.Point(0, 0)
        Me.lvFinalAppList.MinColumnWidth = 50
        Me.lvFinalAppList.MultiSelect = False
        Me.lvFinalAppList.Name = "lvFinalAppList"
        Me.lvFinalAppList.OptionalColumns = ""
        Me.lvFinalAppList.ShowMoreItem = False
        Me.lvFinalAppList.ShowSaveItem = False
        Me.lvFinalAppList.ShowSelectAll = True
        Me.lvFinalAppList.ShowSizeAllColumnsToFit = True
        Me.lvFinalAppList.Size = New System.Drawing.Size(760, 256)
        Me.lvFinalAppList.Sortable = True
        Me.lvFinalAppList.TabIndex = 0
        Me.lvFinalAppList.UseCompatibleStateImageBehavior = False
        Me.lvFinalAppList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhApplicantName
        '
        Me.colhApplicantName.Tag = "colhApplicantName"
        Me.colhApplicantName.Text = "Applicant"
        Me.colhApplicantName.Width = 165
        '
        'colhAddress
        '
        Me.colhAddress.Text = "Address"
        Me.colhAddress.Width = 185
        '
        'colhPhone
        '
        Me.colhPhone.Text = "Contact No"
        Me.colhPhone.Width = 100
        '
        'colhEmail
        '
        Me.colhEmail.Text = "Email"
        Me.colhEmail.Width = 180
        '
        'colhVacancy
        '
        Me.colhVacancy.Text = "Vacancy Title"
        Me.colhVacancy.Width = 0
        '
        'objcolhVacancyunkid
        '
        Me.objcolhVacancyunkid.Text = "objcolhVacancyunkid"
        Me.objcolhVacancyunkid.Width = 0
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 100
        '
        'frmFinalShortListApplicant
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 477)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinalShortListApplicant"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Final Applicant List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbApplicantInfo.ResumeLayout(False)
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.pnlEligibleApplicants.ResumeLayout(False)
        Me.pnlEligibleApplicants.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbApplicantInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlEligibleApplicants As System.Windows.Forms.Panel
    Friend WithEvents lvFinalAppList As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApplicantName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAddress As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPhone As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmail As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchApplicant As eZee.Common.eZeeGradientButton
    Friend WithEvents lblApplicant As System.Windows.Forms.Label
    Friend WithEvents cboApplicant As System.Windows.Forms.ComboBox
    Friend WithEvents btnGenerateLetter As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnSendMail As eZee.Common.eZeeLightButton
    Friend WithEvents btnPrintList As eZee.Common.eZeeLightButton
    Friend WithEvents colhVacancy As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhVacancyunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents lblFinalApplicantType As System.Windows.Forms.Label
    Friend WithEvents cboFinalApplicantType As System.Windows.Forms.ComboBox
    Friend WithEvents cboApplicantType As System.Windows.Forms.ComboBox
    Friend WithEvents lblApplicantType As System.Windows.Forms.Label
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
End Class
