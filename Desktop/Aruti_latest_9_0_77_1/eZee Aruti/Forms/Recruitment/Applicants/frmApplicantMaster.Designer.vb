﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantMaster
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApplicantMaster))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbApplicantInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtVacancy = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objbtnAddVacancy = New eZee.Common.eZeeGradientButton
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.txtEmployeeCode = New eZee.TextBox.AlphanumericTextBox
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.txtApplicantCode = New eZee.TextBox.AlphanumericTextBox
        Me.cboTitle = New System.Windows.Forms.ComboBox
        Me.lblApplicantCode = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.imgImageControl = New eZee.Common.eZeeImageControl
        Me.lblOthername = New System.Windows.Forms.Label
        Me.lblApplicantFirstname = New System.Windows.Forms.Label
        Me.lblSurname = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.txtApplcantFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.txtApplicantOtherName = New eZee.TextBox.AlphanumericTextBox
        Me.txtApplicantSurname = New eZee.TextBox.AlphanumericTextBox
        Me.tabcApplicantInformation = New System.Windows.Forms.TabControl
        Me.tabpPersonalInfo = New System.Windows.Forms.TabPage
        Me.pnlPersonalInfo = New System.Windows.Forms.Panel
        Me.gbPrsonalInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlPersonalInfoinner = New System.Windows.Forms.Panel
        Me.txtIndexNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblIndexNumber = New System.Windows.Forms.Label
        Me.cboResidencyType = New System.Windows.Forms.ComboBox
        Me.lblResidencyType = New System.Windows.Forms.Label
        Me.txtPhoneNumber = New eZee.TextBox.AlphanumericTextBox
        Me.lblPhone1 = New System.Windows.Forms.Label
        Me.txtVillage = New eZee.TextBox.AlphanumericTextBox
        Me.lblVillage = New System.Windows.Forms.Label
        Me.txtNidaMobile = New eZee.TextBox.AlphanumericTextBox
        Me.lblNidaMobile = New System.Windows.Forms.Label
        Me.txtTin = New eZee.TextBox.AlphanumericTextBox
        Me.lblTIN = New System.Windows.Forms.Label
        Me.txtNidaNumber = New eZee.TextBox.AlphanumericTextBox
        Me.lblNin = New System.Windows.Forms.Label
        Me.elBasicInfo = New eZee.Common.eZeeLine
        Me.cboPostTown = New System.Windows.Forms.ComboBox
        Me.cboPermCountry = New System.Windows.Forms.ComboBox
        Me.lblPermPostCountry = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblPermPostCode = New System.Windows.Forms.Label
        Me.lblPostcode = New System.Windows.Forms.Label
        Me.lblPermState = New System.Windows.Forms.Label
        Me.cboPermPostTown = New System.Windows.Forms.ComboBox
        Me.cboPermPostCode = New System.Windows.Forms.ComboBox
        Me.cboPostCode = New System.Windows.Forms.ComboBox
        Me.cboPermState = New System.Windows.Forms.ComboBox
        Me.lblPostCountry = New System.Windows.Forms.Label
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.lnkCopyAddress = New System.Windows.Forms.LinkLabel
        Me.lblState = New System.Windows.Forms.Label
        Me.txtPermFax = New eZee.TextBox.AlphanumericTextBox
        Me.txtPermAltNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblPermFax = New System.Windows.Forms.Label
        Me.lblPermAltNo = New System.Windows.Forms.Label
        Me.txtPermTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblPermTelNo = New System.Windows.Forms.Label
        Me.txtPermMobile = New eZee.TextBox.AlphanumericTextBox
        Me.lblPermMobileNo = New System.Windows.Forms.Label
        Me.txtPermPlotNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblPermPlotNo = New System.Windows.Forms.Label
        Me.txtPermProvince = New eZee.TextBox.AlphanumericTextBox
        Me.lblPermProvince = New System.Windows.Forms.Label
        Me.txtPermEstate = New eZee.TextBox.AlphanumericTextBox
        Me.txtPermRoad = New eZee.TextBox.AlphanumericTextBox
        Me.lblPermRoad = New System.Windows.Forms.Label
        Me.lblPermEstate = New System.Windows.Forms.Label
        Me.lblPermPostTown = New System.Windows.Forms.Label
        Me.txtPermAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtPermAddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblPermAddress = New System.Windows.Forms.Label
        Me.lnPermanentAddress = New eZee.Common.eZeeLine
        Me.txtProvince = New eZee.TextBox.AlphanumericTextBox
        Me.lblProvince = New System.Windows.Forms.Label
        Me.lblPostTown = New System.Windows.Forms.Label
        Me.txtAddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtRoad = New eZee.TextBox.AlphanumericTextBox
        Me.lblRoad = New System.Windows.Forms.Label
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtAlternativeNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtEstate = New eZee.TextBox.AlphanumericTextBox
        Me.lblEstate = New System.Windows.Forms.Label
        Me.lblAlternativeNo = New System.Windows.Forms.Label
        Me.txtMobile = New eZee.TextBox.AlphanumericTextBox
        Me.txtPlotNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblPloteNo = New System.Windows.Forms.Label
        Me.lblMobile = New System.Windows.Forms.Label
        Me.lblTelNo = New System.Windows.Forms.Label
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lnPresentAddress = New eZee.Common.eZeeLine
        Me.tabpAdditionalInfo = New System.Windows.Forms.TabPage
        Me.pnlAdditionalInfo = New System.Windows.Forms.Panel
        Me.gbSkillInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboSkillExpertise = New System.Windows.Forms.ComboBox
        Me.lblSkillExpertise = New System.Windows.Forms.Label
        Me.lnkOtherSkill = New System.Windows.Forms.LinkLabel
        Me.pnlOtherSkill = New System.Windows.Forms.Panel
        Me.txtOtherSkillCategory = New eZee.TextBox.AlphanumericTextBox
        Me.lblOtherSkillCategory = New System.Windows.Forms.Label
        Me.txtOtherSkill = New eZee.TextBox.AlphanumericTextBox
        Me.lblOtherSkill = New System.Windows.Forms.Label
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlSkillInfo = New System.Windows.Forms.Panel
        Me.lvApplicantSkill = New System.Windows.Forms.ListView
        Me.colhSkillCategory = New System.Windows.Forms.ColumnHeader
        Me.colhSkillName = New System.Windows.Forms.ColumnHeader
        Me.colhSkillExpertise = New System.Windows.Forms.ColumnHeader
        Me.colhSkillRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhSkillGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhSkillCategoryunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhSkillunkid = New System.Windows.Forms.ColumnHeader
        Me.objelSkill = New eZee.Common.eZeeLine
        Me.objbtnAddSkill = New eZee.Common.eZeeGradientButton
        Me.cboSkillCategory = New System.Windows.Forms.ComboBox
        Me.objbtnAddSkillCategory = New eZee.Common.eZeeGradientButton
        Me.lblSkillCategory = New System.Windows.Forms.Label
        Me.lblSkillName = New System.Windows.Forms.Label
        Me.cboSkill = New System.Windows.Forms.ComboBox
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.gbOtherInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblMotherTongue = New System.Windows.Forms.Label
        Me.txtMotherTongue = New eZee.TextBox.AlphanumericTextBox
        Me.lblNationality = New System.Windows.Forms.Label
        Me.lblBirtdate = New System.Windows.Forms.Label
        Me.lblLanguage1 = New System.Windows.Forms.Label
        Me.lblMaritalStatus = New System.Windows.Forms.Label
        Me.cboLanguage1 = New System.Windows.Forms.ComboBox
        Me.dtpBirthdate = New System.Windows.Forms.DateTimePicker
        Me.lblLanguage2 = New System.Windows.Forms.Label
        Me.cboNationality = New System.Windows.Forms.ComboBox
        Me.cboMaritalStatus = New System.Windows.Forms.ComboBox
        Me.cboLanguage3 = New System.Windows.Forms.ComboBox
        Me.lblMaritalDate = New System.Windows.Forms.Label
        Me.lblLanguage4 = New System.Windows.Forms.Label
        Me.dtpAnniversaryDate = New System.Windows.Forms.DateTimePicker
        Me.cboLanguage2 = New System.Windows.Forms.ComboBox
        Me.cboLanguage4 = New System.Windows.Forms.ComboBox
        Me.lblLanguage3 = New System.Windows.Forms.Label
        Me.tabpEducationalInfo = New System.Windows.Forms.TabPage
        Me.pnlQualificationDetail = New System.Windows.Forms.Panel
        Me.gbQualificationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkInstitute = New System.Windows.Forms.CheckBox
        Me.chkResultCode = New System.Windows.Forms.CheckBox
        Me.chkqualificationAward = New System.Windows.Forms.CheckBox
        Me.txtOtherInstitute = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherResultCode = New eZee.TextBox.AlphanumericTextBox
        Me.chkqualificationgroup = New System.Windows.Forms.CheckBox
        Me.chkHighestqualification = New System.Windows.Forms.CheckBox
        Me.txtOtherQualification = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherQualificationGrp = New eZee.TextBox.AlphanumericTextBox
        Me.txtCertiNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCertiNo = New System.Windows.Forms.Label
        Me.lnkOtherQualification = New System.Windows.Forms.LinkLabel
        Me.nudGPA = New System.Windows.Forms.NumericUpDown
        Me.cboResultCode = New System.Windows.Forms.ComboBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.lblGPA = New System.Windows.Forms.Label
        Me.pnlQualifyInfo = New System.Windows.Forms.Panel
        Me.pnlOtherQualification = New System.Windows.Forms.Panel
        Me.lblOtherQualificationGrp = New System.Windows.Forms.Label
        Me.lblOtherInstitution = New System.Windows.Forms.Label
        Me.lblOtherResultCode = New System.Windows.Forms.Label
        Me.lblOtherQualification = New System.Windows.Forms.Label
        Me.lvQualificationList = New eZee.Common.eZeeListView(Me.components)
        Me.colhQualifyGroupName = New System.Windows.Forms.ColumnHeader
        Me.colhQualificationName = New System.Windows.Forms.ColumnHeader
        Me.colhRefNo = New System.Windows.Forms.ColumnHeader
        Me.colhStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhInstitute = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhQualifyGroupunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhQualifyunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhTransdate = New System.Windows.Forms.ColumnHeader
        Me.objcolhInstituteId = New System.Windows.Forms.ColumnHeader
        Me.objcolhQualifyGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhResultCodeID = New System.Windows.Forms.ColumnHeader
        Me.objColhgpacode = New System.Windows.Forms.ColumnHeader
        Me.objColhCertiNo = New System.Windows.Forms.ColumnHeader
        Me.objcolhHighestQualification = New System.Windows.Forms.ColumnHeader
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.btnQualifyDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnQualifyEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnQualifyAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboQualificationGroup = New System.Windows.Forms.ComboBox
        Me.lblQualificationGroup = New System.Windows.Forms.Label
        Me.lblQualification = New System.Windows.Forms.Label
        Me.lblRemark = New System.Windows.Forms.Label
        Me.cboQualification = New System.Windows.Forms.ComboBox
        Me.txtQualificationRemark = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnAddInstitution = New eZee.Common.eZeeGradientButton
        Me.objbtnAddQulification = New eZee.Common.eZeeGradientButton
        Me.dtpQEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpQualifyDate = New System.Windows.Forms.DateTimePicker
        Me.lblAwardDate = New System.Windows.Forms.Label
        Me.txtReferenceNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblReferenceNo = New System.Windows.Forms.Label
        Me.dtpQStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.cboInstitution = New System.Windows.Forms.ComboBox
        Me.lblInstitution = New System.Windows.Forms.Label
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.tabpEmployment = New System.Windows.Forms.TabPage
        Me.pnlJobInfo = New System.Windows.Forms.Panel
        Me.gbJobHistory = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtJobAchievement = New eZee.TextBox.AlphanumericTextBox
        Me.lblJobAchievement = New System.Windows.Forms.Label
        Me.pnlJobHistory = New System.Windows.Forms.Panel
        Me.lvJobHistory = New System.Windows.Forms.ListView
        Me.colhEmployer = New System.Windows.Forms.ColumnHeader
        Me.colhCompanyName = New System.Windows.Forms.ColumnHeader
        Me.colhOfficePhone = New System.Windows.Forms.ColumnHeader
        Me.colhDesignation = New System.Windows.Forms.ColumnHeader
        Me.colhResponsibility = New System.Windows.Forms.ColumnHeader
        Me.colhJoinDate = New System.Windows.Forms.ColumnHeader
        Me.colhTermDate = New System.Windows.Forms.ColumnHeader
        Me.colhLeavingReason = New System.Windows.Forms.ColumnHeader
        Me.objcolhJobhistoryGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhJobAchievement = New System.Windows.Forms.ColumnHeader
        Me.btnJobHistoryDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.btnJobHistoryEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnJobHistoryAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtEmployerName = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployer = New System.Windows.Forms.Label
        Me.lblDesignation = New System.Windows.Forms.Label
        Me.dtpTerminationDate = New System.Windows.Forms.DateTimePicker
        Me.lblJoinDate = New System.Windows.Forms.Label
        Me.dtpJoinDate = New System.Windows.Forms.DateTimePicker
        Me.lblOfficePhone = New System.Windows.Forms.Label
        Me.txtLeavingReason = New eZee.TextBox.AlphanumericTextBox
        Me.lblResponsibility = New System.Windows.Forms.Label
        Me.txtResponsibility = New eZee.TextBox.AlphanumericTextBox
        Me.lblReason = New System.Windows.Forms.Label
        Me.txtDesignation = New eZee.TextBox.AlphanumericTextBox
        Me.lblOrgDescription = New System.Windows.Forms.Label
        Me.txtOfficePhone = New eZee.TextBox.AlphanumericTextBox
        Me.lblTermDate = New System.Windows.Forms.Label
        Me.txtCompanyName = New eZee.TextBox.AlphanumericTextBox
        Me.tabpReferences = New System.Windows.Forms.TabPage
        Me.gbReferences = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlReferences = New System.Windows.Forms.Panel
        Me.lvReferences = New System.Windows.Forms.ListView
        Me.colhRefName = New System.Windows.Forms.ColumnHeader
        Me.colhRefPosition = New System.Windows.Forms.ColumnHeader
        Me.colhRefGender = New System.Windows.Forms.ColumnHeader
        Me.colhRefType = New System.Windows.Forms.ColumnHeader
        Me.colhRefEmail = New System.Windows.Forms.ColumnHeader
        Me.objcolhRefAddress = New System.Windows.Forms.ColumnHeader
        Me.objcolhRefcountryId = New System.Windows.Forms.ColumnHeader
        Me.objcolhRefStateId = New System.Windows.Forms.ColumnHeader
        Me.objColhRefTownId = New System.Windows.Forms.ColumnHeader
        Me.objColhRefTypeId = New System.Windows.Forms.ColumnHeader
        Me.objcolhRefTelNo = New System.Windows.Forms.ColumnHeader
        Me.objColhRefMobileNo = New System.Windows.Forms.ColumnHeader
        Me.objColhRefGenderId = New System.Windows.Forms.ColumnHeader
        Me.objColhRefGUID = New System.Windows.Forms.ColumnHeader
        Me.btnRefEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblReferenceName = New System.Windows.Forms.Label
        Me.txtRefTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.btnRefDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblRefTelPhNo = New System.Windows.Forms.Label
        Me.txtRefPosition = New eZee.TextBox.AlphanumericTextBox
        Me.lblRefMobileNo = New System.Windows.Forms.Label
        Me.cboRefState = New System.Windows.Forms.ComboBox
        Me.lblPosition = New System.Windows.Forms.Label
        Me.txtRefMobileNo = New eZee.TextBox.AlphanumericTextBox
        Me.btnRefAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblRefState = New System.Windows.Forms.Label
        Me.lblRefAddress = New System.Windows.Forms.Label
        Me.lblRefEmail = New System.Windows.Forms.Label
        Me.cboRefCountry = New System.Windows.Forms.ComboBox
        Me.txtReferenceName = New eZee.TextBox.AlphanumericTextBox
        Me.txtRefEmail = New eZee.TextBox.AlphanumericTextBox
        Me.cboRefGender = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.txtRefAddress = New eZee.TextBox.AlphanumericTextBox
        Me.lblRefType = New System.Windows.Forms.Label
        Me.lblRefGender = New System.Windows.Forms.Label
        Me.cboReftown = New System.Windows.Forms.ComboBox
        Me.lblRefTown = New System.Windows.Forms.Label
        Me.cboRefType = New System.Windows.Forms.ComboBox
        Me.tabpOtherdetails = New System.Windows.Forms.TabPage
        Me.gbOtherDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboExpectedSalaryCurrency = New System.Windows.Forms.ComboBox
        Me.cboCurrentSalaryCurrency = New System.Windows.Forms.ComboBox
        Me.lblExpectedBenefit = New System.Windows.Forms.Label
        Me.txtExpectedBenefit = New eZee.TextBox.AlphanumericTextBox
        Me.txtImpairment = New eZee.TextBox.AlphanumericTextBox
        Me.chkImpaired = New System.Windows.Forms.CheckBox
        Me.chkWillingToTravel = New System.Windows.Forms.CheckBox
        Me.chkWillingToRelocate = New System.Windows.Forms.CheckBox
        Me.txtNoticePeriodDays = New eZee.TextBox.NumericTextBox
        Me.lblNoticePeriodDays = New System.Windows.Forms.Label
        Me.txtExpectedSalary = New eZee.TextBox.NumericTextBox
        Me.lblExpectedSalary = New System.Windows.Forms.Label
        Me.txtCurrentSalary = New eZee.TextBox.NumericTextBox
        Me.lblCurrentSalary = New System.Windows.Forms.Label
        Me.txtJournalsResearchPapers = New eZee.TextBox.AlphanumericTextBox
        Me.txtAchievements = New eZee.TextBox.AlphanumericTextBox
        Me.txtMemberships = New eZee.TextBox.AlphanumericTextBox
        Me.lblJournalsResearchPapers = New System.Windows.Forms.Label
        Me.lblAchievements = New System.Windows.Forms.Label
        Me.lblMemberships = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSaveInfo = New eZee.Common.eZeeLightButton(Me.components)
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader8 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader9 = New System.Windows.Forms.ColumnHeader
        Me.pnlNida = New System.Windows.Forms.Panel
        Me.pnlPresentAddress = New System.Windows.Forms.Panel
        Me.pnlPermanentAddress = New System.Windows.Forms.Panel
        Me.fpnlData = New System.Windows.Forms.FlowLayoutPanel
        Me.pnlMainInfo.SuspendLayout()
        Me.gbApplicantInfo.SuspendLayout()
        Me.tabcApplicantInformation.SuspendLayout()
        Me.tabpPersonalInfo.SuspendLayout()
        Me.pnlPersonalInfo.SuspendLayout()
        Me.gbPrsonalInfo.SuspendLayout()
        Me.pnlPersonalInfoinner.SuspendLayout()
        Me.tabpAdditionalInfo.SuspendLayout()
        Me.pnlAdditionalInfo.SuspendLayout()
        Me.gbSkillInfo.SuspendLayout()
        Me.pnlOtherSkill.SuspendLayout()
        Me.pnlSkillInfo.SuspendLayout()
        Me.gbOtherInfo.SuspendLayout()
        Me.tabpEducationalInfo.SuspendLayout()
        Me.pnlQualificationDetail.SuspendLayout()
        Me.gbQualificationInfo.SuspendLayout()
        CType(Me.nudGPA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlQualifyInfo.SuspendLayout()
        Me.pnlOtherQualification.SuspendLayout()
        Me.tabpEmployment.SuspendLayout()
        Me.pnlJobInfo.SuspendLayout()
        Me.gbJobHistory.SuspendLayout()
        Me.pnlJobHistory.SuspendLayout()
        Me.tabpReferences.SuspendLayout()
        Me.gbReferences.SuspendLayout()
        Me.pnlReferences.SuspendLayout()
        Me.tabpOtherdetails.SuspendLayout()
        Me.gbOtherDetails.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlNida.SuspendLayout()
        Me.pnlPresentAddress.SuspendLayout()
        Me.pnlPermanentAddress.SuspendLayout()
        Me.fpnlData.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbApplicantInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(890, 622)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbApplicantInfo
        '
        Me.gbApplicantInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbApplicantInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.Checked = False
        Me.gbApplicantInfo.CollapseAllExceptThis = False
        Me.gbApplicantInfo.CollapsedHoverImage = Nothing
        Me.gbApplicantInfo.CollapsedNormalImage = Nothing
        Me.gbApplicantInfo.CollapsedPressedImage = Nothing
        Me.gbApplicantInfo.CollapseOnLoad = False
        Me.gbApplicantInfo.Controls.Add(Me.txtVacancy)
        Me.gbApplicantInfo.Controls.Add(Me.lblEmployeeCode)
        Me.gbApplicantInfo.Controls.Add(Me.objbtnAddVacancy)
        Me.gbApplicantInfo.Controls.Add(Me.cboGender)
        Me.gbApplicantInfo.Controls.Add(Me.lblGender)
        Me.gbApplicantInfo.Controls.Add(Me.txtEmployeeCode)
        Me.gbApplicantInfo.Controls.Add(Me.cboVacancyType)
        Me.gbApplicantInfo.Controls.Add(Me.lblVacancyType)
        Me.gbApplicantInfo.Controls.Add(Me.lblVacancy)
        Me.gbApplicantInfo.Controls.Add(Me.lblTitle)
        Me.gbApplicantInfo.Controls.Add(Me.txtApplicantCode)
        Me.gbApplicantInfo.Controls.Add(Me.cboTitle)
        Me.gbApplicantInfo.Controls.Add(Me.lblApplicantCode)
        Me.gbApplicantInfo.Controls.Add(Me.objelLine1)
        Me.gbApplicantInfo.Controls.Add(Me.imgImageControl)
        Me.gbApplicantInfo.Controls.Add(Me.lblOthername)
        Me.gbApplicantInfo.Controls.Add(Me.lblApplicantFirstname)
        Me.gbApplicantInfo.Controls.Add(Me.lblSurname)
        Me.gbApplicantInfo.Controls.Add(Me.lblEmail)
        Me.gbApplicantInfo.Controls.Add(Me.txtEmail)
        Me.gbApplicantInfo.Controls.Add(Me.txtApplcantFirstName)
        Me.gbApplicantInfo.Controls.Add(Me.txtApplicantOtherName)
        Me.gbApplicantInfo.Controls.Add(Me.txtApplicantSurname)
        Me.gbApplicantInfo.Controls.Add(Me.tabcApplicantInformation)
        Me.gbApplicantInfo.ExpandedHoverImage = Nothing
        Me.gbApplicantInfo.ExpandedNormalImage = Nothing
        Me.gbApplicantInfo.ExpandedPressedImage = Nothing
        Me.gbApplicantInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplicantInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApplicantInfo.HeaderHeight = 25
        Me.gbApplicantInfo.HeaderMessage = ""
        Me.gbApplicantInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApplicantInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.HeightOnCollapse = 0
        Me.gbApplicantInfo.LeftTextSpace = 0
        Me.gbApplicantInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbApplicantInfo.Name = "gbApplicantInfo"
        Me.gbApplicantInfo.OpenHeight = 534
        Me.gbApplicantInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApplicantInfo.ShowBorder = True
        Me.gbApplicantInfo.ShowCheckBox = False
        Me.gbApplicantInfo.ShowCollapseButton = False
        Me.gbApplicantInfo.ShowDefaultBorderColor = True
        Me.gbApplicantInfo.ShowDownButton = False
        Me.gbApplicantInfo.ShowHeader = True
        Me.gbApplicantInfo.Size = New System.Drawing.Size(890, 559)
        Me.gbApplicantInfo.TabIndex = 0
        Me.gbApplicantInfo.Temp = 0
        Me.gbApplicantInfo.Text = "Applicant Infomation"
        Me.gbApplicantInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVacancy
        '
        Me.txtVacancy.BackColor = System.Drawing.Color.White
        Me.txtVacancy.Flags = 0
        Me.txtVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVacancy.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVacancy.Location = New System.Drawing.Point(375, 87)
        Me.txtVacancy.Name = "txtVacancy"
        Me.txtVacancy.ReadOnly = True
        Me.txtVacancy.Size = New System.Drawing.Size(269, 21)
        Me.txtVacancy.TabIndex = 35
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(480, 62)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(65, 15)
        Me.lblEmployeeCode.TabIndex = 14
        Me.lblEmployeeCode.Text = "Emp. Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddVacancy
        '
        Me.objbtnAddVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddVacancy.BorderSelected = False
        Me.objbtnAddVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddVacancy.Image = CType(resources.GetObject("objbtnAddVacancy.Image"), System.Drawing.Image)
        Me.objbtnAddVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddVacancy.Location = New System.Drawing.Point(648, 87)
        Me.objbtnAddVacancy.Name = "objbtnAddVacancy"
        Me.objbtnAddVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddVacancy.TabIndex = 18
        Me.objbtnAddVacancy.Visible = False
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(375, 114)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(99, 21)
        Me.cboGender.TabIndex = 20
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(292, 117)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(77, 15)
        Me.lblGender.TabIndex = 19
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeeCode
        '
        Me.txtEmployeeCode.Flags = 0
        Me.txtEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeCode.Location = New System.Drawing.Point(551, 59)
        Me.txtEmployeeCode.Name = "txtEmployeeCode"
        Me.txtEmployeeCode.Size = New System.Drawing.Size(93, 21)
        Me.txtEmployeeCode.TabIndex = 15
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(375, 60)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(99, 21)
        Me.cboVacancyType.TabIndex = 13
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(292, 63)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(77, 15)
        Me.lblVacancyType.TabIndex = 12
        Me.lblVacancyType.Text = "App. Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(292, 90)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(77, 15)
        Me.lblVacancy.TabIndex = 16
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(183, 36)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(42, 15)
        Me.lblTitle.TabIndex = 2
        Me.lblTitle.Text = "Title"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApplicantCode
        '
        Me.txtApplicantCode.Flags = 0
        Me.txtApplicantCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicantCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicantCode.Location = New System.Drawing.Point(93, 33)
        Me.txtApplicantCode.Name = "txtApplicantCode"
        Me.txtApplicantCode.Size = New System.Drawing.Size(84, 21)
        Me.txtApplicantCode.TabIndex = 1
        '
        'cboTitle
        '
        Me.cboTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitle.DropDownWidth = 100
        Me.cboTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTitle.FormattingEnabled = True
        Me.cboTitle.Items.AddRange(New Object() {"Mr.", "Miss.", "Jr.", "Sr."})
        Me.cboTitle.Location = New System.Drawing.Point(231, 33)
        Me.cboTitle.Name = "cboTitle"
        Me.cboTitle.Size = New System.Drawing.Size(55, 21)
        Me.cboTitle.TabIndex = 3
        '
        'lblApplicantCode
        '
        Me.lblApplicantCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicantCode.Location = New System.Drawing.Point(12, 36)
        Me.lblApplicantCode.Name = "lblApplicantCode"
        Me.lblApplicantCode.Size = New System.Drawing.Size(75, 15)
        Me.lblApplicantCode.TabIndex = 0
        Me.lblApplicantCode.Text = "App. Code"
        Me.lblApplicantCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(7, 138)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(665, 11)
        Me.objelLine1.TabIndex = 33
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'imgImageControl
        '
        Me.imgImageControl._FileMode = False
        Me.imgImageControl._Image = Nothing
        Me.imgImageControl._ShowAddButton = True
        Me.imgImageControl._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.imgImageControl._ShowDeleteButton = True
        Me.imgImageControl.BackColor = System.Drawing.Color.Transparent
        Me.imgImageControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgImageControl.Location = New System.Drawing.Point(675, 33)
        Me.imgImageControl.Name = "imgImageControl"
        Me.imgImageControl.Size = New System.Drawing.Size(153, 125)
        Me.imgImageControl.TabIndex = 21
        '
        'lblOthername
        '
        Me.lblOthername.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOthername.Location = New System.Drawing.Point(12, 117)
        Me.lblOthername.Name = "lblOthername"
        Me.lblOthername.Size = New System.Drawing.Size(75, 15)
        Me.lblOthername.TabIndex = 8
        Me.lblOthername.Text = "Other Name"
        Me.lblOthername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApplicantFirstname
        '
        Me.lblApplicantFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicantFirstname.Location = New System.Drawing.Point(12, 90)
        Me.lblApplicantFirstname.Name = "lblApplicantFirstname"
        Me.lblApplicantFirstname.Size = New System.Drawing.Size(75, 15)
        Me.lblApplicantFirstname.TabIndex = 6
        Me.lblApplicantFirstname.Text = "First Name"
        Me.lblApplicantFirstname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSurname
        '
        Me.lblSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurname.Location = New System.Drawing.Point(12, 63)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(75, 15)
        Me.lblSurname.TabIndex = 4
        Me.lblSurname.Text = "Surname"
        Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(292, 36)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(77, 15)
        Me.lblEmail.TabIndex = 10
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(375, 33)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(269, 21)
        Me.txtEmail.TabIndex = 11
        '
        'txtApplcantFirstName
        '
        Me.txtApplcantFirstName.Flags = 0
        Me.txtApplcantFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplcantFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplcantFirstName.Location = New System.Drawing.Point(93, 87)
        Me.txtApplcantFirstName.Name = "txtApplcantFirstName"
        Me.txtApplcantFirstName.Size = New System.Drawing.Size(193, 21)
        Me.txtApplcantFirstName.TabIndex = 7
        '
        'txtApplicantOtherName
        '
        Me.txtApplicantOtherName.Flags = 0
        Me.txtApplicantOtherName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicantOtherName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicantOtherName.Location = New System.Drawing.Point(93, 114)
        Me.txtApplicantOtherName.Name = "txtApplicantOtherName"
        Me.txtApplicantOtherName.Size = New System.Drawing.Size(193, 21)
        Me.txtApplicantOtherName.TabIndex = 9
        '
        'txtApplicantSurname
        '
        Me.txtApplicantSurname.Flags = 0
        Me.txtApplicantSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicantSurname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicantSurname.Location = New System.Drawing.Point(93, 60)
        Me.txtApplicantSurname.Name = "txtApplicantSurname"
        Me.txtApplicantSurname.Size = New System.Drawing.Size(193, 21)
        Me.txtApplicantSurname.TabIndex = 5
        '
        'tabcApplicantInformation
        '
        Me.tabcApplicantInformation.Controls.Add(Me.tabpPersonalInfo)
        Me.tabcApplicantInformation.Controls.Add(Me.tabpAdditionalInfo)
        Me.tabcApplicantInformation.Controls.Add(Me.tabpEducationalInfo)
        Me.tabcApplicantInformation.Controls.Add(Me.tabpEmployment)
        Me.tabcApplicantInformation.Controls.Add(Me.tabpReferences)
        Me.tabcApplicantInformation.Controls.Add(Me.tabpOtherdetails)
        Me.tabcApplicantInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcApplicantInformation.Location = New System.Drawing.Point(3, 156)
        Me.tabcApplicantInformation.Name = "tabcApplicantInformation"
        Me.tabcApplicantInformation.SelectedIndex = 0
        Me.tabcApplicantInformation.Size = New System.Drawing.Size(887, 403)
        Me.tabcApplicantInformation.TabIndex = 8
        '
        'tabpPersonalInfo
        '
        Me.tabpPersonalInfo.Controls.Add(Me.pnlPersonalInfo)
        Me.tabpPersonalInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpPersonalInfo.Name = "tabpPersonalInfo"
        Me.tabpPersonalInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpPersonalInfo.Size = New System.Drawing.Size(879, 377)
        Me.tabpPersonalInfo.TabIndex = 0
        Me.tabpPersonalInfo.Text = "Address"
        Me.tabpPersonalInfo.UseVisualStyleBackColor = True
        '
        'pnlPersonalInfo
        '
        Me.pnlPersonalInfo.Controls.Add(Me.gbPrsonalInfo)
        Me.pnlPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPersonalInfo.Location = New System.Drawing.Point(3, 3)
        Me.pnlPersonalInfo.Name = "pnlPersonalInfo"
        Me.pnlPersonalInfo.Size = New System.Drawing.Size(873, 371)
        Me.pnlPersonalInfo.TabIndex = 0
        '
        'gbPrsonalInfo
        '
        Me.gbPrsonalInfo.BorderColor = System.Drawing.Color.Black
        Me.gbPrsonalInfo.Checked = False
        Me.gbPrsonalInfo.CollapseAllExceptThis = False
        Me.gbPrsonalInfo.CollapsedHoverImage = Nothing
        Me.gbPrsonalInfo.CollapsedNormalImage = Nothing
        Me.gbPrsonalInfo.CollapsedPressedImage = Nothing
        Me.gbPrsonalInfo.CollapseOnLoad = False
        Me.gbPrsonalInfo.Controls.Add(Me.pnlPersonalInfoinner)
        Me.gbPrsonalInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbPrsonalInfo.ExpandedHoverImage = Nothing
        Me.gbPrsonalInfo.ExpandedNormalImage = Nothing
        Me.gbPrsonalInfo.ExpandedPressedImage = Nothing
        Me.gbPrsonalInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPrsonalInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPrsonalInfo.HeaderHeight = 25
        Me.gbPrsonalInfo.HeaderMessage = ""
        Me.gbPrsonalInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPrsonalInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPrsonalInfo.HeightOnCollapse = 0
        Me.gbPrsonalInfo.LeftTextSpace = 0
        Me.gbPrsonalInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbPrsonalInfo.Name = "gbPrsonalInfo"
        Me.gbPrsonalInfo.OpenHeight = 358
        Me.gbPrsonalInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPrsonalInfo.ShowBorder = True
        Me.gbPrsonalInfo.ShowCheckBox = False
        Me.gbPrsonalInfo.ShowCollapseButton = False
        Me.gbPrsonalInfo.ShowDefaultBorderColor = True
        Me.gbPrsonalInfo.ShowDownButton = False
        Me.gbPrsonalInfo.ShowHeader = True
        Me.gbPrsonalInfo.Size = New System.Drawing.Size(873, 371)
        Me.gbPrsonalInfo.TabIndex = 0
        Me.gbPrsonalInfo.Temp = 0
        Me.gbPrsonalInfo.Text = "Personal Info"
        Me.gbPrsonalInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlPersonalInfoinner
        '
        Me.pnlPersonalInfoinner.AutoScroll = True
        Me.pnlPersonalInfoinner.Controls.Add(Me.fpnlData)
        Me.pnlPersonalInfoinner.Location = New System.Drawing.Point(3, 26)
        Me.pnlPersonalInfoinner.Name = "pnlPersonalInfoinner"
        Me.pnlPersonalInfoinner.Size = New System.Drawing.Size(865, 340)
        Me.pnlPersonalInfoinner.TabIndex = 472
        '
        'txtIndexNo
        '
        Me.txtIndexNo.Flags = 0
        Me.txtIndexNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIndexNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtIndexNo.Location = New System.Drawing.Point(123, 80)
        Me.txtIndexNo.Name = "txtIndexNo"
        Me.txtIndexNo.Size = New System.Drawing.Size(147, 21)
        Me.txtIndexNo.TabIndex = 541
        '
        'lblIndexNumber
        '
        Me.lblIndexNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIndexNumber.Location = New System.Drawing.Point(44, 83)
        Me.lblIndexNumber.Name = "lblIndexNumber"
        Me.lblIndexNumber.Size = New System.Drawing.Size(73, 15)
        Me.lblIndexNumber.TabIndex = 542
        Me.lblIndexNumber.Text = "Index No."
        Me.lblIndexNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResidencyType
        '
        Me.cboResidencyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidencyType.DropDownWidth = 200
        Me.cboResidencyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidencyType.FormattingEnabled = True
        Me.cboResidencyType.Location = New System.Drawing.Point(606, 53)
        Me.cboResidencyType.Name = "cboResidencyType"
        Me.cboResidencyType.Size = New System.Drawing.Size(147, 21)
        Me.cboResidencyType.TabIndex = 539
        '
        'lblResidencyType
        '
        Me.lblResidencyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidencyType.Location = New System.Drawing.Point(527, 56)
        Me.lblResidencyType.Name = "lblResidencyType"
        Me.lblResidencyType.Size = New System.Drawing.Size(73, 15)
        Me.lblResidencyType.TabIndex = 540
        Me.lblResidencyType.Text = "Residency"
        Me.lblResidencyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPhoneNumber
        '
        Me.txtPhoneNumber.Flags = 0
        Me.txtPhoneNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhoneNumber.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhoneNumber.Location = New System.Drawing.Point(361, 53)
        Me.txtPhoneNumber.Name = "txtPhoneNumber"
        Me.txtPhoneNumber.Size = New System.Drawing.Size(147, 21)
        Me.txtPhoneNumber.TabIndex = 537
        '
        'lblPhone1
        '
        Me.lblPhone1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone1.Location = New System.Drawing.Point(282, 56)
        Me.lblPhone1.Name = "lblPhone1"
        Me.lblPhone1.Size = New System.Drawing.Size(73, 15)
        Me.lblPhone1.TabIndex = 538
        Me.lblPhone1.Text = "Phonenumber"
        Me.lblPhone1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVillage
        '
        Me.txtVillage.Flags = 0
        Me.txtVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVillage.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVillage.Location = New System.Drawing.Point(123, 53)
        Me.txtVillage.Name = "txtVillage"
        Me.txtVillage.Size = New System.Drawing.Size(147, 21)
        Me.txtVillage.TabIndex = 535
        '
        'lblVillage
        '
        Me.lblVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVillage.Location = New System.Drawing.Point(44, 56)
        Me.lblVillage.Name = "lblVillage"
        Me.lblVillage.Size = New System.Drawing.Size(73, 15)
        Me.lblVillage.TabIndex = 536
        Me.lblVillage.Text = "Village"
        Me.lblVillage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNidaMobile
        '
        Me.txtNidaMobile.Flags = 0
        Me.txtNidaMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNidaMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNidaMobile.Location = New System.Drawing.Point(606, 26)
        Me.txtNidaMobile.Name = "txtNidaMobile"
        Me.txtNidaMobile.ReadOnly = True
        Me.txtNidaMobile.Size = New System.Drawing.Size(147, 21)
        Me.txtNidaMobile.TabIndex = 533
        '
        'lblNidaMobile
        '
        Me.lblNidaMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNidaMobile.Location = New System.Drawing.Point(527, 29)
        Me.lblNidaMobile.Name = "lblNidaMobile"
        Me.lblNidaMobile.Size = New System.Drawing.Size(73, 15)
        Me.lblNidaMobile.TabIndex = 534
        Me.lblNidaMobile.Text = "Nida Mobile"
        Me.lblNidaMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTin
        '
        Me.txtTin.Flags = 0
        Me.txtTin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTin.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTin.Location = New System.Drawing.Point(361, 26)
        Me.txtTin.Name = "txtTin"
        Me.txtTin.Size = New System.Drawing.Size(147, 21)
        Me.txtTin.TabIndex = 531
        '
        'lblTIN
        '
        Me.lblTIN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTIN.Location = New System.Drawing.Point(282, 29)
        Me.lblTIN.Name = "lblTIN"
        Me.lblTIN.Size = New System.Drawing.Size(73, 15)
        Me.lblTIN.TabIndex = 532
        Me.lblTIN.Text = "Tin Number"
        Me.lblTIN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNidaNumber
        '
        Me.txtNidaNumber.Flags = 0
        Me.txtNidaNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNidaNumber.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNidaNumber.Location = New System.Drawing.Point(123, 26)
        Me.txtNidaNumber.Name = "txtNidaNumber"
        Me.txtNidaNumber.ReadOnly = True
        Me.txtNidaNumber.Size = New System.Drawing.Size(147, 21)
        Me.txtNidaNumber.TabIndex = 529
        '
        'lblNin
        '
        Me.lblNin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNin.Location = New System.Drawing.Point(44, 29)
        Me.lblNin.Name = "lblNin"
        Me.lblNin.Size = New System.Drawing.Size(73, 15)
        Me.lblNin.TabIndex = 530
        Me.lblNin.Text = "Nida Number"
        Me.lblNin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elBasicInfo
        '
        Me.elBasicInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elBasicInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elBasicInfo.Location = New System.Drawing.Point(26, 5)
        Me.elBasicInfo.Name = "elBasicInfo"
        Me.elBasicInfo.Size = New System.Drawing.Size(262, 18)
        Me.elBasicInfo.TabIndex = 528
        Me.elBasicInfo.Text = "Basic Information"
        '
        'cboPostTown
        '
        Me.cboPostTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostTown.DropDownWidth = 200
        Me.cboPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPostTown.FormattingEnabled = True
        Me.cboPostTown.Location = New System.Drawing.Point(361, 83)
        Me.cboPostTown.Name = "cboPostTown"
        Me.cboPostTown.Size = New System.Drawing.Size(147, 21)
        Me.cboPostTown.TabIndex = 478
        '
        'cboPermCountry
        '
        Me.cboPermCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPermCountry.DropDownWidth = 200
        Me.cboPermCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPermCountry.FormattingEnabled = True
        Me.cboPermCountry.Location = New System.Drawing.Point(361, 31)
        Me.cboPermCountry.Name = "cboPermCountry"
        Me.cboPermCountry.Size = New System.Drawing.Size(147, 21)
        Me.cboPermCountry.TabIndex = 490
        '
        'lblPermPostCountry
        '
        Me.lblPermPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermPostCountry.Location = New System.Drawing.Point(275, 34)
        Me.lblPermPostCountry.Name = "lblPermPostCountry"
        Me.lblPermPostCountry.Size = New System.Drawing.Size(71, 15)
        Me.lblPermPostCountry.TabIndex = 517
        Me.lblPermPostCountry.Text = "Post Country"
        Me.lblPermPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.DropDownWidth = 200
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(361, 29)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(147, 21)
        Me.cboCountry.TabIndex = 476
        '
        'lblPermPostCode
        '
        Me.lblPermPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermPostCode.Location = New System.Drawing.Point(275, 115)
        Me.lblPermPostCode.Name = "lblPermPostCode"
        Me.lblPermPostCode.Size = New System.Drawing.Size(71, 15)
        Me.lblPermPostCode.TabIndex = 518
        Me.lblPermPostCode.Text = "Post Code"
        Me.lblPermPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostcode
        '
        Me.lblPostcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostcode.Location = New System.Drawing.Point(275, 114)
        Me.lblPostcode.Name = "lblPostcode"
        Me.lblPostcode.Size = New System.Drawing.Size(73, 15)
        Me.lblPostcode.TabIndex = 508
        Me.lblPostcode.Text = "Post Code"
        Me.lblPostcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPermState
        '
        Me.lblPermState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermState.Location = New System.Drawing.Point(275, 61)
        Me.lblPermState.Name = "lblPermState"
        Me.lblPermState.Size = New System.Drawing.Size(78, 15)
        Me.lblPermState.TabIndex = 519
        Me.lblPermState.Text = "State/Province"
        Me.lblPermState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPermPostTown
        '
        Me.cboPermPostTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPermPostTown.DropDownWidth = 200
        Me.cboPermPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPermPostTown.FormattingEnabled = True
        Me.cboPermPostTown.Location = New System.Drawing.Point(361, 85)
        Me.cboPermPostTown.Name = "cboPermPostTown"
        Me.cboPermPostTown.Size = New System.Drawing.Size(147, 21)
        Me.cboPermPostTown.TabIndex = 492
        '
        'cboPermPostCode
        '
        Me.cboPermPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPermPostCode.DropDownWidth = 200
        Me.cboPermPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPermPostCode.FormattingEnabled = True
        Me.cboPermPostCode.Location = New System.Drawing.Point(361, 112)
        Me.cboPermPostCode.Name = "cboPermPostCode"
        Me.cboPermPostCode.Size = New System.Drawing.Size(147, 21)
        Me.cboPermPostCode.TabIndex = 493
        '
        'cboPostCode
        '
        Me.cboPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostCode.DropDownWidth = 200
        Me.cboPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPostCode.FormattingEnabled = True
        Me.cboPostCode.Location = New System.Drawing.Point(361, 111)
        Me.cboPostCode.Name = "cboPostCode"
        Me.cboPostCode.Size = New System.Drawing.Size(147, 21)
        Me.cboPostCode.TabIndex = 479
        '
        'cboPermState
        '
        Me.cboPermState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPermState.DropDownWidth = 200
        Me.cboPermState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPermState.FormattingEnabled = True
        Me.cboPermState.Location = New System.Drawing.Point(361, 58)
        Me.cboPermState.Name = "cboPermState"
        Me.cboPermState.Size = New System.Drawing.Size(147, 21)
        Me.cboPermState.TabIndex = 491
        '
        'lblPostCountry
        '
        Me.lblPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostCountry.Location = New System.Drawing.Point(275, 32)
        Me.lblPostCountry.Name = "lblPostCountry"
        Me.lblPostCountry.Size = New System.Drawing.Size(73, 15)
        Me.lblPostCountry.TabIndex = 509
        Me.lblPostCountry.Text = "Post Country"
        Me.lblPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.DropDownWidth = 200
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(361, 56)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(147, 21)
        Me.cboState.TabIndex = 477
        '
        'lnkCopyAddress
        '
        Me.lnkCopyAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCopyAddress.Location = New System.Drawing.Point(294, 10)
        Me.lnkCopyAddress.Name = "lnkCopyAddress"
        Me.lnkCopyAddress.Size = New System.Drawing.Size(81, 15)
        Me.lnkCopyAddress.TabIndex = 527
        Me.lnkCopyAddress.TabStop = True
        Me.lnkCopyAddress.Text = "Copy Address"
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(275, 59)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(78, 15)
        Me.lblState.TabIndex = 510
        Me.lblState.Text = "State/Province"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPermFax
        '
        Me.txtPermFax.Flags = 0
        Me.txtPermFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermFax.Location = New System.Drawing.Point(606, 112)
        Me.txtPermFax.Name = "txtPermFax"
        Me.txtPermFax.Size = New System.Drawing.Size(147, 21)
        Me.txtPermFax.TabIndex = 498
        '
        'txtPermAltNo
        '
        Me.txtPermAltNo.Flags = 0
        Me.txtPermAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermAltNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermAltNo.Location = New System.Drawing.Point(606, 58)
        Me.txtPermAltNo.Name = "txtPermAltNo"
        Me.txtPermAltNo.Size = New System.Drawing.Size(147, 21)
        Me.txtPermAltNo.TabIndex = 496
        '
        'lblPermFax
        '
        Me.lblPermFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermFax.Location = New System.Drawing.Point(517, 115)
        Me.lblPermFax.Name = "lblPermFax"
        Me.lblPermFax.Size = New System.Drawing.Size(83, 15)
        Me.lblPermFax.TabIndex = 515
        Me.lblPermFax.Text = "Fax"
        Me.lblPermFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPermAltNo
        '
        Me.lblPermAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermAltNo.Location = New System.Drawing.Point(517, 61)
        Me.lblPermAltNo.Name = "lblPermAltNo"
        Me.lblPermAltNo.Size = New System.Drawing.Size(83, 15)
        Me.lblPermAltNo.TabIndex = 526
        Me.lblPermAltNo.Text = "Alternative No"
        Me.lblPermAltNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPermTelNo
        '
        Me.txtPermTelNo.BackColor = System.Drawing.Color.White
        Me.txtPermTelNo.Flags = 0
        Me.txtPermTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermTelNo.Location = New System.Drawing.Point(606, 85)
        Me.txtPermTelNo.Name = "txtPermTelNo"
        Me.txtPermTelNo.Size = New System.Drawing.Size(147, 21)
        Me.txtPermTelNo.TabIndex = 497
        '
        'lblPermTelNo
        '
        Me.lblPermTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermTelNo.Location = New System.Drawing.Point(517, 88)
        Me.lblPermTelNo.Name = "lblPermTelNo"
        Me.lblPermTelNo.Size = New System.Drawing.Size(83, 15)
        Me.lblPermTelNo.TabIndex = 514
        Me.lblPermTelNo.Text = "Tel. No"
        Me.lblPermTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPermMobile
        '
        Me.txtPermMobile.Flags = 0
        Me.txtPermMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermMobile.Location = New System.Drawing.Point(606, 31)
        Me.txtPermMobile.Name = "txtPermMobile"
        Me.txtPermMobile.Size = New System.Drawing.Size(147, 21)
        Me.txtPermMobile.TabIndex = 495
        '
        'lblPermMobileNo
        '
        Me.lblPermMobileNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermMobileNo.Location = New System.Drawing.Point(517, 34)
        Me.lblPermMobileNo.Name = "lblPermMobileNo"
        Me.lblPermMobileNo.Size = New System.Drawing.Size(83, 15)
        Me.lblPermMobileNo.TabIndex = 525
        Me.lblPermMobileNo.Text = "Mobile"
        Me.lblPermMobileNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPermPlotNo
        '
        Me.txtPermPlotNo.Flags = 0
        Me.txtPermPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermPlotNo.Location = New System.Drawing.Point(123, 85)
        Me.txtPermPlotNo.Name = "txtPermPlotNo"
        Me.txtPermPlotNo.Size = New System.Drawing.Size(147, 21)
        Me.txtPermPlotNo.TabIndex = 487
        '
        'lblPermPlotNo
        '
        Me.lblPermPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermPlotNo.Location = New System.Drawing.Point(44, 88)
        Me.lblPermPlotNo.Name = "lblPermPlotNo"
        Me.lblPermPlotNo.Size = New System.Drawing.Size(71, 15)
        Me.lblPermPlotNo.TabIndex = 524
        Me.lblPermPlotNo.Text = "Plot No"
        Me.lblPermPlotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPermProvince
        '
        Me.txtPermProvince.Flags = 0
        Me.txtPermProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermProvince.Location = New System.Drawing.Point(361, 139)
        Me.txtPermProvince.Name = "txtPermProvince"
        Me.txtPermProvince.Size = New System.Drawing.Size(147, 21)
        Me.txtPermProvince.TabIndex = 494
        '
        'lblPermProvince
        '
        Me.lblPermProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermProvince.Location = New System.Drawing.Point(275, 142)
        Me.lblPermProvince.Name = "lblPermProvince"
        Me.lblPermProvince.Size = New System.Drawing.Size(71, 15)
        Me.lblPermProvince.TabIndex = 523
        Me.lblPermProvince.Text = "Region/Area"
        Me.lblPermProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPermEstate
        '
        Me.txtPermEstate.Flags = 0
        Me.txtPermEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermEstate.Location = New System.Drawing.Point(123, 112)
        Me.txtPermEstate.Name = "txtPermEstate"
        Me.txtPermEstate.Size = New System.Drawing.Size(147, 21)
        Me.txtPermEstate.TabIndex = 488
        '
        'txtPermRoad
        '
        Me.txtPermRoad.Flags = 0
        Me.txtPermRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermRoad.Location = New System.Drawing.Point(123, 139)
        Me.txtPermRoad.Name = "txtPermRoad"
        Me.txtPermRoad.Size = New System.Drawing.Size(147, 21)
        Me.txtPermRoad.TabIndex = 489
        '
        'lblPermRoad
        '
        Me.lblPermRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermRoad.Location = New System.Drawing.Point(44, 142)
        Me.lblPermRoad.Name = "lblPermRoad"
        Me.lblPermRoad.Size = New System.Drawing.Size(71, 15)
        Me.lblPermRoad.TabIndex = 522
        Me.lblPermRoad.Text = "Road/Street"
        Me.lblPermRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPermEstate
        '
        Me.lblPermEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermEstate.Location = New System.Drawing.Point(44, 115)
        Me.lblPermEstate.Name = "lblPermEstate"
        Me.lblPermEstate.Size = New System.Drawing.Size(71, 15)
        Me.lblPermEstate.TabIndex = 521
        Me.lblPermEstate.Text = "Estate"
        Me.lblPermEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPermPostTown
        '
        Me.lblPermPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermPostTown.Location = New System.Drawing.Point(275, 88)
        Me.lblPermPostTown.Name = "lblPermPostTown"
        Me.lblPermPostTown.Size = New System.Drawing.Size(89, 15)
        Me.lblPermPostTown.TabIndex = 520
        Me.lblPermPostTown.Text = "Post Twn/District"
        Me.lblPermPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPermAddress2
        '
        Me.txtPermAddress2.Flags = 0
        Me.txtPermAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermAddress2.Location = New System.Drawing.Point(123, 58)
        Me.txtPermAddress2.Name = "txtPermAddress2"
        Me.txtPermAddress2.Size = New System.Drawing.Size(147, 21)
        Me.txtPermAddress2.TabIndex = 486
        '
        'txtPermAddress1
        '
        Me.txtPermAddress1.BackColor = System.Drawing.Color.White
        Me.txtPermAddress1.Flags = 0
        Me.txtPermAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPermAddress1.Location = New System.Drawing.Point(123, 31)
        Me.txtPermAddress1.Name = "txtPermAddress1"
        Me.txtPermAddress1.Size = New System.Drawing.Size(147, 21)
        Me.txtPermAddress1.TabIndex = 485
        '
        'lblPermAddress
        '
        Me.lblPermAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPermAddress.Location = New System.Drawing.Point(44, 34)
        Me.lblPermAddress.Name = "lblPermAddress"
        Me.lblPermAddress.Size = New System.Drawing.Size(71, 15)
        Me.lblPermAddress.TabIndex = 516
        Me.lblPermAddress.Text = "Address"
        Me.lblPermAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnPermanentAddress
        '
        Me.lnPermanentAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnPermanentAddress.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnPermanentAddress.Location = New System.Drawing.Point(26, 7)
        Me.lnPermanentAddress.Name = "lnPermanentAddress"
        Me.lnPermanentAddress.Size = New System.Drawing.Size(262, 18)
        Me.lnPermanentAddress.TabIndex = 513
        Me.lnPermanentAddress.Text = "Permanent Address"
        '
        'txtProvince
        '
        Me.txtProvince.Flags = 0
        Me.txtProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtProvince.Location = New System.Drawing.Point(361, 138)
        Me.txtProvince.Name = "txtProvince"
        Me.txtProvince.Size = New System.Drawing.Size(147, 21)
        Me.txtProvince.TabIndex = 480
        '
        'lblProvince
        '
        Me.lblProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvince.Location = New System.Drawing.Point(275, 141)
        Me.lblProvince.Name = "lblProvince"
        Me.lblProvince.Size = New System.Drawing.Size(73, 15)
        Me.lblProvince.TabIndex = 512
        Me.lblProvince.Text = "Region/Area"
        Me.lblProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostTown
        '
        Me.lblPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostTown.Location = New System.Drawing.Point(275, 86)
        Me.lblPostTown.Name = "lblPostTown"
        Me.lblPostTown.Size = New System.Drawing.Size(99, 15)
        Me.lblPostTown.TabIndex = 511
        Me.lblPostTown.Text = "Post Twn/District"
        Me.lblPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress1
        '
        Me.txtAddress1.Flags = 0
        Me.txtAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress1.Location = New System.Drawing.Point(123, 29)
        Me.txtAddress1.Name = "txtAddress1"
        Me.txtAddress1.Size = New System.Drawing.Size(147, 21)
        Me.txtAddress1.TabIndex = 471
        '
        'txtAddress2
        '
        Me.txtAddress2.Flags = 0
        Me.txtAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress2.Location = New System.Drawing.Point(123, 56)
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.Size = New System.Drawing.Size(147, 21)
        Me.txtAddress2.TabIndex = 472
        '
        'txtRoad
        '
        Me.txtRoad.Flags = 0
        Me.txtRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRoad.Location = New System.Drawing.Point(123, 137)
        Me.txtRoad.Name = "txtRoad"
        Me.txtRoad.Size = New System.Drawing.Size(147, 21)
        Me.txtRoad.TabIndex = 475
        '
        'lblRoad
        '
        Me.lblRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoad.Location = New System.Drawing.Point(44, 140)
        Me.lblRoad.Name = "lblRoad"
        Me.lblRoad.Size = New System.Drawing.Size(73, 15)
        Me.lblRoad.TabIndex = 507
        Me.lblRoad.Text = "Road/Street"
        Me.lblRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFax
        '
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(606, 110)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(147, 21)
        Me.txtFax.TabIndex = 484
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(517, 113)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(83, 15)
        Me.lblFax.TabIndex = 506
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAlternativeNo
        '
        Me.txtAlternativeNo.Flags = 0
        Me.txtAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlternativeNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAlternativeNo.Location = New System.Drawing.Point(606, 56)
        Me.txtAlternativeNo.Name = "txtAlternativeNo"
        Me.txtAlternativeNo.Size = New System.Drawing.Size(147, 21)
        Me.txtAlternativeNo.TabIndex = 482
        '
        'txtEstate
        '
        Me.txtEstate.Flags = 0
        Me.txtEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEstate.Location = New System.Drawing.Point(123, 110)
        Me.txtEstate.Name = "txtEstate"
        Me.txtEstate.Size = New System.Drawing.Size(147, 21)
        Me.txtEstate.TabIndex = 474
        '
        'lblEstate
        '
        Me.lblEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstate.Location = New System.Drawing.Point(44, 113)
        Me.lblEstate.Name = "lblEstate"
        Me.lblEstate.Size = New System.Drawing.Size(73, 15)
        Me.lblEstate.TabIndex = 505
        Me.lblEstate.Text = "Estate"
        Me.lblEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAlternativeNo
        '
        Me.lblAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlternativeNo.Location = New System.Drawing.Point(517, 59)
        Me.lblAlternativeNo.Name = "lblAlternativeNo"
        Me.lblAlternativeNo.Size = New System.Drawing.Size(83, 15)
        Me.lblAlternativeNo.TabIndex = 504
        Me.lblAlternativeNo.Text = "Alternative No"
        Me.lblAlternativeNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMobile
        '
        Me.txtMobile.Flags = 0
        Me.txtMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMobile.Location = New System.Drawing.Point(606, 29)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(147, 21)
        Me.txtMobile.TabIndex = 481
        '
        'txtPlotNo
        '
        Me.txtPlotNo.Flags = 0
        Me.txtPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPlotNo.Location = New System.Drawing.Point(123, 83)
        Me.txtPlotNo.Name = "txtPlotNo"
        Me.txtPlotNo.Size = New System.Drawing.Size(147, 21)
        Me.txtPlotNo.TabIndex = 473
        '
        'txtTelNo
        '
        Me.txtTelNo.BackColor = System.Drawing.Color.White
        Me.txtTelNo.Flags = 0
        Me.txtTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTelNo.Location = New System.Drawing.Point(606, 83)
        Me.txtTelNo.Name = "txtTelNo"
        Me.txtTelNo.Size = New System.Drawing.Size(147, 21)
        Me.txtTelNo.TabIndex = 483
        '
        'lblPloteNo
        '
        Me.lblPloteNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPloteNo.Location = New System.Drawing.Point(44, 86)
        Me.lblPloteNo.Name = "lblPloteNo"
        Me.lblPloteNo.Size = New System.Drawing.Size(73, 15)
        Me.lblPloteNo.TabIndex = 503
        Me.lblPloteNo.Text = "Plot No"
        Me.lblPloteNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMobile
        '
        Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMobile.Location = New System.Drawing.Point(517, 32)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(83, 15)
        Me.lblMobile.TabIndex = 502
        Me.lblMobile.Text = "Mobile"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTelNo
        '
        Me.lblTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelNo.Location = New System.Drawing.Point(517, 86)
        Me.lblTelNo.Name = "lblTelNo"
        Me.lblTelNo.Size = New System.Drawing.Size(83, 15)
        Me.lblTelNo.TabIndex = 501
        Me.lblTelNo.Text = "Tel. No"
        Me.lblTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(44, 32)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(73, 15)
        Me.lblAddress.TabIndex = 499
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnPresentAddress
        '
        Me.lnPresentAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnPresentAddress.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnPresentAddress.Location = New System.Drawing.Point(26, 8)
        Me.lnPresentAddress.Name = "lnPresentAddress"
        Me.lnPresentAddress.Size = New System.Drawing.Size(262, 18)
        Me.lnPresentAddress.TabIndex = 500
        Me.lnPresentAddress.Text = "Present Address"
        '
        'tabpAdditionalInfo
        '
        Me.tabpAdditionalInfo.Controls.Add(Me.pnlAdditionalInfo)
        Me.tabpAdditionalInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpAdditionalInfo.Name = "tabpAdditionalInfo"
        Me.tabpAdditionalInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpAdditionalInfo.Size = New System.Drawing.Size(879, 377)
        Me.tabpAdditionalInfo.TabIndex = 3
        Me.tabpAdditionalInfo.Text = "Additional Info"
        Me.tabpAdditionalInfo.UseVisualStyleBackColor = True
        '
        'pnlAdditionalInfo
        '
        Me.pnlAdditionalInfo.Controls.Add(Me.gbSkillInfo)
        Me.pnlAdditionalInfo.Controls.Add(Me.gbOtherInfo)
        Me.pnlAdditionalInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAdditionalInfo.Location = New System.Drawing.Point(3, 3)
        Me.pnlAdditionalInfo.Name = "pnlAdditionalInfo"
        Me.pnlAdditionalInfo.Size = New System.Drawing.Size(873, 371)
        Me.pnlAdditionalInfo.TabIndex = 0
        '
        'gbSkillInfo
        '
        Me.gbSkillInfo.BorderColor = System.Drawing.Color.Black
        Me.gbSkillInfo.Checked = False
        Me.gbSkillInfo.CollapseAllExceptThis = False
        Me.gbSkillInfo.CollapsedHoverImage = Nothing
        Me.gbSkillInfo.CollapsedNormalImage = Nothing
        Me.gbSkillInfo.CollapsedPressedImage = Nothing
        Me.gbSkillInfo.CollapseOnLoad = False
        Me.gbSkillInfo.Controls.Add(Me.cboSkillExpertise)
        Me.gbSkillInfo.Controls.Add(Me.lblSkillExpertise)
        Me.gbSkillInfo.Controls.Add(Me.lnkOtherSkill)
        Me.gbSkillInfo.Controls.Add(Me.pnlOtherSkill)
        Me.gbSkillInfo.Controls.Add(Me.btnDelete)
        Me.gbSkillInfo.Controls.Add(Me.btnEdit)
        Me.gbSkillInfo.Controls.Add(Me.btnAdd)
        Me.gbSkillInfo.Controls.Add(Me.pnlSkillInfo)
        Me.gbSkillInfo.Controls.Add(Me.objelSkill)
        Me.gbSkillInfo.Controls.Add(Me.objbtnAddSkill)
        Me.gbSkillInfo.Controls.Add(Me.cboSkillCategory)
        Me.gbSkillInfo.Controls.Add(Me.objbtnAddSkillCategory)
        Me.gbSkillInfo.Controls.Add(Me.lblSkillCategory)
        Me.gbSkillInfo.Controls.Add(Me.lblSkillName)
        Me.gbSkillInfo.Controls.Add(Me.cboSkill)
        Me.gbSkillInfo.Controls.Add(Me.txtRemark)
        Me.gbSkillInfo.Controls.Add(Me.lblDescription)
        Me.gbSkillInfo.ExpandedHoverImage = Nothing
        Me.gbSkillInfo.ExpandedNormalImage = Nothing
        Me.gbSkillInfo.ExpandedPressedImage = Nothing
        Me.gbSkillInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSkillInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSkillInfo.HeaderHeight = 25
        Me.gbSkillInfo.HeaderMessage = ""
        Me.gbSkillInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSkillInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSkillInfo.HeightOnCollapse = 0
        Me.gbSkillInfo.LeftTextSpace = 0
        Me.gbSkillInfo.Location = New System.Drawing.Point(212, 0)
        Me.gbSkillInfo.Name = "gbSkillInfo"
        Me.gbSkillInfo.OpenHeight = 358
        Me.gbSkillInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSkillInfo.ShowBorder = True
        Me.gbSkillInfo.ShowCheckBox = False
        Me.gbSkillInfo.ShowCollapseButton = False
        Me.gbSkillInfo.ShowDefaultBorderColor = True
        Me.gbSkillInfo.ShowDownButton = False
        Me.gbSkillInfo.ShowHeader = True
        Me.gbSkillInfo.Size = New System.Drawing.Size(656, 371)
        Me.gbSkillInfo.TabIndex = 3
        Me.gbSkillInfo.Temp = 0
        Me.gbSkillInfo.Text = "Skills Info"
        Me.gbSkillInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSkillExpertise
        '
        Me.cboSkillExpertise.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkillExpertise.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkillExpertise.FormattingEnabled = True
        Me.cboSkillExpertise.Location = New System.Drawing.Point(337, 60)
        Me.cboSkillExpertise.Name = "cboSkillExpertise"
        Me.cboSkillExpertise.Size = New System.Drawing.Size(121, 21)
        Me.cboSkillExpertise.TabIndex = 227
        '
        'lblSkillExpertise
        '
        Me.lblSkillExpertise.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillExpertise.Location = New System.Drawing.Point(271, 64)
        Me.lblSkillExpertise.Name = "lblSkillExpertise"
        Me.lblSkillExpertise.Size = New System.Drawing.Size(60, 15)
        Me.lblSkillExpertise.TabIndex = 226
        Me.lblSkillExpertise.Text = "Expertise"
        Me.lblSkillExpertise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkOtherSkill
        '
        Me.lnkOtherSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkOtherSkill.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkOtherSkill.Location = New System.Drawing.Point(470, 66)
        Me.lnkOtherSkill.Name = "lnkOtherSkill"
        Me.lnkOtherSkill.Size = New System.Drawing.Size(119, 13)
        Me.lnkOtherSkill.TabIndex = 224
        Me.lnkOtherSkill.TabStop = True
        Me.lnkOtherSkill.Text = "Show / Hide Other Skill"
        '
        'pnlOtherSkill
        '
        Me.pnlOtherSkill.Controls.Add(Me.txtOtherSkillCategory)
        Me.pnlOtherSkill.Controls.Add(Me.lblOtherSkillCategory)
        Me.pnlOtherSkill.Controls.Add(Me.txtOtherSkill)
        Me.pnlOtherSkill.Controls.Add(Me.lblOtherSkill)
        Me.pnlOtherSkill.Location = New System.Drawing.Point(4, 29)
        Me.pnlOtherSkill.Name = "pnlOtherSkill"
        Me.pnlOtherSkill.Size = New System.Drawing.Size(263, 55)
        Me.pnlOtherSkill.TabIndex = 1
        Me.pnlOtherSkill.Visible = False
        '
        'txtOtherSkillCategory
        '
        Me.txtOtherSkillCategory.Flags = 0
        Me.txtOtherSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherSkillCategory.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherSkillCategory.Location = New System.Drawing.Point(117, 5)
        Me.txtOtherSkillCategory.Name = "txtOtherSkillCategory"
        Me.txtOtherSkillCategory.Size = New System.Drawing.Size(141, 21)
        Me.txtOtherSkillCategory.TabIndex = 194
        '
        'lblOtherSkillCategory
        '
        Me.lblOtherSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherSkillCategory.Location = New System.Drawing.Point(5, 8)
        Me.lblOtherSkillCategory.Name = "lblOtherSkillCategory"
        Me.lblOtherSkillCategory.Size = New System.Drawing.Size(107, 15)
        Me.lblOtherSkillCategory.TabIndex = 193
        Me.lblOtherSkillCategory.Text = "Other Skill Category"
        Me.lblOtherSkillCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherSkill
        '
        Me.txtOtherSkill.Flags = 0
        Me.txtOtherSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherSkill.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherSkill.Location = New System.Drawing.Point(117, 31)
        Me.txtOtherSkill.Name = "txtOtherSkill"
        Me.txtOtherSkill.Size = New System.Drawing.Size(141, 21)
        Me.txtOtherSkill.TabIndex = 192
        '
        'lblOtherSkill
        '
        Me.lblOtherSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherSkill.Location = New System.Drawing.Point(5, 34)
        Me.lblOtherSkill.Name = "lblOtherSkill"
        Me.lblOtherSkill.Size = New System.Drawing.Size(107, 15)
        Me.lblOtherSkill.TabIndex = 191
        Me.lblOtherSkill.Text = "Other Skill"
        Me.lblOtherSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(508, 332)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 195
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(412, 332)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 194
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(316, 332)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnAdd.TabIndex = 193
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'pnlSkillInfo
        '
        Me.pnlSkillInfo.Controls.Add(Me.lvApplicantSkill)
        Me.pnlSkillInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlSkillInfo.Location = New System.Drawing.Point(8, 99)
        Me.pnlSkillInfo.Name = "pnlSkillInfo"
        Me.pnlSkillInfo.Size = New System.Drawing.Size(645, 227)
        Me.pnlSkillInfo.TabIndex = 190
        '
        'lvApplicantSkill
        '
        Me.lvApplicantSkill.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhSkillCategory, Me.colhSkillName, Me.colhSkillExpertise, Me.colhSkillRemark, Me.objcolhSkillGUID, Me.objcolhSkillCategoryunkid, Me.objcolhSkillunkid})
        Me.lvApplicantSkill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvApplicantSkill.FullRowSelect = True
        Me.lvApplicantSkill.GridLines = True
        Me.lvApplicantSkill.Location = New System.Drawing.Point(0, 0)
        Me.lvApplicantSkill.Name = "lvApplicantSkill"
        Me.lvApplicantSkill.Size = New System.Drawing.Size(645, 227)
        Me.lvApplicantSkill.TabIndex = 0
        Me.lvApplicantSkill.UseCompatibleStateImageBehavior = False
        Me.lvApplicantSkill.View = System.Windows.Forms.View.Details
        '
        'colhSkillCategory
        '
        Me.colhSkillCategory.Tag = "colhSkillCategory"
        Me.colhSkillCategory.Text = "Skill Category"
        Me.colhSkillCategory.Width = 130
        '
        'colhSkillName
        '
        Me.colhSkillName.Tag = "colhSkillName"
        Me.colhSkillName.Text = "Skill"
        Me.colhSkillName.Width = 160
        '
        'colhSkillExpertise
        '
        Me.colhSkillExpertise.Tag = "colhSkillExpertise"
        Me.colhSkillExpertise.Text = "Expertise"
        Me.colhSkillExpertise.Width = 80
        '
        'colhSkillRemark
        '
        Me.colhSkillRemark.Tag = "colhSkillRemark"
        Me.colhSkillRemark.Text = "Remark"
        Me.colhSkillRemark.Width = 295
        '
        'objcolhSkillGUID
        '
        Me.objcolhSkillGUID.Text = "objcolhSkillGUID"
        Me.objcolhSkillGUID.Width = 0
        '
        'objcolhSkillCategoryunkid
        '
        Me.objcolhSkillCategoryunkid.Text = "objcolhSkillCategoryunkid"
        Me.objcolhSkillCategoryunkid.Width = 0
        '
        'objcolhSkillunkid
        '
        Me.objcolhSkillunkid.Text = "objcolhSkillunkid"
        Me.objcolhSkillunkid.Width = 0
        '
        'objelSkill
        '
        Me.objelSkill.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelSkill.Location = New System.Drawing.Point(8, 84)
        Me.objelSkill.Name = "objelSkill"
        Me.objelSkill.Size = New System.Drawing.Size(590, 12)
        Me.objelSkill.TabIndex = 8
        Me.objelSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddSkill
        '
        Me.objbtnAddSkill.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSkill.BorderSelected = False
        Me.objbtnAddSkill.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSkill.Image = CType(resources.GetObject("objbtnAddSkill.Image"), System.Drawing.Image)
        Me.objbtnAddSkill.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSkill.Location = New System.Drawing.Point(244, 60)
        Me.objbtnAddSkill.Name = "objbtnAddSkill"
        Me.objbtnAddSkill.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSkill.TabIndex = 5
        '
        'cboSkillCategory
        '
        Me.cboSkillCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkillCategory.DropDownWidth = 270
        Me.cboSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkillCategory.FormattingEnabled = True
        Me.cboSkillCategory.Location = New System.Drawing.Point(102, 33)
        Me.cboSkillCategory.Name = "cboSkillCategory"
        Me.cboSkillCategory.Size = New System.Drawing.Size(136, 21)
        Me.cboSkillCategory.TabIndex = 1
        '
        'objbtnAddSkillCategory
        '
        Me.objbtnAddSkillCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSkillCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSkillCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSkillCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSkillCategory.BorderSelected = False
        Me.objbtnAddSkillCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSkillCategory.Image = CType(resources.GetObject("objbtnAddSkillCategory.Image"), System.Drawing.Image)
        Me.objbtnAddSkillCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSkillCategory.Location = New System.Drawing.Point(244, 33)
        Me.objbtnAddSkillCategory.Name = "objbtnAddSkillCategory"
        Me.objbtnAddSkillCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSkillCategory.TabIndex = 2
        '
        'lblSkillCategory
        '
        Me.lblSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillCategory.Location = New System.Drawing.Point(8, 36)
        Me.lblSkillCategory.Name = "lblSkillCategory"
        Me.lblSkillCategory.Size = New System.Drawing.Size(88, 15)
        Me.lblSkillCategory.TabIndex = 0
        Me.lblSkillCategory.Text = "Skill Category"
        Me.lblSkillCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSkillName
        '
        Me.lblSkillName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillName.Location = New System.Drawing.Point(8, 63)
        Me.lblSkillName.Name = "lblSkillName"
        Me.lblSkillName.Size = New System.Drawing.Size(88, 15)
        Me.lblSkillName.TabIndex = 3
        Me.lblSkillName.Text = "Skill "
        Me.lblSkillName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSkill
        '
        Me.cboSkill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkill.DropDownWidth = 270
        Me.cboSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkill.FormattingEnabled = True
        Me.cboSkill.Location = New System.Drawing.Point(102, 60)
        Me.cboSkill.Name = "cboSkill"
        Me.cboSkill.Size = New System.Drawing.Size(136, 21)
        Me.cboSkill.TabIndex = 4
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(337, 33)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(262, 21)
        Me.txtRemark.TabIndex = 7
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(271, 36)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(60, 15)
        Me.lblDescription.TabIndex = 6
        Me.lblDescription.Text = "Remark"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbOtherInfo
        '
        Me.gbOtherInfo.BorderColor = System.Drawing.Color.Black
        Me.gbOtherInfo.Checked = False
        Me.gbOtherInfo.CollapseAllExceptThis = False
        Me.gbOtherInfo.CollapsedHoverImage = Nothing
        Me.gbOtherInfo.CollapsedNormalImage = Nothing
        Me.gbOtherInfo.CollapsedPressedImage = Nothing
        Me.gbOtherInfo.CollapseOnLoad = False
        Me.gbOtherInfo.Controls.Add(Me.lblMotherTongue)
        Me.gbOtherInfo.Controls.Add(Me.txtMotherTongue)
        Me.gbOtherInfo.Controls.Add(Me.lblNationality)
        Me.gbOtherInfo.Controls.Add(Me.lblBirtdate)
        Me.gbOtherInfo.Controls.Add(Me.lblLanguage1)
        Me.gbOtherInfo.Controls.Add(Me.lblMaritalStatus)
        Me.gbOtherInfo.Controls.Add(Me.cboLanguage1)
        Me.gbOtherInfo.Controls.Add(Me.dtpBirthdate)
        Me.gbOtherInfo.Controls.Add(Me.lblLanguage2)
        Me.gbOtherInfo.Controls.Add(Me.cboNationality)
        Me.gbOtherInfo.Controls.Add(Me.cboMaritalStatus)
        Me.gbOtherInfo.Controls.Add(Me.cboLanguage3)
        Me.gbOtherInfo.Controls.Add(Me.lblMaritalDate)
        Me.gbOtherInfo.Controls.Add(Me.lblLanguage4)
        Me.gbOtherInfo.Controls.Add(Me.dtpAnniversaryDate)
        Me.gbOtherInfo.Controls.Add(Me.cboLanguage2)
        Me.gbOtherInfo.Controls.Add(Me.cboLanguage4)
        Me.gbOtherInfo.Controls.Add(Me.lblLanguage3)
        Me.gbOtherInfo.ExpandedHoverImage = Nothing
        Me.gbOtherInfo.ExpandedNormalImage = Nothing
        Me.gbOtherInfo.ExpandedPressedImage = Nothing
        Me.gbOtherInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtherInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOtherInfo.HeaderHeight = 25
        Me.gbOtherInfo.HeaderMessage = ""
        Me.gbOtherInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOtherInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOtherInfo.HeightOnCollapse = 0
        Me.gbOtherInfo.LeftTextSpace = 0
        Me.gbOtherInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbOtherInfo.Name = "gbOtherInfo"
        Me.gbOtherInfo.OpenHeight = 119
        Me.gbOtherInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOtherInfo.ShowBorder = True
        Me.gbOtherInfo.ShowCheckBox = False
        Me.gbOtherInfo.ShowCollapseButton = False
        Me.gbOtherInfo.ShowDefaultBorderColor = True
        Me.gbOtherInfo.ShowDownButton = False
        Me.gbOtherInfo.ShowHeader = True
        Me.gbOtherInfo.Size = New System.Drawing.Size(206, 371)
        Me.gbOtherInfo.TabIndex = 0
        Me.gbOtherInfo.Temp = 0
        Me.gbOtherInfo.Text = "Other Info."
        Me.gbOtherInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMotherTongue
        '
        Me.lblMotherTongue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMotherTongue.Location = New System.Drawing.Point(8, 252)
        Me.lblMotherTongue.Name = "lblMotherTongue"
        Me.lblMotherTongue.Size = New System.Drawing.Size(80, 15)
        Me.lblMotherTongue.TabIndex = 194
        Me.lblMotherTongue.Text = "Mother Tongue"
        Me.lblMotherTongue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMotherTongue
        '
        Me.txtMotherTongue.Flags = 0
        Me.txtMotherTongue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMotherTongue.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMotherTongue.Location = New System.Drawing.Point(87, 249)
        Me.txtMotherTongue.Name = "txtMotherTongue"
        Me.txtMotherTongue.Size = New System.Drawing.Size(108, 21)
        Me.txtMotherTongue.TabIndex = 193
        '
        'lblNationality
        '
        Me.lblNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNationality.Location = New System.Drawing.Point(8, 36)
        Me.lblNationality.Name = "lblNationality"
        Me.lblNationality.Size = New System.Drawing.Size(73, 15)
        Me.lblNationality.TabIndex = 8
        Me.lblNationality.Text = "Nationality"
        Me.lblNationality.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBirtdate
        '
        Me.lblBirtdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirtdate.Location = New System.Drawing.Point(8, 63)
        Me.lblBirtdate.Name = "lblBirtdate"
        Me.lblBirtdate.Size = New System.Drawing.Size(73, 15)
        Me.lblBirtdate.TabIndex = 22
        Me.lblBirtdate.Text = "Birth Date"
        Me.lblBirtdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLanguage1
        '
        Me.lblLanguage1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage1.Location = New System.Drawing.Point(8, 144)
        Me.lblLanguage1.Name = "lblLanguage1"
        Me.lblLanguage1.Size = New System.Drawing.Size(73, 15)
        Me.lblLanguage1.TabIndex = 0
        Me.lblLanguage1.Text = "Language1"
        Me.lblLanguage1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMaritalStatus
        '
        Me.lblMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaritalStatus.Location = New System.Drawing.Point(8, 90)
        Me.lblMaritalStatus.Name = "lblMaritalStatus"
        Me.lblMaritalStatus.Size = New System.Drawing.Size(73, 15)
        Me.lblMaritalStatus.TabIndex = 76
        Me.lblMaritalStatus.Text = "Marital Status"
        Me.lblMaritalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLanguage1
        '
        Me.cboLanguage1.DropDownHeight = 180
        Me.cboLanguage1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage1.DropDownWidth = 150
        Me.cboLanguage1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage1.FormattingEnabled = True
        Me.cboLanguage1.IntegralHeight = False
        Me.cboLanguage1.Location = New System.Drawing.Point(87, 141)
        Me.cboLanguage1.Name = "cboLanguage1"
        Me.cboLanguage1.Size = New System.Drawing.Size(108, 21)
        Me.cboLanguage1.TabIndex = 1
        '
        'dtpBirthdate
        '
        Me.dtpBirthdate.Checked = False
        Me.dtpBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpBirthdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBirthdate.Location = New System.Drawing.Point(87, 60)
        Me.dtpBirthdate.Name = "dtpBirthdate"
        Me.dtpBirthdate.ShowCheckBox = True
        Me.dtpBirthdate.Size = New System.Drawing.Size(108, 21)
        Me.dtpBirthdate.TabIndex = 0
        '
        'lblLanguage2
        '
        Me.lblLanguage2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage2.Location = New System.Drawing.Point(8, 171)
        Me.lblLanguage2.Name = "lblLanguage2"
        Me.lblLanguage2.Size = New System.Drawing.Size(73, 15)
        Me.lblLanguage2.TabIndex = 2
        Me.lblLanguage2.Text = "Language2"
        Me.lblLanguage2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNationality
        '
        Me.cboNationality.DropDownHeight = 180
        Me.cboNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNationality.DropDownWidth = 150
        Me.cboNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNationality.FormattingEnabled = True
        Me.cboNationality.IntegralHeight = False
        Me.cboNationality.Location = New System.Drawing.Point(87, 33)
        Me.cboNationality.Name = "cboNationality"
        Me.cboNationality.Size = New System.Drawing.Size(108, 21)
        Me.cboNationality.TabIndex = 9
        '
        'cboMaritalStatus
        '
        Me.cboMaritalStatus.DropDownHeight = 180
        Me.cboMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMaritalStatus.FormattingEnabled = True
        Me.cboMaritalStatus.IntegralHeight = False
        Me.cboMaritalStatus.Location = New System.Drawing.Point(87, 87)
        Me.cboMaritalStatus.Name = "cboMaritalStatus"
        Me.cboMaritalStatus.Size = New System.Drawing.Size(108, 21)
        Me.cboMaritalStatus.TabIndex = 1
        '
        'cboLanguage3
        '
        Me.cboLanguage3.DropDownHeight = 180
        Me.cboLanguage3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage3.DropDownWidth = 150
        Me.cboLanguage3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage3.FormattingEnabled = True
        Me.cboLanguage3.IntegralHeight = False
        Me.cboLanguage3.Location = New System.Drawing.Point(87, 195)
        Me.cboLanguage3.Name = "cboLanguage3"
        Me.cboLanguage3.Size = New System.Drawing.Size(108, 21)
        Me.cboLanguage3.TabIndex = 5
        '
        'lblMaritalDate
        '
        Me.lblMaritalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaritalDate.Location = New System.Drawing.Point(8, 117)
        Me.lblMaritalDate.Name = "lblMaritalDate"
        Me.lblMaritalDate.Size = New System.Drawing.Size(73, 15)
        Me.lblMaritalDate.TabIndex = 77
        Me.lblMaritalDate.Text = "Married Date"
        Me.lblMaritalDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLanguage4
        '
        Me.lblLanguage4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage4.Location = New System.Drawing.Point(8, 225)
        Me.lblLanguage4.Name = "lblLanguage4"
        Me.lblLanguage4.Size = New System.Drawing.Size(73, 15)
        Me.lblLanguage4.TabIndex = 6
        Me.lblLanguage4.Text = "Language4"
        Me.lblLanguage4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAnniversaryDate
        '
        Me.dtpAnniversaryDate.Checked = False
        Me.dtpAnniversaryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnniversaryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAnniversaryDate.Location = New System.Drawing.Point(87, 114)
        Me.dtpAnniversaryDate.Name = "dtpAnniversaryDate"
        Me.dtpAnniversaryDate.ShowCheckBox = True
        Me.dtpAnniversaryDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpAnniversaryDate.TabIndex = 2
        '
        'cboLanguage2
        '
        Me.cboLanguage2.DropDownHeight = 180
        Me.cboLanguage2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage2.DropDownWidth = 150
        Me.cboLanguage2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage2.FormattingEnabled = True
        Me.cboLanguage2.IntegralHeight = False
        Me.cboLanguage2.Location = New System.Drawing.Point(87, 168)
        Me.cboLanguage2.Name = "cboLanguage2"
        Me.cboLanguage2.Size = New System.Drawing.Size(108, 21)
        Me.cboLanguage2.TabIndex = 3
        '
        'cboLanguage4
        '
        Me.cboLanguage4.DropDownHeight = 180
        Me.cboLanguage4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage4.DropDownWidth = 150
        Me.cboLanguage4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage4.FormattingEnabled = True
        Me.cboLanguage4.IntegralHeight = False
        Me.cboLanguage4.Location = New System.Drawing.Point(87, 222)
        Me.cboLanguage4.Name = "cboLanguage4"
        Me.cboLanguage4.Size = New System.Drawing.Size(108, 21)
        Me.cboLanguage4.TabIndex = 7
        '
        'lblLanguage3
        '
        Me.lblLanguage3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage3.Location = New System.Drawing.Point(8, 198)
        Me.lblLanguage3.Name = "lblLanguage3"
        Me.lblLanguage3.Size = New System.Drawing.Size(73, 15)
        Me.lblLanguage3.TabIndex = 4
        Me.lblLanguage3.Text = "Language3"
        Me.lblLanguage3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpEducationalInfo
        '
        Me.tabpEducationalInfo.Controls.Add(Me.pnlQualificationDetail)
        Me.tabpEducationalInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpEducationalInfo.Name = "tabpEducationalInfo"
        Me.tabpEducationalInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpEducationalInfo.Size = New System.Drawing.Size(879, 377)
        Me.tabpEducationalInfo.TabIndex = 1
        Me.tabpEducationalInfo.Text = "Qualification Details"
        Me.tabpEducationalInfo.UseVisualStyleBackColor = True
        '
        'pnlQualificationDetail
        '
        Me.pnlQualificationDetail.Controls.Add(Me.gbQualificationInfo)
        Me.pnlQualificationDetail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlQualificationDetail.Location = New System.Drawing.Point(3, 3)
        Me.pnlQualificationDetail.Name = "pnlQualificationDetail"
        Me.pnlQualificationDetail.Size = New System.Drawing.Size(873, 371)
        Me.pnlQualificationDetail.TabIndex = 0
        '
        'gbQualificationInfo
        '
        Me.gbQualificationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbQualificationInfo.Checked = False
        Me.gbQualificationInfo.CollapseAllExceptThis = False
        Me.gbQualificationInfo.CollapsedHoverImage = Nothing
        Me.gbQualificationInfo.CollapsedNormalImage = Nothing
        Me.gbQualificationInfo.CollapsedPressedImage = Nothing
        Me.gbQualificationInfo.CollapseOnLoad = False
        Me.gbQualificationInfo.Controls.Add(Me.chkInstitute)
        Me.gbQualificationInfo.Controls.Add(Me.chkResultCode)
        Me.gbQualificationInfo.Controls.Add(Me.chkqualificationAward)
        Me.gbQualificationInfo.Controls.Add(Me.txtOtherInstitute)
        Me.gbQualificationInfo.Controls.Add(Me.txtOtherResultCode)
        Me.gbQualificationInfo.Controls.Add(Me.chkqualificationgroup)
        Me.gbQualificationInfo.Controls.Add(Me.chkHighestqualification)
        Me.gbQualificationInfo.Controls.Add(Me.txtOtherQualification)
        Me.gbQualificationInfo.Controls.Add(Me.txtOtherQualificationGrp)
        Me.gbQualificationInfo.Controls.Add(Me.txtCertiNo)
        Me.gbQualificationInfo.Controls.Add(Me.lblCertiNo)
        Me.gbQualificationInfo.Controls.Add(Me.lnkOtherQualification)
        Me.gbQualificationInfo.Controls.Add(Me.nudGPA)
        Me.gbQualificationInfo.Controls.Add(Me.cboResultCode)
        Me.gbQualificationInfo.Controls.Add(Me.lblResult)
        Me.gbQualificationInfo.Controls.Add(Me.lblGPA)
        Me.gbQualificationInfo.Controls.Add(Me.pnlQualifyInfo)
        Me.gbQualificationInfo.Controls.Add(Me.EZeeStraightLine2)
        Me.gbQualificationInfo.Controls.Add(Me.objLine1)
        Me.gbQualificationInfo.Controls.Add(Me.btnQualifyDelete)
        Me.gbQualificationInfo.Controls.Add(Me.btnQualifyEdit)
        Me.gbQualificationInfo.Controls.Add(Me.btnQualifyAdd)
        Me.gbQualificationInfo.Controls.Add(Me.cboQualificationGroup)
        Me.gbQualificationInfo.Controls.Add(Me.lblQualificationGroup)
        Me.gbQualificationInfo.Controls.Add(Me.lblQualification)
        Me.gbQualificationInfo.Controls.Add(Me.lblRemark)
        Me.gbQualificationInfo.Controls.Add(Me.cboQualification)
        Me.gbQualificationInfo.Controls.Add(Me.txtQualificationRemark)
        Me.gbQualificationInfo.Controls.Add(Me.objbtnAddGroup)
        Me.gbQualificationInfo.Controls.Add(Me.objbtnAddInstitution)
        Me.gbQualificationInfo.Controls.Add(Me.objbtnAddQulification)
        Me.gbQualificationInfo.Controls.Add(Me.dtpQEndDate)
        Me.gbQualificationInfo.Controls.Add(Me.dtpQualifyDate)
        Me.gbQualificationInfo.Controls.Add(Me.lblAwardDate)
        Me.gbQualificationInfo.Controls.Add(Me.txtReferenceNo)
        Me.gbQualificationInfo.Controls.Add(Me.lblEndDate)
        Me.gbQualificationInfo.Controls.Add(Me.lblReferenceNo)
        Me.gbQualificationInfo.Controls.Add(Me.dtpQStartDate)
        Me.gbQualificationInfo.Controls.Add(Me.lblStartDate)
        Me.gbQualificationInfo.Controls.Add(Me.cboInstitution)
        Me.gbQualificationInfo.Controls.Add(Me.lblInstitution)
        Me.gbQualificationInfo.Controls.Add(Me.EZeeStraightLine1)
        Me.gbQualificationInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbQualificationInfo.ExpandedHoverImage = Nothing
        Me.gbQualificationInfo.ExpandedNormalImage = Nothing
        Me.gbQualificationInfo.ExpandedPressedImage = Nothing
        Me.gbQualificationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbQualificationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbQualificationInfo.HeaderHeight = 25
        Me.gbQualificationInfo.HeaderMessage = ""
        Me.gbQualificationInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbQualificationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbQualificationInfo.HeightOnCollapse = 0
        Me.gbQualificationInfo.LeftTextSpace = 0
        Me.gbQualificationInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbQualificationInfo.Name = "gbQualificationInfo"
        Me.gbQualificationInfo.OpenHeight = 358
        Me.gbQualificationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbQualificationInfo.ShowBorder = True
        Me.gbQualificationInfo.ShowCheckBox = False
        Me.gbQualificationInfo.ShowCollapseButton = False
        Me.gbQualificationInfo.ShowDefaultBorderColor = True
        Me.gbQualificationInfo.ShowDownButton = False
        Me.gbQualificationInfo.ShowHeader = True
        Me.gbQualificationInfo.Size = New System.Drawing.Size(873, 371)
        Me.gbQualificationInfo.TabIndex = 0
        Me.gbQualificationInfo.Temp = 0
        Me.gbQualificationInfo.Text = "Qualification Info"
        Me.gbQualificationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInstitute
        '
        Me.chkInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInstitute.Location = New System.Drawing.Point(340, 85)
        Me.chkInstitute.Name = "chkInstitute"
        Me.chkInstitute.Size = New System.Drawing.Size(59, 17)
        Me.chkInstitute.TabIndex = 231
        Me.chkInstitute.Text = "Others"
        Me.chkInstitute.UseVisualStyleBackColor = True
        '
        'chkResultCode
        '
        Me.chkResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkResultCode.Location = New System.Drawing.Point(142, 84)
        Me.chkResultCode.Name = "chkResultCode"
        Me.chkResultCode.Size = New System.Drawing.Size(59, 17)
        Me.chkResultCode.TabIndex = 230
        Me.chkResultCode.Text = "Others"
        Me.chkResultCode.UseVisualStyleBackColor = True
        '
        'chkqualificationAward
        '
        Me.chkqualificationAward.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkqualificationAward.Location = New System.Drawing.Point(340, 36)
        Me.chkqualificationAward.Name = "chkqualificationAward"
        Me.chkqualificationAward.Size = New System.Drawing.Size(59, 17)
        Me.chkqualificationAward.TabIndex = 229
        Me.chkqualificationAward.Text = "Others"
        Me.chkqualificationAward.UseVisualStyleBackColor = True
        '
        'txtOtherInstitute
        '
        Me.txtOtherInstitute.Flags = 0
        Me.txtOtherInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherInstitute.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherInstitute.Location = New System.Drawing.Point(214, 103)
        Me.txtOtherInstitute.Name = "txtOtherInstitute"
        Me.txtOtherInstitute.Size = New System.Drawing.Size(191, 21)
        Me.txtOtherInstitute.TabIndex = 4
        Me.txtOtherInstitute.Visible = False
        '
        'txtOtherResultCode
        '
        Me.txtOtherResultCode.Flags = 0
        Me.txtOtherResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherResultCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherResultCode.Location = New System.Drawing.Point(10, 103)
        Me.txtOtherResultCode.Name = "txtOtherResultCode"
        Me.txtOtherResultCode.Size = New System.Drawing.Size(191, 21)
        Me.txtOtherResultCode.TabIndex = 3
        Me.txtOtherResultCode.Visible = False
        '
        'chkqualificationgroup
        '
        Me.chkqualificationgroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkqualificationgroup.Location = New System.Drawing.Point(143, 37)
        Me.chkqualificationgroup.Name = "chkqualificationgroup"
        Me.chkqualificationgroup.Size = New System.Drawing.Size(59, 17)
        Me.chkqualificationgroup.TabIndex = 228
        Me.chkqualificationgroup.Text = "Others"
        Me.chkqualificationgroup.UseVisualStyleBackColor = True
        '
        'chkHighestqualification
        '
        Me.chkHighestqualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHighestqualification.Location = New System.Drawing.Point(594, 88)
        Me.chkHighestqualification.Name = "chkHighestqualification"
        Me.chkHighestqualification.Size = New System.Drawing.Size(166, 17)
        Me.chkHighestqualification.TabIndex = 1
        Me.chkHighestqualification.Text = "Highest Qualification"
        Me.chkHighestqualification.UseVisualStyleBackColor = True
        '
        'txtOtherQualification
        '
        Me.txtOtherQualification.Flags = 0
        Me.txtOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualification.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualification.Location = New System.Drawing.Point(214, 55)
        Me.txtOtherQualification.Name = "txtOtherQualification"
        Me.txtOtherQualification.Size = New System.Drawing.Size(191, 21)
        Me.txtOtherQualification.TabIndex = 2
        Me.txtOtherQualification.Visible = False
        '
        'txtOtherQualificationGrp
        '
        Me.txtOtherQualificationGrp.Flags = 0
        Me.txtOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualificationGrp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualificationGrp.Location = New System.Drawing.Point(11, 55)
        Me.txtOtherQualificationGrp.Name = "txtOtherQualificationGrp"
        Me.txtOtherQualificationGrp.Size = New System.Drawing.Size(191, 21)
        Me.txtOtherQualificationGrp.TabIndex = 1
        Me.txtOtherQualificationGrp.Visible = False
        '
        'txtCertiNo
        '
        Me.txtCertiNo.Flags = 0
        Me.txtCertiNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCertiNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCertiNo.Location = New System.Drawing.Point(490, 87)
        Me.txtCertiNo.Name = "txtCertiNo"
        Me.txtCertiNo.Size = New System.Drawing.Size(95, 21)
        Me.txtCertiNo.TabIndex = 226
        '
        'lblCertiNo
        '
        Me.lblCertiNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCertiNo.Location = New System.Drawing.Point(415, 89)
        Me.lblCertiNo.Name = "lblCertiNo"
        Me.lblCertiNo.Size = New System.Drawing.Size(69, 16)
        Me.lblCertiNo.TabIndex = 225
        Me.lblCertiNo.Text = "Certificate No."
        Me.lblCertiNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkOtherQualification
        '
        Me.lnkOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkOtherQualification.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkOtherQualification.Location = New System.Drawing.Point(463, 7)
        Me.lnkOtherQualification.Name = "lnkOtherQualification"
        Me.lnkOtherQualification.Size = New System.Drawing.Size(345, 13)
        Me.lnkOtherQualification.TabIndex = 223
        Me.lnkOtherQualification.TabStop = True
        Me.lnkOtherQualification.Text = "Show / Hide Other Qualification/Short Course"
        Me.lnkOtherQualification.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkOtherQualification.Visible = False
        '
        'nudGPA
        '
        Me.nudGPA.DecimalPlaces = 4
        Me.nudGPA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudGPA.Location = New System.Drawing.Point(490, 61)
        Me.nudGPA.Name = "nudGPA"
        Me.nudGPA.Size = New System.Drawing.Size(95, 20)
        Me.nudGPA.TabIndex = 15
        Me.nudGPA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboResultCode
        '
        Me.cboResultCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultCode.DropDownWidth = 360
        Me.cboResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultCode.FormattingEnabled = True
        Me.cboResultCode.Location = New System.Drawing.Point(12, 103)
        Me.cboResultCode.Name = "cboResultCode"
        Me.cboResultCode.Size = New System.Drawing.Size(163, 21)
        Me.cboResultCode.TabIndex = 221
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(9, 84)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(133, 16)
        Me.lblResult.TabIndex = 220
        Me.lblResult.Text = "Result Code"
        Me.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGPA
        '
        Me.lblGPA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGPA.Location = New System.Drawing.Point(415, 63)
        Me.lblGPA.Name = "lblGPA"
        Me.lblGPA.Size = New System.Drawing.Size(69, 16)
        Me.lblGPA.TabIndex = 214
        Me.lblGPA.Text = "GPA"
        Me.lblGPA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlQualifyInfo
        '
        Me.pnlQualifyInfo.Controls.Add(Me.pnlOtherQualification)
        Me.pnlQualifyInfo.Controls.Add(Me.lvQualificationList)
        Me.pnlQualifyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlQualifyInfo.Location = New System.Drawing.Point(11, 152)
        Me.pnlQualifyInfo.Name = "pnlQualifyInfo"
        Me.pnlQualifyInfo.Size = New System.Drawing.Size(855, 216)
        Me.pnlQualifyInfo.TabIndex = 209
        '
        'pnlOtherQualification
        '
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherQualificationGrp)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherInstitution)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherResultCode)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherQualification)
        Me.pnlOtherQualification.Location = New System.Drawing.Point(354, 29)
        Me.pnlOtherQualification.Name = "pnlOtherQualification"
        Me.pnlOtherQualification.Size = New System.Drawing.Size(330, 113)
        Me.pnlOtherQualification.TabIndex = 1
        Me.pnlOtherQualification.Visible = False
        '
        'lblOtherQualificationGrp
        '
        Me.lblOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualificationGrp.Location = New System.Drawing.Point(5, 8)
        Me.lblOtherQualificationGrp.Name = "lblOtherQualificationGrp"
        Me.lblOtherQualificationGrp.Size = New System.Drawing.Size(122, 16)
        Me.lblOtherQualificationGrp.TabIndex = 223
        Me.lblOtherQualificationGrp.Text = "Qualification Group"
        Me.lblOtherQualificationGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherInstitution
        '
        Me.lblOtherInstitution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherInstitution.Location = New System.Drawing.Point(5, 89)
        Me.lblOtherInstitution.Name = "lblOtherInstitution"
        Me.lblOtherInstitution.Size = New System.Drawing.Size(122, 16)
        Me.lblOtherInstitution.TabIndex = 223
        Me.lblOtherInstitution.Text = "Institution/Prov. Name"
        Me.lblOtherInstitution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherResultCode
        '
        Me.lblOtherResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherResultCode.Location = New System.Drawing.Point(5, 62)
        Me.lblOtherResultCode.Name = "lblOtherResultCode"
        Me.lblOtherResultCode.Size = New System.Drawing.Size(122, 16)
        Me.lblOtherResultCode.TabIndex = 223
        Me.lblOtherResultCode.Text = "Result"
        Me.lblOtherResultCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherQualification
        '
        Me.lblOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualification.Location = New System.Drawing.Point(5, 35)
        Me.lblOtherQualification.Name = "lblOtherQualification"
        Me.lblOtherQualification.Size = New System.Drawing.Size(122, 16)
        Me.lblOtherQualification.TabIndex = 211
        Me.lblOtherQualification.Text = "Qualifi. / Course Name"
        Me.lblOtherQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvQualificationList
        '
        Me.lvQualificationList.BackColorOnChecked = True
        Me.lvQualificationList.ColumnHeaders = Nothing
        Me.lvQualificationList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhQualifyGroupName, Me.colhQualificationName, Me.colhRefNo, Me.colhStartDate, Me.colhEndDate, Me.colhInstitute, Me.colhRemark, Me.objcolhQualifyGroupunkid, Me.objcolhQualifyunkid, Me.objcolhTransdate, Me.objcolhInstituteId, Me.objcolhQualifyGUID, Me.objcolhResultCodeID, Me.objColhgpacode, Me.objColhCertiNo, Me.objcolhHighestQualification})
        Me.lvQualificationList.CompulsoryColumns = ""
        Me.lvQualificationList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvQualificationList.FullRowSelect = True
        Me.lvQualificationList.GridLines = True
        Me.lvQualificationList.GroupingColumn = Nothing
        Me.lvQualificationList.HideSelection = False
        Me.lvQualificationList.Location = New System.Drawing.Point(0, 0)
        Me.lvQualificationList.MinColumnWidth = 50
        Me.lvQualificationList.MultiSelect = False
        Me.lvQualificationList.Name = "lvQualificationList"
        Me.lvQualificationList.OptionalColumns = ""
        Me.lvQualificationList.ShowMoreItem = False
        Me.lvQualificationList.ShowSaveItem = False
        Me.lvQualificationList.ShowSelectAll = True
        Me.lvQualificationList.ShowSizeAllColumnsToFit = True
        Me.lvQualificationList.Size = New System.Drawing.Size(855, 216)
        Me.lvQualificationList.Sortable = True
        Me.lvQualificationList.TabIndex = 0
        Me.lvQualificationList.UseCompatibleStateImageBehavior = False
        Me.lvQualificationList.View = System.Windows.Forms.View.Details
        '
        'colhQualifyGroupName
        '
        Me.colhQualifyGroupName.Tag = "colhQualifyGroupName"
        Me.colhQualifyGroupName.Text = "Qualification Group"
        Me.colhQualifyGroupName.Width = 120
        '
        'colhQualificationName
        '
        Me.colhQualificationName.Tag = "colhQualificationName"
        Me.colhQualificationName.Text = "Qualification"
        Me.colhQualificationName.Width = 115
        '
        'colhRefNo
        '
        Me.colhRefNo.Tag = "colhRefNo"
        Me.colhRefNo.Text = "Ref. No."
        Me.colhRefNo.Width = 100
        '
        'colhStartDate
        '
        Me.colhStartDate.Tag = "colhStartDate"
        Me.colhStartDate.Text = "Start Date"
        Me.colhStartDate.Width = 85
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "Award Date"
        Me.colhEndDate.Width = 85
        '
        'colhInstitute
        '
        Me.colhInstitute.Tag = "colhInstitute"
        Me.colhInstitute.Text = "Institute"
        Me.colhInstitute.Width = 125
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 162
        '
        'objcolhQualifyGroupunkid
        '
        Me.objcolhQualifyGroupunkid.Tag = "objcolhQualifyGroupunkid"
        Me.objcolhQualifyGroupunkid.Text = ""
        Me.objcolhQualifyGroupunkid.Width = 0
        '
        'objcolhQualifyunkid
        '
        Me.objcolhQualifyunkid.Tag = "objcolhQualifyunkid"
        Me.objcolhQualifyunkid.Text = ""
        Me.objcolhQualifyunkid.Width = 0
        '
        'objcolhTransdate
        '
        Me.objcolhTransdate.Tag = "objcolhTransdate"
        Me.objcolhTransdate.Text = ""
        Me.objcolhTransdate.Width = 0
        '
        'objcolhInstituteId
        '
        Me.objcolhInstituteId.Tag = "objcolhInstituteId"
        Me.objcolhInstituteId.Text = ""
        Me.objcolhInstituteId.Width = 0
        '
        'objcolhQualifyGUID
        '
        Me.objcolhQualifyGUID.Tag = "objcolhQualifyGUID"
        Me.objcolhQualifyGUID.Text = ""
        Me.objcolhQualifyGUID.Width = 0
        '
        'objcolhResultCodeID
        '
        Me.objcolhResultCodeID.Tag = "objcolhResultCodeIDs"
        Me.objcolhResultCodeID.Text = "ResultCodeID"
        Me.objcolhResultCodeID.Width = 0
        '
        'objColhgpacode
        '
        Me.objColhgpacode.Tag = "objColhgpacode"
        Me.objColhgpacode.Text = "GPAcode"
        Me.objColhgpacode.Width = 0
        '
        'objColhCertiNo
        '
        Me.objColhCertiNo.Tag = "objColhCertiNo"
        Me.objColhCertiNo.Text = "Certi. No"
        Me.objColhCertiNo.Width = 0
        '
        'objcolhHighestQualification
        '
        Me.objcolhHighestQualification.Tag = "objcolhHighestQualification"
        Me.objcolhHighestQualification.Text = "Highest Qualification"
        Me.objcolhHighestQualification.Width = 0
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(764, 34)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(7, 102)
        Me.EZeeStraightLine2.TabIndex = 19
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'objLine1
        '
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(8, 140)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(858, 9)
        Me.objLine1.TabIndex = 23
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQualifyDelete
        '
        Me.btnQualifyDelete.BackColor = System.Drawing.Color.White
        Me.btnQualifyDelete.BackgroundImage = CType(resources.GetObject("btnQualifyDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnQualifyDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnQualifyDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnQualifyDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnQualifyDelete.FlatAppearance.BorderSize = 0
        Me.btnQualifyDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnQualifyDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQualifyDelete.ForeColor = System.Drawing.Color.Black
        Me.btnQualifyDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnQualifyDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnQualifyDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyDelete.Location = New System.Drawing.Point(776, 106)
        Me.btnQualifyDelete.Name = "btnQualifyDelete"
        Me.btnQualifyDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnQualifyDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnQualifyDelete.TabIndex = 22
        Me.btnQualifyDelete.Text = "&Delete"
        Me.btnQualifyDelete.UseVisualStyleBackColor = True
        '
        'btnQualifyEdit
        '
        Me.btnQualifyEdit.BackColor = System.Drawing.Color.White
        Me.btnQualifyEdit.BackgroundImage = CType(resources.GetObject("btnQualifyEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnQualifyEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnQualifyEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnQualifyEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnQualifyEdit.FlatAppearance.BorderSize = 0
        Me.btnQualifyEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnQualifyEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQualifyEdit.ForeColor = System.Drawing.Color.Black
        Me.btnQualifyEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnQualifyEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnQualifyEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyEdit.Location = New System.Drawing.Point(776, 70)
        Me.btnQualifyEdit.Name = "btnQualifyEdit"
        Me.btnQualifyEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnQualifyEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnQualifyEdit.TabIndex = 21
        Me.btnQualifyEdit.Text = "&Edit"
        Me.btnQualifyEdit.UseVisualStyleBackColor = True
        '
        'btnQualifyAdd
        '
        Me.btnQualifyAdd.BackColor = System.Drawing.Color.White
        Me.btnQualifyAdd.BackgroundImage = CType(resources.GetObject("btnQualifyAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnQualifyAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnQualifyAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnQualifyAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnQualifyAdd.FlatAppearance.BorderSize = 0
        Me.btnQualifyAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnQualifyAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQualifyAdd.ForeColor = System.Drawing.Color.Black
        Me.btnQualifyAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnQualifyAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnQualifyAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyAdd.Location = New System.Drawing.Point(776, 34)
        Me.btnQualifyAdd.Name = "btnQualifyAdd"
        Me.btnQualifyAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnQualifyAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnQualifyAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnQualifyAdd.TabIndex = 20
        Me.btnQualifyAdd.Text = "&Add"
        Me.btnQualifyAdd.UseVisualStyleBackColor = True
        '
        'cboQualificationGroup
        '
        Me.cboQualificationGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualificationGroup.DropDownWidth = 360
        Me.cboQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualificationGroup.FormattingEnabled = True
        Me.cboQualificationGroup.Location = New System.Drawing.Point(11, 55)
        Me.cboQualificationGroup.Name = "cboQualificationGroup"
        Me.cboQualificationGroup.Size = New System.Drawing.Size(164, 21)
        Me.cboQualificationGroup.TabIndex = 1
        '
        'lblQualificationGroup
        '
        Me.lblQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblQualificationGroup.Name = "lblQualificationGroup"
        Me.lblQualificationGroup.Size = New System.Drawing.Size(133, 16)
        Me.lblQualificationGroup.TabIndex = 0
        Me.lblQualificationGroup.Text = "Qualification/Award Group"
        Me.lblQualificationGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(212, 36)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(133, 16)
        Me.lblQualification.TabIndex = 3
        Me.lblQualification.Text = "Qualification/Award"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(415, 116)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(69, 16)
        Me.lblRemark.TabIndex = 17
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualification
        '
        Me.cboQualification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualification.DropDownWidth = 360
        Me.cboQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualification.FormattingEnabled = True
        Me.cboQualification.Location = New System.Drawing.Point(214, 55)
        Me.cboQualification.Name = "cboQualification"
        Me.cboQualification.Size = New System.Drawing.Size(155, 21)
        Me.cboQualification.TabIndex = 4
        '
        'txtQualificationRemark
        '
        Me.txtQualificationRemark.Flags = 0
        Me.txtQualificationRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQualificationRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtQualificationRemark.Location = New System.Drawing.Point(491, 114)
        Me.txtQualificationRemark.Multiline = True
        Me.txtQualificationRemark.Name = "txtQualificationRemark"
        Me.txtQualificationRemark.Size = New System.Drawing.Size(270, 20)
        Me.txtQualificationRemark.TabIndex = 18
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(181, 55)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 2
        '
        'objbtnAddInstitution
        '
        Me.objbtnAddInstitution.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddInstitution.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddInstitution.BorderSelected = False
        Me.objbtnAddInstitution.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddInstitution.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddInstitution.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddInstitution.Location = New System.Drawing.Point(376, 104)
        Me.objbtnAddInstitution.Name = "objbtnAddInstitution"
        Me.objbtnAddInstitution.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddInstitution.TabIndex = 10
        '
        'objbtnAddQulification
        '
        Me.objbtnAddQulification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddQulification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddQulification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddQulification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddQulification.BorderSelected = False
        Me.objbtnAddQulification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddQulification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddQulification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddQulification.Location = New System.Drawing.Point(375, 55)
        Me.objbtnAddQulification.Name = "objbtnAddQulification"
        Me.objbtnAddQulification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddQulification.TabIndex = 6
        '
        'dtpQEndDate
        '
        Me.dtpQEndDate.Checked = False
        Me.dtpQEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpQEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpQEndDate.Location = New System.Drawing.Point(665, 34)
        Me.dtpQEndDate.Name = "dtpQEndDate"
        Me.dtpQEndDate.ShowCheckBox = True
        Me.dtpQEndDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpQEndDate.TabIndex = 14
        '
        'dtpQualifyDate
        '
        Me.dtpQualifyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpQualifyDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpQualifyDate.Location = New System.Drawing.Point(665, 61)
        Me.dtpQualifyDate.Name = "dtpQualifyDate"
        Me.dtpQualifyDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpQualifyDate.TabIndex = 16
        Me.dtpQualifyDate.Visible = False
        '
        'lblAwardDate
        '
        Me.lblAwardDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAwardDate.Location = New System.Drawing.Point(591, 63)
        Me.lblAwardDate.Name = "lblAwardDate"
        Me.lblAwardDate.Size = New System.Drawing.Size(66, 16)
        Me.lblAwardDate.TabIndex = 15
        Me.lblAwardDate.Text = "Date"
        Me.lblAwardDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAwardDate.Visible = False
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.Flags = 0
        Me.txtReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceNo.Location = New System.Drawing.Point(12, 103)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.Size = New System.Drawing.Size(163, 21)
        Me.txtReferenceNo.TabIndex = 7
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(591, 36)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(66, 16)
        Me.lblEndDate.TabIndex = 13
        Me.lblEndDate.Text = "Award Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReferenceNo
        '
        Me.lblReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferenceNo.Location = New System.Drawing.Point(9, 84)
        Me.lblReferenceNo.Name = "lblReferenceNo"
        Me.lblReferenceNo.Size = New System.Drawing.Size(133, 16)
        Me.lblReferenceNo.TabIndex = 4
        Me.lblReferenceNo.Text = "Reference No"
        Me.lblReferenceNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpQStartDate
        '
        Me.dtpQStartDate.Checked = False
        Me.dtpQStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpQStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpQStartDate.Location = New System.Drawing.Point(490, 34)
        Me.dtpQStartDate.Name = "dtpQStartDate"
        Me.dtpQStartDate.ShowCheckBox = True
        Me.dtpQStartDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpQStartDate.TabIndex = 12
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(415, 36)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(69, 16)
        Me.lblStartDate.TabIndex = 11
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInstitution
        '
        Me.cboInstitution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitution.DropDownWidth = 360
        Me.cboInstitution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitution.FormattingEnabled = True
        Me.cboInstitution.Location = New System.Drawing.Point(215, 103)
        Me.cboInstitution.Name = "cboInstitution"
        Me.cboInstitution.Size = New System.Drawing.Size(155, 21)
        Me.cboInstitution.TabIndex = 9
        '
        'lblInstitution
        '
        Me.lblInstitution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstitution.Location = New System.Drawing.Point(211, 84)
        Me.lblInstitution.Name = "lblInstitution"
        Me.lblInstitution.Size = New System.Drawing.Size(133, 16)
        Me.lblInstitution.TabIndex = 8
        Me.lblInstitution.Text = "Institution"
        Me.lblInstitution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(403, 34)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(12, 102)
        Me.EZeeStraightLine1.TabIndex = 210
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'tabpEmployment
        '
        Me.tabpEmployment.Controls.Add(Me.pnlJobInfo)
        Me.tabpEmployment.Location = New System.Drawing.Point(4, 22)
        Me.tabpEmployment.Name = "tabpEmployment"
        Me.tabpEmployment.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpEmployment.Size = New System.Drawing.Size(879, 377)
        Me.tabpEmployment.TabIndex = 2
        Me.tabpEmployment.Text = "Job History"
        Me.tabpEmployment.UseVisualStyleBackColor = True
        '
        'pnlJobInfo
        '
        Me.pnlJobInfo.Controls.Add(Me.gbJobHistory)
        Me.pnlJobInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlJobInfo.Location = New System.Drawing.Point(3, 3)
        Me.pnlJobInfo.Name = "pnlJobInfo"
        Me.pnlJobInfo.Size = New System.Drawing.Size(873, 371)
        Me.pnlJobInfo.TabIndex = 0
        '
        'gbJobHistory
        '
        Me.gbJobHistory.BorderColor = System.Drawing.Color.Black
        Me.gbJobHistory.Checked = False
        Me.gbJobHistory.CollapseAllExceptThis = False
        Me.gbJobHistory.CollapsedHoverImage = Nothing
        Me.gbJobHistory.CollapsedNormalImage = Nothing
        Me.gbJobHistory.CollapsedPressedImage = Nothing
        Me.gbJobHistory.CollapseOnLoad = False
        Me.gbJobHistory.Controls.Add(Me.txtJobAchievement)
        Me.gbJobHistory.Controls.Add(Me.lblJobAchievement)
        Me.gbJobHistory.Controls.Add(Me.pnlJobHistory)
        Me.gbJobHistory.Controls.Add(Me.btnJobHistoryDelete)
        Me.gbJobHistory.Controls.Add(Me.EZeeLine1)
        Me.gbJobHistory.Controls.Add(Me.btnJobHistoryEdit)
        Me.gbJobHistory.Controls.Add(Me.btnJobHistoryAdd)
        Me.gbJobHistory.Controls.Add(Me.txtEmployerName)
        Me.gbJobHistory.Controls.Add(Me.lblEmployer)
        Me.gbJobHistory.Controls.Add(Me.lblDesignation)
        Me.gbJobHistory.Controls.Add(Me.dtpTerminationDate)
        Me.gbJobHistory.Controls.Add(Me.lblJoinDate)
        Me.gbJobHistory.Controls.Add(Me.dtpJoinDate)
        Me.gbJobHistory.Controls.Add(Me.lblOfficePhone)
        Me.gbJobHistory.Controls.Add(Me.txtLeavingReason)
        Me.gbJobHistory.Controls.Add(Me.lblResponsibility)
        Me.gbJobHistory.Controls.Add(Me.txtResponsibility)
        Me.gbJobHistory.Controls.Add(Me.lblReason)
        Me.gbJobHistory.Controls.Add(Me.txtDesignation)
        Me.gbJobHistory.Controls.Add(Me.lblOrgDescription)
        Me.gbJobHistory.Controls.Add(Me.txtOfficePhone)
        Me.gbJobHistory.Controls.Add(Me.lblTermDate)
        Me.gbJobHistory.Controls.Add(Me.txtCompanyName)
        Me.gbJobHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbJobHistory.ExpandedHoverImage = Nothing
        Me.gbJobHistory.ExpandedNormalImage = Nothing
        Me.gbJobHistory.ExpandedPressedImage = Nothing
        Me.gbJobHistory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJobHistory.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJobHistory.HeaderHeight = 25
        Me.gbJobHistory.HeaderMessage = ""
        Me.gbJobHistory.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJobHistory.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJobHistory.HeightOnCollapse = 0
        Me.gbJobHistory.LeftTextSpace = 0
        Me.gbJobHistory.Location = New System.Drawing.Point(0, 0)
        Me.gbJobHistory.Name = "gbJobHistory"
        Me.gbJobHistory.OpenHeight = 358
        Me.gbJobHistory.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJobHistory.ShowBorder = True
        Me.gbJobHistory.ShowCheckBox = False
        Me.gbJobHistory.ShowCollapseButton = False
        Me.gbJobHistory.ShowDefaultBorderColor = True
        Me.gbJobHistory.ShowDownButton = False
        Me.gbJobHistory.ShowHeader = True
        Me.gbJobHistory.Size = New System.Drawing.Size(873, 371)
        Me.gbJobHistory.TabIndex = 0
        Me.gbJobHistory.Temp = 0
        Me.gbJobHistory.Text = "Job History"
        Me.gbJobHistory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJobAchievement
        '
        Me.txtJobAchievement.Flags = 0
        Me.txtJobAchievement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobAchievement.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobAchievement.Location = New System.Drawing.Point(383, 87)
        Me.txtJobAchievement.Multiline = True
        Me.txtJobAchievement.Name = "txtJobAchievement"
        Me.txtJobAchievement.Size = New System.Drawing.Size(326, 20)
        Me.txtJobAchievement.TabIndex = 200
        '
        'lblJobAchievement
        '
        Me.lblJobAchievement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobAchievement.Location = New System.Drawing.Point(280, 90)
        Me.lblJobAchievement.Name = "lblJobAchievement"
        Me.lblJobAchievement.Size = New System.Drawing.Size(97, 15)
        Me.lblJobAchievement.TabIndex = 199
        Me.lblJobAchievement.Text = "Achievement"
        Me.lblJobAchievement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlJobHistory
        '
        Me.pnlJobHistory.Controls.Add(Me.lvJobHistory)
        Me.pnlJobHistory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlJobHistory.Location = New System.Drawing.Point(6, 141)
        Me.pnlJobHistory.Name = "pnlJobHistory"
        Me.pnlJobHistory.Size = New System.Drawing.Size(862, 227)
        Me.pnlJobHistory.TabIndex = 197
        '
        'lvJobHistory
        '
        Me.lvJobHistory.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployer, Me.colhCompanyName, Me.colhOfficePhone, Me.colhDesignation, Me.colhResponsibility, Me.colhJoinDate, Me.colhTermDate, Me.colhLeavingReason, Me.objcolhJobhistoryGUID, Me.objcolhJobAchievement})
        Me.lvJobHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvJobHistory.FullRowSelect = True
        Me.lvJobHistory.GridLines = True
        Me.lvJobHistory.Location = New System.Drawing.Point(0, 0)
        Me.lvJobHistory.Name = "lvJobHistory"
        Me.lvJobHistory.Size = New System.Drawing.Size(862, 227)
        Me.lvJobHistory.TabIndex = 0
        Me.lvJobHistory.UseCompatibleStateImageBehavior = False
        Me.lvJobHistory.View = System.Windows.Forms.View.Details
        '
        'colhEmployer
        '
        Me.colhEmployer.Tag = "colhEmployer"
        Me.colhEmployer.Text = "Employer"
        Me.colhEmployer.Width = 86
        '
        'colhCompanyName
        '
        Me.colhCompanyName.Tag = "colhCompanyName"
        Me.colhCompanyName.Text = "Company"
        Me.colhCompanyName.Width = 102
        '
        'colhOfficePhone
        '
        Me.colhOfficePhone.Text = "Phone"
        Me.colhOfficePhone.Width = 83
        '
        'colhDesignation
        '
        Me.colhDesignation.Text = "Designation"
        Me.colhDesignation.Width = 73
        '
        'colhResponsibility
        '
        Me.colhResponsibility.Tag = "colhResponsibility"
        Me.colhResponsibility.Text = "Responsibility"
        Me.colhResponsibility.Width = 80
        '
        'colhJoinDate
        '
        Me.colhJoinDate.Tag = "colhJoinDate"
        Me.colhJoinDate.Text = "Join Date"
        Me.colhJoinDate.Width = 72
        '
        'colhTermDate
        '
        Me.colhTermDate.Tag = "colhTermDate"
        Me.colhTermDate.Text = "Term. Date"
        Me.colhTermDate.Width = 71
        '
        'colhLeavingReason
        '
        Me.colhLeavingReason.Text = "Leaving Reason"
        Me.colhLeavingReason.Width = 230
        '
        'objcolhJobhistoryGUID
        '
        Me.objcolhJobhistoryGUID.Text = "objcolhJobhistoryGUID"
        Me.objcolhJobhistoryGUID.Width = 0
        '
        'objcolhJobAchievement
        '
        Me.objcolhJobAchievement.Tag = "objcolhJobAchievement"
        Me.objcolhJobAchievement.Text = "JobAchievement"
        Me.objcolhJobAchievement.Width = 0
        '
        'btnJobHistoryDelete
        '
        Me.btnJobHistoryDelete.BackColor = System.Drawing.Color.White
        Me.btnJobHistoryDelete.BackgroundImage = CType(resources.GetObject("btnJobHistoryDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnJobHistoryDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnJobHistoryDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnJobHistoryDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnJobHistoryDelete.FlatAppearance.BorderSize = 0
        Me.btnJobHistoryDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnJobHistoryDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJobHistoryDelete.ForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnJobHistoryDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobHistoryDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryDelete.Location = New System.Drawing.Point(715, 105)
        Me.btnJobHistoryDelete.Name = "btnJobHistoryDelete"
        Me.btnJobHistoryDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobHistoryDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryDelete.Size = New System.Drawing.Size(92, 30)
        Me.btnJobHistoryDelete.TabIndex = 18
        Me.btnJobHistoryDelete.Text = "&Delete"
        Me.btnJobHistoryDelete.UseVisualStyleBackColor = True
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(8, 130)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(701, 7)
        Me.EZeeLine1.TabIndex = 19
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnJobHistoryEdit
        '
        Me.btnJobHistoryEdit.BackColor = System.Drawing.Color.White
        Me.btnJobHistoryEdit.BackgroundImage = CType(resources.GetObject("btnJobHistoryEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnJobHistoryEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnJobHistoryEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnJobHistoryEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnJobHistoryEdit.FlatAppearance.BorderSize = 0
        Me.btnJobHistoryEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnJobHistoryEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJobHistoryEdit.ForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnJobHistoryEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobHistoryEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryEdit.Location = New System.Drawing.Point(715, 69)
        Me.btnJobHistoryEdit.Name = "btnJobHistoryEdit"
        Me.btnJobHistoryEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobHistoryEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryEdit.Size = New System.Drawing.Size(92, 30)
        Me.btnJobHistoryEdit.TabIndex = 17
        Me.btnJobHistoryEdit.Text = "&Edit"
        Me.btnJobHistoryEdit.UseVisualStyleBackColor = True
        '
        'btnJobHistoryAdd
        '
        Me.btnJobHistoryAdd.BackColor = System.Drawing.Color.White
        Me.btnJobHistoryAdd.BackgroundImage = CType(resources.GetObject("btnJobHistoryAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnJobHistoryAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnJobHistoryAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnJobHistoryAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnJobHistoryAdd.FlatAppearance.BorderSize = 0
        Me.btnJobHistoryAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnJobHistoryAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJobHistoryAdd.ForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnJobHistoryAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobHistoryAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryAdd.Location = New System.Drawing.Point(715, 33)
        Me.btnJobHistoryAdd.Name = "btnJobHistoryAdd"
        Me.btnJobHistoryAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnJobHistoryAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnJobHistoryAdd.Size = New System.Drawing.Size(92, 30)
        Me.btnJobHistoryAdd.TabIndex = 16
        Me.btnJobHistoryAdd.Text = "&Add"
        Me.btnJobHistoryAdd.UseVisualStyleBackColor = True
        '
        'txtEmployerName
        '
        Me.txtEmployerName.Flags = 0
        Me.txtEmployerName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployerName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployerName.Location = New System.Drawing.Point(103, 33)
        Me.txtEmployerName.Name = "txtEmployerName"
        Me.txtEmployerName.Size = New System.Drawing.Size(169, 21)
        Me.txtEmployerName.TabIndex = 1
        '
        'lblEmployer
        '
        Me.lblEmployer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployer.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployer.Name = "lblEmployer"
        Me.lblEmployer.Size = New System.Drawing.Size(89, 15)
        Me.lblEmployer.TabIndex = 0
        Me.lblEmployer.Text = "Employer"
        Me.lblEmployer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDesignation
        '
        Me.lblDesignation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDesignation.Location = New System.Drawing.Point(280, 36)
        Me.lblDesignation.Name = "lblDesignation"
        Me.lblDesignation.Size = New System.Drawing.Size(97, 15)
        Me.lblDesignation.TabIndex = 6
        Me.lblDesignation.Text = "Designation"
        Me.lblDesignation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpTerminationDate
        '
        Me.dtpTerminationDate.Checked = False
        Me.dtpTerminationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTerminationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTerminationDate.Location = New System.Drawing.Point(609, 60)
        Me.dtpTerminationDate.Name = "dtpTerminationDate"
        Me.dtpTerminationDate.ShowCheckBox = True
        Me.dtpTerminationDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpTerminationDate.TabIndex = 15
        '
        'lblJoinDate
        '
        Me.lblJoinDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJoinDate.Location = New System.Drawing.Point(529, 36)
        Me.lblJoinDate.Name = "lblJoinDate"
        Me.lblJoinDate.Size = New System.Drawing.Size(74, 15)
        Me.lblJoinDate.TabIndex = 12
        Me.lblJoinDate.Text = "Join Date"
        Me.lblJoinDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpJoinDate
        '
        Me.dtpJoinDate.Checked = False
        Me.dtpJoinDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpJoinDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpJoinDate.Location = New System.Drawing.Point(609, 33)
        Me.dtpJoinDate.Name = "dtpJoinDate"
        Me.dtpJoinDate.ShowCheckBox = True
        Me.dtpJoinDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpJoinDate.TabIndex = 13
        '
        'lblOfficePhone
        '
        Me.lblOfficePhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOfficePhone.Location = New System.Drawing.Point(8, 90)
        Me.lblOfficePhone.Name = "lblOfficePhone"
        Me.lblOfficePhone.Size = New System.Drawing.Size(89, 15)
        Me.lblOfficePhone.TabIndex = 4
        Me.lblOfficePhone.Text = "Office Phone"
        Me.lblOfficePhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLeavingReason
        '
        Me.txtLeavingReason.Flags = 0
        Me.txtLeavingReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeavingReason.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLeavingReason.Location = New System.Drawing.Point(383, 114)
        Me.txtLeavingReason.Multiline = True
        Me.txtLeavingReason.Name = "txtLeavingReason"
        Me.txtLeavingReason.Size = New System.Drawing.Size(326, 20)
        Me.txtLeavingReason.TabIndex = 11
        '
        'lblResponsibility
        '
        Me.lblResponsibility.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponsibility.Location = New System.Drawing.Point(280, 63)
        Me.lblResponsibility.Name = "lblResponsibility"
        Me.lblResponsibility.Size = New System.Drawing.Size(97, 15)
        Me.lblResponsibility.TabIndex = 8
        Me.lblResponsibility.Text = "Responsibility"
        Me.lblResponsibility.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtResponsibility
        '
        Me.txtResponsibility.Flags = 0
        Me.txtResponsibility.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResponsibility.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtResponsibility.Location = New System.Drawing.Point(383, 60)
        Me.txtResponsibility.Name = "txtResponsibility"
        Me.txtResponsibility.Size = New System.Drawing.Size(140, 21)
        Me.txtResponsibility.TabIndex = 9
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(280, 117)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(97, 15)
        Me.lblReason.TabIndex = 10
        Me.lblReason.Text = "Leaving Reason"
        Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDesignation
        '
        Me.txtDesignation.Flags = 0
        Me.txtDesignation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDesignation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDesignation.Location = New System.Drawing.Point(383, 33)
        Me.txtDesignation.Name = "txtDesignation"
        Me.txtDesignation.Size = New System.Drawing.Size(140, 21)
        Me.txtDesignation.TabIndex = 7
        '
        'lblOrgDescription
        '
        Me.lblOrgDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrgDescription.Location = New System.Drawing.Point(8, 63)
        Me.lblOrgDescription.Name = "lblOrgDescription"
        Me.lblOrgDescription.Size = New System.Drawing.Size(89, 15)
        Me.lblOrgDescription.TabIndex = 2
        Me.lblOrgDescription.Text = "Company Name"
        Me.lblOrgDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOfficePhone
        '
        Me.txtOfficePhone.Flags = 0
        Me.txtOfficePhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficePhone.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOfficePhone.Location = New System.Drawing.Point(103, 87)
        Me.txtOfficePhone.Name = "txtOfficePhone"
        Me.txtOfficePhone.Size = New System.Drawing.Size(169, 21)
        Me.txtOfficePhone.TabIndex = 5
        '
        'lblTermDate
        '
        Me.lblTermDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTermDate.Location = New System.Drawing.Point(529, 63)
        Me.lblTermDate.Name = "lblTermDate"
        Me.lblTermDate.Size = New System.Drawing.Size(74, 15)
        Me.lblTermDate.TabIndex = 14
        Me.lblTermDate.Text = "Term. Date"
        Me.lblTermDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompanyName
        '
        Me.txtCompanyName.Flags = 0
        Me.txtCompanyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompanyName.Location = New System.Drawing.Point(103, 60)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Size = New System.Drawing.Size(169, 21)
        Me.txtCompanyName.TabIndex = 3
        '
        'tabpReferences
        '
        Me.tabpReferences.Controls.Add(Me.gbReferences)
        Me.tabpReferences.Location = New System.Drawing.Point(4, 22)
        Me.tabpReferences.Name = "tabpReferences"
        Me.tabpReferences.Size = New System.Drawing.Size(879, 377)
        Me.tabpReferences.TabIndex = 4
        Me.tabpReferences.Text = "References"
        Me.tabpReferences.UseVisualStyleBackColor = True
        '
        'gbReferences
        '
        Me.gbReferences.AutoScroll = True
        Me.gbReferences.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.gbReferences.BorderColor = System.Drawing.Color.Black
        Me.gbReferences.Checked = False
        Me.gbReferences.CollapseAllExceptThis = False
        Me.gbReferences.CollapsedHoverImage = Nothing
        Me.gbReferences.CollapsedNormalImage = Nothing
        Me.gbReferences.CollapsedPressedImage = Nothing
        Me.gbReferences.CollapseOnLoad = False
        Me.gbReferences.Controls.Add(Me.pnlReferences)
        Me.gbReferences.ExpandedHoverImage = Nothing
        Me.gbReferences.ExpandedNormalImage = Nothing
        Me.gbReferences.ExpandedPressedImage = Nothing
        Me.gbReferences.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReferences.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbReferences.HeaderHeight = 25
        Me.gbReferences.HeaderMessage = ""
        Me.gbReferences.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReferences.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbReferences.HeightOnCollapse = 0
        Me.gbReferences.LeftTextSpace = 0
        Me.gbReferences.Location = New System.Drawing.Point(2, 3)
        Me.gbReferences.Name = "gbReferences"
        Me.gbReferences.OpenHeight = 300
        Me.gbReferences.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbReferences.ShowBorder = True
        Me.gbReferences.ShowCheckBox = False
        Me.gbReferences.ShowCollapseButton = False
        Me.gbReferences.ShowDefaultBorderColor = True
        Me.gbReferences.ShowDownButton = False
        Me.gbReferences.ShowHeader = True
        Me.gbReferences.Size = New System.Drawing.Size(869, 373)
        Me.gbReferences.TabIndex = 0
        Me.gbReferences.Temp = 0
        Me.gbReferences.Text = "References Detail"
        Me.gbReferences.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlReferences
        '
        Me.pnlReferences.AutoScroll = True
        Me.pnlReferences.Controls.Add(Me.lvReferences)
        Me.pnlReferences.Controls.Add(Me.btnRefEdit)
        Me.pnlReferences.Controls.Add(Me.lblReferenceName)
        Me.pnlReferences.Controls.Add(Me.txtRefTelNo)
        Me.pnlReferences.Controls.Add(Me.btnRefDelete)
        Me.pnlReferences.Controls.Add(Me.lblRefTelPhNo)
        Me.pnlReferences.Controls.Add(Me.txtRefPosition)
        Me.pnlReferences.Controls.Add(Me.lblRefMobileNo)
        Me.pnlReferences.Controls.Add(Me.cboRefState)
        Me.pnlReferences.Controls.Add(Me.lblPosition)
        Me.pnlReferences.Controls.Add(Me.txtRefMobileNo)
        Me.pnlReferences.Controls.Add(Me.btnRefAdd)
        Me.pnlReferences.Controls.Add(Me.lblRefState)
        Me.pnlReferences.Controls.Add(Me.lblRefAddress)
        Me.pnlReferences.Controls.Add(Me.lblRefEmail)
        Me.pnlReferences.Controls.Add(Me.cboRefCountry)
        Me.pnlReferences.Controls.Add(Me.txtReferenceName)
        Me.pnlReferences.Controls.Add(Me.txtRefEmail)
        Me.pnlReferences.Controls.Add(Me.cboRefGender)
        Me.pnlReferences.Controls.Add(Me.lblCountry)
        Me.pnlReferences.Controls.Add(Me.txtRefAddress)
        Me.pnlReferences.Controls.Add(Me.lblRefType)
        Me.pnlReferences.Controls.Add(Me.lblRefGender)
        Me.pnlReferences.Controls.Add(Me.cboReftown)
        Me.pnlReferences.Controls.Add(Me.lblRefTown)
        Me.pnlReferences.Controls.Add(Me.cboRefType)
        Me.pnlReferences.Location = New System.Drawing.Point(3, 25)
        Me.pnlReferences.Name = "pnlReferences"
        Me.pnlReferences.Size = New System.Drawing.Size(863, 346)
        Me.pnlReferences.TabIndex = 245
        '
        'lvReferences
        '
        Me.lvReferences.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhRefName, Me.colhRefPosition, Me.colhRefGender, Me.colhRefType, Me.colhRefEmail, Me.objcolhRefAddress, Me.objcolhRefcountryId, Me.objcolhRefStateId, Me.objColhRefTownId, Me.objColhRefTypeId, Me.objcolhRefTelNo, Me.objColhRefMobileNo, Me.objColhRefGenderId, Me.objColhRefGUID})
        Me.lvReferences.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvReferences.FullRowSelect = True
        Me.lvReferences.GridLines = True
        Me.lvReferences.Location = New System.Drawing.Point(10, 119)
        Me.lvReferences.Name = "lvReferences"
        Me.lvReferences.Size = New System.Drawing.Size(850, 224)
        Me.lvReferences.TabIndex = 234
        Me.lvReferences.UseCompatibleStateImageBehavior = False
        Me.lvReferences.View = System.Windows.Forms.View.Details
        '
        'colhRefName
        '
        Me.colhRefName.Tag = "colhRefName"
        Me.colhRefName.Text = "Name"
        Me.colhRefName.Width = 200
        '
        'colhRefPosition
        '
        Me.colhRefPosition.Tag = "colhRefPosition"
        Me.colhRefPosition.Text = "Position"
        Me.colhRefPosition.Width = 200
        '
        'colhRefGender
        '
        Me.colhRefGender.Tag = "colhRefGender"
        Me.colhRefGender.Text = "Gender"
        Me.colhRefGender.Width = 100
        '
        'colhRefType
        '
        Me.colhRefType.Tag = "colhRefType"
        Me.colhRefType.Text = "Ref. Type"
        Me.colhRefType.Width = 150
        '
        'colhRefEmail
        '
        Me.colhRefEmail.Tag = "colhRefEmail"
        Me.colhRefEmail.Text = "Email"
        Me.colhRefEmail.Width = 150
        '
        'objcolhRefAddress
        '
        Me.objcolhRefAddress.DisplayIndex = 6
        Me.objcolhRefAddress.Tag = "objcolhRefAddress"
        Me.objcolhRefAddress.Text = "Address"
        Me.objcolhRefAddress.Width = 0
        '
        'objcolhRefcountryId
        '
        Me.objcolhRefcountryId.DisplayIndex = 7
        Me.objcolhRefcountryId.Tag = "objcolhRefcountryId"
        Me.objcolhRefcountryId.Text = "objcolhRefcountryId"
        Me.objcolhRefcountryId.Width = 0
        '
        'objcolhRefStateId
        '
        Me.objcolhRefStateId.DisplayIndex = 8
        Me.objcolhRefStateId.Tag = "objcolhRefStateId"
        Me.objcolhRefStateId.Text = "objcolhRefStateId"
        Me.objcolhRefStateId.Width = 0
        '
        'objColhRefTownId
        '
        Me.objColhRefTownId.DisplayIndex = 9
        Me.objColhRefTownId.Tag = "objColhRefTownId"
        Me.objColhRefTownId.Text = "objColhRefTownId"
        Me.objColhRefTownId.Width = 0
        '
        'objColhRefTypeId
        '
        Me.objColhRefTypeId.DisplayIndex = 12
        Me.objColhRefTypeId.Tag = "objColhRefTypeId"
        Me.objColhRefTypeId.Text = "objColhRefTypeId"
        Me.objColhRefTypeId.Width = 0
        '
        'objcolhRefTelNo
        '
        Me.objcolhRefTelNo.Tag = "objcolhRefTelNo"
        Me.objcolhRefTelNo.Text = "objcolhRefTelNo"
        Me.objcolhRefTelNo.Width = 0
        '
        'objColhRefMobileNo
        '
        Me.objColhRefMobileNo.Tag = "objColhRefMobileNo"
        Me.objColhRefMobileNo.Text = "objColhRefMobileNo"
        Me.objColhRefMobileNo.Width = 0
        '
        'objColhRefGenderId
        '
        Me.objColhRefGenderId.DisplayIndex = 13
        Me.objColhRefGenderId.Tag = "objColhRefGenderId"
        Me.objColhRefGenderId.Text = "objColhRefGenderId"
        Me.objColhRefGenderId.Width = 0
        '
        'objColhRefGUID
        '
        Me.objColhRefGUID.DisplayIndex = 5
        Me.objColhRefGUID.Tag = "objColhRefGUID"
        Me.objColhRefGUID.Text = "objColhRefGUID"
        Me.objColhRefGUID.Width = 0
        '
        'btnRefEdit
        '
        Me.btnRefEdit.BackColor = System.Drawing.Color.White
        Me.btnRefEdit.BackgroundImage = CType(resources.GetObject("btnRefEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnRefEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnRefEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRefEdit.FlatAppearance.BorderSize = 0
        Me.btnRefEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefEdit.ForeColor = System.Drawing.Color.Black
        Me.btnRefEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRefEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnRefEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRefEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRefEdit.Location = New System.Drawing.Point(716, 45)
        Me.btnRefEdit.Name = "btnRefEdit"
        Me.btnRefEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRefEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRefEdit.Size = New System.Drawing.Size(92, 30)
        Me.btnRefEdit.TabIndex = 241
        Me.btnRefEdit.Text = "&Edit"
        Me.btnRefEdit.UseVisualStyleBackColor = True
        '
        'lblReferenceName
        '
        Me.lblReferenceName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferenceName.Location = New System.Drawing.Point(3, 12)
        Me.lblReferenceName.Name = "lblReferenceName"
        Me.lblReferenceName.Size = New System.Drawing.Size(69, 15)
        Me.lblReferenceName.TabIndex = 5
        Me.lblReferenceName.Text = "Name"
        Me.lblReferenceName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRefTelNo
        '
        Me.txtRefTelNo.Flags = 0
        Me.txtRefTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRefTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRefTelNo.Location = New System.Drawing.Point(581, 9)
        Me.txtRefTelNo.Name = "txtRefTelNo"
        Me.txtRefTelNo.Size = New System.Drawing.Size(129, 21)
        Me.txtRefTelNo.TabIndex = 229
        '
        'btnRefDelete
        '
        Me.btnRefDelete.BackColor = System.Drawing.Color.White
        Me.btnRefDelete.BackgroundImage = CType(resources.GetObject("btnRefDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnRefDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnRefDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRefDelete.FlatAppearance.BorderSize = 0
        Me.btnRefDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefDelete.ForeColor = System.Drawing.Color.Black
        Me.btnRefDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRefDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnRefDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRefDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRefDelete.Location = New System.Drawing.Point(717, 81)
        Me.btnRefDelete.Name = "btnRefDelete"
        Me.btnRefDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRefDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRefDelete.Size = New System.Drawing.Size(92, 30)
        Me.btnRefDelete.TabIndex = 242
        Me.btnRefDelete.Text = "&Delete"
        Me.btnRefDelete.UseVisualStyleBackColor = True
        '
        'lblRefTelPhNo
        '
        Me.lblRefTelPhNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefTelPhNo.Location = New System.Drawing.Point(518, 11)
        Me.lblRefTelPhNo.Name = "lblRefTelPhNo"
        Me.lblRefTelPhNo.Size = New System.Drawing.Size(57, 16)
        Me.lblRefTelPhNo.TabIndex = 228
        Me.lblRefTelPhNo.Text = "Tel. No"
        Me.lblRefTelPhNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRefPosition
        '
        Me.txtRefPosition.Flags = 0
        Me.txtRefPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRefPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRefPosition.Location = New System.Drawing.Point(75, 36)
        Me.txtRefPosition.Name = "txtRefPosition"
        Me.txtRefPosition.Size = New System.Drawing.Size(213, 21)
        Me.txtRefPosition.TabIndex = 8
        '
        'lblRefMobileNo
        '
        Me.lblRefMobileNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefMobileNo.Location = New System.Drawing.Point(518, 38)
        Me.lblRefMobileNo.Name = "lblRefMobileNo"
        Me.lblRefMobileNo.Size = New System.Drawing.Size(57, 16)
        Me.lblRefMobileNo.TabIndex = 230
        Me.lblRefMobileNo.Text = "Mobile No"
        Me.lblRefMobileNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRefState
        '
        Me.cboRefState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRefState.DropDownWidth = 360
        Me.cboRefState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRefState.FormattingEnabled = True
        Me.cboRefState.Location = New System.Drawing.Point(383, 36)
        Me.cboRefState.Name = "cboRefState"
        Me.cboRefState.Size = New System.Drawing.Size(129, 21)
        Me.cboRefState.TabIndex = 227
        '
        'lblPosition
        '
        Me.lblPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPosition.Location = New System.Drawing.Point(3, 39)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(69, 15)
        Me.lblPosition.TabIndex = 7
        Me.lblPosition.Text = "Position"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRefMobileNo
        '
        Me.txtRefMobileNo.Flags = 0
        Me.txtRefMobileNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRefMobileNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRefMobileNo.Location = New System.Drawing.Point(581, 36)
        Me.txtRefMobileNo.Name = "txtRefMobileNo"
        Me.txtRefMobileNo.Size = New System.Drawing.Size(129, 21)
        Me.txtRefMobileNo.TabIndex = 231
        '
        'btnRefAdd
        '
        Me.btnRefAdd.BackColor = System.Drawing.Color.White
        Me.btnRefAdd.BackgroundImage = CType(resources.GetObject("btnRefAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnRefAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnRefAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRefAdd.FlatAppearance.BorderSize = 0
        Me.btnRefAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefAdd.ForeColor = System.Drawing.Color.Black
        Me.btnRefAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRefAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnRefAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRefAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRefAdd.Location = New System.Drawing.Point(716, 9)
        Me.btnRefAdd.Name = "btnRefAdd"
        Me.btnRefAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRefAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRefAdd.Size = New System.Drawing.Size(92, 30)
        Me.btnRefAdd.TabIndex = 240
        Me.btnRefAdd.Text = "&Add"
        Me.btnRefAdd.UseVisualStyleBackColor = True
        '
        'lblRefState
        '
        Me.lblRefState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefState.Location = New System.Drawing.Point(294, 38)
        Me.lblRefState.Name = "lblRefState"
        Me.lblRefState.Size = New System.Drawing.Size(84, 16)
        Me.lblRefState.TabIndex = 226
        Me.lblRefState.Text = "State/Province"
        Me.lblRefState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRefAddress
        '
        Me.lblRefAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefAddress.Location = New System.Drawing.Point(3, 66)
        Me.lblRefAddress.Name = "lblRefAddress"
        Me.lblRefAddress.Size = New System.Drawing.Size(69, 15)
        Me.lblRefAddress.TabIndex = 9
        Me.lblRefAddress.Text = "Address"
        Me.lblRefAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRefEmail
        '
        Me.lblRefEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefEmail.Location = New System.Drawing.Point(518, 65)
        Me.lblRefEmail.Name = "lblRefEmail"
        Me.lblRefEmail.Size = New System.Drawing.Size(57, 16)
        Me.lblRefEmail.TabIndex = 232
        Me.lblRefEmail.Text = "Email"
        Me.lblRefEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRefCountry
        '
        Me.cboRefCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRefCountry.DropDownWidth = 360
        Me.cboRefCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRefCountry.FormattingEnabled = True
        Me.cboRefCountry.Location = New System.Drawing.Point(383, 9)
        Me.cboRefCountry.Name = "cboRefCountry"
        Me.cboRefCountry.Size = New System.Drawing.Size(129, 21)
        Me.cboRefCountry.TabIndex = 225
        '
        'txtReferenceName
        '
        Me.txtReferenceName.Flags = 0
        Me.txtReferenceName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceName.Location = New System.Drawing.Point(75, 9)
        Me.txtReferenceName.Name = "txtReferenceName"
        Me.txtReferenceName.Size = New System.Drawing.Size(213, 21)
        Me.txtReferenceName.TabIndex = 6
        '
        'txtRefEmail
        '
        Me.txtRefEmail.Flags = 0
        Me.txtRefEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRefEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRefEmail.Location = New System.Drawing.Point(581, 63)
        Me.txtRefEmail.Name = "txtRefEmail"
        Me.txtRefEmail.Size = New System.Drawing.Size(129, 21)
        Me.txtRefEmail.TabIndex = 233
        '
        'cboRefGender
        '
        Me.cboRefGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRefGender.DropDownWidth = 150
        Me.cboRefGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRefGender.FormattingEnabled = True
        Me.cboRefGender.Location = New System.Drawing.Point(581, 90)
        Me.cboRefGender.Name = "cboRefGender"
        Me.cboRefGender.Size = New System.Drawing.Size(129, 21)
        Me.cboRefGender.TabIndex = 238
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(294, 11)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(84, 16)
        Me.lblCountry.TabIndex = 224
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRefAddress
        '
        Me.txtRefAddress.Flags = 0
        Me.txtRefAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRefAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRefAddress.Location = New System.Drawing.Point(75, 62)
        Me.txtRefAddress.Multiline = True
        Me.txtRefAddress.Name = "txtRefAddress"
        Me.txtRefAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRefAddress.Size = New System.Drawing.Size(213, 49)
        Me.txtRefAddress.TabIndex = 10
        '
        'lblRefType
        '
        Me.lblRefType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefType.Location = New System.Drawing.Point(294, 92)
        Me.lblRefType.Name = "lblRefType"
        Me.lblRefType.Size = New System.Drawing.Size(84, 16)
        Me.lblRefType.TabIndex = 235
        Me.lblRefType.Text = "Ref. Type"
        Me.lblRefType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRefGender
        '
        Me.lblRefGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefGender.Location = New System.Drawing.Point(518, 92)
        Me.lblRefGender.Name = "lblRefGender"
        Me.lblRefGender.Size = New System.Drawing.Size(57, 16)
        Me.lblRefGender.TabIndex = 237
        Me.lblRefGender.Text = "Gender"
        Me.lblRefGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReftown
        '
        Me.cboReftown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReftown.DropDownWidth = 360
        Me.cboReftown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReftown.FormattingEnabled = True
        Me.cboReftown.Location = New System.Drawing.Point(383, 63)
        Me.cboReftown.Name = "cboReftown"
        Me.cboReftown.Size = New System.Drawing.Size(129, 21)
        Me.cboReftown.TabIndex = 223
        '
        'lblRefTown
        '
        Me.lblRefTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefTown.Location = New System.Drawing.Point(294, 65)
        Me.lblRefTown.Name = "lblRefTown"
        Me.lblRefTown.Size = New System.Drawing.Size(84, 16)
        Me.lblRefTown.TabIndex = 222
        Me.lblRefTown.Text = "Town/District"
        Me.lblRefTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRefType
        '
        Me.cboRefType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRefType.DropDownWidth = 360
        Me.cboRefType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRefType.FormattingEnabled = True
        Me.cboRefType.Location = New System.Drawing.Point(383, 90)
        Me.cboRefType.Name = "cboRefType"
        Me.cboRefType.Size = New System.Drawing.Size(129, 21)
        Me.cboRefType.TabIndex = 236
        '
        'tabpOtherdetails
        '
        Me.tabpOtherdetails.Controls.Add(Me.gbOtherDetails)
        Me.tabpOtherdetails.Location = New System.Drawing.Point(4, 22)
        Me.tabpOtherdetails.Name = "tabpOtherdetails"
        Me.tabpOtherdetails.Size = New System.Drawing.Size(879, 377)
        Me.tabpOtherdetails.TabIndex = 5
        Me.tabpOtherdetails.Text = "Other Details"
        Me.tabpOtherdetails.UseVisualStyleBackColor = True
        '
        'gbOtherDetails
        '
        Me.gbOtherDetails.BorderColor = System.Drawing.Color.Black
        Me.gbOtherDetails.Checked = False
        Me.gbOtherDetails.CollapseAllExceptThis = False
        Me.gbOtherDetails.CollapsedHoverImage = Nothing
        Me.gbOtherDetails.CollapsedNormalImage = Nothing
        Me.gbOtherDetails.CollapsedPressedImage = Nothing
        Me.gbOtherDetails.CollapseOnLoad = False
        Me.gbOtherDetails.Controls.Add(Me.cboExpectedSalaryCurrency)
        Me.gbOtherDetails.Controls.Add(Me.cboCurrentSalaryCurrency)
        Me.gbOtherDetails.Controls.Add(Me.lblExpectedBenefit)
        Me.gbOtherDetails.Controls.Add(Me.txtExpectedBenefit)
        Me.gbOtherDetails.Controls.Add(Me.txtImpairment)
        Me.gbOtherDetails.Controls.Add(Me.chkImpaired)
        Me.gbOtherDetails.Controls.Add(Me.chkWillingToTravel)
        Me.gbOtherDetails.Controls.Add(Me.chkWillingToRelocate)
        Me.gbOtherDetails.Controls.Add(Me.txtNoticePeriodDays)
        Me.gbOtherDetails.Controls.Add(Me.lblNoticePeriodDays)
        Me.gbOtherDetails.Controls.Add(Me.txtExpectedSalary)
        Me.gbOtherDetails.Controls.Add(Me.lblExpectedSalary)
        Me.gbOtherDetails.Controls.Add(Me.txtCurrentSalary)
        Me.gbOtherDetails.Controls.Add(Me.lblCurrentSalary)
        Me.gbOtherDetails.Controls.Add(Me.txtJournalsResearchPapers)
        Me.gbOtherDetails.Controls.Add(Me.txtAchievements)
        Me.gbOtherDetails.Controls.Add(Me.txtMemberships)
        Me.gbOtherDetails.Controls.Add(Me.lblJournalsResearchPapers)
        Me.gbOtherDetails.Controls.Add(Me.lblAchievements)
        Me.gbOtherDetails.Controls.Add(Me.lblMemberships)
        Me.gbOtherDetails.ExpandedHoverImage = Nothing
        Me.gbOtherDetails.ExpandedNormalImage = Nothing
        Me.gbOtherDetails.ExpandedPressedImage = Nothing
        Me.gbOtherDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtherDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOtherDetails.HeaderHeight = 25
        Me.gbOtherDetails.HeaderMessage = ""
        Me.gbOtherDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOtherDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOtherDetails.HeightOnCollapse = 0
        Me.gbOtherDetails.LeftTextSpace = 0
        Me.gbOtherDetails.Location = New System.Drawing.Point(2, 3)
        Me.gbOtherDetails.Name = "gbOtherDetails"
        Me.gbOtherDetails.OpenHeight = 112
        Me.gbOtherDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOtherDetails.ShowBorder = True
        Me.gbOtherDetails.ShowCheckBox = False
        Me.gbOtherDetails.ShowCollapseButton = False
        Me.gbOtherDetails.ShowDefaultBorderColor = True
        Me.gbOtherDetails.ShowDownButton = False
        Me.gbOtherDetails.ShowHeader = True
        Me.gbOtherDetails.Size = New System.Drawing.Size(869, 371)
        Me.gbOtherDetails.TabIndex = 5
        Me.gbOtherDetails.Temp = 0
        Me.gbOtherDetails.Text = "Other Details"
        Me.gbOtherDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpectedSalaryCurrency
        '
        Me.cboExpectedSalaryCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpectedSalaryCurrency.DropDownWidth = 100
        Me.cboExpectedSalaryCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpectedSalaryCurrency.FormattingEnabled = True
        Me.cboExpectedSalaryCurrency.Items.AddRange(New Object() {"Mr.", "Miss.", "Jr.", "Sr."})
        Me.cboExpectedSalaryCurrency.Location = New System.Drawing.Point(540, 32)
        Me.cboExpectedSalaryCurrency.Name = "cboExpectedSalaryCurrency"
        Me.cboExpectedSalaryCurrency.Size = New System.Drawing.Size(89, 21)
        Me.cboExpectedSalaryCurrency.TabIndex = 30
        '
        'cboCurrentSalaryCurrency
        '
        Me.cboCurrentSalaryCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrentSalaryCurrency.DropDownWidth = 100
        Me.cboCurrentSalaryCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrentSalaryCurrency.FormattingEnabled = True
        Me.cboCurrentSalaryCurrency.Items.AddRange(New Object() {"Mr.", "Miss.", "Jr.", "Sr."})
        Me.cboCurrentSalaryCurrency.Location = New System.Drawing.Point(240, 34)
        Me.cboCurrentSalaryCurrency.Name = "cboCurrentSalaryCurrency"
        Me.cboCurrentSalaryCurrency.Size = New System.Drawing.Size(89, 21)
        Me.cboCurrentSalaryCurrency.TabIndex = 29
        '
        'lblExpectedBenefit
        '
        Me.lblExpectedBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpectedBenefit.Location = New System.Drawing.Point(8, 106)
        Me.lblExpectedBenefit.Name = "lblExpectedBenefit"
        Me.lblExpectedBenefit.Size = New System.Drawing.Size(105, 15)
        Me.lblExpectedBenefit.TabIndex = 27
        Me.lblExpectedBenefit.Text = "Expected Benefit"
        Me.lblExpectedBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExpectedBenefit
        '
        Me.txtExpectedBenefit.Flags = 0
        Me.txtExpectedBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpectedBenefit.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtExpectedBenefit.Location = New System.Drawing.Point(119, 106)
        Me.txtExpectedBenefit.Name = "txtExpectedBenefit"
        Me.txtExpectedBenefit.Size = New System.Drawing.Size(690, 21)
        Me.txtExpectedBenefit.TabIndex = 26
        '
        'txtImpairment
        '
        Me.txtImpairment.Enabled = False
        Me.txtImpairment.Flags = 0
        Me.txtImpairment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImpairment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtImpairment.Location = New System.Drawing.Point(293, 79)
        Me.txtImpairment.Name = "txtImpairment"
        Me.txtImpairment.Size = New System.Drawing.Size(516, 21)
        Me.txtImpairment.TabIndex = 24
        '
        'chkImpaired
        '
        Me.chkImpaired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkImpaired.Location = New System.Drawing.Point(119, 83)
        Me.chkImpaired.Name = "chkImpaired"
        Me.chkImpaired.Size = New System.Drawing.Size(150, 17)
        Me.chkImpaired.TabIndex = 23
        Me.chkImpaired.Text = "Impaired"
        Me.chkImpaired.UseVisualStyleBackColor = True
        '
        'chkWillingToTravel
        '
        Me.chkWillingToTravel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWillingToTravel.Location = New System.Drawing.Point(401, 60)
        Me.chkWillingToTravel.Name = "chkWillingToTravel"
        Me.chkWillingToTravel.Size = New System.Drawing.Size(150, 17)
        Me.chkWillingToTravel.TabIndex = 22
        Me.chkWillingToTravel.Text = "Willing To Travel"
        Me.chkWillingToTravel.UseVisualStyleBackColor = True
        '
        'chkWillingToRelocate
        '
        Me.chkWillingToRelocate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWillingToRelocate.Location = New System.Drawing.Point(119, 60)
        Me.chkWillingToRelocate.Name = "chkWillingToRelocate"
        Me.chkWillingToRelocate.Size = New System.Drawing.Size(150, 17)
        Me.chkWillingToRelocate.TabIndex = 21
        Me.chkWillingToRelocate.Text = "Willing To Relocate"
        Me.chkWillingToRelocate.UseVisualStyleBackColor = True
        '
        'txtNoticePeriodDays
        '
        Me.txtNoticePeriodDays.AllowNegative = True
        Me.txtNoticePeriodDays.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNoticePeriodDays.DigitsInGroup = 0
        Me.txtNoticePeriodDays.Flags = 0
        Me.txtNoticePeriodDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoticePeriodDays.Location = New System.Drawing.Point(747, 33)
        Me.txtNoticePeriodDays.MaxDecimalPlaces = 6
        Me.txtNoticePeriodDays.MaxWholeDigits = 21
        Me.txtNoticePeriodDays.Name = "txtNoticePeriodDays"
        Me.txtNoticePeriodDays.Prefix = ""
        Me.txtNoticePeriodDays.RangeMax = 1.7976931348623157E+308
        Me.txtNoticePeriodDays.RangeMin = -1.7976931348623157E+308
        Me.txtNoticePeriodDays.Size = New System.Drawing.Size(63, 21)
        Me.txtNoticePeriodDays.TabIndex = 20
        Me.txtNoticePeriodDays.Text = "0"
        Me.txtNoticePeriodDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNoticePeriodDays
        '
        Me.lblNoticePeriodDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoticePeriodDays.Location = New System.Drawing.Point(639, 36)
        Me.lblNoticePeriodDays.Name = "lblNoticePeriodDays"
        Me.lblNoticePeriodDays.Size = New System.Drawing.Size(105, 15)
        Me.lblNoticePeriodDays.TabIndex = 19
        Me.lblNoticePeriodDays.Text = "Notice Period (Days)"
        Me.lblNoticePeriodDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExpectedSalary
        '
        Me.txtExpectedSalary.AllowNegative = True
        Me.txtExpectedSalary.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtExpectedSalary.DigitsInGroup = 0
        Me.txtExpectedSalary.Flags = 0
        Me.txtExpectedSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpectedSalary.Location = New System.Drawing.Point(419, 33)
        Me.txtExpectedSalary.MaxDecimalPlaces = 6
        Me.txtExpectedSalary.MaxWholeDigits = 21
        Me.txtExpectedSalary.Name = "txtExpectedSalary"
        Me.txtExpectedSalary.Prefix = ""
        Me.txtExpectedSalary.RangeMax = 1.7976931348623157E+308
        Me.txtExpectedSalary.RangeMin = -1.7976931348623157E+308
        Me.txtExpectedSalary.Size = New System.Drawing.Size(114, 21)
        Me.txtExpectedSalary.TabIndex = 18
        Me.txtExpectedSalary.Text = "0"
        Me.txtExpectedSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblExpectedSalary
        '
        Me.lblExpectedSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpectedSalary.Location = New System.Drawing.Point(341, 36)
        Me.lblExpectedSalary.Name = "lblExpectedSalary"
        Me.lblExpectedSalary.Size = New System.Drawing.Size(85, 15)
        Me.lblExpectedSalary.TabIndex = 17
        Me.lblExpectedSalary.Text = "Expected Salary"
        Me.lblExpectedSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCurrentSalary
        '
        Me.txtCurrentSalary.AllowNegative = True
        Me.txtCurrentSalary.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentSalary.DigitsInGroup = 0
        Me.txtCurrentSalary.Flags = 0
        Me.txtCurrentSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentSalary.Location = New System.Drawing.Point(119, 33)
        Me.txtCurrentSalary.MaxDecimalPlaces = 6
        Me.txtCurrentSalary.MaxWholeDigits = 21
        Me.txtCurrentSalary.Name = "txtCurrentSalary"
        Me.txtCurrentSalary.Prefix = ""
        Me.txtCurrentSalary.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentSalary.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentSalary.Size = New System.Drawing.Size(114, 21)
        Me.txtCurrentSalary.TabIndex = 16
        Me.txtCurrentSalary.Text = "0"
        Me.txtCurrentSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCurrentSalary
        '
        Me.lblCurrentSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentSalary.Location = New System.Drawing.Point(8, 36)
        Me.lblCurrentSalary.Name = "lblCurrentSalary"
        Me.lblCurrentSalary.Size = New System.Drawing.Size(105, 15)
        Me.lblCurrentSalary.TabIndex = 15
        Me.lblCurrentSalary.Text = "Current Salary"
        Me.lblCurrentSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJournalsResearchPapers
        '
        Me.txtJournalsResearchPapers.Flags = 0
        Me.txtJournalsResearchPapers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJournalsResearchPapers.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJournalsResearchPapers.Location = New System.Drawing.Point(119, 291)
        Me.txtJournalsResearchPapers.MaxLength = 2147483647
        Me.txtJournalsResearchPapers.Multiline = True
        Me.txtJournalsResearchPapers.Name = "txtJournalsResearchPapers"
        Me.txtJournalsResearchPapers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtJournalsResearchPapers.Size = New System.Drawing.Size(690, 70)
        Me.txtJournalsResearchPapers.TabIndex = 13
        '
        'txtAchievements
        '
        Me.txtAchievements.Flags = 0
        Me.txtAchievements.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAchievements.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAchievements.Location = New System.Drawing.Point(119, 215)
        Me.txtAchievements.MaxLength = 2147483647
        Me.txtAchievements.Multiline = True
        Me.txtAchievements.Name = "txtAchievements"
        Me.txtAchievements.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAchievements.Size = New System.Drawing.Size(690, 70)
        Me.txtAchievements.TabIndex = 12
        '
        'txtMemberships
        '
        Me.txtMemberships.Flags = 0
        Me.txtMemberships.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMemberships.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMemberships.Location = New System.Drawing.Point(119, 139)
        Me.txtMemberships.MaxLength = 2147483647
        Me.txtMemberships.Multiline = True
        Me.txtMemberships.Name = "txtMemberships"
        Me.txtMemberships.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMemberships.Size = New System.Drawing.Size(690, 70)
        Me.txtMemberships.TabIndex = 11
        '
        'lblJournalsResearchPapers
        '
        Me.lblJournalsResearchPapers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJournalsResearchPapers.Location = New System.Drawing.Point(8, 291)
        Me.lblJournalsResearchPapers.Name = "lblJournalsResearchPapers"
        Me.lblJournalsResearchPapers.Size = New System.Drawing.Size(105, 39)
        Me.lblJournalsResearchPapers.TabIndex = 8
        Me.lblJournalsResearchPapers.Text = "Journals/Research Papers"
        Me.lblJournalsResearchPapers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAchievements
        '
        Me.lblAchievements.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAchievements.Location = New System.Drawing.Point(8, 215)
        Me.lblAchievements.Name = "lblAchievements"
        Me.lblAchievements.Size = New System.Drawing.Size(105, 15)
        Me.lblAchievements.TabIndex = 7
        Me.lblAchievements.Text = "Achievements"
        Me.lblAchievements.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMemberships
        '
        Me.lblMemberships.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemberships.Location = New System.Drawing.Point(8, 139)
        Me.lblMemberships.Name = "lblMemberships"
        Me.lblMemberships.Size = New System.Drawing.Size(105, 15)
        Me.lblMemberships.TabIndex = 6
        Me.lblMemberships.Text = "Memberships"
        Me.lblMemberships.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSaveInfo)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 560)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(890, 62)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(785, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 28
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSaveInfo
        '
        Me.btnSaveInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveInfo.BackColor = System.Drawing.Color.White
        Me.btnSaveInfo.BackgroundImage = CType(resources.GetObject("btnSaveInfo.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveInfo.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveInfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveInfo.FlatAppearance.BorderSize = 0
        Me.btnSaveInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveInfo.ForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveInfo.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Location = New System.Drawing.Point(686, 13)
        Me.btnSaveInfo.Name = "btnSaveInfo"
        Me.btnSaveInfo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Size = New System.Drawing.Size(93, 30)
        Me.btnSaveInfo.TabIndex = 27
        Me.btnSaveInfo.Text = "&Save"
        Me.btnSaveInfo.UseVisualStyleBackColor = True
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Tag = "colhEmployer"
        Me.ColumnHeader1.Text = "Employer"
        Me.ColumnHeader1.Width = 86
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Tag = "colhCompanyName"
        Me.ColumnHeader2.Text = "Company"
        Me.ColumnHeader2.Width = 102
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Phone"
        Me.ColumnHeader3.Width = 83
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Designation"
        Me.ColumnHeader4.Width = 73
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Tag = "colhResponsibility"
        Me.ColumnHeader5.Text = "Responsibility"
        Me.ColumnHeader5.Width = 80
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Tag = "colhJoinDate"
        Me.ColumnHeader6.Text = "Join Date"
        Me.ColumnHeader6.Width = 72
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Tag = "colhTermDate"
        Me.ColumnHeader7.Text = "Term. Date"
        Me.ColumnHeader7.Width = 71
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Leaving Reason"
        Me.ColumnHeader8.Width = 230
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "objcolhJobhistoryGUID"
        Me.ColumnHeader9.Width = 0
        '
        'pnlNida
        '
        Me.pnlNida.Controls.Add(Me.elBasicInfo)
        Me.pnlNida.Controls.Add(Me.txtIndexNo)
        Me.pnlNida.Controls.Add(Me.lblNin)
        Me.pnlNida.Controls.Add(Me.lblIndexNumber)
        Me.pnlNida.Controls.Add(Me.txtNidaNumber)
        Me.pnlNida.Controls.Add(Me.cboResidencyType)
        Me.pnlNida.Controls.Add(Me.lblTIN)
        Me.pnlNida.Controls.Add(Me.lblResidencyType)
        Me.pnlNida.Controls.Add(Me.txtTin)
        Me.pnlNida.Controls.Add(Me.txtPhoneNumber)
        Me.pnlNida.Controls.Add(Me.lblNidaMobile)
        Me.pnlNida.Controls.Add(Me.lblPhone1)
        Me.pnlNida.Controls.Add(Me.txtNidaMobile)
        Me.pnlNida.Controls.Add(Me.txtVillage)
        Me.pnlNida.Controls.Add(Me.lblVillage)
        Me.pnlNida.Location = New System.Drawing.Point(3, 3)
        Me.pnlNida.Name = "pnlNida"
        Me.pnlNida.Size = New System.Drawing.Size(817, 109)
        Me.pnlNida.TabIndex = 543
        Me.pnlNida.Visible = False
        '
        'pnlPresentAddress
        '
        Me.pnlPresentAddress.Controls.Add(Me.lnPresentAddress)
        Me.pnlPresentAddress.Controls.Add(Me.lblAddress)
        Me.pnlPresentAddress.Controls.Add(Me.cboPostTown)
        Me.pnlPresentAddress.Controls.Add(Me.lblTelNo)
        Me.pnlPresentAddress.Controls.Add(Me.lblMobile)
        Me.pnlPresentAddress.Controls.Add(Me.lblPloteNo)
        Me.pnlPresentAddress.Controls.Add(Me.cboCountry)
        Me.pnlPresentAddress.Controls.Add(Me.txtTelNo)
        Me.pnlPresentAddress.Controls.Add(Me.txtPlotNo)
        Me.pnlPresentAddress.Controls.Add(Me.lblPostcode)
        Me.pnlPresentAddress.Controls.Add(Me.txtMobile)
        Me.pnlPresentAddress.Controls.Add(Me.lblAlternativeNo)
        Me.pnlPresentAddress.Controls.Add(Me.lblEstate)
        Me.pnlPresentAddress.Controls.Add(Me.txtEstate)
        Me.pnlPresentAddress.Controls.Add(Me.cboPostCode)
        Me.pnlPresentAddress.Controls.Add(Me.txtAlternativeNo)
        Me.pnlPresentAddress.Controls.Add(Me.lblFax)
        Me.pnlPresentAddress.Controls.Add(Me.lblPostCountry)
        Me.pnlPresentAddress.Controls.Add(Me.txtFax)
        Me.pnlPresentAddress.Controls.Add(Me.cboState)
        Me.pnlPresentAddress.Controls.Add(Me.lblRoad)
        Me.pnlPresentAddress.Controls.Add(Me.txtRoad)
        Me.pnlPresentAddress.Controls.Add(Me.lblState)
        Me.pnlPresentAddress.Controls.Add(Me.txtAddress2)
        Me.pnlPresentAddress.Controls.Add(Me.txtAddress1)
        Me.pnlPresentAddress.Controls.Add(Me.lblPostTown)
        Me.pnlPresentAddress.Controls.Add(Me.lblProvince)
        Me.pnlPresentAddress.Controls.Add(Me.txtProvince)
        Me.pnlPresentAddress.Location = New System.Drawing.Point(3, 118)
        Me.pnlPresentAddress.Name = "pnlPresentAddress"
        Me.pnlPresentAddress.Size = New System.Drawing.Size(817, 165)
        Me.pnlPresentAddress.TabIndex = 544
        '
        'pnlPermanentAddress
        '
        Me.pnlPermanentAddress.Controls.Add(Me.lnPermanentAddress)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermAddress)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermAddress1)
        Me.pnlPermanentAddress.Controls.Add(Me.cboPermCountry)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermAddress2)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermPostCountry)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermPostTown)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermPostCode)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermEstate)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermState)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermRoad)
        Me.pnlPermanentAddress.Controls.Add(Me.cboPermPostTown)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermRoad)
        Me.pnlPermanentAddress.Controls.Add(Me.cboPermPostCode)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermEstate)
        Me.pnlPermanentAddress.Controls.Add(Me.cboPermState)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermProvince)
        Me.pnlPermanentAddress.Controls.Add(Me.lnkCopyAddress)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermProvince)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermFax)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermPlotNo)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermAltNo)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermPlotNo)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermFax)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermMobileNo)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermAltNo)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermMobile)
        Me.pnlPermanentAddress.Controls.Add(Me.txtPermTelNo)
        Me.pnlPermanentAddress.Controls.Add(Me.lblPermTelNo)
        Me.pnlPermanentAddress.Location = New System.Drawing.Point(3, 289)
        Me.pnlPermanentAddress.Name = "pnlPermanentAddress"
        Me.pnlPermanentAddress.Size = New System.Drawing.Size(817, 172)
        Me.pnlPermanentAddress.TabIndex = 545
        '
        'fpnlData
        '
        Me.fpnlData.AutoSize = True
        Me.fpnlData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.fpnlData.Controls.Add(Me.pnlNida)
        Me.fpnlData.Controls.Add(Me.pnlPresentAddress)
        Me.fpnlData.Controls.Add(Me.pnlPermanentAddress)
        Me.fpnlData.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.fpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fpnlData.Location = New System.Drawing.Point(11, 2)
        Me.fpnlData.Name = "fpnlData"
        Me.fpnlData.Size = New System.Drawing.Size(823, 464)
        Me.fpnlData.TabIndex = 543
        '
        'frmApplicantMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(890, 622)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantMaster"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Applicant Master"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbApplicantInfo.ResumeLayout(False)
        Me.gbApplicantInfo.PerformLayout()
        Me.tabcApplicantInformation.ResumeLayout(False)
        Me.tabpPersonalInfo.ResumeLayout(False)
        Me.pnlPersonalInfo.ResumeLayout(False)
        Me.gbPrsonalInfo.ResumeLayout(False)
        Me.pnlPersonalInfoinner.ResumeLayout(False)
        Me.pnlPersonalInfoinner.PerformLayout()
        Me.tabpAdditionalInfo.ResumeLayout(False)
        Me.pnlAdditionalInfo.ResumeLayout(False)
        Me.gbSkillInfo.ResumeLayout(False)
        Me.gbSkillInfo.PerformLayout()
        Me.pnlOtherSkill.ResumeLayout(False)
        Me.pnlOtherSkill.PerformLayout()
        Me.pnlSkillInfo.ResumeLayout(False)
        Me.gbOtherInfo.ResumeLayout(False)
        Me.gbOtherInfo.PerformLayout()
        Me.tabpEducationalInfo.ResumeLayout(False)
        Me.pnlQualificationDetail.ResumeLayout(False)
        Me.gbQualificationInfo.ResumeLayout(False)
        Me.gbQualificationInfo.PerformLayout()
        CType(Me.nudGPA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlQualifyInfo.ResumeLayout(False)
        Me.pnlOtherQualification.ResumeLayout(False)
        Me.tabpEmployment.ResumeLayout(False)
        Me.pnlJobInfo.ResumeLayout(False)
        Me.gbJobHistory.ResumeLayout(False)
        Me.gbJobHistory.PerformLayout()
        Me.pnlJobHistory.ResumeLayout(False)
        Me.tabpReferences.ResumeLayout(False)
        Me.gbReferences.ResumeLayout(False)
        Me.pnlReferences.ResumeLayout(False)
        Me.pnlReferences.PerformLayout()
        Me.tabpOtherdetails.ResumeLayout(False)
        Me.gbOtherDetails.ResumeLayout(False)
        Me.gbOtherDetails.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.pnlNida.ResumeLayout(False)
        Me.pnlNida.PerformLayout()
        Me.pnlPresentAddress.ResumeLayout(False)
        Me.pnlPresentAddress.PerformLayout()
        Me.pnlPermanentAddress.ResumeLayout(False)
        Me.pnlPermanentAddress.PerformLayout()
        Me.fpnlData.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSaveInfo As eZee.Common.eZeeLightButton
    Friend WithEvents gbApplicantInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tabcApplicantInformation As System.Windows.Forms.TabControl
    Friend WithEvents tabpPersonalInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpEducationalInfo As System.Windows.Forms.TabPage
    Friend WithEvents txtApplcantFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtApplicantOtherName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtApplicantSurname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents tabpEmployment As System.Windows.Forms.TabPage
    Friend WithEvents tabpAdditionalInfo As System.Windows.Forms.TabPage
    Friend WithEvents lblOthername As System.Windows.Forms.Label
    Friend WithEvents lblApplicantFirstname As System.Windows.Forms.Label
    Friend WithEvents lblSurname As System.Windows.Forms.Label
    Friend WithEvents imgImageControl As eZee.Common.eZeeImageControl
    Friend WithEvents pnlPersonalInfo As System.Windows.Forms.Panel
    Friend WithEvents pnlAdditionalInfo As System.Windows.Forms.Panel
    Friend WithEvents pnlQualificationDetail As System.Windows.Forms.Panel
    Friend WithEvents pnlJobInfo As System.Windows.Forms.Panel
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents txtApplicantCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboTitle As System.Windows.Forms.ComboBox
    Friend WithEvents lblApplicantCode As System.Windows.Forms.Label
    Friend WithEvents gbSkillInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbOtherInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblBirtdate As System.Windows.Forms.Label
    Friend WithEvents lblMaritalStatus As System.Windows.Forms.Label
    Friend WithEvents dtpBirthdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboMaritalStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblMaritalDate As System.Windows.Forms.Label
    Friend WithEvents dtpAnniversaryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbPrsonalInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbQualificationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbJobHistory As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlPersonalInfoinner As System.Windows.Forms.Panel
    Friend WithEvents cboPostTown As System.Windows.Forms.ComboBox
    Friend WithEvents cboPermCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblPermPostCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblPermPostCode As System.Windows.Forms.Label
    Friend WithEvents lblPostcode As System.Windows.Forms.Label
    Friend WithEvents lblPermState As System.Windows.Forms.Label
    Friend WithEvents cboPermPostTown As System.Windows.Forms.ComboBox
    Friend WithEvents cboPermPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboPermState As System.Windows.Forms.ComboBox
    Friend WithEvents lblPostCountry As System.Windows.Forms.Label
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents lnkCopyAddress As System.Windows.Forms.LinkLabel
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents txtPermFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPermAltNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPermFax As System.Windows.Forms.Label
    Friend WithEvents lblPermAltNo As System.Windows.Forms.Label
    Friend WithEvents txtPermTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPermTelNo As System.Windows.Forms.Label
    Friend WithEvents txtPermMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPermMobileNo As System.Windows.Forms.Label
    Friend WithEvents txtPermPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPermPlotNo As System.Windows.Forms.Label
    Friend WithEvents txtPermProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPermProvince As System.Windows.Forms.Label
    Friend WithEvents txtPermEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPermRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPermRoad As System.Windows.Forms.Label
    Friend WithEvents lblPermEstate As System.Windows.Forms.Label
    Friend WithEvents lblPermPostTown As System.Windows.Forms.Label
    Friend WithEvents txtPermAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPermAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPermAddress As System.Windows.Forms.Label
    Friend WithEvents lnPermanentAddress As eZee.Common.eZeeLine
    Friend WithEvents txtProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblProvince As System.Windows.Forms.Label
    Friend WithEvents lblPostTown As System.Windows.Forms.Label
    Friend WithEvents txtAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRoad As System.Windows.Forms.Label
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtAlternativeNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEstate As System.Windows.Forms.Label
    Friend WithEvents lblAlternativeNo As System.Windows.Forms.Label
    Friend WithEvents txtMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPloteNo As System.Windows.Forms.Label
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents lblTelNo As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lnPresentAddress As eZee.Common.eZeeLine
    Friend WithEvents lblNationality As System.Windows.Forms.Label
    Friend WithEvents lblLanguage1 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboNationality As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage2 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage4 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboLanguage4 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage3 As System.Windows.Forms.Label
    Friend WithEvents objbtnAddSkill As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddSkillCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlSkillInfo As System.Windows.Forms.Panel
    Friend WithEvents lvApplicantSkill As System.Windows.Forms.ListView
    Friend WithEvents colhSkillCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSkillName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSkillRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSkillGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSkillCategoryunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSkillunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objelSkill As eZee.Common.eZeeLine
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents cboSkill As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkillName As System.Windows.Forms.Label
    Friend WithEvents cboSkillCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkillCategory As System.Windows.Forms.Label
    Friend WithEvents pnlQualifyInfo As System.Windows.Forms.Panel
    Friend WithEvents lvQualificationList As eZee.Common.eZeeListView
    Friend WithEvents colhQualifyGroupName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhQualificationName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInstitute As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhQualifyGroupunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhQualifyunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhTransdate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhInstituteId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhQualifyGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddQulification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddInstitution As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboInstitution As System.Windows.Forms.ComboBox
    Friend WithEvents dtpQEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpQStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents lblInstitution As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnQualifyDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnQualifyEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnQualifyAdd As eZee.Common.eZeeLightButton
    Friend WithEvents txtReferenceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboQualification As System.Windows.Forms.ComboBox
    Friend WithEvents txtQualificationRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboQualificationGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents lblQualificationGroup As System.Windows.Forms.Label
    Friend WithEvents dtpQualifyDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReferenceNo As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblAwardDate As System.Windows.Forms.Label
    Friend WithEvents btnJobHistoryDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnJobHistoryEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnJobHistoryAdd As eZee.Common.eZeeLightButton
    Friend WithEvents pnlJobHistory As System.Windows.Forms.Panel
    Friend WithEvents lvJobHistory As System.Windows.Forms.ListView
    Friend WithEvents colhEmployer As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompanyName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOfficePhone As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDesignation As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResponsibility As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJoinDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTermDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLeavingReason As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhJobhistoryGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents dtpTerminationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpJoinDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtLeavingReason As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtResponsibility As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDesignation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOfficePhone As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCompanyName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployerName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblTermDate As System.Windows.Forms.Label
    Friend WithEvents lblOrgDescription As System.Windows.Forms.Label
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents lblResponsibility As System.Windows.Forms.Label
    Friend WithEvents lblOfficePhone As System.Windows.Forms.Label
    Friend WithEvents lblJoinDate As System.Windows.Forms.Label
    Friend WithEvents lblDesignation As System.Windows.Forms.Label
    Friend WithEvents lblEmployer As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblOtherSkill As System.Windows.Forms.Label
    Friend WithEvents txtOtherSkill As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherQualification As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOtherQualification As System.Windows.Forms.Label
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents txtEmployeeCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents lblGPA As System.Windows.Forms.Label
    Friend WithEvents cboResultCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents objcolhResultCodeID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhgpacode As System.Windows.Forms.ColumnHeader
    Friend WithEvents nudGPA As System.Windows.Forms.NumericUpDown
    Friend WithEvents objbtnAddVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents txtVacancy As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabpReferences As System.Windows.Forms.TabPage
    Friend WithEvents gbReferences As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtReferenceName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblReferenceName As System.Windows.Forms.Label
    Friend WithEvents lblRefAddress As System.Windows.Forms.Label
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents txtRefPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRefAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboRefCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboReftown As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefTown As System.Windows.Forms.Label
    Friend WithEvents cboRefState As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefState As System.Windows.Forms.Label
    Friend WithEvents txtRefEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRefEmail As System.Windows.Forms.Label
    Friend WithEvents txtRefMobileNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRefMobileNo As System.Windows.Forms.Label
    Friend WithEvents txtRefTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRefTelPhNo As System.Windows.Forms.Label
    Friend WithEvents lvReferences As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboRefGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefGender As System.Windows.Forms.Label
    Friend WithEvents cboRefType As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefType As System.Windows.Forms.Label
    Friend WithEvents btnRefDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnRefEdit As eZee.Common.eZeeLightButton
    Friend WithEvents pnlReferences As System.Windows.Forms.Panel
    Friend WithEvents colhRefName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefPosition As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefGender As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefEmail As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhRefGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhRefAddress As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhRefcountryId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhRefStateId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhRefTownId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhRefTelNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhRefMobileNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhRefTypeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhRefGenderId As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlOtherQualification As System.Windows.Forms.Panel
    Friend WithEvents txtOtherResultCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherInstitute As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherQualificationGrp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOtherInstitution As System.Windows.Forms.Label
    Friend WithEvents lblOtherResultCode As System.Windows.Forms.Label
    Friend WithEvents lblOtherQualificationGrp As System.Windows.Forms.Label
    Friend WithEvents lnkOtherQualification As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlOtherSkill As System.Windows.Forms.Panel
    Friend WithEvents txtOtherSkillCategory As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOtherSkillCategory As System.Windows.Forms.Label
    Friend WithEvents lnkOtherSkill As System.Windows.Forms.LinkLabel
    Friend WithEvents btnRefAdd As eZee.Common.eZeeLightButton
    Friend WithEvents tabpOtherdetails As System.Windows.Forms.TabPage
    Friend WithEvents gbOtherDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAchievements As System.Windows.Forms.Label
    Friend WithEvents lblMemberships As System.Windows.Forms.Label
    Friend WithEvents lblJournalsResearchPapers As System.Windows.Forms.Label
    Friend WithEvents txtMemberships As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJournalsResearchPapers As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAchievements As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCertiNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCertiNo As System.Windows.Forms.Label
    Friend WithEvents objColhCertiNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtJobAchievement As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblJobAchievement As System.Windows.Forms.Label
    Friend WithEvents objcolhJobAchievement As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboSkillExpertise As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkillExpertise As System.Windows.Forms.Label
    Friend WithEvents colhSkillExpertise As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkHighestqualification As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhHighestQualification As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblMotherTongue As System.Windows.Forms.Label
    Friend WithEvents txtMotherTongue As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCurrentSalary As System.Windows.Forms.Label
    Friend WithEvents txtCurrentSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents txtExpectedSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents lblExpectedSalary As System.Windows.Forms.Label
    Friend WithEvents txtNoticePeriodDays As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNoticePeriodDays As System.Windows.Forms.Label
    Friend WithEvents chkWillingToTravel As System.Windows.Forms.CheckBox
    Friend WithEvents chkWillingToRelocate As System.Windows.Forms.CheckBox
    Friend WithEvents chkImpaired As System.Windows.Forms.CheckBox
    Friend WithEvents txtImpairment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtExpectedBenefit As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblExpectedBenefit As System.Windows.Forms.Label
    Friend WithEvents cboExpectedSalaryCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents cboCurrentSalaryCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents chkqualificationgroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkInstitute As System.Windows.Forms.CheckBox
    Friend WithEvents chkResultCode As System.Windows.Forms.CheckBox
    Friend WithEvents chkqualificationAward As System.Windows.Forms.CheckBox
    Friend WithEvents elBasicInfo As eZee.Common.eZeeLine
    Friend WithEvents txtNidaMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNidaMobile As System.Windows.Forms.Label
    Friend WithEvents txtTin As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblTIN As System.Windows.Forms.Label
    Friend WithEvents txtNidaNumber As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNin As System.Windows.Forms.Label
    Friend WithEvents cboResidencyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblResidencyType As System.Windows.Forms.Label
    Friend WithEvents txtPhoneNumber As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPhone1 As System.Windows.Forms.Label
    Friend WithEvents txtVillage As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVillage As System.Windows.Forms.Label
    Friend WithEvents txtIndexNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblIndexNumber As System.Windows.Forms.Label
    Friend WithEvents pnlNida As System.Windows.Forms.Panel
    Friend WithEvents pnlPermanentAddress As System.Windows.Forms.Panel
    Friend WithEvents pnlPresentAddress As System.Windows.Forms.Panel
    Friend WithEvents fpnlData As System.Windows.Forms.FlowLayoutPanel
End Class
