﻿Option Strict On
Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmApplicantChangeBatch

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApplicantChangeBatch"
    'Private mstrApplicantunkid As String = String.Empty
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAppBatchScheduletranunkid As Integer
    Private objScheduledBatch As clsBatchSchedule
    Private objAppBatchScheduleTran As clsApplicant_Batchschedule_Tran
    Private mintApplicantID As Integer = -1
    Private objApplicantData As clsApplicant_master

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal enAction As enAction) As Boolean

        Try
            mintAppBatchScheduletranunkid = intUnkId

            menAction = enAction
            Me.ShowDialog()
            intUnkId = mintAppBatchScheduletranunkid
            'Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim mstrBatchId As String = ""

        Try
            mstrBatchId = objAppBatchScheduleTran.GetApplicantBatch("List", mintApplicantID)

            objApplicantData._Applicantunkid = mintApplicantID


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objScheduledBatch.getListForCombo("List", True, mstrBatchId, CInt(objApplicantData._Vacancyunkid))
            dsList = objScheduledBatch.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True, mstrBatchId, CInt(objApplicantData._Vacancyunkid))
            'Shani(24-Aug-2015) -- End


            With cboBatchName
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Sub

    Private Sub GetBatchDetails()
        Dim dsList As New DataSet

        Try
            dsList = objAppBatchScheduleTran.GetList("List", mintAppBatchScheduletranunkid)

            txtApplicantName.Text = CStr(dsList.Tables(0).Rows(0).Item("ApplicantName").ToString)
            txtInterviewDate.Text = CStr(eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("Interviewdate").ToString).ToShortDateString) & " " & CStr(dsList.Tables(0).Rows(0).Item("Interviewtime"))
            txtInterviewType.Text = CStr(dsList.Tables(0).Rows(0).Item("InterviewType"))
            txtLocation.Text = CStr(dsList.Tables(0).Rows(0).Item("location"))
            txtVacancy.Text = CStr(dsList.Tables(0).Rows(0).Item("vacancy"))
            mintApplicantID = CInt(dsList.Tables(0).Rows(0).Item("applicantunkid"))
            txtOldBatchName.Text = CStr(dsList.Tables(0).Rows(0).Item("BatchName"))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetBatchDetails", mstrModuleName)
        Finally
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Sub
#End Region

#Region " Form Events "

    Private Sub frmApplicantChangeBatch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objScheduledBatch = New clsBatchSchedule
        objAppBatchScheduleTran = New clsApplicant_Batchschedule_Tran
        objApplicantData = New clsApplicant_master
        Try
            Call Set_Logo(Me, gApplicationType)
            txtApplicantName.Text = ""
            txtInterviewDate.Text = ""
            txtInterviewType.Text = ""
            txtLocation.Text = ""
            txtVacancy.Text = ""
            txtNewInterviewDate.Text = ""
            txtNewInterviewType.Text = ""
            txtNewLocation.Text = ""
            txtNewVacancy.Text = ""
            txtOldBatchName.Text = ""

            Call GetBatchDetails()
            Call FillCombo()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantChangeBatch_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmVendor_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendor_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVendor_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objScheduledBatch = Nothing
            objAppBatchScheduleTran = Nothing
            objApplicantData = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVendor_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub


    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsBatchSchedule.SetMessages()
            clsApplicant_Batchschedule_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchSchedule,clsApplicant_Batchschedule_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region
    
#Region " Buttons Events"
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim mblnFlag As Boolean = False
        If CInt(cboBatchName.SelectedValue) < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Batch. Batch is the mandatory information."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            If menAction = enAction.EDIT_ONE Then
                objAppBatchScheduleTran._IsChanged = True
                objAppBatchScheduleTran._Batchscheduleunkid = CInt(cboBatchName.SelectedValue)
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objAppBatchScheduleTran._FormName = mstrModuleName
                objAppBatchScheduleTran._LoginEmployeeunkid = 0
                objAppBatchScheduleTran._ClientIP = getIP()
                objAppBatchScheduleTran._HostName = getHostName()
                objAppBatchScheduleTran._FromWeb = False
                objAppBatchScheduleTran._AuditUserId = User._Object._Userunkid
objAppBatchScheduleTran._CompanyUnkid = Company._Object._Companyunkid
                objAppBatchScheduleTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END
                mblnFlag = objAppBatchScheduleTran.Update_changebatch(mintAppBatchScheduletranunkid)
                If mblnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Batch changed successfully!"), enMsgBoxStyle.Information)
                End If
            End If
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Control Events "
    Private Sub cboBatchName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBatchName.SelectedIndexChanged
        Dim dsList As New DataSet
        Try
            If CInt(cboBatchName.SelectedValue) > 0 Then
                dsList = objScheduledBatch.GetList("List", False, CInt(cboBatchName.SelectedValue))

                txtNewInterviewDate.Text = CStr(eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("Interviewdate").ToString).ToShortDateString) & " " & CStr(dsList.Tables(0).Rows(0).Item("Interviewtime"))
                txtNewInterviewType.Text = CStr(dsList.Tables(0).Rows(0).Item("InterviewType"))
                txtNewLocation.Text = CStr(dsList.Tables(0).Rows(0).Item("location"))
                txtNewVacancy.Text = CStr(dsList.Tables(0).Rows(0).Item("job"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatchName_SelectedIndexChanged", mstrModuleName)
        Finally
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Sub
#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbChangeBatchInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbChangeBatchInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
			Me.txtVacancy.Text = Language._Object.getCaption(Me.txtVacancy.Name, Me.txtVacancy.Text)
			Me.lblOldInterviewType.Text = Language._Object.getCaption(Me.lblOldInterviewType.Name, Me.lblOldInterviewType.Text)
			Me.txtInterviewType.Text = Language._Object.getCaption(Me.txtInterviewType.Name, Me.txtInterviewType.Text)
			Me.lblInterviewDate.Text = Language._Object.getCaption(Me.lblInterviewDate.Name, Me.lblInterviewDate.Text)
			Me.txtInterviewDate.Text = Language._Object.getCaption(Me.txtInterviewDate.Name, Me.txtInterviewDate.Text)
			Me.lblLocation.Text = Language._Object.getCaption(Me.lblLocation.Name, Me.lblLocation.Text)
			Me.txtLocation.Text = Language._Object.getCaption(Me.txtLocation.Name, Me.txtLocation.Text)
			Me.lblApplicantName.Text = Language._Object.getCaption(Me.lblApplicantName.Name, Me.lblApplicantName.Text)
			Me.lblNewBatch.Text = Language._Object.getCaption(Me.lblNewBatch.Name, Me.lblNewBatch.Text)
			Me.gbChangeBatchInfo.Text = Language._Object.getCaption(Me.gbChangeBatchInfo.Name, Me.gbChangeBatchInfo.Text)
			Me.txtApplicantName.Text = Language._Object.getCaption(Me.txtApplicantName.Name, Me.txtApplicantName.Text)
			Me.lblNewVacancy.Text = Language._Object.getCaption(Me.lblNewVacancy.Name, Me.lblNewVacancy.Text)
			Me.txtNewInterviewType.Text = Language._Object.getCaption(Me.txtNewInterviewType.Name, Me.txtNewInterviewType.Text)
			Me.txtNewVacancy.Text = Language._Object.getCaption(Me.txtNewVacancy.Name, Me.txtNewVacancy.Text)
			Me.lblNewInterviewType.Text = Language._Object.getCaption(Me.lblNewInterviewType.Name, Me.lblNewInterviewType.Text)
			Me.lblNewLocation.Text = Language._Object.getCaption(Me.lblNewLocation.Name, Me.lblNewLocation.Text)
			Me.txtNewInterviewDate.Text = Language._Object.getCaption(Me.txtNewInterviewDate.Name, Me.txtNewInterviewDate.Text)
			Me.lblNewInterviewDate.Text = Language._Object.getCaption(Me.lblNewInterviewDate.Name, Me.lblNewInterviewDate.Text)
			Me.txtNewLocation.Text = Language._Object.getCaption(Me.txtNewLocation.Name, Me.txtNewLocation.Text)
			Me.elNewBatchInfo.Text = Language._Object.getCaption(Me.elNewBatchInfo.Name, Me.elNewBatchInfo.Text)
			Me.elineOldBatchInfo.Text = Language._Object.getCaption(Me.elineOldBatchInfo.Name, Me.elineOldBatchInfo.Text)
			Me.txtOldBatchName.Text = Language._Object.getCaption(Me.txtOldBatchName.Name, Me.txtOldBatchName.Text)
			Me.lblOldBatchName.Text = Language._Object.getCaption(Me.lblOldBatchName.Name, Me.lblOldBatchName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Batch. Batch is the mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Batch changed successfully!")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class