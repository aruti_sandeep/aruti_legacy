﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.IO.Packaging


Public Class frmVacancyList

    Private ReadOnly mstrModuleName As String = "frmVacancyList"
    Private mintVacancyunkid As Integer
    Private objVacancyData As clsVacancy

#Region " Private Methods "
    'Sohail (11 Sep 2010) -- Start
    Private Sub SetColor()
        Try
            cboStation.BackColor = GUI.ColorOptional
            cboJobDepartment.BackColor = GUI.ColorOptional
            cboSections.BackColor = GUI.ColorOptional
            cboUnits.BackColor = GUI.ColorOptional
            cboJobGroup.BackColor = GUI.ColorOptional
            cboJob.BackColor = GUI.ColorOptional
            cboEmployeementType.BackColor = GUI.ColorOptional
            cboPayType.BackColor = GUI.ColorOptional
            txtNoofPosition.BackColor = GUI.ColorOptional
            'Hemant (30 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : On applicant master, they want to have a smart search on the vacancy drop down. Currently its taking a lot of time to scroll through all vacancies.
            cboVacancy.BackColor = GUI.ColorOptional
            cboVacancyType.BackColor = GUI.ColorOptional
            'Hemant (30 Mar 2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objStation As New clsStation
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objCommon As New clsCommon_Master
        Dim dsCombo As DataSet
        Try
            dsCombo = objStation.getComboList("Station", True)
            With cboStation
                .ValueMember = "stationunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Station")
                .SelectedValue = 0
            End With

            dsCombo = objDepartment.getComboList("Department", True)
            With cboJobDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Department")
                .SelectedValue = 0
            End With

            dsCombo = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Section")
                .SelectedValue = 0
            End With

            dsCombo = objUnit.getComboList("Unit", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Unit")
                .SelectedValue = 0
            End With

            dsCombo = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("JobGrp")
                .SelectedValue = 0
            End With

            dsCombo = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Job")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmplType")
            With cboEmployeementType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("EmplType")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
            With cboPayType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("PayType")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsCombo = objVacancyData.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-948
            With cboViewType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Show All"))
                .Items.Add(Language.getMessage(mstrModuleName, 101, "Approved"))
                .Items.Add(Language.getMessage(mstrModuleName, 102, "Pending"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP |0-JUN-2023| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objStation = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objCommon = Nothing
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    Private Sub FillList()
        Dim strSearching As String = "" 'Sohail (11 Sep 2010)
        Dim dsVacancy As New DataSet
        Dim dtTable As DataTable 'Sohail (11 Sep 2010)

        Try

            If User._Object.Privilege._AllowToViewVacancyMasterList = True Then                'Pinkal (02-Jul-2012) -- Start

                objVacancyData = New clsVacancy
                dsVacancy = objVacancyData.GetList("List", True)

                'Sohail (11 Sep 2010) -- Start
                If CInt(cboStation.SelectedValue) > 0 Then
                    strSearching &= "AND stationunkid = " & CInt(cboStation.SelectedValue) & " "
                End If
                If CInt(cboJobDepartment.SelectedValue) > 0 Then
                    strSearching &= "AND departmentunkid = " & CInt(cboJobDepartment.SelectedValue) & " "
                End If
                If CInt(cboSections.SelectedValue) > 0 Then
                    strSearching &= "AND sectionunkid = " & CInt(cboSections.SelectedValue) & " "
                End If
                If CInt(cboUnits.SelectedValue) > 0 Then
                    strSearching &= "AND unitunkid = " & CInt(cboUnits.SelectedValue) & " "
                End If
                If CInt(cboJobGroup.SelectedValue) > 0 Then
                    strSearching &= "AND jobgroupunkid = " & CInt(cboJobGroup.SelectedValue) & " "
                End If
                If CInt(cboJob.SelectedValue) > 0 Then
                    strSearching &= "AND jobunkid = " & CInt(cboJob.SelectedValue) & " "
                End If
                If CInt(cboEmployeementType.SelectedValue) > 0 Then
                    strSearching &= "AND employeementtypeunkid = " & CInt(cboEmployeementType.SelectedValue) & " "
                End If
                If CInt(cboPayType.SelectedValue) > 0 Then
                    strSearching &= "AND paytypeunkid = " & CInt(cboPayType.SelectedValue) & " "
                End If
                If txtNoofPosition.Decimal <> 0 Then
                    strSearching &= "AND noofposition = " & txtNoofPosition.Decimal & " "
                End If
                If dtpStartDate.Checked = True Then
                    strSearching &= "AND openingdate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "' "
                End If
                If dtpEndDate.Checked = True Then
                    strSearching &= "AND closingdate <= '" & eZeeDate.convertDate(dtpEndDate.Value) & "' "
                End If

                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES

                If CInt(cboVacancyType.SelectedValue) > 0 Then
                    Select Case CInt(cboVacancyType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            'Sohail (13 Mar 2020) -- Start
                            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously. (changes in desktop)
                            'strSearching &= "AND isexternalvacancy = 1 "
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND (isexternalvacancy = 1 OR ISNULL(isbothintext, 0) = 1) "
                            strSearching &= "AND (isexternalvacancy = 1 AND ISNULL(isbothintext, 0) = 0) "
                            'Sohail (18 Apr 2020) -- End
                            'Sohail (13 Mar 2020) -- End
                        Case enVacancyType.INTERNAL_VACANCY
                            'Sohail (13 Mar 2020) -- Start
                            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously. (changes in desktop)
                            'strSearching &= "AND isexternalvacancy = 0 "
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND (isexternalvacancy = 0 OR ISNULL(isbothintext, 0) = 1) "
                            strSearching &= "AND (isexternalvacancy = 0 AND ISNULL(isbothintext, 0) = 0) "
                            'Sohail (18 Apr 2020) -- End
                            'Sohail (13 Mar 2020) -- End
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            strSearching &= "AND ISNULL(isbothintext, 0) = 1 "
                            'Sohail (18 Apr 2020) -- End
                    End Select
                End If
                'S.SANDEEP [ 25 DEC 2011 ] -- END

                'Hemant (30 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : On applicant master, they want to have a smart search on the vacancy drop down. Currently its taking a lot of time to scroll through all vacancies.
                If CInt(cboVacancy.SelectedValue) > 0 Then
                    strSearching &= "AND vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " "
                End If
                'Hemant (30 Mar 2022) -- End

                'S.SANDEEP |05-JUN-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-948
                Select Case CInt(cboViewType.SelectedIndex)
                    Case 1 ' APPROVED
                        strSearching &= "AND isapproved = 1 "
                    Case 2 ' PENDING
                        strSearching &= "AND isapproved = 0 "
                End Select
                'S.SANDEEP |0-JUN-2023| -- END

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtTable = New DataView(dsVacancy.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsVacancy.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
                End If
                'Sohail (11 Sep 2010) -- End

                Dim lvItem As ListViewItem

                lvVacancyList.Items.Clear()

                'For Each dtRow As DataRow In dsVacancy.Tables(0).Rows
                For Each dtRow As DataRow In dtTable.Rows 'Sohail (11 Sep 2010)

                    lvItem = New ListViewItem
                    lvItem.Text = dtRow.Item("vacancytitle").ToString
                    'Sohail (11 Feb 2022) -- Start
                    'Enhancement : OLD-564  : NMB - NMB - Vacancy List screen Enhancement - Include a column having the staff requisition number next to the vacancy - Include class and class group columns on the vacancy master screen.
                    lvItem.SubItems.Add(dtRow.Item("formno").ToString)
                    lvItem.SubItems.Add(dtRow.Item("classesname").ToString)
                    lvItem.SubItems.Add(dtRow.Item("classgroupname").ToString)
                    'Sohail ((11 Feb 2022) -- End
                    lvItem.SubItems.Add(dtRow.Item("departmentname").ToString)
                    'Hemant (01 Nov 2021) -- Start
                    'ENHANCEMENT : OLD-510 - Upon final approval of staff requisition,Display Job Title on Vacancy List so that user can easily identify the vacancy title. Currently the system is only showing the number of position(s).
                    lvItem.SubItems.Add(dtRow.Item("jobname").ToString)
                    'Hemant (01 Nov 2021) -- End
                    lvItem.SubItems.Add(dtRow.Item("employeementtypename").ToString)
                    lvItem.SubItems.Add(dtRow.Item("paytype").ToString)
                    lvItem.SubItems.Add(dtRow.Item("noofposition").ToString)
                    'S.SANDEEP [ 25 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("interview_startdate").ToString).ToShortDateString)
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("interview_closedate").ToString).ToShortDateString)
                    'Sohail (28 May 2014) -- Start
                    'Enhancement - Staff Requisition.
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("openingdate").ToString).ToShortDateString)
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("closingdate").ToString).ToShortDateString)
                    If IsDBNull(dtRow.Item("openingdate")) = True Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("openingdate").ToString).ToShortDateString)
                    End If
                    If IsDBNull(dtRow.Item("closingdate")) = True Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("closingdate").ToString).ToShortDateString)
                    End If

                    'Pinkal (14-Apr-2023) -- Start
                    '(A1X-817) NMB - As a user, I want to be able to download attachments added from staff requisition on the vacancy master screen.
                    lvItem.SubItems.Add(dtRow.Item("staffrequisitiontranunkid").ToString)
                    'Pinkal (14-Apr-2023) -- End

                    'S.SANDEEP |05-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-948
                    lvItem.SubItems.Add(dtRow.Item("aprstatus").ToString())
                    'S.SANDEEP |0-JUN-2023| -- END

                    'Sohail (28 May 2014) -- End
                    'S.SANDEEP [ 25 DEC 2011 ] -- END
                    lvItem.Tag = dtRow.Item("vacancyunkid")

                    'S.SANDEEP [ 25 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES

                    If CBool(dtRow.Item("isexternalvacancy")) = True Then
                        lvItem.ForeColor = objpnlExVacancyColor.BackColor
                    End If

                    'S.SANDEEP [ 25 DEC 2011 ] -- END

                    'S.SANDEEP |05-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-948
                    If CBool(dtRow.Item("isapproved")) = False Then
                        lvItem.ForeColor = Color.Red
                    End If
                    'S.SANDEEP |0-JUN-2023| -- END

                    lvVacancyList.Items.Add(lvItem)

                Next

                If lvVacancyList.Items.Count > 11 Then
                    colhVacancy.Width = 170 - 18
                Else
                    colhVacancy.Width = 170
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisiblity()
        Try
            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            'btnNew.Enabled = User._Object.Privilege._AllowtoAddVacancy
            btnNew.Enabled = User._Object.Privilege._AllowtoAddVacancy AndAlso Not (ConfigParameter._Object._ApplyStaffRequisition)
            'Sohail (28 May 2014) -- End
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditVacancy
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteVacancy
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisiblity", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    'Pinkal (14-Apr-2023) -- Start
    '(A1X-817) NMB - As a user, I want to be able to download attachments added from staff requisition on the vacancy master screen.

    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try
            Using zip As Package = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate)
                For Each item As String In fileToAdd
                    Try
                        Dim destFilename As String = ".\" & Path.GetFileName(item)
                        Dim uri As Uri = PackUriHelper.CreatePartUri(New Uri(destFilename, UriKind.Relative))
                        If zip.PartExists(uri) Then
                            zip.DeletePart(uri)
                        End If
                        Dim part As PackagePart = zip.CreatePart(uri, "", compression)
                        Using fileStream As New FileStream(item, FileMode.Open, FileAccess.Read)
                            Using dest As Stream = part.GetStream()
                                CopyStream(CType(fileStream, Stream), part.GetStream())
                            End Using
                        End Using
                    Catch ex As System.ArgumentException
                        Continue For
                    Catch ex As System.UriFormatException
                        Continue For
                    End Try
                Next
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddFileToZip", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
    End Sub

    'Pinkal (14-Apr-2023) -- End

#End Region

#Region " Form's Events "
    'Sohail (11 Sep 2010) -- Start
    Private Sub frmVacancyList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                SendKeys.Send("{TAB}")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyList_KeyDown", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    Private Sub frmVacancyList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        'If e.KeyCode = Keys.Delete Then
        If lvVacancyList.Focused AndAlso e.KeyCode = Keys.Delete Then 'Sohail (11 Sep 2010)
            btnDelete.PerformClick()
        End If
    End Sub
    Private Sub frmVacancyList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmVacancyList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objVacancyData = Nothing
    End Sub

    Private Sub frmVacancyList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objVacancyData = New clsVacancy
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            OtherSettings()  'Hemant (15 Nov 2019)
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Sohail (11 Sep 2010) -- Start
            Call SetColor()
            Call FillCombo()
            'Sohail (11 Sep 2010) -- End

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement REQ # : Before publishing a vacancy to web / exporting to web, provide option to preview the vacancy (Preview it to get a feel of how the applicant will see it) – TC005	
            lnkPreviewExternal.Visible = False
            lnkPreviewInternal.Visible = False
            lnkPreviewExternal.Links.Clear()
            lnkPreviewInternal.Links.Clear()
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : It should be possible to preview a vacancy before publishing it.
            'If ConfigParameter._Object._WebURL.Trim.Length > 0 Then
            '   lnkPreviewExternal.Links.Add(0, lnkPreviewExternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._WebURL, "/login.aspx", "/ALogin.aspx", , , CompareMethod.Text))
            If ConfigParameter._Object._RecruitmentWebServiceLink.Trim.Length > 0 Then
                lnkPreviewExternal.Links.Add(0, lnkPreviewExternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._ArutiSelfServiceURL, "/index.aspx", "", , , CompareMethod.Text) & "/Recruitment/Career.aspx?code=" & Company._Object._Code & "&Ext=7")
                'Sohail (13 Mar 2020) -- End
                lnkPreviewExternal.Visible = True
            End If
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            'If ConfigParameter._Object._WebURLInternal.Trim.Length > 0 Then
            '    lnkPreviewInternal.Links.Add(0, lnkPreviewInternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._WebURLInternal, "/login.aspx", "/ALogin.aspx", , , CompareMethod.Text))
            If ConfigParameter._Object._RecruitmentWebServiceLink.Trim.Length > 0 Then
                lnkPreviewInternal.Links.Add(0, lnkPreviewExternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._ArutiSelfServiceURL, "/index.aspx", "", , , CompareMethod.Text) & "/Recruitment/Career.aspx?code=" & Company._Object._Code & "&Ext=9")
                'Sohail (13 Mar 2020) -- End
                lnkPreviewInternal.Visible = True
            End If
            'Sohail (17 Sep 2019) -- End

            'Call OtherSettings()
            'Call FillCombo()
            Call FillList()

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'chkIsExternalVacancy.Checked = False
            'objpnlExVacancyColor.Visible = False
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            If lvVacancyList.Items.Count > 0 Then lvVacancyList.Items(0).Selected = True
            lvVacancyList.Select()



            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisiblity()
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (08 Mar 2022) -- Start
            'Enhancement :  : NMB - Pick allocation name for class, class group and department columns from common master on vacancy list screen.
            colhClass.Text = Language.getMessage("clsMasterData", 419, "Classes")
            colhClassGroup.Text = Language.getMessage("clsMasterData", 420, "Class Group")
            colhDepartment.Text = Language.getMessage("clsMasterData", 428, "Department")
            'Sohail (08 Mar 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsVacancy.SetMessages()
            objfrm._Other_ModuleNames = "clsVacancy"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjfrmVacancy_LoadAddEdit As New frmVacancy_AddEdit
            'Sohail (11 Sep 2010) -- Start
            'ObjfrmVacancy_LoadAddEdit.displayDialog(-1, enAction.ADD_ONE)
            If ObjfrmVacancy_LoadAddEdit.displayDialog(-1, enAction.ADD_ONE) = True Then
                Call FillList()
            End If
            'Sohail (11 Sep 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvVacancyList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Vacancy from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvVacancyList.Select()
            Exit Sub
        End If

        Dim frm As New frmVacancy_AddEdit

        Try

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvVacancyList.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvVacancyList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            'lvVacancyList.Items(intSelectedIndex).Selected = True
            'lvVacancyList.EnsureVisible(intSelectedIndex)
            'lvVacancyList.Select()



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancy_AddEdit", mstrModuleName)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvVacancyList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Vacancy from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvVacancyList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvVacancyList.SelectedItems(0).Index
            objVacancyData._Vacancyunkid = CInt(lvVacancyList.SelectedItems(0).Tag)
            'Sohail (15 Jul 2014) -- Start
            'Enhancement - Consider Max/Min Level as per Last Approver Mapped and NoOfPosition on Staff Requisition.
            If objVacancyData._Staffrequisitiontranunkid > 0 Then
                Dim objStaffReq As New clsStaffrequisition_Tran
                objStaffReq._Staffrequisitiontranunkid = objVacancyData._Staffrequisitiontranunkid
                If objStaffReq._Isvoid = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You can not delete this Vacancy. Reason : This Vacancy is generated from Staff Requision Form No.") & " " & objStaffReq._Formno, enMsgBoxStyle.Information)
                    Exit Try
                End If
            End If
            'Sohail (15 Jul 2014) -- End
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Vacancy?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                objVacancyData._Voiduserunkid = User._Object._Userunkid
                objVacancyData._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.VACANCY, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objVacancyData._Voidreason = mstrVoidReason
                End If
                frm = Nothing



                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objVacancyData._FormName = mstrModuleName
                objVacancyData._LoginEmployeeunkid = 0
                objVacancyData._ClientIP = getIP()
                objVacancyData._HostName = getHostName()
                objVacancyData._FromWeb = False
                objVacancyData._AuditUserId = User._Object._Userunkid
objVacancyData._CompanyUnkid = Company._Object._Companyunkid
                objVacancyData._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objVacancyData.Delete(CInt(lvVacancyList.SelectedItems(0).Tag))
                'Sandeep [ 09 Oct 2010 ] -- Start
                'lvVacancyList.SelectedItems(0).Remove()
                If objVacancyData._Message <> "" Then
                    eZeeMsgBox.Show(objVacancyData._Message, enMsgBoxStyle.Information)
                Else
                    lvVacancyList.SelectedItems(0).Remove()
                End If
                'Sandeep [ 09 Oct 2010 ] -- End 

                If lvVacancyList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvVacancyList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvVacancyList.Items.Count - 1
                    lvVacancyList.Items(intSelectedIndex).Selected = True
                    lvVacancyList.EnsureVisible(intSelectedIndex)
                ElseIf lvVacancyList.Items.Count <> 0 Then
                    lvVacancyList.Items(intSelectedIndex).Selected = True
                    lvVacancyList.EnsureVisible(intSelectedIndex)
                End If
            End If

            lvVacancyList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub
#End Region

    'Sohail (11 Sep 2010) -- Start
#Region " Other's Controls Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboStation.SelectedValue = 0
            cboJobDepartment.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboUnits.SelectedValue = 0
            cboJobGroup.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboEmployeementType.SelectedValue = 0
            cboPayType.SelectedValue = 0
            txtNoofPosition.Decimal = 0
            dtpStartDate.Value = DateTime.Today.Date
            dtpStartDate.Checked = False
            dtpEndDate.Value = DateTime.Today.Date
            dtpEndDate.Checked = False
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboVacancyType.SelectedIndex = 0
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement REQ # : Before publishing a vacancy to web / exporting to web, provide option to preview the vacancy (Preview it to get a feel of how the applicant will see it) – TC005	
    Private Sub lnkPreviewExternal_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkPreviewExternal.LinkClicked, lnkPreviewInternal.LinkClicked
        Try
            System.Diagnostics.Process.Start(e.Link.LinkData.ToString())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, LinkLabel).Name & "_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Sep 2019) -- End

#End Region
    'Sohail (11 Sep 2010) -- End


    Private Sub dtpStartDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged

        Try
            If dtpStartDate.Checked = True Then
                dtpEndDate.Checked = True
                dtpEndDate.Value = dtpStartDate.Value
            Else
                dtpEndDate.Checked = False
                dtpEndDate.Value = dtpStartDate.Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub



    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub chkIsExternalVacancy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If chkIsExternalVacancy.CheckState = CheckState.Checked Then
    '            objpnlExVacancyColor.Visible = True
    '        Else
    '            objpnlExVacancyColor.Visible = False
    '        End If
    '        'Call FillList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkIsExternalVacancy_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#Region " Combobox Events "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Select Case CInt(cboVacancyType.SelectedValue)
                Case enVacancyType.EXTERNAL_VACANCY
                    objpnlExVacancyColor.Visible = True
                Case enVacancyType.INTERNAL_VACANCY
                    objpnlExVacancyColor.Visible = False
                Case Else
                    objpnlExVacancyColor.Visible = True

            End Select

            'Hemant (30 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : On applicant master, they want to have a smart search on the vacancy drop down. Currently its taking a lot of time to scroll through all vacancies.
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                Dim dsVacList As New DataSet
                Dim objVac As New clsVacancy
                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
                With cboVacancy
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsVacList.Tables("List")
                    .SelectedValue = 0
                End With
            End If
            'Hemant (30 Mar 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (11 Feb 2022) -- Start
    'Enhancement : OLD-564  : NMB - NMB - Vacancy List screen Enhancement - Make the drop down menus on the vacancy master screen as smart searches.
    Private Sub cboJob_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboJob.KeyPress, cboStation.KeyPress, cboJobDepartment.KeyPress, cboSections.KeyPress, cboUnits.KeyPress, cboJobGroup.KeyPress, cboEmployeementType.KeyPress, cboPayType.KeyPress, cboVacancyType.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = ""
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail ((11 Feb 2022) -- End

    'Hemant (30 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : On applicant master, they want to have a smart search on the vacancy drop down. Currently its taking a lot of time to scroll through all vacancies.
    Private Sub cboVacancy_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboVacancy.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = ""

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                    cbo.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Hemant (30 Mar 2022) -- End

#End Region

    'Pinkal (14-Apr-2023) -- Start
    '(A1X-817) NMB - As a user, I want to be able to download attachments added from staff requisition on the vacancy master screen.

#Region "ListView Events"

    Private Sub lvVacancyList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvVacancyList.SelectedIndexChanged
        Try
            If lvVacancyList.SelectedItems.Count > 0 AndAlso lvVacancyList.SelectedItems(0).SubItems(colhFormno.Index).Text.Trim().Length > 0 Then
                mnuSRDownloadAttachment.Enabled = True
                mnuSRPreviewAttachment.Enabled = True
            Else
                mnuSRDownloadAttachment.Enabled = False
                mnuSRPreviewAttachment.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvVacancyList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Menu's Events"

    Private Sub mnuSRDownloadAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSRDownloadAttachment.Click
        Dim objDocument As clsScan_Attach_Documents
        Try
            If lvVacancyList.SelectedItems.Count > 0 AndAlso lvVacancyList.SelectedItems(0).SubItems(colhFormno.Index).Text.Trim().Length > 0 AndAlso CInt(lvVacancyList.SelectedItems(0).SubItems(objcolhStaffRequistionId.Index).Text) > 0 Then

                objDocument = New clsScan_Attach_Documents
                Dim mblnIsSelfServiceInstall As Boolean = IsSelfServiceExist()

                Dim mstrFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", CInt(enScanAttactRefId.STAFF_REQUISITION)).Tables(0).Rows(0)("Name").ToString()

                Dim mstrSearch As String = "scanattachrefid = " & enScanAttactRefId.STAFF_REQUISITION & " AND hrdocuments_tran.transactionunkid IN (" & lvVacancyList.SelectedItems(0).SubItems(objcolhStaffRequistionId.Index).Text & ")"

                Dim dsList As DataSet = objDocument.GetList(ConfigParameter._Object._Document_Path, "List", "", -1, False, Nothing, Nothing, mstrSearch, False, -1, 0, 0, True)
                Dim mdtTran As DataTable = dsList.Tables(0).Copy()


                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no attached document for this staff requisition."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim strError As String = ""
                Dim strLocalPath As String = String.Empty
                Dim fileToAdd As New List(Of String)
                For Each xRow As DataRow In mdtTran.Rows
                    If IsDBNull(xRow("file_data")) = False Then
                        strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                        Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                        Dim fs As New FileStream(strLocalPath, FileMode.Create)
                        ms.WriteTo(fs)
                        ms.Close()
                        fs.Close()
                        fs.Dispose()
                        If strLocalPath <> "" Then
                            fileToAdd.Add(strLocalPath)
                        End If
                    ElseIf IsDBNull(xRow("file_data")) = True Then
                        If IsSelfServiceExist() Then
                            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)
                            If imagebyte IsNot Nothing Then
                                strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                                Dim ms As New MemoryStream(imagebyte)
                                Dim fs As New FileStream(strLocalPath, FileMode.Create)
                                ms.WriteTo(fs)
                                ms.Close()
                                fs.Close()
                                fs.Dispose()
                            End If
                            fileToAdd.Add(strLocalPath)
                        Else
                            strLocalPath = xRow("filepath").ToString()
                            If IO.File.Exists(strLocalPath) = True Then
                                fileToAdd.Add(strLocalPath)
                            End If
                        End If
                    End If
                Next

                If fileToAdd.Count > 0 Then
                    Dim fbd As New FolderBrowserDialog()
                    fbd.ShowNewFolderButton = True
                    If (fbd.ShowDialog() = DialogResult.OK) Then
                        Dim fl As New System.IO.FileInfo(fbd.SelectedPath)
                        For Each item As String In fileToAdd
                            If item.ToString.Trim.Length <= 0 Then Continue For
                            Dim destFilename As String = ".\" & Path.GetFileName(item)
                            System.IO.File.Copy(item.ToString, fl.FullName.ToString & "\" & Path.GetFileName(item))
                        Next
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                    End If

                    For Each fl As String In fileToAdd
                        Try
                            System.IO.File.Delete(fl)
                        Catch ex As Exception
                        End Try
                    Next
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "There is no attached document for this staff requisition."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If  ' If lvVacancyList.SelectedItems.Count > 0 AndAlso lvVacancyList.SelectedItems(0).SubItems(colhFormno.Index).Text.Trim().Length > 0 AndAlso CInt(lvVacancyList.SelectedItems(0).SubItems(objcolhStaffRequistionId.Index).Text) > 0 Then

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSRDownloadAttachment_Click", mstrModuleName)
        Finally
            objDocument = Nothing
        End Try
    End Sub

    Private Sub mnuSRPreviewAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSRPreviewAttachment.Click
        Dim objDocument As New clsScan_Attach_Documents
        Try
            Dim strIds As String = ""
            Dim mstrSearch As String = "scanattachrefid = " & enScanAttactRefId.STAFF_REQUISITION & " AND hrdocuments_tran.transactionunkid IN (" & lvVacancyList.SelectedItems(0).SubItems(objcolhStaffRequistionId.Index).Text & ")"
            Dim dsList As DataSet = objDocument.GetList(ConfigParameter._Object._Document_Path, "List", "", -1, False, Nothing, Nothing, mstrSearch, False, -1, 0, 0, True)
            strIds = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString()).ToArray())

            Dim frm As New frmPreviewDocuments
            frm.displayDialog(strIds, False)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSRPreviewAttachment_Click", mstrModuleName)
        Finally
            objDocument = Nothing
        End Try
    End Sub

#End Region

    'Pinkal (14-Apr-2023) -- End

    
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhVacancy.Text = Language._Object.getCaption(CStr(Me.colhVacancy.Tag), Me.colhVacancy.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhEmployeementType.Text = Language._Object.getCaption(CStr(Me.colhEmployeementType.Tag), Me.colhEmployeementType.Text)
            Me.colhPayType.Text = Language._Object.getCaption(CStr(Me.colhPayType.Tag), Me.colhPayType.Text)
            Me.colhNoofPosition.Text = Language._Object.getCaption(CStr(Me.colhNoofPosition.Tag), Me.colhNoofPosition.Text)
            Me.colhVacancyDateFrom.Text = Language._Object.getCaption(CStr(Me.colhVacancyDateFrom.Tag), Me.colhVacancyDateFrom.Text)
            Me.colhVacancyEnd.Text = Language._Object.getCaption(CStr(Me.colhVacancyEnd.Tag), Me.colhVacancyEnd.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)
            Me.mnuPrintCourse.Text = Language._Object.getCaption(Me.mnuPrintCourse.Name, Me.mnuPrintCourse.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblJobDept.Text = Language._Object.getCaption(Me.lblJobDept.Name, Me.lblJobDept.Text)
            Me.lblEmployeementType.Text = Language._Object.getCaption(Me.lblEmployeementType.Name, Me.lblEmployeementType.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblVacancyEndDate.Text = Language._Object.getCaption(Me.lblVacancyEndDate.Name, Me.lblVacancyEndDate.Text)
            Me.lblVacancyStartDate.Text = Language._Object.getCaption(Me.lblVacancyStartDate.Name, Me.lblVacancyStartDate.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.Name, Me.lblPayType.Text)
            Me.lblNoofPosition.Text = Language._Object.getCaption(Me.lblNoofPosition.Name, Me.lblNoofPosition.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lnkPreviewInternal.Text = Language._Object.getCaption(Me.lnkPreviewInternal.Name, Me.lnkPreviewInternal.Text)
			Me.lnkPreviewExternal.Text = Language._Object.getCaption(Me.lnkPreviewExternal.Name, Me.lnkPreviewExternal.Text)
			Me.lblExternalVacancies.Text = Language._Object.getCaption(Me.lblExternalVacancies.Name, Me.lblExternalVacancies.Text)
			Me.colhJobTitle.Text = Language._Object.getCaption(CStr(Me.colhJobTitle.Tag), Me.colhJobTitle.Text)
			Me.colhFormno.Text = Language._Object.getCaption(CStr(Me.colhFormno.Tag), Me.colhFormno.Text)
			Me.colhClass.Text = Language._Object.getCaption(CStr(Me.colhClass.Tag), Me.colhClass.Text)
			Me.colhClassGroup.Text = Language._Object.getCaption(CStr(Me.colhClassGroup.Tag), Me.colhClassGroup.Text)
            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuSRDownloadAttachment.Text = Language._Object.getCaption(Me.mnuSRDownloadAttachment.Name, Me.mnuSRDownloadAttachment.Text)
            Me.mnuSRPreviewAttachment.Text = Language._Object.getCaption(Me.mnuSRPreviewAttachment.Name, Me.mnuSRPreviewAttachment.Text)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage(mstrModuleName, 1, "Please select Vacancy from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Vacancy?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You can not delete this Vacancy. Reason : This Vacancy is generated from Staff Requision Form No.")
            Language.setMessage(mstrModuleName, 4, "File(s) downloaded successfully to the selected location.")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class