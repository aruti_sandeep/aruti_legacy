﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApproveDisapproveStaffRequisition
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApproveDisapproveStaffRequisition))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.lblJobHeadCountInfo = New System.Windows.Forms.Label
        Me.objlblVariation = New System.Windows.Forms.Label
        Me.objlblAvailable = New System.Windows.Forms.Label
        Me.lblAvailable = New System.Windows.Forms.Label
        Me.lblVariation = New System.Windows.Forms.Label
        Me.objlblPlanned = New System.Windows.Forms.Label
        Me.lblPlanned = New System.Windows.Forms.Label
        Me.gbStaffReqApprovalnfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAttachment = New System.Windows.Forms.Label
        Me.dgvStaffRequisitionAttachment = New System.Windows.Forms.DataGridView
        Me.colhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhfile_path = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhlocalpath = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhfileuniquename = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblApprovalStatus = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.objtlpJob = New System.Windows.Forms.TableLayoutPanel
        Me.lblApproverInfo = New System.Windows.Forms.Label
        Me.objLevelPriorityVal = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.lblLevelPriority = New System.Windows.Forms.Label
        Me.objApproverName = New System.Windows.Forms.Label
        Me.objApproverLevelVal = New System.Windows.Forms.Label
        Me.lblApproverLevel = New System.Windows.Forms.Label
        Me.gbStaffRequisitionList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkViewJobDescription = New System.Windows.Forms.LinkLabel
        Me.txtJobDesc = New System.Windows.Forms.RichTextBox
        Me.pnlEmployee = New System.Windows.Forms.Panel
        Me.dgvEmployeeList = New System.Windows.Forms.DataGridView
        Me.objcolhCheckAll = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objcolhEmpID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSalaryBand = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhActionReasonId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAUD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtNoofPosition = New System.Windows.Forms.TextBox
        Me.lblNoofPosition = New System.Windows.Forms.Label
        Me.txtWorkStartDate = New System.Windows.Forms.TextBox
        Me.txtJob = New System.Windows.Forms.TextBox
        Me.txtAddStaffReason = New System.Windows.Forms.TextBox
        Me.txtLeavingReason = New System.Windows.Forms.TextBox
        Me.txtClass = New System.Windows.Forms.TextBox
        Me.txtClassGroup = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtAllocation = New System.Windows.Forms.TextBox
        Me.txtStatus = New System.Windows.Forms.TextBox
        Me.txtFormNo = New System.Windows.Forms.TextBox
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.lblAddStaffReason = New System.Windows.Forms.Label
        Me.lblLeavingReason = New System.Windows.Forms.Label
        Me.lblWorkStartDate = New System.Windows.Forms.Label
        Me.lblClass = New System.Windows.Forms.Label
        Me.lblJobDesc = New System.Windows.Forms.Label
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.sfdAttachment = New System.Windows.Forms.SaveFileDialog
        Me.lnkViewStaffRequisitionFormReport = New System.Windows.Forms.LinkLabel
        Me.pnlMainInfo.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gbStaffReqApprovalnfo.SuspendLayout()
        CType(Me.dgvStaffRequisitionAttachment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objtlpJob.SuspendLayout()
        Me.gbStaffRequisitionList.SuspendLayout()
        Me.pnlEmployee.SuspendLayout()
        CType(Me.dgvEmployeeList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.TableLayoutPanel1)
        Me.pnlMainInfo.Controls.Add(Me.gbStaffReqApprovalnfo)
        Me.pnlMainInfo.Controls.Add(Me.gbStaffRequisitionList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(904, 646)
        Me.pnlMainInfo.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblJobHeadCountInfo, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.objlblVariation, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.objlblAvailable, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lblAvailable, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lblVariation, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.objlblPlanned, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblPlanned, 0, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(524, 116)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(360, 85)
        Me.TableLayoutPanel1.TabIndex = 250
        '
        'lblJobHeadCountInfo
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.lblJobHeadCountInfo, 2)
        Me.lblJobHeadCountInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblJobHeadCountInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobHeadCountInfo.Location = New System.Drawing.Point(4, 1)
        Me.lblJobHeadCountInfo.Name = "lblJobHeadCountInfo"
        Me.lblJobHeadCountInfo.Size = New System.Drawing.Size(352, 20)
        Me.lblJobHeadCountInfo.TabIndex = 40
        Me.lblJobHeadCountInfo.Text = "Job Head Count Information"
        Me.lblJobHeadCountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objlblVariation
        '
        Me.objlblVariation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblVariation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblVariation.Location = New System.Drawing.Point(219, 64)
        Me.objlblVariation.Name = "objlblVariation"
        Me.objlblVariation.Size = New System.Drawing.Size(137, 20)
        Me.objlblVariation.TabIndex = 39
        Me.objlblVariation.Text = "0"
        Me.objlblVariation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblAvailable
        '
        Me.objlblAvailable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblAvailable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAvailable.Location = New System.Drawing.Point(219, 43)
        Me.objlblAvailable.Name = "objlblAvailable"
        Me.objlblAvailable.Size = New System.Drawing.Size(137, 20)
        Me.objlblAvailable.TabIndex = 39
        Me.objlblAvailable.Text = "0"
        Me.objlblAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAvailable
        '
        Me.lblAvailable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblAvailable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAvailable.Location = New System.Drawing.Point(4, 43)
        Me.lblAvailable.Name = "lblAvailable"
        Me.lblAvailable.Size = New System.Drawing.Size(208, 20)
        Me.lblAvailable.TabIndex = 25
        Me.lblAvailable.Text = "Available Head Counts"
        Me.lblAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVariation
        '
        Me.lblVariation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblVariation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVariation.Location = New System.Drawing.Point(4, 64)
        Me.lblVariation.Name = "lblVariation"
        Me.lblVariation.Size = New System.Drawing.Size(208, 20)
        Me.lblVariation.TabIndex = 4
        Me.lblVariation.Text = "Variations"
        Me.lblVariation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblPlanned
        '
        Me.objlblPlanned.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblPlanned.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblPlanned.Location = New System.Drawing.Point(219, 22)
        Me.objlblPlanned.Name = "objlblPlanned"
        Me.objlblPlanned.Size = New System.Drawing.Size(137, 20)
        Me.objlblPlanned.TabIndex = 3
        Me.objlblPlanned.Text = "0"
        Me.objlblPlanned.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPlanned
        '
        Me.lblPlanned.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblPlanned.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlanned.Location = New System.Drawing.Point(4, 22)
        Me.lblPlanned.Name = "lblPlanned"
        Me.lblPlanned.Size = New System.Drawing.Size(208, 20)
        Me.lblPlanned.TabIndex = 2
        Me.lblPlanned.Text = "Planned Head Counts"
        Me.lblPlanned.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbStaffReqApprovalnfo
        '
        Me.gbStaffReqApprovalnfo.BorderColor = System.Drawing.Color.Black
        Me.gbStaffReqApprovalnfo.Checked = False
        Me.gbStaffReqApprovalnfo.CollapseAllExceptThis = False
        Me.gbStaffReqApprovalnfo.CollapsedHoverImage = Nothing
        Me.gbStaffReqApprovalnfo.CollapsedNormalImage = Nothing
        Me.gbStaffReqApprovalnfo.CollapsedPressedImage = Nothing
        Me.gbStaffReqApprovalnfo.CollapseOnLoad = False
        Me.gbStaffReqApprovalnfo.Controls.Add(Me.lblAttachment)
        Me.gbStaffReqApprovalnfo.Controls.Add(Me.dgvStaffRequisitionAttachment)
        Me.gbStaffReqApprovalnfo.Controls.Add(Me.cboStatus)
        Me.gbStaffReqApprovalnfo.Controls.Add(Me.lblApprovalStatus)
        Me.gbStaffReqApprovalnfo.Controls.Add(Me.txtRemarks)
        Me.gbStaffReqApprovalnfo.Controls.Add(Me.lblRemarks)
        Me.gbStaffReqApprovalnfo.Controls.Add(Me.objtlpJob)
        Me.gbStaffReqApprovalnfo.ExpandedHoverImage = Nothing
        Me.gbStaffReqApprovalnfo.ExpandedNormalImage = Nothing
        Me.gbStaffReqApprovalnfo.ExpandedPressedImage = Nothing
        Me.gbStaffReqApprovalnfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStaffReqApprovalnfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStaffReqApprovalnfo.HeaderHeight = 25
        Me.gbStaffReqApprovalnfo.HeaderMessage = ""
        Me.gbStaffReqApprovalnfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStaffReqApprovalnfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStaffReqApprovalnfo.HeightOnCollapse = 0
        Me.gbStaffReqApprovalnfo.LeftTextSpace = 0
        Me.gbStaffReqApprovalnfo.Location = New System.Drawing.Point(516, 66)
        Me.gbStaffReqApprovalnfo.Name = "gbStaffReqApprovalnfo"
        Me.gbStaffReqApprovalnfo.OpenHeight = 182
        Me.gbStaffReqApprovalnfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStaffReqApprovalnfo.ShowBorder = True
        Me.gbStaffReqApprovalnfo.ShowCheckBox = False
        Me.gbStaffReqApprovalnfo.ShowCollapseButton = False
        Me.gbStaffReqApprovalnfo.ShowDefaultBorderColor = True
        Me.gbStaffReqApprovalnfo.ShowDownButton = False
        Me.gbStaffReqApprovalnfo.ShowHeader = True
        Me.gbStaffReqApprovalnfo.Size = New System.Drawing.Size(376, 519)
        Me.gbStaffReqApprovalnfo.TabIndex = 206
        Me.gbStaffReqApprovalnfo.Temp = 0
        Me.gbStaffReqApprovalnfo.Text = "Staff Requisition Approval Information"
        Me.gbStaffReqApprovalnfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAttachment
        '
        Me.lblAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttachment.Location = New System.Drawing.Point(5, 367)
        Me.lblAttachment.Name = "lblAttachment"
        Me.lblAttachment.Size = New System.Drawing.Size(149, 16)
        Me.lblAttachment.TabIndex = 252
        Me.lblAttachment.Text = "Attached Document"
        Me.lblAttachment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvStaffRequisitionAttachment
        '
        Me.dgvStaffRequisitionAttachment.AllowUserToAddRows = False
        Me.dgvStaffRequisitionAttachment.AllowUserToDeleteRows = False
        Me.dgvStaffRequisitionAttachment.AllowUserToResizeRows = False
        Me.dgvStaffRequisitionAttachment.BackgroundColor = System.Drawing.Color.White
        Me.dgvStaffRequisitionAttachment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvStaffRequisitionAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvStaffRequisitionAttachment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhName, Me.objcolhDownload, Me.objcolhGUID, Me.objcolhScanUnkId, Me.objcolhfile_path, Me.objcolhlocalpath, Me.objcolhfileuniquename})
        Me.dgvStaffRequisitionAttachment.Location = New System.Drawing.Point(8, 388)
        Me.dgvStaffRequisitionAttachment.Name = "dgvStaffRequisitionAttachment"
        Me.dgvStaffRequisitionAttachment.RowHeadersVisible = False
        Me.dgvStaffRequisitionAttachment.Size = New System.Drawing.Size(360, 124)
        Me.dgvStaffRequisitionAttachment.TabIndex = 250
        '
        'colhName
        '
        Me.colhName.HeaderText = "File Name"
        Me.colhName.Name = "colhName"
        Me.colhName.ReadOnly = True
        Me.colhName.Width = 215
        '
        'objcolhDownload
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhDownload.DefaultCellStyle = DataGridViewCellStyle1
        Me.objcolhDownload.HeaderText = "Download"
        Me.objcolhDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhDownload.Name = "objcolhDownload"
        Me.objcolhDownload.ReadOnly = True
        Me.objcolhDownload.Text = "Download"
        Me.objcolhDownload.UseColumnTextForLinkValue = True
        Me.objcolhDownload.Width = 140
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "objcolhGUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.Visible = False
        '
        'objcolhScanUnkId
        '
        Me.objcolhScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Name = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Visible = False
        '
        'objcolhfile_path
        '
        Me.objcolhfile_path.HeaderText = "objcolhfile_path"
        Me.objcolhfile_path.Name = "objcolhfile_path"
        Me.objcolhfile_path.Visible = False
        '
        'objcolhlocalpath
        '
        Me.objcolhlocalpath.HeaderText = "objcolhlocalpath"
        Me.objcolhlocalpath.Name = "objcolhlocalpath"
        Me.objcolhlocalpath.Visible = False
        '
        'objcolhfileuniquename
        '
        Me.objcolhfileuniquename.HeaderText = "objcolhfileuniquename"
        Me.objcolhfileuniquename.Name = "objcolhfileuniquename"
        Me.objcolhfileuniquename.Visible = False
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(102, 262)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(199, 21)
        Me.cboStatus.TabIndex = 0
        '
        'lblApprovalStatus
        '
        Me.lblApprovalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalStatus.Location = New System.Drawing.Point(15, 265)
        Me.lblApprovalStatus.Name = "lblApprovalStatus"
        Me.lblApprovalStatus.Size = New System.Drawing.Size(81, 15)
        Me.lblApprovalStatus.TabIndex = 248
        Me.lblApprovalStatus.Text = "Status"
        Me.lblApprovalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.HideSelection = False
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtRemarks.Location = New System.Drawing.Point(8, 317)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(360, 44)
        Me.txtRemarks.TabIndex = 1
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(5, 295)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(149, 16)
        Me.lblRemarks.TabIndex = 208
        Me.lblRemarks.Text = "Approval Remarks"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objtlpJob
        '
        Me.objtlpJob.BackColor = System.Drawing.SystemColors.Control
        Me.objtlpJob.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objtlpJob.ColumnCount = 2
        Me.objtlpJob.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.objtlpJob.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.objtlpJob.Controls.Add(Me.lblApproverInfo, 0, 0)
        Me.objtlpJob.Controls.Add(Me.objLevelPriorityVal, 1, 3)
        Me.objtlpJob.Controls.Add(Me.lblApprover, 0, 1)
        Me.objtlpJob.Controls.Add(Me.lblLevelPriority, 0, 3)
        Me.objtlpJob.Controls.Add(Me.objApproverName, 1, 1)
        Me.objtlpJob.Controls.Add(Me.objApproverLevelVal, 1, 2)
        Me.objtlpJob.Controls.Add(Me.lblApproverLevel, 0, 2)
        Me.objtlpJob.Location = New System.Drawing.Point(8, 150)
        Me.objtlpJob.Name = "objtlpJob"
        Me.objtlpJob.RowCount = 4
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtlpJob.Size = New System.Drawing.Size(360, 105)
        Me.objtlpJob.TabIndex = 210
        '
        'lblApproverInfo
        '
        Me.objtlpJob.SetColumnSpan(Me.lblApproverInfo, 2)
        Me.lblApproverInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblApproverInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverInfo.Location = New System.Drawing.Point(4, 1)
        Me.lblApproverInfo.Name = "lblApproverInfo"
        Me.lblApproverInfo.Size = New System.Drawing.Size(352, 25)
        Me.lblApproverInfo.TabIndex = 273
        Me.lblApproverInfo.Text = "Approver Information"
        Me.lblApproverInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objLevelPriorityVal
        '
        Me.objLevelPriorityVal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objLevelPriorityVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLevelPriorityVal.Location = New System.Drawing.Point(219, 79)
        Me.objLevelPriorityVal.Name = "objLevelPriorityVal"
        Me.objLevelPriorityVal.Size = New System.Drawing.Size(137, 25)
        Me.objLevelPriorityVal.TabIndex = 272
        Me.objLevelPriorityVal.Text = "0"
        Me.objLevelPriorityVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(4, 27)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(208, 25)
        Me.lblApprover.TabIndex = 141
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLevelPriority
        '
        Me.lblLevelPriority.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblLevelPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelPriority.Location = New System.Drawing.Point(4, 79)
        Me.lblLevelPriority.Name = "lblLevelPriority"
        Me.lblLevelPriority.Size = New System.Drawing.Size(208, 25)
        Me.lblLevelPriority.TabIndex = 271
        Me.lblLevelPriority.Text = "Level Priority"
        Me.lblLevelPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objApproverName
        '
        Me.objApproverName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverName.Location = New System.Drawing.Point(219, 27)
        Me.objApproverName.Name = "objApproverName"
        Me.objApproverName.Size = New System.Drawing.Size(137, 25)
        Me.objApproverName.TabIndex = 207
        Me.objApproverName.Text = "0"
        Me.objApproverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objApproverLevelVal
        '
        Me.objApproverLevelVal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objApproverLevelVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverLevelVal.Location = New System.Drawing.Point(219, 53)
        Me.objApproverLevelVal.Name = "objApproverLevelVal"
        Me.objApproverLevelVal.Size = New System.Drawing.Size(137, 25)
        Me.objApproverLevelVal.TabIndex = 208
        Me.objApproverLevelVal.Text = "0"
        Me.objApproverLevelVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApproverLevel
        '
        Me.lblApproverLevel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverLevel.Location = New System.Drawing.Point(4, 53)
        Me.lblApproverLevel.Name = "lblApproverLevel"
        Me.lblApproverLevel.Size = New System.Drawing.Size(208, 25)
        Me.lblApproverLevel.TabIndex = 204
        Me.lblApproverLevel.Text = "Approver Level"
        Me.lblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbStaffRequisitionList
        '
        Me.gbStaffRequisitionList.BorderColor = System.Drawing.Color.Black
        Me.gbStaffRequisitionList.Checked = False
        Me.gbStaffRequisitionList.CollapseAllExceptThis = False
        Me.gbStaffRequisitionList.CollapsedHoverImage = Nothing
        Me.gbStaffRequisitionList.CollapsedNormalImage = Nothing
        Me.gbStaffRequisitionList.CollapsedPressedImage = Nothing
        Me.gbStaffRequisitionList.CollapseOnLoad = False
        Me.gbStaffRequisitionList.Controls.Add(Me.lnkViewJobDescription)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtJobDesc)
        Me.gbStaffRequisitionList.Controls.Add(Me.pnlEmployee)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtNoofPosition)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblNoofPosition)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtWorkStartDate)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtJob)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtAddStaffReason)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtLeavingReason)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtClass)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtClassGroup)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtName)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtAllocation)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtStatus)
        Me.gbStaffRequisitionList.Controls.Add(Me.txtFormNo)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblFormNo)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblAddStaffReason)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblLeavingReason)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblWorkStartDate)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblClass)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblJobDesc)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblClassGroup)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblName)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblJob)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblEmployee)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblAllocation)
        Me.gbStaffRequisitionList.Controls.Add(Me.lblStatus)
        Me.gbStaffRequisitionList.ExpandedHoverImage = Nothing
        Me.gbStaffRequisitionList.ExpandedNormalImage = Nothing
        Me.gbStaffRequisitionList.ExpandedPressedImage = Nothing
        Me.gbStaffRequisitionList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStaffRequisitionList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStaffRequisitionList.HeaderHeight = 25
        Me.gbStaffRequisitionList.HeaderMessage = ""
        Me.gbStaffRequisitionList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStaffRequisitionList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStaffRequisitionList.HeightOnCollapse = 0
        Me.gbStaffRequisitionList.LeftTextSpace = 0
        Me.gbStaffRequisitionList.Location = New System.Drawing.Point(12, 66)
        Me.gbStaffRequisitionList.Name = "gbStaffRequisitionList"
        Me.gbStaffRequisitionList.OpenHeight = 300
        Me.gbStaffRequisitionList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStaffRequisitionList.ShowBorder = True
        Me.gbStaffRequisitionList.ShowCheckBox = False
        Me.gbStaffRequisitionList.ShowCollapseButton = False
        Me.gbStaffRequisitionList.ShowDefaultBorderColor = True
        Me.gbStaffRequisitionList.ShowDownButton = False
        Me.gbStaffRequisitionList.ShowHeader = True
        Me.gbStaffRequisitionList.Size = New System.Drawing.Size(498, 519)
        Me.gbStaffRequisitionList.TabIndex = 0
        Me.gbStaffRequisitionList.Temp = 0
        Me.gbStaffRequisitionList.Text = "Staff Requisition Information"
        Me.gbStaffRequisitionList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkViewJobDescription
        '
        Me.lnkViewJobDescription.BackColor = System.Drawing.Color.Transparent
        Me.lnkViewJobDescription.Location = New System.Drawing.Point(118, 186)
        Me.lnkViewJobDescription.Name = "lnkViewJobDescription"
        Me.lnkViewJobDescription.Size = New System.Drawing.Size(230, 13)
        Me.lnkViewJobDescription.TabIndex = 319
        Me.lnkViewJobDescription.TabStop = True
        Me.lnkViewJobDescription.Text = "View Job Description"
        '
        'txtJobDesc
        '
        Me.txtJobDesc.AcceptsTab = True
        Me.txtJobDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtJobDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobDesc.Location = New System.Drawing.Point(121, 142)
        Me.txtJobDesc.Name = "txtJobDesc"
        Me.txtJobDesc.ReadOnly = True
        Me.txtJobDesc.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth
        Me.txtJobDesc.Size = New System.Drawing.Size(365, 41)
        Me.txtJobDesc.TabIndex = 80
        Me.txtJobDesc.Text = ""
        '
        'pnlEmployee
        '
        Me.pnlEmployee.Controls.Add(Me.dgvEmployeeList)
        Me.pnlEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployee.Location = New System.Drawing.Point(11, 222)
        Me.pnlEmployee.Name = "pnlEmployee"
        Me.pnlEmployee.Size = New System.Drawing.Size(475, 195)
        Me.pnlEmployee.TabIndex = 82
        '
        'dgvEmployeeList
        '
        Me.dgvEmployeeList.AllowUserToAddRows = False
        Me.dgvEmployeeList.AllowUserToDeleteRows = False
        Me.dgvEmployeeList.BackgroundColor = System.Drawing.Color.White
        Me.dgvEmployeeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmployeeList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhCheckAll, Me.objcolhEmpID, Me.colhEmpCode, Me.colhEmpName, Me.colhGrade, Me.colhSalaryBand, Me.objcolhActionReasonId, Me.objcolhAUD})
        Me.dgvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmployeeList.MultiSelect = False
        Me.dgvEmployeeList.Name = "dgvEmployeeList"
        Me.dgvEmployeeList.RowHeadersVisible = False
        Me.dgvEmployeeList.Size = New System.Drawing.Size(475, 195)
        Me.dgvEmployeeList.TabIndex = 79
        '
        'objcolhCheckAll
        '
        Me.objcolhCheckAll.HeaderText = ""
        Me.objcolhCheckAll.Name = "objcolhCheckAll"
        Me.objcolhCheckAll.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhCheckAll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhCheckAll.Visible = False
        Me.objcolhCheckAll.Width = 5
        '
        'objcolhEmpID
        '
        Me.objcolhEmpID.HeaderText = ""
        Me.objcolhEmpID.Name = "objcolhEmpID"
        Me.objcolhEmpID.ReadOnly = True
        Me.objcolhEmpID.Visible = False
        Me.objcolhEmpID.Width = 5
        '
        'colhEmpCode
        '
        Me.colhEmpCode.HeaderText = "Code"
        Me.colhEmpCode.Name = "colhEmpCode"
        Me.colhEmpCode.ReadOnly = True
        Me.colhEmpCode.Width = 80
        '
        'colhEmpName
        '
        Me.colhEmpName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmpName.HeaderText = "Employee Name"
        Me.colhEmpName.Name = "colhEmpName"
        Me.colhEmpName.ReadOnly = True
        '
        'colhGrade
        '
        Me.colhGrade.HeaderText = "Grade"
        Me.colhGrade.Name = "colhGrade"
        Me.colhGrade.ReadOnly = True
        '
        'colhSalaryBand
        '
        Me.colhSalaryBand.HeaderText = "Salary Band"
        Me.colhSalaryBand.Name = "colhSalaryBand"
        Me.colhSalaryBand.ReadOnly = True
        '
        'objcolhActionReasonId
        '
        Me.objcolhActionReasonId.HeaderText = ""
        Me.objcolhActionReasonId.Name = "objcolhActionReasonId"
        Me.objcolhActionReasonId.ReadOnly = True
        Me.objcolhActionReasonId.Visible = False
        Me.objcolhActionReasonId.Width = 5
        '
        'objcolhAUD
        '
        Me.objcolhAUD.HeaderText = "AUD"
        Me.objcolhAUD.Name = "objcolhAUD"
        Me.objcolhAUD.ReadOnly = True
        Me.objcolhAUD.Visible = False
        Me.objcolhAUD.Width = 5
        '
        'txtNoofPosition
        '
        Me.txtNoofPosition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNoofPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoofPosition.Location = New System.Drawing.Point(121, 488)
        Me.txtNoofPosition.MaxLength = 255
        Me.txtNoofPosition.Name = "txtNoofPosition"
        Me.txtNoofPosition.ReadOnly = True
        Me.txtNoofPosition.Size = New System.Drawing.Size(124, 21)
        Me.txtNoofPosition.TabIndex = 10
        '
        'lblNoofPosition
        '
        Me.lblNoofPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoofPosition.Location = New System.Drawing.Point(8, 490)
        Me.lblNoofPosition.Name = "lblNoofPosition"
        Me.lblNoofPosition.Size = New System.Drawing.Size(107, 16)
        Me.lblNoofPosition.TabIndex = 79
        Me.lblNoofPosition.Text = "No. of Position"
        Me.lblNoofPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWorkStartDate
        '
        Me.txtWorkStartDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtWorkStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkStartDate.Location = New System.Drawing.Point(362, 488)
        Me.txtWorkStartDate.MaxLength = 255
        Me.txtWorkStartDate.Name = "txtWorkStartDate"
        Me.txtWorkStartDate.ReadOnly = True
        Me.txtWorkStartDate.Size = New System.Drawing.Size(124, 21)
        Me.txtWorkStartDate.TabIndex = 11
        '
        'txtJob
        '
        Me.txtJob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJob.Location = New System.Drawing.Point(121, 115)
        Me.txtJob.MaxLength = 255
        Me.txtJob.Name = "txtJob"
        Me.txtJob.ReadOnly = True
        Me.txtJob.Size = New System.Drawing.Size(124, 21)
        Me.txtJob.TabIndex = 6
        '
        'txtAddStaffReason
        '
        Me.txtAddStaffReason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAddStaffReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddStaffReason.Location = New System.Drawing.Point(121, 452)
        Me.txtAddStaffReason.MaxLength = 255
        Me.txtAddStaffReason.Multiline = True
        Me.txtAddStaffReason.Name = "txtAddStaffReason"
        Me.txtAddStaffReason.ReadOnly = True
        Me.txtAddStaffReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAddStaffReason.Size = New System.Drawing.Size(365, 30)
        Me.txtAddStaffReason.TabIndex = 9
        '
        'txtLeavingReason
        '
        Me.txtLeavingReason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLeavingReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeavingReason.Location = New System.Drawing.Point(121, 425)
        Me.txtLeavingReason.MaxLength = 255
        Me.txtLeavingReason.Name = "txtLeavingReason"
        Me.txtLeavingReason.ReadOnly = True
        Me.txtLeavingReason.Size = New System.Drawing.Size(365, 21)
        Me.txtLeavingReason.TabIndex = 8
        '
        'txtClass
        '
        Me.txtClass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClass.Location = New System.Drawing.Point(362, 88)
        Me.txtClass.MaxLength = 255
        Me.txtClass.Name = "txtClass"
        Me.txtClass.ReadOnly = True
        Me.txtClass.Size = New System.Drawing.Size(124, 21)
        Me.txtClass.TabIndex = 5
        '
        'txtClassGroup
        '
        Me.txtClassGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClassGroup.Location = New System.Drawing.Point(121, 88)
        Me.txtClassGroup.MaxLength = 255
        Me.txtClassGroup.Name = "txtClassGroup"
        Me.txtClassGroup.ReadOnly = True
        Me.txtClassGroup.Size = New System.Drawing.Size(124, 21)
        Me.txtClassGroup.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(362, 61)
        Me.txtName.MaxLength = 255
        Me.txtName.Name = "txtName"
        Me.txtName.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(124, 21)
        Me.txtName.TabIndex = 3
        '
        'txtAllocation
        '
        Me.txtAllocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAllocation.Location = New System.Drawing.Point(121, 61)
        Me.txtAllocation.MaxLength = 255
        Me.txtAllocation.Name = "txtAllocation"
        Me.txtAllocation.ReadOnly = True
        Me.txtAllocation.Size = New System.Drawing.Size(124, 21)
        Me.txtAllocation.TabIndex = 2
        '
        'txtStatus
        '
        Me.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatus.Location = New System.Drawing.Point(362, 34)
        Me.txtStatus.MaxLength = 255
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(124, 21)
        Me.txtStatus.TabIndex = 1
        '
        'txtFormNo
        '
        Me.txtFormNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormNo.Location = New System.Drawing.Point(121, 34)
        Me.txtFormNo.MaxLength = 255
        Me.txtFormNo.Name = "txtFormNo"
        Me.txtFormNo.ReadOnly = True
        Me.txtFormNo.Size = New System.Drawing.Size(124, 21)
        Me.txtFormNo.TabIndex = 0
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(8, 36)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(107, 16)
        Me.lblFormNo.TabIndex = 65
        Me.lblFormNo.Text = "Form No."
        Me.lblFormNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddStaffReason
        '
        Me.lblAddStaffReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddStaffReason.Location = New System.Drawing.Point(8, 454)
        Me.lblAddStaffReason.Name = "lblAddStaffReason"
        Me.lblAddStaffReason.Size = New System.Drawing.Size(107, 40)
        Me.lblAddStaffReason.TabIndex = 64
        Me.lblAddStaffReason.Text = "Additional Staff Reason"
        '
        'lblLeavingReason
        '
        Me.lblLeavingReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeavingReason.Location = New System.Drawing.Point(8, 427)
        Me.lblLeavingReason.Name = "lblLeavingReason"
        Me.lblLeavingReason.Size = New System.Drawing.Size(107, 16)
        Me.lblLeavingReason.TabIndex = 63
        Me.lblLeavingReason.Text = "Leaving Reason"
        Me.lblLeavingReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWorkStartDate
        '
        Me.lblWorkStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkStartDate.Location = New System.Drawing.Point(249, 488)
        Me.lblWorkStartDate.Name = "lblWorkStartDate"
        Me.lblWorkStartDate.Size = New System.Drawing.Size(107, 26)
        Me.lblWorkStartDate.TabIndex = 62
        Me.lblWorkStartDate.Text = "Date to Start Work"
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(249, 90)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(107, 16)
        Me.lblClass.TabIndex = 60
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobDesc
        '
        Me.lblJobDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobDesc.Location = New System.Drawing.Point(8, 143)
        Me.lblJobDesc.Name = "lblJobDesc"
        Me.lblJobDesc.Size = New System.Drawing.Size(107, 40)
        Me.lblJobDesc.TabIndex = 61
        Me.lblJobDesc.Text = "Job Description && Indicators"
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(8, 90)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(107, 16)
        Me.lblClassGroup.TabIndex = 59
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(249, 62)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(107, 24)
        Me.lblName.TabIndex = 58
        Me.lblName.Text = "Name"
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 117)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(107, 16)
        Me.lblJob.TabIndex = 57
        Me.lblJob.Text = "Job Title"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 202)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(237, 16)
        Me.lblEmployee.TabIndex = 56
        Me.lblEmployee.Text = "Employee(s) to be replaced"
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(8, 63)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(107, 16)
        Me.lblAllocation.TabIndex = 55
        Me.lblAllocation.Text = "Requisition By"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(249, 36)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(107, 16)
        Me.lblStatus.TabIndex = 54
        Me.lblStatus.Text = "Requisition Type"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(904, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Approve / Disapprove Staff Requisition"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkViewStaffRequisitionFormReport)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 591)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(904, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(692, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(795, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkViewStaffRequisitionFormReport
        '
        Me.lnkViewStaffRequisitionFormReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkViewStaffRequisitionFormReport.Location = New System.Drawing.Point(20, 22)
        Me.lnkViewStaffRequisitionFormReport.Name = "lnkViewStaffRequisitionFormReport"
        Me.lnkViewStaffRequisitionFormReport.Size = New System.Drawing.Size(230, 13)
        Me.lnkViewStaffRequisitionFormReport.TabIndex = 320
        Me.lnkViewStaffRequisitionFormReport.TabStop = True
        Me.lnkViewStaffRequisitionFormReport.Text = "View Staff Requisition Form Report"
        '
        'frmApproveDisapproveStaffRequisition
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(904, 646)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApproveDisapproveStaffRequisition"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approve / Disapprove Staff Requisition"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.gbStaffReqApprovalnfo.ResumeLayout(False)
        Me.gbStaffReqApprovalnfo.PerformLayout()
        CType(Me.dgvStaffRequisitionAttachment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objtlpJob.ResumeLayout(False)
        Me.gbStaffRequisitionList.ResumeLayout(False)
        Me.gbStaffRequisitionList.PerformLayout()
        Me.pnlEmployee.ResumeLayout(False)
        CType(Me.dgvEmployeeList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents gbStaffReqApprovalnfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objLevelPriorityVal As System.Windows.Forms.Label
    Friend WithEvents lblLevelPriority As System.Windows.Forms.Label
    Friend WithEvents objApproverLevelVal As System.Windows.Forms.Label
    Friend WithEvents objApproverName As System.Windows.Forms.Label
    Friend WithEvents lblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents gbStaffRequisitionList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objtlpJob As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents lblAddStaffReason As System.Windows.Forms.Label
    Friend WithEvents lblLeavingReason As System.Windows.Forms.Label
    Friend WithEvents lblWorkStartDate As System.Windows.Forms.Label
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents lblJobDesc As System.Windows.Forms.Label
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtFormNo As System.Windows.Forms.TextBox
    Friend WithEvents txtLeavingReason As System.Windows.Forms.TextBox
    Friend WithEvents txtClass As System.Windows.Forms.TextBox
    Friend WithEvents txtClassGroup As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtAllocation As System.Windows.Forms.TextBox
    Friend WithEvents txtStatus As System.Windows.Forms.TextBox
    Friend WithEvents txtJob As System.Windows.Forms.TextBox
    Friend WithEvents txtAddStaffReason As System.Windows.Forms.TextBox
    Friend WithEvents txtWorkStartDate As System.Windows.Forms.TextBox
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovalStatus As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblVariation As System.Windows.Forms.Label
    Friend WithEvents objlblAvailable As System.Windows.Forms.Label
    Friend WithEvents lblAvailable As System.Windows.Forms.Label
    Friend WithEvents lblVariation As System.Windows.Forms.Label
    Friend WithEvents objlblPlanned As System.Windows.Forms.Label
    Friend WithEvents lblPlanned As System.Windows.Forms.Label
    Friend WithEvents lblJobHeadCountInfo As System.Windows.Forms.Label
    Friend WithEvents lblApproverInfo As System.Windows.Forms.Label
    Friend WithEvents txtNoofPosition As System.Windows.Forms.TextBox
    Friend WithEvents lblNoofPosition As System.Windows.Forms.Label
    Friend WithEvents pnlEmployee As System.Windows.Forms.Panel
    Friend WithEvents dgvEmployeeList As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhCheckAll As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhEmpID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSalaryBand As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhActionReasonId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAUD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtJobDesc As System.Windows.Forms.RichTextBox
    Friend WithEvents lnkViewJobDescription As System.Windows.Forms.LinkLabel
    Friend WithEvents dgvStaffRequisitionAttachment As System.Windows.Forms.DataGridView
    Friend WithEvents sfdAttachment As System.Windows.Forms.SaveFileDialog
    Friend WithEvents lblAttachment As System.Windows.Forms.Label
    Friend WithEvents colhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhfile_path As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhlocalpath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhfileuniquename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkViewStaffRequisitionFormReport As System.Windows.Forms.LinkLabel
End Class
