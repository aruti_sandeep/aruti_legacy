﻿'Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports System.Text
Imports System.IO

#End Region

Public Class frmApplicantFilter_List
    Private ReadOnly mstrModuleName As String = "frmApplicantFilter_List"

#Region " Private Variable(s) "

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
    Private dtList As DataTable = Nothing
    Private dtMainTab As DataTable = Nothing
    Private strCSVRefNo As String = String.Empty
    Private mblnIsSuccess As Boolean = False
    Private strShortListIds As String = String.Empty
    'S.SANDEEP [ 14 May 2013 ] -- END

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()

        Dim dsList As DataSet = Nothing
        Try

            Dim objVacancy As New clsVacancy
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'dsList = objVacancy.getComboList(True, "Vacancy")
            'cboVancancy.DisplayMember = "name"
            'cboVancancy.ValueMember = "id"
            'cboVancancy.DataSource = dsList.Tables(0)
            dsList = objVacancy.getVacancyType()
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            With cboStatus
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "(A). Approved"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "(P). Pending"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP [ 14 May 2013 ] -- END

            dsList = Nothing
            Dim objShortMst As New clsshortlist_master
            dsList = objShortMst.getComboList("List", True)
            cboRefNo.DisplayMember = "refno"
            cboRefNo.ValueMember = "shortlistunkid"
            cboRefNo.DataSource = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private Sub FillList()
        Dim strSearching As String = ""
        Try
            If User._Object.Privilege._AllowToViewShortListApplicants = True Then
                Dim objShortMst As New clsshortlist_master
                Dim dTab As DataTable = Nothing
                dTab = objShortMst.Get_Table("List", CInt(cboVacancy.SelectedValue))

                dtMainTab = dTab

                If CInt(cboRefNo.SelectedValue) > 0 Then
                    strSearching = "AND orefno = '" & cboRefNo.Text.Trim.ToString() & "' "
                End If

                If dtpStartdate.Checked Then
                    strSearching &= "AND openingdate >= '" & eZeeDate.convertDate(dtpStartdate.Value) & "' "
                End If

                If dtEndDate.Checked Then
                    strSearching &= "AND closingdate <= '" & eZeeDate.convertDate(dtEndDate.Value) & "' "
                End If

                If cboStatus.SelectedIndex > 0 Then
                    strSearching &= "AND StatusId = '" & cboStatus.SelectedIndex & "' "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtList = New DataView(dTab, strSearching, "sortid", DataViewRowState.CurrentRows).ToTable
                Else
                    dtList = dTab
                End If

                'Hemant (30 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : On the shortlisting screen, they want to have a serial number column before the reference number.
                If dtList.Columns.Contains("SerialNo") = False Then
                    Dim dtCol As New DataColumn("SerialNo", Type.GetType("System.String"))
                    dtCol.DefaultValue = ""
                    dtList.Columns.Add(dtCol)
                End If
                Dim intSerialNo As Integer = 1
                Dim intVacancyId As Integer
                For Each drRow As DataRow In dtList.Select("IsGrp = 1", "vacancyunkid,shortlistunkid")
                    If intVacancyId <> CInt(drRow.Item("vacancyunkid")) Then intVacancyId = 1
                    drRow.Item("SerialNo") = intSerialNo.ToString
                    intSerialNo = intSerialNo + 1
                Next
                'Hemant (30 Mar 2022) -- End
                dgvData.AutoGenerateColumns = False

                objdgcolhApplicantId.DataPropertyName = "applicantunkid"
                objdgcolhGrpId.DataPropertyName = "GrpId"
                objdgcolhIsCheck.DataPropertyName = "ischeck"
                objdgcolhIsGrp.DataPropertyName = "IsGrp"
                objdgcolhshortlistunkid.DataPropertyName = "shortlistunkid"
                objdgcolhStatusId.DataPropertyName = "StatusId"
                objdgcolhVacId.DataPropertyName = "vacancyunkid"
                dgcolhRefNo.DataPropertyName = "refno"
                dgcolhRemark.DataPropertyName = "remark"
                dgcolhStatus.DataPropertyName = "Status"
                dgcolhVacancy.DataPropertyName = "vacancytitle"
                'Hemant (30 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : On the shortlisting screen, they want to have a serial number column before the reference number.
                dgcolhSerialNo.DataPropertyName = "SerialNo"
                'Hemant (30 Mar 2022) -- End

                dgvData.DataSource = dtList

                Call SetGridColor()
                Call SetCollapseValue()

            End If


            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes
            objChkAll.Checked = False
            'Pinkal (27-Jun-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            Dim pCell As New clsMergeCell

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                    'If CInt(dgvData.Rows(i).Cells(objdgcolhStatusId.Index).Value) = 1 Then dgvData.Rows(i).Cells(objdgcolhIsCheck.Index).ReadOnly = True
                Else
                    pCell.MakeMerge(dgvData, dgvData.Rows(i).Index, 0, 1, Color.White, Color.White, , , picStayView.Image)
                    dgvData.Rows(i).ReadOnly = True
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                    dgvData.Rows(i).Visible = False
                    dgvData.Rows(i).Cells(objdgcolhIsCheck.Index).ToolTipText = ""
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddApplicantFilter
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteApplicantFilter
            mnuSubmitforApproval.Enabled = User._Object.Privilege._AllowToSubmitApplicantFilter_Approval

            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1400) NMB - Post assigned employee assets to P2P
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            If objGroup._Groupname.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                mnuSendFeedBackToApplicants.Visible = True
            Else
                mnuSendFeedBackToApplicants.Visible = False
            End If
            objGroup = Nothing
            'Pinkal (16-Oct-2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Set_Notification_Approvals(ByVal uName As String, ByVal iUserId As Integer) As String 'S.SANDEEP [ 31 DEC 2013 ] -- START {iUserId} -- END
        'Private Function Set_Notification_Approvals(ByVal uName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & uName & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 24, "Dear") & " <b>" & getTitleCase(uName) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to notify you that the initial shortlisting of Applicants for the vacancy of <b>" & cboVacancy.Text & "</b> which is an " & cboVacancyType.Text & " vacancy is complete. </span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 25, "This is to notify you that the initial shortlisting of Applicants for the vacancy of") & " <b>" & cboVacancy.Text & "</b> " & Language.getMessage(mstrModuleName, 26, "which is an") & cboVacancyType.Text & " " & Language.getMessage(mstrModuleName, 27, "vacancy is complete.") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The reference number(s) : <b>" & strCSVRefNo & "</b> was/were automatically generated and attached to the shortlisting process. </span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 28, "The reference number(s):") & " <b>" & strCSVRefNo & "</b> " & Language.getMessage(mstrModuleName, 29, "was/were automatically generated and attached to the shortlisting process.") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<br>")
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'StrMessage.Append("Please login to Aruti Recruitment and <b> Approve/Disapprove </b> the shortlisting criteria used.</span></p>")
            Dim iDataValue As String = Company._Object._Companyunkid & "|" & iUserId & "|" & cboVacancyType.SelectedValue.ToString & "|" & cboVacancy.SelectedValue.ToString & "|" & _
                                       cboRefNo.SelectedValue.ToString & "|" & cboStatus.SelectedIndex.ToString & "|" & IIf(dtpStartdate.Checked = False, "", dtpStartdate.Value.Date) & "|" & IIf(dtEndDate.Checked = False, "", dtEndDate.Value.Date)


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language



            'StrMessage.Append("Please login to Aruti Recruitment or click below link <BR> " & _
            '                  ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/wPg_ApplicantFilterList.aspx?" & _
            '                  System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iDataValue)) & _
            '                  "<BR> to <b> Approve/Disapprove </b> the shortlisting criteria used.</span></p>")

            StrMessage.Append(Language.getMessage(mstrModuleName, 33, "Please login to Aruti Recruitment or click below link") & " <BR> " & _
                              ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/wPg_ApplicantFilterList.aspx?" & _
                              System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iDataValue)) & _
                             "<BR>" & Language.getMessage(mstrModuleName, 30, "to") & " <b>" & Language.getMessage(mstrModuleName, 31, "Approve/Disapprove") & "</b> " & Language.getMessage(mstrModuleName, 32, "the shortlisting criteria used.") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End

            'S.SANDEEP [ 31 DEC 2013 ] -- END


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
            Return ""
        End Try
        Return StrMessage.ToString
    End Function

    Private Sub Send_Notification()
        Try
            Dim dicUnQEmail As New Dictionary(Of String, String)
            Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
            Dim dUList As New DataSet : Dim dtULst As DataTable = Nothing
            dUList = objUsr.Get_UserBy_PrivilegeId(800)
            dtULst = dUList.Tables(0)
            dUList = objUsr.Get_UserBy_PrivilegeId(801)
            dtULst.Merge(dUList.Tables(0), True)
            If dtULst.Rows.Count > 0 Then
                For Each dRow As DataRow In dtULst.Rows
                    If dicUnQEmail.ContainsKey(CStr(dRow.Item("UEmail"))) = True Then Continue For
                    dicUnQEmail.Add(CStr(dRow.Item("UEmail")), CStr(dRow.Item("UEmail")))
                    Dim objSMail As New clsSendMail
                    'S.SANDEEP [ 31 DEC 2013 ] -- START
                    'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")))
                    StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")))
                    'S.SANDEEP [ 31 DEC 2013 ] -- END
                    objSMail._ToEmail = CStr(dRow.Item("UEmail"))
                    objSMail._Subject = Language.getMessage(mstrModuleName, 13, "Notifications to Approve Shortlisting Criteria")
                    objSMail._Message = StrMessage
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    'objSMail._FormName = mstrModuleName
                    'objSMail._LoginEmployeeunkid = -1
                    With objSMail
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    objSMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSMail._UserUnkid = User._Object._Userunkid
                    objSMail._SenderAddress = User._Object._Email
                    objSMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objSMail.SendMail() : objSMail = Nothing
                    objSMail.SendMail(Company._Object._Companyunkid) : objSMail = Nothing
                    'Sohail (30 Nov 2017) -- End
                    mblnIsSuccess = True
                Next
            Else
                mblnIsSuccess = False
            End If
        Catch ex As Exception
            mblnIsSuccess = False
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 14 May 2013 ] -- END



    'Pinkal (18-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function


    Private Function Export_to_Excel(ByVal mstrVacancyType As String, ByVal flFileName As String, ByVal SavePath As String, ByVal IsOpenAfterExport As Boolean, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try


            'START REMOVE NO CONDITION COLUMN FROM DATATABLE

SetColumnCount:
            Dim intColCount As Integer = objDataReader.Columns.Count
            For j As Integer = 0 To intColCount - 1
                Dim drRow As DataRow() = Nothing

                If objDataReader.Columns(j).ColumnName = "logicalcondition" Then Continue For

                If objDataReader.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                    drRow = objDataReader.Select("[" & objDataReader.Columns(j).ColumnName & "] = 0 OR [" & objDataReader.Columns(j).ColumnName & "] = 0.00")
                    If drRow.Length = objDataReader.Rows.Count Then
                        objDataReader.Columns.RemoveAt(j)
                        GoTo SetColumnCount
                    End If

                ElseIf objDataReader.Columns(j).DataType Is Type.GetType("System.String") Then
                    drRow = objDataReader.Select("[" & objDataReader.Columns(j).ColumnName & "] = ''")
                    If drRow.Length = objDataReader.Rows.Count Then
                        objDataReader.Columns.RemoveAt(j)
                        GoTo SetColumnCount
                    End If

                End If
            Next

            'END REMOVE NO CONDITION COLUMN FROM DATATABLE


            'HEADER PART


            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append("<TD WIDTH='10%' COLSPAN = " & objDataReader.Columns.Count - 15 & " ALIGN = 'MIDDLE'> " & vbCrLf)
            strBuilder.Append("<FONT SIZE=3><B>")
            strBuilder.Append(Language.getMessage(mstrModuleName, 17, "Shortlisting Criteria for Reference# :- ") & objDataReader.Rows(0)("RefNo").ToString() & vbCrLf)
            strBuilder.Append(" </FONT></B></CENTER>")
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append("<TD WIDTH='10%' COLSPAN = " & objDataReader.Columns.Count - 15 & " ALIGN = 'MIDDLE'> " & vbCrLf)
            strBuilder.Append("<FONT SIZE=3><B>")
            strBuilder.Append(Language.getMessage(mstrModuleName, 18, "Vacancy Type:- ") & mstrVacancyType)
            strBuilder.Append(" </FONT></B></CENTER>")
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append("<TD WIDTH='10%' COLSPAN = " & objDataReader.Columns.Count - 15 & " ALIGN = 'MIDDLE'> " & vbCrLf)
            strBuilder.Append("<FONT SIZE=3><B>")
            Dim mstrVacancyName As String = objDataReader.Rows(0)("vacancytitle").ToString().Trim
            strBuilder.Append(Language.getMessage(mstrModuleName, 19, "Vacancy Name:- ") & mstrVacancyName)
            strBuilder.Append(" </FONT></B></CENTER>")
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)




            ' Column Caption

            For j As Integer = 0 To objDataReader.Columns.Count - 1
                If objDataReader.Columns(j).ColumnName.ToString() = "refno" Or objDataReader.Columns(j).ColumnName.ToString() = "vacancytitle" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "filterunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "shortlistunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "qualificationgroupunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "qualificationunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "resultunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "qlevel" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "resultlevel" Or objDataReader.Columns(j).ColumnName.ToString() = "result_lvl_condition" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "resultname" Or objDataReader.Columns(j).ColumnName.ToString() = "gpacode" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "gpacode_condition" Or objDataReader.Columns(j).ColumnName.ToString() = "gender" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "age" Or objDataReader.Columns(j).ColumnName.ToString() = "age_condition" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "skillcategoryunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "skillunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "award_year" Or objDataReader.Columns(j).ColumnName.ToString() = "award_year_condition" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "award_start_date" Or objDataReader.Columns(j).ColumnName.ToString() = "userunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "isvoid" Or objDataReader.Columns(j).ColumnName.ToString() = "voiduserunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "voiddatetime" Or objDataReader.Columns(j).ColumnName.ToString() = "voidreason" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "logical_condition" Or objDataReader.Columns(j).ColumnName.ToString() = "nationalityunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "experience_days" Or objDataReader.Columns(j).ColumnName.ToString() = "qlevel_condition" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "experience_condition" Or objDataReader.Columns(j).ColumnName.ToString() = "qualificationgroup" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "qualificationgroup_condition" Then Continue For
                'Hemant (30 Oct 2019) -- [objDataReader.Columns(j).ColumnName.ToString() = "qualificationgroup", objDataReader.Columns(j).ColumnName.ToString() = "qualificationgroup_condition"]
                'Hemant (07 Oct 2019) -- [objDataReader.Columns(j).ColumnName.ToString() = "nationalityunkid", objDataReader.Columns(j).ColumnName.ToString() = "experience_days", objDataReader.Columns(j).ColumnName.ToString() = "experience_condition", objDataReader.Columns(j).ColumnName.ToString() = "qlevel_condition"]

                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            ''Data Part
            For i As Integer = 0 To objDataReader.Rows.Count - 1

                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1

                    If objDataReader.Columns(k).ColumnName.ToString() = "refno" Or objDataReader.Columns(k).ColumnName.ToString() = "vacancytitle" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "filterunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "shortlistunkid" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "qualificationgroupunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "qualificationunkid" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "resultunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "qlevel" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "resultlevel" Or objDataReader.Columns(k).ColumnName.ToString() = "result_lvl_condition" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "resultname" Or objDataReader.Columns(k).ColumnName.ToString() = "gpacode" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "gpacode_condition" Or objDataReader.Columns(k).ColumnName.ToString() = "gender" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "age" Or objDataReader.Columns(k).ColumnName.ToString() = "age_condition" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "skillcategoryunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "skillunkid" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "award_year" Or objDataReader.Columns(k).ColumnName.ToString() = "award_year_condition" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "award_start_date" Or objDataReader.Columns(k).ColumnName.ToString() = "userunkid" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "isvoid" Or objDataReader.Columns(k).ColumnName.ToString() = "voiduserunkid" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "voiddatetime" Or objDataReader.Columns(k).ColumnName.ToString() = "voidreason" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "logical_condition" Or objDataReader.Columns(k).ColumnName.ToString() = "nationalityunkid" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "experience_days" Or objDataReader.Columns(k).ColumnName.ToString() = "qlevel_condition" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "experience_condition" Or objDataReader.Columns(k).ColumnName.ToString() = "qualificationgroup" Or _
                      objDataReader.Columns(k).ColumnName.ToString() = "qualificationgroup_condition" Then Continue For
                    'Hemant (30 Oct 2019) -- [objDataReader.Columns(k).ColumnName.ToString() = "qualificationgroup", objDataReader.Columns(k).ColumnName.ToString() = "qualificationgroup_condition"]
                    'Hemant (07 Oct 2019) -- [objDataReader.Columns(k).ColumnName.ToString() = "nationalityunkid", objDataReader.Columns(k).ColumnName.ToString() = "experience_days", objDataReader.Columns(k).ColumnName.ToString() = "experience_condition", objDataReader.Columns(k).ColumnName.ToString() = "qlevel_condition"]

                    If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
                        If objDataReader.Rows(i)(k).ToString() <> "" Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" </HTML> " & vbCrLf)


            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            blnFlag = SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder)

            If IsOpenAfterExport Then
                Call Shell("explorer " & SavePath & "\" & flFileName & ".xls", vbMaximizedFocus)
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel", mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (18-Jun-2013) -- End


#End Region

#Region " Form's Event(s) "

    Private Sub frmApplicantFilter_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            Call SetVisibility()
            'S.SANDEEP [ 14 May 2013 ] -- END

            'Varsha (03 Jan 2018) -- Start                
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            munViewApplicantCV.Enabled = False
            'Varsha (03 Jan 2018) -- End

            'Pinkal (12-Nov-2020) -- Start
            'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
            mnuSubmitforApproval.Visible = Not ConfigParameter._Object._SkipApprovalFlowForApplicantShortListing
            mnuRefNoApproval.Visible = Not ConfigParameter._Object._SkipApprovalFlowForApplicantShortListing
            'Pinkal (12-Nov-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantFilter_List_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    'Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        'S.SANDEEP [ 14 May 2013 ] -- END
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboRefNo.DataSource, DataTable)
            With cboRefNo
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Vacancy Type is mandatory information. Please select Vacancy Type to Fill List."), enMsgBoxStyle.Information)
                cboVacancyType.Focus() : Exit Sub
            End If

            If CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Vacancy is mandatory information. Please select Vacancy to Fill List."), enMsgBoxStyle.Information)
                cboVacancy.Focus() : Exit Sub
            End If

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboRefNo.Items.Count > 0 Then cboRefNo.SelectedValue = 0
            If cboVacancy.Items.Count > 0 Then cboVacancy.SelectedValue = 0
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            cboVacancyType.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            dtList = Nothing
            'S.SANDEEP [ 14 May 2013 ] -- END
            dtpStartdate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpStartdate.Checked = False
            dtEndDate.Checked = False
            cboRefNo.Focus()


            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes
            FillList()
            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objApplicantFilter As New frmApplicantFilter
            If objApplicantFilter.displayDialog(enAction.ADD_ONE) Then
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim dRow() As DataRow = dtList.Select("IsCheck = True")
        If dRow.Length <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please tick atleast one Applicant Filter from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            Exit Sub
        End If
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete checked Applicant Filter?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim objShortMst As New clsshortlist_master
                Dim frm As New frmReasonSelection : Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.APPLICANT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                Dim blnFlag As Boolean = False
                For i As Integer = 0 To dRow.Length - 1
                    If CInt(dRow(i).Item("StatusId")) = enShortListing_Status.SC_APPROVED Then
                        If blnFlag = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You cannot delete already approved reference No. These Reference No. will be discarded.") & vbCrLf & _
                                                                Language.getMessage(mstrModuleName, 23, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                blnFlag = True : Continue For
                            Else
                                Exit Sub
                            End If
                        Else
                            Continue For
                        End If
                    Else
                        objShortMst._Voidreason = mstrVoidReason
                        objShortMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objShortMst._Voiduserunkid = User._Object._Userunkid
                        With objShortMst
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        objShortMst.Delete(CInt(dRow(i).Item("shortlistunkid")))
                    End If
                Next

                objShortMst = Nothing
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Control's Event(s) "

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                Dim dsVacList As New DataSet
                Dim objVac As New clsVacancy

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
                'Shani(24-Aug-2015) -- End

                With cboVacancy
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsVacList.Tables("List")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboVacancy.DisplayMember
                .ValueMember = cboVacancy.ValueMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With
            If frm.DisplayDialog = True Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next


                        'Pinkal (27-Jun-2013) -- Start
                        'Enhancement : TRA Changes

                    Case 1

                        Dim dtTable As DataTable = CType(dgvData.DataSource, DataTable)
                        dtTable.Rows(e.RowIndex).AcceptChanges()
                        Dim drGrpRow() As DataRow = dtTable.Select("IsGrp = True")
                        Dim drGrpCheckRow() As DataRow = dtTable.Select("ischeck = True AND IsGrp = True")

                        RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

                        If drGrpCheckRow.Length <= 0 Then
                            objChkAll.CheckState = CheckState.Unchecked
                        ElseIf drGrpRow.Length > drGrpCheckRow.Length Then
                            objChkAll.CheckState = CheckState.Indeterminate
                        ElseIf drGrpRow.Length = drGrpCheckRow.Length Then
                            objChkAll.CheckState = CheckState.Checked
                        End If

                        AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

                        'Pinkal (27-Jun-2013) -- End

                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub munViewApplicantCV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles munViewApplicantCV.Click
        Try
            If dtList Is Nothing Then Exit Sub
            If dgvData.SelectedRows.Count > 0 Then
                Dim objAppCV As New ArutiReports.clsApplicantCVReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objAppCV._ApplicantUnkid = CInt(dgvData.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value)
                objAppCV._ShowJobHistory = True
                objAppCV._ShowOther_Qualification = True
                objAppCV._ShowOther_Skills = True
                objAppCV._ShowProfessional_Qualification = True
                objAppCV._ShowProfessional_Skills = True
                objAppCV._ShowReferences = True


                'Varsha (03 Jan 2018) -- Start                
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                objAppCV._ReportTypeId = 0
                objAppCV._ReportTypeName = Language.getMessage("frmApplicantCVReport", 2, "Applicant CV Report")
                objAppCV._VacancyTypeId = CInt(cboVacancyType.SelectedValue)
                objAppCV._VacancyId = CInt(dgvData.SelectedRows(0).Cells(objdgcolhVacId.Index).Value)
                'Varsha (03 Jan 2018) -- End


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objAppCV.generateReport(0, enPrintAction.Preview, enExportAction.None)
                objAppCV._DocumentPath = ConfigParameter._Object._Document_Path
                objAppCV.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           0, enPrintAction.Preview, enExportAction.None)

                'Shani(24-Aug-2015) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "munViewApplicantCV_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            If dtList IsNot Nothing Then
                For Each dRow As DataRow In dtList.Rows
                    If CBool(dRow.Item("IsGrp")) = True Then dRow.Item("IsCheck") = objChkAll.Checked
                Next
                'dtList.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSubmitforApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSubmitforApproval.Click
        Try
            If dtList Is Nothing Then Exit Sub
            Dim blnFlag As Boolean = False
            Dim blnIssent As Boolean = False
            Dim ndtRow As DataRow() = Nothing
            ndtRow = dtList.Select("IsGrp=True AND IsCheck = True")
            strCSVRefNo = "" : strShortListIds = ""

            If ndtRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please tick all reference no in order to submit for approval."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If ndtRow.Length > 0 Then
                For i As Integer = 0 To ndtRow.Length - 1
                    If CInt(ndtRow(i).Item("StatusId")) = enShortListing_Status.SC_APPROVED Then
                        If blnFlag = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot submit already approved reference no for approval. These Reference No. will be discarded.Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                blnFlag = True : GoTo chk
                            Else
                                Exit Sub
                            End If
                        Else
chk:                        If blnFlag = True Then
                                dtList.Rows(dtList.Rows.IndexOf(ndtRow(i))).Item("IsCheck") = False
                            End If
                        End If
                    Else
                        If CBool(ndtRow(i).Item("issent")) = False Then
                            strCSVRefNo &= ", " & ndtRow(i).Item("orefno").ToString
                            strShortListIds &= "," & ndtRow(i).Item("shortlistunkid").ToString
                        Else
                            blnIssent = True
                        End If
                    End If
                Next
                If strCSVRefNo.Trim.Length > 0 Then strCSVRefNo = Mid(strCSVRefNo, 2) : If strShortListIds.Trim.Length > 0 Then strShortListIds = Mid(strShortListIds, 2)
                If strCSVRefNo.Trim.Length > 0 AndAlso strCSVRefNo.Contains(",") Then strCSVRefNo = strCSVRefNo.Insert(strCSVRefNo.LastIndexOf(","), " and ")
                If strCSVRefNo.Trim.Length > 0 AndAlso strCSVRefNo.Contains(",") Then strCSVRefNo = strCSVRefNo.Remove(strCSVRefNo.LastIndexOf(","), 1)
            End If

            Dim odtRow As DataRow() = dtMainTab.Select("IsGrp=True AND vacancyunkid = '" & CInt(cboVacancy.SelectedValue) & "' AND StatusId = 2 ")
            ndtRow = dtList.Select("IsGrp=True AND vacancyunkid = '" & CInt(cboVacancy.SelectedValue) & "' AND StatusId = 2 AND IsCheck = True")
            If odtRow.Length > ndtRow.Length Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Submit for Approval is for all Reference No in the selected vacancy.") & vbCrLf & _
                                                  Language.getMessage(mstrModuleName, 22, "Please tick all Reference no for Approval."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnIssent = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You cannot do submit for approval operation again. Reason : Notification has already been sent."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            If strCSVRefNo.Trim.Length > 0 Then
                Dim blnMsg As Boolean = False
                Call Send_Notification()
                If mblnIsSuccess = True Then
                    If strShortListIds.Trim.Length > 0 Then
                        For Each strId As String In strShortListIds.Split(CChar(","))
                            Dim objSLMaster As New clsshortlist_master
                            objSLMaster._Shortlistunkid = CInt(strId)
                            objSLMaster._Issent = True
                            With objSLMaster
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnMsg = objSLMaster.Update()
                            objSLMaster = Nothing
                        Next
                    End If
                End If
                If blnMsg = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Notification Sent Successfully."), enMsgBoxStyle.Information)
                    Call FillList()

                    'Pinkal (27-Jun-2013) -- Start
                    'Enhancement : TRA Changes

                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Notification has not been sent successfully."), enMsgBoxStyle.Information)
                    Exit Sub
                    'Pinkal (27-Jun-2013) -- End

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitforApproval_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub mnuRefNoApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRefNoApproval.Click
        Try
            If dtList Is Nothing Then Exit Sub
            Dim ndtRow As DataRow() = Nothing
            ndtRow = dtList.Select("IsGrp=True AND IsCheck = True")

            If ndtRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please tick all reference no in order to Approve/Reject."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim strShortListingIds As String = String.Empty
            If ndtRow.Length > 0 Then
                For i As Integer = 0 To ndtRow.Length - 1
                    strShortListingIds &= "," & ndtRow(i).Item("shortlistunkid").ToString
                Next
            End If
            If strShortListingIds.Trim.Length > 0 Then strShortListingIds = Mid(strShortListingIds, 2)
            Dim frm As New frmApplicantFilter_Approval
            If frm.displayDialog(strShortListingIds, CType(ndtRow(0).Item("StatusId"), enShortListing_Status), CInt(cboVacancy.SelectedValue)) = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRefNoApproval_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 14 May 2013 ] -- END


    'Pinkal (18-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub mnuViewShortlistingCriteria_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewShortlistingCriteria.Click
        Try
            'S.SANDEEP [ 20 NOV 2013 ] -- START
            'If dgvData.SelectedRows.Count < 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please Select the specific Reference No. to do futher opeation on it."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            If dtList Is Nothing Then Exit Sub
            Dim ndtRow As DataRow() = Nothing
            ndtRow = dtList.Select("IsGrp=True AND IsCheck = True")
            If ndtRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please Select the specific Reference No. to perform further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            'S.SANDEEP [ 20 NOV 2013 ] -- END

            Dim objShortListFilter As New clsshortlist_filter
            Dim dsList As DataSet = objShortListFilter.GetList("List", True, CInt(dgvData.SelectedRows(0).Cells(objdgcolhshortlistunkid.Index).Value))

            If dsList.Tables(0).Rows.Count > 0 Then

                Dim dtTable As DataTable = dsList.Tables(0)

                'Hemant (30 Oct 2019) -- Start
                'dtTable.Columns("qualificationgroup").Caption = "Qualification Group"
                dtTable.Columns("qualificationgroupcondition").Caption = "Qualification Group"
                'Hemant (30 Oct 2019) -- End
                dtTable.Columns("resultcondition").Caption = "Result"
                dtTable.Columns("gpacodecondtion").Caption = "GPA"
                dtTable.Columns("other_qualificationgrp").Caption = "Other Qualification Group"
                dtTable.Columns("other_qualification").Caption = "Other Qualification"
                dtTable.Columns("other_resultcode").Caption = "Other ResultCode"
                dtTable.Columns("agecondition").Caption = "Age"
                dtTable.Columns("award_yearcondition").Caption = "Award Year"
                dtTable.Columns("gender_name").Caption = "Gender"
                dtTable.Columns("other_skillcategory").Caption = "Other Skill Category"
                dtTable.Columns("other_skill").Caption = "Other Skill"
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
                dtTable.Columns("qlevelcondition").Caption = "Qualification Group Level"
                dtTable.Columns("experiencecondition").Caption = "Experience"
                'Hemant (07 Oct 2019) -- End
                dtTable.Columns("logicalcondition").Caption = "Condition"

                If Export_to_Excel(cboVacancyType.Text, "ShortListing_Criteria_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dtTable) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                End If

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewShortlistingCriteria_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Jun-2013) -- End

    'Varsha (03 Jan 2018) -- Start                
    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
    'This filter should allow user to report on all cv's shortlisted for that particular position. 
    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.

    Private Sub dgvData_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvData.SelectionChanged
        Try
            If dgvData.SelectedRows.Count <= 0 OrElse CInt(dgvData.SelectedRows(0).Cells(objdgcolhApplicantId.Index).Value) <= 0 Then
                munViewApplicantCV.Enabled = False
            Else
                munViewApplicantCV.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_SelectionChanged", mstrModuleName)
        End Try
    End Sub

    'Varsha (03 Jan 2018) -- End


    'Pinkal (16-Oct-2023) -- Start
    'TRA - Online Recruitment Feedback to Applicants.
    Private Sub mnuSendFeedBackToApplicants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSendFeedBackToApplicants.Click
        Dim objSendFeedback As New frmSendFeedBackToApplicants
        Try
            objSendFeedback.displayDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSendFeedBackToApplicants_Click", mstrModuleName)
        Finally
            objSendFeedback = Nothing
        End Try
    End Sub
    'Pinkal (16-Oct-2023) -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblEnddate.Text = Language._Object.getCaption(Me.lblEnddate.Name, Me.lblEnddate.Text)
            Me.lblStartdate.Text = Language._Object.getCaption(Me.lblStartdate.Name, Me.lblStartdate.Text)
            Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuSubmitforApproval.Text = Language._Object.getCaption(Me.mnuSubmitforApproval.Name, Me.mnuSubmitforApproval.Text)
            Me.mnuRefNoApproval.Text = Language._Object.getCaption(Me.mnuRefNoApproval.Name, Me.mnuRefNoApproval.Text)
            Me.mnuViewShortlistingCriteria.Text = Language._Object.getCaption(Me.mnuViewShortlistingCriteria.Name, Me.mnuViewShortlistingCriteria.Text)
            Me.munViewApplicantCV.Text = Language._Object.getCaption(Me.munViewApplicantCV.Name, Me.munViewApplicantCV.Text)
            Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
            Me.dgcolhVacancy.HeaderText = Language._Object.getCaption(Me.dgcolhVacancy.Name, Me.dgcolhVacancy.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please tick atleast one Applicant Filter from the list to perform further operation on it.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete checked Applicant Filter?")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "(A). Approved")
            Language.setMessage(mstrModuleName, 5, "(P). Pending")
            Language.setMessage(mstrModuleName, 6, "Vacancy Type is mandatory information. Please select Vacancy Type to Fill List.")
            Language.setMessage(mstrModuleName, 7, "Vacancy is mandatory information. Please select Vacancy to Fill List.")
            Language.setMessage(mstrModuleName, 8, "Please tick all reference no in order to submit for approval.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot submit already approved reference no for approval. These Reference No. will be discarded.Do you wish to continue?")
            Language.setMessage(mstrModuleName, 10, "Sorry, Submit for Approval is for all Reference No in the selected vacancy.")
            Language.setMessage(mstrModuleName, 11, "Sorry, You cannot delete already approved reference No. These Reference No. will be discarded.")
            Language.setMessage(mstrModuleName, 12, "Please tick all reference no in order to Approve/Reject.")
            Language.setMessage(mstrModuleName, 13, "Notifications to Approve Shortlisting Criteria")
            Language.setMessage(mstrModuleName, 14, "Sorry, You cannot do submit for approval operation again. Reason : Notification has already been sent.")
            Language.setMessage(mstrModuleName, 15, "Notification Sent Successfully.")
            Language.setMessage(mstrModuleName, 16, "Please Select the specific Reference No. to perform further operation.")
            Language.setMessage(mstrModuleName, 17, "Shortlisting Criteria for Reference# :-")
            Language.setMessage(mstrModuleName, 18, "Vacancy Type:-")
            Language.setMessage(mstrModuleName, 19, "Vacancy Name:-")
            Language.setMessage(mstrModuleName, 20, "Notification has not been sent successfully.")
            Language.setMessage(mstrModuleName, 21, "Data Exported successfully.")
            Language.setMessage(mstrModuleName, 22, "Please tick all Reference no for Approval.")
            Language.setMessage(mstrModuleName, 23, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 24, "Dear")
			Language.setMessage(mstrModuleName, 25, "This is to notify you that the initial shortlisting of Applicants for the vacancy of")
			Language.setMessage(mstrModuleName, 26, "which is an")
			Language.setMessage(mstrModuleName, 27, "vacancy is complete.")
			Language.setMessage(mstrModuleName, 28, "The reference number(s):")
			Language.setMessage(mstrModuleName, 29, "was/were automatically generated and attached to the shortlisting process.")
			Language.setMessage(mstrModuleName, 30, "to")
			Language.setMessage(mstrModuleName, 31, "Approve/Disapprove")
			Language.setMessage(mstrModuleName, 32, "the shortlisting criteria used.")
			Language.setMessage(mstrModuleName, 33, "Please login to Aruti Recruitment or click below link")
			Language.setMessage("frmApplicantCVReport", 2, "Applicant CV Report")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

  
End Class
'Private Sub FillList()
'    Dim strSearching As String = ""
'    Dim dtList As DataTable = Nothing
'    Try

'        If User._Object.Privilege._AllowToViewShortListApplicants = True Then                'Pinkal (02-Jul-2012) -- Start

'            Dim objShortMst As New clsshortlist_master
'            Dim dsList As DataSet = objShortMst.GetList("List", True)

'            If CInt(cboRefNo.SelectedValue) > 0 Then
'                strSearching = "AND refno = '" & cboRefNo.Text.Trim.ToString() & "' "
'            End If

'            If CInt(cboVancancy.SelectedValue) > 0 Then
'                strSearching &= "AND vacancyunkid = " & CInt(cboVancancy.SelectedValue) & " "
'            End If

'            If dtpStartdate.Checked Then
'                strSearching &= "AND openingdate >= '" & dtpStartdate.Value.Date & "' "
'            End If

'            If dtEndDate.Checked Then
'                strSearching &= "AND closingdate <= '" & dtEndDate.Value.Date & "' "
'            End If


'            If strSearching.Length > 0 Then
'                strSearching = strSearching.Substring(3)
'                dtList = New DataView(dsList.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtList = dsList.Tables(0)
'            End If

'            lvShortFilter.Items.Clear()
'            Dim lvItem As ListViewItem = Nothing

'            For Each dr As DataRow In dtList.Rows
'                lvItem = New ListViewItem
'                lvItem.Tag = CInt(dr("shortlistunkid"))
'                lvItem.Text = dr("refno").ToString()
'                lvItem.SubItems.Add(dr("vacancytitle").ToString())
'                lvShortFilter.Items.Add(lvItem)
'            Next

'            If lvShortFilter.Items.Count > 18 Then
'                colhVacancy.Width = 380 - 18
'            Else
'                colhVacancy.Width = 380
'            End If

'        End If

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'    End Try
'End Sub
'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'    If lvShortFilter.SelectedItems.Count < 1 Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Applicant Filter from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'        lvShortFilter.Select()
'        Exit Sub
'    End If

'    Try
'        Dim intSelectedIndex As Integer
'        intSelectedIndex = lvShortFilter.SelectedItems(0).Index

'        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Applicant Filter?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'            Dim objShortMst As New clsshortlist_master
'            Dim frm As New frmReasonSelection
'            Dim mstrVoidReason As String = String.Empty
'            frm.displayDialog(enVoidCategoryType.APPLICANT, mstrVoidReason)
'            If mstrVoidReason.Length <= 0 Then
'                Exit Sub
'            Else
'                objShortMst._Voidreason = mstrVoidReason
'            End If
'            objShortMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'            objShortMst._Voiduserunkid = User._Object._Userunkid
'            objShortMst.Delete(CInt(lvShortFilter.SelectedItems(0).Tag))
'            lvShortFilter.SelectedItems(0).Remove()

'            If lvShortFilter.Items.Count <= 0 Then
'                Exit Try
'            End If

'            If lvShortFilter.Items.Count = intSelectedIndex Then
'                intSelectedIndex = lvShortFilter.Items.Count - 1
'                lvShortFilter.Items(intSelectedIndex).Selected = True
'                lvShortFilter.EnsureVisible(intSelectedIndex)
'            ElseIf lvShortFilter.Items.Count <> 0 Then
'                lvShortFilter.Items(intSelectedIndex).Selected = True
'                lvShortFilter.EnsureVisible(intSelectedIndex)
'            End If

'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'    End Try
'End Sub