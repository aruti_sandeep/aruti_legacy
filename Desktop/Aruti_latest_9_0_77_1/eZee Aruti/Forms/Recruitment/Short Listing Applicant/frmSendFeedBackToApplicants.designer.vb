﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSendFeedBackToApplicants
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSendFeedBackToApplicants))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objbtnOpenFile = New eZee.Common.eZeeGradientButton
        Me.rdNonShortListedApplicantEmail = New System.Windows.Forms.RadioButton
        Me.rdShortListedApplicantEmail = New System.Windows.Forms.RadioButton
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.mnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuGetShortListedFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetNonShortListedFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.lnkApplicantProcess = New System.Windows.Forms.LinkLabel
        Me.btnSendEmail = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New System.Windows.Forms.TextBox
        Me.lblFileName = New System.Windows.Forms.Label
        Me.lnkProcess = New System.Windows.Forms.LinkLabel
        Me.dgApplicant = New System.Windows.Forms.DataGridView
        Me.dgcolhApplicantCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVacancy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOpeningDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClosingDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblCaption = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.mnuOperation.SuspendLayout()
        CType(Me.dgApplicant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objbtnOpenFile)
        Me.pnlMain.Controls.Add(Me.rdNonShortListedApplicantEmail)
        Me.pnlMain.Controls.Add(Me.rdShortListedApplicantEmail)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Controls.Add(Me.txtFilePath)
        Me.pnlMain.Controls.Add(Me.lblFileName)
        Me.pnlMain.Controls.Add(Me.lnkProcess)
        Me.pnlMain.Controls.Add(Me.dgApplicant)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(651, 517)
        Me.pnlMain.TabIndex = 0
        Me.pnlMain.Tag = ""
        '
        'objbtnOpenFile
        '
        Me.objbtnOpenFile.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOpenFile.BorderSelected = False
        Me.objbtnOpenFile.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnOpenFile.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOpenFile.Location = New System.Drawing.Point(618, 47)
        Me.objbtnOpenFile.Name = "objbtnOpenFile"
        Me.objbtnOpenFile.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOpenFile.TabIndex = 305
        '
        'rdNonShortListedApplicantEmail
        '
        Me.rdNonShortListedApplicantEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdNonShortListedApplicantEmail.Location = New System.Drawing.Point(329, 12)
        Me.rdNonShortListedApplicantEmail.Name = "rdNonShortListedApplicantEmail"
        Me.rdNonShortListedApplicantEmail.Size = New System.Drawing.Size(283, 24)
        Me.rdNonShortListedApplicantEmail.TabIndex = 308
        Me.rdNonShortListedApplicantEmail.Text = "Send Email To Non-ShortListed Applicant(s)"
        Me.rdNonShortListedApplicantEmail.UseVisualStyleBackColor = True
        '
        'rdShortListedApplicantEmail
        '
        Me.rdShortListedApplicantEmail.Checked = True
        Me.rdShortListedApplicantEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdShortListedApplicantEmail.Location = New System.Drawing.Point(10, 12)
        Me.rdShortListedApplicantEmail.Name = "rdShortListedApplicantEmail"
        Me.rdShortListedApplicantEmail.Size = New System.Drawing.Size(283, 24)
        Me.rdShortListedApplicantEmail.TabIndex = 307
        Me.rdShortListedApplicantEmail.TabStop = True
        Me.rdShortListedApplicantEmail.Text = "Send Email To ShortListed Applicant(s)"
        Me.rdShortListedApplicantEmail.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnOperation)
        Me.EZeeFooter1.Controls.Add(Me.lnkApplicantProcess)
        Me.EZeeFooter1.Controls.Add(Me.btnSendEmail)
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 462)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(651, 55)
        Me.EZeeFooter1.TabIndex = 306
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.mnuOperation
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(10, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(97, 30)
        Me.btnOperation.SplitButtonMenu = Me.mnuOperation
        Me.btnOperation.TabIndex = 9
        Me.btnOperation.Text = "&Operation"
        '
        'mnuOperation
        '
        Me.mnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuGetShortListedFileFormat, Me.mnuGetNonShortListedFileFormat})
        Me.mnuOperation.Name = "mnuOperation"
        Me.mnuOperation.Size = New System.Drawing.Size(324, 48)
        '
        'mnuGetShortListedFileFormat
        '
        Me.mnuGetShortListedFileFormat.Name = "mnuGetShortListedFileFormat"
        Me.mnuGetShortListedFileFormat.Size = New System.Drawing.Size(323, 22)
        Me.mnuGetShortListedFileFormat.Text = "Get File Format For Short Listed Applicants"
        '
        'mnuGetNonShortListedFileFormat
        '
        Me.mnuGetNonShortListedFileFormat.Name = "mnuGetNonShortListedFileFormat"
        Me.mnuGetNonShortListedFileFormat.Size = New System.Drawing.Size(323, 22)
        Me.mnuGetNonShortListedFileFormat.Text = "Get File Format For Non-ShortListed Applicants"
        '
        'lnkApplicantProcess
        '
        Me.lnkApplicantProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkApplicantProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkApplicantProcess.Location = New System.Drawing.Point(170, 17)
        Me.lnkApplicantProcess.Name = "lnkApplicantProcess"
        Me.lnkApplicantProcess.Size = New System.Drawing.Size(229, 23)
        Me.lnkApplicantProcess.TabIndex = 5
        Me.lnkApplicantProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSendEmail
        '
        Me.btnSendEmail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSendEmail.BackColor = System.Drawing.Color.White
        Me.btnSendEmail.BackgroundImage = CType(resources.GetObject("btnSendEmail.BackgroundImage"), System.Drawing.Image)
        Me.btnSendEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSendEmail.BorderColor = System.Drawing.Color.Empty
        Me.btnSendEmail.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSendEmail.FlatAppearance.BorderSize = 0
        Me.btnSendEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSendEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSendEmail.ForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSendEmail.GradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendEmail.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.Location = New System.Drawing.Point(439, 13)
        Me.btnSendEmail.Name = "btnSendEmail"
        Me.btnSendEmail.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendEmail.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.Size = New System.Drawing.Size(97, 30)
        Me.btnSendEmail.TabIndex = 4
        Me.btnSendEmail.Text = "&Send Email"
        Me.btnSendEmail.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(542, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(86, 47)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(526, 21)
        Me.txtFilePath.TabIndex = 304
        '
        'lblFileName
        '
        Me.lblFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(7, 49)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(76, 17)
        Me.lblFileName.TabIndex = 303
        Me.lblFileName.Text = "File Name"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkProcess
        '
        Me.lnkProcess.AutoSize = True
        Me.lnkProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkProcess.Location = New System.Drawing.Point(22, 447)
        Me.lnkProcess.Name = "lnkProcess"
        Me.lnkProcess.Size = New System.Drawing.Size(0, 13)
        Me.lnkProcess.TabIndex = 302
        Me.lnkProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgApplicant
        '
        Me.dgApplicant.AllowUserToAddRows = False
        Me.dgApplicant.AllowUserToDeleteRows = False
        Me.dgApplicant.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgApplicant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgApplicant.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhApplicantCode, Me.dgcolhVacancy, Me.dgcolhOpeningDate, Me.dgcolhClosingDate, Me.dgcolhReason})
        Me.dgApplicant.Location = New System.Drawing.Point(10, 74)
        Me.dgApplicant.Name = "dgApplicant"
        Me.dgApplicant.ReadOnly = True
        Me.dgApplicant.RowHeadersVisible = False
        Me.dgApplicant.Size = New System.Drawing.Size(629, 379)
        Me.dgApplicant.TabIndex = 26
        '
        'dgcolhApplicantCode
        '
        Me.dgcolhApplicantCode.HeaderText = "Applicant Code"
        Me.dgcolhApplicantCode.Name = "dgcolhApplicantCode"
        Me.dgcolhApplicantCode.ReadOnly = True
        Me.dgcolhApplicantCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApplicantCode.Width = 150
        '
        'dgcolhVacancy
        '
        Me.dgcolhVacancy.HeaderText = "Vacancy"
        Me.dgcolhVacancy.Name = "dgcolhVacancy"
        Me.dgcolhVacancy.ReadOnly = True
        Me.dgcolhVacancy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhVacancy.Width = 200
        '
        'dgcolhOpeningDate
        '
        Me.dgcolhOpeningDate.HeaderText = "Opening Date"
        Me.dgcolhOpeningDate.Name = "dgcolhOpeningDate"
        Me.dgcolhOpeningDate.ReadOnly = True
        Me.dgcolhOpeningDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOpeningDate.Width = 125
        '
        'dgcolhClosingDate
        '
        Me.dgcolhClosingDate.HeaderText = "Closing Date"
        Me.dgcolhClosingDate.Name = "dgcolhClosingDate"
        Me.dgcolhClosingDate.ReadOnly = True
        Me.dgcolhClosingDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhClosingDate.Width = 125
        '
        'dgcolhReason
        '
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhReason.Width = 300
        '
        'lblCaption
        '
        Me.lblCaption.Location = New System.Drawing.Point(12, 71)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(97, 15)
        Me.lblCaption.TabIndex = 7
        Me.lblCaption.Text = "Select Company"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Applicant Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Vacancy"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Opening Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 125
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Closing Date"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 125
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Reason"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 300
        '
        'frmSendFeedBackToApplicants
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 517)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.lblCaption)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSendFeedBackToApplicants"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Send Feedback to Applicant(s)"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.mnuOperation.ResumeLayout(False)
        CType(Me.dgApplicant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents dgApplicant As System.Windows.Forms.DataGridView
    Friend WithEvents lnkProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnOpenFile As eZee.Common.eZeeGradientButton
    Friend WithEvents txtFilePath As System.Windows.Forms.TextBox
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSendEmail As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents lnkApplicantProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents rdShortListedApplicantEmail As System.Windows.Forms.RadioButton
    Friend WithEvents rdNonShortListedApplicantEmail As System.Windows.Forms.RadioButton
    Friend WithEvents dgcolhApplicantCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVacancy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOpeningDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClosingDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuGetShortListedFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGetNonShortListedFileFormat As System.Windows.Forms.ToolStripMenuItem

End Class
