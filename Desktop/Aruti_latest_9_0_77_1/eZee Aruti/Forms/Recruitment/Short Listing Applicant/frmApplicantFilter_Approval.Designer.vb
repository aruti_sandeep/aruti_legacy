﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantFilter_Approval
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApplicantFilter_Approval))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.dgApplicantFilter = New System.Windows.Forms.DataGridView
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReject = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.fpnlLayout = New System.Windows.Forms.FlowLayoutPanel
        Me.gbCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnl1 = New System.Windows.Forms.Panel
        Me.gbApplicantInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnl2 = New System.Windows.Forms.Panel
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.gbRemark = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVacancy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhVacId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApplicantId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCollaps = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSCVacancy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhQlGrpLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhQlGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhResultCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhRLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhRCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGPA = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGPACondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAge = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAgeCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAwardYear = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhYearCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterSkillCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterSkill = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherQuliGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherResultCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherSkillCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherSkill = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhNationality = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhExperience = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhExperienceCond = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLogicalCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhFilterunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhshortlistunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgApplicantFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.fpnlLayout.SuspendLayout()
        Me.gbCriteria.SuspendLayout()
        Me.objpnl1.SuspendLayout()
        Me.gbApplicantInfo.SuspendLayout()
        Me.objpnl2.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbRemark.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(894, 472)
        Me.pnlMain.TabIndex = 0
        '
        'dgApplicantFilter
        '
        Me.dgApplicantFilter.AllowUserToAddRows = False
        Me.dgApplicantFilter.AllowUserToDeleteRows = False
        Me.dgApplicantFilter.BackgroundColor = System.Drawing.Color.White
        Me.dgApplicantFilter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgApplicantFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgApplicantFilter.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollaps, Me.colhRefNo, Me.dgcolhSCVacancy, Me.colhQlGrpLevel, Me.colhQlGrp, Me.colhdgQualification, Me.colhResultCode, Me.colhRLevel, Me.colhRCondition, Me.colhGPA, Me.colhGPACondition, Me.colhAge, Me.colhAgeCondition, Me.colhAwardYear, Me.colhYearCondition, Me.colhGender, Me.colhFilterSkillCategory, Me.colhFilterSkill, Me.colhFilterOtherQuliGrp, Me.colhFilterOtherQualification, Me.colhFilterOtherResultCode, Me.colhFilterOtherSkillCategory, Me.colhFilterOtherSkill, Me.colhNationality, Me.colhExperience, Me.colhExperienceCond, Me.colhLogicalCondition, Me.objcolhFilterunkid, Me.objdgcolhshortlistunkid, Me.objdgcolhIsGroup})
        Me.dgApplicantFilter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgApplicantFilter.Location = New System.Drawing.Point(0, 0)
        Me.dgApplicantFilter.Name = "dgApplicantFilter"
        Me.dgApplicantFilter.ReadOnly = True
        Me.dgApplicantFilter.RowHeadersVisible = False
        Me.dgApplicantFilter.RowHeadersWidth = 25
        Me.dgApplicantFilter.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgApplicantFilter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgApplicantFilter.Size = New System.Drawing.Size(867, 164)
        Me.dgApplicantFilter.TabIndex = 463
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnApprove)
        Me.EZeeFooter1.Controls.Add(Me.btnReject)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 417)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(894, 55)
        Me.EZeeFooter1.TabIndex = 4
        '
        'btnApprove
        '
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(602, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(90, 30)
        Me.btnApprove.TabIndex = 7
        Me.btnApprove.Text = "Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnReject
        '
        Me.btnReject.BackColor = System.Drawing.Color.White
        Me.btnReject.BackgroundImage = CType(resources.GetObject("btnReject.BackgroundImage"), System.Drawing.Image)
        Me.btnReject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReject.BorderColor = System.Drawing.Color.Empty
        Me.btnReject.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReject.FlatAppearance.BorderSize = 0
        Me.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReject.ForeColor = System.Drawing.Color.Black
        Me.btnReject.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReject.GradientForeColor = System.Drawing.Color.Black
        Me.btnReject.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Location = New System.Drawing.Point(698, 13)
        Me.btnReject.Name = "btnReject"
        Me.btnReject.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Size = New System.Drawing.Size(90, 30)
        Me.btnReject.TabIndex = 5
        Me.btnReject.Text = "Disapprove"
        Me.btnReject.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(794, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'fpnlLayout
        '
        Me.fpnlLayout.AutoScroll = True
        Me.fpnlLayout.Controls.Add(Me.gbCriteria)
        Me.fpnlLayout.Controls.Add(Me.gbApplicantInfo)
        Me.fpnlLayout.Controls.Add(Me.gbRemark)
        Me.fpnlLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.fpnlLayout.Location = New System.Drawing.Point(0, 0)
        Me.fpnlLayout.Name = "fpnlLayout"
        Me.fpnlLayout.Size = New System.Drawing.Size(894, 417)
        Me.fpnlLayout.TabIndex = 5
        '
        'gbCriteria
        '
        Me.gbCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbCriteria.Checked = False
        Me.gbCriteria.CollapseAllExceptThis = False
        Me.gbCriteria.CollapsedHoverImage = CType(resources.GetObject("gbCriteria.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbCriteria.CollapsedNormalImage = CType(resources.GetObject("gbCriteria.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbCriteria.CollapsedPressedImage = CType(resources.GetObject("gbCriteria.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbCriteria.CollapseOnLoad = True
        Me.gbCriteria.Controls.Add(Me.objpnl1)
        Me.gbCriteria.ExpandedHoverImage = CType(resources.GetObject("gbCriteria.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbCriteria.ExpandedNormalImage = CType(resources.GetObject("gbCriteria.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbCriteria.ExpandedPressedImage = CType(resources.GetObject("gbCriteria.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCriteria.HeaderHeight = 25
        Me.gbCriteria.HeaderMessage = ""
        Me.gbCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCriteria.HeightOnCollapse = 0
        Me.gbCriteria.LeftTextSpace = 0
        Me.gbCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbCriteria.Name = "gbCriteria"
        Me.gbCriteria.OpenHeight = 192
        Me.gbCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCriteria.ShowBorder = True
        Me.gbCriteria.ShowCheckBox = False
        Me.gbCriteria.ShowCollapseButton = True
        Me.gbCriteria.ShowDefaultBorderColor = True
        Me.gbCriteria.ShowDownButton = False
        Me.gbCriteria.ShowHeader = True
        Me.gbCriteria.Size = New System.Drawing.Size(871, 192)
        Me.gbCriteria.TabIndex = 0
        Me.gbCriteria.Temp = 0
        Me.gbCriteria.Text = "Shortlisting Criteria"
        Me.gbCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnl1
        '
        Me.objpnl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objpnl1.Controls.Add(Me.dgApplicantFilter)
        Me.objpnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnl1.Location = New System.Drawing.Point(2, 26)
        Me.objpnl1.Name = "objpnl1"
        Me.objpnl1.Size = New System.Drawing.Size(867, 164)
        Me.objpnl1.TabIndex = 464
        '
        'gbApplicantInfo
        '
        Me.gbApplicantInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.Checked = False
        Me.gbApplicantInfo.CollapseAllExceptThis = False
        Me.gbApplicantInfo.CollapsedHoverImage = CType(resources.GetObject("gbApplicantInfo.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbApplicantInfo.CollapsedNormalImage = CType(resources.GetObject("gbApplicantInfo.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbApplicantInfo.CollapsedPressedImage = CType(resources.GetObject("gbApplicantInfo.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbApplicantInfo.CollapseOnLoad = True
        Me.gbApplicantInfo.Controls.Add(Me.objpnl2)
        Me.gbApplicantInfo.ExpandedHoverImage = CType(resources.GetObject("gbApplicantInfo.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbApplicantInfo.ExpandedNormalImage = CType(resources.GetObject("gbApplicantInfo.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbApplicantInfo.ExpandedPressedImage = CType(resources.GetObject("gbApplicantInfo.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbApplicantInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplicantInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApplicantInfo.HeaderHeight = 25
        Me.gbApplicantInfo.HeaderMessage = ""
        Me.gbApplicantInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbApplicantInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.HeightOnCollapse = 0
        Me.gbApplicantInfo.LeftTextSpace = 0
        Me.gbApplicantInfo.Location = New System.Drawing.Point(3, 201)
        Me.gbApplicantInfo.Name = "gbApplicantInfo"
        Me.gbApplicantInfo.OpenHeight = 223
        Me.gbApplicantInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApplicantInfo.ShowBorder = True
        Me.gbApplicantInfo.ShowCheckBox = False
        Me.gbApplicantInfo.ShowCollapseButton = True
        Me.gbApplicantInfo.ShowDefaultBorderColor = True
        Me.gbApplicantInfo.ShowDownButton = False
        Me.gbApplicantInfo.ShowHeader = True
        Me.gbApplicantInfo.Size = New System.Drawing.Size(871, 223)
        Me.gbApplicantInfo.TabIndex = 1
        Me.gbApplicantInfo.Temp = 0
        Me.gbApplicantInfo.Text = "Applicant Info"
        Me.gbApplicantInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnl2
        '
        Me.objpnl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objpnl2.Controls.Add(Me.picStayView)
        Me.objpnl2.Controls.Add(Me.dgvData)
        Me.objpnl2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnl2.Location = New System.Drawing.Point(2, 26)
        Me.objpnl2.Name = "objpnl2"
        Me.objpnl2.Size = New System.Drawing.Size(867, 195)
        Me.objpnl2.TabIndex = 3
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(664, 198)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 37
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.dgcolhRefNo, Me.dgcolhVacancy, Me.objdgcolhVacId, Me.objdgcolhStatusId, Me.objdgcolhApplicantId, Me.objdgcolhIsGrp, Me.objdgcolhGrpId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(867, 195)
        Me.dgvData.TabIndex = 36
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.Width = 25
        '
        'gbRemark
        '
        Me.gbRemark.BorderColor = System.Drawing.Color.Black
        Me.gbRemark.Checked = False
        Me.gbRemark.CollapseAllExceptThis = False
        Me.gbRemark.CollapsedHoverImage = CType(resources.GetObject("gbRemark.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbRemark.CollapsedNormalImage = CType(resources.GetObject("gbRemark.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbRemark.CollapsedPressedImage = CType(resources.GetObject("gbRemark.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbRemark.CollapseOnLoad = True
        Me.gbRemark.Controls.Add(Me.Panel1)
        Me.gbRemark.ExpandedHoverImage = CType(resources.GetObject("gbRemark.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbRemark.ExpandedNormalImage = CType(resources.GetObject("gbRemark.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbRemark.ExpandedPressedImage = CType(resources.GetObject("gbRemark.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRemark.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRemark.HeaderHeight = 25
        Me.gbRemark.HeaderMessage = ""
        Me.gbRemark.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbRemark.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRemark.HeightOnCollapse = 0
        Me.gbRemark.LeftTextSpace = 0
        Me.gbRemark.Location = New System.Drawing.Point(3, 430)
        Me.gbRemark.Name = "gbRemark"
        Me.gbRemark.OpenHeight = 83
        Me.gbRemark.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRemark.ShowBorder = True
        Me.gbRemark.ShowCheckBox = False
        Me.gbRemark.ShowCollapseButton = True
        Me.gbRemark.ShowDefaultBorderColor = True
        Me.gbRemark.ShowDownButton = False
        Me.gbRemark.ShowHeader = True
        Me.gbRemark.Size = New System.Drawing.Size(871, 83)
        Me.gbRemark.TabIndex = 2
        Me.gbRemark.Temp = 0
        Me.gbRemark.Text = "Remark"
        Me.gbRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.txtRemark)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(2, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(867, 55)
        Me.Panel1.TabIndex = 3
        '
        'txtRemark
        '
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.ShortcutsEnabled = False
        Me.txtRemark.Size = New System.Drawing.Size(867, 55)
        Me.txtRemark.TabIndex = 0
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhRefNo.HeaderText = "Reference No"
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        Me.dgcolhRefNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhVacancy
        '
        Me.dgcolhVacancy.HeaderText = "Vacancy"
        Me.dgcolhVacancy.Name = "dgcolhVacancy"
        Me.dgcolhVacancy.ReadOnly = True
        Me.dgcolhVacancy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhVacancy.Width = 270
        '
        'objdgcolhVacId
        '
        Me.objdgcolhVacId.HeaderText = "VacId"
        Me.objdgcolhVacId.Name = "objdgcolhVacId"
        Me.objdgcolhVacId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhVacId.Visible = False
        '
        'objdgcolhStatusId
        '
        Me.objdgcolhStatusId.HeaderText = "StatusId"
        Me.objdgcolhStatusId.Name = "objdgcolhStatusId"
        Me.objdgcolhStatusId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhStatusId.Visible = False
        '
        'objdgcolhApplicantId
        '
        Me.objdgcolhApplicantId.HeaderText = "ApplicantId"
        Me.objdgcolhApplicantId.Name = "objdgcolhApplicantId"
        Me.objdgcolhApplicantId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApplicantId.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhCollaps
        '
        Me.objdgcolhCollaps.HeaderText = ""
        Me.objdgcolhCollaps.Name = "objdgcolhCollaps"
        Me.objdgcolhCollaps.ReadOnly = True
        Me.objdgcolhCollaps.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhCollaps.Width = 25
        '
        'colhRefNo
        '
        Me.colhRefNo.HeaderText = "Reference No"
        Me.colhRefNo.Name = "colhRefNo"
        Me.colhRefNo.ReadOnly = True
        Me.colhRefNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhRefNo.Width = 130
        '
        'dgcolhSCVacancy
        '
        Me.dgcolhSCVacancy.HeaderText = "Vacancy"
        Me.dgcolhSCVacancy.Name = "dgcolhSCVacancy"
        Me.dgcolhSCVacancy.ReadOnly = True
        Me.dgcolhSCVacancy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSCVacancy.Width = 150
        '
        'colhQlGrpLevel
        '
        Me.colhQlGrpLevel.HeaderText = "Ql.Grp Level"
        Me.colhQlGrpLevel.Name = "colhQlGrpLevel"
        Me.colhQlGrpLevel.ReadOnly = True
        Me.colhQlGrpLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhQlGrpLevel.Width = 75
        '
        'colhQlGrp
        '
        Me.colhQlGrp.HeaderText = "Qual. Group"
        Me.colhQlGrp.Name = "colhQlGrp"
        Me.colhQlGrp.ReadOnly = True
        Me.colhQlGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhQlGrp.Width = 150
        '
        'colhdgQualification
        '
        Me.colhdgQualification.HeaderText = "Qualification"
        Me.colhdgQualification.Name = "colhdgQualification"
        Me.colhdgQualification.ReadOnly = True
        Me.colhdgQualification.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhdgQualification.Width = 150
        '
        'colhResultCode
        '
        Me.colhResultCode.HeaderText = "Result Code"
        Me.colhResultCode.Name = "colhResultCode"
        Me.colhResultCode.ReadOnly = True
        Me.colhResultCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhResultCode.Width = 75
        '
        'colhRLevel
        '
        Me.colhRLevel.HeaderText = "Result Level"
        Me.colhRLevel.Name = "colhRLevel"
        Me.colhRLevel.ReadOnly = True
        Me.colhRLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhRLevel.Width = 75
        '
        'colhRCondition
        '
        Me.colhRCondition.HeaderText = "Result Level Condition"
        Me.colhRCondition.Name = "colhRCondition"
        Me.colhRCondition.ReadOnly = True
        Me.colhRCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhRCondition.Width = 125
        '
        'colhGPA
        '
        Me.colhGPA.HeaderText = "GPA"
        Me.colhGPA.Name = "colhGPA"
        Me.colhGPA.ReadOnly = True
        Me.colhGPA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhGPA.Width = 60
        '
        'colhGPACondition
        '
        Me.colhGPACondition.HeaderText = "GPA Condition"
        Me.colhGPACondition.Name = "colhGPACondition"
        Me.colhGPACondition.ReadOnly = True
        Me.colhGPACondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhAge
        '
        Me.colhAge.HeaderText = "Age"
        Me.colhAge.Name = "colhAge"
        Me.colhAge.ReadOnly = True
        Me.colhAge.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhAge.Width = 60
        '
        'colhAgeCondition
        '
        Me.colhAgeCondition.HeaderText = "Age Condition"
        Me.colhAgeCondition.Name = "colhAgeCondition"
        Me.colhAgeCondition.ReadOnly = True
        Me.colhAgeCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhAwardYear
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.colhAwardYear.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhAwardYear.HeaderText = "Awarded Year"
        Me.colhAwardYear.Name = "colhAwardYear"
        Me.colhAwardYear.ReadOnly = True
        Me.colhAwardYear.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'colhYearCondition
        '
        Me.colhYearCondition.HeaderText = "Year Condition"
        Me.colhYearCondition.Name = "colhYearCondition"
        Me.colhYearCondition.ReadOnly = True
        Me.colhYearCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'colhGender
        '
        Me.colhGender.HeaderText = "Gender"
        Me.colhGender.Name = "colhGender"
        Me.colhGender.ReadOnly = True
        Me.colhGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhGender.Width = 75
        '
        'colhFilterSkillCategory
        '
        Me.colhFilterSkillCategory.HeaderText = "SKill Category"
        Me.colhFilterSkillCategory.Name = "colhFilterSkillCategory"
        Me.colhFilterSkillCategory.ReadOnly = True
        Me.colhFilterSkillCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterSkillCategory.Width = 150
        '
        'colhFilterSkill
        '
        Me.colhFilterSkill.HeaderText = "Skill"
        Me.colhFilterSkill.Name = "colhFilterSkill"
        Me.colhFilterSkill.ReadOnly = True
        Me.colhFilterSkill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterSkill.Width = 125
        '
        'colhFilterOtherQuliGrp
        '
        Me.colhFilterOtherQuliGrp.HeaderText = "Other Qual. Group"
        Me.colhFilterOtherQuliGrp.Name = "colhFilterOtherQuliGrp"
        Me.colhFilterOtherQuliGrp.ReadOnly = True
        Me.colhFilterOtherQuliGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterOtherQuliGrp.Width = 150
        '
        'colhFilterOtherQualification
        '
        Me.colhFilterOtherQualification.HeaderText = "Other Qualification"
        Me.colhFilterOtherQualification.Name = "colhFilterOtherQualification"
        Me.colhFilterOtherQualification.ReadOnly = True
        Me.colhFilterOtherQualification.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterOtherQualification.Width = 150
        '
        'colhFilterOtherResultCode
        '
        Me.colhFilterOtherResultCode.HeaderText = "Other Result Code"
        Me.colhFilterOtherResultCode.Name = "colhFilterOtherResultCode"
        Me.colhFilterOtherResultCode.ReadOnly = True
        Me.colhFilterOtherResultCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterOtherResultCode.Width = 150
        '
        'colhFilterOtherSkillCategory
        '
        Me.colhFilterOtherSkillCategory.HeaderText = "Other Skill Category"
        Me.colhFilterOtherSkillCategory.Name = "colhFilterOtherSkillCategory"
        Me.colhFilterOtherSkillCategory.ReadOnly = True
        Me.colhFilterOtherSkillCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterOtherSkillCategory.Width = 150
        '
        'colhFilterOtherSkill
        '
        Me.colhFilterOtherSkill.HeaderText = "Other Skill"
        Me.colhFilterOtherSkill.Name = "colhFilterOtherSkill"
        Me.colhFilterOtherSkill.ReadOnly = True
        Me.colhFilterOtherSkill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterOtherSkill.Width = 150
        '
        'colhNationality
        '
        Me.colhNationality.HeaderText = "Nationality"
        Me.colhNationality.Name = "colhNationality"
        Me.colhNationality.ReadOnly = True
        '
        'colhExperience
        '
        Me.colhExperience.HeaderText = "Experience"
        Me.colhExperience.Name = "colhExperience"
        Me.colhExperience.ReadOnly = True
        '
        'colhExperienceCond
        '
        Me.colhExperienceCond.HeaderText = "Experience Condition"
        Me.colhExperienceCond.Name = "colhExperienceCond"
        Me.colhExperienceCond.ReadOnly = True
        Me.colhExperienceCond.Width = 150
        '
        'colhLogicalCondition
        '
        Me.colhLogicalCondition.HeaderText = "Logical Condition"
        Me.colhLogicalCondition.Name = "colhLogicalCondition"
        Me.colhLogicalCondition.ReadOnly = True
        Me.colhLogicalCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhLogicalCondition.Width = 125
        '
        'objcolhFilterunkid
        '
        Me.objcolhFilterunkid.HeaderText = "Filterunkid"
        Me.objcolhFilterunkid.Name = "objcolhFilterunkid"
        Me.objcolhFilterunkid.ReadOnly = True
        Me.objcolhFilterunkid.Visible = False
        '
        'objdgcolhshortlistunkid
        '
        Me.objdgcolhshortlistunkid.HeaderText = "shortlistunkid"
        Me.objdgcolhshortlistunkid.Name = "objdgcolhshortlistunkid"
        Me.objdgcolhshortlistunkid.ReadOnly = True
        Me.objdgcolhshortlistunkid.Visible = False
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = ""
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Visible = False
        '
        'frmApplicantFilter_Approval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 472)
        Me.Controls.Add(Me.fpnlLayout)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantFilter_Approval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approve/Disapprove Applicant Filter"
        CType(Me.dgApplicantFilter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.fpnlLayout.ResumeLayout(False)
        Me.gbCriteria.ResumeLayout(False)
        Me.objpnl1.ResumeLayout(False)
        Me.gbApplicantInfo.ResumeLayout(False)
        Me.objpnl2.ResumeLayout(False)
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbRemark.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnReject As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgApplicantFilter As System.Windows.Forms.DataGridView
    Friend WithEvents fpnlLayout As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents gbCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objpnl1 As System.Windows.Forms.Panel
    Friend WithEvents gbApplicantInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objpnl2 As System.Windows.Forms.Panel
    Friend WithEvents gbRemark As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVacancy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhVacId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApplicantId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCollaps As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSCVacancy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhQlGrpLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhQlGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhResultCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhRLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhRCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGPA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGPACondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAge As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAgeCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAwardYear As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhYearCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterSkillCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterSkill As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherQuliGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherResultCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherSkillCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherSkill As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhNationality As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhExperience As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhExperienceCond As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLogicalCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhFilterunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhshortlistunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
