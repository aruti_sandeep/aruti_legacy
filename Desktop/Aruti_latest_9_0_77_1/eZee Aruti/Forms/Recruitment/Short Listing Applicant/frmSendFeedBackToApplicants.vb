﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region


Public Class frmSendFeedBackToApplicants

    Private ReadOnly mstrModuleName As String = "frmSendFeedBackToApplicants"

#Region " Private Variable(s) "

    Private mblnCancel As Boolean = True
    Dim dsList As DataSet = Nothing
    Dim dtView As DataView = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmSendFeedBackToApplicants_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            rdShortListedApplicantEmail.Checked = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSendFeedBackToApplicants_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Public Function Export_ErrorList(ByVal StrPath As String, ByVal objDataReader As DataTable, ByVal strSourceName As String) As Boolean
        Dim strBuilder As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            Dim dsError As New DataSet()
            dsError.Tables.Add(objDataReader.Copy())
            If Aruti.Data.modGlobal.OpenXML_Export(strFilePath:=StrPath, ds:=dsError) = 0 Then
                blnFlag = True
            End If
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_ErrorList", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Button's Events "

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try

            If rdNonShortListedApplicantEmail.Checked = False AndAlso rdShortListedApplicantEmail.Checked = False Then
                eZeeMsgBox.Show("Please Select Atleast one option to do importation.", enMsgBoxStyle.Information)
                Exit Sub
            End If


            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then

                Cursor = Cursors.WaitCursor
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                dsList = OpenXML_Import(txtFilePath.Text)


                dgApplicant.AutoGenerateColumns = False

                dgcolhApplicantCode.DataPropertyName = "App.Code"
                dgcolhVacancy.DataPropertyName = "Vacancy"
                dgcolhOpeningDate.DataPropertyName = "openingdate"
                dgcolhClosingDate.DataPropertyName = "closingdate"
                dgcolhReason.DataPropertyName = "Remark"

                dgApplicant.DataSource = dsList.Tables(0)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnExportFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSendEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click

        Try
            If dsList Is Nothing OrElse dsList.Tables(0).Rows.Count <= 0 Then
                MsgBox("There is no record to send email(s) to applicants.", MsgBoxStyle.Information)
                Exit Sub
            End If



            If dsList.Tables(0).Columns.Contains("Comp_Code") = False Then
                Dim dcColumn As New DataColumn("Comp_Code")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("companyunkid") = False Then
                Dim dcColumn As New DataColumn("companyunkid")
                dcColumn.DataType = Type.GetType("System.Int32")
                dcColumn.DefaultValue = 0
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("sender_address") = False Then
                Dim dcColumn As New DataColumn("sender_address")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("recipient_address") = False Then
                Dim dcColumn As New DataColumn("recipient_address")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("send_datetime") = False Then
                Dim dcColumn As New DataColumn("send_datetime")
                dcColumn.DataType = Type.GetType("System.DateTime")
                dcColumn.DefaultValue = DBNull.Value
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("sent_data") = False Then
                Dim dcColumn As New DataColumn("sent_data")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("subject") = False Then
                Dim dcColumn As New DataColumn("subject")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("vacancytypeunkid") = False Then
                Dim dcColumn As New DataColumn("vacancytypeunkid")
                dcColumn.DataType = Type.GetType("System.Int32")
                dcColumn.DefaultValue = 0
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("vacancyunkid") = False Then
                Dim dcColumn As New DataColumn("vacancyunkid")
                dcColumn.DataType = Type.GetType("System.Int32")
                dcColumn.DefaultValue = 0
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("viewtypeunkid") = False Then
                Dim dcColumn As New DataColumn("viewtypeunkid")
                dcColumn.DataType = Type.GetType("System.Int32")
                dcColumn.DefaultValue = 0
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("isvoid") = False Then
                Dim dcColumn As New DataColumn("isvoid")
                dcColumn = New DataColumn("isvoid")
                dcColumn.DataType = Type.GetType("System.Boolean")
                dcColumn.DefaultValue = False
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("voiddatetime") = False Then
                Dim dcColumn As New DataColumn("voiddatetime")
                dcColumn = New DataColumn("voiddatetime")
                dcColumn.DataType = Type.GetType("System.DateTime")
                dcColumn.DefaultValue = DBNull.Value
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("voidreason") = False Then
                Dim dcColumn As New DataColumn("voidreason")
                dcColumn = New DataColumn("voidreason")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("iserror") = False Then
                Dim dcColumn As New DataColumn("iserror")
                dcColumn.DataType = Type.GetType("System.Boolean")
                dcColumn.DefaultValue = False
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If

            If dsList.Tables(0).Columns.Contains("Message") = False Then
                Dim dcColumn As New DataColumn("Message")
                dcColumn = New DataColumn("Message")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dsList.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing
            End If


            Me.Cursor = Cursors.WaitCursor

            Dim mstrCompany As String = ""

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            mstrCompany = info1.ToTitleCase(Company._Object._Name.ToString())

            Me.ControlBox = False
            Me.Enabled = False

            Dim xCountApplicant As Integer = 0
            Dim objApplicant As New clsApplicant_master
            Dim objCommon As New clsCommon_Master
            Dim objVacancy As New clsVacancy

            For Each dr As DataRow In dsList.Tables(0).Rows

                Try
                    Application.DoEvents()
                    dgApplicant.FirstDisplayedScrollingRowIndex = dsList.Tables(0).Rows.IndexOf(dr) - 12
                Catch ex As Exception

                End Try

                lnkApplicantProcess.Text = "Applicant(s) Email Sent : " & xCountApplicant + 1 & "/" & dsList.Tables(0).Rows.Count


                Dim xVacancyTitleId As Integer = 0
                Dim xVacancyId As Integer = 0
                Dim xVacancyTypeId As Integer = 0

                '* START GETTING VACANCY TITLE ID
                Dim dsCommon As DataSet = objCommon.GetList(clsCommon_Master.enCommonMaster.VACANCY_MASTER, "List", -1, True, "AND cfcommon_master.name = '" & dr("Vacancy").ToString().Trim() & "'")

                If dsCommon IsNot Nothing AndAlso dsCommon.Tables(0).Rows.Count > 0 Then
                    xVacancyTitleId = CInt(dsCommon.Tables(0).Rows(0)("masterunkid"))
                Else
                    dgApplicant.Rows(dsList.Tables(0).Rows.IndexOf(dr)).DefaultCellStyle.ForeColor = Color.Red
                    dr("Message") = Language.getMessage(mstrModuleName, 1, "Vacancy Not Found")
                    dr("iserror") = True
                    Continue For
                End If
                '* END GETTING VACANCY TITLE ID

                If xVacancyTitleId > 0 Then

                    '* START GETTING VACANCY 
                    Dim dsVacancy As DataSet = objVacancy.GetList("List", True, "rcvacancy_master.vacancytitle = " & xVacancyTitleId & " AND CONVERT(CHAR(8),rcvacancy_master.openingdate,112) = '" & eZeeDate.convertDate(CDate(dr("Openingdate").ToString())) & _
                                                              "' AND CONVERT(CHAR(8),rcvacancy_master.closingdate,112) = '" & eZeeDate.convertDate(CDate(dr("Closingdate").ToString())) & "'")

                    If dsVacancy IsNot Nothing AndAlso dsVacancy.Tables(0).Rows.Count > 0 Then
                        xVacancyId = CInt(dsVacancy.Tables(0).Rows(0)("vacancyunkid"))
                        dr("vacancyunkid") = xVacancyId
                    Else
                        dgApplicant.Rows(dsList.Tables(0).Rows.IndexOf(dr)).DefaultCellStyle.ForeColor = Color.Red
                        dr("Message") = Language.getMessage(mstrModuleName, 2, "Vacancy Not Found.Invalid Opening Date or Closing Date.")
                        dr("iserror") = True
                        Continue For
                    End If
                    '* END GETTING VACANCY 


                    '* START GETTING VACANCY TYPE
                    objVacancy._Vacancyunkid = xVacancyId
                    If objVacancy._Is_External_Vacancy AndAlso objVacancy._Isbothintext = False Then
                        xVacancyTypeId = enVacancyType.EXTERNAL_VACANCY
                    ElseIf objVacancy._Is_External_Vacancy = False AndAlso objVacancy._Isbothintext = False Then
                        xVacancyTypeId = enVacancyType.INTERNAL_VACANCY
                    ElseIf objVacancy._Isbothintext Then
                        xVacancyTypeId = enVacancyType.INTERNAL_AND_EXTERNAL
                    End If
                    dr("vacancytypeunkid") = xVacancyTypeId
                    '* END GETTING VACANCY TYPE

                End If

                If rdNonShortListedApplicantEmail.Checked Then
                    dr("viewtypeunkid") = 2 ' Non ShortListed Applicant  CHECK ON frmCandidateFeedback.vb (line No : 132)  FillCombo Method.
                ElseIf rdShortListedApplicantEmail.Checked Then
                    dr("viewtypeunkid") = 3 'ShortListed Applicant  CHECK ON frmCandidateFeedback.vb (line No : 133)  FillCombo Method.
                End If


                objApplicant._Applicantunkid = 0
                Dim xApplicantId As Integer = 0
                Dim dsRecruitment As DataSet = objApplicant.GetList("List", True, False, "AND rcapplicant_master.applicant_code = '" & dr("App.Code").ToString().Trim() & "'")

                If dsRecruitment Is Nothing OrElse dsRecruitment.Tables(0).Rows.Count <= 0 Then Continue For

                xApplicantId = CInt(dsRecruitment.Tables(0).Rows(0)("applicantunkid"))

                objApplicant._Applicantunkid = xApplicantId

                If objApplicant._Email.Trim.Length > 0 Then
                    Dim objMail As New clsSendMail
                    Dim strMessage As String = ""

                    If rdShortListedApplicantEmail.Checked Then

                        strMessage = "<HTML> <BODY>"

                        strMessage &= "<B>Date : " & Now.Date.ToShortDateString() & "</B>"
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "<B>Dear" & " " & info1.ToTitleCase(objApplicant._Firstname.Trim().ToLower() & " " & objApplicant._Surname.Trim().ToLower()) & "</B>"
                        strMessage &= "<BR></BR><BR></BR>"

                        'Pinkal (16-Oct-2023) -- Start
                        '(A1X-1400) NMB - Post assigned employee assets to P2P
                        'strMessage &= "Kindly refer your job application with Tanzania Revenue Authority of " & "<B>" & CDate(dr("VacanyDate")).ToShortDateString() & "</B>."
                        strMessage &= "Kindly refer your job application with Tanzania Revenue Authority of " & "<B>" & CDate(dr("Openingdate")).ToShortDateString() & "</B>."
                        'Pinkal (16-Oct-2023) -- End

                        strMessage &= "<BR></BR>"
                        strMessage &= "We are glad to inform you that you have been shortlisted to attend written interview for the Post of <B>" & info1.ToTitleCase(dr("Vacancy").ToString().Trim()) & "</B>."
                        strMessage &= "<BR></BR>"
                        strMessage &= "The interview will take place on <B>" & CDate(dr("Date")).Date.ToShortDateString() & ", " & dr("Time").ToString() & "</B> at <B> " & dr("Location").ToString() & " </B> and Your Interview Number is <B>" & info1.ToTitleCase(dr("Interview Number").ToString().Trim()) & "</B>."
                        strMessage &= "<BR></BR>"
                        strMessage &= "Kindly take note that for those who will be invited for more than one position will be required to sit for only one interview.Please bring this invitation letter with you."
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "<B>For more information please visit TRA Website: https://www.tra.go.tz </B>"
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "<B>Wish you best of luck.</B>"
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "<B>''Together We Build our Nation''</B>"
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "<B>Commissioner General</B><BR></BR>"
                        strMessage &= "<B>" & mstrCompany & " </B>"
                        strMessage &= "</BODY></HTML>"

                        objMail._Subject = "Job Interview Invitation"


                    ElseIf rdNonShortListedApplicantEmail.Checked Then

                        strMessage = "<HTML> <BODY>"
                        strMessage &= "Dear" & " " & info1.ToTitleCase(objApplicant._Firstname.Trim().ToLower()) & ", <BR><BR>"

                        strMessage &= "Thank you for taking your time to apply for the position of <B>" & info1.ToTitleCase(dr("Vacancy").ToString().Trim()) & "</B> at  <B>" & mstrCompany & ".</B>"
                        strMessage &= "<BR></BR>"
                        strMessage &= "We have reviewed your application and we regret to inform you that you have not been shortlisted for the next stage of the hiring process due to the following reasons: "
                        strMessage &= "<BR></BR>"
                        strMessage &= "<B>" & dr("Remark").ToString() & "</B>"
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "Thank you once again for considering " & mstrCompany & " as a potential employer. "
                        strMessage &= "<BR></BR>"
                        strMessage &= "We wish you all the best of luck in your job search and professional endeavours."
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "<B>''Together We Build our Nation''</B>"
                        strMessage &= "<BR></BR><BR></BR>"
                        strMessage &= "<B>Commissioner General</B><BR></BR>"
                        strMessage &= "<B>" & mstrCompany & " </B>"
                        strMessage &= "</BODY></HTML>"

                        objMail._Subject = "TRA Job Application Feedback"

                    End If

                    objMail._Message = strMessage
                    objMail._SenderAddress = mstrCompany
                    objMail._ToEmail = objApplicant._Email
                    objMail._OperationModeId = enLogin_Mode.DESKTOP
                    objMail._UserUnkid = 1  'Admin
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                    objMail._VacancyId = xVacancyId
                    objMail._VacancyTypeId = xVacancyTypeId
                    objMail._VacancyViewId = CInt(dr("viewtypeunkid"))


                    Dim mstrError As String = objMail.SendMail(Company._Object._Companyunkid)

                    If mstrError.Trim.Length > 0 Then
                        dgApplicant.Rows(dsList.Tables(0).Rows.IndexOf(dr)).DefaultCellStyle.ForeColor = Color.Red
                        dr("Message") = mstrError
                        dr("iserror") = True
                    Else
                        dgApplicant.Rows(dsList.Tables(0).Rows.IndexOf(dr)).DefaultCellStyle.ForeColor = Color.Green
                        dr("Message") = ""
                        dr("iserror") = False
                    End If

                    dr("Comp_Code") = Company._Object._Code
                    dr("companyunkid") = Company._Object._Companyunkid
                    dr("sender_address") = Company._Object._Senderaddress
                    dr("recipient_address") = objApplicant._Email
                    dr("send_datetime") = Now
                    dr("sent_data") = objMail._Message
                    dr("subject") = objMail._Subject

                    xCountApplicant += 1
                    objMail = Nothing
                End If

            Next
            objApplicant = Nothing



            'START TO EXPORT THE DATA WHICH IS SUCCESSFULLY SEND TO ONLINE RECRUITMENT
            Dim dtSendFeedback As DataTable = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iserror") = False AndAlso x.Field(Of String)("Message") = "").ToList.CopyToDataTable()
            If dtSendFeedback IsNot Nothing AndAlso dtSendFeedback.Rows.Count > 0 Then

                Dim dsSendFeedback As New DataSet("Feedback")
                dsSendFeedback.Tables.Add(dtSendFeedback)
                dsSendFeedback.AcceptChanges()

                Dim objExportApplicant As New clsApplicant_master
                objExportApplicant._CompanyCode = Company._Object._Code
                objExportApplicant._CompanyName = Company._Object._Name
                objExportApplicant._DatabaseName = FinancialYear._Object._DatabaseName

                If objExportApplicant.ExportApplicantFeedbackToWeb(Company._Object._Code, ConfigParameter._Object._WebClientCode, ConfigParameter._Object._DatabaseServer _
                                                                                             , ConfigParameter._Object._DatabaseServerSetting.ToString(), ConfigParameter._Object._AuthenticationCode _
                                                                                             , Company._Object._ConfigDatabaseName, ConfigParameter._Object._RecruitmentWebServiceLink, dsSendFeedback) = False Then
                    eZeeMsgBox.Show(objApplicant._Message, enMsgBoxStyle.Information)
                End If
                objExportApplicant = Nothing
            End If
            'END TO EXPORT THE DATA WHICH IS SUCCESSFULLY SEND TO ONLINE RECRUITMENT




            Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iserror") = True)
            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                eZeeMsgBox.Show("Some of emails didn't able to send.Please check the exported file for the error(s).", CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))

                Dim savDialog As New SaveFileDialog
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    If Export_ErrorList(savDialog.FileName, drRow.CopyToDataTable(), "Email Sending Error") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            Else
                eZeeMsgBox.Show("Email(s) sent successfully to all applicants.", CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSendEmail_Click", mstrModuleName)
        Finally
            Cursor = Cursors.Default()
            Me.ControlBox = True
            Me.Enabled = True
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region


#Region "Context Menu Events"

    Private Sub mnuGetShortListedFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetShortListedFileFormat.Click
        Dim objShortListingMst As New clsshortlist_master
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim dsList As DataSet = objShortListingMst.GetFileStrctureForSendFeedBackToApplicants(True)
                OpenXML_Export(objSave.FileName, dsList)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetShortListedFileFormat_Click", mstrModuleName)
        Finally
            objShortListingMst = Nothing
        End Try
    End Sub

    Private Sub mnuGetNonShortListedFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetNonShortListedFileFormat.Click
        Dim objShortListingMst As New clsshortlist_master
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim dsList As DataSet = objShortListingMst.GetFileStrctureForSendFeedBackToApplicants(False)
                OpenXML_Export(objSave.FileName, dsList)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetNonShortListedFileFormat_Click", mstrModuleName)
        Finally
            objShortListingMst = Nothing
        End Try
    End Sub

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnSendEmail.GradientBackColor = GUI._ButttonBackColor
            Me.btnSendEmail.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lnkProcess.Text = Language._Object.getCaption(Me.lnkProcess.Name, Me.lnkProcess.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnSendEmail.Text = Language._Object.getCaption(Me.btnSendEmail.Name, Me.btnSendEmail.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.dgcolhApplicantCode.HeaderText = Language._Object.getCaption(Me.dgcolhApplicantCode.Name, Me.dgcolhApplicantCode.HeaderText)
            Me.dgcolhVacancy.HeaderText = Language._Object.getCaption(Me.dgcolhVacancy.Name, Me.dgcolhVacancy.HeaderText)
            Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)
            Me.lnkApplicantProcess.Text = Language._Object.getCaption(Me.lnkApplicantProcess.Name, Me.lnkApplicantProcess.Text)
            Me.rdShortListedApplicantEmail.Text = Language._Object.getCaption(Me.rdShortListedApplicantEmail.Name, Me.rdShortListedApplicantEmail.Text)
            Me.rdNonShortListedApplicantEmail.Text = Language._Object.getCaption(Me.rdNonShortListedApplicantEmail.Name, Me.rdNonShortListedApplicantEmail.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
   
End Class

