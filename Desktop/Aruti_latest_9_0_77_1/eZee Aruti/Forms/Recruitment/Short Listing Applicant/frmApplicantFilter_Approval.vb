﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports System.Text
Imports System.IO

#End Region

Public Class frmApplicantFilter_Approval
    Private ReadOnly mstrModuleName As String = "frmApplicantFilter_Approval"

#Region " Private Variable(s) "

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrShortFilterId As String = ""
    Dim mSize As Size
    Private mintVacancyId As Integer = 0
    Private eSCType As enShortListing_Status
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal _ShortFilterID As String, ByVal _eScType As enShortListing_Status, ByVal _VacancyId As Integer) As Boolean
        Try
            mstrShortFilterId = _ShortFilterID
            mintVacancyId = _VacancyId
            eSCType = _eScType
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub GetApplicantFilter()
        Try
            Dim objApplicantFilter As New clsshortlist_filter
            Dim dsList As DataSet = objApplicantFilter.GetList("Filter", True)

            Dim mdtApplicantFilter As DataTable = New DataView(dsList.Tables(0), "shortlistunkid IN (" & mstrShortFilterId & ")", "", DataViewRowState.CurrentRows).ToTable
            Dim mdtTable As DataTable = mdtApplicantFilter.Clone


            If mdtTable.Columns.Contains("IsGroup") = False Then
                mdtTable.Columns.Add("IsGroup", Type.GetType("System.Boolean"))
            End If

            If mdtTable.Columns.Contains("qualification") = False Then
                mdtTable.Columns.Add("qualification", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("skill_category") = False Then
                mdtTable.Columns.Add("skill_category", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("skill") = False Then
                mdtTable.Columns.Add("skill", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("ResultLvl_Condition") = False Then
                mdtTable.Columns.Add("ResultLvl_Condition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("gpa_condition") = False Then
                mdtTable.Columns.Add("gpa_condition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("agecondition") = False Then
                mdtTable.Columns.Add("agecondition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("year_condition") = False Then
                mdtTable.Columns.Add("year_condition", Type.GetType("System.String"))
            End If

            If mdtTable.Columns.Contains("logicalcondition") = False Then
                mdtTable.Columns.Add("logicalcondition", Type.GetType("System.String"))
            End If

            Dim mintShortListID As Integer = -1
            Dim drRow As DataRow = Nothing

            Dim objQualification As New clsqualification_master
            Dim objSkill As New clsskill_master
            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'Dim dsCondtion As DataSet = objMaster.GetCondition(False)
            Dim dsCondtion As DataSet = objMaster.GetCondition(False, True, False, False, False)
            'Nilay (10-Nov-2016) -- End

            For i As Integer = 0 To mdtApplicantFilter.Rows.Count - 1

                If mintShortListID <> CInt(mdtApplicantFilter.Rows(i)("shortlistunkid")) Then
                    drRow = mdtTable.NewRow()
                    mintShortListID = CInt(mdtApplicantFilter.Rows(i)("shortlistunkid"))
                    drRow("shortlistunkid") = CInt(mdtApplicantFilter.Rows(i)("shortlistunkid"))
                    drRow("filterunkid") = CInt(mdtApplicantFilter.Rows(i)("filterunkid"))
                    drRow("refno") = mdtApplicantFilter.Rows(i)("refno").ToString()
                    drRow("vacancytitle") = mdtApplicantFilter.Rows(i)("vacancytitle").ToString()
                    drRow("qlevel") = DBNull.Value
                    drRow("qualificationgroup") = DBNull.Value
                    drRow("qualification") = DBNull.Value
                    drRow("resultname") = DBNull.Value
                    drRow("resultlevel") = DBNull.Value
                    drRow("result_lvl_condition") = DBNull.Value
                    drRow("gpacode") = DBNull.Value
                    drRow("gpa_condition") = DBNull.Value
                    drRow("age") = DBNull.Value
                    drRow("agecondition") = DBNull.Value
                    drRow("award_year") = DBNull.Value
                    drRow("year_condition") = DBNull.Value
                    drRow("gender_name") = DBNull.Value
                    drRow("skill_category") = DBNull.Value
                    drRow("skill") = DBNull.Value
                    drRow("other_qualificationgrp") = DBNull.Value
                    drRow("other_qualification") = DBNull.Value
                    drRow("other_resultcode") = DBNull.Value
                    drRow("other_skillcategory") = DBNull.Value
                    drRow("other_skill") = DBNull.Value
                    drRow("logicalcondition") = DBNull.Value
                    drRow("IsGroup") = True
                    mdtTable.Rows.Add(drRow)
                End If

                drRow = mdtTable.NewRow()
                drRow("shortlistunkid") = CInt(mdtApplicantFilter.Rows(i)("shortlistunkid"))
                drRow("filterunkid") = CInt(mdtApplicantFilter.Rows(i)("filterunkid"))
                drRow("refno") = ""
                drRow("vacancytitle") = ""
                drRow("qlevel") = CInt(mdtApplicantFilter.Rows(i)("qlevel").ToString())
                drRow("qualificationgroup") = mdtApplicantFilter.Rows(i)("qualificationgroup").ToString()
                If mdtApplicantFilter.Rows(i)("qualificationunkid").ToString().Length > 0 Then
                    drRow("qualification") = objQualification.GetQualification(mdtApplicantFilter.Rows(i)("qualificationunkid").ToString())
                Else
                    drRow("qualification") = ""
                End If
                drRow("resultname") = mdtApplicantFilter.Rows(i)("resultname").ToString()
                drRow("resultlevel") = mdtApplicantFilter.Rows(i)("resultlevel").ToString()

                If CInt(mdtApplicantFilter.Rows(i)("result_lvl_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("result_lvl_condition")))
                    If dr.Length > 0 Then
                        drRow("ResultLvl_Condition") = dr(0)("Name").ToString()
                    Else
                        drRow("ResultLvl_Condition") = ""
                    End If
                Else
                    drRow("ResultLvl_Condition") = ""
                End If

                drRow("gpacode") = CDec(mdtApplicantFilter.Rows(i)("gpacode"))

                If CInt(mdtApplicantFilter.Rows(i)("gpacode_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("gpacode_condition")))
                    If dr.Length > 0 Then
                        drRow("gpa_condition") = dr(0)("Name").ToString()
                    Else
                        drRow("gpa_condition") = ""
                    End If
                Else
                    drRow("gpa_condition") = ""
                End If

                drRow("age") = CInt(mdtApplicantFilter.Rows(i)("age"))

                If CInt(mdtApplicantFilter.Rows(i)("age_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("age_condition")))
                    If dr.Length > 0 Then
                        drRow("agecondition") = dr(0)("Name").ToString()
                    Else
                        drRow("agecondition") = ""
                    End If
                Else
                    drRow("agecondition") = ""
                End If

                drRow("award_year") = mdtApplicantFilter.Rows(i)("award_year").ToString()

                If CInt(mdtApplicantFilter.Rows(i)("award_year_condition")) > 0 Then
                    Dim dr() As DataRow = dsCondtion.Tables(0).Select("id = " & CInt(mdtApplicantFilter.Rows(i)("award_year_condition")))
                    If dr.Length > 0 Then
                        drRow("year_condition") = dr(0)("Name").ToString()
                    Else
                        drRow("year_condition") = ""
                    End If
                Else
                    drRow("year_condition") = ""
                End If

                drRow("gender_name") = mdtApplicantFilter.Rows(i)("gender_name").ToString()

                If mdtApplicantFilter.Rows(i)("skillcategoryunkid").ToString().Length > 0 Then
                    drRow("skill_category") = objSkill.GetSkillCategory(mdtApplicantFilter.Rows(i)("skillcategoryunkid").ToString())
                Else
                    drRow("skill_category") = ""
                End If

                If mdtApplicantFilter.Rows(i)("skillunkid").ToString().Length > 0 Then
                    drRow("skill") = objSkill.GetSkill(mdtApplicantFilter.Rows(i)("skillunkid").ToString())
                Else
                    drRow("skill") = ""
                End If
                drRow("other_qualificationgrp") = mdtApplicantFilter.Rows(i)("other_qualificationgrp").ToString()
                drRow("other_qualification") = mdtApplicantFilter.Rows(i)("other_qualification").ToString()
                drRow("other_resultcode") = mdtApplicantFilter.Rows(i)("other_resultcode").ToString()
                drRow("other_skillcategory") = mdtApplicantFilter.Rows(i)("other_skillcategory").ToString()
                drRow("other_skill") = mdtApplicantFilter.Rows(i)("other_skill").ToString()

                If CInt(mdtApplicantFilter.Rows(i)("logical_condition")) > 0 Then
                    If CInt(mdtApplicantFilter.Rows(i)("logical_condition")) = 1 Then
                        drRow("logicalcondition") = Language.getMessage(mstrModuleName, 1, "AND")
                    ElseIf CInt(mdtApplicantFilter.Rows(i)("logical_condition")) = 2 Then
                        drRow("logicalcondition") = Language.getMessage(mstrModuleName, 2, "OR")
                    Else
                        drRow("logicalcondition") = ""
                    End If
                Else
                    drRow("logicalcondition") = ""
                End If

                'Sohail (21 Apr 2020) -- Start
                'NMB Enhancement # : On shortlisting screen, the screen is returning false results when AND condition is used and combined with other different parameters. E.g qualification group AND Nationality.
                drRow("nationality") = mdtApplicantFilter.Rows(i)("nationality").ToString()
                drRow("experience_days") = mdtApplicantFilter.Rows(i)("experience_days").ToString()
                drRow("experiencecondition") = mdtApplicantFilter.Rows(i)("experiencecondition").ToString()
                'Sohail (21 Apr 2020) -- End

                drRow("IsGroup") = False
                mdtTable.Rows.Add(drRow)

            Next

            dgApplicantFilter.AutoGenerateColumns = False
            colhRefNo.DataPropertyName = "refno"
            colhQlGrpLevel.DataPropertyName = "qlevel"
            colhQlGrp.DataPropertyName = "qualificationgroup"
            colhdgQualification.DataPropertyName = "qualification"
            colhResultCode.DataPropertyName = "resultname"
            colhRLevel.DataPropertyName = "resultlevel"
            colhRCondition.DataPropertyName = "ResultLvl_Condition"
            colhGPA.DataPropertyName = "gpacode"
            colhGPACondition.DataPropertyName = "gpacode_condition"
            colhAge.DataPropertyName = "age"
            colhAgeCondition.DataPropertyName = "agecondition"
            colhAwardYear.DataPropertyName = "award_year"
            colhYearCondition.DataPropertyName = "year_condition"
            colhGender.DataPropertyName = "gender_name"
            colhFilterSkillCategory.DataPropertyName = "skill_category"
            colhFilterSkill.DataPropertyName = "skill"
            colhFilterOtherQuliGrp.DataPropertyName = "other_qualificationgrp"
            colhFilterOtherQualification.DataPropertyName = "other_qualification"
            colhFilterOtherResultCode.DataPropertyName = "other_resultcode"
            colhFilterSkillCategory.DataPropertyName = "other_skillcategory"
            colhFilterOtherSkill.DataPropertyName = "other_skill"
            colhLogicalCondition.DataPropertyName = "logicalcondition"
            objdgcolhIsGroup.DataPropertyName = "IsGroup"
            objdgcolhshortlistunkid.DataPropertyName = "shortlistunkid"
            objcolhFilterunkid.DataPropertyName = "filterunkid"
            dgcolhSCVacancy.DataPropertyName = "vacancytitle"
            'Sohail (21 Apr 2020) -- Start
            'NMB Enhancement # : On shortlisting criteria approval / rejection screen on MSS, the shortlisting criteria does not show all the parameters used for shortlisting.
            colhNationality.DataPropertyName = "nationality"
            colhExperience.DataPropertyName = "experience_days"
            colhExperienceCond.DataPropertyName = "experiencecondition"
            'Sohail (21 Apr 2020) -- End
            dgApplicantFilter.DataSource = mdtTable

            objQualification = Nothing
            objSkill = Nothing


            Call SetGridColor(dgApplicantFilter, objdgcolhIsGroup)
            Call SetCollapseValue(dgApplicantFilter, objdgcolhIsGroup, objdgcolhCollaps)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantFilter", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim strSearching As String = ""
        Try
            If User._Object.Privilege._AllowToViewShortListApplicants = True Then
                Dim objShortMst As New clsshortlist_master
                Dim dTab As DataTable = Nothing
                dTab = objShortMst.Get_Table("List", mintVacancyId, mstrShortFilterId)

                dgvData.AutoGenerateColumns = False

                objdgcolhApplicantId.DataPropertyName = "applicantunkid"
                objdgcolhGrpId.DataPropertyName = "GrpId"
                objdgcolhIsGrp.DataPropertyName = "IsGrp"
                objdgcolhshortlistunkid.DataPropertyName = "shortlistunkid"
                objdgcolhStatusId.DataPropertyName = "StatusId"
                objdgcolhVacId.DataPropertyName = "vacancyunkid"
                dgcolhRefNo.DataPropertyName = "refno"
                dgcolhVacancy.DataPropertyName = "vacancytitle"

                dgvData.DataSource = dTab

                Call SetGridColor(dgvData, objdgcolhIsGrp)
                Call SetCollapseValue(dgvData, objdgcolhIsGrp, objdgcolhCollapse)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor(ByVal dgGrid As DataGridView, ByVal dgGrpCol As DataGridViewColumn)
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            Dim pCell As New clsMergeCell

            For i As Integer = 0 To dgGrid.RowCount - 1
                If CBool(dgGrid.Rows(i).Cells(dgGrpCol.Index).Value) = True Then
                    dgGrid.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgGrid.Rows(i).ReadOnly = True
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue(ByVal dgGrid As DataGridView, ByVal dgGrpCol As DataGridViewColumn, ByVal dgCollapseCol As DataGridViewColumn)
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgGrid.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i As Integer = 0 To dgGrid.RowCount - 1
                If CBool(dgGrid.Rows(i).Cells(dgGrpCol.Index).Value) = True Then
                    If dgGrid.Rows(i).Cells(dgCollapseCol.Index).Value Is Nothing Then
                        dgGrid.Rows(i).Cells(dgCollapseCol.Index).Value = imgPlusIcon
                    End If
                    dgGrid.Rows(i).Cells(dgCollapseCol.Index).Style = objdgvsCollapseHeader
                Else
                    dgGrid.Rows(i).Cells(dgCollapseCol.Index).Value = imgBlankIcon
                    dgGrid.Rows(i).Visible = False
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnApprove.Visible = True : btnReject.Visible = True
            Select Case eSCType
                Case enShortListing_Status.SC_APPROVED
                    btnApprove.Visible = False
                    btnReject.Enabled = User._Object.Privilege._AllowToRejectApplicantFilter
                Case enShortListing_Status.SC_PENDING
                    btnApprove.Enabled = User._Object.Privilege._AllowToApproveApplicantFilter
                    btnReject.Enabled = User._Object.Privilege._AllowToRejectApplicantFilter
                Case enShortListing_Status.SC_REJECT
                    btnReject.Visible = False
                    btnApprove.Enabled = User._Object.Privilege._AllowToApproveApplicantFilter
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmApplicantFilter_Approval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            gbApplicantInfo.Collapse()
            gbCriteria.Collapse()
            gbRemark.Collapse()

            gbApplicantInfo.Size = New Size(888, gbApplicantInfo.Size.Height)
            gbCriteria.Size = New Size(888, gbCriteria.Size.Height)
            gbRemark.Size = New Size(888, gbRemark.Size.Height)

            mSize = fpnlLayout.Size

            Call GetApplicantFilter()
            Call Fill_Grid()

            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantFilter_Approval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim blnFlag As Boolean = False
            If mstrShortFilterId.Trim.Length > 0 Then
                If txtRemark.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Remark is mandatory information. Please enter remark to continue."), enMsgBoxStyle.Information)
                    txtRemark.Focus()
                    Exit Sub
                End If
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You are about to approve all shorlisting criteria. Due to this all reference no for this vacancy will be approved and therefore user cannot delete them.") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 8, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    For Each sUnkid As String In mstrShortFilterId.Split(CChar(","))
                        Dim objSLMaster As New clsshortlist_master
                        objSLMaster._Shortlistunkid = CInt(sUnkid)
                        objSLMaster._Approveuserunkid = User._Object._Userunkid
                        objSLMaster._Remark = txtRemark.Text
                        objSLMaster._Statustypid = enShortListing_Status.SC_APPROVED
                        objSLMaster._Appr_Date = ConfigParameter._Object._CurrentDateAndTime
                        objSLMaster._Voiddatetime = Nothing
                        objSLMaster._Isvoid = False
                        With objSLMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objSLMaster.Update() Then
                            blnFlag = True
                        Else
                            blnFlag = False
                        End If
                        objSLMaster = Nothing
                    Next
                End If
            End If
            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Information successfully saved."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim blnFlag As Boolean = False
            If mstrShortFilterId.Trim.Length > 0 Then
                If txtRemark.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Remark is mandatory information. Please enter remark to continue."), enMsgBoxStyle.Information)
                    txtRemark.Focus()
                    Exit Sub
                End If

                Dim objSFApplicant As New clsshortlist_finalapplicant

                If objSFApplicant.Can_RefNo_Disapproved(mstrShortFilterId) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry. You cannot disapprove Operation. Reason : Some of the applicants are already approved for batch scheduling."), enMsgBoxStyle.Information)
                    objSFApplicant = Nothing
                    Exit Sub
                End If
                objSFApplicant = Nothing

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You are about to disapprove all shorlisting criteria. Due to this all reference no for this vacancy will be disapproved and therefore user can delete them.") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 8, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    For Each sUnkid As String In mstrShortFilterId.Split(CChar(","))
                        Dim objSLMaster As New clsshortlist_master
                        objSLMaster._Shortlistunkid = CInt(sUnkid)
                        objSLMaster._Approveuserunkid = User._Object._Userunkid
                        objSLMaster._Remark = txtRemark.Text
                        objSLMaster._Statustypid = enShortListing_Status.SC_REJECT
                        objSLMaster._Appr_Date = ConfigParameter._Object._CurrentDateAndTime
                        objSLMaster._Voiddatetime = Nothing
                        objSLMaster._Issent = False
                        objSLMaster._Isvoid = False
                        With objSLMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objSLMaster.Update() Then
                            blnFlag = True
                        Else
                            blnFlag = False
                        End If
                        objSLMaster = Nothing
                    Next
                End If
            End If
            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Information successfully saved."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReject_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid's Event(s) "

    Private Sub dgApplicantFilter_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgApplicantFilter.CellContentClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgApplicantFilter.IsCurrentCellDirty Then
                Me.dgApplicantFilter.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgApplicantFilter.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgApplicantFilter.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value Is imgMinusIcon Then
                            dgApplicantFilter.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = imgPlusIcon
                        Else
                            dgApplicantFilter.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgApplicantFilter.RowCount - 1
                            If CInt(dgApplicantFilter.Rows(e.RowIndex).Cells(objdgcolhshortlistunkid.Index).Value) = CInt(dgApplicantFilter.Rows(i).Cells(objdgcolhshortlistunkid.Index).Value) Then
                                If dgApplicantFilter.Rows(i).Visible = False Then
                                    dgApplicantFilter.Rows(i).Visible = True
                                Else
                                    dgApplicantFilter.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next

                    Case 1

                        'Dim blnFlg As Boolean = CBool(dgApplicantFilter.Rows(e.RowIndex).Cells(objcolhIschecked.Index).Value)
                        'Dim drRow() As DataRow = mdtTable.Select("Employeeunkid = " & CInt(dgDependants.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value))

                        'If drRow.Length > 0 Then
                        '    RemoveHandler dgDependants.DataBindingComplete, AddressOf dgDependants_DataBindingComplete

                        '    For k As Integer = 1 To drRow.Length - 1
                        '        dgDependants.Rows(e.RowIndex + k).Cells(objcolhIschecked.Index).Value = blnFlg
                        '        mdtTable.Rows(e.RowIndex + k).AcceptChanges()
                        '    Next
                        '    AddHandler dgDependants.DataBindingComplete, AddressOf dgDependants_DataBindingComplete

                        '    SetCheckBoxValue()
                        'End If

                End Select

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgApplicantFilter_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub gbCriteria_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbCriteria.HeaderClick, gbApplicantInfo.HeaderClick, gbRemark.HeaderClick
        Try
            If (gbCriteria.Height + gbApplicantInfo.Height + gbRemark.Height) > mSize.Height Then
                gbApplicantInfo.Size = New Size(871, gbApplicantInfo.Size.Height)
                gbCriteria.Size = New Size(871, gbCriteria.Size.Height)
                gbRemark.Size = New Size(871, gbRemark.Size.Height)
            Else
                gbApplicantInfo.Size = New Size(888, gbApplicantInfo.Size.Height)
                gbCriteria.Size = New Size(888, gbCriteria.Size.Height)
                gbRemark.Size = New Size(888, gbRemark.Size.Height)
            End If
            fpnlLayout.Refresh() : fpnlLayout.Invalidate(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbCriteria_HeaderClick", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
			
			Me.gbCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbApplicantInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbApplicantInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbRemark.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbRemark.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnApprove.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

			Me.btnReject.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReject.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
			Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbCriteria.Text = Language._Object.getCaption(Me.gbCriteria.Name, Me.gbCriteria.Text)
			Me.gbApplicantInfo.Text = Language._Object.getCaption(Me.gbApplicantInfo.Name, Me.gbApplicantInfo.Text)
			Me.gbRemark.Text = Language._Object.getCaption(Me.gbRemark.Name, Me.gbRemark.Text)
			Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
			Me.dgcolhVacancy.HeaderText = Language._Object.getCaption(Me.dgcolhVacancy.Name, Me.dgcolhVacancy.HeaderText)
			Me.colhRefNo.HeaderText = Language._Object.getCaption(Me.colhRefNo.Name, Me.colhRefNo.HeaderText)
			Me.dgcolhSCVacancy.HeaderText = Language._Object.getCaption(Me.dgcolhSCVacancy.Name, Me.dgcolhSCVacancy.HeaderText)
			Me.colhQlGrpLevel.HeaderText = Language._Object.getCaption(Me.colhQlGrpLevel.Name, Me.colhQlGrpLevel.HeaderText)
			Me.colhQlGrp.HeaderText = Language._Object.getCaption(Me.colhQlGrp.Name, Me.colhQlGrp.HeaderText)
			Me.colhdgQualification.HeaderText = Language._Object.getCaption(Me.colhdgQualification.Name, Me.colhdgQualification.HeaderText)
			Me.colhResultCode.HeaderText = Language._Object.getCaption(Me.colhResultCode.Name, Me.colhResultCode.HeaderText)
			Me.colhRLevel.HeaderText = Language._Object.getCaption(Me.colhRLevel.Name, Me.colhRLevel.HeaderText)
			Me.colhRCondition.HeaderText = Language._Object.getCaption(Me.colhRCondition.Name, Me.colhRCondition.HeaderText)
			Me.colhGPA.HeaderText = Language._Object.getCaption(Me.colhGPA.Name, Me.colhGPA.HeaderText)
			Me.colhGPACondition.HeaderText = Language._Object.getCaption(Me.colhGPACondition.Name, Me.colhGPACondition.HeaderText)
			Me.colhAge.HeaderText = Language._Object.getCaption(Me.colhAge.Name, Me.colhAge.HeaderText)
			Me.colhAgeCondition.HeaderText = Language._Object.getCaption(Me.colhAgeCondition.Name, Me.colhAgeCondition.HeaderText)
			Me.colhAwardYear.HeaderText = Language._Object.getCaption(Me.colhAwardYear.Name, Me.colhAwardYear.HeaderText)
			Me.colhYearCondition.HeaderText = Language._Object.getCaption(Me.colhYearCondition.Name, Me.colhYearCondition.HeaderText)
			Me.colhGender.HeaderText = Language._Object.getCaption(Me.colhGender.Name, Me.colhGender.HeaderText)
			Me.colhFilterSkillCategory.HeaderText = Language._Object.getCaption(Me.colhFilterSkillCategory.Name, Me.colhFilterSkillCategory.HeaderText)
			Me.colhFilterSkill.HeaderText = Language._Object.getCaption(Me.colhFilterSkill.Name, Me.colhFilterSkill.HeaderText)
			Me.colhFilterOtherQuliGrp.HeaderText = Language._Object.getCaption(Me.colhFilterOtherQuliGrp.Name, Me.colhFilterOtherQuliGrp.HeaderText)
			Me.colhFilterOtherQualification.HeaderText = Language._Object.getCaption(Me.colhFilterOtherQualification.Name, Me.colhFilterOtherQualification.HeaderText)
			Me.colhFilterOtherResultCode.HeaderText = Language._Object.getCaption(Me.colhFilterOtherResultCode.Name, Me.colhFilterOtherResultCode.HeaderText)
			Me.colhFilterOtherSkillCategory.HeaderText = Language._Object.getCaption(Me.colhFilterOtherSkillCategory.Name, Me.colhFilterOtherSkillCategory.HeaderText)
			Me.colhFilterOtherSkill.HeaderText = Language._Object.getCaption(Me.colhFilterOtherSkill.Name, Me.colhFilterOtherSkill.HeaderText)
			Me.colhNationality.HeaderText = Language._Object.getCaption(Me.colhNationality.Name, Me.colhNationality.HeaderText)
			Me.colhExperience.HeaderText = Language._Object.getCaption(Me.colhExperience.Name, Me.colhExperience.HeaderText)
			Me.colhExperienceCond.HeaderText = Language._Object.getCaption(Me.colhExperienceCond.Name, Me.colhExperienceCond.HeaderText)
			Me.colhLogicalCondition.HeaderText = Language._Object.getCaption(Me.colhLogicalCondition.Name, Me.colhLogicalCondition.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "AND")
			Language.setMessage(mstrModuleName, 2, "OR")
			Language.setMessage(mstrModuleName, 3, "You are about to approve all shorlisting criteria. Due to this all reference no for this vacancy will be approved and therefore user cannot delete them.")
			Language.setMessage(mstrModuleName, 4, "You are about to disapprove all shorlisting criteria. Due to this all reference no for this vacancy will be disapproved and therefore user can delete them.")
			Language.setMessage(mstrModuleName, 5, "Information successfully saved.")
			Language.setMessage(mstrModuleName, 6, "Remark is mandatory information. Please enter remark to continue.")
			Language.setMessage(mstrModuleName, 7, "Sorry. You cannot disapprove Operation. Reason : Some of the applicants are already approved for batch scheduling.")
			Language.setMessage(mstrModuleName, 8, "Do you wish to continue?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class