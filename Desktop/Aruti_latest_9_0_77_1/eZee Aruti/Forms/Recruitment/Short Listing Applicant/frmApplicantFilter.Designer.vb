﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantFilter
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApplicantFilter))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.grpCondition = New System.Windows.Forms.GroupBox
        Me.rdANDCondition = New System.Windows.Forms.RadioButton
        Me.rdORCondition = New System.Windows.Forms.RadioButton
        Me.gbShortListedApplicant = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lvApplicant = New eZee.Common.eZeeListView(Me.components)
        Me.colhApplicantCode = New System.Windows.Forms.ColumnHeader
        Me.colhApplicant = New System.Windows.Forms.ColumnHeader
        Me.colhEmail = New System.Windows.Forms.ColumnHeader
        Me.colhAppliQlfication = New System.Windows.Forms.ColumnHeader
        Me.btnFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgFilter = New System.Windows.Forms.DataGridView
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkOtherQualification = New System.Windows.Forms.CheckBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.pnlResidenceType = New System.Windows.Forms.Panel
        Me.cboResidenceType = New System.Windows.Forms.ComboBox
        Me.lblResidenceType = New System.Windows.Forms.Label
        Me.nudAptitudeResult = New System.Windows.Forms.NumericUpDown
        Me.LblAptitudeCondition = New System.Windows.Forms.Label
        Me.LblAptitudeResult = New System.Windows.Forms.Label
        Me.cboAptitudeResultCondition = New System.Windows.Forms.ComboBox
        Me.lblExperienceMonths = New System.Windows.Forms.Label
        Me.lblExperienceYears = New System.Windows.Forms.Label
        Me.cboExperienceCondition = New System.Windows.Forms.ComboBox
        Me.nudExperienceMonths = New System.Windows.Forms.NumericUpDown
        Me.lblExperience = New System.Windows.Forms.Label
        Me.nudExperienceYears = New System.Windows.Forms.NumericUpDown
        Me.pnlOtherQualification = New System.Windows.Forms.Panel
        Me.txtOtherSkill = New eZee.TextBox.AlphanumericTextBox
        Me.lblOtherSkill = New System.Windows.Forms.Label
        Me.lblOtherQualification = New System.Windows.Forms.Label
        Me.lblOtherQualificationGrp = New System.Windows.Forms.Label
        Me.txtOtherSkillCategory = New eZee.TextBox.AlphanumericTextBox
        Me.lblOtherSkillCategory = New System.Windows.Forms.Label
        Me.txtOtherQualification = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherQualificationGrp = New eZee.TextBox.AlphanumericTextBox
        Me.chkQualificationGroup = New System.Windows.Forms.CheckBox
        Me.lblQualificationGrpCondition = New System.Windows.Forms.Label
        Me.cboQualificationGrpCondition = New System.Windows.Forms.ComboBox
        Me.dgvQualificationGroup = New System.Windows.Forms.DataGridView
        Me.objdgcolhQlGrpCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhQlGrpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhQlGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblNationality = New System.Windows.Forms.Label
        Me.cboNationality = New System.Windows.Forms.ComboBox
        Me.chkSkill = New System.Windows.Forms.CheckBox
        Me.dgvSkillName = New System.Windows.Forms.DataGridView
        Me.objdgcolhSkillCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhSkillName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSkillId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCategoryName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCategoryId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkSkillCategory = New System.Windows.Forms.CheckBox
        Me.dgvSkillCategory = New System.Windows.Forms.DataGridView
        Me.objdgcolhSCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhSkillCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSkillCategoryId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkQualification = New System.Windows.Forms.CheckBox
        Me.dgvQualification = New System.Windows.Forms.DataGridView
        Me.objdgcolhQlCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhQlName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhQlId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnQualificationReset = New eZee.Common.eZeeGradientButton
        Me.objbtnQualificationSearch = New eZee.Common.eZeeGradientButton
        Me.objbtnSkillCategoryReset = New eZee.Common.eZeeGradientButton
        Me.objbtnSkillCategorySearch = New eZee.Common.eZeeGradientButton
        Me.objbtnResetSkills = New eZee.Common.eZeeGradientButton
        Me.objbtnSeachSkills = New eZee.Common.eZeeGradientButton
        Me.txtSCSearch = New eZee.TextBox.AlphanumericTextBox
        Me.txtSkillSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.lblYearCondition = New System.Windows.Forms.Label
        Me.cboYearCondition = New System.Windows.Forms.ComboBox
        Me.cboAwardedYear = New System.Windows.Forms.ComboBox
        Me.lblAwardYear = New System.Windows.Forms.Label
        Me.lblVacanyType = New System.Windows.Forms.Label
        Me.nudGPA = New System.Windows.Forms.NumericUpDown
        Me.lblGPACondition = New System.Windows.Forms.Label
        Me.cboResultLvlCondition = New System.Windows.Forms.ComboBox
        Me.lblGPA = New System.Windows.Forms.Label
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.cboGPACondition = New System.Windows.Forms.ComboBox
        Me.lblAge = New System.Windows.Forms.Label
        Me.txtResultLevel = New eZee.TextBox.NumericTextBox
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.nudAge = New System.Windows.Forms.NumericUpDown
        Me.lblLevel = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.lblAgeCondition = New System.Windows.Forms.Label
        Me.nudLevel = New System.Windows.Forms.NumericUpDown
        Me.cboResultCode = New System.Windows.Forms.ComboBox
        Me.lblQualificationGrpLevel = New System.Windows.Forms.Label
        Me.cboAgeCondition = New System.Windows.Forms.ComboBox
        Me.lblSkill = New System.Windows.Forms.Label
        Me.lblResultCode = New System.Windows.Forms.Label
        Me.lblQualificationgrp = New System.Windows.Forms.Label
        Me.lblGender = New System.Windows.Forms.Label
        Me.cboQualificationGrp = New System.Windows.Forms.ComboBox
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblSkillCategory = New System.Windows.Forms.Label
        Me.lblQualification = New System.Windows.Forms.Label
        Me.txtOtherResultCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtQSearch = New eZee.TextBox.AlphanumericTextBox
        Me.lblQualificationGroupCondition = New System.Windows.Forms.Label
        Me.cboQualificationGroupCondition = New System.Windows.Forms.ComboBox
        Me.chkInvalidCode = New System.Windows.Forms.CheckBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblCaption = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhVacancy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhQlGrpLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhQlGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhResultCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhRLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhRCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGPA = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGPACondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAge = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAgeCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAwardYear = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhYearCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterSkillCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterSkill = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherQuliGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherResultCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherSkillCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFilterOtherSkill = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhFilterunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhFilter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhNationality = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhExperience = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhExperienceCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAptitudeResult = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAptitudeCondition = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhResidenceType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.grpCondition.SuspendLayout()
        Me.gbShortListedApplicant.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlResidenceType.SuspendLayout()
        CType(Me.nudAptitudeResult, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudExperienceMonths, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudExperienceYears, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOtherQualification.SuspendLayout()
        CType(Me.dgvQualificationGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSkillName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSkillCategory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvQualification, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudGPA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudAge, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.grpCondition)
        Me.pnlMain.Controls.Add(Me.gbShortListedApplicant)
        Me.pnlMain.Controls.Add(Me.btnFilter)
        Me.pnlMain.Controls.Add(Me.btnDelete)
        Me.pnlMain.Controls.Add(Me.dgFilter)
        Me.pnlMain.Controls.Add(Me.btnAdd)
        Me.pnlMain.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(970, 594)
        Me.pnlMain.TabIndex = 0
        '
        'grpCondition
        '
        Me.grpCondition.Controls.Add(Me.rdANDCondition)
        Me.grpCondition.Controls.Add(Me.rdORCondition)
        Me.grpCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCondition.Location = New System.Drawing.Point(209, 367)
        Me.grpCondition.Name = "grpCondition"
        Me.grpCondition.Size = New System.Drawing.Size(168, 36)
        Me.grpCondition.TabIndex = 32
        Me.grpCondition.TabStop = False
        Me.grpCondition.Text = "Condition To Apply Filter"
        '
        'rdANDCondition
        '
        Me.rdANDCondition.Location = New System.Drawing.Point(92, 13)
        Me.rdANDCondition.Name = "rdANDCondition"
        Me.rdANDCondition.Size = New System.Drawing.Size(64, 17)
        Me.rdANDCondition.TabIndex = 1
        Me.rdANDCondition.Text = "AND"
        Me.rdANDCondition.UseVisualStyleBackColor = True
        '
        'rdORCondition
        '
        Me.rdORCondition.Checked = True
        Me.rdORCondition.Location = New System.Drawing.Point(22, 13)
        Me.rdORCondition.Name = "rdORCondition"
        Me.rdORCondition.Size = New System.Drawing.Size(64, 17)
        Me.rdORCondition.TabIndex = 0
        Me.rdORCondition.TabStop = True
        Me.rdORCondition.Text = "OR"
        Me.rdORCondition.UseVisualStyleBackColor = True
        '
        'gbShortListedApplicant
        '
        Me.gbShortListedApplicant.BorderColor = System.Drawing.Color.Black
        Me.gbShortListedApplicant.Checked = False
        Me.gbShortListedApplicant.CollapseAllExceptThis = False
        Me.gbShortListedApplicant.CollapsedHoverImage = Nothing
        Me.gbShortListedApplicant.CollapsedNormalImage = Nothing
        Me.gbShortListedApplicant.CollapsedPressedImage = Nothing
        Me.gbShortListedApplicant.CollapseOnLoad = False
        Me.gbShortListedApplicant.Controls.Add(Me.objchkAll)
        Me.gbShortListedApplicant.Controls.Add(Me.Panel1)
        Me.gbShortListedApplicant.ExpandedHoverImage = Nothing
        Me.gbShortListedApplicant.ExpandedNormalImage = Nothing
        Me.gbShortListedApplicant.ExpandedPressedImage = Nothing
        Me.gbShortListedApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShortListedApplicant.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShortListedApplicant.HeaderHeight = 25
        Me.gbShortListedApplicant.HeaderMessage = ""
        Me.gbShortListedApplicant.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbShortListedApplicant.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShortListedApplicant.HeightOnCollapse = 0
        Me.gbShortListedApplicant.LeftTextSpace = 0
        Me.gbShortListedApplicant.Location = New System.Drawing.Point(670, 9)
        Me.gbShortListedApplicant.Name = "gbShortListedApplicant"
        Me.gbShortListedApplicant.OpenHeight = 300
        Me.gbShortListedApplicant.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShortListedApplicant.ShowBorder = True
        Me.gbShortListedApplicant.ShowCheckBox = False
        Me.gbShortListedApplicant.ShowCollapseButton = False
        Me.gbShortListedApplicant.ShowDefaultBorderColor = True
        Me.gbShortListedApplicant.ShowDownButton = False
        Me.gbShortListedApplicant.ShowHeader = True
        Me.gbShortListedApplicant.Size = New System.Drawing.Size(291, 400)
        Me.gbShortListedApplicant.TabIndex = 31
        Me.gbShortListedApplicant.Temp = 0
        Me.gbShortListedApplicant.Text = "Short List Applicant"
        Me.gbShortListedApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.BackColor = System.Drawing.Color.Transparent
        Me.objchkAll.Location = New System.Drawing.Point(272, 6)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 3
        Me.objchkAll.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lvApplicant)
        Me.Panel1.Location = New System.Drawing.Point(2, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(287, 371)
        Me.Panel1.TabIndex = 1
        '
        'lvApplicant
        '
        Me.lvApplicant.BackColorOnChecked = False
        Me.lvApplicant.CheckBoxes = True
        Me.lvApplicant.ColumnHeaders = Nothing
        Me.lvApplicant.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhApplicantCode, Me.colhApplicant, Me.colhEmail, Me.colhAppliQlfication})
        Me.lvApplicant.CompulsoryColumns = ""
        Me.lvApplicant.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvApplicant.FullRowSelect = True
        Me.lvApplicant.GridLines = True
        Me.lvApplicant.GroupingColumn = Nothing
        Me.lvApplicant.HideSelection = False
        Me.lvApplicant.Location = New System.Drawing.Point(0, 0)
        Me.lvApplicant.MinColumnWidth = 50
        Me.lvApplicant.MultiSelect = False
        Me.lvApplicant.Name = "lvApplicant"
        Me.lvApplicant.OptionalColumns = ""
        Me.lvApplicant.ShowMoreItem = False
        Me.lvApplicant.ShowSaveItem = False
        Me.lvApplicant.ShowSelectAll = True
        Me.lvApplicant.ShowSizeAllColumnsToFit = True
        Me.lvApplicant.Size = New System.Drawing.Size(287, 371)
        Me.lvApplicant.Sortable = True
        Me.lvApplicant.TabIndex = 0
        Me.lvApplicant.UseCompatibleStateImageBehavior = False
        Me.lvApplicant.View = System.Windows.Forms.View.Details
        '
        'colhApplicantCode
        '
        Me.colhApplicantCode.Tag = "colhApplicantCode"
        Me.colhApplicantCode.Text = "Code"
        Me.colhApplicantCode.Width = 70
        '
        'colhApplicant
        '
        Me.colhApplicant.Tag = "colhApplicant"
        Me.colhApplicant.Text = "Applicant"
        Me.colhApplicant.Width = 175
        '
        'colhEmail
        '
        Me.colhEmail.Tag = "colhEmail"
        Me.colhEmail.Text = "Email"
        Me.colhEmail.Width = 125
        '
        'colhAppliQlfication
        '
        Me.colhAppliQlfication.Tag = "colhAppliQlfication"
        Me.colhAppliQlfication.Text = "Qualification"
        Me.colhAppliQlfication.Width = 0
        '
        'btnFilter
        '
        Me.btnFilter.BackColor = System.Drawing.Color.White
        Me.btnFilter.BackgroundImage = CType(resources.GetObject("btnFilter.BackgroundImage"), System.Drawing.Image)
        Me.btnFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFilter.BorderColor = System.Drawing.Color.Empty
        Me.btnFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFilter.FlatAppearance.BorderSize = 0
        Me.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.ForeColor = System.Drawing.Color.Black
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.Color.Black
        Me.btnFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFilter.Location = New System.Drawing.Point(575, 373)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFilter.Size = New System.Drawing.Size(89, 30)
        Me.btnFilter.TabIndex = 30
        Me.btnFilter.Text = "Apply Fi&lter"
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(479, 373)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'dgFilter
        '
        Me.dgFilter.AllowUserToAddRows = False
        Me.dgFilter.AllowUserToDeleteRows = False
        Me.dgFilter.BackgroundColor = System.Drawing.Color.White
        Me.dgFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgFilter.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhVacancy, Me.colhQlGrpLevel, Me.colhQlGrp, Me.colhdgQualification, Me.colhResultCode, Me.colhRLevel, Me.colhRCondition, Me.colhGPA, Me.colhGPACondition, Me.colhAge, Me.colhAgeCondition, Me.colhAwardYear, Me.colhYearCondition, Me.colhGender, Me.colhFilterSkillCategory, Me.colhFilterSkill, Me.colhFilterOtherQuliGrp, Me.colhFilterOtherQualification, Me.colhFilterOtherResultCode, Me.colhFilterOtherSkillCategory, Me.colhFilterOtherSkill, Me.objcolhFilterunkid, Me.objcolhGUID, Me.objcolhFilter, Me.colhNationality, Me.colhExperience, Me.colhExperienceCondition, Me.colhAptitudeResult, Me.colhAptitudeCondition, Me.colhResidenceType})
        Me.dgFilter.Location = New System.Drawing.Point(9, 414)
        Me.dgFilter.Name = "dgFilter"
        Me.dgFilter.ReadOnly = True
        Me.dgFilter.RowHeadersVisible = False
        Me.dgFilter.RowHeadersWidth = 25
        Me.dgFilter.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgFilter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgFilter.Size = New System.Drawing.Size(952, 120)
        Me.dgFilter.TabIndex = 29
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(383, 373)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkOtherQualification)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.Panel2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkInvalidCode)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(9, 9)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(655, 353)
        Me.EZeeCollapsibleContainer1.TabIndex = 28
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Applicant Filter Information"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOtherQualification
        '
        Me.chkOtherQualification.BackColor = System.Drawing.Color.Transparent
        Me.chkOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOtherQualification.Location = New System.Drawing.Point(310, 4)
        Me.chkOtherQualification.Name = "chkOtherQualification"
        Me.chkOtherQualification.Size = New System.Drawing.Size(148, 17)
        Me.chkOtherQualification.TabIndex = 45
        Me.chkOtherQualification.Text = "Other Qualification"
        Me.chkOtherQualification.UseVisualStyleBackColor = False
        Me.chkOtherQualification.Visible = False
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.pnlResidenceType)
        Me.Panel2.Controls.Add(Me.nudAptitudeResult)
        Me.Panel2.Controls.Add(Me.LblAptitudeCondition)
        Me.Panel2.Controls.Add(Me.LblAptitudeResult)
        Me.Panel2.Controls.Add(Me.cboAptitudeResultCondition)
        Me.Panel2.Controls.Add(Me.lblExperienceMonths)
        Me.Panel2.Controls.Add(Me.lblExperienceYears)
        Me.Panel2.Controls.Add(Me.cboExperienceCondition)
        Me.Panel2.Controls.Add(Me.nudExperienceMonths)
        Me.Panel2.Controls.Add(Me.lblExperience)
        Me.Panel2.Controls.Add(Me.nudExperienceYears)
        Me.Panel2.Controls.Add(Me.pnlOtherQualification)
        Me.Panel2.Controls.Add(Me.chkQualificationGroup)
        Me.Panel2.Controls.Add(Me.lblQualificationGrpCondition)
        Me.Panel2.Controls.Add(Me.cboQualificationGrpCondition)
        Me.Panel2.Controls.Add(Me.dgvQualificationGroup)
        Me.Panel2.Controls.Add(Me.lblNationality)
        Me.Panel2.Controls.Add(Me.cboNationality)
        Me.Panel2.Controls.Add(Me.chkSkill)
        Me.Panel2.Controls.Add(Me.dgvSkillName)
        Me.Panel2.Controls.Add(Me.chkSkillCategory)
        Me.Panel2.Controls.Add(Me.dgvSkillCategory)
        Me.Panel2.Controls.Add(Me.chkQualification)
        Me.Panel2.Controls.Add(Me.dgvQualification)
        Me.Panel2.Controls.Add(Me.objbtnQualificationReset)
        Me.Panel2.Controls.Add(Me.objbtnQualificationSearch)
        Me.Panel2.Controls.Add(Me.objbtnSkillCategoryReset)
        Me.Panel2.Controls.Add(Me.objbtnSkillCategorySearch)
        Me.Panel2.Controls.Add(Me.objbtnResetSkills)
        Me.Panel2.Controls.Add(Me.objbtnSeachSkills)
        Me.Panel2.Controls.Add(Me.txtSCSearch)
        Me.Panel2.Controls.Add(Me.txtSkillSearch)
        Me.Panel2.Controls.Add(Me.objbtnSearchVacancy)
        Me.Panel2.Controls.Add(Me.lblYearCondition)
        Me.Panel2.Controls.Add(Me.cboYearCondition)
        Me.Panel2.Controls.Add(Me.cboAwardedYear)
        Me.Panel2.Controls.Add(Me.lblAwardYear)
        Me.Panel2.Controls.Add(Me.lblVacanyType)
        Me.Panel2.Controls.Add(Me.nudGPA)
        Me.Panel2.Controls.Add(Me.lblGPACondition)
        Me.Panel2.Controls.Add(Me.cboResultLvlCondition)
        Me.Panel2.Controls.Add(Me.lblGPA)
        Me.Panel2.Controls.Add(Me.cboVacancyType)
        Me.Panel2.Controls.Add(Me.cboGPACondition)
        Me.Panel2.Controls.Add(Me.lblAge)
        Me.Panel2.Controls.Add(Me.txtResultLevel)
        Me.Panel2.Controls.Add(Me.cboVacancy)
        Me.Panel2.Controls.Add(Me.nudAge)
        Me.Panel2.Controls.Add(Me.lblLevel)
        Me.Panel2.Controls.Add(Me.lblVacancy)
        Me.Panel2.Controls.Add(Me.lblAgeCondition)
        Me.Panel2.Controls.Add(Me.nudLevel)
        Me.Panel2.Controls.Add(Me.cboResultCode)
        Me.Panel2.Controls.Add(Me.lblQualificationGrpLevel)
        Me.Panel2.Controls.Add(Me.cboAgeCondition)
        Me.Panel2.Controls.Add(Me.lblSkill)
        Me.Panel2.Controls.Add(Me.lblResultCode)
        Me.Panel2.Controls.Add(Me.lblQualificationgrp)
        Me.Panel2.Controls.Add(Me.lblGender)
        Me.Panel2.Controls.Add(Me.cboQualificationGrp)
        Me.Panel2.Controls.Add(Me.cboGender)
        Me.Panel2.Controls.Add(Me.lblSkillCategory)
        Me.Panel2.Controls.Add(Me.lblQualification)
        Me.Panel2.Controls.Add(Me.txtOtherResultCode)
        Me.Panel2.Controls.Add(Me.txtQSearch)
        Me.Panel2.Controls.Add(Me.lblQualificationGroupCondition)
        Me.Panel2.Controls.Add(Me.cboQualificationGroupCondition)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(3, 26)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(649, 318)
        Me.Panel2.TabIndex = 43
        '
        'pnlResidenceType
        '
        Me.pnlResidenceType.Controls.Add(Me.cboResidenceType)
        Me.pnlResidenceType.Controls.Add(Me.lblResidenceType)
        Me.pnlResidenceType.Location = New System.Drawing.Point(276, 262)
        Me.pnlResidenceType.Name = "pnlResidenceType"
        Me.pnlResidenceType.Size = New System.Drawing.Size(349, 27)
        Me.pnlResidenceType.TabIndex = 491
        Me.pnlResidenceType.Visible = False
        '
        'cboResidenceType
        '
        Me.cboResidenceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidenceType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidenceType.FormattingEnabled = True
        Me.cboResidenceType.Location = New System.Drawing.Point(98, 3)
        Me.cboResidenceType.Name = "cboResidenceType"
        Me.cboResidenceType.Size = New System.Drawing.Size(205, 21)
        Me.cboResidenceType.TabIndex = 490
        '
        'lblResidenceType
        '
        Me.lblResidenceType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidenceType.Location = New System.Drawing.Point(11, 4)
        Me.lblResidenceType.Name = "lblResidenceType"
        Me.lblResidenceType.Size = New System.Drawing.Size(81, 17)
        Me.lblResidenceType.TabIndex = 489
        Me.lblResidenceType.Text = "Residene Type"
        Me.lblResidenceType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudAptitudeResult
        '
        Me.nudAptitudeResult.DecimalPlaces = 2
        Me.nudAptitudeResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAptitudeResult.Location = New System.Drawing.Point(374, 240)
        Me.nudAptitudeResult.Name = "nudAptitudeResult"
        Me.nudAptitudeResult.Size = New System.Drawing.Size(72, 20)
        Me.nudAptitudeResult.TabIndex = 486
        Me.nudAptitudeResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudAptitudeResult.Visible = False
        '
        'LblAptitudeCondition
        '
        Me.LblAptitudeCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAptitudeCondition.Location = New System.Drawing.Point(452, 242)
        Me.LblAptitudeCondition.Name = "LblAptitudeCondition"
        Me.LblAptitudeCondition.Size = New System.Drawing.Size(63, 17)
        Me.LblAptitudeCondition.TabIndex = 487
        Me.LblAptitudeCondition.Text = "Condition"
        Me.LblAptitudeCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblAptitudeCondition.Visible = False
        '
        'LblAptitudeResult
        '
        Me.LblAptitudeResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAptitudeResult.Location = New System.Drawing.Point(287, 242)
        Me.LblAptitudeResult.Name = "LblAptitudeResult"
        Me.LblAptitudeResult.Size = New System.Drawing.Size(81, 17)
        Me.LblAptitudeResult.TabIndex = 485
        Me.LblAptitudeResult.Text = "Apt. Result"
        Me.LblAptitudeResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblAptitudeResult.Visible = False
        '
        'cboAptitudeResultCondition
        '
        Me.cboAptitudeResultCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAptitudeResultCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAptitudeResultCondition.FormattingEnabled = True
        Me.cboAptitudeResultCondition.Location = New System.Drawing.Point(528, 240)
        Me.cboAptitudeResultCondition.Name = "cboAptitudeResultCondition"
        Me.cboAptitudeResultCondition.Size = New System.Drawing.Size(52, 21)
        Me.cboAptitudeResultCondition.TabIndex = 488
        Me.cboAptitudeResultCondition.Visible = False
        '
        'lblExperienceMonths
        '
        Me.lblExperienceMonths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExperienceMonths.Location = New System.Drawing.Point(583, 215)
        Me.lblExperienceMonths.Name = "lblExperienceMonths"
        Me.lblExperienceMonths.Size = New System.Drawing.Size(42, 17)
        Me.lblExperienceMonths.TabIndex = 482
        Me.lblExperienceMonths.Text = "Months"
        Me.lblExperienceMonths.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExperienceYears
        '
        Me.lblExperienceYears.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExperienceYears.Location = New System.Drawing.Point(486, 215)
        Me.lblExperienceYears.Name = "lblExperienceYears"
        Me.lblExperienceYears.Size = New System.Drawing.Size(34, 17)
        Me.lblExperienceYears.TabIndex = 481
        Me.lblExperienceYears.Text = "Years"
        Me.lblExperienceYears.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExperienceCondition
        '
        Me.cboExperienceCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExperienceCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExperienceCondition.FormattingEnabled = True
        Me.cboExperienceCondition.Location = New System.Drawing.Point(374, 213)
        Me.cboExperienceCondition.Name = "cboExperienceCondition"
        Me.cboExperienceCondition.Size = New System.Drawing.Size(50, 21)
        Me.cboExperienceCondition.TabIndex = 480
        '
        'nudExperienceMonths
        '
        Me.nudExperienceMonths.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudExperienceMonths.Location = New System.Drawing.Point(529, 213)
        Me.nudExperienceMonths.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudExperienceMonths.Name = "nudExperienceMonths"
        Me.nudExperienceMonths.Size = New System.Drawing.Size(51, 20)
        Me.nudExperienceMonths.TabIndex = 479
        Me.nudExperienceMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblExperience
        '
        Me.lblExperience.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExperience.Location = New System.Drawing.Point(287, 215)
        Me.lblExperience.Name = "lblExperience"
        Me.lblExperience.Size = New System.Drawing.Size(81, 17)
        Me.lblExperience.TabIndex = 477
        Me.lblExperience.Text = "Experience"
        Me.lblExperience.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudExperienceYears
        '
        Me.nudExperienceYears.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudExperienceYears.Location = New System.Drawing.Point(430, 213)
        Me.nudExperienceYears.Name = "nudExperienceYears"
        Me.nudExperienceYears.Size = New System.Drawing.Size(50, 20)
        Me.nudExperienceYears.TabIndex = 478
        Me.nudExperienceYears.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pnlOtherQualification
        '
        Me.pnlOtherQualification.Controls.Add(Me.txtOtherSkill)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherSkill)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherQualification)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherQualificationGrp)
        Me.pnlOtherQualification.Controls.Add(Me.txtOtherSkillCategory)
        Me.pnlOtherQualification.Controls.Add(Me.lblOtherSkillCategory)
        Me.pnlOtherQualification.Controls.Add(Me.txtOtherQualification)
        Me.pnlOtherQualification.Controls.Add(Me.txtOtherQualificationGrp)
        Me.pnlOtherQualification.Location = New System.Drawing.Point(1, 452)
        Me.pnlOtherQualification.Name = "pnlOtherQualification"
        Me.pnlOtherQualification.Size = New System.Drawing.Size(627, 122)
        Me.pnlOtherQualification.TabIndex = 47
        '
        'txtOtherSkill
        '
        Me.txtOtherSkill.Flags = 0
        Me.txtOtherSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherSkill.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherSkill.Location = New System.Drawing.Point(372, 57)
        Me.txtOtherSkill.Multiline = True
        Me.txtOtherSkill.Name = "txtOtherSkill"
        Me.txtOtherSkill.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherSkill.Size = New System.Drawing.Size(252, 56)
        Me.txtOtherSkill.TabIndex = 236
        '
        'lblOtherSkill
        '
        Me.lblOtherSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherSkill.Location = New System.Drawing.Point(287, 58)
        Me.lblOtherSkill.Name = "lblOtherSkill"
        Me.lblOtherSkill.Size = New System.Drawing.Size(80, 18)
        Me.lblOtherSkill.TabIndex = 235
        Me.lblOtherSkill.Text = "Skill "
        Me.lblOtherSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherQualification
        '
        Me.lblOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualification.Location = New System.Drawing.Point(6, 57)
        Me.lblOtherQualification.Name = "lblOtherQualification"
        Me.lblOtherQualification.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherQualification.TabIndex = 233
        Me.lblOtherQualification.Text = "Qualification"
        Me.lblOtherQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherQualificationGrp
        '
        Me.lblOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualificationGrp.Location = New System.Drawing.Point(6, 6)
        Me.lblOtherQualificationGrp.Name = "lblOtherQualificationGrp"
        Me.lblOtherQualificationGrp.Size = New System.Drawing.Size(85, 19)
        Me.lblOtherQualificationGrp.TabIndex = 232
        Me.lblOtherQualificationGrp.Text = "Qualif. Group"
        Me.lblOtherQualificationGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherSkillCategory
        '
        Me.txtOtherSkillCategory.Flags = 0
        Me.txtOtherSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherSkillCategory.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherSkillCategory.Location = New System.Drawing.Point(372, 5)
        Me.txtOtherSkillCategory.Multiline = True
        Me.txtOtherSkillCategory.Name = "txtOtherSkillCategory"
        Me.txtOtherSkillCategory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherSkillCategory.Size = New System.Drawing.Size(252, 45)
        Me.txtOtherSkillCategory.TabIndex = 231
        '
        'lblOtherSkillCategory
        '
        Me.lblOtherSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherSkillCategory.Location = New System.Drawing.Point(287, 6)
        Me.lblOtherSkillCategory.Name = "lblOtherSkillCategory"
        Me.lblOtherSkillCategory.Size = New System.Drawing.Size(80, 18)
        Me.lblOtherSkillCategory.TabIndex = 230
        Me.lblOtherSkillCategory.Text = "Skill Category"
        Me.lblOtherSkillCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherQualification
        '
        Me.txtOtherQualification.Flags = 0
        Me.txtOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualification.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualification.Location = New System.Drawing.Point(99, 56)
        Me.txtOtherQualification.Multiline = True
        Me.txtOtherQualification.Name = "txtOtherQualification"
        Me.txtOtherQualification.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherQualification.Size = New System.Drawing.Size(169, 57)
        Me.txtOtherQualification.TabIndex = 227
        '
        'txtOtherQualificationGrp
        '
        Me.txtOtherQualificationGrp.Flags = 0
        Me.txtOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualificationGrp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualificationGrp.Location = New System.Drawing.Point(99, 5)
        Me.txtOtherQualificationGrp.Multiline = True
        Me.txtOtherQualificationGrp.Name = "txtOtherQualificationGrp"
        Me.txtOtherQualificationGrp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtherQualificationGrp.Size = New System.Drawing.Size(169, 45)
        Me.txtOtherQualificationGrp.TabIndex = 225
        '
        'chkQualificationGroup
        '
        Me.chkQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkQualificationGroup.Location = New System.Drawing.Point(108, 69)
        Me.chkQualificationGroup.Name = "chkQualificationGroup"
        Me.chkQualificationGroup.Size = New System.Drawing.Size(13, 13)
        Me.chkQualificationGroup.TabIndex = 476
        Me.chkQualificationGroup.UseVisualStyleBackColor = True
        '
        'lblQualificationGrpCondition
        '
        Me.lblQualificationGrpCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGrpCondition.Location = New System.Drawing.Point(181, 40)
        Me.lblQualificationGrpCondition.Name = "lblQualificationGrpCondition"
        Me.lblQualificationGrpCondition.Size = New System.Drawing.Size(51, 18)
        Me.lblQualificationGrpCondition.TabIndex = 474
        Me.lblQualificationGrpCondition.Text = "Condition"
        Me.lblQualificationGrpCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualificationGrpCondition
        '
        Me.cboQualificationGrpCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualificationGrpCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualificationGrpCondition.FormattingEnabled = True
        Me.cboQualificationGrpCondition.Location = New System.Drawing.Point(232, 40)
        Me.cboQualificationGrpCondition.Name = "cboQualificationGrpCondition"
        Me.cboQualificationGrpCondition.Size = New System.Drawing.Size(40, 21)
        Me.cboQualificationGrpCondition.TabIndex = 475
        '
        'dgvQualificationGroup
        '
        Me.dgvQualificationGroup.AllowUserToAddRows = False
        Me.dgvQualificationGroup.AllowUserToDeleteRows = False
        Me.dgvQualificationGroup.AllowUserToResizeRows = False
        Me.dgvQualificationGroup.BackgroundColor = System.Drawing.Color.White
        Me.dgvQualificationGroup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvQualificationGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvQualificationGroup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhQlGrpCheck, Me.dgcolhQlGrpName, Me.objdgcolhQlGrpId, Me.dgcolhQLevel})
        Me.dgvQualificationGroup.Location = New System.Drawing.Point(101, 64)
        Me.dgvQualificationGroup.Name = "dgvQualificationGroup"
        Me.dgvQualificationGroup.RowHeadersVisible = False
        Me.dgvQualificationGroup.Size = New System.Drawing.Size(172, 91)
        Me.dgvQualificationGroup.TabIndex = 473
        '
        'objdgcolhQlGrpCheck
        '
        Me.objdgcolhQlGrpCheck.Frozen = True
        Me.objdgcolhQlGrpCheck.HeaderText = ""
        Me.objdgcolhQlGrpCheck.Name = "objdgcolhQlGrpCheck"
        Me.objdgcolhQlGrpCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhQlGrpCheck.Width = 25
        '
        'dgcolhQlGrpName
        '
        Me.dgcolhQlGrpName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhQlGrpName.HeaderText = "Qua. Grp."
        Me.dgcolhQlGrpName.Name = "dgcolhQlGrpName"
        Me.dgcolhQlGrpName.ReadOnly = True
        '
        'objdgcolhQlGrpId
        '
        Me.objdgcolhQlGrpId.HeaderText = "objdgcolhQlGrpId"
        Me.objdgcolhQlGrpId.Name = "objdgcolhQlGrpId"
        Me.objdgcolhQlGrpId.ReadOnly = True
        Me.objdgcolhQlGrpId.Visible = False
        '
        'dgcolhQLevel
        '
        Me.dgcolhQLevel.HeaderText = "Level"
        Me.dgcolhQLevel.Name = "dgcolhQLevel"
        Me.dgcolhQLevel.ReadOnly = True
        Me.dgcolhQLevel.Width = 30
        '
        'lblNationality
        '
        Me.lblNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNationality.Location = New System.Drawing.Point(9, 268)
        Me.lblNationality.Name = "lblNationality"
        Me.lblNationality.Size = New System.Drawing.Size(84, 17)
        Me.lblNationality.TabIndex = 471
        Me.lblNationality.Text = "Nationality"
        Me.lblNationality.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNationality
        '
        Me.cboNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNationality.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNationality.FormattingEnabled = True
        Me.cboNationality.Location = New System.Drawing.Point(101, 266)
        Me.cboNationality.Name = "cboNationality"
        Me.cboNationality.Size = New System.Drawing.Size(169, 21)
        Me.cboNationality.TabIndex = 472
        '
        'chkSkill
        '
        Me.chkSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSkill.Location = New System.Drawing.Point(382, 328)
        Me.chkSkill.Name = "chkSkill"
        Me.chkSkill.Size = New System.Drawing.Size(13, 13)
        Me.chkSkill.TabIndex = 42
        Me.chkSkill.UseVisualStyleBackColor = True
        '
        'dgvSkillName
        '
        Me.dgvSkillName.AllowUserToAddRows = False
        Me.dgvSkillName.AllowUserToDeleteRows = False
        Me.dgvSkillName.AllowUserToResizeRows = False
        Me.dgvSkillName.BackgroundColor = System.Drawing.Color.White
        Me.dgvSkillName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvSkillName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSkillName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSkillCheck, Me.dgcolhSkillName, Me.objdgcolhSkillId, Me.objdgcolhCategoryName, Me.objdgcolhCategoryId})
        Me.dgvSkillName.Location = New System.Drawing.Point(374, 323)
        Me.dgvSkillName.Name = "dgvSkillName"
        Me.dgvSkillName.RowHeadersVisible = False
        Me.dgvSkillName.Size = New System.Drawing.Size(252, 123)
        Me.dgvSkillName.TabIndex = 470
        '
        'objdgcolhSkillCheck
        '
        Me.objdgcolhSkillCheck.Frozen = True
        Me.objdgcolhSkillCheck.HeaderText = ""
        Me.objdgcolhSkillCheck.Name = "objdgcolhSkillCheck"
        Me.objdgcolhSkillCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhSkillCheck.Width = 25
        '
        'dgcolhSkillName
        '
        Me.dgcolhSkillName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhSkillName.HeaderText = "Skill Name"
        Me.dgcolhSkillName.Name = "dgcolhSkillName"
        Me.dgcolhSkillName.ReadOnly = True
        '
        'objdgcolhSkillId
        '
        Me.objdgcolhSkillId.HeaderText = "objdgcolhSkillId"
        Me.objdgcolhSkillId.Name = "objdgcolhSkillId"
        Me.objdgcolhSkillId.ReadOnly = True
        Me.objdgcolhSkillId.Visible = False
        '
        'objdgcolhCategoryName
        '
        Me.objdgcolhCategoryName.HeaderText = "objdgcolhCategoryName"
        Me.objdgcolhCategoryName.Name = "objdgcolhCategoryName"
        Me.objdgcolhCategoryName.Visible = False
        '
        'objdgcolhCategoryId
        '
        Me.objdgcolhCategoryId.HeaderText = "objdgcolhCategoryId"
        Me.objdgcolhCategoryId.Name = "objdgcolhCategoryId"
        Me.objdgcolhCategoryId.Visible = False
        '
        'chkSkillCategory
        '
        Me.chkSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSkillCategory.Location = New System.Drawing.Point(107, 328)
        Me.chkSkillCategory.Name = "chkSkillCategory"
        Me.chkSkillCategory.Size = New System.Drawing.Size(13, 13)
        Me.chkSkillCategory.TabIndex = 40
        Me.chkSkillCategory.UseVisualStyleBackColor = True
        '
        'dgvSkillCategory
        '
        Me.dgvSkillCategory.AllowUserToAddRows = False
        Me.dgvSkillCategory.AllowUserToDeleteRows = False
        Me.dgvSkillCategory.AllowUserToResizeRows = False
        Me.dgvSkillCategory.BackgroundColor = System.Drawing.Color.White
        Me.dgvSkillCategory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvSkillCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSkillCategory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSCheck, Me.dgcolhSkillCategory, Me.objdgcolhSkillCategoryId})
        Me.dgvSkillCategory.Location = New System.Drawing.Point(100, 323)
        Me.dgvSkillCategory.Name = "dgvSkillCategory"
        Me.dgvSkillCategory.RowHeadersVisible = False
        Me.dgvSkillCategory.Size = New System.Drawing.Size(264, 123)
        Me.dgvSkillCategory.TabIndex = 469
        '
        'objdgcolhSCheck
        '
        Me.objdgcolhSCheck.Frozen = True
        Me.objdgcolhSCheck.HeaderText = ""
        Me.objdgcolhSCheck.Name = "objdgcolhSCheck"
        Me.objdgcolhSCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhSCheck.Width = 25
        '
        'dgcolhSkillCategory
        '
        Me.dgcolhSkillCategory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhSkillCategory.HeaderText = "Skill Category"
        Me.dgcolhSkillCategory.Name = "dgcolhSkillCategory"
        Me.dgcolhSkillCategory.ReadOnly = True
        '
        'objdgcolhSkillCategoryId
        '
        Me.objdgcolhSkillCategoryId.HeaderText = "objdgcolhSkillCategoryId"
        Me.objdgcolhSkillCategoryId.Name = "objdgcolhSkillCategoryId"
        Me.objdgcolhSkillCategoryId.ReadOnly = True
        Me.objdgcolhSkillCategoryId.Visible = False
        '
        'chkQualification
        '
        Me.chkQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkQualification.Location = New System.Drawing.Point(381, 67)
        Me.chkQualification.Name = "chkQualification"
        Me.chkQualification.Size = New System.Drawing.Size(13, 13)
        Me.chkQualification.TabIndex = 41
        Me.chkQualification.UseVisualStyleBackColor = True
        '
        'dgvQualification
        '
        Me.dgvQualification.AllowUserToAddRows = False
        Me.dgvQualification.AllowUserToDeleteRows = False
        Me.dgvQualification.AllowUserToResizeRows = False
        Me.dgvQualification.BackgroundColor = System.Drawing.Color.White
        Me.dgvQualification.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvQualification.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvQualification.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhQlCheck, Me.dgcolhQlName, Me.objdgcolhQlId})
        Me.dgvQualification.Location = New System.Drawing.Point(374, 62)
        Me.dgvQualification.Name = "dgvQualification"
        Me.dgvQualification.RowHeadersVisible = False
        Me.dgvQualification.Size = New System.Drawing.Size(252, 93)
        Me.dgvQualification.TabIndex = 468
        '
        'objdgcolhQlCheck
        '
        Me.objdgcolhQlCheck.Frozen = True
        Me.objdgcolhQlCheck.HeaderText = ""
        Me.objdgcolhQlCheck.Name = "objdgcolhQlCheck"
        Me.objdgcolhQlCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhQlCheck.Width = 25
        '
        'dgcolhQlName
        '
        Me.dgcolhQlName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhQlName.HeaderText = "Qualification"
        Me.dgcolhQlName.Name = "dgcolhQlName"
        Me.dgcolhQlName.ReadOnly = True
        '
        'objdgcolhQlId
        '
        Me.objdgcolhQlId.HeaderText = "objdgcolhQlId"
        Me.objdgcolhQlId.Name = "objdgcolhQlId"
        Me.objdgcolhQlId.ReadOnly = True
        Me.objdgcolhQlId.Visible = False
        '
        'objbtnQualificationReset
        '
        Me.objbtnQualificationReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnQualificationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnQualificationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnQualificationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnQualificationReset.BorderSelected = False
        Me.objbtnQualificationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnQualificationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objbtnQualificationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnQualificationReset.Location = New System.Drawing.Point(605, 35)
        Me.objbtnQualificationReset.Name = "objbtnQualificationReset"
        Me.objbtnQualificationReset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnQualificationReset.TabIndex = 467
        '
        'objbtnQualificationSearch
        '
        Me.objbtnQualificationSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnQualificationSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnQualificationSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnQualificationSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnQualificationSearch.BorderSelected = False
        Me.objbtnQualificationSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnQualificationSearch.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.objbtnQualificationSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnQualificationSearch.Location = New System.Drawing.Point(580, 35)
        Me.objbtnQualificationSearch.Name = "objbtnQualificationSearch"
        Me.objbtnQualificationSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnQualificationSearch.TabIndex = 466
        '
        'objbtnSkillCategoryReset
        '
        Me.objbtnSkillCategoryReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSkillCategoryReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSkillCategoryReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSkillCategoryReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSkillCategoryReset.BorderSelected = False
        Me.objbtnSkillCategoryReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSkillCategoryReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objbtnSkillCategoryReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSkillCategoryReset.Location = New System.Drawing.Point(343, 296)
        Me.objbtnSkillCategoryReset.Name = "objbtnSkillCategoryReset"
        Me.objbtnSkillCategoryReset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSkillCategoryReset.TabIndex = 465
        '
        'objbtnSkillCategorySearch
        '
        Me.objbtnSkillCategorySearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSkillCategorySearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSkillCategorySearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSkillCategorySearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSkillCategorySearch.BorderSelected = False
        Me.objbtnSkillCategorySearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSkillCategorySearch.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.objbtnSkillCategorySearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSkillCategorySearch.Location = New System.Drawing.Point(318, 296)
        Me.objbtnSkillCategorySearch.Name = "objbtnSkillCategorySearch"
        Me.objbtnSkillCategorySearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSkillCategorySearch.TabIndex = 464
        '
        'objbtnResetSkills
        '
        Me.objbtnResetSkills.BackColor = System.Drawing.Color.Transparent
        Me.objbtnResetSkills.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnResetSkills.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnResetSkills.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnResetSkills.BorderSelected = False
        Me.objbtnResetSkills.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnResetSkills.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objbtnResetSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnResetSkills.Location = New System.Drawing.Point(609, 296)
        Me.objbtnResetSkills.Name = "objbtnResetSkills"
        Me.objbtnResetSkills.Size = New System.Drawing.Size(21, 21)
        Me.objbtnResetSkills.TabIndex = 463
        '
        'objbtnSeachSkills
        '
        Me.objbtnSeachSkills.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSeachSkills.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSeachSkills.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSeachSkills.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSeachSkills.BorderSelected = False
        Me.objbtnSeachSkills.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSeachSkills.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.objbtnSeachSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSeachSkills.Location = New System.Drawing.Point(584, 296)
        Me.objbtnSeachSkills.Name = "objbtnSeachSkills"
        Me.objbtnSeachSkills.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSeachSkills.TabIndex = 462
        '
        'txtSCSearch
        '
        Me.txtSCSearch.BackColor = System.Drawing.Color.White
        Me.txtSCSearch.Flags = 0
        Me.txtSCSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSCSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSCSearch.Location = New System.Drawing.Point(100, 296)
        Me.txtSCSearch.Name = "txtSCSearch"
        Me.txtSCSearch.Size = New System.Drawing.Size(214, 21)
        Me.txtSCSearch.TabIndex = 460
        '
        'txtSkillSearch
        '
        Me.txtSkillSearch.BackColor = System.Drawing.Color.White
        Me.txtSkillSearch.Flags = 0
        Me.txtSkillSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSkillSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSkillSearch.Location = New System.Drawing.Point(416, 296)
        Me.txtSkillSearch.Name = "txtSkillSearch"
        Me.txtSkillSearch.Size = New System.Drawing.Size(162, 21)
        Me.txtSkillSearch.TabIndex = 459
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(607, 11)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 432
        '
        'lblYearCondition
        '
        Me.lblYearCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearCondition.Location = New System.Drawing.Point(179, 215)
        Me.lblYearCondition.Name = "lblYearCondition"
        Me.lblYearCondition.Size = New System.Drawing.Size(51, 17)
        Me.lblYearCondition.TabIndex = 45
        Me.lblYearCondition.Text = "Condition"
        Me.lblYearCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYearCondition
        '
        Me.cboYearCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYearCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYearCondition.FormattingEnabled = True
        Me.cboYearCondition.Location = New System.Drawing.Point(232, 213)
        Me.cboYearCondition.Name = "cboYearCondition"
        Me.cboYearCondition.Size = New System.Drawing.Size(40, 21)
        Me.cboYearCondition.TabIndex = 46
        '
        'cboAwardedYear
        '
        Me.cboAwardedYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAwardedYear.DropDownWidth = 169
        Me.cboAwardedYear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAwardedYear.FormattingEnabled = True
        Me.cboAwardedYear.Location = New System.Drawing.Point(101, 213)
        Me.cboAwardedYear.Name = "cboAwardedYear"
        Me.cboAwardedYear.Size = New System.Drawing.Size(72, 21)
        Me.cboAwardedYear.TabIndex = 44
        '
        'lblAwardYear
        '
        Me.lblAwardYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAwardYear.Location = New System.Drawing.Point(9, 215)
        Me.lblAwardYear.Name = "lblAwardYear"
        Me.lblAwardYear.Size = New System.Drawing.Size(84, 17)
        Me.lblAwardYear.TabIndex = 43
        Me.lblAwardYear.Text = "Awarded Year"
        Me.lblAwardYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacanyType
        '
        Me.lblVacanyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacanyType.Location = New System.Drawing.Point(9, 13)
        Me.lblVacanyType.Name = "lblVacanyType"
        Me.lblVacanyType.Size = New System.Drawing.Size(84, 17)
        Me.lblVacanyType.TabIndex = 33
        Me.lblVacanyType.Text = "Vacancy Type"
        Me.lblVacanyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudGPA
        '
        Me.nudGPA.DecimalPlaces = 4
        Me.nudGPA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudGPA.Location = New System.Drawing.Point(101, 187)
        Me.nudGPA.Name = "nudGPA"
        Me.nudGPA.Size = New System.Drawing.Size(72, 20)
        Me.nudGPA.TabIndex = 14
        Me.nudGPA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGPACondition
        '
        Me.lblGPACondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGPACondition.Location = New System.Drawing.Point(179, 189)
        Me.lblGPACondition.Name = "lblGPACondition"
        Me.lblGPACondition.Size = New System.Drawing.Size(51, 17)
        Me.lblGPACondition.TabIndex = 15
        Me.lblGPACondition.Text = "Condition"
        Me.lblGPACondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResultLvlCondition
        '
        Me.cboResultLvlCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultLvlCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultLvlCondition.FormattingEnabled = True
        Me.cboResultLvlCondition.Location = New System.Drawing.Point(519, 161)
        Me.cboResultLvlCondition.Name = "cboResultLvlCondition"
        Me.cboResultLvlCondition.Size = New System.Drawing.Size(107, 21)
        Me.cboResultLvlCondition.TabIndex = 12
        '
        'lblGPA
        '
        Me.lblGPA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGPA.Location = New System.Drawing.Point(9, 189)
        Me.lblGPA.Name = "lblGPA"
        Me.lblGPA.Size = New System.Drawing.Size(84, 17)
        Me.lblGPA.TabIndex = 13
        Me.lblGPA.Text = "GPA"
        Me.lblGPA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 300
        Me.cboVacancyType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(101, 11)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(169, 21)
        Me.cboVacancyType.TabIndex = 34
        '
        'cboGPACondition
        '
        Me.cboGPACondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGPACondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGPACondition.FormattingEnabled = True
        Me.cboGPACondition.Location = New System.Drawing.Point(232, 187)
        Me.cboGPACondition.Name = "cboGPACondition"
        Me.cboGPACondition.Size = New System.Drawing.Size(40, 21)
        Me.cboGPACondition.TabIndex = 16
        '
        'lblAge
        '
        Me.lblAge.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Location = New System.Drawing.Point(287, 189)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(81, 17)
        Me.lblAge.TabIndex = 17
        Me.lblAge.Text = "Age"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtResultLevel
        '
        Me.txtResultLevel.AllowNegative = False
        Me.txtResultLevel.BackColor = System.Drawing.Color.White
        Me.txtResultLevel.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtResultLevel.DigitsInGroup = 0
        Me.txtResultLevel.Flags = 65536
        Me.txtResultLevel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResultLevel.Location = New System.Drawing.Point(374, 161)
        Me.txtResultLevel.MaxDecimalPlaces = 0
        Me.txtResultLevel.MaxWholeDigits = 6
        Me.txtResultLevel.Name = "txtResultLevel"
        Me.txtResultLevel.Prefix = ""
        Me.txtResultLevel.RangeMax = 999999
        Me.txtResultLevel.RangeMin = 1
        Me.txtResultLevel.ReadOnly = True
        Me.txtResultLevel.Size = New System.Drawing.Size(141, 20)
        Me.txtResultLevel.TabIndex = 11
        Me.txtResultLevel.Text = "0"
        Me.txtResultLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 360
        Me.cboVacancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(374, 11)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(227, 21)
        Me.cboVacancy.TabIndex = 28
        '
        'nudAge
        '
        Me.nudAge.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAge.Location = New System.Drawing.Point(374, 187)
        Me.nudAge.Name = "nudAge"
        Me.nudAge.Size = New System.Drawing.Size(71, 20)
        Me.nudAge.TabIndex = 18
        Me.nudAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(287, 162)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(81, 17)
        Me.lblLevel.TabIndex = 10
        Me.lblLevel.Text = "Level"
        Me.lblLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(287, 12)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(80, 18)
        Me.lblVacancy.TabIndex = 27
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAgeCondition
        '
        Me.lblAgeCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgeCondition.Location = New System.Drawing.Point(452, 189)
        Me.lblAgeCondition.Name = "lblAgeCondition"
        Me.lblAgeCondition.Size = New System.Drawing.Size(63, 17)
        Me.lblAgeCondition.TabIndex = 19
        Me.lblAgeCondition.Text = "Condition"
        Me.lblAgeCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudLevel
        '
        Me.nudLevel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLevel.Location = New System.Drawing.Point(101, 40)
        Me.nudLevel.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.nudLevel.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.nudLevel.Name = "nudLevel"
        Me.nudLevel.Size = New System.Drawing.Size(72, 20)
        Me.nudLevel.TabIndex = 3
        Me.nudLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboResultCode
        '
        Me.cboResultCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultCode.DropDownWidth = 300
        Me.cboResultCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultCode.FormattingEnabled = True
        Me.cboResultCode.Location = New System.Drawing.Point(101, 161)
        Me.cboResultCode.Name = "cboResultCode"
        Me.cboResultCode.Size = New System.Drawing.Size(169, 21)
        Me.cboResultCode.TabIndex = 9
        '
        'lblQualificationGrpLevel
        '
        Me.lblQualificationGrpLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGrpLevel.Location = New System.Drawing.Point(9, 40)
        Me.lblQualificationGrpLevel.Name = "lblQualificationGrpLevel"
        Me.lblQualificationGrpLevel.Size = New System.Drawing.Size(84, 17)
        Me.lblQualificationGrpLevel.TabIndex = 2
        Me.lblQualificationGrpLevel.Text = "Qua. Grp. Level"
        Me.lblQualificationGrpLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAgeCondition
        '
        Me.cboAgeCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAgeCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAgeCondition.FormattingEnabled = True
        Me.cboAgeCondition.Location = New System.Drawing.Point(528, 188)
        Me.cboAgeCondition.Name = "cboAgeCondition"
        Me.cboAgeCondition.Size = New System.Drawing.Size(52, 21)
        Me.cboAgeCondition.TabIndex = 20
        '
        'lblSkill
        '
        Me.lblSkill.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkill.Location = New System.Drawing.Point(375, 297)
        Me.lblSkill.Name = "lblSkill"
        Me.lblSkill.Size = New System.Drawing.Size(35, 18)
        Me.lblSkill.TabIndex = 25
        Me.lblSkill.Text = "Skill"
        Me.lblSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblResultCode
        '
        Me.lblResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultCode.Location = New System.Drawing.Point(9, 163)
        Me.lblResultCode.Name = "lblResultCode"
        Me.lblResultCode.Size = New System.Drawing.Size(84, 17)
        Me.lblResultCode.TabIndex = 8
        Me.lblResultCode.Text = "Result Code"
        Me.lblResultCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQualificationgrp
        '
        Me.lblQualificationgrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationgrp.Location = New System.Drawing.Point(9, 66)
        Me.lblQualificationgrp.Name = "lblQualificationgrp"
        Me.lblQualificationgrp.Size = New System.Drawing.Size(84, 17)
        Me.lblQualificationgrp.TabIndex = 4
        Me.lblQualificationgrp.Text = "Qua. Grp."
        Me.lblQualificationgrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(9, 242)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(84, 17)
        Me.lblGender.TabIndex = 21
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualificationGrp
        '
        Me.cboQualificationGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualificationGrp.DropDownWidth = 300
        Me.cboQualificationGrp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualificationGrp.FormattingEnabled = True
        Me.cboQualificationGrp.Location = New System.Drawing.Point(101, 64)
        Me.cboQualificationGrp.Name = "cboQualificationGrp"
        Me.cboQualificationGrp.Size = New System.Drawing.Size(169, 21)
        Me.cboQualificationGrp.TabIndex = 5
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(101, 240)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(169, 21)
        Me.cboGender.TabIndex = 22
        '
        'lblSkillCategory
        '
        Me.lblSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillCategory.Location = New System.Drawing.Point(8, 298)
        Me.lblSkillCategory.Name = "lblSkillCategory"
        Me.lblSkillCategory.Size = New System.Drawing.Size(84, 17)
        Me.lblSkillCategory.TabIndex = 23
        Me.lblSkillCategory.Text = "Skill Category"
        Me.lblSkillCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(287, 38)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(82, 17)
        Me.lblQualification.TabIndex = 6
        Me.lblQualification.Text = "Qualification"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherResultCode
        '
        Me.txtOtherResultCode.Flags = 0
        Me.txtOtherResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherResultCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherResultCode.Location = New System.Drawing.Point(101, 161)
        Me.txtOtherResultCode.Name = "txtOtherResultCode"
        Me.txtOtherResultCode.Size = New System.Drawing.Size(169, 21)
        Me.txtOtherResultCode.TabIndex = 229
        '
        'txtQSearch
        '
        Me.txtQSearch.BackColor = System.Drawing.Color.White
        Me.txtQSearch.Flags = 0
        Me.txtQSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtQSearch.Location = New System.Drawing.Point(374, 35)
        Me.txtQSearch.Name = "txtQSearch"
        Me.txtQSearch.Size = New System.Drawing.Size(202, 21)
        Me.txtQSearch.TabIndex = 458
        '
        'lblQualificationGroupCondition
        '
        Me.lblQualificationGroupCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGroupCondition.Location = New System.Drawing.Point(4, 105)
        Me.lblQualificationGroupCondition.Name = "lblQualificationGroupCondition"
        Me.lblQualificationGroupCondition.Size = New System.Drawing.Size(51, 18)
        Me.lblQualificationGroupCondition.TabIndex = 483
        Me.lblQualificationGroupCondition.Text = "Condition"
        Me.lblQualificationGroupCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualificationGroupCondition
        '
        Me.cboQualificationGroupCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualificationGroupCondition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualificationGroupCondition.FormattingEnabled = True
        Me.cboQualificationGroupCondition.Location = New System.Drawing.Point(55, 105)
        Me.cboQualificationGroupCondition.Name = "cboQualificationGroupCondition"
        Me.cboQualificationGroupCondition.Size = New System.Drawing.Size(40, 21)
        Me.cboQualificationGroupCondition.TabIndex = 484
        '
        'chkInvalidCode
        '
        Me.chkInvalidCode.BackColor = System.Drawing.Color.Transparent
        Me.chkInvalidCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInvalidCode.Location = New System.Drawing.Point(462, 4)
        Me.chkInvalidCode.Name = "chkInvalidCode"
        Me.chkInvalidCode.Size = New System.Drawing.Size(179, 17)
        Me.chkInvalidCode.TabIndex = 36
        Me.chkInvalidCode.Text = "Ignore Invalid Employee Code"
        Me.chkInvalidCode.UseVisualStyleBackColor = False
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.lblCaption)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 539)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(970, 55)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(555, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 4
        Me.btnReset.Text = "R&eset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'lblCaption
        '
        Me.lblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(9, 13)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(518, 30)
        Me.lblCaption.TabIndex = 3
        Me.lblCaption.Text = "Shortlist with multiple data (Qualif. Group,Qualification,Skill Category,Skill,Re" & _
            "sult Code) at a time. Separate with a comma (,)"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCaption.Visible = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(747, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(118, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save And Export"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(871, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(651, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 0
        Me.btnExport.Text = "E&xport"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "First Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Other Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Last Name"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "GPA"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 60
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "GPA Condition"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Age"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 60
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Age Condition"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        Me.DataGridViewTextBoxColumn12.Width = 75
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "SKill Category"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Width = 125
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Skill"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Width = 125
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        Me.DataGridViewTextBoxColumn15.Width = 75
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn16.FillWeight = 123.8579!
        Me.DataGridViewTextBoxColumn16.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn17.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn18.FillWeight = 123.8579!
        Me.DataGridViewTextBoxColumn18.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn19.FillWeight = 123.8579!
        Me.DataGridViewTextBoxColumn19.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "GUID"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Filter"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "GUID"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "Filter"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn24.HeaderText = "Skill Name"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn25.HeaderText = "objdgcolhSkillId"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn26.HeaderText = "Skill Category"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn26.Visible = False
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn27.HeaderText = "objdgcolhSkillCategoryId"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.Visible = False
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn28.HeaderText = "Qualification"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.Visible = False
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn29.HeaderText = "objdgcolhQlId"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.Visible = False
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn30.HeaderText = "Qualification"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn31.HeaderText = "objdgcolhQlId"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "objdgcolhQlId"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        Me.DataGridViewTextBoxColumn32.Visible = False
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn33.HeaderText = "Qualification"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "objdgcolhQlId"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn35.HeaderText = "Qualification"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.ReadOnly = True
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.HeaderText = "objdgcolhQlId"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        Me.DataGridViewTextBoxColumn36.ReadOnly = True
        Me.DataGridViewTextBoxColumn36.Visible = False
        '
        'colhVacancy
        '
        Me.colhVacancy.HeaderText = "Vacancy"
        Me.colhVacancy.Name = "colhVacancy"
        Me.colhVacancy.ReadOnly = True
        Me.colhVacancy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhVacancy.Width = 101
        '
        'colhQlGrpLevel
        '
        Me.colhQlGrpLevel.HeaderText = "Ql.Grp Level"
        Me.colhQlGrpLevel.Name = "colhQlGrpLevel"
        Me.colhQlGrpLevel.ReadOnly = True
        Me.colhQlGrpLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhQlGrpLevel.Width = 75
        '
        'colhQlGrp
        '
        Me.colhQlGrp.HeaderText = "Qul. Group"
        Me.colhQlGrp.Name = "colhQlGrp"
        Me.colhQlGrp.ReadOnly = True
        Me.colhQlGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhQlGrp.Width = 75
        '
        'colhdgQualification
        '
        Me.colhdgQualification.HeaderText = "Qualification"
        Me.colhdgQualification.Name = "colhdgQualification"
        Me.colhdgQualification.ReadOnly = True
        Me.colhdgQualification.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhdgQualification.Width = 75
        '
        'colhResultCode
        '
        Me.colhResultCode.HeaderText = "Result Code"
        Me.colhResultCode.Name = "colhResultCode"
        Me.colhResultCode.ReadOnly = True
        Me.colhResultCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhResultCode.Width = 75
        '
        'colhRLevel
        '
        Me.colhRLevel.HeaderText = "Result Level"
        Me.colhRLevel.Name = "colhRLevel"
        Me.colhRLevel.ReadOnly = True
        Me.colhRLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhRLevel.Width = 75
        '
        'colhRCondition
        '
        Me.colhRCondition.HeaderText = "R.Condition"
        Me.colhRCondition.Name = "colhRCondition"
        Me.colhRCondition.ReadOnly = True
        Me.colhRCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhRCondition.Visible = False
        Me.colhRCondition.Width = 75
        '
        'colhGPA
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colhGPA.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhGPA.HeaderText = "GPA"
        Me.colhGPA.Name = "colhGPA"
        Me.colhGPA.ReadOnly = True
        Me.colhGPA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhGPA.Width = 60
        '
        'colhGPACondition
        '
        Me.colhGPACondition.HeaderText = "GPA Condition"
        Me.colhGPACondition.Name = "colhGPACondition"
        Me.colhGPACondition.ReadOnly = True
        Me.colhGPACondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhGPACondition.Visible = False
        '
        'colhAge
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.colhAge.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhAge.HeaderText = "Age"
        Me.colhAge.Name = "colhAge"
        Me.colhAge.ReadOnly = True
        Me.colhAge.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhAge.Width = 60
        '
        'colhAgeCondition
        '
        Me.colhAgeCondition.HeaderText = "Age Condition"
        Me.colhAgeCondition.Name = "colhAgeCondition"
        Me.colhAgeCondition.ReadOnly = True
        Me.colhAgeCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhAgeCondition.Visible = False
        '
        'colhAwardYear
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F0"
        Me.colhAwardYear.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhAwardYear.HeaderText = "Awarded Year"
        Me.colhAwardYear.Name = "colhAwardYear"
        Me.colhAwardYear.ReadOnly = True
        Me.colhAwardYear.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'colhYearCondition
        '
        Me.colhYearCondition.HeaderText = "Year Condition"
        Me.colhYearCondition.Name = "colhYearCondition"
        Me.colhYearCondition.ReadOnly = True
        Me.colhYearCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colhYearCondition.Visible = False
        '
        'colhGender
        '
        Me.colhGender.HeaderText = "Gender"
        Me.colhGender.Name = "colhGender"
        Me.colhGender.ReadOnly = True
        Me.colhGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhGender.Width = 75
        '
        'colhFilterSkillCategory
        '
        Me.colhFilterSkillCategory.HeaderText = "SKill Category"
        Me.colhFilterSkillCategory.Name = "colhFilterSkillCategory"
        Me.colhFilterSkillCategory.ReadOnly = True
        Me.colhFilterSkillCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterSkillCategory.Width = 125
        '
        'colhFilterSkill
        '
        Me.colhFilterSkill.HeaderText = "Skill"
        Me.colhFilterSkill.Name = "colhFilterSkill"
        Me.colhFilterSkill.ReadOnly = True
        Me.colhFilterSkill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFilterSkill.Width = 125
        '
        'colhFilterOtherQuliGrp
        '
        Me.colhFilterOtherQuliGrp.HeaderText = "Other Qul. Group"
        Me.colhFilterOtherQuliGrp.Name = "colhFilterOtherQuliGrp"
        Me.colhFilterOtherQuliGrp.ReadOnly = True
        Me.colhFilterOtherQuliGrp.Width = 125
        '
        'colhFilterOtherQualification
        '
        Me.colhFilterOtherQualification.HeaderText = "Other Qualification"
        Me.colhFilterOtherQualification.Name = "colhFilterOtherQualification"
        Me.colhFilterOtherQualification.ReadOnly = True
        Me.colhFilterOtherQualification.Width = 125
        '
        'colhFilterOtherResultCode
        '
        Me.colhFilterOtherResultCode.HeaderText = "Other Result Code"
        Me.colhFilterOtherResultCode.Name = "colhFilterOtherResultCode"
        Me.colhFilterOtherResultCode.ReadOnly = True
        Me.colhFilterOtherResultCode.Width = 125
        '
        'colhFilterOtherSkillCategory
        '
        Me.colhFilterOtherSkillCategory.HeaderText = "Other Skill Category"
        Me.colhFilterOtherSkillCategory.Name = "colhFilterOtherSkillCategory"
        Me.colhFilterOtherSkillCategory.ReadOnly = True
        Me.colhFilterOtherSkillCategory.Width = 125
        '
        'colhFilterOtherSkill
        '
        Me.colhFilterOtherSkill.HeaderText = "Other Skill"
        Me.colhFilterOtherSkill.Name = "colhFilterOtherSkill"
        Me.colhFilterOtherSkill.ReadOnly = True
        '
        'objcolhFilterunkid
        '
        Me.objcolhFilterunkid.HeaderText = "Filterunkid"
        Me.objcolhFilterunkid.Name = "objcolhFilterunkid"
        Me.objcolhFilterunkid.ReadOnly = True
        Me.objcolhFilterunkid.Visible = False
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "GUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.ReadOnly = True
        Me.objcolhGUID.Visible = False
        '
        'objcolhFilter
        '
        Me.objcolhFilter.HeaderText = "Filter"
        Me.objcolhFilter.Name = "objcolhFilter"
        Me.objcolhFilter.ReadOnly = True
        Me.objcolhFilter.Visible = False
        '
        'colhNationality
        '
        Me.colhNationality.HeaderText = "Nationality"
        Me.colhNationality.Name = "colhNationality"
        Me.colhNationality.ReadOnly = True
        Me.colhNationality.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhNationality.Width = 75
        '
        'colhExperience
        '
        Me.colhExperience.HeaderText = "Experience"
        Me.colhExperience.Name = "colhExperience"
        Me.colhExperience.ReadOnly = True
        Me.colhExperience.Width = 60
        '
        'colhExperienceCondition
        '
        Me.colhExperienceCondition.HeaderText = "Experience Condition"
        Me.colhExperienceCondition.Name = "colhExperienceCondition"
        Me.colhExperienceCondition.ReadOnly = True
        Me.colhExperienceCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhExperienceCondition.Visible = False
        '
        'colhAptitudeResult
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.colhAptitudeResult.DefaultCellStyle = DataGridViewCellStyle4
        Me.colhAptitudeResult.HeaderText = "Aptitude Result"
        Me.colhAptitudeResult.Name = "colhAptitudeResult"
        Me.colhAptitudeResult.ReadOnly = True
        Me.colhAptitudeResult.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhAptitudeCondition
        '
        Me.colhAptitudeCondition.HeaderText = "Aptitude Condition"
        Me.colhAptitudeCondition.Name = "colhAptitudeCondition"
        Me.colhAptitudeCondition.ReadOnly = True
        Me.colhAptitudeCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhAptitudeCondition.Visible = False
        '
        'colhResidenceType
        '
        Me.colhResidenceType.HeaderText = "Residence Type"
        Me.colhResidenceType.Name = "colhResidenceType"
        Me.colhResidenceType.ReadOnly = True
        Me.colhResidenceType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhResidenceType.Visible = False
        '
        'frmApplicantFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(970, 594)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantFilter"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Applicant Filter"
        Me.pnlMain.ResumeLayout(False)
        Me.grpCondition.ResumeLayout(False)
        Me.gbShortListedApplicant.ResumeLayout(False)
        Me.gbShortListedApplicant.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgFilter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlResidenceType.ResumeLayout(False)
        CType(Me.nudAptitudeResult, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudExperienceMonths, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudExperienceYears, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOtherQualification.ResumeLayout(False)
        Me.pnlOtherQualification.PerformLayout()
        CType(Me.dgvQualificationGroup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSkillName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSkillCategory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvQualification, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudGPA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudAge, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents lblQualificationGrpLevel As System.Windows.Forms.Label
    Friend WithEvents nudLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboQualificationGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualificationgrp As System.Windows.Forms.Label
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents cboResultCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultCode As System.Windows.Forms.Label
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents txtResultLevel As eZee.TextBox.NumericTextBox
    Friend WithEvents nudGPA As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblGPA As System.Windows.Forms.Label
    Friend WithEvents cboAgeCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblAgeCondition As System.Windows.Forms.Label
    Friend WithEvents nudAge As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents cboGPACondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblGPACondition As System.Windows.Forms.Label
    Friend WithEvents cboResultLvlCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkillCategory As System.Windows.Forms.Label
    Friend WithEvents lblSkill As System.Windows.Forms.Label
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents dgFilter As System.Windows.Forms.DataGridView
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnFilter As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacanyType As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbShortListedApplicant As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lvApplicant As eZee.Common.eZeeListView
    Friend WithEvents colhAppliQlfication As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApplicantCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApplicant As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkInvalidCode As System.Windows.Forms.CheckBox
    Friend WithEvents grpCondition As System.Windows.Forms.GroupBox
    Friend WithEvents rdANDCondition As System.Windows.Forms.RadioButton
    Friend WithEvents rdORCondition As System.Windows.Forms.RadioButton
    Friend WithEvents chkSkillCategory As System.Windows.Forms.CheckBox
    Friend WithEvents chkSkill As System.Windows.Forms.CheckBox
    Friend WithEvents chkQualification As System.Windows.Forms.CheckBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents colhEmail As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblYearCondition As System.Windows.Forms.Label
    Friend WithEvents cboYearCondition As System.Windows.Forms.ComboBox
    Friend WithEvents cboAwardedYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblAwardYear As System.Windows.Forms.Label
    Friend WithEvents pnlOtherQualification As System.Windows.Forms.Panel
    Friend WithEvents txtOtherSkillCategory As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOtherSkillCategory As System.Windows.Forms.Label
    Friend WithEvents txtOtherQualification As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherQualificationGrp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOtherQualificationGrp As System.Windows.Forms.Label
    Friend WithEvents lblOtherQualification As System.Windows.Forms.Label
    Friend WithEvents lblOtherSkill As System.Windows.Forms.Label
    Friend WithEvents txtOtherSkill As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkOtherQualification As System.Windows.Forms.CheckBox
    Friend WithEvents txtOtherResultCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtQSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSCSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSkillSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnResetSkills As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSeachSkills As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSkillCategoryReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSkillCategorySearch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnQualificationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnQualificationSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents dgvQualification As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSkillCategory As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSkillName As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhSkillCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSkillCategoryId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhQlCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhQlName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhQlId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSkillCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhSkillName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSkillId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCategoryName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCategoryId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblNationality As System.Windows.Forms.Label
    Friend WithEvents cboNationality As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents lblQualificationGrpCondition As System.Windows.Forms.Label
    Friend WithEvents cboQualificationGrpCondition As System.Windows.Forms.ComboBox
    Friend WithEvents dgvQualificationGroup As System.Windows.Forms.DataGridView
    Friend WithEvents chkQualificationGroup As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblExperienceMonths As System.Windows.Forms.Label
    Friend WithEvents lblExperienceYears As System.Windows.Forms.Label
    Friend WithEvents cboExperienceCondition As System.Windows.Forms.ComboBox
    Friend WithEvents nudExperienceMonths As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblExperience As System.Windows.Forms.Label
    Friend WithEvents nudExperienceYears As System.Windows.Forms.NumericUpDown
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblQualificationGroupCondition As System.Windows.Forms.Label
    Friend WithEvents cboQualificationGroupCondition As System.Windows.Forms.ComboBox
    Friend WithEvents objdgcolhQlGrpCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhQlGrpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhQlGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nudAptitudeResult As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblAptitudeCondition As System.Windows.Forms.Label
    Friend WithEvents LblAptitudeResult As System.Windows.Forms.Label
    Friend WithEvents cboAptitudeResultCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblResidenceType As System.Windows.Forms.Label
    Friend WithEvents cboResidenceType As System.Windows.Forms.ComboBox
    Friend WithEvents pnlResidenceType As System.Windows.Forms.Panel
    Friend WithEvents colhVacancy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhQlGrpLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhQlGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhResultCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhRLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhRCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGPA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGPACondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAge As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAgeCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAwardYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhYearCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterSkillCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterSkill As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherQuliGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherResultCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherSkillCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFilterOtherSkill As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhFilterunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhFilter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhNationality As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhExperience As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhExperienceCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAptitudeResult As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAptitudeCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhResidenceType As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
