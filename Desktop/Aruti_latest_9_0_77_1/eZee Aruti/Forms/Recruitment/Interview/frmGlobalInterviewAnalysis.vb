﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmGlobalInterviewAnalysis
#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGlobalInterviewAnalysis"
    Private objApplicantBatchInteview As clsApplicant_Batchschedule_Tran
    Private objAnalysisMaster As clsInterviewAnalysis_master
    Private objBatchScheduleInterviewer_tran As clsBatchSchedule_interviewer_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAnalysisMasterUnkid As Integer = -1
    Private mintResultGroup As Integer = -1
    Private mintAppBatchScheduleTranId As Integer = -1
    Private mintAnalysisTranId As Integer = -1
    Private mintVacancyunkid As Integer = -1
    Private mintApplicantUnkid As Integer = 0
    Private mstrReport_GroupName As String = ""
    Private mblnCancel As Boolean = True
    Private mdtDataView As DataView
    Private mdicInterviewer As New Dictionary(Of Integer, String)
    Private mdtTable As DataTable
    Private mdtTranDataView As DataView
    Private mdtTranTable As DataTable
    Private mdsInterviewList As DataSet
#End Region

#Region " Display Dialog "

#End Region

#Region " Form's Events "
    Private Sub frmGlobalInterviewAnalysis_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            objApplicantBatchInteview = New clsApplicant_Batchschedule_Tran
            objAnalysisMaster = New clsInterviewAnalysis_master
            objBatchScheduleInterviewer_tran = New clsBatchSchedule_interviewer_tran

            chkIncomplete.Checked = True

            Call FillCombo()
            objbtnInterviewer.Enabled = False


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalInterviewAnalysis_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalInterviewAnalysis_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAnalysisMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsInterviewAnalysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsInterviewAnalysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim objInterviewType As New clsCommon_Master
        Dim objResult As New clsresult_master
        Dim objVac As New clsVacancy
        Dim dsList As New DataSet
        Try
            dsList = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True)
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objVac.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objResult.getComboList("List", True, mintResultGroup)
            With cboResult
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objInterviewType = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objResult As New clsresult_master
        Dim dsList As New DataSet
        Dim dsInterviewerList As New DataSet
        Dim strSearching As String = ""
        Dim dtTable As DataTable
        Dim dsCombo As DataSet
        Dim dCol As DataGridViewTextBoxColumn
        Dim dColCombo As DataGridViewComboBoxColumn
        Dim dColChk As DataGridViewCheckBoxColumn
        Try

            If User._Object.Privilege._AllowtoViewGlobalInterviewAnalysisList = True Then

                If CInt(cboBatch.SelectedValue) > 0 Then
                    strSearching &= "AND rcapplicant_batchschedule_tran.batchscheduleunkid = " & CInt(cboBatch.SelectedValue) & " "
                End If

                If CInt(cboInterviewType.SelectedValue) > 0 Then
                    strSearching &= "AND rcbatchschedule_master.interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " "
                End If

                If CInt(cboVacancy.SelectedValue) > 0 Then
                    strSearching &= "AND rcbatchschedule_master.vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " "
                End If


                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                End If

                dsList = objApplicantBatchInteview.GetList_InterviewAnalysis("List", , , strSearching)
                For Each drIntviewer As DataRow In mdsInterviewList.Tables(0).Select("Id <= -1")
                    For Each drTran As DataRow In dsList.Tables(1).Select("batchscheduleinterviewertranunkid =" & CInt(drIntviewer("batchscheduleinterviewertranunkid")))
                        drTran("interviewerunkid") = drIntviewer("Id")
                    Next
                Next

                With dgvData
                    .DataSource = Nothing
                    .AutoGenerateColumns = False
                    .IgnoreFirstColumn = True
                    .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
                    If .Columns.Count <= 0 Then
                        .ColumnHeadersHeight = .ColumnHeadersHeight * 2
                    End If
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter

                    If Not .DataSource Is Nothing Then .DataSource = Nothing
                    .Columns.Clear()
                    .Rows.Clear()


                    dColChk = New DataGridViewCheckBoxColumn
                    dColChk.Name = "colhIsChecked"
                    dColChk.HeaderText = ""
                    dColChk.SortMode = DataGridViewColumnSortMode.NotSortable
                    dColChk.Width = 25
                    dColChk.Frozen = True
                    dColChk.Visible = True
                    dColChk.ReadOnly = False
                    dColChk.DataPropertyName = "IsChecked"
                    .Columns.Add(dColChk)


                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhApplicantUnkID"
                    dCol.HeaderText = mstrReport_GroupName.Replace(" :", "")
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 100
                    dCol.Frozen = True
                    dCol.Visible = False
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "ApplicantunkId"
                    .Columns.Add(dCol)

                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhApplicantName"
                    dCol.HeaderText = "Applicant Name"
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 225
                    dCol.Frozen = True
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "ApplicantName"
                    .Columns.Add(dCol)

                    'dsInterviewerList = objBatchScheduleInterviewer_tran.getComboList(CInt(cboBatch.SelectedValue), False, "List", , , _
                    '" AND rcbatchschedule_interviewer_tran.interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " ")
                    Dim dView = New DataView(mdsInterviewList.Tables(0).Copy, "Id <> 0", "", DataViewRowState.CurrentRows)
                    dsInterviewerList.Tables.Add(dView.ToTable)


                    Dim dColumn As DataColumn
                    dColumn = New DataColumn("IsChecked")
                    dColumn.DataType = System.Type.GetType("System.Boolean")
                    dColumn.DefaultValue = False
                    dColumn.Caption = "IsChecked"
                    dsList.Tables(0).Columns.Add(dColumn)

                    dColumn = New DataColumn("analysis_date")
                    dColumn.DataType = System.Type.GetType("System.DateTime")
                    dColumn.DefaultValue = Now.Date
                    dsList.Tables(0).Columns.Add(dColumn)

                    If dsInterviewerList.Tables("List").Rows.Count > 0 Then
                        For Each dtRow As DataRow In dsInterviewerList.Tables("List").Rows

                            dColumn = New DataColumn("|_" & dtRow.Item("Id").ToString)
                            dColumn.DataType = System.Type.GetType("System.Int32")
                            dColumn.DefaultValue = 0
                            dColumn.Caption = "|_" & dtRow.Item("Id").ToString
                            dsList.Tables(0).Columns.Add(dColumn)

                            dColumn = New DataColumn("||_" & dtRow.Item("Id").ToString)
                            dColumn.DataType = System.Type.GetType("System.String")
                            dColumn.DefaultValue = ""
                            dColumn.Caption = "||_" & dtRow.Item("Id").ToString
                            dsList.Tables(0).Columns.Add(dColumn)
                        Next
                    End If
                    mdicInterviewer = (From p In dsInterviewerList.Tables(0) Select New With {.Id = CInt(p.Item("Id")), .Name = CStr(p.Item("Name"))}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
                    For Each dsRow As DataRow In dsInterviewerList.Tables(0).Rows


                        'Adding DataGrid Columns
                        'Hemant (19 Jun 2020) -- Start
                        'dsCombo = objResult.getComboList("Score", True, -1)
                        dsCombo = objResult.getComboList("Score", True, mintResultGroup)
                        'Hemant (19 Jun 2020) -- End
                        
                        dColCombo = New DataGridViewComboBoxColumn
                        dColCombo.Name = "colhScore" & dsRow.Item("Id").ToString
                        dColCombo.HeaderText = "Score"
                        dColCombo.Tag = dsRow.Item("Name").ToString
                        dColCombo.SortMode = DataGridViewColumnSortMode.NotSortable
                        dColCombo.Width = 100
                        dColCombo.Frozen = False
                        dColCombo.ReadOnly = False
                        dColCombo.DataPropertyName = "|_" & dsRow.Item("Id").ToString
                        dColCombo.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                        dColCombo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                        dColCombo.ValueMember = "Id"

                        dColCombo.DisplayMember = "Name"
                        dColCombo.DataSource = dsCombo.Tables(0)
                        dColCombo.DataPropertyName = "|_" & dsRow.Item("Id").ToString
                        dColCombo.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                        .Columns.Add(dColCombo)

                        dCol = New DataGridViewTextBoxColumn
                        dCol.Name = "colhRemark" & dsRow.Item("Id").ToString
                        dCol.HeaderText = "Remark"
                        dCol.Tag = dsRow.Item("Name").ToString
                        dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                        dCol.Width = 100
                        dCol.Frozen = False
                        dCol.ReadOnly = False
                        dCol.DataPropertyName = "||_" & dsRow.Item("Id").ToString
                        dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                        dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                        .Columns.Add(dCol)

                        dCol = New DataGridViewTextBoxColumn
                        dCol.Name = "objcolh" & dsRow.Item("Id").ToString
                        dCol.HeaderText = "Interviewer Name"
                        dCol.Tag = dsRow.Item("NAME").ToString
                        dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                        dCol.Width = 0
                        dCol.Frozen = False
                        dCol.ReadOnly = True
                        dCol.Visible = False
                        dCol.DataPropertyName = "|_" & dsRow.Item("Id").ToString
                        dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                        dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                        .Columns.Add(dCol)
                    Next



                End With

                For Each drRow As DataRow In dsList.Tables(0).Rows
                    If CInt(drRow("analysisunkid")) > 0 Then
                        For Each drTranRow As DataRow In dsList.Tables(1).Select("analysisunkid =" & CInt(drRow("analysisunkid")))
                            Dim intInterviewtranunkid As Integer = CInt(drTranRow("interviewerunkid"))
                            If dsList.Tables(0).Columns.Contains("|_" & intInterviewtranunkid) AndAlso dsList.Tables(0).Columns.Contains("||_" & intInterviewtranunkid) Then
                                drRow("|_" & intInterviewtranunkid) = drTranRow("resultcodeunkid")
                                drRow("||_" & intInterviewtranunkid) = drTranRow("remark")
                            End If
                        Next

                    End If
                Next

                dsList.Tables(0).AcceptChanges()
                mdtTable = New DataView(dsList.Tables(0), "", "Applicantname", DataViewRowState.CurrentRows).ToTable
                mdtTranTable = dsList.Tables(1)

                objchkSelectAll_Budget.Visible = True
                mdtDataView = mdtTable.DefaultView
                mdtTranDataView = mdtTranTable.DefaultView
                dgvData.DataSource = mdtDataView

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

    Private Sub SetValue()

        Try
            objAnalysisMaster._Appbatchscheduletranunkid = mintAppBatchScheduleTranId

            objAnalysisMaster._Userunkid = User._Object._Userunkid
            objAnalysisMaster._Statustypid = enShortListing_Status.SC_PENDING
            objAnalysisMaster._Vacancyunkid = mintVacancyunkid
            objAnalysisMaster._Applicantunkid = mintApplicantUnkid

            If menAction = enAction.EDIT_ONE Then
                objAnalysisMaster._Appbatchscheduletranunkid = objAnalysisMaster._Appbatchscheduletranunkid
            Else
                objAnalysisMaster._Appbatchscheduletranunkid = mintAppBatchScheduleTranId
            End If

            objAnalysisMaster._Isvoid = False
            objAnalysisMaster._Voidatetime = ConfigParameter._Object._CurrentDateAndTime
            objAnalysisMaster._Voidreason = ""
            objAnalysisMaster._Voiduserunkid = User._Object._Userunkid
            objAnalysisMaster._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try

    End Sub

    Private Sub Update_DataGridview_DataTable_AUD(ByVal intRowIndex As Integer, ByVal intInterviewerId As Integer)
        Try
            If CInt(CType(dgvData.DataSource, DataView)(intRowIndex)("analysisunkid")) > 0 Then
                CType(dgvData.DataSource, DataView)(intRowIndex)("AUD") = "U"
                Dim drIsExist() As DataRow = mdtTranDataView.Table.Select("analysisunkid = " & CInt(CType(dgvData.DataSource, DataView)(intRowIndex)("analysisunkid")) & " AND interviewerunkid = " & intInterviewerId)
                If drIsExist.Length > 0 Then
                    For Each drRow As DataRow In mdtTranDataView.Table.Select("analysisunkid = " & CInt(CType(dgvData.DataSource, DataView)(intRowIndex)("analysisunkid")))
                        If CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("|_" & drRow("interviewerunkid").ToString)) <> drRow("resultcodeunkid").ToString OrElse _
                           IIf(IsDBNull(CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & drRow("interviewerunkid").ToString)), String.Empty, CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & drRow("interviewerunkid").ToString)).ToString <> drRow("remark").ToString Then
                            If CInt(CType(dgvData.DataSource, DataView)(intRowIndex)("|_" & drRow("interviewerunkid").ToString)) = 0 AndAlso _
                               (IsDBNull(CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & drRow("interviewerunkid").ToString)) = True OrElse CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & drRow("interviewerunkid").ToString).ToString = "") Then
                                drRow("AUD") = "D"
                            Else
                                drRow("resultcodeunkid") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("|_" & drRow("interviewerunkid").ToString))
                                drRow("remark") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & drRow("interviewerunkid").ToString))
                                If CInt(drRow("analysistranunkid")) > 0 Then
                                    drRow("AUD") = "U"
                                End If
                            End If

                        End If
                    Next
                Else
                    Dim drNewRow As DataRow = mdtTranTable.NewRow
                    drNewRow("analysistranunkid") = 0
                    drNewRow("analysisunkid") = CInt(CType(dgvData.DataSource, DataView)(intRowIndex)("analysisunkid"))
                    Dim drInterviewer() As DataRow = mdsInterviewList.Tables(0).Select("Id = " & CInt(intInterviewerId))
                    If drInterviewer.Length > 0 Then
                        drNewRow("interviewertranunkid") = drInterviewer(0).Item("batchscheduleinterviewertranunkid")
                    Else
                        drNewRow("interviewertranunkid") = 0
                    End If

                    drNewRow("analysis_date") = Now.Date
                    drNewRow("isvoid") = False
                    drNewRow("voiduserunkid") = 0
                    drNewRow("resultcodeunkid") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("|_" & CInt(intInterviewerId)))
                    drNewRow("remark") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & CInt(intInterviewerId)))
                    drNewRow("interviewerunkid") = CInt(intInterviewerId)
                    drNewRow("applicantunkid") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("applicantunkid"))
                    drNewRow("AUD") = "A"
                    mdtTranTable.Rows.Add(drNewRow)
                End If
                Dim drNotDeleteRow() As DataRow = mdtTranDataView.Table.Select("AUD <> 'D' AND analysisunkid = " & CInt(CType(dgvData.DataSource, DataView)(intRowIndex)("analysisunkid")))
                If drNotDeleteRow.Length <= 0 Then
                    CType(dgvData.DataSource, DataView)(intRowIndex)("AUD") = "D"
                End If
            Else
                CType(dgvData.DataSource, DataView)(intRowIndex)("AUD") = "A"
                Dim drTranRow() As DataRow = mdtTranDataView.Table.Select("applicantunkid =" & CInt(CType(dgvData.DataSource, DataView)(intRowIndex)("applicantunkid")) & " AND interviewerunkid = " & intInterviewerId)

                If drTranRow.Length > 0 Then
                    drTranRow(0).Item("resultcodeunkid") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("|_" & CInt(intInterviewerId)))
                    drTranRow(0).Item("remark") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & CInt(intInterviewerId)))
                Else

                    Dim drNewRow As DataRow = mdtTranTable.NewRow
                    drNewRow("analysistranunkid") = 0
                    drNewRow("analysisunkid") = 0
                    Dim drInterviewer() As DataRow = mdsInterviewList.Tables(0).Select("Id = " & CInt(intInterviewerId))
                    If drInterviewer.Length > 0 Then
                        drNewRow("interviewertranunkid") = drInterviewer(0).Item("batchscheduleinterviewertranunkid")
                    Else
                        drNewRow("interviewertranunkid") = 0
                    End If

                    drNewRow("analysis_date") = Now.Date
                    drNewRow("isvoid") = False
                    drNewRow("voiduserunkid") = 0
                    drNewRow("resultcodeunkid") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("|_" & CInt(intInterviewerId)))
                    drNewRow("remark") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("||_" & CInt(intInterviewerId)))
                    drNewRow("interviewerunkid") = CInt(intInterviewerId)
                    drNewRow("applicantunkid") = CStr(CType(dgvData.DataSource, DataView)(intRowIndex)("applicantunkid"))
                    drNewRow("AUD") = "A"
                    mdtTranTable.Rows.Add(drNewRow)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_DataGridview_DataTable_AUD", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Vacancy Type is mandatory information. Please select Vacancy Type."), enMsgBoxStyle.Information)
                cboVacancyType.Focus()
                Exit Sub
            End If

            If CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Vacancy is mandatory information. Please select Vacancy."), enMsgBoxStyle.Information)
                cboVacancy.Focus()
                Exit Sub
            End If

            If CInt(cboBatch.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Batch is mandatory information. Please select Batch."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Sub
            End If

            If CInt(cboInterviewType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Interview Type is mandatory information. Please select Interview Type."), enMsgBoxStyle.Information)
                cboInterviewType.Focus()
                Exit Sub
            End If


            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try


            cboInterviewType.SelectedValue = 0
            cboResult.SelectedValue = 0
            chkComplete.Checked = False
            chkIncomplete.Checked = False

            cboVacancyType.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            txtRemark.Text = ""

            dgvData.DataSource = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)

            End If

            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If mdtDataView Is Nothing OrElse mdtDataView.Table.Rows.Count <= 0 Then Exit Sub

            If radApplyToAll.Checked = False AndAlso radApplyToChecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one either Apply To All OR Apply To Checked."), enMsgBoxStyle.Information)
                radApplyToAll.Focus()
                Exit Sub
            End If

            If CInt(cboResult.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Score."), enMsgBoxStyle.Information)
                cboResult.Focus()
                Exit Sub
            End If

            If CInt(cboInterviewer.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Interviewer."), enMsgBoxStyle.Information)
                cboInterviewer.Focus()
                Exit Sub
            End If


            Dim lstRow As List(Of DataRow)

            If radApplyToChecked.Checked = True Then
                lstRow = (From p In mdtDataView.Table Where (CInt(p.Item("appbatchscheduletranunkid")) <> -1 AndAlso CBool(p.Item("IsChecked")) = True) Select (p)).ToList
            Else
                lstRow = (From p In mdtDataView.Table Where (CInt(p.Item("appbatchscheduletranunkid")) <> -1) Select (p)).ToList
            End If

            Dim strRemarkCol As String = String.Empty
            Dim strScoreCol As String = String.Empty

            strScoreCol = "|_" & CStr(cboInterviewer.SelectedValue)
            strRemarkCol = "||_" & CStr(cboInterviewer.SelectedValue)

            For Each dtRow As DataRow In lstRow
                If strRemarkCol.Trim.Length > 0 Then
                    dtRow.Item(strRemarkCol) = txtRemark.Text
                End If
                If strScoreCol.Trim.Length > 0 AndAlso CInt(cboResult.SelectedValue) > 0 Then
                    dtRow.Item(strScoreCol) = CInt(cboResult.SelectedValue)
                End If
                If CInt(dtRow.Item("analysisunkid")) > 0 Then
                    dtRow.Item("AUD") = "U"
                    Dim drRow() As DataRow = mdtTranTable.Select("analysistranunkid > 0 and analysisunkid = " & CInt(dtRow.Item("analysisunkid")) & " and interviewerunkid = " & CInt(cboInterviewer.SelectedValue))
                    If drRow.Length > 0 Then
                        drRow(0).Item("resultcodeunkid") = CStr(cboResult.SelectedValue)
                        drRow(0).Item("remark") = txtRemark.Text
                        drRow(0).Item("AUD") = "U"
                    Else
                        Dim drTranRow() As DataRow = mdtTranDataView.Table.Select("applicantunkid =" & CInt(dtRow.Item("applicantunkid")) & " AND interviewerunkid = " & CInt(cboInterviewer.SelectedValue))

                        If drTranRow.Length > 0 Then
                            drTranRow(0).Item("resultcodeunkid") = CStr(dtRow.Item("|_" & CInt(cboInterviewer.SelectedValue)))
                            drTranRow(0).Item("remark") = CStr(dtRow.Item("||_" & CInt(cboInterviewer.SelectedValue)))
                        Else
                            Dim drNewRow As DataRow = mdtTranTable.NewRow
                            drNewRow("analysistranunkid") = 0
                            drNewRow("analysisunkid") = CInt(dtRow.Item("analysisunkid"))
                            Dim drInterviewer() As DataRow = mdsInterviewList.Tables(0).Select("Id = " & CInt(cboInterviewer.SelectedValue))
                            If drInterviewer.Length > 0 Then
                                drNewRow("interviewertranunkid") = drInterviewer(0).Item("batchscheduleinterviewertranunkid")
                            Else
                                drNewRow("interviewertranunkid") = 0
                            End If

                            drNewRow("analysis_date") = Now.Date
                            drNewRow("isvoid") = False
                            drNewRow("voiduserunkid") = 0
                            drNewRow("resultcodeunkid") = CStr(cboResult.SelectedValue)
                            drNewRow("remark") = txtRemark.Text
                            drNewRow("interviewerunkid") = CInt(cboInterviewer.SelectedValue)
                            drNewRow("applicantunkid") = CStr(dtRow.Item("applicantunkid"))
                            drNewRow("AUD") = "A"
                            mdtTranTable.Rows.Add(drNewRow)
                        End If
                    End If

                Else
                    dtRow.Item("AUD") = "A"

                    Dim drTranRow() As DataRow = mdtTranDataView.Table.Select("applicantunkid =" & CInt(dtRow.Item("applicantunkid")) & " AND interviewerunkid = " & CInt(cboInterviewer.SelectedValue))

                    If drTranRow.Length > 0 Then
                        drTranRow(0).Item("resultcodeunkid") = CStr(dtRow.Item("|_" & CInt(cboInterviewer.SelectedValue)))
                        drTranRow(0).Item("remark") = CStr(dtRow.Item("||_" & CInt(cboInterviewer.SelectedValue)))
                    Else

                        Dim drNewRow As DataRow = mdtTranTable.NewRow
                        drNewRow("analysistranunkid") = 0
                        drNewRow("analysisunkid") = CInt(dtRow.Item("analysisunkid"))
                        Dim drInterviewer() As DataRow = mdsInterviewList.Tables(0).Select("Id = " & CInt(cboInterviewer.SelectedValue))
                        If drInterviewer.Length > 0 Then
                            drNewRow("interviewertranunkid") = drInterviewer(0).Item("batchscheduleinterviewertranunkid")
                        Else
                            drNewRow("interviewertranunkid") = 0
                        End If

                        drNewRow("analysis_date") = Now.Date
                        drNewRow("isvoid") = False
                        drNewRow("voiduserunkid") = 0
                        drNewRow("resultcodeunkid") = CStr(cboResult.SelectedValue)
                        drNewRow("remark") = txtRemark.Text
                        drNewRow("interviewerunkid") = CInt(cboInterviewer.SelectedValue)
                        drNewRow("applicantunkid") = CStr(dtRow.Item("applicantunkid"))
                        drNewRow("AUD") = "A"
                        mdtTranTable.Rows.Add(drNewRow)
                    End If
                End If
            Next

            mdtDataView.Table.AcceptChanges()
            mdtTranTable.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApply_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim objVacancyData As New clsVacancy
        Try
            If dgvData.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There is nothing to save in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call SetValue()

            blnFlag = objAnalysisMaster.InsertUpdateDelete_All(mdtTable, mdtTranTable, False)

            If blnFlag = False And objAnalysisMaster._Message <> "" Then
                eZeeMsgBox.Show(objAnalysisMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag = True Then
                mblnCancel = False

                Me.Close()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnInterviewer.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboInterviewer.ValueMember
                .DisplayMember = cboInterviewer.DisplayMember
                .DataSource = CType(cboInterviewer.DataSource, DataTable)
                .CodeMember = ""
            End With

            If objFrm.DisplayDialog Then
                cboInterviewer.SelectedValue = objFrm.SelectedValue
                cboInterviewer.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnInterviewer_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResult.Click
        Dim frm As New frmResultCode_AddEdit
        Dim intRefId As Integer = 0
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objResult As New clsresult_master
                dsList = objResult.getComboList("Result", True, mintResultGroup)
                With cboResult
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Result")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddResult_Click", mstrmodulename)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Gridview Events"
    'Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
    '    Try
    '        If mdtDataView Is Nothing Then Exit Try

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
    '    Finally
    '        objbtnSearch.ShowResult(mdtDataView.ToTable.Select("Id <> -1 ").Count.ToString)
    '    End Try
    'End Sub
    'Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
    '    Try
    '        If e.RowIndex <= -1 Then Exit Sub

    '        If dgvData.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHRESULTCODE") Then
    '            SendKeys.Send("{F2}")
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub dgvData_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvData.CellPainting
        Try
            If e.RowIndex = -1 AndAlso e.ColumnIndex > -1 Then
                Dim r2 As Rectangle = e.CellBounds
                r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
                r2.Height = CInt(e.CellBounds.Height / 2)
                e.PaintBackground(r2, True)
                e.PaintContent(r2)
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles dgvData.ColumnWidthChanged
        Try
            Dim rtHeader As Rectangle = Me.dgvData.DisplayRectangle
            rtHeader.Height = CInt(Me.dgvData.ColumnHeadersHeight / 2)
            Me.dgvData.Invalidate(rtHeader)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_ColumnWidthChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Dim intInterviewerId As Integer = 0
            If dgvData.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHSCORE") = True Then
                intInterviewerId = CInt(dgvData.Columns(e.ColumnIndex).Name.ToUpper.Replace("COLHSCORE", "").ToString)
            ElseIf dgvData.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHREMARK") = True Then
                intInterviewerId = CInt(dgvData.Columns(e.ColumnIndex).Name.ToUpper.Replace("COLHREMARK", "").ToString)
            End If
            If intInterviewerId <> 0 Then
                Call Update_DataGridview_DataTable_AUD(e.RowIndex, intInterviewerId)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgvData.Paint
        Try
            If (mdtTable Is Nothing OrElse mdtTable.Rows.Count <= 0) Then Exit Sub

            Dim j As Integer = dgvData.Columns("colhScore" & mdicInterviewer.ElementAt(0).Key.ToString).Index
            While j < dgvData.Columns("colhScore" & mdicInterviewer.ElementAt(0).Key.ToString).Index + (mdicInterviewer.Count * 3)
                Dim r1 As Rectangle = Me.dgvData.GetCellDisplayRectangle(j, -1, True)
                Dim w2 As Integer = Me.dgvData.GetCellDisplayRectangle(j + 1, -1, True).Width
                r1.X += 1
                r1.Y += 1
                r1.Width = r1.Width + w2 - 2
                r1.Height = CInt(r1.Height / 2 - 2)
                e.Graphics.FillRectangle(New SolidBrush(Me.dgvData.ColumnHeadersDefaultCellStyle.BackColor), r1)
                Dim format As New StringFormat()
                format.Alignment = StringAlignment.Center
                format.LineAlignment = StringAlignment.Center
                e.Graphics.DrawString(dgvData.Columns(j).Tag.ToString, Me.dgvData.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Me.dgvData.ColumnHeadersDefaultCellStyle.ForeColor), r1, format)
                j += 3
            End While
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_Paint", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvData.Scroll
        Try
            Dim rtHeader As Rectangle = Me.dgvData.DisplayRectangle
            rtHeader.Height = CInt(Me.dgvData.ColumnHeadersHeight / 2)
            Me.dgvData.Invalidate(rtHeader)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataError", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Combobox Events "
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsVac As New DataSet

            dsVac = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))

            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsVac.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            If CInt(cboVacancy.SelectedValue) > 0 Then
                Dim dsBatch As New DataSet
                Dim objBatch As New clsBatchSchedule

                dsBatch = objBatch.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True)

                Dim dtFilter = New DataView(dsBatch.Tables(0), "vacancyid IN(0," & CInt(cboVacancy.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable
                With cboBatch
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dtFilter
                    .SelectedValue = 0
                End With
            Else
                cboBatch.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboBatch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBatch.SelectedIndexChanged
        Try
            If CInt(cboBatch.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objBatch As New clsBatchSchedule
                Dim objResult As New clsresult_master
                objBatch._Batchscheduleunkid = CInt(cboBatch.SelectedValue)
                'Hemant (19 Jun 2020) -- Start
                mintResultGroup = CInt(objBatch._Resultgroupunkid)
                'Hemant (19 Jun 2020) -- End
                dsList = objResult.getComboList("List", True, objBatch._Resultgroupunkid)
                objBatch = Nothing
                With cboResult
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With

                mdsInterviewList = objBatchScheduleInterviewer_tran.getComboList(CInt(cboBatch.SelectedValue), True, "List", , , _
                                                                                      " AND rcbatchschedule_interviewer_tran.interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " ")

                Dim intId As Integer = -1
                For Each drRow As DataRow In mdsInterviewList.Tables(0).Select("Id = -1")
                    drRow("Id") = intId
                    intId = intId - 1
                Next
                mdsInterviewList.Tables(0).AcceptChanges()

                With cboInterviewer
                    .ValueMember = "Id"
                    .DisplayMember = "name"
                    .DataSource = mdsInterviewList.Tables(0)
                    .SelectedValue = 0
                End With
                objbtnInterviewer.Enabled = True
            Else
                cboResult.DataSource = Nothing
                cboInterviewer.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatch_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboInterviewType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboInterviewType.SelectedIndexChanged
        Try
            If CInt(cboInterviewType.SelectedValue) > 0 Then
                Dim dsList As New DataSet


                mdsInterviewList = objBatchScheduleInterviewer_tran.getComboList(CInt(cboBatch.SelectedValue), True, "List", , , _
                                                                                      " AND rcbatchschedule_interviewer_tran.interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " ")

                Dim intId As Integer = -1
                For Each drRow As DataRow In mdsInterviewList.Tables(0).Select("Id = -1")
                    drRow("Id") = intId
                    intId = intId - 1
                Next
                mdsInterviewList.Tables(0).AcceptChanges()
                With cboInterviewer
                    .ValueMember = "Id"
                    .DisplayMember = "name"
                    .DataSource = mdsInterviewList.Tables(0)
                    .SelectedValue = 0
                End With
                objbtnInterviewer.Enabled = True
            Else
                cboInterviewer.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboInterviewType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region "CheckBox's Events"
    Private Sub objchkSelectAll_Budget_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll_Budget.CheckedChanged
        Try
            For Each dtRow As DataRowView In mdtDataView
                dtRow.Item("IsChecked") = objchkSelectAll_Budget.Checked
            Next
            mdtDataView.Table.AcceptChanges()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_Budget_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbBasicInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbBasicInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.btnApply.GradientBackColor = GUI._ButttonBackColor
			Me.btnApply.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkIncomplete.Text = Language._Object.getCaption(Me.chkIncomplete.Name, Me.chkIncomplete.Text)
			Me.chkComplete.Text = Language._Object.getCaption(Me.chkComplete.Name, Me.chkComplete.Text)
			Me.lblBatch.Text = Language._Object.getCaption(Me.lblBatch.Name, Me.lblBatch.Text)
			Me.lblInterviewType.Text = Language._Object.getCaption(Me.lblInterviewType.Name, Me.lblInterviewType.Text)
			Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
			Me.lblVacanyType.Text = Language._Object.getCaption(Me.lblVacanyType.Name, Me.lblVacanyType.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.gbBasicInfo.Text = Language._Object.getCaption(Me.gbBasicInfo.Name, Me.gbBasicInfo.Text)
			Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.Name, Me.btnApply.Text)
			Me.radApplyToAll.Text = Language._Object.getCaption(Me.radApplyToAll.Name, Me.radApplyToAll.Text)
			Me.radApplyToChecked.Text = Language._Object.getCaption(Me.radApplyToChecked.Name, Me.radApplyToChecked.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblScore.Text = Language._Object.getCaption(Me.lblScore.Name, Me.lblScore.Text)
			Me.dgvData.Text = Language._Object.getCaption(Me.dgvData.Name, Me.dgvData.Text)
			Me.lblInterviewer.Text = Language._Object.getCaption(Me.lblInterviewer.Name, Me.lblInterviewer.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Vacancy Type is mandatory information. Please select Vacancy Type.")
			Language.setMessage(mstrModuleName, 2, "Vacancy is mandatory information. Please select Vacancy.")
			Language.setMessage(mstrModuleName, 3, "Batch is mandatory information. Please select Batch.")
			Language.setMessage(mstrModuleName, 4, "Interview Type is mandatory information. Please select Interview Type.")
			Language.setMessage(mstrModuleName, 5, "Please check atleast one either Apply To All OR Apply To Checked.")
			Language.setMessage(mstrModuleName, 6, "Please select Score.")
			Language.setMessage(mstrModuleName, 7, "Please select Interviewer.")
			Language.setMessage(mstrModuleName, 8, "There is nothing to save in the list.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class