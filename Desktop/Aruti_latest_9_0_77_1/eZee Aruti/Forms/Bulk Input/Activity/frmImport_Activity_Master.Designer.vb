﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImport_Activity_Master
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImport_Activity_Master))
        Me.eZeeWizActivityMst = New eZee.Common.eZeeWizard
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboMeasures = New System.Windows.Forms.ComboBox
        Me.lblMeasures = New System.Windows.Forms.Label
        Me.chkTaxablePH = New System.Windows.Forms.CheckBox
        Me.chkAppearPHOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkTaxableOT = New System.Windows.Forms.CheckBox
        Me.chkAppearOTOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkTaxable = New System.Windows.Forms.CheckBox
        Me.chkAppearOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkOTActivity = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboHeadType = New System.Windows.Forms.ComboBox
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.cboActivityCode = New System.Windows.Forms.ComboBox
        Me.lblActivitycode = New System.Windows.Forms.Label
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.objlblSign5 = New System.Windows.Forms.Label
        Me.lblActivityName = New System.Windows.Forms.Label
        Me.cboActivityName = New System.Windows.Forms.ComboBox
        Me.lblCaption = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhActivityCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhActivityName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.objbuttonSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeWizActivityMst.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeWizActivityMst
        '
        Me.eZeeWizActivityMst.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizActivityMst.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizActivityMst.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizActivityMst.Controls.Add(Me.objbuttonSave)
        Me.eZeeWizActivityMst.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizActivityMst.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizActivityMst.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizActivityMst.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizActivityMst.Name = "eZeeWizActivityMst"
        Me.eZeeWizActivityMst.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizActivityMst.SaveEnabled = True
        Me.eZeeWizActivityMst.SaveText = "Save && Finish"
        Me.eZeeWizActivityMst.SaveVisible = False
        Me.eZeeWizActivityMst.SetSaveIndexBeforeFinishIndex = False
        Me.eZeeWizActivityMst.Size = New System.Drawing.Size(558, 353)
        Me.eZeeWizActivityMst.TabIndex = 6
        Me.eZeeWizActivityMst.WelcomeImage = Nothing
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(558, 305)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.Label2)
        Me.gbFieldMapping.Controls.Add(Me.cboMeasures)
        Me.gbFieldMapping.Controls.Add(Me.lblMeasures)
        Me.gbFieldMapping.Controls.Add(Me.chkTaxablePH)
        Me.gbFieldMapping.Controls.Add(Me.chkAppearPHOnPayslip)
        Me.gbFieldMapping.Controls.Add(Me.chkTaxableOT)
        Me.gbFieldMapping.Controls.Add(Me.chkAppearOTOnPayslip)
        Me.gbFieldMapping.Controls.Add(Me.chkTaxable)
        Me.gbFieldMapping.Controls.Add(Me.chkAppearOnPayslip)
        Me.gbFieldMapping.Controls.Add(Me.chkOTActivity)
        Me.gbFieldMapping.Controls.Add(Me.Label1)
        Me.gbFieldMapping.Controls.Add(Me.cboHeadType)
        Me.gbFieldMapping.Controls.Add(Me.lblTransactionHead)
        Me.gbFieldMapping.Controls.Add(Me.cboActivityCode)
        Me.gbFieldMapping.Controls.Add(Me.lblActivitycode)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign4)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign5)
        Me.gbFieldMapping.Controls.Add(Me.lblActivityName)
        Me.gbFieldMapping.Controls.Add(Me.cboActivityName)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFieldMapping.Margin = New System.Windows.Forms.Padding(0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(393, 305)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(20, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 15)
        Me.Label2.TabIndex = 113
        Me.Label2.Text = "*"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboMeasures
        '
        Me.cboMeasures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMeasures.DropDownWidth = 350
        Me.cboMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMeasures.FormattingEnabled = True
        Me.cboMeasures.Location = New System.Drawing.Point(140, 87)
        Me.cboMeasures.Name = "cboMeasures"
        Me.cboMeasures.Size = New System.Drawing.Size(237, 21)
        Me.cboMeasures.TabIndex = 112
        '
        'lblMeasures
        '
        Me.lblMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeasures.Location = New System.Drawing.Point(36, 90)
        Me.lblMeasures.Name = "lblMeasures"
        Me.lblMeasures.Size = New System.Drawing.Size(95, 15)
        Me.lblMeasures.TabIndex = 111
        Me.lblMeasures.Text = "Measures"
        Me.lblMeasures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkTaxablePH
        '
        Me.chkTaxablePH.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTaxablePH.Location = New System.Drawing.Point(140, 279)
        Me.chkTaxablePH.Name = "chkTaxablePH"
        Me.chkTaxablePH.Size = New System.Drawing.Size(237, 17)
        Me.chkTaxablePH.TabIndex = 110
        Me.chkTaxablePH.Text = "Taxable PH"
        Me.chkTaxablePH.UseVisualStyleBackColor = True
        '
        'chkAppearPHOnPayslip
        '
        Me.chkAppearPHOnPayslip.Checked = True
        Me.chkAppearPHOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearPHOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAppearPHOnPayslip.Location = New System.Drawing.Point(140, 210)
        Me.chkAppearPHOnPayslip.Name = "chkAppearPHOnPayslip"
        Me.chkAppearPHOnPayslip.Size = New System.Drawing.Size(237, 17)
        Me.chkAppearPHOnPayslip.TabIndex = 109
        Me.chkAppearPHOnPayslip.Text = "Appear PH on Payslip"
        Me.chkAppearPHOnPayslip.UseVisualStyleBackColor = True
        '
        'chkTaxableOT
        '
        Me.chkTaxableOT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTaxableOT.Location = New System.Drawing.Point(140, 256)
        Me.chkTaxableOT.Name = "chkTaxableOT"
        Me.chkTaxableOT.Size = New System.Drawing.Size(237, 17)
        Me.chkTaxableOT.TabIndex = 108
        Me.chkTaxableOT.Text = "Taxable OT"
        Me.chkTaxableOT.UseVisualStyleBackColor = True
        '
        'chkAppearOTOnPayslip
        '
        Me.chkAppearOTOnPayslip.Checked = True
        Me.chkAppearOTOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearOTOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAppearOTOnPayslip.Location = New System.Drawing.Point(140, 187)
        Me.chkAppearOTOnPayslip.Name = "chkAppearOTOnPayslip"
        Me.chkAppearOTOnPayslip.Size = New System.Drawing.Size(237, 17)
        Me.chkAppearOTOnPayslip.TabIndex = 107
        Me.chkAppearOTOnPayslip.Text = "Appear OT on Payslip"
        Me.chkAppearOTOnPayslip.UseVisualStyleBackColor = True
        '
        'chkTaxable
        '
        Me.chkTaxable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTaxable.Location = New System.Drawing.Point(140, 233)
        Me.chkTaxable.Name = "chkTaxable"
        Me.chkTaxable.Size = New System.Drawing.Size(237, 17)
        Me.chkTaxable.TabIndex = 106
        Me.chkTaxable.Text = "Taxable Activity"
        Me.chkTaxable.UseVisualStyleBackColor = True
        '
        'chkAppearOnPayslip
        '
        Me.chkAppearOnPayslip.Checked = True
        Me.chkAppearOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAppearOnPayslip.Location = New System.Drawing.Point(140, 164)
        Me.chkAppearOnPayslip.Name = "chkAppearOnPayslip"
        Me.chkAppearOnPayslip.Size = New System.Drawing.Size(237, 17)
        Me.chkAppearOnPayslip.TabIndex = 105
        Me.chkAppearOnPayslip.Text = "Appear Activity on Payslip"
        Me.chkAppearOnPayslip.UseVisualStyleBackColor = True
        '
        'chkOTActivity
        '
        Me.chkOTActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOTActivity.Location = New System.Drawing.Point(140, 141)
        Me.chkOTActivity.Name = "chkOTActivity"
        Me.chkOTActivity.Size = New System.Drawing.Size(237, 17)
        Me.chkOTActivity.TabIndex = 104
        Me.chkOTActivity.Text = "Consider This Activity for OT"
        Me.chkOTActivity.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(20, 117)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 15)
        Me.Label1.TabIndex = 103
        Me.Label1.Text = "*"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboHeadType
        '
        Me.cboHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadType.DropDownWidth = 350
        Me.cboHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadType.FormattingEnabled = True
        Me.cboHeadType.Location = New System.Drawing.Point(140, 114)
        Me.cboHeadType.Name = "cboHeadType"
        Me.cboHeadType.Size = New System.Drawing.Size(237, 21)
        Me.cboHeadType.TabIndex = 12
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(36, 117)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(95, 15)
        Me.lblTransactionHead.TabIndex = 11
        Me.lblTransactionHead.Text = "Head Type"
        Me.lblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboActivityCode
        '
        Me.cboActivityCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivityCode.FormattingEnabled = True
        Me.cboActivityCode.Location = New System.Drawing.Point(140, 33)
        Me.cboActivityCode.Name = "cboActivityCode"
        Me.cboActivityCode.Size = New System.Drawing.Size(237, 21)
        Me.cboActivityCode.TabIndex = 101
        '
        'lblActivitycode
        '
        Me.lblActivitycode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivitycode.Location = New System.Drawing.Point(36, 36)
        Me.lblActivitycode.Name = "lblActivitycode"
        Me.lblActivitycode.Size = New System.Drawing.Size(95, 15)
        Me.lblActivitycode.TabIndex = 100
        Me.lblActivitycode.Text = "Activity Code"
        Me.lblActivitycode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(20, 36)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign4.TabIndex = 99
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign5
        '
        Me.objlblSign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign5.ForeColor = System.Drawing.Color.Red
        Me.objlblSign5.Location = New System.Drawing.Point(20, 63)
        Me.objlblSign5.Name = "objlblSign5"
        Me.objlblSign5.Size = New System.Drawing.Size(10, 15)
        Me.objlblSign5.TabIndex = 96
        Me.objlblSign5.Text = "*"
        Me.objlblSign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblActivityName
        '
        Me.lblActivityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityName.Location = New System.Drawing.Point(36, 63)
        Me.lblActivityName.Name = "lblActivityName"
        Me.lblActivityName.Size = New System.Drawing.Size(95, 15)
        Me.lblActivityName.TabIndex = 94
        Me.lblActivityName.Text = "Activity Name"
        Me.lblActivityName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboActivityName
        '
        Me.cboActivityName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivityName.FormattingEnabled = True
        Me.cboActivityName.Location = New System.Drawing.Point(140, 60)
        Me.cboActivityName.Name = "cboActivityName"
        Me.cboActivityName.Size = New System.Drawing.Size(237, 21)
        Me.cboActivityName.TabIndex = 89
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(186, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(204, 19)
        Me.lblCaption.TabIndex = 34
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(558, 305)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeight = 22
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhActivityCode, Me.colhActivityName, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(534, 195)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 25
        '
        'colhActivityCode
        '
        Me.colhActivityCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhActivityCode.HeaderText = "Activity Code"
        Me.colhActivityCode.Name = "colhActivityCode"
        Me.colhActivityCode.ReadOnly = True
        '
        'colhActivityName
        '
        Me.colhActivityName.HeaderText = "Activity Name"
        Me.colhActivityName.Name = "colhActivityName"
        Me.colhActivityName.ReadOnly = True
        Me.colhActivityName.Width = 146
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.Width = 80
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 150
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 270)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(95, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 21
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(187, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(186, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(186, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(534, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(412, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(298, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(412, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(344, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(457, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(298, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(457, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(344, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(558, 305)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(181, 63)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(365, 45)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import 'Activity Master' records made from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(181, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(365, 51)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Activity Master Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(522, 154)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(24, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(179, 154)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(337, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(179, 131)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(310, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonSave
        '
        Me.objbuttonSave.BackColor = System.Drawing.Color.White
        Me.objbuttonSave.BackgroundImage = CType(resources.GetObject("objbuttonSave.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonSave.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonSave.FlatAppearance.BorderSize = 0
        Me.objbuttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonSave.ForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonSave.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Location = New System.Drawing.Point(229, 224)
        Me.objbuttonSave.Name = "objbuttonSave"
        Me.objbuttonSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Size = New System.Drawing.Size(99, 29)
        Me.objbuttonSave.TabIndex = 7
        Me.objbuttonSave.Text = "Save && Finish"
        Me.objbuttonSave.UseVisualStyleBackColor = False
        Me.objbuttonSave.Visible = False
        '
        'frmImport_Activity_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(558, 353)
        Me.Controls.Add(Me.eZeeWizActivityMst)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImport_Activity_Master"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Activity Master"
        Me.eZeeWizActivityMst.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.WizPageImporting.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeWizActivityMst As eZee.Common.eZeeWizard
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonSave As eZee.Common.eZeeLightButton
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboActivityCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblActivitycode As System.Windows.Forms.Label
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents objlblSign5 As System.Windows.Forms.Label
    Friend WithEvents lblActivityName As System.Windows.Forms.Label
    Friend WithEvents cboActivityName As System.Windows.Forms.ComboBox
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkTaxablePH As System.Windows.Forms.CheckBox
    Friend WithEvents chkAppearPHOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents chkTaxableOT As System.Windows.Forms.CheckBox
    Friend WithEvents chkAppearOTOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents chkTaxable As System.Windows.Forms.CheckBox
    Friend WithEvents chkAppearOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents chkOTActivity As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboMeasures As System.Windows.Forms.ComboBox
    Friend WithEvents lblMeasures As System.Windows.Forms.Label
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhActivityCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhActivityName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
