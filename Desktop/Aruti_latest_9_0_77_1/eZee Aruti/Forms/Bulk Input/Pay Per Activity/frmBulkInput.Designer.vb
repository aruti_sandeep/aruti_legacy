﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBulkInput
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBulkInput))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tabcTotals = New System.Windows.Forms.TabControl
        Me.tabpEmployeeTotals = New System.Windows.Forms.TabPage
        Me.pnlEmpTotal = New System.Windows.Forms.Panel
        Me.lvEmpTotal = New eZee.Common.eZeeListView(Me.components)
        Me.colhEDisplayFor = New System.Windows.Forms.ColumnHeader
        Me.colhETotal = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmp = New System.Windows.Forms.ColumnHeader
        Me.objEHeading = New eZee.Common.eZeeHeading
        Me.objPanel1 = New System.Windows.Forms.Panel
        Me.radEByActivity = New System.Windows.Forms.RadioButton
        Me.radEByUoM = New System.Windows.Forms.RadioButton
        Me.objPanel2 = New System.Windows.Forms.Panel
        Me.radEByDate = New System.Windows.Forms.RadioButton
        Me.radEPeriod = New System.Windows.Forms.RadioButton
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.tabpCompanyTotals = New System.Windows.Forms.TabPage
        Me.plnCompanyTotals = New System.Windows.Forms.Panel
        Me.objCHeading = New eZee.Common.eZeeHeading
        Me.radPeriod = New System.Windows.Forms.RadioButton
        Me.radByDate = New System.Windows.Forms.RadioButton
        Me.objHeading = New eZee.Common.eZeeHeading
        Me.radCByActivity = New System.Windows.Forms.RadioButton
        Me.radCByUoM = New System.Windows.Forms.RadioButton
        Me.lvCompanyTotal = New System.Windows.Forms.ListView
        Me.colhCDisplayFor = New System.Windows.Forms.ColumnHeader
        Me.colhCTotal = New System.Windows.Forms.ColumnHeader
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocations = New System.Windows.Forms.ColumnHeader
        Me.gbOperation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.rdUpdateExisting = New System.Windows.Forms.RadioButton
        Me.rdAddNew = New System.Windows.Forms.RadioButton
        Me.rdNoAction = New System.Windows.Forms.RadioButton
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnChangeDate = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuExportPPA = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportPPA = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportWeeklyPPATemplate = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbActivityDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtFinalAmt = New eZee.TextBox.NumericTextBox
        Me.txtRate = New eZee.TextBox.AlphanumericTextBox
        Me.lblActivityRate = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lvDetails = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhActivity = New System.Windows.Forms.ColumnHeader
        Me.colhValue = New System.Windows.Forms.ColumnHeader
        Me.colhUoM = New System.Windows.Forms.ColumnHeader
        Me.colhRate = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhCostCenter = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployee = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsPosted = New System.Windows.Forms.ColumnHeader
        Me.chkCostCenter = New System.Windows.Forms.CheckBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblDate = New System.Windows.Forms.Label
        Me.cboActivity = New System.Windows.Forms.ComboBox
        Me.txtActivity = New eZee.TextBox.AlphanumericTextBox
        Me.lblActivityCode = New System.Windows.Forms.Label
        Me.lblActivity = New System.Windows.Forms.Label
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.txtMeasure = New eZee.TextBox.AlphanumericTextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblMeasure = New System.Windows.Forms.Label
        Me.lblValue = New System.Windows.Forms.Label
        Me.txtValue = New eZee.TextBox.NumericTextBox
        Me.pnlMainInfo.SuspendLayout()
        Me.tabcTotals.SuspendLayout()
        Me.tabpEmployeeTotals.SuspendLayout()
        Me.pnlEmpTotal.SuspendLayout()
        Me.objEHeading.SuspendLayout()
        Me.objPanel1.SuspendLayout()
        Me.objPanel2.SuspendLayout()
        Me.tabpCompanyTotals.SuspendLayout()
        Me.plnCompanyTotals.SuspendLayout()
        Me.objCHeading.SuspendLayout()
        Me.objHeading.SuspendLayout()
        Me.gbOperation.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.gbActivityDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbOperation)
        Me.pnlMainInfo.Controls.Add(Me.tabcTotals)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbActivityDetails)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(993, 572)
        Me.pnlMainInfo.TabIndex = 0
        '
        'tabcTotals
        '
        Me.tabcTotals.Controls.Add(Me.tabpEmployeeTotals)
        Me.tabcTotals.Controls.Add(Me.tabpCompanyTotals)
        Me.tabcTotals.Location = New System.Drawing.Point(572, 3)
        Me.tabcTotals.Name = "tabcTotals"
        Me.tabcTotals.SelectedIndex = 0
        Me.tabcTotals.Size = New System.Drawing.Size(418, 513)
        Me.tabcTotals.TabIndex = 21
        '
        'tabpEmployeeTotals
        '
        Me.tabpEmployeeTotals.Controls.Add(Me.pnlEmpTotal)
        Me.tabpEmployeeTotals.Location = New System.Drawing.Point(4, 22)
        Me.tabpEmployeeTotals.Name = "tabpEmployeeTotals"
        Me.tabpEmployeeTotals.Size = New System.Drawing.Size(410, 487)
        Me.tabpEmployeeTotals.TabIndex = 0
        Me.tabpEmployeeTotals.Text = "Employee Totals"
        Me.tabpEmployeeTotals.UseVisualStyleBackColor = True
        '
        'pnlEmpTotal
        '
        Me.pnlEmpTotal.Controls.Add(Me.lvEmpTotal)
        Me.pnlEmpTotal.Controls.Add(Me.objEHeading)
        Me.pnlEmpTotal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmpTotal.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmpTotal.Name = "pnlEmpTotal"
        Me.pnlEmpTotal.Size = New System.Drawing.Size(410, 487)
        Me.pnlEmpTotal.TabIndex = 6
        '
        'lvEmpTotal
        '
        Me.lvEmpTotal.BackColorOnChecked = True
        Me.lvEmpTotal.ColumnHeaders = Nothing
        Me.lvEmpTotal.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEDisplayFor, Me.colhETotal, Me.objcolhEmp})
        Me.lvEmpTotal.CompulsoryColumns = ""
        Me.lvEmpTotal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmpTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEmpTotal.FullRowSelect = True
        Me.lvEmpTotal.GridLines = True
        Me.lvEmpTotal.GroupingColumn = Nothing
        Me.lvEmpTotal.HideSelection = False
        Me.lvEmpTotal.Location = New System.Drawing.Point(0, 25)
        Me.lvEmpTotal.MinColumnWidth = 50
        Me.lvEmpTotal.MultiSelect = False
        Me.lvEmpTotal.Name = "lvEmpTotal"
        Me.lvEmpTotal.OptionalColumns = ""
        Me.lvEmpTotal.ShowMoreItem = False
        Me.lvEmpTotal.ShowSaveItem = False
        Me.lvEmpTotal.ShowSelectAll = True
        Me.lvEmpTotal.ShowSizeAllColumnsToFit = True
        Me.lvEmpTotal.Size = New System.Drawing.Size(410, 462)
        Me.lvEmpTotal.Sortable = True
        Me.lvEmpTotal.TabIndex = 0
        Me.lvEmpTotal.UseCompatibleStateImageBehavior = False
        Me.lvEmpTotal.View = System.Windows.Forms.View.Details
        '
        'colhEDisplayFor
        '
        Me.colhEDisplayFor.Tag = "colhEDisplayFor"
        Me.colhEDisplayFor.Text = ""
        Me.colhEDisplayFor.Width = 255
        '
        'colhETotal
        '
        Me.colhETotal.Tag = "colhETotal"
        Me.colhETotal.Text = "Total"
        Me.colhETotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhETotal.Width = 127
        '
        'objcolhEmp
        '
        Me.objcolhEmp.Tag = "objcolhEmp"
        Me.objcolhEmp.Text = ""
        Me.objcolhEmp.Width = 0
        '
        'objEHeading
        '
        Me.objEHeading.BorderColor = System.Drawing.Color.Black
        Me.objEHeading.Controls.Add(Me.objPanel1)
        Me.objEHeading.Controls.Add(Me.objPanel2)
        Me.objEHeading.Controls.Add(Me.EZeeStraightLine2)
        Me.objEHeading.Dock = System.Windows.Forms.DockStyle.Top
        Me.objEHeading.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objEHeading.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objEHeading.Location = New System.Drawing.Point(0, 0)
        Me.objEHeading.Name = "objEHeading"
        Me.objEHeading.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.objEHeading.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objEHeading.ShowDefaultBorderColor = True
        Me.objEHeading.Size = New System.Drawing.Size(410, 25)
        Me.objEHeading.TabIndex = 5
        '
        'objPanel1
        '
        Me.objPanel1.BackColor = System.Drawing.Color.Transparent
        Me.objPanel1.Controls.Add(Me.radEByActivity)
        Me.objPanel1.Controls.Add(Me.radEByUoM)
        Me.objPanel1.Location = New System.Drawing.Point(4, 1)
        Me.objPanel1.Name = "objPanel1"
        Me.objPanel1.Size = New System.Drawing.Size(197, 23)
        Me.objPanel1.TabIndex = 6
        '
        'radEByActivity
        '
        Me.radEByActivity.BackColor = System.Drawing.Color.Transparent
        Me.radEByActivity.Checked = True
        Me.radEByActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEByActivity.Location = New System.Drawing.Point(3, 3)
        Me.radEByActivity.Name = "radEByActivity"
        Me.radEByActivity.Size = New System.Drawing.Size(106, 17)
        Me.radEByActivity.TabIndex = 1
        Me.radEByActivity.TabStop = True
        Me.radEByActivity.Text = "By Activity"
        Me.radEByActivity.UseVisualStyleBackColor = False
        '
        'radEByUoM
        '
        Me.radEByUoM.BackColor = System.Drawing.Color.Transparent
        Me.radEByUoM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEByUoM.Location = New System.Drawing.Point(115, 3)
        Me.radEByUoM.Name = "radEByUoM"
        Me.radEByUoM.Size = New System.Drawing.Size(79, 17)
        Me.radEByUoM.TabIndex = 2
        Me.radEByUoM.Text = "By UoM"
        Me.radEByUoM.UseVisualStyleBackColor = False
        '
        'objPanel2
        '
        Me.objPanel2.Controls.Add(Me.radEByDate)
        Me.objPanel2.Controls.Add(Me.radEPeriod)
        Me.objPanel2.Location = New System.Drawing.Point(212, 1)
        Me.objPanel2.Name = "objPanel2"
        Me.objPanel2.Size = New System.Drawing.Size(193, 23)
        Me.objPanel2.TabIndex = 7
        '
        'radEByDate
        '
        Me.radEByDate.BackColor = System.Drawing.Color.Transparent
        Me.radEByDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEByDate.Location = New System.Drawing.Point(3, 3)
        Me.radEByDate.Name = "radEByDate"
        Me.radEByDate.Size = New System.Drawing.Size(89, 17)
        Me.radEByDate.TabIndex = 6
        Me.radEByDate.Text = "By Date"
        Me.radEByDate.UseVisualStyleBackColor = False
        '
        'radEPeriod
        '
        Me.radEPeriod.BackColor = System.Drawing.Color.Transparent
        Me.radEPeriod.Checked = True
        Me.radEPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEPeriod.Location = New System.Drawing.Point(98, 3)
        Me.radEPeriod.Name = "radEPeriod"
        Me.radEPeriod.Size = New System.Drawing.Size(89, 17)
        Me.radEPeriod.TabIndex = 7
        Me.radEPeriod.TabStop = True
        Me.radEPeriod.Text = "By Period"
        Me.radEPeriod.UseVisualStyleBackColor = False
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(204, 0)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(1, 24)
        Me.EZeeStraightLine2.TabIndex = 4
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'tabpCompanyTotals
        '
        Me.tabpCompanyTotals.Controls.Add(Me.plnCompanyTotals)
        Me.tabpCompanyTotals.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompanyTotals.Name = "tabpCompanyTotals"
        Me.tabpCompanyTotals.Size = New System.Drawing.Size(410, 487)
        Me.tabpCompanyTotals.TabIndex = 1
        Me.tabpCompanyTotals.Text = "Company Totals"
        Me.tabpCompanyTotals.UseVisualStyleBackColor = True
        '
        'plnCompanyTotals
        '
        Me.plnCompanyTotals.Controls.Add(Me.objCHeading)
        Me.plnCompanyTotals.Controls.Add(Me.objHeading)
        Me.plnCompanyTotals.Controls.Add(Me.lvCompanyTotal)
        Me.plnCompanyTotals.Controls.Add(Me.objchkAll)
        Me.plnCompanyTotals.Controls.Add(Me.lblAllocation)
        Me.plnCompanyTotals.Controls.Add(Me.cboAllocations)
        Me.plnCompanyTotals.Controls.Add(Me.lvAllocation)
        Me.plnCompanyTotals.Dock = System.Windows.Forms.DockStyle.Fill
        Me.plnCompanyTotals.Location = New System.Drawing.Point(0, 0)
        Me.plnCompanyTotals.Name = "plnCompanyTotals"
        Me.plnCompanyTotals.Size = New System.Drawing.Size(410, 487)
        Me.plnCompanyTotals.TabIndex = 6
        '
        'objCHeading
        '
        Me.objCHeading.BorderColor = System.Drawing.Color.Black
        Me.objCHeading.Controls.Add(Me.radPeriod)
        Me.objCHeading.Controls.Add(Me.radByDate)
        Me.objCHeading.Dock = System.Windows.Forms.DockStyle.Top
        Me.objCHeading.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCHeading.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objCHeading.Location = New System.Drawing.Point(0, 0)
        Me.objCHeading.Name = "objCHeading"
        Me.objCHeading.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.objCHeading.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objCHeading.ShowDefaultBorderColor = True
        Me.objCHeading.Size = New System.Drawing.Size(410, 25)
        Me.objCHeading.TabIndex = 4
        '
        'radPeriod
        '
        Me.radPeriod.BackColor = System.Drawing.Color.Transparent
        Me.radPeriod.Checked = True
        Me.radPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radPeriod.Location = New System.Drawing.Point(118, 4)
        Me.radPeriod.Name = "radPeriod"
        Me.radPeriod.Size = New System.Drawing.Size(108, 17)
        Me.radPeriod.TabIndex = 7
        Me.radPeriod.TabStop = True
        Me.radPeriod.Text = "By Period"
        Me.radPeriod.UseVisualStyleBackColor = False
        '
        'radByDate
        '
        Me.radByDate.BackColor = System.Drawing.Color.Transparent
        Me.radByDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radByDate.Location = New System.Drawing.Point(4, 4)
        Me.radByDate.Name = "radByDate"
        Me.radByDate.Size = New System.Drawing.Size(108, 17)
        Me.radByDate.TabIndex = 6
        Me.radByDate.Text = "By Date"
        Me.radByDate.UseVisualStyleBackColor = False
        '
        'objHeading
        '
        Me.objHeading.BorderColor = System.Drawing.Color.Black
        Me.objHeading.Controls.Add(Me.radCByActivity)
        Me.objHeading.Controls.Add(Me.radCByUoM)
        Me.objHeading.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objHeading.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objHeading.Location = New System.Drawing.Point(2, 235)
        Me.objHeading.Name = "objHeading"
        Me.objHeading.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.objHeading.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objHeading.ShowDefaultBorderColor = True
        Me.objHeading.Size = New System.Drawing.Size(406, 25)
        Me.objHeading.TabIndex = 1
        '
        'radCByActivity
        '
        Me.radCByActivity.BackColor = System.Drawing.Color.Transparent
        Me.radCByActivity.Checked = True
        Me.radCByActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCByActivity.Location = New System.Drawing.Point(11, 4)
        Me.radCByActivity.Name = "radCByActivity"
        Me.radCByActivity.Size = New System.Drawing.Size(95, 17)
        Me.radCByActivity.TabIndex = 0
        Me.radCByActivity.TabStop = True
        Me.radCByActivity.Text = "By Activity"
        Me.radCByActivity.UseVisualStyleBackColor = False
        '
        'radCByUoM
        '
        Me.radCByUoM.BackColor = System.Drawing.Color.Transparent
        Me.radCByUoM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCByUoM.Location = New System.Drawing.Point(115, 4)
        Me.radCByUoM.Name = "radCByUoM"
        Me.radCByUoM.Size = New System.Drawing.Size(179, 17)
        Me.radCByUoM.TabIndex = 1
        Me.radCByUoM.Text = "By UoM (Unit Of Measure)"
        Me.radCByUoM.UseVisualStyleBackColor = False
        '
        'lvCompanyTotal
        '
        Me.lvCompanyTotal.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCDisplayFor, Me.colhCTotal})
        Me.lvCompanyTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvCompanyTotal.GridLines = True
        Me.lvCompanyTotal.Location = New System.Drawing.Point(2, 261)
        Me.lvCompanyTotal.Name = "lvCompanyTotal"
        Me.lvCompanyTotal.Size = New System.Drawing.Size(405, 223)
        Me.lvCompanyTotal.TabIndex = 0
        Me.lvCompanyTotal.UseCompatibleStateImageBehavior = False
        Me.lvCompanyTotal.View = System.Windows.Forms.View.Details
        '
        'colhCDisplayFor
        '
        Me.colhCDisplayFor.Tag = "colhCDisplayFor"
        Me.colhCDisplayFor.Text = ""
        Me.colhCDisplayFor.Width = 258
        '
        'colhCTotal
        '
        Me.colhCTotal.Tag = "colhCTotal"
        Me.colhCTotal.Text = "Total"
        Me.colhCTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhCTotal.Width = 125
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(382, 63)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 2
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(4, 34)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(71, 15)
        Me.lblAllocation.TabIndex = 4
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(81, 31)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(324, 21)
        Me.cboAllocations.TabIndex = 5
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(2, 58)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(406, 176)
        Me.lvAllocation.TabIndex = 3
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocations
        '
        Me.colhAllocations.Tag = "colhAllocations"
        Me.colhAllocations.Text = ""
        Me.colhAllocations.Width = 366
        '
        'gbOperation
        '
        Me.gbOperation.BackColor = System.Drawing.SystemColors.Control
        Me.gbOperation.BorderColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbOperation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.gbOperation.Checked = False
        Me.gbOperation.CollapseAllExceptThis = False
        Me.gbOperation.CollapsedHoverImage = Nothing
        Me.gbOperation.CollapsedNormalImage = Nothing
        Me.gbOperation.CollapsedPressedImage = Nothing
        Me.gbOperation.CollapseOnLoad = False
        Me.gbOperation.Controls.Add(Me.rdUpdateExisting)
        Me.gbOperation.Controls.Add(Me.rdAddNew)
        Me.gbOperation.Controls.Add(Me.rdNoAction)
        Me.gbOperation.Controls.Add(Me.EZeeFooter1)
        Me.gbOperation.ExpandedHoverImage = Nothing
        Me.gbOperation.ExpandedNormalImage = Nothing
        Me.gbOperation.ExpandedPressedImage = Nothing
        Me.gbOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOperation.GradientColor = System.Drawing.SystemColors.ControlLight
        Me.gbOperation.HeaderHeight = 25
        Me.gbOperation.HeaderMessage = ""
        Me.gbOperation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbOperation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOperation.HeightOnCollapse = 0
        Me.gbOperation.LeftTextSpace = 0
        Me.gbOperation.Location = New System.Drawing.Point(383, 199)
        Me.gbOperation.Name = "gbOperation"
        Me.gbOperation.OpenHeight = 300
        Me.gbOperation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOperation.ShowBorder = True
        Me.gbOperation.ShowCheckBox = False
        Me.gbOperation.ShowCollapseButton = False
        Me.gbOperation.ShowDefaultBorderColor = True
        Me.gbOperation.ShowDownButton = False
        Me.gbOperation.ShowHeader = True
        Me.gbOperation.Size = New System.Drawing.Size(297, 179)
        Me.gbOperation.TabIndex = 19
        Me.gbOperation.Temp = 0
        Me.gbOperation.Text = "Set Operation"
        Me.gbOperation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.gbOperation.Visible = False
        '
        'rdUpdateExisting
        '
        Me.rdUpdateExisting.Checked = True
        Me.rdUpdateExisting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdUpdateExisting.Location = New System.Drawing.Point(15, 42)
        Me.rdUpdateExisting.Name = "rdUpdateExisting"
        Me.rdUpdateExisting.Size = New System.Drawing.Size(266, 17)
        Me.rdUpdateExisting.TabIndex = 2
        Me.rdUpdateExisting.TabStop = True
        Me.rdUpdateExisting.Text = "Update Existing"
        Me.rdUpdateExisting.UseVisualStyleBackColor = True
        '
        'rdAddNew
        '
        Me.rdAddNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAddNew.Location = New System.Drawing.Point(15, 66)
        Me.rdAddNew.Name = "rdAddNew"
        Me.rdAddNew.Size = New System.Drawing.Size(266, 17)
        Me.rdAddNew.TabIndex = 2
        Me.rdAddNew.Text = "Add New"
        Me.rdAddNew.UseVisualStyleBackColor = True
        '
        'rdNoAction
        '
        Me.rdNoAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdNoAction.Location = New System.Drawing.Point(15, 89)
        Me.rdNoAction.Name = "rdNoAction"
        Me.rdNoAction.Size = New System.Drawing.Size(266, 17)
        Me.rdNoAction.TabIndex = 2
        Me.rdNoAction.Text = "Take No Action"
        Me.rdNoAction.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnOk)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 127)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(295, 50)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(182, 10)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 2
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnChangeDate)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(993, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnChangeDate
        '
        Me.btnChangeDate.BackColor = System.Drawing.Color.White
        Me.btnChangeDate.BackgroundImage = CType(resources.GetObject("btnChangeDate.BackgroundImage"), System.Drawing.Image)
        Me.btnChangeDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnChangeDate.BorderColor = System.Drawing.Color.Empty
        Me.btnChangeDate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnChangeDate.FlatAppearance.BorderSize = 0
        Me.btnChangeDate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeDate.ForeColor = System.Drawing.Color.Black
        Me.btnChangeDate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnChangeDate.GradientForeColor = System.Drawing.Color.Black
        Me.btnChangeDate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeDate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeDate.Location = New System.Drawing.Point(123, 13)
        Me.btnChangeDate.Name = "btnChangeDate"
        Me.btnChangeDate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeDate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeDate.Size = New System.Drawing.Size(121, 30)
        Me.btnChangeDate.TabIndex = 6
        Me.btnChangeDate.Text = "C&hange Date"
        Me.btnChangeDate.UseVisualStyleBackColor = True
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(11, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(106, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperations
        Me.btnOperations.TabIndex = 0
        Me.btnOperations.Text = "Operations"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExportPPA, Me.mnuGetFileFormat, Me.mnuImportPPA, Me.mnuGlobalAssign, Me.mnuExportWeeklyPPATemplate})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(271, 114)
        '
        'mnuExportPPA
        '
        Me.mnuExportPPA.Name = "mnuExportPPA"
        Me.mnuExportPPA.Size = New System.Drawing.Size(270, 22)
        Me.mnuExportPPA.Tag = "mnuExportPPA"
        Me.mnuExportPPA.Text = "Export Pay Per Activity"
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(270, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "Get File Format"
        '
        'mnuImportPPA
        '
        Me.mnuImportPPA.Name = "mnuImportPPA"
        Me.mnuImportPPA.Size = New System.Drawing.Size(270, 22)
        Me.mnuImportPPA.Tag = "mnuImportPPA"
        Me.mnuImportPPA.Text = "Import Pay Per Activity"
        '
        'mnuGlobalAssign
        '
        Me.mnuGlobalAssign.Name = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Size = New System.Drawing.Size(270, 22)
        Me.mnuGlobalAssign.Text = "Global Assign Pay Per Activity"
        '
        'mnuExportWeeklyPPATemplate
        '
        Me.mnuExportWeeklyPPATemplate.Name = "mnuExportWeeklyPPATemplate"
        Me.mnuExportWeeklyPPATemplate.Size = New System.Drawing.Size(270, 22)
        Me.mnuExportWeeklyPPATemplate.Text = "Export Weekly Pay Per Activity Template"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(781, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(884, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbActivityDetails
        '
        Me.gbActivityDetails.BorderColor = System.Drawing.Color.Black
        Me.gbActivityDetails.Checked = False
        Me.gbActivityDetails.CollapseAllExceptThis = False
        Me.gbActivityDetails.CollapsedHoverImage = Nothing
        Me.gbActivityDetails.CollapsedNormalImage = Nothing
        Me.gbActivityDetails.CollapsedPressedImage = Nothing
        Me.gbActivityDetails.CollapseOnLoad = False
        Me.gbActivityDetails.Controls.Add(Me.chkSelectAll)
        Me.gbActivityDetails.Controls.Add(Me.lblAmount)
        Me.gbActivityDetails.Controls.Add(Me.txtFinalAmt)
        Me.gbActivityDetails.Controls.Add(Me.txtRate)
        Me.gbActivityDetails.Controls.Add(Me.lblActivityRate)
        Me.gbActivityDetails.Controls.Add(Me.cboPeriod)
        Me.gbActivityDetails.Controls.Add(Me.dtpDate)
        Me.gbActivityDetails.Controls.Add(Me.lvDetails)
        Me.gbActivityDetails.Controls.Add(Me.chkCostCenter)
        Me.gbActivityDetails.Controls.Add(Me.lblEmployee)
        Me.gbActivityDetails.Controls.Add(Me.cboEmployee)
        Me.gbActivityDetails.Controls.Add(Me.lblDate)
        Me.gbActivityDetails.Controls.Add(Me.cboActivity)
        Me.gbActivityDetails.Controls.Add(Me.txtActivity)
        Me.gbActivityDetails.Controls.Add(Me.lblActivityCode)
        Me.gbActivityDetails.Controls.Add(Me.lblActivity)
        Me.gbActivityDetails.Controls.Add(Me.cboCostcenter)
        Me.gbActivityDetails.Controls.Add(Me.txtMeasure)
        Me.gbActivityDetails.Controls.Add(Me.lblPeriod)
        Me.gbActivityDetails.Controls.Add(Me.lblMeasure)
        Me.gbActivityDetails.Controls.Add(Me.lblValue)
        Me.gbActivityDetails.Controls.Add(Me.txtValue)
        Me.gbActivityDetails.ExpandedHoverImage = Nothing
        Me.gbActivityDetails.ExpandedNormalImage = Nothing
        Me.gbActivityDetails.ExpandedPressedImage = Nothing
        Me.gbActivityDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbActivityDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbActivityDetails.HeaderHeight = 25
        Me.gbActivityDetails.HeaderMessage = ""
        Me.gbActivityDetails.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbActivityDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbActivityDetails.HeightOnCollapse = 0
        Me.gbActivityDetails.LeftTextSpace = 0
        Me.gbActivityDetails.Location = New System.Drawing.Point(3, 3)
        Me.gbActivityDetails.Name = "gbActivityDetails"
        Me.gbActivityDetails.OpenHeight = 300
        Me.gbActivityDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbActivityDetails.ShowBorder = True
        Me.gbActivityDetails.ShowCheckBox = False
        Me.gbActivityDetails.ShowCollapseButton = False
        Me.gbActivityDetails.ShowDefaultBorderColor = True
        Me.gbActivityDetails.ShowDownButton = False
        Me.gbActivityDetails.ShowHeader = True
        Me.gbActivityDetails.Size = New System.Drawing.Size(566, 513)
        Me.gbActivityDetails.TabIndex = 20
        Me.gbActivityDetails.Temp = 0
        Me.gbActivityDetails.Text = "Set Activity Information"
        Me.gbActivityDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(11, 147)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 74
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(354, 117)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(84, 15)
        Me.lblAmount.TabIndex = 22
        Me.lblAmount.Text = "Amount"
        '
        'txtFinalAmt
        '
        Me.txtFinalAmt.AcceptsReturn = True
        Me.txtFinalAmt.AcceptsTab = True
        Me.txtFinalAmt.AllowNegative = True
        Me.txtFinalAmt.BackColor = System.Drawing.Color.White
        Me.txtFinalAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtFinalAmt.DigitsInGroup = 0
        Me.txtFinalAmt.Flags = 0
        Me.txtFinalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinalAmt.Location = New System.Drawing.Point(441, 114)
        Me.txtFinalAmt.MaxDecimalPlaces = 6
        Me.txtFinalAmt.MaxWholeDigits = 21
        Me.txtFinalAmt.Name = "txtFinalAmt"
        Me.txtFinalAmt.Prefix = ""
        Me.txtFinalAmt.RangeMax = 1.7976931348623157E+308
        Me.txtFinalAmt.RangeMin = -1.7976931348623157E+308
        Me.txtFinalAmt.ReadOnly = True
        Me.txtFinalAmt.Size = New System.Drawing.Size(116, 21)
        Me.txtFinalAmt.TabIndex = 23
        Me.txtFinalAmt.Text = "0"
        Me.txtFinalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRate
        '
        Me.txtRate.BackColor = System.Drawing.Color.White
        Me.txtRate.Flags = 0
        Me.txtRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRate.Location = New System.Drawing.Point(85, 114)
        Me.txtRate.Name = "txtRate"
        Me.txtRate.ReadOnly = True
        Me.txtRate.Size = New System.Drawing.Size(119, 21)
        Me.txtRate.TabIndex = 21
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActivityRate
        '
        Me.lblActivityRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityRate.Location = New System.Drawing.Point(5, 117)
        Me.lblActivityRate.Name = "lblActivityRate"
        Me.lblActivityRate.Size = New System.Drawing.Size(74, 15)
        Me.lblActivityRate.TabIndex = 20
        Me.lblActivityRate.Text = "Activity Rate"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(85, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(119, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'dtpDate
        '
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(251, 33)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpDate.TabIndex = 2
        '
        'lvDetails
        '
        Me.lvDetails.BackColorOnChecked = False
        Me.lvDetails.CheckBoxes = True
        Me.lvDetails.ColumnHeaders = Nothing
        Me.lvDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhDate, Me.colhActivity, Me.colhValue, Me.colhUoM, Me.colhRate, Me.colhAmount, Me.colhCostCenter, Me.objcolhEmployee, Me.objcolhIsPosted})
        Me.lvDetails.CompulsoryColumns = ""
        Me.lvDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDetails.FullRowSelect = True
        Me.lvDetails.GridLines = True
        Me.lvDetails.GroupingColumn = Nothing
        Me.lvDetails.HideSelection = False
        Me.lvDetails.Location = New System.Drawing.Point(3, 141)
        Me.lvDetails.MinColumnWidth = 50
        Me.lvDetails.MultiSelect = False
        Me.lvDetails.Name = "lvDetails"
        Me.lvDetails.OptionalColumns = ""
        Me.lvDetails.ShowMoreItem = False
        Me.lvDetails.ShowSaveItem = False
        Me.lvDetails.ShowSelectAll = True
        Me.lvDetails.ShowSizeAllColumnsToFit = True
        Me.lvDetails.Size = New System.Drawing.Size(554, 370)
        Me.lvDetails.Sortable = True
        Me.lvDetails.TabIndex = 12
        Me.lvDetails.UseCompatibleStateImageBehavior = False
        Me.lvDetails.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 30
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 100
        '
        'colhActivity
        '
        Me.colhActivity.Tag = "colhActivity"
        Me.colhActivity.Text = "Activity"
        Me.colhActivity.Width = 120
        '
        'colhValue
        '
        Me.colhValue.Tag = "colhValue"
        Me.colhValue.Text = "Value"
        Me.colhValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhValue.Width = 80
        '
        'colhUoM
        '
        Me.colhUoM.Tag = "colhUoM"
        Me.colhUoM.Text = "UoM"
        Me.colhUoM.Width = 55
        '
        'colhRate
        '
        Me.colhRate.Tag = "colhRate"
        Me.colhRate.Text = "Rate"
        Me.colhRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhRate.Width = 110
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 110
        '
        'colhCostCenter
        '
        Me.colhCostCenter.Tag = "colhCostCenter"
        Me.colhCostCenter.Text = "Cost Center"
        Me.colhCostCenter.Width = 90
        '
        'objcolhEmployee
        '
        Me.objcolhEmployee.Tag = "objcolhEmployee"
        Me.objcolhEmployee.Text = "Employee"
        Me.objcolhEmployee.Width = 0
        '
        'objcolhIsPosted
        '
        Me.objcolhIsPosted.Tag = "objcolhIsPosted"
        Me.objcolhIsPosted.Text = ""
        Me.objcolhIsPosted.Width = 0
        '
        'chkCostCenter
        '
        Me.chkCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCostCenter.Location = New System.Drawing.Point(354, 36)
        Me.chkCostCenter.Name = "chkCostCenter"
        Me.chkCostCenter.Size = New System.Drawing.Size(84, 15)
        Me.chkCostCenter.TabIndex = 4
        Me.chkCostCenter.TabStop = False
        Me.chkCostCenter.Text = "Cost Center"
        Me.chkCostCenter.UseVisualStyleBackColor = True
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(5, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 6
        Me.lblEmployee.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(85, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(261, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(210, 36)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(37, 15)
        Me.lblDate.TabIndex = 2
        Me.lblDate.Text = "Date"
        '
        'cboActivity
        '
        Me.cboActivity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.cboActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboActivity.DropDownWidth = 350
        Me.cboActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivity.FormattingEnabled = True
        Me.cboActivity.Location = New System.Drawing.Point(441, 60)
        Me.cboActivity.Name = "cboActivity"
        Me.cboActivity.Size = New System.Drawing.Size(116, 21)
        Me.cboActivity.TabIndex = 4
        '
        'txtActivity
        '
        Me.txtActivity.BackColor = System.Drawing.Color.White
        Me.txtActivity.Flags = 0
        Me.txtActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActivity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtActivity.Location = New System.Drawing.Point(85, 87)
        Me.txtActivity.Name = "txtActivity"
        Me.txtActivity.ReadOnly = True
        Me.txtActivity.Size = New System.Drawing.Size(261, 21)
        Me.txtActivity.TabIndex = 18
        '
        'lblActivityCode
        '
        Me.lblActivityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityCode.Location = New System.Drawing.Point(354, 63)
        Me.lblActivityCode.Name = "lblActivityCode"
        Me.lblActivityCode.Size = New System.Drawing.Size(84, 15)
        Me.lblActivityCode.TabIndex = 8
        Me.lblActivityCode.Text = "Activity Code"
        '
        'lblActivity
        '
        Me.lblActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivity.Location = New System.Drawing.Point(5, 90)
        Me.lblActivity.Name = "lblActivity"
        Me.lblActivity.Size = New System.Drawing.Size(74, 15)
        Me.lblActivity.TabIndex = 17
        Me.lblActivity.Text = "Activity"
        '
        'cboCostcenter
        '
        Me.cboCostcenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboCostcenter.DropDownWidth = 350
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(441, 33)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(116, 21)
        Me.cboCostcenter.TabIndex = 20
        Me.cboCostcenter.TabStop = False
        '
        'txtMeasure
        '
        Me.txtMeasure.BackColor = System.Drawing.Color.White
        Me.txtMeasure.Flags = 0
        Me.txtMeasure.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMeasure.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMeasure.Location = New System.Drawing.Point(441, 87)
        Me.txtMeasure.Name = "txtMeasure"
        Me.txtMeasure.ReadOnly = True
        Me.txtMeasure.Size = New System.Drawing.Size(116, 21)
        Me.txtMeasure.TabIndex = 14
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(5, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(74, 15)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Period"
        '
        'lblMeasure
        '
        Me.lblMeasure.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeasure.Location = New System.Drawing.Point(354, 90)
        Me.lblMeasure.Name = "lblMeasure"
        Me.lblMeasure.Size = New System.Drawing.Size(84, 15)
        Me.lblMeasure.TabIndex = 13
        Me.lblMeasure.Text = "Measure"
        '
        'lblValue
        '
        Me.lblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValue.Location = New System.Drawing.Point(210, 117)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(37, 15)
        Me.lblValue.TabIndex = 10
        Me.lblValue.Text = "Value"
        '
        'txtValue
        '
        Me.txtValue.AcceptsReturn = True
        Me.txtValue.AcceptsTab = True
        Me.txtValue.AllowNegative = False
        Me.txtValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtValue.DigitsInGroup = 0
        Me.txtValue.Flags = 65536
        Me.txtValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValue.Location = New System.Drawing.Point(251, 114)
        Me.txtValue.MaxDecimalPlaces = 6
        Me.txtValue.MaxWholeDigits = 21
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Prefix = ""
        Me.txtValue.RangeMax = 1.7976931348623157E+308
        Me.txtValue.RangeMin = -1.7976931348623157E+308
        Me.txtValue.Size = New System.Drawing.Size(95, 21)
        Me.txtValue.TabIndex = 5
        Me.txtValue.Text = "0"
        Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmBulkInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(993, 572)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBulkInput"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Pay Per Activity"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tabcTotals.ResumeLayout(False)
        Me.tabpEmployeeTotals.ResumeLayout(False)
        Me.pnlEmpTotal.ResumeLayout(False)
        Me.objEHeading.ResumeLayout(False)
        Me.objPanel1.ResumeLayout(False)
        Me.objPanel2.ResumeLayout(False)
        Me.tabpCompanyTotals.ResumeLayout(False)
        Me.plnCompanyTotals.ResumeLayout(False)
        Me.plnCompanyTotals.PerformLayout()
        Me.objCHeading.ResumeLayout(False)
        Me.objHeading.ResumeLayout(False)
        Me.gbOperation.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.gbActivityDetails.ResumeLayout(False)
        Me.gbActivityDetails.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkCostCenter As System.Windows.Forms.CheckBox
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboActivity As System.Windows.Forms.ComboBox
    Friend WithEvents lblActivityCode As System.Windows.Forms.Label
    Friend WithEvents lblValue As System.Windows.Forms.Label
    Friend WithEvents txtValue As eZee.TextBox.NumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents txtMeasure As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMeasure As System.Windows.Forms.Label
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents txtActivity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblActivity As System.Windows.Forms.Label
    Friend WithEvents radEByActivity As System.Windows.Forms.RadioButton
    Friend WithEvents radEByUoM As System.Windows.Forms.RadioButton
    Friend WithEvents lvEmpTotal As eZee.Common.eZeeListView
    Friend WithEvents colhEDisplayFor As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhETotal As System.Windows.Forms.ColumnHeader
    Friend WithEvents radByDate As System.Windows.Forms.RadioButton
    Friend WithEvents radPeriod As System.Windows.Forms.RadioButton
    Friend WithEvents radCByActivity As System.Windows.Forms.RadioButton
    Friend WithEvents radCByUoM As System.Windows.Forms.RadioButton
    Friend WithEvents lvDetails As eZee.Common.eZeeListView
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhActivity As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUoM As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCostCenter As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvCompanyTotal As System.Windows.Forms.ListView
    Friend WithEvents objHeading As eZee.Common.eZeeHeading
    Friend WithEvents colhCDisplayFor As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCTotal As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnChangeDate As eZee.Common.eZeeLightButton
    Friend WithEvents gbActivityDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbOperation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents rdUpdateExisting As System.Windows.Forms.RadioButton
    Friend WithEvents rdAddNew As System.Windows.Forms.RadioButton
    Friend WithEvents rdNoAction As System.Windows.Forms.RadioButton
    Friend WithEvents txtRate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblActivityRate As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtFinalAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents colhRate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsPosted As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmp As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportPPA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportPPA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuGlobalAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportWeeklyPPATemplate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tabcTotals As System.Windows.Forms.TabControl
    Friend WithEvents tabpEmployeeTotals As System.Windows.Forms.TabPage
    Friend WithEvents tabpCompanyTotals As System.Windows.Forms.TabPage
    Friend WithEvents objCHeading As eZee.Common.eZeeHeading
    Friend WithEvents objEHeading As eZee.Common.eZeeHeading
    Friend WithEvents radEPeriod As System.Windows.Forms.RadioButton
    Friend WithEvents radEByDate As System.Windows.Forms.RadioButton
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents pnlEmpTotal As System.Windows.Forms.Panel
    Friend WithEvents plnCompanyTotals As System.Windows.Forms.Panel
    Friend WithEvents objPanel2 As System.Windows.Forms.Panel
    Friend WithEvents objPanel1 As System.Windows.Forms.Panel
End Class
