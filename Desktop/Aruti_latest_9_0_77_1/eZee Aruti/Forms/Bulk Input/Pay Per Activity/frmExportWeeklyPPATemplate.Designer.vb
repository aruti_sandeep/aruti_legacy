﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExportWeeklyPPATemplate
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportWeeklyPPATemplate))
        Me.gbDateFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.LblToDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.LblFromDate = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnExport = New eZee.Common.eZeeSplitButton
        Me.objmnuReportExport = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.objmnuExportToRichText = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToWord = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToExcel = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToPDF = New System.Windows.Forms.ToolStripMenuItem
        Me.objmnuExportToHTML = New System.Windows.Forms.ToolStripMenuItem
        Me.btnReport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbDateFilter.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.objmnuReportExport.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbDateFilter
        '
        Me.gbDateFilter.BorderColor = System.Drawing.Color.Black
        Me.gbDateFilter.Checked = False
        Me.gbDateFilter.CollapseAllExceptThis = False
        Me.gbDateFilter.CollapsedHoverImage = Nothing
        Me.gbDateFilter.CollapsedNormalImage = Nothing
        Me.gbDateFilter.CollapsedPressedImage = Nothing
        Me.gbDateFilter.CollapseOnLoad = False
        Me.gbDateFilter.Controls.Add(Me.lnkAnalysisBy)
        Me.gbDateFilter.Controls.Add(Me.LblToDate)
        Me.gbDateFilter.Controls.Add(Me.dtpToDate)
        Me.gbDateFilter.Controls.Add(Me.dtpStartDate)
        Me.gbDateFilter.Controls.Add(Me.LblFromDate)
        Me.gbDateFilter.ExpandedHoverImage = Nothing
        Me.gbDateFilter.ExpandedNormalImage = Nothing
        Me.gbDateFilter.ExpandedPressedImage = Nothing
        Me.gbDateFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDateFilter.HeaderHeight = 25
        Me.gbDateFilter.HeaderMessage = ""
        Me.gbDateFilter.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDateFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDateFilter.HeightOnCollapse = 0
        Me.gbDateFilter.LeftTextSpace = 0
        Me.gbDateFilter.Location = New System.Drawing.Point(3, 1)
        Me.gbDateFilter.Name = "gbDateFilter"
        Me.gbDateFilter.OpenHeight = 300
        Me.gbDateFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDateFilter.ShowBorder = True
        Me.gbDateFilter.ShowCheckBox = False
        Me.gbDateFilter.ShowCollapseButton = False
        Me.gbDateFilter.ShowDefaultBorderColor = True
        Me.gbDateFilter.ShowDownButton = False
        Me.gbDateFilter.ShowHeader = True
        Me.gbDateFilter.Size = New System.Drawing.Size(366, 70)
        Me.gbDateFilter.TabIndex = 0
        Me.gbDateFilter.Temp = 0
        Me.gbDateFilter.Text = "Filter Criteria"
        Me.gbDateFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(270, 3)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 196
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblToDate
        '
        Me.LblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToDate.Location = New System.Drawing.Point(189, 36)
        Me.LblToDate.Name = "LblToDate"
        Me.LblToDate.Size = New System.Drawing.Size(69, 15)
        Me.LblToDate.TabIndex = 6
        Me.LblToDate.Text = "To Date"
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(263, 33)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpToDate.TabIndex = 5
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(89, 33)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpStartDate.TabIndex = 4
        '
        'LblFromDate
        '
        Me.LblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFromDate.Location = New System.Drawing.Point(9, 36)
        Me.LblFromDate.Name = "LblFromDate"
        Me.LblFromDate.Size = New System.Drawing.Size(74, 15)
        Me.LblFromDate.TabIndex = 2
        Me.LblFromDate.Text = "Start Date"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnReport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 75)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(373, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'objbtnExport
        '
        Me.objbtnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnExport.BorderColor = System.Drawing.Color.Black
        Me.objbtnExport.ContextMenuStrip = Me.objmnuReportExport
        Me.objbtnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnExport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnExport.Location = New System.Drawing.Point(65, 13)
        Me.objbtnExport.Name = "objbtnExport"
        Me.objbtnExport.ShowDefaultBorderColor = True
        Me.objbtnExport.Size = New System.Drawing.Size(90, 30)
        Me.objbtnExport.SplitButtonMenu = Me.objmnuReportExport
        Me.objbtnExport.TabIndex = 6
        Me.objbtnExport.Text = "&Export"
        '
        'objmnuReportExport
        '
        Me.objmnuReportExport.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objmnuExportToRichText, Me.objmnuExportToWord, Me.objmnuExportToExcel, Me.objmnuExportToPDF, Me.objmnuExportToHTML})
        Me.objmnuReportExport.Name = "ContextMenuStrip1"
        Me.objmnuReportExport.Size = New System.Drawing.Size(120, 114)
        '
        'objmnuExportToRichText
        '
        Me.objmnuExportToRichText.Name = "objmnuExportToRichText"
        Me.objmnuExportToRichText.Size = New System.Drawing.Size(119, 22)
        Me.objmnuExportToRichText.Text = "RichText "
        '
        'objmnuExportToWord
        '
        Me.objmnuExportToWord.Name = "objmnuExportToWord"
        Me.objmnuExportToWord.Size = New System.Drawing.Size(119, 22)
        Me.objmnuExportToWord.Text = "Word "
        '
        'objmnuExportToExcel
        '
        Me.objmnuExportToExcel.Name = "objmnuExportToExcel"
        Me.objmnuExportToExcel.Size = New System.Drawing.Size(119, 22)
        Me.objmnuExportToExcel.Text = "Excel "
        '
        'objmnuExportToPDF
        '
        Me.objmnuExportToPDF.Name = "objmnuExportToPDF"
        Me.objmnuExportToPDF.Size = New System.Drawing.Size(119, 22)
        Me.objmnuExportToPDF.Text = "PDF"
        '
        'objmnuExportToHTML
        '
        Me.objmnuExportToHTML.Name = "objmnuExportToHTML"
        Me.objmnuExportToHTML.Size = New System.Drawing.Size(119, 22)
        Me.objmnuExportToHTML.Text = "HTML "
        '
        'btnReport
        '
        Me.btnReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReport.BackColor = System.Drawing.Color.White
        Me.btnReport.BackgroundImage = CType(resources.GetObject("btnReport.BackgroundImage"), System.Drawing.Image)
        Me.btnReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReport.BorderColor = System.Drawing.Color.Empty
        Me.btnReport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReport.FlatAppearance.BorderSize = 0
        Me.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.ForeColor = System.Drawing.Color.Black
        Me.btnReport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReport.GradientForeColor = System.Drawing.Color.Black
        Me.btnReport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReport.Location = New System.Drawing.Point(161, 13)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReport.Size = New System.Drawing.Size(97, 30)
        Me.btnReport.TabIndex = 5
        Me.btnReport.Text = "&Report"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(264, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmExportWeeklyPPATemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(373, 130)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.gbDateFilter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportWeeklyPPATemplate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Export Weekly Pay Per Activity Template"
        Me.gbDateFilter.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.objmnuReportExport.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbDateFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents LblFromDate As System.Windows.Forms.Label
    Friend WithEvents LblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents objmnuReportExport As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents objmnuExportToRichText As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuExportToWord As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuExportToExcel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuExportToPDF As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objmnuExportToHTML As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objbtnExport As eZee.Common.eZeeSplitButton
End Class
