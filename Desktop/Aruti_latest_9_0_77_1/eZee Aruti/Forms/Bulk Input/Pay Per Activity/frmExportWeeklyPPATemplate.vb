﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region

Public Class frmExportWeeklyPPATemplate

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmExportWeeklyPPATemplate"
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrReportName As String = ""
    Dim objExportPPA As clsExportPPA
#End Region

#Region " Private Methods & Functions "

    Private Function SetFilrer() As Boolean
        Try
            objExportPPA._StartDate = dtpStartDate.Value.Date
            objExportPPA._EndDate = dtpToDate.Value.Date
            objExportPPA._UserUnkId = User._Object._Userunkid
            objExportPPA._CompanyUnkId = Company._Object._Companyunkid
            objExportPPA._ViewIndex = mintViewIdx
            objExportPPA._Analysis_Fields = mstrAnalysis_Fields
            objExportPPA._Analysis_Join = mstrAnalysis_Join
            objExportPPA._Analysis_OrderBy = mstrAnalysis_OrderBy
            objExportPPA._Report_GroupName = mstrReport_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilrer", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmExportWeeklyPPATemplate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call Set_Logo(Me, gApplicationType)

            OtherSettings()
            objExportPPA = New clsExportPPA
            dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            objExportPPA._ReportName = Language.getMessage(mstrModuleName, 1, "Weekly Pay Per Activity Report")
            objExportPPA._Company = Company._Object._Name
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExportWeeklyPPATemplate_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExportPPA.SetMessages()
            objfrm._Other_ModuleNames = "clsExportPPA"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilrer() Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objExportPPA.generateReport(0, enPrintAction.Preview, enExportAction.None)
                objExportPPA.generateReportNew(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                  , Company._Object._Companyunkid, dtpStartDate.Value.Date, dtpToDate.Value.Date _
                                                                  , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath _
                                                                  , ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None)
                'Pinkal (24-Aug-2015) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Export Context Menu "

    Private Sub objmnuExportToRichText_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToRichText.Click
        Try
            If SetFilrer() Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objExportPPA.generateReport(0, enPrintAction.None, enExportAction.RichText)
                objExportPPA.generateReportNew(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                  , Company._Object._Companyunkid, dtpStartDate.Value.Date, dtpToDate.Value.Date _
                                                                  , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath _
                                                                  , ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.RichText)
                'Pinkal (24-Aug-2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Data Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objmnuExportToRichText_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objmnuExportToWord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToWord.Click
        Try
            If SetFilrer() Then
                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objExportPPA.generateReport(0, enPrintAction.None, enExportAction.Word)
                objExportPPA.generateReportNew(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                 , Company._Object._Companyunkid, dtpStartDate.Value.Date, dtpToDate.Value.Date _
                                                                 , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath _
                                                                 , ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.Word)
                'Pinkal (24-Aug-2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Data Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objmnuExportToWord_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objmnuExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToExcel.Click
        Try
            If SetFilrer() Then
                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objExportPPA.generateReport(0, enPrintAction.None, enExportAction.Excel)
                objExportPPA.generateReportNew(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                , Company._Object._Companyunkid, dtpStartDate.Value.Date, dtpToDate.Value.Date _
                                                                , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath _
                                                                , ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.Excel)
                'Pinkal (24-Aug-2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Data Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objmnuExportToExcel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objmnuExportToPDF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToPDF.Click
        Try
            If SetFilrer() Then
                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objExportPPA.generateReport(0, enPrintAction.None, enExportAction.PDF)
                objExportPPA.generateReportNew(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                               , Company._Object._Companyunkid, dtpStartDate.Value.Date, dtpToDate.Value.Date _
                                                               , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath _
                                                               , ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.PDF)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Data Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objmnuExportToPDF_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objmnuExportToHTML_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objmnuExportToHTML.Click
        Try
            If SetFilrer() Then
                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objExportPPA.generateReport(0, enPrintAction.None, enExportAction.HTML)
                objExportPPA.generateReportNew(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                               , Company._Object._Companyunkid, dtpStartDate.Value.Date, dtpToDate.Value.Date _
                                                               , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath _
                                                               , ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.HTML)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Data Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objmnuExportToHTML_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DatePicker Event"

    Private Sub dtpStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged
        Try
            dtpToDate.MinDate = CDate("01-Jan-1900")
            dtpToDate.MaxDate = CDate("01-Jan-9998")
            dtpToDate.MinDate = dtpStartDate.Value
            dtpToDate.MaxDate = dtpStartDate.Value.AddDays(6)
            dtpToDate.Value = dtpStartDate.Value.AddDays(6)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbDateFilter.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDateFilter.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnReport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnExport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbDateFilter.Text = Language._Object.getCaption(Me.gbDateFilter.Name, Me.gbDateFilter.Text)
			Me.btnReport.Text = Language._Object.getCaption(Me.btnReport.Name, Me.btnReport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
			Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Weekly Pay Per Activity Report")
			Language.setMessage(mstrModuleName, 2, "Data Exported Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class