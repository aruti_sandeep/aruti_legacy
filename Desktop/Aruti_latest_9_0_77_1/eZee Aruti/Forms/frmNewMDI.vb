﻿#Region " Imports "

Imports System.Windows.Forms
Imports Aruti.Data
'Sandeep [ 10 FEB 2011 ] -- Start
Imports eZeeCommonLib
Imports System
Imports ArutiReports

'Sandeep [ 10 FEB 2011 ] -- End 

#End Region

Public Class frmNewMDI

#Region " Private Variables "

    Public ReadOnly mstrModuleName As String = "frmNewMDI"
    Private mblnFormLoad As Boolean = False
    Public mblnRestart As Boolean = False
    Dim objAppSetting As New clsApplicationSettings

#End Region

#Region " Form Property "

    Public ReadOnly Property _Restart() As Boolean
        Get
            Return mblnRestart
        End Get
    End Property

#End Region

#Region " Private Functions "

    Private Sub SetVisibility()

        Try
            'btnEmployeeMovement.Enabled = User._Object.Privilege._AllowViewEmployeeMovement
            btnBudgetPreparation.Enabled = User._Object.Privilege._AllowPrepareBudget
            'Nilay (28-Apr-2016) -- Start
            'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
            'btnGlobalPayslipMessage.Enabled = User._Object.Privilege._AllowAddEditGPayslipMessage
            'Nilay (28-Apr-2016) -- End
            btnProcessPayroll.Enabled = User._Object.Privilege._AllowProcessPayroll
            btnWagesTable.Enabled = User._Object.Privilege._AllowDefineWageTable
            btnLeaveAccrue.Enabled = User._Object.Privilege._AllowLeaveAccrue
            btnLeaveSchedular.Enabled = User._Object.Privilege._AllowtoViewPlannedLeaveViewer
            btnEmployeeAbsent.Enabled = User._Object.Privilege._AllowProcessEmployeeAbsent
            btnImportData.Enabled = User._Object.Privilege._AllowImportLeaveTnA
            btnExportData.Enabled = User._Object.Privilege._AllowExportLeaveTnA
            btnHoldEmployee.Enabled = User._Object.Privilege._AllowHoldEmployee
            btnGroupBenefit.Enabled = User._Object.Privilege._AllowToAssignBenefitGroup
            btnGlobalAssignED.Enabled = User._Object.Privilege._AllowPerformGlobalAssignOnED
            btnCommonExportData.Enabled = User._Object.Privilege._AllowCommonExport
            btnCommonImportData.Enabled = User._Object.Privilege._AllowCommonImport
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            btnExporttoWeb.Enabled = User._Object.Privilege._AllowtoExportDataOnWeb
            btnImportfromWeb.Enabled = User._Object.Privilege._AllowtoImportDataFromWeb
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Anjan (11 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            btnApplicantFilter.Enabled = User._Object.Privilege._AllowtoShortListFinalShortListApplicants
            btnFinalShortListedApplicant.Enabled = User._Object.Privilege._AllowtoShortListFinalShortListApplicants
            'Anjan (11 Jan 2012)-End 

            'Anjan (20 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            btnGeneralSettings.Enabled = User._Object.Privilege._AllowChangeGeneralSettings
            'Anjan (20 Jan 2012)-End 

            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            btnReviewer.Visible = ConfigParameter._Object._IsCompanyNeedReviewer
            btnReviewerAssessment.Visible = ConfigParameter._Object._IsCompanyNeedReviewer
            btnReviewerBSCList.Visible = ConfigParameter._Object._IsCompanyNeedReviewer
            'S.SANDEEP [ 23 JAN 2012 ] -- END


            mnuCustomReporting.Enabled = User._Object.Privilege._AllowToAccessCustomReport


            'S.SANDEEP [ 21 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            btnAllocationMapping.Visible = ConfigParameter._Object._IsAllocation_Hierarchy_Set
            'S.SANDEEP [ 21 SEP 2012 ] -- END

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'btnLoanApprover_mapping.Enabled = User._Object.Privilege._AllowtoMapLoanApprover

            'Shani(26-Nov-2015) -- Start
            'ENHANCEMENT : Add Loan Importation Form
            btnImportLA.Enabled = User._Object.Privilege._AllowtoImportLoan_Advance
            'Shani(25-Nov-2015) -- End

            btnImportSaving.Enabled = User._Object.Privilege._AllowtoImportSavings
            btnImportAccrueLeave.Enabled = User._Object.Privilege._AllowtoImportAccrueLeave
            BtnTransferApprover.Enabled = User._Object.Privilege._AllowtoMigrateLeaveApprover
            btnImportAttendancedata.Enabled = User._Object.Privilege._AllowtoImportAttendanceData
            btnGroupAttendance.Enabled = User._Object.Privilege._AllowtoMakeGroupAttendance
            btnChangeUser.Enabled = User._Object.Privilege._AllowtoChangeUser
            btnChangeCompany.Enabled = User._Object.Privilege._AllowtoChangeCompany
            btnChangeDatabase.Enabled = User._Object.Privilege._AllowtoChangeDatabase
            btnChangePwd.Enabled = User._Object.Privilege._AllowtoChangePassword
            btnScanAttachments.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            btnLetterType.Enabled = User._Object.Privilege._AllowToAddTemplate
            mnuAuditTrails.Enabled = User._Object.Privilege._AllowtoViewAuditTrails
            btnApplicationEventLog.Enabled = User._Object.Privilege._AllowtoViewApplicationEvtLog
            btnUserAuthenticationLog.Enabled = User._Object.Privilege._AllowtoViewUserAuthLog
            btnUserAttempts.Enabled = User._Object.Privilege._AllowtoViewUserAttemptsLog
            btnLogView.Enabled = User._Object.Privilege._AllowtoViewAuditLogs
            'S.SANDEEP [ 29 OCT 2012 ] -- END

            'Anjan (26 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            btnAutoImportFromWeb.Enabled = User._Object.Privilege._AllowtoImportDataFromWeb
            'Anjan (26 Oct 2012)-End 


            'Anjan [ 20 Feb 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            btnPayApproverMap.Visible = ConfigParameter._Object._SetPayslipPaymentApproval
            btnPayAppLevel.Visible = ConfigParameter._Object._SetPayslipPaymentApproval
            'Anjan [ 20 Feb 2013 ] -- End
            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                btnEndELC.Visible = User._Object.Privilege._AllowToEndLeaveCycle
            Else
                btnEndELC.Visible = False
            End If


            'btnDependantException.Visible = User._Object.Privilege._AllowToAddMedicalDependantException

            'Pinkal (21-Jun-2012) -- Start
            'Enhancement : TRA Changes
            Dim objPassword As New clsPassowdOptions
            If objPassword._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                btnChangeUser.Enabled = False
                btnChangePwd.Enabled = False
            End If
            If objPassword._ProhibitPasswordChange = True AndAlso User._Object._Userunkid <> 1 Then
                btnChangePwd.Enabled = False
            End If
            'Pinkal (21-Jun-2012) -- End


            'Anjan [ 29 May 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew

            If objAppSetting._IsClient = 1 Then
                mnuRegister.Visible = False
            Else
                mnuRegister.Visible = True
            End If
            'Anjan [ 29 May 2013 ] -- End

            'S.SANDEEP [ 10 JUNE 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            btnPayPerActivity.Enabled = User._Object.Privilege._AllowtoAdd_EditPayPerActivity
            btnActivityRate.Enabled = User._Object.Privilege._AllowtoAdd_EditActivityRate
            'S.SANDEEP [ 10 JUNE 2013 ] -- END



            'Pinkal (20-Sep-2013) -- Start
            'Enhancement : TRA Changes
            btnPolicyList.Visible = ConfigParameter._Object._PolicyManagementTNA
            'btnRecalculateTiming.Visible = ConfigParameter._Object._PolicyManagementTNA
            'Pinkal (20-Sep-2013) -- End


            'Pinkal (09-Nov-2013) -- Start
            'Enhancement : Oman Changes
            btnRoundOff.Visible = ConfigParameter._Object._PolicyManagementTNA
            'Pinkal (09-Nov-2013) -- End


            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            btnStaffReqAppLevel.Visible = ConfigParameter._Object._ApplyStaffRequisition
            btnStaffReqAppMapping.Visible = ConfigParameter._Object._ApplyStaffRequisition
            btnStaffRequisition.Visible = ConfigParameter._Object._ApplyStaffRequisition
            'Sohail (28 May 2014) -- End
            'Sohail (31 Jan 2022) -- Start
            'Enhancement :  OLD-547 : NMB - Creation of staff requisition approver migration.
            btnTransferStaffRequisitionApprover.Visible = ConfigParameter._Object._ApplyStaffRequisition
            'Sohail (31 Jan 2022) -- End

            'S.SANDEEP [ 17 OCT 2014 ] -- START
            btnRemovePPALogs.Visible = User._Object.Privilege._AllowtoDeletePayPerActivityAuditTrails
            'S.SANDEEP [ 17 OCT 2014 ] -- END

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            btnOpenPeriodicReview.Visible = ConfigParameter._Object._AllowPeriodicReview
            If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                btnAssessCompanyGoals.Visible = False
                btnAllocationGoals.Visible = False
            Else
                btnAssessCompanyGoals.Visible = True
                btnAllocationGoals.Visible = True
            End If
            'S.SANDEEP [ 05 NOV 2014 ] -- END


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            btnExApprMigration.Enabled = User._Object.Privilege._AllowtoMigrateExpenseApprover
            btnClaimSwapApprover.Enabled = User._Object.Privilege._AllowtoSwapExpenseApprover
            'Pinkal (13-Jul-2015) -- End

            'Nilay (03-Feb-2016) -- Start
            'TO DO - Set visible true when Loan swap approver get ready
            btnLoanSwapApprover.Visible = True
            'Nilay (03-Feb-2016) -- End


            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT

            'Shani(19-APR-2016) -- Start
            'btnOpenPeriodicReview.Visible = User._Object.Privilege._AllowToOpenPeriodicReiew
            If ConfigParameter._Object._AllowPeriodicReview Then
                btnOpenPeriodicReview.Visible = User._Object.Privilege._AllowToOpenPeriodicReiew
            Else
                btnOpenPeriodicReview.Visible = False
            End If
            'Shani(19-APR-2016) -- End
            btnGlobalVoidAssessment.Visible = User._Object.Privilege._AllowToGlobalVoidAssessment
            'Shani(06-Feb-2016) -- End


            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If ConfigParameter._Object._SectorRouteAssignToEmp Then
            btnSecRouteAssignment.Visible = ConfigParameter._Object._SectorRouteAssignToEmp
            ElseIf ConfigParameter._Object._SectorRouteAssignToExpense Then
                btnSecRouteAssignment.Visible = ConfigParameter._Object._SectorRouteAssignToExpense
            Else
                btnSecRouteAssignment.Visible = False
            End If
            'Pinkal (20-Feb-2019) -- End


            'Pinkal (21-Dec-2016) -- Start
            'Enhancement - Adding Swap Approver Privilage for Leave Module.
            btnLVSwapApprover.Enabled = User._Object.Privilege._AllowToSwapLeaveApprover
            'Pinkal (21-Dec-2016) -- End


            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            btnTimesheetApproverMigration.Visible = User._Object.Privilege._AllowToMigrateBudgetTimesheetApprover
            'Pinkal (03-May-2017) -- End


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in payroll module.
            btnLoanApproverMigration.Enabled = User._Object.Privilege._AllowToTransferLoanApprover
            btnLoanSwapApprover.Enabled = User._Object.Privilege._AllowToSwapLoanApprover

            btnLeaveAdjustment.Enabled = User._Object.Privilege._AllowToPerformLeaveAdjustment
            btnRecalculateTiming.Enabled = User._Object.Privilege._AllowToReCalculateTimings
            btnEditTimeSheet.Enabled = User._Object.Privilege._AllowToEditTimesheetCard

            btnGlobalEditDelete.Enabled = User._Object.Privilege._AllowToAddEditDeleteGlobalTimesheet
            btnRoundOff.Enabled = User._Object.Privilege._AllowToRoundoffTimings

            'Varsha Rana (17-Oct-2017) -- End


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'btnEmpApproverLevel.Visible = Not ConfigParameter._Object._SkipEmployeeApprovalFlow
            'btnEmployeeApprover.Visible = Not ConfigParameter._Object._SkipEmployeeApprovalFlow


            If ConfigParameter._Object._SkipEmployeeApprovalFlow = False OrElse ConfigParameter._Object._oldSkipApprovalOnEmpData.Trim.Length > 0 Then
                btnEmpApproverLevel.Visible = True
                btnEmployeeApprover.Visible = True

            Else
                btnEmpApproverLevel.Visible = False
                btnEmployeeApprover.Visible = False

            End If


           
            'Gajanan [17-April-2019] -- End


            If ConfigParameter._Object._SkipEmployeeApprovalFlow = False Then
                btnApproveEmployee.Visible = User._Object.Privilege._AllowToApproveEmployee
            Else
                btnApproveEmployee.Visible = False
            End If
            'S.SANDEEP [20-JUN-2018] -- End



            'Gajanan (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            If ConfigParameter._Object._AssetDeclarationTemplate = enAsset_Declaration_Template.Template2 Then
                mnuAssetDeclaration.Visible = False
            End If
            'Gajanan(06 Oct 2018) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If ConfigParameter._Object._SkipApprovalOnEmpData.Trim.Length <= 0 Then
                frmAppRejEmpData.Visible = False
            End If
            'Gajanan [17-DEC-2018] -- End

            'Hemant (27 Oct 2022) -- Start
            If ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
                btnFlexcubeLoanApplication.Visible = True
            Else
                btnFlexcubeLoanApplication.Visible = False
            End If
            'Hemant (27 Oct 2022) -- End


            'Pinkal (31-Mar-2023) -- Start
            '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            If objGroup._Groupname.ToUpper() <> "NMB PLC" Then

                'Pinkal (15-May-2023) -- Start
                '(A1X-902) Candidate Feedback Screen - Expose applicant feedback screen to all customers.
                If objGroup._Groupname.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                btnCandidateFeedback.Visible = False
                End If
                'Pinkal (15-May-2023) -- End

                'Pinkal (28-Apr-2023) -- Start
                '(A1X-865) NMB - Read aptitude test results from provided views.
                btnDownloadAptitudeResult.Visible = False
                'Pinkal (28-Apr-2023) -- End
            End If
            objGroup = Nothing
            btnCandidateFeedback.Enabled = User._Object.Privilege._AllowToSendCandidateFeedback
            'Pinkal (31-Mar-2023) -- End

            'Pinkal (28-Apr-2023) -- Start
            '(A1X-865) NMB - Read aptitude test results from provided views.
            btnDownloadAptitudeResult.Enabled = User._Object.Privilege._AllowToDownloadAptitudeResult
            'Pinkal (28-Apr-2023) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "


    'Sandeep [ 07 APRIL 2011 ] -- Start
    Private Sub frmNewMDI_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            If mblnIsSendingPayslip = True Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sending payslip is in progress. Are you sure you want to exit?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                    e.Cancel = False
                Else
                    e.Cancel = True
                End If
            End If

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Emails in Application Backgropund.
            If gobjEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 77, "Sending Email(s) process is in progress. Please wait for some time."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Try
            End If
            'Sohail (10 Mar 2014) -- End

            If System.IO.Directory.Exists(mstrExportEPaySlipPath) Then
                Dim StrDir As String = mstrExportEPaySlipPath.Substring(0, mstrExportEPaySlipPath.IndexOf("\") + 1)
                Shell("CMD.exe /C RD /S /Q " & StrDir & "Progra~1\NPK\Aruti\Data\Report\ATemp")
            End If
            If blnIsIdealTimeSet = False Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Are you sure you want to exit?"), enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                    e.Cancel = False
                    'S.SANDEEP [ 21 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.DESKTOP, , True)
                    'S.SANDEEP [ 21 MAY 2012 ] -- END
                Else
                    e.Cancel = True
                    If mblnRestart = True Then mblnRestart = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNewMDI_FormClosing", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 07 APRIL 2011 ] -- End 


    Private Sub frmNewMDI_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (21-Jun-2011) -- Start
            If User._Object.PWDOptions._IsApplication_Idle Then
                CheckForIllegalCrossThreadCalls = False
                RemoveHandler clsApplicationIdleTimer.ApplicationIdle, AddressOf Application_Idle
                AddHandler clsApplicationIdleTimer.ApplicationIdle, AddressOf Application_Idle
            End If
            'Pinkal (21-Jun-2011) -- End

            mstrExportEPaySlipPath = objAppSetting._ApplicationPath & "Data\Report\ATemp"

            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Dim intDaysDiff As Integer = 0
            If User._Object._Exp_Days.ToString.Length = 8 Then
                Dim dtDate As Date = eZeeDate.convertDate(User._Object._Exp_Days.ToString).ToShortDateString
                intDaysDiff = DateDiff(DateInterval.Day, dtDate, ConfigParameter._Object._CurrentDateAndTime.AddDays(-1))
                If intDaysDiff.ToString <= "-2" Then
                    Dim strMessage As String = Language.getMessage(mstrModuleName, 3, "There are only [") & _
                    Math.Abs(intDaysDiff) & Language.getMessage(mstrModuleName, 4, "] Days Left for expiry of your password.") & vbCrLf & _
                    Language.getMessage(mstrModuleName, 5, "Please change it or increase the notification date, Or contact Administrator.")
                    eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                End If
            End If

            objbtnPropertyName.Text = Company._Object._Name
            objbtnWorkingDate.Text = Format(ConfigParameter._Object._CurrentDateAndTime, "dddd, MMM dd, yyyy")
            objbtnUserName.Text = User._Object._Username
            objbtnYearName.Text = FinancialYear._Object._FinancialYear_Name

            If CBool(FinancialYear._Object._IsFin_Close) = True Then
                Call mnuReportView_Click(sender, e)
                mnuPayrollView.Enabled = False
                mnuUtilitiesMain.Enabled = True

                btnGeneralSettings.Enabled = False
                btnReminder.Enabled = False
                btnLetterTemplate.Enabled = False
                btnLetterType.Enabled = False
                btnMailbox.Enabled = False
                btnCommonExportData.Enabled = False
                btnCommonImportData.Enabled = False
            Else
                mnuPayrollView.Enabled = True
                'Call mnuCommonMaster_Click(sender, e)
                Call mnuCoreMaster_Click(sender, e)

                'S.SANDEEP [ 19 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If objAppSetting._LastViewAccessed = 0 Then
                '    Call mnuPayrollView_Click(sender, e)
                'Else
                '    Call mnuReportView_Click(sender, e)
                'End If

                If objAppSetting._LastViewAccessed = 0 Then
                    Call mnuPayrollView_Click(sender, e)
                ElseIf objAppSetting._LastViewAccessed = 1 Then
                    Call mnuReportView_Click(sender, e)
                ElseIf objAppSetting._LastViewAccessed = 2 Then
                    If User._Object.Privilege._AllowToAccessCustomReport = True Then
                        Call mnuCustomReporting_Click(sender, e)
                    Else
                        Call mnuPayrollView_Click(sender, e)
                    End If
                End If
                'S.SANDEEP [ 19 JUNE 2012 ] -- END


            End If
            'Sandeep ( 18 JAN 2011 ) -- END 
            ''Sohail (15 Feb 2011) -- Start
            'If ConfigParameter._Object._AccountingSoftWare > 0 Then
            '    objelLine8.Visible = False
            '    btnAccount.Visible = False
            '    btnAccountConfiguration.Visible = False
            '    btnEmpAccConfig.Visible = False
            '    btnCCAccConfig.Visible = False
            'Else
            '    objelLine8.Visible = True
            '    btnAccount.Visible = True
            '    btnAccountConfiguration.Visible = True
            '    btnEmpAccConfig.Visible = True
            '    btnCCAccConfig.Visible = True
            'End If
            'Sohail (15 Feb 2011) -- End

            Call SetVisibility()

            'Anjan (02 Mar 2011)-Start
            'Issue: Now both database backup and restore will be taken from configuration. So Need to give from here.
            btnBackup.Visible = False
            btnRestore.Visible = False
            'Anjan (02 Mar 2011)-End


            'Sohail (08 May 2013) -- Start
            'TRA - ENHANCEMENT
            'If ArtLic._Object.IsDemo = False Then
            '    Call CheckModLic()
            'End If
            If ConfigParameter._Object._IsArutiDemo = False Then 'IsLicenced
                Call CheckModLic()
            End If
            'Sohail (08 May 2013) -- End
            'Sohail (20 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._ApplyPayPerActivity = False Then
                mnuPayActivity.Visible = False
            End If
            'Sohail (20 Jul 2013) -- End


            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            dtpEmployeeAsOnDate.MinDate = CDate("01-Jan-1900")
            dtpEmployeeAsOnDate.MaxDate = CDate("01-Jan-9998")
            dtpEmployeeAsOnDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            If dtpEmployeeAsOnDate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
                dtpEmployeeAsOnDate.Value = FinancialYear._Object._Database_End_Date.Date
            ElseIf dtpEmployeeAsOnDate.Value.Date < FinancialYear._Object._Database_Start_Date.Date Then
                dtpEmployeeAsOnDate.Value = FinancialYear._Object._Database_Start_Date.Date
            End If
            dtpEmployeeAsOnDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpEmployeeAsOnDate.MaxDate = FinancialYear._Object._Database_End_Date

            ConfigParameter._Object._EmployeeAsOnDate = eZeeDate.convertDate(dtpEmployeeAsOnDate.Value.Date)
            ConfigParameter._Object.updateParam()
            ConfigParameter._Object.Refresh()
            'Sohail (06 Jan 2012) -- End

            'S.SANDEEP [ 06 June 2011 ] -- START
            'ISSUE : DASHBOARD
            'Me.SuspendLayout()
            'S.SANDEEP [ 06 June 2011 ] -- END 


            'Pinkal (18-Jul-2012) -- Start
            'Enhancement : TRA Changes
            eZee.Common.Ui.Language.eZeeSearchResetButton.SearchMessage = "[ " & Language.getMessage(mstrModuleName, 12, "Searching...") & " ]"
            eZee.Common.Ui.Language.eZeeSearchResetButton.ResultMessage = "[ # " & Language.getMessage(mstrModuleName, 13, "Record(s) Found.") & " ]"
            'Pinkal (18-Jul-2012) -- End


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            dtpEmployeeAsOnDate.Format = DateTimePickerFormat.Custom
            dtpEmployeeAsOnDate.CustomFormat = ConfigParameter._Object._CompanyDateFormat
            'Pinkal (16-Apr-2016) -- End

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            btnGroupBenefit.Visible = False
            'Sohail (17 Sep 2019) -- End


            If ConfigParameter._Object._IsArutiDemo = False Then
                If clsMasterData.GetAMS_Date().Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    mnuTroubleshoot.Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNewMDI_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMDI_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Try
            If Me.WindowState = FormWindowState.Minimized Then
                Me.Text = "Aruti"
            Else
                Me.Text = "Aruti" & " " & _
                                              Language.getMessage(mstrModuleName, 6, "Version") & ": " & System.String.Format("{0}.{1:0}.{2:0}.{3:000}", _
                                              My.Application.Info.Version.Major, _
                                              My.Application.Info.Version.Minor, _
                                              My.Application.Info.Version.Build, _
                                              My.Application.Info.Version.Revision)

            End If

            If pnlMenuItems.Controls.Count > 0 Then
                Call SetDyanamicSize(pnlMenuItems.Controls(0))
            End If


            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            If pnlSummary.Controls.Count > 0 Then
                If TypeOf pnlSummary.Controls(0) Is Form Then
                    CType(pnlSummary.Controls(0), Form).TopLevel = False
                    pnlSummary.Controls(0).Dock = DockStyle.Fill
                End If
            End If
            'S.SANDEEP [ 29 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMDI_Resize", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 06 June 2011 ] -- START
    'ISSUE : DASHBOARD
    Private Sub frmNewMDI_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            Me.ResumeLayout()
            'Call SetDashBoard()
            tmrReminder.Enabled = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmNewMDI_Shown ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 06 June 2011 ] -- END 

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'S.SANDEEP [23 MAR 2016] -- START
            clsMasterData.SetMessages()
            clsDashboard_Class.SetMessages()
            ArutiReports.clsAssetDeclarationT2.SetMessages()

            'Gajanan (28 Nov 2018) -- Start
            clsAssetdeclaration_masterT2.SetMessages()
            clsAssetDeclarationCategoryWiseT2.SetMessages()
            'Gajanan (28 Nov 2018)  -- End

            clsAssetDeclarationT2.SetMessages()
            'Sohail (22 Nov 2018) -- Start
            'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
            clsgrievanceapprover_master.SetMessages()
            clsgrievanceapprover_Tran.SetMessages()
            clsGrievanceApproverLevel.SetMessages()
            clsGrievance_Master.SetMessages()
            clsgreinitiater_response_tran.SetMessages()
            clsResolution_Step_Tran.SetMessages()
            clsAssetdeclaration_masterT2.SetMessages()
            'Hemant (26 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT of Asset Declaration Template 2
            clsAsset_Declaration_Status_ReportT2.SetMessages()
            'Hemant (26 Nov 2018) -- End
            'Sohail (19 Feb 2020) -- Start
            'NMB Enhancement # : Once user clicks on acknowledge button, system to trigger email to employee and to specified users mapped on configuration.
            clsEmpnondisclosure_declaration_tran.SetMessages()
            clsNonDisclosureDeclareStatusRerport.SetMessages()
            'Sohail (19 Feb 2020) -- End


            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report
            clsGrievanceReport.SetMessages()
            'Gajanan [5-July-2019] -- End

            'Pinkal (27-Jun-2019) -- Start
            'Enhancement - IMPLEMENTING OT MODULE.
            clsTna_approverlevel_master.SetMessages()
            clsTnaapprover_master.SetMessages()
            clsOT_Requisition_Tran.SetMessages()
            clsTnaotrequisition_approval_Tran.SetMessages()
            'Pinkal (27-Jun-2019) -- End

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            clscalibrate_approverlevel_master.SetMessages()
            clscalibrate_approver_master.SetMessages()
            clsScoreCalibrationApproval.SetMessages()
            'S.SANDEEP |27-JUL-2019| -- END

            'Hemant (15 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            clsPotentialTalent_Tran.SetMessages()
            clstlcycle_master.SetMessages()
            clstlpipeline_master.SetMessages()
            clstlquestionnaire_master.SetMessages()
            clstlscreener_master.SetMessages()
            clstlsettings_master.SetMessages()
            clstlstages_master.SetMessages()

            clsPotentialSuccession_tran.SetMessages()
            clssucpipeline_master.SetMessages()
            clssucquestionnaire_master.SetMessages()
            clssucscreener_master.SetMessages()
            clssucsettings_master.SetMessages()
            clssucstages_master.SetMessages()
            'Hemant (15 Jan 2021) -- End
            'Sohail (22 Nov 2018) -- End
            'Sohail (26 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            clsDepartmentaltrainingneed_master.SetMessages()
            'Sohail (26 Apr 2021) -- End

            'S.SANDEEP |19-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES REPORTS
            clsStaffCompetenciesReport.SetMessages()
            clsCompetencyScoreAnalysisReport.SetMessages()
            clsFamilyCompetenciesReport.SetMessages()
            clsStaffCompetenciesReport.SetMessages()
            clsCompetenceFormReport.SetMessages()
            'S.SANDEEP |19-MAY-2021| -- END


            'Pinkal (01-Jun-2021)-- Start
            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            clsclaim_retirement_master.SetMessages()
            clsclaim_retirement_approval_Tran.SetMessages()
            clsretire_process_tran.SetMessages()
            'Pinkal (01-Jun-2021) -- End

            'Hemant (13 Aug 2021) -- Start
            clstraining_request_master.SetMessages()
            'Hemant (13 Aug 2021) -- End
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            clstraining_evaluation_tran.SetMessages()
            'Hemant (20 Aug 2021) -- End
            'Hemant (22 Dec 2022) -- Start            
            clspdp_categoryitem_mapping.SetMessages()
            clspdpaction_plan_category.SetMessages()
            clspdpcategory_master.SetMessages()
            clspdpform_master.SetMessages()
            clsPdpgoals_master.SetMessages()
            clsPdpgoals_trainingneed_Tran.SetMessages()
            clspdpitem_master.SetMessages()
            clspdpreviewer_master.SetMessages()
            clspdpreviewerlevel_master.SetMessages()
            clspdpsettings_master.SetMessages()
            'Hemant (22 Dec 2022) -- End

            'Pinkal (10-Jul-2023) -- Start
            'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
            clsstapproverlevel_master.SetMessages()
            clsstapproverlevel_role_mapping.SetMessages()
            clstransfer_request_tran.SetMessages()
            clsStaffTransfer_Report.SetMessages()
            clsStaffTransferRequest_Report.SetMessages()
            'Pinkal (10-Jul-2023) -- End

            'Hemant (15 Sep 2023) -- Start
            'ENHANCEMENT(TRA): A1X-1314 - New report: Coaches Coachees nomination form
            clsCoaching_Approver_Level_master.SetMessages()
            clscoaching_role_mapping.SetMessages()
            clsCoaching_Nomination_Form.SetMessages()
            clscoachingapproval_process_tran.SetMessages()
            clsCoachingNominationFormReport.SetMessages()
            'Hemant (15 Sep 2023) -- End


            objfrm._Other_ModuleNames = "clsMasterData,clsDashboard_Class,clsAssetDeclarationT2,clsgrievanceapprover_master,clsgrievanceapprover_Tran " & _
                                                          ",clsGrievanceApproverLevel,clsGrievance_Master,clsgreinitiater_response_tran,clsresolution_step_tran,clsAssetdeclaration_masterT2 " & _
                                                          ",clsAsset_Declaration_Status_ReportT2,clsAssetDeclarationCategoryWiseT2,clsGrievanceReport,clsTna_approverlevel_master,clsTnaapprover_master " & _
                                                          ",clsOT_Requisition_Tran,clsTnaotrequisition_approval_Tran,clsScoreCalibrationApproval,clscalibrate_approverlevel_master,clscalibrate_approver_master " & _
                                                          ",clsclaim_retirement_master,clsclaim_retirement_Tran,clsclaim_retirement_approval_Tran,clsEmployeeDataApproval,clsassignemp_ot,clsEmpnondisclosure_declaration_tran,clsEmpnondisclosuredec_lockunlock,clsNonDisclosureDeclareStatusRerport,clsEmployeeNonDisclosureDeclarationReport" & _
                                                          ",clsPotentialTalent_Tran,clstlcycle_master,clstlpipeline_master,clstlquestionnaire_master,clstlscreener_master,clstlsettings_master,clstlstages_master " & _
                                                          ",clsPotentialSuccession_tran,clssucpipeline_master,clssucquestionnaire_master,clssucscreener_master,clssucsettings_master,clssucstages_master " & _
                                                          ",clsclaim_retirement_master,clsclaim_retirement_Tran,clsclaim_retirement_approval_Tran,clsretire_process_tran,clsDepartmentaltrainingneed_master,clsStaffCompetenciesReport,clsCompetencyScoreAnalysisReport,clsFamilyCompetenciesReport,clsStaffCompetenciesReport,clsCompetenceFormReport" & _
                                        ",clstraining_request_master,clstraining_evaluation_tran,clspdp_categoryitem_mapping,clspdpaction_plan_category,clspdpcategory_master" & _
                                                          ",clspdpformmaster,clsPdpgoals_master,clsPdpgoals_trainingneed_Tran,clspdpitem_master,clspdpreviewer_master,clspdpreviewerlevel_master,clspdpsettings_master" & _
                                        ",clsStapproverlevel_master,clsstapproverlevel_role_mapping,clstransfer_request_tran,clssttransfer_approval_Tran,clsStaffTransfer_Report,clsStaffTransferRequest_Report" & _
                                        ",clsCoaching_Approver_Level_master,clscoaching_role_mapping,clsCoaching_Nomination_Form,clscoachingapproval_process_tran,clsCoachingNominationFormReport"
            'Hemant (15 Sep 2023) -- [clsCoaching_Approver_Level_master,clscoaching_role_mapping,clscoachingapproval_process_tran,clsCoachingNominationFormReport]
            'Pinkal (10-Jul-2023) --NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers[clsStapproverlevel_master,clsstapproverlevel_role_mapping,clstransfer_request_tran,clssttransfer_approval_Tran,clsStaffTransfer_Report,clsStaffTransferRequest_Report]
            'Hemant (22 Dec 2022) -- [clspdp_categoryitem_mapping,clspdpaction_plan_category,clspdpcategory_master,clspdpform_master,clsPdpgoals_master,clsPdpgoals_trainingneed_Tran,clspdpitem_master,clspdpreviewer_master,clspdpreviewerlevel_master,clspdpsettings_master]
            'Hemant (20 Aug 2021) -- [clstraining_evaluation_tran]
            'Hemant (13 Aug 2021) -- [clstraining_request_master]
            'S.SANDEEP |19-MAY-2021| -- START {clsStaffCompetenciesReport,clsCompetencyScoreAnalysisReport,clsFamilyCompetenciesReport,clsStaffCompetenciesReport,clsCompetenceFormReport} -- END
            'Sohail (26 Apr 2021) - [clsDepartmentaltrainingneed_master]
            'Pinkal (10-Mar-2021) -- Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.[",clsclaim_retirement_master,clsclaim_retirement_Tran,clsclaim_retirement_approval_Tran,clsretire_process_tran"]

            'Hemant (15 Jan 2021) -- [clsPotentialTalent_Tran,clstlcycle_master,clstlpipeline_master,clstlquestionnaire_master,clstlscreener_master,clstlsettings_master,clstlstages_master,clsPotentialSuccession_tran,clssucpipeline_master,clssucquestionnaire_master,clssucscreener_master,clssucsettings_master,clssucstages_master]
            'Sohail (04 Feb 2020) - [clsEmpnondisclosure_declaration_tran,clsEmpnondisclosuredec_lockunlock,clsNonDisclosureDeclareStatusRerport]
            'Pinkal (24-Oct-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[clsassignemp_ot]

            'Pinkal (11-Sep-2019) --Enhancement NMB - Working On Claim Retirement for NMB.[clsclaim_retirement_master,clsclaim_retirement_Tran,clsclaim_retirement_approval_Tran]

            'Pinkal (27-Jun-2019) -- 'Enhancement - IMPLEMENTING OT MODULE.[clsTna_approverlevel_master,clsTnaapprover_master,clsOT_Requisition_Tran,clsTnaotrequisition_approval_Tran]
            'Hemant (26 Nov 2018) -- [clsAsset_Declaration_Status_ReportT2]
            'Sohail (22 Nov 2018) - [clsgrievanceapprover_master,clsgrievanceapprover_Tran,clsGrievanceApproverLevel,clsGrievance_Master,clsgreinitiater_response_tran,clsresolution_step_tran]
            'Sohail (22 Nov 2018) -- Start
            'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
            'S.SANDEEP |27-JUL-2019| -- START {clsScoreCalibrationApproval,clscalibrate_approverlevel_master,clscalibrate_approver_master} -- END

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'objfrm._Other_FormNames = "frmEmpAssetDeclarationListT2, frmEmpAssetDeclarationT2, frmAgreeDisagreeResponse, frmGrievanceEmployeeList, frmGrievanceEmployeeAddEdit, frmGrievanceApproverLevelList, frmGrievanceApproverLevelAddEdit, frmGrievanceApproverList, frmGrievanceApproverAddedit, frmGrievanceInitiatorResponseList, frmResolutionStep, frmResolutionStepList, frmEmployeeAssetDeclarationStatusReportT2,frmEmployeeDeclarationCategorywiseReportT2,frmEmpAssetDeclarationListT2,frmEmpAssetDeclarationT2,frmEmployeeAssetDeclarationReportT2"


            objfrm._Other_FormNames = "frmEmpAssetDeclarationListT2, frmEmpAssetDeclarationT2, frmAgreeDisagreeResponse, frmGrievanceEmployeeList, frmGrievanceEmployeeAddEdit " & _
                                                       ", frmGrievanceApproverLevelList, frmGrievanceApproverLevelAddEdit, frmGrievanceApproverList, frmGrievanceApproverAddedit, frmGrievanceInitiatorResponseList " & _
                                                       ", frmResolutionStep, frmResolutionStepList, frmEmployeeAssetDeclarationStatusReportT2,frmEmployeeDeclarationCategorywiseReportT2,frmEmpAssetDeclarationListT2 " & _
                                                       ",frmEmpAssetDeclarationT2,frmEmployeeAssetDeclarationReportT2,frmLeaveDetails,frmGrievanceResolutionStatus,frmOTRequisitionApproverLevelList " & _
                                                       ",frmOTRequisitionApproverLevelAddEdit,frmOTRequisitionApproverList,frmOTRequisitionApproverAddedit,frmEmployeeOTRequisitionList,frmEmployeeOTRequisitionAddEdit " & _
                                                       ",frmOtRequisitionApprovalList,frmOtRequisitionApproval,frmExemptEmployeeList,frmExemptEmployee,frmClaimRetirementList,frmClaimRetirementInformation,frmClaimRetirementApprovalList " & _
                                                       ", frmClaimRetirementApproval,frmCalibrationApproverList,frmCalibrationApproverAddedit,frmCalibrationApproverLevelList,frmCalibrationApproverLevelAddEdit,frmApproveRejectCalibrationList " & _
                                                       ",frmApproveRejectCalibrationAddEdit,frmScoreCalibrationList,frmScoreCalibrationAddEdit,frmViewRating,frmOTCancelRequisition,frmOTEmployeeAssignmentList,frmOTEmployeeAssignment " & _
                                                       ", frmTrainingNeedFormList, frmTrainingNeedForm, frmEmpNonDisclosure_Declaration, frmEmpUnlockNonDisclosureDeclaration, frmNonDisclosureDeclareStatusRerport " & _
                                                       ",frmOTRequisitionApproverMigration,frmOTSubmitForApproval,frmEmployeeNonDisclosureDeclarationReport,frmTalentPipeline,frmPotentialTalent,frmTalentScreening " & _
                                                       ",frmTalentApproveReject,frmTalentPool,frmTalentSettingsAddEdit,frmTalentCycleAddEdit,frmTalentStagingAddEdit,frmTalentQualifyingCriteriaSetup,frmTalentScreenersSetup,frmTalentQuestionnaireAddEdit,frmTalentRatingsAddEdit " & _
                                                       ",frmTalentProfile,frmSuccessionScreening,frmSuccessionApproveReject,frmSuccessionPipeline,frmPotentialSuccession,frmSuccessionPool,frmSuccessionProfile,frmSuccessionSettingsAddEdit,frmSuccessionStagingAddEdit " & _
                                                       ",frmSuccessionQualifyingCriteriaSetup,frmSuccessionScreenersSetup,frmSuccessionQuestionnaireAddEdit,frmSuccessionRatingsAddEdit,frmEmployeeNominationMaster,frmClaimRetirementList,frmClaimRetirementInformation " & _
                                                       ",frmClaimRetirementApprovalList,frmClaimRetirementApproval,frmRetirementPosting, frmDepartmentalTrainingNeedsList, frmDepartmentalTrainingNeeds, frmSetMaxSummaryBudget,frmStaffCompetenciesReport,frmFamilyCompetenciesReport,frmCompetenceAssessmentForm,frmCompetenciesTrainingReport,frmCompetenciesScoreAnalysisReport " & _
                                                       ",frmTrainingApproverMapping,frmTrainingScheduleView,frmTrainingScheduleViewList,frmTrainingRequestFormList,frmTrainingRequestForm,frmGroupTrainingRequest,frmTrainingRequestApprovalList,frmTrainingCompletionApprovalList,frmDailyAttendanceOperation" & _
                                                       ",frmTrainingSettings,frmTrainingEvalutionCategoryAddEdit,frmTrainingEvalutionQuestionnaireAddEdit,frmTrainingEvalutionSubQuestionAddEdit,frmTrainingEvalutionForm,frmSkipTrainingRequisitionAndApproval,frmCloseTrainingCalendar" & _
                                                       ",frmPDPSettings,frmPDPReviewerLevelAddEdit,frmPDPEvaluatorsAddEdit,frmPDPReviewerAddEdit,frmPDPActionPlanCategoriesAddEdit,frmPDPCategoriesAddEdit,frmPDPItemsAddEdit,frmPDPCategoryItemMapping,frmPDPCategoryInstruction" & _
                                                       ",frmStaffTransferApproverLevelList,frmStaffTransferApproverLevelAddEdit,frmStaffTransferLevelRoleMapping,frmStaffTransferRequest,frmStaffTransferRequestList,frmStaffTransferApproval,frmStaffTransferApprovalList,frmStaffTransferReport,frmStaffTransferRequestReport " & _
                                                       ",frmCoachingApproverLevelList,frmCoachingApproverLevelAddEdit,frmCoachingRoleMappingList,frmCoachingRoleMappingAddEdit,frmCoachingNominationFormList,frmCoachingNominationForm,frmCoachingNominationApprovalList"
            'Hemant (15 Sep 2023) -- [frmCoachingApproverLevelAddEdit,frmCoachingApproverLevelAddEdit,frmCoachingRoleMappingList,frmCoachingRoleMappingAddEdit,frmCoachingNominationFormList,frmCoachingNominationForm,frmCoachingNominationApprovalList]
            'Pinkal (10-Jul-2023) --NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers[,frmStaffTransferApproverLevelList,frmStaffTransferApproverLevelAddEdit,frmStaffTransferLevelRoleMapping,frmStaffTransferRequest,frmStaffTransferRequestList,frmStaffTransferApproval,frmStaffTransferApprovalList,frmStaffTransferReport,frmStaffTransferRequestReport ]
            'Hemant (22 Dec 2022) -- [frmPDPSettings,frmPDPReviewerLevelAddEdit,frmPDPEvaluatorsAddEdit,frmPDPReviewerAddEdit,frmPDPActionPlanCategoriesAddEdit,frmPDPCategoriesAddEdit,frmPDPItemsAddEdit,frmPDPCategoryItemMapping,frmPDPCategoryInstruction]
            'Hemant (03 Dec 2021) -- [frmCloseTrainingCalendar]
            'Hemant (23 Sep 2021) -- [frmSkipTrainingRequisitionAndApproval]
'Pinkal (09-Aug-2021)-- NMB New UI Enhancements.[frmDailyAttendanceOperation]
            'Hemant (13 Aug 2021) -- [frmTrainingSettings,frmTrainingEvalutionCategoryAddEdit,frmTrainingEvalutionQuestionnaireAddEdit,frmTrainingEvalutionSubQuestionAddEdit,frmTrainingEvalutionForm]
            'Hemant (07 Jun 2021) -- [frmTrainingApproverMapping,frmTrainingScheduleView,frmTrainingScheduleViewList,frmTrainingRequestFormList,frmTrainingRequestForm,frmGroupTrainingRequest,frmTrainingRequestApprovalList,frmTrainingCompletionApprovalList]
            'S.SANDEEP |19-MAY-2021| -- START {frmStaffCompetenciesReport,frmFamilyCompetenciesReport,frmCompetenceAssessmentForm,frmCompetenciesTrainingReport,frmCompetenciesScoreAnalysisReport} -- END
            'Sohail (26 Apr 2021) - [frmDepartmentalTrainingNeedsList, frmDepartmentalTrainingNeeds, frmSetMaxSummaryBudget]

            'Pinkal (10-Mar-2021) -- Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.[frmClaimRetirementList,frmClaimRetirementInformation,frmClaimRetirementApprovalList,frmClaimRetirementApproval,frmRetirementPosting]

            'Hemant (15 Jan 2021) -- [frmPotentialTalent,frmTalentScreening,frmTalentApproveReject,frmTalentPool,frmTalentSettingsAddEdit,frmTalentCycleAddEdit,frmTalentStagingAddEdit,frmTalentQualifyingCriteriaSetup,frmTalentScreenersSetup,frmTalentQuestionnaireAddEdit,frmTalentRatingsAddEdit,frmTalentProfile,frmSuccessionScreening,frmSuccessionApproveReject,frmSuccessionPipeline,frmPotentialSuccession,frmSuccessionPool,frmSuccessionProfile,frmSuccessionSettingsAddEdit,frmSuccessionStagingAddEdit,frmSuccessionQualifyingCriteriaSetup,frmSuccessionScreenersSetup,frmSuccessionQuestionnaireAddEdit,frmSuccessionRatingsAddEdit]
            'Pinkal (09-Mar-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[frmOTSubmitForApproval]

            'Pinkal (03-Mar-2020) -- ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.[frmOTApproverMigration]

            'Sohail (04 Feb 2020) - [frmEmpNonDisclosure_Declaration, frmEmpUnlockNonDisclosureDeclaration, frmNonDisclosureDeclareStatusRerport]
            'Sohail (14 Nov 2019) -- [frmTrainingNeedFormList, frmTrainingNeedForm]
            'Pinkal (24-Oct-2019) -- Enhancement NMB - Working On OT Enhancement for NMB.[frmOTCancelRequisition,frmOTEmployeeAssignmentList,frmOTEmployeeAssignment]

            'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[frmClaimRetirementList,frmClaimRetirementInformation,frmClaimRetirementApprovalList , frmClaimRetirementApproval]

            'Pinkal (13-Aug-2019) -- 'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.[frmExemptEmployeeList,frmExemptEmployee]
                       'Pinkal (27-Jun-2019) -- 'Enhancement - IMPLEMENTING OT MODULE.[frmOTRequisitionApproverLevelList,frmOTRequisitionApproverLevelAddEdit,frmOTRequisitionApproverList,frmOTRequisitionApproverAddedit,frmEmployeeOTRequisitionList,frmEmployeeOTRequisitionAddEdit,frmOtRequisitionApprovalList,frmOtRequisitionApproval]
            'Pinkal (26-Feb-2019) -- End


            'Hemant (26 Nov 2018) -- [frmEmployeeAssetDeclarationStatusReportT2]
            'Sohail (22 Nov 2018) -- End
            'S.SANDEEP [23 MAR 2016] -- END

            'S.SANDEEP |27-JUL-2019| -- START {frmCalibrationApproverList,frmCalibrationApproverAddedit,frmCalibrationApproverLevelList,frmCalibrationApproverLevelAddEdit,frmApproveRejectCalibrationList,frmApproveRejectCalibrationAddEdit,frmScoreCalibrationList,frmScoreCalibrationAddEdit,frmViewRating} -- END


            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 




#End Region

#Region " Menu Items "

#Region " General Master "

    Private Sub mnuCommonMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCommonSetups.Click
        Try
            Call SetControlVisibility(fpnlCommomMasters)
            objlblCaption.Text = mnuCommonSetups.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuCommonSetups.OwnerItem.Name
            'StrModuleName2 = mnuCommonSetups.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCommonMaster_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuCoreMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCoreSetups.Click
        Try
            Call SetControlVisibility(fpnlCoreSetups)
            objlblCaption.Text = mnuCoreSetups.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuCoreSetups.OwnerItem.Name
            'StrModuleName2 = mnuCoreSetups.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCoreMaster_Click", mstrModuleName)
        End Try
    End Sub

    'Anjan (16 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS

    Private Sub mnuWagesSetup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuWagesSetup.Click
        Try
            Call SetControlVisibility(fnlWagesSetups)
            objlblCaption.Text = mnuWagesSetup.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuWagesSetup.OwnerItem.Name
            'StrModuleName2 = mnuWagesSetup.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuWagesSetup_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (16 Jan 2012)-End 



#End Region

#Region " Recruitement "

    Private Sub mnuRecruitment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRecruitment.Click, mnuRecruitmentItem.Click
        Try
            Call SetControlVisibility(fpnlRecruitment)
            objlblCaption.Text = mnuRecruitment.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuRecruitmentItem.OwnerItem.Name
            'StrModuleName2 = mnuRecruitmentItem.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRecruitment_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Personnel "

    Private Sub mnuHRMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuHRMaster.Click
        Try
            Call SetControlVisibility(fpnlHRMaster)
            objlblCaption.Text = mnuHRMaster.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuPersonnel.OwnerItem.Name
            'StrModuleName2 = mnuHRMaster.OwnerItem.Name
            'StrModuleName3 = mnuHRMaster.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuHRMaster_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployeeData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployeeData.Click
        Try
            Call SetControlVisibility(fpnlEmpData)
            objlblCaption.Text = mnuEmployeeData.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuPersonnel.OwnerItem.Name
            'StrModuleName2 = mnuEmployeeData.OwnerItem.Name
            'StrModuleName3 = mnuEmployeeData.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEmployeeData_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuDiscipline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDiscipline.Click
        Try
            Call SetControlVisibility(fpnlDiscipline)
            objlblCaption.Text = mnuDiscipline.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuDiscipline.OwnerItem.Name
            'StrModuleName2 = mnuDiscipline.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDiscipline_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    'Private Sub mnuEmployeeAssessment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssessment.Click
    '    Try
    '        Call SetControlVisibility(fpnlAssessment)
    '        objlblCaption.Text = mnuAssessment.Text
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuEmployeeAssessment_Click", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub mnuGeneralAssessment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGeneralAssessment.Click
        Try
            Call SetControlVisibility(fpnlAssessment)
            objlblCaption.Text = mnuAssessment.Text

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuAssessment.OwnerItem.Name
            'StrModuleName2 = mnuGeneralAssessment.OwnerItem.Name
            'StrModuleName3 = mnuGeneralAssessment.Name

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGeneralAssessment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuBalancedScoreCard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBalancedScoreCard.Click
        Try
            Call SetControlVisibility(fpnlBSC)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuAssessment.OwnerItem.Name
            'StrModuleName2 = mnuBalancedScoreCard.OwnerItem.Name
            'StrModuleName3 = mnuBalancedScoreCard.Name

            objlblCaption.Text = mnuBalancedScoreCard.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBalancedScoreCard_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Private Sub mnuPerformaceEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPerformaceEvaluation.Click
        Try
            Call SetControlVisibility(fpnlPerformanceEval)
            objlblCaption.Text = mnuPerformaceEvaluation.Text
            'Call Blank_ModuleName()
            'StrModuleName1 = mnuAssessment.OwnerItem.Name
            'StrModuleName2 = mnuPerformaceEvaluation.OwnerItem.Name
            'StrModuleName3 = mnuPerformaceEvaluation.Name
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPerformaceEvaluation_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 NOV 2014 ] -- END

    Private Sub mnuSetups_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetups.Click
        Try
            Call SetControlVisibility(fpnlAssessmentSetups)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuAssessment.OwnerItem.Name
            'StrModuleName2 = mnuSetups.OwnerItem.Name
            'StrModuleName3 = mnuSetups.Name

            objlblCaption.Text = mnuSetups.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBalancedScoreCard_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'S.SANDEEP [ 04 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuAppraisal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAppraisal.Click
        Call SetControlVisibility(fpnlAppraisal)

        'Call Blank_ModuleName()
        'StrModuleName1 = mnuAssessment.OwnerItem.Name
        'StrModuleName2 = mnuAppraisal.OwnerItem.Name
        'StrModuleName3 = mnuAppraisal.Name

        objlblCaption.Text = mnuAppraisal.Text
    End Sub
    'S.SANDEEP [ 04 FEB 2012 ] -- END



    'S.SANDEEP [ 04 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub mnuTrainingInformation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTrainingInformation.Click
    '    Try
    '        Call SetControlVisibility(fpnlTrainingInfo)
    '        objlblCaption.Text = mnuTrainingInformation.Text
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuTrainingInformation_Click", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub mnuTrainingNeedAnalysis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTrainingEvaluation.Click
        Try
            Call SetControlVisibility(fpnlTrainingFeedback)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuTrainingInformation.OwnerItem.Name
            'StrModuleName2 = mnuTrainingEvaluation.OwnerItem.Name
            'StrModuleName3 = mnuTrainingEvaluation.Name

            objlblCaption.Text = mnuTrainingEvaluation.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuTrainingNeedAnalysis_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuJobTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuJobTraining.Click
        Try
            Call SetControlVisibility(fpnlTrainingInfo)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuTrainingInformation.OwnerItem.Name
            'StrModuleName2 = mnuJobTraining.OwnerItem.Name
            'StrModuleName3 = mnuJobTraining.Name

            objlblCaption.Text = mnuJobTraining.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuJobTraining_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 04 FEB 2012 ] -- END


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuAssetDeclaration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssetDeclaration.Click
        Dim objFrm As New frmEmpAssetDeclarationList

        'Call Blank_ModuleName()
        'StrModuleName1 = mnuAssetDeclaration.OwnerItem.Name
        'StrModuleName2 = mnuAssetDeclaration.Name

        If objFrm IsNot Nothing Then
            objFrm.ShowDialog()
        End If
    End Sub
    'S.SANDEEP [ 19 JULY 2012 ] -- END


#End Region

#Region " Medical "

    Private Sub mnuMedical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMedical.Click
        Try
            Call SetControlVisibility(fpnlMedicalInfo)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuMedical.OwnerItem.Name
            'StrModuleName2 = mnuMedical.Name

            objlblCaption.Text = mnuMedical.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMedical_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Payroll "

    Private Sub mnuPayrollMasters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayrollMasters.Click
        Try
            Call SetControlVisibility(fpnlPayrollMasters)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuPayroll.Name

            objlblCaption.Text = mnuPayrollMasters.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayrollMasters_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPayrollTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayrollTransactions.Click
        Try
            Call SetControlVisibility(fpnlPayrollTransaction)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuPayroll.Name

            objlblCaption.Text = mnuPayrollTransactions.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayrollTransactions_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuLoan_Advance_Savings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLoan_Advance_Savings.Click
        Try
            Call SetControlVisibility(fpnlLoanInfo)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuPayroll.Name
            'StrModuleName2 = mnuLoan_Advance_Savings.Name

            objlblCaption.Text = mnuLoan_Advance_Savings.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuLoan_Advance_Savings_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 10 JUNE 2013 ] -- START
    'ENHANCEMENT : OTHER CHANGES
    Private Sub mnuPayActivity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayActivity.Click
        Try
            Call SetControlVisibility(fpnlPayActivity)
            'Call Blank_ModuleName()
            'StrModuleName1 = mnuPayroll.Name
            'StrModuleName2 = mnuPayActivity.Name
            objlblCaption.Text = mnuPayActivity.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayActivity_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 10 JUNE 2013 ] -- END

    'Nilay (20-May-2016) -- Start
    'ENHANCEMENT - 61.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise)
    Private Sub mnuBudget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuBudget.Click
        Try
            Call SetControlVisibility(fpnlBudgetInfo)
            'Call Blank_ModuleName()
            'StrModuleName1 = mnuBudget.Name
            'StrModuleName2 = mnuBudget.Name
            objlblCaption.Text = mnuBudget.Text

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBudget_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (20-May-2016) -- End


    'Pinkal (13-Apr-2017) -- Start
    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
    'Private Sub mnuTimesheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTimesheet.Click
    '    Try
    '        Call SetControlVisibility(fpnlTimesheetInfo)
    '        Call Blank_ModuleName()
    '        StrModuleName1 = mnuTimesheet.Name
    '        objlblCaption.Text = mnuTimesheet.Text
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuBudget_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub mnuEmpBudgetTimesheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEmpBudgetTimesheet.Click
        Try
            Call SetControlVisibility(fpnlTimesheetInfo)
            'Call Blank_ModuleName()
            'StrModuleName1 = mnuEmpBudgetTimesheet.Name
            objlblCaption.Text = mnuEmpBudgetTimesheet.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEmpBudgetTimesheet_Click", mstrModuleName)
        End Try
    End Sub
    'Pinkal (13-Apr-2017) -- End


#End Region

#Region " Leave Information "

    Private Sub mnuLeaveInformation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLeaveInformation.Click
        Try
            Call SetControlVisibility(fpnlLeaveInfo)


            'Call Blank_ModuleName()
            'StrModuleName1 = mnuLeaveInformation.Name

            objlblCaption.Text = mnuLeaveInformation.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuLeaveInformation_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " TnA Information "

    Private Sub mnuTnAInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTnAInfo.Click
        Try
            Call SetControlVisibility(fpnlTnAInfo)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuTnAInfo.Name

            objlblCaption.Text = mnuTnAInfo.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuTnAInfo_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Utilities "

    Private Sub mnuUtilities_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUtilities.Click
        Try
            'Sandeep ( 18 JAN 2011 ) -- START
            If CBool(FinancialYear._Object._IsFin_Close) = True Then
                mnuReportView.Checked = False
                'mnuPayrollView.Checked = True
                If pnlMainInfo.Controls.Contains(ObjfrmReportView) Then
                    ObjfrmReportView = Nothing
                    pnlMainInfo.Controls.Remove(ObjfrmReportView)
                    tblMDILayout.Dock = DockStyle.Fill
                    tblMDILayout.Visible = True
                    tblMDILayout.BringToFront()
                    pnlMainInfo.Controls.Add(tblMDILayout)
                End If
            Else
                btnGeneralSettings.Enabled = True
                btnReminder.Enabled = True
                btnLetterTemplate.Enabled = True
                btnLetterType.Enabled = True
                btnMailbox.Enabled = True
                btnCommonExportData.Enabled = True
                btnCommonImportData.Enabled = True
                objAppSetting._LastViewAccessed = 0
            End If
            'Sandeep ( 18 JAN 2011 ) -- END 

            Call SetControlVisibility(fpnlUtility)

            'Call Blank_ModuleName()
            'StrModuleName1 = mnuUtilitiesMain.Name

            objlblCaption.Text = mnuUtilitiesMain.Text
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUtilities_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Audit Trails "

    Private Sub mnuAuditTrail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAuditTrails.Click
        Try
            Call SetControlVisibility(fpnlAuditTrails)
            objlblCaption.Text = mnuAuditTrail.Text
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuAuditTrail_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 19 JULY 2012 ] -- END


    'S.SANDEEP [ 03 FEB 2014 ] -- START
#Region " Claims & Expenses "

    Private Sub mnuClaimsExpenses_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClaimsExpenses.Click
        Try
            Call SetControlVisibility(fpnlClaimsExpenses)
            'Call Blank_ModuleName()
            'StrModuleName1 = mnuUtilities.OwnerItem.Name
            'StrModuleName2 = mnuClaimsExpenses.Name
            objlblCaption.Text = mnuClaimsExpenses.Text
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuClaimsExpenses_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 03 FEB 2014 ] -- END


#Region " View "

    Private Sub mnuPayrollView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayrollView.Click
        Try
            mnuReportView.Checked = False
            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuCustomReporting.Checked = False
            'S.SANDEEP [ 19 JUNE 2012 ] -- END

            Call SetVisibility(True)
            mnuPayrollView.Checked = True

            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If pnlMainInfo.Controls.Contains(ObjfrmReportView) Then
            'pnlMainInfo.Controls.Remove(ObjfrmReportView)
            If pnlMainInfo.Controls.Count > 0 Then
                pnlMainInfo.Controls.RemoveAt(0)
                'S.SANDEEP [ 19 JUNE 2012 ] -- END
                tblMDILayout.Dock = DockStyle.Fill
                tblMDILayout.Visible = True
                tblMDILayout.BringToFront()
                pnlMainInfo.Controls.Add(tblMDILayout)
                frmSlidingDashBoard.TopLevel = False
                frmSlidingDashBoard.Dock = DockStyle.Fill
                frmSlidingDashBoard.tabcDashBoard.SelectedTab = frmSlidingDashBoard.tabEmpDates
                frmSlidingDashBoard.SuspendLayout() 'Sohail (25 Apr 2019)
                pnlSummary.Controls.Add(frmSlidingDashBoard)
                frmSlidingDashBoard.objbtnSummaryReset.PerformClick()
                frmSlidingDashBoard.Show()
                frmSlidingDashBoard.ResumeLayout() 'Sohail (25 Apr 2019)
                frmSlidingDashBoard.BringToFront()
            End If
            objAppSetting._LastViewAccessed = 0


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayrollView_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuReportView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportView.Click
        Try

            mnuPayrollView.Checked = False
            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuCustomReporting.Checked = False
            'S.SANDEEP [ 19 JUNE 2012 ] -- END

            Call SetVisibility(False)
            mnuReportView.Checked = True

            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If pnlMainInfo.Controls.Contains(tblMDILayout) Then
            'pnlMainInfo.Controls.Remove(tblMDILayout)
            If pnlMainInfo.Controls.Count > 0 Then
                pnlMainInfo.Controls.RemoveAt(0)


                'Pinkal (28-Feb-2014) -- Start
                'Enhancement : TRA Changes
                tblMDILayout.Dock = DockStyle.Fill
                tblMDILayout.Visible = True
                tblMDILayout.BringToFront()
                pnlMainInfo.Controls.Add(tblMDILayout)
                'Pinkal (28-Feb-2014) -- End

                'S.SANDEEP [ 19 JUNE 2012 ] -- END
                ObjfrmReportView.TopLevel = False
                ObjfrmReportView.Size = pnlMainInfo.Size
                'Sandeep [ 10 FEB 2011 ] -- Start
                ObjfrmReportView.Dock = DockStyle.Fill
                'Sandeep [ 10 FEB 2011 ] -- End 
                pnlMainInfo.Controls.Add(ObjfrmReportView)
                ObjfrmReportView.Show()
                ObjfrmReportView.BringToFront()



            End If
            objAppSetting._LastViewAccessed = 1

            'Sandeep ( 18 JAN 2011 ) -- START
            If FinancialYear._Object._IsFin_Close = True Then
                mnuUtilitiesMain.Enabled = True
                btnGeneralSettings.Enabled = False
                btnReminder.Enabled = False
                btnLetterTemplate.Enabled = False
                btnLetterType.Enabled = False
                btnMailbox.Enabled = False
                btnCommonExportData.Enabled = False
                btnCommonImportData.Enabled = False
            Else
                btnGeneralSettings.Enabled = True
                btnReminder.Enabled = True
                btnLetterTemplate.Enabled = True
                btnLetterType.Enabled = True
                btnMailbox.Enabled = True
                btnCommonExportData.Enabled = True
                btnCommonImportData.Enabled = True
            End If
            'Sandeep ( 18 JAN 2011 ) -- END 



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuReportView_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 19 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuCustomReporting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCustomReporting.Click
        Try
            mnuPayrollView.Checked = False
            Call SetVisibility(False)
            mnuReportView.Checked = False
            mnuCustomReporting.Checked = True

            If pnlMainInfo.Controls.Count > 0 Then
                pnlMainInfo.Controls.RemoveAt(0)
                frmCustom_Report_View.TopLevel = False
                frmCustom_Report_View.Size = pnlMainInfo.Size
                pnlMainInfo.Controls.Add(frmCustom_Report_View)
                frmCustom_Report_View.Show()
                frmCustom_Report_View.BringToFront()
            End If
            objAppSetting._LastViewAccessed = 2

            If FinancialYear._Object._IsFin_Close = True Then
                mnuUtilitiesMain.Enabled = True
                btnGeneralSettings.Enabled = False
                btnReminder.Enabled = False
                btnLetterTemplate.Enabled = False
                btnLetterType.Enabled = False
                btnMailbox.Enabled = False
                btnCommonExportData.Enabled = False
                btnCommonImportData.Enabled = False
            Else
                btnGeneralSettings.Enabled = True
                btnReminder.Enabled = True
                btnLetterTemplate.Enabled = True
                btnLetterType.Enabled = True
                btnMailbox.Enabled = True
                btnCommonExportData.Enabled = True
                btnCommonImportData.Enabled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCustomReporting_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 19 JUNE 2012 ] -- END

#End Region

#Region " Exit "

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExit_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sandeep | 04 JAN 2010 | -- Start
#Region " HELP "

    Private Sub mnuProduct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProduct.Click
        Try
            Dim frm As New frmProductSupport
            frm.ShowDialog()
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuProduct_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        Try
            Dim frm As New frmAbout
            frm.ShowDialog()
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAbout_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuRegister.Click
        Try
            Dim frm As New frmNGLicense
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRegister_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuTroubleshoot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTroubleshoot.Click
        Try
            Dim frm As New frmExtendAMC
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRegister_Click", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sandeep | 04 JAN 2010 | -- END 

#End Region

#Region " Buttons "

#Region " General Masters "

    Private Sub btnBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBranch.Click, _
                                                                                                                                                                  btnDepartmentGroup.Click, btnDepartment.Click, btnGradeLevel.Click, _
                                                                                                                                                                  btnGrade.Click, btnGradeGroup.Click, btnClasses.Click, btnClassGroup.Click, _
                                                                                                  btnJob.Click, btnJobGroup.Click, btnUnit.Click, btnSection.Click, _
                                                                                                  btnSectionGroup.Click, btnUnitGroup.Click, _
                                                                                                  btnTeam.Click, btnAllocationMapping.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlCommomMasters)

            Dim frm As Form = Nothing
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNBRANCH"
                    frm = New frmStationList
                Case "BTNDEPARTMENTGROUP"
                    frm = New frmDepartmentGroupList
                Case "BTNDEPARTMENT"
                    frm = New frmDepartmentList
                Case "BTNGRADELEVEL"
                    frm = New frmGradelevelList
                Case "BTNGRADE"
                    frm = New frmGradeList
                Case "BTNGRADEGROUP"
                    frm = New frmGradeGroupList
                Case "BTNCLASSES"
                    frm = New frmClassesList
                Case "BTNCLASSGROUP"
                    frm = New frmClassGroupList
                Case "BTNJOB"
                    frm = New frmJobsList
                Case "BTNJOBGROUP"
                    frm = New frmJobGroupList
                Case "BTNUNIT"
                    frm = New frmUnitsList
                Case "BTNSECTION"
                    frm = New frmSectionList
                Case "BTNSECTIONGROUP"
                    frm = New frmSectionGroupList
                Case "BTNUNITGROUP"
                    frm = New frmUnitGroupList
                Case "BTNTEAM"
                    frm = New frmTeamsList
                    'S.SANDEEP [ 21 SEP 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNALLOCATIONMAPPING"
                    frm = New frmAlloc_Mapping_List
                    'S.SANDEEP [ 21 SEP 2012 ] -- END
            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnBranch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCoreSetups_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommonMaster.Click, btnMemebership.Click, btnState.Click, btnCity.Click, btnZipcode.Click, _
                                                                                                                                                                btnEmployeeData.Click, btnDependants.Click, btnEmployeeMovement.Click, btnShift.Click, _
                                                                                                        btnEmpApproverLevel.Click, btnEmployeeApprover.Click, btnApproveEmployee.Click, btnCountry.Click, _
                                                                                                        btnGarnishees.Click

        'Hemant (23 Jun 2023) -- [btnGarnishees]
        'Sohail (10 Jan 2020) - [btnCountry]

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[btnEmpApproverLevel.Click, btnEmployeeApprover.Click, btnApproveEmployee.Click]

        Try

            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlCoreSetups)

            Dim frm As Form = Nothing
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNCOMMONMASTER"
                    frm = New frmCommonMaster
                Case "BTNMEMEBERSHIP"
                    frm = New frmMembershipList
                    'Sohail (10 Jan 2020) -- Start
                    'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
                Case btnCountry.Name.ToUpper
                    frm = New frmCountry
                    'Sohail (10 Jan 2020) -- End
                Case "BTNSTATE"
                    frm = New frmstateList
                Case "BTNCITY"
                    frm = New frmCityList
                Case "BTNZIPCODE"
                    frm = New frmZipCodeList
                Case "BTNEMPLOYEEDATA"
                    Dim Efrm As New frmEmployeeList
                    Efrm.objefemailFooter.Visible = False
                    If Efrm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                Case "BTNDEPENDANTS"
                    frm = New frmDependantsAndBeneficiariesList
                Case "BTNEMPLOYEEMOVEMENT"
                    frm = New frmEmployeeMovementLog_List
                Case "BTNSHIFT"
                    frm = New frmShiftList

                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .

                Case "BTNEMPAPPROVERLEVEL"
                    frm = New frmEmpApproverLevelList
                    CType(frm, frmEmpApproverLevelList)._ApproverType = enEmpApproverType.APPR_EMPLOYEE
                Case "BTNEMPLOYEEAPPROVER"
                    frm = New frmEmployee_ApproverList
                    CType(frm, frmEmployee_ApproverList)._ApproverType = enEmpApproverType.APPR_EMPLOYEE
                Case "BTNAPPROVEEMPLOYEE"
                    frm = New frmEmployeeApproval
                    'S.SANDEEP [20-JUN-2018] -- End
                    'Hemant (23 Jun 2023) -- Start
                    'ENHANCEMENT(TRA) : A1X-1033 - Garnishment window (Capture beneficiary name, Bank Name, Branch, A/C no, Effective Period)
                Case "BTNGARNISHEES"
                    frm = New frmDependantsAndBeneficiariesList
                    CType(frm, frmDependantsAndBeneficiariesList)._IsGarnishees = True
                    'frm._IsFinalizeTraining = True
                    'Hemant (23 Jun 2023) -- End


            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCoreSetups_click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Human Resource "

#Region " Personnel "

    '======================================= HR Masters -- START
    Private Sub btnHRMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommonMaster.Click, btnReasons.Click, _
                                                                                                                                                                                btnSkillMaster.Click, btnAdvertiseMaster.Click, btnQualification.Click, _
                                                                                                                                                                                btnResultCode.Click, btnMemebership.Click, btnBenefitPlan.Click, _
                                                                                                      btnState.Click, btnCity.Click, btnZipcode.Click, _
                                                                                                      btnSkillExpertise.Click

        'Nilay (13 Apr 2017) -- [btnSkillExpertise.Click]

        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlHRMaster)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                'Case "BTNCOMMONMASTER"
                '    frm = New frmCommonMaster
                Case "BTNREASONS"
                    frm = New frmAction_ReasonList
                    frm._IsAction = False
                Case "BTNSKILLMASTER"
                    frm = New frmSkillList
                    'Nilay (13 Apr 2017) -- Start
                Case "BTNSKILLEXPERTISE"
                    frm = New frmSkillExpertiseList
                    'Nilay (13 Apr 2017) -- End

                Case "BTNADVERTISEMASTER"
                    frm = New frmAdvertiseList
                Case "BTNQUALIFICATION"
                    frm = New frmQualificationCourseList
                Case "BTNRESULTCODE"
                    frm = New frmResultCodesList
                    'Case "BTNMEMEBERSHIP"
                    '    frm = New frmMembershipList
                Case "BTNBENEFITPLAN"
                    frm = New frmBenefitplanList
                    'Case "BTNSTATE"
                    '    frm = New frmstateList
                    'Case "BTNCITY"
                    '    frm = New frmCityList
                    'Case "BTNZIPCODE"
                    '    frm = New frmZipCodeList
            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnHRMaster_Click", mstrModuleName)
        End Try
    End Sub
    '======================================= HR Masters -- END

    '======================================= Employee Data -- START
    Private Sub btnEmployeeData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmployeeData2.Click, btnDependants2.Click, _
                                                                                                                                                                              btnBenefitAllocation.Click, btnEmployeeBenefit.Click, btnGroupBenefit.Click, _
                                                                                                                                                                              btnReferee.Click, btnEmployeeAssets.Click, btnEmployeeSkill.Click, _
                                                                                                                                                                              btnEmployeeQualification.Click, btnEmployeeExperence.Click, btnEmployeeMovement2.Click, btnEmployeeDiary.Click, btnEmployeeDataApproval.Click 'Gajanan [17-DEC-2018] -- Start {btnEmployeeDataApproval} -- End
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlEmpData)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                'Case "BTNEMPLOYEEDATA"
                '    Dim Efrm As New frmEmployeeList
                '    Efrm.objefemailFooter.Visible = False
                '    If Efrm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                'Case "BTNDEPENDANTS"
                '    frm = New frmDependantsAndBeneficiariesList
                Case "BTNBENEFITALLOCATION"
                    frm = New frmBenefitallocationList
                Case "BTNEMPLOYEEBENEFIT"
                    frm = New frmBenefitCoverage_List
                Case "BTNGROUPBENEFIT"
                    'Sohail (17 Sep 2019) -- Start
                    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                    'frm = New frmAssignGroupBenefit
                    'Sohail (17 Sep 2019) -- End
                Case "BTNREFEREE"
                    frm = New frmEmployeeRefereeList
                Case "BTNEMPLOYEEASSETS"
                    frm = New frmAssetsRegisterList
                Case "BTNEMPLOYEESKILL"
                    frm = New frmEmployee_Skill_List
                Case "BTNEMPLOYEEQUALIFICATION"
                    frm = New frmQualificationsList
                Case "BTNEMPLOYEEEXPERENCE"
                    frm = New frmJobHistory_ExperienceList
                    'Case "BTNEMPLOYEEMOVEMENT"
                    '    frm = New frmEmployeeMovementLog_List
                Case "BTNEMPLOYEEDIARY"
                    frm = New frmEmployeeDiary
                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case "BTNEMPLOYEEDATAAPPROVAL"
                    frm = New frmAppRejEmpData
                    'Gajanan [17-DEC-2018] -- End

            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmployeeData_Click", mstrModuleName)
        End Try
    End Sub
    '======================================= Employee Data -- END

    '======================================= Discipline -- START
    Private Sub btnDisciplineType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisciplineType.Click, btnDisciplinaryAction.Click, _
                                                                                                    btnDisciplineStatus.Click, btnDisciplineFiling.Click, _
                                                                                                     btnResolutionSteps.Click, btnCommittee.Click, _
                                                                                                     btnHearingList.Click
        'S.SANDEEP [24 MAY 2016] -- [btnHearingList.Click]

        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlDiscipline)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNDISCIPLINETYPE"
                    frm = New frmDisciplineTypeList
                Case "BTNDISCIPLINARYACTION"
                    frm = New frmAction_ReasonList
                    frm._IsAction = True
                Case "BTNDISCIPLINESTATUS"
                    frm = New frmDisciplineStatusList
                Case "BTNDISCIPLINEFILING"
                    frm = New frmDisciplineFilingList
                Case "BTNRESOLUTIONSTEPS"
                    'S.SANDEEP [24 MAY 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    'frm = New frmDisciplineResolutionList
                    frm = New frmDisciplineProceedingList
                    'S.SANDEEP [24 MAY 2016] -- END
                Case "BTNCOMMITTEE"
                    frm = New frmCommitteeList
                    'S.SANDEEP [24 MAY 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                Case "BTNHEARINGLIST"
                    frm = New frmDisciplineHearingList
                    'S.SANDEEP [24 MAY 2016] -- END

            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDisciplineType_Click", mstrModuleName)
        End Try
    End Sub
    '======================================= Discipline -- END

    '======================================= Employee Assessment -- START
    Private Sub btnAssessmentPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssessmentPeriod.Click, btnAssessGroup.Click, btnAssessItems.Click, _
                                                                                                                                                                                        btnAssessor.Click, btnAssessorAccess.Click, _
                                                                                                                                                                                        btnEmployeeAssessment.Click, btnAssessorAssessmentlist.Click, _
                                                                                                              btnAssessSubItem.Click, btnReviewer.Click, btnReviewerAssessment.Click, btnExternalAssessor.Click, btnMigration.Click, btnCloseAssessmentPeriod.Click, btnAssessmentRatio.Click, _
                                                                                                              btnAssessCaptionSettings.Click, btnAssessScale.Click, _
                                                                                                              btnAssessCompanyGoals.Click, btnAllocationGoals.Click, btnEmployeeGoals.Click, btnAssessmentMapping.Click, _
                                                                                                              btnCompetencies.Click, btnAssignCompetencies.Click, btnCustomHeaders.Click, btnCustomItems.Click, btnBSCViewSetting.Click, _
                                                                                                              btnOpenPeriodicReview.Click, btnPerspective.Click, btnComputationFormula.Click, btnPerformancePlan.Click

        Try


            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlAssessment)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNASSESSMENTPERIOD"
                    Dim APfrm As New frmPeriodList

                    'Pinkal (24-Jan-2011) -- Start
                    APfrm.isAssessment = True
                    APfrm.mintModuleRefId = CInt(enModuleReference.Assessment)
                    If APfrm IsNot Nothing Then
                        If APfrm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    End If
                    'Pinkal (24-Jan-2011) -- End
                Case "BTNASSESSGROUP"
                    frm = New frmGroupList
                Case "BTNASSESSITEMS"
                    frm = New frmAssessmentItemsList
                    'S.SANDEEP [ 22 OCT 2013 ] -- START
                    'ENHANCEMENT : ENHANCEMENT
                    'Case "BTNASSESSOR"
                    '    frm = New frmAssessor_AddEdit
                    'S.SANDEEP [ 22 OCT 2013 ] -- END
                Case "BTNASSESSORACCESS"
                    frm = New frmAssessor_List
                Case "BTNEMPLOYEEASSESSMENT"
                    frm = New frmSelfAssessmentList
                Case "BTNASSESSORASSESSMENTLIST"
                    frm = New frmAssessor_AssessmentList
                Case "BTNASSESSSUBITEM"
                    frm = New frmAssessSubItemList
                Case "BTNREVIEWER"
                    frm = New frmReviewer_List
                Case "BTNREVIEWERASSESSMENT"
                    frm = New frmReviewerAssessment_List
                Case "BTNEXTERNALASSESSOR"
                    frm = New frmExternalAssessorList
                Case "BTNMIGRATION"
                    frm = New frmMigration
                    'S.SANDEEP [ 28 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNCLOSEASSESSMENTPERIOD"
                    frm = New frmCloseAssessmentPeriod
                    'S.SANDEEP [ 28 DEC 2012 ] -- END

                    'S.SANDEEP [ 09 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNASSESSMENTRATIO"
                    frm = New frmAssessmentRatio_List
                    'S.SANDEEP [ 09 AUG 2013 ] -- END

                    'S.SANDEEP [ 05 NOV 2014 ] -- START
                Case "BTNASSESSCAPTIONSETTINGS"
                    frm = New frmAssessmentCaptions
                Case "BTNASSESSMENTMAPPING"
                    frm = New frmCaptionMappingList
                Case "BTNASSESSSCALE"
                    frm = New frmAssessScale_List
                Case "BTNASSESSCOMPANYGOALS"
                    frm = New frmCoyFieldsList
                Case "BTNALLOCATIONGOALS"
                    frm = New frmOwrFieldsList
                Case "BTNEMPLOYEEGOALS"
                    frm = New frmEmpFieldsList
                Case "BTNCOMPETENCIES"
                    frm = New frmCompetenciesList
                Case "BTNASSIGNCOMPETENCIES"
                    frm = New frmAssignedCompetenciesList
                Case "BTNCUSTOMHEADERS"
                    frm = New frmCustomHeaderList
                Case "BTNCUSTOMITEMS"
                    frm = New frmCustomItemsList
                Case "BTNBSCVIEWSETTING"
                    frm = New frmCaptionViewSetting
                Case "BTNOPENPERIODICREVIEW"
                    frm = New frmPeriodicReview
                Case "BTNPERSPECTIVE"
                    frm = New frmPerspective_List
                    'S.SANDEEP [ 05 NOV 2014 ] -- END

                    'S.SANDEEP [ 01 JAN 2015 ] -- START
                Case "BTNCOMPUTATIONFORMULA"
                    frm = New frmComputationFormulaList
                    'S.SANDEEP [ 01 JAN 2015 ] -- END

                    'S.SANDEEP [29 JAN 2015] -- START
                Case "BTNPERFORMANCEPLAN"
                    frm = New frmViewPerformancePlanning
                    'S.SANDEEP [29 JAN 2015] -- END
            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssessmentPeriod_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Private Sub btnSelfPEvaluation_Clicl(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelfPEvaluation.Click, btnAssessorPEvaluation.Click, btnReviewerPEvaluation.Click, btnGlobalVoidAssessment.Click, btnComputeProcess.Click, btnExportPrintEmplForm.Click
        'S.SANDEEP [16 DEC 2016] -- START {btnExportPrintEmplForm} -- END

        'Shani (24-May-2016) -- Start {btnComputeProcess} --END
        'S.SANDEEP [04 JUN 2015] -- START { btnGlobalVoidAssessment }-- END

        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlPerformanceEval)
            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case btnSelfPEvaluation.Name.ToUpper
                    frm = New frmSelfEvaluationList
                Case btnAssessorPEvaluation.Name.ToUpper
                    frm = New frmAssessorEvaluationList
                Case btnReviewerPEvaluation.Name.ToUpper
                    frm = New frmReviewerEvaluationList
                    'S.SANDEEP [04 JUN 2015] -- START
                Case btnGlobalVoidAssessment.Name.ToUpper
                    frm = New frmGlobalVoidAssessment
                    'S.SANDEEP [04 JUN 2015] -- END
                    'Shani (24-May-2016) -- Start
                Case btnComputeProcess.Name.ToUpper
                    frm = New frmComputeScore
                    'Shani (24-May-2016) -- End

                    'S.SANDEEP [16 DEC 2016] -- START
                Case btnExportPrintEmplForm.Name.ToUpper
                    frm = New frmPrintExportEmployeeForm
                    'S.SANDEEP [16 DEC 2016] -- END


            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSelfPEvaluation_Clicl", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 NOV 2014 ] -- END

    Private Sub btnObjectiveList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnObjectiveList.Click, _
                                                                                                                                                               btnInitiativeList.Click, _
                                                                                                                                                               btnTargetsList.Click, _
                                                                                                                                                               btnKPIMeasureList.Click, btnSelfAssessedBSC.Click, _
                                                                                                                                                               btnAssessorBSCList.Click, btnReviewerBSCList.Click, _
                                                                                                                                                               btnWeightedSetting.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlBSC)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNOBJECTIVELIST"
                    frm = New frmBSC_ObjectiveList
                Case "BTNINITIATIVELIST"
                    frm = New frmBSC_InitiativeList
                Case "BTNTARGETSLIST"
                    frm = New frmBSC_TargetList
                Case "BTNKPIMEASURELIST"
                    frm = New frmBSC_KPIMeasureList
                Case "BTNSELFASSESSEDBSC"
                    frm = New frmBSCSelfAssessmentList
                Case "BTNASSESSORBSCLIST"
                    frm = New frmBSCAssessorAssessmentList
                Case "BTNREVIEWERBSCLIST"
                    frm = New frmBSCReviewerAssessmentList
                    'S.SANDEEP [ 28 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNWEIGHTEDSETTING"
                    frm = New frmBSC_WeightSettings
                    'S.SANDEEP [ 28 DEC 2012 ] -- END
            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 04 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub btnShortListEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShortListEmployee.Click, btnFinalShortListedEmployee.Click, btnAppraisalSetups.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlAppraisal)
            Dim frm As Object = Nothing
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNSHORTLISTEMPLOYEE"
                    frm = New frmAppraisalRefrenceList
                Case "BTNFINALSHORTLISTEDEMPLOYEE"
                    frm = New frmAppraisals
                    'S.SANDEEP [ 14 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNAPPRAISALSETUPS"
                    frm = New frmAppraisalSetups
                    'S.SANDEEP [ 14 AUG 2013 ] -- END
            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnShortListEmployee_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 04 FEB 2012 ] -- END
    '======================================= Employee Assessment -- END

    '======================================= Employee Training -- START
    Private Sub btnTrainingInstitute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTrainingInstitute.Click, btnTrainingScheduling.Click, _
                                                                                                        btnTrainingEnrollment.Click, btnTrainingAttendance.Click, _
                                                                                                               btnTrainingAnalysis.Click, btnTrainingApproverEmployeeMapping.Click
        'Hemant (18 Jul 2022) -- [btnTrainingApproverEmployeeMapping.Click]
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlTrainingInfo)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNTRAININGINSTITUTE"
                    frm = New frmTraningInstitutesList
                    frm._IsHospital = False
                Case "BTNTRAININGSCHEDULING"
                    frm = New frmCourseSchedulingList
                Case "BTNTRAININGENROLLMENT"
                    frm = New frmTraningRegistraionCancelList
                Case "BTNTRAININGATTENDANCE"
                    frm = New frmTrainingAttendanceList
                Case "BTNTRAININGANALYSIS"
                    frm = New frmTrainingAnalysisList
                    'S.SANDEEP [ 04 FEB 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNTRAININGPRIORITY"
                    frm = New frmTraningPriorityList
                Case "BTNFINALIZINGTRAININGPRIORITY"
                    frm = New frmTraining_Priority
                    frm._IsFinalizeTraining = True
                    'S.SANDEEP [ 04 FEB 2012 ] -- END
                    'Hemant (18 Jul 2022) -- Start            
                    'ENHANCEMENT(NMB) : AC2-722) - Approver import via excel
                Case "BTNTRAININGAPPROVEREMPLOYEEMAPPING"
                    frm = New frmTrainingApprEmpMappingImportWizard
                    'Hemant (18 Jul 2022) -- End
            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTrainingInstitute_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnTrainingPriority_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTrainingPriority.Click, btnFinalizingTrainingPriority.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlTrainingNeed)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNTRAININGPRIORITY"
                    frm = New frmTraningPriorityList
                Case "BTNFINALIZINGTRAININGPRIORITY"
                    frm = New frmTraining_Priority
                    frm._IsFinalizeTraining = True
            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTrainingPriority_Click", mstrModuleName)
        End Try
    End Sub
    '======================================= Employee Training -- END

    '======================================= Training Feedback -- START
    Private Sub btnFeedbackGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFeedbackGroup.Click, _
                                                                                                           btnFeedbackItems.Click, _
                                                                                                           btnFeedbackSubItems.Click, _
                                                                                                           btnFeedback.Click, btnEvaluationLevel3.Click, btnImpactItems.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlTrainingFeedback)
            Dim frm As Object = Nothing
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNFEEDBACKGROUP"
                    frm = frmFeedbackGroupList
                Case "BTNFEEDBACKITEMS"
                    frm = frmFeedbackItemList
                Case "BTNFEEDBACKSUBITEMS"
                    frm = frmFeedbackSubItemList
                Case "BTNFEEDBACK"
                    frm = frmEmployeeFeedbackList
                Case "BTNEVALUATIONLEVEL3"
                    frm = frmTrainingImpactList
                Case "BTNIMPACTITEMS"
                    frm = New frmImpact_feedbackList
            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFeedbackGroup_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    '======================================= Training Feedback  -- END

#End Region

#Region " Medical "



    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Private Sub btnMedicalMasters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMedicalMasters.Click, btnMedicalInstitute.Click, _
                                                                                                                                                                                btnEmployeeInjuries.Click, btnCategoryAssignment.Click, _
                                                                                                                                                                                btnCoverAssignment.Click, btnMedicalClaim.Click, _
                                                                                                                                                                                btnSickSheet.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlMedicalInfo)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNMEDICALMASTERS"
                    frm = New frmMedicalMastersList
                Case "BTNMEDICALINSTITUTE"
                    frm = New frmTraningInstitutesList
                    frm._IsHospital = True
                Case "BTNEMPLOYEEINJURIES"
                    frm = New frmMedicalFormList
                Case "BTNCATEGORYASSIGNMENT"
                    frm = New frmMedicalCategoryTranList
                Case "BTNCOVERASSIGNMENT"
                    frm = New frmCoverAllocationList
                Case "BTNMEDICALCLAIM"
                    frm = New frmMedical_Claim_List
                Case "BTNSICKSHEET"
                    frm = New frmSickSheetList

                    'Pinkal (20-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                    CType(frm, frmSickSheetList).objEFooter.Visible = False
                    'Pinkal (20-Jan-2012) -- End
                    'Case "BTNDEPENDANTEXCEPTION"
                    'frm = New frmDependantException

            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMedicalMasters_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Oct-2011) -- End

#End Region

#Region " Recruitment "
    Private Sub btnApplicantMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplicantMaster.Click, btnVacancy.Click, btnFinalApplicants.Click, btnInterviewAnalysis.Click, _
                                                                                                             btnInterviewScheduling.Click, btnBatchScheduling.Click, btnImportfromWeb.Click, _
                                                                                                             btnExporttoWeb.Click, btnApplicantFilter.Click, btnFinalShortListedApplicant.Click, _
                                                                                                             btnAutoImportFromWeb.Click, btnGlobalInterviewAnalysis.Click, btnCandidateFeedback.Click, btnDownloadAptitudeResult.Click

        'Pinkal (28-Apr-2023) --(A1X-865) NMB - Read aptitude test results from provided views.[btnDownloadAptitudeResult.Click]
        'Pinkal (31-Mar-2023) -- (A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.[, btnCandidateFeedback.Click]
        Try

            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlRecruitment)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNAPPLICANTMASTER"
                    Dim Afrm As New frmApplicantList
                    Afrm.objefemailFooter.Visible = False
                    If Afrm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                Case "BTNVACANCY"
                    frm = New frmVacancyList
                Case "BTNFINALAPPLICANTS"
                    frm = New frmFinalShortListApplicant
                Case "BTNINTERVIEWANALYSIS"
                    frm = New frmInterviewAnalysisList
                Case "BTNINTERVIEWSCHEDULING"
                    frm = New frmApplicantInterviewSchedulingList
                Case "BTNBATCHSCHEDULING"
                    frm = New frmInterviewBatchScheduleList
                    'S.SANDEEP [ 25 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNIMPORTFROMWEB"

                    frm = New frmSynchronizeData(True)

                Case "BTNEXPORTTOWEB"

                    frm = New frmSynchronizeData(False)

                    'S.SANDEEP [ 25 DEC 2011 ] -- END
                    'Pinkal (12-Oct-2011) -- Start
                    'Enhancement : TRA Changes
                Case "BTNAPPLICANTFILTER"
                    frm = New frmApplicantFilter_List

                Case "BTNFINALSHORTLISTEDAPPLICANT"
                    frm = New frmFinalApplicant

                    'Pinkal (12-Oct-2011) -- End

                    'Sohail (23 Oct 2012) -- Start
                    'TRA - ENHANCEMENT
                Case btnAutoImportFromWeb.Name.ToUpper
                    frm = New frmAutoImportApplicantWizard
                    'Sohail (23 Oct 2012) -- End

                    'Hemant (03 Jun 2020) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
                Case "BTNGLOBALINTERVIEWANALYSIS"
                    frm = New frmGlobalInterviewAnalysis
                    'Hemant (03 Jun 2020) -- End

                    'Pinkal (31-Mar-2023) -- Start
                    '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
                Case "BTNCANDIDATEFEEDBACK"
                    'Pinkal (15-May-2023) -- Start
                    ' (A1X-850) Tujijenge - Dashboard pending actions to show list of records after redirecting to the action page.
                    'frm = New frmDownloadAptitudeResult
                    frm = New frmCandidateFeedback
                    'Pinkal (15-May-2023) -- End
                    'Pinkal (31-Mar-2023) -- End

                    'Pinkal (28-Apr-2023) -- Start
                    '(A1X-865) NMB - Read aptitude test results from provided views.
                Case "BTNDOWNLOADAPTITUDERESULT"
                    frm = New frmDownloadAptitudeResult
                    'Pinkal (28-Apr-2023) -- End


            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
            Else
                CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
            'S.SANDEEP [ 25 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApplicantMaster_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#End Region

#Region " Payroll "

#Region " Payroll Masters "

    Private Sub btnPayrollGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayrollGroup.Click, btnPayrollPeriods.Click, _
                                                                                                          btnCostcenter.Click, btnPayPoint.Click, btnBankBranch.Click, _
                                                                                                          btnBankAccountType.Click, btnBankEDI.Click, btnCurrency.Click, btnDemomination.Click, btnPayslipMessage.Click, btnGlobalPayslipMessage.Click, btnAccount.Click, btnAccountConfiguration.Click, btnEmpAccConfig.Click, btnCCAccConfig.Click, btnPayAppLevel.Click, btnPayApproverMap.Click, btnStaffReqAppLevel.Click, btnStaffReqAppMapping.Click, btnStaffRequisition.Click, btnTransferStaffRequisitionApprover.Click
        'Sohail (31 Jan 2022) - [btnTransferStaffRequisitionApprover]
        'Sohail (28 May 2014) - [btnStaffReqAppLevel, btnStaffReqAppMapping, btnStaffRequisition]
        'S.SANDEEP [ 13 FEB 2013 ] -- START -- END

        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlPayrollMasters)

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNPAYROLLGROUP"
                    frm = New frmPayrollGroupsList
                Case "BTNPAYROLLPERIODS"
                    Dim Pfrm As New frmPeriodList

                    'Pinkal (24-Jan-2011) -- Start

                    Pfrm.mintModuleRefId = CInt(enModuleReference.Payroll)
                    If Pfrm IsNot Nothing Then
                        If Pfrm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    End If
                    'Pinkal (24-Jan-2011) -- End
                Case "BTNCOSTCENTER"
                    frm = New frmCostcenterList
                Case "BTNPAYPOINT"
                    frm = New frmPaypointList
                Case "BTNBANKBRANCH"
                    frm = New frmBankBranchList
                Case "BTNBANKACCOUNTTYPE"
                    frm = New frmBankAccountTypeList
                Case "BTNBANKEDI"
                    frm = New frmBankEDIList
                Case "BTNCURRENCY"
                    frm = New frmExchangeRateList
                Case "BTNDEMOMINATION"
                    frm = New frmdenominationList
                Case "BTNPAYSLIPMESSAGE"
                    frm = New frmPaySlipMsgList
                Case "BTNGLOBALPAYSLIPMESSAGE"
                    'Nilay (28-Apr-2016) -- Start
                    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                    'frm = New frmPayslipGlobalMsg
                    frm = New frmPayslipGlobalMsgList
                    'Nilay (28-Apr-2016) -- End
                Case "BTNACCOUNT"
                    frm = New frmAccountList
                Case "BTNACCOUNTCONFIGURATION"
                    frm = New frmAccountConfigurationList
                Case "BTNEMPACCCONFIG"
                    frm = New frmEmpAccountConfigurationList
                Case "BTNCCACCCONFIG"
                    frm = New frmCCAccountConfigurationList
                    'S.SANDEEP [ 13 FEB 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNPAYAPPLEVEL"
                    frm = New frmPaymentApproverLevelList
                Case "BTNPAYAPPROVERMAP"
                    frm = New frmPaymentApproverMappingList
                    'S.SANDEEP [ 13 FEB 2013 ] -- END
                    'Sohail (28 May 2014) -- Start
                    'Enhancement - Staff Requisition.
                Case btnStaffReqAppLevel.Name.ToUpper
                    frm = New frmStaffRequisitionApproverLevelList
                Case btnStaffReqAppMapping.Name.ToUpper
                    frm = New frmStaffRequisitionApproverMappingList
                Case btnStaffRequisition.Name.ToUpper
                    frm = New frmStaffRequisitionList
                    'Sohail (28 May 2014) -- End
                    'Sohail (31 Jan 2022) -- Start
                    'Enhancement :  OLD-547 : NMB - Creation of staff requisition approver migration.
                Case btnTransferStaffRequisitionApprover.Name.ToUpper
                    frm = New frmTransferStaffRequisitionApprover
                    'Sohail (31 Jan 2022) -- End

            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPayrollGroup_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Payroll Transaction "

    Private Sub btnEmployeeCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmployeeCostCenter.Click, btnEmployeeBanks.Click, btnBatchTransaction.Click, _
                                                                                                                                                                                         btnTransactionHeads.Click, btnEarningDeduction.Click, btnProcessPayroll.Click, _
                                                                                                                                                                                         btnBudgetPreparation.Click, btnPayslip.Click, btnSalaryIncrement.Click, _
                                                                                                                                                                                         btnWagesTable.Click, btnEmployeeExemption.Click, btnGlobalAssignED.Click, _
                                                                                                                                                                                         btnBatchEntry.Click, btnClosePayroll.Click, btnCompanyJV.Click, btnCashDenomination.Click, btnBatchPosting.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlPayrollTransaction)

            Dim blnECC As Boolean = False

            Dim frm As Object = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNEMPLOYEECOSTCENTER"
                    'frm = New frmEmployeeCostCenter
                    'blnECC = True
                    frm = New frmEmpDistributedCostCenterList

                Case "BTNEMPLOYEEBANKS"
                    frm = New frmEmployeeBankList
                Case "BTNBATCHTRANSACTION"
                    frm = New frmBatchTransactionList
                Case "BTNTRANSACTIONHEADS"
                    frm = New frmTransactionHeadList
                Case "BTNEARNINGDEDUCTION"
                    frm = New frmEarningDeductionList
                Case "BTNPROCESSPAYROLL"
                    frm = New frmProcessPayroll
                Case "BTNBUDGETPREPARATION"
                    frm = New frmBudgetPreparation_Wizard
                Case "BTNPAYSLIP"
                    frm = New frmPayslipList
                Case "BTNSALARYINCREMENT"
                    frm = New frmSalaryIncrementList
                Case "BTNWAGESTABLE"
                    frm = New frmWagetable_AddEdit
                Case "BTNEMPLOYEEEXEMPTION"
                    frm = New frmEmployee_Exemption_List
                Case "BTNGLOBALASSIGNED"
                    'Sohail (03 Nov 2010) -- Start
                    'frm = New frmGlobalAssignEarningDeduction
                    Dim Gfrm As New frmGlobalAssignEarningDeduction
                    If Gfrm.DisplayDialog(enAction.ADD_CONTINUE) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    'Sohail (03 Nov 2010) -- End
                Case "BTNBATCHENTRY"
                    'Sohail (03 Nov 2010) -- Start
                    'frm = New frmBatchEntry
                    Dim Gfrm As New frmBatchEntry
                    If Gfrm.DisplayDialog(enAction.ADD_CONTINUE) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    'Sohail (03 Nov 2010) -- End
                Case "BTNCLOSEPAYROLL"
                    tmrReminder.Enabled = False
                    'tmrReminder.Stop()
                    frm = New frmClosePayroll
                    'Case "BTNCOMPANYJV"
                    '    frm = New frmJournalVoucherList
                Case "BTNCASHDENOMINATION"
                    frm = New frmCashDenominationList

                    'Anjan (06 Dec 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Rutta's Request
                Case "BTNBATCHPOSTING"
                    frm = New frmBatchPostingList
                    'Anjan (06 Dec 2012)-End 

            End Select

            If blnECC = True Then
                If frm.displayDialog(-1, enAction.ADD_CONTINUE) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            Else
                If frm IsNot Nothing Then
                    If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmployeeCostCenter_Click", mstrModuleName)
        Finally
            tmrReminder.Enabled = True
            'tmrReminder.Start()
        End Try
    End Sub

#End Region

#Region " Loan & Savings "

    Private Sub btnLoanScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoanScheme.Click, btnLoanApproverLevel.Click _
                                                                                                                                                                    , btnLoanApprover.Click, btnLoanApplication.Click _
                                                                                                                                                                    , btnProcessPendingLoan.Click, btnLoanAdvance.Click _
                                                                                                                                                                    , btnImportLA.Click, btnSavingScheme.Click _
                                                                                                                                                                    , btnEmployeeSavings.Click, btnImportSaving.Click _
                                                                                                                                                                    , btnLoanSwapApprover.Click, btnLoanApproverMigration.Click _
                                                                                                                                                                    , btnLoanSchemeCategoryMapping.Click _
                                                                                                                                                                    , btnFlexcubeLoanApplication.Click

        'Hemant (27 Oct 2022) -- [btnFlexcubeLoanApplication.Click]
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlLoanInfo)

            Dim frm As Form = Nothing
            Dim blnViewer As Boolean = False        'Pinkal (14-Apr-2015) -- Start        'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNLOANSCHEME"
                    frm = New frmLoanSchemeList
                Case "BTNLOANAPPROVER"
                    frm = New frmLoanApproverList
                Case "BTNSAVINGSCHEME"
                    frm = New frmSavingSchemeList
                Case "BTNEMPLOYEESAVINGS"
                    frm = New frmEmployeeSavingsList
                Case "BTNPROCESSPENDINGLOAN"
                    frm = New frmLoanApprovalList
                Case "BTNLOANADVANCE"
                    frm = New frmNewLoanAdvanceList
                Case "BTNLOANAPPROVER_MAPPING"
                    frm = New frmLoanApproverList
                Case "BTNIMPORTLA"
                    frm = New frmImportLoan_AdvanceWizard
                Case "BTNIMPORTSAVING"
                    frm = New frmImportSavingWizard
                Case "BTNLOANAPPROVERMIGRATION"
                    frm = New frmLoanApprover_Migration


                    'Pinkal (14-Apr-2015) -- Start
                    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

                Case "BTNLOANAPPROVERLEVEL"
                    frm = New frmLoanApproverLevelList

                Case "BTNLOANAPPLICATION"
                    'Hemant (27 Oct 2022) -- Start
                    'frm = New frmLoanApplicationList
                    Dim vfrm As New frmLoanApplicationList
                    If vfrm.displayDialog(False) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True
                    'Hemant (27 Oct 2022) -- End


                Case "BTNLOANSWAPAPPROVER"
                    Dim Vfrm As New frmApprover_Swapping
                    If Vfrm.displayDialog(-1, enAction.ADD_ONE, enSwapApproverType.Loan) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True

                    'Pinkal (14-Apr-2015) -- End

                    'Hemant (22 Sep 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-875 - NMB - As a user, I want Upon clicking on the save button, system should not allow saving the application if the identity is blank or pending for approval where identity type = TZ National ID
                Case "BTNLOANSCHEMECATEGORYMAPPING"
                    frm = New frmLoanSchemeCategoryMapping
                    'Hemant (22 Sep 2022) -- End

                    'Hemant (27 Oct 2022) -- Start
                Case "BTNFLEXCUBELOANAPPLICATION"
                    Dim vfrm As New frmLoanApplicationList
                    If vfrm.displayDialog(True) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True
                    'Hemant (27 Oct 2022) -- End


            End Select


            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            If blnViewer = False Then
                If frm IsNot Nothing Then
                    If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                End If
            End If

            'Pinkal (14-Apr-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLoanScheme_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Nilay (20-May-2016) -- Start
    'ENHANCEMENT - 61.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise)
#Region " Budget "

    Private Sub btnFundSources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFundSources.Click, _
                                                                                                  btnFundActivity.Click, _
                                                                                                  btnProjectCodeAdjustments.Click, _
                                                                                                  btnFundActivityAdjustment.Click, _
                                                                                                  btnBudgetFormula.Click, _
                                                                                                  btnBudget.Click, _
                                                                                                  btnBudgetCodes.Click, _
                                                                                                  btnFundProjectCode.Click, _
                                                                                                  btnBgtApproverLevel.Click, _
                                                                                                  btnBgtApproverMapping.Click
        Try
            Dim frm As Form = Nothing
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlLoanInfo)

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case btnFundSources.Name.ToUpper
                    frm = New frmFundSource_List

                Case btnFundActivity.Name.ToUpper
                    frm = New frmFundActivityList

                Case btnFundActivityAdjustment.Name.ToUpper
                    frm = New frmFundActivityAdjustment_List

                Case btnBudgetFormula.Name.ToUpper
                    frm = New frmBudgetFormulaList

                Case btnProjectCodeAdjustments.Name.ToUpper
                    frm = New frmFundAdjustment_List

                Case btnBudgetFormula.Name.ToUpper
                    frm = New frmBudgetFormulaList

                Case btnBudget.Name.ToUpper
                    frm = New frmBudgetList

                    'Sohail (07 Jun 2016) -- Start
                    'Enhancement -  62.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise).
                Case btnBudgetCodes.Name.ToUpper
                    frm = New frmBudgetCodesList

                Case btnFundProjectCode.Name.ToUpper
                    frm = New frmFundProjectCodeList
                    'Sohail (07 Jun 2016) -- End
                    'Pinkal (08-Aug-2016) -- Start
                    'Enhancement - Budget Approver Level and Budget Approver Level Mapping.

                Case btnBgtApproverLevel.Name.ToUpper
                    frm = New frmApproverLevelList

                Case btnBgtApproverMapping.Name.ToUpper
                    frm = New frmApproverMappingList

                    'Pinkal (08-Aug-2016) -- End   

            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFundSources_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    'Nilay (20-May-2016) -- End


    'S.SANDEEP [ 10 JUNE 2013 ] -- START
    'ENHANCEMENT : OTHER CHANGES
#Region " Pay Per Activity "

    Private Sub btnActivity_List_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivity_List.Click, btnMeasureList.Click, btnPayPerActivity.Click, btnActivityRate.Click, btnPayrollPosting.Click, btnActivityGlobalAssign.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlPayActivity)

            Dim frm As Form = Nothing
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNACTIVITY_LIST"
                    frm = New frmActivityList
                Case "BTNMEASURELIST"
                    frm = New frmMeasureList
                Case "BTNPAYPERACTIVITY"
                    frm = New frmBulkInput
                Case "BTNACTIVITYRATE"
                    frm = New frmSet_Activity_Rate
                Case "BTNPAYROLLPOSTING"
                    frm = New frmActivity_Posting
                Case "BTNACTIVITYGLOBALASSIGN"
                    frm = New frmGlobalAssignAcitivty
            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnActivity_List_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 10 JUNE 2013 ] -- END



    'Pinkal (21-Oct-2016) -- Start
    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.

#Region "Timesheet"

    Private Sub btnTimesheetApproverLevel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimesheetApproverLevel.Click _
                                                                                                                                                                    , btnTimesheetApproverMst.Click _
                                                                                                                                                                    , btnEmployeeTimesheet.Click _
                                                                                                                                                                    , btnTimesheetApproval.Click _
                                                                                                                                                                    , btnTimesheetApproverMigration.Click
        Try
            Dim frm As Form = Nothing
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlTimesheetInfo)

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper

                Case btnTimesheetApproverLevel.Name.ToUpper
                    frm = New frmTimesheetApproverLevelList

                Case btnTimesheetApproverMst.Name.ToUpper
                    frm = New frmTimesheetApproversList

                Case btnEmployeeTimesheet.Name.ToUpper
                    frm = New frmBudgetEmp_Timesheet

                Case btnTimesheetApproval.Name.ToUpper
                    frm = New frmTimesheetApprovalList

                Case btnTimesheetApproverMigration.Name.ToUpper
                    frm = New frmBudgetTimesheetApprover_Migration

            End Select

            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTimesheetApproverLevel_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    'Pinkal (21-Oct-2016) -- End


#End Region

#Region " Leave & TnA "

#Region " Leave Info "

    Private Sub btnLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLeaveType.Click, btnHolidays.Click, btnEmployeeHolidays.Click, _
                                                                                                                                                                             btnApproverLevel.Click, btnLeaveApprover.Click, btnLeaveForm.Click, _
                                                                                                                                                                             btnLeaveProcess.Click, btnLeaveAccrue.Click, btnLeaveViewer.Click, _
                                                                                                       btnLeaveSchedular.Click, btnLeavePlanner.Click, _
                                                                                                       btnImportAccrueLeave.Click, BtnTransferApprover.Click, btnEndELC.Click, btnLeaveFrequency.Click, _
                                                                                                       btnLeaveAdjustment.Click, btnLVSwapApprover.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlLeaveInfo)

            Dim frm As Form = Nothing
            Dim blnViewer As Boolean = False
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNLEAVETYPE"
                    frm = New frmLeaveTypeList
                Case "BTNHOLIDAYS"
                    frm = New frmHolidaysList
                Case "BTNEMPLOYEEHOLIDAYS"
                    frm = New frmAssignHolidaysList
                Case "BTNAPPROVERLEVEL"
                    frm = New frmLeaveApproverLevelList
                Case "BTNLEAVEAPPROVER"
                    frm = New frmLeaveApproversList
                Case "BTNLEAVEFORM"
                    frm = New frmLeaveFormList
                Case "BTNLEAVEPROCESS"
                    frm = New frmProcessLeaveList
                Case "BTNLEAVEACCRUE"
                    frm = New frmLeaveAccrue_AddEdit
                Case "BTNLEAVEVIEWER"
                    Dim Vfrm = New frmLeaveIssue_AddEdit
                    Vfrm.gbLeaveIssue.Visible = False
                    Vfrm.pnlLeaveIssueInfo.Visible = False
                    Vfrm.gbLeaveViewer.Visible = True
                    Vfrm.pnlLeaveViewer.Visible = True
                    Vfrm.gbLeaveViewer.BringToFront()
                    Vfrm.pnlLeaveViewer.BringToFront()
                    Vfrm.Text = Language.getMessage(mstrModuleName, 1, "Leave Viewer")
                    Vfrm.eZeeHeader.Title = Language.getMessage(mstrModuleName, 1, "Leave Viewer")
                    Vfrm.btnSave.Visible = False
                    If Vfrm.displayDialog(-1, True, enAction.ADD_ONE) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True
                Case "BTNLEAVESCHEDULAR"
                    frm = New frmLeaveSchedular
                Case "BTNLEAVEPLANNER"
                    frm = New frmleaveplannerList
                    'Pinkal (26-Mar-2011) -- Start

                Case "BTNIMPORTACCRUELEAVE"
                    frm = New frmImportAccrueLeave
                    'Pinkal (26-Mar-2011) -- End
                Case "BTNTRANSFERAPPROVER"
                    frm = New frmTransfer_Approver

                    'S.SANDEEP [ 15 FEB 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNENDELC"
                    frm = New frmEnd_LeaveCycle
                    'S.SANDEEP [ 15 FEB 2013 ] -- END

                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNLEAVEFREQUENCY"
                    'S.SANDEEP [ 15 OCT 2013 ] -- START
                    'ENHANCEMENT : ENHANCEMENT
                    'frm = New frmGlobalLvFreq_Assign
                    frm = New frmLeaveFrequencyList
                    'S.SANDEEP [ 15 OCT 2013 ] -- END

                    'S.SANDEEP [ 26 SEPT 2013 ] -- END



                    'Shani [ 24 DEC 2014 ] -- START
                    'ENHANCEMENT : CREATE NEW FROM LEAVE ADJUSTMENT
                Case "BTNLEAVEADJUSTMENT"
                    frm = New frmLeaveAdjustment
                    'Shani [ 24 DEC 2014 ] -- END

                    'Pinkal (19-Mar-2015) -- Start
                    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
                Case "BTNLVSWAPAPPROVER"
                    Dim Vfrm As New frmApprover_Swapping
                    If Vfrm.displayDialog(-1, enAction.ADD_ONE, enSwapApproverType.Leave) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True
                    'Pinkal (19-Mar-2015) -- End

                    'Pinkal (21-Oct-2016) -- Start
                    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
                Case "BTNBUDEMPTIMESHEET"
                    Dim Vfrm As New frmBudgetEmp_Timesheet
                    If Vfrm.displayDialog(-1, enAction.ADD_ONE) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    'Pinkal (21-Oct-2016) -- End

            End Select

            If blnViewer = False Then
                If frm IsNot Nothing Then
                    If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLeaveType_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " TnA "

    Private Sub btnShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click, btnTimesheet.Click, _
                                                                                                   btnHoldEmployee.Click, btnEmployeeAbsent.Click, _
                                                                                                   btnImportData.Click, btnExportData.Click, btnGroupAttendance.Click, btnImportAttendancedata.Click, _
                                                                                                   btnPolicyList.Click, btnEditTimeSheet.Click, btnRecalculateTiming.Click, btnDeviceUserMapping.Click, _
                                                                                                   btnRoundOff.Click, btnGlobalEditDelete.Click ' Pinkal (20-Sep-2013) [btnPolicyList.Click, btnEditTimeSheet.Click, btnRecalculateTiming.Click,btnRoundOff.Click ]
        ' Pinkal (15-Nov-2013) [ btnGlobalEditDelete.Click]  

        Dim frm As Form = Nothing
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlTnAInfo)

            'Pinkal (09-Nov-2013) -- Start
            'Enhancement : Oman Changes
            Dim blnViewer As Boolean = False
            'Pinkal (09-Nov-2013) -- End


            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                'Case "BTNSHIFT"
                '    frm = New frmShiftList
                'Case "BTNLOGIN"
                '    Dim frmLogin As New frmHRLogin
                '    frmLogin.Show()
                Case "BTNTIMESHEET"
                    frm = New frmTimesheet

                Case "BTNHOLDEMPLOYEE"
                    frm = New frmDeleteholdunhold_Employee
                    'Dim Hfrm As New frmHoldemployee
                    'Hfrm.btnUnhold.Visible = False
                    'Hfrm.chkPostTimesheet.Visible = False
                    'Hfrm.Text = Language.getMessage(mstrModuleName, 10, "Hold Employee")
                    'Hfrm.EZeeHeader1.Title = Language.getMessage(mstrModuleName, 2, "Hold Employee Information")
                    'Hfrm.EZeeCollapsibleContainer1.Text = Language.getMessage(mstrModuleName, 10, "Hold Employee")
                    'If Hfrm IsNot Nothing Then
                    '    If Hfrm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    'End If
                    'Case "BTNUNHOLDEMPLOYEE"
                    '    Dim Ufrm As New frmHoldemployee
                    '    Ufrm.btnHold.Visible = False
                    '    Ufrm.Text = "Unhold Employee"
                    '    Ufrm.EZeeHeader1.Title = "Unhold Employee Information"
                    '    Ufrm.EZeeCollapsibleContainer1.Text = "Unhold Employee"
                    '    If Ufrm IsNot Nothing Then
                    '        If Ufrm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    '    End If
                Case "BTNEMPLOYEEABSENT"
                    frm = New frmEmployeeAbsents
                Case "BTNIMPORTDATA"
                    frm = New frmImportdata
                Case "BTNEXPORTDATA"
                    frm = New frmExportdata

                    'Pinkal (16-Feb-2011) -- Start

                Case "BTNIMPORTATTENDANCEDATA"
                    frm = New frmImportAttendancedata

                Case "BTNGROUPATTENDANCE"
                    frm = New frmGroupattendance

                    'Pinkal (16-Feb-2011) -- End

                    'Pinkal (20-Sep-2013) -- Start
                    'Enhancement : TRA Changes

                Case "BTNPOLICYLIST"
                    frm = New frmPolicyList

                Case "BTNEDITTIMESHEET"
                    frm = New frmTimesheetDetail


                    'Pinkal (09-Nov-2013) -- Start
                    'Enhancement : Oman Changes

                Case "BTNRECALCULATETIMING"
                    Dim frmRecalculate As New frmRecalculateTimings
                    If frmRecalculate.displayDialog(enAction.ADD_ONE, True, CType(sender, eZee.Common.eZeeLightButton).Text) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True

                    'Pinkal (09-Nov-2013) -- End

                    'Pinkal (20-Sep-2013) -- End

                    'Sohail (04 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                Case btnDeviceUserMapping.Name.ToUpper
                    frm = New frmEmpIdDeviceMapping
                    'Sohail (04 Oct 2013) -- End


                    'Pinkal (09-Nov-2013) -- Start
                    'Enhancement : Oman Changes
                Case "BTNROUNDOFF"
                    Dim frmRoundoff As New frmRecalculateTimings
                    If frmRoundoff.displayDialog(enAction.ADD_ONE, False, CType(sender, eZee.Common.eZeeLightButton).Text) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True
                    'Pinkal (09-Nov-2013) -- End 


                    'Pinkal (15-Nov-2013) -- Start
                    'Enhancement : Oman Changes

                Case "BTNGLOBALEDITDELETE"
                    frm = New frmGlobalEditDelete_Timesheet

                    'Pinkal (15-Nov-2013) -- End

            End Select

            If blnViewer = False Then
                If frm IsNot Nothing Then
                    If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                End If
            End If

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnShift_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#End Region

#Region " Utilities "


    'Pinkal (5-Jul-2011) -- Start
    'ISSUE : ADD CHANGE PASSWORD FORM

    Private Sub btnReminder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReminder.Click, btnBackup.Click, btnRestore.Click, _
                                                                                                      btnLetterType.Click, btnLetterTemplate.Click, btnMailbox.Click, _
                                                                                                      btnChangeUser.Click, btnChangeCompany.Click, btnChangeDatabase.Click, _
                                                                                                      btnGeneralSettings.Click, btnCommonImportData.Click, btnCommonExportData.Click, btnUserLog.Click, btnChangePwd.Click, btnScanAttachments.Click, btnMapOrbitParams.Click, btnReportLanguage.Click 'S.SANDEEP |09-JAN-2020| -- START {btnMapOrbitParams}-- END
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlUtility)

            Dim frm As Form = Nothing

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNREMINDER"
                    frm = New frmReminderList
                    'Case "BTNBACKUP"
                    '    frm = New frmBackUp
                Case "BTNRESTORE"
                    frm = New frmRestore
                Case "BTNLETTERTYPE"
                    frm = New frmLetterType_AddEdit
                Case "BTNLETTERTEMPLATE"
                    Dim LTfrm As New frmLetterTemplate
                    LTfrm.displayDialog(False)
                Case "BTNMAILBOX"
                    'S.SANDEEP [ 25 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If mblnIsSendingMail = False Then
                        frm = New frmMailBox
                    End If
                    'S.SANDEEP [ 25 DEC 2011 ] -- END
                Case "BTNCHANGEUSER"
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    tmrReminder.Enabled = False

                    'S.SANDEEP [ 21 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.DESKTOP, , False)
                    'S.SANDEEP [ 21 MAY 2012 ] -- END

                    'tmrReminder.Stop()
                    'Sandeep [ 10 FEB 2011 ] -- End 

                    'Pinkal (27-Jun-2013) -- Start
                    'Enhancement : TRA Changes

                    If pnlMainInfo.Controls.Count > 0 Then
                        ObjfrmReportView.Dispose()
                        'Sohail (13 Jan 2020) -- Start
                        'NMB Issue # : Don't show employee dates counts if user does not have access.
                        frmSlidingDashBoard.Dispose()
                        'Sohail (13 Jan 2020) -- End
                    End If

                    'Pinkal (27-Jun-2013) -- End

                    mblnRestart = True
                    Me.Close()
                Case "BTNCHANGECOMPANY"
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    tmrReminder.Enabled = False
                    'tmrReminder.Stop()
                    'Sandeep [ 10 FEB 2011 ] -- End 
                    Dim dbOldDataBase As String = String.Empty
                    Dim intOldCompanyId As Integer = Company._Object._Companyunkid
                    dbOldDataBase = FinancialYear._Object._DatabaseName
                    eZeeCommonLib.eZeeDatabase.change_database("hrmsConfiguration")
                    'Sandeep | 17 JAN 2011 | -- START
                    gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Configuration)
                    'Sandeep | 17 JAN 2011 | -- END
                    Dim cfrm As New frmCommonSelection
                    Dim blnCompanyResult As Boolean = cfrm.displayDialog
                    cfrm = Nothing
                    If blnCompanyResult Then
                        Dim dfrm As New frmCommonSelection
                        dfrm._IsFinancial_Year = True
                        Dim blnDatabaseResult As Boolean = dfrm.displayDialog
                        dfrm = Nothing
                        If blnDatabaseResult Then
                            eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                            gobjLocalization = New clsLocalization(gApplicationType)
                            'Sandeep ( 18 JAN 2011 ) -- START
                            Call frmNewMDI_Load(sender, e)

                            'S.SANDEEP [ 06 June 2011 ] -- START
                            'ISSUE : DASHBOARD
                            Call SetDashBoard()
                            'S.SANDEEP [ 06 June 2011 ] -- END 

                            'S.SANDEEP [ 05 MARCH 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            ConfigParameter._Object.Refresh()
                            'S.SANDEEP [ 05 MARCH 2012 ] -- END

                            'Sandeep ( 18 JAN 2011 ) -- END 
                        Else
                            Company._Object._Companyunkid = intOldCompanyId
                            If dbOldDataBase.Length > 0 Then eZeeCommonLib.eZeeDatabase.change_database(dbOldDataBase)
                            gobjLocalization = New clsLocalization(gApplicationType)
                        End If
                    Else
                        Company._Object._Companyunkid = intOldCompanyId
                        If dbOldDataBase.Length > 0 Then eZeeCommonLib.eZeeDatabase.change_database(dbOldDataBase)
                    End If
                    objbtnPropertyName.Text = Company._Object._Name
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    If tmrReminder.Enabled = False Then
                        tmrReminder.Enabled = True
                        'tmrReminder.Start()
                    End If


                    'Pinkal (27-Jun-2013) -- Start
                    'Enhancement : TRA Changes

                    If pnlMainInfo.Controls.Count > 0 Then
                        ObjfrmReportView.Dispose()
                        If objAppSetting._LastViewAccessed = 0 Then
                            mnuPayrollView_Click(sender, e)
                        ElseIf objAppSetting._LastViewAccessed = 1 Then
                            mnuReportView_Click(sender, e)
                        ElseIf objAppSetting._LastViewAccessed = 2 Then
                            If User._Object.Privilege._AllowToAccessCustomReport = True Then
                                mnuCustomReporting_Click(sender, e)
                            Else
                                mnuPayrollView_Click(sender, e)
                            End If
                        End If
                    End If

                    'Pinkal (27-Jun-2013) -- End


                    'Sandeep [ 10 FEB 2011 ] -- End 
                Case "BTNCHANGEDATABASE"
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    tmrReminder.Enabled = False
                    'tmrReminder.Stop()
                    'Sandeep [ 10 FEB 2011 ] -- End 
                    Dim dbOldDataBase As String = String.Empty
                    dbOldDataBase = FinancialYear._Object._DatabaseName
                    eZeeCommonLib.eZeeDatabase.change_database("hrmsConfiguration")
                    'Sandeep | 17 JAN 2011 | -- START
                    gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Configuration)
                    'Sandeep | 17 JAN 2011 | -- END
                    Dim dfrm As New frmCommonSelection
                    dfrm._IsFinancial_Year = True
                    Dim blnDatabaseResult As Boolean = dfrm.displayDialog
                    dfrm = Nothing
                    If blnDatabaseResult Then
                        eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
                        gobjLocalization = New clsLocalization(gApplicationType)
                        'Sandeep ( 18 JAN 2011 ) -- START
                        Call frmNewMDI_Load(sender, e)

                        'S.SANDEEP [ 06 June 2011 ] -- START
                        'ISSUE : DASHBOARD
                        Call SetDashBoard()
                        'S.SANDEEP [ 06 June 2011 ] -- END 

                        'Sandeep ( 18 JAN 2011 ) -- END 
                    Else
                        If dbOldDataBase.Length > 0 Then eZeeCommonLib.eZeeDatabase.change_database(dbOldDataBase)
                        gobjLocalization = New clsLocalization(gApplicationType)
                    End If
                    objbtnYearName.Text = FinancialYear._Object._FinancialYear_Name
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    If tmrReminder.Enabled = False Then
                        tmrReminder.Enabled = True
                        'tmrReminder.Start()
                    End If
                    'Sandeep [ 10 FEB 2011 ] -- End 
                Case "BTNGENERALSETTINGS"
                    frm = New frmGeneralSettings
                Case "BTNCOMMONIMPORTDATA"
                    frm = New frmCommonImportData
                Case "BTNCOMMONEXPORTDATA"
                    frm = New frmCommonExportData

                    'Pinkal (23-Jun-2011) -- Start
                Case "BTNUSERLOG"
                    frm = New frmUserLogList
                    'Pinkal (23-Jun-2011) -- End

                Case "BTNCHANGEPWD"
                    Dim objFrm As New frmChangePassword
                    If objFrm IsNot Nothing Then
                        If objFrm.displayDialog(False) = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    End If
                    'S.SANDEEP [ 04 FEB 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNSCANATTACHMENTS"
                    frm = New frmScanAttachmentList
                    'S.SANDEEP [ 04 FEB 2012 ] -- END

                    'S.SANDEEP |09-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : ARUTI - ORBIT INTEGRATION
                Case btnMapOrbitParams.Name.ToUpper
                    frm = New frmOrbitDictionaryMapping
                    'S.SANDEEP |09-JAN-2020| -- END

                Case btnReportLanguage.Name.ToUpper
                    frm = New frmReportLanguage
            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            Else
                CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReminder_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (5-Jul-2011) -- End

#End Region

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Audit Trails "

    Private Sub btnApplicationEventLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplicationEventLog.Click, _
                                                                                                                 btnUserAttempts.Click, _
                                                                                                                 btnUserAuthenticationLog.Click, _
                                                                                                                 btnLogView.Click, btnEmailLogView.Click, btnRemovePPALogs.Click, btnSystemErrorLog.Click 'S.SANDEEP [ 28 JAN 2014 ] -- START -- END
        'Sohail (12 Apr 2019) - [btnSystemErrorLog]
        'S.SANDEEP [ 17 OCT 2014 ] -- START -- END
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlAuditTrails)
            Dim frm As Form = Nothing
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNAPPLICATIONEVENTLOG"
                    frm = New frmATLogView_New
                Case "BTNUSERATTEMPTS"
                    frm = New frmUserAttemptsLog
                Case "BTNUSERAUTHENTICATIONLOG"
                    frm = New frmUserLogList
                Case "BTNLOGVIEW"
                    frm = New frmAuditTrailViewer
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                Case "BTNEMAILLOGVIEW"
                    frm = New frmATEmail_LogView
                    'S.SANDEEP [ 28 JAN 2014 ] -- END

                    'S.SANDEEP [ 17 OCT 2014 ] -- START
                Case "BTNREMOVEPPALOGS"
                    frm = New frmPermanentlyDeletePPALogs
                    'S.SANDEEP [ 17 OCT 2014 ] -- END

                    'Sohail (12 Apr 2019) -- Start
                    'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
                Case btnSystemErrorLog.Name.ToUpper
                    Dim ofrm As New frmErrorLog
                    ofrm.displayDialog()
                    'Sohail (12 Apr 2019) -- End

            End Select
            If frm IsNot Nothing Then
                If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnApplicationEventLog_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 04 FEB 2014 ] -- START
#Region " Claims & Expenses "

    Private Sub btnExpensesList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExpensesList.Click, _
                                                                                                          btnCostingList.Click, _
                                                                                                          btnExpenseAssignment.Click, _
                                                                                                          btnExApprovalLevel.Click, _
                                                                                                          btnExApprovers.Click, _
                                                                                                          btnExApprMigration.Click, _
                                                                                                          btnClaims_Request.Click, _
                                                                                                          btnExpenseApproval.Click, _
                                                                                                          btnPostToPayroll.Click, _
                                                                                                          btnClaimSwapApprover.Click, _
                                                                                                          btnSecRouteAssignment.Click
        Try
            Call DoOperation(CType(sender, eZee.Common.eZeeLightButton).Name, fpnlClaimsExpenses)
            Dim frm As Form = Nothing

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            Dim blnViewer As Boolean = False
            'Pinkal (19-Mar-2015) -- End

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNEXPENSESLIST"
                    frm = New frmExpensesList
                Case "BTNCOSTINGLIST"
                    frm = New frmExpense_CostingList
                Case "BTNEXPENSEASSIGNMENT"
                    frm = New frmEmployeeExpensesList
                Case "BTNEXAPPROVALLEVEL"
                    frm = New frmExpenseApprLevelList
                Case "BTNEXAPPROVERS"
                    frm = New frmExApproverList
                Case "BTNEXAPPRMIGRATION"
                    frm = New frmExpApprMigration
                Case "BTNCLAIMS_REQUEST"
                    frm = New frmClaims_RequestList
                Case "BTNEXPENSEAPPROVAL"
                    frm = New frmExpenseApprovalList
                Case "BTNPOSTTOPAYROLL"
                    frm = New frmClaimPosting

                    'Pinkal (19-Mar-2015) -- Start
                    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
                Case "BTNCLAIMSWAPAPPROVER"
                    Dim Vfrm As New frmApprover_Swapping
                    If Vfrm.displayDialog(-1, enAction.ADD_ONE, enSwapApproverType.Claim_Request) = False Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                    blnViewer = True
                    'Pinkal (19-Mar-2015) -- End

                    'Pinkal (22-Mar-2016) -- Start
                    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                Case "BTNSECROUTEASSIGNMENT"
                    'Pinkal (20-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    If ConfigParameter._Object._SectorRouteAssignToEmp Then
                    frm = New frmEmployeeSectorList
                    ElseIf ConfigParameter._Object._SectorRouteAssignToExpense Then
                        frm = New frmExpenseSecMappingList
                    End If
                    'Pinkal (20-Feb-2019) -- End

                    'Pinkal (22-Mar-2016) -- End

            End Select

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

            If blnViewer = False Then
                If frm IsNot Nothing Then
                    If frm.ShowDialog = Windows.Forms.DialogResult.Cancel Then CType(sender, eZee.Common.eZeeLightButton).Selected = False
                End If
            End If

            'Pinkal (19-Mar-2015) -- End

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnExpensesList_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 04 FEB 2014 ] -- END
#End Region
    'S.SANDEEP [ 19 JULY 2012 ] -- END


#End Region

#Region " Private Function "

    Private Sub DoOperation(ByVal strName As String, ByVal fpnl As FlowLayoutPanel)
        For Each ctrl As Control In fpnl.Controls
            If TypeOf ctrl Is eZee.Common.eZeeLightButton Then
                If CStr(ctrl.Name).ToUpper = strName.ToUpper Then
                    CType(ctrl, eZee.Common.eZeeLightButton).Selected = True
                Else
                    CType(ctrl, eZee.Common.eZeeLightButton).Selected = False
                End If
            End If
        Next
    End Sub

    Private Sub SetControlVisibility(ByVal fpnl As FlowLayoutPanel)
        Try
            If pnlMenuItems.Controls.Count > 0 Then
                pnlMenuItems.Controls(0).Visible = False
                pnlMenuItems.Controls.RemoveAt(0)
            End If
            fpnl.Dock = DockStyle.Fill
            fpnl.Visible = True
            fpnl.BringToFront()
            pnlMenuItems.Controls.Add(fpnl)
            Call SetDyanamicSize(fpnl)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetControlVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDyanamicSize(ByVal fpnl As FlowLayoutPanel)
        Try
            Dim styles As TableLayoutColumnStyleCollection = Me.tblMDILayout.ColumnStyles
            For Each style As ColumnStyle In styles
                If style.SizeType = SizeType.Absolute Then
                    If fpnl.VerticalScroll.Visible = True Then
                        style.Width = 197
                    Else
                        style.Width = 180
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDyanamicSize", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility(ByVal blnValue As Boolean)
        Try
            mnuGeneralMaster.Enabled = blnValue
            mnuRecruitment.Enabled = blnValue
            mnuHumanResource.Enabled = blnValue
            mnuPayroll.Enabled = blnValue
            mnuLeaveAndTnA.Enabled = blnValue
            mnuUtilitiesMain.Enabled = blnValue
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuAuditTrail.Enabled = blnValue
            'S.SANDEEP [ 19 JULY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    Private Sub CheckModLic()
        Try

            'Anjan [ 05 Feb 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
                    mnuLeaveInformation.Visible = False
                End If
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) Then
                    mnuPayrollMasters.Visible = False
                    mnuPayrollTransactions.Visible = False
                    'S.SANDEEP [ 10 JUNE 2013 ] -- START
                    'ENHANCEMENT : OTHER CHANGES
                    mnuPayActivity.Visible = False
                    'S.SANDEEP [ 10 JUNE 2013 ] -- END
                End If
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) Then
                    mnuRecruitmentItem.Visible = False
                End If
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) Then
                    mnuAssetDeclaration.Visible = False
                End If
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) Then
                    mnuDiscipline.Visible = False
                End If
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) Then
                    mnuTrainingEvaluation.Visible = False
                    btnTrainingScheduling.Visible = False
                    btnTrainingEnrollment.Visible = False
                End If
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) Then
                    mnuMedical.Visible = False
                End If
                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) Then
                    mnuEmployeeData.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) Then
                    mnuBalancedScoreCard.Visible = False
                    mnuAssessment.Visible = False
                    mnuAppraisal.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) Then
                    mnuLoan_Advance_Savings.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Custom_Report_Engine) Then
                    mnuCustomReporting.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) Then
                    mnuTnAInfo.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) AndAlso Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
                    mnuLeaveAndTnA.Visible = False
                End If

                'Anjan [ 01 Aug 2017  ] -- Start
                'Issue : To open budget module from payroll module as its not license for time being until its place is fixed to make is visible.
                'If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) AndAlso Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) Then
                '    mnuPayroll.Visible = False
                'End If
                'Anjan [ 01 Aug 2017 ] -- End

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis) Then
                    btnTrainingPriority.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Electronic_Banking_Interface) Then
                    btnBankEDI.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Online_Job_Applications_Management) Then
                    btnExporttoWeb.Visible = False
                    btnImportfromWeb.Visible = False
                    btnAutoImportFromWeb.Visible = False
                End If

                If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis) AndAlso Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) Then
                    mnuTrainingInformation.Visible = False
                End If
            End If

            mnuGeneralMaster.Visible = True
            mnuHRMaster.Visible = True
            'Anjan [ 05 Feb 2013 ] -- End






        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckModLic", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 06 June 2011 ] -- START
    'ISSUE : DASHBOARD
    Private Sub SetDashBoard()
        Try
            tmrReminder.Enabled = False
            Dim frm As New frmSlidingDashBoard

            'If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.LeaveTnA) Then
            '    frm.tabcDashBoard.TabPages.Remove(frm.tabpLeaveTnA)
            'End If
            'If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll) Then
            '    frm.tabcDashBoard.TabPages.Remove(frm.tabpPayroll)
            'End If
            'If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.AdvancedHRTraining) Then
            '    frm.tabcDashBoard.TabPages.Remove(frm.tabpHR)
            'End If


            If pnlSummary.Contains(frm) Then pnlSummary.Controls.Remove(frm)

            frm.TopLevel = False
            frm.Dock = DockStyle.Fill
            pnlSummary.Controls.Add(frm)
            frm.Show()

            frm.BringToFront()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDashBoard", mstrModuleName)
        Finally
            If tmrReminder.Enabled = False Then tmrReminder.Enabled = True
        End Try
    End Sub
    'S.SANDEEP [ 06 June 2011 ] -- END 

#End Region

#Region " Menu Events "
    'Private Sub mnuPayroll_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPayroll.MouseHover

    '    Try
    '        mnuPayroll.ShowDropDown()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuPayroll_MouseHover", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub mnuHR() Handles mnuHumanResource.MouseHover

    '    Try
    '        mnuHumanResource.ShowDropDown()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuHR", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub mnuLeaveTnA() Handles mnuLeaveAndTnA.MouseHover

    '    Try
    '        mnuLeaveAndTnA.ShowDropDown()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuLeaveTnA", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub mnuViewdropdown() Handles mnuView.MouseHover

    '    Try
    '        mnuView.ShowDropDown()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try

    'End Sub
#End Region

    'Sandeep [ 10 FEB 2011 ] -- Start
#Region " Controls "

    Public Sub tmrReminder_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrReminder.Tick
        Dim objReminder As New clsreminder_master
        Dim dtMsg As DataTable
        Try

            'Sohail (10 Dec 2019) -- Start
            'MAIN SPRING RESOURCES Support Issue Id # 0004311 : Add employee code, cost centre and class group columns in email notification.
            If gobjEmailList IsNot Nothing AndAlso gobjEmailList.Count > 0 Then
                Exit Sub
            End If
            'Sohail (10 Dec 2019) -- End

            'Pinkal (07-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            Dim objconfig As New clsConfigOptions
            Dim mblnLockUserOnSuspension As Boolean = True
            Dim mstrLockUserOnSuspension As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "LockUserOnSuspension", Nothing)
            objconfig = Nothing

            If mstrLockUserOnSuspension.Trim.Length > 0 Then
                mblnLockUserOnSuspension = CBool(mstrLockUserOnSuspension)
            End If

            Dim objUserTracing As New clsUser_tracking_Tran
            Dim objEmpDates As New clsemployee_dates_tran
            Dim minttrackingunkid As Integer = 0
            Dim mblnSuspended As Boolean = False
            Dim dsList As DataSet = Nothing

            If mblnLockUserOnSuspension Then

                dsList = objEmpDates.Get_Current_Dates(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                          , enEmp_Dates_Transaction.DT_SUSPENSION, 0, Nothing, "'" & ConfigParameter._Object._EmployeeAsOnDate & "' BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112)")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    minttrackingunkid = objUserTracing.isUserLoginExist(User._Object._Userunkid, getIP(), enLogin_Mode.DESKTOP, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    objUserTracing._Trackingunkid = minttrackingunkid

                    Dim mintCount As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = User._Object._EmployeeUnkid).Count

                    If mintCount > 0 Then
                        If objUserTracing._Isforcelogout = False Then
                            objUserTracing._Isforcelogout = True
                            objUserTracing._Logout_Reason = "Due to Suspension on real time."
                            If objUserTracing.Update() = False Then
                                eZeeMsgBox.Show(objUserTracing._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                mblnSuspended = True
                            End If 'If objUserTracing.Update() = False Then

                        End If 'If objUserTracing._Isforcelogout = False Then

                    End If 'If mintCount > 0 Then

                End If 'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

            End If  'If mblnLockUserOnSuspension Then

            objEmpDates = Nothing

            If minttrackingunkid <= 0 Then minttrackingunkid = objUserTracing.GetTrackTranUnkId(User._Object._Userunkid, getIP(), enLogin_Mode.DESKTOP, False)

            If minttrackingunkid > 0 Then
                objUserTracing._Trackingunkid = minttrackingunkid
                If objUserTracing._Isforcelogout = True Then
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.DESKTOP, , True)

                    'Pinkal (07-Jun-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    If mblnSuspended Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You are forcefully logged out. Reason : You are in suspension period. Please contact Administrator."), enMsgBoxStyle.Information)
                    Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 123, "Sorry! You are forcefully logged out. Please Re-Login later."), enMsgBoxStyle.Information)
                    End If
                    'Pinkal (07-Jun-2019) -- End

                    blnIsIdealTimeSet = True
                    Application.Exit()
                End If
            End If

            'Pinkal (07-Jun-2019) -- End

            tmrReminder.Enabled = False
            dtMsg = objReminder.getTodayReminder(User._Object._Userunkid).Tables(0)
            For Each dtRow As DataRow In dtMsg.Rows
                Dim objFrm As New frmReminderDisplay
                Try
                    If User._Object._Isrighttoleft = True Then
                        objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        objFrm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(objFrm)
                    End If

                    objFrm.displayDialog(CInt(dtRow.Item("reminderunkid")))

                Catch ex As Exception
                    Call DisplayError.Show("-1", ex.Message, "tmrReminder_Tick", mstrModuleName)
                Finally
                    objFrm.Dispose()
                    objFrm = Nothing
                End Try
            Next

            'ConfigParameter._Object.Refresh()
            'If ConfigParameter._Object._IsDailyBackupEnabled = True Then
            '    If ConfigParameter._Object._CurrentDateAndTime.ToLongTimeString = ConfigParameter._Object._BackupTime.ToLongTimeString Then
            '        RemoveHandler tmrReminder.Tick, AddressOf tmrReminder_Tick
            '        Dim frm As New frmDailyBackup
            '        frm.ShowDialog()
            '        AddHandler tmrReminder.Tick, AddressOf tmrReminder_Tick
            '    End If
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tmrReminder_Tick", mstrModuleName)
        Finally
            'eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)
            objReminder = Nothing
            dtMsg = Nothing
            tmrReminder.Enabled = True
        End Try
    End Sub

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dtpEmployeeAsOnDate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpEmployeeAsOnDate.LostFocus
        Try
            ConfigParameter._Object._EmployeeAsOnDate = eZeeDate.convertDate(dtpEmployeeAsOnDate.Value.Date)
            ConfigParameter._Object.updateParam()
            ConfigParameter._Object.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpEmployeeAsOnDate_LostFocus", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Jan 2012) -- End

#End Region
    'Sandeep [ 10 FEB 2011 ] -- End 



    'Pinkal (21-Jun-2011) -- Start

#Region "Private Event"

    Public Sub Application_Idle(ByVal e As clsApplicationIdleTimer.ApplicationIdleEventArgs)
        Try
            If User._Object.PWDOptions._IsApplication_Idle Then
                If e.IdleDuration.Minutes >= User._Object.PWDOptions._Idletime Then
                    If clsApplicationIdleTimer.LastIdealTime.Enabled = True Then
                        clsApplicationIdleTimer.LastIdealTime.Stop()
                        clsApplicationIdleTimer.LastIdealTime.Enabled = False

                        Dim objDataOperation As New clsDataOperation
                        objDataOperation.ExecNonQuery("ALTER DATABASE " & FinancialYear._Object._DatabaseName & " SET MULTI_USER")
                        'Pinkal (12-Oct-2011) -- Start

                        'S.SANDEEP [14-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {#2252|#ARUTI-162}
                        'Dim obj As New FrmMessageBox
                        'obj.LblMessage.Text = Language.getMessage(mstrModuleName, 4, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok.")
                        'MsgBox(Language.getMessage(mstrModuleName, 8, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok."), MsgBoxStyle.Information)
                        ''If CBool(obj.ShowDialog(Me)) Then
                        ''    blnIsIdealTimeSet = True
                        ''    Application.Restart()
                        ''End If
                        'blnIsIdealTimeSet = True
                        'Application.Restart()
                        Me.Enabled = False
                        Dim obj As New FrmMessageBox
                        obj.LblMessage.Text = Language.getMessage(mstrModuleName, 8, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok.")
                        obj.ShowDialog()
                        blnIsIdealTimeSet = True
                        Application.Restart()
                        'S.SANDEEP [14-May-2018] -- END

                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You are logged out of the system. Reason : Application was idle for more time than set in configuration. In order to relogin into system Press Ok."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly) Then
                        '    Application.Restart()
                        'End If

                        'Pinkal (12-Oct-2011) -- End

                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Application_Idle", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (21-Jun-2011) -- End


    Private Sub mnuArutiHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuArutiHelp.Click
        Try
            Help.ShowHelp(Me, "help.chm", HelpNavigator.Topic)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuArutiHelp_Click", mstrModuleName)
        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()
			
			Me.btnBranch.GradientBackColor = GUI._ButttonBackColor 
            Me.btnBranch.GradientForeColor = GUI._ButttonFontColor

			Me.btnDepartmentGroup.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDepartmentGroup.GradientForeColor = GUI._ButttonFontColor

			Me.btnDepartment.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDepartment.GradientForeColor = GUI._ButttonFontColor

			Me.btnSection.GradientBackColor = GUI._ButttonBackColor 
            Me.btnSection.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnUnit.GradientForeColor = GUI._ButttonFontColor

            Me.btnJobGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnJobGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnJob.GradientBackColor = GUI._ButttonBackColor
            Me.btnJob.GradientForeColor = GUI._ButttonFontColor

            Me.btnClassGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnClassGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnClasses.GradientBackColor = GUI._ButttonBackColor
            Me.btnClasses.GradientForeColor = GUI._ButttonFontColor

            Me.btnGradeGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnGradeGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnGrade.GradientBackColor = GUI._ButttonBackColor
            Me.btnGrade.GradientForeColor = GUI._ButttonFontColor

            Me.btnGradeLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnGradeLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnQualification.GradientBackColor = GUI._ButttonBackColor
            Me.btnQualification.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdvertiseMaster.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdvertiseMaster.GradientForeColor = GUI._ButttonFontColor

            Me.btnSkillMaster.GradientBackColor = GUI._ButttonBackColor
            Me.btnSkillMaster.GradientForeColor = GUI._ButttonFontColor

            Me.btnReasons.GradientBackColor = GUI._ButttonBackColor
            Me.btnReasons.GradientForeColor = GUI._ButttonFontColor

            Me.btnBenefitPlan.GradientBackColor = GUI._ButttonBackColor
            Me.btnBenefitPlan.GradientForeColor = GUI._ButttonFontColor

            Me.btnResultCode.GradientBackColor = GUI._ButttonBackColor
            Me.btnResultCode.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeData2.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeData2.GradientForeColor = GUI._ButttonFontColor

            Me.btnDependants2.GradientBackColor = GUI._ButttonBackColor
            Me.btnDependants2.GradientForeColor = GUI._ButttonFontColor

            Me.btnBenefitAllocation.GradientBackColor = GUI._ButttonBackColor
            Me.btnBenefitAllocation.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeBenefit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeBenefit.GradientForeColor = GUI._ButttonFontColor

            Me.btnGroupBenefit.GradientBackColor = GUI._ButttonBackColor
            Me.btnGroupBenefit.GradientForeColor = GUI._ButttonFontColor

            Me.btnReferee.GradientBackColor = GUI._ButttonBackColor
            Me.btnReferee.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeAssets.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeAssets.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeSkill.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeSkill.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeQualification.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeQualification.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeExperence.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeExperence.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisciplineType.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisciplineType.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisciplinaryAction.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisciplinaryAction.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisciplineStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisciplineStatus.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisciplineFiling.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisciplineFiling.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessmentPeriod.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessmentPeriod.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessItems.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessItems.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessor.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessor.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessorAccess.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessorAccess.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeAssessment.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeAssessment.GradientForeColor = GUI._ButttonFontColor

            Me.btnTrainingInstitute.GradientBackColor = GUI._ButttonBackColor
            Me.btnTrainingInstitute.GradientForeColor = GUI._ButttonFontColor

            Me.btnTrainingScheduling.GradientBackColor = GUI._ButttonBackColor
            Me.btnTrainingScheduling.GradientForeColor = GUI._ButttonFontColor

            Me.btnTrainingEnrollment.GradientBackColor = GUI._ButttonBackColor
            Me.btnTrainingEnrollment.GradientForeColor = GUI._ButttonFontColor

            Me.btnTrainingAttendance.GradientBackColor = GUI._ButttonBackColor
            Me.btnTrainingAttendance.GradientForeColor = GUI._ButttonFontColor

            Me.btnTrainingAnalysis.GradientBackColor = GUI._ButttonBackColor
            Me.btnTrainingAnalysis.GradientForeColor = GUI._ButttonFontColor

            Me.btnMedicalMasters.GradientBackColor = GUI._ButttonBackColor
            Me.btnMedicalMasters.GradientForeColor = GUI._ButttonFontColor

            Me.btnMedicalInstitute.GradientBackColor = GUI._ButttonBackColor
            Me.btnMedicalInstitute.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeInjuries.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeInjuries.GradientForeColor = GUI._ButttonFontColor

            Me.btnCategoryAssignment.GradientBackColor = GUI._ButttonBackColor
            Me.btnCategoryAssignment.GradientForeColor = GUI._ButttonFontColor

            Me.btnCoverAssignment.GradientBackColor = GUI._ButttonBackColor
            Me.btnCoverAssignment.GradientForeColor = GUI._ButttonFontColor

            Me.btnMedicalClaim.GradientBackColor = GUI._ButttonBackColor
            Me.btnMedicalClaim.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayrollGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayrollGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayrollPeriods.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayrollPeriods.GradientForeColor = GUI._ButttonFontColor

            Me.btnCostcenter.GradientBackColor = GUI._ButttonBackColor
            Me.btnCostcenter.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayPoint.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayPoint.GradientForeColor = GUI._ButttonFontColor

            Me.btnBankBranch.GradientBackColor = GUI._ButttonBackColor
            Me.btnBankBranch.GradientForeColor = GUI._ButttonFontColor

            Me.btnBankAccountType.GradientBackColor = GUI._ButttonBackColor
            Me.btnBankAccountType.GradientForeColor = GUI._ButttonFontColor

            Me.btnBankEDI.GradientBackColor = GUI._ButttonBackColor
            Me.btnBankEDI.GradientForeColor = GUI._ButttonFontColor

            Me.btnCurrency.GradientBackColor = GUI._ButttonBackColor
            Me.btnCurrency.GradientForeColor = GUI._ButttonFontColor

            Me.btnDemomination.GradientBackColor = GUI._ButttonBackColor
            Me.btnDemomination.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeCostCenter.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeCostCenter.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeBanks.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeBanks.GradientForeColor = GUI._ButttonFontColor

            Me.btnBatchTransaction.GradientBackColor = GUI._ButttonBackColor
            Me.btnBatchTransaction.GradientForeColor = GUI._ButttonFontColor

            Me.btnTransactionHeads.GradientBackColor = GUI._ButttonBackColor
            Me.btnTransactionHeads.GradientForeColor = GUI._ButttonFontColor

            Me.btnEarningDeduction.GradientBackColor = GUI._ButttonBackColor
            Me.btnEarningDeduction.GradientForeColor = GUI._ButttonFontColor

            Me.btnProcessPayroll.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcessPayroll.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayslip.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayslip.GradientForeColor = GUI._ButttonFontColor

            Me.btnBudgetPreparation.GradientBackColor = GUI._ButttonBackColor
            Me.btnBudgetPreparation.GradientForeColor = GUI._ButttonFontColor

            Me.btnSalaryIncrement.GradientBackColor = GUI._ButttonBackColor
            Me.btnSalaryIncrement.GradientForeColor = GUI._ButttonFontColor

            Me.btnWagesTable.GradientBackColor = GUI._ButttonBackColor
            Me.btnWagesTable.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeExemption.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeExemption.GradientForeColor = GUI._ButttonFontColor

            Me.btnGlobalAssignED.GradientBackColor = GUI._ButttonBackColor
            Me.btnGlobalAssignED.GradientForeColor = GUI._ButttonFontColor

            Me.btnBatchEntry.GradientBackColor = GUI._ButttonBackColor
            Me.btnBatchEntry.GradientForeColor = GUI._ButttonFontColor

            Me.btnClosePayroll.GradientBackColor = GUI._ButttonBackColor
            Me.btnClosePayroll.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveType.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveType.GradientForeColor = GUI._ButttonFontColor

            Me.btnHolidays.GradientBackColor = GUI._ButttonBackColor
            Me.btnHolidays.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeHolidays.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeHolidays.GradientForeColor = GUI._ButttonFontColor

            Me.btnApproverLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnApproverLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveApprover.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveApprover.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveForm.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveForm.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveAccrue.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveAccrue.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveViewer.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveViewer.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveSchedular.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveSchedular.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeavePlanner.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeavePlanner.GradientForeColor = GUI._ButttonFontColor

            Me.btnShift.GradientBackColor = GUI._ButttonBackColor
            Me.btnShift.GradientForeColor = GUI._ButttonFontColor

            Me.btnLogin.GradientBackColor = GUI._ButttonBackColor
            Me.btnLogin.GradientForeColor = GUI._ButttonFontColor

            Me.btnTimesheet.GradientBackColor = GUI._ButttonBackColor
            Me.btnTimesheet.GradientForeColor = GUI._ButttonFontColor

            Me.btnHoldEmployee.GradientBackColor = GUI._ButttonBackColor
            Me.btnHoldEmployee.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeAbsent.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeAbsent.GradientForeColor = GUI._ButttonFontColor

            Me.btnImportData.GradientBackColor = GUI._ButttonBackColor
            Me.btnImportData.GradientForeColor = GUI._ButttonFontColor

            Me.btnExportData.GradientBackColor = GUI._ButttonBackColor
            Me.btnExportData.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoanScheme.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoanScheme.GradientForeColor = GUI._ButttonFontColor

            Me.btnApplicantMaster.GradientBackColor = GUI._ButttonBackColor
            Me.btnApplicantMaster.GradientForeColor = GUI._ButttonFontColor

            Me.btnVacancy.GradientBackColor = GUI._ButttonBackColor
            Me.btnVacancy.GradientForeColor = GUI._ButttonFontColor

            Me.btnBatchScheduling.GradientBackColor = GUI._ButttonBackColor
            Me.btnBatchScheduling.GradientForeColor = GUI._ButttonFontColor

            Me.btnInterviewScheduling.GradientBackColor = GUI._ButttonBackColor
            Me.btnInterviewScheduling.GradientForeColor = GUI._ButttonFontColor

            Me.btnInterviewAnalysis.GradientBackColor = GUI._ButttonBackColor
            Me.btnInterviewAnalysis.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalApplicants.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalApplicants.GradientForeColor = GUI._ButttonFontColor

            Me.btnLetterType.GradientBackColor = GUI._ButttonBackColor
            Me.btnLetterType.GradientForeColor = GUI._ButttonFontColor

            Me.btnLetterTemplate.GradientBackColor = GUI._ButttonBackColor
            Me.btnLetterTemplate.GradientForeColor = GUI._ButttonFontColor

            Me.btnMailbox.GradientBackColor = GUI._ButttonBackColor
            Me.btnMailbox.GradientForeColor = GUI._ButttonFontColor

            Me.btnReminder.GradientBackColor = GUI._ButttonBackColor
            Me.btnReminder.GradientForeColor = GUI._ButttonFontColor

            Me.btnBackup.GradientBackColor = GUI._ButttonBackColor
            Me.btnBackup.GradientForeColor = GUI._ButttonFontColor

            Me.btnRestore.GradientBackColor = GUI._ButttonBackColor
            Me.btnRestore.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeUser.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeUser.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeCompany.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeCompany.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeDatabase.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeDatabase.GradientForeColor = GUI._ButttonFontColor

            Me.btnGeneralSettings.GradientBackColor = GUI._ButttonBackColor
            Me.btnGeneralSettings.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayslipMessage.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayslipMessage.GradientForeColor = GUI._ButttonFontColor

            Me.btnGlobalPayslipMessage.GradientBackColor = GUI._ButttonBackColor
            Me.btnGlobalPayslipMessage.GradientForeColor = GUI._ButttonFontColor

            Me.btnCompanyJV.GradientBackColor = GUI._ButttonBackColor
            Me.btnCompanyJV.GradientForeColor = GUI._ButttonFontColor

            Me.btnAccount.GradientBackColor = GUI._ButttonBackColor
            Me.btnAccount.GradientForeColor = GUI._ButttonFontColor

            Me.btnAccountConfiguration.GradientBackColor = GUI._ButttonBackColor
            Me.btnAccountConfiguration.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeMovement2.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeMovement2.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeDiary.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeDiary.GradientForeColor = GUI._ButttonFontColor

            Me.btnCashDenomination.GradientBackColor = GUI._ButttonBackColor
            Me.btnCashDenomination.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessorAssessmentlist.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessorAssessmentlist.GradientForeColor = GUI._ButttonFontColor

            Me.btnCommonImportData.GradientBackColor = GUI._ButttonBackColor
            Me.btnCommonImportData.GradientForeColor = GUI._ButttonFontColor

            Me.btnCommonExportData.GradientBackColor = GUI._ButttonBackColor
            Me.btnCommonExportData.GradientForeColor = GUI._ButttonFontColor

            Me.btnCommonMaster.GradientBackColor = GUI._ButttonBackColor
            Me.btnCommonMaster.GradientForeColor = GUI._ButttonFontColor

            Me.btnMemebership.GradientBackColor = GUI._ButttonBackColor
            Me.btnMemebership.GradientForeColor = GUI._ButttonFontColor

            Me.btnState.GradientBackColor = GUI._ButttonBackColor
            Me.btnState.GradientForeColor = GUI._ButttonFontColor

            Me.btnCity.GradientBackColor = GUI._ButttonBackColor
            Me.btnCity.GradientForeColor = GUI._ButttonFontColor

            Me.btnZipcode.GradientBackColor = GUI._ButttonBackColor
            Me.btnZipcode.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeData.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeData.GradientForeColor = GUI._ButttonFontColor

            Me.btnDependants.GradientBackColor = GUI._ButttonBackColor
            Me.btnDependants.GradientForeColor = GUI._ButttonFontColor

            Me.btnGarnishees.GradientBackColor = GUI._ButttonBackColor
            Me.btnGarnishees.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeMovement.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeMovement.GradientForeColor = GUI._ButttonFontColor

            Me.btnImportAttendancedata.GradientBackColor = GUI._ButttonBackColor
            Me.btnImportAttendancedata.GradientForeColor = GUI._ButttonFontColor

            Me.btnGroupAttendance.GradientBackColor = GUI._ButttonBackColor
            Me.btnGroupAttendance.GradientForeColor = GUI._ButttonFontColor

            Me.btnImportAccrueLeave.GradientBackColor = GUI._ButttonBackColor
            Me.btnImportAccrueLeave.GradientForeColor = GUI._ButttonFontColor

            Me.btnUserLog.GradientBackColor = GUI._ButttonBackColor
            Me.btnUserLog.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangePwd.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangePwd.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmpAccConfig.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmpAccConfig.GradientForeColor = GUI._ButttonFontColor

            Me.btnCCAccConfig.GradientBackColor = GUI._ButttonBackColor
            Me.btnCCAccConfig.GradientForeColor = GUI._ButttonFontColor

            Me.btnSectionGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnSectionGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnUnitGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnUnitGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnTeam.GradientBackColor = GUI._ButttonBackColor
            Me.btnTeam.GradientForeColor = GUI._ButttonFontColor

            Me.btnExporttoWeb.GradientBackColor = GUI._ButttonBackColor
            Me.btnExporttoWeb.GradientForeColor = GUI._ButttonFontColor

            Me.btnImportfromWeb.GradientBackColor = GUI._ButttonBackColor
            Me.btnImportfromWeb.GradientForeColor = GUI._ButttonFontColor

            Me.btnApplicantFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnApplicantFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalShortListedApplicant.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalShortListedApplicant.GradientForeColor = GUI._ButttonFontColor

            Me.btnResolutionSteps.GradientBackColor = GUI._ButttonBackColor
            Me.btnResolutionSteps.GradientForeColor = GUI._ButttonFontColor

            Me.btnSickSheet.GradientBackColor = GUI._ButttonBackColor
            Me.btnSickSheet.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessSubItem.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessSubItem.GradientForeColor = GUI._ButttonFontColor

            Me.btnObjectiveList.GradientBackColor = GUI._ButttonBackColor
            Me.btnObjectiveList.GradientForeColor = GUI._ButttonFontColor

            Me.btnKPIMeasureList.GradientBackColor = GUI._ButttonBackColor
            Me.btnKPIMeasureList.GradientForeColor = GUI._ButttonFontColor

            Me.btnTargetsList.GradientBackColor = GUI._ButttonBackColor
            Me.btnTargetsList.GradientForeColor = GUI._ButttonFontColor

            Me.btnInitiativeList.GradientBackColor = GUI._ButttonBackColor
            Me.btnInitiativeList.GradientForeColor = GUI._ButttonFontColor

            Me.btnReviewer.GradientBackColor = GUI._ButttonBackColor
            Me.btnReviewer.GradientForeColor = GUI._ButttonFontColor

            Me.btnReviewerAssessment.GradientBackColor = GUI._ButttonBackColor
            Me.btnReviewerAssessment.GradientForeColor = GUI._ButttonFontColor

            Me.btnSelfAssessedBSC.GradientBackColor = GUI._ButttonBackColor
            Me.btnSelfAssessedBSC.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessorBSCList.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessorBSCList.GradientForeColor = GUI._ButttonFontColor

            Me.btnReviewerBSCList.GradientBackColor = GUI._ButttonBackColor
            Me.btnReviewerBSCList.GradientForeColor = GUI._ButttonFontColor

            Me.btnExternalAssessor.GradientBackColor = GUI._ButttonBackColor
            Me.btnExternalAssessor.GradientForeColor = GUI._ButttonFontColor

            Me.btnShortListEmployee.GradientBackColor = GUI._ButttonBackColor
            Me.btnShortListEmployee.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalShortListedEmployee.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalShortListedEmployee.GradientForeColor = GUI._ButttonFontColor

            Me.btnTrainingPriority.GradientBackColor = GUI._ButttonBackColor
            Me.btnTrainingPriority.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalizingTrainingPriority.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalizingTrainingPriority.GradientForeColor = GUI._ButttonFontColor

            Me.btnScanAttachments.GradientBackColor = GUI._ButttonBackColor
            Me.btnScanAttachments.GradientForeColor = GUI._ButttonFontColor

            Me.btnFeedbackGroup.GradientBackColor = GUI._ButttonBackColor
            Me.btnFeedbackGroup.GradientForeColor = GUI._ButttonFontColor

            Me.btnFeedbackItems.GradientBackColor = GUI._ButttonBackColor
            Me.btnFeedbackItems.GradientForeColor = GUI._ButttonFontColor

            Me.btnFeedbackSubItems.GradientBackColor = GUI._ButttonBackColor
            Me.btnFeedbackSubItems.GradientForeColor = GUI._ButttonFontColor

            Me.btnFeedback.GradientBackColor = GUI._ButttonBackColor
            Me.btnFeedback.GradientForeColor = GUI._ButttonFontColor

            Me.btnEvaluationLevel3.GradientBackColor = GUI._ButttonBackColor
            Me.btnEvaluationLevel3.GradientForeColor = GUI._ButttonFontColor

            Me.btnImpactItems.GradientBackColor = GUI._ButttonBackColor
            Me.btnImpactItems.GradientForeColor = GUI._ButttonFontColor

            Me.btnCommittee.GradientBackColor = GUI._ButttonBackColor
            Me.btnCommittee.GradientForeColor = GUI._ButttonFontColor

            Me.btnMigration.GradientBackColor = GUI._ButttonBackColor
            Me.btnMigration.GradientForeColor = GUI._ButttonFontColor

            Me.BtnTransferApprover.GradientBackColor = GUI._ButttonBackColor
            Me.BtnTransferApprover.GradientForeColor = GUI._ButttonFontColor

            Me.btnApplicationEventLog.GradientBackColor = GUI._ButttonBackColor
            Me.btnApplicationEventLog.GradientForeColor = GUI._ButttonFontColor

            Me.btnUserAttempts.GradientBackColor = GUI._ButttonBackColor
            Me.btnUserAttempts.GradientForeColor = GUI._ButttonFontColor

            Me.btnUserAuthenticationLog.GradientBackColor = GUI._ButttonBackColor
            Me.btnUserAuthenticationLog.GradientForeColor = GUI._ButttonFontColor

            Me.btnLogView.GradientBackColor = GUI._ButttonBackColor
            Me.btnLogView.GradientForeColor = GUI._ButttonFontColor

            Me.btnAllocationMapping.GradientBackColor = GUI._ButttonBackColor
            Me.btnAllocationMapping.GradientForeColor = GUI._ButttonFontColor

            Me.btnAutoImportFromWeb.GradientBackColor = GUI._ButttonBackColor
            Me.btnAutoImportFromWeb.GradientForeColor = GUI._ButttonFontColor

            Me.btnBatchPosting.GradientBackColor = GUI._ButttonBackColor
            Me.btnBatchPosting.GradientForeColor = GUI._ButttonFontColor

            Me.btnWeightedSetting.GradientBackColor = GUI._ButttonBackColor
            Me.btnWeightedSetting.GradientForeColor = GUI._ButttonFontColor

            Me.btnCloseAssessmentPeriod.GradientBackColor = GUI._ButttonBackColor
            Me.btnCloseAssessmentPeriod.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayAppLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayAppLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayApproverMap.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayApproverMap.GradientForeColor = GUI._ButttonFontColor

            Me.btnEndELC.GradientBackColor = GUI._ButttonBackColor
            Me.btnEndELC.GradientForeColor = GUI._ButttonFontColor

            Me.btnDependantException.GradientBackColor = GUI._ButttonBackColor
            Me.btnDependantException.GradientForeColor = GUI._ButttonFontColor

            Me.btnActivity_List.GradientBackColor = GUI._ButttonBackColor
            Me.btnActivity_List.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayPerActivity.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayPerActivity.GradientForeColor = GUI._ButttonFontColor

            Me.btnMeasureList.GradientBackColor = GUI._ButttonBackColor
            Me.btnMeasureList.GradientForeColor = GUI._ButttonFontColor

            Me.btnActivityRate.GradientBackColor = GUI._ButttonBackColor
            Me.btnActivityRate.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayrollPosting.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayrollPosting.GradientForeColor = GUI._ButttonFontColor

            Me.btnActivityGlobalAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnActivityGlobalAssign.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessmentRatio.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessmentRatio.GradientForeColor = GUI._ButttonFontColor

            Me.btnAppraisalSetups.GradientBackColor = GUI._ButttonBackColor
            Me.btnAppraisalSetups.GradientForeColor = GUI._ButttonFontColor

            Me.btnPolicyList.GradientBackColor = GUI._ButttonBackColor
            Me.btnPolicyList.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditTimeSheet.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditTimeSheet.GradientForeColor = GUI._ButttonFontColor

            Me.btnRecalculateTiming.GradientBackColor = GUI._ButttonBackColor
            Me.btnRecalculateTiming.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveFrequency.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveFrequency.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeviceUserMapping.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeviceUserMapping.GradientForeColor = GUI._ButttonFontColor

            Me.btnRoundOff.GradientBackColor = GUI._ButttonBackColor
            Me.btnRoundOff.GradientForeColor = GUI._ButttonFontColor

            Me.btnGlobalEditDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnGlobalEditDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmailLogView.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmailLogView.GradientForeColor = GUI._ButttonFontColor

            Me.btnStaffReqAppLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnStaffReqAppLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnTransferStaffRequisitionApprover.GradientBackColor = GUI._ButttonBackColor
            Me.btnTransferStaffRequisitionApprover.GradientForeColor = GUI._ButttonFontColor

			Me.btnStaffReqAppMapping.GradientBackColor = GUI._ButttonBackColor 
            Me.btnStaffReqAppMapping.GradientForeColor = GUI._ButttonFontColor

			Me.btnStaffRequisition.GradientBackColor = GUI._ButttonBackColor 
            Me.btnStaffRequisition.GradientForeColor = GUI._ButttonFontColor

			Me.btnRemovePPALogs.GradientBackColor = GUI._ButttonBackColor 
            Me.btnRemovePPALogs.GradientForeColor = GUI._ButttonFontColor

            Me.btnExpensesList.GradientBackColor = GUI._ButttonBackColor
            Me.btnExpensesList.GradientForeColor = GUI._ButttonFontColor

            Me.btnCostingList.GradientBackColor = GUI._ButttonBackColor
            Me.btnCostingList.GradientForeColor = GUI._ButttonFontColor

            Me.btnExpenseAssignment.GradientBackColor = GUI._ButttonBackColor
            Me.btnExpenseAssignment.GradientForeColor = GUI._ButttonFontColor

            Me.btnExApprovalLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnExApprovalLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnExApprovers.GradientBackColor = GUI._ButttonBackColor
            Me.btnExApprovers.GradientForeColor = GUI._ButttonFontColor

            Me.btnExApprMigration.GradientBackColor = GUI._ButttonBackColor
            Me.btnExApprMigration.GradientForeColor = GUI._ButttonFontColor

            Me.btnClaims_Request.GradientBackColor = GUI._ButttonBackColor
            Me.btnClaims_Request.GradientForeColor = GUI._ButttonFontColor

            Me.btnExpenseApproval.GradientBackColor = GUI._ButttonBackColor
            Me.btnExpenseApproval.GradientForeColor = GUI._ButttonFontColor

            Me.btnPostToPayroll.GradientBackColor = GUI._ButttonBackColor
            Me.btnPostToPayroll.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessCaptionSettings.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessCaptionSettings.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessScale.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessScale.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessCompanyGoals.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessCompanyGoals.GradientForeColor = GUI._ButttonFontColor

            Me.btnAllocationGoals.GradientBackColor = GUI._ButttonBackColor
            Me.btnAllocationGoals.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeGoals.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeGoals.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessmentMapping.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessmentMapping.GradientForeColor = GUI._ButttonFontColor

            Me.btnCompetencies.GradientBackColor = GUI._ButttonBackColor
            Me.btnCompetencies.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssignCompetencies.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssignCompetencies.GradientForeColor = GUI._ButttonFontColor

            Me.btnCustomHeaders.GradientBackColor = GUI._ButttonBackColor
            Me.btnCustomHeaders.GradientForeColor = GUI._ButttonFontColor

            Me.btnCustomItems.GradientBackColor = GUI._ButttonBackColor
            Me.btnCustomItems.GradientForeColor = GUI._ButttonFontColor

            Me.btnSelfPEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnSelfPEvaluation.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssessorPEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssessorPEvaluation.GradientForeColor = GUI._ButttonFontColor

            Me.btnReviewerPEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnReviewerPEvaluation.GradientForeColor = GUI._ButttonFontColor

            Me.btnBSCViewSetting.GradientBackColor = GUI._ButttonBackColor
            Me.btnBSCViewSetting.GradientForeColor = GUI._ButttonFontColor

            Me.btnOpenPeriodicReview.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenPeriodicReview.GradientForeColor = GUI._ButttonFontColor

            Me.btnPerspective.GradientBackColor = GUI._ButttonBackColor
            Me.btnPerspective.GradientForeColor = GUI._ButttonFontColor

            Me.btnComputationFormula.GradientBackColor = GUI._ButttonBackColor
            Me.btnComputationFormula.GradientForeColor = GUI._ButttonFontColor

            Me.btnPerformancePlan.GradientBackColor = GUI._ButttonBackColor
            Me.btnPerformancePlan.GradientForeColor = GUI._ButttonFontColor

            Me.btnLeaveAdjustment.GradientBackColor = GUI._ButttonBackColor
            Me.btnLeaveAdjustment.GradientForeColor = GUI._ButttonFontColor

            Me.btnLVSwapApprover.GradientBackColor = GUI._ButttonBackColor
            Me.btnLVSwapApprover.GradientForeColor = GUI._ButttonFontColor

            Me.btnClaimSwapApprover.GradientBackColor = GUI._ButttonBackColor
            Me.btnClaimSwapApprover.GradientForeColor = GUI._ButttonFontColor

            Me.btnGlobalVoidAssessment.GradientBackColor = GUI._ButttonBackColor
            Me.btnGlobalVoidAssessment.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoanApproverLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoanApproverLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoanApprover.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoanApprover.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoanApplication.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoanApplication.GradientForeColor = GUI._ButttonFontColor

            Me.btnProcessPendingLoan.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcessPendingLoan.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoanAdvance.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoanAdvance.GradientForeColor = GUI._ButttonFontColor

            Me.btnImportLA.GradientBackColor = GUI._ButttonBackColor
            Me.btnImportLA.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoanApproverMigration.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoanApproverMigration.GradientForeColor = GUI._ButttonFontColor

            Me.btnLoanSwapApprover.GradientBackColor = GUI._ButttonBackColor
            Me.btnLoanSwapApprover.GradientForeColor = GUI._ButttonFontColor

            Me.btnImportSaving.GradientBackColor = GUI._ButttonBackColor
            Me.btnImportSaving.GradientForeColor = GUI._ButttonFontColor

            Me.btnSavingScheme.GradientBackColor = GUI._ButttonBackColor
            Me.btnSavingScheme.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeSavings.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeSavings.GradientForeColor = GUI._ButttonFontColor

            Me.btnSecRouteAssignment.GradientBackColor = GUI._ButttonBackColor
            Me.btnSecRouteAssignment.GradientForeColor = GUI._ButttonFontColor

            Me.btnComputeProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnComputeProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnFundSources.GradientBackColor = GUI._ButttonBackColor
            Me.btnFundSources.GradientForeColor = GUI._ButttonFontColor

            Me.btnProjectCodeAdjustments.GradientBackColor = GUI._ButttonBackColor
            Me.btnProjectCodeAdjustments.GradientForeColor = GUI._ButttonFontColor

            Me.btnBudgetFormula.GradientBackColor = GUI._ButttonBackColor
            Me.btnBudgetFormula.GradientForeColor = GUI._ButttonFontColor

            Me.btnBudget.GradientBackColor = GUI._ButttonBackColor
            Me.btnBudget.GradientForeColor = GUI._ButttonFontColor

            Me.btnFundActivity.GradientBackColor = GUI._ButttonBackColor
            Me.btnFundActivity.GradientForeColor = GUI._ButttonFontColor

            Me.btnHearingList.GradientBackColor = GUI._ButttonBackColor
            Me.btnHearingList.GradientForeColor = GUI._ButttonFontColor

            Me.btnFundActivityAdjustment.GradientBackColor = GUI._ButttonBackColor
            Me.btnFundActivityAdjustment.GradientForeColor = GUI._ButttonFontColor

            Me.btnBudgetCodes.GradientBackColor = GUI._ButttonBackColor
            Me.btnBudgetCodes.GradientForeColor = GUI._ButttonFontColor

            Me.btnFundProjectCode.GradientBackColor = GUI._ButttonBackColor
            Me.btnFundProjectCode.GradientForeColor = GUI._ButttonFontColor

            Me.btnBgtApproverLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnBgtApproverLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnBgtApproverMapping.GradientBackColor = GUI._ButttonBackColor
            Me.btnBgtApproverMapping.GradientForeColor = GUI._ButttonFontColor

            Me.btnTimesheetApproverLevel.GradientBackColor = GUI._ButttonBackColor
            Me.btnTimesheetApproverLevel.GradientForeColor = GUI._ButttonFontColor

            Me.btnExportPrintEmplForm.GradientBackColor = GUI._ButttonBackColor
            Me.btnExportPrintEmplForm.GradientForeColor = GUI._ButttonFontColor

            Me.btnTimesheetApproverMst.GradientBackColor = GUI._ButttonBackColor
            Me.btnTimesheetApproverMst.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmployeeTimesheet.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmployeeTimesheet.GradientForeColor = GUI._ButttonFontColor

            Me.btnTimesheetApproval.GradientBackColor = GUI._ButttonBackColor
            Me.btnTimesheetApproval.GradientForeColor = GUI._ButttonFontColor

            Me.btnTimesheetApproverMigration.GradientBackColor = GUI._ButttonBackColor
            Me.btnTimesheetApproverMigration.GradientForeColor = GUI._ButttonFontColor

			Me.btnSkillExpertise.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSkillExpertise.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmpApproverLevel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmpApproverLevel.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmployeeApprover.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmployeeApprover.GradientForeColor = GUI._ButttonFontColor

			Me.btnApproveEmployee.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApproveEmployee.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmployeeDataApproval.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmployeeDataApproval.GradientForeColor = GUI._ButttonFontColor

			Me.btnSystemErrorLog.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSystemErrorLog.GradientForeColor = GUI._ButttonFontColor

			Me.btnCountry.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCountry.GradientForeColor = GUI._ButttonFontColor

			Me.btnMapOrbitParams.GradientBackColor = GUI._ButttonBackColor 
			Me.btnMapOrbitParams.GradientForeColor = GUI._ButttonFontColor

			Me.btnGlobalInterviewAnalysis.GradientBackColor = GUI._ButttonBackColor 
			Me.btnGlobalInterviewAnalysis.GradientForeColor = GUI._ButttonFontColor

			Me.btnReportLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReportLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnBranch.Text = Language._Object.getCaption(Me.btnBranch.Name, Me.btnBranch.Text)
            Me.btnDepartmentGroup.Text = Language._Object.getCaption(Me.btnDepartmentGroup.Name, Me.btnDepartmentGroup.Text)
            Me.btnDepartment.Text = Language._Object.getCaption(Me.btnDepartment.Name, Me.btnDepartment.Text)
            Me.btnSection.Text = Language._Object.getCaption(Me.btnSection.Name, Me.btnSection.Text)
            Me.btnUnit.Text = Language._Object.getCaption(Me.btnUnit.Name, Me.btnUnit.Text)
            Me.btnJobGroup.Text = Language._Object.getCaption(Me.btnJobGroup.Name, Me.btnJobGroup.Text)
            Me.btnJob.Text = Language._Object.getCaption(Me.btnJob.Name, Me.btnJob.Text)
            Me.btnClassGroup.Text = Language._Object.getCaption(Me.btnClassGroup.Name, Me.btnClassGroup.Text)
            Me.btnClasses.Text = Language._Object.getCaption(Me.btnClasses.Name, Me.btnClasses.Text)
            Me.btnGradeGroup.Text = Language._Object.getCaption(Me.btnGradeGroup.Name, Me.btnGradeGroup.Text)
            Me.btnGrade.Text = Language._Object.getCaption(Me.btnGrade.Name, Me.btnGrade.Text)
            Me.btnGradeLevel.Text = Language._Object.getCaption(Me.btnGradeLevel.Name, Me.btnGradeLevel.Text)
            Me.btnQualification.Text = Language._Object.getCaption(Me.btnQualification.Name, Me.btnQualification.Text)
            Me.btnAdvertiseMaster.Text = Language._Object.getCaption(Me.btnAdvertiseMaster.Name, Me.btnAdvertiseMaster.Text)
            Me.btnSkillMaster.Text = Language._Object.getCaption(Me.btnSkillMaster.Name, Me.btnSkillMaster.Text)
            Me.btnReasons.Text = Language._Object.getCaption(Me.btnReasons.Name, Me.btnReasons.Text)
            Me.btnBenefitPlan.Text = Language._Object.getCaption(Me.btnBenefitPlan.Name, Me.btnBenefitPlan.Text)
            Me.btnResultCode.Text = Language._Object.getCaption(Me.btnResultCode.Name, Me.btnResultCode.Text)
            Me.btnEmployeeData2.Text = Language._Object.getCaption(Me.btnEmployeeData2.Name, Me.btnEmployeeData2.Text)
            Me.btnDependants2.Text = Language._Object.getCaption(Me.btnDependants2.Name, Me.btnDependants2.Text)
            Me.btnBenefitAllocation.Text = Language._Object.getCaption(Me.btnBenefitAllocation.Name, Me.btnBenefitAllocation.Text)
            Me.btnEmployeeBenefit.Text = Language._Object.getCaption(Me.btnEmployeeBenefit.Name, Me.btnEmployeeBenefit.Text)
            Me.btnGroupBenefit.Text = Language._Object.getCaption(Me.btnGroupBenefit.Name, Me.btnGroupBenefit.Text)
            Me.btnReferee.Text = Language._Object.getCaption(Me.btnReferee.Name, Me.btnReferee.Text)
            Me.btnEmployeeAssets.Text = Language._Object.getCaption(Me.btnEmployeeAssets.Name, Me.btnEmployeeAssets.Text)
            Me.btnEmployeeSkill.Text = Language._Object.getCaption(Me.btnEmployeeSkill.Name, Me.btnEmployeeSkill.Text)
            Me.btnEmployeeQualification.Text = Language._Object.getCaption(Me.btnEmployeeQualification.Name, Me.btnEmployeeQualification.Text)
            Me.btnEmployeeExperence.Text = Language._Object.getCaption(Me.btnEmployeeExperence.Name, Me.btnEmployeeExperence.Text)
            Me.btnDisciplineType.Text = Language._Object.getCaption(Me.btnDisciplineType.Name, Me.btnDisciplineType.Text)
            Me.btnDisciplinaryAction.Text = Language._Object.getCaption(Me.btnDisciplinaryAction.Name, Me.btnDisciplinaryAction.Text)
            Me.btnDisciplineStatus.Text = Language._Object.getCaption(Me.btnDisciplineStatus.Name, Me.btnDisciplineStatus.Text)
            Me.btnDisciplineFiling.Text = Language._Object.getCaption(Me.btnDisciplineFiling.Name, Me.btnDisciplineFiling.Text)
            Me.btnAssessmentPeriod.Text = Language._Object.getCaption(Me.btnAssessmentPeriod.Name, Me.btnAssessmentPeriod.Text)
            Me.btnAssessGroup.Text = Language._Object.getCaption(Me.btnAssessGroup.Name, Me.btnAssessGroup.Text)
            Me.btnAssessItems.Text = Language._Object.getCaption(Me.btnAssessItems.Name, Me.btnAssessItems.Text)
            Me.btnAssessor.Text = Language._Object.getCaption(Me.btnAssessor.Name, Me.btnAssessor.Text)
            Me.btnAssessorAccess.Text = Language._Object.getCaption(Me.btnAssessorAccess.Name, Me.btnAssessorAccess.Text)
            Me.btnEmployeeAssessment.Text = Language._Object.getCaption(Me.btnEmployeeAssessment.Name, Me.btnEmployeeAssessment.Text)
            Me.btnTrainingInstitute.Text = Language._Object.getCaption(Me.btnTrainingInstitute.Name, Me.btnTrainingInstitute.Text)
            Me.btnTrainingScheduling.Text = Language._Object.getCaption(Me.btnTrainingScheduling.Name, Me.btnTrainingScheduling.Text)
            Me.btnTrainingEnrollment.Text = Language._Object.getCaption(Me.btnTrainingEnrollment.Name, Me.btnTrainingEnrollment.Text)
            Me.btnTrainingAttendance.Text = Language._Object.getCaption(Me.btnTrainingAttendance.Name, Me.btnTrainingAttendance.Text)
            Me.btnTrainingAnalysis.Text = Language._Object.getCaption(Me.btnTrainingAnalysis.Name, Me.btnTrainingAnalysis.Text)
            Me.btnMedicalMasters.Text = Language._Object.getCaption(Me.btnMedicalMasters.Name, Me.btnMedicalMasters.Text)
            Me.btnMedicalInstitute.Text = Language._Object.getCaption(Me.btnMedicalInstitute.Name, Me.btnMedicalInstitute.Text)
            Me.btnEmployeeInjuries.Text = Language._Object.getCaption(Me.btnEmployeeInjuries.Name, Me.btnEmployeeInjuries.Text)
            Me.btnCategoryAssignment.Text = Language._Object.getCaption(Me.btnCategoryAssignment.Name, Me.btnCategoryAssignment.Text)
            Me.btnCoverAssignment.Text = Language._Object.getCaption(Me.btnCoverAssignment.Name, Me.btnCoverAssignment.Text)
            Me.btnMedicalClaim.Text = Language._Object.getCaption(Me.btnMedicalClaim.Name, Me.btnMedicalClaim.Text)
            Me.btnPayrollGroup.Text = Language._Object.getCaption(Me.btnPayrollGroup.Name, Me.btnPayrollGroup.Text)
            Me.btnPayrollPeriods.Text = Language._Object.getCaption(Me.btnPayrollPeriods.Name, Me.btnPayrollPeriods.Text)
            Me.btnCostcenter.Text = Language._Object.getCaption(Me.btnCostcenter.Name, Me.btnCostcenter.Text)
            Me.btnPayPoint.Text = Language._Object.getCaption(Me.btnPayPoint.Name, Me.btnPayPoint.Text)
            Me.btnBankBranch.Text = Language._Object.getCaption(Me.btnBankBranch.Name, Me.btnBankBranch.Text)
            Me.btnBankAccountType.Text = Language._Object.getCaption(Me.btnBankAccountType.Name, Me.btnBankAccountType.Text)
            Me.btnBankEDI.Text = Language._Object.getCaption(Me.btnBankEDI.Name, Me.btnBankEDI.Text)
            Me.btnCurrency.Text = Language._Object.getCaption(Me.btnCurrency.Name, Me.btnCurrency.Text)
            Me.btnDemomination.Text = Language._Object.getCaption(Me.btnDemomination.Name, Me.btnDemomination.Text)
            Me.btnEmployeeCostCenter.Text = Language._Object.getCaption(Me.btnEmployeeCostCenter.Name, Me.btnEmployeeCostCenter.Text)
            Me.btnEmployeeBanks.Text = Language._Object.getCaption(Me.btnEmployeeBanks.Name, Me.btnEmployeeBanks.Text)
            Me.btnBatchTransaction.Text = Language._Object.getCaption(Me.btnBatchTransaction.Name, Me.btnBatchTransaction.Text)
            Me.btnTransactionHeads.Text = Language._Object.getCaption(Me.btnTransactionHeads.Name, Me.btnTransactionHeads.Text)
            Me.btnEarningDeduction.Text = Language._Object.getCaption(Me.btnEarningDeduction.Name, Me.btnEarningDeduction.Text)
            Me.btnProcessPayroll.Text = Language._Object.getCaption(Me.btnProcessPayroll.Name, Me.btnProcessPayroll.Text)
            Me.btnPayslip.Text = Language._Object.getCaption(Me.btnPayslip.Name, Me.btnPayslip.Text)
            Me.btnBudgetPreparation.Text = Language._Object.getCaption(Me.btnBudgetPreparation.Name, Me.btnBudgetPreparation.Text)
            Me.btnSalaryIncrement.Text = Language._Object.getCaption(Me.btnSalaryIncrement.Name, Me.btnSalaryIncrement.Text)
            Me.btnWagesTable.Text = Language._Object.getCaption(Me.btnWagesTable.Name, Me.btnWagesTable.Text)
            Me.btnEmployeeExemption.Text = Language._Object.getCaption(Me.btnEmployeeExemption.Name, Me.btnEmployeeExemption.Text)
            Me.btnGlobalAssignED.Text = Language._Object.getCaption(Me.btnGlobalAssignED.Name, Me.btnGlobalAssignED.Text)
            Me.btnBatchEntry.Text = Language._Object.getCaption(Me.btnBatchEntry.Name, Me.btnBatchEntry.Text)
            Me.btnClosePayroll.Text = Language._Object.getCaption(Me.btnClosePayroll.Name, Me.btnClosePayroll.Text)
            Me.btnLeaveType.Text = Language._Object.getCaption(Me.btnLeaveType.Name, Me.btnLeaveType.Text)
            Me.btnHolidays.Text = Language._Object.getCaption(Me.btnHolidays.Name, Me.btnHolidays.Text)
            Me.btnEmployeeHolidays.Text = Language._Object.getCaption(Me.btnEmployeeHolidays.Name, Me.btnEmployeeHolidays.Text)
            Me.btnApproverLevel.Text = Language._Object.getCaption(Me.btnApproverLevel.Name, Me.btnApproverLevel.Text)
            Me.btnLeaveApprover.Text = Language._Object.getCaption(Me.btnLeaveApprover.Name, Me.btnLeaveApprover.Text)
            Me.btnLeaveForm.Text = Language._Object.getCaption(Me.btnLeaveForm.Name, Me.btnLeaveForm.Text)
            Me.btnLeaveProcess.Text = Language._Object.getCaption(Me.btnLeaveProcess.Name, Me.btnLeaveProcess.Text)
            Me.btnLeaveAccrue.Text = Language._Object.getCaption(Me.btnLeaveAccrue.Name, Me.btnLeaveAccrue.Text)
            Me.btnLeaveViewer.Text = Language._Object.getCaption(Me.btnLeaveViewer.Name, Me.btnLeaveViewer.Text)
            Me.btnLeaveSchedular.Text = Language._Object.getCaption(Me.btnLeaveSchedular.Name, Me.btnLeaveSchedular.Text)
            Me.btnLeavePlanner.Text = Language._Object.getCaption(Me.btnLeavePlanner.Name, Me.btnLeavePlanner.Text)
            Me.btnShift.Text = Language._Object.getCaption(Me.btnShift.Name, Me.btnShift.Text)
            Me.btnLogin.Text = Language._Object.getCaption(Me.btnLogin.Name, Me.btnLogin.Text)
            Me.btnTimesheet.Text = Language._Object.getCaption(Me.btnTimesheet.Name, Me.btnTimesheet.Text)
            Me.btnHoldEmployee.Text = Language._Object.getCaption(Me.btnHoldEmployee.Name, Me.btnHoldEmployee.Text)
            Me.btnEmployeeAbsent.Text = Language._Object.getCaption(Me.btnEmployeeAbsent.Name, Me.btnEmployeeAbsent.Text)
            Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
            Me.btnExportData.Text = Language._Object.getCaption(Me.btnExportData.Name, Me.btnExportData.Text)
            Me.btnLoanScheme.Text = Language._Object.getCaption(Me.btnLoanScheme.Name, Me.btnLoanScheme.Text)
            Me.btnApplicantMaster.Text = Language._Object.getCaption(Me.btnApplicantMaster.Name, Me.btnApplicantMaster.Text)
            Me.btnVacancy.Text = Language._Object.getCaption(Me.btnVacancy.Name, Me.btnVacancy.Text)
            Me.btnBatchScheduling.Text = Language._Object.getCaption(Me.btnBatchScheduling.Name, Me.btnBatchScheduling.Text)
            Me.btnInterviewScheduling.Text = Language._Object.getCaption(Me.btnInterviewScheduling.Name, Me.btnInterviewScheduling.Text)
            Me.btnInterviewAnalysis.Text = Language._Object.getCaption(Me.btnInterviewAnalysis.Name, Me.btnInterviewAnalysis.Text)
            Me.btnFinalApplicants.Text = Language._Object.getCaption(Me.btnFinalApplicants.Name, Me.btnFinalApplicants.Text)
            Me.btnLetterType.Text = Language._Object.getCaption(Me.btnLetterType.Name, Me.btnLetterType.Text)
            Me.btnLetterTemplate.Text = Language._Object.getCaption(Me.btnLetterTemplate.Name, Me.btnLetterTemplate.Text)
            Me.btnMailbox.Text = Language._Object.getCaption(Me.btnMailbox.Name, Me.btnMailbox.Text)
            Me.btnReminder.Text = Language._Object.getCaption(Me.btnReminder.Name, Me.btnReminder.Text)
            Me.btnBackup.Text = Language._Object.getCaption(Me.btnBackup.Name, Me.btnBackup.Text)
            Me.btnRestore.Text = Language._Object.getCaption(Me.btnRestore.Name, Me.btnRestore.Text)
            Me.btnChangeUser.Text = Language._Object.getCaption(Me.btnChangeUser.Name, Me.btnChangeUser.Text)
            Me.btnChangeCompany.Text = Language._Object.getCaption(Me.btnChangeCompany.Name, Me.btnChangeCompany.Text)
            Me.btnChangeDatabase.Text = Language._Object.getCaption(Me.btnChangeDatabase.Name, Me.btnChangeDatabase.Text)
            Me.btnGeneralSettings.Text = Language._Object.getCaption(Me.btnGeneralSettings.Name, Me.btnGeneralSettings.Text)
            Me.btnPayslipMessage.Text = Language._Object.getCaption(Me.btnPayslipMessage.Name, Me.btnPayslipMessage.Text)
            Me.btnGlobalPayslipMessage.Text = Language._Object.getCaption(Me.btnGlobalPayslipMessage.Name, Me.btnGlobalPayslipMessage.Text)
            Me.btnCompanyJV.Text = Language._Object.getCaption(Me.btnCompanyJV.Name, Me.btnCompanyJV.Text)
            Me.btnAccount.Text = Language._Object.getCaption(Me.btnAccount.Name, Me.btnAccount.Text)
            Me.btnAccountConfiguration.Text = Language._Object.getCaption(Me.btnAccountConfiguration.Name, Me.btnAccountConfiguration.Text)
            Me.btnEmployeeMovement2.Text = Language._Object.getCaption(Me.btnEmployeeMovement2.Name, Me.btnEmployeeMovement2.Text)
            Me.btnEmployeeDiary.Text = Language._Object.getCaption(Me.btnEmployeeDiary.Name, Me.btnEmployeeDiary.Text)
            Me.btnCashDenomination.Text = Language._Object.getCaption(Me.btnCashDenomination.Name, Me.btnCashDenomination.Text)
            Me.mnuGeneralMaster.Text = Language._Object.getCaption(Me.mnuGeneralMaster.Name, Me.mnuGeneralMaster.Text)
            Me.mnuRecruitment.Text = Language._Object.getCaption(Me.mnuRecruitment.Name, Me.mnuRecruitment.Text)
            Me.mnuHumanResource.Text = Language._Object.getCaption(Me.mnuHumanResource.Name, Me.mnuHumanResource.Text)
            Me.mnuPersonnel.Text = Language._Object.getCaption(Me.mnuPersonnel.Name, Me.mnuPersonnel.Text)
            Me.mnuHRMaster.Text = Language._Object.getCaption(Me.mnuHRMaster.Name, Me.mnuHRMaster.Text)
            Me.mnuEmployeeData.Text = Language._Object.getCaption(Me.mnuEmployeeData.Name, Me.mnuEmployeeData.Text)
            Me.mnuMedical.Text = Language._Object.getCaption(Me.mnuMedical.Name, Me.mnuMedical.Text)
            Me.mnuPayroll.Text = Language._Object.getCaption(Me.mnuPayroll.Name, Me.mnuPayroll.Text)
            Me.mnuPayrollMasters.Text = Language._Object.getCaption(Me.mnuPayrollMasters.Name, Me.mnuPayrollMasters.Text)
            Me.mnuPayrollTransactions.Text = Language._Object.getCaption(Me.mnuPayrollTransactions.Name, Me.mnuPayrollTransactions.Text)
            Me.mnuLoan_Advance_Savings.Text = Language._Object.getCaption(Me.mnuLoan_Advance_Savings.Name, Me.mnuLoan_Advance_Savings.Text)
            Me.mnuLeaveAndTnA.Text = Language._Object.getCaption(Me.mnuLeaveAndTnA.Name, Me.mnuLeaveAndTnA.Text)
            Me.mnuLeaveInformation.Text = Language._Object.getCaption(Me.mnuLeaveInformation.Name, Me.mnuLeaveInformation.Text)
            Me.mnuTnAInfo.Text = Language._Object.getCaption(Me.mnuTnAInfo.Name, Me.mnuTnAInfo.Text)
            Me.mnuUtilitiesMain.Text = Language._Object.getCaption(Me.mnuUtilitiesMain.Name, Me.mnuUtilitiesMain.Text)
            Me.mnuView.Text = Language._Object.getCaption(Me.mnuView.Name, Me.mnuView.Text)
            Me.mnuPayrollView.Text = Language._Object.getCaption(Me.mnuPayrollView.Name, Me.mnuPayrollView.Text)
            Me.mnuReportView.Text = Language._Object.getCaption(Me.mnuReportView.Name, Me.mnuReportView.Text)
            Me.mnuExit.Text = Language._Object.getCaption(Me.mnuExit.Name, Me.mnuExit.Text)
            Me.mnuRecruitmentItem.Text = Language._Object.getCaption(Me.mnuRecruitmentItem.Name, Me.mnuRecruitmentItem.Text)
            Me.mnuHelp.Text = Language._Object.getCaption(Me.mnuHelp.Name, Me.mnuHelp.Text)
            Me.mnuProduct.Text = Language._Object.getCaption(Me.mnuProduct.Name, Me.mnuProduct.Text)
            Me.mnuAbout.Text = Language._Object.getCaption(Me.mnuAbout.Name, Me.mnuAbout.Text)
            Me.btnAssessorAssessmentlist.Text = Language._Object.getCaption(Me.btnAssessorAssessmentlist.Name, Me.btnAssessorAssessmentlist.Text)
            Me.btnCommonImportData.Text = Language._Object.getCaption(Me.btnCommonImportData.Name, Me.btnCommonImportData.Text)
            Me.btnCommonExportData.Text = Language._Object.getCaption(Me.btnCommonExportData.Name, Me.btnCommonExportData.Text)
            Me.StatusStrip1.Text = Language._Object.getCaption(Me.StatusStrip1.Name, Me.StatusStrip1.Text)
            Me.lblPropertyName.Text = Language._Object.getCaption(Me.lblPropertyName.Name, Me.lblPropertyName.Text)
            Me.lblPayrollYear.Text = Language._Object.getCaption(Me.lblPayrollYear.Name, Me.lblPayrollYear.Text)
            Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
            Me.lblWorkingDate.Text = Language._Object.getCaption(Me.lblWorkingDate.Name, Me.lblWorkingDate.Text)
            Me.mnuDiscipline.Text = Language._Object.getCaption(Me.mnuDiscipline.Name, Me.mnuDiscipline.Text)
            Me.mnuAssessment.Text = Language._Object.getCaption(Me.mnuAssessment.Name, Me.mnuAssessment.Text)
            Me.mnuTrainingInformation.Text = Language._Object.getCaption(Me.mnuTrainingInformation.Name, Me.mnuTrainingInformation.Text)
            Me.mnuCommonSetups.Text = Language._Object.getCaption(Me.mnuCommonSetups.Name, Me.mnuCommonSetups.Text)
            Me.mnuCoreSetups.Text = Language._Object.getCaption(Me.mnuCoreSetups.Name, Me.mnuCoreSetups.Text)
            Me.btnCommonMaster.Text = Language._Object.getCaption(Me.btnCommonMaster.Name, Me.btnCommonMaster.Text)
            Me.btnMemebership.Text = Language._Object.getCaption(Me.btnMemebership.Name, Me.btnMemebership.Text)
            Me.btnState.Text = Language._Object.getCaption(Me.btnState.Name, Me.btnState.Text)
            Me.btnCity.Text = Language._Object.getCaption(Me.btnCity.Name, Me.btnCity.Text)
            Me.btnZipcode.Text = Language._Object.getCaption(Me.btnZipcode.Name, Me.btnZipcode.Text)
            Me.btnEmployeeData.Text = Language._Object.getCaption(Me.btnEmployeeData.Name, Me.btnEmployeeData.Text)
            Me.btnDependants.Text = Language._Object.getCaption(Me.btnDependants.Name, Me.btnDependants.Text)
            Me.btnEmployeeMovement.Text = Language._Object.getCaption(Me.btnEmployeeMovement.Name, Me.btnEmployeeMovement.Text)
            Me.btnImportAttendancedata.Text = Language._Object.getCaption(Me.btnImportAttendancedata.Name, Me.btnImportAttendancedata.Text)
            Me.btnGroupAttendance.Text = Language._Object.getCaption(Me.btnGroupAttendance.Name, Me.btnGroupAttendance.Text)
            Me.btnImportAccrueLeave.Text = Language._Object.getCaption(Me.btnImportAccrueLeave.Name, Me.btnImportAccrueLeave.Text)
            Me.btnUserLog.Text = Language._Object.getCaption(Me.btnUserLog.Name, Me.btnUserLog.Text)
            Me.btnChangePwd.Text = Language._Object.getCaption(Me.btnChangePwd.Name, Me.btnChangePwd.Text)
            Me.btnEmpAccConfig.Text = Language._Object.getCaption(Me.btnEmpAccConfig.Name, Me.btnEmpAccConfig.Text)
            Me.btnCCAccConfig.Text = Language._Object.getCaption(Me.btnCCAccConfig.Name, Me.btnCCAccConfig.Text)
            Me.mnuRegister.Text = Language._Object.getCaption(Me.mnuRegister.Name, Me.mnuRegister.Text)
            Me.btnSectionGroup.Text = Language._Object.getCaption(Me.btnSectionGroup.Name, Me.btnSectionGroup.Text)
            Me.btnUnitGroup.Text = Language._Object.getCaption(Me.btnUnitGroup.Name, Me.btnUnitGroup.Text)
            Me.btnTeam.Text = Language._Object.getCaption(Me.btnTeam.Name, Me.btnTeam.Text)
            Me.btnExporttoWeb.Text = Language._Object.getCaption(Me.btnExporttoWeb.Name, Me.btnExporttoWeb.Text)
            Me.btnImportfromWeb.Text = Language._Object.getCaption(Me.btnImportfromWeb.Name, Me.btnImportfromWeb.Text)
            Me.btnApplicantFilter.Text = Language._Object.getCaption(Me.btnApplicantFilter.Name, Me.btnApplicantFilter.Text)
            Me.btnFinalShortListedApplicant.Text = Language._Object.getCaption(Me.btnFinalShortListedApplicant.Name, Me.btnFinalShortListedApplicant.Text)
            Me.btnResolutionSteps.Text = Language._Object.getCaption(Me.btnResolutionSteps.Name, Me.btnResolutionSteps.Text)
            Me.btnSickSheet.Text = Language._Object.getCaption(Me.btnSickSheet.Name, Me.btnSickSheet.Text)
            Me.lblEmployeeAsOnDate.Text = Language._Object.getCaption(Me.lblEmployeeAsOnDate.Name, Me.lblEmployeeAsOnDate.Text)
            Me.mnuWagesSetup.Text = Language._Object.getCaption(Me.mnuWagesSetup.Name, Me.mnuWagesSetup.Text)
            Me.mnuGeneralAssessment.Text = Language._Object.getCaption(Me.mnuGeneralAssessment.Name, Me.mnuGeneralAssessment.Text)
            Me.mnuBalancedScoreCard.Text = Language._Object.getCaption(Me.mnuBalancedScoreCard.Name, Me.mnuBalancedScoreCard.Text)
            Me.btnAssessSubItem.Text = Language._Object.getCaption(Me.btnAssessSubItem.Name, Me.btnAssessSubItem.Text)
            Me.btnObjectiveList.Text = Language._Object.getCaption(Me.btnObjectiveList.Name, Me.btnObjectiveList.Text)
            Me.btnKPIMeasureList.Text = Language._Object.getCaption(Me.btnKPIMeasureList.Name, Me.btnKPIMeasureList.Text)
            Me.btnTargetsList.Text = Language._Object.getCaption(Me.btnTargetsList.Name, Me.btnTargetsList.Text)
            Me.btnInitiativeList.Text = Language._Object.getCaption(Me.btnInitiativeList.Name, Me.btnInitiativeList.Text)
            Me.btnReviewer.Text = Language._Object.getCaption(Me.btnReviewer.Name, Me.btnReviewer.Text)
            Me.btnReviewerAssessment.Text = Language._Object.getCaption(Me.btnReviewerAssessment.Name, Me.btnReviewerAssessment.Text)
            Me.btnSelfAssessedBSC.Text = Language._Object.getCaption(Me.btnSelfAssessedBSC.Name, Me.btnSelfAssessedBSC.Text)
            Me.btnAssessorBSCList.Text = Language._Object.getCaption(Me.btnAssessorBSCList.Name, Me.btnAssessorBSCList.Text)
            Me.btnReviewerBSCList.Text = Language._Object.getCaption(Me.btnReviewerBSCList.Name, Me.btnReviewerBSCList.Text)
            Me.btnExternalAssessor.Text = Language._Object.getCaption(Me.btnExternalAssessor.Name, Me.btnExternalAssessor.Text)
            Me.mnuSetups.Text = Language._Object.getCaption(Me.mnuSetups.Name, Me.mnuSetups.Text)
            Me.mnuAppraisal.Text = Language._Object.getCaption(Me.mnuAppraisal.Name, Me.mnuAppraisal.Text)
            Me.btnShortListEmployee.Text = Language._Object.getCaption(Me.btnShortListEmployee.Name, Me.btnShortListEmployee.Text)
            Me.btnFinalShortListedEmployee.Text = Language._Object.getCaption(Me.btnFinalShortListedEmployee.Name, Me.btnFinalShortListedEmployee.Text)
            Me.btnTrainingPriority.Text = Language._Object.getCaption(Me.btnTrainingPriority.Name, Me.btnTrainingPriority.Text)
            Me.btnFinalizingTrainingPriority.Text = Language._Object.getCaption(Me.btnFinalizingTrainingPriority.Name, Me.btnFinalizingTrainingPriority.Text)
            Me.mnuTrainingEvaluation.Text = Language._Object.getCaption(Me.mnuTrainingEvaluation.Name, Me.mnuTrainingEvaluation.Text)
            Me.mnuJobTraining.Text = Language._Object.getCaption(Me.mnuJobTraining.Name, Me.mnuJobTraining.Text)
            Me.btnScanAttachments.Text = Language._Object.getCaption(Me.btnScanAttachments.Name, Me.btnScanAttachments.Text)
            Me.mnuAssetDeclaration.Text = Language._Object.getCaption(Me.mnuAssetDeclaration.Name, Me.mnuAssetDeclaration.Text)
            Me.mnuTrainingFeedback.Text = Language._Object.getCaption(Me.mnuTrainingFeedback.Name, Me.mnuTrainingFeedback.Text)
            Me.btnFeedbackGroup.Text = Language._Object.getCaption(Me.btnFeedbackGroup.Name, Me.btnFeedbackGroup.Text)
            Me.btnFeedbackItems.Text = Language._Object.getCaption(Me.btnFeedbackItems.Name, Me.btnFeedbackItems.Text)
            Me.btnFeedbackSubItems.Text = Language._Object.getCaption(Me.btnFeedbackSubItems.Name, Me.btnFeedbackSubItems.Text)
            Me.btnFeedback.Text = Language._Object.getCaption(Me.btnFeedback.Name, Me.btnFeedback.Text)
            Me.btnEvaluationLevel3.Text = Language._Object.getCaption(Me.btnEvaluationLevel3.Name, Me.btnEvaluationLevel3.Text)
            Me.btnImpactItems.Text = Language._Object.getCaption(Me.btnImpactItems.Name, Me.btnImpactItems.Text)
            Me.btnCommittee.Text = Language._Object.getCaption(Me.btnCommittee.Name, Me.btnCommittee.Text)
            Me.btnMigration.Text = Language._Object.getCaption(Me.btnMigration.Name, Me.btnMigration.Text)
            Me.BtnTransferApprover.Text = Language._Object.getCaption(Me.BtnTransferApprover.Name, Me.BtnTransferApprover.Text)
            Me.mnuCustomReporting.Text = Language._Object.getCaption(Me.mnuCustomReporting.Name, Me.mnuCustomReporting.Text)
            Me.btnApplicationEventLog.Text = Language._Object.getCaption(Me.btnApplicationEventLog.Name, Me.btnApplicationEventLog.Text)
            Me.btnUserAttempts.Text = Language._Object.getCaption(Me.btnUserAttempts.Name, Me.btnUserAttempts.Text)
            Me.btnUserAuthenticationLog.Text = Language._Object.getCaption(Me.btnUserAuthenticationLog.Name, Me.btnUserAuthenticationLog.Text)
            Me.btnLogView.Text = Language._Object.getCaption(Me.btnLogView.Name, Me.btnLogView.Text)
            Me.mnuAuditTrail.Text = Language._Object.getCaption(Me.mnuAuditTrail.Name, Me.mnuAuditTrail.Text)
            Me.mnuUtilities.Text = Language._Object.getCaption(Me.mnuUtilities.Name, Me.mnuUtilities.Text)
            Me.mnuAuditTrails.Text = Language._Object.getCaption(Me.mnuAuditTrails.Name, Me.mnuAuditTrails.Text)
            Me.btnAllocationMapping.Text = Language._Object.getCaption(Me.btnAllocationMapping.Name, Me.btnAllocationMapping.Text)
            Me.btnAutoImportFromWeb.Text = Language._Object.getCaption(Me.btnAutoImportFromWeb.Name, Me.btnAutoImportFromWeb.Text)
            Me.btnBatchPosting.Text = Language._Object.getCaption(Me.btnBatchPosting.Name, Me.btnBatchPosting.Text)
            Me.btnWeightedSetting.Text = Language._Object.getCaption(Me.btnWeightedSetting.Name, Me.btnWeightedSetting.Text)
            Me.btnCloseAssessmentPeriod.Text = Language._Object.getCaption(Me.btnCloseAssessmentPeriod.Name, Me.btnCloseAssessmentPeriod.Text)
            Me.btnPayAppLevel.Text = Language._Object.getCaption(Me.btnPayAppLevel.Name, Me.btnPayAppLevel.Text)
            Me.btnPayApproverMap.Text = Language._Object.getCaption(Me.btnPayApproverMap.Name, Me.btnPayApproverMap.Text)
            Me.btnEndELC.Text = Language._Object.getCaption(Me.btnEndELC.Name, Me.btnEndELC.Text)
            Me.mnuArutiHelp.Text = Language._Object.getCaption(Me.mnuArutiHelp.Name, Me.mnuArutiHelp.Text)
            Me.btnDependantException.Text = Language._Object.getCaption(Me.btnDependantException.Name, Me.btnDependantException.Text)
            Me.mnuPayActivity.Text = Language._Object.getCaption(Me.mnuPayActivity.Name, Me.mnuPayActivity.Text)
            Me.btnActivity_List.Text = Language._Object.getCaption(Me.btnActivity_List.Name, Me.btnActivity_List.Text)
            Me.btnPayPerActivity.Text = Language._Object.getCaption(Me.btnPayPerActivity.Name, Me.btnPayPerActivity.Text)
            Me.btnMeasureList.Text = Language._Object.getCaption(Me.btnMeasureList.Name, Me.btnMeasureList.Text)
            Me.btnActivityRate.Text = Language._Object.getCaption(Me.btnActivityRate.Name, Me.btnActivityRate.Text)
            Me.btnPayrollPosting.Text = Language._Object.getCaption(Me.btnPayrollPosting.Name, Me.btnPayrollPosting.Text)
            Me.btnActivityGlobalAssign.Text = Language._Object.getCaption(Me.btnActivityGlobalAssign.Name, Me.btnActivityGlobalAssign.Text)
            Me.btnAssessmentRatio.Text = Language._Object.getCaption(Me.btnAssessmentRatio.Name, Me.btnAssessmentRatio.Text)
            Me.btnAppraisalSetups.Text = Language._Object.getCaption(Me.btnAppraisalSetups.Name, Me.btnAppraisalSetups.Text)
            Me.btnPolicyList.Text = Language._Object.getCaption(Me.btnPolicyList.Name, Me.btnPolicyList.Text)
            Me.btnEditTimeSheet.Text = Language._Object.getCaption(Me.btnEditTimeSheet.Name, Me.btnEditTimeSheet.Text)
            Me.btnRecalculateTiming.Text = Language._Object.getCaption(Me.btnRecalculateTiming.Name, Me.btnRecalculateTiming.Text)
            Me.btnLeaveFrequency.Text = Language._Object.getCaption(Me.btnLeaveFrequency.Name, Me.btnLeaveFrequency.Text)
            Me.btnDeviceUserMapping.Text = Language._Object.getCaption(Me.btnDeviceUserMapping.Name, Me.btnDeviceUserMapping.Text)
            Me.btnRoundOff.Text = Language._Object.getCaption(Me.btnRoundOff.Name, Me.btnRoundOff.Text)
            Me.btnGlobalEditDelete.Text = Language._Object.getCaption(Me.btnGlobalEditDelete.Name, Me.btnGlobalEditDelete.Text)
            Me.btnEmailLogView.Text = Language._Object.getCaption(Me.btnEmailLogView.Name, Me.btnEmailLogView.Text)
            Me.btnStaffReqAppLevel.Text = Language._Object.getCaption(Me.btnStaffReqAppLevel.Name, Me.btnStaffReqAppLevel.Text)
            Me.btnStaffReqAppMapping.Text = Language._Object.getCaption(Me.btnStaffReqAppMapping.Name, Me.btnStaffReqAppMapping.Text)
            Me.btnStaffRequisition.Text = Language._Object.getCaption(Me.btnStaffRequisition.Name, Me.btnStaffRequisition.Text)
            Me.btnRemovePPALogs.Text = Language._Object.getCaption(Me.btnRemovePPALogs.Name, Me.btnRemovePPALogs.Text)
            Me.mnuClaimsExpenses.Text = Language._Object.getCaption(Me.mnuClaimsExpenses.Name, Me.mnuClaimsExpenses.Text)
            Me.btnExpensesList.Text = Language._Object.getCaption(Me.btnExpensesList.Name, Me.btnExpensesList.Text)
            Me.btnCostingList.Text = Language._Object.getCaption(Me.btnCostingList.Name, Me.btnCostingList.Text)
            Me.btnExpenseAssignment.Text = Language._Object.getCaption(Me.btnExpenseAssignment.Name, Me.btnExpenseAssignment.Text)
            Me.btnExApprovalLevel.Text = Language._Object.getCaption(Me.btnExApprovalLevel.Name, Me.btnExApprovalLevel.Text)
            Me.btnExApprovers.Text = Language._Object.getCaption(Me.btnExApprovers.Name, Me.btnExApprovers.Text)
            Me.btnExApprMigration.Text = Language._Object.getCaption(Me.btnExApprMigration.Name, Me.btnExApprMigration.Text)
            Me.btnClaims_Request.Text = Language._Object.getCaption(Me.btnClaims_Request.Name, Me.btnClaims_Request.Text)
            Me.btnExpenseApproval.Text = Language._Object.getCaption(Me.btnExpenseApproval.Name, Me.btnExpenseApproval.Text)
            Me.btnPostToPayroll.Text = Language._Object.getCaption(Me.btnPostToPayroll.Name, Me.btnPostToPayroll.Text)
            Me.btnAssessCaptionSettings.Text = Language._Object.getCaption(Me.btnAssessCaptionSettings.Name, Me.btnAssessCaptionSettings.Text)
            Me.btnAssessScale.Text = Language._Object.getCaption(Me.btnAssessScale.Name, Me.btnAssessScale.Text)
            Me.btnAssessCompanyGoals.Text = Language._Object.getCaption(Me.btnAssessCompanyGoals.Name, Me.btnAssessCompanyGoals.Text)
            Me.btnAllocationGoals.Text = Language._Object.getCaption(Me.btnAllocationGoals.Name, Me.btnAllocationGoals.Text)
            Me.btnEmployeeGoals.Text = Language._Object.getCaption(Me.btnEmployeeGoals.Name, Me.btnEmployeeGoals.Text)
            Me.btnAssessmentMapping.Text = Language._Object.getCaption(Me.btnAssessmentMapping.Name, Me.btnAssessmentMapping.Text)
            Me.btnCompetencies.Text = Language._Object.getCaption(Me.btnCompetencies.Name, Me.btnCompetencies.Text)
            Me.btnAssignCompetencies.Text = Language._Object.getCaption(Me.btnAssignCompetencies.Name, Me.btnAssignCompetencies.Text)
            Me.btnCustomHeaders.Text = Language._Object.getCaption(Me.btnCustomHeaders.Name, Me.btnCustomHeaders.Text)
            Me.btnCustomItems.Text = Language._Object.getCaption(Me.btnCustomItems.Name, Me.btnCustomItems.Text)
            Me.mnuPerformaceEvaluation.Text = Language._Object.getCaption(Me.mnuPerformaceEvaluation.Name, Me.mnuPerformaceEvaluation.Text)
            Me.btnSelfPEvaluation.Text = Language._Object.getCaption(Me.btnSelfPEvaluation.Name, Me.btnSelfPEvaluation.Text)
            Me.btnAssessorPEvaluation.Text = Language._Object.getCaption(Me.btnAssessorPEvaluation.Name, Me.btnAssessorPEvaluation.Text)
            Me.btnReviewerPEvaluation.Text = Language._Object.getCaption(Me.btnReviewerPEvaluation.Name, Me.btnReviewerPEvaluation.Text)
            Me.btnBSCViewSetting.Text = Language._Object.getCaption(Me.btnBSCViewSetting.Name, Me.btnBSCViewSetting.Text)
            Me.btnOpenPeriodicReview.Text = Language._Object.getCaption(Me.btnOpenPeriodicReview.Name, Me.btnOpenPeriodicReview.Text)
            Me.btnPerspective.Text = Language._Object.getCaption(Me.btnPerspective.Name, Me.btnPerspective.Text)
            Me.btnComputationFormula.Text = Language._Object.getCaption(Me.btnComputationFormula.Name, Me.btnComputationFormula.Text)
            Me.btnPerformancePlan.Text = Language._Object.getCaption(Me.btnPerformancePlan.Name, Me.btnPerformancePlan.Text)
            Me.btnLeaveAdjustment.Text = Language._Object.getCaption(Me.btnLeaveAdjustment.Name, Me.btnLeaveAdjustment.Text)
            Me.btnLVSwapApprover.Text = Language._Object.getCaption(Me.btnLVSwapApprover.Name, Me.btnLVSwapApprover.Text)
            Me.btnClaimSwapApprover.Text = Language._Object.getCaption(Me.btnClaimSwapApprover.Name, Me.btnClaimSwapApprover.Text)
            Me.btnGlobalVoidAssessment.Text = Language._Object.getCaption(Me.btnGlobalVoidAssessment.Name, Me.btnGlobalVoidAssessment.Text)
            Me.btnLoanApproverLevel.Text = Language._Object.getCaption(Me.btnLoanApproverLevel.Name, Me.btnLoanApproverLevel.Text)
            Me.btnLoanApprover.Text = Language._Object.getCaption(Me.btnLoanApprover.Name, Me.btnLoanApprover.Text)
            Me.btnLoanApplication.Text = Language._Object.getCaption(Me.btnLoanApplication.Name, Me.btnLoanApplication.Text)
            Me.btnProcessPendingLoan.Text = Language._Object.getCaption(Me.btnProcessPendingLoan.Name, Me.btnProcessPendingLoan.Text)
            Me.btnLoanAdvance.Text = Language._Object.getCaption(Me.btnLoanAdvance.Name, Me.btnLoanAdvance.Text)
            Me.btnImportLA.Text = Language._Object.getCaption(Me.btnImportLA.Name, Me.btnImportLA.Text)
            Me.btnLoanApproverMigration.Text = Language._Object.getCaption(Me.btnLoanApproverMigration.Name, Me.btnLoanApproverMigration.Text)
            Me.btnLoanSwapApprover.Text = Language._Object.getCaption(Me.btnLoanSwapApprover.Name, Me.btnLoanSwapApprover.Text)
            Me.btnImportSaving.Text = Language._Object.getCaption(Me.btnImportSaving.Name, Me.btnImportSaving.Text)
            Me.btnSavingScheme.Text = Language._Object.getCaption(Me.btnSavingScheme.Name, Me.btnSavingScheme.Text)
            Me.btnEmployeeSavings.Text = Language._Object.getCaption(Me.btnEmployeeSavings.Name, Me.btnEmployeeSavings.Text)
            Me.btnSecRouteAssignment.Text = Language._Object.getCaption(Me.btnSecRouteAssignment.Name, Me.btnSecRouteAssignment.Text)
            Me.btnComputeProcess.Text = Language._Object.getCaption(Me.btnComputeProcess.Name, Me.btnComputeProcess.Text)
            Me.mnuBudget.Text = Language._Object.getCaption(Me.mnuBudget.Name, Me.mnuBudget.Text)
            Me.btnFundSources.Text = Language._Object.getCaption(Me.btnFundSources.Name, Me.btnFundSources.Text)
            Me.btnProjectCodeAdjustments.Text = Language._Object.getCaption(Me.btnProjectCodeAdjustments.Name, Me.btnProjectCodeAdjustments.Text)
            Me.btnBudgetFormula.Text = Language._Object.getCaption(Me.btnBudgetFormula.Name, Me.btnBudgetFormula.Text)
            Me.btnBudget.Text = Language._Object.getCaption(Me.btnBudget.Name, Me.btnBudget.Text)
            Me.btnFundActivity.Text = Language._Object.getCaption(Me.btnFundActivity.Name, Me.btnFundActivity.Text)
            Me.btnHearingList.Text = Language._Object.getCaption(Me.btnHearingList.Name, Me.btnHearingList.Text)
            Me.btnFundActivityAdjustment.Text = Language._Object.getCaption(Me.btnFundActivityAdjustment.Name, Me.btnFundActivityAdjustment.Text)
            Me.btnBudgetCodes.Text = Language._Object.getCaption(Me.btnBudgetCodes.Name, Me.btnBudgetCodes.Text)
            Me.btnFundProjectCode.Text = Language._Object.getCaption(Me.btnFundProjectCode.Name, Me.btnFundProjectCode.Text)
            Me.btnBgtApproverLevel.Text = Language._Object.getCaption(Me.btnBgtApproverLevel.Name, Me.btnBgtApproverLevel.Text)
            Me.btnBgtApproverMapping.Text = Language._Object.getCaption(Me.btnBgtApproverMapping.Name, Me.btnBgtApproverMapping.Text)
            Me.btnTimesheetApproverLevel.Text = Language._Object.getCaption(Me.btnTimesheetApproverLevel.Name, Me.btnTimesheetApproverLevel.Text)
            Me.btnExportPrintEmplForm.Text = Language._Object.getCaption(Me.btnExportPrintEmplForm.Name, Me.btnExportPrintEmplForm.Text)
            Me.btnTimesheetApproverMst.Text = Language._Object.getCaption(Me.btnTimesheetApproverMst.Name, Me.btnTimesheetApproverMst.Text)
            Me.btnEmployeeTimesheet.Text = Language._Object.getCaption(Me.btnEmployeeTimesheet.Name, Me.btnEmployeeTimesheet.Text)
            Me.btnTimesheetApproval.Text = Language._Object.getCaption(Me.btnTimesheetApproval.Name, Me.btnTimesheetApproval.Text)
            Me.btnTimesheetApproverMigration.Text = Language._Object.getCaption(Me.btnTimesheetApproverMigration.Name, Me.btnTimesheetApproverMigration.Text)
            Me.mnuEmpBudgetTimesheet.Text = Language._Object.getCaption(Me.mnuEmpBudgetTimesheet.Name, Me.mnuEmpBudgetTimesheet.Text)
			Me.btnSkillExpertise.Text = Language._Object.getCaption(Me.btnSkillExpertise.Name, Me.btnSkillExpertise.Text)
			Me.btnEmpApproverLevel.Text = Language._Object.getCaption(Me.btnEmpApproverLevel.Name, Me.btnEmpApproverLevel.Text)
			Me.btnEmployeeApprover.Text = Language._Object.getCaption(Me.btnEmployeeApprover.Name, Me.btnEmployeeApprover.Text)
			Me.btnApproveEmployee.Text = Language._Object.getCaption(Me.btnApproveEmployee.Name, Me.btnApproveEmployee.Text)
			Me.btnEmployeeDataApproval.Text = Language._Object.getCaption(Me.btnEmployeeDataApproval.Name, Me.btnEmployeeDataApproval.Text)
			Me.btnSystemErrorLog.Text = Language._Object.getCaption(Me.btnSystemErrorLog.Name, Me.btnSystemErrorLog.Text)
			Me.btnCountry.Text = Language._Object.getCaption(Me.btnCountry.Name, Me.btnCountry.Text)
			Me.btnMapOrbitParams.Text = Language._Object.getCaption(Me.btnMapOrbitParams.Name, Me.btnMapOrbitParams.Text)
			Me.btnGlobalInterviewAnalysis.Text = Language._Object.getCaption(Me.btnGlobalInterviewAnalysis.Name, Me.btnGlobalInterviewAnalysis.Text)
			Me.btnReportLanguage.Text = Language._Object.getCaption(Me.btnReportLanguage.Name, Me.btnReportLanguage.Text)
            Me.btnTransferStaffRequisitionApprover.Text = Language._Object.getCaption(Me.btnTransferStaffRequisitionApprover.Name, Me.btnTransferStaffRequisitionApprover.Text)
            Me.btnGarnishees.Text = Language._Object.getCaption(Me.btnGarnishees.Name, Me.btnGarnishees.Text)
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Leave Viewer")
            Language.setMessage(mstrModuleName, 3, "There are only [")
            Language.setMessage(mstrModuleName, 4, "] Days Left for expiry of your password.")
            Language.setMessage(mstrModuleName, 5, "Please change it or increase the notification date, Or contact Administrator.")
            Language.setMessage(mstrModuleName, 6, "Version")
            Language.setMessage(mstrModuleName, 7, "Sending payslip is in progress. Are you sure you want to exit?")
            Language.setMessage(mstrModuleName, 8, "You are logged out from the system. Reason : Application exceeded Idle time set in configuration. In order to relogin to system, Press Ok.")
            Language.setMessage(mstrModuleName, 9, "Are you sure you want to exit?")
            Language.setMessage(mstrModuleName, 12, "Searching...")
            Language.setMessage(mstrModuleName, 13, "Record(s) Found.")
			Language.setMessage(mstrModuleName, 14, "Sorry, You are forcefully logged out. Reason : You are in suspension period. Please contact Administrator.")
            Language.setMessage(mstrModuleName, 77, "Sending Email(s) process is in progress. Please wait for some time.")
            Language.setMessage(mstrModuleName, 123, "Sorry! You are forcefully logged out. Please Re-Login later.")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
