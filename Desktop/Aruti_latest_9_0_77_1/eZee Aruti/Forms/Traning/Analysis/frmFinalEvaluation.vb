﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'S.SANDEEP [ 24 FEB 2012 ] -- START
'ENHANCEMENT : TRA CHANGES [<TRAINING>]
Public Class frmFinalEvaluation

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmFinalEvaluation"
    Private mblnCancel As Boolean = True
    Private objAnalysisMaster As clsTraining_Analysis_Master
    Private objScheduling As clsTraining_Scheduling
    Private objEnrollemt As clsTraining_Enrollment_Tran
    Private objTrainers As clsTraining_Trainers_Tran
    Private objEmployee As clsEmployee_Master
    Private objAnaLysisTran As clsTraining_Analysis_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAnalysisMasterUnkid As Integer = -1
    Private mintEnrollmentUnkid As Integer = -1
    Private mdtAnalysisTran As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intEnrollId As Integer) As Boolean
        Try
            mintAnalysisMasterUnkid = intUnkId
            menAction = eAction
            mintEnrollmentUnkid = intEnrollId

            Me.ShowDialog()

            intUnkId = mintAnalysisMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboScore.BackColor = GUI.ColorComp
            cboTrainer.BackColor = GUI.ColorComp
            txtGPA.BackColor = GUI.ColorOptional
            txtRemark.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            dtpFinalDate.Value = ConfigParameter._Object._CurrentDateAndTime

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAnalysisMaster._Trainingenrolltranunkid = mintEnrollmentUnkid
            objAnalysisMaster._Isvoid = False
            objAnalysisMaster._Userinkid = User._Object._Userunkid
            objAnalysisMaster._Iscomplete = True
            objAnalysisMaster._Trainee_Remark = ""

            Dim dtRow As DataRow
            dtRow = mdtAnalysisTran.NewRow
            dtRow.Item("analysistranunkid") = -1
            dtRow.Item("analysisunkid") = mintAnalysisMasterUnkid
            dtRow.Item("trainerstranunkid") = CInt(cboTrainer.SelectedValue)
            dtRow.Item("resultunkid") = CInt(cboScore.SelectedValue)
            dtRow.Item("reviewer_remark") = txtRemark.Text
            dtRow.Item("analysis_date") = dtpFinalDate.Value
            dtRow.Item("AUD") = "A"
            dtRow.Item("GUID") = Guid.NewGuid.ToString
            dtRow.Item("gpa_value") = txtGPA.Text

            mdtAnalysisTran.Rows.Add(dtRow)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objResult As New clsresult_master
        Try
            dsList = objResult.getComboList("Result", True, objScheduling._Resultgroupunkid)
            With cboScore
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Result")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTrainers.getComboList(objScheduling._Trainingschedulingunkid,objEnrollemt._Trainingenrolltranunkid, True, "Trainers", True)

            dsList = objTrainers.getComboList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                                              objScheduling._Trainingschedulingunkid, _
                                              objEnrollemt._Trainingenrolltranunkid, True, "Trainers", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboTrainer
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Trainers")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillInfo()
        Try
            txtTaineeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            Dim objCMaster As New clsCommon_Master
            objCMaster._Masterunkid = CInt(objScheduling._Course_Title)
            txtCourseName.Text = objCMaster._Name & " ( " & objScheduling._Start_Date.ToShortDateString & " - " & objScheduling._End_Date.ToShortDateString & " )"

            objCMaster._Masterunkid = objScheduling._Qualificationgroupunkid
            txtQualificationGroup.Text = objCMaster._Name
            objCMaster = Nothing

            Dim objQMaster As New clsqualification_master
            objQMaster._Qualificationunkid = objScheduling._Qualificationunkid
            txtQualification.Text = objQMaster._Qualificationname
            objQMaster = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            'If CInt(cboTrainer.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Trainer is compulsory information. Please select Trainer to continue."), enMsgBoxStyle.Information)
            '    cboTrainer.Focus()
            '    Return False
            'End If

            If CInt(cboScore.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Score is compulsory information. Please select Score to continue."), enMsgBoxStyle.Information)
                cboScore.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmFinalEvaluation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            ElseIf Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFinalEvaluation_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmFinalEvaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAnalysisMaster = New clsTraining_Analysis_Master
        objScheduling = New clsTraining_Scheduling
        objEnrollemt = New clsTraining_Enrollment_Tran
        objTrainers = New clsTraining_Trainers_Tran
        objEmployee = New clsEmployee_Master
        objAnaLysisTran = New clsTraining_Analysis_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()

            If menAction = enAction.EDIT_ONE Then
                objAnalysisMaster._Analysisunkid = mintAnalysisMasterUnkid
                objEnrollemt._Trainingenrolltranunkid = objAnalysisMaster._Trainingenrolltranunkid
                objScheduling._Trainingschedulingunkid = objEnrollemt._Trainingschedulingunkid

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = objEnrollemt._Employeeunkid
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objEnrollemt._Employeeunkid
                'S.SANDEEP [04 JUN 2015] -- END

            Else
                objEnrollemt._Trainingenrolltranunkid = mintEnrollmentUnkid
                objScheduling._Trainingschedulingunkid = objEnrollemt._Trainingschedulingunkid

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = objEnrollemt._Employeeunkid
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objEnrollemt._Employeeunkid
                'S.SANDEEP [04 JUN 2015] -- END

            End If

            Call FillCombo()
            Call FillInfo()

            objAnaLysisTran._AnalysisUnkid = mintAnalysisMasterUnkid
            mdtAnalysisTran = objAnaLysisTran._DataTable

            txtTaineeName.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFinalEvaluation_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmFinalEvaluation_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAnalysisMaster = Nothing
        objScheduling = Nothing
        objEnrollemt = Nothing
        objTrainers = Nothing
        objEmployee = Nothing
        objAnaLysisTran = Nothing
    End Sub

    Private Sub frmFinalEvaluation_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Analysis_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsTraining_Analysis_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objAnalysisMaster._FormName = mstrModuleName
            objAnalysisMaster._LoginEmployeeunkid = 0
            objAnalysisMaster._ClientIP = getIP()
            objAnalysisMaster._HostName = getHostName()
            objAnalysisMaster._FromWeb = False
            objAnalysisMaster._AuditUserId = User._Object._Userunkid
objAnalysisMaster._CompanyUnkid = Company._Object._Companyunkid
            objAnalysisMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAnalysisMaster.Update(mdtAnalysisTran, True)
            Else
                blnFlag = objAnalysisMaster.Insert(mdtAnalysisTran, True)
            End If


            'S.SANDEEP [19-MAY-2017] -- START
            If blnFlag Then
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Call objAnalysisMaster.SendNotification(CInt(objScheduling._Trainingschedulingunkid), objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username)
                Call objAnalysisMaster.SendNotification(CInt(objScheduling._Trainingschedulingunkid), objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username, Company._Object._Companyunkid)
                'Sohail (30 Nov 2017) -- End
            End If
            'S.SANDEEP [19-MAY-2017] -- END

            mblnCancel = True

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbTrainingAnalysis.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTrainingAnalysis.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbTrainingAnalysis.Text = Language._Object.getCaption(Me.gbTrainingAnalysis.Name, Me.gbTrainingAnalysis.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblCourseName.Text = Language._Object.getCaption(Me.lblCourseName.Name, Me.lblCourseName.Text)
			Me.lblTraineeName.Text = Language._Object.getCaption(Me.lblTraineeName.Name, Me.lblTraineeName.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblScore.Text = Language._Object.getCaption(Me.lblScore.Name, Me.lblScore.Text)
			Me.lblTrainer.Text = Language._Object.getCaption(Me.lblTrainer.Name, Me.lblTrainer.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblQualificationAward.Text = Language._Object.getCaption(Me.lblQualificationAward.Name, Me.lblQualificationAward.Text)
			Me.lblTraningCourse.Text = Language._Object.getCaption(Me.lblTraningCourse.Name, Me.lblTraningCourse.Text)
			Me.elCourseInfo.Text = Language._Object.getCaption(Me.elCourseInfo.Name, Me.elCourseInfo.Text)
			Me.elEvaluationInfo.Text = Language._Object.getCaption(Me.elEvaluationInfo.Name, Me.elEvaluationInfo.Text)
			Me.lblGPA.Text = Language._Object.getCaption(Me.lblGPA.Name, Me.lblGPA.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Score is compulsory information. Please select Score to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'Public Class frmFinalEvaluation

'#Region " Private Varaibles "
'    Private ReadOnly mstrModuleName As String = "frmFinalEvaluation"
'    Private mblnCancel As Boolean = True
'    Private objAnalysisMaster As clsTraining_Analysis_Master
'    Private objScheduling As clsTraining_Scheduling
'    Private objEnrollemt As clsTraining_Enrollment_Tran
'    Private objTrainers As clsTraining_Trainers_Tran
'    Private objEmployee As clsEmployee_Master
'    Private objAnaLysisTran As clsTraining_Analysis_Tran
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintAnalysisMasterUnkid As Integer = -1
'    Private mintEnrollmentUnkid As Integer = -1
'    Private mdtAnalysisTran As DataTable
'#End Region

'#Region " Display Dialog "
'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intEnrollId As Integer) As Boolean
'        Try
'            mintAnalysisMasterUnkid = intUnkId
'            menAction = eAction
'            mintEnrollmentUnkid = intEnrollId

'            Me.ShowDialog()

'            intUnkId = mintAnalysisMasterUnkid

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function
'#End Region

'#Region " Private Methods "
'    Private Sub SetColor()
'        Try
'            txtCourseName.BackColor = GUI.ColorComp
'            txtRemark.BackColor = GUI.ColorOptional
'            txtTaineeName.BackColor = GUI.ColorComp
'            cboEmployee.BackColor = GUI.ColorComp
'            cboScore.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try

'            'Sandeep [ 02 Oct 2010 ] -- Start
'            'If objAnalysisMaster._Analysis_Date <> Nothing Then
'            '    dtpFinalDate.Value = objAnalysisMaster._Analysis_Date
'            'End If
'            dtpFinalDate.Value = Now
'            'Sandeep [ 02 Oct 2010 ] -- End 


'            'S.SANDEEP [ 24 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
'            'chkMarkAsComplete.Checked = objAnalysisMaster._Iscomplete
'            'S.SANDEEP [ 24 FEB 2012 ] -- END 


'            For Each dtRow As DataRow In mdtAnalysisTran.Rows
'                objAnalysisMaster._Analysisunkid = CInt(dtRow.Item("analysisunkid"))
'                If objAnalysisMaster._Iscomplete = True Then
'                    cboEmployee.SelectedValue = CInt(dtRow.Item("trainerstranunkid"))
'                    cboScore.SelectedValue = CInt(dtRow.Item("resultunkid"))
'                    txtRemark.Text = dtRow.Item("reviewer_remark").ToString
'                End If
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try

'            'Sandeep [ 02 Oct 2010 ] -- Start
'            'objAnalysisMaster._Analysis_Date = dtpFinalDate.Value
'            'Sandeep [ 02 Oct 2010 ] -- End 

'            'Sandeep [ 21 Aug 2010 ] -- Start
'            'objAnalysisMaster._Iscomplete = chkMarkAsComplete.Checked
'            objAnalysisMaster._Iscomplete = True
'            'Sandeep [ 21 Aug 2010 ] -- End 
'            objAnalysisMaster._Trainingenrolltranunkid = mintEnrollmentUnkid
'            Dim dtRow As DataRow
'            dtRow = mdtAnalysisTran.NewRow
'            dtRow.Item("analysistranunkid") = -1
'            dtRow.Item("analysisunkid") = mintAnalysisMasterUnkid
'            dtRow.Item("trainerstranunkid") = CInt(cboEmployee.SelectedValue)
'            dtRow.Item("resultunkid") = CInt(cboScore.SelectedValue)
'            dtRow.Item("reviewer_remark") = txtRemark.Text
'            dtRow.Item("analysis_date") = dtpFinalDate.Value
'                dtRow.Item("AUD") = "A"
'            dtRow.Item("GUID") = Guid.NewGuid.ToString

'            mdtAnalysisTran.Rows.Add(dtRow)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim dsList As New DataSet
'        Dim objResult As New clsresult_master
'        Try
'            dsList = objResult.getComboList("Result", True, objScheduling._Resultgroupunkid)
'            With cboScore
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsList.Tables("Result")
'                .SelectedValue = 0
'            End With

'            dsList = objTrainers.getComboList(objScheduling._Trainingschedulingunkid, objEnrollemt._Trainingenrolltranunkid, True, "Trainers", True)
'            With cboEmployee
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsList.Tables("Trainers")
'                .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    'Private Sub FillList()
'    '    Dim dsList As New DataSet
'    '    Dim lvItem As ListViewItem
'    '    Try
'    '        dsList = objAnalysisMaster.Get_Analysis_Data("AnalysisData", mintEnrollmentUnkid)

'    '        lvFinalEvaluation.Items.Clear()
'    '        For Each dtRow As DataRow In dsList.Tables("AnalysisData").Rows
'    '            lvItem = New ListViewItem

'    '            lvItem.Text = eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString
'    '            lvItem.SubItems.Add(dtRow.Item("Reviewer").ToString)
'    '            lvItem.SubItems.Add(dtRow.Item("Score").ToString)
'    '            lvItem.SubItems.Add(dtRow.Item("reviewer_remark").ToString)

'    '            lvFinalEvaluation.Items.Add(lvItem)

'    '            lvItem = Nothing
'    '        Next

'    '        lvFinalEvaluation.GroupingColumn = objcolhDate
'    '        lvFinalEvaluation.DisplayGroups(True)

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'    '    End Try
'    'End Sub

'    Private Sub FillInfo()
'        Try
'            txtTaineeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
'            txtCourseName.Text = objScheduling._Course_Title
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
'        End Try
'    End Sub

'    Private Function IsValid() As Boolean
'        Try
'            If CInt(cboEmployee.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Trainer is compulsory information. Please select Trainer to continue."), enMsgBoxStyle.Information)
'                cboEmployee.Focus()
'                Return False
'            End If

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
'        End Try
'    End Function

'    Private Sub Insert_Qualification()
'        Try
'            Dim objQualification As New clsEmp_Qualification_Tran
'            objQualification._Employeeunkid = objEnrollemt._Employeeunkid
'            objQualification._Instituteunkid = objScheduling._Traninginstituteunkid
'            objQualification._Reference_No = ""
'            objQualification._Transaction_Date = dtpFinalDate.Value
'            objQualification._Qualificationgroupunkid = objScheduling._Qualificationgroupunkid
'            objQualification._Qualificationunkid = objScheduling._Qualificationunkid
'            objQualification.Insert()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Insert_Qualification", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Form's Events "
'    Private Sub frmFinalEvaluation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        If Asc(e.KeyChar) = 27 Then
'            Me.Close()
'        ElseIf Asc(e.KeyChar) = 13 Then
'            Windows.Forms.SendKeys.Send("{Tab}")
'            e.Handled = True
'        End If
'    End Sub

'    Private Sub frmFinalEvaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objAnalysisMaster = New clsTraining_Analysis_Master
'        objScheduling = New clsTraining_Scheduling
'        objEnrollemt = New clsTraining_Enrollment_Tran
'        objTrainers = New clsTraining_Trainers_Tran
'        objEmployee = New clsEmployee_Master
'        objAnaLysisTran = New clsTraining_Analysis_Tran
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'S.SANDEEP [ 20 AUG 2011 ] -- END

'            If menAction = enAction.EDIT_ONE Then
'                objAnalysisMaster._Analysisunkid = mintAnalysisMasterUnkid
'                objEnrollemt._Trainingenrolltranunkid = objAnalysisMaster._Trainingenrolltranunkid
'                objScheduling._Trainingschedulingunkid = objEnrollemt._Trainingschedulingunkid
'                objEmployee._Employeeunkid = objEnrollemt._Employeeunkid
'            Else
'                objEnrollemt._Trainingenrolltranunkid = mintEnrollmentUnkid
'                objScheduling._Trainingschedulingunkid = objEnrollemt._Trainingschedulingunkid
'                objEmployee._Employeeunkid = objEnrollemt._Employeeunkid
'            End If
'            Call FillCombo()
'            'Call FillList()
'            Call FillInfo()

'            objAnaLysisTran._AnalysisUnkid = mintAnalysisMasterUnkid
'            mdtAnalysisTran = objAnaLysisTran._DataTable

'            Call GetValue()

'            'If lvFinalEvaluation.Items.Count > 0 Then lvFinalEvaluation.Items(0).Selected = True
'            'lvFinalEvaluation.Select()

'            txtTaineeName.Focus()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmFinalEvaluation_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmFinalEvaluation_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objAnalysisMaster = Nothing
'        objScheduling = Nothing
'        objEnrollemt = Nothing
'        objTrainers = Nothing
'        objEmployee = Nothing
'        objAnaLysisTran = Nothing
'    End Sub

'    Private Sub frmFinalEvaluation_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
'            Call btnSave.PerformClick()
'        End If
'    End Sub

'    'S.SANDEEP [ 20 AUG 2011 ] -- START
'    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsTraining_Analysis_Master.SetMessages()
'            objfrm._Other_ModuleNames = "clsTraining_Analysis_Master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 20 AUG 2011 ] -- END
'#End Region

'#Region " Button's Events "
'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If IsValid() = False Then Exit Sub

'            Call SetValue()

'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objAnalysisMaster.Update(mdtAnalysisTran, True)
'            Else
'                blnFlag = objAnalysisMaster.Insert(mdtAnalysisTran, True)
'            End If

'            If blnFlag = True Then
'                If objAnalysisMaster.IsAlreadyQualified(objScheduling._Qualificationunkid, objEnrollemt._Employeeunkid) = True Then
'                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Particular Employee is already having this qualification." & vbCrLf & _
'                                       "Do you still want to add this qualification."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                        Call Insert_Qualification()
'                    End If
'                Else
'                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Do you want to update this qualification to particular employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                        Call Insert_Qualification()
'                    End If
'                End If
'                mblnCancel = True
'                Me.Close()
'            End If



'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try

'    End Sub
'#End Region



'End Class

'S.SANDEEP [ 24 FEB 2012 ] -- END 