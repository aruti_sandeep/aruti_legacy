﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFeedbackItemList

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmFeedbackItemList"
    Private objFeedBackItem As clshrtnafdbk_item_master

#End Region

#Region "Form's Event"

    Private Sub frmFeedbackItemList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFeedBackItem = New clshrtnafdbk_item_master
        Try
            Call Set_Logo(Me, gApplicationType)
            
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            FillCombo()
            fillList()
            Call SetVisibility()

            If lvFeedbackItem.Items.Count > 0 Then lvFeedbackItem.Items(0).Selected = True
            lvFeedbackItem.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackItemList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFeedbackItemList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvFeedbackItem.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFeedbackItemList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFeedbackItemList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objFeedBackItem = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnafdbk_item_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnafdbk_item_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmFeedbackItems_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvFeedbackItem.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Feedback Item from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvFeedbackItem.Select()
                Exit Sub
            End If
            Dim frm As New frmFeedbackItems_AddEdit
            Try
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                If frm.displayDialog(CInt(lvFeedbackItem.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                frm = Nothing

            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If frm IsNot Nothing Then frm.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvFeedbackItem.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Feedback Item from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvFeedbackItem.Select()
            Exit Sub
        End If
        If objFeedBackItem.isUsed(CInt(lvFeedbackItem.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Feedback Item. Reason: This Feedback Item is in use."), enMsgBoxStyle.Information) '?2
            lvFeedbackItem.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvFeedbackItem.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Feedback Item?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objFeedBackItem._FormName = mstrModuleName
                objFeedBackItem._LoginEmployeeunkid = 0
                objFeedBackItem._ClientIP = getIP()
                objFeedBackItem._HostName = getHostName()
                objFeedBackItem._FromWeb = False
                objFeedBackItem._AuditUserId = User._Object._Userunkid
objFeedBackItem._CompanyUnkid = Company._Object._Companyunkid
                objFeedBackItem._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objFeedBackItem.Delete(CInt(lvFeedbackItem.SelectedItems(0).Tag))
                lvFeedbackItem.SelectedItems(0).Remove()

                If lvFeedbackItem.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
            lvFeedbackItem.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboFeedbackGroup.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        If cboFeedbackGroup.DataSource Is Nothing Then Exit Sub
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboFeedbackGroup.ValueMember
                .DisplayMember = cboFeedbackGroup.DisplayMember
                .DataSource = CType(cboFeedbackGroup.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboFeedbackGroup.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsGroup As DataSet = Nothing
        Try
            Dim objFeedBackGroup As New clshrtnafdbk_group_master
            dsGroup = objFeedBackGroup.getComboList("Group", True)
            cboFeedbackGroup.ValueMember = "fdbkgroupunkid"
            cboFeedbackGroup.DisplayMember = "name"
            cboFeedbackGroup.DataSource = dsGroup.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsFeedbackItem As New DataSet
        Dim strSearching As String = ""
        Dim dtFeedbackItem As New DataTable
        Try

            If User._Object.Privilege._AllowToViewEvaluationItemList = True Then 'Pinkal (09-Jul-2012) -- Start

            dsFeedbackItem = objFeedBackItem.GetList("FeedbackItem")

            If CInt(cboFeedbackGroup.SelectedValue) > 0 Then
                strSearching = "AND fdbkgroupunkid =" & CInt(cboFeedbackGroup.SelectedValue) & " "
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtFeedbackItem = New DataView(dsFeedbackItem.Tables("FeedbackItem"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtFeedbackItem = dsFeedbackItem.Tables("FeedbackItem")
            End If

            Dim lvItem As ListViewItem

            lvFeedbackItem.Items.Clear()
            For Each drRow As DataRow In dtFeedbackItem.Rows
                lvItem = New ListViewItem

                lvItem.Text = drRow("groupname").ToString
                lvItem.SubItems.Add(drRow("code").ToString)
                lvItem.SubItems.Add(drRow("name").ToString)
                lvItem.SubItems.Add(drRow("description").ToString)
                lvItem.Tag = drRow("fdbkitemunkid")

                lvFeedbackItem.Items.Add(lvItem)
            Next

            If lvFeedbackItem.Items.Count > 10 Then
                colhDescription.Width = 214 - 18
            Else
                colhDescription.Width = 214
            End If


            End If


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsFeedbackItem.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            btnNew.Enabled = User._Object.Privilege._AllowToAddEvaluationItems
            btnEdit.Enabled = User._Object.Privilege._AllowToEditEvaluationItems
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEvaluationItems
            'S.SANDEEP [ 16 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
			Me.colhGroup.Text = Language._Object.getCaption(CStr(Me.colhGroup.Tag), Me.colhGroup.Text)
			Me.colhItemCode.Text = Language._Object.getCaption(CStr(Me.colhItemCode.Tag), Me.colhItemCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Feedback Item from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Feedback Item. Reason: This Feedback Item is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Feedback Item?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class