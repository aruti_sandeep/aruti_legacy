﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeFeedbackList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeFeedbackList"
    Private objFMaster As clshrtnafeedback_master

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objTSch As New clsTraining_Scheduling
        Try
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'Sohail (23 Nov 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objTSch.getComboList("List", True, FinancialYear._Object._YearUnkid, True)
            With cboCourseTitle
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objEmp = Nothing : objTSch = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim lvItem As ListViewItem
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try

            If User._Object.Privilege._AllowToViewLevel1EvaluationList = True Then                'Pinkal (02-Jul-2012) -- Start

                dsList = objFMaster.GetList("List")

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
                End If

                If CInt(cboCourseTitle.SelectedValue) > 0 Then
                    StrSearching &= "AND trainingschedulingunkid = '" & CInt(cboCourseTitle.SelectedValue) & "' "
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    dtTable = New DataView(dsList.Tables(0), StrSearching, "TrainingAttended", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsList.Tables(0), "", "TrainingAttended", DataViewRowState.CurrentRows).ToTable
                End If

                lvFeedbackList.Items.Clear()

                For Each dRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = eZeeDate.convertDate(dRow.Item("feedbackdate").ToString).ToShortDateString
                    lvItem.SubItems.Add(dRow.Item("ECode").ToString)
                    lvItem.SubItems.Add(dRow.Item("EName").ToString)
                    lvItem.SubItems.Add(dRow.Item("TrainingAttended").ToString)

                    lvItem.Tag = dRow.Item("feedbackmasterunkid")

                    If CBool(dRow.Item("iscomplete")) = True Then lvItem.ForeColor = Color.Blue

                    lvFeedbackList.Items.Add(lvItem)
                Next

                If lvFeedbackList.Items.Count > 2 Then
                    colhEName.Width = 433 - 20
                Else
                    colhEName.Width = 433
                End If

                lvFeedbackList.GroupingColumn = objcolhTitle
                lvFeedbackList.DisplayGroups(True)

                lvFeedbackList.GridLines = False

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Public Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddLevelIEvaluation
            btnEdit.Enabled = User._Object.Privilege._AllowToEditLevelIEvaluation
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteLevelIEvaluation
            mnuPreview.Enabled = User._Object.Privilege._AllowToPreviewLevelIEvaluation
            mnuPrint.Enabled = User._Object.Privilege._AllowToPrintLevelIEvaluation
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeFeedbackList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFMaster = New clshrtnafeedback_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            FillCombo()

            If lvFeedbackList.Items.Count > 0 Then lvFeedbackList.Items(0).Selected = True
            lvFeedbackList.Select()

            lvFeedbackList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeFeedbackList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeFeedbackList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeFeedbackList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeFeedbackList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objFMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnafeedback_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnafeedback_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeFeedback
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvFeedbackList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Feedback to perform operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Dim frm As New frmEmployeeFeedback
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvFeedbackList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvFeedbackList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Feedback to perform operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Feedback?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objFMaster._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objFMaster._Isvoid = True
                objFMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objFMaster._Voiduserunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objFMaster._FormName = mstrModuleName
                objFMaster._LoginEmployeeunkid = 0
                objFMaster._ClientIP = getIP()
                objFMaster._HostName = getHostName()
                objFMaster._FromWeb = False
                objFMaster._AuditUserId = User._Object._Userunkid
objFMaster._CompanyUnkid = Company._Object._Companyunkid
                objFMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objFMaster.Delete(CInt(lvFeedbackList.SelectedItems(0).Tag))

                If objFMaster._Message <> "" Then
                    eZeeMsgBox.Show(objFMaster._Message, enMsgBoxStyle.Information)
                Else
                    lvFeedbackList.SelectedItems(0).Remove()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboCourseTitle.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCourseTitle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourseTitle.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboCourseTitle.ValueMember
            frm.DisplayMember = cboCourseTitle.DisplayMember
            frm.DataSource = CType(cboCourseTitle.DataSource, DataTable)

            If frm.DisplayDialog Then
                cboCourseTitle.SelectedValue = frm.SelectedValue
                cboCourseTitle.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCourseTitle_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvFeedbackList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvFeedbackList.SelectedIndexChanged
        Try
            If lvFeedbackList.SelectedItems.Count > 0 Then
                If lvFeedbackList.SelectedItems(0).ForeColor = Color.Blue Then
                    btnEdit.Enabled = False : btnDelete.Enabled = False
                Else
                    btnEdit.Enabled = True : btnDelete.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvFeedbackList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 04 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreview.Click
        If lvFeedbackList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Feedback to perform operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            Dim objEvaluationLevelI As New ArutiReports.clsTrainingEvaluationLevelI_Report
            objEvaluationLevelI.SetDefaultValue()
            objEvaluationLevelI._FeebackMasterId = CInt(lvFeedbackList.SelectedItems(0).Tag)
            objEvaluationLevelI.generateReport(0, enPrintAction.Preview, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreview_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If lvFeedbackList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select at least one Feedback to perform operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            Dim objEvaluationLevelI As New ArutiReports.clsTrainingEvaluationLevelI_Report
            objEvaluationLevelI.SetDefaultValue()
            objEvaluationLevelI._FeebackMasterId = CInt(lvFeedbackList.SelectedItems(0).Tag)
            objEvaluationLevelI.generateReport(0, enPrintAction.Print, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrint_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 04 APRIL 2012 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
            Me.lblCourseTitle.Text = Language._Object.getCaption(Me.lblCourseTitle.Name, Me.lblCourseTitle.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhFeedbackDate.Text = Language._Object.getCaption(CStr(Me.colhFeedbackDate.Tag), Me.colhFeedbackDate.Text)
            Me.colhECode.Text = Language._Object.getCaption(CStr(Me.colhECode.Tag), Me.colhECode.Text)
            Me.colhEName.Text = Language._Object.getCaption(CStr(Me.colhEName.Tag), Me.colhEName.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)
            Me.mnuPrint.Text = Language._Object.getCaption(Me.mnuPrint.Name, Me.mnuPrint.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select at least one Feedback to perform operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Feedback?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class