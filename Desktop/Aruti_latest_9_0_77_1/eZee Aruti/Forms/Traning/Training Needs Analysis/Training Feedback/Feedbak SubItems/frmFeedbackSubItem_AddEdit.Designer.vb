﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFeedbackSubItem_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFeedbackSubItem_AddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbFeedbackSubItems = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.cboFeedbackGroup = New System.Windows.Forms.ComboBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.objbtnSearchItems = New eZee.Common.eZeeGradientButton
        Me.objbtnAddFeedbackItem = New eZee.Common.eZeeGradientButton
        Me.cboFeedbackItem = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblAssessmentDescription = New System.Windows.Forms.Label
        Me.lblAssessmentItem = New System.Windows.Forms.Label
        Me.lblAssessmentItemCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbFeedbackSubItems.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbFeedbackSubItems)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(405, 315)
        Me.pnlMain.TabIndex = 0
        '
        'gbFeedbackSubItems
        '
        Me.gbFeedbackSubItems.BorderColor = System.Drawing.Color.Black
        Me.gbFeedbackSubItems.Checked = False
        Me.gbFeedbackSubItems.CollapseAllExceptThis = False
        Me.gbFeedbackSubItems.CollapsedHoverImage = Nothing
        Me.gbFeedbackSubItems.CollapsedNormalImage = Nothing
        Me.gbFeedbackSubItems.CollapsedPressedImage = Nothing
        Me.gbFeedbackSubItems.CollapseOnLoad = False
        Me.gbFeedbackSubItems.Controls.Add(Me.objbtnAddGroup)
        Me.gbFeedbackSubItems.Controls.Add(Me.cboFeedbackGroup)
        Me.gbFeedbackSubItems.Controls.Add(Me.lblGroup)
        Me.gbFeedbackSubItems.Controls.Add(Me.objbtnSearchItems)
        Me.gbFeedbackSubItems.Controls.Add(Me.objbtnAddFeedbackItem)
        Me.gbFeedbackSubItems.Controls.Add(Me.cboFeedbackItem)
        Me.gbFeedbackSubItems.Controls.Add(Me.lblResultGroup)
        Me.gbFeedbackSubItems.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbFeedbackSubItems.Controls.Add(Me.txtDescription)
        Me.gbFeedbackSubItems.Controls.Add(Me.txtName)
        Me.gbFeedbackSubItems.Controls.Add(Me.txtCode)
        Me.gbFeedbackSubItems.Controls.Add(Me.lblAssessmentDescription)
        Me.gbFeedbackSubItems.Controls.Add(Me.lblAssessmentItem)
        Me.gbFeedbackSubItems.Controls.Add(Me.lblAssessmentItemCode)
        Me.gbFeedbackSubItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFeedbackSubItems.ExpandedHoverImage = Nothing
        Me.gbFeedbackSubItems.ExpandedNormalImage = Nothing
        Me.gbFeedbackSubItems.ExpandedPressedImage = Nothing
        Me.gbFeedbackSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFeedbackSubItems.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFeedbackSubItems.HeaderHeight = 25
        Me.gbFeedbackSubItems.HeaderMessage = ""
        Me.gbFeedbackSubItems.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFeedbackSubItems.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFeedbackSubItems.HeightOnCollapse = 0
        Me.gbFeedbackSubItems.LeftTextSpace = 0
        Me.gbFeedbackSubItems.Location = New System.Drawing.Point(0, 0)
        Me.gbFeedbackSubItems.Name = "gbFeedbackSubItems"
        Me.gbFeedbackSubItems.OpenHeight = 119
        Me.gbFeedbackSubItems.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFeedbackSubItems.ShowBorder = True
        Me.gbFeedbackSubItems.ShowCheckBox = False
        Me.gbFeedbackSubItems.ShowCollapseButton = False
        Me.gbFeedbackSubItems.ShowDefaultBorderColor = True
        Me.gbFeedbackSubItems.ShowDownButton = False
        Me.gbFeedbackSubItems.ShowHeader = True
        Me.gbFeedbackSubItems.Size = New System.Drawing.Size(405, 260)
        Me.gbFeedbackSubItems.TabIndex = 3
        Me.gbFeedbackSubItems.Temp = 0
        Me.gbFeedbackSubItems.Text = "Feedback Sub Items"
        Me.gbFeedbackSubItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(353, 34)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 222
        '
        'cboFeedbackGroup
        '
        Me.cboFeedbackGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFeedbackGroup.DropDownWidth = 450
        Me.cboFeedbackGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFeedbackGroup.FormattingEnabled = True
        Me.cboFeedbackGroup.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboFeedbackGroup.Location = New System.Drawing.Point(110, 34)
        Me.cboFeedbackGroup.Name = "cboFeedbackGroup"
        Me.cboFeedbackGroup.Size = New System.Drawing.Size(237, 21)
        Me.cboFeedbackGroup.TabIndex = 0
        '
        'lblGroup
        '
        Me.lblGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup.Location = New System.Drawing.Point(7, 37)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(97, 15)
        Me.lblGroup.TabIndex = 221
        Me.lblGroup.Text = "Feedback Group"
        Me.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchItems
        '
        Me.objbtnSearchItems.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchItems.BorderSelected = False
        Me.objbtnSearchItems.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchItems.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchItems.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchItems.Location = New System.Drawing.Point(377, 61)
        Me.objbtnSearchItems.Name = "objbtnSearchItems"
        Me.objbtnSearchItems.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchItems.TabIndex = 218
        '
        'objbtnAddFeedbackItem
        '
        Me.objbtnAddFeedbackItem.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddFeedbackItem.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddFeedbackItem.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddFeedbackItem.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddFeedbackItem.BorderSelected = False
        Me.objbtnAddFeedbackItem.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddFeedbackItem.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddFeedbackItem.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddFeedbackItem.Location = New System.Drawing.Point(353, 61)
        Me.objbtnAddFeedbackItem.Name = "objbtnAddFeedbackItem"
        Me.objbtnAddFeedbackItem.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddFeedbackItem.TabIndex = 216
        '
        'cboFeedbackItem
        '
        Me.cboFeedbackItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFeedbackItem.DropDownWidth = 450
        Me.cboFeedbackItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFeedbackItem.FormattingEnabled = True
        Me.cboFeedbackItem.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboFeedbackItem.Location = New System.Drawing.Point(110, 61)
        Me.cboFeedbackItem.Name = "cboFeedbackItem"
        Me.cboFeedbackItem.Size = New System.Drawing.Size(237, 21)
        Me.cboFeedbackItem.TabIndex = 1
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(7, 64)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(97, 15)
        Me.lblResultGroup.TabIndex = 214
        Me.lblResultGroup.Text = "Feedback Item"
        Me.lblResultGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(353, 115)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(110, 183)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(237, 70)
        Me.txtDescription.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(110, 115)
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtName.Size = New System.Drawing.Size(237, 62)
        Me.txtName.TabIndex = 3
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(110, 88)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(237, 21)
        Me.txtCode.TabIndex = 2
        '
        'lblAssessmentDescription
        '
        Me.lblAssessmentDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentDescription.Location = New System.Drawing.Point(7, 187)
        Me.lblAssessmentDescription.Name = "lblAssessmentDescription"
        Me.lblAssessmentDescription.Size = New System.Drawing.Size(97, 15)
        Me.lblAssessmentDescription.TabIndex = 4
        Me.lblAssessmentDescription.Text = "Description"
        Me.lblAssessmentDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessmentItem
        '
        Me.lblAssessmentItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItem.Location = New System.Drawing.Point(7, 118)
        Me.lblAssessmentItem.Name = "lblAssessmentItem"
        Me.lblAssessmentItem.Size = New System.Drawing.Size(97, 15)
        Me.lblAssessmentItem.TabIndex = 3
        Me.lblAssessmentItem.Text = "Name"
        Me.lblAssessmentItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessmentItemCode
        '
        Me.lblAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItemCode.Location = New System.Drawing.Point(7, 91)
        Me.lblAssessmentItemCode.Name = "lblAssessmentItemCode"
        Me.lblAssessmentItemCode.Size = New System.Drawing.Size(97, 15)
        Me.lblAssessmentItemCode.TabIndex = 2
        Me.lblAssessmentItemCode.Text = "Code"
        Me.lblAssessmentItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 260)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(405, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(193, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(296, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmFeedbackSubItem_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(405, 315)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFeedbackSubItem_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Feedback Sub Item"
        Me.pnlMain.ResumeLayout(False)
        Me.gbFeedbackSubItems.ResumeLayout(False)
        Me.gbFeedbackSubItems.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFeedbackSubItems As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFeedbackGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchItems As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddFeedbackItem As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFeedbackItem As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAssessmentDescription As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentItem As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentItemCode As System.Windows.Forms.Label
End Class
