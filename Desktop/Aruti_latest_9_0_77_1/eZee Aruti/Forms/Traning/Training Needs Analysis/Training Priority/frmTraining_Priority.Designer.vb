﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTraining_Priority
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTraining_Priority))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlTraningInfo = New System.Windows.Forms.Panel
        Me.pnlOtherDetail = New System.Windows.Forms.Panel
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkPerformanceEvalutation = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblRemark = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeGradientButton
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.cboCourse = New System.Windows.Forms.ComboBox
        Me.lblCourse = New System.Windows.Forms.Label
        Me.objbtnSearchCourse = New eZee.Common.eZeeGradientButton
        Me.pnlRefCombo = New System.Windows.Forms.Panel
        Me.txtReferenceNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblReferenceNo = New System.Windows.Forms.Label
        Me.gbTrainingPriority = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnHide = New eZee.Common.eZeeGradientButton
        Me.objbtnShow = New eZee.Common.eZeeGradientButton
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlGrid = New System.Windows.Forms.Panel
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.dgvTrainingPriority = New System.Windows.Forms.DataGridView
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSaveExport = New eZee.Common.eZeeSplitButton
        Me.mnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuExportWithName = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportWithOutName = New System.Windows.Forms.ToolStripMenuItem
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeSplitButton
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFinalSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhECollapse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhTrainingCourse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTotEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGAP = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhBusinessPriority = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhKRA = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRanking = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhTraningSol = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTraningObjective = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProvider = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhtrainingVenue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGoal = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhemployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTrainingCourseId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSkill_Gap = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhbusiness = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProvider = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStartegicGoal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.pnlTraningInfo.SuspendLayout()
        Me.pnlOtherDetail.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlRefCombo.SuspendLayout()
        Me.gbTrainingPriority.SuspendLayout()
        Me.pnlGrid.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTrainingPriority, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.mnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.SplitContainer1)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(934, 509)
        Me.pnlMain.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.gbInfo)
        Me.SplitContainer1.Panel1MinSize = 0
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbTrainingPriority)
        Me.SplitContainer1.Size = New System.Drawing.Size(934, 454)
        Me.SplitContainer1.SplitterDistance = 358
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 111
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.pnlTraningInfo)
        Me.gbInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(358, 454)
        Me.gbInfo.TabIndex = 109
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Training Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTraningInfo
        '
        Me.pnlTraningInfo.AutoScroll = True
        Me.pnlTraningInfo.Controls.Add(Me.pnlOtherDetail)
        Me.pnlTraningInfo.Controls.Add(Me.pnlRefCombo)
        Me.pnlTraningInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlTraningInfo.Location = New System.Drawing.Point(2, 26)
        Me.pnlTraningInfo.Name = "pnlTraningInfo"
        Me.pnlTraningInfo.Size = New System.Drawing.Size(353, 426)
        Me.pnlTraningInfo.TabIndex = 247
        '
        'pnlOtherDetail
        '
        Me.pnlOtherDetail.Controls.Add(Me.btnAdd)
        Me.pnlOtherDetail.Controls.Add(Me.chkPerformanceEvalutation)
        Me.pnlOtherDetail.Controls.Add(Me.lblPeriod)
        Me.pnlOtherDetail.Controls.Add(Me.lblRemark)
        Me.pnlOtherDetail.Controls.Add(Me.cboPeriod)
        Me.pnlOtherDetail.Controls.Add(Me.objbtnSearchPeriod)
        Me.pnlOtherDetail.Controls.Add(Me.tblpAssessorEmployee)
        Me.pnlOtherDetail.Controls.Add(Me.txtRemark)
        Me.pnlOtherDetail.Controls.Add(Me.cboCourse)
        Me.pnlOtherDetail.Controls.Add(Me.lblCourse)
        Me.pnlOtherDetail.Controls.Add(Me.objbtnSearchCourse)
        Me.pnlOtherDetail.Location = New System.Drawing.Point(10, 34)
        Me.pnlOtherDetail.Name = "pnlOtherDetail"
        Me.pnlOtherDetail.Size = New System.Drawing.Size(324, 466)
        Me.pnlOtherDetail.TabIndex = 259
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(223, 433)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(98, 30)
        Me.btnAdd.TabIndex = 241
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'chkPerformanceEvalutation
        '
        Me.chkPerformanceEvalutation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPerformanceEvalutation.Location = New System.Drawing.Point(3, 3)
        Me.chkPerformanceEvalutation.Name = "chkPerformanceEvalutation"
        Me.chkPerformanceEvalutation.Size = New System.Drawing.Size(289, 17)
        Me.chkPerformanceEvalutation.TabIndex = 257
        Me.chkPerformanceEvalutation.Text = "Include Training Needs from Performance Evaluation."
        Me.chkPerformanceEvalutation.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(3, 29)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(77, 15)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Period"
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(3, 83)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(77, 15)
        Me.lblRemark.TabIndex = 248
        Me.lblRemark.Text = "Remark"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(86, 26)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(207, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(299, 26)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 237
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.Panel3, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.Panel1, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(6, 143)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(314, 284)
        Me.tblpAssessorEmployee.TabIndex = 108
        '
        'Panel3
        '
        Me.Panel3.AutoSize = True
        Me.Panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel3.Controls.Add(Me.objbtnSearch)
        Me.Panel3.Controls.Add(Me.objbtnReset)
        Me.Panel3.Controls.Add(Me.txtSearch)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(306, 27)
        Me.Panel3.TabIndex = 107
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.ClickColor1 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.objbtnSearch.ClickColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.HoverColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(255, 3)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.SelectedColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.SelectedColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.SelectedHoverColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.SelectedHoverColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 246
        '
        'objbtnReset
        '
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnReset.BorderSelected = False
        Me.objbtnReset.ClickColor1 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.objbtnReset.ClickColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.objbtnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnReset.HoverColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnReset.Location = New System.Drawing.Point(282, 3)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.SelectedColor1 = System.Drawing.Color.Transparent
        Me.objbtnReset.SelectedColor2 = System.Drawing.Color.Transparent
        Me.objbtnReset.SelectedHoverColor1 = System.Drawing.Color.Transparent
        Me.objbtnReset.SelectedHoverColor2 = System.Drawing.Color.Transparent
        Me.objbtnReset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnReset.TabIndex = 239
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(4, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(245, 21)
        Me.txtSearch.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkEmployee)
        Me.Panel1.Controls.Add(Me.dgvEmployee)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(4, 38)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(306, 242)
        Me.Panel1.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvEmployee
        '
        Me.dgvEmployee.AllowUserToAddRows = False
        Me.dgvEmployee.AllowUserToDeleteRows = False
        Me.dgvEmployee.AllowUserToResizeColumns = False
        Me.dgvEmployee.AllowUserToResizeRows = False
        Me.dgvEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvEmployee.ColumnHeadersHeight = 21
        Me.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmployee.MultiSelect = False
        Me.dgvEmployee.Name = "dgvEmployee"
        Me.dgvEmployee.RowHeadersVisible = False
        Me.dgvEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmployee.Size = New System.Drawing.Size(306, 242)
        Me.dgvEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(86, 80)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(207, 57)
        Me.txtRemark.TabIndex = 247
        '
        'cboCourse
        '
        Me.cboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourse.DropDownWidth = 350
        Me.cboCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourse.FormattingEnabled = True
        Me.cboCourse.Location = New System.Drawing.Point(86, 53)
        Me.cboCourse.Name = "cboCourse"
        Me.cboCourse.Size = New System.Drawing.Size(207, 21)
        Me.cboCourse.TabIndex = 3
        '
        'lblCourse
        '
        Me.lblCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.Location = New System.Drawing.Point(3, 56)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(77, 15)
        Me.lblCourse.TabIndex = 2
        Me.lblCourse.Text = "Course"
        '
        'objbtnSearchCourse
        '
        Me.objbtnSearchCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCourse.BorderSelected = False
        Me.objbtnSearchCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCourse.Location = New System.Drawing.Point(299, 53)
        Me.objbtnSearchCourse.Name = "objbtnSearchCourse"
        Me.objbtnSearchCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCourse.TabIndex = 238
        '
        'pnlRefCombo
        '
        Me.pnlRefCombo.Controls.Add(Me.txtReferenceNo)
        Me.pnlRefCombo.Controls.Add(Me.lblReferenceNo)
        Me.pnlRefCombo.Location = New System.Drawing.Point(10, 8)
        Me.pnlRefCombo.Name = "pnlRefCombo"
        Me.pnlRefCombo.Size = New System.Drawing.Size(324, 25)
        Me.pnlRefCombo.TabIndex = 258
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.BackColor = System.Drawing.Color.White
        Me.txtReferenceNo.Flags = 0
        Me.txtReferenceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceNo.Location = New System.Drawing.Point(86, 2)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.ReadOnly = True
        Me.txtReferenceNo.Size = New System.Drawing.Size(207, 21)
        Me.txtReferenceNo.TabIndex = 251
        '
        'lblReferenceNo
        '
        Me.lblReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferenceNo.Location = New System.Drawing.Point(3, 5)
        Me.lblReferenceNo.Name = "lblReferenceNo"
        Me.lblReferenceNo.Size = New System.Drawing.Size(77, 15)
        Me.lblReferenceNo.TabIndex = 250
        Me.lblReferenceNo.Text = "Reference No"
        '
        'gbTrainingPriority
        '
        Me.gbTrainingPriority.BorderColor = System.Drawing.Color.Black
        Me.gbTrainingPriority.Checked = False
        Me.gbTrainingPriority.CollapseAllExceptThis = False
        Me.gbTrainingPriority.CollapsedHoverImage = Nothing
        Me.gbTrainingPriority.CollapsedNormalImage = Nothing
        Me.gbTrainingPriority.CollapsedPressedImage = Nothing
        Me.gbTrainingPriority.CollapseOnLoad = False
        Me.gbTrainingPriority.Controls.Add(Me.objbtnHide)
        Me.gbTrainingPriority.Controls.Add(Me.objbtnShow)
        Me.gbTrainingPriority.Controls.Add(Me.btnDelete)
        Me.gbTrainingPriority.Controls.Add(Me.pnlGrid)
        Me.gbTrainingPriority.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTrainingPriority.ExpandedHoverImage = Nothing
        Me.gbTrainingPriority.ExpandedNormalImage = Nothing
        Me.gbTrainingPriority.ExpandedPressedImage = Nothing
        Me.gbTrainingPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrainingPriority.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrainingPriority.HeaderHeight = 25
        Me.gbTrainingPriority.HeaderMessage = ""
        Me.gbTrainingPriority.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbTrainingPriority.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTrainingPriority.HeightOnCollapse = 0
        Me.gbTrainingPriority.LeftTextSpace = 0
        Me.gbTrainingPriority.Location = New System.Drawing.Point(0, 0)
        Me.gbTrainingPriority.Name = "gbTrainingPriority"
        Me.gbTrainingPriority.OpenHeight = 300
        Me.gbTrainingPriority.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrainingPriority.ShowBorder = True
        Me.gbTrainingPriority.ShowCheckBox = False
        Me.gbTrainingPriority.ShowCollapseButton = False
        Me.gbTrainingPriority.ShowDefaultBorderColor = True
        Me.gbTrainingPriority.ShowDownButton = False
        Me.gbTrainingPriority.ShowHeader = True
        Me.gbTrainingPriority.Size = New System.Drawing.Size(574, 454)
        Me.gbTrainingPriority.TabIndex = 111
        Me.gbTrainingPriority.Temp = 0
        Me.gbTrainingPriority.Text = "Training Priority"
        Me.gbTrainingPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnHide
        '
        Me.objbtnHide.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnHide.BackColor = System.Drawing.Color.Transparent
        Me.objbtnHide.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnHide.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnHide.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnHide.BorderSelected = False
        Me.objbtnHide.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnHide.Image = Global.Aruti.Main.My.Resources.Resources.left_arrow_24
        Me.objbtnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnHide.Location = New System.Drawing.Point(550, 2)
        Me.objbtnHide.Name = "objbtnHide"
        Me.objbtnHide.Size = New System.Drawing.Size(21, 21)
        Me.objbtnHide.TabIndex = 242
        '
        'objbtnShow
        '
        Me.objbtnShow.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnShow.BackColor = System.Drawing.Color.Transparent
        Me.objbtnShow.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnShow.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnShow.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnShow.BorderSelected = False
        Me.objbtnShow.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnShow.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_24
        Me.objbtnShow.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnShow.Location = New System.Drawing.Point(550, 2)
        Me.objbtnShow.Name = "objbtnShow"
        Me.objbtnShow.Size = New System.Drawing.Size(21, 21)
        Me.objbtnShow.TabIndex = 243
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(474, 418)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(91, 30)
        Me.btnDelete.TabIndex = 242
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'pnlGrid
        '
        Me.pnlGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlGrid.Controls.Add(Me.picStayView)
        Me.pnlGrid.Controls.Add(Me.dgvTrainingPriority)
        Me.pnlGrid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlGrid.Location = New System.Drawing.Point(2, 26)
        Me.pnlGrid.Name = "pnlGrid"
        Me.pnlGrid.Size = New System.Drawing.Size(570, 386)
        Me.pnlGrid.TabIndex = 1
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(600, 330)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 111
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'dgvTrainingPriority
        '
        Me.dgvTrainingPriority.AllowUserToAddRows = False
        Me.dgvTrainingPriority.AllowUserToDeleteRows = False
        Me.dgvTrainingPriority.AllowUserToResizeRows = False
        Me.dgvTrainingPriority.BackgroundColor = System.Drawing.Color.White
        Me.dgvTrainingPriority.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvTrainingPriority.ColumnHeadersHeight = 22
        Me.dgvTrainingPriority.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvTrainingPriority.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECollapse, Me.objdgcolhCheck, Me.dgcolhTrainingCourse, Me.dgcolhTotEmp, Me.dgcolhGAP, Me.dgcolhBusinessPriority, Me.dgcolhKRA, Me.dgcolhRanking, Me.dgcolhTraningSol, Me.dgcolhTraningObjective, Me.dgcolhProvider, Me.dgcolhtrainingVenue, Me.dgcolhGoal, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhemployeeunkid, Me.objdgcolhTrainingCourseId, Me.objdgcolhSkill_Gap, Me.objdgcolhbusiness, Me.objdgcolhProvider, Me.objdgcolhStartegicGoal})
        Me.dgvTrainingPriority.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvTrainingPriority.Location = New System.Drawing.Point(0, 0)
        Me.dgvTrainingPriority.Name = "dgvTrainingPriority"
        Me.dgvTrainingPriority.RowHeadersVisible = False
        Me.dgvTrainingPriority.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvTrainingPriority.Size = New System.Drawing.Size(570, 386)
        Me.dgvTrainingPriority.TabIndex = 4
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSaveExport)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnFinalSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 454)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(934, 55)
        Me.EZeeFooter1.TabIndex = 5
        '
        'btnSaveExport
        '
        Me.btnSaveExport.BorderColor = System.Drawing.Color.Black
        Me.btnSaveExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveExport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnSaveExport.Location = New System.Drawing.Point(700, 13)
        Me.btnSaveExport.Name = "btnSaveExport"
        Me.btnSaveExport.ShowDefaultBorderColor = True
        Me.btnSaveExport.Size = New System.Drawing.Size(128, 30)
        Me.btnSaveExport.SplitButtonMenu = Me.mnuOperation
        Me.btnSaveExport.TabIndex = 1
        Me.btnSaveExport.Text = "&Save And Export"
        '
        'mnuOperation
        '
        Me.mnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExportWithName, Me.mnuExportWithOutName})
        Me.mnuOperation.Name = "mnuOperation"
        Me.mnuOperation.Size = New System.Drawing.Size(220, 48)
        '
        'mnuExportWithName
        '
        Me.mnuExportWithName.Name = "mnuExportWithName"
        Me.mnuExportWithName.Size = New System.Drawing.Size(219, 22)
        Me.mnuExportWithName.Text = "Export &With  Employees"
        '
        'mnuExportWithOutName
        '
        Me.mnuExportWithOutName.Name = "mnuExportWithOutName"
        Me.mnuExportWithOutName.Size = New System.Drawing.Size(219, 22)
        Me.mnuExportWithOutName.Text = "Export with &Out  Employees"
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(738, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.BorderColor = System.Drawing.Color.Black
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnExport.Location = New System.Drawing.Point(597, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.ShowDefaultBorderColor = True
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.SplitButtonMenu = Me.mnuOperation
        Me.btnExport.TabIndex = 1
        Me.btnExport.Text = "&Export"
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(834, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(91, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnFinalSave
        '
        Me.btnFinalSave.BackColor = System.Drawing.Color.White
        Me.btnFinalSave.BackgroundImage = CType(resources.GetObject("btnFinalSave.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalSave.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalSave.FlatAppearance.BorderSize = 0
        Me.btnFinalSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalSave.ForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Location = New System.Drawing.Point(642, 13)
        Me.btnFinalSave.Name = "btnFinalSave"
        Me.btnFinalSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Size = New System.Drawing.Size(90, 30)
        Me.btnFinalSave.TabIndex = 7
        Me.btnFinalSave.Text = "&Final Save"
        Me.btnFinalSave.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 125
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = ""
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 25
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn4.HeaderText = "Training Course"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "KRA"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Priority Ranking"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Training Venue"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "dgcolhemployeeunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhTrainingCourseId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhSkill_Gap"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "objdgcolhbusiness"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhProvider"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhStartegicGoal"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'objdgcolhECollapse
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhECollapse.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhECollapse.HeaderText = ""
        Me.objdgcolhECollapse.Name = "objdgcolhECollapse"
        Me.objdgcolhECollapse.ReadOnly = True
        Me.objdgcolhECollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhECollapse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhECollapse.Width = 25
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhTrainingCourse
        '
        Me.dgcolhTrainingCourse.HeaderText = "Training Course"
        Me.dgcolhTrainingCourse.Name = "dgcolhTrainingCourse"
        Me.dgcolhTrainingCourse.ReadOnly = True
        Me.dgcolhTrainingCourse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTrainingCourse.Width = 200
        '
        'dgcolhTotEmp
        '
        Me.dgcolhTotEmp.HeaderText = "Total Employee"
        Me.dgcolhTotEmp.Name = "dgcolhTotEmp"
        Me.dgcolhTotEmp.ReadOnly = True
        Me.dgcolhTotEmp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGAP
        '
        Me.dgcolhGAP.HeaderText = "Skill GAP Priority"
        Me.dgcolhGAP.Name = "dgcolhGAP"
        Me.dgcolhGAP.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgcolhBusinessPriority
        '
        Me.dgcolhBusinessPriority.HeaderText = "Business Priority"
        Me.dgcolhBusinessPriority.Name = "dgcolhBusinessPriority"
        Me.dgcolhBusinessPriority.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgcolhKRA
        '
        Me.dgcolhKRA.HeaderText = "KRA"
        Me.dgcolhKRA.Name = "dgcolhKRA"
        Me.dgcolhKRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRanking
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhRanking.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhRanking.HeaderText = "Priority Ranking"
        Me.dgcolhRanking.Name = "dgcolhRanking"
        Me.dgcolhRanking.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhRanking.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTraningSol
        '
        Me.dgcolhTraningSol.HeaderText = "Training Solution"
        Me.dgcolhTraningSol.Name = "dgcolhTraningSol"
        Me.dgcolhTraningSol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTraningObjective
        '
        Me.dgcolhTraningObjective.HeaderText = "Training Objective"
        Me.dgcolhTraningObjective.Name = "dgcolhTraningObjective"
        Me.dgcolhTraningObjective.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhProvider
        '
        Me.dgcolhProvider.HeaderText = "Preferred Provider"
        Me.dgcolhProvider.Name = "dgcolhProvider"
        Me.dgcolhProvider.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgcolhtrainingVenue
        '
        Me.dgcolhtrainingVenue.HeaderText = "Training Venue"
        Me.dgcolhtrainingVenue.Name = "dgcolhtrainingVenue"
        Me.dgcolhtrainingVenue.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhtrainingVenue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGoal
        '
        Me.dgcolhGoal.HeaderText = "Strategic Goal"
        Me.dgcolhGoal.Name = "dgcolhGoal"
        Me.dgcolhGoal.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhemployeeunkid
        '
        Me.objdgcolhemployeeunkid.HeaderText = "dgcolhemployeeunkid"
        Me.objdgcolhemployeeunkid.Name = "objdgcolhemployeeunkid"
        Me.objdgcolhemployeeunkid.Visible = False
        '
        'objdgcolhTrainingCourseId
        '
        Me.objdgcolhTrainingCourseId.HeaderText = "objdgcolhTrainingCourseId"
        Me.objdgcolhTrainingCourseId.Name = "objdgcolhTrainingCourseId"
        Me.objdgcolhTrainingCourseId.Visible = False
        '
        'objdgcolhSkill_Gap
        '
        Me.objdgcolhSkill_Gap.HeaderText = "objdgcolhSkill_Gap"
        Me.objdgcolhSkill_Gap.Name = "objdgcolhSkill_Gap"
        Me.objdgcolhSkill_Gap.Visible = False
        '
        'objdgcolhbusiness
        '
        Me.objdgcolhbusiness.HeaderText = "objdgcolhbusiness"
        Me.objdgcolhbusiness.Name = "objdgcolhbusiness"
        Me.objdgcolhbusiness.Visible = False
        '
        'objdgcolhProvider
        '
        Me.objdgcolhProvider.HeaderText = "objdgcolhProvider"
        Me.objdgcolhProvider.Name = "objdgcolhProvider"
        Me.objdgcolhProvider.Visible = False
        '
        'objdgcolhStartegicGoal
        '
        Me.objdgcolhStartegicGoal.HeaderText = "objdgcolhStartegicGoal"
        Me.objdgcolhStartegicGoal.Name = "objdgcolhStartegicGoal"
        Me.objdgcolhStartegicGoal.Visible = False
        '
        'frmTraining_Priority
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(934, 509)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTraining_Priority"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Training Priority"
        Me.pnlMain.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.pnlTraningInfo.ResumeLayout(False)
        Me.pnlOtherDetail.ResumeLayout(False)
        Me.pnlOtherDetail.PerformLayout()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlRefCombo.ResumeLayout(False)
        Me.pnlRefCombo.PerformLayout()
        Me.gbTrainingPriority.ResumeLayout(False)
        Me.pnlGrid.ResumeLayout(False)
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTrainingPriority, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.mnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents cboCourse As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents dgvTrainingPriority As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchCourse As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbTrainingPriority As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents pnlGrid As System.Windows.Forms.Panel
    Friend WithEvents objbtnHide As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnShow As eZee.Common.eZeeGradientButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents objbtnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnSaveExport As eZee.Common.eZeeSplitButton
    Friend WithEvents btnExport As eZee.Common.eZeeSplitButton
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblReferenceNo As System.Windows.Forms.Label
    Friend WithEvents btnFinalSave As eZee.Common.eZeeLightButton
    Friend WithEvents chkPerformanceEvalutation As System.Windows.Forms.CheckBox
    Friend WithEvents pnlOtherDetail As System.Windows.Forms.Panel
    Friend WithEvents pnlRefCombo As System.Windows.Forms.Panel
    Friend WithEvents pnlTraningInfo As System.Windows.Forms.Panel
    Friend WithEvents txtReferenceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents mnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuExportWithName As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportWithOutName As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objdgcolhECollapse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhTrainingCourse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTotEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGAP As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhBusinessPriority As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhKRA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRanking As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhTraningSol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTraningObjective As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProvider As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhtrainingVenue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGoal As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhemployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTrainingCourseId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSkill_Gap As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhbusiness As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProvider As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStartegicGoal As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
