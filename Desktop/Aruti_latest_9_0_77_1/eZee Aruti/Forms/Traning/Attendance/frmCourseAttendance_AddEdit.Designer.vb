﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCourseAttendance_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCourseAttendance_AddEdit))
        Me.pnlAttendanceSheet = New System.Windows.Forms.Panel
        Me.gbCourseInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtEndDateTime = New eZee.TextBox.AlphanumericTextBox
        Me.txtStartDateTime = New eZee.TextBox.AlphanumericTextBox
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.txtCourseTitle = New eZee.TextBox.AlphanumericTextBox
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblAttendanceDate = New System.Windows.Forms.Label
        Me.dtpAttendanceDate = New System.Windows.Forms.DateTimePicker
        Me.lblTraningCourse = New System.Windows.Forms.Label
        Me.gbAttendanceInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAttendanceInfo = New System.Windows.Forms.Panel
        Me.dgvAttendanceSheet = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objAttendanceTranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objAttendanceUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objEmployeeUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colPresent = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colAbsent = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkMarkAllPresent = New System.Windows.Forms.CheckBox
        Me.chkMarkAllAbsent = New System.Windows.Forms.CheckBox
        Me.pnlAttendanceSheet.SuspendLayout()
        Me.gbCourseInfo.SuspendLayout()
        Me.gbAttendanceInfo.SuspendLayout()
        Me.pnlAttendanceInfo.SuspendLayout()
        CType(Me.dgvAttendanceSheet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAttendanceSheet
        '
        Me.pnlAttendanceSheet.Controls.Add(Me.gbCourseInfo)
        Me.pnlAttendanceSheet.Controls.Add(Me.gbAttendanceInfo)
        Me.pnlAttendanceSheet.Controls.Add(Me.objFooter)
        Me.pnlAttendanceSheet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAttendanceSheet.Location = New System.Drawing.Point(0, 0)
        Me.pnlAttendanceSheet.Name = "pnlAttendanceSheet"
        Me.pnlAttendanceSheet.Size = New System.Drawing.Size(633, 467)
        Me.pnlAttendanceSheet.TabIndex = 0
        '
        'gbCourseInfo
        '
        Me.gbCourseInfo.BorderColor = System.Drawing.Color.Black
        Me.gbCourseInfo.Checked = False
        Me.gbCourseInfo.CollapseAllExceptThis = False
        Me.gbCourseInfo.CollapsedHoverImage = Nothing
        Me.gbCourseInfo.CollapsedNormalImage = Nothing
        Me.gbCourseInfo.CollapsedPressedImage = Nothing
        Me.gbCourseInfo.CollapseOnLoad = False
        Me.gbCourseInfo.Controls.Add(Me.txtEndDateTime)
        Me.gbCourseInfo.Controls.Add(Me.txtStartDateTime)
        Me.gbCourseInfo.Controls.Add(Me.lblEndDate)
        Me.gbCourseInfo.Controls.Add(Me.lblStartDate)
        Me.gbCourseInfo.Controls.Add(Me.txtCourseTitle)
        Me.gbCourseInfo.Controls.Add(Me.txtRemark)
        Me.gbCourseInfo.Controls.Add(Me.lblRemark)
        Me.gbCourseInfo.Controls.Add(Me.lblAttendanceDate)
        Me.gbCourseInfo.Controls.Add(Me.dtpAttendanceDate)
        Me.gbCourseInfo.Controls.Add(Me.lblTraningCourse)
        Me.gbCourseInfo.ExpandedHoverImage = Nothing
        Me.gbCourseInfo.ExpandedNormalImage = Nothing
        Me.gbCourseInfo.ExpandedPressedImage = Nothing
        Me.gbCourseInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCourseInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCourseInfo.HeaderHeight = 25
        Me.gbCourseInfo.HeightOnCollapse = 0
        Me.gbCourseInfo.LeftTextSpace = 0
        Me.gbCourseInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbCourseInfo.Name = "gbCourseInfo"
        Me.gbCourseInfo.OpenHeight = 109
        Me.gbCourseInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCourseInfo.ShowBorder = True
        Me.gbCourseInfo.ShowCheckBox = False
        Me.gbCourseInfo.ShowCollapseButton = False
        Me.gbCourseInfo.ShowDefaultBorderColor = True
        Me.gbCourseInfo.ShowDownButton = False
        Me.gbCourseInfo.ShowHeader = True
        Me.gbCourseInfo.Size = New System.Drawing.Size(609, 109)
        Me.gbCourseInfo.TabIndex = 6
        Me.gbCourseInfo.Temp = 0
        Me.gbCourseInfo.Text = "Course Info and Attendance Date"
        Me.gbCourseInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEndDateTime
        '
        Me.txtEndDateTime.Flags = 0
        Me.txtEndDateTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEndDateTime.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEndDateTime.Location = New System.Drawing.Point(508, 60)
        Me.txtEndDateTime.Name = "txtEndDateTime"
        Me.txtEndDateTime.ReadOnly = True
        Me.txtEndDateTime.Size = New System.Drawing.Size(89, 21)
        Me.txtEndDateTime.TabIndex = 176
        '
        'txtStartDateTime
        '
        Me.txtStartDateTime.Flags = 0
        Me.txtStartDateTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDateTime.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtStartDateTime.Location = New System.Drawing.Point(508, 33)
        Me.txtStartDateTime.Name = "txtStartDateTime"
        Me.txtStartDateTime.ReadOnly = True
        Me.txtStartDateTime.Size = New System.Drawing.Size(89, 21)
        Me.txtStartDateTime.TabIndex = 175
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(436, 63)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(66, 15)
        Me.lblEndDate.TabIndex = 174
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(436, 36)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(66, 15)
        Me.lblStartDate.TabIndex = 173
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCourseTitle
        '
        Me.txtCourseTitle.Flags = 0
        Me.txtCourseTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCourseTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCourseTitle.Location = New System.Drawing.Point(117, 33)
        Me.txtCourseTitle.Name = "txtCourseTitle"
        Me.txtCourseTitle.ReadOnly = True
        Me.txtCourseTitle.Size = New System.Drawing.Size(122, 21)
        Me.txtCourseTitle.TabIndex = 172
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(117, 60)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(313, 42)
        Me.txtRemark.TabIndex = 164
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(11, 64)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(103, 15)
        Me.lblRemark.TabIndex = 163
        Me.lblRemark.Text = "Remark"
        '
        'lblAttendanceDate
        '
        Me.lblAttendanceDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttendanceDate.Location = New System.Drawing.Point(245, 36)
        Me.lblAttendanceDate.Name = "lblAttendanceDate"
        Me.lblAttendanceDate.Size = New System.Drawing.Size(90, 15)
        Me.lblAttendanceDate.TabIndex = 160
        Me.lblAttendanceDate.Text = "Attendance Date"
        Me.lblAttendanceDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAttendanceDate
        '
        Me.dtpAttendanceDate.Checked = False
        Me.dtpAttendanceDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAttendanceDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAttendanceDate.Location = New System.Drawing.Point(341, 33)
        Me.dtpAttendanceDate.Name = "dtpAttendanceDate"
        Me.dtpAttendanceDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpAttendanceDate.TabIndex = 159
        '
        'lblTraningCourse
        '
        Me.lblTraningCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningCourse.Location = New System.Drawing.Point(8, 36)
        Me.lblTraningCourse.Name = "lblTraningCourse"
        Me.lblTraningCourse.Size = New System.Drawing.Size(103, 15)
        Me.lblTraningCourse.TabIndex = 158
        Me.lblTraningCourse.Text = "Scheduled Course"
        Me.lblTraningCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbAttendanceInfo
        '
        Me.gbAttendanceInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAttendanceInfo.Checked = False
        Me.gbAttendanceInfo.CollapseAllExceptThis = False
        Me.gbAttendanceInfo.CollapsedHoverImage = Nothing
        Me.gbAttendanceInfo.CollapsedNormalImage = Nothing
        Me.gbAttendanceInfo.CollapsedPressedImage = Nothing
        Me.gbAttendanceInfo.CollapseOnLoad = False
        Me.gbAttendanceInfo.Controls.Add(Me.pnlAttendanceInfo)
        Me.gbAttendanceInfo.ExpandedHoverImage = Nothing
        Me.gbAttendanceInfo.ExpandedNormalImage = Nothing
        Me.gbAttendanceInfo.ExpandedPressedImage = Nothing
        Me.gbAttendanceInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAttendanceInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAttendanceInfo.HeaderHeight = 25
        Me.gbAttendanceInfo.HeightOnCollapse = 0
        Me.gbAttendanceInfo.LeftTextSpace = 0
        Me.gbAttendanceInfo.Location = New System.Drawing.Point(12, 127)
        Me.gbAttendanceInfo.Name = "gbAttendanceInfo"
        Me.gbAttendanceInfo.OpenHeight = 269
        Me.gbAttendanceInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAttendanceInfo.ShowBorder = True
        Me.gbAttendanceInfo.ShowCheckBox = False
        Me.gbAttendanceInfo.ShowCollapseButton = False
        Me.gbAttendanceInfo.ShowDefaultBorderColor = True
        Me.gbAttendanceInfo.ShowDownButton = False
        Me.gbAttendanceInfo.ShowHeader = True
        Me.gbAttendanceInfo.Size = New System.Drawing.Size(609, 279)
        Me.gbAttendanceInfo.TabIndex = 5
        Me.gbAttendanceInfo.Temp = 0
        Me.gbAttendanceInfo.Text = "Attendance Info"
        Me.gbAttendanceInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAttendanceInfo
        '
        Me.pnlAttendanceInfo.Controls.Add(Me.dgvAttendanceSheet)
        Me.pnlAttendanceInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAttendanceInfo.Location = New System.Drawing.Point(1, 25)
        Me.pnlAttendanceInfo.Name = "pnlAttendanceInfo"
        Me.pnlAttendanceInfo.Size = New System.Drawing.Size(606, 253)
        Me.pnlAttendanceInfo.TabIndex = 1
        '
        'dgvAttendanceSheet
        '
        Me.dgvAttendanceSheet.AllowUserToAddRows = False
        Me.dgvAttendanceSheet.AllowUserToDeleteRows = False
        Me.dgvAttendanceSheet.AllowUserToResizeColumns = False
        Me.dgvAttendanceSheet.AllowUserToResizeRows = False
        Me.dgvAttendanceSheet.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAttendanceSheet.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvAttendanceSheet.ColumnHeadersHeight = 21
        Me.dgvAttendanceSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAttendanceSheet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objAttendanceTranunkid, Me.objAttendanceUnkid, Me.objEmployeeUnkid, Me.colEmployeeName, Me.colPresent, Me.colAbsent, Me.colRemark})
        Me.dgvAttendanceSheet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAttendanceSheet.Location = New System.Drawing.Point(0, 0)
        Me.dgvAttendanceSheet.MultiSelect = False
        Me.dgvAttendanceSheet.Name = "dgvAttendanceSheet"
        Me.dgvAttendanceSheet.RowHeadersVisible = False
        Me.dgvAttendanceSheet.Size = New System.Drawing.Size(606, 253)
        Me.dgvAttendanceSheet.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.chkMarkAllAbsent)
        Me.objFooter.Controls.Add(Me.chkMarkAllPresent)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 412)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(633, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(421, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(524, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objcolhTrainerGUID"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        Me.DataGridViewTextBoxColumn6.Width = 5
        '
        'objAttendanceTranunkid
        '
        Me.objAttendanceTranunkid.HeaderText = "AttendanceTranunkid"
        Me.objAttendanceTranunkid.Name = "objAttendanceTranunkid"
        Me.objAttendanceTranunkid.ReadOnly = True
        Me.objAttendanceTranunkid.Visible = False
        Me.objAttendanceTranunkid.Width = 350
        '
        'objAttendanceUnkid
        '
        Me.objAttendanceUnkid.HeaderText = "AttendanceUnkid"
        Me.objAttendanceUnkid.Name = "objAttendanceUnkid"
        Me.objAttendanceUnkid.ReadOnly = True
        Me.objAttendanceUnkid.Visible = False
        '
        'objEmployeeUnkid
        '
        Me.objEmployeeUnkid.HeaderText = "EmployeeUnkid"
        Me.objEmployeeUnkid.Name = "objEmployeeUnkid"
        Me.objEmployeeUnkid.ReadOnly = True
        Me.objEmployeeUnkid.Visible = False
        '
        'colEmployeeName
        '
        Me.colEmployeeName.HeaderText = "Employee Name"
        Me.colEmployeeName.Name = "colEmployeeName"
        Me.colEmployeeName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colEmployeeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colEmployeeName.Width = 200
        '
        'colPresent
        '
        Me.colPresent.HeaderText = "Present"
        Me.colPresent.Name = "colPresent"
        Me.colPresent.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colPresent.Width = 55
        '
        'colAbsent
        '
        Me.colAbsent.HeaderText = "Absent"
        Me.colAbsent.Name = "colAbsent"
        Me.colAbsent.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colAbsent.Width = 50
        '
        'colRemark
        '
        Me.colRemark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colRemark.HeaderText = "Remark"
        Me.colRemark.Name = "colRemark"
        Me.colRemark.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'chkMarkAllPresent
        '
        Me.chkMarkAllPresent.Location = New System.Drawing.Point(13, 20)
        Me.chkMarkAllPresent.Name = "chkMarkAllPresent"
        Me.chkMarkAllPresent.Size = New System.Drawing.Size(132, 17)
        Me.chkMarkAllPresent.TabIndex = 129
        Me.chkMarkAllPresent.Text = "Mark as Present"
        Me.chkMarkAllPresent.UseVisualStyleBackColor = True
        '
        'chkMarkAllAbsent
        '
        Me.chkMarkAllAbsent.Location = New System.Drawing.Point(151, 20)
        Me.chkMarkAllAbsent.Name = "chkMarkAllAbsent"
        Me.chkMarkAllAbsent.Size = New System.Drawing.Size(132, 17)
        Me.chkMarkAllAbsent.TabIndex = 130
        Me.chkMarkAllAbsent.Text = "Mark as Absent"
        Me.chkMarkAllAbsent.UseVisualStyleBackColor = True
        '
        'frmCourseAttendance_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(633, 467)
        Me.Controls.Add(Me.pnlAttendanceSheet)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCourseAttendance_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Attendance Sheet"
        Me.pnlAttendanceSheet.ResumeLayout(False)
        Me.gbCourseInfo.ResumeLayout(False)
        Me.gbCourseInfo.PerformLayout()
        Me.gbAttendanceInfo.ResumeLayout(False)
        Me.pnlAttendanceInfo.ResumeLayout(False)
        CType(Me.dgvAttendanceSheet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlAttendanceSheet As System.Windows.Forms.Panel
    Friend WithEvents gbAttendanceInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAttendanceInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvAttendanceSheet As System.Windows.Forms.DataGridView
    Friend WithEvents gbCourseInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAttendanceDate As System.Windows.Forms.Label
    Friend WithEvents dtpAttendanceDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTraningCourse As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtCourseTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEndDateTime As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtStartDateTime As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objAttendanceTranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objAttendanceUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objEmployeeUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPresent As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colAbsent As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkMarkAllAbsent As System.Windows.Forms.CheckBox
    Friend WithEvents chkMarkAllPresent As System.Windows.Forms.CheckBox
End Class
