﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTraningRegistraionCancelList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTraningRegistraionCancelList))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnAwardStatus = New eZee.Common.eZeeSplitButton
        Me.mnuEvaluation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuAnalyse = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAttendance = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuScanAttachDocument = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEnrollEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAwardStatus = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuUpdateQualification = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRecategorize = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSalaryIncrement = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVoidSalaryIncrement = New System.Windows.Forms.ToolStripMenuItem
        Me.btnEvaluation = New eZee.Common.eZeeSplitButton
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.radVoid = New System.Windows.Forms.RadioButton
        Me.objstLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.radShowAll = New System.Windows.Forms.RadioButton
        Me.radCancelled = New System.Windows.Forms.RadioButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dtpCancellationTo = New System.Windows.Forms.DateTimePicker
        Me.dtpEnrollmentTo = New System.Windows.Forms.DateTimePicker
        Me.dtpCancellationDate = New System.Windows.Forms.DateTimePicker
        Me.dtpEnrollmentDateFrom = New System.Windows.Forms.DateTimePicker
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.cboCourse = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblCancellationDateTo = New System.Windows.Forms.Label
        Me.lblCancellationDateFrom = New System.Windows.Forms.Label
        Me.lblEnrollmentDateTo = New System.Windows.Forms.Label
        Me.lblEnrollmentdateFrom = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblCourse = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.pnlEnrollmentAndCancellationList = New System.Windows.Forms.Panel
        Me.chkCheckAll = New System.Windows.Forms.CheckBox
        Me.lvEnrollment = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhCourse = New System.Windows.Forms.ColumnHeader
        Me.colhEnrollDate = New System.Windows.Forms.ColumnHeader
        Me.colhCancelDate = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhAwardStatus = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriod = New System.Windows.Forms.ColumnHeader
        Me.objcolhscheduleunkid = New System.Windows.Forms.ColumnHeader
        Me.colhUpdateQuali = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter.SuspendLayout()
        Me.mnuEvaluation.SuspendLayout()
        Me.mnuAwardStatus.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEnrollmentAndCancellationList.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnAwardStatus)
        Me.objFooter.Controls.Add(Me.btnEvaluation)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 433)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(819, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnAwardStatus
        '
        Me.btnAwardStatus.BorderColor = System.Drawing.Color.Black
        Me.btnAwardStatus.ContextMenuStrip = Me.mnuEvaluation
        Me.btnAwardStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAwardStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAwardStatus.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnAwardStatus.Location = New System.Drawing.Point(275, 13)
        Me.btnAwardStatus.Name = "btnAwardStatus"
        Me.btnAwardStatus.ShowDefaultBorderColor = True
        Me.btnAwardStatus.Size = New System.Drawing.Size(120, 30)
        Me.btnAwardStatus.SplitButtonMenu = Me.mnuAwardStatus
        Me.btnAwardStatus.TabIndex = 158
        Me.btnAwardStatus.Text = "&Award Actions"
        '
        'mnuEvaluation
        '
        Me.mnuEvaluation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAnalyse, Me.mnuAttendance, Me.mnuScanAttachDocument, Me.mnuEnrollEmployee, Me.mnuGetFileFormat})
        Me.mnuEvaluation.Name = "mnuEvaluation"
        Me.mnuEvaluation.Size = New System.Drawing.Size(219, 136)
        '
        'mnuAnalyse
        '
        Me.mnuAnalyse.Name = "mnuAnalyse"
        Me.mnuAnalyse.Size = New System.Drawing.Size(218, 22)
        Me.mnuAnalyse.Text = "Training Awards"
        '
        'mnuAttendance
        '
        Me.mnuAttendance.Name = "mnuAttendance"
        Me.mnuAttendance.Size = New System.Drawing.Size(218, 22)
        Me.mnuAttendance.Text = "Attendance"
        Me.mnuAttendance.Visible = False
        '
        'mnuScanAttachDocument
        '
        Me.mnuScanAttachDocument.Name = "mnuScanAttachDocument"
        Me.mnuScanAttachDocument.Size = New System.Drawing.Size(217, 22)
        Me.mnuScanAttachDocument.Text = "&Browse"
        '
        'mnuEnrollEmployee
        '
        Me.mnuEnrollEmployee.Name = "mnuEnrollEmployee"
        Me.mnuEnrollEmployee.Size = New System.Drawing.Size(218, 22)
        Me.mnuEnrollEmployee.Tag = "mnuEnrollEmployee"
        Me.mnuEnrollEmployee.Text = "Import Training Enrollment"
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(218, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "Get File Format"
        '
        'mnuAwardStatus
        '
        Me.mnuAwardStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUpdateQualification, Me.mnuRecategorize, Me.mnuSalaryIncrement, Me.mnuVoidSalaryIncrement})
        Me.mnuAwardStatus.Name = "mnuAwardStatus"
        Me.mnuAwardStatus.Size = New System.Drawing.Size(190, 92)
        '
        'mnuUpdateQualification
        '
        Me.mnuUpdateQualification.Name = "mnuUpdateQualification"
        Me.mnuUpdateQualification.Size = New System.Drawing.Size(189, 22)
        Me.mnuUpdateQualification.Text = "Update Qualification "
        '
        'mnuRecategorize
        '
        Me.mnuRecategorize.Name = "mnuRecategorize"
        Me.mnuRecategorize.Size = New System.Drawing.Size(189, 22)
        Me.mnuRecategorize.Text = "Re-categorize"
        '
        'mnuSalaryIncrement
        '
        Me.mnuSalaryIncrement.Name = "mnuSalaryIncrement"
        Me.mnuSalaryIncrement.Size = New System.Drawing.Size(189, 22)
        Me.mnuSalaryIncrement.Text = "Salary Increment "
        '
        'mnuVoidSalaryIncrement
        '
        Me.mnuVoidSalaryIncrement.Name = "mnuVoidSalaryIncrement"
        Me.mnuVoidSalaryIncrement.Size = New System.Drawing.Size(189, 22)
        Me.mnuVoidSalaryIncrement.Text = "Void Salary Increment"
        '
        'btnEvaluation
        '
        Me.btnEvaluation.BorderColor = System.Drawing.Color.Black
        Me.btnEvaluation.ContextMenuStrip = Me.mnuEvaluation
        Me.btnEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEvaluation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnEvaluation.Location = New System.Drawing.Point(12, 13)
        Me.btnEvaluation.Name = "btnEvaluation"
        Me.btnEvaluation.ShowDefaultBorderColor = True
        Me.btnEvaluation.Size = New System.Drawing.Size(97, 30)
        Me.btnEvaluation.SplitButtonMenu = Me.mnuEvaluation
        Me.btnEvaluation.TabIndex = 157
        Me.btnEvaluation.Text = "&Operations"
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(401, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 155
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(710, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 154
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(607, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 153
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(504, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 152
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.txtRemark)
        Me.gbFilterCriteria.Controls.Add(Me.lblRemark)
        Me.gbFilterCriteria.Controls.Add(Me.radVoid)
        Me.gbFilterCriteria.Controls.Add(Me.objstLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.radShowAll)
        Me.gbFilterCriteria.Controls.Add(Me.radCancelled)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.dtpCancellationTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEnrollmentTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpCancellationDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEnrollmentDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboCourse)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblCancellationDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCancellationDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblEnrollmentDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblEnrollmentdateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblCourse)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 118
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(797, 117)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(660, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 104
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(304, 33)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(128, 21)
        Me.cboBranch.TabIndex = 24
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(250, 36)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(48, 15)
        Me.lblBranch.TabIndex = 23
        Me.lblBranch.Text = "Branch"
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(88, 87)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(344, 21)
        Me.txtRemark.TabIndex = 8
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(8, 89)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(74, 15)
        Me.lblRemark.TabIndex = 7
        Me.lblRemark.Text = "Remark"
        '
        'radVoid
        '
        Me.radVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radVoid.Location = New System.Drawing.Point(567, 89)
        Me.radVoid.Name = "radVoid"
        Me.radVoid.Size = New System.Drawing.Size(104, 17)
        Me.radVoid.TabIndex = 20
        Me.radVoid.TabStop = True
        Me.radVoid.Text = "Void"
        Me.radVoid.UseVisualStyleBackColor = True
        '
        'objstLine1
        '
        Me.objstLine1.BackColor = System.Drawing.Color.Transparent
        Me.objstLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objstLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine1.Location = New System.Drawing.Point(438, 33)
        Me.objstLine1.Name = "objstLine1"
        Me.objstLine1.Size = New System.Drawing.Size(11, 75)
        Me.objstLine1.TabIndex = 9
        Me.objstLine1.Text = "EZeeStraightLine1"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(222, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'radShowAll
        '
        Me.radShowAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowAll.Location = New System.Drawing.Point(677, 89)
        Me.radShowAll.Name = "radShowAll"
        Me.radShowAll.Size = New System.Drawing.Size(103, 17)
        Me.radShowAll.TabIndex = 21
        Me.radShowAll.TabStop = True
        Me.radShowAll.Text = "Show All"
        Me.radShowAll.UseVisualStyleBackColor = True
        '
        'radCancelled
        '
        Me.radCancelled.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCancelled.Location = New System.Drawing.Point(458, 89)
        Me.radCancelled.Name = "radCancelled"
        Me.radCancelled.Size = New System.Drawing.Size(103, 17)
        Me.radCancelled.TabIndex = 19
        Me.radCancelled.TabStop = True
        Me.radCancelled.Text = "Cancelled"
        Me.radCancelled.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(770, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 4
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(747, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 3
        Me.objbtnSearch.TabStop = False
        '
        'dtpCancellationTo
        '
        Me.dtpCancellationTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCancellationTo.Checked = False
        Me.dtpCancellationTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCancellationTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCancellationTo.Location = New System.Drawing.Point(677, 60)
        Me.dtpCancellationTo.Name = "dtpCancellationTo"
        Me.dtpCancellationTo.ShowCheckBox = True
        Me.dtpCancellationTo.Size = New System.Drawing.Size(103, 21)
        Me.dtpCancellationTo.TabIndex = 17
        '
        'dtpEnrollmentTo
        '
        Me.dtpEnrollmentTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnrollmentTo.Checked = False
        Me.dtpEnrollmentTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnrollmentTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnrollmentTo.Location = New System.Drawing.Point(677, 33)
        Me.dtpEnrollmentTo.Name = "dtpEnrollmentTo"
        Me.dtpEnrollmentTo.ShowCheckBox = True
        Me.dtpEnrollmentTo.Size = New System.Drawing.Size(103, 21)
        Me.dtpEnrollmentTo.TabIndex = 13
        '
        'dtpCancellationDate
        '
        Me.dtpCancellationDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCancellationDate.Checked = False
        Me.dtpCancellationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCancellationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCancellationDate.Location = New System.Drawing.Point(534, 60)
        Me.dtpCancellationDate.Name = "dtpCancellationDate"
        Me.dtpCancellationDate.ShowCheckBox = True
        Me.dtpCancellationDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpCancellationDate.TabIndex = 15
        '
        'dtpEnrollmentDateFrom
        '
        Me.dtpEnrollmentDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnrollmentDateFrom.Checked = False
        Me.dtpEnrollmentDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnrollmentDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnrollmentDateFrom.Location = New System.Drawing.Point(534, 33)
        Me.dtpEnrollmentDateFrom.Name = "dtpEnrollmentDateFrom"
        Me.dtpEnrollmentDateFrom.ShowCheckBox = True
        Me.dtpEnrollmentDateFrom.Size = New System.Drawing.Size(97, 21)
        Me.dtpEnrollmentDateFrom.TabIndex = 11
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(304, 60)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(128, 21)
        Me.cboStatus.TabIndex = 6
        '
        'cboCourse
        '
        Me.cboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourse.DropDownWidth = 350
        Me.cboCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourse.FormattingEnabled = True
        Me.cboCourse.Location = New System.Drawing.Point(88, 60)
        Me.cboCourse.Name = "cboCourse"
        Me.cboCourse.Size = New System.Drawing.Size(128, 21)
        Me.cboCourse.TabIndex = 4
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(88, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(128, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblCancellationDateTo
        '
        Me.lblCancellationDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancellationDateTo.Location = New System.Drawing.Point(637, 63)
        Me.lblCancellationDateTo.Name = "lblCancellationDateTo"
        Me.lblCancellationDateTo.Size = New System.Drawing.Size(34, 15)
        Me.lblCancellationDateTo.TabIndex = 16
        Me.lblCancellationDateTo.Text = "To"
        '
        'lblCancellationDateFrom
        '
        Me.lblCancellationDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancellationDateFrom.Location = New System.Drawing.Point(455, 63)
        Me.lblCancellationDateFrom.Name = "lblCancellationDateFrom"
        Me.lblCancellationDateFrom.Size = New System.Drawing.Size(73, 15)
        Me.lblCancellationDateFrom.TabIndex = 14
        Me.lblCancellationDateFrom.Text = "Cancel Date"
        '
        'lblEnrollmentDateTo
        '
        Me.lblEnrollmentDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnrollmentDateTo.Location = New System.Drawing.Point(637, 36)
        Me.lblEnrollmentDateTo.Name = "lblEnrollmentDateTo"
        Me.lblEnrollmentDateTo.Size = New System.Drawing.Size(34, 15)
        Me.lblEnrollmentDateTo.TabIndex = 12
        Me.lblEnrollmentDateTo.Text = "To"
        '
        'lblEnrollmentdateFrom
        '
        Me.lblEnrollmentdateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnrollmentdateFrom.Location = New System.Drawing.Point(455, 36)
        Me.lblEnrollmentdateFrom.Name = "lblEnrollmentdateFrom"
        Me.lblEnrollmentdateFrom.Size = New System.Drawing.Size(73, 15)
        Me.lblEnrollmentdateFrom.TabIndex = 10
        Me.lblEnrollmentdateFrom.Text = "Enroll Date"
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(250, 63)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(48, 15)
        Me.lblStatus.TabIndex = 5
        Me.lblStatus.Text = "Status"
        '
        'lblCourse
        '
        Me.lblCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.Location = New System.Drawing.Point(8, 63)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(74, 15)
        Me.lblCourse.TabIndex = 3
        Me.lblCourse.Text = "Course Title"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'pnlEnrollmentAndCancellationList
        '
        Me.pnlEnrollmentAndCancellationList.Controls.Add(Me.chkCheckAll)
        Me.pnlEnrollmentAndCancellationList.Controls.Add(Me.lvEnrollment)
        Me.pnlEnrollmentAndCancellationList.Controls.Add(Me.eZeeHeader)
        Me.pnlEnrollmentAndCancellationList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlEnrollmentAndCancellationList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEnrollmentAndCancellationList.Location = New System.Drawing.Point(0, 0)
        Me.pnlEnrollmentAndCancellationList.Name = "pnlEnrollmentAndCancellationList"
        Me.pnlEnrollmentAndCancellationList.Size = New System.Drawing.Size(819, 488)
        Me.pnlEnrollmentAndCancellationList.TabIndex = 0
        '
        'chkCheckAll
        '
        Me.chkCheckAll.AutoSize = True
        Me.chkCheckAll.Location = New System.Drawing.Point(18, 194)
        Me.chkCheckAll.Name = "chkCheckAll"
        Me.chkCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.chkCheckAll.TabIndex = 6
        Me.chkCheckAll.UseVisualStyleBackColor = True
        '
        'lvEnrollment
        '
        Me.lvEnrollment.BackColorOnChecked = True
        Me.lvEnrollment.CheckBoxes = True
        Me.lvEnrollment.ColumnHeaders = Nothing
        Me.lvEnrollment.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhEmployee, Me.colhCourse, Me.colhEnrollDate, Me.colhCancelDate, Me.colhStatus, Me.colhAwardStatus, Me.colhRemark, Me.objcolhEmpId, Me.objcolhPeriod, Me.objcolhscheduleunkid, Me.colhUpdateQuali})
        Me.lvEnrollment.CompulsoryColumns = ""
        Me.lvEnrollment.FullRowSelect = True
        Me.lvEnrollment.GridLines = True
        Me.lvEnrollment.GroupingColumn = Nothing
        Me.lvEnrollment.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEnrollment.HideSelection = False
        Me.lvEnrollment.Location = New System.Drawing.Point(12, 188)
        Me.lvEnrollment.MinColumnWidth = 50
        Me.lvEnrollment.MultiSelect = False
        Me.lvEnrollment.Name = "lvEnrollment"
        Me.lvEnrollment.OptionalColumns = ""
        Me.lvEnrollment.ShowMoreItem = False
        Me.lvEnrollment.ShowSaveItem = False
        Me.lvEnrollment.ShowSelectAll = True
        Me.lvEnrollment.ShowSizeAllColumnsToFit = True
        Me.lvEnrollment.Size = New System.Drawing.Size(797, 239)
        Me.lvEnrollment.Sortable = True
        Me.lvEnrollment.TabIndex = 5
        Me.lvEnrollment.UseCompatibleStateImageBehavior = False
        Me.lvEnrollment.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 25
        '
        'colhEmployee
        '
        Me.colhEmployee.DisplayIndex = 2
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 220
        '
        'colhCourse
        '
        Me.colhCourse.DisplayIndex = 1
        Me.colhCourse.Tag = "colhCourse"
        Me.colhCourse.Text = "Course Title"
        Me.colhCourse.Width = 0
        '
        'colhEnrollDate
        '
        Me.colhEnrollDate.Tag = "colhEnrollDate"
        Me.colhEnrollDate.Text = "Enroll Date"
        Me.colhEnrollDate.Width = 90
        '
        'colhCancelDate
        '
        Me.colhCancelDate.Tag = "colhCancelDate"
        Me.colhCancelDate.Text = "Cancel Date"
        Me.colhCancelDate.Width = 90
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 105
        '
        'colhAwardStatus
        '
        Me.colhAwardStatus.Tag = "colhAwardStatus"
        Me.colhAwardStatus.Text = "Award Status"
        Me.colhAwardStatus.Width = 80
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 125
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.Tag = "objcolhEmpId"
        Me.objcolhEmpId.Text = ""
        Me.objcolhEmpId.Width = 0
        '
        'objcolhPeriod
        '
        Me.objcolhPeriod.Tag = "objcolhPeriod"
        Me.objcolhPeriod.Text = "Period"
        Me.objcolhPeriod.Width = 0
        '
        'objcolhscheduleunkid
        '
        Me.objcolhscheduleunkid.Tag = "objcolhscheduleunkid"
        Me.objcolhscheduleunkid.Text = ""
        Me.objcolhscheduleunkid.Width = 0
        '
        'colhUpdateQuali
        '
        Me.colhUpdateQuali.Tag = "colhUpdateQuali"
        Me.colhUpdateQuali.Text = "Qua. Updated"
        Me.colhUpdateQuali.Width = 90
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(819, 58)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Enrollment And Cancellation List"
        '
        'frmTraningRegistraionCancelList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(819, 488)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlEnrollmentAndCancellationList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTraningRegistraionCancelList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enrollment And Cancellation List"
        Me.objFooter.ResumeLayout(False)
        Me.mnuEvaluation.ResumeLayout(False)
        Me.mnuAwardStatus.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEnrollmentAndCancellationList.ResumeLayout(False)
        Me.pnlEnrollmentAndCancellationList.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dtpCancellationTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpEnrollmentTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpCancellationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpEnrollmentDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents cboCourse As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblCancellationDateTo As System.Windows.Forms.Label
    Friend WithEvents lblCancellationDateFrom As System.Windows.Forms.Label
    Friend WithEvents lblEnrollmentDateTo As System.Windows.Forms.Label
    Friend WithEvents lblEnrollmentdateFrom As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents pnlEnrollmentAndCancellationList As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents radCancelled As System.Windows.Forms.RadioButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents radShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents btnEvaluation As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuEvaluation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuAnalyse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objstLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents radVoid As System.Windows.Forms.RadioButton
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvEnrollment As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCourse As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEnrollDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCancelDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuAttendance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents mnuScanAttachDocument As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAwardStatus As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuAwardStatus As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuUpdateQualification As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecategorize As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSalaryIncrement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents colhAwardStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuVoidSalaryIncrement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhscheduleunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUpdateQuali As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuEnrollEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
End Class
