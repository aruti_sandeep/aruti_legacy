﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEnrollmentAndCancellation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEnrollmentAndCancellation))
        Me.pnlCourseEnrollmentAndCancellation = New System.Windows.Forms.Panel
        Me.tabcRemark = New System.Windows.Forms.TabControl
        Me.tabpEnrollRemark = New System.Windows.Forms.TabPage
        Me.txtEnrollRemark = New eZee.TextBox.AlphanumericTextBox
        Me.tabpCancellationRemark = New System.Windows.Forms.TabPage
        Me.txtCancellationRemark = New eZee.TextBox.AlphanumericTextBox
        Me.gbCourseDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblTrainingYear = New System.Windows.Forms.Label
        Me.cboTrainingYears = New System.Windows.Forms.ComboBox
        Me.txtEligibility = New eZee.TextBox.AlphanumericTextBox
        Me.txtTraningFee = New eZee.TextBox.NumericTextBox
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.dtpEnrolldate = New System.Windows.Forms.DateTimePicker
        Me.lblEnrollmentDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnAddTraining = New eZee.Common.eZeeGradientButton
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.lblCourse = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboCourse = New System.Windows.Forms.ComboBox
        Me.lblEligibility = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblNoOfPositons = New System.Windows.Forms.Label
        Me.txtFilledPosition = New eZee.TextBox.AlphanumericTextBox
        Me.txtTotalPosition = New eZee.TextBox.AlphanumericTextBox
        Me.lblFilledPositions = New System.Windows.Forms.Label
        Me.lblCourseFee = New System.Windows.Forms.Label
        Me.gbEmployeeInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtJob = New eZee.TextBox.AlphanumericTextBox
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEnroll = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnMakeActive = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlCourseEnrollmentAndCancellation.SuspendLayout()
        Me.tabcRemark.SuspendLayout()
        Me.tabpEnrollRemark.SuspendLayout()
        Me.tabpCancellationRemark.SuspendLayout()
        Me.gbCourseDetails.SuspendLayout()
        Me.gbEmployeeInformation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCourseEnrollmentAndCancellation
        '
        Me.pnlCourseEnrollmentAndCancellation.Controls.Add(Me.tabcRemark)
        Me.pnlCourseEnrollmentAndCancellation.Controls.Add(Me.gbCourseDetails)
        Me.pnlCourseEnrollmentAndCancellation.Controls.Add(Me.gbEmployeeInformation)
        Me.pnlCourseEnrollmentAndCancellation.Controls.Add(Me.objFooter)
        Me.pnlCourseEnrollmentAndCancellation.Controls.Add(Me.eZeeHeader)
        Me.pnlCourseEnrollmentAndCancellation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCourseEnrollmentAndCancellation.Location = New System.Drawing.Point(0, 0)
        Me.pnlCourseEnrollmentAndCancellation.Name = "pnlCourseEnrollmentAndCancellation"
        Me.pnlCourseEnrollmentAndCancellation.Size = New System.Drawing.Size(735, 397)
        Me.pnlCourseEnrollmentAndCancellation.TabIndex = 0
        '
        'tabcRemark
        '
        Me.tabcRemark.Controls.Add(Me.tabpEnrollRemark)
        Me.tabcRemark.Controls.Add(Me.tabpCancellationRemark)
        Me.tabcRemark.Location = New System.Drawing.Point(393, 186)
        Me.tabcRemark.Name = "tabcRemark"
        Me.tabcRemark.SelectedIndex = 0
        Me.tabcRemark.Size = New System.Drawing.Size(330, 151)
        Me.tabcRemark.TabIndex = 43
        '
        'tabpEnrollRemark
        '
        Me.tabpEnrollRemark.Controls.Add(Me.txtEnrollRemark)
        Me.tabpEnrollRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpEnrollRemark.Name = "tabpEnrollRemark"
        Me.tabpEnrollRemark.Size = New System.Drawing.Size(322, 125)
        Me.tabpEnrollRemark.TabIndex = 0
        Me.tabpEnrollRemark.Tag = "tabpEnrollRemark"
        Me.tabpEnrollRemark.Text = "Enroll Remark"
        Me.tabpEnrollRemark.UseVisualStyleBackColor = True
        '
        'txtEnrollRemark
        '
        Me.txtEnrollRemark.BackColor = System.Drawing.Color.White
        Me.txtEnrollRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEnrollRemark.Flags = 0
        Me.txtEnrollRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEnrollRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEnrollRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtEnrollRemark.Multiline = True
        Me.txtEnrollRemark.Name = "txtEnrollRemark"
        Me.txtEnrollRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEnrollRemark.Size = New System.Drawing.Size(322, 125)
        Me.txtEnrollRemark.TabIndex = 42
        '
        'tabpCancellationRemark
        '
        Me.tabpCancellationRemark.Controls.Add(Me.txtCancellationRemark)
        Me.tabpCancellationRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpCancellationRemark.Name = "tabpCancellationRemark"
        Me.tabpCancellationRemark.Size = New System.Drawing.Size(322, 125)
        Me.tabpCancellationRemark.TabIndex = 1
        Me.tabpCancellationRemark.Tag = "tabpCancellationRemark"
        Me.tabpCancellationRemark.Text = "Cancellation Remark"
        Me.tabpCancellationRemark.UseVisualStyleBackColor = True
        '
        'txtCancellationRemark
        '
        Me.txtCancellationRemark.BackColor = System.Drawing.Color.White
        Me.txtCancellationRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCancellationRemark.Flags = 0
        Me.txtCancellationRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCancellationRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCancellationRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtCancellationRemark.Multiline = True
        Me.txtCancellationRemark.Name = "txtCancellationRemark"
        Me.txtCancellationRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCancellationRemark.Size = New System.Drawing.Size(322, 125)
        Me.txtCancellationRemark.TabIndex = 40
        '
        'gbCourseDetails
        '
        Me.gbCourseDetails.BorderColor = System.Drawing.Color.Black
        Me.gbCourseDetails.Checked = False
        Me.gbCourseDetails.CollapseAllExceptThis = False
        Me.gbCourseDetails.CollapsedHoverImage = Nothing
        Me.gbCourseDetails.CollapsedNormalImage = Nothing
        Me.gbCourseDetails.CollapsedPressedImage = Nothing
        Me.gbCourseDetails.CollapseOnLoad = False
        Me.gbCourseDetails.Controls.Add(Me.lblTrainingYear)
        Me.gbCourseDetails.Controls.Add(Me.cboTrainingYears)
        Me.gbCourseDetails.Controls.Add(Me.txtEligibility)
        Me.gbCourseDetails.Controls.Add(Me.txtTraningFee)
        Me.gbCourseDetails.Controls.Add(Me.lblEndDate)
        Me.gbCourseDetails.Controls.Add(Me.dtpEnrolldate)
        Me.gbCourseDetails.Controls.Add(Me.lblEnrollmentDate)
        Me.gbCourseDetails.Controls.Add(Me.lblStartDate)
        Me.gbCourseDetails.Controls.Add(Me.dtpEndDate)
        Me.gbCourseDetails.Controls.Add(Me.dtpStartDate)
        Me.gbCourseDetails.Controls.Add(Me.objbtnAddTraining)
        Me.gbCourseDetails.Controls.Add(Me.EZeeLine1)
        Me.gbCourseDetails.Controls.Add(Me.objelLine1)
        Me.gbCourseDetails.Controls.Add(Me.lblCourse)
        Me.gbCourseDetails.Controls.Add(Me.lblStatus)
        Me.gbCourseDetails.Controls.Add(Me.cboCourse)
        Me.gbCourseDetails.Controls.Add(Me.lblEligibility)
        Me.gbCourseDetails.Controls.Add(Me.cboStatus)
        Me.gbCourseDetails.Controls.Add(Me.lblNoOfPositons)
        Me.gbCourseDetails.Controls.Add(Me.txtFilledPosition)
        Me.gbCourseDetails.Controls.Add(Me.txtTotalPosition)
        Me.gbCourseDetails.Controls.Add(Me.lblFilledPositions)
        Me.gbCourseDetails.Controls.Add(Me.lblCourseFee)
        Me.gbCourseDetails.ExpandedHoverImage = Nothing
        Me.gbCourseDetails.ExpandedNormalImage = Nothing
        Me.gbCourseDetails.ExpandedPressedImage = Nothing
        Me.gbCourseDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCourseDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCourseDetails.HeaderHeight = 25
        Me.gbCourseDetails.HeaderMessage = ""
        Me.gbCourseDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCourseDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCourseDetails.HeightOnCollapse = 0
        Me.gbCourseDetails.LeftTextSpace = 0
        Me.gbCourseDetails.Location = New System.Drawing.Point(12, 64)
        Me.gbCourseDetails.Name = "gbCourseDetails"
        Me.gbCourseDetails.OpenHeight = 176
        Me.gbCourseDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCourseDetails.ShowBorder = True
        Me.gbCourseDetails.ShowCheckBox = False
        Me.gbCourseDetails.ShowCollapseButton = False
        Me.gbCourseDetails.ShowDefaultBorderColor = True
        Me.gbCourseDetails.ShowDownButton = False
        Me.gbCourseDetails.ShowHeader = True
        Me.gbCourseDetails.Size = New System.Drawing.Size(375, 272)
        Me.gbCourseDetails.TabIndex = 3
        Me.gbCourseDetails.Temp = 0
        Me.gbCourseDetails.Text = "Course Details"
        Me.gbCourseDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTrainingYear
        '
        Me.lblTrainingYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingYear.Location = New System.Drawing.Point(8, 36)
        Me.lblTrainingYear.Name = "lblTrainingYear"
        Me.lblTrainingYear.Size = New System.Drawing.Size(104, 15)
        Me.lblTrainingYear.TabIndex = 140
        Me.lblTrainingYear.Text = "Training Years"
        Me.lblTrainingYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainingYears
        '
        Me.cboTrainingYears.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingYears.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingYears.FormattingEnabled = True
        Me.cboTrainingYears.Location = New System.Drawing.Point(118, 33)
        Me.cboTrainingYears.Name = "cboTrainingYears"
        Me.cboTrainingYears.Size = New System.Drawing.Size(217, 21)
        Me.cboTrainingYears.TabIndex = 139
        '
        'txtEligibility
        '
        Me.txtEligibility.Flags = 0
        Me.txtEligibility.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEligibility.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEligibility.Location = New System.Drawing.Point(118, 87)
        Me.txtEligibility.Name = "txtEligibility"
        Me.txtEligibility.ReadOnly = True
        Me.txtEligibility.Size = New System.Drawing.Size(217, 21)
        Me.txtEligibility.TabIndex = 28
        '
        'txtTraningFee
        '
        Me.txtTraningFee.AllowNegative = False
        Me.txtTraningFee.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTraningFee.DigitsInGroup = 0
        Me.txtTraningFee.Flags = 65536
        Me.txtTraningFee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTraningFee.Location = New System.Drawing.Point(118, 87)
        Me.txtTraningFee.MaxDecimalPlaces = 6
        Me.txtTraningFee.MaxWholeDigits = 21
        Me.txtTraningFee.Name = "txtTraningFee"
        Me.txtTraningFee.Prefix = ""
        Me.txtTraningFee.RangeMax = 1.7976931348623157E+308
        Me.txtTraningFee.RangeMin = -1.7976931348623157E+308
        Me.txtTraningFee.ReadOnly = True
        Me.txtTraningFee.Size = New System.Drawing.Size(105, 21)
        Me.txtTraningFee.TabIndex = 137
        Me.txtTraningFee.Text = "0"
        Me.txtTraningFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTraningFee.Visible = False
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(177, 166)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(63, 15)
        Me.lblEndDate.TabIndex = 73
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEnrolldate
        '
        Me.dtpEnrolldate.Checked = False
        Me.dtpEnrolldate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnrolldate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnrolldate.Location = New System.Drawing.Point(118, 212)
        Me.dtpEnrolldate.Name = "dtpEnrolldate"
        Me.dtpEnrolldate.Size = New System.Drawing.Size(90, 21)
        Me.dtpEnrolldate.TabIndex = 36
        '
        'lblEnrollmentDate
        '
        Me.lblEnrollmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnrollmentDate.Location = New System.Drawing.Point(11, 215)
        Me.lblEnrollmentDate.Name = "lblEnrollmentDate"
        Me.lblEnrollmentDate.Size = New System.Drawing.Size(101, 15)
        Me.lblEnrollmentDate.TabIndex = 35
        Me.lblEnrollmentDate.Text = "Enrollment Date"
        Me.lblEnrollmentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(177, 139)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(63, 15)
        Me.lblStartDate.TabIndex = 72
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Enabled = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(246, 163)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpEndDate.TabIndex = 71
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Enabled = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(246, 136)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpStartDate.TabIndex = 70
        '
        'objbtnAddTraining
        '
        Me.objbtnAddTraining.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddTraining.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddTraining.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddTraining.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddTraining.BorderSelected = False
        Me.objbtnAddTraining.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddTraining.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddTraining.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddTraining.Location = New System.Drawing.Point(341, 60)
        Me.objbtnAddTraining.Name = "objbtnAddTraining"
        Me.objbtnAddTraining.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddTraining.TabIndex = 68
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(8, 187)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(327, 22)
        Me.EZeeLine1.TabIndex = 67
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(8, 111)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(316, 21)
        Me.objelLine1.TabIndex = 59
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCourse
        '
        Me.lblCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.Location = New System.Drawing.Point(8, 63)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(104, 15)
        Me.lblCourse.TabIndex = 1
        Me.lblCourse.Text = "Scheduled Course"
        Me.lblCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(11, 242)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(101, 15)
        Me.lblStatus.TabIndex = 38
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCourse
        '
        Me.cboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourse.DropDownWidth = 400
        Me.cboCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourse.FormattingEnabled = True
        Me.cboCourse.Location = New System.Drawing.Point(118, 60)
        Me.cboCourse.Name = "cboCourse"
        Me.cboCourse.Size = New System.Drawing.Size(217, 21)
        Me.cboCourse.TabIndex = 7
        '
        'lblEligibility
        '
        Me.lblEligibility.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEligibility.Location = New System.Drawing.Point(8, 90)
        Me.lblEligibility.Name = "lblEligibility"
        Me.lblEligibility.Size = New System.Drawing.Size(104, 15)
        Me.lblEligibility.TabIndex = 3
        Me.lblEligibility.Text = "Eligibility"
        Me.lblEligibility.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(118, 239)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(217, 21)
        Me.cboStatus.TabIndex = 37
        '
        'lblNoOfPositons
        '
        Me.lblNoOfPositons.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfPositons.Location = New System.Drawing.Point(8, 139)
        Me.lblNoOfPositons.Name = "lblNoOfPositons"
        Me.lblNoOfPositons.Size = New System.Drawing.Size(104, 15)
        Me.lblNoOfPositons.TabIndex = 4
        Me.lblNoOfPositons.Text = "No. of Participants"
        Me.lblNoOfPositons.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFilledPosition
        '
        Me.txtFilledPosition.Flags = 0
        Me.txtFilledPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilledPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFilledPosition.Location = New System.Drawing.Point(118, 163)
        Me.txtFilledPosition.Name = "txtFilledPosition"
        Me.txtFilledPosition.ReadOnly = True
        Me.txtFilledPosition.Size = New System.Drawing.Size(53, 21)
        Me.txtFilledPosition.TabIndex = 30
        '
        'txtTotalPosition
        '
        Me.txtTotalPosition.Flags = 0
        Me.txtTotalPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTotalPosition.Location = New System.Drawing.Point(118, 136)
        Me.txtTotalPosition.Name = "txtTotalPosition"
        Me.txtTotalPosition.ReadOnly = True
        Me.txtTotalPosition.Size = New System.Drawing.Size(53, 21)
        Me.txtTotalPosition.TabIndex = 29
        '
        'lblFilledPositions
        '
        Me.lblFilledPositions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilledPositions.Location = New System.Drawing.Point(8, 166)
        Me.lblFilledPositions.Name = "lblFilledPositions"
        Me.lblFilledPositions.Size = New System.Drawing.Size(104, 15)
        Me.lblFilledPositions.TabIndex = 5
        Me.lblFilledPositions.Text = "Enrolled Participants"
        Me.lblFilledPositions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCourseFee
        '
        Me.lblCourseFee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourseFee.Location = New System.Drawing.Point(8, 90)
        Me.lblCourseFee.Name = "lblCourseFee"
        Me.lblCourseFee.Size = New System.Drawing.Size(104, 15)
        Me.lblCourseFee.TabIndex = 74
        Me.lblCourseFee.Text = "Course Fees"
        Me.lblCourseFee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCourseFee.Visible = False
        '
        'gbEmployeeInformation
        '
        Me.gbEmployeeInformation.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInformation.Checked = False
        Me.gbEmployeeInformation.CollapseAllExceptThis = False
        Me.gbEmployeeInformation.CollapsedHoverImage = Nothing
        Me.gbEmployeeInformation.CollapsedNormalImage = Nothing
        Me.gbEmployeeInformation.CollapsedPressedImage = Nothing
        Me.gbEmployeeInformation.CollapseOnLoad = False
        Me.gbEmployeeInformation.Controls.Add(Me.txtJob)
        Me.gbEmployeeInformation.Controls.Add(Me.txtDepartment)
        Me.gbEmployeeInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeInformation.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeInformation.Controls.Add(Me.lblJob)
        Me.gbEmployeeInformation.Controls.Add(Me.lblDepartment)
        Me.gbEmployeeInformation.Controls.Add(Me.lblEmployeeName)
        Me.gbEmployeeInformation.ExpandedHoverImage = Nothing
        Me.gbEmployeeInformation.ExpandedNormalImage = Nothing
        Me.gbEmployeeInformation.ExpandedPressedImage = Nothing
        Me.gbEmployeeInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInformation.HeaderHeight = 25
        Me.gbEmployeeInformation.HeaderMessage = ""
        Me.gbEmployeeInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInformation.HeightOnCollapse = 0
        Me.gbEmployeeInformation.LeftTextSpace = 0
        Me.gbEmployeeInformation.Location = New System.Drawing.Point(393, 64)
        Me.gbEmployeeInformation.Name = "gbEmployeeInformation"
        Me.gbEmployeeInformation.OpenHeight = 176
        Me.gbEmployeeInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInformation.ShowBorder = True
        Me.gbEmployeeInformation.ShowCheckBox = False
        Me.gbEmployeeInformation.ShowCollapseButton = False
        Me.gbEmployeeInformation.ShowDefaultBorderColor = True
        Me.gbEmployeeInformation.ShowDownButton = False
        Me.gbEmployeeInformation.ShowHeader = True
        Me.gbEmployeeInformation.Size = New System.Drawing.Size(330, 116)
        Me.gbEmployeeInformation.TabIndex = 2
        Me.gbEmployeeInformation.Temp = 0
        Me.gbEmployeeInformation.Text = "Employee Info"
        Me.gbEmployeeInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJob
        '
        Me.txtJob.Flags = 0
        Me.txtJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJob.Location = New System.Drawing.Point(103, 88)
        Me.txtJob.Name = "txtJob"
        Me.txtJob.ReadOnly = True
        Me.txtJob.Size = New System.Drawing.Size(188, 21)
        Me.txtJob.TabIndex = 33
        '
        'txtDepartment
        '
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(103, 61)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(188, 21)
        Me.txtDepartment.TabIndex = 32
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(297, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 14
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(103, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(188, 21)
        Me.cboEmployee.TabIndex = 15
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 91)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(89, 15)
        Me.lblJob.TabIndex = 11
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 64)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(89, 15)
        Me.lblDepartment.TabIndex = 10
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(89, 15)
        Me.lblEmployeeName.TabIndex = 9
        Me.lblEmployeeName.Text = "Employee Name"
        Me.lblEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEnroll)
        Me.objFooter.Controls.Add(Me.btnMakeActive)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 342)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(735, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(397, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(121, 30)
        Me.btnCancel.TabIndex = 125
        Me.btnCancel.Text = "C&ancel Enrollment"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(627, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 124
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEnroll
        '
        Me.btnEnroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEnroll.BackColor = System.Drawing.Color.White
        Me.btnEnroll.BackgroundImage = CType(resources.GetObject("btnEnroll.BackgroundImage"), System.Drawing.Image)
        Me.btnEnroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEnroll.BorderColor = System.Drawing.Color.Empty
        Me.btnEnroll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEnroll.FlatAppearance.BorderSize = 0
        Me.btnEnroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnroll.ForeColor = System.Drawing.Color.Black
        Me.btnEnroll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEnroll.GradientForeColor = System.Drawing.Color.Black
        Me.btnEnroll.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnroll.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEnroll.Location = New System.Drawing.Point(524, 13)
        Me.btnEnroll.Name = "btnEnroll"
        Me.btnEnroll.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnroll.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEnroll.Size = New System.Drawing.Size(97, 30)
        Me.btnEnroll.TabIndex = 123
        Me.btnEnroll.Text = "&Save"
        Me.btnEnroll.UseVisualStyleBackColor = True
        '
        'btnMakeActive
        '
        Me.btnMakeActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMakeActive.BackColor = System.Drawing.Color.White
        Me.btnMakeActive.BackgroundImage = CType(resources.GetObject("btnMakeActive.BackgroundImage"), System.Drawing.Image)
        Me.btnMakeActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMakeActive.BorderColor = System.Drawing.Color.Empty
        Me.btnMakeActive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMakeActive.FlatAppearance.BorderSize = 0
        Me.btnMakeActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMakeActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMakeActive.ForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMakeActive.GradientForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMakeActive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.Location = New System.Drawing.Point(421, 13)
        Me.btnMakeActive.Name = "btnMakeActive"
        Me.btnMakeActive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMakeActive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMakeActive.Size = New System.Drawing.Size(97, 30)
        Me.btnMakeActive.TabIndex = 126
        Me.btnMakeActive.Text = "&Make Active"
        Me.btnMakeActive.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(735, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Training Course Enrollment / Cancellation"
        '
        'frmEnrollmentAndCancellation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 397)
        Me.Controls.Add(Me.pnlCourseEnrollmentAndCancellation)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEnrollmentAndCancellation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enrollment And Cancellation"
        Me.pnlCourseEnrollmentAndCancellation.ResumeLayout(False)
        Me.tabcRemark.ResumeLayout(False)
        Me.tabpEnrollRemark.ResumeLayout(False)
        Me.tabpEnrollRemark.PerformLayout()
        Me.tabpCancellationRemark.ResumeLayout(False)
        Me.tabpCancellationRemark.PerformLayout()
        Me.gbCourseDetails.ResumeLayout(False)
        Me.gbCourseDetails.PerformLayout()
        Me.gbEmployeeInformation.ResumeLayout(False)
        Me.gbEmployeeInformation.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlCourseEnrollmentAndCancellation As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEnroll As eZee.Common.eZeeLightButton
    Friend WithEvents gbCourseDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbEmployeeInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents lblFilledPositions As System.Windows.Forms.Label
    Friend WithEvents lblNoOfPositons As System.Windows.Forms.Label
    Friend WithEvents lblEligibility As System.Windows.Forms.Label
    Friend WithEvents cboCourse As System.Windows.Forms.ComboBox
    Friend WithEvents txtEligibility As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFilledPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTotalPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents dtpEnrolldate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEnrollmentDate As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents txtCancellationRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEnrollRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents txtJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnMakeActive As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents objbtnAddTraining As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents lblCourseFee As System.Windows.Forms.Label
    Friend WithEvents txtTraningFee As eZee.TextBox.NumericTextBox
    Friend WithEvents tabcRemark As System.Windows.Forms.TabControl
    Friend WithEvents tabpEnrollRemark As System.Windows.Forms.TabPage
    Friend WithEvents tabpCancellationRemark As System.Windows.Forms.TabPage
    Friend WithEvents lblTrainingYear As System.Windows.Forms.Label
    Friend WithEvents cboTrainingYears As System.Windows.Forms.ComboBox
End Class
