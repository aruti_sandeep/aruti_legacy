﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveFormList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveFormList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.lvLeaveFormList = New eZee.Common.eZeeListView(Me.components)
        Me.objColhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhFormNo = New System.Windows.Forms.ColumnHeader
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colLeaveType = New System.Windows.Forms.ColumnHeader
        Me.colhStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhLeaveDays = New System.Windows.Forms.ColumnHeader
        Me.colhApprovedStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhApprovedEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhApprovedDays = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhApplyDate = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhStatusunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhLeavetypeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhUserunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhleaveissueunkid = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtApprover = New eZee.TextBox.AlphanumericTextBox
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblReturnDate = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpApplyDate = New System.Windows.Forms.DateTimePicker
        Me.txtFormNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblApplyDate = New System.Windows.Forms.Label
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnPrint = New eZee.Common.eZeeSplitButton
        Me.cmnuPrintOptions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPreviewForm = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintForm = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmailForm = New System.Windows.Forms.ToolStripMenuItem
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuLeaveFormOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuLeaveAccrue = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCancelLeaveForm = New System.Windows.Forms.ToolStripMenuItem
        Me.objSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.mnuScanFormImages = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPreviewScanDoc = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objEFooter = New eZee.Common.eZeeFooter
        Me.btnEClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmailOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.objdgcolhSkipApproverFlow = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuPrintOptions.SuspendLayout()
        Me.cmnuLeaveFormOperations.SuspendLayout()
        Me.objEFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objChkAll)
        Me.pnlMainInfo.Controls.Add(Me.lvLeaveFormList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objEFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(846, 499)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(20, 192)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 91
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'lvLeaveFormList
        '
        Me.lvLeaveFormList.BackColorOnChecked = True
        Me.lvLeaveFormList.CheckBoxes = True
        Me.lvLeaveFormList.ColumnHeaders = Nothing
        Me.lvLeaveFormList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhCheck, Me.colhFormNo, Me.colhEmpCode, Me.colhEmployee, Me.colLeaveType, Me.colhStartDate, Me.colhEndDate, Me.colhLeaveDays, Me.colhApprovedStartDate, Me.colhApprovedEndDate, Me.colhApprovedDays, Me.colhStatus, Me.colhApplyDate, Me.objcolhEmployeeunkid, Me.objcolhStatusunkid, Me.objcolhLeavetypeunkid, Me.objcolhUserunkid, Me.objcolhleaveissueunkid, Me.objdgcolhSkipApproverFlow})
        Me.lvLeaveFormList.CompulsoryColumns = ""
        Me.lvLeaveFormList.FullRowSelect = True
        Me.lvLeaveFormList.GridLines = True
        Me.lvLeaveFormList.GroupingColumn = Nothing
        Me.lvLeaveFormList.HideSelection = False
        Me.lvLeaveFormList.Location = New System.Drawing.Point(12, 187)
        Me.lvLeaveFormList.MinColumnWidth = 50
        Me.lvLeaveFormList.MultiSelect = False
        Me.lvLeaveFormList.Name = "lvLeaveFormList"
        Me.lvLeaveFormList.OptionalColumns = ""
        Me.lvLeaveFormList.ShowMoreItem = False
        Me.lvLeaveFormList.ShowSaveItem = False
        Me.lvLeaveFormList.ShowSelectAll = True
        Me.lvLeaveFormList.ShowSizeAllColumnsToFit = True
        Me.lvLeaveFormList.Size = New System.Drawing.Size(822, 248)
        Me.lvLeaveFormList.Sortable = True
        Me.lvLeaveFormList.TabIndex = 13
        Me.lvLeaveFormList.UseCompatibleStateImageBehavior = False
        Me.lvLeaveFormList.View = System.Windows.Forms.View.Details
        '
        'objColhCheck
        '
        Me.objColhCheck.Text = ""
        Me.objColhCheck.Width = 25
        '
        'colhFormNo
        '
        Me.colhFormNo.Tag = "colhFormNo"
        Me.colhFormNo.Text = "Application No"
        Me.colhFormNo.Width = 90
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Text = "Employee Code"
        Me.colhEmpCode.Width = 100
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 175
        '
        'colLeaveType
        '
        Me.colLeaveType.Tag = "colLeaveType"
        Me.colLeaveType.Text = "Leave"
        Me.colLeaveType.Width = 150
        '
        'colhStartDate
        '
        Me.colhStartDate.Tag = "colhStartDate"
        Me.colhStartDate.Text = "Start Date"
        Me.colhStartDate.Width = 90
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 90
        '
        'colhLeaveDays
        '
        Me.colhLeaveDays.Tag = "colhLeaveDays"
        Me.colhLeaveDays.Text = "Days"
        Me.colhLeaveDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'colhApprovedStartDate
        '
        Me.colhApprovedStartDate.Tag = "colhApprovedStartDate"
        Me.colhApprovedStartDate.Text = "Approved Start Date"
        Me.colhApprovedStartDate.Width = 125
        '
        'colhApprovedEndDate
        '
        Me.colhApprovedEndDate.Tag = "colhApprovedEndDate"
        Me.colhApprovedEndDate.Text = "Approved End Date"
        Me.colhApprovedEndDate.Width = 125
        '
        'colhApprovedDays
        '
        Me.colhApprovedDays.Tag = "colhApprovedDays"
        Me.colhApprovedDays.Text = "Approved Days"
        Me.colhApprovedDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhApprovedDays.Width = 100
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 85
        '
        'colhApplyDate
        '
        Me.colhApplyDate.Tag = "colhApplyDate"
        Me.colhApplyDate.Text = "Apply Date"
        Me.colhApplyDate.Width = 0
        '
        'objcolhEmployeeunkid
        '
        Me.objcolhEmployeeunkid.Tag = "objcolhEmployeeunkid"
        Me.objcolhEmployeeunkid.Text = "Employeeunkid"
        Me.objcolhEmployeeunkid.Width = 0
        '
        'objcolhStatusunkid
        '
        Me.objcolhStatusunkid.Tag = "objcolhStatusunkid"
        Me.objcolhStatusunkid.Text = "Statusunkid"
        Me.objcolhStatusunkid.Width = 0
        '
        'objcolhLeavetypeunkid
        '
        Me.objcolhLeavetypeunkid.Tag = "objcolhLeavetypeunkid"
        Me.objcolhLeavetypeunkid.Text = "LeaveTypeunkid"
        Me.objcolhLeavetypeunkid.Width = 0
        '
        'objcolhUserunkid
        '
        Me.objcolhUserunkid.Tag = "objcolhUserunkid"
        Me.objcolhUserunkid.Text = "Userunkid"
        Me.objcolhUserunkid.Width = 0
        '
        'objcolhleaveissueunkid
        '
        Me.objcolhleaveissueunkid.Tag = "objcolhleaveissueunkid"
        Me.objcolhleaveissueunkid.Text = "leaveissueunkid"
        Me.objcolhleaveissueunkid.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.txtApprover)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblReturnDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpApplyDate)
        Me.gbFilterCriteria.Controls.Add(Me.txtFormNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblApplyDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblFormNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprover)
        Me.gbFilterCriteria.Controls.Add(Me.objLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(822, 115)
        Me.gbFilterCriteria.TabIndex = 12
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(684, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 234
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.DropDownWidth = 400
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(81, 87)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(161, 21)
        Me.cboLeaveType.TabIndex = 232
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(8, 90)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(67, 15)
        Me.lblLeaveType.TabIndex = 231
        Me.lblLeaveType.Text = "Leave "
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(714, 36)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(96, 21)
        Me.cboStatus.TabIndex = 229
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(641, 39)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(67, 15)
        Me.lblStatus.TabIndex = 228
        Me.lblStatus.Text = "Status"
        '
        'txtApprover
        '
        Me.txtApprover.BackColor = System.Drawing.Color.White
        Me.txtApprover.Flags = 0
        Me.txtApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprover.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApprover.Location = New System.Drawing.Point(81, 60)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.ReadOnly = True
        Me.txtApprover.Size = New System.Drawing.Size(161, 21)
        Me.txtApprover.TabIndex = 228
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(527, 60)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpEndDate.TabIndex = 224
        '
        'lblReturnDate
        '
        Me.lblReturnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReturnDate.Location = New System.Drawing.Point(453, 63)
        Me.lblReturnDate.Name = "lblReturnDate"
        Me.lblReturnDate.Size = New System.Drawing.Size(70, 15)
        Me.lblReturnDate.TabIndex = 223
        Me.lblReturnDate.Text = "End Date"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(527, 33)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpStartDate.TabIndex = 222
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(453, 36)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(70, 15)
        Me.lblStartDate.TabIndex = 221
        Me.lblStartDate.Text = "Start Date"
        '
        'dtpApplyDate
        '
        Me.dtpApplyDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Checked = False
        Me.dtpApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplyDate.Location = New System.Drawing.Point(345, 60)
        Me.dtpApplyDate.Name = "dtpApplyDate"
        Me.dtpApplyDate.ShowCheckBox = True
        Me.dtpApplyDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpApplyDate.TabIndex = 220
        '
        'txtFormNo
        '
        Me.txtFormNo.Flags = 0
        Me.txtFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFormNo.Location = New System.Drawing.Point(345, 33)
        Me.txtFormNo.Name = "txtFormNo"
        Me.txtFormNo.Size = New System.Drawing.Size(101, 21)
        Me.txtFormNo.TabIndex = 13
        '
        'lblApplyDate
        '
        Me.lblApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplyDate.Location = New System.Drawing.Point(259, 63)
        Me.lblApplyDate.Name = "lblApplyDate"
        Me.lblApplyDate.Size = New System.Drawing.Size(80, 15)
        Me.lblApplyDate.TabIndex = 219
        Me.lblApplyDate.Text = "Apply Date"
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(259, 36)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(80, 15)
        Me.lblFormNo.TabIndex = 217
        Me.lblFormNo.Text = "Application No"
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 63)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(67, 15)
        Me.lblApprover.TabIndex = 214
        Me.lblApprover.Text = "Approver"
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(248, 24)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(9, 88)
        Me.objLine1.TabIndex = 88
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(795, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(221, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(772, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(81, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(134, 21)
        Me.cboEmployee.TabIndex = 87
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(67, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnPrint)
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 389)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(846, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnPrint
        '
        Me.btnPrint.BorderColor = System.Drawing.Color.Black
        Me.btnPrint.ContextMenuStrip = Me.cmnuPrintOptions
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrint.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnPrint.Location = New System.Drawing.Point(115, 13)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.ShowDefaultBorderColor = True
        Me.btnPrint.Size = New System.Drawing.Size(97, 30)
        Me.btnPrint.SplitButtonMenu = Me.cmnuPrintOptions
        Me.btnPrint.TabIndex = 123
        Me.btnPrint.Text = "&Print"
        '
        'cmnuPrintOptions
        '
        Me.cmnuPrintOptions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPreviewForm, Me.mnuPrintForm, Me.mnuEmailForm})
        Me.cmnuPrintOptions.Name = "cmnuPrintOptions"
        Me.cmnuPrintOptions.Size = New System.Drawing.Size(147, 70)
        '
        'mnuPreviewForm
        '
        Me.mnuPreviewForm.Name = "mnuPreviewForm"
        Me.mnuPreviewForm.Size = New System.Drawing.Size(146, 22)
        Me.mnuPreviewForm.Text = "Pre&view Form"
        '
        'mnuPrintForm
        '
        Me.mnuPrintForm.Name = "mnuPrintForm"
        Me.mnuPrintForm.Size = New System.Drawing.Size(146, 22)
        Me.mnuPrintForm.Text = "&Print Form"
        '
        'mnuEmailForm
        '
        Me.mnuEmailForm.Name = "mnuEmailForm"
        Me.mnuEmailForm.Size = New System.Drawing.Size(146, 22)
        Me.mnuEmailForm.Text = "&Email Form"
        Me.mnuEmailForm.Visible = False
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.cmnuLeaveFormOperations
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(97, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuLeaveFormOperations
        Me.btnOperation.TabIndex = 122
        Me.btnOperation.Text = "&Operation"
        '
        'cmnuLeaveFormOperations
        '
        Me.cmnuLeaveFormOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLeaveAccrue, Me.mnuCancelLeaveForm, Me.objSeparator, Me.mnuScanFormImages, Me.mnuPreviewScanDoc})
        Me.cmnuLeaveFormOperations.Name = "cmnuLeaveFormOperations"
        Me.cmnuLeaveFormOperations.Size = New System.Drawing.Size(228, 98)
        '
        'mnuLeaveAccrue
        '
        Me.mnuLeaveAccrue.Name = "mnuLeaveAccrue"
        Me.mnuLeaveAccrue.Size = New System.Drawing.Size(227, 22)
        Me.mnuLeaveAccrue.Text = "Leave &Accrue"
        '
        'mnuCancelLeaveForm
        '
        Me.mnuCancelLeaveForm.Name = "mnuCancelLeaveForm"
        Me.mnuCancelLeaveForm.Size = New System.Drawing.Size(227, 22)
        Me.mnuCancelLeaveForm.Text = "Cancel Leave Form"
        '
        'objSeparator
        '
        Me.objSeparator.Name = "objSeparator"
        Me.objSeparator.Size = New System.Drawing.Size(224, 6)
        '
        'mnuScanFormImages
        '
        Me.mnuScanFormImages.Name = "mnuScanFormImages"
        Me.mnuScanFormImages.Size = New System.Drawing.Size(227, 22)
        Me.mnuScanFormImages.Text = "S&can Documents"
        '
        'mnuPreviewScanDoc
        '
        Me.mnuPreviewScanDoc.Name = "mnuPreviewScanDoc"
        Me.mnuPreviewScanDoc.Size = New System.Drawing.Size(227, 22)
        Me.mnuPreviewScanDoc.Text = "Preview Scanned Documents"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(645, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 76
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(741, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 73
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(548, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 75
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(452, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(846, 60)
        Me.eZeeHeader.TabIndex = 10
        Me.eZeeHeader.Title = "Leave Form List"
        '
        'objEFooter
        '
        Me.objEFooter.BorderColor = System.Drawing.Color.Silver
        Me.objEFooter.Controls.Add(Me.btnEClose)
        Me.objEFooter.Controls.Add(Me.btnEmailOk)
        Me.objEFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objEFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objEFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objEFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objEFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objEFooter.Location = New System.Drawing.Point(0, 444)
        Me.objEFooter.Name = "objEFooter"
        Me.objEFooter.Size = New System.Drawing.Size(846, 55)
        Me.objEFooter.TabIndex = 90
        '
        'btnEClose
        '
        Me.btnEClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEClose.BackColor = System.Drawing.Color.White
        Me.btnEClose.BackgroundImage = CType(resources.GetObject("btnEClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEClose.FlatAppearance.BorderSize = 0
        Me.btnEClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEClose.ForeColor = System.Drawing.Color.Black
        Me.btnEClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Location = New System.Drawing.Point(742, 14)
        Me.btnEClose.Name = "btnEClose"
        Me.btnEClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Size = New System.Drawing.Size(90, 30)
        Me.btnEClose.TabIndex = 79
        Me.btnEClose.Text = "Close"
        Me.btnEClose.UseVisualStyleBackColor = True
        '
        'btnEmailOk
        '
        Me.btnEmailOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailOk.BackColor = System.Drawing.Color.White
        Me.btnEmailOk.BackgroundImage = CType(resources.GetObject("btnEmailOk.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailOk.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailOk.FlatAppearance.BorderSize = 0
        Me.btnEmailOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailOk.ForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Location = New System.Drawing.Point(643, 14)
        Me.btnEmailOk.Name = "btnEmailOk"
        Me.btnEmailOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Size = New System.Drawing.Size(90, 30)
        Me.btnEmailOk.TabIndex = 78
        Me.btnEmailOk.Text = "&Ok"
        Me.btnEmailOk.UseVisualStyleBackColor = True
        '
        'objdgcolhSkipApproverFlow
        '
        Me.objdgcolhSkipApproverFlow.Tag = "objdgcolhSkipApproverFlow"
        Me.objdgcolhSkipApproverFlow.Text = "Skip Approver Flow"
        Me.objdgcolhSkipApproverFlow.Width = 0
        '
        'frmLeaveFormList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 499)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveFormList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Form List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuPrintOptions.ResumeLayout(False)
        Me.cmnuLeaveFormOperations.ResumeLayout(False)
        Me.objEFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents lblApplyDate As System.Windows.Forms.Label
    Friend WithEvents dtpApplyDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFormNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReturnDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents lvLeaveFormList As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFormNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApplyDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cmnuLeaveFormOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuLeaveAccrue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnPrint As eZee.Common.eZeeSplitButton
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmnuPrintOptions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPreviewForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrintForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmailForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtApprover As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuScanFormImages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhEmployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhStatusunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhLeavetypeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents colLeaveType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLeaveDays As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhUserunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhleaveissueunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents mnuCancelLeaveForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objEFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnEClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmailOk As eZee.Common.eZeeLightButton
    Friend WithEvents objColhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents colhApprovedStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApprovedEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApprovedDays As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuPreviewScanDoc As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objdgcolhSkipApproverFlow As System.Windows.Forms.ColumnHeader
End Class
