﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 4

Public Class frmLeaveType_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmLeaveType_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLeaveTypeMaster As clsleavetype_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLeaveTypeMasterUnkid As Integer = -1

    'Pinkal (20-Feb-2023) -- Start
    '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
    Private mdtOldLeavePaySetting As DataTable = Nothing
    Private mdtLeavePaySetting As DataTable = Nothing
    Private mstrLeaveNotificationUserIds As String = ""
    'Pinkal (20-Feb-2023) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintLeaveTypeMasterUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintLeaveTypeMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmLeaveType_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveTypeMaster = New clsleavetype_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            setColor()

            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            If objGroup._Groupname.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                chkExcuseLeave.Visible = True
            End If
            objGroup = Nothing
            'Pinkal (14-Feb-2022) -- End

            If menAction = enAction.EDIT_ONE Then
                objLeaveTypeMaster._Leavetypeunkid = mintLeaveTypeMasterUnkid
            End If
            GetValue()
            txtCode.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveType_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveType_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveType_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveType_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveType_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveType_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveTypeMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleavetype_master.SetMessages()
            objfrm._Other_ModuleNames = "clsleavetype_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Type Code cannot be blank. Leave Type Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Type Name cannot be blank. Leave Type Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
                'S.SANDEEP [ 26 SEPT 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'ElseIf (txtMaxamount.Text.Trim = "0" Or txtMaxamount.Text.Trim = "") And txtMaxamount.Visible Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Max Amount cannot be 0.Leave Max Amount is required information."), enMsgBoxStyle.Information)
                '    txtMaxamount.Focus()
                '    Exit Sub
                'S.SANDEEP [ 26 SEPT 2013 ] -- END
            Else
                Dim objMaster As New clsMasterData
                If objMaster.IsColorExist(objlblColor.BackColor.ToArgb(), mintLeaveTypeMasterUnkid) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This Color is used by another transaction.Please select other color for this leave type."), enMsgBoxStyle.Information)
                    txtName.Focus()
                    Exit Sub
                End If
            End If

            If menAction = enAction.EDIT_ONE And objLeaveTypeMaster._IsPaid And objLeaveTypeMaster._IsAccrueAmount AndAlso CInt(cboDeductFrom.SelectedValue) > 0 Then
                If objLeaveTypeMaster.IsDeductedLeaveType(objLeaveTypeMaster._Leavetypeunkid) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can not map ") & txtName.Text.Trim & Language.getMessage(mstrModuleName, 8, " to ") & cboDeductFrom.Text.Trim & "." & Language.getMessage(mstrModuleName, 9, "Reason : ") & txtName.Text.Trim & Language.getMessage(mstrModuleName, 10, " is already mapped with other Leave Type(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If menAction = enAction.EDIT_ONE AndAlso CInt(cboDeductFrom.SelectedValue) <> objLeaveTypeMaster._DeductFromLeaveTypeunkid Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You are about to change or remove mapping of ''Deduct from Leave Type''. Do you wish to continue ?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If

            If radUnpaidleave.Checked = True Then
                If (txtMaxamount.Text.Trim = "0" Or txtMaxamount.Text.Trim = "") And txtMaxamount.Visible Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Max Amount cannot be 0.Leave Max Amount is required information."), enMsgBoxStyle.Information)
                    txtMaxamount.Focus()
                    Exit Sub
                End If
            End If

            If (txtMaxamount.Text.Trim = "0" Or txtMaxamount.Text.Trim = "") And txtMaxamount.Visible Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Max Amount cannot be 0.Leave Max Amount is required information."), enMsgBoxStyle.Information)
                txtMaxamount.Focus()
                Exit Sub
            End If

            'Pinkal (08-Oct-2018) -- End


            If radpaidleave.Checked AndAlso chkLeaveEncashment.Checked AndAlso CInt(cboDeductFrom.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Deduct from Leave Type is compulsory information for Leave Encashment.Please Select Deduct from Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboDeductFrom.Select()
                Exit Sub
            End If



            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            If menAction = enAction.EDIT_ONE Then
                If objLeaveTypeMaster._ConsiderLeaveOnTnAPeriod <> chkLinkTnAPeriod.Checked Then
                    Dim objMaster As New clsMasterData
                    Dim mintPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, False, Nothing)
                    If mintPeriodId > 0 Then
                        Dim objPeriod As New clscommom_period_Tran
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodId

                        Dim mdtStartDate As DateTime = Nothing
                        Dim mdtEndDate As DateTime = Nothing
                        Dim mintModuleRefence As enModuleReference

                        If chkLinkTnAPeriod.Checked Then
                            mdtStartDate = objPeriod._TnA_StartDate
                            mdtEndDate = objPeriod._TnA_EndDate
                            mintModuleRefence = enModuleReference.TnA
                        Else
                            mdtStartDate = objPeriod._Start_Date
                            mdtEndDate = objPeriod._End_Date
                            mintModuleRefence = enModuleReference.Payroll
                        End If

                        Dim objLvForm As New clsleaveform
                        Dim mstrEmployeeIDs As String = objLvForm.GetEmployeeIdsFromIssuedForm(objLeaveTypeMaster._Leavetypeunkid, mdtStartDate, mdtEndDate)
                        objLvForm = Nothing

                        If mstrEmployeeIDs.Length > 0 Then
                            Dim objTnALeaveTran As New clsTnALeaveTran
                            If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), mstrEmployeeIDs, mdtEndDate.Date, mintModuleRefence, Nothing) Then
                                'Sohail (19 Apr 2019) -- Start
                                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You cannot  link/unlink this leave type with TnA period. Reason: Process Payroll is already done for last date of period in which leave dates are falling."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You cannot  link/unlink this leave type with TnA period. Reason: Process Payroll is already done for last date of period in which leave dates are falling.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 16, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                    Dim objFrm As New frmProcessPayroll
                                    objFrm.ShowDialog()
                                End If
                                'Sohail (19 Apr 2019) -- End
                                objTnALeaveTran = Nothing
                                Exit Sub
                            End If
                            objTnALeaveTran = Nothing
                        End If
                    End If
                    objMaster = Nothing
                End If
            End If
            'Pinkal (01-Jan-2019) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If chkIsBlockLeave.Checked = True AndAlso CInt(cboDeductFrom.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Deduct from Leave Type is compulsory information for block leave.Please Select Deduct from Leave Type."), enMsgBoxStyle.Information)
                cboDeductFrom.Select()
                Exit Sub
            End If
            If chkIsBlockLeave.Checked = True Then
                If nudMinDays.Value <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Please set leave application minimum day limit in order to create block leave."), enMsgBoxStyle.Information)
                    nudMinDays.Focus()
                    Exit Sub
                End If
                If nudMaxDays.Value > 0 AndAlso nudMaxDays.Value < nudMinDays.Value Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Maximum day cannot be less than leave application minimum day(s)."), enMsgBoxStyle.Information)
                    nudMaxDays.Focus()
                    Exit Sub
                End If
            End If
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (20-Apr-2022) -- Start
            'Enhancement Prevision Air  - Doing Leave module Enhancement.	
            If chkSetFutureDaysLimit.Checked AndAlso nudSetFutureDaysLimit.Value <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Maximum Future Day Limit cannot be 0.Maximum Future Day Limit is required information."), enMsgBoxStyle.Information)
                nudSetFutureDaysLimit.Focus()
                Exit Sub
            End If
            'Pinkal (20-Apr-2022) -- End


            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLeaveTypeMaster._FormName = mstrModuleName
            objLeaveTypeMaster._LoginEmployeeunkid = 0
            objLeaveTypeMaster._ClientIP = getIP()
            objLeaveTypeMaster._HostName = getHostName()
            objLeaveTypeMaster._FromWeb = False
            objLeaveTypeMaster._AuditUserId = User._Object._Userunkid
objLeaveTypeMaster._CompanyUnkid = Company._Object._Companyunkid
            objLeaveTypeMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then

                'START FOR CHECK WHETHER PARTICULAR LEAVE TYPE MAX AMOUNT IS USED IN LEAVE BALANCE OR NOT  
                If radUnpaidleave.Checked Then
                    Dim objBalancetran As New clsleavebalance_tran

                    Dim dsList As DataSet = objBalancetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, False, False, False, "", Nothing)


                    If dsList.Tables("AccrueTran").Rows.Count > 0 Then 'S.SANDEEP [21 JUL 2016] -- START -- END

                        'S.SANDEEP [21 JUL 2016] -- START
                        'Dim drRow As DataRow() = dsList.Tables("List").Select("leavetypeunkid = " & mintLeaveTypeMasterUnkid)
                        Dim drRow As DataRow() = dsList.Tables("AccrueTran").Select("leavetypeunkid = " & mintLeaveTypeMasterUnkid)
                        'S.SANDEEP [21 JUL 2016] -- END

                    If drRow.Length > 0 And objLeaveTypeMaster._MaxAmount <> CInt(txtMaxamount.Tag) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You cannot Change Maximum Amount for this Leave type.Reason : It is already in use."), enMsgBoxStyle.Information)
                        txtMaxamount.Focus()
                        Exit Sub
                    End If
                    End If 'S.SANDEEP [21 JUL 2016] -- START -- END
                End If

                'END FOR CHECK WHETHER PARTICULAR LEAVE TYPE MAX AMOUNT IS USED IN LEAVE BALANCE OR NOT

                blnFlag = objLeaveTypeMaster.Update()
            Else
                blnFlag = objLeaveTypeMaster.Insert()
            End If

            If blnFlag = False And objLeaveTypeMaster._Message <> "" Then
                eZeeMsgBox.Show(objLeaveTypeMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLeaveTypeMaster = Nothing
                    objLeaveTypeMaster = New clsleavetype_master
                    Call GetValue()
                    txtCode.Select()
                Else
                    mintLeaveTypeMasterUnkid = objLeaveTypeMaster._Leavetypeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnFillColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnFillColor.Click
        Try

            If ColorDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            objlblColor.BackColor = ColorDialog1.Color
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnFillColor_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            Call objFrmLangPopup.displayDialog(txtName.Text, objLeaveTypeMaster._Leavename1, objLeaveTypeMaster._Leavename2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Radio Button's Event"

    Private Sub radpaidleave_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radpaidleave.CheckedChanged, radUnpaidleave.CheckedChanged
        Try
            If radpaidleave.Checked Then
                lblMaxAmount.Visible = False
                txtMaxamount.Visible = False
                grpAccrueAmount.Visible = True
                chkShortLeave.Enabled = True
                cboDeductFrom.Enabled = True
                LblCFAmount.Visible = True
                txtCfAmount.Visible = True
                LblEligibilityAfter.Visible = True
                LblDays.Visible = True

                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA.
                'txtEligibilityAfter.Visible = True
                'Pinkal (29-Sep-2017) -- End


                chkNoAction.Visible = True
                chkIsSickLeave.Enabled = True
                chkLeaveEncashment.Enabled = True
                chkLeaveEncashment.Checked = False


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                nudMinDays.Enabled = True
                nudMaxDays.Enabled = True
                'Pinkal (08-Oct-2018) -- End

                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                chkIsBlockLeave.Enabled = True
                'Pinkal (26-Feb-2019) -- End

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                chkExempFromPayroll.Checked = False
                chkExempFromPayroll.Enabled = False
                'Sohail (21 Oct 2019) -- End

            ElseIf radUnpaidleave.Checked Then
                lblMaxAmount.Visible = True
                txtMaxamount.Visible = True
                grpAccrueAmount.Visible = False
                chkShortLeave.Enabled = False
                chkShortLeave.Checked = False
                cboDeductFrom.Enabled = False
                LblCFAmount.Visible = False
                txtCfAmount.Visible = False
                LblEligibilityAfter.Visible = False
                LblDays.Visible = False


                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA.
                'txtEligibilityAfter.Visible = False
                'Pinkal (29-Sep-2017) -- End

                chkNoAction.Checked = False
                chkNoAction.Visible = False
                chkIsSickLeave.Checked = False
                chkIsSickLeave.Enabled = False
                chkLeaveEncashment.Enabled = False
                chkLeaveEncashment.Checked = False

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                nudMinDays.Value = 0
                nudMinDays.Enabled = False
                nudMaxDays.Value = 0
                nudMaxDays.Enabled = False
                'Pinkal (08-Oct-2018) -- End

                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                chkIsBlockLeave.Checked = False
                chkIsBlockLeave.Enabled = False
                'Pinkal (26-Feb-2019) -- End

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                chkExempFromPayroll.Enabled = True
                'Sohail (21 Oct 2019) -- End
            End If


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If cboDeductFrom.Items.Count > 0 Then cboDeductFrom.SelectedIndex = 0
            If radUnpaidleave.Checked Then If cboDeductFrom.Items.Count > 0 Then cboDeductFrom.SelectedIndex = 0
            'Pinkal (26-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radpaidleave_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radAccrueYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAccrueYes.CheckedChanged, radAccrueNo.CheckedChanged
        Try
            If radAccrueYes.Checked Then
                LblCFAmount.Visible = True
                txtCfAmount.Visible = True
            ElseIf radAccrueNo.Checked Then
                LblCFAmount.Visible = False
                txtCfAmount.Visible = False
                txtCfAmount.Decimal = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAccrueYes_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radIssueWkYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radIssueWkYes.CheckedChanged, radIssueWKNo.CheckedChanged, _
                                                                                                                                                                              radIssueYes.CheckedChanged, radIssueNo.CheckedChanged
        Try

            If (radIssueWkYes.Checked AndAlso radIssueYes.Checked) OrElse (radIssueWKNo.Checked AndAlso radIssueNo.Checked) Then
                chkConsiderLvHlOnWk.Checked = False
                chkConsiderLvHlOnWk.Enabled = False
            Else
                chkConsiderLvHlOnWk.Enabled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radIssueWkYes_CheckedChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtMaxamount.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboGender.BackColor = GUI.ColorOptional
            cboDeductFrom.BackColor = GUI.ColorOptional

            'Pinkal (24-Feb-2013) -- Start
            'Enhancement : TRA Changes
            txtCfAmount.BackColor = GUI.ColorOptional

            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            'txtEligibilityAfter.BackColor = GUI.ColorOptional
            nudEligibilityExpenseDays.BackColor = GUI.ColorOptional
            'Pinkal (29-Sep-2017) -- End



            'Pinkal (24-Feb-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objLeaveTypeMaster._Leavetypecode
            txtName.Text = objLeaveTypeMaster._Leavename
            objlblColor.BackColor = Color.FromArgb(objLeaveTypeMaster._Color)
            txtDescription.Text = objLeaveTypeMaster._Description
            radIssueYes.Checked = objLeaveTypeMaster._Isissueonholiday
            radIssueNo.Checked = Not objLeaveTypeMaster._Isissueonholiday
            radAccrueYes.Checked = objLeaveTypeMaster._IsAccrueAmount
            radAccrueNo.Checked = Not objLeaveTypeMaster._IsAccrueAmount
            chkShortLeave.Checked = objLeaveTypeMaster._IsShortLeave
            radIssueWkYes.Checked = objLeaveTypeMaster._Isissueonweekend
            radIssueWKNo.Checked = Not objLeaveTypeMaster._Isissueonweekend
            chkConsiderLvHlOnWk.Checked = objLeaveTypeMaster._IsLeaveHL_onWK

            If objLeaveTypeMaster._IsPaid Then
                radpaidleave.Checked = objLeaveTypeMaster._IsPaid
            Else
                If menAction = enAction.ADD_ONE Or menAction = enAction.ADD_CONTINUE Then
                    radUnpaidleave.Checked = False
                    radpaidleave.Checked = True
                Else
                    radUnpaidleave.Checked = True
                End If

            End If
            If menAction = enAction.ADD_ONE Or menAction = enAction.ADD_CONTINUE Then
                radIssueNo.Checked = False
                radIssueYes.Checked = True

                radAccrueNo.Checked = False
                radAccrueYes.Checked = True
            End If

            chkLeaveEncashment.Checked = objLeaveTypeMaster._IsLeaveEncashment
            cboGender.SelectedValue = objLeaveTypeMaster._Gender
            cboDeductFrom.SelectedValue = objLeaveTypeMaster._DeductFromLeaveTypeunkid
            txtCfAmount.Text = objLeaveTypeMaster._CFAmount.ToString

            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            'txtEligibilityAfter.Text = objLeaveTypeMaster._EligibilityAfter.ToString()
            nudEligibilityExpenseDays.Value = objLeaveTypeMaster._EligibilityOnAfter_Expense
            'Pinkal (29-Sep-2017) -- End

            chkNoAction.Checked = objLeaveTypeMaster._IsNoAction
            chkNoAction_CheckedChanged(New Object(), New EventArgs())
            chkIsSickLeave.Checked = objLeaveTypeMaster._IsSickLeave
            chkLeaveForm.Checked = objLeaveTypeMaster._IsCheckDocOnLeaveForm
            chkLeaveIssue.Checked = objLeaveTypeMaster._IsCheckDocOnLeaveIssue
            txtMaxamount.Text = objLeaveTypeMaster._MaxAmount.ToString
            txtMaxamount.Tag = objLeaveTypeMaster._MaxAmount.ToString

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            chkLvCancelDocMandatory.Checked = objLeaveTypeMaster._CancelLvFormDocMandatory
            'Pinkal (28-Nov-2017) -- End

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            chkSkipApprovalflow.Checked = objLeaveTypeMaster._SkipApproverFlow
            If menAction = enAction.EDIT_ONE Then
                chkSkipApprovalflow.Enabled = False
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            nudMinDays.Value = CDec(objLeaveTypeMaster._LVDaysMinLimit)
            nudMaxDays.Value = CDec(objLeaveTypeMaster._LVDaysMaxLimit)
            'Pinkal (08-Oct-2018) -- End

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            chkLinkTnAPeriod.Checked = objLeaveTypeMaster._ConsiderLeaveOnTnAPeriod
            chkLinkTnAPeriod.Enabled = ConfigParameter._Object._IsSeparateTnAPeriod
            'Pinkal (01-Jan-2019) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            chkIsBlockLeave.Checked = objLeaveTypeMaster._IsBlockLeave
            chkLvAccrossFYELC.Checked = objLeaveTypeMaster._IsStopApplyAcrossFYELC
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            chkConsiderExpense.Checked = objLeaveTypeMaster._ConsiderExpense
            'Pinkal (03-May-2019) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            chkShowOnESS.Checked = objLeaveTypeMaster._ShowOnESS
            'Pinkal (25-May-2019) -- End

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            chkExempFromPayroll.Checked = objLeaveTypeMaster._Isexempt_from_payroll
            'Sohail (21 Oct 2019) -- End


            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            chkDoNotApplyFutureDates.Checked = objLeaveTypeMaster._DoNotApplyForFutureDates
            'Pinkal (05-Jun-2020) -- End


            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            chkExcuseLeave.Checked = objLeaveTypeMaster._IsExcuseLeave
            'Pinkal (14-Feb-2022) -- End


            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            chkDoNotApplyFutureDates_CheckedChanged(chkDoNotApplyFutureDates, New EventArgs())
            nudSetFutureDaysLimit.Value = CInt(objLeaveTypeMaster._MaxFutureDaysLimit)
            If nudSetFutureDaysLimit.Value > 0 Then
                chkSetFutureDaysLimit.Checked = True
            End If
            'Pinkal (14-Feb-2022) -- End

            'Pinkal (20-Feb-2023) -- Start
            '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
            mstrLeaveNotificationUserIds = objLeaveTypeMaster._LeaveNotificationUserIds
            'Pinkal (20-Feb-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLeaveTypeMaster._Leavetypecode = txtCode.Text.Trim
            objLeaveTypeMaster._Leavename = txtName.Text.Trim
            objLeaveTypeMaster._Description = txtDescription.Text.Trim
            objLeaveTypeMaster._Color = objlblColor.BackColor.ToArgb
            objLeaveTypeMaster._Isissueonholiday = radIssueYes.Checked
            objLeaveTypeMaster._Isissueonweekend = radIssueWkYes.Checked
            objLeaveTypeMaster._IsLeaveHL_onWK = chkConsiderLvHlOnWk.Checked


            If radpaidleave.Checked Then
                objLeaveTypeMaster._IsPaid = radpaidleave.Checked
                objLeaveTypeMaster._IsAccrueAmount = radAccrueYes.Checked

                If chkShortLeave.Checked Then
                    objLeaveTypeMaster._MaxAmount = CInt(IIf(txtMaxamount.Text.Trim = "", 0, CInt(txtMaxamount.Text)))
                Else

                    If CInt(cboDeductFrom.SelectedValue) > 0 Then
                        objLeaveTypeMaster._MaxAmount = CInt(IIf(txtMaxamount.Text.Trim.Length > 0, CInt(txtMaxamount.Text.Trim), 0))
                    Else
                objLeaveTypeMaster._MaxAmount = 0
                End If
                End If

                objLeaveTypeMaster._IsShortLeave = chkShortLeave.Checked
                objLeaveTypeMaster._CFAmount = CDec(IIf(txtCfAmount.Text.Trim.Length > 0, txtCfAmount.Text, 0))

                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
                'objLeaveTypeMaster._EligibilityAfter = CInt(IIf(txtEligibilityAfter.Text.Trim.Length > 0, txtEligibilityAfter.Text, 0))
                objLeaveTypeMaster._EligibilityOnAfter_Expense = CInt(nudEligibilityExpenseDays.Value)
                'Pinkal (29-Sep-2017) -- End


                objLeaveTypeMaster._IsNoAction = chkNoAction.Checked
                objLeaveTypeMaster._IsSickLeave = chkIsSickLeave.Checked
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                objLeaveTypeMaster._Isexempt_from_payroll = False
                'Sohail (21 Oct 2019) -- End

            ElseIf radUnpaidleave.Checked Then
                objLeaveTypeMaster._IsAccrueAmount = False
                objLeaveTypeMaster._IsPaid = radpaidleave.Checked
                If txtMaxamount.Text.Trim = "" Then
                    txtMaxamount.Text = "0"
                End If

                objLeaveTypeMaster._MaxAmount = CInt(txtMaxamount.Text)
                objLeaveTypeMaster._CFAmount = 0


                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA.
                'objLeaveTypeMaster._EligibilityAfter = 0
                objLeaveTypeMaster._EligibilityOnAfter_Expense = 0
                'Pinkal (29-Sep-2017) -- End

                objLeaveTypeMaster._IsNoAction = False
                objLeaveTypeMaster._IsSickLeave = False
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                objLeaveTypeMaster._Isexempt_from_payroll = chkExempFromPayroll.Checked
                'Sohail (21 Oct 2019) -- End

            End If

            objLeaveTypeMaster._Gender = CInt(cboGender.SelectedValue)
            objLeaveTypeMaster._DeductFromLeaveTypeunkid = CInt(cboDeductFrom.SelectedValue)
            objLeaveTypeMaster._IsCheckDocOnLeaveForm = chkLeaveForm.Checked
            objLeaveTypeMaster._IsCheckDocOnLeaveIssue = chkLeaveIssue.Checked
            objLeaveTypeMaster._IsLeaveEncashment = chkLeaveEncashment.Checked

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            objLeaveTypeMaster._CancelLvFormDocMandatory = chkLvCancelDocMandatory.Checked
            'Pinkal (28-Nov-2017) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objLeaveTypeMaster._SkipApproverFlow = chkSkipApprovalflow.Checked
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objLeaveTypeMaster._LVDaysMinLimit = CInt(nudMinDays.Value)
            objLeaveTypeMaster._LVDaysMaxLimit = CInt(nudMaxDays.Value)
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objLeaveTypeMaster._ConsiderLeaveOnTnAPeriod = chkLinkTnAPeriod.Checked
            'Pinkal (01-Jan-2019) -- End

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objLeaveTypeMaster._IsBlockLeave = chkIsBlockLeave.Checked
            objLeaveTypeMaster._IsStopApplyAcrossFYELC = chkLvAccrossFYELC.Checked
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            objLeaveTypeMaster._ConsiderExpense = chkConsiderExpense.Checked
            'Pinkal (03-May-2019) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            objLeaveTypeMaster._ShowOnESS = chkShowOnESS.Checked
            'Pinkal (25-May-2019) -- End


            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            objLeaveTypeMaster._DoNotApplyForFutureDates = chkDoNotApplyFutureDates.Checked
            'Pinkal (05-Jun-2020) -- End


            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            objLeaveTypeMaster._IsExcuseLeave = chkExcuseLeave.Checked
            'Pinkal (14-Feb-2022) -- End


            'Pinkal (20-Apr-2022) -- Start
            'Enhancement Prevision Air  - Doing Leave module Enhancement.	
            objLeaveTypeMaster._MaxFutureDaysLimit = CInt(nudSetFutureDaysLimit.Value)
            'Pinkal (20-Apr-2022) -- End

            'Pinkal (20-Feb-2023) -- Start
            '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
            objLeaveTypeMaster._dtLeavePay = mdtLeavePaySetting
            objLeaveTypeMaster._OlddtLeavePay = mdtOldLeavePaySetting
            objLeaveTypeMaster._LeaveNotificationUserIds = mstrLeaveNotificationUserIds
            objLeaveTypeMaster._UserId = User._Object._Userunkid
            objLeaveTypeMaster._WebFormName = Me.Name
            objLeaveTypeMaster._Isweb = False
            'Pinkal (20-Feb-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMaster As New clsMasterData
            Dim dsList As DataSet = objMaster.getGenderList("Gender", True)
            With cboGender

                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Gender")
            End With

            dsList = Nothing
            dsList = objLeaveTypeMaster.GetListForDeductFromLeaveType("LeaveType", True)

            If menAction = enAction.EDIT_ONE Then
                Dim drRow As DataRow() = dsList.Tables(0).Select("leavetypeunkid = " & mintLeaveTypeMasterUnkid)
                If drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If
            End If

            With cboDeductFrom
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LeaveType")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo ", mstrModuleName)
        End Try
    End Sub


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private Sub VisibilityForDeductFromBlockLeave()
        Try
            If chkShortLeave.Checked Then
                txtMaxamount.Visible = False
                lblMaxAmount.Visible = False
            ElseIf (chkShortLeave.Checked = False AndAlso CInt(cboDeductFrom.SelectedValue) > 0) OrElse radUnpaidleave.Checked Then
                txtMaxamount.Visible = True
                lblMaxAmount.Visible = True
            End If

            If radpaidleave.Checked AndAlso chkIsBlockLeave.Checked = False Then
                txtMaxamount.Decimal = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VisibilityForDeductFromBlockLeave", mstrModuleName)
        End Try
    End Sub
    'Pinkal (26-Feb-2019) -- End


#End Region

#Region "ComboBox Event"

    Private Sub cboDeductFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDeductFrom.SelectedIndexChanged
        Try

            If CInt(cboDeductFrom.SelectedValue) > 0 Then
                chkShortLeave.Enabled = False

                'Pinkal (18-Jun-2015) -- Start
                'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
                chkShortLeave.Checked = False
                'Pinkal (18-Jun-2015) -- End

                grpAccrueAmount.Visible = False
                lblMaxAmount.Visible = True
                txtMaxamount.Visible = True

                'Pinkal (02-Jan-2018) -- Start
                'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                'Control to be put on application/not just issuing. employee to be warned and stopped.
                'radAccrueNo.Checked = False
                radAccrueYes.Checked = False
                'Pinkal (02-Jan-2018) -- End
                radAccrueNo.Checked = True
            Else
                chkShortLeave.Enabled = True
                grpAccrueAmount.Visible = True
                lblMaxAmount.Visible = False
                txtMaxamount.Visible = False
                radAccrueYes.Checked = True
            End If
            If menAction <> enAction.EDIT_ONE Then txtMaxamount.Decimal = 0

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            VisibilityForDeductFromBlockLeave()
            'Pinkal (26-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeductFrom_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkShortLeave_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShortLeave.CheckedChanged
        Try
            If chkShortLeave.Checked Then
                grpAccrueAmount.Visible = False
            Else
                grpAccrueAmount.Visible = True
                lblMaxDays.Visible = False
                txtMaxamount.Visible = False
                txtMaxamount.BackColor = GUI.ColorOptional
            End If

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            chkLvAccrossFYELC.Enabled = Not chkShortLeave.Checked
            chkLvAccrossFYELC.Checked = chkShortLeave.Checked
            'Pinkal (26-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShortLeave_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkNoAction_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoAction.CheckedChanged
        Try
            If chkNoAction.Checked Then
                txtCfAmount.Enabled = False
                txtCfAmount.Decimal = 0
            Else
                txtCfAmount.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNoAction_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkLeaveForm_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLeaveForm.CheckedChanged, chkLeaveIssue.CheckedChanged
        Try

            If CType(sender, CheckBox).Name = "chkLeaveForm" Then

                If CType(sender, CheckBox).Checked Then
                    chkLeaveIssue.Checked = False
                End If

            ElseIf CType(sender, CheckBox).Name = "chkLeaveIssue" Then

                If CType(sender, CheckBox).Checked Then
                    chkLeaveForm.Checked = False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkLeaveForm_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private Sub chkIsBlockLeave_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIsBlockLeave.CheckedChanged
        Try
            VisibilityForDeductFromBlockLeave()
            chkLvAccrossFYELC.Checked = chkIsBlockLeave.Checked
            chkLvAccrossFYELC.Enabled = Not chkIsBlockLeave.Checked
            chkSkipApprovalflow.Enabled = Not chkIsBlockLeave.Checked
            If chkIsBlockLeave.Checked Then
                chkSkipApprovalflow.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIsBlockLeave_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (26-Feb-2019) -- End


    'Pinkal (20-Apr-2022) -- Start
    'Enhancement Prevision Air  - Doing Leave module Enhancement.	
    Private Sub chkDoNotApplyFutureDates_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDoNotApplyFutureDates.CheckedChanged
        Try
            chkSetFutureDaysLimit.Enabled = Not chkDoNotApplyFutureDates.Checked
            nudSetFutureDaysLimit.Enabled = chkSetFutureDaysLimit.Checked
            If chkDoNotApplyFutureDates.Checked Then chkSetFutureDaysLimit.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkDoNotApplyFutureDates_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSetFutureDaysLimit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSetFutureDaysLimit.CheckedChanged
        Try
            nudSetFutureDaysLimit.Enabled = chkSetFutureDaysLimit.Checked
            If chkSetFutureDaysLimit.Checked = False Then nudSetFutureDaysLimit.Value = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSetFutureDaysLimit_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Apr-2022) -- End

    'Pinkal (03-Feb-2023) -- Start
    '(A1X-593) New report - Claim Approvers Report.
    Private Sub chkIsSickLeave_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIsSickLeave.CheckedChanged
        Try
            lnkLeavePaySettings.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIsSickLeave_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (03-Feb-2023) -- End


#End Region

#Region "Linkbutton Events"

    'Pinkal (20-Feb-2023) -- Start
    '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
    Private Sub lnkLeavePaySettings_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkLeavePaySettings.LinkClicked
        Dim objLeavePay As New frmLeavePaySetting
        Try
            If menAction <> enAction.EDIT_ONE Then mstrLeaveNotificationUserIds = ""

            Dim objLeavePaySetting As New clsleavepaysettings
            Dim mstrFilter As String = ""
            objLeavePaySetting.GetList("List", mintLeaveTypeMasterUnkid, True, Nothing)

            mdtOldLeavePaySetting = objLeavePaySetting._dtPaySetting.Copy

            objLeavePay.displayDialog(mdtLeavePaySetting, mdtOldLeavePaySetting, mstrLeaveNotificationUserIds, mintLeaveTypeMasterUnkid, menAction)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkLeavePaySettings_LinkClicked", mstrModuleName)
        Finally
            objLeavePay = Nothing
        End Try
    End Sub
    'Pinkal (20-Feb-2023) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()

            Me.gbLeaveTypeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveTypeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbLeaveSettings.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveSettings.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbLeaveTypeInfo.Text = Language._Object.getCaption(Me.gbLeaveTypeInfo.Name, Me.gbLeaveTypeInfo.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblColor.Text = Language._Object.getCaption(Me.lblColor.Name, Me.lblColor.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.radUnpaidleave.Text = Language._Object.getCaption(Me.radUnpaidleave.Name, Me.radUnpaidleave.Text)
			Me.radpaidleave.Text = Language._Object.getCaption(Me.radpaidleave.Name, Me.radpaidleave.Text)
			Me.grpIssueHoliday.Text = Language._Object.getCaption(Me.grpIssueHoliday.Name, Me.grpIssueHoliday.Text)
			Me.radIssueYes.Text = Language._Object.getCaption(Me.radIssueYes.Name, Me.radIssueYes.Text)
			Me.radIssueNo.Text = Language._Object.getCaption(Me.radIssueNo.Name, Me.radIssueNo.Text)
            Me.lblMaxDays.Text = Language._Object.getCaption(Me.lblMaxDays.Name, Me.lblMaxDays.Text)
			Me.chkShortLeave.Text = Language._Object.getCaption(Me.chkShortLeave.Name, Me.chkShortLeave.Text)
			Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
			Me.lblDeductFrom.Text = Language._Object.getCaption(Me.lblDeductFrom.Name, Me.lblDeductFrom.Text)
			Me.LblCFAmount.Text = Language._Object.getCaption(Me.LblCFAmount.Name, Me.LblCFAmount.Text)
			Me.LblDays.Text = Language._Object.getCaption(Me.LblDays.Name, Me.LblDays.Text)
			Me.LblEligibilityAfter.Text = Language._Object.getCaption(Me.LblEligibilityAfter.Name, Me.LblEligibilityAfter.Text)
			Me.chkNoAction.Text = Language._Object.getCaption(Me.chkNoAction.Name, Me.chkNoAction.Text)
			Me.grpAccrueAmount.Text = Language._Object.getCaption(Me.grpAccrueAmount.Name, Me.grpAccrueAmount.Text)
			Me.radAccrueNo.Text = Language._Object.getCaption(Me.radAccrueNo.Name, Me.radAccrueNo.Text)
			Me.radAccrueYes.Text = Language._Object.getCaption(Me.radAccrueYes.Name, Me.radAccrueYes.Text)
			Me.chkIsSickLeave.Text = Language._Object.getCaption(Me.chkIsSickLeave.Name, Me.chkIsSickLeave.Text)
			Me.grpAttachDocument.Text = Language._Object.getCaption(Me.grpAttachDocument.Name, Me.grpAttachDocument.Text)
			Me.chkLeaveForm.Text = Language._Object.getCaption(Me.chkLeaveForm.Name, Me.chkLeaveForm.Text)
			Me.chkLeaveIssue.Text = Language._Object.getCaption(Me.chkLeaveIssue.Name, Me.chkLeaveIssue.Text)
			Me.chkLeaveEncashment.Text = Language._Object.getCaption(Me.chkLeaveEncashment.Name, Me.chkLeaveEncashment.Text)
			Me.grpIssueWeekend.Text = Language._Object.getCaption(Me.grpIssueWeekend.Name, Me.grpIssueWeekend.Text)
			Me.radIssueWKNo.Text = Language._Object.getCaption(Me.radIssueWKNo.Name, Me.radIssueWKNo.Text)
			Me.radIssueWkYes.Text = Language._Object.getCaption(Me.radIssueWkYes.Name, Me.radIssueWkYes.Text)
			Me.chkConsiderLvHlOnWk.Text = Language._Object.getCaption(Me.chkConsiderLvHlOnWk.Name, Me.chkConsiderLvHlOnWk.Text)
			Me.lblEligibilityDaysforLeaveExpense.Text = Language._Object.getCaption(Me.lblEligibilityDaysforLeaveExpense.Name, Me.lblEligibilityDaysforLeaveExpense.Text)
			Me.chkLvCancelDocMandatory.Text = Language._Object.getCaption(Me.chkLvCancelDocMandatory.Name, Me.chkLvCancelDocMandatory.Text)
			Me.chkSkipApprovalflow.Text = Language._Object.getCaption(Me.chkSkipApprovalflow.Name, Me.chkSkipApprovalflow.Text)
			Me.gbLeaveSettings.Text = Language._Object.getCaption(Me.gbLeaveSettings.Name, Me.gbLeaveSettings.Text)
			Me.LblMinDays.Text = Language._Object.getCaption(Me.LblMinDays.Name, Me.LblMinDays.Text)
			Me.lblMaxAmount.Text = Language._Object.getCaption(Me.lblMaxAmount.Name, Me.lblMaxAmount.Text)
			Me.grpLeaveDays.Text = Language._Object.getCaption(Me.grpLeaveDays.Name, Me.grpLeaveDays.Text)
			Me.chkIsBlockLeave.Text = Language._Object.getCaption(Me.chkIsBlockLeave.Name, Me.chkIsBlockLeave.Text)
			Me.chkLvAccrossFYELC.Text = Language._Object.getCaption(Me.chkLvAccrossFYELC.Name, Me.chkLvAccrossFYELC.Text)
			Me.chkLinkTnAPeriod.Text = Language._Object.getCaption(Me.chkLinkTnAPeriod.Name, Me.chkLinkTnAPeriod.Text)
			Me.chkConsiderExpense.Text = Language._Object.getCaption(Me.chkConsiderExpense.Name, Me.chkConsiderExpense.Text)
			Me.chkShowOnESS.Text = Language._Object.getCaption(Me.chkShowOnESS.Name, Me.chkShowOnESS.Text)
            Me.chkExempFromPayroll.Text = Language._Object.getCaption(Me.chkExempFromPayroll.Name, Me.chkExempFromPayroll.Text)
			Me.chkDoNotApplyFutureDates.Text = Language._Object.getCaption(Me.chkDoNotApplyFutureDates.Name, Me.chkDoNotApplyFutureDates.Text)
            Me.chkExcuseLeave.Text = Language._Object.getCaption(Me.chkExcuseLeave.Name, Me.chkExcuseLeave.Text)
            Me.chkSetFutureDaysLimit.Text = Language._Object.getCaption(Me.chkSetFutureDaysLimit.Name, Me.chkSetFutureDaysLimit.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub


	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Leave Type Code cannot be blank. Leave Type Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Leave Type Name cannot be blank. Leave Type Name is required information.")
			Language.setMessage(mstrModuleName, 3, "Leave Max Amount cannot be 0.Leave Max Amount is required information.")
			Language.setMessage(mstrModuleName, 4, "This Color is used by another transaction.Please select other color for this leave type.")
			Language.setMessage(mstrModuleName, 5, "You cannot Change Maximum Amount for this Leave type.Reason : It is already in use.")
			Language.setMessage(mstrModuleName, 6, "You are about to change or remove mapping of ''Deduct from Leave Type''. Do you wish to continue ?")
			Language.setMessage(mstrModuleName, 7, "You can not map")
			Language.setMessage(mstrModuleName, 8, " to")
			Language.setMessage(mstrModuleName, 9, "Reason :")
			Language.setMessage(mstrModuleName, 10, " is already mapped with other Leave Type(s).")
			Language.setMessage(mstrModuleName, 11, "Deduct from Leave Type is compulsory information for Leave Encashment.Please Select Deduct from Leave Type.")
			Language.setMessage(mstrModuleName, 12, "You cannot  link/unlink this leave type with TnA period. Reason: Process Payroll is already done for last date of period in which leave dates are falling.")
			Language.setMessage(mstrModuleName, 13, "Sorry, Please set leave application minimum day limit in order to create block leave.")
			Language.setMessage(mstrModuleName, 14, "Sorry, Maximum day cannot be less than leave application minimum day(s).")
			Language.setMessage(mstrModuleName, 16, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 17, "Deduct from Leave Type is compulsory information for block leave.Please Select Deduct from Leave Type.")
            Language.setMessage(mstrModuleName, 18, "Sorry, Maximum Future Day Limit cannot be 0.Maximum Future Day Limit is required information.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
    
End Class