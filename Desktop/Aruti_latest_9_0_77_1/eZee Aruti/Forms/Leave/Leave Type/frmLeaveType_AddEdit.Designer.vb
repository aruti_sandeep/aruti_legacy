﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveType_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveType_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbLeaveTypeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbLeaveSettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlLeaveSettings = New System.Windows.Forms.Panel
        Me.chkExcuseLeave = New System.Windows.Forms.CheckBox
        Me.chkDoNotApplyFutureDates = New System.Windows.Forms.CheckBox
        Me.chkExempFromPayroll = New System.Windows.Forms.CheckBox
        Me.chkShowOnESS = New System.Windows.Forms.CheckBox
        Me.chkConsiderExpense = New System.Windows.Forms.CheckBox
        Me.chkLinkTnAPeriod = New System.Windows.Forms.CheckBox
        Me.chkLvAccrossFYELC = New System.Windows.Forms.CheckBox
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.grpAccrueAmount = New System.Windows.Forms.GroupBox
        Me.radAccrueNo = New System.Windows.Forms.RadioButton
        Me.radAccrueYes = New System.Windows.Forms.RadioButton
        Me.grpAttachDocument = New System.Windows.Forms.GroupBox
        Me.chkLvCancelDocMandatory = New System.Windows.Forms.CheckBox
        Me.chkLeaveIssue = New System.Windows.Forms.CheckBox
        Me.chkLeaveForm = New System.Windows.Forms.CheckBox
        Me.chkIsBlockLeave = New System.Windows.Forms.CheckBox
        Me.grpLeaveDays = New System.Windows.Forms.GroupBox
        Me.nudMaxDays = New System.Windows.Forms.NumericUpDown
        Me.lblMaxDays = New System.Windows.Forms.Label
        Me.LblMinDays = New System.Windows.Forms.Label
        Me.nudMinDays = New System.Windows.Forms.NumericUpDown
        Me.txtMaxamount = New eZee.TextBox.NumericTextBox
        Me.lblMaxAmount = New System.Windows.Forms.Label
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.radpaidleave = New System.Windows.Forms.RadioButton
        Me.grpIssueWeekend = New System.Windows.Forms.GroupBox
        Me.radIssueWKNo = New System.Windows.Forms.RadioButton
        Me.radIssueWkYes = New System.Windows.Forms.RadioButton
        Me.chkConsiderLvHlOnWk = New System.Windows.Forms.CheckBox
        Me.lblEligibilityDaysforLeaveExpense = New System.Windows.Forms.Label
        Me.radUnpaidleave = New System.Windows.Forms.RadioButton
        Me.nudEligibilityExpenseDays = New System.Windows.Forms.NumericUpDown
        Me.chkSkipApprovalflow = New System.Windows.Forms.CheckBox
        Me.chkShortLeave = New System.Windows.Forms.CheckBox
        Me.chkIsSickLeave = New System.Windows.Forms.CheckBox
        Me.grpIssueHoliday = New System.Windows.Forms.GroupBox
        Me.radIssueNo = New System.Windows.Forms.RadioButton
        Me.radIssueYes = New System.Windows.Forms.RadioButton
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.cboDeductFrom = New System.Windows.Forms.ComboBox
        Me.lblDeductFrom = New System.Windows.Forms.Label
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.objlblColor = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnFillColor = New eZee.Common.eZeeGradientButton
        Me.lblColor = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.chkNoAction = New System.Windows.Forms.CheckBox
        Me.LblDays = New System.Windows.Forms.Label
        Me.LblCFAmount = New System.Windows.Forms.Label
        Me.txtCfAmount = New eZee.TextBox.NumericTextBox
        Me.LblEligibilityAfter = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkLeaveEncashment = New System.Windows.Forms.CheckBox
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog
        Me.lnkLeavePaySettings = New System.Windows.Forms.LinkLabel
        Me.pnlMainInfo.SuspendLayout()
        Me.gbLeaveTypeInfo.SuspendLayout()
        Me.gbLeaveSettings.SuspendLayout()
        Me.pnlLeaveSettings.SuspendLayout()
        CType(Me.nudSetFutureDaysLimit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.grpAccrueAmount.SuspendLayout()
        Me.grpAttachDocument.SuspendLayout()
        Me.grpLeaveDays.SuspendLayout()
        CType(Me.nudMaxDays, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMinDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpIssueWeekend.SuspendLayout()
        CType(Me.nudEligibilityExpenseDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpIssueHoliday.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbLeaveTypeInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.chkLeaveEncashment)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(669, 467)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbLeaveTypeInfo
        '
        Me.gbLeaveTypeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveTypeInfo.Checked = False
        Me.gbLeaveTypeInfo.CollapseAllExceptThis = False
        Me.gbLeaveTypeInfo.CollapsedHoverImage = Nothing
        Me.gbLeaveTypeInfo.CollapsedNormalImage = Nothing
        Me.gbLeaveTypeInfo.CollapsedPressedImage = Nothing
        Me.gbLeaveTypeInfo.CollapseOnLoad = False
        Me.gbLeaveTypeInfo.Controls.Add(Me.gbLeaveSettings)
        Me.gbLeaveTypeInfo.Controls.Add(Me.EZeeStraightLine1)
        Me.gbLeaveTypeInfo.Controls.Add(Me.cboDeductFrom)
        Me.gbLeaveTypeInfo.Controls.Add(Me.lblDeductFrom)
        Me.gbLeaveTypeInfo.Controls.Add(Me.cboGender)
        Me.gbLeaveTypeInfo.Controls.Add(Me.lblGender)
        Me.gbLeaveTypeInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbLeaveTypeInfo.Controls.Add(Me.objlblColor)
        Me.gbLeaveTypeInfo.Controls.Add(Me.lblDescription)
        Me.gbLeaveTypeInfo.Controls.Add(Me.txtDescription)
        Me.gbLeaveTypeInfo.Controls.Add(Me.objbtnFillColor)
        Me.gbLeaveTypeInfo.Controls.Add(Me.lblColor)
        Me.gbLeaveTypeInfo.Controls.Add(Me.txtName)
        Me.gbLeaveTypeInfo.Controls.Add(Me.lblName)
        Me.gbLeaveTypeInfo.Controls.Add(Me.txtCode)
        Me.gbLeaveTypeInfo.Controls.Add(Me.lblCode)
        Me.gbLeaveTypeInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbLeaveTypeInfo.ExpandedHoverImage = Nothing
        Me.gbLeaveTypeInfo.ExpandedNormalImage = Nothing
        Me.gbLeaveTypeInfo.ExpandedPressedImage = Nothing
        Me.gbLeaveTypeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveTypeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveTypeInfo.HeaderHeight = 25
        Me.gbLeaveTypeInfo.HeaderMessage = ""
        Me.gbLeaveTypeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveTypeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveTypeInfo.HeightOnCollapse = 0
        Me.gbLeaveTypeInfo.LeftTextSpace = 0
        Me.gbLeaveTypeInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbLeaveTypeInfo.Name = "gbLeaveTypeInfo"
        Me.gbLeaveTypeInfo.OpenHeight = 300
        Me.gbLeaveTypeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveTypeInfo.ShowBorder = True
        Me.gbLeaveTypeInfo.ShowCheckBox = False
        Me.gbLeaveTypeInfo.ShowCollapseButton = False
        Me.gbLeaveTypeInfo.ShowDefaultBorderColor = True
        Me.gbLeaveTypeInfo.ShowDownButton = False
        Me.gbLeaveTypeInfo.ShowHeader = True
        Me.gbLeaveTypeInfo.Size = New System.Drawing.Size(669, 408)
        Me.gbLeaveTypeInfo.TabIndex = 6
        Me.gbLeaveTypeInfo.Temp = 0
        Me.gbLeaveTypeInfo.Text = "Leave Type Information"
        Me.gbLeaveTypeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbLeaveSettings
        '
        Me.gbLeaveSettings.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveSettings.Checked = False
        Me.gbLeaveSettings.CollapseAllExceptThis = False
        Me.gbLeaveSettings.CollapsedHoverImage = Nothing
        Me.gbLeaveSettings.CollapsedNormalImage = Nothing
        Me.gbLeaveSettings.CollapsedPressedImage = Nothing
        Me.gbLeaveSettings.CollapseOnLoad = False
        Me.gbLeaveSettings.Controls.Add(Me.lnkLeavePaySettings)
        Me.gbLeaveSettings.Controls.Add(Me.pnlLeaveSettings)
        Me.gbLeaveSettings.ExpandedHoverImage = Nothing
        Me.gbLeaveSettings.ExpandedNormalImage = Nothing
        Me.gbLeaveSettings.ExpandedPressedImage = Nothing
        Me.gbLeaveSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveSettings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveSettings.HeaderHeight = 25
        Me.gbLeaveSettings.HeaderMessage = ""
        Me.gbLeaveSettings.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveSettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveSettings.HeightOnCollapse = 0
        Me.gbLeaveSettings.LeftTextSpace = 0
        Me.gbLeaveSettings.Location = New System.Drawing.Point(11, 170)
        Me.gbLeaveSettings.Name = "gbLeaveSettings"
        Me.gbLeaveSettings.OpenHeight = 300
        Me.gbLeaveSettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveSettings.ShowBorder = True
        Me.gbLeaveSettings.ShowCheckBox = False
        Me.gbLeaveSettings.ShowCollapseButton = False
        Me.gbLeaveSettings.ShowDefaultBorderColor = True
        Me.gbLeaveSettings.ShowDownButton = False
        Me.gbLeaveSettings.ShowHeader = True
        Me.gbLeaveSettings.Size = New System.Drawing.Size(647, 232)
        Me.gbLeaveSettings.TabIndex = 260
        Me.gbLeaveSettings.Temp = 0
        Me.gbLeaveSettings.Text = "Leave Settings"
        Me.gbLeaveSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLeaveSettings
        '
        Me.pnlLeaveSettings.AutoScroll = True
        Me.pnlLeaveSettings.Controls.Add(Me.nudSetFutureDaysLimit)
        Me.pnlLeaveSettings.Controls.Add(Me.chkSetFutureDaysLimit)
        Me.pnlLeaveSettings.Controls.Add(Me.chkExcuseLeave)
        Me.pnlLeaveSettings.Controls.Add(Me.chkDoNotApplyFutureDates)
        Me.pnlLeaveSettings.Controls.Add(Me.chkExempFromPayroll)
        Me.pnlLeaveSettings.Controls.Add(Me.chkShowOnESS)
        Me.pnlLeaveSettings.Controls.Add(Me.chkConsiderExpense)
        Me.pnlLeaveSettings.Controls.Add(Me.chkLinkTnAPeriod)
        Me.pnlLeaveSettings.Controls.Add(Me.chkLvAccrossFYELC)
        Me.pnlLeaveSettings.Controls.Add(Me.FlowLayoutPanel1)
        Me.pnlLeaveSettings.Controls.Add(Me.chkIsBlockLeave)
        Me.pnlLeaveSettings.Controls.Add(Me.grpLeaveDays)
        Me.pnlLeaveSettings.Controls.Add(Me.txtMaxamount)
        Me.pnlLeaveSettings.Controls.Add(Me.lblMaxAmount)
        Me.pnlLeaveSettings.Controls.Add(Me.objStLine1)
        Me.pnlLeaveSettings.Controls.Add(Me.radpaidleave)
        Me.pnlLeaveSettings.Controls.Add(Me.grpIssueWeekend)
        Me.pnlLeaveSettings.Controls.Add(Me.chkConsiderLvHlOnWk)
        Me.pnlLeaveSettings.Controls.Add(Me.lblEligibilityDaysforLeaveExpense)
        Me.pnlLeaveSettings.Controls.Add(Me.radUnpaidleave)
        Me.pnlLeaveSettings.Controls.Add(Me.nudEligibilityExpenseDays)
        Me.pnlLeaveSettings.Controls.Add(Me.chkSkipApprovalflow)
        Me.pnlLeaveSettings.Controls.Add(Me.chkShortLeave)
        Me.pnlLeaveSettings.Controls.Add(Me.chkIsSickLeave)
        Me.pnlLeaveSettings.Controls.Add(Me.grpIssueHoliday)
        Me.pnlLeaveSettings.Location = New System.Drawing.Point(2, 26)
        Me.pnlLeaveSettings.Name = "pnlLeaveSettings"
        Me.pnlLeaveSettings.Size = New System.Drawing.Size(644, 206)
        Me.pnlLeaveSettings.TabIndex = 9
        '
        'nudSetFutureDaysLimit
        '
        Me.nudSetFutureDaysLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudSetFutureDaysLimit.Location = New System.Drawing.Point(183, 215)
        Me.nudSetFutureDaysLimit.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.nudSetFutureDaysLimit.Name = "nudSetFutureDaysLimit"
        Me.nudSetFutureDaysLimit.Size = New System.Drawing.Size(52, 21)
        Me.nudSetFutureDaysLimit.TabIndex = 275
        Me.nudSetFutureDaysLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkSetFutureDaysLimit
        '
        Me.chkSetFutureDaysLimit.BackColor = System.Drawing.Color.Transparent
        Me.chkSetFutureDaysLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSetFutureDaysLimit.Location = New System.Drawing.Point(10, 217)
        Me.chkSetFutureDaysLimit.Name = "chkSetFutureDaysLimit"
        Me.chkSetFutureDaysLimit.Size = New System.Drawing.Size(167, 17)
        Me.chkSetFutureDaysLimit.TabIndex = 274
        Me.chkSetFutureDaysLimit.Text = "Set Max. Future Days Limit"
        Me.chkSetFutureDaysLimit.UseVisualStyleBackColor = False
        '
        'chkExcuseLeave
        '
        Me.chkExcuseLeave.BackColor = System.Drawing.Color.Transparent
        Me.chkExcuseLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExcuseLeave.Location = New System.Drawing.Point(172, 81)
        Me.chkExcuseLeave.Name = "chkExcuseLeave"
        Me.chkExcuseLeave.Size = New System.Drawing.Size(152, 17)
        Me.chkExcuseLeave.TabIndex = 273
        Me.chkExcuseLeave.Text = "Excuse Leave"
        Me.chkExcuseLeave.UseVisualStyleBackColor = False
        '
        'chkDoNotApplyFutureDates
        '
        Me.chkDoNotApplyFutureDates.BackColor = System.Drawing.Color.Transparent
        Me.chkDoNotApplyFutureDates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDoNotApplyFutureDates.Location = New System.Drawing.Point(10, 194)
        Me.chkDoNotApplyFutureDates.Name = "chkDoNotApplyFutureDates"
        Me.chkDoNotApplyFutureDates.Size = New System.Drawing.Size(311, 17)
        Me.chkDoNotApplyFutureDates.TabIndex = 272
        Me.chkDoNotApplyFutureDates.Text = "Do not Allow To Apply For Future Date(s)"
        Me.chkDoNotApplyFutureDates.UseVisualStyleBackColor = False
        '
        'chkExempFromPayroll
        '
        Me.chkExempFromPayroll.BackColor = System.Drawing.Color.Transparent
        Me.chkExempFromPayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExempFromPayroll.Location = New System.Drawing.Point(172, 36)
        Me.chkExempFromPayroll.Name = "chkExempFromPayroll"
        Me.chkExempFromPayroll.Size = New System.Drawing.Size(152, 17)
        Me.chkExempFromPayroll.TabIndex = 271
        Me.chkExempFromPayroll.Text = "Exemp From Payroll"
        Me.chkExempFromPayroll.UseVisualStyleBackColor = False
        '
        'chkShowOnESS
        '
        Me.chkShowOnESS.BackColor = System.Drawing.Color.Transparent
        Me.chkShowOnESS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowOnESS.Location = New System.Drawing.Point(172, 171)
        Me.chkShowOnESS.Name = "chkShowOnESS"
        Me.chkShowOnESS.Size = New System.Drawing.Size(152, 17)
        Me.chkShowOnESS.TabIndex = 270
        Me.chkShowOnESS.Text = "Show on ESS"
        Me.chkShowOnESS.UseVisualStyleBackColor = False
        '
        'chkConsiderExpense
        '
        Me.chkConsiderExpense.BackColor = System.Drawing.Color.Transparent
        Me.chkConsiderExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConsiderExpense.Location = New System.Drawing.Point(10, 171)
        Me.chkConsiderExpense.Name = "chkConsiderExpense"
        Me.chkConsiderExpense.Size = New System.Drawing.Size(151, 17)
        Me.chkConsiderExpense.TabIndex = 269
        Me.chkConsiderExpense.Text = "Consider Expense"
        Me.chkConsiderExpense.UseVisualStyleBackColor = False
        '
        'chkLinkTnAPeriod
        '
        Me.chkLinkTnAPeriod.BackColor = System.Drawing.Color.Transparent
        Me.chkLinkTnAPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLinkTnAPeriod.Location = New System.Drawing.Point(10, 149)
        Me.chkLinkTnAPeriod.Name = "chkLinkTnAPeriod"
        Me.chkLinkTnAPeriod.Size = New System.Drawing.Size(311, 17)
        Me.chkLinkTnAPeriod.TabIndex = 268
        Me.chkLinkTnAPeriod.Text = "Consider leave on TnA Period"
        Me.chkLinkTnAPeriod.UseVisualStyleBackColor = False
        '
        'chkLvAccrossFYELC
        '
        Me.chkLvAccrossFYELC.BackColor = System.Drawing.Color.Transparent
        Me.chkLvAccrossFYELC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLvAccrossFYELC.Location = New System.Drawing.Point(10, 126)
        Me.chkLvAccrossFYELC.Name = "chkLvAccrossFYELC"
        Me.chkLvAccrossFYELC.Size = New System.Drawing.Size(311, 17)
        Me.chkLvAccrossFYELC.TabIndex = 267
        Me.chkLvAccrossFYELC.Text = "Stop apply This Leave Accross FY / ELC"
        Me.chkLvAccrossFYELC.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.grpAccrueAmount)
        Me.FlowLayoutPanel1.Controls.Add(Me.grpAttachDocument)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 241)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(323, 125)
        Me.FlowLayoutPanel1.TabIndex = 264
        '
        'grpAccrueAmount
        '
        Me.grpAccrueAmount.Controls.Add(Me.radAccrueNo)
        Me.grpAccrueAmount.Controls.Add(Me.radAccrueYes)
        Me.grpAccrueAmount.Location = New System.Drawing.Point(3, 3)
        Me.grpAccrueAmount.Name = "grpAccrueAmount"
        Me.grpAccrueAmount.Size = New System.Drawing.Size(312, 43)
        Me.grpAccrueAmount.TabIndex = 12
        Me.grpAccrueAmount.TabStop = False
        Me.grpAccrueAmount.Text = "Accrue Amount"
        '
        'radAccrueNo
        '
        Me.radAccrueNo.AutoSize = True
        Me.radAccrueNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAccrueNo.Location = New System.Drawing.Point(100, 16)
        Me.radAccrueNo.Name = "radAccrueNo"
        Me.radAccrueNo.Size = New System.Drawing.Size(38, 17)
        Me.radAccrueNo.TabIndex = 1
        Me.radAccrueNo.Text = "&No"
        Me.radAccrueNo.UseVisualStyleBackColor = True
        '
        'radAccrueYes
        '
        Me.radAccrueYes.AutoSize = True
        Me.radAccrueYes.Checked = True
        Me.radAccrueYes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAccrueYes.Location = New System.Drawing.Point(38, 16)
        Me.radAccrueYes.Name = "radAccrueYes"
        Me.radAccrueYes.Size = New System.Drawing.Size(42, 17)
        Me.radAccrueYes.TabIndex = 6
        Me.radAccrueYes.TabStop = True
        Me.radAccrueYes.Text = "&Yes"
        Me.radAccrueYes.UseVisualStyleBackColor = True
        '
        'grpAttachDocument
        '
        Me.grpAttachDocument.Controls.Add(Me.chkLvCancelDocMandatory)
        Me.grpAttachDocument.Controls.Add(Me.chkLeaveIssue)
        Me.grpAttachDocument.Controls.Add(Me.chkLeaveForm)
        Me.grpAttachDocument.Location = New System.Drawing.Point(3, 52)
        Me.grpAttachDocument.Name = "grpAttachDocument"
        Me.grpAttachDocument.Size = New System.Drawing.Size(312, 67)
        Me.grpAttachDocument.TabIndex = 13
        Me.grpAttachDocument.TabStop = False
        Me.grpAttachDocument.Text = "Attach Document"
        '
        'chkLvCancelDocMandatory
        '
        Me.chkLvCancelDocMandatory.BackColor = System.Drawing.Color.Transparent
        Me.chkLvCancelDocMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLvCancelDocMandatory.Location = New System.Drawing.Point(13, 43)
        Me.chkLvCancelDocMandatory.Name = "chkLvCancelDocMandatory"
        Me.chkLvCancelDocMandatory.Size = New System.Drawing.Size(289, 17)
        Me.chkLvCancelDocMandatory.TabIndex = 249
        Me.chkLvCancelDocMandatory.Text = "Attach document while cancelling leave form"
        Me.chkLvCancelDocMandatory.UseVisualStyleBackColor = False
        '
        'chkLeaveIssue
        '
        Me.chkLeaveIssue.BackColor = System.Drawing.Color.Transparent
        Me.chkLeaveIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLeaveIssue.Location = New System.Drawing.Point(161, 20)
        Me.chkLeaveIssue.Name = "chkLeaveIssue"
        Me.chkLeaveIssue.Size = New System.Drawing.Size(141, 17)
        Me.chkLeaveIssue.TabIndex = 248
        Me.chkLeaveIssue.Text = "Check on Leave Issue"
        Me.chkLeaveIssue.UseVisualStyleBackColor = False
        '
        'chkLeaveForm
        '
        Me.chkLeaveForm.BackColor = System.Drawing.Color.Transparent
        Me.chkLeaveForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLeaveForm.Location = New System.Drawing.Point(13, 20)
        Me.chkLeaveForm.Name = "chkLeaveForm"
        Me.chkLeaveForm.Size = New System.Drawing.Size(142, 17)
        Me.chkLeaveForm.TabIndex = 247
        Me.chkLeaveForm.Text = "Check on Leave Form"
        Me.chkLeaveForm.UseVisualStyleBackColor = False
        '
        'chkIsBlockLeave
        '
        Me.chkIsBlockLeave.BackColor = System.Drawing.Color.Transparent
        Me.chkIsBlockLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsBlockLeave.Location = New System.Drawing.Point(10, 104)
        Me.chkIsBlockLeave.Name = "chkIsBlockLeave"
        Me.chkIsBlockLeave.Size = New System.Drawing.Size(311, 17)
        Me.chkIsBlockLeave.TabIndex = 266
        Me.chkIsBlockLeave.Text = "Set This Leave As Block Leave"
        Me.chkIsBlockLeave.UseVisualStyleBackColor = False
        '
        'grpLeaveDays
        '
        Me.grpLeaveDays.Controls.Add(Me.nudMaxDays)
        Me.grpLeaveDays.Controls.Add(Me.lblMaxDays)
        Me.grpLeaveDays.Controls.Add(Me.LblMinDays)
        Me.grpLeaveDays.Controls.Add(Me.nudMinDays)
        Me.grpLeaveDays.Location = New System.Drawing.Point(345, 127)
        Me.grpLeaveDays.Name = "grpLeaveDays"
        Me.grpLeaveDays.Size = New System.Drawing.Size(275, 48)
        Me.grpLeaveDays.TabIndex = 13
        Me.grpLeaveDays.TabStop = False
        Me.grpLeaveDays.Text = "Leave Application Leave Days Limit"
        '
        'nudMaxDays
        '
        Me.nudMaxDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxDays.Location = New System.Drawing.Point(221, 19)
        Me.nudMaxDays.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.nudMaxDays.Name = "nudMaxDays"
        Me.nudMaxDays.Size = New System.Drawing.Size(45, 21)
        Me.nudMaxDays.TabIndex = 263
        Me.nudMaxDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxDays
        '
        Me.lblMaxDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxDays.Location = New System.Drawing.Point(147, 21)
        Me.lblMaxDays.Name = "lblMaxDays"
        Me.lblMaxDays.Size = New System.Drawing.Size(68, 16)
        Me.lblMaxDays.TabIndex = 227
        Me.lblMaxDays.Text = "Max. Days"
        Me.lblMaxDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMinDays
        '
        Me.LblMinDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMinDays.Location = New System.Drawing.Point(8, 21)
        Me.LblMinDays.Name = "LblMinDays"
        Me.LblMinDays.Size = New System.Drawing.Size(79, 16)
        Me.LblMinDays.TabIndex = 258
        Me.LblMinDays.Text = "Min. Days"
        Me.LblMinDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudMinDays
        '
        Me.nudMinDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinDays.Location = New System.Drawing.Point(93, 19)
        Me.nudMinDays.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.nudMinDays.Name = "nudMinDays"
        Me.nudMinDays.Size = New System.Drawing.Size(45, 21)
        Me.nudMinDays.TabIndex = 262
        Me.nudMinDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMaxamount
        '
        Me.txtMaxamount.AllowNegative = False
        Me.txtMaxamount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxamount.DigitsInGroup = 0
        Me.txtMaxamount.Flags = 65536
        Me.txtMaxamount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxamount.Location = New System.Drawing.Point(438, 206)
        Me.txtMaxamount.MaxDecimalPlaces = 4
        Me.txtMaxamount.MaxWholeDigits = 3
        Me.txtMaxamount.Name = "txtMaxamount"
        Me.txtMaxamount.Prefix = ""
        Me.txtMaxamount.RangeMax = 1.7976931348623157E+308
        Me.txtMaxamount.RangeMin = -1.7976931348623157E+308
        Me.txtMaxamount.Size = New System.Drawing.Size(45, 21)
        Me.txtMaxamount.TabIndex = 265
        Me.txtMaxamount.Text = "0"
        Me.txtMaxamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxAmount
        '
        Me.lblMaxAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxAmount.Location = New System.Drawing.Point(353, 208)
        Me.lblMaxAmount.Name = "lblMaxAmount"
        Me.lblMaxAmount.Size = New System.Drawing.Size(79, 16)
        Me.lblMaxAmount.TabIndex = 264
        Me.lblMaxAmount.Text = "Max.Amount"
        Me.lblMaxAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(331, 4)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(5, 323)
        Me.objStLine1.TabIndex = 261
        Me.objStLine1.Text = "EZeeStraightLine2"
        '
        'radpaidleave
        '
        Me.radpaidleave.Checked = True
        Me.radpaidleave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radpaidleave.Location = New System.Drawing.Point(10, 9)
        Me.radpaidleave.Name = "radpaidleave"
        Me.radpaidleave.Size = New System.Drawing.Size(84, 17)
        Me.radpaidleave.TabIndex = 8
        Me.radpaidleave.TabStop = True
        Me.radpaidleave.Text = "Paid Leave"
        Me.radpaidleave.UseVisualStyleBackColor = True
        '
        'grpIssueWeekend
        '
        Me.grpIssueWeekend.Controls.Add(Me.radIssueWKNo)
        Me.grpIssueWeekend.Controls.Add(Me.radIssueWkYes)
        Me.grpIssueWeekend.Location = New System.Drawing.Point(345, 82)
        Me.grpIssueWeekend.Name = "grpIssueWeekend"
        Me.grpIssueWeekend.Size = New System.Drawing.Size(275, 43)
        Me.grpIssueWeekend.TabIndex = 12
        Me.grpIssueWeekend.TabStop = False
        Me.grpIssueWeekend.Text = "Issue on Weekend"
        '
        'radIssueWKNo
        '
        Me.radIssueWKNo.AutoSize = True
        Me.radIssueWKNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssueWKNo.Location = New System.Drawing.Point(100, 16)
        Me.radIssueWKNo.Name = "radIssueWKNo"
        Me.radIssueWKNo.Size = New System.Drawing.Size(38, 17)
        Me.radIssueWKNo.TabIndex = 1
        Me.radIssueWKNo.Text = "&No"
        Me.radIssueWKNo.UseVisualStyleBackColor = True
        '
        'radIssueWkYes
        '
        Me.radIssueWkYes.AutoSize = True
        Me.radIssueWkYes.Checked = True
        Me.radIssueWkYes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssueWkYes.Location = New System.Drawing.Point(38, 16)
        Me.radIssueWkYes.Name = "radIssueWkYes"
        Me.radIssueWkYes.Size = New System.Drawing.Size(42, 17)
        Me.radIssueWkYes.TabIndex = 6
        Me.radIssueWkYes.TabStop = True
        Me.radIssueWkYes.Text = "&Yes"
        Me.radIssueWkYes.UseVisualStyleBackColor = True
        '
        'chkConsiderLvHlOnWk
        '
        Me.chkConsiderLvHlOnWk.BackColor = System.Drawing.Color.Transparent
        Me.chkConsiderLvHlOnWk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConsiderLvHlOnWk.Location = New System.Drawing.Point(345, 9)
        Me.chkConsiderLvHlOnWk.Name = "chkConsiderLvHlOnWk"
        Me.chkConsiderLvHlOnWk.Size = New System.Drawing.Size(281, 18)
        Me.chkConsiderLvHlOnWk.TabIndex = 249
        Me.chkConsiderLvHlOnWk.Text = "Do Not Consider Leave IF Holiday Falls On Weekend"
        Me.chkConsiderLvHlOnWk.UseVisualStyleBackColor = False
        '
        'lblEligibilityDaysforLeaveExpense
        '
        Me.lblEligibilityDaysforLeaveExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEligibilityDaysforLeaveExpense.Location = New System.Drawing.Point(353, 181)
        Me.lblEligibilityDaysforLeaveExpense.Name = "lblEligibilityDaysforLeaveExpense"
        Me.lblEligibilityDaysforLeaveExpense.Size = New System.Drawing.Size(206, 16)
        Me.lblEligibilityDaysforLeaveExpense.TabIndex = 254
        Me.lblEligibilityDaysforLeaveExpense.Text = "Eligibility for Leave Expense After(Days)"
        '
        'radUnpaidleave
        '
        Me.radUnpaidleave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUnpaidleave.Location = New System.Drawing.Point(100, 9)
        Me.radUnpaidleave.Name = "radUnpaidleave"
        Me.radUnpaidleave.Size = New System.Drawing.Size(94, 17)
        Me.radUnpaidleave.TabIndex = 9
        Me.radUnpaidleave.Text = "Unpaid Leave"
        Me.radUnpaidleave.UseVisualStyleBackColor = True
        '
        'nudEligibilityExpenseDays
        '
        Me.nudEligibilityExpenseDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudEligibilityExpenseDays.Location = New System.Drawing.Point(566, 179)
        Me.nudEligibilityExpenseDays.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.nudEligibilityExpenseDays.Name = "nudEligibilityExpenseDays"
        Me.nudEligibilityExpenseDays.Size = New System.Drawing.Size(45, 21)
        Me.nudEligibilityExpenseDays.TabIndex = 252
        Me.nudEligibilityExpenseDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkSkipApprovalflow
        '
        Me.chkSkipApprovalflow.BackColor = System.Drawing.Color.Transparent
        Me.chkSkipApprovalflow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSkipApprovalflow.Location = New System.Drawing.Point(10, 81)
        Me.chkSkipApprovalflow.Name = "chkSkipApprovalflow"
        Me.chkSkipApprovalflow.Size = New System.Drawing.Size(152, 17)
        Me.chkSkipApprovalflow.TabIndex = 256
        Me.chkSkipApprovalflow.Text = "Skip Approval Flow"
        Me.chkSkipApprovalflow.UseVisualStyleBackColor = False
        '
        'chkShortLeave
        '
        Me.chkShortLeave.BackColor = System.Drawing.Color.Transparent
        Me.chkShortLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShortLeave.Location = New System.Drawing.Point(10, 36)
        Me.chkShortLeave.Name = "chkShortLeave"
        Me.chkShortLeave.Size = New System.Drawing.Size(158, 17)
        Me.chkShortLeave.TabIndex = 10
        Me.chkShortLeave.Text = "Short Leave"
        Me.chkShortLeave.UseVisualStyleBackColor = False
        '
        'chkIsSickLeave
        '
        Me.chkIsSickLeave.BackColor = System.Drawing.Color.Transparent
        Me.chkIsSickLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsSickLeave.Location = New System.Drawing.Point(10, 59)
        Me.chkIsSickLeave.Name = "chkIsSickLeave"
        Me.chkIsSickLeave.Size = New System.Drawing.Size(311, 17)
        Me.chkIsSickLeave.TabIndex = 245
        Me.chkIsSickLeave.Text = "Set Leave End Date as Optional"
        Me.chkIsSickLeave.UseVisualStyleBackColor = False
        '
        'grpIssueHoliday
        '
        Me.grpIssueHoliday.Controls.Add(Me.radIssueNo)
        Me.grpIssueHoliday.Controls.Add(Me.radIssueYes)
        Me.grpIssueHoliday.Location = New System.Drawing.Point(345, 33)
        Me.grpIssueHoliday.Name = "grpIssueHoliday"
        Me.grpIssueHoliday.Size = New System.Drawing.Size(275, 43)
        Me.grpIssueHoliday.TabIndex = 11
        Me.grpIssueHoliday.TabStop = False
        Me.grpIssueHoliday.Text = "Issue on Holiday"
        '
        'radIssueNo
        '
        Me.radIssueNo.AutoSize = True
        Me.radIssueNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssueNo.Location = New System.Drawing.Point(100, 16)
        Me.radIssueNo.Name = "radIssueNo"
        Me.radIssueNo.Size = New System.Drawing.Size(38, 17)
        Me.radIssueNo.TabIndex = 1
        Me.radIssueNo.Text = "&No"
        Me.radIssueNo.UseVisualStyleBackColor = True
        '
        'radIssueYes
        '
        Me.radIssueYes.AutoSize = True
        Me.radIssueYes.Checked = True
        Me.radIssueYes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssueYes.Location = New System.Drawing.Point(38, 16)
        Me.radIssueYes.Name = "radIssueYes"
        Me.radIssueYes.Size = New System.Drawing.Size(42, 17)
        Me.radIssueYes.TabIndex = 6
        Me.radIssueYes.TabStop = True
        Me.radIssueYes.Text = "&Yes"
        Me.radIssueYes.UseVisualStyleBackColor = True
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(344, 24)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(5, 146)
        Me.EZeeStraightLine1.TabIndex = 262
        Me.EZeeStraightLine1.Text = "EZeeStraightLine2"
        '
        'cboDeductFrom
        '
        Me.cboDeductFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductFrom.FormattingEnabled = True
        Me.cboDeductFrom.Location = New System.Drawing.Point(82, 142)
        Me.cboDeductFrom.Name = "cboDeductFrom"
        Me.cboDeductFrom.Size = New System.Drawing.Size(226, 21)
        Me.cboDeductFrom.TabIndex = 6
        '
        'lblDeductFrom
        '
        Me.lblDeductFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductFrom.Location = New System.Drawing.Point(8, 144)
        Me.lblDeductFrom.Name = "lblDeductFrom"
        Me.lblDeductFrom.Size = New System.Drawing.Size(68, 16)
        Me.lblDeductFrom.TabIndex = 234
        Me.lblDeductFrom.Text = "Deduct From"
        Me.lblDeductFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(82, 114)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(226, 21)
        Me.cboGender.TabIndex = 5
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(8, 117)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(68, 16)
        Me.lblGender.TabIndex = 232
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(314, 61)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 3
        '
        'objlblColor
        '
        Me.objlblColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objlblColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColor.Location = New System.Drawing.Point(82, 87)
        Me.objlblColor.Name = "objlblColor"
        Me.objlblColor.Size = New System.Drawing.Size(226, 21)
        Me.objlblColor.TabIndex = 221
        Me.objlblColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(359, 35)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(285, 16)
        Me.lblDescription.TabIndex = 95
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(359, 60)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(299, 103)
        Me.txtDescription.TabIndex = 7
        '
        'objbtnFillColor
        '
        Me.objbtnFillColor.BackColor = System.Drawing.Color.Transparent
        Me.objbtnFillColor.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnFillColor.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnFillColor.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnFillColor.BorderSelected = False
        Me.objbtnFillColor.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnFillColor.Image = Global.Aruti.Main.My.Resources.Resources.Misc_16
        Me.objbtnFillColor.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnFillColor.Location = New System.Drawing.Point(314, 87)
        Me.objbtnFillColor.Name = "objbtnFillColor"
        Me.objbtnFillColor.Size = New System.Drawing.Size(21, 21)
        Me.objbtnFillColor.TabIndex = 4
        '
        'lblColor
        '
        Me.lblColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColor.Location = New System.Drawing.Point(8, 89)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(68, 16)
        Me.lblColor.TabIndex = 5
        Me.lblColor.Text = "Color"
        Me.lblColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(82, 60)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(226, 21)
        Me.txtName.TabIndex = 2
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 62)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(68, 16)
        Me.lblName.TabIndex = 3
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(82, 33)
        Me.txtCode.MaxLength = 5
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(119, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 35)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(68, 16)
        Me.lblCode.TabIndex = 1
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.Panel1)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 408)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(669, 59)
        Me.objFooter.TabIndex = 11
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkNoAction)
        Me.Panel1.Controls.Add(Me.LblDays)
        Me.Panel1.Controls.Add(Me.LblCFAmount)
        Me.Panel1.Controls.Add(Me.txtCfAmount)
        Me.Panel1.Controls.Add(Me.LblEligibilityAfter)
        Me.Panel1.Location = New System.Drawing.Point(13, 13)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(41, 25)
        Me.Panel1.TabIndex = 245
        '
        'chkNoAction
        '
        Me.chkNoAction.BackColor = System.Drawing.Color.Transparent
        Me.chkNoAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNoAction.Location = New System.Drawing.Point(14, 112)
        Me.chkNoAction.Name = "chkNoAction"
        Me.chkNoAction.Size = New System.Drawing.Size(215, 17)
        Me.chkNoAction.TabIndex = 243
        Me.chkNoAction.Text = "No Action For Leave C.F Amount"
        Me.chkNoAction.UseVisualStyleBackColor = False
        '
        'LblDays
        '
        Me.LblDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDays.Location = New System.Drawing.Point(174, 59)
        Me.LblDays.Name = "LblDays"
        Me.LblDays.Size = New System.Drawing.Size(58, 17)
        Me.LblDays.TabIndex = 241
        Me.LblDays.Text = "(In Days)"
        Me.LblDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCFAmount
        '
        Me.LblCFAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCFAmount.Location = New System.Drawing.Point(11, 85)
        Me.LblCFAmount.Name = "LblCFAmount"
        Me.LblCFAmount.Size = New System.Drawing.Size(80, 16)
        Me.LblCFAmount.TabIndex = 237
        Me.LblCFAmount.Text = "C.F Amount"
        Me.LblCFAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCfAmount
        '
        Me.txtCfAmount.AllowNegative = False
        Me.txtCfAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCfAmount.DigitsInGroup = 0
        Me.txtCfAmount.Flags = 65536
        Me.txtCfAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCfAmount.Location = New System.Drawing.Point(97, 83)
        Me.txtCfAmount.MaxDecimalPlaces = 2
        Me.txtCfAmount.MaxWholeDigits = 4
        Me.txtCfAmount.Name = "txtCfAmount"
        Me.txtCfAmount.Prefix = ""
        Me.txtCfAmount.RangeMax = 1.7976931348623157E+308
        Me.txtCfAmount.RangeMin = -1.7976931348623157E+308
        Me.txtCfAmount.Size = New System.Drawing.Size(71, 21)
        Me.txtCfAmount.TabIndex = 14
        Me.txtCfAmount.Text = "0"
        Me.txtCfAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblEligibilityAfter
        '
        Me.LblEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEligibilityAfter.Location = New System.Drawing.Point(11, 59)
        Me.LblEligibilityAfter.Name = "LblEligibilityAfter"
        Me.LblEligibilityAfter.Size = New System.Drawing.Size(80, 16)
        Me.LblEligibilityAfter.TabIndex = 239
        Me.LblEligibilityAfter.Text = "Eligibility After"
        Me.LblEligibilityAfter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(457, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(560, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'chkLeaveEncashment
        '
        Me.chkLeaveEncashment.BackColor = System.Drawing.Color.Transparent
        Me.chkLeaveEncashment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLeaveEncashment.Location = New System.Drawing.Point(110, 436)
        Me.chkLeaveEncashment.Name = "chkLeaveEncashment"
        Me.chkLeaveEncashment.Size = New System.Drawing.Size(282, 21)
        Me.chkLeaveEncashment.TabIndex = 247
        Me.chkLeaveEncashment.Text = "Leave Encashement"
        Me.chkLeaveEncashment.UseVisualStyleBackColor = False
        Me.chkLeaveEncashment.Visible = False
        '
        'lnkLeavePaySettings
        '
        Me.lnkLeavePaySettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkLeavePaySettings.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkLeavePaySettings.Location = New System.Drawing.Point(493, 3)
        Me.lnkLeavePaySettings.Name = "lnkLeavePaySettings"
        Me.lnkLeavePaySettings.Size = New System.Drawing.Size(150, 18)
        Me.lnkLeavePaySettings.TabIndex = 276
        Me.lnkLeavePaySettings.TabStop = True
        Me.lnkLeavePaySettings.Text = "Leave Pay Setting"
        Me.lnkLeavePaySettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkLeavePaySettings.Visible = False
        '
        'frmLeaveType_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(669, 467)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveType_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit LeaveType"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbLeaveTypeInfo.ResumeLayout(False)
        Me.gbLeaveTypeInfo.PerformLayout()
        Me.gbLeaveSettings.ResumeLayout(False)
        Me.pnlLeaveSettings.ResumeLayout(False)
        Me.pnlLeaveSettings.PerformLayout()
        CType(Me.nudSetFutureDaysLimit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.grpAccrueAmount.ResumeLayout(False)
        Me.grpAccrueAmount.PerformLayout()
        Me.grpAttachDocument.ResumeLayout(False)
        Me.grpLeaveDays.ResumeLayout(False)
        CType(Me.nudMaxDays, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMinDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpIssueWeekend.ResumeLayout(False)
        Me.grpIssueWeekend.PerformLayout()
        CType(Me.nudEligibilityExpenseDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpIssueHoliday.ResumeLayout(False)
        Me.grpIssueHoliday.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbLeaveTypeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents objbtnFillColor As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents radUnpaidleave As System.Windows.Forms.RadioButton
    Friend WithEvents radpaidleave As System.Windows.Forms.RadioButton
    Friend WithEvents objlblColor As System.Windows.Forms.Label
    Friend WithEvents grpIssueHoliday As System.Windows.Forms.GroupBox
    Friend WithEvents radIssueYes As System.Windows.Forms.RadioButton
    Friend WithEvents radIssueNo As System.Windows.Forms.RadioButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents lblMaxDays As System.Windows.Forms.Label
    Friend WithEvents chkShortLeave As System.Windows.Forms.CheckBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents cboDeductFrom As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeductFrom As System.Windows.Forms.Label
    Friend WithEvents LblCFAmount As System.Windows.Forms.Label
    Friend WithEvents txtCfAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents LblDays As System.Windows.Forms.Label
    Friend WithEvents LblEligibilityAfter As System.Windows.Forms.Label
    Friend WithEvents chkNoAction As System.Windows.Forms.CheckBox
    Friend WithEvents grpAccrueAmount As System.Windows.Forms.GroupBox
    Friend WithEvents radAccrueNo As System.Windows.Forms.RadioButton
    Friend WithEvents radAccrueYes As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkIsSickLeave As System.Windows.Forms.CheckBox
    Friend WithEvents grpAttachDocument As System.Windows.Forms.GroupBox
    Friend WithEvents chkLeaveForm As System.Windows.Forms.CheckBox
    Friend WithEvents chkLeaveIssue As System.Windows.Forms.CheckBox
    Friend WithEvents chkLeaveEncashment As System.Windows.Forms.CheckBox
    Friend WithEvents grpIssueWeekend As System.Windows.Forms.GroupBox
    Friend WithEvents radIssueWKNo As System.Windows.Forms.RadioButton
    Friend WithEvents radIssueWkYes As System.Windows.Forms.RadioButton
    Friend WithEvents chkConsiderLvHlOnWk As System.Windows.Forms.CheckBox
    Friend WithEvents nudEligibilityExpenseDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblEligibilityDaysforLeaveExpense As System.Windows.Forms.Label
    Friend WithEvents chkLvCancelDocMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents chkSkipApprovalflow As System.Windows.Forms.CheckBox
    Friend WithEvents gbLeaveSettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlLeaveSettings As System.Windows.Forms.Panel
    Friend WithEvents LblMinDays As System.Windows.Forms.Label
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents nudMaxDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudMinDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMaxAmount As System.Windows.Forms.Label
    Friend WithEvents txtMaxamount As eZee.TextBox.NumericTextBox
    Friend WithEvents grpLeaveDays As System.Windows.Forms.GroupBox
    Friend WithEvents chkIsBlockLeave As System.Windows.Forms.CheckBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkLvAccrossFYELC As System.Windows.Forms.CheckBox
    Friend WithEvents chkLinkTnAPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents chkConsiderExpense As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowOnESS As System.Windows.Forms.CheckBox
    Friend WithEvents chkExempFromPayroll As System.Windows.Forms.CheckBox
    Friend WithEvents chkDoNotApplyFutureDates As System.Windows.Forms.CheckBox
    Friend WithEvents chkExcuseLeave As System.Windows.Forms.CheckBox
    Friend WithEvents nudSetFutureDaysLimit As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkSetFutureDaysLimit As System.Windows.Forms.CheckBox
    Friend WithEvents lnkLeavePaySettings As System.Windows.Forms.LinkLabel
End Class
