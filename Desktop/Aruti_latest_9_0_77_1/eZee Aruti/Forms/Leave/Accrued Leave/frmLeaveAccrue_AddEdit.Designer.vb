﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveAccrue_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveAccrue_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lblAs = New eZee.Common.eZeeLine
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.objlblBalancevalue = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.objlblLeaveBFvalue = New System.Windows.Forms.Label
        Me.LblLeaveBF = New System.Windows.Forms.Label
        Me.objlblTodateIssuedvalue = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblTodateIssued = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.lblToDateAccrued = New System.Windows.Forms.Label
        Me.objlblAccruedToDateValue = New System.Windows.Forms.Label
        Me.LblTotalAdjustment = New System.Windows.Forms.Label
        Me.objlblTotalAdjustment = New System.Windows.Forms.Label
        Me.gbIssueList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkIssueLeave = New System.Windows.Forms.LinkLabel
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.pnlIssueLeave = New System.Windows.Forms.Panel
        Me.lstIssueLeave = New eZee.Common.eZeeListView(Me.components)
        Me.colhIssueStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhIssueLeave = New System.Windows.Forms.ColumnHeader
        Me.colhIssueFormNo = New System.Windows.Forms.ColumnHeader
        Me.colhIssueTotLeave = New System.Windows.Forms.ColumnHeader
        Me.objcolhLeaveIssueunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhLeaveIssueTranunkid = New System.Windows.Forms.ColumnHeader
        Me.cboForm = New System.Windows.Forms.ComboBox
        Me.txtIssuemount = New eZee.TextBox.NumericTextBox
        Me.cboIssueLeaveCode = New System.Windows.Forms.ComboBox
        Me.lblIssueLeave = New System.Windows.Forms.Label
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.lblIssueLeaveCode = New System.Windows.Forms.Label
        Me.objbtnDeleteByBatch = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDeleteByLeave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAccrueList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.EZeeLine2 = New eZee.Common.eZeeLine
        Me.chkNoAction = New System.Windows.Forms.CheckBox
        Me.txtCfAmount = New eZee.TextBox.NumericTextBox
        Me.LblCFAmount = New System.Windows.Forms.Label
        Me.txtEligibilityAfter = New eZee.TextBox.NumericTextBox
        Me.txtAccrualAmount = New eZee.TextBox.NumericTextBox
        Me.objbtnAddLeaveType = New eZee.Common.eZeeGradientButton
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.LblEligibilityAfter = New System.Windows.Forms.Label
        Me.btnGlobalAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlAccrueLeave = New System.Windows.Forms.Panel
        Me.lvAccrue = New eZee.Common.eZeeListView(Me.components)
        Me.colhLeave = New System.Windows.Forms.ColumnHeader
        Me.colhStartdate = New System.Windows.Forms.ColumnHeader
        Me.colhenddate = New System.Windows.Forms.ColumnHeader
        Me.colhActualAmount = New System.Windows.Forms.ColumnHeader
        Me.colhMonthlyAcrrue = New System.Windows.Forms.ColumnHeader
        Me.colhTotLeave = New System.Windows.Forms.ColumnHeader
        Me.objcolhAccrueSetting = New System.Windows.Forms.ColumnHeader
        Me.objcolhDailyAmount = New System.Windows.Forms.ColumnHeader
        Me.objcolhLeavetypeunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhCFAmt = New System.Windows.Forms.ColumnHeader
        Me.objcolhEDays = New System.Windows.Forms.ColumnHeader
        Me.objcolhNoAction = New System.Windows.Forms.ColumnHeader
        Me.objcolhMaxNegativeLimit = New System.Windows.Forms.ColumnHeader
        Me.dtpStopDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.cboLeaveCode = New System.Windows.Forms.ComboBox
        Me.lblAccrualAmount = New System.Windows.Forms.Label
        Me.lblStopDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.lblLeaveCode = New System.Windows.Forms.Label
        Me.LblMonthlyAccrue = New System.Windows.Forms.Label
        Me.dtpAsOnDate = New System.Windows.Forms.DateTimePicker
        Me.gbLeaveBalance = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radIssueOnExceedBalAsonDate = New System.Windows.Forms.RadioButton
        Me.nudMaxNegativeDays = New System.Windows.Forms.NumericUpDown
        Me.lblMaxNegativeLimit = New System.Windows.Forms.Label
        Me.radDonotIssueAsonDate = New System.Windows.Forms.RadioButton
        Me.radExceedBal = New System.Windows.Forms.RadioButton
        Me.radIssuebal = New System.Windows.Forms.RadioButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboLeaveYear = New System.Windows.Forms.ComboBox
        Me.lblLeaveYear = New System.Windows.Forms.Label
        Me.radDonotIssue = New System.Windows.Forms.RadioButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblLastYearIssued = New System.Windows.Forms.Label
        Me.objlblLastYearIssuedValue = New System.Windows.Forms.Label
        Me.objlblLastYearAccruedValue = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblLastYearAccrued = New System.Windows.Forms.Label
        Me.objbtnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.objTpToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gbIssueList.SuspendLayout()
        Me.pnlIssueLeave.SuspendLayout()
        Me.gbAccrueList.SuspendLayout()
        Me.pnlAccrueLeave.SuspendLayout()
        Me.gbLeaveBalance.SuspendLayout()
        CType(Me.nudMaxNegativeDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lblAs)
        Me.pnlMainInfo.Controls.Add(Me.objelLine1)
        Me.pnlMainInfo.Controls.Add(Me.TableLayoutPanel1)
        Me.pnlMainInfo.Controls.Add(Me.gbIssueList)
        Me.pnlMainInfo.Controls.Add(Me.gbAccrueList)
        Me.pnlMainInfo.Controls.Add(Me.dtpAsOnDate)
        Me.pnlMainInfo.Controls.Add(Me.gbLeaveBalance)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(895, 597)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lblAs
        '
        Me.lblAs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAs.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lblAs.Location = New System.Drawing.Point(357, 10)
        Me.lblAs.Name = "lblAs"
        Me.lblAs.Size = New System.Drawing.Size(395, 17)
        Me.lblAs.TabIndex = 269
        Me.lblAs.Text = "As On Date"
        Me.lblAs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(15, 210)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(869, 4)
        Me.objelLine1.TabIndex = 268
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.60268!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.39732!))
        Me.TableLayoutPanel1.Controls.Add(Me.objlblBalancevalue, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.lblBalance, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.objlblLeaveBFvalue, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.LblLeaveBF, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.objlblTodateIssuedvalue, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblTodateIssued, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblToDateAccrued, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.objlblAccruedToDateValue, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LblTotalAdjustment, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.objlblTotalAdjustment, 1, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(357, 33)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(529, 174)
        Me.TableLayoutPanel1.TabIndex = 266
        '
        'objlblBalancevalue
        '
        Me.objlblBalancevalue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblBalancevalue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblBalancevalue.Location = New System.Drawing.Point(355, 140)
        Me.objlblBalancevalue.Name = "objlblBalancevalue"
        Me.objlblBalancevalue.Size = New System.Drawing.Size(170, 33)
        Me.objlblBalancevalue.TabIndex = 11
        Me.objlblBalancevalue.Text = "0.00"
        Me.objlblBalancevalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBalance
        '
        Me.lblBalance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(4, 140)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(344, 33)
        Me.lblBalance.TabIndex = 10
        Me.lblBalance.Text = "Total Remaining Balance"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblLeaveBFvalue
        '
        Me.objlblLeaveBFvalue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblLeaveBFvalue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblLeaveBFvalue.Location = New System.Drawing.Point(355, 32)
        Me.objlblLeaveBFvalue.Name = "objlblLeaveBFvalue"
        Me.objlblLeaveBFvalue.Size = New System.Drawing.Size(170, 26)
        Me.objlblLeaveBFvalue.TabIndex = 14
        Me.objlblLeaveBFvalue.Text = "0.00"
        Me.objlblLeaveBFvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblLeaveBF
        '
        Me.LblLeaveBF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblLeaveBF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeaveBF.Location = New System.Drawing.Point(4, 32)
        Me.LblLeaveBF.Name = "LblLeaveBF"
        Me.LblLeaveBF.Size = New System.Drawing.Size(344, 26)
        Me.LblLeaveBF.TabIndex = 13
        Me.LblLeaveBF.Text = "Leave BF"
        Me.LblLeaveBF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblTodateIssuedvalue
        '
        Me.objlblTodateIssuedvalue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblTodateIssuedvalue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTodateIssuedvalue.Location = New System.Drawing.Point(355, 86)
        Me.objlblTodateIssuedvalue.Name = "objlblTodateIssuedvalue"
        Me.objlblTodateIssuedvalue.Size = New System.Drawing.Size(170, 26)
        Me.objlblTodateIssuedvalue.TabIndex = 9
        Me.objlblTodateIssuedvalue.Text = "0.00"
        Me.objlblTodateIssuedvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(355, 1)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(170, 30)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Amount"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTodateIssued
        '
        Me.lblTodateIssued.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTodateIssued.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodateIssued.Location = New System.Drawing.Point(4, 86)
        Me.lblTodateIssued.Name = "lblTodateIssued"
        Me.lblTodateIssued.Size = New System.Drawing.Size(344, 26)
        Me.lblTodateIssued.TabIndex = 8
        Me.lblTodateIssued.Text = "Total Issued To Date"
        Me.lblTodateIssued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(4, 1)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(344, 30)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Description"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToDateAccrued
        '
        Me.lblToDateAccrued.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblToDateAccrued.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDateAccrued.Location = New System.Drawing.Point(4, 59)
        Me.lblToDateAccrued.Name = "lblToDateAccrued"
        Me.lblToDateAccrued.Size = New System.Drawing.Size(344, 26)
        Me.lblToDateAccrued.TabIndex = 4
        Me.lblToDateAccrued.Text = "Total Accrued To Date"
        Me.lblToDateAccrued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblAccruedToDateValue
        '
        Me.objlblAccruedToDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblAccruedToDateValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAccruedToDateValue.Location = New System.Drawing.Point(355, 59)
        Me.objlblAccruedToDateValue.Name = "objlblAccruedToDateValue"
        Me.objlblAccruedToDateValue.Size = New System.Drawing.Size(170, 26)
        Me.objlblAccruedToDateValue.TabIndex = 5
        Me.objlblAccruedToDateValue.Text = "0.00"
        Me.objlblAccruedToDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblTotalAdjustment
        '
        Me.LblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblTotalAdjustment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotalAdjustment.Location = New System.Drawing.Point(4, 113)
        Me.LblTotalAdjustment.Name = "LblTotalAdjustment"
        Me.LblTotalAdjustment.Size = New System.Drawing.Size(344, 26)
        Me.LblTotalAdjustment.TabIndex = 15
        Me.LblTotalAdjustment.Text = "Total Adjustment"
        Me.LblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblTotalAdjustment
        '
        Me.objlblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblTotalAdjustment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalAdjustment.Location = New System.Drawing.Point(355, 113)
        Me.objlblTotalAdjustment.Name = "objlblTotalAdjustment"
        Me.objlblTotalAdjustment.Size = New System.Drawing.Size(170, 26)
        Me.objlblTotalAdjustment.TabIndex = 16
        Me.objlblTotalAdjustment.Text = "0.00"
        Me.objlblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gbIssueList
        '
        Me.gbIssueList.BorderColor = System.Drawing.Color.Black
        Me.gbIssueList.Checked = False
        Me.gbIssueList.CollapseAllExceptThis = False
        Me.gbIssueList.CollapsedHoverImage = Nothing
        Me.gbIssueList.CollapsedNormalImage = Nothing
        Me.gbIssueList.CollapsedPressedImage = Nothing
        Me.gbIssueList.CollapseOnLoad = False
        Me.gbIssueList.Controls.Add(Me.lnkIssueLeave)
        Me.gbIssueList.Controls.Add(Me.EZeeLine1)
        Me.gbIssueList.Controls.Add(Me.pnlIssueLeave)
        Me.gbIssueList.Controls.Add(Me.cboForm)
        Me.gbIssueList.Controls.Add(Me.txtIssuemount)
        Me.gbIssueList.Controls.Add(Me.cboIssueLeaveCode)
        Me.gbIssueList.Controls.Add(Me.lblIssueLeave)
        Me.gbIssueList.Controls.Add(Me.lblFormNo)
        Me.gbIssueList.Controls.Add(Me.lblIssueLeaveCode)
        Me.gbIssueList.Controls.Add(Me.objbtnDeleteByBatch)
        Me.gbIssueList.Controls.Add(Me.objbtnDeleteByLeave)
        Me.gbIssueList.ExpandedHoverImage = Nothing
        Me.gbIssueList.ExpandedNormalImage = Nothing
        Me.gbIssueList.ExpandedPressedImage = Nothing
        Me.gbIssueList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbIssueList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbIssueList.HeaderHeight = 25
        Me.gbIssueList.HeaderMessage = ""
        Me.gbIssueList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbIssueList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbIssueList.HeightOnCollapse = 0
        Me.gbIssueList.LeftTextSpace = 0
        Me.gbIssueList.Location = New System.Drawing.Point(467, 219)
        Me.gbIssueList.Name = "gbIssueList"
        Me.gbIssueList.OpenHeight = 300
        Me.gbIssueList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbIssueList.ShowBorder = True
        Me.gbIssueList.ShowCheckBox = False
        Me.gbIssueList.ShowCollapseButton = False
        Me.gbIssueList.ShowDefaultBorderColor = True
        Me.gbIssueList.ShowDownButton = False
        Me.gbIssueList.ShowHeader = True
        Me.gbIssueList.Size = New System.Drawing.Size(419, 315)
        Me.gbIssueList.TabIndex = 265
        Me.gbIssueList.Temp = 0
        Me.gbIssueList.Text = "Issued Leave"
        Me.gbIssueList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkIssueLeave
        '
        Me.lnkIssueLeave.BackColor = System.Drawing.Color.Transparent
        Me.lnkIssueLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkIssueLeave.Location = New System.Drawing.Point(291, 3)
        Me.lnkIssueLeave.Name = "lnkIssueLeave"
        Me.lnkIssueLeave.Size = New System.Drawing.Size(123, 18)
        Me.lnkIssueLeave.TabIndex = 278
        Me.lnkIssueLeave.TabStop = True
        Me.lnkIssueLeave.Text = "Issue Leave"
        Me.lnkIssueLeave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkIssueLeave.Visible = False
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(3, 84)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(408, 7)
        Me.EZeeLine1.TabIndex = 277
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlIssueLeave
        '
        Me.pnlIssueLeave.Controls.Add(Me.lstIssueLeave)
        Me.pnlIssueLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlIssueLeave.Location = New System.Drawing.Point(3, 96)
        Me.pnlIssueLeave.Name = "pnlIssueLeave"
        Me.pnlIssueLeave.Size = New System.Drawing.Size(410, 214)
        Me.pnlIssueLeave.TabIndex = 275
        '
        'lstIssueLeave
        '
        Me.lstIssueLeave.BackColorOnChecked = True
        Me.lstIssueLeave.ColumnHeaders = Nothing
        Me.lstIssueLeave.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhIssueStartDate, Me.colhIssueLeave, Me.colhIssueFormNo, Me.colhIssueTotLeave, Me.objcolhLeaveIssueunkid, Me.objcolhLeaveIssueTranunkid})
        Me.lstIssueLeave.CompulsoryColumns = ""
        Me.lstIssueLeave.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstIssueLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstIssueLeave.FullRowSelect = True
        Me.lstIssueLeave.GridLines = True
        Me.lstIssueLeave.GroupingColumn = Nothing
        Me.lstIssueLeave.HideSelection = False
        Me.lstIssueLeave.Location = New System.Drawing.Point(0, 0)
        Me.lstIssueLeave.MinColumnWidth = 50
        Me.lstIssueLeave.MultiSelect = False
        Me.lstIssueLeave.Name = "lstIssueLeave"
        Me.lstIssueLeave.OptionalColumns = ""
        Me.lstIssueLeave.ShowMoreItem = False
        Me.lstIssueLeave.ShowSaveItem = False
        Me.lstIssueLeave.ShowSelectAll = True
        Me.lstIssueLeave.ShowSizeAllColumnsToFit = True
        Me.lstIssueLeave.Size = New System.Drawing.Size(410, 214)
        Me.lstIssueLeave.Sortable = True
        Me.lstIssueLeave.TabIndex = 273
        Me.lstIssueLeave.UseCompatibleStateImageBehavior = False
        Me.lstIssueLeave.View = System.Windows.Forms.View.Details
        '
        'colhIssueStartDate
        '
        Me.colhIssueStartDate.Tag = "colhIssueStartDate"
        Me.colhIssueStartDate.Text = "Date"
        Me.colhIssueStartDate.Width = 87
        '
        'colhIssueLeave
        '
        Me.colhIssueLeave.Tag = "colhIssueLeave"
        Me.colhIssueLeave.Text = "Leave"
        Me.colhIssueLeave.Width = 141
        '
        'colhIssueFormNo
        '
        Me.colhIssueFormNo.Tag = "colhIssueFormNo"
        Me.colhIssueFormNo.Text = "Form No"
        Me.colhIssueFormNo.Width = 96
        '
        'colhIssueTotLeave
        '
        Me.colhIssueTotLeave.Tag = "colhIssueTotLeave"
        Me.colhIssueTotLeave.Text = "Total Leave"
        Me.colhIssueTotLeave.Width = 80
        '
        'objcolhLeaveIssueunkid
        '
        Me.objcolhLeaveIssueunkid.Tag = "objcolhLeaveIssueunkid"
        Me.objcolhLeaveIssueunkid.Text = "LeaveIssueunkid"
        Me.objcolhLeaveIssueunkid.Width = 0
        '
        'objcolhLeaveIssueTranunkid
        '
        Me.objcolhLeaveIssueTranunkid.Tag = "objcolhLeaveIssueTranunkid"
        Me.objcolhLeaveIssueTranunkid.Text = "LeaveIssueTranunkid"
        Me.objcolhLeaveIssueTranunkid.Width = 0
        '
        'cboForm
        '
        Me.cboForm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboForm.FormattingEnabled = True
        Me.cboForm.Location = New System.Drawing.Point(291, 31)
        Me.cboForm.Name = "cboForm"
        Me.cboForm.Size = New System.Drawing.Size(121, 21)
        Me.cboForm.TabIndex = 274
        '
        'txtIssuemount
        '
        Me.txtIssuemount.AllowNegative = True
        Me.txtIssuemount.BackColor = System.Drawing.SystemColors.Window
        Me.txtIssuemount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIssuemount.DigitsInGroup = 0
        Me.txtIssuemount.Flags = 0
        Me.txtIssuemount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIssuemount.Location = New System.Drawing.Point(97, 58)
        Me.txtIssuemount.MaxDecimalPlaces = 6
        Me.txtIssuemount.MaxWholeDigits = 21
        Me.txtIssuemount.Name = "txtIssuemount"
        Me.txtIssuemount.Prefix = ""
        Me.txtIssuemount.RangeMax = 1.7976931348623157E+308
        Me.txtIssuemount.RangeMin = -1.7976931348623157E+308
        Me.txtIssuemount.ReadOnly = True
        Me.txtIssuemount.Size = New System.Drawing.Size(113, 21)
        Me.txtIssuemount.TabIndex = 16
        Me.txtIssuemount.Text = "0"
        '
        'cboIssueLeaveCode
        '
        Me.cboIssueLeaveCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIssueLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIssueLeaveCode.FormattingEnabled = True
        Me.cboIssueLeaveCode.Location = New System.Drawing.Point(97, 31)
        Me.cboIssueLeaveCode.Name = "cboIssueLeaveCode"
        Me.cboIssueLeaveCode.Size = New System.Drawing.Size(113, 21)
        Me.cboIssueLeaveCode.TabIndex = 14
        '
        'lblIssueLeave
        '
        Me.lblIssueLeave.BackColor = System.Drawing.Color.Transparent
        Me.lblIssueLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueLeave.Location = New System.Drawing.Point(6, 61)
        Me.lblIssueLeave.Name = "lblIssueLeave"
        Me.lblIssueLeave.Size = New System.Drawing.Size(84, 15)
        Me.lblIssueLeave.TabIndex = 268
        Me.lblIssueLeave.Text = "Total Leave"
        '
        'lblFormNo
        '
        Me.lblFormNo.BackColor = System.Drawing.Color.Transparent
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(217, 34)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(68, 15)
        Me.lblFormNo.TabIndex = 267
        Me.lblFormNo.Text = "Form No "
        '
        'lblIssueLeaveCode
        '
        Me.lblIssueLeaveCode.BackColor = System.Drawing.Color.Transparent
        Me.lblIssueLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueLeaveCode.Location = New System.Drawing.Point(6, 34)
        Me.lblIssueLeaveCode.Name = "lblIssueLeaveCode"
        Me.lblIssueLeaveCode.Size = New System.Drawing.Size(84, 15)
        Me.lblIssueLeaveCode.TabIndex = 265
        Me.lblIssueLeaveCode.Text = "Leave Type"
        '
        'objbtnDeleteByBatch
        '
        Me.objbtnDeleteByBatch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnDeleteByBatch.BackColor = System.Drawing.Color.White
        Me.objbtnDeleteByBatch.BackgroundImage = CType(resources.GetObject("objbtnDeleteByBatch.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDeleteByBatch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDeleteByBatch.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDeleteByBatch.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDeleteByBatch.FlatAppearance.BorderSize = 0
        Me.objbtnDeleteByBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDeleteByBatch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnDeleteByBatch.ForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByBatch.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDeleteByBatch.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByBatch.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDeleteByBatch.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByBatch.Image = Global.Aruti.Main.My.Resources.Resources.Delete_batch_wise
        Me.objbtnDeleteByBatch.Location = New System.Drawing.Point(368, 53)
        Me.objbtnDeleteByBatch.Name = "objbtnDeleteByBatch"
        Me.objbtnDeleteByBatch.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDeleteByBatch.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByBatch.Size = New System.Drawing.Size(30, 30)
        Me.objbtnDeleteByBatch.TabIndex = 11
        Me.objbtnDeleteByBatch.UseVisualStyleBackColor = True
        Me.objbtnDeleteByBatch.Visible = False
        '
        'objbtnDeleteByLeave
        '
        Me.objbtnDeleteByLeave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnDeleteByLeave.BackColor = System.Drawing.Color.White
        Me.objbtnDeleteByLeave.BackgroundImage = CType(resources.GetObject("objbtnDeleteByLeave.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDeleteByLeave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDeleteByLeave.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDeleteByLeave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDeleteByLeave.FlatAppearance.BorderSize = 0
        Me.objbtnDeleteByLeave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDeleteByLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnDeleteByLeave.ForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByLeave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDeleteByLeave.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByLeave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDeleteByLeave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByLeave.Image = Global.Aruti.Main.My.Resources.Resources.Delete_leave_type_wise
        Me.objbtnDeleteByLeave.Location = New System.Drawing.Point(332, 53)
        Me.objbtnDeleteByLeave.Name = "objbtnDeleteByLeave"
        Me.objbtnDeleteByLeave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDeleteByLeave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDeleteByLeave.Size = New System.Drawing.Size(30, 30)
        Me.objbtnDeleteByLeave.TabIndex = 10
        Me.objbtnDeleteByLeave.UseVisualStyleBackColor = True
        Me.objbtnDeleteByLeave.Visible = False
        '
        'gbAccrueList
        '
        Me.gbAccrueList.BorderColor = System.Drawing.Color.Black
        Me.gbAccrueList.Checked = False
        Me.gbAccrueList.CollapseAllExceptThis = False
        Me.gbAccrueList.CollapsedHoverImage = Nothing
        Me.gbAccrueList.CollapsedNormalImage = Nothing
        Me.gbAccrueList.CollapsedPressedImage = Nothing
        Me.gbAccrueList.CollapseOnLoad = False
        Me.gbAccrueList.Controls.Add(Me.EZeeLine2)
        Me.gbAccrueList.Controls.Add(Me.chkNoAction)
        Me.gbAccrueList.Controls.Add(Me.txtCfAmount)
        Me.gbAccrueList.Controls.Add(Me.LblCFAmount)
        Me.gbAccrueList.Controls.Add(Me.txtEligibilityAfter)
        Me.gbAccrueList.Controls.Add(Me.txtAccrualAmount)
        Me.gbAccrueList.Controls.Add(Me.objbtnAddLeaveType)
        Me.gbAccrueList.Controls.Add(Me.objelLine3)
        Me.gbAccrueList.Controls.Add(Me.LblEligibilityAfter)
        Me.gbAccrueList.Controls.Add(Me.btnGlobalAssign)
        Me.gbAccrueList.Controls.Add(Me.pnlAccrueLeave)
        Me.gbAccrueList.Controls.Add(Me.dtpStopDate)
        Me.gbAccrueList.Controls.Add(Me.dtpStartDate)
        Me.gbAccrueList.Controls.Add(Me.cboLeaveCode)
        Me.gbAccrueList.Controls.Add(Me.lblAccrualAmount)
        Me.gbAccrueList.Controls.Add(Me.lblStopDate)
        Me.gbAccrueList.Controls.Add(Me.lblStartDate)
        Me.gbAccrueList.Controls.Add(Me.lblLeaveCode)
        Me.gbAccrueList.Controls.Add(Me.LblMonthlyAccrue)
        Me.gbAccrueList.ExpandedHoverImage = Nothing
        Me.gbAccrueList.ExpandedNormalImage = Nothing
        Me.gbAccrueList.ExpandedPressedImage = Nothing
        Me.gbAccrueList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAccrueList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAccrueList.HeaderHeight = 25
        Me.gbAccrueList.HeaderMessage = ""
        Me.gbAccrueList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAccrueList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAccrueList.HeightOnCollapse = 0
        Me.gbAccrueList.LeftTextSpace = 0
        Me.gbAccrueList.Location = New System.Drawing.Point(14, 219)
        Me.gbAccrueList.Name = "gbAccrueList"
        Me.gbAccrueList.OpenHeight = 300
        Me.gbAccrueList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAccrueList.ShowBorder = True
        Me.gbAccrueList.ShowCheckBox = False
        Me.gbAccrueList.ShowCollapseButton = False
        Me.gbAccrueList.ShowDefaultBorderColor = True
        Me.gbAccrueList.ShowDownButton = False
        Me.gbAccrueList.ShowHeader = True
        Me.gbAccrueList.Size = New System.Drawing.Size(447, 315)
        Me.gbAccrueList.TabIndex = 264
        Me.gbAccrueList.Temp = 0
        Me.gbAccrueList.Text = "Accrue Leave"
        Me.gbAccrueList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine2
        '
        Me.EZeeLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine2.Location = New System.Drawing.Point(3, 84)
        Me.EZeeLine2.Name = "EZeeLine2"
        Me.EZeeLine2.Size = New System.Drawing.Size(440, 7)
        Me.EZeeLine2.TabIndex = 280
        Me.EZeeLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNoAction
        '
        Me.chkNoAction.BackColor = System.Drawing.Color.Transparent
        Me.chkNoAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNoAction.Location = New System.Drawing.Point(11, 96)
        Me.chkNoAction.Name = "chkNoAction"
        Me.chkNoAction.Size = New System.Drawing.Size(286, 17)
        Me.chkNoAction.TabIndex = 296
        Me.chkNoAction.Text = "Carry Forward Existing Balance to Next  Leave Cycle"
        Me.chkNoAction.UseVisualStyleBackColor = False
        '
        'txtCfAmount
        '
        Me.txtCfAmount.AllowNegative = False
        Me.txtCfAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCfAmount.DigitsInGroup = 0
        Me.txtCfAmount.Flags = 65536
        Me.txtCfAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCfAmount.Location = New System.Drawing.Point(198, 119)
        Me.txtCfAmount.MaxDecimalPlaces = 2
        Me.txtCfAmount.MaxWholeDigits = 4
        Me.txtCfAmount.Name = "txtCfAmount"
        Me.txtCfAmount.Prefix = ""
        Me.txtCfAmount.RangeMax = 1.7976931348623157E+308
        Me.txtCfAmount.RangeMin = -1.7976931348623157E+308
        Me.txtCfAmount.Size = New System.Drawing.Size(52, 21)
        Me.txtCfAmount.TabIndex = 298
        Me.txtCfAmount.Text = "0"
        Me.txtCfAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblCFAmount
        '
        Me.LblCFAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCFAmount.Location = New System.Drawing.Point(11, 121)
        Me.LblCFAmount.Name = "LblCFAmount"
        Me.LblCFAmount.Size = New System.Drawing.Size(184, 17)
        Me.LblCFAmount.TabIndex = 299
        Me.LblCFAmount.Text = "Max Days to be C/F to Next Leave Cycle"
        Me.LblCFAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEligibilityAfter
        '
        Me.txtEligibilityAfter.AllowNegative = False
        Me.txtEligibilityAfter.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEligibilityAfter.DigitsInGroup = 0
        Me.txtEligibilityAfter.Flags = 65536
        Me.txtEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEligibilityAfter.Location = New System.Drawing.Point(198, 143)
        Me.txtEligibilityAfter.MaxDecimalPlaces = 0
        Me.txtEligibilityAfter.MaxWholeDigits = 3
        Me.txtEligibilityAfter.Name = "txtEligibilityAfter"
        Me.txtEligibilityAfter.Prefix = ""
        Me.txtEligibilityAfter.RangeMax = 1.7976931348623157E+308
        Me.txtEligibilityAfter.RangeMin = -1.7976931348623157E+308
        Me.txtEligibilityAfter.Size = New System.Drawing.Size(52, 21)
        Me.txtEligibilityAfter.TabIndex = 295
        Me.txtEligibilityAfter.Text = "0"
        Me.txtEligibilityAfter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAccrualAmount
        '
        Me.txtAccrualAmount.AllowNegative = True
        Me.txtAccrualAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAccrualAmount.DigitsInGroup = 0
        Me.txtAccrualAmount.Flags = 0
        Me.txtAccrualAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccrualAmount.Location = New System.Drawing.Point(97, 58)
        Me.txtAccrualAmount.MaxDecimalPlaces = 6
        Me.txtAccrualAmount.MaxWholeDigits = 21
        Me.txtAccrualAmount.Name = "txtAccrualAmount"
        Me.txtAccrualAmount.Prefix = ""
        Me.txtAccrualAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAccrualAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAccrualAmount.Size = New System.Drawing.Size(92, 21)
        Me.txtAccrualAmount.TabIndex = 279
        Me.txtAccrualAmount.Text = "0"
        Me.txtAccrualAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnAddLeaveType
        '
        Me.objbtnAddLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLeaveType.BorderSelected = False
        Me.objbtnAddLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLeaveType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLeaveType.Location = New System.Drawing.Point(233, 31)
        Me.objbtnAddLeaveType.Name = "objbtnAddLeaveType"
        Me.objbtnAddLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLeaveType.TabIndex = 277
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(3, 169)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(442, 5)
        Me.objelLine3.TabIndex = 275
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEligibilityAfter
        '
        Me.LblEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEligibilityAfter.Location = New System.Drawing.Point(11, 145)
        Me.LblEligibilityAfter.Name = "LblEligibilityAfter"
        Me.LblEligibilityAfter.Size = New System.Drawing.Size(184, 17)
        Me.LblEligibilityAfter.TabIndex = 296
        Me.LblEligibilityAfter.Text = "Eligibility After (In Days)"
        Me.LblEligibilityAfter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnGlobalAssign
        '
        Me.btnGlobalAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGlobalAssign.BackColor = System.Drawing.Color.White
        Me.btnGlobalAssign.BackgroundImage = CType(resources.GetObject("btnGlobalAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnGlobalAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGlobalAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnGlobalAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnGlobalAssign.FlatAppearance.BorderSize = 0
        Me.btnGlobalAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGlobalAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGlobalAssign.ForeColor = System.Drawing.Color.Black
        Me.btnGlobalAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGlobalAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnGlobalAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGlobalAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGlobalAssign.Location = New System.Drawing.Point(193, 58)
        Me.btnGlobalAssign.Name = "btnGlobalAssign"
        Me.btnGlobalAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGlobalAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGlobalAssign.Size = New System.Drawing.Size(37, 21)
        Me.btnGlobalAssign.TabIndex = 7
        Me.btnGlobalAssign.Text = "GA"
        Me.btnGlobalAssign.UseVisualStyleBackColor = True
        '
        'pnlAccrueLeave
        '
        Me.pnlAccrueLeave.Controls.Add(Me.lvAccrue)
        Me.pnlAccrueLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAccrueLeave.Location = New System.Drawing.Point(3, 178)
        Me.pnlAccrueLeave.Name = "pnlAccrueLeave"
        Me.pnlAccrueLeave.Size = New System.Drawing.Size(440, 133)
        Me.pnlAccrueLeave.TabIndex = 265
        '
        'lvAccrue
        '
        Me.lvAccrue.BackColorOnChecked = True
        Me.lvAccrue.ColumnHeaders = Nothing
        Me.lvAccrue.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLeave, Me.colhStartdate, Me.colhenddate, Me.colhActualAmount, Me.colhMonthlyAcrrue, Me.colhTotLeave, Me.objcolhAccrueSetting, Me.objcolhDailyAmount, Me.objcolhLeavetypeunkid, Me.objcolhCFAmt, Me.objcolhEDays, Me.objcolhNoAction, Me.objcolhMaxNegativeLimit})
        Me.lvAccrue.CompulsoryColumns = ""
        Me.lvAccrue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAccrue.FullRowSelect = True
        Me.lvAccrue.GridLines = True
        Me.lvAccrue.GroupingColumn = Nothing
        Me.lvAccrue.HideSelection = False
        Me.lvAccrue.Location = New System.Drawing.Point(0, 0)
        Me.lvAccrue.MinColumnWidth = 50
        Me.lvAccrue.MultiSelect = False
        Me.lvAccrue.Name = "lvAccrue"
        Me.lvAccrue.OptionalColumns = ""
        Me.lvAccrue.ShowMoreItem = False
        Me.lvAccrue.ShowSaveItem = False
        Me.lvAccrue.ShowSelectAll = True
        Me.lvAccrue.ShowSizeAllColumnsToFit = True
        Me.lvAccrue.Size = New System.Drawing.Size(440, 133)
        Me.lvAccrue.Sortable = True
        Me.lvAccrue.TabIndex = 0
        Me.lvAccrue.UseCompatibleStateImageBehavior = False
        Me.lvAccrue.View = System.Windows.Forms.View.Details
        '
        'colhLeave
        '
        Me.colhLeave.Tag = "colhLeave"
        Me.colhLeave.Text = "Leave"
        Me.colhLeave.Width = 110
        '
        'colhStartdate
        '
        Me.colhStartdate.Tag = "colhStartdate"
        Me.colhStartdate.Text = "Startdate"
        Me.colhStartdate.Width = 80
        '
        'colhenddate
        '
        Me.colhenddate.Tag = "colhenddate"
        Me.colhenddate.Text = "Enddate"
        Me.colhenddate.Width = 80
        '
        'colhActualAmount
        '
        Me.colhActualAmount.Tag = "colhActualAmount"
        Me.colhActualAmount.Text = "Actual Amt"
        Me.colhActualAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhActualAmount.Width = 90
        '
        'colhMonthlyAcrrue
        '
        Me.colhMonthlyAcrrue.Tag = "colhMonthlyAcrrue"
        Me.colhMonthlyAcrrue.Text = "Monthly Accrue"
        Me.colhMonthlyAcrrue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhMonthlyAcrrue.Width = 0
        '
        'colhTotLeave
        '
        Me.colhTotLeave.Tag = "colhTotLeave"
        Me.colhTotLeave.Text = "Total Leave"
        Me.colhTotLeave.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTotLeave.Width = 80
        '
        'objcolhAccrueSetting
        '
        Me.objcolhAccrueSetting.Tag = "objcolhAccrueSetting"
        Me.objcolhAccrueSetting.Text = "AccrueSetting"
        Me.objcolhAccrueSetting.Width = 0
        '
        'objcolhDailyAmount
        '
        Me.objcolhDailyAmount.DisplayIndex = 8
        Me.objcolhDailyAmount.Tag = "objcolhDailyAmount"
        Me.objcolhDailyAmount.Text = "DailyAmount"
        Me.objcolhDailyAmount.Width = 0
        '
        'objcolhLeavetypeunkid
        '
        Me.objcolhLeavetypeunkid.DisplayIndex = 7
        Me.objcolhLeavetypeunkid.Tag = "objcolhLeavetypeunkid"
        Me.objcolhLeavetypeunkid.Text = "Leavetypeunkid"
        Me.objcolhLeavetypeunkid.Width = 0
        '
        'objcolhCFAmt
        '
        Me.objcolhCFAmt.Tag = "objcolhCFAmt"
        Me.objcolhCFAmt.Text = ""
        Me.objcolhCFAmt.Width = 0
        '
        'objcolhEDays
        '
        Me.objcolhEDays.Tag = "objcolhEDays"
        Me.objcolhEDays.Text = ""
        Me.objcolhEDays.Width = 0
        '
        'objcolhNoAction
        '
        Me.objcolhNoAction.Tag = "objcolhNoAction"
        Me.objcolhNoAction.Text = ""
        Me.objcolhNoAction.Width = 0
        '
        'objcolhMaxNegativeLimit
        '
        Me.objcolhMaxNegativeLimit.Tag = "objcolhMaxNegativeLimit"
        Me.objcolhMaxNegativeLimit.Text = "Max. Negative Limit"
        Me.objcolhMaxNegativeLimit.Width = 0
        '
        'dtpStopDate
        '
        Me.dtpStopDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Checked = False
        Me.dtpStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStopDate.Location = New System.Drawing.Point(322, 58)
        Me.dtpStopDate.Name = "dtpStopDate"
        Me.dtpStopDate.ShowCheckBox = True
        Me.dtpStopDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpStopDate.TabIndex = 8
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(323, 31)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(118, 21)
        Me.dtpStartDate.TabIndex = 5
        '
        'cboLeaveCode
        '
        Me.cboLeaveCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveCode.DropDownWidth = 300
        Me.cboLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveCode.FormattingEnabled = True
        Me.cboLeaveCode.Location = New System.Drawing.Point(97, 31)
        Me.cboLeaveCode.Name = "cboLeaveCode"
        Me.cboLeaveCode.Size = New System.Drawing.Size(133, 21)
        Me.cboLeaveCode.TabIndex = 4
        '
        'lblAccrualAmount
        '
        Me.lblAccrualAmount.BackColor = System.Drawing.Color.Transparent
        Me.lblAccrualAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccrualAmount.Location = New System.Drawing.Point(7, 61)
        Me.lblAccrualAmount.Name = "lblAccrualAmount"
        Me.lblAccrualAmount.Size = New System.Drawing.Size(84, 15)
        Me.lblAccrualAmount.TabIndex = 228
        Me.lblAccrualAmount.Text = "Yearly Accrue"
        '
        'lblStopDate
        '
        Me.lblStopDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopDate.Location = New System.Drawing.Point(256, 61)
        Me.lblStopDate.Name = "lblStopDate"
        Me.lblStopDate.Size = New System.Drawing.Size(61, 15)
        Me.lblStopDate.TabIndex = 227
        Me.lblStopDate.Text = "Stop Date"
        '
        'lblStartDate
        '
        Me.lblStartDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(256, 34)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(61, 15)
        Me.lblStartDate.TabIndex = 226
        Me.lblStartDate.Text = "Start Date"
        '
        'lblLeaveCode
        '
        Me.lblLeaveCode.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveCode.Location = New System.Drawing.Point(7, 34)
        Me.lblLeaveCode.Name = "lblLeaveCode"
        Me.lblLeaveCode.Size = New System.Drawing.Size(84, 15)
        Me.lblLeaveCode.TabIndex = 225
        Me.lblLeaveCode.Text = "Leave Type"
        '
        'LblMonthlyAccrue
        '
        Me.LblMonthlyAccrue.BackColor = System.Drawing.Color.Transparent
        Me.LblMonthlyAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonthlyAccrue.Location = New System.Drawing.Point(7, 61)
        Me.LblMonthlyAccrue.Name = "LblMonthlyAccrue"
        Me.LblMonthlyAccrue.Size = New System.Drawing.Size(84, 15)
        Me.LblMonthlyAccrue.TabIndex = 301
        Me.LblMonthlyAccrue.Text = "Monthly Accrue"
        '
        'dtpAsOnDate
        '
        Me.dtpAsOnDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsOnDate.Checked = False
        Me.dtpAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsOnDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsOnDate.Location = New System.Drawing.Point(758, 6)
        Me.dtpAsOnDate.Name = "dtpAsOnDate"
        Me.dtpAsOnDate.Size = New System.Drawing.Size(128, 21)
        Me.dtpAsOnDate.TabIndex = 261
        '
        'gbLeaveBalance
        '
        Me.gbLeaveBalance.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveBalance.Checked = False
        Me.gbLeaveBalance.CollapseAllExceptThis = False
        Me.gbLeaveBalance.CollapsedHoverImage = Nothing
        Me.gbLeaveBalance.CollapsedNormalImage = Nothing
        Me.gbLeaveBalance.CollapsedPressedImage = Nothing
        Me.gbLeaveBalance.CollapseOnLoad = False
        Me.gbLeaveBalance.Controls.Add(Me.radIssueOnExceedBalAsonDate)
        Me.gbLeaveBalance.Controls.Add(Me.nudMaxNegativeDays)
        Me.gbLeaveBalance.Controls.Add(Me.lblMaxNegativeLimit)
        Me.gbLeaveBalance.Controls.Add(Me.radDonotIssueAsonDate)
        Me.gbLeaveBalance.Controls.Add(Me.radExceedBal)
        Me.gbLeaveBalance.Controls.Add(Me.radIssuebal)
        Me.gbLeaveBalance.Controls.Add(Me.cboEmployee)
        Me.gbLeaveBalance.Controls.Add(Me.lblEmployee)
        Me.gbLeaveBalance.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbLeaveBalance.Controls.Add(Me.cboLeaveYear)
        Me.gbLeaveBalance.Controls.Add(Me.lblLeaveYear)
        Me.gbLeaveBalance.Controls.Add(Me.radDonotIssue)
        Me.gbLeaveBalance.ExpandedHoverImage = Nothing
        Me.gbLeaveBalance.ExpandedNormalImage = Nothing
        Me.gbLeaveBalance.ExpandedPressedImage = Nothing
        Me.gbLeaveBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveBalance.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveBalance.HeaderHeight = 25
        Me.gbLeaveBalance.HeaderMessage = ""
        Me.gbLeaveBalance.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveBalance.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveBalance.HeightOnCollapse = 0
        Me.gbLeaveBalance.LeftTextSpace = 0
        Me.gbLeaveBalance.Location = New System.Drawing.Point(14, 7)
        Me.gbLeaveBalance.Name = "gbLeaveBalance"
        Me.gbLeaveBalance.OpenHeight = 300
        Me.gbLeaveBalance.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveBalance.ShowBorder = True
        Me.gbLeaveBalance.ShowCheckBox = False
        Me.gbLeaveBalance.ShowCollapseButton = False
        Me.gbLeaveBalance.ShowDefaultBorderColor = True
        Me.gbLeaveBalance.ShowDownButton = False
        Me.gbLeaveBalance.ShowHeader = True
        Me.gbLeaveBalance.Size = New System.Drawing.Size(337, 200)
        Me.gbLeaveBalance.TabIndex = 263
        Me.gbLeaveBalance.Temp = 0
        Me.gbLeaveBalance.Text = "Leave Accrue Info"
        Me.gbLeaveBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radIssueOnExceedBalAsonDate
        '
        Me.radIssueOnExceedBalAsonDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssueOnExceedBalAsonDate.Location = New System.Drawing.Point(11, 176)
        Me.radIssueOnExceedBalAsonDate.Name = "radIssueOnExceedBalAsonDate"
        Me.radIssueOnExceedBalAsonDate.Size = New System.Drawing.Size(304, 17)
        Me.radIssueOnExceedBalAsonDate.TabIndex = 318
        Me.radIssueOnExceedBalAsonDate.Text = "Issue On Exceeding Balance as on Date"
        Me.radIssueOnExceedBalAsonDate.UseVisualStyleBackColor = True
        '
        'nudMaxNegativeDays
        '
        Me.nudMaxNegativeDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxNegativeDays.Location = New System.Drawing.Point(278, 109)
        Me.nudMaxNegativeDays.Maximum = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudMaxNegativeDays.Minimum = New Decimal(New Integer() {999, 0, 0, -2147483648})
        Me.nudMaxNegativeDays.Name = "nudMaxNegativeDays"
        Me.nudMaxNegativeDays.Size = New System.Drawing.Size(48, 21)
        Me.nudMaxNegativeDays.TabIndex = 316
        Me.nudMaxNegativeDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxNegativeLimit
        '
        Me.lblMaxNegativeLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxNegativeLimit.Location = New System.Drawing.Point(212, 91)
        Me.lblMaxNegativeLimit.Name = "lblMaxNegativeLimit"
        Me.lblMaxNegativeLimit.Size = New System.Drawing.Size(120, 17)
        Me.lblMaxNegativeLimit.TabIndex = 315
        Me.lblMaxNegativeLimit.Text = "Set Max. Negative Limit"
        Me.lblMaxNegativeLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDonotIssueAsonDate
        '
        Me.radDonotIssueAsonDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDonotIssueAsonDate.Location = New System.Drawing.Point(11, 154)
        Me.radDonotIssueAsonDate.Name = "radDonotIssueAsonDate"
        Me.radDonotIssueAsonDate.Size = New System.Drawing.Size(304, 17)
        Me.radDonotIssueAsonDate.TabIndex = 232
        Me.radDonotIssueAsonDate.Text = "Don't Issue on Exceeding Leave Balance As On Date"
        Me.radDonotIssueAsonDate.UseVisualStyleBackColor = True
        '
        'radExceedBal
        '
        Me.radExceedBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExceedBal.Location = New System.Drawing.Point(11, 111)
        Me.radExceedBal.Name = "radExceedBal"
        Me.radExceedBal.Size = New System.Drawing.Size(205, 17)
        Me.radExceedBal.TabIndex = 229
        Me.radExceedBal.Text = "Exceeding Leave Balance with unpaid"
        Me.radExceedBal.UseVisualStyleBackColor = True
        '
        'radIssuebal
        '
        Me.radIssuebal.Checked = True
        Me.radIssuebal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssuebal.Location = New System.Drawing.Point(11, 91)
        Me.radIssuebal.Name = "radIssuebal"
        Me.radIssuebal.Size = New System.Drawing.Size(169, 16)
        Me.radIssuebal.TabIndex = 228
        Me.radIssuebal.TabStop = True
        Me.radIssuebal.Text = "Issue Balance with Paid"
        Me.radIssuebal.UseVisualStyleBackColor = True
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(93, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(202, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(79, 15)
        Me.lblEmployee.TabIndex = 226
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(298, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboLeaveYear
        '
        Me.cboLeaveYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveYear.FormattingEnabled = True
        Me.cboLeaveYear.Location = New System.Drawing.Point(93, 33)
        Me.cboLeaveYear.Name = "cboLeaveYear"
        Me.cboLeaveYear.Size = New System.Drawing.Size(113, 21)
        Me.cboLeaveYear.TabIndex = 1
        '
        'lblLeaveYear
        '
        Me.lblLeaveYear.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveYear.Location = New System.Drawing.Point(8, 36)
        Me.lblLeaveYear.Name = "lblLeaveYear"
        Me.lblLeaveYear.Size = New System.Drawing.Size(79, 15)
        Me.lblLeaveYear.TabIndex = 224
        Me.lblLeaveYear.Text = "Leave Year"
        '
        'radDonotIssue
        '
        Me.radDonotIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDonotIssue.Location = New System.Drawing.Point(11, 133)
        Me.radDonotIssue.Name = "radDonotIssue"
        Me.radDonotIssue.Size = New System.Drawing.Size(304, 17)
        Me.radDonotIssue.TabIndex = 230
        Me.radDonotIssue.Text = "Don't Issue on Exceeding Leave Balance On Total Balance"
        Me.radDonotIssue.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.lblLastYearIssued)
        Me.objFooter.Controls.Add(Me.objlblLastYearIssuedValue)
        Me.objFooter.Controls.Add(Me.objlblLastYearAccruedValue)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.lblLastYearAccrued)
        Me.objFooter.Controls.Add(Me.objbtnExport)
        Me.objFooter.Controls.Add(Me.objbtnImport)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 542)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(895, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(694, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 18
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(790, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 19
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblLastYearIssued
        '
        Me.lblLastYearIssued.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastYearIssued.Location = New System.Drawing.Point(104, 29)
        Me.lblLastYearIssued.Name = "lblLastYearIssued"
        Me.lblLastYearIssued.Size = New System.Drawing.Size(173, 19)
        Me.lblLastYearIssued.TabIndex = 6
        Me.lblLastYearIssued.Text = "Total Issued Upto Last Year"
        Me.lblLastYearIssued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLastYearIssued.Visible = False
        '
        'objlblLastYearIssuedValue
        '
        Me.objlblLastYearIssuedValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblLastYearIssuedValue.Location = New System.Drawing.Point(284, 26)
        Me.objlblLastYearIssuedValue.Name = "objlblLastYearIssuedValue"
        Me.objlblLastYearIssuedValue.Size = New System.Drawing.Size(76, 21)
        Me.objlblLastYearIssuedValue.TabIndex = 7
        Me.objlblLastYearIssuedValue.Text = "0"
        Me.objlblLastYearIssuedValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.objlblLastYearIssuedValue.Visible = False
        '
        'objlblLastYearAccruedValue
        '
        Me.objlblLastYearAccruedValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblLastYearAccruedValue.Location = New System.Drawing.Point(284, 4)
        Me.objlblLastYearAccruedValue.Name = "objlblLastYearAccruedValue"
        Me.objlblLastYearAccruedValue.Size = New System.Drawing.Size(76, 22)
        Me.objlblLastYearAccruedValue.TabIndex = 3
        Me.objlblLastYearAccruedValue.Text = "0"
        Me.objlblLastYearAccruedValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.objlblLastYearAccruedValue.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(597, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 17
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblLastYearAccrued
        '
        Me.lblLastYearAccrued.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastYearAccrued.Location = New System.Drawing.Point(104, 4)
        Me.lblLastYearAccrued.Name = "lblLastYearAccrued"
        Me.lblLastYearAccrued.Size = New System.Drawing.Size(173, 22)
        Me.lblLastYearAccrued.TabIndex = 2
        Me.lblLastYearAccrued.Text = "Total Accrued Upto Last Year"
        Me.lblLastYearAccrued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLastYearAccrued.Visible = False
        '
        'objbtnExport
        '
        Me.objbtnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnExport.BackColor = System.Drawing.Color.White
        Me.objbtnExport.BackgroundImage = CType(resources.GetObject("objbtnExport.BackgroundImage"), System.Drawing.Image)
        Me.objbtnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnExport.BorderColor = System.Drawing.Color.Empty
        Me.objbtnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnExport.FlatAppearance.BorderSize = 0
        Me.objbtnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnExport.ForeColor = System.Drawing.Color.Black
        Me.objbtnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnExport.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnExport.Location = New System.Drawing.Point(50, 13)
        Me.objbtnExport.Name = "objbtnExport"
        Me.objbtnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnExport.Size = New System.Drawing.Size(30, 30)
        Me.objbtnExport.TabIndex = 13
        Me.objbtnExport.UseVisualStyleBackColor = True
        Me.objbtnExport.Visible = False
        '
        'objbtnImport
        '
        Me.objbtnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnImport.BackColor = System.Drawing.Color.White
        Me.objbtnImport.BackgroundImage = CType(resources.GetObject("objbtnImport.BackgroundImage"), System.Drawing.Image)
        Me.objbtnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnImport.BorderColor = System.Drawing.Color.Empty
        Me.objbtnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnImport.FlatAppearance.BorderSize = 0
        Me.objbtnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnImport.ForeColor = System.Drawing.Color.Black
        Me.objbtnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnImport.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnImport.Location = New System.Drawing.Point(14, 13)
        Me.objbtnImport.Name = "objbtnImport"
        Me.objbtnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnImport.Size = New System.Drawing.Size(30, 30)
        Me.objbtnImport.TabIndex = 12
        Me.objbtnImport.UseVisualStyleBackColor = True
        Me.objbtnImport.Visible = False
        '
        'frmLeaveAccrue_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(895, 597)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveAccrue_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Accrue"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.gbIssueList.ResumeLayout(False)
        Me.gbIssueList.PerformLayout()
        Me.pnlIssueLeave.ResumeLayout(False)
        Me.gbAccrueList.ResumeLayout(False)
        Me.gbAccrueList.PerformLayout()
        Me.pnlAccrueLeave.ResumeLayout(False)
        Me.gbLeaveBalance.ResumeLayout(False)
        CType(Me.nudMaxNegativeDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents cboLeaveYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveYear As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpAsOnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblLastYearAccruedValue As System.Windows.Forms.Label
    Friend WithEvents lblLastYearAccrued As System.Windows.Forms.Label
    Friend WithEvents gbLeaveBalance As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbIssueList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbAccrueList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAccrualAmount As System.Windows.Forms.Label
    Friend WithEvents lblLeaveCode As System.Windows.Forms.Label
    Friend WithEvents cboLeaveCode As System.Windows.Forms.ComboBox
    Friend WithEvents pnlAccrueLeave As System.Windows.Forms.Panel
    Friend WithEvents lvAccrue As eZee.Common.eZeeListView
    Friend WithEvents colhLeave As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTotLeave As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtIssuemount As eZee.TextBox.NumericTextBox
    Friend WithEvents cboIssueLeaveCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblIssueLeave As System.Windows.Forms.Label
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents lblIssueLeaveCode As System.Windows.Forms.Label
    Friend WithEvents lstIssueLeave As eZee.Common.eZeeListView
    Friend WithEvents colhIssueLeave As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIssueStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIssueFormNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIssueTotLeave As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboForm As System.Windows.Forms.ComboBox
    Friend WithEvents pnlIssueLeave As System.Windows.Forms.Panel
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents objlblBalancevalue As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblLastYearIssued As System.Windows.Forms.Label
    Friend WithEvents objlblLastYearIssuedValue As System.Windows.Forms.Label
    Friend WithEvents lblTodateIssued As System.Windows.Forms.Label
    Friend WithEvents objlblTodateIssuedvalue As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents objbtnExport As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnImport As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDeleteByBatch As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDeleteByLeave As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnGlobalAssign As eZee.Common.eZeeLightButton
    Friend WithEvents lnkIssueLeave As System.Windows.Forms.LinkLabel
    Friend WithEvents objcolhLeaveIssueunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhLeaveIssueTranunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpStopDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStopDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents radDonotIssue As System.Windows.Forms.RadioButton
    Friend WithEvents radExceedBal As System.Windows.Forms.RadioButton
    Friend WithEvents radIssuebal As System.Windows.Forms.RadioButton
    Friend WithEvents objcolhAccrueSetting As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhLeavetypeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartdate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhenddate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents objTpToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents txtAccrualAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents objcolhDailyAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhActualAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtEligibilityAfter As eZee.TextBox.NumericTextBox
    Friend WithEvents LblEligibilityAfter As System.Windows.Forms.Label
    Friend WithEvents txtCfAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents LblCFAmount As System.Windows.Forms.Label
    Friend WithEvents chkNoAction As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhCFAmt As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEDays As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhNoAction As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeLine2 As eZee.Common.eZeeLine
    Friend WithEvents objlblLeaveBFvalue As System.Windows.Forms.Label
    Friend WithEvents LblMonthlyAccrue As System.Windows.Forms.Label
    Friend WithEvents colhMonthlyAcrrue As System.Windows.Forms.ColumnHeader
    Friend WithEvents radDonotIssueAsonDate As System.Windows.Forms.RadioButton
    Friend WithEvents nudMaxNegativeDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMaxNegativeLimit As System.Windows.Forms.Label
    Friend WithEvents objcolhMaxNegativeLimit As System.Windows.Forms.ColumnHeader
    Friend WithEvents radIssueOnExceedBalAsonDate As System.Windows.Forms.RadioButton
    Friend WithEvents LblLeaveBF As System.Windows.Forms.Label
    Friend WithEvents objlblTotalAdjustment As System.Windows.Forms.Label
    Friend WithEvents LblTotalAdjustment As System.Windows.Forms.Label
    Friend WithEvents lblToDateAccrued As System.Windows.Forms.Label
    Friend WithEvents objlblAccruedToDateValue As System.Windows.Forms.Label
    Friend WithEvents lblAs As eZee.Common.eZeeLine
End Class
