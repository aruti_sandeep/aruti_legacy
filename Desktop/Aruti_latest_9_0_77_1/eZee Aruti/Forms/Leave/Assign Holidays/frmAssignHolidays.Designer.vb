﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignHolidays
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssignHolidays))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbRemark = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlRemark = New System.Windows.Forms.Panel
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.gbHolidays = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objchkHolidayAll = New System.Windows.Forms.CheckBox
        Me.lblholidays = New System.Windows.Forms.Label
        Me.pnlHolidays = New System.Windows.Forms.Panel
        Me.lvHolidays = New System.Windows.Forms.ListView
        Me.objcolhDate = New System.Windows.Forms.ColumnHeader
        Me.objColhName = New System.Windows.Forms.ColumnHeader
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvEmployeeList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeeName = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblClass = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblShift = New System.Windows.Forms.Label
        Me.cboShiftInfo = New System.Windows.Forms.ComboBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.pnlMainInfo.SuspendLayout()
        Me.gbRemark.SuspendLayout()
        Me.pnlRemark.SuspendLayout()
        Me.gbHolidays.SuspendLayout()
        Me.pnlHolidays.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbRemark)
        Me.pnlMainInfo.Controls.Add(Me.gbHolidays)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(779, 554)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbRemark
        '
        Me.gbRemark.BorderColor = System.Drawing.Color.Black
        Me.gbRemark.Checked = False
        Me.gbRemark.CollapseAllExceptThis = False
        Me.gbRemark.CollapsedHoverImage = Nothing
        Me.gbRemark.CollapsedNormalImage = Nothing
        Me.gbRemark.CollapsedPressedImage = Nothing
        Me.gbRemark.CollapseOnLoad = False
        Me.gbRemark.Controls.Add(Me.pnlRemark)
        Me.gbRemark.ExpandedHoverImage = Nothing
        Me.gbRemark.ExpandedNormalImage = Nothing
        Me.gbRemark.ExpandedPressedImage = Nothing
        Me.gbRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRemark.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRemark.HeaderHeight = 25
        Me.gbRemark.HeaderMessage = ""
        Me.gbRemark.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRemark.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRemark.HeightOnCollapse = 0
        Me.gbRemark.LeftTextSpace = 0
        Me.gbRemark.Location = New System.Drawing.Point(260, 391)
        Me.gbRemark.Name = "gbRemark"
        Me.gbRemark.OpenHeight = 300
        Me.gbRemark.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRemark.ShowBorder = True
        Me.gbRemark.ShowCheckBox = False
        Me.gbRemark.ShowCollapseButton = False
        Me.gbRemark.ShowDefaultBorderColor = True
        Me.gbRemark.ShowDownButton = False
        Me.gbRemark.ShowHeader = True
        Me.gbRemark.Size = New System.Drawing.Size(506, 102)
        Me.gbRemark.TabIndex = 4
        Me.gbRemark.Temp = 0
        Me.gbRemark.Text = "Remark"
        Me.gbRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlRemark
        '
        Me.pnlRemark.Controls.Add(Me.txtRemark)
        Me.pnlRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlRemark.Location = New System.Drawing.Point(2, 26)
        Me.pnlRemark.Name = "pnlRemark"
        Me.pnlRemark.Size = New System.Drawing.Size(502, 74)
        Me.pnlRemark.TabIndex = 1
        '
        'txtRemark
        '
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(502, 74)
        Me.txtRemark.TabIndex = 12
        '
        'gbHolidays
        '
        Me.gbHolidays.BorderColor = System.Drawing.Color.Black
        Me.gbHolidays.Checked = False
        Me.gbHolidays.CollapseAllExceptThis = False
        Me.gbHolidays.CollapsedHoverImage = Nothing
        Me.gbHolidays.CollapsedNormalImage = Nothing
        Me.gbHolidays.CollapsedPressedImage = Nothing
        Me.gbHolidays.CollapseOnLoad = False
        Me.gbHolidays.Controls.Add(Me.objchkHolidayAll)
        Me.gbHolidays.Controls.Add(Me.lblholidays)
        Me.gbHolidays.Controls.Add(Me.pnlHolidays)
        Me.gbHolidays.ExpandedHoverImage = Nothing
        Me.gbHolidays.ExpandedNormalImage = Nothing
        Me.gbHolidays.ExpandedPressedImage = Nothing
        Me.gbHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHolidays.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHolidays.HeaderHeight = 25
        Me.gbHolidays.HeaderMessage = ""
        Me.gbHolidays.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbHolidays.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHolidays.HeightOnCollapse = 0
        Me.gbHolidays.LeftTextSpace = 0
        Me.gbHolidays.Location = New System.Drawing.Point(260, 166)
        Me.gbHolidays.Name = "gbHolidays"
        Me.gbHolidays.OpenHeight = 300
        Me.gbHolidays.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHolidays.ShowBorder = True
        Me.gbHolidays.ShowCheckBox = False
        Me.gbHolidays.ShowCollapseButton = False
        Me.gbHolidays.ShowDefaultBorderColor = True
        Me.gbHolidays.ShowDownButton = False
        Me.gbHolidays.ShowHeader = True
        Me.gbHolidays.Size = New System.Drawing.Size(504, 219)
        Me.gbHolidays.TabIndex = 3
        Me.gbHolidays.Temp = 0
        Me.gbHolidays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkHolidayAll
        '
        Me.objchkHolidayAll.AutoSize = True
        Me.objchkHolidayAll.Location = New System.Drawing.Point(10, 6)
        Me.objchkHolidayAll.Name = "objchkHolidayAll"
        Me.objchkHolidayAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkHolidayAll.TabIndex = 74
        Me.objchkHolidayAll.UseVisualStyleBackColor = True
        '
        'lblholidays
        '
        Me.lblholidays.BackColor = System.Drawing.Color.Transparent
        Me.lblholidays.Location = New System.Drawing.Point(31, 4)
        Me.lblholidays.Name = "lblholidays"
        Me.lblholidays.Size = New System.Drawing.Size(88, 17)
        Me.lblholidays.TabIndex = 224
        Me.lblholidays.Text = "Holidays"
        Me.lblholidays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlHolidays
        '
        Me.pnlHolidays.Controls.Add(Me.lvHolidays)
        Me.pnlHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlHolidays.Location = New System.Drawing.Point(2, 26)
        Me.pnlHolidays.Name = "pnlHolidays"
        Me.pnlHolidays.Size = New System.Drawing.Size(499, 191)
        Me.pnlHolidays.TabIndex = 1
        '
        'lvHolidays
        '
        Me.lvHolidays.CheckBoxes = True
        Me.lvHolidays.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhDate, Me.objColhName})
        Me.lvHolidays.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvHolidays.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvHolidays.Location = New System.Drawing.Point(0, 0)
        Me.lvHolidays.Name = "lvHolidays"
        Me.lvHolidays.Size = New System.Drawing.Size(499, 191)
        Me.lvHolidays.TabIndex = 11
        Me.lvHolidays.UseCompatibleStateImageBehavior = False
        Me.lvHolidays.View = System.Windows.Forms.View.Details
        '
        'objcolhDate
        '
        Me.objcolhDate.Tag = "objcolhDate"
        Me.objcolhDate.Text = ""
        Me.objcolhDate.Width = 100
        '
        'objColhName
        '
        Me.objColhName.Tag = "objColhName"
        Me.objColhName.Text = ""
        Me.objColhName.Width = 200
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.lnkAllocation)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 166)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(242, 327)
        Me.gbEmployeeList.TabIndex = 2
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.chkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvEmployeeList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(238, 301)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 73
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'lvEmployeeList
        '
        Me.lvEmployeeList.BackColorOnChecked = True
        Me.lvEmployeeList.CheckBoxes = True
        Me.lvEmployeeList.ColumnHeaders = Nothing
        Me.lvEmployeeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhCode, Me.colhEmployeeName})
        Me.lvEmployeeList.CompulsoryColumns = ""
        Me.lvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmployeeList.FullRowSelect = True
        Me.lvEmployeeList.GridLines = True
        Me.lvEmployeeList.GroupingColumn = Nothing
        Me.lvEmployeeList.HideSelection = False
        Me.lvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmployeeList.MinColumnWidth = 50
        Me.lvEmployeeList.MultiSelect = False
        Me.lvEmployeeList.Name = "lvEmployeeList"
        Me.lvEmployeeList.OptionalColumns = ""
        Me.lvEmployeeList.ShowMoreItem = False
        Me.lvEmployeeList.ShowSaveItem = False
        Me.lvEmployeeList.ShowSelectAll = True
        Me.lvEmployeeList.ShowSizeAllColumnsToFit = True
        Me.lvEmployeeList.Size = New System.Drawing.Size(238, 301)
        Me.lvEmployeeList.Sortable = True
        Me.lvEmployeeList.TabIndex = 9
        Me.lvEmployeeList.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 30
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 88
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.Tag = "colhEmployeeName"
        Me.colhEmployeeName.Text = "Name"
        Me.colhEmployeeName.Width = 144
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.Label1)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboSections)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblShift)
        Me.gbFilterCriteria.Controls.Add(Me.cboShiftInfo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 67)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(755, 93)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(212, 64)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(43, 17)
        Me.lblClass.TabIndex = 79
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(274, 62)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(104, 21)
        Me.cboClass.TabIndex = 7
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(573, 36)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(57, 17)
        Me.lblGrade.TabIndex = 75
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(636, 33)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(104, 21)
        Me.cboGrade.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(396, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 17)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "Job"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(445, 33)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(104, 21)
        Me.cboJob.TabIndex = 3
        '
        'cboSections
        '
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(274, 33)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(104, 21)
        Me.cboSections.TabIndex = 2
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(212, 36)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(56, 17)
        Me.lblSection.TabIndex = 71
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(728, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(705, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(8, 62)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(68, 15)
        Me.lblShift.TabIndex = 67
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboShiftInfo
        '
        Me.cboShiftInfo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShiftInfo.FormattingEnabled = True
        Me.cboShiftInfo.Location = New System.Drawing.Point(82, 60)
        Me.cboShiftInfo.Name = "cboShiftInfo"
        Me.cboShiftInfo.Size = New System.Drawing.Size(104, 21)
        Me.cboShiftInfo.TabIndex = 5
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 36)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(68, 15)
        Me.lblDepartment.TabIndex = 14
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(82, 33)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(104, 21)
        Me.cboDepartment.TabIndex = 1
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(779, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Assign Holidays"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 499)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(779, 55)
        Me.objFooter.TabIndex = 20
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(567, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(670, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(154, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'frmAssignHolidays
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 554)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssignHolidays"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assign Holidays"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbRemark.ResumeLayout(False)
        Me.pnlRemark.ResumeLayout(False)
        Me.pnlRemark.PerformLayout()
        Me.gbHolidays.ResumeLayout(False)
        Me.gbHolidays.PerformLayout()
        Me.pnlHolidays.ResumeLayout(False)
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboShiftInfo As System.Windows.Forms.ComboBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents lvEmployeeList As eZee.Common.eZeeListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployeeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbHolidays As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlHolidays As System.Windows.Forms.Panel
    Friend WithEvents gbRemark As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlRemark As System.Windows.Forms.Panel
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvHolidays As System.Windows.Forms.ListView
    Friend WithEvents objcolhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lblholidays As System.Windows.Forms.Label
    Friend WithEvents objchkHolidayAll As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
End Class
