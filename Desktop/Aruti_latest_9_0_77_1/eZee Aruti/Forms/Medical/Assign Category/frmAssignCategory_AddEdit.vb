﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmAssignCategory_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssignCategory_AddEdit"
    Private mblnCancel As Boolean = True
    Private objMedicalCategoryMaster As clsmedical_category_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMedicalCategoryUnkid As Integer = -1
    Dim objCurrency As clsExchangeRate

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMedicalCategoryUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintMedicalCategoryUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmAssignCategory_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMedicalCategoryMaster = New clsmedical_category_master
        objCurrency = New clsExchangeRate
        Try
            Call Set_Logo(Me, gApplicationType)
            objCurrency._ExchangeRateunkid = 1
            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objMedicalCategoryMaster._Medicalcategoryunkid = mintMedicalCategoryUnkid
            End If
            FillCombo()
            GetValue()
            cboCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignCategory_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignCategory_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignCategory_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignCategory_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignCategory_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignCategory_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMedicalCategoryMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsmedical_category_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_category_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"


    'Pinkal (01-Oct-2014) -- Start
    'Enhancement -  Changes For FDRC Report

    Private Sub objbtnSearchMaritalStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMaritalStatus.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboMaritalStatus.DataSource, DataTable)
            objfrm.ValueMember = cboMaritalStatus.ValueMember
            objfrm.DisplayMember = cboMaritalStatus.DisplayMember
            If objfrm.DisplayDialog Then
                cboMaritalStatus.SelectedValue = objfrm.SelectedValue
            End If
            cboMaritalStatus.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMaritalStatus_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (01-Oct-2014) -- End


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboCategory.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Medical Category is compulsory information.Please Select Medical Category."), enMsgBoxStyle.Information)
                cboCategory.Focus()
                Exit Sub

                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                ' AS PER MR.RUTTA'S COMMENT 

                'ElseIf nudDependants.Value = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Dependants cannot be 0. Dependants must greater than 0."), enMsgBoxStyle.Information)
                '    nudDependants.Focus()
                '    Exit Sub

                'Pinkal (06-Feb-2012) -- End


                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                'ElseIf Trim(txtAmount.Text) = "" Or cdec(txtAmount.Text) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Amount cannot be 0. Amount must greater than 0."), enMsgBoxStyle.Information)
                '    txtAmount.Focus()
                '    Exit Sub
                'ElseIf txtAmount.Decimal <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Amount cannot be 0. Amount must greater than 0."), enMsgBoxStyle.Information)
                '    txtAmount.Focus()
                '    Exit Sub
                'Sandeep [ 09 Oct 2010 ] -- End 
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objMedicalCategoryMaster._FormName = mstrModuleName
            objMedicalCategoryMaster._LoginEmployeeunkid = 0
            objMedicalCategoryMaster._ClientIP = getIP()
            objMedicalCategoryMaster._HostName = getHostName()
            objMedicalCategoryMaster._FromWeb = False
            objMedicalCategoryMaster._AuditUserId = User._Object._Userunkid
objMedicalCategoryMaster._CompanyUnkid = Company._Object._Companyunkid
            objMedicalCategoryMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objMedicalCategoryMaster.Update()
            Else
                blnFlag = objMedicalCategoryMaster.Insert()
            End If

            If blnFlag = False And objMedicalCategoryMaster._Message <> "" Then
                eZeeMsgBox.Show(objMedicalCategoryMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objMedicalCategoryMaster = Nothing
                    objMedicalCategoryMaster = New clsmedical_category_master
                    Call GetValue()
                    cboCategory.Select()
                Else
                    mintMedicalCategoryUnkid = objMedicalCategoryMaster._Medicalcategoryunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCategory.Click
        Try
            Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If objfrmMedicalMaster_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, enMedicalMasterType.Category) Then
            '    FillCombo()
            '    cboCategory.Select()
            'End If

            If objfrmMedicalMaster_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, enMedicalMasterType.Medical_Category) Then
                FillCombo()
                cboCategory.Select()
            End If

            'Pinkal (12-Oct-2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboCategory.BackColor = GUI.ColorComp
            nudDependants.BackColor = GUI.ColorComp
            txtAmount.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional


            'Pinkal (01-Oct-2014) -- Start
            'Enhancement -  Changes For FDRC Report
            cboMaritalStatus.BackColor = GUI.ColorOptional
            'Pinkal (01-Oct-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboCategory.SelectedValue = objMedicalCategoryMaster._Mdcategorymasterunkid
            nudDependants.Value = objMedicalCategoryMaster._Depandants_No
            txtAmount.Text = objMedicalCategoryMaster._Amount.ToString.Trim
            txtDescription.Text = objMedicalCategoryMaster._Description
            txtAmount.Text = CStr(Format(objMedicalCategoryMaster._Amount, objCurrency._fmtCurrency))


            'Pinkal (01-Oct-2014) -- Start
            'Enhancement -  Changes For FDRC Report
            cboMaritalStatus.SelectedValue = objMedicalCategoryMaster._MaritalStatusunkid
            'Pinkal (01-Oct-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objMedicalCategoryMaster._Mdcategorymasterunkid = CInt(cboCategory.SelectedValue)
            objMedicalCategoryMaster._Depandants_No = CInt(nudDependants.Value)

            If txtAmount.Text.Trim = "" Then
                txtAmount.Decimal = 0
            End If

            objMedicalCategoryMaster._Amount = txtAmount.Decimal
            objMedicalCategoryMaster._Description = txtDescription.Text.Trim

            'Pinkal (01-Oct-2014) -- Start
            'Enhancement -  Changes For FDRC Report
            objMedicalCategoryMaster._MaritalStatusunkid = CInt(cboMaritalStatus.SelectedValue)
            'Pinkal (01-Oct-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMedicalMaster As New clsmedical_master

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'Dim dsMedicalCategory As DataSet = objMedicalMaster.getListForCombo(CInt(enMedicalMasterType.Category), "Medical Category List", True)
            Dim dsMedicalCategory As DataSet = objMedicalMaster.getListForCombo(CInt(enMedicalMasterType.Medical_Category), "Medical Category List", True)
            'Pinkal (12-Oct-2011) -- End


            cboCategory.ValueMember = "mdmasterunkid"
            cboCategory.DisplayMember = "name"
            cboCategory.DataSource = dsMedicalCategory.Tables(0)


            'Pinkal (01-Oct-2014) -- Start
            'Enhancement -  Changes For FDRC Report

            Dim objMaster As New clsCommon_Master
            Dim dsList As DataSet = objMaster.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, "List")
            cboMaritalStatus.ValueMember = "masterunkid"
            cboMaritalStatus.DisplayMember = "name"
            cboMaritalStatus.DataSource = dsList.Tables(0)
            cboMaritalStatus.SelectedValue = 0
            'Pinkal (01-Oct-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            objbtnAddCategory.Enabled = User._Object.Privilege._AddMedicalMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMedicalCategory.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMedicalCategory.ForeColor = GUI._eZeeContainerHeaderForeColor 


	
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbMedicalCategory.Text = Language._Object.getCaption(Me.gbMedicalCategory.Name, Me.gbMedicalCategory.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblMaxDependants.Text = Language._Object.getCaption(Me.lblMaxDependants.Name, Me.lblMaxDependants.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Medical Category is compulsory information.Please Select Medical Category.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class