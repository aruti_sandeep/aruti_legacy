﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCoverAllocation_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCoverAllocation_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbEffectiveDate = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.gbCoverAllocation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.objbtnAddMedicalCategory = New eZee.Common.eZeeGradientButton
        Me.lblMedicalCategory = New System.Windows.Forms.Label
        Me.cboMedicalCategory = New System.Windows.Forms.ComboBox
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAdvanceSearch = New System.Windows.Forms.LinkLabel
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvEmployeeList = New System.Windows.Forms.ListView
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmpName = New System.Windows.Forms.ColumnHeader
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkInactiveEmp = New System.Windows.Forms.CheckBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbEffectiveDate.SuspendLayout()
        Me.gbCoverAllocation.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbEffectiveDate)
        Me.pnlMainInfo.Controls.Add(Me.gbCoverAllocation)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(649, 425)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbEffectiveDate
        '
        Me.gbEffectiveDate.BorderColor = System.Drawing.Color.Black
        Me.gbEffectiveDate.Checked = False
        Me.gbEffectiveDate.CollapseAllExceptThis = False
        Me.gbEffectiveDate.CollapsedHoverImage = Nothing
        Me.gbEffectiveDate.CollapsedNormalImage = Nothing
        Me.gbEffectiveDate.CollapsedPressedImage = Nothing
        Me.gbEffectiveDate.CollapseOnLoad = False
        Me.gbEffectiveDate.Controls.Add(Me.chkInactiveEmp)
        Me.gbEffectiveDate.Controls.Add(Me.lblEndDate)
        Me.gbEffectiveDate.Controls.Add(Me.lblStartDate)
        Me.gbEffectiveDate.Controls.Add(Me.dtpEndDate)
        Me.gbEffectiveDate.Controls.Add(Me.dtpStartDate)
        Me.gbEffectiveDate.ExpandedHoverImage = Nothing
        Me.gbEffectiveDate.ExpandedNormalImage = Nothing
        Me.gbEffectiveDate.ExpandedPressedImage = Nothing
        Me.gbEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEffectiveDate.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEffectiveDate.HeaderHeight = 25
        Me.gbEffectiveDate.HeaderMessage = ""
        Me.gbEffectiveDate.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEffectiveDate.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEffectiveDate.HeightOnCollapse = 0
        Me.gbEffectiveDate.LeftTextSpace = 0
        Me.gbEffectiveDate.Location = New System.Drawing.Point(3, 3)
        Me.gbEffectiveDate.Name = "gbEffectiveDate"
        Me.gbEffectiveDate.OpenHeight = 300
        Me.gbEffectiveDate.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEffectiveDate.ShowBorder = True
        Me.gbEffectiveDate.ShowCheckBox = False
        Me.gbEffectiveDate.ShowCollapseButton = False
        Me.gbEffectiveDate.ShowDefaultBorderColor = True
        Me.gbEffectiveDate.ShowDownButton = False
        Me.gbEffectiveDate.ShowHeader = True
        Me.gbEffectiveDate.Size = New System.Drawing.Size(311, 113)
        Me.gbEffectiveDate.TabIndex = 239
        Me.gbEffectiveDate.Temp = 0
        Me.gbEffectiveDate.Text = "Effective Date"
        Me.gbEffectiveDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(13, 62)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(86, 16)
        Me.lblEndDate.TabIndex = 90
        Me.lblEndDate.Text = "End Date"
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(13, 35)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(86, 16)
        Me.lblStartDate.TabIndex = 89
        Me.lblStartDate.Text = "Start Date"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(105, 60)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpEndDate.TabIndex = 88
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(105, 33)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpStartDate.TabIndex = 87
        '
        'gbCoverAllocation
        '
        Me.gbCoverAllocation.BorderColor = System.Drawing.Color.Black
        Me.gbCoverAllocation.Checked = False
        Me.gbCoverAllocation.CollapseAllExceptThis = False
        Me.gbCoverAllocation.CollapsedHoverImage = Nothing
        Me.gbCoverAllocation.CollapsedNormalImage = Nothing
        Me.gbCoverAllocation.CollapsedPressedImage = Nothing
        Me.gbCoverAllocation.CollapseOnLoad = False
        Me.gbCoverAllocation.Controls.Add(Me.txtRemark)
        Me.gbCoverAllocation.Controls.Add(Me.lblRemark)
        Me.gbCoverAllocation.Controls.Add(Me.objbtnAddMedicalCategory)
        Me.gbCoverAllocation.Controls.Add(Me.lblMedicalCategory)
        Me.gbCoverAllocation.Controls.Add(Me.cboMedicalCategory)
        Me.gbCoverAllocation.ExpandedHoverImage = Nothing
        Me.gbCoverAllocation.ExpandedNormalImage = Nothing
        Me.gbCoverAllocation.ExpandedPressedImage = Nothing
        Me.gbCoverAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCoverAllocation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCoverAllocation.HeaderHeight = 25
        Me.gbCoverAllocation.HeaderMessage = ""
        Me.gbCoverAllocation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCoverAllocation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCoverAllocation.HeightOnCollapse = 0
        Me.gbCoverAllocation.LeftTextSpace = 0
        Me.gbCoverAllocation.Location = New System.Drawing.Point(317, 3)
        Me.gbCoverAllocation.Name = "gbCoverAllocation"
        Me.gbCoverAllocation.OpenHeight = 300
        Me.gbCoverAllocation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCoverAllocation.ShowBorder = True
        Me.gbCoverAllocation.ShowCheckBox = False
        Me.gbCoverAllocation.ShowCollapseButton = False
        Me.gbCoverAllocation.ShowDefaultBorderColor = True
        Me.gbCoverAllocation.ShowDownButton = False
        Me.gbCoverAllocation.ShowHeader = True
        Me.gbCoverAllocation.Size = New System.Drawing.Size(325, 363)
        Me.gbCoverAllocation.TabIndex = 238
        Me.gbCoverAllocation.Temp = 0
        Me.gbCoverAllocation.Text = "Cover Allocation Info"
        Me.gbCoverAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(4, 82)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(317, 279)
        Me.txtRemark.TabIndex = 240
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(7, 62)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(109, 14)
        Me.lblRemark.TabIndex = 239
        Me.lblRemark.Text = "Remark"
        '
        'objbtnAddMedicalCategory
        '
        Me.objbtnAddMedicalCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddMedicalCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddMedicalCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddMedicalCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddMedicalCategory.BorderSelected = False
        Me.objbtnAddMedicalCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddMedicalCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddMedicalCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddMedicalCategory.Location = New System.Drawing.Point(304, 33)
        Me.objbtnAddMedicalCategory.Name = "objbtnAddMedicalCategory"
        Me.objbtnAddMedicalCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddMedicalCategory.TabIndex = 238
        '
        'lblMedicalCategory
        '
        Me.lblMedicalCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalCategory.Location = New System.Drawing.Point(7, 35)
        Me.lblMedicalCategory.Name = "lblMedicalCategory"
        Me.lblMedicalCategory.Size = New System.Drawing.Size(99, 14)
        Me.lblMedicalCategory.TabIndex = 1
        Me.lblMedicalCategory.Text = "Medical Category"
        '
        'cboMedicalCategory
        '
        Me.cboMedicalCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedicalCategory.DropDownWidth = 150
        Me.cboMedicalCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedicalCategory.FormattingEnabled = True
        Me.cboMedicalCategory.Location = New System.Drawing.Point(112, 33)
        Me.cboMedicalCategory.Name = "cboMedicalCategory"
        Me.cboMedicalCategory.Size = New System.Drawing.Size(186, 21)
        Me.cboMedicalCategory.TabIndex = 237
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeList.Controls.Add(Me.lnkAdvanceSearch)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(3, 120)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(311, 246)
        Me.gbEmployeeList.TabIndex = 2
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAdvanceSearch
        '
        Me.lnkAdvanceSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAdvanceSearch.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAdvanceSearch.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAdvanceSearch.Location = New System.Drawing.Point(162, 4)
        Me.lnkAdvanceSearch.Name = "lnkAdvanceSearch"
        Me.lnkAdvanceSearch.Size = New System.Drawing.Size(90, 17)
        Me.lnkAdvanceSearch.TabIndex = 85
        Me.lnkAdvanceSearch.TabStop = True
        Me.lnkAdvanceSearch.Text = "Advance Search"
        Me.lnkAdvanceSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvEmployeeList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(306, 218)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(6, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvEmployeeList
        '
        Me.lvEmployeeList.CheckBoxes = True
        Me.lvEmployeeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhEmpCode, Me.colhEmpName})
        Me.lvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmployeeList.FullRowSelect = True
        Me.lvEmployeeList.GridLines = True
        Me.lvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmployeeList.Name = "lvEmployeeList"
        Me.lvEmployeeList.Size = New System.Drawing.Size(306, 218)
        Me.lvEmployeeList.TabIndex = 0
        Me.lvEmployeeList.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeList.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 25
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Tag = "colhEmpCode"
        Me.colhEmpCode.Text = "Code"
        Me.colhEmpCode.Width = 65
        '
        'colhEmpName
        '
        Me.colhEmpName.Tag = "colhEmpName"
        Me.colhEmpName.Text = "Name"
        Me.colhEmpName.Width = 200
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(262, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 70
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 370)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(649, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(437, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(540, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 11
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'chkInactiveEmp
        '
        Me.chkInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveEmp.Location = New System.Drawing.Point(105, 90)
        Me.chkInactiveEmp.Name = "chkInactiveEmp"
        Me.chkInactiveEmp.Size = New System.Drawing.Size(188, 16)
        Me.chkInactiveEmp.TabIndex = 19
        Me.chkInactiveEmp.Text = "Include Inactive Employee"
        Me.chkInactiveEmp.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(287, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 87
        Me.objbtnReset.TabStop = False
        '
        'frmCoverAllocation_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(649, 396)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCoverAllocation_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Medical Cover Allocation"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbEffectiveDate.ResumeLayout(False)
        Me.gbCoverAllocation.ResumeLayout(False)
        Me.gbCoverAllocation.PerformLayout()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbCoverAllocation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvEmployeeList As System.Windows.Forms.ListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblMedicalCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnAddMedicalCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMedicalCategory As System.Windows.Forms.ComboBox
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAdvanceSearch As System.Windows.Forms.LinkLabel
    Friend WithEvents gbEffectiveDate As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkInactiveEmp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
End Class
