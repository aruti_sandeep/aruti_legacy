﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmpAssetDeclaration

#Region " Private Variables "

    Private mstrModuleName As String = "frmEmpAssetDeclaration"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objAssetDeclare As clsAssetdeclaration_master
    Private mintAssetDeclarationUnkid As Integer = -1
    'Private mblnIsSelfAssessment As Boolean = False
    'Private mintEmplId As Integer = 0
    'Private mintYearId As Integer = 0
    'Private mintPeriodId As Integer = 0
    'Private mintAssessorId As Integer = 0

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Dim mintBaseCurrencuUnkId As Integer = 0
    Dim mstrBaseCurrSign As String = ""
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    'Sohail (29 Jan 2013) -- End

    Private mdtBank As DataTable
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, ByVal blnIsSelf As Boolean) As Boolean
        Try
            mintAssetDeclarationUnkid = intUnkid
            menAction = eAction
            'mblnIsSelfAssessment = blnIsSelf
            'Select Case mblnIsSelfAssessment
            '    Case True
            '        Me.Name = "frmSelfAssessment"
            '        mstrModuleName = Me.Name
            '    Case False
            '        Me.Name = "frmAssessorAssessment"
            '        mstrModuleName = Me.Name
            'End Select
            Me.ShowDialog()
            'intUnkid = mintAssessAnalysisUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            'objbtnAddItems.Enabled = User._Object.Privilege._AddAssessmentItem
            'objbtnAddResult.Enabled = User._Object.Privilege._AddResultCode
            'objbtnAddGroup.Enabled = User._Object.Privilege._AddAssessmentGroup
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboEmployee_Step1.BackColor = GUI.ColorComp
            txtCash_Step2.BackColor = GUI.ColorOptional
            txtCashExistingBank_Step2.BackColor = GUI.ColorOptional
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'txtResources_step5.BackColor = GUI.ColorOptional
            'txtDebt_step5.BackColor = GUI.ColorOptional
            'Sohail (29 Jan 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'cboYear.SelectedValue = objAssessMaster._Yearunkid
            'cboPeriod.SelectedValue = objAssessMaster._Periodunkid
            'cboAssessGroup.SelectedValue = objAssessMaster._Assessgroupunkid
            'cboAssessItem.SelectedValue = objAssessMaster._Assessitemunkid
            'txtImprovement.Text = objAssessMaster._Improvement
            'cboResultCode.SelectedValue = objAssessMaster._Resultcodeunkid
            'txtStrength.Text = objAssessMaster._Strength
            'txtTrainingNeeds.Text = objAssessMaster._Trainingneed
            'txtRemark.Text = objAssessMaster._Remark
            'If objAssessMaster._Assessmentdate <> Nothing Then
            '    dtpAssessdate.Value = objAssessMaster._Assessmentdate
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAssetDeclare._Employeeunkid = CInt(cboEmployee_Step1.SelectedValue)
            objAssetDeclare._Cash = txtCash_Step2.Decimal
            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'objAssetDeclare._CashExistingBank = txtCashExistingBank_Step2.Text.Trim
            objAssetDeclare._CashExistingBank = txtCashExistingBank_Step2.Decimal
            'Sohail (26 Mar 2012) -- End
            objAssetDeclare._Finyear_Start = dtpFinYearStart_Step1.Value
            objAssetDeclare._Finyear_End = dtpFinYearEnd_Step1.Value
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'objAssetDeclare._Resources = txtResources_step5.Text.Trim
            'objAssetDeclare._Debt = txtDebt_step5.Text.Trim
            objAssetDeclare._Savedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Finalsavedate = Nothing
            objAssetDeclare._Unlockfinalsavedate = Nothing
            'Sohail (29 Jan 2013) -- End
            objAssetDeclare._Userunkid = User._Object._Userunkid
            objAssetDeclare._Isvoid = False
            objAssetDeclare._Voiddatetime = Nothing
            objAssetDeclare._Voidreason = ""
            objAssetDeclare._Voiduserunkid = -1
            objAssetDeclare._TransactionDate = dtpDate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objExchange As New clsExchangeRate 'Sohail (06 Apr 2012)

        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              FinancialYear._Object._Database_Start_Date.Date, _
                                              FinancialYear._Object._Database_End_Date.Date, _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee_Step1
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT

            'Bank
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyBank
                .DataSource = dsCombos.Tables("Currency")
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (29 Jan 2013) -- End
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            'Share Dividend
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyShare
                .DataSource = dsCombos.Tables("Currency")
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (29 Jan 2013) -- End
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            'House
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyHouse
                .DataSource = dsCombos.Tables("Currency")
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (29 Jan 2013) -- End
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            'Park
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyPark
                .DataSource = dsCombos.Tables("Currency")
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (29 Jan 2013) -- End
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            'Vehicle
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyVehicle
                .DataSource = dsCombos.Tables("Currency")
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (29 Jan 2013) -- End
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            'Machinery
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyMachine
                .DataSource = dsCombos.Tables("Currency")
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (29 Jan 2013) -- End
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            'Other Business
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyBusiness
                .DataSource = dsCombos.Tables("Currency")
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (29 Jan 2013) -- End
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With
            'Sohail (06 Apr 2012) -- End

            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'Machinery
            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyResources
                .DataSource = dsCombos.Tables("Currency")
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            dsCombos = objExchange.getComboList("Currency", True)
            With colhCurrencyDebts
                .DataSource = dsCombos.Tables("Currency")
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            dsCombos = objExchange.getComboList("BaseCurr", False, True)
            If dsCombos.Tables("BaseCurr").Rows.Count > 0 Then
                mintBaseCurrencuUnkId = CInt(dsCombos.Tables("BaseCurr").Rows(0).Item("exchangerateunkid"))
                mstrBaseCurrSign = dsCombos.Tables("BaseCurr").Rows(0).Item("currency_sign").ToString
            End If
            'Sohail (29 Jan 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            objEmp = Nothing : objMaster = Nothing
            objExchange = Nothing 'Sohail (06 Apr 2012)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
    End Function

    Private Sub FillBankGrid()
        Dim objAssetBank As New clsAsset_bank_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetBank._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetBank._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetBank._Datasource

            With dgvBank_Step2
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhBank_step3.DataPropertyName = "bank"
                colhAccount_step3.DataPropertyName = "account"
                colhAmount_step3.DataPropertyName = "amount"
                colhAmount_step3.DefaultCellStyle.Format = GUI.fmtCurrency
                colhServant_step3.DataPropertyName = "servant"
                colhWifeHusband_step3.DataPropertyName = "wifehusband"
                colhChildren_step3.DataPropertyName = "children"
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'colhCurrencyBank.DataPropertyName = "currencyunkid" 'Sohail (06 Apr 2012)
                colhCurrencyBank.DataPropertyName = "countryunkid"
                'Sohail (29 Jan 2013) -- End

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBankGrid", mstrModuleName)
        Finally
            objAssetBank = Nothing
        End Try
    End Sub

    Private Sub FillShareDividendGrid()
        Dim objAssetShareDividend As New clsAsset_sharedividend_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetShareDividend._assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetShareDividend._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetShareDividend._Datasource

            With dgvShareDividend_Step2
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhShareValue_step3.DataPropertyName = "sharevalue"
                colhShareValue_step3.DefaultCellStyle.Format = GUI.fmtCurrency
                colhLocation_step3.DataPropertyName = "location"
                colhDividendAmount_step3.DataPropertyName = "dividendamount"
                colhDividendAmount_step3.DefaultCellStyle.Format = GUI.fmtCurrency
                colhServant_step3Shares.DataPropertyName = "servant"
                colhWifeHusband_step3Shares.DataPropertyName = "wifehusband"
                colhChildren_step3Shares.DataPropertyName = "children"
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'colhCurrencyShare.DataPropertyName = "currencyunkid" 'Sohail (06 Apr 2012)
                colhCurrencyShare.DataPropertyName = "countryunkid"
                'Sohail (29 Jan 2013) -- End

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillShareDividendGrid", mstrModuleName)
        Finally
            objAssetShareDividend = Nothing
        End Try
    End Sub

    Private Sub FillHouseBuildingGrid()
        Dim objAssetHouseBuilding As New clsAsset_housebuilding_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetHouseBuilding._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetHouseBuilding._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetHouseBuilding._Datasource

            With dgvHouse_step3
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhHomesBuilding_step4.DataPropertyName = "homebuilding"
                colhLocation_step4.DataPropertyName = "location"
                colhValue_step4.DataPropertyName = "value"
                colhValue_step4.DefaultCellStyle.Format = GUI.fmtCurrency
                colhServant_step4House.DataPropertyName = "servant"
                colhWifeHusband_step4House.DataPropertyName = "wifehusband"
                colhChildren_step4House.DataPropertyName = "children"
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'colhCurrencyHouse.DataPropertyName = "currencyunkid" 'Sohail (06 Apr 2012)
                colhCurrencyHouse.DataPropertyName = "countryunkid"
                'Sohail (29 Jan 2013) -- End

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHouseBuildingGrid", mstrModuleName)
        Finally
            objAssetHouseBuilding = Nothing
        End Try
    End Sub

    Private Sub FillParkFarmMinesGrid()
        Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetParkFarm._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetParkFarm._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetParkFarm._Datasource

            With dgvParkFarm_step3
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhParkFarmMines_step4Parks.DataPropertyName = "parkfarmmines"
                colhAreaSize_step4Parks.DataPropertyName = "sizeofarea"
                colhAreaSize_step4Parks.DefaultCellStyle.Format = GUI.fmtCurrency
                colhPlaceClad_step4Parks.DataPropertyName = "placeclad"
                colhValue_step4Parks.DataPropertyName = "value"
                colhValue_step4Parks.DefaultCellStyle.Format = GUI.fmtCurrency
                colhServant_step4Parks.DataPropertyName = "servant"
                colhWifeHusband_step4Parks.DataPropertyName = "wifehusband"
                colhChildren_step4Parks.DataPropertyName = "children"
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'colhCurrencyPark.DataPropertyName = "currencyunkid" 'Sohail (06 Apr 2012)
                colhCurrencyPark.DataPropertyName = "countryunkid"
                'Sohail (29 Jan 2013) -- End

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillParkFarmMinesGrid", mstrModuleName)
        Finally
            objAssetParkFarm = Nothing
        End Try
    End Sub

    Private Sub FillVehiclesGrid()
        Dim objAssetVehicles As New clsAsset_vehicles_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetVehicles._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetVehicles._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetVehicles._Datasource

            With dgvVehicles_step4
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhCarMotorcycle_step5Vehicle.DataPropertyName = "vehicletype"
                colhValue_step5Vehicle.DataPropertyName = "value"
                colhValue_step5Vehicle.DefaultCellStyle.Format = GUI.fmtCurrency
                colhServant_step5Vehicle.DataPropertyName = "servant"
                colhWifeHusband_step5Vehicle.DataPropertyName = "wifehusband"
                colhChildren_step5Vehicle.DataPropertyName = "children"
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'colhCurrencyVehicle.DataPropertyName = "currencyunkid" 'Sohail (06 Apr 2012)
                colhCurrencyVehicle.DataPropertyName = "countryunkid"
                colhCarModel_step5Vehicle.DataPropertyName = "model"
                colhCarColour_step5Vehicle.DataPropertyName = "colour"
                colhCarRegNo_step5Vehicle.DataPropertyName = "regno"
                colhCarUse_step5Vehicle.DataPropertyName = "vehicleuse"
                colhCarAcqDate_step5Vehicle.DataPropertyName = "acquisitiondate"
                colhCarAcqDate_step5Vehicle.DefaultCellStyle.Format = "yyyy/MM/dd"
                'Sohail (29 Jan 2013) -- End

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillVehiclesGrid", mstrModuleName)
        Finally
            objAssetVehicles = Nothing
        End Try
    End Sub

    Private Sub FillMachineryGrid()
        Dim objAssetMachinery As New clsAsset_machinery_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetMachinery._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetMachinery._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetMachinery._Datasource

            With dgvMachinery_step4
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhMachine_step5Machinery.DataPropertyName = "machines"
                colhLocation_step5Machinery.DataPropertyName = "location"
                colhValue_step5Machinery.DataPropertyName = "value"
                colhValue_step5Machinery.DefaultCellStyle.Format = GUI.fmtCurrency
                colhServant_step5Machinery.DataPropertyName = "servant"
                colhWifeHusband_step5Machinery.DataPropertyName = "wifehusband"
                colhChildren_step5Machinery.DataPropertyName = "children"
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'colhCurrencyMachine.DataPropertyName = "currencyunkid" 'Sohail (06 Apr 2012)
                colhCurrencyMachine.DataPropertyName = "countryunkid"
                'Sohail (29 Jan 2013) -- End

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMachineryGrid", mstrModuleName)
        Finally
            objAssetMachinery = Nothing
        End Try
    End Sub

    Private Sub FillOtherBusinessGrid()
        Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objAssetOtherBusiness._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetOtherBusiness._Datasource

            With dgvOtherBusiness_step5
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhBusinessType_step6OtherBusiness.DataPropertyName = "businesstype"
                colhPlaceClad_step6OtherBusiness.DataPropertyName = "placeclad"
                colhValue_step6OtherBusiness.DataPropertyName = "value"
                colhValue_step6OtherBusiness.DefaultCellStyle.Format = GUI.fmtCurrency
                colhServant_step6OtherBusiness.DataPropertyName = "servant"
                colhWifeHusband_step6OtherBusiness.DataPropertyName = "wifehusband"
                colhChildren_step6OtherBusiness.DataPropertyName = "children"
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'colhCurrencyBusiness.DataPropertyName = "currencyunkid" 'Sohail (06 Apr 2012)
                colhCurrencyBusiness.DataPropertyName = "countryunkid"
                'Sohail (29 Jan 2013) -- End

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillOtherBusinessGrid", mstrModuleName)
        Finally
            objAssetOtherBusiness = Nothing
        End Try
    End Sub

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    '*** SelectedValueChanged event for Datagridview Combobox column.
    Public Sub combobox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try

            Dim combobox As ComboBox
            combobox = CType(sender, ComboBox)

            objlblExchangeRate.Text = ""
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            Dim SelValue As Integer = 0
            If combobox.SelectedValue IsNot Nothing AndAlso TypeOf (combobox.SelectedValue) Is Integer AndAlso CInt(combobox.SelectedIndex) > 0 Then
                If TypeOf (combobox.SelectedValue) Is Integer Then
                    SelValue = CInt(combobox.SelectedValue)
                ElseIf TypeOf (combobox.SelectedValue) Is DataRowView Then
                    SelValue = CInt(CType(combobox.SelectedValue, System.Data.DataRowView).Item("countryunkid"))
                End If

                dsList = objExRate.GetList("ExRate", True, , , SelValue, True, ConfigParameter._Object._CurrentDateAndTime.Date, True)
                dtTable = New DataView(dsList.Tables("ExRate")).ToTable
                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    objlblExchangeRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "combobox_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub FillResourcesGrid()
        Dim objAssetResources As New clsAsset_otherresources_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetResources._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetResources._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetResources._Datasource

            With dgvOtherResources_step5
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhResources_step6Resources.DataPropertyName = "otherresources"
                colhLocation_step6Resources.DataPropertyName = "location"
                colhValue_step6Resources.DataPropertyName = "value"
                colhValue_step6Resources.DefaultCellStyle.Format = GUI.fmtCurrency
                colhEmployee_step6Resources.DataPropertyName = "servant"
                colhWifeHusband_step6Resources.DataPropertyName = "wifehusband"
                colhChildren_step6Resources.DataPropertyName = "children"
                colhCurrencyResources.DataPropertyName = "countryunkid"

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillResourcesGrid", mstrModuleName)
        Finally
            objAssetResources = Nothing
        End Try
    End Sub

    Private Sub FillDebtGrid()
        Dim objAssetDebt As New clsAsset_debts_tran
        Dim dtTable As DataTable
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDebt._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetDebt._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
            'Shani(24-Aug-2015) -- End
            dtTable = objAssetDebt._Datasource

            With dgvDebts_step6
                .AutoGenerateColumns = False
                .DataSource = dtTable

                colhDebts_step6Debts.DataPropertyName = "debts"
                colhLocation_step6Debts.DataPropertyName = "location"
                colhValue_step6Debts.DataPropertyName = "value"
                colhValue_step6Debts.DefaultCellStyle.Format = GUI.fmtCurrency
                colhEmployee_step6Debts.DataPropertyName = "servant"
                colhWifeHusband_step6Debts.DataPropertyName = "wifehusband"
                colhChildren_step6Debts.DataPropertyName = "children"
                colhCurrencyDebts.DataPropertyName = "countryunkid"

                .Refresh()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDebtGrid", mstrModuleName)
        Finally
            objAssetDebt = Nothing
        End Try
    End Sub

    Private Sub SetBankTotal()
        Try
            Dim decBankTotal As Decimal = 0
            For Each dtRow As DataRow In CType(dgvBank_Step2.DataSource, DataTable).Rows
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                'If dtRow.Cells(colhAmount_step3.Index).Value IsNot Nothing Then
                'decBankTotal += CDec(dtRow.Item("baseamount"))
                'End If
                If dtRow.RowState = DataRowState.Added Or dtRow.RowState = DataRowState.Modified Or dtRow.RowState = DataRowState.Unchanged Then
                    decBankTotal += CDec(dtRow.Item("baseamount"))
                End If
                'S.SANDEEP [28-May-2018] -- END
            Next

            txtCashExistingBank_Step2.Text = decBankTotal.ToString(GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetBankTotal", mstrModuleName)
        End Try
    End Sub

    Private Sub Update_DataGridview_DataTable_Extra_Columns(ByVal objDataGridView As DataGridView, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Try
            If objDataGridView.Rows(e.RowIndex).IsNewRow = True Then Exit Try

            CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("userunkid") = User._Object._Userunkid
            If CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("transactiondate") Is Nothing OrElse IsDBNull(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("transactiondate")) Then
                CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
            End If

            Dim objExRate As New clsExchangeRate
            Dim dsList As DataSet
            Dim decBaseRate As Decimal = 0
            Dim decPaidRate As Decimal = 0
            Dim decPaidAmt As Decimal = 0
            Dim decPaidDividendAmt As Decimal = 0
            dsList = objExRate.GetList("ExRate", True, , , CInt(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("countryunkid")), True, CDate(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("transactiondate")), True)
            If dsList.Tables("ExRate").Rows.Count > 0 Then
                CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("currencyunkid") = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("basecurrencyid") = mintBaseCurrencuUnkId
                decBaseRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                decPaidRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                If objDataGridView.Name = dgvBank_Step2.Name Then
                    'Sohail (13 May 2013) -- Start
                    'TRA - ENHANCEMENT
                    'decPaidAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("amount"))
                    If IsDBNull(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("amount")) = True Then
                        decPaidAmt = 0
                    Else
                        decPaidAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("amount"))
                    End If
                    'Sohail (13 May 2013) -- End
                ElseIf objDataGridView.Name = dgvShareDividend_Step2.Name Then
                    'Sohail (13 May 2013) -- Start
                    'TRA - ENHANCEMENT
                    'decPaidAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("sharevalue"))
                    'decPaidDividendAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("dividendamount"))
                    If IsDBNull(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("sharevalue")) = True Then
                        decPaidAmt = 1
                    Else
                        decPaidAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("sharevalue"))
                    End If
                    If IsDBNull(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("dividendamount")) = True Then
                        decPaidDividendAmt = 0
                    Else
                        decPaidDividendAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("dividendamount"))
                    End If
                    'Sohail (13 May 2013) -- End
                Else
                    'Sohail (13 May 2013) -- Start
                    'TRA - ENHANCEMENT
                    'decPaidAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("value"))
                    If IsDBNull(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("value")) = True Then
                        decPaidAmt = 0
                    Else
                        decPaidAmt = CDec(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("value"))
                    End If
                    'Sohail (13 May 2013) -- End
                End If

                CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("baseexchangerate") = decBaseRate
                CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("expaidrate") = decPaidRate
                If decPaidRate <> 0 Then
                    CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("baseamount") = decPaidAmt * decBaseRate / decPaidRate
                    If objDataGridView.Name = dgvShareDividend_Step2.Name Then
                        CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("basedividendamount") = decPaidDividendAmt * decBaseRate / decPaidRate
                    End If
                Else
                    CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("baseamount") = decPaidAmt * decBaseRate
                    If objDataGridView.Name = dgvShareDividend_Step2.Name Then
                        CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex).Item("basedividendamount") = decPaidDividendAmt * decBaseRate
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_DataGridview_DataTable_Extra_Columns", mstrModuleName)
        End Try
    End Sub

    Private Sub Update_DataGridview_DataTable_AUD(ByVal objDataGridView As DataGridView, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs, ByVal strChildUnkIdColName As String)
        Try
            If menAction = enAction.EDIT_ONE AndAlso objDataGridView.DataSource IsNot Nothing Then
                objDataGridView.UpdateCellValue(e.ColumnIndex, e.RowIndex)
                If e.RowIndex < CType(objDataGridView.DataSource, DataTable).Rows.Count AndAlso CInt(CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex)(strChildUnkIdColName)) > 0 Then
                    CType(objDataGridView.DataSource, DataTable).Rows(e.RowIndex)("AUD") = "U"
                    CType(objDataGridView.DataSource, DataTable).AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_DataGridview_DataTable_AUD", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End
#End Region

#Region " Form's Events "

    Private Sub frmEmpAssetDeclaration_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            'objAssessMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpAssetDeclaration_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpAssetDeclaration_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                'SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpAssetDeclaration_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpAssetDeclaration_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAssetDeclare = New clsAssetdeclaration_master

        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetDeclare._Assetdeclarationunkid = mintAssetDeclarationUnkid
                objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = mintAssetDeclarationUnkid
                'Shani(24-Aug-2015) -- End
                cboEmployee_Step1.SelectedValue = objAssetDeclare._Employeeunkid
                cboEmployee_Step1.Enabled = False
                objbtnSearchEmployee.Enabled = False
                txtCash_Step2.Text = objAssetDeclare._Cash.ToString(GUI.fmtCurrency)
                'Sohail (26 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'txtCashExistingBank_Step2.Text = objAssetDeclare._CashExistingBank
                txtCashExistingBank_Step2.Text = objAssetDeclare._CashExistingBank.ToString(GUI.fmtCurrency)
                'Sohail (26 Mar 2012) -- End
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'txtResources_step5.Text = objAssetDeclare._Resources
                'txtDebt_step5.Text = objAssetDeclare._Debt
                'Sohail (29 Jan 2013) -- End
                dtpDate.Value = objAssetDeclare._TransactionDate 'Sohail (06 Apr 2012)
            End If
            Call GetValue()
            txtADInstruction.Text = ConfigParameter._Object._AssetDeclarationInstruction 'Sohail (06 Apr 2012)

            Call FillBankGrid()
            Call FillShareDividendGrid()
            Call FillHouseBuildingGrid()
            Call FillParkFarmMinesGrid()
            Call FillVehiclesGrid()
            Call FillMachineryGrid()
            Call FillOtherBusinessGrid()
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            Call FillResourcesGrid()
            Call FillDebtGrid()
            objlblExchangeRate.Text = ""
            'Sohail (29 Jan 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpAssetDeclaration_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Combobox's Events "
    Private Sub cboEmployee_Step2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee_Step1.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Try

            dtpFinYearStart_Step1.MaxDate = CDate("01/Jan/8888")
            dtpFinYearStart_Step1.MinDate = CDate("01/Jan/1900")

            dtpFinYearEnd_Step1.MaxDate = CDate("01/Jan/8888")
            dtpFinYearEnd_Step1.MinDate = CDate("01/Jan/1900")

            If CInt(cboEmployee_Step1.SelectedValue) > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(cboEmployee_Step1.SelectedValue)
                objEmployee._Employeeunkid(FinancialYear._Object._Database_End_Date.Date) = CInt(cboEmployee_Step1.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END


                txtNoOfEmployment_Step1.Text = objEmployee._Employeecode

                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmployee._Departmentunkid
                txtCenterforWork_Step1.Text = objDept._Name
                objDept = Nothing

                Dim objJob As New clsJobs
                objJob._Jobunkid = objEmployee._Jobunkid
                txtOfficePosition_Step1.Text = objJob._Job_Name
                objJob = Nothing

                'Sohail (02 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                txtWorkStation_Step1.Text = objClass._Name
                objClass = Nothing
                'Sohail (02 Apr 2012) -- End
                '--------------------------------------------------------------------------
                If objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
                    dtpFinYearStart_Step1.MaxDate = objEmployee._Appointeddate.Date
                    dtpFinYearStart_Step1.MinDate = objEmployee._Appointeddate.Date
                Else
                    dtpFinYearStart_Step1.MaxDate = FinancialYear._Object._Database_Start_Date.Date
                    dtpFinYearStart_Step1.MinDate = FinancialYear._Object._Database_Start_Date.Date
                End If

                Dim EndDate As Date = FinancialYear._Object._Database_End_Date.Date
                If objEmployee._Termination_From_Date.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Termination_From_Date.Date < FinancialYear._Object._Database_End_Date.Date, objEmployee._Termination_From_Date.Date, FinancialYear._Object._Database_End_Date.Date))
                End If
                If objEmployee._Termination_To_Date.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Termination_To_Date.Date < EndDate, objEmployee._Termination_To_Date.Date, EndDate))
                End If
                'Sohail (16 Apr 2015) -- Start
                'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
                If objEmployee._Empl_Enddate.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Empl_Enddate.Date < EndDate, objEmployee._Empl_Enddate.Date, EndDate))
                End If
                'Sohail (16 Apr 2015) -- End

                If EndDate <> FinancialYear._Object._Database_End_Date.Date Then
                    dtpFinYearEnd_Step1.MaxDate = EndDate
                    dtpFinYearEnd_Step1.MinDate = EndDate
                Else
                    dtpFinYearEnd_Step1.MaxDate = FinancialYear._Object._Database_End_Date.Date
                    dtpFinYearEnd_Step1.MinDate = FinancialYear._Object._Database_End_Date.Date
                End If

            Else
                txtNoOfEmployment_Step1.Text = ""
                txtCenterforWork_Step1.Text = ""
                txtOfficePosition_Step1.Text = ""
                txtWorkStation_Step1.Text = "" 'Sohail (02 Apr 2012)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Step2_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "

#End Region

#Region " Datagrid's Events "

#Region " Bank "

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvBank_Step2_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBank_Step2.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvBank_Step2, e, "assetbanktranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

    Private Sub dgvBank_Step2_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBank_Step2.DataError
        Try
            If e.ColumnIndex = dgvBank_Step2.Columns(colhAmount_step3.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBank_Step2_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBank_Step2.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox 'Sohail (29 Jan 2013)
        Try
            If dgvBank_Step2.CurrentCell.ColumnIndex = colhAmount_step3.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvBank_Step2.CurrentCell.ColumnIndex = colhCurrencyBank.Index AndAlso TypeOf e.Control Is ComboBox Then
                'Nothing
                'Sohail (06 Apr 2012) -- End

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                'Sohail (29 Jan 2013) -- End

            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBank_Step2_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBank_Step2.RowValidating
        Try
            Dim decAmount As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvBank_Step2.Rows(e.RowIndex).IsNewRow = True Then Exit Try 'Sohail (29 Jan 2013)

            Decimal.TryParse(dgvBank_Step2.Rows(e.RowIndex).Cells(colhAmount_step3.Index).Value.ToString, decAmount)
            Decimal.TryParse(dgvBank_Step2.Rows(e.RowIndex).Cells(colhServant_step3.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvBank_Step2.Rows(e.RowIndex).Cells(colhWifeHusband_step3.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvBank_Step2.Rows(e.RowIndex).Cells(colhChildren_step3.Index).Value.ToString, decChildren)

            If dgvBank_Step2.Rows(e.RowIndex).Cells(colhBank_step3.Index).Value.ToString.Trim = "" AndAlso _
                       dgvBank_Step2.Rows(e.RowIndex).Cells(colhAccount_step3.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvBank_Step2.Rows(e.RowIndex).Cells(colhCurrencyBank.Index).Value) <= 0 AndAlso _
                       decAmount = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvBank_Step2.CurrentRow.Index = dgvBank_Step2.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvBank_Step2.Rows(e.RowIndex).Cells(colhBank_step3.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvBank_Step2.Rows(e.RowIndex).Cells(colhAccount_step3.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(dgvBank_Step2.Rows(e.RowIndex).Cells(colhCurrencyBank.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- End
            Else

                If decAmount = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_RowValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvBank_Step2_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBank_Step2.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyBank.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_CellEnter", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvBank_Step2_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBank_Step2.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvBank_Step2, e)

            Call SetBankTotal()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_RowValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBank_Step2_UserDeletedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles dgvBank_Step2.UserDeletedRow
        Try
            Call SetBankTotal()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_UserDeletedRow", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBank_Step2_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles dgvBank_Step2.RowsRemoved
        Try
            If dgvBank_Step2.Rows.Count = 1 Then
                Call SetBankTotal()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBank_Step2_RowsRemoved", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

#End Region

#Region " Share Dividend "

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvShareDividend_Step2_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvShareDividend_Step2.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvShareDividend_Step2, e, "assetsharedividendtranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvShareDividend_Step2_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

    Private Sub dgvShareDividend_Step2_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvShareDividend_Step2.DataError
        Try
            If e.ColumnIndex = dgvShareDividend_Step2.Columns(colhDividendAmount_step3.Index).Index OrElse e.ColumnIndex = dgvShareDividend_Step2.Columns(colhShareValue_step3.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvShareDividend_Step2_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvShareDividend_Step2_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvShareDividend_Step2.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox 'Sohail (29 Jan 2013)
        Try
            If (dgvShareDividend_Step2.CurrentCell.ColumnIndex = colhDividendAmount_step3.Index OrElse dgvShareDividend_Step2.CurrentCell.ColumnIndex = colhShareValue_step3.Index) AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvShareDividend_Step2.CurrentCell.ColumnIndex = colhCurrencyShare.Index Then
                'Nothing
                'Sohail (06 Apr 2012) -- End

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                'Sohail (29 Jan 2013) -- End
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvShareDividend_Step2_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvShareDividend_Step2_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvShareDividend_Step2.RowValidating
        Try
            Dim decAmount As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvShareDividend_Step2.Rows(e.RowIndex).IsNewRow = True Then Exit Try 'Sohail (29 Jan 2013)

            Decimal.TryParse(dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhDividendAmount_step3.Index).Value.ToString, decAmount)
            Decimal.TryParse(dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhServant_step3Shares.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhWifeHusband_step3Shares.Index).Value.ToString, decWifeHusband)
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Decimal.TryParse(dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhChildren_step3.Index).Value.ToString, decChildren)
            Decimal.TryParse(dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhChildren_step3Shares.Index).Value.ToString, decChildren)
            'Sohail (06 Apr 2012) -- End

            If dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhShareValue_step3.Index).Value.ToString.Trim = "" AndAlso _
                       dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhLocation_step3.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhCurrencyShare.Index).Value) <= 0 AndAlso _
                       decAmount = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvShareDividend_Step2.CurrentRow.Index = dgvShareDividend_Step2.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhShareValue_step3.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhLocation_step3.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(dgvShareDividend_Step2.Rows(e.RowIndex).Cells(colhCurrencyShare.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- End
            Else

                If decAmount = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvShareDividend_Step2_RowValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvShareDividend_Step2_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvShareDividend_Step2.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyShare.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvShareDividend_Step2_CellEnter", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvShareDividend_Step2_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvShareDividend_Step2.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvShareDividend_Step2, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvShareDividend_Step2_RowValidated", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End
#End Region

#Region " House Building "

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvHouse_step3_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHouse_step3.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvHouse_step3, e, "assethousebuildingtranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHouse_step3_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

    Private Sub dgvHouse_step3_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvHouse_step3.DataError
        Try
            If e.ColumnIndex = dgvHouse_step3.Columns(colhValue_step4.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHouse_step3_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvHouse_step3_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvHouse_step3.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox 'Sohail (29 Jan 2013)
        Try
            If dgvHouse_step3.CurrentCell.ColumnIndex = colhValue_step4.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvHouse_step3.CurrentCell.ColumnIndex = colhCurrencyHouse.Index Then
                'Nothing
                'Sohail (06 Apr 2012) -- End

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                'Sohail (29 Jan 2013) -- End
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHouse_step3_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvHouse_step3_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvHouse_step3.RowValidating
        Try
            Dim decValue As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvHouse_step3.Rows(e.RowIndex).IsNewRow = True Then Exit Try 'Sohail (29 Jan 2013)

            Decimal.TryParse(dgvHouse_step3.Rows(e.RowIndex).Cells(colhValue_step4.Index).Value.ToString, decValue)
            Decimal.TryParse(dgvHouse_step3.Rows(e.RowIndex).Cells(colhServant_step4House.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvHouse_step3.Rows(e.RowIndex).Cells(colhWifeHusband_step4House.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvHouse_step3.Rows(e.RowIndex).Cells(colhChildren_step4House.Index).Value.ToString, decChildren)

            If dgvHouse_step3.Rows(e.RowIndex).Cells(colhHomesBuilding_step4.Index).Value.ToString.Trim = "" AndAlso _
                       dgvHouse_step3.Rows(e.RowIndex).Cells(colhLocation_step4.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvHouse_step3.Rows(e.RowIndex).Cells(colhCurrencyHouse.Index).Value) <= 0 AndAlso _
                       decValue = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvHouse_step3.CurrentRow.Index = dgvHouse_step3.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvHouse_step3.Rows(e.RowIndex).Cells(colhHomesBuilding_step4.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvHouse_step3.Rows(e.RowIndex).Cells(colhLocation_step4.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(dgvHouse_step3.Rows(e.RowIndex).Cells(colhCurrencyHouse.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- End
            Else

                If decValue = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHouse_step3_RowValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvHouse_step3_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHouse_step3.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyHouse.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHouse_step3_CellEnter", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvHouse_step3_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHouse_step3.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvHouse_step3, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHouse_step3_RowValidated", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

#End Region

#Region " Park, Farms, Mines "

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvParkFarm_step3_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvParkFarm_step3.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvParkFarm_step3, e, "assetparkfarmminestranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvParkFarm_step3_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

    Private Sub dgvParkFarm_step3_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvParkFarm_step3.DataError
        Try
            If e.ColumnIndex = dgvParkFarm_step3.Columns(colhValue_step4Parks.Index).Index OrElse e.ColumnIndex = dgvParkFarm_step3.Columns(colhAreaSize_step4Parks.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvParkFarm_step3_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvParkFarm_step3_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvParkFarm_step3.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox 'Sohail (29 Jan 2013)
        Try
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If (dgvParkFarm_step3.CurrentCell.ColumnIndex = colhValue_step4Parks.Index) AndAlso TypeOf e.Control Is TextBox Then
                'If (dgvParkFarm_step3.CurrentCell.ColumnIndex = colhValue_step4Parks.Index OrElse dgvParkFarm_step3.CurrentCell.ColumnIndex = colhAreaSize_step4Parks.Index) AndAlso TypeOf e.Control Is TextBox Then
                'Sohail (06 Apr 2012) -- End
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvParkFarm_step3.CurrentCell.ColumnIndex = colhCurrencyPark.Index Then
                'Nothing
                'Sohail (06 Apr 2012) -- End

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                'Sohail (29 Jan 2013) -- End
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvParkFarm_step3_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvParkFarm_step3_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvParkFarm_step3.RowValidating
        Try
            'Dim decAreaSize As Decimal 'Sohail (06 Apr 2012)
            Dim decValue As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvParkFarm_step3.Rows(e.RowIndex).IsNewRow = True Then Exit Try 'Sohail (29 Jan 2013)

            Decimal.TryParse(dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhValue_step4Parks.Index).Value.ToString, decValue)
            'Decimal.TryParse(dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhAreaSize_step4Parks.Index).Value.ToString, decAreaSize) 'Sohail (06 Apr 2012)
            Decimal.TryParse(dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhServant_step4Parks.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhWifeHusband_step4Parks.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhChildren_step4Parks.Index).Value.ToString, decChildren)

            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'If dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhParkFarmMines_step4Parks.Index).Value.ToString.Trim = "" AndAlso _
            '           dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhPlaceClad_step4Parks.Index).Value.ToString.Trim = "" AndAlso _
            '           decValue = 0 AndAlso _
            '           decAreaSize = 0 AndAlso _
            '           decServant = 0 AndAlso _
            '           decWifeHusband = 0 AndAlso _
            '           decChildren = 0 Then
            '    Exit Sub
            'End If
            If dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhParkFarmMines_step4Parks.Index).Value.ToString.Trim = "" AndAlso _
                       dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhPlaceClad_step4Parks.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhCurrencyPark.Index).Value) <= 0 AndAlso _
                       decValue = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvParkFarm_step3.CurrentRow.Index = dgvParkFarm_step3.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If
            'Sohail (06 Apr 2012) -- End

            If dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhParkFarmMines_step4Parks.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhPlaceClad_step4Parks.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(dgvParkFarm_step3.Rows(e.RowIndex).Cells(colhCurrencyPark.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- End
            Else

                If decValue = 0 Then
                    e.Cancel = True
                    Exit Sub
                    'Sohail (06 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'ElseIf decAreaSize = 0 Then
                    '    e.Cancel = True
                    '    Exit Sub
                    'Sohail (06 Apr 2012) -- End
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvParkFarm_step3_RowValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvParkFarm_step3_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvParkFarm_step3.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyPark.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvParkFarm_step3_CellEnter", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvParkFarm_step3_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvParkFarm_step3.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvParkFarm_step3, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvParkFarm_step3_RowValidated", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

#End Region

#Region " Vehicles "

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvVehicles_step4_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvVehicles_step4.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvVehicles_step4, e, "assetvehiclestranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvVehicles_step4_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

    Private Sub dgvVehicles_step4_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvVehicles_step4.DataError
        Try
            If e.ColumnIndex = dgvVehicles_step4.Columns(colhValue_step5Vehicle.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvVehicles_step4_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvVehicles_step4_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvVehicles_step4.EditingControlShowing
        Dim tb As TextBox
        'Sohail (29 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        Dim editingComboBox As ComboBox
        Dim masktb As MaskedTextBox
        'Sohail (29 Jan 2013) -- End
        Try
            If dgvVehicles_step4.CurrentCell.ColumnIndex = colhValue_step5Vehicle.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvVehicles_step4.CurrentCell.ColumnIndex = colhCurrencyVehicle.Index Then
                'Nothing
                'Sohail (06 Apr 2012) -- End

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged

            ElseIf dgvVehicles_step4.CurrentCell.ColumnIndex = colhCarAcqDate_step5Vehicle.Index Then
                masktb = CType(e.Control, MaskedTextBox)
                'Sohail (29 Jan 2013) -- End

            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvVehicles_step4_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvVehicles_step4_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvVehicles_step4.RowValidating
        Try
            Dim decValue As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvVehicles_step4.CurrentRow.IsNewRow Then Exit Try 'Sohail (29 Jan 2013)

            Decimal.TryParse(dgvVehicles_step4.Rows(e.RowIndex).Cells(colhValue_step5Vehicle.Index).Value.ToString, decValue)
            Decimal.TryParse(dgvVehicles_step4.Rows(e.RowIndex).Cells(colhServant_step5Vehicle.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvVehicles_step4.Rows(e.RowIndex).Cells(colhWifeHusband_step5Vehicle.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvVehicles_step4.Rows(e.RowIndex).Cells(colhChildren_step5Vehicle.Index).Value.ToString, decChildren)

            If dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarMotorcycle_step5Vehicle.Index).Value.ToString.Trim = "" AndAlso _
                       dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarModel_step5Vehicle.Index).Value.ToString.Trim = "" AndAlso _
                       dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarColour_step5Vehicle.Index).Value.ToString.Trim = "" AndAlso _
                       dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarRegNo_step5Vehicle.Index).Value.ToString.Trim = "" AndAlso _
                       dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarUse_step5Vehicle.Index).Value.ToString.Trim = "" AndAlso _
                       dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarAcqDate_step5Vehicle.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCurrencyVehicle.Index).Value) <= 0 AndAlso _
                       decValue = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvVehicles_step4.CurrentRow.Index = dgvVehicles_step4.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarMotorcycle_step5Vehicle.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarModel_step5Vehicle.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarColour_step5Vehicle.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarRegNo_step5Vehicle.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarUse_step5Vehicle.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarAcqDate_step5Vehicle.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
                'Sohail (29 Jan 2013) -- End

                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCurrencyVehicle.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- End
            Else

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                If IsDate(dgvVehicles_step4.Rows(e.RowIndex).Cells(colhCarAcqDate_step5Vehicle.Index).Value) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Enter correct Date in yyyy/MM/dd format."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    Exit Sub
                End If
                'Sohail (29 Jan 2013) -- End

                If decValue = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvVehicles_step4_RowValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvVehicles_step4_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvVehicles_step4.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyVehicle.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvVehicles_step4_CellEnter", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvVehicles_step4_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvVehicles_step4.CellValidating
        Try
            If dgvVehicles_step4.CurrentRow.IsNewRow = False AndAlso e.ColumnIndex = colhCarAcqDate_step5Vehicle.Index Then
                If IsDate(dgvVehicles_step4.CurrentCell.EditedFormattedValue) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Enter correct Date in yyyy/MM/dd format."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    Exit Sub
                ElseIf CDate(dgvVehicles_step4.CurrentCell.EditedFormattedValue) > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry! Acquisition date should not be greater than current date."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvVehicles_step4_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvVehicles_step4_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvVehicles_step4.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvVehicles_step4, e)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvVehicles_step4_RowValidated", mstrModuleName)
        End Try
    End Sub

    'Sohail (29 Jan 2013) -- End

#End Region

#Region " Machinery "

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvMachinery_step4_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMachinery_step4.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvMachinery_step4, e, "assetmachinerytranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMachinery_step4_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

    Private Sub dgvMachinery_step4_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvMachinery_step4.DataError
        Try
            If e.ColumnIndex = dgvMachinery_step4.Columns(colhValue_step5Machinery.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMachinery_step4_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMachinery_step4_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvMachinery_step4.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox 'Sohail (29 Jan 2013)
        Try
            If dgvMachinery_step4.CurrentCell.ColumnIndex = colhValue_step5Machinery.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvMachinery_step4.CurrentCell.ColumnIndex = colhCurrencyMachine.Index Then
                'Nothing
                'Sohail (06 Apr 2012) -- End

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                'Sohail (29 Jan 2013) -- End
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMachinery_step4_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMachinery_step4_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvMachinery_step4.RowValidating
        Try
            Dim decValue As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvMachinery_step4.Rows(e.RowIndex).IsNewRow = True Then Exit Try 'Sohail (29 Jan 2013)

            Decimal.TryParse(dgvMachinery_step4.Rows(e.RowIndex).Cells(colhValue_step5Machinery.Index).Value.ToString, decValue)
            Decimal.TryParse(dgvMachinery_step4.Rows(e.RowIndex).Cells(colhServant_step5Machinery.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvMachinery_step4.Rows(e.RowIndex).Cells(colhWifeHusband_step5Machinery.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvMachinery_step4.Rows(e.RowIndex).Cells(colhChildren_step5Machinery.Index).Value.ToString, decChildren)

            If dgvMachinery_step4.Rows(e.RowIndex).Cells(colhMachine_step5Machinery.Index).Value.ToString.Trim = "" AndAlso _
                       dgvMachinery_step4.Rows(e.RowIndex).Cells(colhLocation_step5Machinery.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvMachinery_step4.Rows(e.RowIndex).Cells(colhCurrencyMachine.Index).Value) <= 0 AndAlso _
                       decValue = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvMachinery_step4.CurrentRow.Index = dgvMachinery_step4.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvMachinery_step4.Rows(e.RowIndex).Cells(colhMachine_step5Machinery.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvMachinery_step4.Rows(e.RowIndex).Cells(colhLocation_step5Machinery.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(dgvMachinery_step4.Rows(e.RowIndex).Cells(colhCurrencyMachine.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- End
            Else

                If decValue = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMachinery_step4_RowValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvMachinery_step4_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMachinery_step4.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyMachine.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMachinery_step4_CellEnter", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvMachinery_step4_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMachinery_step4.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvMachinery_step4, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMachinery_step4_RowValidated", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

#End Region

#Region " Other Business "

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvOtherBusiness_step5_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOtherBusiness_step5.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvOtherBusiness_step5, e, "assetotherbusinesstranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherBusiness_step5_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End

    Private Sub dgvOtherBusiness_step5_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvOtherBusiness_step5.DataError
        Try
            If e.ColumnIndex = dgvOtherBusiness_step5.Columns(colhValue_step6OtherBusiness.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherBusiness_step5_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvOtherBusiness_step5_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvOtherBusiness_step5.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox 'Sohail (29 Jan 2013)
        Try
            If dgvOtherBusiness_step5.CurrentCell.ColumnIndex = colhValue_step6OtherBusiness.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf dgvOtherBusiness_step5.CurrentCell.ColumnIndex = colhCurrencyBusiness.Index Then
                'Nothing
                'Sohail (06 Apr 2012) -- End

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                'Sohail (29 Jan 2013) -- End
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherBusiness_step5_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvOtherBusiness_step5_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvOtherBusiness_step5.RowValidating
        Try
            Dim decValue As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvOtherBusiness_step5.Rows(e.RowIndex).IsNewRow = True Then Exit Try 'Sohail (29 Jan 2013)

            Decimal.TryParse(dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhValue_step6OtherBusiness.Index).Value.ToString, decValue)
            Decimal.TryParse(dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhServant_step6OtherBusiness.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhWifeHusband_step6OtherBusiness.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhChildren_step6OtherBusiness.Index).Value.ToString, decChildren)

            If dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhBusinessType_step6OtherBusiness.Index).Value.ToString.Trim = "" AndAlso _
                       dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhPlaceClad_step6OtherBusiness.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhCurrencyBusiness.Index).Value) <= 0 AndAlso _
                       decValue = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvOtherBusiness_step5.CurrentRow.Index = dgvOtherBusiness_step5.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhPlaceClad_step6OtherBusiness.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhPlaceClad_step6OtherBusiness.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(dgvOtherBusiness_step5.Rows(e.RowIndex).Cells(colhCurrencyBusiness.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
                'Sohail (06 Apr 2012) -- End
            Else

                If decValue = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherBusiness_step5_RowValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvOtherBusiness_step5_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOtherBusiness_step5.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyBusiness.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherBusiness_step5_CellEnter", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub dgvOtherBusiness_step5_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOtherBusiness_step5.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvOtherBusiness_step5, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherBusiness_step5_RowValidated", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Jan 2013) -- End
#End Region

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Resources "
    Private Sub dgvOtherResources_step5_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOtherResources_step5.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyResources.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherResources_step5_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvOtherResources_step5_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOtherResources_step5.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvOtherResources_step5, e, "assetotherresourcestranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherResources_step5_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvOtherResources_step5_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvOtherResources_step5.DataError
        Try
            If e.ColumnIndex = dgvOtherResources_step5.Columns(colhValue_step6Resources.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherResources_step5_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvOtherResources_step5_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvOtherResources_step5.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox
        Try
            If dgvOtherResources_step5.CurrentCell.ColumnIndex = colhValue_step6Resources.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
            ElseIf dgvOtherResources_step5.CurrentCell.ColumnIndex = colhCurrencyResources.Index Then
                'Nothing

                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherResources_step5_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvOtherResources_step5_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOtherResources_step5.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvOtherResources_step5, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherResources_step5_RowValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvOtherResources_step5_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvOtherResources_step5.RowValidating
        Try
            Dim decValue As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvOtherResources_step5.Rows(e.RowIndex).IsNewRow = True Then Exit Try

            Decimal.TryParse(dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhValue_step6Resources.Index).Value.ToString, decValue)
            Decimal.TryParse(dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhEmployee_step6Resources.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhWifeHusband_step6Resources.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhChildren_step6Resources.Index).Value.ToString, decChildren)

            If dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhResources_step6Resources.Index).Value.ToString.Trim = "" AndAlso _
                       dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhLocation_step6Resources.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhCurrencyResources.Index).Value) <= 0 AndAlso _
                       decValue = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvOtherResources_step5.CurrentRow.Index = dgvOtherResources_step5.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhResources_step6Resources.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhLocation_step6Resources.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf CInt(dgvOtherResources_step5.Rows(e.RowIndex).Cells(colhCurrencyResources.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
            Else

                If decValue = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOtherResources_step5_RowValidating", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Debts "

    Private Sub dgvDebts_step6_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDebts_step6.CellEnter
        Try
            If e.ColumnIndex = colhCurrencyDebts.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDebts_step6_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvDebts_step6_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDebts_step6.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Try
            Call Update_DataGridview_DataTable_AUD(dgvDebts_step6, e, "assetdebtstranunkid")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDebts_step6_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvDebts_step6_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvDebts_step6.DataError
        Try
            If e.ColumnIndex = dgvDebts_step6.Columns(colhValue_step6Debts.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Enter Correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDebts_step6_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvDebts_step6_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDebts_step6.EditingControlShowing
        Dim tb As TextBox
        Dim editingComboBox As ComboBox
        Try
            If dgvDebts_step6.CurrentCell.ColumnIndex = colhValue_step6Debts.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
            ElseIf dgvDebts_step6.CurrentCell.ColumnIndex = colhCurrencyDebts.Index Then
                'Nothing

                editingComboBox = CType(e.Control, ComboBox)
                RemoveHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
                AddHandler editingComboBox.SelectedIndexChanged, AddressOf combobox_SelectedValueChanged
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDebts_step6_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvDebts_step6_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDebts_step6.RowValidated
        Try
            Update_DataGridview_DataTable_Extra_Columns(dgvDebts_step6, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDebts_step6_RowValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvDebts_step6_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvDebts_step6.RowValidating
        Try
            Dim decValue As Decimal
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            If dgvDebts_step6.Rows(e.RowIndex).IsNewRow = True Then Exit Try

            Decimal.TryParse(dgvDebts_step6.Rows(e.RowIndex).Cells(colhValue_step6Debts.Index).Value.ToString, decValue)
            Decimal.TryParse(dgvDebts_step6.Rows(e.RowIndex).Cells(colhEmployee_step6Debts.Index).Value.ToString, decServant)
            Decimal.TryParse(dgvDebts_step6.Rows(e.RowIndex).Cells(colhWifeHusband_step6Debts.Index).Value.ToString, decWifeHusband)
            Decimal.TryParse(dgvDebts_step6.Rows(e.RowIndex).Cells(colhChildren_step6Debts.Index).Value.ToString, decChildren)

            If dgvDebts_step6.Rows(e.RowIndex).Cells(colhDebts_step6Debts.Index).Value.ToString.Trim = "" AndAlso _
                       dgvDebts_step6.Rows(e.RowIndex).Cells(colhLocation_step6Debts.Index).Value.ToString.Trim = "" AndAlso _
                       CInt(dgvDebts_step6.Rows(e.RowIndex).Cells(colhCurrencyDebts.Index).Value) <= 0 AndAlso _
                       decValue = 0 AndAlso _
                       decServant = 0 AndAlso _
                       decWifeHusband = 0 AndAlso _
                       decChildren = 0 Then
                'Sohail (13 May 2013) -- Start
                'TRA - ENHANCEMENT
                'Exit Sub
                If dgvDebts_step6.CurrentRow.Index = dgvDebts_step6.RowCount - 1 Then
                    Exit Sub
                End If
                'Sohail (13 May 2013) -- End
            End If

            If dgvDebts_step6.Rows(e.RowIndex).Cells(colhDebts_step6Debts.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf dgvDebts_step6.Rows(e.RowIndex).Cells(colhLocation_step6Debts.Index).Value.ToString.Trim = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf CInt(dgvDebts_step6.Rows(e.RowIndex).Cells(colhCurrencyDebts.Index).Value.ToString) <= 0 Then
                e.Cancel = True
                Exit Sub
            Else

                If decValue = 0 Then
                    e.Cancel = True
                    Exit Sub
                End If

                If decServant + decWifeHusband + decChildren <> 100 Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDebts_step6_RowValidating", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (29 Jan 2013) -- End

#End Region

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Textbox's Events "

#Region " Bank "
    Private Sub txtCash_Step2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCash_Step2.LostFocus
        Try
            txtCash_Step2.Text = txtCash_Step2.Decimal.ToString(GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtCash_Step2_LostFocus", mstrModuleName)
        End Try
    End Sub
#End Region

#End Region
    'Sohail (29 Jan 2013) -- End

#Region " Wizard's Events "
    Private Sub wizAsset_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wizAssetDeclaration.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case wizAssetDeclaration.Pages.IndexOf(wizpEmployee)
                    If e.NewIndex > e.OldIndex Then
                        If CInt(cboEmployee_Step1.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee."), enMsgBoxStyle.Information)
                            cboEmployee_Step1.Focus()
                            e.Cancel = True
                            Exit Sub
                            'Sohail (06 Apr 2012) -- Start
                            'TRA - ENHANCEMENT
                        ElseIf dtpDate.Value < FinancialYear._Object._Database_Start_Date OrElse dtpDate.Value > FinancialYear._Object._Database_End_Date Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry! Transaction Date should be between ") & FinancialYear._Object._Database_Start_Date.ToShortDateString & " AND " & FinancialYear._Object._Database_End_Date.ToShortDateString & ".", enMsgBoxStyle.Information)
                            dtpDate.Focus()
                            e.Cancel = True
                            Exit Sub
                            'Sohail (06 Apr 2012) -- End
                        End If
                        If menAction <> enAction.EDIT_ONE AndAlso objAssetDeclare.isExist(CInt(cboEmployee_Step1.SelectedValue)) = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! Asset declaration for this employee already exists."), enMsgBoxStyle.Information)
                            cboEmployee_Step1.Focus()
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If

                    'Sohail (29 Jan 2013) -- Start
                    'TRA - ENHANCEMENT
                Case wizAssetDeclaration.Pages.IndexOf(wizpBankShares)
                    If e.NewIndex > e.OldIndex Then
                        Dim decBankTotal As Decimal = 0
                        For Each dtRow As DataRow In CType(dgvBank_Step2.DataSource, DataTable).Rows
                            decBankTotal += CDec(dtRow.Item("baseamount"))
                        Next
                        If decBankTotal <> txtCashExistingBank_Step2.Decimal Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry! Total amount of respective banks should match with cash in existing bank."), enMsgBoxStyle.Information)
                            txtCash_Step2.Focus()
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                    'Sohail (29 Jan 2013) -- End

                    'Sohail (29 Jan 2013) -- Start
                    'TRA - ENHANCEMENT
                Case wizAssetDeclaration.Pages.IndexOf(wizpOtherResources)
                    'Sohail (29 Jan 2013) -- End
                    If e.NewIndex > e.OldIndex Then

                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to Save this Asset Declaration?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            e.Cancel = True
                            Exit Sub
                        End If

                        Cursor.Current = Cursors.WaitCursor
                        Call SetValue()

                        objAssetDeclare._Datasource_Bank = CType(dgvBank_Step2.DataSource, DataTable)
                        objAssetDeclare._Datasource_ShareDividend = CType(dgvShareDividend_Step2.DataSource, DataTable)
                        objAssetDeclare._Datasource_HouseBuilding = CType(dgvHouse_step3.DataSource, DataTable)
                        objAssetDeclare._Datasource_ParkFarmMines = CType(dgvParkFarm_step3.DataSource, DataTable)
                        objAssetDeclare._Datasource_Vehicles = CType(dgvVehicles_step4.DataSource, DataTable)
                        objAssetDeclare._Datasource_Machinery = CType(dgvMachinery_step4.DataSource, DataTable)
                        objAssetDeclare._Datasource_OtherBusiness = CType(dgvOtherBusiness_step5.DataSource, DataTable)
                        'Sohail (29 Jan 2013) -- Start
                        'TRA - ENHANCEMENT
                        objAssetDeclare._Datasource_Resources = CType(dgvOtherResources_step5.DataSource, DataTable)
                        objAssetDeclare._Datasource_Debts = CType(dgvDebts_step6.DataSource, DataTable)
                        'Sohail (29 Jan 2013) -- End

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objAssetDeclare._FormName = mstrModuleName
                        objAssetDeclare._LoginEmployeeunkid = 0
                        objAssetDeclare._ClientIP = getIP()
                        objAssetDeclare._HostName = getHostName()
                        objAssetDeclare._FromWeb = False
                        objAssetDeclare._AuditUserId = User._Object._Userunkid
                        objAssetDeclare._CompanyUnkid = Company._Object._Companyunkid
                        objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objAssetDeclare.InsertData() = False Then
                        If objAssetDeclare.InsertData(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime) = False Then
                            'Shani(24-Aug-2015) -- End
                            e.Cancel = True
                        Else
                            mblnCancel = False
                        End If
                    End If
            End Select
        Catch ex As Exception
            e.Cancel = True
            DisplayError.Show("-1", ex.Message, "wizAsset_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            frm.ValueMember = cboEmployee_Step1.ValueMember
            frm.DisplayMember = cboEmployee_Step1.DisplayMember
            frm.CodeMember = "employeecode"
            frm.DataSource = CType(cboEmployee_Step1.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee_Step1.SelectedValue = frm.SelectedValue
                cboEmployee_Step1.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region " Language to be include Manually everytime when language tool is ran on this page"
    'Me.wizAssetDeclaration.SaveText = Language._Object.getCaption(Me.wizAssetDeclaration.Name & "_SaveText", Me.wizAssetDeclaration.SaveText)
#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep2.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep2.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep3.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep3.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep4.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep4.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep5.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep5.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep7.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep7.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep6.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep6.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.wizAssetDeclaration.CancelText = Language._Object.getCaption(Me.wizAssetDeclaration.Name & "_CancelText", Me.wizAssetDeclaration.CancelText)
            Me.wizAssetDeclaration.NextText = Language._Object.getCaption(Me.wizAssetDeclaration.Name & "_NextText", Me.wizAssetDeclaration.NextText)
            Me.wizAssetDeclaration.BackText = Language._Object.getCaption(Me.wizAssetDeclaration.Name & "_BackText", Me.wizAssetDeclaration.BackText)
            Me.wizAssetDeclaration.FinishText = Language._Object.getCaption(Me.wizAssetDeclaration.Name & "_FinishText", Me.wizAssetDeclaration.FinishText)
            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.gbStep2.Text = Language._Object.getCaption(Me.gbStep2.Name, Me.gbStep2.Text)
            Me.gbStep3.Text = Language._Object.getCaption(Me.gbStep3.Name, Me.gbStep3.Text)
            Me.gbStep4.Text = Language._Object.getCaption(Me.gbStep4.Name, Me.gbStep4.Text)
            Me.gbStep5.Text = Language._Object.getCaption(Me.gbStep5.Name, Me.gbStep5.Text)
            Me.gbStep7.Text = Language._Object.getCaption(Me.gbStep7.Name, Me.gbStep7.Text)
            Me.lblStep7_Finish.Text = Language._Object.getCaption(Me.lblStep7_Finish.Name, Me.lblStep7_Finish.Text)
            Me.lblFinYearEnd.Text = Language._Object.getCaption(Me.lblFinYearEnd.Name, Me.lblFinYearEnd.Text)
            Me.lblFinYearStart.Text = Language._Object.getCaption(Me.lblFinYearStart.Name, Me.lblFinYearStart.Text)
            Me.lblCenterforWork.Text = Language._Object.getCaption(Me.lblCenterforWork.Name, Me.lblCenterforWork.Text)
            Me.lblOfficePosition.Text = Language._Object.getCaption(Me.lblOfficePosition.Name, Me.lblOfficePosition.Text)
            Me.lblStep1_NoOfEmployment.Text = Language._Object.getCaption(Me.lblStep1_NoOfEmployment.Name, Me.lblStep1_NoOfEmployment.Text)
            Me.lblStep1_Employee.Text = Language._Object.getCaption(Me.lblStep1_Employee.Name, Me.lblStep1_Employee.Text)
            Me.lblShareDividend_step2.Text = Language._Object.getCaption(Me.lblShareDividend_step2.Name, Me.lblShareDividend_step2.Text)
            Me.lblCashExistingBank_Step2.Text = Language._Object.getCaption(Me.lblCashExistingBank_Step2.Name, Me.lblCashExistingBank_Step2.Text)
            Me.lblCash_Step2.Text = Language._Object.getCaption(Me.lblCash_Step2.Name, Me.lblCash_Step2.Text)
            Me.lblFinanceToDate_Step2.Text = Language._Object.getCaption(Me.lblFinanceToDate_Step2.Name, Me.lblFinanceToDate_Step2.Text)
            Me.lblIncompleteConstruction_step3.Text = Language._Object.getCaption(Me.lblIncompleteConstruction_step3.Name, Me.lblIncompleteConstruction_step3.Text)
            Me.lblParkFarm_step3.Text = Language._Object.getCaption(Me.lblParkFarm_step3.Name, Me.lblParkFarm_step3.Text)
            Me.lblHouseBuilding_step3.Text = Language._Object.getCaption(Me.lblHouseBuilding_step3.Name, Me.lblHouseBuilding_step3.Text)
            Me.lblResources_step5.Text = Language._Object.getCaption(Me.lblResources_step5.Name, Me.lblResources_step5.Text)
            Me.lblOtherBusiness_step5.Text = Language._Object.getCaption(Me.lblOtherBusiness_step5.Name, Me.lblOtherBusiness_step5.Text)
            Me.lblGrindingMachine.Text = Language._Object.getCaption(Me.lblGrindingMachine.Name, Me.lblGrindingMachine.Text)
            Me.lblVehicles_step4.Text = Language._Object.getCaption(Me.lblVehicles_step4.Name, Me.lblVehicles_step4.Text)
            Me.txtNoOfEmployment_Step1.Text = Language._Object.getCaption(Me.txtNoOfEmployment_Step1.Name, Me.txtNoOfEmployment_Step1.Text)
            Me.txtOfficePosition_Step1.Text = Language._Object.getCaption(Me.txtOfficePosition_Step1.Name, Me.txtOfficePosition_Step1.Text)
            Me.txtCenterforWork_Step1.Text = Language._Object.getCaption(Me.txtCenterforWork_Step1.Name, Me.txtCenterforWork_Step1.Text)
            Me.lblWorkStation.Text = Language._Object.getCaption(Me.lblWorkStation.Name, Me.lblWorkStation.Text)
            Me.txtWorkStation_Step1.Text = Language._Object.getCaption(Me.txtWorkStation_Step1.Name, Me.txtWorkStation_Step1.Text)
            Me.lblInstruction.Text = Language._Object.getCaption(Me.lblInstruction.Name, Me.lblInstruction.Text)
            Me.colhShareValue_step3.HeaderText = Language._Object.getCaption(Me.colhShareValue_step3.Name, Me.colhShareValue_step3.HeaderText)
            Me.colhLocation_step3.HeaderText = Language._Object.getCaption(Me.colhLocation_step3.Name, Me.colhLocation_step3.HeaderText)
            Me.colhDividendAmount_step3.HeaderText = Language._Object.getCaption(Me.colhDividendAmount_step3.Name, Me.colhDividendAmount_step3.HeaderText)
            Me.colhCurrencyShare.HeaderText = Language._Object.getCaption(Me.colhCurrencyShare.Name, Me.colhCurrencyShare.HeaderText)
            Me.colhServant_step3Shares.HeaderText = Language._Object.getCaption(Me.colhServant_step3Shares.Name, Me.colhServant_step3Shares.HeaderText)
            Me.colhWifeHusband_step3Shares.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step3Shares.Name, Me.colhWifeHusband_step3Shares.HeaderText)
            Me.colhChildren_step3Shares.HeaderText = Language._Object.getCaption(Me.colhChildren_step3Shares.Name, Me.colhChildren_step3Shares.HeaderText)
            Me.colhHomesBuilding_step4.HeaderText = Language._Object.getCaption(Me.colhHomesBuilding_step4.Name, Me.colhHomesBuilding_step4.HeaderText)
            Me.colhLocation_step4.HeaderText = Language._Object.getCaption(Me.colhLocation_step4.Name, Me.colhLocation_step4.HeaderText)
            Me.colhValue_step4.HeaderText = Language._Object.getCaption(Me.colhValue_step4.Name, Me.colhValue_step4.HeaderText)
            Me.colhCurrencyHouse.HeaderText = Language._Object.getCaption(Me.colhCurrencyHouse.Name, Me.colhCurrencyHouse.HeaderText)
            Me.colhServant_step4House.HeaderText = Language._Object.getCaption(Me.colhServant_step4House.Name, Me.colhServant_step4House.HeaderText)
            Me.colhWifeHusband_step4House.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step4House.Name, Me.colhWifeHusband_step4House.HeaderText)
            Me.colhChildren_step4House.HeaderText = Language._Object.getCaption(Me.colhChildren_step4House.Name, Me.colhChildren_step4House.HeaderText)
            Me.colhMachine_step5Machinery.HeaderText = Language._Object.getCaption(Me.colhMachine_step5Machinery.Name, Me.colhMachine_step5Machinery.HeaderText)
            Me.colhLocation_step5Machinery.HeaderText = Language._Object.getCaption(Me.colhLocation_step5Machinery.Name, Me.colhLocation_step5Machinery.HeaderText)
            Me.colhValue_step5Machinery.HeaderText = Language._Object.getCaption(Me.colhValue_step5Machinery.Name, Me.colhValue_step5Machinery.HeaderText)
            Me.colhCurrencyMachine.HeaderText = Language._Object.getCaption(Me.colhCurrencyMachine.Name, Me.colhCurrencyMachine.HeaderText)
            Me.colhServant_step5Machinery.HeaderText = Language._Object.getCaption(Me.colhServant_step5Machinery.Name, Me.colhServant_step5Machinery.HeaderText)
            Me.colhWifeHusband_step5Machinery.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step5Machinery.Name, Me.colhWifeHusband_step5Machinery.HeaderText)
            Me.colhChildren_step5Machinery.HeaderText = Language._Object.getCaption(Me.colhChildren_step5Machinery.Name, Me.colhChildren_step5Machinery.HeaderText)
            Me.colhBusinessType_step6OtherBusiness.HeaderText = Language._Object.getCaption(Me.colhBusinessType_step6OtherBusiness.Name, Me.colhBusinessType_step6OtherBusiness.HeaderText)
            Me.colhPlaceClad_step6OtherBusiness.HeaderText = Language._Object.getCaption(Me.colhPlaceClad_step6OtherBusiness.Name, Me.colhPlaceClad_step6OtherBusiness.HeaderText)
            Me.colhValue_step6OtherBusiness.HeaderText = Language._Object.getCaption(Me.colhValue_step6OtherBusiness.Name, Me.colhValue_step6OtherBusiness.HeaderText)
            Me.colhCurrencyBusiness.HeaderText = Language._Object.getCaption(Me.colhCurrencyBusiness.Name, Me.colhCurrencyBusiness.HeaderText)
            Me.colhServant_step6OtherBusiness.HeaderText = Language._Object.getCaption(Me.colhServant_step6OtherBusiness.Name, Me.colhServant_step6OtherBusiness.HeaderText)
            Me.colhWifeHusband_step6OtherBusiness.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step6OtherBusiness.Name, Me.colhWifeHusband_step6OtherBusiness.HeaderText)
            Me.colhChildren_step6OtherBusiness.HeaderText = Language._Object.getCaption(Me.colhChildren_step6OtherBusiness.Name, Me.colhChildren_step6OtherBusiness.HeaderText)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.colhParkFarmMines_step4Parks.HeaderText = Language._Object.getCaption(Me.colhParkFarmMines_step4Parks.Name, Me.colhParkFarmMines_step4Parks.HeaderText)
            Me.colhAreaSize_step4Parks.HeaderText = Language._Object.getCaption(Me.colhAreaSize_step4Parks.Name, Me.colhAreaSize_step4Parks.HeaderText)
            Me.colhPlaceClad_step4Parks.HeaderText = Language._Object.getCaption(Me.colhPlaceClad_step4Parks.Name, Me.colhPlaceClad_step4Parks.HeaderText)
            Me.colhValue_step4Parks.HeaderText = Language._Object.getCaption(Me.colhValue_step4Parks.Name, Me.colhValue_step4Parks.HeaderText)
            Me.colhCurrencyPark.HeaderText = Language._Object.getCaption(Me.colhCurrencyPark.Name, Me.colhCurrencyPark.HeaderText)
            Me.colhServant_step4Parks.HeaderText = Language._Object.getCaption(Me.colhServant_step4Parks.Name, Me.colhServant_step4Parks.HeaderText)
            Me.colhWifeHusband_step4Parks.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step4Parks.Name, Me.colhWifeHusband_step4Parks.HeaderText)
            Me.colhChildren_step4Parks.HeaderText = Language._Object.getCaption(Me.colhChildren_step4Parks.Name, Me.colhChildren_step4Parks.HeaderText)
            Me.colhCarMotorcycle_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhCarMotorcycle_step5Vehicle.Name, Me.colhCarMotorcycle_step5Vehicle.HeaderText)
            Me.colhCarModel_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhCarModel_step5Vehicle.Name, Me.colhCarModel_step5Vehicle.HeaderText)
            Me.colhCarColour_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhCarColour_step5Vehicle.Name, Me.colhCarColour_step5Vehicle.HeaderText)
            Me.colhCarRegNo_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhCarRegNo_step5Vehicle.Name, Me.colhCarRegNo_step5Vehicle.HeaderText)
            Me.colhCarUse_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhCarUse_step5Vehicle.Name, Me.colhCarUse_step5Vehicle.HeaderText)
            Me.colhCarAcqDate_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhCarAcqDate_step5Vehicle.Name, Me.colhCarAcqDate_step5Vehicle.HeaderText)
            Me.colhValue_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhValue_step5Vehicle.Name, Me.colhValue_step5Vehicle.HeaderText)
            Me.colhCurrencyVehicle.HeaderText = Language._Object.getCaption(Me.colhCurrencyVehicle.Name, Me.colhCurrencyVehicle.HeaderText)
            Me.colhServant_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhServant_step5Vehicle.Name, Me.colhServant_step5Vehicle.HeaderText)
            Me.colhWifeHusband_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step5Vehicle.Name, Me.colhWifeHusband_step5Vehicle.HeaderText)
            Me.colhChildren_step5Vehicle.HeaderText = Language._Object.getCaption(Me.colhChildren_step5Vehicle.Name, Me.colhChildren_step5Vehicle.HeaderText)
            Me.gbStep6.Text = Language._Object.getCaption(Me.gbStep6.Name, Me.gbStep6.Text)
            Me.lblDebt_step6.Text = Language._Object.getCaption(Me.lblDebt_step6.Name, Me.lblDebt_step6.Text)
            Me.colhDebts_step6Debts.HeaderText = Language._Object.getCaption(Me.colhDebts_step6Debts.Name, Me.colhDebts_step6Debts.HeaderText)
            Me.colhLocation_step6Debts.HeaderText = Language._Object.getCaption(Me.colhLocation_step6Debts.Name, Me.colhLocation_step6Debts.HeaderText)
            Me.colhValue_step6Debts.HeaderText = Language._Object.getCaption(Me.colhValue_step6Debts.Name, Me.colhValue_step6Debts.HeaderText)
            Me.colhCurrencyDebts.HeaderText = Language._Object.getCaption(Me.colhCurrencyDebts.Name, Me.colhCurrencyDebts.HeaderText)
            Me.colhEmployee_step6Debts.HeaderText = Language._Object.getCaption(Me.colhEmployee_step6Debts.Name, Me.colhEmployee_step6Debts.HeaderText)
            Me.colhWifeHusband_step6Debts.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step6Debts.Name, Me.colhWifeHusband_step6Debts.HeaderText)
            Me.colhChildren_step6Debts.HeaderText = Language._Object.getCaption(Me.colhChildren_step6Debts.Name, Me.colhChildren_step6Debts.HeaderText)
            Me.colhResources_step6Resources.HeaderText = Language._Object.getCaption(Me.colhResources_step6Resources.Name, Me.colhResources_step6Resources.HeaderText)
            Me.colhLocation_step6Resources.HeaderText = Language._Object.getCaption(Me.colhLocation_step6Resources.Name, Me.colhLocation_step6Resources.HeaderText)
            Me.colhValue_step6Resources.HeaderText = Language._Object.getCaption(Me.colhValue_step6Resources.Name, Me.colhValue_step6Resources.HeaderText)
            Me.colhCurrencyResources.HeaderText = Language._Object.getCaption(Me.colhCurrencyResources.Name, Me.colhCurrencyResources.HeaderText)
            Me.colhEmployee_step6Resources.HeaderText = Language._Object.getCaption(Me.colhEmployee_step6Resources.Name, Me.colhEmployee_step6Resources.HeaderText)
            Me.colhWifeHusband_step6Resources.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step6Resources.Name, Me.colhWifeHusband_step6Resources.HeaderText)
            Me.colhChildren_step6Resources.HeaderText = Language._Object.getCaption(Me.colhChildren_step6Resources.Name, Me.colhChildren_step6Resources.HeaderText)
            Me.lblAcqDateFormat.Text = Language._Object.getCaption(Me.lblAcqDateFormat.Name, Me.lblAcqDateFormat.Text)
            Me.colhBank_step3.HeaderText = Language._Object.getCaption(Me.colhBank_step3.Name, Me.colhBank_step3.HeaderText)
            Me.colhAccount_step3.HeaderText = Language._Object.getCaption(Me.colhAccount_step3.Name, Me.colhAccount_step3.HeaderText)
            Me.colhAmount_step3.HeaderText = Language._Object.getCaption(Me.colhAmount_step3.Name, Me.colhAmount_step3.HeaderText)
            Me.colhCurrencyBank.HeaderText = Language._Object.getCaption(Me.colhCurrencyBank.Name, Me.colhCurrencyBank.HeaderText)
            Me.colhServant_step3.HeaderText = Language._Object.getCaption(Me.colhServant_step3.Name, Me.colhServant_step3.HeaderText)
            Me.colhWifeHusband_step3.HeaderText = Language._Object.getCaption(Me.colhWifeHusband_step3.Name, Me.colhWifeHusband_step3.HeaderText)
            Me.colhChildren_step3.HeaderText = Language._Object.getCaption(Me.colhChildren_step3.Name, Me.colhChildren_step3.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee.")
            Language.setMessage(mstrModuleName, 2, "Please Enter Correct Amount.")
            Language.setMessage(mstrModuleName, 3, "Sorry! Transaction Date should be between")
            Language.setMessage(mstrModuleName, 4, "Sorry! Asset declaration for this employee already exists.")
            Language.setMessage(mstrModuleName, 5, "Sorry! Total amount of respective banks should match with cash in existing bank.")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to Save this Asset Declaration?")
            Language.setMessage(mstrModuleName, 7, "Please Enter correct Date in yyyy/MM/dd format.")
            Language.setMessage(mstrModuleName, 8, "Sorry! Acquisition date should not be greater than current date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class