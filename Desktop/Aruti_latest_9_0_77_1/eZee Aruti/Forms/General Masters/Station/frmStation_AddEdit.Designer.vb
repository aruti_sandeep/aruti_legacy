﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStation_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStation_AddEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbStationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.objStLine = New eZee.Common.eZeeStraightLine
        Me.lblWebsite = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtWebsite = New eZee.TextBox.AlphanumericTextBox
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.txtCompanyEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblPhone3 = New System.Windows.Forms.Label
        Me.lblPhone2 = New System.Windows.Forms.Label
        Me.lblPhone1 = New System.Windows.Forms.Label
        Me.txtPhone3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtPhone2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtPhone1 = New eZee.TextBox.AlphanumericTextBox
        Me.elContactInfo = New eZee.Common.eZeeLine
        Me.cboZipcode = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblPostal = New System.Windows.Forms.Label
        Me.lblCountry = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.txtAddressLine2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddressLine1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress1 = New System.Windows.Forms.Label
        Me.elAddressInfo = New eZee.Common.eZeeLine
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbStationInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.gbStationInfo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(665, 356)
        Me.Panel1.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 301)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(665, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(453, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(556, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbStationInfo
        '
        Me.gbStationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbStationInfo.Checked = False
        Me.gbStationInfo.CollapseAllExceptThis = False
        Me.gbStationInfo.CollapsedHoverImage = Nothing
        Me.gbStationInfo.CollapsedNormalImage = Nothing
        Me.gbStationInfo.CollapsedPressedImage = Nothing
        Me.gbStationInfo.CollapseOnLoad = False
        Me.gbStationInfo.Controls.Add(Me.lblAddress2)
        Me.gbStationInfo.Controls.Add(Me.objStLine)
        Me.gbStationInfo.Controls.Add(Me.lblWebsite)
        Me.gbStationInfo.Controls.Add(Me.lblEmail)
        Me.gbStationInfo.Controls.Add(Me.lblFax)
        Me.gbStationInfo.Controls.Add(Me.txtWebsite)
        Me.gbStationInfo.Controls.Add(Me.txtFax)
        Me.gbStationInfo.Controls.Add(Me.txtCompanyEmail)
        Me.gbStationInfo.Controls.Add(Me.lblPhone3)
        Me.gbStationInfo.Controls.Add(Me.lblPhone2)
        Me.gbStationInfo.Controls.Add(Me.lblPhone1)
        Me.gbStationInfo.Controls.Add(Me.txtPhone3)
        Me.gbStationInfo.Controls.Add(Me.txtPhone2)
        Me.gbStationInfo.Controls.Add(Me.txtPhone1)
        Me.gbStationInfo.Controls.Add(Me.elContactInfo)
        Me.gbStationInfo.Controls.Add(Me.cboZipcode)
        Me.gbStationInfo.Controls.Add(Me.cboState)
        Me.gbStationInfo.Controls.Add(Me.cboCity)
        Me.gbStationInfo.Controls.Add(Me.lblCity)
        Me.gbStationInfo.Controls.Add(Me.lblPostal)
        Me.gbStationInfo.Controls.Add(Me.lblCountry)
        Me.gbStationInfo.Controls.Add(Me.cboCountry)
        Me.gbStationInfo.Controls.Add(Me.lblState)
        Me.gbStationInfo.Controls.Add(Me.txtAddressLine2)
        Me.gbStationInfo.Controls.Add(Me.txtAddressLine1)
        Me.gbStationInfo.Controls.Add(Me.lblAddress1)
        Me.gbStationInfo.Controls.Add(Me.elAddressInfo)
        Me.gbStationInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbStationInfo.Controls.Add(Me.lblDescription)
        Me.gbStationInfo.Controls.Add(Me.lblName)
        Me.gbStationInfo.Controls.Add(Me.txtDescription)
        Me.gbStationInfo.Controls.Add(Me.txtName)
        Me.gbStationInfo.Controls.Add(Me.txtCode)
        Me.gbStationInfo.Controls.Add(Me.lblCode)
        Me.gbStationInfo.ExpandedHoverImage = Nothing
        Me.gbStationInfo.ExpandedNormalImage = Nothing
        Me.gbStationInfo.ExpandedPressedImage = Nothing
        Me.gbStationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStationInfo.HeaderHeight = 25
        Me.gbStationInfo.HeaderMessage = ""
        Me.gbStationInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStationInfo.HeightOnCollapse = 0
        Me.gbStationInfo.LeftTextSpace = 0
        Me.gbStationInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbStationInfo.Name = "gbStationInfo"
        Me.gbStationInfo.OpenHeight = 300
        Me.gbStationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStationInfo.ShowBorder = True
        Me.gbStationInfo.ShowCheckBox = False
        Me.gbStationInfo.ShowCollapseButton = False
        Me.gbStationInfo.ShowDefaultBorderColor = True
        Me.gbStationInfo.ShowDownButton = False
        Me.gbStationInfo.ShowHeader = True
        Me.gbStationInfo.Size = New System.Drawing.Size(643, 284)
        Me.gbStationInfo.TabIndex = 0
        Me.gbStationInfo.Temp = 0
        Me.gbStationInfo.Text = "Branch Info"
        Me.gbStationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(26, 148)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(66, 15)
        Me.lblAddress2.TabIndex = 8
        Me.lblAddress2.Text = "Address2"
        Me.lblAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine
        '
        Me.objStLine.BackColor = System.Drawing.SystemColors.Control
        Me.objStLine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.objStLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine.Location = New System.Drawing.Point(322, 26)
        Me.objStLine.Name = "objStLine"
        Me.objStLine.Size = New System.Drawing.Size(13, 256)
        Me.objStLine.TabIndex = 18
        '
        'lblWebsite
        '
        Me.lblWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWebsite.Location = New System.Drawing.Point(359, 256)
        Me.lblWebsite.Name = "lblWebsite"
        Me.lblWebsite.Size = New System.Drawing.Size(73, 15)
        Me.lblWebsite.TabIndex = 32
        Me.lblWebsite.Text = "Website"
        Me.lblWebsite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(359, 229)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(73, 15)
        Me.lblEmail.TabIndex = 30
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(359, 202)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(73, 15)
        Me.lblFax.TabIndex = 28
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWebsite
        '
        Me.txtWebsite.Flags = 0
        Me.txtWebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWebsite.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWebsite.Location = New System.Drawing.Point(437, 253)
        Me.txtWebsite.Name = "txtWebsite"
        Me.txtWebsite.Size = New System.Drawing.Size(191, 21)
        Me.txtWebsite.TabIndex = 33
        '
        'txtFax
        '
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(437, 199)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(191, 21)
        Me.txtFax.TabIndex = 29
        '
        'txtCompanyEmail
        '
        Me.txtCompanyEmail.Flags = 0
        Me.txtCompanyEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompanyEmail.Location = New System.Drawing.Point(437, 226)
        Me.txtCompanyEmail.Name = "txtCompanyEmail"
        Me.txtCompanyEmail.Size = New System.Drawing.Size(191, 21)
        Me.txtCompanyEmail.TabIndex = 31
        '
        'lblPhone3
        '
        Me.lblPhone3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone3.Location = New System.Drawing.Point(359, 175)
        Me.lblPhone3.Name = "lblPhone3"
        Me.lblPhone3.Size = New System.Drawing.Size(73, 15)
        Me.lblPhone3.TabIndex = 26
        Me.lblPhone3.Text = "Telephone 3"
        Me.lblPhone3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPhone2
        '
        Me.lblPhone2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone2.Location = New System.Drawing.Point(359, 148)
        Me.lblPhone2.Name = "lblPhone2"
        Me.lblPhone2.Size = New System.Drawing.Size(73, 15)
        Me.lblPhone2.TabIndex = 24
        Me.lblPhone2.Text = "Telephone 2"
        Me.lblPhone2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPhone1
        '
        Me.lblPhone1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone1.Location = New System.Drawing.Point(359, 121)
        Me.lblPhone1.Name = "lblPhone1"
        Me.lblPhone1.Size = New System.Drawing.Size(73, 15)
        Me.lblPhone1.TabIndex = 22
        Me.lblPhone1.Text = "Telephone 1"
        Me.lblPhone1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPhone3
        '
        Me.txtPhone3.Flags = 0
        Me.txtPhone3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone3.Location = New System.Drawing.Point(437, 172)
        Me.txtPhone3.Name = "txtPhone3"
        Me.txtPhone3.Size = New System.Drawing.Size(191, 21)
        Me.txtPhone3.TabIndex = 27
        '
        'txtPhone2
        '
        Me.txtPhone2.Flags = 0
        Me.txtPhone2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone2.Location = New System.Drawing.Point(437, 145)
        Me.txtPhone2.Name = "txtPhone2"
        Me.txtPhone2.Size = New System.Drawing.Size(191, 21)
        Me.txtPhone2.TabIndex = 25
        '
        'txtPhone1
        '
        Me.txtPhone1.Flags = 0
        Me.txtPhone1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone1.Location = New System.Drawing.Point(437, 118)
        Me.txtPhone1.Name = "txtPhone1"
        Me.txtPhone1.Size = New System.Drawing.Size(191, 21)
        Me.txtPhone1.TabIndex = 23
        '
        'elContactInfo
        '
        Me.elContactInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elContactInfo.Location = New System.Drawing.Point(341, 94)
        Me.elContactInfo.Name = "elContactInfo"
        Me.elContactInfo.Size = New System.Drawing.Size(286, 16)
        Me.elContactInfo.TabIndex = 21
        Me.elContactInfo.Text = "Contact Info"
        '
        'cboZipcode
        '
        Me.cboZipcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboZipcode.DropDownWidth = 120
        Me.cboZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboZipcode.FormattingEnabled = True
        Me.cboZipcode.Location = New System.Drawing.Point(98, 253)
        Me.cboZipcode.Name = "cboZipcode"
        Me.cboZipcode.Size = New System.Drawing.Size(191, 21)
        Me.cboZipcode.TabIndex = 17
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(98, 199)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(191, 21)
        Me.cboState.TabIndex = 13
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.DropDownWidth = 120
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(98, 226)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(191, 21)
        Me.cboCity.TabIndex = 15
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(26, 229)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(66, 15)
        Me.lblCity.TabIndex = 14
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostal
        '
        Me.lblPostal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostal.Location = New System.Drawing.Point(26, 255)
        Me.lblPostal.Name = "lblPostal"
        Me.lblPostal.Size = New System.Drawing.Size(66, 15)
        Me.lblPostal.TabIndex = 16
        Me.lblPostal.Text = "Postal"
        Me.lblPostal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(26, 175)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(66, 15)
        Me.lblCountry.TabIndex = 10
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(98, 172)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(191, 21)
        Me.cboCountry.TabIndex = 11
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(26, 202)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(66, 15)
        Me.lblState.TabIndex = 12
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddressLine2
        '
        Me.txtAddressLine2.Flags = 0
        Me.txtAddressLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddressLine2.Location = New System.Drawing.Point(98, 145)
        Me.txtAddressLine2.Name = "txtAddressLine2"
        Me.txtAddressLine2.Size = New System.Drawing.Size(191, 21)
        Me.txtAddressLine2.TabIndex = 9
        '
        'txtAddressLine1
        '
        Me.txtAddressLine1.Flags = 0
        Me.txtAddressLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressLine1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddressLine1.Location = New System.Drawing.Point(98, 118)
        Me.txtAddressLine1.Name = "txtAddressLine1"
        Me.txtAddressLine1.Size = New System.Drawing.Size(191, 21)
        Me.txtAddressLine1.TabIndex = 7
        '
        'lblAddress1
        '
        Me.lblAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress1.Location = New System.Drawing.Point(26, 121)
        Me.lblAddress1.Name = "lblAddress1"
        Me.lblAddress1.Size = New System.Drawing.Size(66, 15)
        Me.lblAddress1.TabIndex = 6
        Me.lblAddress1.Text = "Address1"
        Me.lblAddress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elAddressInfo
        '
        Me.elAddressInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elAddressInfo.Location = New System.Drawing.Point(8, 94)
        Me.elAddressInfo.Name = "elAddressInfo"
        Me.elAddressInfo.Size = New System.Drawing.Size(281, 16)
        Me.elAddressInfo.TabIndex = 5
        Me.elAddressInfo.Text = "Address Information"
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(295, 60)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(359, 36)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(73, 15)
        Me.lblDescription.TabIndex = 19
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(26, 63)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(66, 15)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(437, 33)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(191, 48)
        Me.txtDescription.TabIndex = 20
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(98, 60)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(191, 21)
        Me.txtName.TabIndex = 3
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(98, 33)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(104, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(26, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(66, 15)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmStation_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(665, 356)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStation_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Branch"
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbStationInfo.ResumeLayout(False)
        Me.gbStationInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbStationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents cboZipcode As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblPostal As System.Windows.Forms.Label
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents txtAddressLine2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddressLine1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress1 As System.Windows.Forms.Label
    Friend WithEvents elAddressInfo As eZee.Common.eZeeLine
    Friend WithEvents lblWebsite As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtWebsite As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCompanyEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPhone3 As System.Windows.Forms.Label
    Friend WithEvents lblPhone2 As System.Windows.Forms.Label
    Friend WithEvents lblPhone1 As System.Windows.Forms.Label
    Friend WithEvents txtPhone3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPhone2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPhone1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents elContactInfo As eZee.Common.eZeeLine
    Friend WithEvents objStLine As eZee.Common.eZeeStraightLine
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
End Class
