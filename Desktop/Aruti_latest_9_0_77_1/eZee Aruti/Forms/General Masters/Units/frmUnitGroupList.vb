﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUnitGroupList

#Region " Private Varaibles "
    Private objUnitGroup As clsUnitGroup
    Private ReadOnly mstrModuleName As String = "frmUnitGroupList"
#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddUnitGroup
            btnEdit.Enabled = User._Object.Privilege._EditUnitGroup
            btnDelete.Enabled = User._Object.Privilege._DeleteUnitGroup
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsAccess As New DataSet
        Dim dtTable As DataTable
        Dim StrSearch As String = ""
        Try

            If User._Object.Privilege._AllowToViewUnitGroupList = True Then    'Pinkal (09-Jul-2012) -- Start

                dsAccess = objUnitGroup.GetList("UnitGroup")

                Dim lvItem As ListViewItem

                lvUnitGroup.Items.Clear()

                If CInt(cboSections.SelectedValue) > 0 Then
                    StrSearch &= "sectionunkid = " & CInt(cboSections.SelectedValue) & " "
                End If

                If StrSearch.Length > 0 Then
                    dtTable = New DataView(dsAccess.Tables("UnitGroup"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsAccess.Tables("UnitGroup"), "", "", DataViewRowState.CurrentRows).ToTable
                End If

                For Each drRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("code").ToString
                    lvItem.Tag = drRow("unitgroupunkid")
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvItem.SubItems.Add(drRow("Section").ToString)

                    lvUnitGroup.Items.Add(lvItem)
                Next

                If lvUnitGroup.Items.Count > 16 Then
                    colhDescription.Width = 250 - 18
                Else
                    colhDescription.Width = 250
                End If

            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAccess.Dispose()
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim dsSection As New DataSet
            Dim objSection As New clsSections

            dsSection = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsSection.Tables("Section")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDepartmentGroupList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvUnitGroup.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmDepartmentGroupList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmDepartmentGroupList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUnitGroup = New clsUnitGroup
        Try
            Call Set_Logo(Me, gApplicationType)


            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call FillCombo()
            Call fillList()

            If lvUnitGroup.Items.Count > 0 Then lvUnitGroup.Items(0).Selected = True
            lvUnitGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDepartmentGroupList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDepartmentGroupList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUnitGroup = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUnitGroup.SetMessages()
            objfrm._Other_ModuleNames = "clsUnitGroup"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvUnitGroup.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Unit Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvUnitGroup.Select()
            Exit Sub
        End If
        'If objDeptGroup.isUsed(CInt(lvDeptGroup.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Unit Group. Reason: This Department Group is in use."), enMsgBoxStyle.Information) '?2
        '    lvDeptGroup.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvUnitGroup.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Unit Group?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objUnitGroup._FormName = mstrModuleName
                objUnitGroup._LoginEmployeeunkid = 0
                objUnitGroup._ClientIP = getIP()
                objUnitGroup._HostName = getHostName()
                objUnitGroup._FromWeb = False
                objUnitGroup._AuditUserId = User._Object._Userunkid
objUnitGroup._CompanyUnkid = Company._Object._Companyunkid
                objUnitGroup._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objUnitGroup.Delete(CInt(lvUnitGroup.SelectedItems(0).Tag))
                If objUnitGroup._Message <> "" Then
                    eZeeMsgBox.Show(objUnitGroup._Message, enMsgBoxStyle.Information)
                Else
                    lvUnitGroup.SelectedItems(0).Remove()
                End If
                If lvUnitGroup.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvUnitGroup.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvUnitGroup.Items.Count - 1
                    lvUnitGroup.Items(intSelectedIndex).Selected = True
                    lvUnitGroup.EnsureVisible(intSelectedIndex)
                ElseIf lvUnitGroup.Items.Count <> 0 Then
                    lvUnitGroup.Items(intSelectedIndex).Selected = True
                    lvUnitGroup.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvUnitGroup.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvUnitGroup.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Unit Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvUnitGroup.Select()
            Exit Sub
        End If
        Dim frm As New frmUnitGroup_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvUnitGroup.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            If frm.displayDialog(CInt(lvUnitGroup.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvUnitGroup.Items(intSelectedIndex).Selected = True
            lvUnitGroup.EnsureVisible(intSelectedIndex)
            lvUnitGroup.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmUnitGroup_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Unit Group from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Unit Group?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class