﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmJobsList

#Region " Private Varaibles "
    Private objJobs As clsJobs
    Private ReadOnly mstrModuleName As String = "frmJobsList"
#End Region

#Region " Private Function "

    'Sandeep [ 29 Oct 2010 ] -- Start
    'Privilege Changes
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddJob
            btnEdit.Enabled = User._Object.Privilege._EditJob
            btnDelete.Enabled = User._Object.Privilege._DeleteJob
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 29 Oct 2010 ] -- End

    Private Sub FillCombo()
        Dim objJobGrp As New clsJobGroup
        Dim objGrade As New clsGrade
        'Dim objJobHead As New clsJobHead
        'Sandeep [ 15 DEC 2010 ] -- Start
        'Issue : Mr. Rutta's Comment
        'Dim objReportTo As New clsJobHead
        Dim objReportTo As New clsJobs
        'Sandeep [ 15 DEC 2010 ] -- End 
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim dsList As New DataSet
        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim objTeam As New clsTeams
        'S.SANDEEP [ 07 NOV 2011 ] -- END

        'Pinkal (03-Dec-2015) -- Start
        'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
        Dim objDepartment As New clsDepartment
        Dim objClassGroup As New clsClassGroup
        'Pinkal (03-Dec-2015) -- End

        Try

            dsList = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("JobGrp")
            End With
            cboJobGroup.SelectedValue = 0

            dsList = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Grade")
            End With
            cboGrade.SelectedValue = 0

            'dsList = objJobHead.getComboList("JobHead", True)
            'With cboJobHead
            '    .ValueMember = "jobheadunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("JobHead")
            'End With
            'cboJobHead.SelectedValue = 0


            'Sandeep [ 15 DEC 2010 ] -- Start
            'Issue : Mr. Rutta's Comment
            'dsList = objReportTo.getComboList("ReportTo", True)
            'With cboReportTo
            '    .ValueMember = "jobheadunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("ReportTo")
            'End With
            'cboReportTo.SelectedValue = 0

            dsList = objReportTo.getComboList("ReportTo", True)
            With cboReportTo
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("ReportTo")
            End With
            cboReportTo.SelectedValue = 0

            'Sandeep [ 15 DEC 2010 ] -- End 


            dsList = objSection.getComboList("Section", True)
            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Section")
            End With
            cboSection.SelectedValue = 0

            dsList = objUnit.getComboList("Units", True)
            With cboUnit
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Units")
            End With
            cboUnit.SelectedValue = 0

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objTeam.getComboList("List", True)
            With cboTeam
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            With cboInDirectReportTo
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = CType(cboReportTo.DataSource, DataTable).Copy
            End With
            cboInDirectReportTo.SelectedValue = 0

            dsList = objDepartment.getComboList("List", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboDepartment.SelectedValue = 0

            dsList = objClassGroup.getComboList("List", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboClassGroup.SelectedValue = 0

            'Pinkal (03-Dec-2015) -- End

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            dsList = (New clsStation).getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboBranch.SelectedValue = 0

            dsList = (New clsDepartmentGroup).getComboList("List", True)
            With cboDeparmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboDeparmentGrp.SelectedValue = 0

            dsList = (New clsSectionGroup).getComboList("List", True)
            With cboSectionGrp
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboSectionGrp.SelectedValue = 0

            dsList = (New clsUnitGroup).getComboList("List", True)
            With cboUnitGrp
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboUnitGrp.SelectedValue = 0

            dsList = (New clsClass).getComboList("List", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboClass.SelectedValue = 0

            dsList = (New clsGradeLevel).getComboList("List", True)
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboGradeLevel.SelectedValue = 0
            'Shani(18-JUN-2016) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objJobGrp = Nothing
            objGrade = Nothing
            'objJobHead = Nothing
            objReportTo = Nothing
            objSection = Nothing
            objUnit = Nothing
        End Try
    End Sub

    Private Sub fillList()
        Dim dsAccess As New DataSet
        Dim dtJobs As New DataTable
        Dim strSearching As String = ""
        Dim strLevel As String = String.Empty

        strLevel = Language.getMessage(mstrModuleName, 2, "Level")
        Try

            If User._Object.Privilege._AllowToViewJobList = True Then    'Pinkal (09-Jul-2012) -- Start

                dsAccess = objJobs.GetList("Jobs", , True)

                If CInt(cboJobGroup.SelectedValue) > 0 Then
                    strSearching &= "AND jobgroupunkid = " & CInt(cboJobGroup.SelectedValue) & " "
                End If

                'If CInt(cboJobHead.SelectedValue) > 0 Then
                '    strSearching &= "AND jobheadunkid = " & CInt(cboJobHead.SelectedValue) & " "
                'End If

                If CInt(cboSection.SelectedValue) > 0 Then
                    strSearching &= "AND jobsectionunkid = " & CInt(cboSection.SelectedValue) & " "
                End If

                If CInt(cboGrade.SelectedValue) > 0 Then
                    strSearching &= "AND jobgradeunkid = " & CInt(cboGrade.SelectedValue) & " "
                End If

                If CInt(cboUnit.SelectedValue) > 0 Then
                    strSearching &= "AND jobunitunkid = " & CInt(cboUnit.SelectedValue) & " "
                End If

                If CInt(cboReportTo.SelectedValue) > 0 Then
                    strSearching &= "AND Reportunkid = " & CInt(cboReportTo.SelectedValue) & " "
                End If

                If CStr(txtName.Text.Trim) <> "" Then
                    strSearching &= "AND JobName Like '%" & txtName.Text & "%' "
                End If

                If dtpCreateDate.Checked = True Then
                    strSearching &= "AND Createdate = '" & eZeeDate.convertDate(dtpCreateDate.Value) & "' "
                End If

                If dtpTerminateDate.Checked = True Then
                    strSearching &= "AND TerminateDate =  '" & eZeeDate.convertDate(dtpTerminateDate.Value) & "' "
                End If

                If CInt(nudPosition.Value) > 0 Then
                    strSearching &= "AND Positon = " & CInt(nudPosition.Value) & " "
                End If

                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboTeam.SelectedValue) > 0 Then
                    strSearching &= "AND teamunkid = " & CInt(cboTeam.SelectedValue) & " "
                End If
                'S.SANDEEP [ 07 NOV 2011 ] -- END



                'Pinkal (03-Dec-2015) -- Start
                'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

                If CInt(cboInDirectReportTo.SelectedValue) > 0 Then
                    strSearching &= "AND indirectreport_tounkid = " & CInt(cboInDirectReportTo.SelectedValue) & " "
                End If

                If CInt(cboDepartment.SelectedValue) > 0 Then
                    strSearching &= "AND jobdepartmentunkid = " & CInt(cboDepartment.SelectedValue) & " "
                End If

                If CInt(cboClassGroup.SelectedValue) > 0 Then
                    strSearching &= "AND jobclassgroupunkid = " & CInt(cboClassGroup.SelectedValue) & " "
                End If
                'Pinkal (03-Dec-2015) -- End


                'Shani(18-JUN-2016) -- Start
                'Enhancement : Add Allocation Column in Job master[Rutta]

                If CInt(cboBranch.SelectedValue) > 0 Then
                    strSearching &= "AND jobbranchunkid = " & CInt(cboBranch.SelectedValue) & " "
                End If

                If CInt(cboDeparmentGrp.SelectedValue) > 0 Then
                    strSearching &= "AND jobdepartmentgrpunkid = " & CInt(cboDeparmentGrp.SelectedValue) & " "
                End If

                If CInt(cboSectionGrp.SelectedValue) > 0 Then
                    strSearching &= "AND jobsectiongrpunkid = " & CInt(cboSectionGrp.SelectedValue) & " "
                End If

                If CInt(cboUnitGrp.SelectedValue) > 0 Then
                    strSearching &= "AND jobunitgrpunkid = " & CInt(cboUnitGrp.SelectedValue) & " "
                End If

                If CInt(cboClass.SelectedValue) > 0 Then
                    strSearching &= "AND jobclassunkid = " & CInt(cboClass.SelectedValue) & " "
                End If

                If CInt(cboGradeLevel.SelectedValue) > 0 Then
                    strSearching &= "AND jobgradelevelunkid = " & CInt(cboGradeLevel.SelectedValue) & " "
                End If

                'Shani(18-JUN-2016) -- End


                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtJobs = New DataView(dsAccess.Tables("Jobs"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtJobs = dsAccess.Tables("Jobs")
                End If



                Dim lvItem As ListViewItem

                lvJobList.Items.Clear()


                'Pinkal (03-Dec-2015) -- Start
                'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

                'For Each drRow As DataRow In dtJobs.Rows
                '    lvItem = New ListViewItem
                '    lvItem.Text = drRow("JobsBy").ToString
                '    lvItem.Tag = drRow("jobunkid")
                '    lvItem.SubItems.Add(drRow("Code").ToString)
                '    lvItem.SubItems.Add(drRow("JobName").ToString)
                '    lvItem.SubItems.Add(drRow.Item("job_level").ToString)
                '    If Trim(drRow("Createdate").ToString) <> "" Then
                '        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Createdate").ToString).ToShortDateString)
                '    Else
                '        lvItem.SubItems.Add("")
                '    End If
                '    If Trim(drRow("TerminateDate").ToString) <> "" Then
                '        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("TerminateDate").ToString).ToShortDateString)
                '    Else
                '        lvItem.SubItems.Add("")
                '    End If
                '    If CInt(drRow("Positon")) > 0 Then
                '        lvItem.SubItems.Add(drRow("Positon").ToString)
                '    Else
                '        lvItem.SubItems.Add("")
                '    End If
                '    If drRow("ReportTo").ToString <> "" Then
                '        lvItem.SubItems.Add(drRow("ReportTo").ToString & " " & strLevel & " " & drRow.Item("RJob_Level").ToString)
                '    Else
                '        lvItem.SubItems.Add("")
                '    End If
                '    lvItem.SubItems.Add(drRow("jobgrpoup").ToString)
                '    lvItem.SubItems.Add(drRow("section").ToString)
                '    lvItem.SubItems.Add(drRow("unit").ToString)
                '    lvItem.SubItems.Add(drRow("teamname").ToString)
                '    lvItem.SubItems.Add(drRow("grade").ToString)
                '    lvJobList.Items.Add(lvItem)
                'Next

                For Each drRow As DataRow In dtJobs.Rows
                    lvItem = New ListViewItem
                    'Shani(05-JUL-2016) -- Start
                    'lvItem.Text = drRow("jobgrpoup").ToString()
                    lvItem.Text = drRow("Code").ToString
                    lvItem.SubItems.Add(drRow("JobName").ToString)
                    lvItem.SubItems.Add(drRow("jobgrpoup").ToString())
                    'Shani(05-JUL-2016) -- End

                    lvItem.Tag = drRow("jobunkid")
                    lvItem.SubItems.Add(drRow("branch").ToString)
                    lvItem.SubItems.Add(drRow("department_grp").ToString)
                    lvItem.SubItems.Add(drRow("department").ToString)
                    lvItem.SubItems.Add(drRow("section_grp").ToString)
                    lvItem.SubItems.Add(drRow("section").ToString)
                    lvItem.SubItems.Add(drRow("unit_grp").ToString)
                    lvItem.SubItems.Add(drRow("unit").ToString)
                    lvItem.SubItems.Add(drRow("teamname").ToString)
                    lvItem.SubItems.Add(drRow("classgroup").ToString)
                    lvItem.SubItems.Add(drRow("class").ToString)
                    lvItem.SubItems.Add(drRow("grade").ToString)
                    lvItem.SubItems.Add(drRow("grade_level").ToString)
                    lvItem.SubItems.Add(drRow("JobsBy").ToString)

                    'Shani(05-JUL-2016) -- Start
                    'lvItem.SubItems.Add(drRow("Code").ToString)
                    'lvItem.SubItems.Add(drRow("JobName").ToString)
                    'Shani(05-JUL-2016) -- End
                    lvItem.SubItems.Add(drRow.Item("job_level").ToString)
                    If Trim(drRow("Createdate").ToString) <> "" Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Createdate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    If Trim(drRow("TerminateDate").ToString) <> "" Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("TerminateDate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    If CInt(drRow("Positon")) > 0 Then
                        lvItem.SubItems.Add(drRow("Positon").ToString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    If drRow("ReportTo").ToString <> "" Then
                        lvItem.SubItems.Add(drRow("ReportTo").ToString & " " & strLevel & " " & drRow.Item("RJob_Level").ToString)
                    Else
                        lvItem.SubItems.Add("")
                    End If

                    lvJobList.Items.Add(lvItem)
                Next


                'Pinkal (03-Dec-2015) -- End

                If lvJobList.Items.Count > 16 Then
                    colhJobName.Width = 300 - 20
                Else
                    colhJobName.Width = 300
                End If

            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAccess.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmJobsList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvJobList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobsList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobsList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobsList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objJobs = New clsJobs
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sandeep [ 29 Oct 2010 ] -- Start
            'Privilege Changes
            Call SetVisibility()
            'Sandeep [ 29 Oct 2010 ] -- End

            Call FillCombo()
            Call fillList()

            If lvJobList.Items.Count > 0 Then lvJobList.Items(0).Selected = True
            lvJobList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobsList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobsList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objJobs = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJobs.SetMessages()
            objfrm._Other_ModuleNames = "clsJobs"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvJobList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Job from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobList.Select()
            Exit Sub
        End If
        'If objJobs.isUsed(CInt(lvJobList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Job . Reason: This Job  is in use."), enMsgBoxStyle.Information) '?2
        '    lvJobList.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvJobList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Job?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Pinkal (03-Dec-2015) -- Start
                'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
                objJobs._Userunkid = User._Object._Userunkid
                'Pinkal (03-Dec-2015) -- End

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objJobs._FormName = mstrModuleName
                objJobs._LoginEmployeeunkid = 0
                objJobs._ClientIP = getIP()
                objJobs._HostName = getHostName()
                objJobs._FromWeb = False
                objJobs._AuditUserId = User._Object._Userunkid
objJobs._CompanyUnkid = Company._Object._Companyunkid
                objJobs._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objJobs.Delete(CInt(lvJobList.SelectedItems(0).Tag))
                If objJobs._Message <> "" Then
                    eZeeMsgBox.Show(objJobs._Message, enMsgBoxStyle.Information)
                Else
                    lvJobList.SelectedItems(0).Remove()
                End If


                If lvJobList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvJobList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvJobList.Items.Count - 1
                    lvJobList.Items(intSelectedIndex).Selected = True
                    lvJobList.EnsureVisible(intSelectedIndex)
                ElseIf lvJobList.Items.Count <> 0 Then
                    lvJobList.Items(intSelectedIndex).Selected = True
                    lvJobList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvJobList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvJobList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Job from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobList.Select()
            Exit Sub
        End If
        Dim frm As New frmJobs_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvJobList.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvJobList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvJobList.Items(intSelectedIndex).Selected = True
            lvJobList.EnsureVisible(intSelectedIndex)
            lvJobList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmJobs_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'Nilay (18-Mar-2015) -- Start
            'Enhancement-
            'RemoveHandler radJobGroup.CheckedChanged, AddressOf radJobGroup_CheckedChanged
            RemoveHandler chkJobGroup.CheckedChanged, AddressOf chkJobGroup_CheckedChanged
            'Nilay (18-Mar-2015) -- End

            cboGrade.SelectedValue = 0
            cboJobGroup.SelectedValue = 0
            'cboJobHead.SelectedValue = 0
            cboReportTo.SelectedValue = 0
            cboSection.SelectedValue = 0
            cboUnit.SelectedValue = 0
            txtName.Text = ""
            nudPosition.Value = 0
            dtpCreateDate.Checked = False
            dtpTerminateDate.Checked = False
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboTeam.SelectedValue = 0

            chkJobGroup.Checked = False 'Nilay (18-Mar-2015) -- radJobGroup.Checked = False
            chkSection.Checked = False 'Nilay (18-Mar-2015) -- radSection.Checked = False
            chkUnit.Checked = False 'Nilay (18-Mar-2015) -- radUnit.Checked = False
            chkTeam.Checked = False 'Nilay (18-Mar-2015) -- radTeam.Checked = False
            chkGrade.Checked = False 'Nilay (18-Mar-2015) -- radGrade.Checked = False


            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            chkBranch.Checked = False
            chkDepartmentGroup.Checked = False
            chkSectionGroup.Checked = False
            chkUnitGroup.Checked = False
            chkClass.Checked = False
            ChkGradeLevel.Checked = False
            cboBranch.SelectedValue = 0
            cboDeparmentGrp.SelectedValue = 0
            cboSectionGrp.SelectedValue = 0
            cboUnitGrp.SelectedValue = 0
            cboClass.SelectedValue = 0
            cboGradeLevel.SelectedValue = 0
            'Shani(18-JUN-2016) -- End


            cboGrade.Enabled = True
            cboJobGroup.Enabled = True
            cboReportTo.Enabled = True
            cboSection.Enabled = True
            cboTeam.Enabled = True
            cboUnit.Enabled = True
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            chkDepartment.Checked = False
            chkClassGroup.Checked = False
            cboDepartment.SelectedValue = 0
            cboClassGroup.SelectedValue = 0
            cboReportTo.SelectedValue = 0
            cboInDirectReportTo.SelectedValue = 0
            'Pinkal (03-Dec-2015) -- End


            Call fillList()
            'Nilay (18-Mar-2015) -- Start
            'Enhancement-
            'AddHandler radJobGroup.CheckedChanged, AddressOf radJobGroup_CheckedChanged
            AddHandler chkJobGroup.CheckedChanged, AddressOf chkJobGroup_CheckedChanged
            'Nilay (18-Mar-2015) -- End
            objbtnReset.ShowResult(CStr(lvJobList.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
            objbtnSearch.ShowResult(CStr(lvJobList.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    Private Sub objbtnSearchDirectReportTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDirectReportTo.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboReportTo.ValueMember
                .DisplayMember = cboReportTo.DisplayMember
                .DataSource = CType(cboReportTo.DataSource, DataTable)
                .CodeMember = ""
            End With
            If frm.DisplayDialog Then
                cboReportTo.SelectedValue = frm.SelectedValue
                cboReportTo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchDirectReportTo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchInDirectReportTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInDirectReportTo.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboInDirectReportTo.ValueMember
                .DisplayMember = cboInDirectReportTo.DisplayMember
                .DataSource = CType(cboInDirectReportTo.DataSource, DataTable)
                .CodeMember = ""
            End With
            If frm.DisplayDialog Then
                cboInDirectReportTo.SelectedValue = frm.SelectedValue
                cboInDirectReportTo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchInDirectReportTo_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- End



#End Region

    'Nilay (18-Mar-2015) -- Start
    'Enhancement-
#Region "CheckBox Controls"
    Private Sub chkJobGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkJobGroup.CheckedChanged, _
                                                                                                        chkSection.CheckedChanged, _
                                                                                                        chkUnit.CheckedChanged, _
                                                                                                        chkTeam.CheckedChanged, _
                                                                                                        chkGrade.CheckedChanged, _
                                                                                                        chkDepartment.CheckedChanged, _
                                                                                                        chkClassGroup.CheckedChanged, _
                                                                                                        chkBranch.CheckedChanged, _
                                                                                                        chkDepartmentGroup.CheckedChanged, _
                                                                                                        chkSectionGroup.CheckedChanged, _
                                                                                                        chkUnitGroup.CheckedChanged, _
                                                                                                        chkClass.CheckedChanged, _
                                                                                                        ChkGradeLevel.CheckedChanged

        'Shani(18-JUN-2016) --ADD-->[jobbranchunkid,jobdepartmentgrpunkid,jobsectiongrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid]


        Try
            Select Case CType(sender, CheckBox).Name.ToUpper
                Case chkJobGroup.Name.ToUpper
                    If chkJobGroup.Checked = False Then cboJobGroup.SelectedValue = 0
                Case chkSection.Name.ToUpper
                    If chkSection.Checked = False Then cboSection.SelectedValue = 0
                Case chkUnit.Name.ToUpper
                    If chkUnit.Checked = False Then cboUnit.SelectedValue = 0
                Case chkTeam.Name.ToUpper
                    If chkTeam.Checked = False Then cboTeam.SelectedValue = 0
                Case chkGrade.Name.ToUpper
                    If chkGrade.Checked = False Then cboGrade.SelectedValue = 0

                    'Pinkal (03-Dec-2015) -- Start
                    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
                Case chkDepartment.Name.ToUpper
                    If chkDepartment.Checked = False Then cboDepartment.SelectedValue = 0
                Case chkClassGroup.Name.ToUpper
                    If chkClassGroup.Checked = False Then cboClassGroup.SelectedValue = 0
                    'Pinkal (03-Dec-2015) -- End

                    'Shani(18-JUN-2016) -- Start
                    'Enhancement : Add Allocation Column in Job master[Rutta]
                Case chkBranch.Name.ToUpper
                    If chkBranch.Checked = False Then cboBranch.SelectedValue = 0
                Case chkDepartmentGroup.Name.ToUpper
                    If chkDepartmentGroup.Checked = False Then cboDeparmentGrp.SelectedValue = 0
                Case chkSectionGroup.Name.ToUpper
                    If chkSectionGroup.Checked = False Then cboSectionGrp.SelectedValue = 0
                Case chkUnitGroup.Name.ToUpper
                    If chkUnitGroup.Checked = False Then cboUnitGrp.SelectedValue = 0
                Case chkClass.Name.ToUpper
                    If chkClass.Checked = False Then cboClass.SelectedValue = 0
                Case ChkGradeLevel.Name.ToUpper
                    If ChkGradeLevel.Checked = False Then cboGradeLevel.SelectedValue = 0
                    'Shani(18-JUN-2016) -- End

            End Select


            'If chkJobGroup.Checked = True Then
            '    cboJobGroup.Enabled = True
            '    If chkSection.Checked = True Then
            '        cboSection.Enabled = True
            '    ElseIf chkUnit.Checked = True Then
            '        cboUnit.Enabled = True
            '    ElseIf chkTeam.Checked = True Then
            '        cboTeam.Enabled = True
            '    ElseIf chkGrade.Checked = True Then
            '        cboGrade.Enabled = True
            '    End If
            'ElseIf chkSection.Checked = True Then
            '    cboSection.Enabled = True
            '    If chkJobGroup.Checked = True Then
            '        cboJobGroup.Enabled = True
            '    ElseIf chkUnit.Checked = True Then
            '        cboUnit.Enabled = True
            '    ElseIf chkTeam.Checked = True Then
            '        cboTeam.Enabled = True
            '    ElseIf chkGrade.Checked = True Then
            '        cboGrade.Enabled = True
            '    End If
            'ElseIf chkUnit.Checked = True Then
            '    cboUnit.Enabled = True
            '    If chkJobGroup.Checked = True Then
            '        cboJobGroup.Enabled = True
            '    ElseIf chkSection.Checked = True Then
            '        cboSection.Enabled = True
            '    ElseIf chkTeam.Checked = True Then
            '        cboTeam.Enabled = True
            '    ElseIf chkGrade.Checked = True Then
            '        cboGrade.Enabled = True
            '    End If
            'ElseIf chkTeam.Checked = True Then
            '    cboTeam.Enabled = True
            '    If chkJobGroup.Checked = True Then
            '        cboJobGroup.Enabled = True
            '    ElseIf chkSection.Checked = True Then
            '        cboSection.Enabled = True
            '    ElseIf chkUnit.Checked = True Then
            '        cboUnit.Enabled = True
            '    ElseIf chkGrade.Checked = True Then
            '        cboGrade.Enabled = True
            '    End If
            'ElseIf chkGrade.Checked = True Then
            '    cboGrade.Enabled = True
            '    If chkJobGroup.Checked = True Then
            '        cboJobGroup.Enabled = True
            '    ElseIf chkSection.Checked = True Then
            '        cboSection.Enabled = True
            '    ElseIf chkUnit.Checked = True Then
            '        cboUnit.Enabled = True
            '    ElseIf chkTeam.Checked = True Then
            '        cboTeam.Enabled = True
            '    End If
            'End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "chkJobGroup_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Nilay (18-Mar-2015) -- End

#Region " Other Controls "

    'Nilay (18-Mar-2015) -- Start
    'Enhancement-
    'Private Sub radJobGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If radJobGroup.Checked = True Then
    '            cboJobGroup.Enabled = True
    '            'cboJobGroup.SelectedValue = 0
    '            cboGrade.Enabled = False
    '            cboGrade.SelectedValue = 0
    '            'cboJobHead.Enabled = False
    '            'cboJobHead.SelectedValue = 0
    '            cboSection.Enabled = False
    '            cboSection.SelectedValue = 0
    '            cboUnit.Enabled = False
    '            cboUnit.SelectedValue = 0
    '            'ElseIf radJobHead.Checked = True Then
    '            '    cboJobGroup.Enabled = False
    '            '    cboJobGroup.SelectedValue = 0
    '            '    cboGrade.Enabled = False
    '            '    cboGrade.SelectedValue = 0
    '            '    cboJobHead.Enabled = True
    '            '    'cboJobHead.SelectedValue = 0
    '            '    cboSection.Enabled = False
    '            '    cboSection.SelectedValue = 0
    '            '    cboUnit.Enabled = False
    '            '    cboUnit.SelectedValue = 0
    '            'S.SANDEEP [ 07 NOV 2011 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            cboTeam.Enabled = False
    '            cboTeam.SelectedValue = 0
    '            'S.SANDEEP [ 07 NOV 2011 ] -- END
    '        ElseIf radSection.Checked = True Then
    '    cboJobGroup.Enabled = False
    '    cboJobGroup.SelectedValue = 0
    '    cboGrade.Enabled = False
    '    cboGrade.SelectedValue = 0
    '            'cboJobHead.Enabled = False
    '            'cboJobHead.SelectedValue = 0
    '            cboSection.Enabled = True
    '            'cboSection.SelectedValue = 0
    '            cboUnit.Enabled = False
    '            cboUnit.SelectedValue = 0
    '            'S.SANDEEP [ 07 NOV 2011 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            cboTeam.Enabled = False
    '            cboTeam.SelectedValue = 0
    '            'S.SANDEEP [ 07 NOV 2011 ] -- END
    '        ElseIf radGrade.Checked = True Then
    '            cboJobGroup.Enabled = False
    '            cboJobGroup.SelectedValue = 0
    '            cboGrade.Enabled = True
    '            'cboGrade.SelectedValue = 0
    '            'cboJobHead.Enabled = False
    '    'cboJobHead.SelectedValue = 0
    '    cboSection.Enabled = False
    '    cboSection.SelectedValue = 0
    '    cboUnit.Enabled = False
    '    cboUnit.SelectedValue = 0
    '            'S.SANDEEP [ 07 NOV 2011 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            cboTeam.Enabled = False
    '            cboTeam.SelectedValue = 0
    '            'S.SANDEEP [ 07 NOV 2011 ] -- END
    '        ElseIf radUnit.Checked = True Then
    '            cboJobGroup.Enabled = False
    '            cboJobGroup.SelectedValue = 0
    '            cboGrade.Enabled = False
    '            cboGrade.SelectedValue = 0
    '            'cboJobHead.Enabled = False
    '            'cboJobHead.SelectedValue = 0
    '            cboSection.Enabled = False
    'cboSection.SelectedValue = 0
    '            cboUnit.Enabled = True
    '            'cboUnit.SelectedValue = 0
    '            'S.SANDEEP [ 07 NOV 2011 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            cboTeam.Enabled = False
    '            cboTeam.SelectedValue = 0
    '        ElseIf radTeam.Checked = True Then
    '            cboJobGroup.Enabled = False
    '            cboJobGroup.SelectedValue = 0
    '            cboGrade.Enabled = False
    'cboGrade.SelectedValue = 0
    '            cboSection.Enabled = False
    '            cboSection.SelectedValue = 0
    '            cboUnit.Enabled = False
    '            cboTeam.Enabled = True
    '            'S.SANDEEP [ 07 NOV 2011 ] -- END
    '        End If
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "radJobGroup_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Nilay (18-Mar-2015) -- End



    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Try
            If CInt(cboGrade.SelectedValue) > 0 Then
                chkGrade.Checked = True 'Nilay (18-Mar-2015) -- radGrade.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSection_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSection.SelectedIndexChanged
        Try
            If CInt(cboSection.SelectedValue) > 0 Then
                chkSection.Checked = True  'Nilay (18-Mar-2015) -- radSection.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSection_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
        Try
            If CInt(cboUnit.SelectedValue) > 0 Then
                chkUnit.Checked = True 'Nilay (18-Mar-2015) -- radUnit.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnit_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboJobHead_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If CInt(cboJobHead.SelectedValue) > 0 Then
    '            radJobHead.Checked = True
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboJobHead_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
        Try
            If CInt(cboJobGroup.SelectedValue) > 0 Then
                chkJobGroup.Checked = True 'Nilay (18-Mar-2015) -- radJobGroup.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTeam.SelectedIndexChanged
        Try
            If CInt(cboTeam.SelectedValue) > 0 Then
                chkTeam.Checked = True 'Nilay (18-Mar-2015) -- radTeam.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTeam_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If CInt(cboDepartment.SelectedValue) > 0 Then
                chkDepartment.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDepartment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Try
            If CInt(cboClassGroup.SelectedValue) > 0 Then
                chkClassGroup.Checked = True 'Nilay (18-Mar-2015) -- radTeam.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportTo.SelectedIndexChanged
        Try
            Dim dtTable As DataTable = CType(cboReportTo.DataSource, DataTable).Copy
            If CInt(cboReportTo.SelectedValue) > 0 Then
                dtTable = New DataView(dtTable, "jobunkid <>" & CInt(cboReportTo.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboInDirectReportTo
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
            End With
            cboInDirectReportTo.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportTo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- End

    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]
    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Try
            If CInt(cboBranch.SelectedValue) > 0 Then
                chkBranch.Checked = True
            Else
                chkBranch.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBranch_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDeparmentGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDeparmentGrp.SelectedIndexChanged
        Try
            If CInt(cboDeparmentGrp.SelectedValue) > 0 Then
                chkDepartmentGroup.Checked = True
            Else
                chkDepartmentGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeparmentGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSectionGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSectionGrp.SelectedIndexChanged
        Try
            If CInt(cboSectionGrp.SelectedValue) > 0 Then
                chkSectionGroup.Checked = True
            Else
                chkSectionGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSectionGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnitGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnitGrp.SelectedIndexChanged
        Try
            If CInt(cboUnitGrp.SelectedValue) > 0 Then
                chkUnitGroup.Checked = True
            Else
                chkUnitGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnitGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboClass_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClass.SelectedIndexChanged
        Try
            If CInt(cboClass.SelectedValue) > 0 Then
                chkClass.Checked = True
            Else
                chkClass.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClass_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Try
            If CInt(cboGradeLevel.SelectedValue) > 0 Then
                ChkGradeLevel.Checked = True
            Else
                ChkGradeLevel.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Shani(18-JUN-2016) -- End

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
            Me.lblTerminateDate.Text = Language._Object.getCaption(Me.lblTerminateDate.Name, Me.lblTerminateDate.Text)
            Me.lblCreateDate.Text = Language._Object.getCaption(Me.lblCreateDate.Name, Me.lblCreateDate.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblReportTo.Text = Language._Object.getCaption(Me.lblReportTo.Name, Me.lblReportTo.Text)
            Me.colhJobsBy.Text = Language._Object.getCaption(CStr(Me.colhJobsBy.Tag), Me.colhJobsBy.Text)
            Me.colhJobCode.Text = Language._Object.getCaption(CStr(Me.colhJobCode.Tag), Me.colhJobCode.Text)
            Me.colhJobName.Text = Language._Object.getCaption(CStr(Me.colhJobName.Tag), Me.colhJobName.Text)
            Me.colhCreateDate.Text = Language._Object.getCaption(CStr(Me.colhCreateDate.Tag), Me.colhCreateDate.Text)
            Me.colhTerminateDate.Text = Language._Object.getCaption(CStr(Me.colhTerminateDate.Tag), Me.colhTerminateDate.Text)
            Me.colhPosition.Text = Language._Object.getCaption(CStr(Me.colhPosition.Tag), Me.colhPosition.Text)
            Me.colhReportTo.Text = Language._Object.getCaption(CStr(Me.colhReportTo.Tag), Me.colhReportTo.Text)
            Me.colhJobLevel.Text = Language._Object.getCaption(CStr(Me.colhJobLevel.Tag), Me.colhJobLevel.Text)
            Me.chkJobGroup.Text = Language._Object.getCaption(Me.chkJobGroup.Name, Me.chkJobGroup.Text)
            Me.chkUnit.Text = Language._Object.getCaption(Me.chkUnit.Name, Me.chkUnit.Text)
            Me.chkTeam.Text = Language._Object.getCaption(Me.chkTeam.Name, Me.chkTeam.Text)
            Me.chkSection.Text = Language._Object.getCaption(Me.chkSection.Name, Me.chkSection.Text)
            Me.chkGrade.Text = Language._Object.getCaption(Me.chkGrade.Name, Me.chkGrade.Text)
            Me.colhJobGruop.Text = Language._Object.getCaption(CStr(Me.colhJobGruop.Tag), Me.colhJobGruop.Text)
            Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
            Me.colhUnit.Text = Language._Object.getCaption(CStr(Me.colhUnit.Tag), Me.colhUnit.Text)
            Me.colhTeam.Text = Language._Object.getCaption(CStr(Me.colhTeam.Tag), Me.colhTeam.Text)
            Me.colhGrade.Text = Language._Object.getCaption(CStr(Me.colhGrade.Tag), Me.colhGrade.Text)
            Me.chkDepartment.Text = Language._Object.getCaption(Me.chkDepartment.Name, Me.chkDepartment.Text)
            Me.chkClassGroup.Text = Language._Object.getCaption(Me.chkClassGroup.Name, Me.chkClassGroup.Text)
            Me.LblInDirectReportTo.Text = Language._Object.getCaption(Me.LblInDirectReportTo.Name, Me.LblInDirectReportTo.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhClassGroup.Text = Language._Object.getCaption(CStr(Me.colhClassGroup.Tag), Me.colhClassGroup.Text)
            Me.colhBranch.Text = Language._Object.getCaption(CStr(Me.colhBranch.Tag), Me.colhBranch.Text)
            Me.colhDepartmentGrp.Text = Language._Object.getCaption(CStr(Me.colhDepartmentGrp.Tag), Me.colhDepartmentGrp.Text)
            Me.colhsectionGrp.Text = Language._Object.getCaption(CStr(Me.colhsectionGrp.Tag), Me.colhsectionGrp.Text)
            Me.colhUnitGrp.Text = Language._Object.getCaption(CStr(Me.colhUnitGrp.Tag), Me.colhUnitGrp.Text)
            Me.colhClass.Text = Language._Object.getCaption(CStr(Me.colhClass.Tag), Me.colhClass.Text)
            Me.colhGradeLevel.Text = Language._Object.getCaption(CStr(Me.colhGradeLevel.Tag), Me.colhGradeLevel.Text)
            Me.ChkGradeLevel.Text = Language._Object.getCaption(Me.ChkGradeLevel.Name, Me.ChkGradeLevel.Text)
            Me.chkBranch.Text = Language._Object.getCaption(Me.chkBranch.Name, Me.chkBranch.Text)
            Me.chkUnitGroup.Text = Language._Object.getCaption(Me.chkUnitGroup.Name, Me.chkUnitGroup.Text)
            Me.chkSectionGroup.Text = Language._Object.getCaption(Me.chkSectionGroup.Name, Me.chkSectionGroup.Text)
            Me.chkClass.Text = Language._Object.getCaption(Me.chkClass.Name, Me.chkClass.Text)
            Me.chkDepartmentGroup.Text = Language._Object.getCaption(Me.chkDepartmentGroup.Name, Me.chkDepartmentGroup.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Job from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Level")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Job?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class