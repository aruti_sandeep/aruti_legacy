﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmJobGroupList

#Region " Private Varaibles "
    Private objJobGroup As clsJobGroup
    Private ReadOnly mstrModuleName As String = "frmJobGroupList"
#End Region

#Region " Private Function "

    'Sandeep [ 29 Oct 2010 ] -- Start
    'Privilege Changes
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddJobGroup
            btnEdit.Enabled = User._Object.Privilege._EditJobGroup
            btnDelete.Enabled = User._Object.Privilege._DeleteJobGroup
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 29 Oct 2010 ] -- End

    Private Sub fillList()
        Dim dsAccess As New DataSet
        Try

            If User._Object.Privilege._AllowToViewJobGroupList = True Then    'Pinkal (09-Jul-2012) -- Start

                dsAccess = objJobGroup.GetList("ClassGroup")

                Dim lvItem As ListViewItem

                lvJobGroupList.Items.Clear()
                For Each drRow As DataRow In dsAccess.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("code").ToString
                    lvItem.Tag = drRow("jobgroupunkid")
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvJobGroupList.Items.Add(lvItem)
                Next

                If lvJobGroupList.Items.Count > 16 Then
                    colhDescription.Width = 378 - 18
                Else
                    colhDescription.Width = 378
                End If

            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAccess.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmJobGroupList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvJobGroupList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobGroupList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobGroupList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobGroupList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobGroupList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objJobGroup = New clsJobGroup
        Try

            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sandeep [ 29 Oct 2010 ] -- Start
            'Privilege Changes
            Call SetVisibility()
            'Sandeep [ 29 Oct 2010 ] -- End

            Call fillList()

            If lvJobGroupList.Items.Count > 0 Then lvJobGroupList.Items(0).Selected = True
            lvJobGroupList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAirlineList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobGroupList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objJobGroup = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJobGroup.SetMessages()
            objfrm._Other_ModuleNames = "clsJobGroup"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvJobGroupList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Job Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobGroupList.Select()
            Exit Sub
        End If
        'If objJobGroup.isUsed(CInt(lvJobGroupList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Job Group. Reason: This Job Group is in use."), enMsgBoxStyle.Information) '?2
        '    lvJobGroupList.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvJobGroupList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Job Group?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objJobGroup._FormName = mstrModuleName
                objJobGroup._LoginEmployeeunkid = 0
                objJobGroup._ClientIP = getIP()
                objJobGroup._HostName = getHostName()
                objJobGroup._FromWeb = False
                objJobGroup._AuditUserId = User._Object._Userunkid
objJobGroup._CompanyUnkid = Company._Object._Companyunkid
                objJobGroup._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objJobGroup.Delete(CInt(lvJobGroupList.SelectedItems(0).Tag))
                If objJobGroup._Message <> "" Then
                    eZeeMsgBox.Show(objJobGroup._Message, enMsgBoxStyle.Information)
                Else
                    lvJobGroupList.SelectedItems(0).Remove()
                End If


                If lvJobGroupList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvJobGroupList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvJobGroupList.Items.Count - 1
                    lvJobGroupList.Items(intSelectedIndex).Selected = True
                    lvJobGroupList.EnsureVisible(intSelectedIndex)
                ElseIf lvJobGroupList.Items.Count <> 0 Then
                    lvJobGroupList.Items(intSelectedIndex).Selected = True
                    lvJobGroupList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvJobGroupList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvJobGroupList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Job Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobGroupList.Select()
            Exit Sub
        End If
        Dim frm As New frmJobGroup_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvJobGroupList.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvJobGroupList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvJobGroupList.Items(intSelectedIndex).Selected = True
            lvJobGroupList.EnsureVisible(intSelectedIndex)
            lvJobGroupList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmJobGroup_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)



        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Job Group from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Job Group?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class