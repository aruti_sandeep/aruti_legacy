﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMapExpenses_Approver

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMapExpenses_Approver"
    Private mintExpenseTypeId As Integer
    Private mdtMapping As DataTable
    Private mintExApproverId As Integer
    Private mstrExApproverName As String = String.Empty
    Private mstrExApproverLevel As String = String.Empty
    Private objExMapping As clsExpenseApproverMapping
    Private mblnPromptMessage As Boolean = False

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal iExApprName As String, _
                                  ByVal iExApprLevel As String, _
                                  ByVal iExTypeId As Integer, _
                                  ByVal iExApprId As Integer, _
                                  ByRef mdtTab As DataTable) As Boolean
        Try
            objlblApprover.Text = iExApprName
            objlblApproverLevel.Text = iExApprLevel
            mintExpenseTypeId = iExTypeId
            mintExApproverId = iExApprId
            mdtMapping = mdtTab
            Me.ShowDialog()
            mdtTab = mdtMapping
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmMapExpenses_Approver_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objExMapping = New clsExpenseApproverMapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMapExpenses_Approver_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_Grid()
        Try
            If mdtMapping Is Nothing Then
                objExMapping._UserId = User._Object._Userunkid
                objExMapping._ExpenseTypeId = mintExpenseTypeId
                objExMapping._ExApproverUnkid = mintExApproverId
                mdtMapping = objExMapping._DataTable.Copy
            End If

            dgvExpense.AutoGenerateColumns = False

            objdgcolhSelect.DataPropertyName = "isassigned"
            objdgcolhExpenseId.DataPropertyName = "expensemappingunkid"
            dgcolhCode.DataPropertyName = "code"
            dgcolhName.DataPropertyName = "name"
            dgcolhUoM.DataPropertyName = "UoM"

            dgvExpense.DataSource = mdtMapping

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            Dim dtmp() As DataRow = Nothing
            If mblnPromptMessage = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "You have usassigned expenses from the selected approver due to this you need to set the void reason for unassigning expense(s) from this approver."), enMsgBoxStyle.Information)
                Dim frm As New frmReasonSelection
                Dim sVoidReason As String = String.Empty

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                frm.Dispose()
                If sVoidReason.Trim.Length <= 0 Then Exit Sub

                dtmp = mdtMapping.Select("AUD = 'D'")
                If dtmp.Length > 0 Then
                    For i As Integer = 0 To dtmp.Length - 1
                        dtmp(i).Item("isvoid") = True
                        dtmp(i).Item("voiduserunkid") = User._Object._Userunkid
                        dtmp(i).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtmp(i).Item("voidreason") = sVoidReason
                    Next
                    mdtMapping.AcceptChanges()
                End If
            Else
                dtmp = mdtMapping.Select("isassigned = true")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please tick atleast one expense to map with approver."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            Call btnCancel_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            RemoveHandler dgvExpense.CellContentClick, AddressOf dgvExpense_CellContentClick
            For Each dr As DataRow In mdtMapping.Rows
                dr.Item("isassigned") = CBool(objchkSelectAll.CheckState)
            Next
            dgvExpense.Refresh()
            AddHandler dgvExpense.CellContentClick, AddressOf dgvExpense_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvExpense_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExpense.CellContentClick
        Try
            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            If e.ColumnIndex = objdgcolhSelect.Index Then
                If Me.dgvExpense.IsCurrentCellDirty Then
                    Me.dgvExpense.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = mdtMapping.Select("isassigned = true", "")
                If drRow.Length > 0 Then
                    If mdtMapping.Rows.Count = drRow.Length Then
                        objchkSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkSelectAll.CheckState = CheckState.Unchecked
                End If

                If Convert.ToInt32(dgvExpense.Rows(e.RowIndex).Cells(objdgcolhExpenseId.Index).Value) > 0 Then
                    If Convert.ToBoolean(dgvExpense.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = False Then
                        mdtMapping.Rows(e.RowIndex).Item("AUD") = "D"
                    ElseIf Convert.ToBoolean(dgvExpense.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = False Then
                        mdtMapping.Rows(e.RowIndex).Item("AUD") = ""
                    End If
                Else
                    If Convert.ToBoolean(dgvExpense.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = True Then
                        mdtMapping.Rows(e.RowIndex).Item("AUD") = "A"
                    ElseIf Convert.ToBoolean(dgvExpense.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value) = False Then
                        mdtMapping.Rows(e.RowIndex).Item("AUD") = ""
                    End If
                End If
                mdtMapping.AcceptChanges()
                drRow = mdtMapping.Select("AUD = 'D'")
                If drRow.Length > 0 Then
                    mblnPromptMessage = True
                Else
                    mblnPromptMessage = False
                End If
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvExpense_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbLeavedetail.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeavedetail.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbLeavedetail.Text = Language._Object.getCaption(Me.gbLeavedetail.Name, Me.gbLeavedetail.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.Name, Me.lblApproverLevel.Text)
            Me.objchkSelectAll.Text = Language._Object.getCaption(Me.objchkSelectAll.Name, Me.objchkSelectAll.Text)
            Me.dgcolhCode.HeaderText = Language._Object.getCaption(Me.dgcolhCode.Name, Me.dgcolhCode.HeaderText)
            Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)
            Me.dgcolhUoM.HeaderText = Language._Object.getCaption(Me.dgcolhUoM.Name, Me.dgcolhUoM.HeaderText)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class