﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpenseCostingAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExpenseCostingAddEdit))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbExpenseCosting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtCosting = New eZee.TextBox.NumericTextBox
        Me.objbtnSearchRoute = New eZee.Common.eZeeGradientButton
        Me.dtpEffDate = New System.Windows.Forms.DateTimePicker
        Me.cboSecRoute = New System.Windows.Forms.ComboBox
        Me.lblSector_Route = New System.Windows.Forms.Label
        Me.lblCost = New System.Windows.Forms.Label
        Me.lblEffDate = New System.Windows.Forms.Label
        Me.objFooter.SuspendLayout()
        Me.gbExpenseCosting.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 121)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(362, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(150, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(253, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbExpenseCosting
        '
        Me.gbExpenseCosting.BorderColor = System.Drawing.Color.Black
        Me.gbExpenseCosting.Checked = False
        Me.gbExpenseCosting.CollapseAllExceptThis = False
        Me.gbExpenseCosting.CollapsedHoverImage = Nothing
        Me.gbExpenseCosting.CollapsedNormalImage = Nothing
        Me.gbExpenseCosting.CollapsedPressedImage = Nothing
        Me.gbExpenseCosting.CollapseOnLoad = False
        Me.gbExpenseCosting.Controls.Add(Me.txtCosting)
        Me.gbExpenseCosting.Controls.Add(Me.objbtnSearchRoute)
        Me.gbExpenseCosting.Controls.Add(Me.dtpEffDate)
        Me.gbExpenseCosting.Controls.Add(Me.cboSecRoute)
        Me.gbExpenseCosting.Controls.Add(Me.lblSector_Route)
        Me.gbExpenseCosting.Controls.Add(Me.lblCost)
        Me.gbExpenseCosting.Controls.Add(Me.lblEffDate)
        Me.gbExpenseCosting.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbExpenseCosting.ExpandedHoverImage = Nothing
        Me.gbExpenseCosting.ExpandedNormalImage = Nothing
        Me.gbExpenseCosting.ExpandedPressedImage = Nothing
        Me.gbExpenseCosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExpenseCosting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExpenseCosting.HeaderHeight = 25
        Me.gbExpenseCosting.HeaderMessage = ""
        Me.gbExpenseCosting.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExpenseCosting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExpenseCosting.HeightOnCollapse = 0
        Me.gbExpenseCosting.LeftTextSpace = 0
        Me.gbExpenseCosting.Location = New System.Drawing.Point(0, 0)
        Me.gbExpenseCosting.Name = "gbExpenseCosting"
        Me.gbExpenseCosting.OpenHeight = 300
        Me.gbExpenseCosting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExpenseCosting.ShowBorder = True
        Me.gbExpenseCosting.ShowCheckBox = False
        Me.gbExpenseCosting.ShowCollapseButton = False
        Me.gbExpenseCosting.ShowDefaultBorderColor = True
        Me.gbExpenseCosting.ShowDownButton = False
        Me.gbExpenseCosting.ShowHeader = True
        Me.gbExpenseCosting.Size = New System.Drawing.Size(362, 121)
        Me.gbExpenseCosting.TabIndex = 4
        Me.gbExpenseCosting.Temp = 0
        Me.gbExpenseCosting.Text = "Costing Information"
        Me.gbExpenseCosting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCosting
        '
        Me.txtCosting.AllowNegative = False
        Me.txtCosting.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCosting.DigitsInGroup = 0
        Me.txtCosting.Flags = 65536
        Me.txtCosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCosting.Location = New System.Drawing.Point(118, 90)
        Me.txtCosting.MaxDecimalPlaces = 4
        Me.txtCosting.MaxWholeDigits = 9
        Me.txtCosting.Name = "txtCosting"
        Me.txtCosting.Prefix = ""
        Me.txtCosting.RangeMax = 1.7976931348623157E+308
        Me.txtCosting.RangeMin = -1.7976931348623157E+308
        Me.txtCosting.Size = New System.Drawing.Size(205, 21)
        Me.txtCosting.TabIndex = 301
        Me.txtCosting.Text = "0"
        Me.txtCosting.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchRoute
        '
        Me.objbtnSearchRoute.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRoute.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRoute.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRoute.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRoute.BorderSelected = False
        Me.objbtnSearchRoute.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRoute.Image = CType(resources.GetObject("objbtnSearchRoute.Image"), System.Drawing.Image)
        Me.objbtnSearchRoute.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRoute.Location = New System.Drawing.Point(329, 34)
        Me.objbtnSearchRoute.Name = "objbtnSearchRoute"
        Me.objbtnSearchRoute.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRoute.TabIndex = 300
        '
        'dtpEffDate
        '
        Me.dtpEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffDate.Location = New System.Drawing.Point(118, 62)
        Me.dtpEffDate.Name = "dtpEffDate"
        Me.dtpEffDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpEffDate.TabIndex = 299
        '
        'cboSecRoute
        '
        Me.cboSecRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSecRoute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSecRoute.FormattingEnabled = True
        Me.cboSecRoute.Location = New System.Drawing.Point(118, 34)
        Me.cboSecRoute.Name = "cboSecRoute"
        Me.cboSecRoute.Size = New System.Drawing.Size(205, 21)
        Me.cboSecRoute.TabIndex = 294
        '
        'lblSector_Route
        '
        Me.lblSector_Route.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSector_Route.Location = New System.Drawing.Point(12, 36)
        Me.lblSector_Route.Name = "lblSector_Route"
        Me.lblSector_Route.Size = New System.Drawing.Size(100, 17)
        Me.lblSector_Route.TabIndex = 295
        Me.lblSector_Route.Text = "Sector/ Route"
        '
        'lblCost
        '
        Me.lblCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCost.Location = New System.Drawing.Point(12, 90)
        Me.lblCost.Name = "lblCost"
        Me.lblCost.Size = New System.Drawing.Size(100, 17)
        Me.lblCost.TabIndex = 292
        Me.lblCost.Text = "Costing"
        '
        'lblEffDate
        '
        Me.lblEffDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffDate.Location = New System.Drawing.Point(12, 63)
        Me.lblEffDate.Name = "lblEffDate"
        Me.lblEffDate.Size = New System.Drawing.Size(100, 17)
        Me.lblEffDate.TabIndex = 290
        Me.lblEffDate.Text = "Effective Date"
        '
        'frmExpenseCostingAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 176)
        Me.Controls.Add(Me.gbExpenseCosting)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseCostingAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Sector/Route Cost Add/Edit"
        Me.objFooter.ResumeLayout(False)
        Me.gbExpenseCosting.ResumeLayout(False)
        Me.gbExpenseCosting.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbExpenseCosting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpEffDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboSecRoute As System.Windows.Forms.ComboBox
    Friend WithEvents lblSector_Route As System.Windows.Forms.Label
    Friend WithEvents lblCost As System.Windows.Forms.Label
    Friend WithEvents lblEffDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchRoute As eZee.Common.eZeeGradientButton
    Friend WithEvents txtCosting As eZee.TextBox.NumericTextBox
End Class
