﻿'Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports System.IO
Imports System
Imports System.Web

#End Region

Public Class frmClaims_RequestAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmClaims_RequestAddEdit"
    Private objClaimMaster As clsclaim_request_master
    Private objClaimTran As clsclaim_request_tran
    Private mintClaimRequestMasterId As Integer = 0
    Private mdtTran As DataTable
    Private mdView As DataView
    Private iRow As DataRow() = Nothing
    Private imgEdit As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.edit)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.remove)
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintEmployeeID As Integer = 0
    Private mintLeaveTypeId As Integer = 0
    Private mintLeaveFormID As Integer = -1
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private mintExpenseCategoryID As Integer = 0
    Private mblnIsFromLeave As Boolean = False
    Private mblnIsLeaveEncashment As Boolean = False
    Private mdtLeave As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mblnIsLeaveForm As Boolean = False
    Dim mdtFinalclaimTransaction As DataTable = Nothing
    Dim mdtAttachement As DataTable = Nothing

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Dim mblnIsClaimFormBudgetMandatory As Boolean = False
    'Pinkal (20-Nov-2018) -- End



    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    Dim mintBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End


    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mintClaimRequestExpenseTranId As Integer = 0
    Dim mstrClaimRequestExpenseGUID As String = ""
    'Pinkal (07-Mar-2019) -- End


    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Dim mstrP2PToken As String = ""
    'Pinkal (29-Aug-2019) -- End


    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    Dim LstClaimEmailList As New List(Of clsEmailCollection)
    'Pinkal (20-May-2022) -- End


    'Pinkal (10-Jun-2022) -- Start
    'Enhancement KBC :(AC2-558) Provide setting on expense master to stop new imprest request if a user has unretired imprest or a retirement that's pending approval.
    Private mblnDoNotAllowToApplyForImprestIfUnretired As Boolean = False
    'Pinkal (10-Jun-2022) -- End


#End Region

#Region "Properties"

    Public Property _EmployeeId() As Integer
        Get
            Return mintEmployeeID
        End Get
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    Public Property _LeaveTypeId() As Integer
        Get
            Return mintLeaveTypeId
        End Get
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    Public Property _ExpenseCategoryID() As Integer
        Get
            Return mintExpenseCategoryID
        End Get
        Set(ByVal value As Integer)
            mintExpenseCategoryID = value
        End Set
    End Property

    Public Property _LeaveFormID() As Integer
        Get
            Return mintLeaveFormID
        End Get
        Set(ByVal value As Integer)
            mintLeaveFormID = value
        End Set
    End Property

    Public Property _dtExpense() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _IsFromLeave() As Boolean
        Get
            Return mblnIsFromLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromLeave = value
        End Set
    End Property

    Public WriteOnly Property _Leavedate() As Date
        Set(ByVal value As Date)
            mdtLeave = value
        End Set
    End Property

    Public Property _dtAttchment() As DataTable
        Get
            Return mdtAttachement
        End Get
        Set(ByVal value As DataTable)
            mdtAttachement = value
        End Set
    End Property


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Public ReadOnly Property _blnIsClaimFormBudgetMandatory() As Boolean
        Get
            Return mblnIsClaimFormBudgetMandatory
        End Get
    End Property
    'Pinkal (20-Nov-2018) -- End

    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Public ReadOnly Property _blnIsHRExpense() As Boolean
        Get
            Return mblnIsHRExpense
        End Get
    End Property

    'Pinkal (07-Mar-2019) -- End


    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Public ReadOnly Property _P2PToken() As String
        Get
            Return mstrP2PToken
        End Get
    End Property
    'Pinkal (29-Aug-2019) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef iRefUnkid As Integer, ByVal eAction As enAction, Optional ByVal blnPaymentApprovalwithLeaveApproval As Boolean = False _
                                            , Optional ByRef _objClaimMaster As clsclaim_request_master = Nothing, Optional ByVal blnIsLeaveForm As Boolean = False) As Boolean
        Try
            mintClaimRequestMasterId = iRefUnkid
            mblnPaymentApprovalwithLeaveApproval = blnPaymentApprovalwithLeaveApproval
            menAction = eAction
            mblnIsLeaveForm = blnIsLeaveForm
            Me.ShowDialog()
            iRefUnkid = mintClaimRequestMasterId
            _objClaimMaster = objClaimMaster
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
            End With

            Select Case mintExpenseCategoryID

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.

                'Case enExpenseType.EXP_LEAVE
                '    If mblnIsLeaveForm = True Then
                '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False)
                '    Else
                '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, False, "List", True)
                '    End If

                'Case Else
                '    If mblnIsLeaveForm = True Then
                '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List")
                '    Else
                '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
                '    End If

                Case enExpenseType.EXP_LEAVE
                    If mblnIsLeaveForm = True Then
                        'S.SANDEEP |10-MAR-2022| -- START
                        'ISSUE/ENHANCEMENT : OLD-580
                        'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, False)
                        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, False, False, False, False)
                        'S.SANDEEP |10-MAR-2022| -- END
                    Else
                        'S.SANDEEP |25-APR-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-143V2
                        'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, False, "List", True, True)
                        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, False, "List", True, True, False, False, False)
                        'S.SANDEEP |25-APR-2022| -- END
                    End If

                Case Else
                    If mblnIsLeaveForm = True Then
                        'S.SANDEEP |10-MAR-2022| -- START
                        'ISSUE/ENHANCEMENT : OLD-580
                        'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", False, False)
                        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", False, False, False, False)
                        'S.SANDEEP |10-MAR-2022| -- END                        
                    Else
                        'S.SANDEEP |25-APR-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-143V2
                        'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True)
                        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True, False, False, False)
                        'S.SANDEEP |25-APR-2022| -- END
                    End If

                    'Pinkal (11-Sep-2019) -- End

            End Select

            With cboExpCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintExpenseCategoryID
            End With


            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If ConfigParameter._Object._SectorRouteAssignToEmp = False AndAlso ConfigParameter._Object._SectorRouteAssignToExpense = False Then
            Dim objcommonMst As New clsCommon_Master
            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSectorRoute
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
            End With
            End If

            'Pinkal (20-Feb-2019) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                'If ConfigParameter._Object._OpexRequestCCP2PServiceURL.Trim.Length > 0 OrElse ConfigParameter._Object._CapexRequestCCP2PServiceURL.Trim.Length > 0 Then
                If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtTable
            End With
            'Pinkal (20-Nov-2018) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombo.Tables(0)

                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = CInt(drRow(0)("countryunkid"))
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            'Pinkal (04-Feb-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try
            mdView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D'"
            dgvData.AutoGenerateColumns = False
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhExpense.DataPropertyName = "expense"
            dgcolhQty.DataPropertyName = "quantity"
            dgcolhExpenseRemark.DataPropertyName = "expense_remark"
            dgcolhSectorRoute.DataPropertyName = "sector"
            dgcolhUnitPrice.DataPropertyName = "unitprice"
            dgcolhUoM.DataPropertyName = "uom"
            objdgcolhMasterId.DataPropertyName = "crmasterunkid"
            objdgcolhTranId.DataPropertyName = "crtranunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            dgvData.DataSource = mdView

            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhUnitPrice.DefaultCellStyle.Format = GUI.fmtCurrency

            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D'")
                If dTotal.Length > 0 Then
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D'")), GUI.fmtCurrency)
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Expense", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If ConfigParameter._Object._ClaimRequestVocNoType = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), enMsgBoxStyle.Information)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
            '    cboPeriod.Focus()
            '    Return False
            'End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), enMsgBoxStyle.Information)
                cboExpCategory.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If dgvData.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please add atleast one expense in order to save."), enMsgBoxStyle.Information)
                dgvData.Focus()
                Return False
            End If

            Dim objExpense As New clsExpense_Master
            Dim xRow() As DataRow = Nothing

            For Each dRow As DataRow In mdtTran.Select("AUD <> 'D'")
                objExpense._Expenseunkid = CInt(dRow.Item("expenseunkid"))
                If objExpense._IsAttachDocMandetory = True Then

                    If CInt(dRow.Item("crtranunkid")) > 0 Then
                        xRow = mdtAttachement.Select("transactionunkid = '" & CInt(dRow.Item("crtranunkid")) & "' AND AUD <> 'D' ")
                    ElseIf CStr(dRow.Item("GUID")).Trim.Length > 0 Then
                        xRow = mdtAttachement.Select("GUID = '" & CStr(dRow.Item("GUID")) & "' AND AUD <> 'D' ")
                    End If

                    If xRow.Count <= 0 Then
                        eZeeMsgBox.Show(dRow.Item("expense").ToString & " " & Language.getMessage(mstrModuleName, 22, "has mandatory document attachment. please attach document."), enMsgBoxStyle.Information)
                        Return False
                    End If

                End If
            Next

            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            If ConfigParameter._Object._ClaimRemarkMandatoryForClaim Then
                If txtClaimRemark.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Claim Remark is mandatory information. Please enter claim remark to continue."), enMsgBoxStyle.Information)
                    tabRemarks.SelectedIndex = 1
                    txtClaimRemark.Focus()
                    Return False
                End If
            End If
            'Pinkal (20-May-2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objClaimMaster._Cancel_Datetime = Nothing
            objClaimMaster._Cancel_Remark = ""
            objClaimMaster._Canceluserunkid = -1
            objClaimMaster._Iscancel = False
            objClaimMaster._Claimrequestno = txtClaimNo.Text
            objClaimMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objClaimMaster._Expensetypeid = CInt(cboExpCategory.SelectedValue)
            objClaimMaster._Isvoid = False
            objClaimMaster._Loginemployeeunkid = -1
            objClaimMaster._Transactiondate = dtpDate.Value
            objClaimMaster._Userunkid = User._Object._Userunkid
            objClaimMaster._Voiddatetime = Nothing
            objClaimMaster._Voiduserunkid = -1
            objClaimMaster._Claim_Remark = txtClaimRemark.Text

            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                    objClaimMaster._FromModuleId = enExpFromModuleID.FROM_EXPENSE
                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE AndAlso mblnIsFromLeave Then
                objClaimMaster._FromModuleId = enExpFromModuleID.FROM_LEAVE
                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE AndAlso mblnIsFromLeave = False Then
                    objClaimMaster._FromModuleId = enExpFromModuleID.FROM_EXPENSE
                End If
            Else
                objClaimMaster._FromModuleId = enExpFromModuleID.FROM_EXPENSE
            End If



            objClaimMaster._IsBalanceDeduct = False

            objClaimMaster._Statusunkid = 2 'DEFAULT PENDING
            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_LEAVE
                    objClaimMaster._Modulerefunkid = enModuleReference.Leave
                Case enExpenseType.EXP_MEDICAL
                    objClaimMaster._Modulerefunkid = enModuleReference.Medical
                Case enExpenseType.EXP_TRAINING
                    objClaimMaster._Modulerefunkid = enModuleReference.Training
                Case enExpenseType.EXP_MISCELLANEOUS
                    objClaimMaster._Modulerefunkid = enModuleReference.Miscellaneous
            End Select

            objClaimMaster._Referenceunkid = CInt(cboReference.SelectedValue)
            objClaimMaster._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objClaimMaster._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objClaimMaster._LeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
            objClaimMaster._YearId = FinancialYear._Object._YearUnkid
            objClaimMaster._CompanyID = Company._Object._Companyunkid
            objClaimMaster._ArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL
            objClaimMaster._LoginMode = enLogin_Mode.DESKTOP

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            objClaimMaster._IsPaymentApprovalwithLeaveApproval = mblnPaymentApprovalwithLeaveApproval
            'Pinkal (10-Jan-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtClaimNo.Text = objClaimMaster._Claimrequestno

            If mintExpenseCategoryID > 0 Then
                cboExpCategory.SelectedValue = mintExpenseCategoryID
            Else
                cboExpCategory.SelectedValue = objClaimMaster._Expensetypeid
            End If

            If mintEmployeeID > 0 Then
                cboEmployee.SelectedValue = mintEmployeeID
            Else
                cboEmployee.SelectedValue = objClaimMaster._Employeeunkid
            End If


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            'If objClaimMaster._Transactiondate.Date <> Nothing Then
            '    dtpDate.Value = objClaimMaster._Transactiondate.Date
            'Else
            '    dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'End If
            dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime
            If objClaimMaster._Transactiondate.Date <> Nothing Then
                dtpDate.Value = objClaimMaster._Transactiondate
            End If
            'Pinkal (20-Nov-2018) -- End

            txtClaimRemark.Text = objClaimMaster._Claim_Remark

            If mintLeaveTypeId > 0 Then
                cboLeaveType.SelectedValue = mintLeaveTypeId
            Else
                cboLeaveType.SelectedValue = objClaimMaster._LeaveTypeId
            End If

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            If mintLeaveFormID > 0 Then
                cboReference.SelectedValue = mintLeaveFormID
            Else
            cboReference.SelectedValue = objClaimMaster._Referenceunkid
            End If
            'Pinkal (10-Jan-2017) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
            If mintLeaveFormID > 0 AndAlso mintClaimRequestMasterId > 0 AndAlso ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim().Length > 0 Then
                Dim objClaim As New clsclaim_request_tran
                objClaim._ClaimRequestMasterId = mintClaimRequestMasterId
                Dim dtTable As DataTable = objClaim._DataTable
                objClaim = Nothing
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                    If xBudgetMandatoryCount > 0 Then
                        objdgcolhEdit.Visible = False
                        objdgcolhDelete.Visible = False
                        objdgcolhAttachment.Visible = False
                        btnAdd.Enabled = False
                        btnEdit.Enabled = False
                        btnSave.Enabled = False
                    End If
                End If
                '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
            End If
            'Pinkal (20-Nov-2018) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If ConfigParameter._Object._ClaimRequestVocNoType = 1 Then
                txtClaimNo.Enabled = False
            End If

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintClaimRequestMasterId > 0 Then
                cboReference.Enabled = False
                objbtnSearchReference.Enabled = False
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[On claim form when applying for leave expense, they want the leave form field removed.]
            objlblValue.Visible = False
            cboReference.Visible = False
            objbtnSearchReference.Visible = False
            'Pinkal (25-May-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboExpCategory.Enabled = iFlag
            cboEmployee.Enabled = iFlag : objbtnSearchEmployee.Enabled = iFlag
            cboPeriod.Enabled = iFlag
            dtpDate.Enabled = iFlag
            If menAction = enAction.EDIT_ONE Then
                If objClaimMaster._Expensetypeid = enExpenseType.EXP_LEAVE Then
                    cboLeaveType.Enabled = iFlag
                    objbtnSearchLeaveType.Enabled = iFlag
                End If
            Else
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    cboLeaveType.Enabled = iFlag
                    objbtnSearchLeaveType.Enabled = iFlag
                End If
            End If
            cboReference.Enabled = iFlag : objbtnSearchReference.Enabled = iFlag

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtTran IsNot Nothing Then
                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First()
                Else
                    cboCurrency.SelectedValue = mintBaseCountryId
                    cboCurrency.Enabled = True
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Enable_Disable_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = 0
            txtExpRemark.Text = ""

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtQty.Decimal = 0
            txtQty.Decimal = 1
            'Pinkal (04-Feb-2020) -- End



            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtUnitPrice.Decimal = 0
            txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End

            cboSectorRoute.SelectedValue = 0

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            mintEmpMaxCountDependentsForCR = 0
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mintClaimRequestExpenseTranId = 0
            mstrClaimRequestExpenseGUID = ""
            'Pinkal (07-Mar-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Clear_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), enMsgBoxStyle.Information)
                cboExpCategory.Focus()
                Return False
            End If
            'Pinkal (10-Feb-2021) -- End

            If CInt(cboExpense.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Expense is mandatory information. Please select Expense to continue."), enMsgBoxStyle.Information)
                cboExpense.Focus()
                Return False
            End If

            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), enMsgBoxStyle.Information)
                cboSectorRoute.Select()
                Return False
            End If


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < txtQty.Decimal Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), enMsgBoxStyle.Information)
                txtQty.Select()
                Return False
            End If
            'Pinkal (25-Oct-2018) -- End



            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < txtQty.Decimal Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), enMsgBoxStyle.Information)
                txtQty.Select()
                Return False
            End If
            'Pinkal (10-Jun-2020) -- End



            If txtQty.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Quantity is mandatory information. Please enter Quantity to continue."), enMsgBoxStyle.Information)
                txtQty.Focus()
                Return False
            End If


            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            'If txtUnitPrice.Decimal <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Unit Price is mandatory information. Please enter Unit Price to continue."), enMsgBoxStyle.Information)
            '    txtUnitPrice.Focus()
            '    Return False
            'End If
            'Pinkal (22-Oct-2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'If objExpense._IsBudgetMandatory = False Then
            Dim blnIsHRExpense As Boolean = True
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                blnIsHRExpense = objExpense._IsHRExpense
            End If

            If blnIsHRExpense Then

                'Pinkal (04-Feb-2019) -- End

            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval AndAlso CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then

                Dim objapprover As New clsleaveapprover_master
                Dim dtList As DataTable = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                                           , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString())
                If dtList.Rows.Count = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user."), enMsgBoxStyle.Information)
                    Return False
                End If

                If dtList.Rows.Count > 0 Then
                    Dim objUsermapping As New clsapprover_Usermapping
                    Dim isUserExist As Boolean = False
                    For Each dr As DataRow In dtList.Rows
                        objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                        If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                    Next

                    If isUserExist = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Map this employee's Leave Approver to system user."), enMsgBoxStyle.Information)
                        Return False
                    End If

                End If

                If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then

                    dtList = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                  , -1, CInt(cboEmployee.SelectedValue), -1, CInt(cboLeaveType.SelectedValue), ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString())
                    If dtList.Rows.Count = 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please Map this Leave type to this employee's Leave Approver(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                End If

            Else

                Dim objExapprover As New clsExpenseApprover_Master
                Dim dsList As DataSet = objExapprover.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(cboEmployee.SelectedValue), "List", Nothing)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user."), enMsgBoxStyle.Information)
                    Return False
                End If

            End If

            End If

            objExpense = Nothing

            'Pinkal (20-Nov-2018) -- End

            Dim sMsg As String = String.Empty

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue))


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid _
            '                                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
            '                                                                                , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting))


            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid _
            '                                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
            '                                                                            , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), mintClaimRequestMasterId)

            sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid _
                                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
                                                                                        , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), mintClaimRequestMasterId, True)

            'Pinkal (22-Jan-2020) -- End


            'Pinkal (07-Mar-2019) -- End

            'Pinkal (26-Feb-2019) -- End
            If sMsg <> "" Then
                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                Exit Function
            End If
            sMsg = ""


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtTran IsNot Nothing Then
                Dim objExpBalance As New clsEmployeeExpenseBalance
                Dim dsBalance As DataSet = objExpBalance.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date _
                                                                                        , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC, True, False)), CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC, True, False)))

                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestExpenseTranId > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") <> mintClaimRequestExpenseTranId And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrClaimRequestExpenseGUID.Trim.Length > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrClaimRequestExpenseGUID.Trim() And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            eZeeMsgBox.Show(Language.getMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                               Language.getMessage("clsclaim_request_master", 4, " ] time(s)."), enMsgBoxStyle.Information)
                            Exit Function
                        End If
                    End If
                End If
            End If
            'Pinkal (07-Mar-2019) -- End


            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                If CInt(cboLeaveType.SelectedValue) > 0 Then
                    If cboReference.Enabled = True AndAlso CInt(cboReference.SelectedValue) > 0 Then
                        Dim objlvtype As New clsleavetype_master : Dim objlvform As New clsleaveform
                        objlvtype._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                        If objlvtype._EligibilityOnAfter_Expense > 0 Then
                            objlvform._Formunkid = CInt(cboReference.SelectedValue)
                            Dim objlvissuemst As New clsleaveissue_master : Dim intIssueDays As Decimal = 0
                            intIssueDays = objlvissuemst.GetEmployeeTotalIssue(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), True, Nothing, Nothing, False, objlvform._Formunkid, -1)
                            If intIssueDays < objlvtype._EligibilityOnAfter_Expense Then
                                'Pinkal (25-May-2019) -- Start
                                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & objlvtype._EligibilityOnAfter_Expense & ").", enMsgBoxStyle.Information)
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type."), enMsgBoxStyle.Information)
                                'Pinkal (25-May-2019) -- End

                                objlvform = Nothing : objlvtype = Nothing : objlvissuemst = Nothing
                                Return False
                            End If
                            objlvissuemst = Nothing
                        End If
                        objlvform = Nothing : objlvtype = Nothing
                    End If
                End If
            End If
            'Pinkal (29-Sep-2017) -- End


            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            'If objClaimMaster.GetEmployeeLastExpenseStatus(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(cboSectorRoute.SelectedValue), mintClaimRequestMasterId) = 2 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot apply for this expense now as your previous claim form contains this expense which has not been approved yet."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Function
            'End If
            'Pinkal (30-Apr-2018) - end



            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtExpRemark.Text.Trim.Length <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 45, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 26, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    txtExpRemark.Focus()
                    Return False
                End If
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "Currency is mandatory information. Please select Currency to continue."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
            End If
            'Pinkal (04-Feb-2019) -- End





        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Function Add_DataRow(ByVal dr As DataRow, ByVal intRowIdx As Integer, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing
            If CInt(dgvData.Rows(intRowIdx).Cells(objdgcolhTranId.Index).Value) > 0 Then
                xRow = mdtAttachement.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvData.Rows(intRowIdx).Cells(objdgcolhTranId.Index).Value) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            ElseIf CStr(dgvData.Rows(intRowIdx).Cells(objdgcolhGUID.Index).Value).Trim.Length > 0 Then
                xRow = mdtAttachement.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvData.Rows(intRowIdx).Cells(objdgcolhGUID.Index).Value) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If


            If xRow.Length <= 0 Then
                mdtAttachement.ImportRow(dr)
            Else
                mdtAttachement.Rows.Remove(xRow(0))
                mdtAttachement.ImportRow(dr)
            End If
            mdtAttachement.AcceptChanges()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Function GetEmployeeDepedentCountForCR() As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue))
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue), dtpDate.Value)
            'Sohail (18 May 2019) -- End
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            objDependents = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeDepedentCountForCR", mstrModuleName)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    'Pinkal (25-Oct-2018) -- End    


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try
            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If xExpenditureTypeId <> enP2PExpenditureType.None Then

            If xGLCodeId > 0 Then
                Dim objAccout As New clsAccount_master
                objAccout._Accountunkid = xGLCodeId
                mstrGLCode = objAccout._Account_Code
                objAccout = Nothing
            End If

            If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'Dim objEmpCC As New clsemployee_cctranhead_tran
                    'Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.Value.Date, True, CInt(cboEmployee.SelectedValue))
                    'objEmpCC = Nothing
                    'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    '    mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
                    'End If
                    'dsList.Clear()
                    'dsList = Nothing
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    'Pinkal (04-Feb-2019) -- End
                If mintEmpCostCenterId > 0 Then
                    Dim objCostCenter As New clscostcenter_master
                    objCostCenter._Costcenterunkid = mintEmpCostCenterId
                    mstrCostCenterCode = objCostCenter._Costcentercode
                    objCostCenter = Nothing
                End If

            ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                If CInt(cboCostCenter.SelectedValue) > 0 Then
                    mintEmpCostCenterId = CInt(cboCostCenter.SelectedValue)
                    mstrCostCenterCode = CType(cboCostCenter.SelectedItem, DataRowView).Item("costcentercode").ToString()
                End If
            End If

            End If

            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetP2PRequireData", mstrModuleName)
        End Try
    End Sub


    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1399) NMB - Post approved claims amounts to P2P.
    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xGLCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), enMsgBoxStyle.Information)
                Return False
            End If

            Dim objMaster As New clsMasterData
            mstrP2PToken = objMaster.GetP2PToken(Company._Object._Companyunkid, Nothing)

            If mstrP2PToken.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())) = CInt(cboEmployee.SelectedValue)

                Dim objUnitGroup As New clsUnitGroup
                objUnitGroup._Unitgroupunkid = objEmployee._Unitgroupunkid
                Dim mstrUnitGroupCode As String = objUnitGroup._Code.ToString().Trim()
                objUnitGroup = Nothing

                Dim objDepartment As New clsDepartment
                objDepartment._Departmentunkid = objEmployee._Departmentunkid
                Dim mstrDepartmentCode As String = objDepartment._Code.ToString().Trim
                objDepartment = Nothing

                Dim objSectionGroup As New clsSectionGroup
                objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                Dim mstrSectionGroupCode As String = objSectionGroup._Code.ToString().Trim
                objSectionGroup = Nothing

                Dim objClassGroup As New clsClassGroup
                objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
                Dim mstrClassGroupCode As String = objClassGroup._Code.ToString().Trim()
                objClassGroup = Nothing

                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                Dim mstrClassCode As String = objClass._Code.ToString().Trim()
                objClass = Nothing

                objEmployee = Nothing



                ' Dimensionvalue : NMB – SUB UNIT I – FUNCTION – DEPARTMENT – ZONE – BRANCH

                'NMB(-Hardcoded)
                'Sub UNIT I - Unit Group
                'FUNCTION - Department
                'Department - Section Group
                'Zone - Class Group
                'Branch - Class

                Dim xResponseData As String = ""
                Dim xPostedData As String = ""

                'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                'Dim mstrDimensionValue As String = "NMB" & IIf(mstrUnitGroupCode.Trim.Length > 0, "-" & mstrUnitGroupCode, "") & If(mstrDepartmentCode.Trim.Length > 0, "-" & mstrDepartmentCode, "") & _
                '                                                      IIf(mstrSectionGroupCode.Trim.Length > 0, "-" & mstrSectionGroupCode, "") & IIf(mstrClassGroupCode.Trim.Length > 0, "-" & mstrClassGroupCode, "") & _
                '                                                      IIf(mstrClassCode.Trim.Length > 0, "-" & mstrClassCode, "")

                'Dim mstrDimensionValue As String = xGLCode.Trim & IIf(mstrUnitGroupCode.Trim.Length > 0, "-" & mstrUnitGroupCode, "") & If(mstrDepartmentCode.Trim.Length > 0, "-" & mstrDepartmentCode, "") & _
                '                                                     IIf(mstrSectionGroupCode.Trim.Length > 0, "-" & mstrSectionGroupCode, "") & IIf(mstrClassGroupCode.Trim.Length > 0, "-" & mstrClassGroupCode, "") & _
                '                                                     IIf(mstrClassCode.Trim.Length > 0, "-" & mstrClassCode, "")

                Dim mstrDimensionValue As String = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrDepartmentCode.Trim() & "-" & mstrSectionGroupCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                'Pinkal (23-Dec-2023) -- End

                Dim objP2PBudgetBalanceService As New clsP2PBudgetBalanceService
                objP2PBudgetBalanceService.budgetBalanceRequest.CompanyCode = Company._Object._Code
                objP2PBudgetBalanceService.budgetBalanceRequest.GLCode = xGLCode.Trim
                objP2PBudgetBalanceService.budgetBalanceRequest.DimensionValue = mstrDimensionValue
                objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Company._Object._Financial_Start_Date.ToString("dd/MM/yyyy")
                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY" & Company._Object._Financial_Start_Date.Year.ToString()
                'objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Format(New Date(2024, 1, 1).ToString(), "dd/MM/yyyy")
                'objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY2024"
                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetCycleCode = "Fiscal"
                objP2PBudgetBalanceService.budgetBalanceRequest.CalenderCode = "NMB-STD"


                If objMaster.GetSetP2PWebRequest(ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim(), True, True, "BudgetService", xResponseData, mstrError, objP2PBudgetBalanceService, xPostedData, mstrP2PToken.Trim) = False Then
                    eZeeMsgBox.Show(mstrError, enMsgBoxStyle.Information)
                    Return False
                End If

                If xResponseData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(xResponseData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mdecBudgetAmount = CDec(dtTable.Rows(0)("TotalFundsAvailableAmountMST"))
                    End If
                End If

            End If
            objMaster = Nothing

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckBudgetRequestValidationForP2P", mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (16-Oct-2023) -- End


    Private Function UpdateDeleteAttachment(ByVal dr As DataRow, ByVal mblnDeleteAttachment As Boolean, ByVal mstrAUD As String) As Boolean
        Try
            If dr IsNot Nothing Then dr("AUD") = mstrAUD
            dr.AcceptChanges()

            If mblnDeleteAttachment Then
                Try
                    If dr IsNot Nothing Then File.Delete(dr("filepath").ToString())
                Catch ex As Exception
                End Try
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateDeleteAttachment", mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (20-Nov-2018) -- End


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.Value.Date, True, CInt(cboEmployee.SelectedValue))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeCostCenter", mstrModuleName)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, CInt(cboCurrency.SelectedValue), True, dtpDate.Value.Date, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (txtUnitPrice.Decimal * txtQty.Decimal) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            objExchange = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCurrencyRate", mstrModuleName)
        End Try
    End Sub
    
    'Pinkal (04-Feb-2019) -- End

    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1399) NMB - Post approved claims amounts to P2P.

    'Private Function GetP2PToken() As String
    '    Dim mstrToken As String = ""
    '    Dim mstrError As String = ""
    '    Dim mstrGetData As String = ""
    '        Try
    '        If ConfigParameter._Object._GetTokenP2PServiceURL.Trim.Length > 0 Then
    '            Dim objMstData As New clsMasterData
    '            Dim objCRUser As New clsCRP2PUserDetail
    '            Dim mstrUserNamePwd As String = objCRUser.username
    '            If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._GetTokenP2PServiceURL.Trim, True, True, mstrUserNamePwd, mstrGetData, mstrError, objCRUser, "", "") = False Then
    '                eZeeMsgBox.Show(mstrError, enMsgBoxStyle.Information)
    '                Return mstrToken
    '                    End If

    '            If mstrGetData.Trim.Length > 0 Then
    '                Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
    '                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
    '                    mstrToken = dtTable.Rows(0)("token").ToString()
    '                    End If
    '                    End If
    '            objMstData = Nothing
    '            End If
    '        Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetP2PToken", mstrModuleName)
    '        End Try
    '    Return mstrToken
    'End Function

    'Pinkal (16-Oct-2023) -- End

    Private Function CheckImpreseRetireOrNot(ByVal xClaimRequestMasterId As Integer) As Boolean
        Dim mblnFlag As Boolean = True
        Try
            Dim mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory As String = ConfigParameter._Object._DoNotAllowToApplyImprestIfUnRetiredForExpCategory
            If mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory.Trim.Length > 0 Then
                Dim ar() = mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory.Trim.Split(CChar(","))
                If Array.Exists(ar, Function(x) x = CInt(cboExpCategory.SelectedValue)) Then
                    Dim xClaimRequestId As Integer = -1
                    Dim xStatusId As Integer = objClaimMaster.GetEmployeeLastClaimFormStatus(CInt(cboExpCategory.SelectedValue), CInt(cboEmployee.SelectedValue), xClaimRequestId, xClaimRequestMasterId)
                    If xStatusId = 2 Then    'Pending
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Sorry, you cannot apply for this claim application now as your previous claim application has not been approved for this expense category yet."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        mblnFlag = False
                        Return mblnFlag

                    ElseIf xStatusId = 1 Then    'Approved
                        Dim xClaimRetirementStatusId As Integer = 2
                        Dim objRetirement As New clsclaim_retirement_master
                        Dim mintclaimRetirementId As Integer = objRetirement.GetRetirementFromClaimRequest(xClaimRequestId, xClaimRetirementStatusId, Nothing)
                        If xClaimRetirementStatusId = 2 Then  'Pending
                            If mintclaimRetirementId <= 0 Then
                                Dim objRetirementProcess As New clsretire_process_tran
                                Dim dsRetirement As DataSet = objRetirementProcess.GetUnRetiredList("List", False, True, -1, "cmclaim_request_master.crmasterunkid = " & xClaimRequestId, Nothing)
                                If dsRetirement IsNot Nothing AndAlso dsRetirement.Tables(0).Rows.Count > 0 Then
                                    If dsRetirement.Tables(0).Rows(0)("periodunkid") <= 0 AndAlso CBool(dsRetirement.Tables(0).Rows(0)("isposted")) = False AndAlso CInt(dsRetirement.Tables(0).Rows(0)("crmasterunkid")) > 0 Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, you cannot apply for this claim application now as your previous claim application has not been retire for this expense category yet."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                        mblnFlag = False
                                        Return mblnFlag
                                    End If  'If dsRetirement.Tables(0).Rows(0)("periodunkid") <= 0 AndAlso CBool(dsRetirement.Tables(0).Rows(0)("isposted")) = False AndAlso CInt(dsRetirement.Tables(0).Rows(0)("crmasterunkid")) > 0 Then 
                                    objRetirementProcess = Nothing
                                End If  'If dsRetirement IsNot Nothing AndAlso dsRetirement.Tables(0).Rows.Count > 0 Then
                            ElseIf mintclaimRetirementId > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, you cannot apply for this claim application now as your previous claim application has not been retire for this expense category yet."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                mblnFlag = False
                                Return mblnFlag
                            End If '     If mintclaimRetirementId <= 0 Then
                            objRetirement = Nothing

                        ElseIf xClaimRetirementStatusId = 3 Then  'Rejected
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, you cannot apply for this claim application now as your previous claim application has not been retire for this expense category yet."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            mblnFlag = False
                            Return mblnFlag
                        End If '    If xClaimRetirementStatusId = 2 Then  'Pending

                    End If '    ElseIf xStatusId = 1 Then    'Approved

                End If ' If Array.Exists(ar, Function(x) x = CInt(cboExpCategory.SelectedValue)) Then

            End If '  If mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory.Trim.Length > 0 Then
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckImpreseRetireOrNot", mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    'Pinkal (10-Jun-2022) -- End


#End Region

#Region " Form's Events "

    Private Sub frmClaims_RequestAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objClaimMaster = New clsclaim_request_master
        objClaimTran = New clsclaim_request_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()

            ' txtUnitPrice.Enabled = False
            If menAction = enAction.EDIT_ONE Then
                objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
                If objClaimMaster._Crmasterunkid > 0 Then
                    Call Enable_Disable_Ctrls(False)
                End If
                If objClaimMaster._Referenceunkid > 0 Then
                    mintLeaveFormID = objClaimMaster._Referenceunkid
                End If
            End If
            Call FillCombo()
            Call GetValue()

            If _dtExpense Is Nothing Then
            objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId
            mdtTran = objClaimTran._DataTable
            Else
                mdtFinalclaimTransaction = mdtTran.Copy()
            End If

            Call Fill_Expense()
            cboExpCategory.Select()


            If mblnIsFromLeave Then
                dtpDate.Enabled = False
                dtpDate.Value = mdtLeave
            End If


            'Pinkal (23-Nov-2019) -- Start
            'Testing At NMB - Working On Claim Retirement Enhancement for NMB.
            If mdtAttachement Is Nothing Then
                'If mdtAttachement Is Nothing AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                'Pinkal (23-Nov-2019) -- End
                Dim objAttchement As New clsScan_Attach_Documents

                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                If CInt(cboEmployee.SelectedValue) > 0 Then
                Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
                End If
                'Pinkal (10-Jun-2020) -- End

                Dim strTranIds As String = String.Join(",", mdtTran.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                objAttchement = Nothing
            End If

            'Pinkal (07-Sep-2015) -- Start
            'Enhancement - ADDING EXPENSE REMARK AND SECTOR ROUTE COLUMN FOR TRA AS PER DENNIS MAIL SUBJECT "TRA upgrade" SENT ON 03-Sep-2015.
            dgcolhQty.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhUnitPrice.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'Pinkal (07-Sep-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClaims_RequestAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmClaims_RequestAddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClaims_RequestAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsclaim_request_master.SetMessages()
            clsclaim_request_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsclaim_request_master,clsclaim_request_tran"
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event(s) "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            If ConfigParameter._Object._AllowOnlyOneExpenseInClaimApplication AndAlso dgvData.RowCount > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            Dim dtmp() As DataRow = Nothing
            If mdtTran.Rows.Count > 0 Then

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND AUD <> 'D'")
                'If dtmp.Length > 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If

                dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & txtQty.Decimal & _
                                                               "' AND unitprice = '" & txtUnitPrice.Decimal & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")

                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'Pinkal (04-Feb-2019) -- End


            End If


            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If txtUnitPrice.Decimal <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 26, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    txtUnitPrice.Focus()
                    Exit Sub
                End If
            End If
            'Pinkal (22-Oct-2018) -- End



            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)


            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.Value.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 50, "Sorry,You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."))
                dtpDate.Focus()
                Exit Sub
            End If
            'Pinkal (16-Sep-2020) -- End



            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mblnIsHRExpense = objExpMaster._IsHRExpense
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""
            If mblnIsClaimFormBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If
            'Pinkal (20-Nov-2018) -- End


            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then

                    'Pinkal (07-Jul-2018) -- Start
                    'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
                    If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                If txtQty.Decimal > txtBalance.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), enMsgBoxStyle.Information)
                    txtQty.Focus()
                    Exit Sub
                End If
                    ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        If txtQty.Decimal > txtBalanceAsOnDate.Decimal Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), enMsgBoxStyle.Information)
                            txtQty.Focus()
                            Exit Sub
                        End If
                    End If
                    'Pinkal (07-Jul-2018) -- End

            ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                If txtBalance.Decimal < CDec(txtQty.Decimal * txtUnitPrice.Decimal) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set Amount greater than balance set."), enMsgBoxStyle.Information)
                    txtQty.Focus()
                    Exit Sub
                End If
            End If
            End If

            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1399) NMB - Post approved claims amounts to P2P.
            If mblnIsClaimFormBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim(), mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (txtQty.Decimal * txtUnitPrice.Decimal) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Pinkal (16-Oct-2023) -- End


            If dgvData.Rows.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Rows(0).Cells(objdgcolhTranId.Index).Value) > 0 Then
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Rows(0).Cells(objdgcolhTranId.Index).Value) & "' AND AUD <> 'D'")
                ElseIf dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                End If
                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses on the same form they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    'Pinkal (30-Apr-2018) - End
                    Exit Sub

                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    Exit Sub
                    'Pinkal (20-Nov-2018) -- End

                End If
                objExpMasterOld = Nothing
            End If
            objExpMaster = Nothing


            'Pinkal (10-Jun-2022) -- Start
            'Enhancement KBC :(AC2-558) Provide setting on expense master to stop new imprest request if a user has unretired imprest or a retirement that's pending approval.
            If CheckImpreseRetireOrNot(mintClaimRequestMasterId) = False Then Exit Sub
            'Pinkal (10-Jun-2022) -- End


            'MADE OPTIONAL -- FOR OLD DATABASE DATA COZ IF EMPLOYEE FORGETS TO MAKE CLAIM ON THAT PARTICULAR YEAR.
            'If CInt(cboReference.SelectedValue) <= 0 Then
            '    Dim iMsg As String = String.Empty
            '    iMsg = Language.getMessage(mstrModuleName, 12, "Sorry,") & cboExpCategory.Text & Language.getMessage(mstrModuleName, 13, " is mandatory information. Please select ") & cboExpCategory.Text & _
            '    Language.getMessage(mstrModuleName, 14, " to continue.")
            '    eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
            '    cboReference.Focus()
            '    Exit Sub
            'End If

            Dim dRow As DataRow = mdtTran.NewRow

            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = mintClaimRequestMasterId
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCosting.Tag Is Nothing, 0, txtCosting.Tag)
            dRow.Item("unitprice") = txtUnitPrice.Decimal
            dRow.Item("quantity") = txtQty.Decimal
            dRow.Item("amount") = txtUnitPrice.Decimal * txtQty.Decimal
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("loginemployeeunkid") = -1
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("voidloginemployeeunkid") = -1
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.Text)

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            'Pinkal (20-Nov-2018) -- End



            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.Value.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            'Pinkal (04-Feb-2019) -- End


            mdtTran.Rows.Add(dRow) : Call Fill_Expense()

            Call Enable_Disable_Ctrls(False) : Call Clear_Controls()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

            If Validation() = False Then
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            If ConfigParameter._Object._AllowOnlyOneExpenseInClaimApplication AndAlso dgvData.RowCount > 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Dim dtmp() As DataRow = Nothing
            If iRow IsNot Nothing Then
                If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                    dtmp = mdtTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & txtQty.Decimal & _
                                                         "' AND unitprice = '" & txtUnitPrice.Decimal & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & txtQty.Decimal & _
                                                      "' AND unitprice = '" & txtUnitPrice.Decimal & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Pinkal (04-Feb-2019) -- End



            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If txtUnitPrice.Decimal <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 26, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    txtUnitPrice.Focus()
                    Exit Sub
                End If
            End If
            'Pinkal (22-Oct-2018) -- End


            If iRow IsNot Nothing Then
                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsHRExpense = objExpMaster._IsHRExpense
                'Pinkal (04-Feb-2019) -- End

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
                Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = objExpMaster._Description
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""
                If mblnIsClaimFormBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If
                'Pinkal (20-Nov-2018) -- End


                If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then

                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then

                        'Pinkal (07-Jul-2018) -- Start
                        'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
                        If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                    If txtQty.Decimal > txtBalance.Decimal Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), enMsgBoxStyle.Information)
                        txtQty.Focus()
                        Exit Sub
                    End If
                        ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                            If txtQty.Decimal > txtBalanceAsOnDate.Decimal Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), enMsgBoxStyle.Information)
                                txtQty.Focus()
                                Exit Sub
                            End If
                        End If
                        'Pinkal (07-Jul-2018) -- End


                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                        If txtBalance.Decimal < CDec(txtQty.Decimal * txtUnitPrice.Decimal) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set Amount greater than balance set."), enMsgBoxStyle.Information)
                        txtQty.Focus()
                        Exit Sub
                    End If
                End If
                End If

                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1399) NMB - Post approved claims amounts to P2P.
                If objExpMaster._IsBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                    Dim mdecBudgetAmount As Decimal = 0
                    If CheckBudgetRequestValidationForP2P(ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim(), mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                    If mdecBudgetAmount < (txtQty.Decimal * txtUnitPrice.Decimal) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'Pinkal (16-Oct-2023) -- End


                If dgvData.Rows.Count >= 1 Then
                    Dim objExpMasterOld As New clsExpense_Master
                    Dim iEncashment As DataRow() = Nothing
                    If CInt(dgvData.Rows(0).Cells(objdgcolhTranId.Index).Value) > 0 Then
                        iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Rows(0).Cells(objdgcolhTranId.Index).Value) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                        iEncashment = mdtTran.Select("GUID = '" & dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                    End If
                    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                        Exit Sub
                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                    ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                        Exit Sub
                        'Pinkal (20-Nov-2018) -- End
                    End If
                    objExpMasterOld = Nothing
                End If
                objExpMaster = Nothing

                iRow(0).Item("crtranunkid") = iRow(0).Item("crtranunkid")
                iRow(0).Item("crmasterunkid") = mintClaimRequestMasterId
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCosting.Tag Is Nothing, 0, txtCosting.Tag)
                iRow(0).Item("unitprice") = txtUnitPrice.Decimal
                iRow(0).Item("quantity") = txtQty.Decimal
                iRow(0).Item("amount") = txtUnitPrice.Decimal * txtQty.Decimal
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("loginemployeeunkid") = -1
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("voidloginemployeeunkid") = -1

                'Pinkal (07-Sep-2015) -- Start
                'Enhancement - ADDING EXPENSE REMARK AND SECTOR ROUTE COLUMN FOR TRA AS PER DENNIS MAIL SUBJECT "TRA upgrade" SENT ON 03-Sep-2015.
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.Text)
                'Pinkal (07-Sep-2015) -- End

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                'Pinkal (20-Nov-2018) -- End


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                iRow(0).Item("ishrexpense") = mblnIsHRExpense
                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.Value.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                'Pinkal (04-Feb-2019) -- End

                mdtTran.AcceptChanges()
                Call Fill_Expense() : Call Clear_Controls()

                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True : objbtnSearchExpense.Enabled = True

                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim mstrFolderName As String = ""
        Dim strError As String = ""
        Try
            If Is_Valid() = False Then Exit Sub

            'Pinkal (10-Jun-2022) -- Start
            'Enhancement KBC :(AC2-558) Provide setting on expense master to stop new imprest request if a user has unretired imprest or a retirement that's pending approval.
            If CheckImpreseRetireOrNot(mintClaimRequestMasterId) = False Then Exit Sub
            'Pinkal (10-Jun-2022) -- End

            Call SetValue()

            If mblnIsFromLeave Then
                mblnCancel = False
                Me.Close()
                Exit Sub
            End If

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                mblnPaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
            End If


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            If mdtAttachement IsNot Nothing AndAlso mdtAttachement.Rows.Count > 0 Then

            Me.Cursor = Cursors.WaitCursor

            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            Dim strFileName As String
            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")

            mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault
            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                strPath += "/"
            End If

            For Each dRow As DataRow In mdtAttachement.Rows

                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))

                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath & "uploadimage/" & mstrFolderName & "/" + strFileName
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strDocLocalPath


                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    strFileName = dRow("fileuniquename").ToString

                    If blnIsIISInstalled Then
                            'Gajanan [8-April-2019] -- Start
                            'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                'Gajanan [8-April-2019] -- End
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If File.Exists(strDocLocalPath) Then
                                File.Delete(strDocLocalPath)
                            End If
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
            Next

            End If

            'Pinkal (20-Nov-2018) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objClaimMaster._FormName = mstrModuleName
            objClaimMaster._Loginemployeeunkid = 0
            objClaimMaster._ClientIP = getIP()
            objClaimMaster._HostName = getHostName()
            objClaimMaster._FromWeb = False
            objClaimMaster._AuditUserId = User._Object._Userunkid
objClaimMaster._CompanyUnkid = Company._Object._Companyunkid
            objClaimMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objClaimMaster.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp, mdtTran _
                                                                , ConfigParameter._Object._CurrentDateAndTime, mdtAttachement, Nothing, mblnPaymentApprovalwithLeaveApproval)

            Else
                blnFlag = objClaimMaster.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                , mdtTran, ConfigParameter._Object._CurrentDateAndTime, mdtAttachement, Nothing, mblnPaymentApprovalwithLeaveApproval)

            End If

            If blnFlag = False And objClaimMaster._Message <> "" Then
                eZeeMsgBox.Show(objClaimMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag = True Then

                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1399) NMB - Post approved claims amounts to P2P.


                ''Pinkal (04-Feb-2019) -- Start
                ''Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'If (menAction = enAction.ADD_ONE OrElse menAction = enAction.ADD_CONTINUE) AndAlso mblnIsClaimFormBudgetMandatory _
                '    AndAlso mblnIsHRExpense = False AndAlso ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then

                '    Dim objP2P As New clsCRP2PIntegration
                '    Dim mstrP2PRequest As String = objClaimMaster.GenerateNewRequisitionRequestForP2P(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                                                                                                                                                            , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting _
                '                                                                                                                                                                                            , objClaimMaster._Employeeunkid, objClaimMaster._Claimrequestno, objClaimMaster._Transactiondate _
                '                                                                                                                                                                                            , objClaimMaster._Claim_Remark, User._Object._Username, mdtTran, mdtAttachement, objP2P)
                '    Dim objMstData As New clsMasterData
                '    Dim mstrGetData As String = ""
                '    Dim mstrError As String = ""

                '    'Pinkal (31-May-2019) -- Start
                '    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                '    Dim mstrPostedData As String = ""

                '    'If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P) = False Then

                '    'Pinkal (29-Aug-2019) -- Start
                '    'Enhancement NMB - Working on P2P Get Token Service URL.
                '    'If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P, mstrPostedData) = False Then
                '    If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P, mstrPostedData, mstrP2PToken) = False Then
                '        'Pinkal (29-Aug-2019) -- End
                '        'Pinkal (31-May-2019) -- End

                '        '/* START TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.

                '        If mdtAttachement IsNot Nothing AndAlso mdtAttachement.Rows.Count > 0 Then
                '            Dim dr As List(Of DataRow) = mdtAttachement.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").ToList()
                '            If dr.Count > 0 Then
                '                dr.ForEach(Function(x) UpdateDeleteAttachment(x, False, "D"))
                '            End If
                '        End If

                '        objClaimMaster._Isvoid = True
                '        objClaimMaster._Voiduserunkid = User._Object._Userunkid
                '        objClaimMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '        objClaimMaster._Voidreason = "Voided due to connection fail or error from P2P system."
                '        If objClaimMaster.Delete(objClaimMaster._Crmasterunkid, User._Object._Userunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid _
                '                                                       , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                       , True, mdtAttachement, True, Nothing, mblnIsClaimFormBudgetMandatory) = False Then
                '            eZeeMsgBox.Show(objClaimMaster._Message, enMsgBoxStyle.Information)
                '            Exit Sub
                '        End If

                '        If mdtAttachement IsNot Nothing AndAlso mdtAttachement.Rows.Count > 0 Then
                '            Dim dr As List(Of DataRow) = mdtAttachement.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") = "D" AndAlso x.Field(Of Integer)("scanattachtranunkid") > 0).ToList()
                '            If dr.Count > 0 Then
                '                dr.ForEach(Function(x) UpdateDeleteAttachment(x, True, "A"))
                '            End If
                '        End If

                '        '/* END TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.
                '        eZeeMsgBox.Show(mstrError, enMsgBoxStyle.Information)
                '        Exit Sub

                '    Else
                '        '/* START TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                '        'mstrGetData = "{""requisitionId"": ""NMBHR-011118-000012"",""message"": ""New requisition details saved successfully."",""status"": 200,""timestamp"": ""01-Nov-2018 12:58:59 PM""}"
                '        'mstrGetData = "{""timeStamp"":""1550050679772"",""PmtReferenceNo"":""E02130219123759763"",""message"": ""saved sucessfully"",""status"": ""200""}"

                '        If mstrGetData.Trim.Length > 0 Then
                '            Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)

                '            'Pinkal (31-May-2019) -- Start
                '            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                '            'If objClaimMaster.UpdateP2PResponseToClaimMst(dtTable, objClaimMaster._Crmasterunkid, User._Object._Userunkid) = False Then
                '            If objClaimMaster.UpdateP2PResponseToClaimMst(dtTable, objClaimMaster._Crmasterunkid, User._Object._Userunkid, mstrPostedData) = False Then
                '                'Pinkal (31-May-2019) -- End
                '                eZeeMsgBox.Show(objClaimMaster._Message, enMsgBoxStyle.Information)
                '                Exit Sub
                '            End If
                '        End If
                '        '/* END TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                '    End If
                '    objMstData = Nothing

                'End If
                ''Pinkal (04-Feb-2019) -- End

                'Pinkal (16-Oct-2023) -- End

                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                ''Pinkal (22-Oct-2021)-- Start
                ''Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                'If gobjEmailList.Count > 0 Then
                '    Dim objThread As Threading.Thread
                '    If HttpContext.Current Is Nothing Then
                '        objThread = New Threading.Thread(AddressOf Send_Notification)
                '        objThread.IsBackground = True
                '        Dim arr(1) As Object
                '        arr(0) = Company._Object._Companyunkid
                '        objThread.Start(arr)
                '    Else
                '        Call Send_Notification(Company._Object._Companyunkid)
                '    End If
                'End If
                ''Pinkal (22-Oct-2021)-- End
                'Pinkal (20-May-2022) -- End
              



                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Claim Request saved successfully."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (03-Nov-2015) -- Start
            'Enhancement - WORKING ON Changes Required by TRA FOR Leave Expense Web Changes in Leave Application and Leave Approval.
            If mdtFinalclaimTransaction IsNot Nothing Then
                mdtTran = mdtFinalclaimTransaction.Copy
                mdtFinalclaimTransaction.Clear()
                mdtFinalclaimTransaction = Nothing
            Else
                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                If mdtTran IsNot Nothing Then mdtTran.Clear()
                'Pinkal (20-May-2022) -- End
                mdtTran = Nothing
            End If
            Me.Close()
            'Pinkal (03-Nov-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)

                If .DisplayDialog Then
                    cboEmployee.SelectedValue = .SelectedValue
                    cboEmployee.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchReference_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReference.Click
        Dim frm As New frmCommonSearch
        Try
            If cboReference.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboReference.ValueMember
                .DisplayMember = cboReference.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboReference.DataSource, DataTable)

                If .DisplayDialog Then
                    cboReference.SelectedValue = .SelectedValue
                    cboReference.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReference_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            If cboExpense.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboExpense.ValueMember
                .DisplayMember = cboExpense.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboExpense.DataSource, DataTable)

                If .DisplayDialog Then
                    cboExpense.SelectedValue = .SelectedValue
                    cboExpense.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchSecRoute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSecRoute.Click
        Dim frm As New frmCommonSearch
        Try
            If cboSectorRoute.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboSectorRoute.ValueMember
                .DisplayMember = cboSectorRoute.DisplayMember
                .DataSource = CType(cboSectorRoute.DataSource, DataTable)
                If .DisplayDialog Then
                    cboSectorRoute.SelectedValue = .SelectedValue
                    cboSectorRoute.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSecRoute_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim frm As New frmCommonSearch
        Try
            If cboLeaveType.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboLeaveType.ValueMember
                .DisplayMember = cboLeaveType.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboLeaveType.DataSource, DataTable)

                If .DisplayDialog Then
                    cboLeaveType.SelectedValue = .SelectedValue
                    cboLeaveType.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnScanAttchment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScanAttchment.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If ConfigParameter._Object._ClaimRequestVocNoType = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), enMsgBoxStyle.Information)
                    txtClaimNo.Focus()
                    Exit Sub
                End If
            End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), enMsgBoxStyle.Information)
                cboExpCategory.Focus()
                Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub
            End If

            If dgvData.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please add atleast one expense in order to save."), enMsgBoxStyle.Information)
                dgvData.Focus()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If mdtAttachement IsNot Nothing Then frm._dtAttachment = mdtAttachement.Copy()
            frm._TransactionID = mintClaimRequestMasterId
            'S.SANDEEP |26-APR-2019| -- START
            'If frm.displayDialog(Language.getMessage(mstrModuleName, 21, "Select Employee"), _
            '                     enImg_Email_RefId.Claim_Request, menAction, "-1", _
            '                     mstrModuleName, CInt(cboEmployee.SelectedValue), _
            '                     enScanAttactRefId.CLAIM_REQUEST, True) Then
            If frm.displayDialog(Language.getMessage(mstrModuleName, 21, "Select Employee"), _
                                 enImg_Email_RefId.Claim_Request, menAction, "-1", _
                             mstrModuleName, True, CInt(cboEmployee.SelectedValue), _
                                 enScanAttactRefId.CLAIM_REQUEST, True) Then
                'S.SANDEEP |26-APR-2019| -- END


                mdtAttachement = frm._dtAttachment
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnScanAttchment_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Private Sub objbtnSearchCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCostCenter.Click
        Dim frm As New frmCommonSearch
        Try
            If cboCostCenter.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboCostCenter.ValueMember
                .DisplayMember = cboCostCenter.DisplayMember
                .DataSource = CType(cboCostCenter.DataSource, DataTable)
                If .DisplayDialog Then
                    cboCostCenter.SelectedValue = .SelectedValue
                    cboCostCenter.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCostCenter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (20-Nov-2018) -- End


#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objEMaster As New clsEmployee_Master
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            dtpDate.MinDate = CDate(eZeeDate.convertDate("17530101"))
            dtpDate.MaxDate = CDate(eZeeDate.convertDate("99981231"))
            dtpDate.MinDate = FinancialYear._Object._Database_Start_Date.Date

            objPeriod = Nothing

            RemoveHandler cboEmployee.SelectedIndexChanged, AddressOf cboExpCategory_SelectedIndexChanged

            dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 dtpDate.Value.Date, _
                                                 dtpDate.Value.Date, _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, False, "List", CBool(IIf(mintEmployeeID > 0, False, True)), mintEmployeeID)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintEmployeeID
            End With

            AddHandler cboEmployee.SelectedIndexChanged, AddressOf cboExpCategory_SelectedIndexChanged

            objEMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboExpCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(cboExpCategory.SelectedValue), True, "List", CInt(cboEmployee.SelectedValue), True)

            Dim dtTable As DataTable = Nothing
            If mblnIsFromLeave = False Then
                dtTable = New DataView(dsCombo.Tables(0), "cr_expinvisible = 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With


            If ConfigParameter._Object._SectorRouteAssignToEmp Then
                Dim objAssignEmp As New clsassignemp_sector
                Dim dtSector As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboEmployee.SelectedValue), True)
                With cboSectorRoute
                    .ValueMember = "secrouteunkid"
                    .DisplayMember = "Sector"
                    .DataSource = dtSector
                    .SelectedIndex = 0
                End With
            End If


            If CInt(cboExpCategory.SelectedValue) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)

                    Case enExpenseType.EXP_LEAVE

                        objlblValue.Text = Language.getMessage(mstrModuleName, 12, "Leave Form")

                        If menAction = enAction.EDIT_ONE Then
                            cboLeaveType.Enabled = False
                            objbtnSearchLeaveType.Enabled = False
                        Else
                            cboLeaveType.Enabled = True
                            objbtnSearchLeaveType.Enabled = True
                            If cboReference.DataSource IsNot Nothing Then cboReference.SelectedIndex = 0
                            cboReference.Enabled = True
                            objbtnSearchReference.Enabled = True
                        End If

                        Dim objLeaveType As New clsleavetype_master
                        Dim mdtLeaveType As DataTable = Nothing
                        dsCombo = objLeaveType.getListForCombo("List", True, 1)
                        If mintLeaveTypeId > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & mintLeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .ValueMember = "leavetypeunkid"
                            .DisplayMember = "name"
                            .DataSource = mdtLeaveType
                            .SelectedValue = mintLeaveTypeId
                        End With
                        objLeaveType = Nothing


                        'Pinkal (11-Sep-2019) -- Start
                        'Enhancement NMB - Working On Claim Retirement for NMB.
                        'Case enExpenseType.EXP_MEDICAL
                    Case enExpenseType.EXP_MEDICAL, enExpenseType.EXP_IMPREST
                        'Pinkal (11-Sep-2019) -- End
                        If cboLeaveType.DataSource IsNot Nothing Then
                        cboLeaveType.SelectedIndex = 0
                        End If
                        cboLeaveType.Enabled = False
                        objbtnSearchLeaveType.Enabled = False
                        objlblValue.Text = ""
                        If cboReference.DataSource IsNot Nothing Then
                        cboReference.SelectedIndex = 0
                        End If
                        objbtnSearchReference.Enabled = False
                        cboReference.Enabled = False

                    Case enExpenseType.EXP_TRAINING

                        cboLeaveType.SelectedIndex = 0
                        cboLeaveType.Enabled = False
                        objbtnSearchLeaveType.Enabled = False
                        objlblValue.Text = Language.getMessage(mstrModuleName, 13, "Training")

                        cboReference.DataSource = Nothing
                        dsCombo = clsTraining_Enrollment_Tran.GetEmployee_TrainingList(CInt(cboEmployee.SelectedValue), True)
                        With cboReference
                            .ValueMember = "Id"
                            .DisplayMember = "Name"
                            .DataSource = dsCombo.Tables("List")
                            .SelectedValue = 0
                        End With

                End Select

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If

            Else
                objlblValue.Text = "" : cboReference.DataSource = Nothing : cboReference.Text = "" : cboReference.Enabled = False : objbtnSearchReference.Enabled = False
                cboLeaveType.Enabled = False : objbtnSearchLeaveType.Enabled = False
                txtDomicileAddress.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboExpense_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try

            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If ConfigParameter._Object._SectorRouteAssignToExpense Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .ValueMember = "secrouteunkid"
                    .DisplayMember = "Sector"
                    .DataSource = dtSector
                    .SelectedIndex = 0
                End With
            End If
            'Pinkal (20-Feb-2019) -- End


            Dim objExpMaster As New clsExpense_Master

            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment
                cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                objbtnSearchSecRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSectorRoute.SelectedValue = 0


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                cboCostCenter.SelectedValue = 0

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                    cboCostCenter.Enabled = False
                    objbtnSearchCostCenter.Enabled = False
                ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                    cboCostCenter.Enabled = False
                    cboCostCenter.SelectedValue = GetEmployeeCostCenter()
                    objbtnSearchCostCenter.Enabled = False
                ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                    cboCostCenter.Enabled = True
                    objbtnSearchCostCenter.Enabled = True
                End If
                Else
                    cboCostCenter.Enabled = False
                    objbtnSearchCostCenter.Enabled = False
                End If
                'Pinkal (25-May-2019) -- End

                'Pinkal (04-Feb-2019) -- End



                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtBalance.Text = "0.00"

                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet

                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid)
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date)
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal"))
                        txtBalanceAsOnDate.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate"))
                        'Pinkal (30-Apr-2018) - End
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then
                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing


                    'Pinkal (09-Aug-2018) -- Start
                    'Bug - Solving Bug for PACRA.

                    'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    '    dsList = objLeave.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                    '                                         , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                    '                                         , True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "", Nothing)
                    'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    '    dsList = objLeave.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                    '                                         , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                    '                                         , True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "", Nothing)
                    'End If


                    'dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable

                    Dim mstrLeaveTypeID As String = ""
                    Dim objleavetype As New clsleavetype_master
                    mstrLeaveTypeID = objleavetype.GetDeductdToLeaveTypeIDs(CInt(objExpMaster._Leavetypeunkid))

                    If mstrLeaveTypeID.Trim.Length > 0 Then
                        mstrLeaveTypeID &= "," & CInt(objExpMaster._Leavetypeunkid).ToString()
                        Else
                        mstrLeaveTypeID = CInt(objExpMaster._Leavetypeunkid).ToString()
                        End If

                    objleavetype = Nothing


                            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.Value.Date, mstrLeaveTypeID, CInt(cboEmployee.SelectedValue).ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting _
                                                                     , ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date _
                                                                     , False, False, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid, Nothing)
                                Else
                        objLeave._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                        objLeave._DBEnddate = FinancialYear._Object._Database_End_Date.Date

                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.Value.Date, mstrLeaveTypeID, CInt(cboEmployee.SelectedValue).ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting _
                                                                     , ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing _
                                                                     , True, True, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid, Nothing)
                                End If

                    dtbalance = dsList.Tables(0)

                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then
                        'txtBalance.Decimal = CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount"))

                        ''Pinkal (30-Apr-2018) - Start
                        ''Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.

                        'If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then

                        '    If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = FinancialYear._Object._Database_End_Date.Date
                        '    If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.Value.Date Then
                        '        txtBalanceAsOnDate.Decimal = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2)
                        '    Else
                        '        txtBalanceAsOnDate.Decimal = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.Value.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2)
                        '    End If

                        'ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                        '    'Pinkal (18-Jul-2018) -- Start
                        '    'Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.
                        '    'Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, dtpDate.Value.Date))

                        '    Dim mdtDate As DateTime = dtpDate.Value.Date
                        '    Dim mdtDays As Integer = 0
                        '    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        '        If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                        '            mdtDate = FinancialYear._Object._Database_End_Date.Date
                        '            mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, FinancialYear._Object._Database_End_Date.Date))
                        '        Else
                        '            If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '                mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '            End If
                        '        End If

                        '    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        '        If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '            mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '        End If
                        '        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '    End If

                        '    Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                        '    If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > dtpDate.Value.Date.Day OrElse intDiff > mdtDays Then
                        '        intDiff = intDiff - 1
                        '    End If

                        '    'Pinkal (18-Jul-2018) -- End

                        '    txtBalanceAsOnDate.Decimal = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2)

                        txtBalance.Decimal = CDec(dtbalance.Rows(0)("TotalAcccrueAmt")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))
                        txtBalanceAsOnDate.Decimal = CDec(CDec(dtbalance.Rows(0)("Accrue_amount")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment"))))

                        'End If

                        'Pinkal (30-Apr-2018) - End

                        'Pinkal (09-Aug-2018) -- End

                        txtUoMType.Text = Language.getMessage("clsExpCommonMethods", 6, "Quantity")
                    End If

                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid)
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date)
                    'Pinkal (30-Apr-2018) - End

                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal"))
                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        txtBalanceAsOnDate.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate"))
                        'Pinkal (30-Apr-2018) - End

                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    objEmpExpBal = Nothing
                End If
            Else
                txtUnitPrice.Enabled = True

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End

                txtUoMType.Text = ""
                'Pinkal (30-Apr-2018) - Start
                'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                txtBalance.Decimal = 0
                txtBalanceAsOnDate.Decimal = 0
                'Pinkal (30-Apr-2018) - End

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                cboCostCenter.Enabled = False
                objbtnSearchCostCenter.Enabled = False
                cboCostCenter.SelectedValue = 0
                'Pinkal (20-Nov-2018) -- End


                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                txtQty.Decimal = 1
                'Pinkal (04-Feb-2020) -- End


            End If

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager. AS Per Guidance with Matthew on 06-Feb-2020. 
            'If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory = False Then
            If objExpMaster._IsConsiderDependants Then
                txtQty.Decimal = GetEmployeeDepedentCountForCR()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Decimal)
            End If
            'Pinkal (04-Feb-2020) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[NMB WANTS THIS WHEN CONSIDER DEPENDENT IS SET ON EXPENSE MASTER]
            If objExpMaster._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
            txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End






            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                lblBalance.Visible = True
                lblBalanceasondate.Visible = True
                txtBalance.Visible = True
                txtBalanceAsOnDate.Visible = True
                cboCurrency.SelectedValue = mintBaseCountryId
                cboCurrency.Enabled = False
            Else
                lblBalance.Visible = False
                lblBalanceasondate.Visible = False
                txtBalance.Visible = False
                txtBalanceAsOnDate.Visible = False
                If mdtTran IsNot Nothing Then
                    If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First()
                    Else
                        cboCurrency.SelectedValue = mintBaseCountryId
                        cboCurrency.Enabled = True
                    End If
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpense_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboLeaveType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        Try
            Dim objleave As New clsleaveform
            Dim dsLeave As New DataSet
            Dim mdtLeaveForm As DataTable = Nothing

            'START TAKE ONLY PENDING,APPROVED AND ISSUED LEAVE FORM

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            'dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "1,2,7", CInt(cboEmployee.SelectedValue), True, True, mintLeaveFormID)
            If mblnIsFromLeave Then

                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                'dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "2,7", CInt(cboEmployee.SelectedValue), True, True, mintLeaveFormID)
                dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "2,7", CInt(cboEmployee.SelectedValue), True, CBool(IIf(mintLeaveFormID > 0, False, True)), mintLeaveFormID)
                'Pinkal (10-Jan-2017) -- End
            Else
                dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "7", CInt(cboEmployee.SelectedValue), True, True, mintLeaveFormID)
            End If
            'Pinkal (22-Jun-2015) -- End
            'END TAKE ONLY PENDING,APPROVED AND ISSUED LEAVE FORM

            If mintLeaveFormID > 0 Then
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormID, "", DataViewRowState.CurrentRows).ToTable
            Else
                If mblnIsFromLeave AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval AndAlso (menAction = enAction.ADD_CONTINUE OrElse menAction = enAction.ADD_ONE) Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & CInt(IIf(mintLeaveFormID < 0, 0, mintLeaveFormID)), "", DataViewRowState.CurrentRows).ToTable
                ElseIf mblnIsFromLeave AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval = False Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & CInt(IIf(mintLeaveFormID < 0, 0, mintLeaveFormID)), "", DataViewRowState.CurrentRows).ToTable
                Else
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            cboReference.DataSource = Nothing
            With cboReference
                .ValueMember = "formunkid"
                .DisplayMember = "name"
                .DataSource = mdtLeaveForm
                .SelectedValue = IIf(mintLeaveFormID < 0, 0, mintLeaveFormID)
            End With
            objleave = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtQty_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtQty.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Enter, Keys.Tab
                    txtUnitPrice.Focus()
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtQty_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtQty_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles txtQty.PreviewKeyDown
        Try
            If e.KeyData = Keys.Tab Then
                e.IsInputKey = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtQty_PreviewKeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        Try
            If txtQty.Decimal > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                If mblnIsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    'Pinkal (18-Sep-2015) -- Start
                    'Enhancement - PROBLEM SOLVING IN  C & R FOR TRA.
                    If txtUnitPrice.Decimal <= 0 Then
                    txtUnitPrice.Decimal = CDec(Format(CDec(txtCosting.Text), GUI.fmtCurrency))
                    End If
                    'Pinkal (18-Sep-2015) -- End
                Else
                    txtUnitPrice.Enabled = False
                End If
            ElseIf txtQty.Decimal <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                'Pinkal (18-Sep-2015) -- Start
                'Enhancement - PROBLEM SOLVING IN  C & R FOR TRA.
                If txtUnitPrice.Decimal <= 0 Then
                txtUnitPrice.Decimal = CDec(Format(CDec(txtCosting.Text), GUI.fmtCurrency))
            End If
                'Pinkal (18-Sep-2015) -- End
            End If

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            objExpense = Nothing
            'Pinkal (18-Mar-2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtQty_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DateTime Picker Event(s) "

    Private Sub dtpDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDate.ValueChanged, cboSectorRoute.SelectedValueChanged
        Try
            Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
            Dim objCosting As New clsExpenseCosting
            objCosting.GetDefaultCosting(CInt(cboSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.Value.Date)
            txtCosting.Text = Format(CDec(iAmount), GUI.fmtCurrency)
            txtCosting.Tag = iCostingId
            txtUnitPrice.Text = Format(CDec(iAmount), GUI.fmtCurrency)
            txtUnitPrice.Tag = iCostingId


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
                Dim objExpnese As New clsExpense_Master
                objExpnese._Expenseunkid = CInt(cboExpense.SelectedValue)
            'Pinkal (25-Oct-2018) -- End

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then


                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                'If objExpnese._Uomunkid = enExpUoM.UOM_QTY Then
                '    txtQty.Decimal = 0
                'Else
                    txtQty.Decimal = 1
                'End If
                'Pinkal (04-Feb-2020) -- End


            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                txtQty.Decimal = 1
            End If

            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date
            If (sender.GetType().FullName = "System.Windows.Forms.DateTimePicker" AndAlso CType(sender, DateTimePicker).Name.ToUpper() = dtpDate.Name.ToUpper()) AndAlso CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboExpense.SelectedValue) > 0 Then
                cboExpense_SelectedIndexChanged(New Object(), New EventArgs())
            End If
            'Pinkal (30-Apr-2018) - End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                txtQty.Decimal = GetEmployeeDepedentCountForCR()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Decimal)
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[NMB WANTS THIS WHEN CONSIDER DEPENDENT IS SET ON EXPENSE MASTER]
            If objExpnese._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
            txtUnitPrice.Enabled = objExpnese._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End

            objExpnese = Nothing
            'Pinkal (25-Oct-2018) -- End


            objCosting = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index

                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) > 0 Then
                        iRow = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                        iRow = mdtTran.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                    End If

                    If iRow.Length > 0 Then
                        btnAdd.Visible = False : btnEdit.Visible = True
                        cboExpense.SelectedValue = iRow(0).Item("expenseunkid")
                        cboSectorRoute.SelectedValue = iRow(0).Item("secrouteunkid")
                        txtQty.Decimal = CDec(iRow(0).Item("quantity"))
                        txtUnitPrice.Decimal = CDec(Format(CDec(iRow(0).Item("unitprice")), GUI.fmtCurrency))
                        txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))
                        cboExpense.Enabled = False : objbtnSearchExpense.Enabled = False

                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                        cboCostCenter.SelectedValue = CInt(iRow(0).Item("costcenterunkid"))
                        'Pinkal (20-Nov-2018) -- End


                        'Pinkal (07-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        mintClaimRequestExpenseTranId = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value)
                        mstrClaimRequestExpenseGUID = dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString()
                        'Pinkal (07-Mar-2019) -- End

                    End If
                Case objdgcolhDelete.Index


                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                    Dim dRow() As DataRow = Nothing
                    'Pinkal (10-Jan-2017) -- End

                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) > 0 Then
                        iRow = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            Dim iVoidReason As String = String.Empty
                            Dim frm As New frmReasonSelection
                            If User._Object._Isrighttoleft = True Then
                                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                frm.RightToLeftLayout = True
                                Call Language.ctlRightToLeftlayOut(frm)
                            End If

                            frm.displayDialog(enVoidCategoryType.OTHERS, iVoidReason)
                            If iVoidReason.Trim.Length <= 0 Then Exit Sub

                            'Pinkal (10-Jan-2017) -- Start
                            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                            dRow = mdtAttachement.Select("transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) & "' AND AUD <> 'D'")
                            'Pinkal (10-Jan-2017) -- End

                            iRow(0).Item("isvoid") = True
                            iRow(0).Item("voiduserunkid") = User._Object._Userunkid
                            iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            iRow(0).Item("voidreason") = iVoidReason
                            iRow(0).Item("AUD") = "D"
                            iRow(0).Item("voidloginemployeeunkid") = -1
                        End If
                    ElseIf dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then

                        'Pinkal (10-Jan-2017) -- Start
                        'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                        dRow = mdtAttachement.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                        'Pinkal (10-Jan-2017) -- End

                        iRow = mdtTran.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            iRow(0).Item("AUD") = "D"
                        End If
                    End If
                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                    'Dim dRow() As DataRow = Nothing
                    'If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) > 0 Then
                    '    dRow = mdtAttachement.Select("transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) & "' AND AUD <> 'D'")
                    'ElseIf dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                    '    dRow = mdtAttachement.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                    'End If
                    'Pinkal (10-Jan-2017) -- End

                    If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                        For Each dtRow As DataRow In dRow
                            dtRow.Item("AUD") = "D"
                        Next
                    End If
                    mdtAttachement.AcceptChanges()

                    mdtTran.AcceptChanges()
                    Call Fill_Expense()
                    cboExpense.Enabled = True : objbtnSearchExpense.Enabled = True
                    Call Enable_Disable_Ctrls(False) : Call Clear_Controls() : btnAdd.Visible = True
                    btnEdit.Visible = False

                Case objdgcolhAttachment.Index
                    Dim frm As New frmScanOrAttachmentInfo

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    Dim dtTable As DataTable = mdtAttachement.Clone
                    Dim xRow() As DataRow = Nothing
                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) > 0 Then
                        xRow = mdtAttachement.Select("transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                        xRow = mdtAttachement.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                    End If
                    If xRow.Count > 0 Then
                        dtTable = xRow.CopyToDataTable
                    End If

                    If mdtAttachement IsNot Nothing Then frm._dtAttachment = dtTable.Copy

                    frm._TransactionID = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhTranId.Index).Value)
                    frm._TransactionGuidString = dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString
                    frm._TransactionMstUnkId = mintClaimRequestMasterId

                    'S.SANDEEP |26-APR-2019| -- START
                    'If frm.displayDialog(Language.getMessage(mstrModuleName, 21, "Select Employee"), _
                    '                     enImg_Email_RefId.Claim_Request, menAction, "-1", _
                    '                     mstrModuleName, CInt(cboEmployee.SelectedValue), _
                    '                     enScanAttactRefId.CLAIM_REQUEST, True) Then
                    If frm.displayDialog(Language.getMessage(mstrModuleName, 21, "Select Employee"), _
                                         enImg_Email_RefId.Claim_Request, menAction, "-1", _
                                         mstrModuleName, True, CInt(cboEmployee.SelectedValue), _
                                         enScanAttactRefId.CLAIM_REQUEST, True) Then
                        'S.SANDEEP |26-APR-2019| -- END

                        frm._dtAttachment.Select("").ToList().ForEach(Function(x) Add_DataRow(x, e.RowIndex, mstrModuleName))
                    End If
                    frm = Nothing
                    'Shani (20-Aug-2016) -- End


            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Link Button Event"

    Private Sub LnkViewDependants_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LnkViewDependants.LinkClicked
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub
            End If

            Dim objfrm As New frmDependentsList

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                objfrm.displayDialog(CInt(cboEmployee.SelectedValue), False, True, dtpDate.Value.Date)
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                objfrm.displayDialog(CInt(cboEmployee.SelectedValue), True, False, dtpDate.Value.Date)
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                objfrm.displayDialog(CInt(cboEmployee.SelectedValue), False, False, dtpDate.Value.Date)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LnkViewDependants_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbExpenseInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExpenseInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnScanAttchment.GradientBackColor = GUI._ButttonBackColor
            Me.btnScanAttchment.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbExpenseInformation.Text = Language._Object.getCaption(Me.gbExpenseInformation.Name, Me.gbExpenseInformation.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.Name, Me.lblClaimNo.Text)
			Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.Name, Me.lblExpCategory.Text)
			Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.Name, Me.lblExpense.Text)
			Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.Name, Me.lblUoM.Text)
			Me.lblCosting.Text = Language._Object.getCaption(Me.lblCosting.Name, Me.lblCosting.Text)
			Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
			Me.lblQty.Text = Language._Object.getCaption(Me.lblQty.Name, Me.lblQty.Text)
			Me.lblUnitPrice.Text = Language._Object.getCaption(Me.lblUnitPrice.Name, Me.lblUnitPrice.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.Name, Me.lblGrandTotal.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.Name, Me.lblSector.Text)
			Me.tbExpenseRemark.Text = Language._Object.getCaption(Me.tbExpenseRemark.Name, Me.tbExpenseRemark.Text)
			Me.tbClaimRemark.Text = Language._Object.getCaption(Me.tbClaimRemark.Name, Me.tbClaimRemark.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.LnkViewDependants.Text = Language._Object.getCaption(Me.LnkViewDependants.Name, Me.LnkViewDependants.Text)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.LblDomicileAdd.Text = Language._Object.getCaption(Me.LblDomicileAdd.Name, Me.LblDomicileAdd.Text)
            Me.btnScanAttchment.Text = Language._Object.getCaption(Me.btnScanAttchment.Name, Me.btnScanAttchment.Text)
			Me.dgcolhExpense.HeaderText = Language._Object.getCaption(Me.dgcolhExpense.Name, Me.dgcolhExpense.HeaderText)
			Me.dgcolhSectorRoute.HeaderText = Language._Object.getCaption(Me.dgcolhSectorRoute.Name, Me.dgcolhSectorRoute.HeaderText)
			Me.dgcolhUoM.HeaderText = Language._Object.getCaption(Me.dgcolhUoM.Name, Me.dgcolhUoM.HeaderText)
			Me.dgcolhQty.HeaderText = Language._Object.getCaption(Me.dgcolhQty.Name, Me.dgcolhQty.HeaderText)
			Me.dgcolhUnitPrice.HeaderText = Language._Object.getCaption(Me.dgcolhUnitPrice.Name, Me.dgcolhUnitPrice.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhExpenseRemark.HeaderText = Language._Object.getCaption(Me.dgcolhExpenseRemark.Name, Me.dgcolhExpenseRemark.HeaderText)
			Me.lblBalanceasondate.Text = Language._Object.getCaption(Me.lblBalanceasondate.Name, Me.lblBalanceasondate.Text)
			Me.LblCostCenter.Text = Language._Object.getCaption(Me.LblCostCenter.Name, Me.LblCostCenter.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
			Language.setMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
			Language.setMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 5, "Expense is mandatory information. Please select Expense to continue.")
			Language.setMessage(mstrModuleName, 6, "Quantity is mandatory information. Please enter Quantity to continue.")
			Language.setMessage(mstrModuleName, 8, "Please add atleast one expense in order to save.")
			Language.setMessage(mstrModuleName, 9, "Claim Request saved successfully.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list.")
			Language.setMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set.")
			Language.setMessage(mstrModuleName, 12, "Leave Form")
			Language.setMessage(mstrModuleName, 13, "Training")
			Language.setMessage(mstrModuleName, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user.")
			Language.setMessage(mstrModuleName, 15, "Please Map this employee's Leave Approver to system user.")
			Language.setMessage(mstrModuleName, 16, "Please Map this Leave type to this employee's Leave Approver(s).")
			Language.setMessage(mstrModuleName, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user.")
			Language.setMessage(mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
			Language.setMessage(mstrModuleName, 19, "Sorry, you cannot set Amount greater than balance set.")
			Language.setMessage(mstrModuleName, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
            Language.setMessage(mstrModuleName, 21, "Select Employee")
            Language.setMessage(mstrModuleName, 22, "has mandatory document attachment. please attach document.")
            Language.setMessage(mstrModuleName, 23, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set.")
			Language.setMessage(mstrModuleName, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
			Language.setMessage(mstrModuleName, 26, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.")
			Language.setMessage(mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Language.setMessage(mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount.")
			Language.setMessage(mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
			Language.setMessage(mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type.")
			Language.setMessage(mstrModuleName, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
			Language.setMessage(mstrModuleName, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
			Language.setMessage(mstrModuleName, 45, "You have not set your expense remark.")
			Language.setMessage(mstrModuleName, 46, "Currency is mandatory information. Please select Currency to continue.")
			Language.setMessage(mstrModuleName, 47, "Claim Remark is mandatory information. Please enter claim remark to continue.")
			Language.setMessage(mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
			Language.setMessage(mstrModuleName, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
			Language.setMessage(mstrModuleName, 50, "Sorry,You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")
			Language.setMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [")
			Language.setMessage("clsclaim_request_master", 4, " ] time(s).")
			Language.setMessage("clsExpCommonMethods", 6, "Quantity")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class