﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpenseApproval
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExpenseApproval))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbExpenseInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LnkViewAllocation = New System.Windows.Forms.LinkLabel
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.objbtnSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.LblCostCenter = New System.Windows.Forms.Label
        Me.txtBalanceAsOnDate = New eZee.TextBox.NumericTextBox
        Me.lblBalanceasondate = New System.Windows.Forms.Label
        Me.lblUnitPrice = New System.Windows.Forms.Label
        Me.LnkViewDependants = New System.Windows.Forms.LinkLabel
        Me.txtDomicileAddress = New eZee.TextBox.AlphanumericTextBox
        Me.txtBalance = New eZee.TextBox.NumericTextBox
        Me.lblBalance = New System.Windows.Forms.Label
        Me.LblDomicileAdd = New System.Windows.Forms.Label
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.tabRemarks = New System.Windows.Forms.TabControl
        Me.tbExpenseRemark = New System.Windows.Forms.TabPage
        Me.txtExpRemark = New eZee.TextBox.AlphanumericTextBox
        Me.tbClaimRemark = New System.Windows.Forms.TabPage
        Me.txtClaimRemark = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchSecRoute = New eZee.Common.eZeeGradientButton
        Me.cboSectorRoute = New System.Windows.Forms.ComboBox
        Me.lblCosting = New System.Windows.Forms.Label
        Me.lblSector = New System.Windows.Forms.Label
        Me.txtEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchExpense = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchReference = New eZee.Common.eZeeGradientButton
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.txtUnitPrice = New eZee.TextBox.NumericTextBox
        Me.cboReference = New System.Windows.Forms.ComboBox
        Me.objlblValue = New System.Windows.Forms.Label
        Me.txtQty = New eZee.TextBox.NumericTextBox
        Me.lblQty = New System.Windows.Forms.Label
        Me.lblUoM = New System.Windows.Forms.Label
        Me.txtUoMType = New eZee.TextBox.AlphanumericTextBox
        Me.cboExpense = New System.Windows.Forms.ComboBox
        Me.lblExpense = New System.Windows.Forms.Label
        Me.cboExpCategory = New System.Windows.Forms.ComboBox
        Me.lblName = New System.Windows.Forms.Label
        Me.lblExpCategory = New System.Windows.Forms.Label
        Me.txtClaimNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.txtCosting = New eZee.TextBox.AlphanumericTextBox
        Me.gbRejectRemark = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnRemarkOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnRemarkClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblGrandTotal = New System.Windows.Forms.Label
        Me.txtGrandTotal = New eZee.TextBox.AlphanumericTextBox
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objcolhPreview = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhExpense = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSectorRoute = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUoM = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUnitPrice = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExpenseRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApproverTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhRequestTranID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnViewScanAttchment = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReject = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOK = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkShowFuelConsumptionReport = New System.Windows.Forms.LinkLabel
        Me.gbExpenseInformation.SuspendLayout()
        Me.tabRemarks.SuspendLayout()
        Me.tbExpenseRemark.SuspendLayout()
        Me.tbClaimRemark.SuspendLayout()
        Me.gbRejectRemark.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbExpenseInformation
        '
        Me.gbExpenseInformation.BorderColor = System.Drawing.Color.Black
        Me.gbExpenseInformation.Checked = False
        Me.gbExpenseInformation.CollapseAllExceptThis = False
        Me.gbExpenseInformation.CollapsedHoverImage = Nothing
        Me.gbExpenseInformation.CollapsedNormalImage = Nothing
        Me.gbExpenseInformation.CollapsedPressedImage = Nothing
        Me.gbExpenseInformation.CollapseOnLoad = False
        Me.gbExpenseInformation.Controls.Add(Me.LnkViewAllocation)
        Me.gbExpenseInformation.Controls.Add(Me.cboCurrency)
        Me.gbExpenseInformation.Controls.Add(Me.LblCurrency)
        Me.gbExpenseInformation.Controls.Add(Me.cboCostCenter)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnSearchCostCenter)
        Me.gbExpenseInformation.Controls.Add(Me.LblCostCenter)
        Me.gbExpenseInformation.Controls.Add(Me.txtBalanceAsOnDate)
        Me.gbExpenseInformation.Controls.Add(Me.lblBalanceasondate)
        Me.gbExpenseInformation.Controls.Add(Me.lblUnitPrice)
        Me.gbExpenseInformation.Controls.Add(Me.LnkViewDependants)
        Me.gbExpenseInformation.Controls.Add(Me.txtDomicileAddress)
        Me.gbExpenseInformation.Controls.Add(Me.txtBalance)
        Me.gbExpenseInformation.Controls.Add(Me.lblBalance)
        Me.gbExpenseInformation.Controls.Add(Me.LblDomicileAdd)
        Me.gbExpenseInformation.Controls.Add(Me.EZeeStraightLine1)
        Me.gbExpenseInformation.Controls.Add(Me.tabRemarks)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnSearchSecRoute)
        Me.gbExpenseInformation.Controls.Add(Me.cboSectorRoute)
        Me.gbExpenseInformation.Controls.Add(Me.lblCosting)
        Me.gbExpenseInformation.Controls.Add(Me.lblSector)
        Me.gbExpenseInformation.Controls.Add(Me.txtEmployee)
        Me.gbExpenseInformation.Controls.Add(Me.btnAdd)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnSearchExpense)
        Me.gbExpenseInformation.Controls.Add(Me.objbtnSearchReference)
        Me.gbExpenseInformation.Controls.Add(Me.cboLeaveType)
        Me.gbExpenseInformation.Controls.Add(Me.lblLeaveType)
        Me.gbExpenseInformation.Controls.Add(Me.txtUnitPrice)
        Me.gbExpenseInformation.Controls.Add(Me.cboReference)
        Me.gbExpenseInformation.Controls.Add(Me.objlblValue)
        Me.gbExpenseInformation.Controls.Add(Me.txtQty)
        Me.gbExpenseInformation.Controls.Add(Me.lblQty)
        Me.gbExpenseInformation.Controls.Add(Me.lblUoM)
        Me.gbExpenseInformation.Controls.Add(Me.txtUoMType)
        Me.gbExpenseInformation.Controls.Add(Me.cboExpense)
        Me.gbExpenseInformation.Controls.Add(Me.lblExpense)
        Me.gbExpenseInformation.Controls.Add(Me.cboExpCategory)
        Me.gbExpenseInformation.Controls.Add(Me.lblName)
        Me.gbExpenseInformation.Controls.Add(Me.lblExpCategory)
        Me.gbExpenseInformation.Controls.Add(Me.txtClaimNo)
        Me.gbExpenseInformation.Controls.Add(Me.lblEmployee)
        Me.gbExpenseInformation.Controls.Add(Me.dtpDate)
        Me.gbExpenseInformation.Controls.Add(Me.lblDate)
        Me.gbExpenseInformation.Controls.Add(Me.btnEdit)
        Me.gbExpenseInformation.Controls.Add(Me.lblPeriod)
        Me.gbExpenseInformation.Controls.Add(Me.cboPeriod)
        Me.gbExpenseInformation.Controls.Add(Me.txtCosting)
        Me.gbExpenseInformation.ExpandedHoverImage = Nothing
        Me.gbExpenseInformation.ExpandedNormalImage = Nothing
        Me.gbExpenseInformation.ExpandedPressedImage = Nothing
        Me.gbExpenseInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExpenseInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExpenseInformation.HeaderHeight = 25
        Me.gbExpenseInformation.HeaderMessage = ""
        Me.gbExpenseInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExpenseInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExpenseInformation.HeightOnCollapse = 0
        Me.gbExpenseInformation.LeftTextSpace = 0
        Me.gbExpenseInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbExpenseInformation.Name = "gbExpenseInformation"
        Me.gbExpenseInformation.OpenHeight = 300
        Me.gbExpenseInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExpenseInformation.ShowBorder = True
        Me.gbExpenseInformation.ShowCheckBox = False
        Me.gbExpenseInformation.ShowCollapseButton = False
        Me.gbExpenseInformation.ShowDefaultBorderColor = True
        Me.gbExpenseInformation.ShowDownButton = False
        Me.gbExpenseInformation.ShowHeader = True
        Me.gbExpenseInformation.Size = New System.Drawing.Size(913, 213)
        Me.gbExpenseInformation.TabIndex = 0
        Me.gbExpenseInformation.Temp = 0
        Me.gbExpenseInformation.Text = "Expense Approval Information"
        Me.gbExpenseInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LnkViewAllocation
        '
        Me.LnkViewAllocation.BackColor = System.Drawing.Color.Transparent
        Me.LnkViewAllocation.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LnkViewAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LnkViewAllocation.Location = New System.Drawing.Point(566, 4)
        Me.LnkViewAllocation.Name = "LnkViewAllocation"
        Me.LnkViewAllocation.Size = New System.Drawing.Size(200, 17)
        Me.LnkViewAllocation.TabIndex = 290
        Me.LnkViewAllocation.TabStop = True
        Me.LnkViewAllocation.Text = "View Employee Allocation"
        Me.LnkViewAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 250
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(806, 113)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(94, 21)
        Me.cboCurrency.TabIndex = 288
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(735, 115)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(64, 16)
        Me.LblCurrency.TabIndex = 287
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.DropDownWidth = 350
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(482, 86)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(221, 21)
        Me.cboCostCenter.TabIndex = 284
        '
        'objbtnSearchCostCenter
        '
        Me.objbtnSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCostCenter.BorderSelected = False
        Me.objbtnSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCostCenter.Location = New System.Drawing.Point(709, 86)
        Me.objbtnSearchCostCenter.Name = "objbtnSearchCostCenter"
        Me.objbtnSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCostCenter.TabIndex = 285
        '
        'LblCostCenter
        '
        Me.LblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCostCenter.Location = New System.Drawing.Point(375, 88)
        Me.LblCostCenter.Name = "LblCostCenter"
        Me.LblCostCenter.Size = New System.Drawing.Size(101, 16)
        Me.LblCostCenter.TabIndex = 283
        Me.LblCostCenter.Text = "Cost Center"
        Me.LblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBalanceAsOnDate
        '
        Me.txtBalanceAsOnDate.AcceptsReturn = True
        Me.txtBalanceAsOnDate.AcceptsTab = True
        Me.txtBalanceAsOnDate.AllowNegative = True
        Me.txtBalanceAsOnDate.BackColor = System.Drawing.Color.White
        Me.txtBalanceAsOnDate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBalanceAsOnDate.DigitsInGroup = 0
        Me.txtBalanceAsOnDate.Flags = 0
        Me.txtBalanceAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalanceAsOnDate.Location = New System.Drawing.Point(482, 113)
        Me.txtBalanceAsOnDate.MaxDecimalPlaces = 2
        Me.txtBalanceAsOnDate.MaxWholeDigits = 20
        Me.txtBalanceAsOnDate.Name = "txtBalanceAsOnDate"
        Me.txtBalanceAsOnDate.Prefix = ""
        Me.txtBalanceAsOnDate.RangeMax = 1.7976931348623157E+308
        Me.txtBalanceAsOnDate.RangeMin = -1.7976931348623157E+308
        Me.txtBalanceAsOnDate.ReadOnly = True
        Me.txtBalanceAsOnDate.Size = New System.Drawing.Size(67, 21)
        Me.txtBalanceAsOnDate.TabIndex = 17
        Me.txtBalanceAsOnDate.Text = "0"
        Me.txtBalanceAsOnDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBalanceasondate
        '
        Me.lblBalanceasondate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceasondate.Location = New System.Drawing.Point(375, 115)
        Me.lblBalanceasondate.Name = "lblBalanceasondate"
        Me.lblBalanceasondate.Size = New System.Drawing.Size(101, 16)
        Me.lblBalanceasondate.TabIndex = 16
        Me.lblBalanceasondate.Text = "Balance As on Date"
        Me.lblBalanceasondate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUnitPrice
        '
        Me.lblUnitPrice.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitPrice.Location = New System.Drawing.Point(735, 88)
        Me.lblUnitPrice.Name = "lblUnitPrice"
        Me.lblUnitPrice.Size = New System.Drawing.Size(64, 16)
        Me.lblUnitPrice.TabIndex = 26
        Me.lblUnitPrice.Text = "Unit Price"
        Me.lblUnitPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LnkViewDependants
        '
        Me.LnkViewDependants.BackColor = System.Drawing.Color.Transparent
        Me.LnkViewDependants.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LnkViewDependants.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LnkViewDependants.Location = New System.Drawing.Point(765, 4)
        Me.LnkViewDependants.Name = "LnkViewDependants"
        Me.LnkViewDependants.Size = New System.Drawing.Size(145, 17)
        Me.LnkViewDependants.TabIndex = 13
        Me.LnkViewDependants.TabStop = True
        Me.LnkViewDependants.Text = "View Dependants List"
        Me.LnkViewDependants.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDomicileAddress
        '
        Me.txtDomicileAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtDomicileAddress.Flags = 0
        Me.txtDomicileAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileAddress.Location = New System.Drawing.Point(84, 140)
        Me.txtDomicileAddress.Multiline = True
        Me.txtDomicileAddress.Name = "txtDomicileAddress"
        Me.txtDomicileAddress.ReadOnly = True
        Me.txtDomicileAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDomicileAddress.Size = New System.Drawing.Size(269, 62)
        Me.txtDomicileAddress.TabIndex = 28
        '
        'txtBalance
        '
        Me.txtBalance.AcceptsReturn = True
        Me.txtBalance.AcceptsTab = True
        Me.txtBalance.AllowNegative = True
        Me.txtBalance.BackColor = System.Drawing.Color.White
        Me.txtBalance.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBalance.DigitsInGroup = 0
        Me.txtBalance.Flags = 0
        Me.txtBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.Location = New System.Drawing.Point(636, 113)
        Me.txtBalance.MaxDecimalPlaces = 2
        Me.txtBalance.MaxWholeDigits = 20
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Prefix = ""
        Me.txtBalance.RangeMax = 1.7976931348623157E+308
        Me.txtBalance.RangeMin = -1.7976931348623157E+308
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(67, 21)
        Me.txtBalance.TabIndex = 19
        Me.txtBalance.Text = "0"
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBalance
        '
        Me.lblBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(553, 115)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(78, 16)
        Me.lblBalance.TabIndex = 18
        Me.lblBalance.Text = "Total Balance"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblDomicileAdd
        '
        Me.LblDomicileAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDomicileAdd.Location = New System.Drawing.Point(7, 142)
        Me.LblDomicileAdd.Name = "LblDomicileAdd"
        Me.LblDomicileAdd.Size = New System.Drawing.Size(71, 31)
        Me.LblDomicileAdd.TabIndex = 281
        Me.LblDomicileAdd.Text = "Domicile Address"
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(360, 27)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(10, 178)
        Me.EZeeStraightLine1.TabIndex = 227
        Me.EZeeStraightLine1.Text = "s"
        '
        'tabRemarks
        '
        Me.tabRemarks.Controls.Add(Me.tbExpenseRemark)
        Me.tabRemarks.Controls.Add(Me.tbClaimRemark)
        Me.tabRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabRemarks.Location = New System.Drawing.Point(372, 140)
        Me.tabRemarks.Name = "tabRemarks"
        Me.tabRemarks.SelectedIndex = 0
        Me.tabRemarks.Size = New System.Drawing.Size(427, 69)
        Me.tabRemarks.TabIndex = 29
        '
        'tbExpenseRemark
        '
        Me.tbExpenseRemark.Controls.Add(Me.txtExpRemark)
        Me.tbExpenseRemark.Location = New System.Drawing.Point(4, 22)
        Me.tbExpenseRemark.Name = "tbExpenseRemark"
        Me.tbExpenseRemark.Padding = New System.Windows.Forms.Padding(3)
        Me.tbExpenseRemark.Size = New System.Drawing.Size(419, 43)
        Me.tbExpenseRemark.TabIndex = 0
        Me.tbExpenseRemark.Text = "Expense Remark"
        Me.tbExpenseRemark.UseVisualStyleBackColor = True
        '
        'txtExpRemark
        '
        Me.txtExpRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtExpRemark.Flags = 0
        Me.txtExpRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtExpRemark.Location = New System.Drawing.Point(3, 3)
        Me.txtExpRemark.Multiline = True
        Me.txtExpRemark.Name = "txtExpRemark"
        Me.txtExpRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtExpRemark.Size = New System.Drawing.Size(413, 37)
        Me.txtExpRemark.TabIndex = 0
        '
        'tbClaimRemark
        '
        Me.tbClaimRemark.Controls.Add(Me.txtClaimRemark)
        Me.tbClaimRemark.Location = New System.Drawing.Point(4, 22)
        Me.tbClaimRemark.Name = "tbClaimRemark"
        Me.tbClaimRemark.Padding = New System.Windows.Forms.Padding(3)
        Me.tbClaimRemark.Size = New System.Drawing.Size(419, 43)
        Me.tbClaimRemark.TabIndex = 1
        Me.tbClaimRemark.Text = "Claim Remark"
        Me.tbClaimRemark.UseVisualStyleBackColor = True
        '
        'txtClaimRemark
        '
        Me.txtClaimRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtClaimRemark.Flags = 0
        Me.txtClaimRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClaimRemark.Location = New System.Drawing.Point(3, 3)
        Me.txtClaimRemark.Multiline = True
        Me.txtClaimRemark.Name = "txtClaimRemark"
        Me.txtClaimRemark.ReadOnly = True
        Me.txtClaimRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtClaimRemark.Size = New System.Drawing.Size(413, 36)
        Me.txtClaimRemark.TabIndex = 11
        '
        'objbtnSearchSecRoute
        '
        Me.objbtnSearchSecRoute.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSecRoute.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecRoute.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecRoute.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSecRoute.BorderSelected = False
        Me.objbtnSearchSecRoute.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSecRoute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchSecRoute.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSecRoute.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSecRoute.Location = New System.Drawing.Point(709, 59)
        Me.objbtnSearchSecRoute.Name = "objbtnSearchSecRoute"
        Me.objbtnSearchSecRoute.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSecRoute.TabIndex = 24
        '
        'cboSectorRoute
        '
        Me.cboSectorRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectorRoute.DropDownWidth = 350
        Me.cboSectorRoute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectorRoute.FormattingEnabled = True
        Me.cboSectorRoute.Location = New System.Drawing.Point(482, 59)
        Me.cboSectorRoute.Name = "cboSectorRoute"
        Me.cboSectorRoute.Size = New System.Drawing.Size(221, 21)
        Me.cboSectorRoute.TabIndex = 23
        '
        'lblCosting
        '
        Me.lblCosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCosting.Location = New System.Drawing.Point(736, 88)
        Me.lblCosting.Name = "lblCosting"
        Me.lblCosting.Size = New System.Drawing.Size(65, 16)
        Me.lblCosting.TabIndex = 20
        Me.lblCosting.Text = "Costing"
        Me.lblCosting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCosting.Visible = False
        '
        'lblSector
        '
        Me.lblSector.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSector.Location = New System.Drawing.Point(375, 61)
        Me.lblSector.Name = "lblSector"
        Me.lblSector.Size = New System.Drawing.Size(101, 16)
        Me.lblSector.TabIndex = 22
        Me.lblSector.Text = "Sector/ Route"
        Me.lblSector.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployee
        '
        Me.txtEmployee.BackColor = System.Drawing.Color.White
        Me.txtEmployee.Flags = 0
        Me.txtEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployee.Location = New System.Drawing.Point(85, 86)
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.ReadOnly = True
        Me.txtEmployee.Size = New System.Drawing.Size(268, 21)
        Me.txtEmployee.TabIndex = 7
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(806, 163)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(94, 39)
        Me.btnAdd.TabIndex = 29
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'objbtnSearchExpense
        '
        Me.objbtnSearchExpense.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExpense.BorderSelected = False
        Me.objbtnSearchExpense.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchExpense.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExpense.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExpense.Location = New System.Drawing.Point(709, 32)
        Me.objbtnSearchExpense.Name = "objbtnSearchExpense"
        Me.objbtnSearchExpense.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExpense.TabIndex = 15
        '
        'objbtnSearchReference
        '
        Me.objbtnSearchReference.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReference.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReference.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReference.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReference.BorderSelected = False
        Me.objbtnSearchReference.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReference.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchReference.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReference.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReference.Location = New System.Drawing.Point(332, 142)
        Me.objbtnSearchReference.Name = "objbtnSearchReference"
        Me.objbtnSearchReference.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReference.TabIndex = 12
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.DropDownWidth = 350
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(85, 113)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(268, 21)
        Me.cboLeaveType.TabIndex = 9
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(7, 115)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(71, 16)
        Me.lblLeaveType.TabIndex = 8
        Me.lblLeaveType.Text = "Leave Type"
        Me.lblLeaveType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUnitPrice
        '
        Me.txtUnitPrice.AcceptsReturn = True
        Me.txtUnitPrice.AcceptsTab = True
        Me.txtUnitPrice.AllowNegative = False
        Me.txtUnitPrice.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtUnitPrice.DigitsInGroup = 0
        Me.txtUnitPrice.Flags = 65536
        Me.txtUnitPrice.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnitPrice.Location = New System.Drawing.Point(806, 86)
        Me.txtUnitPrice.MaxDecimalPlaces = 6
        Me.txtUnitPrice.MaxWholeDigits = 21
        Me.txtUnitPrice.Name = "txtUnitPrice"
        Me.txtUnitPrice.Prefix = ""
        Me.txtUnitPrice.RangeMax = 1.7976931348623157E+308
        Me.txtUnitPrice.RangeMin = -1.7976931348623157E+308
        Me.txtUnitPrice.Size = New System.Drawing.Size(94, 21)
        Me.txtUnitPrice.TabIndex = 27
        Me.txtUnitPrice.Text = "0"
        Me.txtUnitPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboReference
        '
        Me.cboReference.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReference.DropDownWidth = 350
        Me.cboReference.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReference.FormattingEnabled = True
        Me.cboReference.Location = New System.Drawing.Point(85, 140)
        Me.cboReference.Name = "cboReference"
        Me.cboReference.Size = New System.Drawing.Size(242, 21)
        Me.cboReference.TabIndex = 11
        '
        'objlblValue
        '
        Me.objlblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblValue.Location = New System.Drawing.Point(7, 142)
        Me.objlblValue.Name = "objlblValue"
        Me.objlblValue.Size = New System.Drawing.Size(71, 16)
        Me.objlblValue.TabIndex = 10
        Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtQty
        '
        Me.txtQty.AcceptsReturn = True
        Me.txtQty.AcceptsTab = True
        Me.txtQty.AllowNegative = False
        Me.txtQty.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtQty.DigitsInGroup = 0
        Me.txtQty.Flags = 65536
        Me.txtQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQty.Location = New System.Drawing.Point(806, 59)
        Me.txtQty.MaxDecimalPlaces = 6
        Me.txtQty.MaxWholeDigits = 21
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Prefix = ""
        Me.txtQty.RangeMax = 1.7976931348623157E+308
        Me.txtQty.RangeMin = -1.7976931348623157E+308
        Me.txtQty.Size = New System.Drawing.Size(94, 21)
        Me.txtQty.TabIndex = 25
        Me.txtQty.Text = "0"
        Me.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblQty
        '
        Me.lblQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQty.Location = New System.Drawing.Point(735, 61)
        Me.lblQty.Name = "lblQty"
        Me.lblQty.Size = New System.Drawing.Size(64, 16)
        Me.lblQty.TabIndex = 24
        Me.lblQty.Text = "Qty."
        Me.lblQty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUoM
        '
        Me.lblUoM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUoM.Location = New System.Drawing.Point(735, 34)
        Me.lblUoM.Name = "lblUoM"
        Me.lblUoM.Size = New System.Drawing.Size(64, 16)
        Me.lblUoM.TabIndex = 20
        Me.lblUoM.Text = "UoM Type"
        Me.lblUoM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUoMType
        '
        Me.txtUoMType.BackColor = System.Drawing.Color.White
        Me.txtUoMType.Flags = 0
        Me.txtUoMType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUoMType.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtUoMType.Location = New System.Drawing.Point(806, 32)
        Me.txtUoMType.Name = "txtUoMType"
        Me.txtUoMType.ReadOnly = True
        Me.txtUoMType.Size = New System.Drawing.Size(94, 21)
        Me.txtUoMType.TabIndex = 21
        '
        'cboExpense
        '
        Me.cboExpense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpense.DropDownWidth = 350
        Me.cboExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpense.FormattingEnabled = True
        Me.cboExpense.Location = New System.Drawing.Point(482, 32)
        Me.cboExpense.Name = "cboExpense"
        Me.cboExpense.Size = New System.Drawing.Size(221, 21)
        Me.cboExpense.TabIndex = 15
        '
        'lblExpense
        '
        Me.lblExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpense.Location = New System.Drawing.Point(375, 34)
        Me.lblExpense.Name = "lblExpense"
        Me.lblExpense.Size = New System.Drawing.Size(101, 16)
        Me.lblExpense.TabIndex = 14
        Me.lblExpense.Text = "Expense"
        Me.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpCategory
        '
        Me.cboExpCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpCategory.DropDownWidth = 350
        Me.cboExpCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpCategory.FormattingEnabled = True
        Me.cboExpCategory.Location = New System.Drawing.Point(85, 32)
        Me.cboExpCategory.Name = "cboExpCategory"
        Me.cboExpCategory.Size = New System.Drawing.Size(269, 21)
        Me.cboExpCategory.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(7, 61)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(71, 16)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Claim No."
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExpCategory
        '
        Me.lblExpCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpCategory.Location = New System.Drawing.Point(7, 34)
        Me.lblExpCategory.Name = "lblExpCategory"
        Me.lblExpCategory.Size = New System.Drawing.Size(71, 16)
        Me.lblExpCategory.TabIndex = 0
        Me.lblExpCategory.Text = "Exp. Cat."
        Me.lblExpCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClaimNo
        '
        Me.txtClaimNo.Flags = 0
        Me.txtClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClaimNo.Location = New System.Drawing.Point(85, 59)
        Me.txtClaimNo.Name = "txtClaimNo"
        Me.txtClaimNo.Size = New System.Drawing.Size(122, 21)
        Me.txtClaimNo.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(7, 88)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(71, 16)
        Me.lblEmployee.TabIndex = 6
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate
        '
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(265, 59)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpDate.TabIndex = 5
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(213, 61)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(46, 16)
        Me.lblDate.TabIndex = 4
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(806, 163)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(94, 39)
        Me.btnEdit.TabIndex = 35
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        Me.btnEdit.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(200, 34)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(49, 16)
        Me.lblPeriod.TabIndex = 3
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPeriod.Visible = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(265, 32)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(88, 21)
        Me.cboPeriod.TabIndex = 4
        Me.cboPeriod.Visible = False
        '
        'txtCosting
        '
        Me.txtCosting.BackColor = System.Drawing.Color.White
        Me.txtCosting.Flags = 0
        Me.txtCosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCosting.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCosting.Location = New System.Drawing.Point(806, 86)
        Me.txtCosting.Name = "txtCosting"
        Me.txtCosting.ReadOnly = True
        Me.txtCosting.Size = New System.Drawing.Size(94, 21)
        Me.txtCosting.TabIndex = 21
        Me.txtCosting.Visible = False
        '
        'gbRejectRemark
        '
        Me.gbRejectRemark.BorderColor = System.Drawing.Color.Black
        Me.gbRejectRemark.Checked = False
        Me.gbRejectRemark.CollapseAllExceptThis = False
        Me.gbRejectRemark.CollapsedHoverImage = Nothing
        Me.gbRejectRemark.CollapsedNormalImage = Nothing
        Me.gbRejectRemark.CollapsedPressedImage = Nothing
        Me.gbRejectRemark.CollapseOnLoad = False
        Me.gbRejectRemark.Controls.Add(Me.btnRemarkOk)
        Me.gbRejectRemark.Controls.Add(Me.btnRemarkClose)
        Me.gbRejectRemark.Controls.Add(Me.txtRemarks)
        Me.gbRejectRemark.ExpandedHoverImage = Nothing
        Me.gbRejectRemark.ExpandedNormalImage = Nothing
        Me.gbRejectRemark.ExpandedPressedImage = Nothing
        Me.gbRejectRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRejectRemark.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRejectRemark.HeaderHeight = 25
        Me.gbRejectRemark.HeaderMessage = ""
        Me.gbRejectRemark.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRejectRemark.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRejectRemark.HeightOnCollapse = 0
        Me.gbRejectRemark.LeftTextSpace = 0
        Me.gbRejectRemark.Location = New System.Drawing.Point(312, 34)
        Me.gbRejectRemark.Name = "gbRejectRemark"
        Me.gbRejectRemark.OpenHeight = 300
        Me.gbRejectRemark.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRejectRemark.ShowBorder = True
        Me.gbRejectRemark.ShowCheckBox = False
        Me.gbRejectRemark.ShowCollapseButton = False
        Me.gbRejectRemark.ShowDefaultBorderColor = True
        Me.gbRejectRemark.ShowDownButton = False
        Me.gbRejectRemark.ShowHeader = True
        Me.gbRejectRemark.Size = New System.Drawing.Size(343, 185)
        Me.gbRejectRemark.TabIndex = 1
        Me.gbRejectRemark.Temp = 0
        Me.gbRejectRemark.Text = "Rejection Remark"
        Me.gbRejectRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbRejectRemark.Visible = False
        '
        'btnRemarkOk
        '
        Me.btnRemarkOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemarkOk.BackColor = System.Drawing.Color.White
        Me.btnRemarkOk.BackgroundImage = CType(resources.GetObject("btnRemarkOk.BackgroundImage"), System.Drawing.Image)
        Me.btnRemarkOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRemarkOk.BorderColor = System.Drawing.Color.Empty
        Me.btnRemarkOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRemarkOk.FlatAppearance.BorderSize = 0
        Me.btnRemarkOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemarkOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemarkOk.ForeColor = System.Drawing.Color.Black
        Me.btnRemarkOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRemarkOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemarkOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkOk.Location = New System.Drawing.Point(146, 150)
        Me.btnRemarkOk.Name = "btnRemarkOk"
        Me.btnRemarkOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemarkOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkOk.Size = New System.Drawing.Size(93, 30)
        Me.btnRemarkOk.TabIndex = 1
        Me.btnRemarkOk.Text = "Ok"
        Me.btnRemarkOk.UseVisualStyleBackColor = True
        '
        'btnRemarkClose
        '
        Me.btnRemarkClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemarkClose.BackColor = System.Drawing.Color.White
        Me.btnRemarkClose.BackgroundImage = CType(resources.GetObject("btnRemarkClose.BackgroundImage"), System.Drawing.Image)
        Me.btnRemarkClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRemarkClose.BorderColor = System.Drawing.Color.Empty
        Me.btnRemarkClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRemarkClose.FlatAppearance.BorderSize = 0
        Me.btnRemarkClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemarkClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemarkClose.ForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRemarkClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemarkClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.Location = New System.Drawing.Point(245, 150)
        Me.btnRemarkClose.Name = "btnRemarkClose"
        Me.btnRemarkClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemarkClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRemarkClose.Size = New System.Drawing.Size(93, 30)
        Me.btnRemarkClose.TabIndex = 2
        Me.btnRemarkClose.Text = "Close"
        Me.btnRemarkClose.UseVisualStyleBackColor = True
        '
        'txtRemarks
        '
        Me.txtRemarks.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(4, 29)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.Size = New System.Drawing.Size(334, 116)
        Me.txtRemarks.TabIndex = 0
        '
        'lblGrandTotal
        '
        Me.lblGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrandTotal.Location = New System.Drawing.Point(688, 472)
        Me.lblGrandTotal.Name = "lblGrandTotal"
        Me.lblGrandTotal.Size = New System.Drawing.Size(83, 16)
        Me.lblGrandTotal.TabIndex = 1
        Me.lblGrandTotal.Text = "Grand Total"
        Me.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGrandTotal
        '
        Me.txtGrandTotal.BackColor = System.Drawing.Color.White
        Me.txtGrandTotal.Flags = 0
        Me.txtGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrandTotal.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGrandTotal.Location = New System.Drawing.Point(777, 470)
        Me.txtGrandTotal.Name = "txtGrandTotal"
        Me.txtGrandTotal.ReadOnly = True
        Me.txtGrandTotal.Size = New System.Drawing.Size(125, 21)
        Me.txtGrandTotal.TabIndex = 2
        Me.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.gbRejectRemark)
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(1, 219)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(911, 243)
        Me.pnlData.TabIndex = 29
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objcolhPreview, Me.dgcolhExpense, Me.dgcolhSectorRoute, Me.dgcolhUoM, Me.dgcolhQty, Me.dgcolhUnitPrice, Me.dgcolhAmount, Me.dgcolhExpenseRemark, Me.objdgcolhApproverTranId, Me.objcolhRequestTranID, Me.objdgcolhMasterId, Me.objdgcolhGUID})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(911, 243)
        Me.dgvData.TabIndex = 0
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objcolhPreview
        '
        Me.objcolhPreview.Frozen = True
        Me.objcolhPreview.HeaderText = ""
        Me.objcolhPreview.Image = Global.Aruti.Main.My.Resources.Resources.doc_view
        Me.objcolhPreview.Name = "objcolhPreview"
        Me.objcolhPreview.ReadOnly = True
        Me.objcolhPreview.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhPreview.Width = 25
        '
        'dgcolhExpense
        '
        Me.dgcolhExpense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhExpense.HeaderText = "Claim/Expense Desc"
        Me.dgcolhExpense.Name = "dgcolhExpense"
        Me.dgcolhExpense.ReadOnly = True
        Me.dgcolhExpense.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhExpense.Width = 150
        '
        'dgcolhSectorRoute
        '
        Me.dgcolhSectorRoute.HeaderText = "Sector / Route"
        Me.dgcolhSectorRoute.Name = "dgcolhSectorRoute"
        Me.dgcolhSectorRoute.ReadOnly = True
        Me.dgcolhSectorRoute.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSectorRoute.Width = 175
        '
        'dgcolhUoM
        '
        Me.dgcolhUoM.HeaderText = "UoM"
        Me.dgcolhUoM.Name = "dgcolhUoM"
        Me.dgcolhUoM.ReadOnly = True
        Me.dgcolhUoM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhQty
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhQty.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhQty.HeaderText = "Quantity"
        Me.dgcolhQty.Name = "dgcolhQty"
        Me.dgcolhQty.ReadOnly = True
        Me.dgcolhQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhUnitPrice
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhUnitPrice.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhUnitPrice.HeaderText = "Unit Price"
        Me.dgcolhUnitPrice.Name = "dgcolhUnitPrice"
        Me.dgcolhUnitPrice.ReadOnly = True
        Me.dgcolhUnitPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAmount.Width = 125
        '
        'dgcolhExpenseRemark
        '
        Me.dgcolhExpenseRemark.HeaderText = "Expense Remark"
        Me.dgcolhExpenseRemark.Name = "dgcolhExpenseRemark"
        Me.dgcolhExpenseRemark.ReadOnly = True
        Me.dgcolhExpenseRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhExpenseRemark.Width = 175
        '
        'objdgcolhApproverTranId
        '
        Me.objdgcolhApproverTranId.HeaderText = "objdgcolhApproverTranId"
        Me.objdgcolhApproverTranId.Name = "objdgcolhApproverTranId"
        Me.objdgcolhApproverTranId.ReadOnly = True
        Me.objdgcolhApproverTranId.Visible = False
        '
        'objcolhRequestTranID
        '
        Me.objcolhRequestTranID.HeaderText = "objcolhRequestTranID"
        Me.objcolhRequestTranID.Name = "objcolhRequestTranID"
        Me.objcolhRequestTranID.ReadOnly = True
        Me.objcolhRequestTranID.Visible = False
        '
        'objdgcolhMasterId
        '
        Me.objdgcolhMasterId.HeaderText = "objdgcolhMasterId"
        Me.objdgcolhMasterId.Name = "objdgcolhMasterId"
        Me.objdgcolhMasterId.ReadOnly = True
        Me.objdgcolhMasterId.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnViewScanAttchment)
        Me.objFooter.Controls.Add(Me.btnReject)
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnOK)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 499)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(913, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnViewScanAttchment
        '
        Me.btnViewScanAttchment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewScanAttchment.BackColor = System.Drawing.Color.White
        Me.btnViewScanAttchment.BackgroundImage = CType(resources.GetObject("btnViewScanAttchment.BackgroundImage"), System.Drawing.Image)
        Me.btnViewScanAttchment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewScanAttchment.BorderColor = System.Drawing.Color.Empty
        Me.btnViewScanAttchment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewScanAttchment.FlatAppearance.BorderSize = 0
        Me.btnViewScanAttchment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewScanAttchment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewScanAttchment.ForeColor = System.Drawing.Color.Black
        Me.btnViewScanAttchment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewScanAttchment.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewScanAttchment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewScanAttchment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewScanAttchment.Location = New System.Drawing.Point(12, 13)
        Me.btnViewScanAttchment.Name = "btnViewScanAttchment"
        Me.btnViewScanAttchment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewScanAttchment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewScanAttchment.Size = New System.Drawing.Size(149, 30)
        Me.btnViewScanAttchment.TabIndex = 0
        Me.btnViewScanAttchment.Text = "View Scan/Attchment"
        Me.btnViewScanAttchment.UseVisualStyleBackColor = True
        Me.btnViewScanAttchment.Visible = False
        '
        'btnReject
        '
        Me.btnReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReject.BackColor = System.Drawing.Color.White
        Me.btnReject.BackgroundImage = CType(resources.GetObject("btnReject.BackgroundImage"), System.Drawing.Image)
        Me.btnReject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReject.BorderColor = System.Drawing.Color.Empty
        Me.btnReject.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReject.FlatAppearance.BorderSize = 0
        Me.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReject.ForeColor = System.Drawing.Color.Black
        Me.btnReject.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReject.GradientForeColor = System.Drawing.Color.Black
        Me.btnReject.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Location = New System.Drawing.Point(701, 13)
        Me.btnReject.Name = "btnReject"
        Me.btnReject.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Size = New System.Drawing.Size(97, 30)
        Me.btnReject.TabIndex = 2
        Me.btnReject.Text = "&Reject"
        Me.btnReject.UseVisualStyleBackColor = True
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(598, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnApprove.TabIndex = 1
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(804, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.White
        Me.btnOK.BackgroundImage = CType(resources.GetObject("btnOK.BackgroundImage"), System.Drawing.Image)
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOK.BorderColor = System.Drawing.Color.Empty
        Me.btnOK.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOK.GradientForeColor = System.Drawing.Color.Black
        Me.btnOK.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Location = New System.Drawing.Point(701, 13)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Size = New System.Drawing.Size(97, 30)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "Ok"
        Me.btnOK.UseVisualStyleBackColor = True
        Me.btnOK.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Expense"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "UoM"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn3.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn4.HeaderText = "Unit Price"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn5.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 125
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "dgcolhRemark"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhTranId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhMasterId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'lnkShowFuelConsumptionReport
        '
        Me.lnkShowFuelConsumptionReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkShowFuelConsumptionReport.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShowFuelConsumptionReport.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkShowFuelConsumptionReport.Location = New System.Drawing.Point(7, 474)
        Me.lnkShowFuelConsumptionReport.Name = "lnkShowFuelConsumptionReport"
        Me.lnkShowFuelConsumptionReport.Size = New System.Drawing.Size(363, 17)
        Me.lnkShowFuelConsumptionReport.TabIndex = 292
        Me.lnkShowFuelConsumptionReport.TabStop = True
        Me.lnkShowFuelConsumptionReport.Text = "Show Monthly Fuel Consumption Report"
        Me.lnkShowFuelConsumptionReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkShowFuelConsumptionReport.Visible = False
        '
        'frmExpenseApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(913, 554)
        Me.Controls.Add(Me.lnkShowFuelConsumptionReport)
        Me.Controls.Add(Me.gbExpenseInformation)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.txtGrandTotal)
        Me.Controls.Add(Me.lblGrandTotal)
        Me.Controls.Add(Me.pnlData)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseApproval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense Approval"
        Me.gbExpenseInformation.ResumeLayout(False)
        Me.gbExpenseInformation.PerformLayout()
        Me.tabRemarks.ResumeLayout(False)
        Me.tbExpenseRemark.ResumeLayout(False)
        Me.tbExpenseRemark.PerformLayout()
        Me.tbClaimRemark.ResumeLayout(False)
        Me.tbClaimRemark.PerformLayout()
        Me.gbRejectRemark.ResumeLayout(False)
        Me.gbRejectRemark.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbExpenseInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtClaimNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboExpCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpCategory As System.Windows.Forms.Label
    Friend WithEvents cboExpense As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpense As System.Windows.Forms.Label
    Friend WithEvents lblUoM As System.Windows.Forms.Label
    Friend WithEvents txtUoMType As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCosting As System.Windows.Forms.Label
    Friend WithEvents txtCosting As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents lblQty As System.Windows.Forms.Label
    Friend WithEvents lblUnitPrice As System.Windows.Forms.Label
    Friend WithEvents txtQty As eZee.TextBox.NumericTextBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboReference As System.Windows.Forms.ComboBox
    Friend WithEvents objlblValue As System.Windows.Forms.Label
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents txtGrandTotal As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtExpRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtUnitPrice As eZee.TextBox.NumericTextBox
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchExpense As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchReference As eZee.Common.eZeeGradientButton
    Friend WithEvents txtBalance As eZee.TextBox.NumericTextBox
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnReject As eZee.Common.eZeeLightButton
    Friend WithEvents txtClaimRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbRejectRemark As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnRemarkClose As eZee.Common.eZeeLightButton
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnRemarkOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnOK As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchSecRoute As eZee.Common.eZeeGradientButton
    Friend WithEvents cboSectorRoute As System.Windows.Forms.ComboBox
    Friend WithEvents lblSector As System.Windows.Forms.Label
    Friend WithEvents tabRemarks As System.Windows.Forms.TabControl
    Friend WithEvents tbExpenseRemark As System.Windows.Forms.TabPage
    Friend WithEvents tbClaimRemark As System.Windows.Forms.TabPage
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents txtDomicileAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblDomicileAdd As System.Windows.Forms.Label
    Friend WithEvents LnkViewDependants As System.Windows.Forms.LinkLabel
    Friend WithEvents btnViewScanAttchment As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objcolhPreview As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhExpense As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSectorRoute As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUoM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUnitPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExpenseRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApproverTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhRequestTranID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtBalanceAsOnDate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBalanceasondate As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents LblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents LnkViewAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkShowFuelConsumptionReport As System.Windows.Forms.LinkLabel
End Class
