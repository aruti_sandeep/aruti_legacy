﻿'Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports ArutiReports
Imports System.Web

#End Region

Public Class frmExpenseApproval

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExpenseApproval"
    Private objClaimMaster As clsclaim_request_master
    Private objClaimTran As clsclaim_request_tran
    Private objExpApproverTran As clsclaim_request_approval_tran
    Private mintClaimRequestMasterId As Integer = 0
    Private mintClaimApproverId As Integer = 0
    Private mintClaimApproverEmpID As Integer = 0
    Private mintApproverPriority As Integer = 0
    Private mdtTran As DataTable
    Private mdView As DataView
    Private iRow As DataRow() = Nothing
    Private imgEdit As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.edit)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.remove)
    Private mblnCancel As Boolean = True
    Private mblnIsFromLeave As Boolean = False
    Private mblnIsLeaveEncashment As Boolean = False
    Dim mdtFinalclaimTransaction As DataTable = Nothing
    Private mblnIsExternalApprover As Boolean = False
    Private mdtClaimAttchment As DataTable = Nothing

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    Dim mblnIsClaimFormBudgetMandatory As Boolean = False
    Dim mintBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End

    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mintClaimRequestApprovalExpTranId As Integer = 0
    Dim mstrClaimRequestApprovalExpGUID As String = ""
    'Pinkal (07-Mar-2019) -- End

    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    Dim LstClaimEmailList As New List(Of clsEmailCollection)
    'Pinkal (20-May-2022) -- End

    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1399) NMB - Post approved claims amounts to P2P.
    Dim mstrP2PToken As String = ""
    Dim mintGLCodeID As Integer = 0
    Dim mstrGLCode As String = ""
    Dim mstrGLDescription As String = ""
    Dim mintCostCenterID As Integer = 0
    Dim mstrCostCenterCode As String = ""
    'Pinkal (16-Oct-2023) -- End


    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Private mstrJournal As String = String.Empty
    Private mstrPaymentMode As String = String.Empty
    Private mblnIsDebit As Boolean = False
    Private mblnIsCredit As Boolean = False
    'Pinkal (23-Dec-2023) -- End

#End Region

#Region "Properties "

    Public Property _dtExpense() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intClaimRequestMstId As Integer, ByVal intClaimApproverId As Integer _
                                            , ByVal intApproverEmpId As Integer, ByVal intApproverPriority As Integer _
                                            , ByVal blnIsExternalApprover As Boolean _
                                            , Optional ByVal blnIsFromLeave As Boolean = False) As Boolean


        'Pinkal (01-Mar-2016) --Implementing External Approver in Claim Request & Leave Module.[, ByVal blnIsExternalApprover As Boolean]

        Try
            mintClaimRequestMasterId = intClaimRequestMstId
            mintClaimApproverId = intClaimApproverId
            mintClaimApproverEmpID = intApproverEmpId
            mintApproverPriority = intApproverPriority
            mblnIsFromLeave = blnIsFromLeave

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            mblnIsExternalApprover = blnIsExternalApprover
            'Pinkal (01-Mar-2016) -- End

            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)

            'S.SANDEEP |25-APR-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-143V2
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True)
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True, False, False, False)
            'S.SANDEEP |25-APR-2022| -- END

            'Pinkal (11-Sep-2019) -- End

            With cboExpCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If ConfigParameter._Object._SectorRouteAssignToEmp = False AndAlso ConfigParameter._Object._SectorRouteAssignToExpense = False Then
            Dim objcommonMst As New clsCommon_Master
            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSectorRoute
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
            End With
                objcommonMst = Nothing
            End If


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtTable
            End With

            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombo.Tables(0)

                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = CInt(drRow(0)("countryunkid"))
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With

            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try
            mdView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D' AND iscancel = 0 "
            dgvData.AutoGenerateColumns = False
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhExpense.DataPropertyName = "expense"
            dgcolhQty.DataPropertyName = "quantity"
            'Pinkal (07-Sep-2015) -- Start
            'Enhancement - ADDING EXPENSE REMARK AND SECTOR ROUTE COLUMN FOR TRA AS PER DENNIS MAIL SUBJECT "TRA upgrade" SENT ON 03-Sep-2015.
            dgcolhExpenseRemark.DataPropertyName = "expense_remark"
            dgcolhSectorRoute.DataPropertyName = "sector"
            'Pinkal (07-Sep-2015) -- End
            dgcolhUnitPrice.DataPropertyName = "unitprice"
            dgcolhUoM.DataPropertyName = "uom"
            objdgcolhMasterId.DataPropertyName = "crmasterunkid"
            objdgcolhApproverTranId.DataPropertyName = "crapprovaltranunkid"
            objcolhRequestTranID.DataPropertyName = "crtranunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            dgvData.DataSource = mdView

            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhUnitPrice.DefaultCellStyle.Format = GUI.fmtCurrency

            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D' AND iscancel = 0")
                If dTotal.Length > 0 Then
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D' AND iscancel = 0 ")), GUI.fmtCurrency)
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Expense", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If ConfigParameter._Object._ClaimRequestVocNoType = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), enMsgBoxStyle.Information)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
            '    cboPeriod.Focus()
            '    Return False
            'End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), enMsgBoxStyle.Information)
                cboExpCategory.Focus()
                Return False
            End If

            If dgvData.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please add atleast one expense in order to do futher operation."), enMsgBoxStyle.Information)
                dgvData.Focus()
                Return False
            End If

            If gbRejectRemark.Visible AndAlso txtRemarks.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Rejection Remark cannot be blank. Rejection Remark is required information."), enMsgBoxStyle.Information)
                txtRemarks.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub GetValue()
        Try
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMaster._Employeeunkid
            txtEmployee.Text = objEmployee._Employeecode & " - " & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            txtEmployee.Tag = objClaimMaster._Employeeunkid

            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            If ConfigParameter._Object._SectorRouteAssignToEmp Then
                Dim objAssignEmp As New clsassignemp_sector
                Dim dtTable As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(txtEmployee.Tag), True)
                With cboSectorRoute
                    .ValueMember = "secrouteunkid"
                    .DisplayMember = "Sector"
                    .DataSource = dtTable
                    .SelectedIndex = 0
                End With
                objAssignEmp = Nothing
            End If
            'Pinkal (22-Mar-2016) -- End
            txtClaimNo.Text = objClaimMaster._Claimrequestno
            dtpDate.Value = objClaimMaster._Transactiondate.Date
            cboExpCategory.SelectedValue = objClaimMaster._Expensetypeid
            txtClaimRemark.Text = objClaimMaster._Claim_Remark
            cboLeaveType.SelectedValue = objClaimMaster._LeaveTypeId
            cboReference.SelectedValue = objClaimMaster._Referenceunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If ConfigParameter._Object._ClaimRequestVocNoType = 1 Then
                txtClaimNo.Enabled = False
            End If
            btnApprove.Visible = Not mblnIsFromLeave
            btnReject.Visible = Not mblnIsFromLeave
            btnOK.Visible = mblnIsFromLeave

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[On claim form when applying for leave expense, they want the leave form field removed.]
            objlblValue.Visible = False
            cboReference.Visible = False
            objbtnSearchReference.Visible = False
            'Pinkal (25-May-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboReference.Enabled = iFlag
            objbtnSearchReference.Enabled = iFlag
            cboLeaveType.Enabled = iFlag
            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtTran IsNot Nothing Then
                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First()
                Else
                    cboCurrency.SelectedValue = mintBaseCountryId
                    cboCurrency.Enabled = True
                End If
            End If
            'Pinkal (04-Feb-2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Enable_Disable_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = 0
            txtExpRemark.Text = ""

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtQty.Decimal = 0
            txtQty.Decimal = 1
            'Pinkal (04-Feb-2020) -- End


            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtUnitPrice.Decimal = 0
            txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End



            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            cboSectorRoute.SelectedValue = 0
            mintEmpMaxCountDependentsForCR = 0
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mintClaimRequestApprovalExpTranId = 0
            mstrClaimRequestApprovalExpGUID = ""
            'Pinkal (07-Mar-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Clear_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SaveData(ByVal sender As Object) As Boolean
        Dim blnFlag As Boolean = False
        Dim blnLastApprover As Boolean = False
        Dim mstrRejectRemark As String = ""
        Dim mintMaxPriority As Integer = -1
        Try
            Dim mintStatusID As Integer = 2 'PENDING
            Dim mintVisibleID As Integer = 2 'PENDING

            'Shani(08-Aug-2015) -- Start
            'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
            Dim mdtApprovalDate As DateTime = Nothing
            'Shani(08-Aug-2015) -- End

            objExpApproverTran._DataTable = mdtTran

            Dim drRow() As DataRow = mdtTran.Select("AUD = ''")

            If drRow.Length > 0 Then
                For Each dr As DataRow In drRow
                    dr("AUD") = "U"
                    dr.AcceptChanges()
                Next
            End If



            'Pinkal (03-Nov-2015) -- Start
            'Enhancement - WORKING ON Changes Required by TRA FOR Leave Expense Web Changes in Leave Application and Leave Approval.
            'If mdtTran.Columns.Contains("crapprovaltranunkid") Then
            '    mdtTran.Columns.Remove("crapprovaltranunkid")
            'End If

            'If mblnIsFromLeave Then Return True

            If mblnIsFromLeave Then
                mdtFinalclaimTransaction = mdtTran.Copy
                Return True
            End If

            If mdtTran.Columns.Contains("crapprovaltranunkid") Then
                mdtTran.Columns.Remove("crapprovaltranunkid")
            End If

            'Pinkal (03-Nov-2015) -- End



            'Dim dsList As DataSet = objExpAppr.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(txtEmployee.Tag), "List", Nothing)
            Dim mblnIssued As Boolean = False
            Dim objLeaveApprover As clsleaveapprover_master = Nothing
            Dim objExpAppr As New clsExpenseApprover_Master

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            'Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpCategory.SelectedValue), True, -1, -1, "", mintClaimRequestMasterId)

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpCategory.SelectedValue), False, True, -1, -1, "", mintClaimRequestMasterId)
            Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName _
                                                                                                                  , User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboExpCategory.SelectedValue) _
                                                                                                                  , False, True, -1, "", mintClaimRequestMasterId)
            'Pinkal (24-Aug-2015) -- End


            'Pinkal (22-Jun-2015) -- End


            Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & mintApproverPriority, "", DataViewRowState.CurrentRows).ToTable
            mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

            Dim objClaimMst As New clsclaim_request_master
            objClaimMst._Crmasterunkid = mintClaimRequestMasterId

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            'If objClaimMst._FromModuleId = enExpenseType.EXP_LEAVE AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
            'Pinkal (22-Jun-2015) -- End

            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                objLeaveApprover = New clsleaveapprover_master
                objLeaveApprover._Approverunkid = mintClaimApproverId

                Dim objLeaveForm As New clsleaveform
                objLeaveForm._Formunkid = objClaimMst._Referenceunkid

                If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = mintApproverPriority) Then mblnIssued = True

            End If

            Dim mblnIsRejected As Boolean = False
            Dim mintApproverApplicationStatusId As Integer = 2

            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If (mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0) AndAlso CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = btnApprove.Name.ToUpper() Then
                Dim drUnit() As DataRow = mdtTran.Select("unitprice <= 0")
                If drUnit.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Some of the expense(s) had 0 unit price for selected employee.") & vbCrLf & _
                                                    Language.getMessage(mstrModuleName, 17, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then

                        drUnit = Nothing
                        Return False
                    End If
                End If
                drUnit = Nothing
            End If
            'Pinkal (22-Oct-2018) -- End


            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1399) NMB - Post approved claims amounts to P2P.
            'If mblnIsClaimFormBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
            '    Dim mdecBudgetAmount As Decimal = 0
            '    If CheckBudgetRequestValidationForP2P(ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Function
            '    If mdecBudgetAmount < (txtGrandTotal.Text) Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry,you cannot approve for this claim application.Reason : Total expenses claim amount is exceeded the budget amount."), enMsgBoxStyle.Information)
            '        Return False
            '    End If
            'End If
            'Pinkal (16-Oct-2023) -- End



            For i As Integer = 0 To dtApproverTable.Rows.Count - 1
                blnLastApprover = False
                Dim mintApproverEmpID As Integer = -1
                Dim mintApproverID As Integer = -1


                If ConfigParameter._Object._PaymentApprovalwithLeaveApproval AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then

                    objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))
                    If mintClaimApproverId = objLeaveApprover._Approverunkid Then

                        If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNAPPROVE" Then
                            mintStatusID = 1
                            mintVisibleID = 1
                            mdtApprovalDate = DateTime.Now
                            If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID

                        ElseIf CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNREMARKOK" Then
                            mintStatusID = 3
                            mintVisibleID = 3
                            mdtApprovalDate = DateTime.Now
                            mblnIsRejected = True
                            blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID
                            mstrRejectRemark = txtRemarks.Text.Trim
                        End If

                    Else
                        Dim mintPriority As Integer = -1

                        Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintApproverPriority & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

                        If dRow.Length > 0 Then
                            mintStatusID = 2
                            mintVisibleID = 1
                            'Pinkal (13-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            mdtApprovalDate = Nothing
                            'Pinkal (13-Aug-2020) -- End
                    Else

                            mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)))

                        If mintPriority <= -1 Then
                            mintStatusID = 2
                            mintVisibleID = 1
                            GoTo AssignApprover
                        ElseIf mblnIsRejected Then
                            mintVisibleID = -1
                        ElseIf mintApproverPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                            mintVisibleID = 1
                        ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                            mintVisibleID = 2
                        Else
                            mintVisibleID = -1
                        End If
                        mintStatusID = 2
                    End If
                        mdtApprovalDate = Nothing
                    End If

AssignApprover:
                    mintApproverEmpID = objLeaveApprover._leaveapproverunkid
                    mintApproverID = objLeaveApprover._Approverunkid

                Else
                    objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))
                    If mintClaimApproverId = objExpAppr._crApproverunkid Then
                        If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNAPPROVE" Then
                            mintStatusID = 1
                            mintVisibleID = 1
                            mdtApprovalDate = DateTime.Now
                            If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID
                        ElseIf CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNREMARKOK" Then
                            mintStatusID = 3
                            mintVisibleID = 3
                            mdtApprovalDate = DateTime.Now
                            mblnIsRejected = True
                            blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID
                            mstrRejectRemark = txtRemarks.Text.Trim
                        End If
                    Else

                        Dim mintPriority As Integer = -1


                        Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintApproverPriority & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

                        If dRow.Length > 0 Then
                            mintStatusID = 2
                            mintVisibleID = 1
                            'Pinkal (13-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            mdtApprovalDate = Nothing
                            'Pinkal (13-Aug-2020) -- End
                        Else
                            mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)))

                        If mintPriority <= -1 Then GoTo AssignApprover1

                        If mblnIsRejected Then
                            mintVisibleID = -1
                        ElseIf mintApproverPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                            mintVisibleID = 1
                        ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                            mintVisibleID = 2
                        Else
                            mintVisibleID = -1
                        End If
                        mintStatusID = 2
                            mdtApprovalDate = Nothing
                    End If

                    End If
AssignApprover1:
                    mintApproverEmpID = objExpAppr._Employeeunkid
                    mintApproverID = objExpAppr._crApproverunkid
                End If

                objExpApproverTran._YearId = FinancialYear._Object._YearUnkid
                objExpApproverTran._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                objExpApproverTran._EmployeeID = CInt(txtEmployee.Tag)


                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1399) NMB - Post approved claims amounts to P2P.
                objExpApproverTran._CompanyID = Company._Object._Companyunkid
                objExpApproverTran._CompanyCode = Company._Object._Code
                'Pinkal (16-Oct-2023) -- End



                'Pinkal (29-Feb-2016) -- Start
                'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
                If blnLastApprover AndAlso mintStatusID = 1 Then
                    If mdtTran IsNot Nothing Then
                        For Each dRow As DataRow In mdtTran.Rows
                            Dim sMsg As String = String.Empty

                            'Pinkal (26-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'sMsg = objClaimMaster.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")))

                            'Pinkal (07-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'sMsg = objClaimMaster.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")), FinancialYear._Object._YearUnkid _
                            '                                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
                            '                                                                                , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting))


                            'Pinkal (22-Jan-2020) -- Start
                            'Enhancements -  Working on OT Requisistion Reports for NMB.

                            'sMsg = objClaimMaster.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")), FinancialYear._Object._YearUnkid _
                            '                                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
                            '                                                                               , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), mintClaimRequestMasterId)

                            sMsg = objClaimMaster.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")), FinancialYear._Object._YearUnkid _
                                                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
                                                                                                          , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), mintClaimRequestMasterId, False)

                            'Pinkal (22-Jan-2020) -- End

                            'Pinkal (07-Mar-2019) -- End
                            'Pinkal (26-Feb-2019) -- End

                            If sMsg <> "" Then
                                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                                Return False
                            End If
                            sMsg = ""
                        Next
                    End If
                End If
                'Pinkal (29-Feb-2016) -- End

                With objExpApproverTran
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                If objExpApproverTran.Insert_Update_ApproverData(mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mintClaimRequestMasterId, Nothing, mstrRejectRemark, blnLastApprover, mdtApprovalDate) = False Then
                    Return False
                End If
            Next

            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.
            objClaimMst._Crmasterunkid = mintClaimRequestMasterId

            'Hemant (18 Aug 2023) -- Start
            'ENHANCEMENT(ZSSF): A1X-1194 - Real time posting of approved loans and leave expenses to Navision
            If ConfigParameter._Object._EFTIntegration = enEFTIntegration.DYNAMICS_NAVISION Then

                If objClaimMst._Statusunkid = 1 Then
                    Dim objCurrentPeriodId As New clsMasterData
                    Dim objPeriod As New clscommom_period_Tran
                    Dim mintCurrentPeriodId As Integer = objCurrentPeriodId.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, , True)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintCurrentPeriodId
                    objClaimMst.SendToDynamicNavision(mintClaimRequestMasterId, User._Object._Userunkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, _
                                                           ConfigParameter._Object._SQLDataSource, _
                                                           ConfigParameter._Object._SQLDatabaseName, _
                                                           ConfigParameter._Object._SQLDatabaseOwnerName, _
                                                           ConfigParameter._Object._SQLUserName, _
                                                           ConfigParameter._Object._SQLUserPassword, _
                                                           FinancialYear._Object._FinancialYear_Name)

                End If

            End If
            'Hemant (18 Aug 2023) -- End

            'If mintApproverApplicationStatusId = 1 Then
            If objClaimMst._Crmasterunkid = 2 AndAlso mintApproverApplicationStatusId = 1 Then
                'objExpApproverTran.SendMailToApprover(CInt(cboExpCategory.SelectedValue), ConfigParameter._Object._PaymentApprovalwithLeaveApproval, mintClaimRequestMasterId _
                '                                                       , txtClaimNo.Text.Trim, CInt(txtEmployee.Tag), mintApproverPriority, 1, "crpriority > " & mintApproverPriority, FinancialYear._Object._DatabaseName _
                '                                                       , ConfigParameter._Object._EmployeeAsOnDate, Company._Object._Companyunkid, ConfigParameter._Object._ArutiSelfServiceURL.ToString() _
                '                                                       , enLogin_Mode.DESKTOP, , User._Object._Userunkid, "")

                objExpApproverTran.SendMailToApprover(CInt(cboExpCategory.SelectedValue), ConfigParameter._Object._PaymentApprovalwithLeaveApproval, mintClaimRequestMasterId _
                                                                            , txtClaimNo.Text.Trim, CInt(txtEmployee.Tag), mintApproverPriority, 1, "crpriority > " & mintApproverPriority, FinancialYear._Object._DatabaseName _
                                                                            , ConfigParameter._Object._EmployeeAsOnDate, Company._Object._Companyunkid, ConfigParameter._Object._ArutiSelfServiceURL.ToString() _
                                                                     , enLogin_Mode.DESKTOP, , User._Object._Userunkid, "", Nothing, True)
            End If
            'Pinkal (20-May-2022) -- End

            If objClaimMst._Statusunkid = 1 Then
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
                objClaimMst._EmployeeCode = objEmployee._Employeecode
                objClaimMst._EmployeeFirstName = objEmployee._Firstname
                objClaimMst._EmployeeMiddleName = objEmployee._Othername
                objClaimMst._EmployeeSurName = objEmployee._Surname
                objClaimMst._EmpMail = objEmployee._Email
                With objClaimMst
                    ._FormName = mstrModuleName
                    ._Loginemployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                    objLeaveForm._EmployeeCode = objEmployee._Employeecode
                    objLeaveForm._EmployeeFirstName = objEmployee._Firstname
                    objLeaveForm._EmployeeMiddleName = objEmployee._Othername
                    objLeaveForm._EmployeeSurName = objEmployee._Surname
                    objLeaveForm._EmpMail = objEmployee._Email
                    With objLeaveForm
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    If objLeaveForm._Statusunkid = 7 Then 'only Issued
                        Dim objfrmsendmail As New frmSendMail
                        Dim strPath As String = objfrmsendmail.Export_ELeaveForm(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                                , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                                                , True, ConfigParameter._Object._UserAccessModeSetting, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid, ConfigParameter._Object._LeaveBalanceSetting _
                                                                                                                , FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date, True)


                        objfrmsendmail = Nothing

                        'Pinkal (01-Apr-2019) -- Start
                        'Enhancement - Working on Leave Changes for NMB.
                        'objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, cboLeaveType.Text, objLeaveForm._Startdate, objLeaveForm._Returndate, objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath, strPath, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, "", mintClaimRequestMasterId)
                        objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, cboLeaveType.Text, objLeaveForm._Startdate, objLeaveForm._Returndate _
                                                                          , objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath _
                                                                          , strPath, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, "", mintClaimRequestMasterId, objLeaveForm._Formno)
                        'Pinkal (01-Apr-2019) -- End


                        objLeaveForm = Nothing
                    End If
                Else
                    'Pinkal (20-May-2022) -- Start
                    'Optimize Global Claim Request for NMB.
                    'objClaimMst.SendMailToEmployee(CInt(txtEmployee.Tag), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, "")
                    objClaimMst.SendMailToEmployee(CInt(txtEmployee.Tag), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, "", True)
                    'Pinkal (20-May-2022) -- End

                End If
                objEmployee = Nothing
            ElseIf objClaimMst._Statusunkid = 3 Then
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
                objClaimMst._EmployeeCode = objEmployee._Employeecode
                objClaimMst._EmployeeFirstName = objEmployee._Firstname
                objClaimMst._EmployeeMiddleName = objEmployee._Othername
                objClaimMst._EmployeeSurName = objEmployee._Surname
                objClaimMst._EmpMail = objEmployee._Email
                With objClaimMst
                    ._FormName = mstrModuleName
                    ._Loginemployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objClaimMst.SendMailToEmployee(CInt(txtEmployee.Tag), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtRemarks.Text.Trim)
                   'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                'objClaimMst.SendMailToEmployee(CInt(txtEmployee.Tag), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtRemarks.Text.Trim)
                objClaimMst.SendMailToEmployee(CInt(txtEmployee.Tag), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtRemarks.Text.Trim, True)
                'Pinkal (20-May-2022) -- End

                objEmployee = Nothing
            End If


            'Hemant (22 Dec 2023) -- Start
            'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
            If blnLastApprover AndAlso mintStatusID = 1 Then
                If mdtTran IsNot Nothing Then
                    objExpApproverTran.SendMailToExpensewiseUser(mdtTran, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, , User._Object._Userunkid, "", True)
                End If
            End If
            'Hemant (22 Dec 2023) -- End


            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.

            ''Pinkal (22-Oct-2021)-- Start
            ''Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
            'If gobjEmailList.Count > 0 Then
            '    Dim objThread As Threading.Thread
            '    If HttpContext.Current Is Nothing Then
            '        objThread = New Threading.Thread(AddressOf Send_Notification)
            '        objThread.IsBackground = True
            '        Dim arr(1) As Object
            '        arr(0) = Company._Object._Companyunkid
            '        objThread.Start(arr)
            '    Else
            '        Call Send_Notification(Company._Object._Companyunkid)
            '    End If
            'End If
            ''Pinkal (22-Oct-2021)-- End


            If objExpApproverTran._ClaimEmailList IsNot Nothing AndAlso objExpApproverTran._ClaimEmailList.Count > 0 Then
                LstClaimEmailList.AddRange(objExpApproverTran._ClaimEmailList)
            End If

            If objClaimMst._ClaimEmailList IsNot Nothing AndAlso objClaimMst._ClaimEmailList.Count > 0 Then
                LstClaimEmailList.AddRange(objClaimMst._ClaimEmailList)
            End If

            If LstClaimEmailList.Count > 0 Then
                Dim objThread As Threading.Thread
                If HttpContext.Current Is Nothing Then
                    objThread = New Threading.Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = Company._Object._Companyunkid
                    objThread.Start(arr)
                Else
                    Call Send_Notification(Company._Object._Companyunkid)
                End If
            End If
            'Pinkal (20-May-2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveData", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Function GetEmployeeDepedentCountForCR() As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(txtEmployee.Tag))
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(txtEmployee.Tag), dtpDate.Value)
            'Sohail (18 May 2019) -- End
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            objDependents = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeDepedentCountForCR", mstrModuleName)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    'Pinkal (25-Oct-2018) -- End    


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try
            If xExpenditureTypeId <> enP2PExpenditureType.None Then

                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'Dim objEmpCC As New clsemployee_cctranhead_tran
                    'Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.Value.Date, True, CInt(txtEmployee.Tag))
                    'objEmpCC = Nothing
                    'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    '    mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
                    'End If
                    'dsList.Clear()
                    'dsList = Nothing
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    'Pinkal (04-Feb-2019) -- End
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        mintEmpCostCenterId = CInt(cboCostCenter.SelectedValue)
                        mstrCostCenterCode = CType(cboCostCenter.SelectedItem, DataRowView).Item("costcentercode").ToString()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetP2PRequireData", mstrModuleName)
        End Try
    End Sub


    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1399) NMB - Post approved claims amounts to P2P.
    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xGLCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), enMsgBoxStyle.Information)
                Return False
            End If

            Dim objMaster As New clsMasterData
            mstrP2PToken = objMaster.GetP2PToken(Company._Object._Companyunkid, Nothing)

            If mstrP2PToken.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())) = CInt(txtEmployee.Tag)

                Dim objUnitGroup As New clsUnitGroup
                objUnitGroup._Unitgroupunkid = objEmployee._Unitgroupunkid
                Dim mstrUnitGroupCode As String = objUnitGroup._Code.ToString().Trim()
                objUnitGroup = Nothing

                Dim objDepartment As New clsDepartment
                objDepartment._Departmentunkid = objEmployee._Departmentunkid
                Dim mstrDepartmentCode As String = objDepartment._Code.ToString().Trim
                objDepartment = Nothing

                Dim objSectionGroup As New clsSectionGroup
                objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                Dim mstrSectionGroupCode As String = objSectionGroup._Code.ToString().Trim
                objSectionGroup = Nothing

                Dim objClassGroup As New clsClassGroup
                objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
                Dim mstrClassGroupCode As String = objClassGroup._Code.ToString().Trim()
                objClassGroup = Nothing

                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                Dim mstrClassCode As String = objClass._Code.ToString().Trim()
                objClass = Nothing

                objEmployee = Nothing


                ' Dimensionvalue : NMB – SUB UNIT I – FUNCTION – DEPARTMENT – ZONE – BRANCH

                'NMB(-Hardcoded)
                'Sub UNIT I - Unit Group
                'FUNCTION - Department
                'Department - Section Group
                'Zone - Class Group
                'Branch - Class

                'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                'Dim mstrDimensionValue As String = "NMB" & IIf(mstrUnitGroupCode.Trim.Length > 0, "-" & mstrUnitGroupCode, "") & If(mstrDepartmentCode.Trim.Length > 0, "-" & mstrDepartmentCode, "") & _
                '                                                      IIf(mstrSectionGroupCode.Trim.Length > 0, "-" & mstrSectionGroupCode, "") & IIf(mstrClassGroupCode.Trim.Length > 0, "-" & mstrClassGroupCode, "") & _
                '                                                      IIf(mstrClassCode.Trim.Length > 0, "-" & mstrClassCode, "")
                'Dim mstrDimensionValue As String = xGLCode.Trim & IIf(mstrUnitGroupCode.Trim.Length > 0, "-" & mstrUnitGroupCode, "") & If(mstrDepartmentCode.Trim.Length > 0, "-" & mstrDepartmentCode, "") & _
                '                                                     IIf(mstrSectionGroupCode.Trim.Length > 0, "-" & mstrSectionGroupCode, "") & IIf(mstrClassGroupCode.Trim.Length > 0, "-" & mstrClassGroupCode, "") & _
                '                                                     IIf(mstrClassCode.Trim.Length > 0, "-" & mstrClassCode, "")

                Dim mstrDimensionValue As String = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrDepartmentCode.Trim() & "-" & mstrSectionGroupCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                'Pinkal (23-Dec-2023) -- End


                Dim xResponseData As String = ""
                Dim xPostedData As String = ""
                Dim objP2PBudgetBalanceService As New clsP2PBudgetBalanceService
                objP2PBudgetBalanceService.budgetBalanceRequest.CompanyCode = Company._Object._Code
                objP2PBudgetBalanceService.budgetBalanceRequest.GLCode = xGLCode.Trim
                objP2PBudgetBalanceService.budgetBalanceRequest.DimensionValue = mstrDimensionValue
                objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Company._Object._Financial_Start_Date.ToString("dd/MM/yyyy")
                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY" & Company._Object._Financial_Start_Date.ToString()
                'objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Format(New Date(2024, 1, 1).ToString(), "dd/MM/yyyy")
                'objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY2024"
                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetCycleCode = "Fiscal"
                objP2PBudgetBalanceService.budgetBalanceRequest.CalenderCode = "NMB-STD"

                If objMaster.GetSetP2PWebRequest(ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim(), True, True, "BudgetService", xResponseData, mstrError, objP2PBudgetBalanceService, xPostedData, mstrP2PToken.Trim) = False Then
                    eZeeMsgBox.Show(mstrError, enMsgBoxStyle.Information)
                    Return False
                End If


                If xResponseData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(xResponseData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mdecBudgetAmount = CDec(dtTable.Rows(0)("TotalFundsAvailableAmountMST"))
                    End If
                    objP2PBudgetBalanceService = Nothing
                End If  '  If xResponseData.Trim.Length > 0 Then

                End If

            objMaster = Nothing
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckBudgetRequestValidationForP2P", mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (16-Oct-2023) -- End

    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.Value.Date, True, CInt(txtEmployee.Tag))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeCostCenter", mstrModuleName)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, CInt(cboCurrency.SelectedValue), True, dtpDate.Value.Date, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (txtUnitPrice.Decimal * txtQty.Decimal) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            objExchange = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCurrencyRate", mstrModuleName)
        End Try
    End Sub

    'Pinkal (04-Feb-2019) -- End


    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Function CheckForOccurrence() As Boolean
        Try
            If mdtTran IsNot Nothing Then
                Dim objExpBalance As New clsEmployeeExpenseBalance
                Dim dsBalance As DataSet = objExpBalance.Get_Balance(CInt(txtEmployee.Tag), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date _
                                                                                        , CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC, True, False)), CBool(IIf(ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC, True, False)))

                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestApprovalExpTranId > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crapprovaltranunkid") <> mintClaimRequestApprovalExpTranId And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrClaimRequestApprovalExpGUID.Trim.Length > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrClaimRequestApprovalExpGUID.Trim() And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            eZeeMsgBox.Show(Language.getMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                               Language.getMessage("clsclaim_request_master", 4, " ] time(s)."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckForOccurrence", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (07-Mar-2019) -- End

    'Pinkal (22-Oct-2021)-- Start
    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.

    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.


            '            If gobjEmailList.Count > 0 Then
            '                Dim objSendMail As New clsSendMail
            'SendEmail:
            '                For Each objEmail In gobjEmailList
            '                    objSendMail._ToEmail = objEmail._EmailTo
            '                    objSendMail._Subject = objEmail._Subject
            '                    objSendMail._Message = objEmail._Message
            '                    objSendMail._Form_Name = objEmail._Form_Name
            '                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
            '                    objSendMail._OperationModeId = objEmail._OperationModeId
            '                    objSendMail._UserUnkid = objEmail._UserUnkid
            '                    objSendMail._SenderAddress = objEmail._SenderAddress
            '                    objSendMail._ModuleRefId = objEmail._ModuleRefId

            '                    If objEmail._FileName.ToString.Trim.Length > 0 Then
            '                        objSendMail._AttachedFiles = objEmail._FileName
            '                    End If

            '                    Dim intCUnkId As Integer = 0
            '                    If TypeOf intCompanyID Is Integer Then
            '                        intCUnkId = CInt(intCompanyID)
            '                    Else
            '                        intCUnkId = CInt(intCompanyID(0))
            '                    End If
            '                    If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
            '                                                objEmail._ExportReportPath).ToString.Length > 0 Then
            '                        gobjEmailList.Remove(objEmail)
            '                        GoTo SendEmail
            '                    End If
            '                Next
            '                gobjEmailList.Clear()
            '            End If

            If LstClaimEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In LstClaimEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._FormName
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = CInt(intCompanyID(0))
                    End If
                    objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), objEmail._ExportReportPath)
                Next
                If LstClaimEmailList.Count > 0 Then LstClaimEmailList.Clear()
                objSendMail = Nothing
                mblnCancel = False
                Me.Close()
            End If

            'Pinkal (20-May-2022) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Pinkal (22-Oct-2021)-- End


#End Region

#Region " Form's Events "

    Private Sub frmClaims_RequestAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objClaimMaster = New clsclaim_request_master
        objClaimTran = New clsclaim_request_tran
        objExpApproverTran = New clsclaim_request_approval_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            'txtUnitPrice.Enabled = False
            cboExpCategory.Enabled = False
            cboPeriod.Enabled = False
            dtpDate.Enabled = False
            objClaimMaster._Crmasterunkid = mintClaimRequestMasterId

            If mblnIsFromLeave Then
                Call Enable_Disable_Ctrls(False)
            Else
                Call Enable_Disable_Ctrls(True)
            End If
            Call GetValue()
            objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            'mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpCategory.SelectedValue), True, mintClaimApproverId, -1, "", mintClaimRequestMasterId).Tables(0)

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpCategory.SelectedValue), False, True, mintClaimApproverId, -1, "", mintClaimRequestMasterId).Tables(0)
            'Pinkal (03-Nov-2015) -- Start
            'Enhancement - WORKING ON Changes Required by TRA FOR Leave Expense Web Changes in Leave Application and Leave Approval.
            If _dtExpense Is Nothing Then
                mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName _
                                                                    , User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboExpCategory.SelectedValue), False _
                                                                    , True, mintClaimApproverId, "", mintClaimRequestMasterId).Tables(0)
            Else
                mdtFinalclaimTransaction = mdtTran.Copy
            End If
            'Pinkal (03-Nov-2015) -- End                                                                                 

            'Pinkal (24-Aug-2015) -- End


            cboLeaveType.Enabled = False
            cboReference.Enabled = False
            objbtnSearchReference.Enabled = False
            'Pinkal (22-Jun-2015) -- End


            Call Fill_Expense()

            If mblnIsFromLeave AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval = False Then
                btnOK.Visible = False
                objdgcolhEdit.Visible = False
                objdgcolhDelete.Visible = False
            End If

            'SHANI (06 JUN 2015) -- Start
            'Enhancement : Changes in C & R module given by Mr.Andrew.
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                cboLeaveType.Enabled = False
                cboReference.Enabled = False
                objbtnSearchReference.Enabled = False
            End If
            'SHANI (06 JUN 2015) -- End 

            'Pinkal (07-Sep-2015) -- Start
            'Enhancement - ADDING EXPENSE REMARK AND SECTOR ROUTE COLUMN FOR TRA AS PER DENNIS MAIL SUBJECT "TRA upgrade" SENT ON 03-Sep-2015.
            dgcolhQty.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhUnitPrice.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'Pinkal (07-Sep-2015) -- End

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            If mdtClaimAttchment Is Nothing Then
                Dim objAttchement As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(txtEmployee.Tag))
                Dim blnOnlyStrut As Boolean = False
                If CInt(txtEmployee.Tag) <= 0 Then
                    blnOnlyStrut = True
                End If
                Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(txtEmployee.Tag), , , , , blnOnlyStrut)
                'S.SANDEEP |04-SEP-2021| -- END
                Dim strTranIds As String = String.Join(",", mdtTran.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)

                If strTranIds.Trim.Length <= 0 Then strTranIds = "0"

                mdtClaimAttchment = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                objAttchement = Nothing
            End If
            'Shani (20-Aug-2016) -- End

            'Pinkal (20-Nov-2020) -- Start
            'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
            If Company._Object._Name.ToUpper() = "KADCO" Then
                lnkShowFuelConsumptionReport.Visible = True
            Else
                lnkShowFuelConsumptionReport.Visible = False
            End If
            'Pinkal (20-Nov-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClaims_RequestAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsclaim_request_approval_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsclaim_request_approval_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    Private Sub frmExpenseApproval_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If LstClaimEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmNewMDI", 77, "Sending Email(s) process is in progress. Please wait for some time."), enMsgBoxStyle.Information)
                e.Cancel = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpenseApproval_FormClosing", mstrModuleName)
        End Try
    End Sub
    'Pinkal (20-May-2022) -- End

#End Region

#Region " Button Event(s) "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If CInt(cboExpense.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Expense is mandatory information. Please select Expense to continue."), enMsgBoxStyle.Information)
                cboExpense.Focus()
                Exit Sub
            End If


            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            If ConfigParameter._Object._AllowOnlyOneExpenseInClaimApplication AndAlso dgvData.RowCount > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End


            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)



            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            If objExpense._DoNotAllowToApplyForBackDate AndAlso dtpDate.Value.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."))
                dtpDate.Focus()
                Exit Sub
            End If
            'Pinkal (16-Sep-2020) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            mblnIsHRExpense = objExpense._IsHRExpense

            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1399) NMB - Post approved claims amounts to P2P.

            mblnIsClaimFormBudgetMandatory = objExpense._IsBudgetMandatory
            mintGLCodeID = objExpense._GLCodeId
            mstrGLCode = ""
            mstrGLDescription = objExpense._Description
            mintCostCenterID = 0
            mstrCostCenterCode = ""
            If mblnIsClaimFormBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                If objExpense._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpense._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            'Pinkal (16-Oct-2023) -- End



            If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), enMsgBoxStyle.Information)
                cboSectorRoute.Select()
                Exit Sub
            End If


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < txtQty.Decimal Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), enMsgBoxStyle.Information)
                txtQty.Select()
                Exit Sub
            End If
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < txtQty.Decimal Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), enMsgBoxStyle.Information)
                txtQty.Select()
                Exit Sub
            End If
            'Pinkal (10-Jun-2020) -- End

            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            Dim objCommon As New clsCommon_Master
            objCommon._Masterunkid = objExpense._Journalunkid
            mstrJournal = objCommon._Name
            objCommon._Masterunkid = objExpense._Paymentmodeunkid
            mstrPaymentMode = objCommon._Name
            mblnIsDebit = objExpense._P2PDebit
            mblnIsCredit = objExpense._P2PCredit
            objCommon = Nothing
            'Pinkal (23-Dec-2023) -- End

            objExpense = Nothing


            If txtQty.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue."), enMsgBoxStyle.Information)
                txtQty.Focus()
                Exit Sub
            End If


            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            'If txtUnitPrice.Decimal <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Unit Price is mandatory information. Please enter Unit Price to continue."), enMsgBoxStyle.Information)
            '    txtUnitPrice.Focus()
            '    Exit Sub
            'End If

            If txtUnitPrice.Decimal <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 17, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                txtUnitPrice.Focus()
                Exit Sub
            End If
            End If
            'Pinkal (22-Oct-2018) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtExpRemark.Text.Trim.Length <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 17, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    txtExpRemark.Focus()
                    Exit Sub
                End If
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            End If

            'Pinkal (04-Feb-2019) -- End


            Dim dtmp() As DataRow = Nothing
            If mdtTran.Rows.Count > 0 Then

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND AUD <> 'D'")

                dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & txtQty.Decimal & _
                                                            "' AND unitprice = '" & txtUnitPrice.Decimal & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")

                'Pinkal (04-Feb-2019) -- End

                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                    'Pinkal (07-Jul-2018) -- Start
                    'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
                    If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                If txtQty.Decimal > txtBalance.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), enMsgBoxStyle.Information)
                    txtQty.Focus()
                    Exit Sub
                End If
                    ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        If txtQty.Decimal > txtBalanceAsOnDate.Decimal Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set."), enMsgBoxStyle.Information)
                            txtQty.Focus()
                            Exit Sub
                        End If
                    End If
                    'Pinkal (07-Jul-2018) -- End

            ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                If txtBalance.Decimal < CDec(txtQty.Decimal * txtUnitPrice.Decimal) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot set Amount greater than balance set."), enMsgBoxStyle.Information)
                    txtQty.Focus()
                    Exit Sub
                End If
            End If
            End If



            'Pinkal (29-Feb-2016) -- Start
            'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.

            Dim sMsg As String = String.Empty

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(txtEmployee.Tag), CInt(cboExpense.SelectedValue))


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.


            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(txtEmployee.Tag), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid _
            '                                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
            '                                                                                  , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), mintClaimRequestMasterId)

            sMsg = objClaimMaster.IsValid_Expense(CInt(txtEmployee.Tag), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid _
                                                                                              , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, dtpDate.Value.Date _
                                                                                             , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), mintClaimRequestMasterId, True)

            'Pinkal (22-Jan-2020) -- End

            'Pinkal (07-Mar-2019) -- End

            'Pinkal (26-Feb-2019) -- End


            If sMsg <> "" Then
                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                Exit Sub
            End If
            sMsg = ""


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CheckForOccurrence() = False Then Exit Sub
            'Pinkal (07-Mar-2019) -- End


            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1399) NMB - Post approved claims amounts to P2P.
            If objExpMaster._IsBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim(), mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (txtQty.Decimal * txtUnitPrice.Decimal) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Pinkal (16-Oct-2023) -- End


            If dgvData.Rows.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Rows(0).Cells(objcolhRequestTranID.Index).Value) > 0 Then
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Rows(0).Cells(objcolhRequestTranID.Index).Value) & "' AND AUD <> 'D'")
                ElseIf dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                End If
                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses on the same form they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    'Pinkal (30-Apr-2018) - End
                    Exit Sub


                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    Exit Sub
                    'Pinkal (04-Feb-2019) -- End

                End If
                objExpMasterOld = Nothing
            End If
            objExpMaster = Nothing

            Dim dRow As DataRow = mdtTran.NewRow

            dRow.Item("crapprovaltranunkid") = -1
            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = mintClaimRequestMasterId
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCosting.Tag Is Nothing, 0, txtCosting.Tag)
            dRow.Item("unitprice") = txtUnitPrice.Decimal
            dRow.Item("quantity") = txtQty.Decimal
            dRow.Item("amount") = txtUnitPrice.Decimal * txtQty.Decimal
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("iscancel") = False
            dRow.Item("canceluserunkid") = -1
            dRow.Item("cancel_datetime") = DBNull.Value
            dRow.Item("cancel_remark") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("employeeunkid") = CInt(txtEmployee.Tag)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.Text)

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.Value.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            'Pinkal (04-Feb-2019) -- End


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtTran.Columns.Contains("tdate") Then
                dRow.Item("tdate") = eZeeDate.convertDate(dtpDate.Value.Date).ToString()
            End If
            'Pinkal (07-Mar-2019) -- End

            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1399) NMB - Post approved claims amounts to P2P.
            dRow.Item("claimrequestno") = txtClaimNo.Text.Trim
            dRow.Item("expense") = cboExpense.Text
            dRow.Item("claim_remark") = txtClaimRemark.Text.Trim
            'Pinkal (16-Oct-2023) -- End

            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            dRow.Item("journal") = mstrJournal
            dRow.Item("paymentmode") = mstrPaymentMode
            dRow.Item("isdebit") = mblnIsDebit
            dRow.Item("iscredit") = mblnIsCredit
            'Pinkal (23-Dec-2023) -- End


            mdtTran.Rows.Add(dRow) : Call Fill_Expense()

            Call Enable_Disable_Ctrls(False) : Call Clear_Controls()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try


            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            If ConfigParameter._Object._AllowOnlyOneExpenseInClaimApplication AndAlso dgvData.RowCount > 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Dim dtmp() As DataRow = Nothing
            If iRow IsNot Nothing Then
                If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                    dtmp = mdtTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & txtQty.Decimal & _
                                                         "' AND unitprice = '" & txtUnitPrice.Decimal & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & txtQty.Decimal & _
                                                      "' AND unitprice = '" & txtUnitPrice.Decimal & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim mintGLCodeID As Integer = 0
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = ""
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""

            'Pinkal (04-Feb-2019) -- End


            If dgvData.Rows.Count >= 1 Then
                Dim objExpMaster As New clsExpense_Master
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Rows(0).Cells(objcolhRequestTranID.Index).Value) > 0 Then
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Rows(0).Cells(objcolhRequestTranID.Index).Value) & "' AND AUD <> 'D'")
                ElseIf dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Rows(0).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                End If
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    Exit Sub

                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), enMsgBoxStyle.Information)
                    Exit Sub
                    'Pinkal (04-Feb-2019) -- End

                End If

                'Pinkal (22-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                If txtUnitPrice.Decimal <= 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                          Language.getMessage(mstrModuleName, 17, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        txtUnitPrice.Focus()
                        Exit Sub
                    End If
                End If
                'Pinkal (22-Oct-2018) -- End

                'Pinkal (25-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .

                If txtQty.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue."), enMsgBoxStyle.Information)
                    txtQty.Focus()
                    Exit Sub
                End If


                If objExpMaster._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < txtQty.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), enMsgBoxStyle.Information)
                    txtQty.Select()
                    Exit Sub
                End If
                'Pinkal (25-Oct-2018) -- End

                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                If objExpMaster._Expense_MaxQuantity > 0 AndAlso objExpMaster._Expense_MaxQuantity < txtQty.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), enMsgBoxStyle.Information)
                    txtQty.Select()
                    Exit Sub
                End If
                'Pinkal (10-Jun-2020) -- End


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                If txtExpRemark.Text.Trim.Length <= 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 17, "Do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        txtExpRemark.Focus()
                        Exit Sub
                    End If
                End If

                If CInt(cboCurrency.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue."), enMsgBoxStyle.Information)
                    cboCurrency.Focus()
                    Exit Sub
                End If

                mblnIsHRExpense = objExpMaster._IsHRExpense
                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

                mintGLCodeID = objExpMaster._GLCodeId
                mstrGLDescription = objExpMaster._Description
                If mblnIsClaimFormBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If

                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1399) NMB - Post approved claims amounts to P2P.
                If objExpMaster._IsBudgetMandatory AndAlso ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim.Length > 0 Then
                    Dim mdecBudgetAmount As Decimal = 0
                    If CheckBudgetRequestValidationForP2P(ConfigParameter._Object._BgtRequestValidationP2PServiceURL.Trim(), mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                    If mdecBudgetAmount < (txtQty.Decimal * txtUnitPrice.Decimal) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'Pinkal (16-Oct-2023) -- End

                'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                Dim objCommon As New clsCommon_Master
                objCommon._Masterunkid = objExpMaster._Journalunkid
                mstrJournal = objCommon._Name
                objCommon._Masterunkid = objExpMaster._Paymentmodeunkid
                mstrPaymentMode = objCommon._Name
                mblnIsDebit = objExpMaster._P2PDebit
                mblnIsCredit = objExpMaster._P2PCredit
                objCommon = Nothing
                'Pinkal (23-Dec-2023) -- End

                If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then

                        'Pinkal (07-Jul-2018) -- Start
                        'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
                        If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                    If txtQty.Decimal > txtBalance.Decimal Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), enMsgBoxStyle.Information)
                        txtQty.Focus()
                        Exit Sub
                    End If
                        ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                            If txtQty.Decimal > txtBalanceAsOnDate.Decimal Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set."), enMsgBoxStyle.Information)
                                txtQty.Focus()
                                Exit Sub
                            End If
                        End If
                        'Pinkal (07-Jul-2018) -- End 
                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    If txtBalance.Decimal < CDec(txtQty.Decimal * txtUnitPrice.Decimal) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot set Amount greater than balance set."), enMsgBoxStyle.Information)
                        txtQty.Focus()
                        Exit Sub
                    End If
                End If
                End If
                objExpMaster = Nothing
                objExpMasterOld = Nothing
            End If


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CheckForOccurrence() = False Then Exit Sub
            'Pinkal (07-Mar-2019) -- End


            If iRow IsNot Nothing Then

                iRow(0).Item("crapprovaltranunkid") = iRow(0).Item("crapprovaltranunkid")
                iRow(0).Item("crmasterunkid") = mintClaimRequestMasterId
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCosting.Tag Is Nothing, 0, txtCosting.Tag)
                iRow(0).Item("unitprice") = txtUnitPrice.Decimal
                iRow(0).Item("quantity") = txtQty.Decimal
                iRow(0).Item("amount") = txtUnitPrice.Decimal * txtQty.Decimal
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("iscancel") = False
                iRow(0).Item("canceluserunkid") = -1
                iRow(0).Item("cancel_datetime") = DBNull.Value
                iRow(0).Item("cancel_remark") = ""
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("employeeunkid") = CInt(txtEmployee.Tag)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.Text)


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                iRow(0).Item("ishrexpense") = mblnIsHRExpense
                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.Value.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                'Pinkal (04-Feb-2019) -- End


                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1399) NMB - Post approved claims amounts to P2P.
                iRow(0).Item("claimrequestno") = txtClaimNo.Text.Trim
                iRow(0).Item("expense") = cboExpense.Text
                iRow(0).Item("claim_remark") = txtClaimRemark.Text.Trim
                'Pinkal (16-Oct-2023) -- End

                'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                iRow(0).Item("journal") = mstrJournal
                iRow(0).Item("paymentmode") = mstrPaymentMode
                iRow(0).Item("isdebit") = mblnIsDebit
                iRow(0).Item("iscredit") = mblnIsCredit
                'Pinkal (23-Dec-2023) -- End


                mdtTran.AcceptChanges()
                Call Fill_Expense() : Call Clear_Controls()

                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True : objbtnSearchExpense.Enabled = True

                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If Is_Valid() = False Then Exit Sub

            If SaveData(sender) Then
                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                If LstClaimEmailList.Count <= 0 Then
                mblnCancel = False
                Me.Close()
            End If
                'Pinkal (20-May-2022) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            If Is_Valid() = False Then Exit Sub

            gbExpenseInformation.Enabled = False
            dgvData.Enabled = False
            txtGrandTotal.Enabled = False
            lblGrandTotal.Enabled = False
            objFooter.Enabled = False
            gbRejectRemark.Visible = True
            gbRejectRemark.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReject_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRemarkOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemarkOk.Click
        Try

            If Is_Valid() = False Then Exit Sub

            If SaveData(sender) Then
                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                If LstClaimEmailList.Count <= 0 Then
                mblnCancel = False
                Me.Close()
            End If
                'Pinkal (20-May-2022) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRemarkOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRemarkClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemarkClose.Click
        Try
            gbExpenseInformation.Enabled = True
            txtGrandTotal.Enabled = True
            lblGrandTotal.Enabled = True
            dgvData.Enabled = True
            objFooter.Enabled = True
            gbRejectRemark.Visible = False
            gbRejectRemark.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRemarkClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            If Is_Valid() = False Then Exit Sub

            If SaveData(sender) Then
                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                If LstClaimEmailList.Count <= 0 Then
                mblnCancel = False
                Me.Close()
            End If
                'Pinkal (20-May-2022) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOK_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (03-Nov-2015) -- Start
            'Enhancement - WORKING ON Changes Required by TRA FOR Leave Expense Web Changes in Leave Application and Leave Approval.
            If mblnIsFromLeave Then
                If mdtFinalclaimTransaction IsNot Nothing Then
                    mdtTran = mdtFinalclaimTransaction.Copy
                    mdtFinalclaimTransaction.Clear()
                    mdtFinalclaimTransaction = Nothing
                Else
                    mdtTran.Clear()
                    mdtTran = Nothing
                End If
            End If
            'Pinkal (03-Nov-2015) -- End

            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.
            If LstClaimEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmNewMDI", 77, "Sending Email(s) process is in progress. Please wait for some time."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (20-May-2022) -- End

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchReference_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReference.Click
        Dim frm As New frmCommonSearch
        Try
            If cboReference.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboReference.ValueMember
                .DisplayMember = cboReference.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboReference.DataSource, DataTable)

                If .DisplayDialog Then
                    cboReference.SelectedValue = .SelectedValue
                    cboReference.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReference_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            If cboExpense.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboExpense.ValueMember
                .DisplayMember = cboExpense.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboExpense.DataSource, DataTable)

                If .DisplayDialog Then
                    cboExpense.SelectedValue = .SelectedValue
                    cboExpense.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchSecRoute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSecRoute.Click
        Dim frm As New frmCommonSearch
        Try
            If cboSectorRoute.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboSectorRoute.ValueMember
                .DisplayMember = cboSectorRoute.DisplayMember
                .DataSource = CType(cboSectorRoute.DataSource, DataTable)

                If .DisplayDialog Then
                    cboSectorRoute.SelectedValue = .SelectedValue
                    cboSectorRoute.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSecRoute_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnViewScanAttchment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewScanAttchment.Click
        Try
            Dim objForm As New frmPreviewDocuments
            If User._Object._Isrighttoleft = True Then
                objForm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objForm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objForm)
            End If


            Dim mstrTranId As String = ""
            Dim objScanAttach As New clsScan_Attach_Documents
            Dim dtTable As DataTable = objScanAttach.GetAttachmentTranunkIds(CInt(txtEmployee.Tag), _
                                                                             enScanAttactRefId.CLAIM_REQUEST, _
                                                                             enImg_Email_RefId.Claim_Request, _
                                                                             mintClaimRequestMasterId)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrTranId = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If

            objForm.displayDialog(mstrTranId.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnViewScanAttchment_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Sub objbtnSearchCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCostCenter.Click
        Dim frm As New frmCommonSearch
        Try
            If cboCostCenter.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboCostCenter.ValueMember
                .DisplayMember = cboCostCenter.DisplayMember
                .DataSource = CType(cboCostCenter.DataSource, DataTable)
                If .DisplayDialog Then
                    cboCostCenter.SelectedValue = .SelectedValue
                    cboCostCenter.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCostCenter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (04-Feb-2019) -- End

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End

                dtpDate.MinDate = CDate(eZeeDate.convertDate("17530101"))
                dtpDate.MaxDate = CDate(eZeeDate.convertDate("99981231"))

                dtpDate.Value = objPeriod._Start_Date
                'Shani [ 26 SEP 2014 ] -- START
                'Changes- Removeing Period ComboBox Control From Screen. From GIVEN BY Andrew Sir 
                dtpDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                'Shani [ 26 SEP 2014 ] -- END
                dtpDate.MaxDate = objPeriod._End_Date

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboExpCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(cboExpCategory.SelectedValue), True, "List", CInt(txtEmployee.Tag), True)

            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            'With cboExpense
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("List")
            '    .SelectedValue = 0
            'End With

            Dim dtTable As DataTable = Nothing
            If mblnIsFromLeave = False Then
                'Pinkal (04-Aug-2020) -- Start
                'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	
                If ConfigParameter._Object._PaymentApprovalwithLeaveApproval = False Then
                    dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable()
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "cr_expinvisible = 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                'Pinkal (04-Aug-2020) -- End
            Else
                dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            'Pinkal (22-Mar-2016) -- End

            If CInt(cboExpCategory.SelectedValue) > 0 Then

                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_LEAVE

                        'Pinkal (22-Jun-2015) -- Start
                        'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
                        objlblValue.Text = Language.getMessage(mstrModuleName, 14, "Leave Form")
                        'Pinkal (22-Jun-2015) -- End
                        Dim objLeaveType As New clsleavetype_master
                        dsCombo = objLeaveType.getListForCombo("List", True, 1)
                        With cboLeaveType
                            .ValueMember = "leavetypeunkid"
                            .DisplayMember = "name"
                            .DataSource = dsCombo.Tables("List")
                            .SelectedValue = 0
                        End With
                        objLeaveType = Nothing

                    Case enExpenseType.EXP_MEDICAL
                        'Dim obj
                    Case enExpenseType.EXP_TRAINING
                        dsCombo = clsTraining_Enrollment_Tran.GetEmployee_TrainingList(CInt(txtEmployee.Tag), True)
                        With cboReference
                            .ValueMember = "Id"
                            .DisplayMember = "Name"
                            .DataSource = dsCombo.Tables("List")
                            .SelectedValue = 0
                        End With

                End Select

                'Pinkal (23-Oct-2015) -- Start
                'Enhancement - Putting Domicile Address for TRA as per Dennis Requirement
                If txtEmployee.Tag IsNot Nothing Then
                    Dim objEmployee As New clsEmployee_Master
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployee._Employeeunkid = CInt(txtEmployee.Tag)
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtEmployee.Tag)
                    'S.SANDEEP [04 JUN 2015] -- END

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If
                'Pinkal (23-Oct-2015) -- End

            Else
                objlblValue.Text = "" : cboReference.DataSource = Nothing : cboReference.Text = ""
                cboLeaveType.Enabled = False
                'Pinkal (23-Oct-2015) -- Start
                'Enhancement - Putting Domicile Address for TRA as per Dennis Requirement
                txtDomicileAddress.Text = ""
                'Pinkal (23-Oct-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboExpense_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try

            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If ConfigParameter._Object._SectorRouteAssignToExpense Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .ValueMember = "secrouteunkid"
                    .DisplayMember = "Sector"
                    .DataSource = dtSector
                    .SelectedIndex = 0
                End With
            End If
            'Pinkal (20-Feb-2019) -- End

            Dim objExpMaster As New clsExpense_Master

            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment
                cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                objbtnSearchSecRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSectorRoute.SelectedValue = 0



                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                cboCostCenter.SelectedValue = 0

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                    cboCostCenter.Enabled = False
                    objbtnSearchCostCenter.Enabled = False
                ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                    cboCostCenter.SelectedValue = GetEmployeeCostCenter()
                    cboCostCenter.Enabled = False
                    objbtnSearchCostCenter.Enabled = False
                ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                    cboCostCenter.Enabled = True
                    objbtnSearchCostCenter.Enabled = True
                End If
                Else
                    cboCostCenter.Enabled = False
                    objbtnSearchCostCenter.Enabled = False
                End If
                'Pinkal (25-May-2019) -- End

                'Pinkal (04-Feb-2019) -- End

                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtBalance.Text = "0.00"
                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    dsBal = objEmpExpBal.Get_Balance(CInt(txtEmployee.Tag), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date)
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal"))
                        txtBalanceAsOnDate.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate"))
                        'Pinkal (30-Apr-2018) - End
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then
                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing

                    'Pinkal (09-Aug-2018) -- Start
                    'Bug - Solving Bug for PACRA.

                    'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    '    dsList = objLeave.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                         , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                    '                                         , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing, mblnIsExternalApprover)

                    'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    '    dsList = objLeave.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                         , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                    '                                         , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing, mblnIsExternalApprover)
                    'End If

                    'dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable

                    Dim mstrLeaveTypeID As String = ""
                    Dim objleavetype As New clsleavetype_master
                    mstrLeaveTypeID = objleavetype.GetDeductdToLeaveTypeIDs(CInt(objExpMaster._Leavetypeunkid))

                    If mstrLeaveTypeID.Trim.Length > 0 Then
                        mstrLeaveTypeID &= "," & CInt(objExpMaster._Leavetypeunkid).ToString()
                    Else
                        mstrLeaveTypeID = CInt(objExpMaster._Leavetypeunkid).ToString()
                    End If

                    objleavetype = Nothing


                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.Value.Date, mstrLeaveTypeID, CInt(txtEmployee.Tag).ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting _
                                                                     , ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date _
                                                                     , False, False, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid, Nothing)
                    Else
                        objLeave._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                        objLeave._DBEnddate = FinancialYear._Object._Database_End_Date.Date

                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.Value.Date, mstrLeaveTypeID, CInt(txtEmployee.Tag).ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting _
                                                                     , ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing _
                                                                     , True, True, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid, Nothing)
                    End If

                    dtbalance = dsList.Tables(0)



                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then
                        txtBalance.Decimal = CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount"))

                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.

                        'If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then

                        '    If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = FinancialYear._Object._Database_End_Date.Date
                        '    If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.Value.Date Then
                        '        txtBalanceAsOnDate.Decimal = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2)
                        '    Else
                        '        txtBalanceAsOnDate.Decimal = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.Value.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2)
                        '    End If

                        'ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then

                        '    'Pinkal (18-Jul-2018) -- Start
                        '    'Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.
                        '    'Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, dtpDate.Value.Date))

                        '    Dim mdtDate As DateTime = dtpDate.Value.Date
                        '    Dim mdtDays As Integer = 0
                        '    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        '        If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                        '            mdtDate = FinancialYear._Object._Database_End_Date.Date
                        '            mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, FinancialYear._Object._Database_End_Date.Date))
                        '        Else
                        '            If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '                mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '            End If
                        '        End If

                        '    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        '        If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '            mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '        End If
                        '        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '    End If

                        '    Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                        '    If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > dtpDate.Value.Date.Day OrElse intDiff > mdtDays Then
                        '        intDiff = intDiff - 1
                        '    End If
                        '    txtBalanceAsOnDate.Decimal = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2)
                        'End If
                        'Pinkal (30-Apr-2018) - End

                        txtBalance.Decimal = CDec(dtbalance.Rows(0)("TotalAcccrueAmt")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))
                        txtBalanceAsOnDate.Decimal = CDec(CDec(dtbalance.Rows(0)("Accrue_amount")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment"))))

                        'Pinkal (09-Aug-2018) -- End

                        txtUoMType.Text = Language.getMessage("clsExpCommonMethods", 6, "Quantity")
                    End If

                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance
                    Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    dsBal = objEmpExpBal.Get_Balance(CInt(txtEmployee.Tag), CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date)
                    'Pinkal (30-Apr-2018) - End

                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal"))
                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        txtBalanceAsOnDate.Decimal = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate"))
                        'Pinkal (30-Apr-2018) - End
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    objEmpExpBal = Nothing
                End If
            Else
                txtUnitPrice.Enabled = True

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End

                txtUoMType.Text = ""
                'Pinkal (30-Apr-2018) - Start
                'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                txtBalance.Decimal = 0
                txtBalanceAsOnDate.Decimal = 0
                'Pinkal (30-Apr-2018) - End


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                cboCostCenter.Enabled = False
                objbtnSearchCostCenter.Enabled = False
                cboCostCenter.SelectedValue = 0
                'Pinkal (04-Feb-2019) -- End

                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                txtQty.Decimal = 1
                'Pinkal (04-Feb-2020) -- End


            End If


            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager. AS Per Guidance with Matthew on 06-Feb-2020. 
            'If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory = False Then
            If objExpMaster._IsConsiderDependants Then
                txtQty.Decimal = GetEmployeeDepedentCountForCR()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Decimal)
            Else
                txtQty.Decimal = 1
            End If
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES. [shall i allow to edit quantity in Claim approval as per yesterday conversation ? Matthew 26-Jun-2019, 11:44  Yes, allow. Only on approval only]
            'If objExpMaster._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
            'Pinkal (13-Jun-2019) -- End
            txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                lblBalance.Visible = True
                lblBalanceasondate.Visible = True
                txtBalance.Visible = True
                txtBalanceAsOnDate.Visible = True
                cboCurrency.SelectedValue = mintBaseCountryId
                cboCurrency.Enabled = False
            Else
                lblBalance.Visible = False
                lblBalanceasondate.Visible = False
                txtBalance.Visible = False
                txtBalanceAsOnDate.Visible = False
                If mdtTran IsNot Nothing Then
                    If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First()
                    Else
                        cboCurrency.SelectedValue = mintBaseCountryId
                        cboCurrency.Enabled = True
                    End If
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpense_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboLeaveType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        Try
            Dim objleave As New clsleaveform : Dim dsLeave As New DataSet

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If mblnIsFromLeave Then
                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                'dsLeave = objleave.getListForCombo(CInt(cboLeaveType.SelectedValue), "2,7", CInt(txtEmployee.Tag), True, True)
                dsLeave = objleave.getListForCombo(CInt(cboLeaveType.SelectedValue), "2,7", CInt(txtEmployee.Tag), True, False)
                'Pinkal (10-Jan-2017) -- End
            Else
            dsLeave = objleave.getListForCombo(CInt(cboLeaveType.SelectedValue), "7", CInt(txtEmployee.Tag), True, True)
            End If
            'dsLeave = objleave.getListForCombo(CInt(cboLeaveType.SelectedValue), "7", CInt(txtEmployee.Tag), True, True)
            'Pinkal (06-Jan-2016) -- End


            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            Dim dtTable As DataTable = Nothing
            If objClaimMaster._Referenceunkid > 0 Then
                dtTable = New DataView(dsLeave.Tables(0), "formunkid=" & objClaimMaster._Referenceunkid, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsLeave.Tables(0)
            End If
            'Pinkal (10-Jan-2017) -- End



            With cboReference
                .ValueMember = "formunkid"
                .DisplayMember = "name"
                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                '.DataSource = dsLeave.Tables(0)
                '.SelectedValue = 0
                .DataSource = dtTable

                'Pinkal (16-Nov-2021)-- Start
                'NMB Claim Request Approval Bug when approver wants to change the approval status.
                '.SelectedIndex = 0
                .SelectedValue = 0
                'Pinkal (16-Nov-2021) -- End


                'Pinkal (10-Jan-2017) -- End


            End With
            objleave = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtQty_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtQty.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Enter, Keys.Tab
                    txtUnitPrice.Focus()
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtQty_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtQty_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles txtQty.PreviewKeyDown
        Try
            If e.KeyData = Keys.Tab Then
                e.IsInputKey = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtQty_PreviewKeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        Try
            If txtQty.Decimal > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                If mblnIsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    'Pinkal (18-Sep-2015) -- Start
                    'Enhancement - PROBLEM SOLVING IN  C & R FOR TRA.
                    If txtUnitPrice.Decimal <= 0 Then
                    txtUnitPrice.Decimal = CDec(Format(CDec(txtCosting.Text), GUI.fmtCurrency))
                    End If
                    'Pinkal (18-Sep-2015) -- End
                Else
                    txtUnitPrice.Enabled = False
                End If
            ElseIf txtQty.Decimal <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                'Pinkal (18-Sep-2015) -- Start
                'Enhancement - PROBLEM SOLVING IN  C & R FOR TRA.
                If txtUnitPrice.Decimal <= 0 Then
                txtUnitPrice.Decimal = CDec(Format(CDec(txtCosting.Text), GUI.fmtCurrency))
            End If
                'Pinkal (18-Sep-2015) -- End
            End If

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            objExpense = Nothing
            'Pinkal (18-Mar-2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtQty_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DateTime Picker Event(s) "

    Private Sub dtpDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDate.ValueChanged, cboSectorRoute.SelectedValueChanged
        Try
            Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
            Dim objCosting As New clsExpenseCosting
            objCosting.GetDefaultCosting(CInt(cboSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.Value.Date)
            txtCosting.Text = Format(CDec(iAmount), GUI.fmtCurrency)
            txtCosting.Tag = iCostingId
            objCosting = Nothing
            txtUnitPrice.Text = Format(CDec(iAmount), GUI.fmtCurrency)


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory Then
                txtQty.Decimal = GetEmployeeDepedentCountForCR()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Decimal)
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES. [shall i allow to edit quantity in Claim approval as per yesterday conversation ? Matthew 26-Jun-2019, 11:44  Yes, allow. Only on approval only]
            'If objExpMaster._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
            'Pinkal (13-Jun-2019) -- End
            txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End

            objExpMaster = Nothing
            'Pinkal (25-Oct-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index

                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhApproverTranId.Index).Value) > 0 Then
                        iRow = mdtTran.Select("crapprovaltranunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhApproverTranId.Index).Value) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                        iRow = mdtTran.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                    End If

                    If iRow.Length > 0 Then
                        btnAdd.Visible = False : btnEdit.Visible = True
                        cboExpense.SelectedValue = iRow(0).Item("expenseunkid")
                        cboSectorRoute.SelectedValue = iRow(0).Item("secrouteunkid")
                        txtQty.Decimal = CDec(iRow(0).Item("quantity"))
                        txtUnitPrice.Decimal = CDec(Format(CDec(iRow(0).Item("unitprice")), GUI.fmtCurrency))
                        txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))

                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        cboExpense.Enabled = False : objbtnSearchExpense.Enabled = False
                        cboCostCenter.SelectedValue = CInt(iRow(0).Item("costcenterunkid"))
                        cboCostCenter.Enabled = False : objbtnSearchCostCenter.Enabled = False
                        'Pinkal (04-Feb-2019) -- End


                        'Pinkal (07-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        mintClaimRequestApprovalExpTranId = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhApproverTranId.Index).Value)
                        mstrClaimRequestApprovalExpGUID = dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString()
                        'Pinkal (07-Mar-2019) -- End


                        Enable_Disable_Ctrls(False)
                    End If

                Case objdgcolhDelete.Index

                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhApproverTranId.Index).Value) > 0 Then
                        iRow = mdtTran.Select("crapprovaltranunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhApproverTranId.Index).Value) & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            Dim iVoidReason As String = String.Empty
                            Dim frm As New frmReasonSelection
                            If User._Object._Isrighttoleft = True Then
                                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                                frm.RightToLeftLayout = True
                                Call Language.ctlRightToLeftlayOut(frm)
                            End If

                            frm.displayDialog(enVoidCategoryType.OTHERS, iVoidReason)
                            If iVoidReason.Trim.Length <= 0 Then Exit Sub

                            iRow(0).Item("isvoid") = True
                            iRow(0).Item("voiduserunkid") = User._Object._Userunkid
                            iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            iRow(0).Item("voidreason") = iVoidReason
                            iRow(0).Item("AUD") = "D"

                        End If
                    ElseIf dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString <> "" Then
                        iRow = mdtTran.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            iRow(0).Item("AUD") = "D"
                        End If
                    End If
                    mdtTran.AcceptChanges()
                    Call Fill_Expense()
                    Call Clear_Controls()
                    btnEdit.Visible = False
                    btnAdd.Visible = True
                    Enable_Disable_Ctrls(True)

                    'Shani (20-Aug-2016) -- Start
                    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                Case objcolhPreview.Index
                    Dim objForm As New frmPreviewDocuments
                    If User._Object._Isrighttoleft = True Then
                        objForm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        objForm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(objForm)
                    End If


                    Dim mstrTranId As String = ""
                    If mdtClaimAttchment IsNot Nothing AndAlso mdtClaimAttchment.Rows.Count > 0 Then
                        Dim dRow() As DataRow
                        If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhApproverTranId.Index).Value) > 0 Then
                            dRow = mdtClaimAttchment.Select("transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objcolhRequestTranID.Index).Value) & "' AND AUD <> 'D'")
                            mstrTranId = String.Join(",", dRow.Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
                        End If
                    End If
                    objForm.displayDialog(mstrTranId.ToString)
                    'Shani (20-Aug-2016) -- End


            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Link Button Event"

    Private Sub LnkViewDependants_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LnkViewDependants.LinkClicked
        Try

            Dim objfrm As New frmDependentsList

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                objfrm.displayDialog(CInt(txtEmployee.Tag), False, True, dtpDate.Value.Date)
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                objfrm.displayDialog(CInt(txtEmployee.Tag), True, False, dtpDate.Value.Date)
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                objfrm.displayDialog(CInt(txtEmployee.Tag), False, False, dtpDate.Value.Date)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LnkViewDependants_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'Gajanan [26-Dec-2019] -- Start   
    'Enhancement:Worked On Grievance Reporting To Testing Document
    Private Sub LnkViewAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LnkViewAllocation.LinkClicked
        Try
            Dim frm As New frmEmployeeDiary
            frm.displayDialog(CInt(txtEmployee.Tag), True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LnkViewAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [26-Dec-2019] -- End


    'Pinkal (20-Nov-2020) -- Start
    'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
    Private Sub lnkShowFuelConsumptionReport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShowFuelConsumptionReport.LinkClicked
        Dim objFuelConsumption As New clsFuelConsumptionReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        Try
            objFuelConsumption.SetDefaultValue()
            objFuelConsumption._FromDate = New DateTime(dtpDate.Value.Year, dtpDate.Value.Month, 1)
            objFuelConsumption._ToDate = New DateTime(dtpDate.Value.Year, dtpDate.Value.Month, DateTime.DaysInMonth(dtpDate.Value.Year, dtpDate.Value.Month))
            objFuelConsumption._ExpCateId = CInt(cboExpCategory.SelectedValue)
            objFuelConsumption._ExpCateName = cboExpCategory.Text.ToString()
            objFuelConsumption._EmpUnkId = CInt(txtEmployee.Tag)
            objFuelConsumption._EmpName = txtEmployee.Text.Trim()
            objFuelConsumption._StatusId = 1
            objFuelConsumption._StatusName = Language.getMessage("clsMasterData", 110, "Approved")
            objFuelConsumption._ShowRequiredQuanitty = True
            objFuelConsumption._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objFuelConsumption._UserUnkid = User._Object._Userunkid
            objFuelConsumption._CompanyUnkId = Company._Object._Companyunkid

            objFuelConsumption.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, _
                                         0, enPrintAction.Preview, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShowFuelConsumptionReport_LinkClicked", mstrModuleName)
        Finally
            objFuelConsumption = Nothing
        End Try
    End Sub
    'Pinkal (20-Nov-2020) -- End

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()

            Me.gbExpenseInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbExpenseInformation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbRejectRemark.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRejectRemark.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
			Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnReject.GradientBackColor = GUI._ButttonBackColor
			Me.btnReject.GradientForeColor = GUI._ButttonFontColor

            Me.btnRemarkClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnRemarkClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnRemarkOk.GradientBackColor = GUI._ButttonBackColor
			Me.btnRemarkOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnOK.GradientBackColor = GUI._ButttonBackColor
			Me.btnOK.GradientForeColor = GUI._ButttonFontColor

            Me.btnViewScanAttchment.GradientBackColor = GUI._ButttonBackColor
			Me.btnViewScanAttchment.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.gbExpenseInformation.Text = Language._Object.getCaption(Me.gbExpenseInformation.Name, Me.gbExpenseInformation.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.Name, Me.lblExpCategory.Text)
			Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.Name, Me.lblExpense.Text)
			Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.Name, Me.lblUoM.Text)
			Me.lblCosting.Text = Language._Object.getCaption(Me.lblCosting.Name, Me.lblCosting.Text)
			Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
			Me.lblQty.Text = Language._Object.getCaption(Me.lblQty.Name, Me.lblQty.Text)
			Me.lblUnitPrice.Text = Language._Object.getCaption(Me.lblUnitPrice.Name, Me.lblUnitPrice.Text)
			Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.Name, Me.lblGrandTotal.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.gbRejectRemark.Text = Language._Object.getCaption(Me.gbRejectRemark.Name, Me.gbRejectRemark.Text)
			Me.btnRemarkClose.Text = Language._Object.getCaption(Me.btnRemarkClose.Name, Me.btnRemarkClose.Text)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.btnRemarkOk.Text = Language._Object.getCaption(Me.btnRemarkOk.Name, Me.btnRemarkOk.Text)
			Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.Name, Me.btnOK.Text)
			Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.Name, Me.lblSector.Text)
			Me.tbExpenseRemark.Text = Language._Object.getCaption(Me.tbExpenseRemark.Name, Me.tbExpenseRemark.Text)
			Me.tbClaimRemark.Text = Language._Object.getCaption(Me.tbClaimRemark.Name, Me.tbClaimRemark.Text)
			Me.LblDomicileAdd.Text = Language._Object.getCaption(Me.LblDomicileAdd.Name, Me.LblDomicileAdd.Text)
			Me.LnkViewDependants.Text = Language._Object.getCaption(Me.LnkViewDependants.Name, Me.LnkViewDependants.Text)
			Me.btnViewScanAttchment.Text = Language._Object.getCaption(Me.btnViewScanAttchment.Name, Me.btnViewScanAttchment.Text)
			Me.dgcolhExpense.HeaderText = Language._Object.getCaption(Me.dgcolhExpense.Name, Me.dgcolhExpense.HeaderText)
			Me.dgcolhSectorRoute.HeaderText = Language._Object.getCaption(Me.dgcolhSectorRoute.Name, Me.dgcolhSectorRoute.HeaderText)
			Me.dgcolhUoM.HeaderText = Language._Object.getCaption(Me.dgcolhUoM.Name, Me.dgcolhUoM.HeaderText)
			Me.dgcolhQty.HeaderText = Language._Object.getCaption(Me.dgcolhQty.Name, Me.dgcolhQty.HeaderText)
			Me.dgcolhUnitPrice.HeaderText = Language._Object.getCaption(Me.dgcolhUnitPrice.Name, Me.dgcolhUnitPrice.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhExpenseRemark.HeaderText = Language._Object.getCaption(Me.dgcolhExpenseRemark.Name, Me.dgcolhExpenseRemark.HeaderText)
			Me.lblBalanceasondate.Text = Language._Object.getCaption(Me.lblBalanceasondate.Name, Me.lblBalanceasondate.Text)
			Me.LblCostCenter.Text = Language._Object.getCaption(Me.LblCostCenter.Name, Me.LblCostCenter.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.LnkViewAllocation.Text = Language._Object.getCaption(Me.LnkViewAllocation.Name, Me.LnkViewAllocation.Text)
			Me.lnkShowFuelConsumptionReport.Text = Language._Object.getCaption(Me.lnkShowFuelConsumptionReport.Name, Me.lnkShowFuelConsumptionReport.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub


	Private Sub SetMessages()
		Try
            Language.setMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [")
            Language.setMessage("clsclaim_request_master", 4, " ] time(s).")
            Language.setMessage("clsExpCommonMethods", 6, "Quantity")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("frmNewMDI", 77, "Sending Email(s) process is in progress. Please wait for some time.")
			Language.setMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
			Language.setMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
            Language.setMessage(mstrModuleName, 4, "Expense is mandatory information. Please select Expense to continue.")
			Language.setMessage(mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue.")
			Language.setMessage(mstrModuleName, 7, "Please add atleast one expense in order to do futher operation.")
			Language.setMessage(mstrModuleName, 8, "Rejection Remark cannot be blank. Rejection Remark is required information.")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
			Language.setMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set.")
			Language.setMessage(mstrModuleName, 12, "Sorry, you cannot set Amount greater than balance set.")
			Language.setMessage(mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
			Language.setMessage(mstrModuleName, 14, "Leave Form")
			Language.setMessage(mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set.")
			Language.setMessage(mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.")
			Language.setMessage(mstrModuleName, 17, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 18, "Some of the expense(s) had 0 unit price for selected employee.")
            Language.setMessage(mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
			Language.setMessage(mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount.")
			Language.setMessage(mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue.")
			Language.setMessage(mstrModuleName, 22, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
			Language.setMessage(mstrModuleName, 23, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
			Language.setMessage(mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
			Language.setMessage(mstrModuleName, 25, "You have not set your expense remark.")
			Language.setMessage(mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue.")
			Language.setMessage(mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
			Language.setMessage(mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
			Language.setMessage(mstrModuleName, 29, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")
            Language.setMessage(mstrModuleName, 30, "Sorry,you cannot approve for this claim application.Reason : Total expenses claim amount is exceeded the budget amount.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class