﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpensesAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExpensesAddEdit"
    Private mblnCancel As Boolean = True
    Private objExpense As clsExpense_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintExpenseUnkid As Integer = -1
    Private mblnEditExpenseCategory As Boolean = False
    Private mblnEditExpenseType As Boolean = False
    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
    Private dvUser As DataView = Nothing
    'Hemant (22 Dec 2023) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal blnEditExpenseCategory As Boolean = False, Optional ByVal blnEditExpenseType As Boolean = False) As Boolean
        Try
            mintExpenseUnkid = intUnkId
            menAction = eAction

            'Shani [ 17 SEP 2014 ] -- START
            'Change -  From C&RTesing1.Docx File Point No:-6 GIVEN BY Andrew Sir  
            mblnEditExpenseCategory = blnEditExpenseCategory
            mblnEditExpenseType = blnEditExpenseType
            'Shani [ 17 SEP 2014 ] -- END


            Me.ShowDialog()

            intUnkId = mintExpenseUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorComp
            cboExCategory.BackColor = GUI.ColorComp
            cboUoM.BackColor = GUI.ColorComp
            cboHeadType.BackColor = GUI.ColorComp
            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            cboDefaultCostCenter.BackColor = GUI.ColorComp
            'Hemant (06 Jul 2020) -- End

            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                cboJournal.BackColor = GUI.ColorComp
                cboPaymentMode.BackColor = GUI.ColorComp
            Else
                cboJournal.BackColor = GUI.ColorOptional
                cboPaymentMode.BackColor = GUI.ColorOptional
            End If
            'Pinkal (23-Dec-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboExCategory.SelectedValue = objExpense._Expensetypeid


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_IMPREST AndAlso ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.ExpenseWise_Retirement Then
                cboImprestCode.SelectedValue = objExpense._LinkedExpenseId
            Else
            txtCode.Text = objExpense._Code
            txtName.Text = objExpense._Name
            End If
            'Pinkal (11-Sep-2019) -- End


            txtDescription.Text = objExpense._Description
            chkIsAccrued.Checked = objExpense._Isaccrue
            chkLeaveEncashment.Checked = objExpense._IsLeaveEncashment
            cboLeaveType.SelectedValue = objExpense._Leavetypeunkid
            cboUoM.SelectedValue = objExpense._Uomunkid
            chkConsiderPayroll.Checked = objExpense._IsConsiderForPayroll
            cboHeadType.SelectedValue = objExpense._tranheadtypeunkid

            If objExpense._IsLeaveEncashment = True Then
                cboUoM.Enabled = False
            End If
            chkSecRouteMandatory.Checked = objExpense._IsSecRouteMandatory

            chkDoNotShowExpInCR.Checked = objExpense._ExpenseInvisibleInCR
            ChkAttachDocumentMandatory.Checked = objExpense._IsAttachDocMandetory

            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
            If objExpense._AccrueSetting = clsExpense_Master.enExpAccrueSetting.None Then
                rdIssueQtyBalAsOnDate.Checked = False
                rdIssueQtyTotalBal.Checked = False
            ElseIf objExpense._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                rdIssueQtyTotalBal.Checked = True
            ElseIf objExpense._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                rdIssueQtyBalAsOnDate.Checked = True
            End If
            'Pinkal (07-Jul-2018) -- End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            chkConsiderDependants.Checked = objExpense._IsConsiderDependants
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            chkHRExpense.Checked = objExpense._IsHRExpense
            chkBudgetMandatory.Checked = objExpense._IsBudgetMandatory
            If gbP2PExpenseSetting.Visible Then
                gbP2PExpenseSetting.Enabled = Not chkLeaveEncashment.Checked
            End If

            Select Case objExpense._ExpenditureTypeId
                Case enP2PExpenditureType.Opex
                    rdOpex.Checked = True
                Case enP2PExpenditureType.Capex
                    rdCapex.Checked = True
            End Select

            cboGLCode.SelectedValue = CInt(objExpense._GLCodeId)

            If menAction = enAction.EDIT_ONE Then
                Dim mblnFlag As Boolean = objExpense.isUsed(mintExpenseUnkid, "cmclaim_request_tran")  'FOR CHECKING WHETHER P2P SETTING APPLIED IS USED IN CLAIM REQUEST TRAN OR NOT.IF YES THEN DISABLED ALL THE P2P SETTING IN EXPENSE MASTER.

                'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                'gbP2PExpenseSetting.Enabled = Not mblnFlag
                chkBudgetMandatory.Enabled = Not mblnFlag
                chkHRExpense.Enabled = Not mblnFlag
                'Pinkal (23-Dec-2023) -- End

                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'REMOVE AS PER MARTHA REQUEST BECAUSE NMB MOST OF THE EXPENSES ARE NOT MAPPED WITH GLCODE SO THEY WANT TO DISABLE IT.
                'cboGLCode.Enabled = Not mblnFlag
                'objbtnSearchGLCode.Enabled = Not mblnFlag
                'objbtnAddGLCode.Enabled = Not mblnFlag
                'Pinkal (07-Mar-2019) -- End
            End If

            'Pinkal (20-Nov-2018) -- End


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            chkShowOnESS.Checked = objExpense._ShowOnESS
            'Pinkal (26-Dec-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            chkSecRouteMandatory_CheckedChanged(chkSecRouteMandatory, New EventArgs())
            chkMakeUnitPriceEditable.Checked = objExpense._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            chkIsImprest.Checked = objExpense._IsImprest
            'If menAction = enAction.EDIT_ONE Then chkIsImprest.Enabled = False


            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_IMPREST Then
                If ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.General_Retirement Then
                    txtCode.Visible = True
                    txtName.Visible = True
                    objbtnOtherLanguage.Visible = True
                    cboImprestCode.Visible = False
                    cboImprestName.Visible = False
                    objSearchImprestCode.Visible = False
                    objSearchImprestName.Visible = False
                ElseIf ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.ExpenseWise_Retirement Then
                    cboImprestCode.Visible = True
                    cboImprestName.Visible = True
                    objSearchImprestCode.Visible = True
                    objSearchImprestName.Visible = True
                    txtCode.Visible = False
                    txtName.Visible = False
                    objbtnOtherLanguage.Visible = False
                End If
            End If
            'Pinkal (11-Sep-2019) -- End

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            nudMaxExpenseQty.Value = objExpense._Expense_MaxQuantity
            'Pinkal (10-Jun-2020) -- End

            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            cboDefaultCostCenter.SelectedValue = objExpense._CostCenterUnkId
            'Hemant (06 Jul 2020) -- End

            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            chkNotAllowBackDate.Checked = objExpense._DoNotAllowToApplyForBackDate
            'Pinkal (16-Sep-2020) -- End
            'Hemant (22 Dec 2023) -- Start
            'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
            If objExpense._NotifyApprovedUserIds.Trim.Length > 0 Then
                chkNtfAfterFinalApproval.Checked = True
                Dim drUser = CType(dgFinalApprovedUser.DataSource, DataTable).Select("userunkid in (" & objExpense._NotifyApprovedUserIds.Trim & ")")
                If drUser IsNot Nothing AndAlso drUser.Count > 0 Then
                    For i As Integer = 0 To drUser.Length - 1
                        drUser(i)("ischeck") = True
                        drUser(i).AcceptChanges()
                    Next
                End If
                SetCheckBoxValue()
            Else
                chkNtfAfterFinalApproval_CheckedChanged(chkNtfAfterFinalApproval, New EventArgs())
            End If
            'Hemant (22 Dec 2023) -- End

 'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            cboJournal.SelectedValue = objExpense._Journalunkid.ToString()
            cboPaymentMode.SelectedValue = objExpense._Paymentmodeunkid.ToString()
            chkDebit.Checked = objExpense._P2PDebit
            chkCredit.Checked = objExpense._P2PCredit
            'If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
            '    chkDebit.Enabled = True
            '    chkCredit.Enabled = True
            'End If
            'Pinkal (23-Dec-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.


            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_IMPREST AndAlso ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.ExpenseWise_Retirement Then
                Dim dr As DataRowView = CType(cboImprestName.SelectedItem, DataRowView)
                If dr IsNot Nothing Then
                    objExpense._Code = dr("code").ToString() & " - " & Language.getMessage(mstrModuleName, 9, "Imprest")
                    objExpense._Name = dr("Name").ToString() & " - " & Language.getMessage(mstrModuleName, 9, "Imprest")
                End If
            Else
            objExpense._Code = txtCode.Text
            objExpense._Name = txtName.Text
            End If
            'Pinkal (11-Sep-2019) -- End


            objExpense._Description = txtDescription.Text
            objExpense._Isaccrue = chkIsAccrued.Checked
            objExpense._IsLeaveEncashment = chkLeaveEncashment.Checked
            objExpense._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            objExpense._Expensetypeid = CInt(cboExCategory.SelectedValue)
            objExpense._Uomunkid = CInt(cboUoM.SelectedValue)
            objExpense._IsConsiderForPayroll = chkConsiderPayroll.Checked
            objExpense._tranheadtypeunkid = CInt(cboHeadType.SelectedValue)

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            objExpense._IsSecRouteMandatory = chkSecRouteMandatory.Checked
            'Pinkal (22-Jun-2015) -- End

            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            objExpense._ExpenseInvisibleInCR = chkDoNotShowExpInCR.Checked
            'Pinkal (22-Mar-2016) -- End

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            objExpense._IsAttachDocMandetory = ChkAttachDocumentMandatory.Checked
            'Shani (20-Aug-2016) -- End


            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]

            If pnlBalanceSetting.Enabled Then
                If rdIssueQtyTotalBal.Checked Then
                    objExpense._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance
                ElseIf rdIssueQtyBalAsOnDate.Checked Then
                    objExpense._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date
                End If
            Else
                objExpense._AccrueSetting = clsExpense_Master.enExpAccrueSetting.None
            End If

            'Pinkal (07-Jul-2018) -- End

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            objExpense._IsConsiderDependants = chkConsiderDependants.Checked
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            objExpense._IsBudgetMandatory = chkBudgetMandatory.Checked
            objExpense._IsHRExpense = chkHRExpense.Checked
            If gbP2PExpenseSetting.Visible Then
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'objExpense._ExpenditureTypeId = CInt(IIf(rdOpex.Checked, enP2PExpenditureType.Opex, enP2PExpenditureType.Capex))
                If rdOpex.Checked Then
                    objExpense._ExpenditureTypeId = enP2PExpenditureType.Opex
                ElseIf rdCapex.Checked Then
                    objExpense._ExpenditureTypeId = enP2PExpenditureType.Capex
                Else
                    objExpense._ExpenditureTypeId = enP2PExpenditureType.None
                End If
                'Pinkal (04-Feb-2019) -- End
            Else
                objExpense._ExpenditureTypeId = enP2PExpenditureType.None
            End If
            objExpense._GLCodeId = CInt(cboGLCode.SelectedValue)
            'Pinkal (20-Nov-2018) -- End


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            objExpense._ShowOnESS = chkShowOnESS.Checked
            'Pinkal (26-Dec-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            objExpense._IsUnitPriceEditable = chkMakeUnitPriceEditable.Checked
            'Pinkal (25-May-2019) -- End


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objExpense._IsImprest = chkIsImprest.Checked
            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_IMPREST AndAlso ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.ExpenseWise_Retirement Then
                objExpense._LinkedExpenseId = CInt(cboImprestName.SelectedValue)
            Else
                objExpense._LinkedExpenseId = 0
            End If
            'Pinkal (11-Sep-2019) -- End


            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            objExpense._Expense_MaxQuantity = CInt(nudMaxExpenseQty.Value)
            'Pinkal (10-Jun-2020) -- End

            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            objExpense._CostCenterUnkId = CInt(cboDefaultCostCenter.SelectedValue)
            'Hemant (06 Jul 2020) -- End


            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            objExpense._DoNotAllowToApplyForBackDate = chkNotAllowBackDate.Checked
            'Pinkal (16-Sep-2020) -- End
            'Hemant (22 Dec 2023) -- Start
            'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
            Dim lstNotifyApprovedUserIds As String = ""
            lstNotifyApprovedUserIds = String.Join(",", dgFinalApprovedUser.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhFinalApprovedUserCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhFinalApprovedUserId.Index).Value.ToString()).ToArray())
            objExpense._NotifyApprovedUserIds = lstNotifyApprovedUserIds
            'Hemant (22 Dec 2023) -- End

'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            objExpense._Journalunkid = CInt(cboJournal.SelectedValue)
            objExpense._Paymentmodeunkid = CInt(cboPaymentMode.SelectedValue)
            objExpense._P2PDebit = chkDebit.Checked
            objExpense._P2PCredit = chkCredit.Checked
            'Pinkal (23-Dec-2023) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            'If ConfigParameter._Object._OpexRequestCCP2PServiceURL.Trim.Length > 0 OrElse ConfigParameter._Object._CapexRequestCCP2PServiceURL.Trim.Length > 0 Then
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1399) NMB - Post approved claims amounts to P2P.
                gbP2PExpenseSetting.Visible = True
                rdOpex.Visible = False
                rdCapex.Visible = False
                LblExpenditureType.Visible = False
                'Pinkal (16-Oct-2023) -- End
            Else
                gbP2PExpenseSetting.Visible = False
                chkHRExpense.Checked = False
                chkBudgetMandatory.Checked = False
            End If
            'Pinkal (20-Nov-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End
            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUoM
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            Dim objLeaveType As New clsleavetype_master
            dsList = objLeaveType.GetList("LeaveType", True, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "ispaid=1 AND isaccrueamount = 1", "", DataViewRowState.CurrentRows).ToTable
            Dim drRow As DataRow = dtTable.NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavename") = Language.getMessage(mstrModuleName, 4, "Select")
            dtTable.Rows.InsertAt(drRow, 0)

            With cboLeaveType
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "leavename"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            Dim objMaster As New clsMasterData
            dsList = objMaster.getComboListForHeadType("HeadType")
            Dim dTab As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions & ")", "Id", DataViewRowState.CurrentRows).ToTable
            With cboHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dTab
                If .Items.Count > 0 Then .SelectedValue = 0
            End With


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            If gbP2PExpenseSetting.Visible Then
                Dim objGLCode As New clsAccount_master
                Dim dsGLCodeList As DataSet = objGLCode.getComboList("List", True, 0)
                cboGLCode.DisplayMember = "accname"
                cboGLCode.ValueMember = "accountunkid"
                cboGLCode.DataSource = dsGLCodeList.Tables(0).Copy
                dsGLCodeList.Clear()
                dsGLCodeList = Nothing
                objGLCode = Nothing
            End If
            'Pinkal (20-Nov-2018) -- End


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            If ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.ExpenseWise_Retirement Then
                dsList = objExpense.getImprestExpesneComboList(enExpenseType.EXP_NONE, True, "List", 0, False, 0, "cmexpense_master.isimprest =1", "")
                With cboImprestCode
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables(0)
                    If .Items.Count > 0 Then .SelectedValue = 0
                End With

                With cboImprestName
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables(0)
                    If .Items.Count > 0 Then .SelectedValue = 0
                End With
            End If
            'Pinkal (11-Sep-2019) -- End

            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            dsList = Nothing
            Dim objCostcenter As New clscostcenter_master
            dsList = objCostcenter.getComboList("CostCenter", True)
            cboDefaultCostCenter.ValueMember = "costcenterunkid"
            cboDefaultCostCenter.DisplayMember = "costcentername"
            cboDefaultCostCenter.DataSource = dsList.Tables("CostCenter")
            'Hemant (06 Jul 2020) -- End

            'Hemant (22 Dec 2023) -- Start
            'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
            Dim objUser As New clsUserAddEdit
            dsList = objUser.GetCompanyBasedUserList("List", ConfigParameter._Object._Companyunkid)
            If dsList.Tables(0).Columns.Contains("ischeck") = False Then
                Dim dcColumn As New DataColumn("ischeck")
                dcColumn.DataType = Type.GetType("System.Boolean")
                dcColumn.DefaultValue = False
                dsList.Tables(0).Columns.Add(dcColumn)
            End If

            dvUser = dsList.Tables(0).DefaultView

            dgFinalApprovedUser.AutoGenerateColumns = False
            objdgcolhFinalApprovedUserCheck.DataPropertyName = "ischeck"
            objdgcolhFinalApprovedUserId.DataPropertyName = "userunkid"
            dgcolhFinalApprovedUser.DataPropertyName = "username"
            dgFinalApprovedUser.DataSource = dsList.Tables(0)
            objUser = Nothing
            'Hemant (22 Dec 2023) -- End


'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            Dim objCommon As New clsCommon_Master
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.CLAIM_JOURNAL, True, "List")
            With cboJournal
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.CLAIM_PAYMENTMODE, True, "List")
            With cboPaymentMode
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            objCommon = Nothing
            'Pinkal (23-Dec-2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            If txtCode.Visible AndAlso txtCode.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Code is mandatory information. Please provide Expense Code."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Return False
            End If


            If txtName.Visible AndAlso txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense Name is mandatory information. Please provide Expense Name."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If
            'Pinkal (11-Sep-2019) -- End


            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            '            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
            'valid:
            '                If CInt(cboUoM.SelectedValue) <= 0 Then
            '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "UoM is mandatory information. Please select UoM."), enMsgBoxStyle.Information)
            '                    cboUoM.Focus()
            '                    Return False
            '                End If
            '                'Pinkal (22-Jun-2015) -- Start
            '                'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            '                'ElseIf (CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS) AndAlso chkIsAccrued.Checked = True Then
            '            ElseIf (CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS) Then
            '                'Pinkal (22-Jun-2015) -- End
            '                GoTo valid
            '            End If
            Select Case CInt(cboExCategory.SelectedValue)
                Case enExpenseType.EXP_IMPREST, enExpenseType.EXP_TRAINING, enExpenseType.EXP_NONE
                Case Else
            If CInt(cboUoM.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "UoM is mandatory information. Please select UoM."), enMsgBoxStyle.Information)
                cboUoM.Focus()
                Return False
            End If
            End Select
           

            'S.SANDEEP |10-MAR-2022| -- END
            

            If CInt(cboExCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Expense Category is compulsory information.Please Select Expense Category."), enMsgBoxStyle.Information)
                cboExCategory.Focus()
                Return False
            End If

            If chkLeaveEncashment.Checked Then
                If CInt(cboLeaveType.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                    cboLeaveType.Focus()
                    Return False
                End If
            End If

            If chkConsiderPayroll.Checked AndAlso CInt(cboHeadType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Transaction Head Type is compulsory information.Please Select Transaction Head Type."), enMsgBoxStyle.Information)
                cboHeadType.Focus()
                Return False
            End If


            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
            If ((chkIsAccrued.Checked OrElse chkLeaveEncashment.Checked) AndAlso CInt(cboUoM.SelectedValue) = enExpUoM.UOM_QTY) AndAlso (rdIssueQtyTotalBal.Checked = False AndAlso rdIssueQtyBalAsOnDate.Checked = False) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Accrue setting is compulsory information.Please set Accrue Setting."), enMsgBoxStyle.Information)
                rdIssueQtyTotalBal.Select()
                Return False
            End If
            'Pinkal (07-Jul-2018) -- End


            'S.SANDEEP |29-JUN-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-390_V2
            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE Then
                If chkIsAccrued.Checked Then
                    If objExpense.isExist("", "", CInt(cboExCategory.SelectedValue), mintExpenseUnkid, 0, "isaccrue = 1") = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you can only define one expense for the selected category which can have") & " " & chkIsAccrued.Text & " " & _
                                        Language.getMessage(mstrModuleName, 11, "setting enabled."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End If

            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                Dim iList As New DataSet
                iList = objExpense.GetList("List", True, "expensetypeid = " & enExpenseType.EXP_REBATE_PRIVILEGE & " AND isaccrue = 1")
                If iList IsNot Nothing AndAlso iList.Tables.Count > 0 AndAlso iList.Tables(0).Rows.Count > 1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot define expense for the selected category, As you have defined multiple expense under rebate privilege category with") & " " & chkIsAccrued.Text & " " & _
                                    Language.getMessage(mstrModuleName, 11, "setting enabled."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'S.SANDEEP |29-JUN-2022| -- END

            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                If CInt(cboJournal.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Journal is compulsory information.Please select journal."), enMsgBoxStyle.Information)
                    cboJournal.Focus()
                    Return False
                ElseIf CInt(cboPaymentMode.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Payment mode is compulsory information.Please select payment mode."), enMsgBoxStyle.Information)
                    cboPaymentMode.Focus()
                    Return False
                ElseIf chkDebit.Checked = False AndAlso chkCredit.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Payment Setting is compulsory information.Please set atleast one payment setting."), enMsgBoxStyle.Information)
                    chkDebit.Focus()
                    Return False
                End If
            End If
            'Pinkal (23-Dec-2023) -- End
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isValid_Data", mstrModuleName)
        Finally
        End Try
    End Function


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    Private Sub ControlVisibility(ByVal blnvisible As Boolean)
        Try
            txtCode.Visible = blnvisible
            txtName.Visible = blnvisible
            objbtnOtherLanguage.Visible = blnvisible
            cboImprestCode.Visible = Not blnvisible
            cboImprestName.Visible = Not blnvisible
            objSearchImprestCode.Visible = Not blnvisible
            objSearchImprestName.Visible = Not blnvisible

 'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                cboJournal.Enabled = True
                cboPaymentMode.Enabled = True
                objbtnSearchJournal.Enabled = True
                objbtnSearchPaymentMode.Enabled = True
                chkDebit.Enabled = True
                chkCredit.Enabled = True
            Else
                cboJournal.Enabled = False
                cboPaymentMode.Enabled = False
                objbtnSearchJournal.Enabled = False
                objbtnSearchPaymentMode.Enabled = False
                chkDebit.Enabled = False
                chkCredit.Enabled = False
            End If
            'Pinkal (23-Dec-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Pinkal (11-Sep-2019) -- End
    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvUser.Table.Select("ischeck = true")

            RemoveHandler chkAllFinalApprovedUser.CheckedChanged, AddressOf chkAllFinalApprovedUser_CheckedChanged

            If drRow.Length <= 0 Then
                chkAllFinalApprovedUser.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgFinalApprovedUser.Rows.Count Then
                chkAllFinalApprovedUser.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgFinalApprovedUser.Rows.Count Then
                chkAllFinalApprovedUser.CheckState = CheckState.Checked
            End If

            AddHandler chkAllFinalApprovedUser.CheckedChanged, AddressOf chkAllFinalApprovedUser_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 Dec 2023) -- End


#End Region

#Region " Form's Events "

    Private Sub frmExpensesAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExpense = Nothing
    End Sub

    Private Sub frmExpensesAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpensesAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExpensesAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExpense = New clsExpense_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call setColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objExpense._Expenseunkid = mintExpenseUnkid
                cboExCategory.Enabled = False : cboUoM.Enabled = False : cboLeaveType.Enabled = False : objbtnSearchLeaveType.Enabled = False

            End If
            'SHANI (06 JUN 2015) -- Start
            'Enhancement : Changes in C & R module given by Mr.Andrew.
            If objExpense._Expensetypeid = enExpenseType.EXP_MISCELLANEOUS OrElse objExpense._Expensetypeid = enExpenseType.EXP_MEDICAL Then
                If menAction = enAction.EDIT_ONE Then
                    If chkIsAccrued.Checked = False AndAlso chkLeaveEncashment.Checked = False Then
                        cboLeaveType.Enabled = False
                        objbtnSearchLeaveType.Enabled = False
                        cboUoM.Enabled = True
                    ElseIf chkIsAccrued.Checked Then
                        Call chkIsAccrued_CheckedChanged(chkIsAccrued, Nothing)
                    ElseIf chkLeaveEncashment.Checked Then
                        Call chkLeaveEncashment_CheckedChanged(chkLeaveEncashment, Nothing)
                    End If
                End If
            End If
            'SHANI (06 JUN 2015) -- End
            Call GetValue()


            'Shani [ 26 SEP 2014 ] -- START
            'Changes - Based On Andrew Sir Request for the Following Changes are done : C&mTesting1.docx Point No 2,3
            '1. Allow to Edit Expense Category If it's not used in Any transaction.- for this point we have done Expense category,Isaccrud,leave Encashment Control Enable False
            '2. Allow to Edit Only Name & Code of Expense Category
            If menAction = enAction.EDIT_ONE Then
                If mblnEditExpenseCategory = False AndAlso mblnEditExpenseType = False Then
                    cboExCategory.Enabled = False
                    chkIsAccrued.Enabled = False
                    chkLeaveEncashment.Enabled = False
                    cboUoM.Enabled = False
                    cboLeaveType.Enabled = False
                    objbtnSearchLeaveType.Enabled = False

                    'Pinkal (22-Mar-2016) -- Start
                    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                    chkDoNotShowExpInCR.Enabled = False
                    'Pinkal (22-Mar-2016) -- End

                Else

                    'SHANI (06 JUN 2015) -- Start
                    'Enhancement : Changes in C & R module given by Mr.Andrew.
                    'cboExCategory.Enabled = True
                    'chkIsAccrued.Enabled = True
                    'chkLeaveEncashment.Enabled = True
                    ''cboUoM.Enabled = True
                    'cboLeaveType.Enabled = True
                    'objbtnSearchLeaveType.Enabled = True
                    If CInt(cboExCategory.SelectedValue) <> enExpenseType.EXP_MISCELLANEOUS AndAlso CInt(cboExCategory.SelectedValue) <> enExpenseType.EXP_MEDICAL Then
                    cboExCategory.Enabled = True
                        chkIsAccrued.Enabled = True
                        chkLeaveEncashment.Enabled = True
                    cboLeaveType.Enabled = True
                    objbtnSearchLeaveType.Enabled = True
                End If
                    'SHANI (06 JUN 2015) -- End
                End If
            End If
            'Shani [ 26 SEP 2014 ] -- END

            chkConsiderPayroll_CheckedChanged(New Object, New EventArgs())

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivity_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpense_Master.SetMessages()
            clsExpCommonMethods.SetMessages()
            objfrm._Other_ModuleNames = "clsExpense_Master,clsExpCommonMethods"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If isValid_Data() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objExpense._FormName = mstrModuleName
            objExpense._LoginEmployeeunkid = 0
            objExpense._ClientIP = getIP()
            objExpense._HostName = getHostName()
            objExpense._FromWeb = False
            objExpense._AuditUserId = User._Object._Userunkid
objExpense._CompanyUnkid = Company._Object._Companyunkid
            objExpense._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objExpense.Update()
            Else
                blnFlag = objExpense.Insert()
            End If

            If blnFlag = False And objExpense._Message <> "" Then
                eZeeMsgBox.Show(objExpense._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objExpense = Nothing
                    objExpense = New clsExpense_Master
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintExpenseUnkid = objExpense._Expenseunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtName.Text, objExpense._Name1, objExpense._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim frm As New frmCommonSearch
        Try
            If cboLeaveType.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboLeaveType.ValueMember
                .DisplayMember = cboLeaveType.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboLeaveType.DataSource, DataTable)

                If .DisplayDialog Then
                    cboLeaveType.SelectedValue = .SelectedValue
                    cboLeaveType.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Private Sub objbtnSearchGLCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGLCode.Click
        Dim frm As New frmCommonSearch
        Try
            If cboGLCode.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboGLCode.ValueMember
                .DisplayMember = cboGLCode.DisplayMember
                .CodeMember = "account_code"
                .DataSource = CType(cboGLCode.DataSource, DataTable)

                If .DisplayDialog Then
                    cboGLCode.SelectedValue = .SelectedValue
                    cboGLCode.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGLCode_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGLCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGLCode.Click
        Try
            Dim intGLCodeId As Integer = -1
            Dim frm As New frmAccount_AddEdit
            frm.displayDialog(intGLCodeId, enAction.ADD_ONE)
            If intGLCodeId > -1 Then
                Dim objAccount As New clsAccount_master
                Dim dsList As New DataSet
                dsList = objAccount.getComboList("Account", True, 0)
                With cboGLCode
                    .ValueMember = "accountunkid"
                    .DisplayMember = "accname"
                    .DataSource = dsList.Tables("Account")
                    .SelectedValue = intGLCodeId
                End With
                objAccount = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGLCode_Click", mstrModuleName)

        End Try
    End Sub

    'Pinkal (20-Nov-2018) -- End


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    Private Sub objSearchImprestCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchImprestCode.Click, objSearchImprestName.Click
        Dim frm As New frmCommonSearch
        Dim cbo As New ComboBox
        Try

            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper()
                Case "OBJSEARCHIMPRESTCODE"
                    cbo = cboImprestCode
                Case "OBJSEARCHIMPRESTNAME"
                    cbo = cboImprestName
            End Select


            If cbo.DataSource IsNot Nothing Then
                With frm
                    .DisplayMember = cbo.DisplayMember
                    .ValueMember = cbo.ValueMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "Code"
                End With

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Focus()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchImprestCode_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (11-Sep-2019) -- End

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Private Sub objbtnSearchJournal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJournal.Click
        Dim frm As New frmCommonSearch
        Try
            If cboJournal.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboJournal.ValueMember
                .DisplayMember = cboJournal.DisplayMember
                .DataSource = CType(cboJournal.DataSource, DataTable)

                If .DisplayDialog Then
                    cboJournal.SelectedValue = .SelectedValue
                    cboJournal.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJournal_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchPaymentMode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPaymentMode.Click
        Dim frm As New frmCommonSearch
        Try
            If cboPaymentMode.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPaymentMode.ValueMember
                .DisplayMember = cboPaymentMode.DisplayMember
                .DataSource = CType(cboPaymentMode.DataSource, DataTable)

                If .DisplayDialog Then
                    cboPaymentMode.SelectedValue = .SelectedValue
                    cboPaymentMode.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPaymentMode_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (23-Dec-2023) -- End

#End Region

#Region " Combobox Events "

    Private Sub cboExCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try

            Select Case CInt(cboExCategory.SelectedValue)
                Case enExpenseType.EXP_TRAINING
                    chkIsAccrued.Checked = False
                    chkLeaveEncashment.Checked = False
                    chkIsAccrued.Enabled = False
                    chkLeaveEncashment.Enabled = False
                    cboLeaveType.SelectedValue = 0
                    cboLeaveType.Enabled = False


                    'Pinkal (11-Sep-2019) -- Start
                    'Enhancement NMB - Working On Claim Retirement for NMB.
                    chkSecRouteMandatory.Checked = False
                    chkSecRouteMandatory.Enabled = True
                    chkIsImprest.Checked = False
                    chkIsImprest.Enabled = True
                    ControlVisibility(True)
                    'Pinkal (11-Sep-2019) -- End


                Case enExpenseType.EXP_MISCELLANEOUS, enExpenseType.EXP_MEDICAL
                    chkIsAccrued.Checked = False
                    chkIsAccrued.Enabled = True
                    chkLeaveEncashment.Checked = False
                    chkLeaveEncashment.Enabled = False
                    cboLeaveType.SelectedValue = 0
                    cboLeaveType.Enabled = False
                    objbtnSearchLeaveType.Enabled = False
                    cboUoM.SelectedValue = 0

                    'S.SANDEEP |10-MAR-2022| -- START
                    'ISSUE/ENHANCEMENT : OLD-580
                    chkConsiderPayroll.Enabled = True
                    'S.SANDEEP |10-MAR-2022| -- END

                    'Pinkal (11-Sep-2019) -- Start
                    'Enhancement NMB - Working On Claim Retirement for NMB.
                    chkSecRouteMandatory.Checked = False
                    chkSecRouteMandatory.Enabled = True
                    chkIsImprest.Checked = False
                    chkIsImprest.Enabled = True
                    ControlVisibility(True)

                Case enExpenseType.EXP_IMPREST

                    If ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.General_Retirement Then
                        ControlVisibility(True)
                        chkIsImprest.Checked = True
                        chkIsImprest.Enabled = False
                    ElseIf ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.ExpenseWise_Retirement Then
                        ControlVisibility(False)
                        cboImprestCode.SelectedValue = 0
                        cboImprestName.SelectedValue = 0
                        'Pinkal (24-Oct-2019) -- Start
                        'Enhancement NMB - Working On OT Enhancement for NMB.
                        chkIsImprest.Checked = False
                        chkIsImprest.Enabled = True
                        'Pinkal (24-Oct-2019) -- End
                    End If
                    chkLeaveEncashment.Checked = False
                    chkLeaveEncashment.Enabled = False


                    'Pinkal (10-Feb-2021) -- Start
                    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                    chkSecRouteMandatory.Checked = False
                    If ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.General_Retirement Then
                        chkSecRouteMandatory.Enabled = True
                    ElseIf ConfigParameter._Object._ClaimRetirementTypeId = enClaimRetirementType.ExpenseWise_Retirement Then
                    chkSecRouteMandatory.Enabled = False
                    End If
                    cboUoM.SelectedValue = enExpUoM.UOM_AMOUNT
                    cboUoM.Enabled = False
                    'Pinkal (10-Feb-2021) -- End

                    'Pinkal (11-Sep-2019) -- End

                    'S.SANDEEP |10-MAR-2022| -- START
                    'ISSUE/ENHANCEMENT : OLD-580
                    chkConsiderPayroll.Enabled = True
                    'S.SANDEEP |10-MAR-2022| -- END

                    'S.SANDEEP |10-MAR-2022| -- START
                    'ISSUE/ENHANCEMENT : OLD-580
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    chkConsiderPayroll.Checked = False
                    chkConsiderPayroll.Enabled = False
                    'S.SANDEEP |10-MAR-2022| -- END

                Case Else

                    If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                        chkIsAccrued.Checked = True
                    Else
                        chkIsAccrued.Checked = False
                    End If

                    chkIsAccrued.Visible = True
                    chkLeaveEncashment.Visible = True
                    chkIsAccrued.Enabled = True
                    chkLeaveEncashment.Enabled = True
                    cboLeaveType.SelectedValue = 0
                    If chkIsAccrued.Checked = False AndAlso chkLeaveEncashment.Checked = False Then
                        cboLeaveType.Enabled = False
                        objbtnSearchLeaveType.Enabled = False
                    Else
                        cboLeaveType.Enabled = True
                        objbtnSearchLeaveType.Enabled = True
                    End If


                    'S.SANDEEP |10-MAR-2022| -- START
                    'ISSUE/ENHANCEMENT : OLD-580
                    chkConsiderPayroll.Enabled = True
                    'S.SANDEEP |10-MAR-2022| -- END

                    'Pinkal (11-Sep-2019) -- Start
                    'Enhancement NMB - Working On Claim Retirement for NMB.
                    chkSecRouteMandatory.Checked = False
                    chkSecRouteMandatory.Enabled = True
                    chkIsImprest.Checked = False
                    chkIsImprest.Enabled = True
                    ControlVisibility(True)
                    'Pinkal (11-Sep-2019) -- End


            End Select

            chkLeaveEncashment_CheckedChanged(chkLeaveEncashment, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboUoM_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUoM.SelectedIndexChanged
        Try
            If (chkIsAccrued.Checked OrElse chkLeaveEncashment.Checked) AndAlso CInt(cboUoM.SelectedValue) = enExpUoM.UOM_QTY Then
                pnlBalanceSetting.Enabled = True
                If (menAction = enAction.ADD_CONTINUE OrElse menAction = enAction.ADD_ONE) Then
                    rdIssueQtyTotalBal.Checked = True
                End If
            Else
                pnlBalanceSetting.Enabled = False
                rdIssueQtyTotalBal.Checked = False
                rdIssueQtyBalAsOnDate.Checked = False
            End If


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            If gbP2PExpenseSetting.Visible Then
                If CInt(cboUoM.SelectedValue) <= 0 OrElse CInt(cboUoM.SelectedValue) = enExpUoM.UOM_QTY Then
                    chkBudgetMandatory.Enabled = False
                    chkBudgetMandatory.Checked = False
                    chkHRExpense.Enabled = False
                ElseIf CInt(cboUoM.SelectedValue) = enExpUoM.UOM_AMOUNT Then
                    chkBudgetMandatory.Enabled = True
                    chkHRExpense.Enabled = True
                End If
            End If

            If CInt(cboUoM.SelectedValue) = enExpUoM.UOM_QTY OrElse CInt(cboUoM.SelectedValue) <= 0 Then
                LblGLCode.Enabled = False
                cboGLCode.Enabled = False
                cboGLCode.SelectedValue = 0
                objbtnSearchGLCode.Enabled = False
                objbtnAddGLCode.Enabled = False
                rdCapex.Enabled = False
                rdCapex.Checked = False
                rdOpex.Enabled = False
                rdOpex.Checked = False

            ElseIf CInt(cboUoM.SelectedValue) = enExpUoM.UOM_AMOUNT Then
                LblGLCode.Enabled = True
                cboGLCode.Enabled = True
                objbtnSearchGLCode.Enabled = True
                objbtnAddGLCode.Enabled = True
                rdCapex.Enabled = True
                rdOpex.Enabled = True
                If menAction <> enAction.EDIT_ONE Then
                    rdOpex.Checked = True
                ElseIf menAction = enAction.EDIT_ONE Then
                    If rdOpex.Checked = False AndAlso rdCapex.Checked = False Then rdOpex.Checked = True
                End If
            End If

            'Pinkal (20-Nov-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUoM_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboImprestCode_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboImprestCode.SelectedValueChanged, cboImprestName.SelectedValueChanged
        Try
            If CInt(cboImprestCode.SelectedValue) > 0 Then
                Dim objExp As New clsExpense_Master
                objExp._Expenseunkid = CInt(cboImprestCode.SelectedValue)
                cboLeaveType.SelectedValue = objExp._Leavetypeunkid
                cboLeaveType.Enabled = False
                objbtnSearchLeaveType.Enabled = False
                cboUoM.SelectedIndex = objExp._Uomunkid
                cboUoM.Enabled = False
                If objExp._AccrueSetting = clsExpense_Master.enExpAccrueSetting.None Then
                    rdIssueQtyBalAsOnDate.Checked = False
                    rdIssueQtyTotalBal.Checked = False
                ElseIf objExp._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                    rdIssueQtyTotalBal.Checked = True
                ElseIf objExp._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                    rdIssueQtyBalAsOnDate.Checked = True
                End If
                pnlBalanceSetting.Enabled = False
                chkLeaveEncashment.Checked = False
                chkIsAccrued.Checked = False
                chkIsAccrued.Enabled = False
                chkLeaveEncashment.Enabled = False
                cboHeadType.Enabled = True
                cboGLCode.Enabled = True
                objbtnAddGLCode.Enabled = True
                objbtnSearchGLCode.Enabled = True
                chkIsImprest.Checked = True
                chkIsImprest.Enabled = False
                objExp = Nothing
            Else
                cboLeaveType.SelectedValue = 0
                cboLeaveType.Enabled = True
                objbtnSearchLeaveType.Enabled = True
                cboUoM.SelectedValue = 0
                cboUoM.Enabled = True
                rdIssueQtyBalAsOnDate.Checked = False
                rdIssueQtyTotalBal.Checked = False
                pnlBalanceSetting.Enabled = True
                chkIsAccrued.Enabled = True
                chkLeaveEncashment.Enabled = False
                cboGLCode.Enabled = False
                objbtnAddGLCode.Enabled = False
                objbtnSearchGLCode.Enabled = False
                chkIsImprest.Checked = False
                chkIsImprest.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboImprestCode_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Radiobutton Event"

    Private Sub rdIsAccrued_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If CType(sender, RadioButton).Name.ToString.ToUpper = "chkIsAccrued" Then

                'SHANI (11 APR 2015)-START
                'Enhancement - 
                'cboLeaveType.Enabled = False
                'cboLeaveType.SelectedValue = 0
                'SHANI (11 APR 2015)--END
                objbtnSearchLeaveType.Enabled = False
                cboUoM.Enabled = True
            ElseIf CType(sender, RadioButton).Name.ToString.ToUpper = "RDLEAVEENCASHMENT" Then
                If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    cboLeaveType.Enabled = True
                    cboLeaveType.SelectedValue = 0
                    objbtnSearchLeaveType.Enabled = True
                    cboUoM.SelectedValue = enExpUoM.UOM_QTY
                    cboUoM.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdIsAccrued_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub chkConsiderPayroll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConsiderPayroll.CheckedChanged
        Try
            If chkConsiderPayroll.Checked Then
                cboHeadType.Enabled = True
            Else
                cboHeadType.SelectedIndex = 0
                cboHeadType.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkConsiderPayroll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkLeaveEncashment_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLeaveEncashment.CheckedChanged
        Try
            RemoveHandler chkIsAccrued.CheckedChanged, AddressOf chkIsAccrued_CheckedChanged
            chkIsAccrued.Checked = False

            If chkLeaveEncashment.Checked = False Then
                chkDoNotShowExpInCR.Checked = False
                chkDoNotShowExpInCR.Enabled = True
            Else
                chkDoNotShowExpInCR.Checked = False
                chkDoNotShowExpInCR.Enabled = False
            End If

            If CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                If chkIsAccrued.Checked = False AndAlso chkLeaveEncashment.Checked = False Then
                    cboLeaveType.Enabled = False
                    objbtnSearchLeaveType.Enabled = False
                    cboUoM.Enabled = True
                    cboUoM.SelectedValue = 0
                Else
                    cboLeaveType.Enabled = True
                    objbtnSearchLeaveType.Enabled = True
                    cboUoM.Enabled = False
                    cboUoM.SelectedValue = enExpUoM.UOM_QTY
                End If

            ElseIf CInt(cboExCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                If chkIsAccrued.Checked = False AndAlso chkLeaveEncashment.Checked = False Then
                    cboUoM.Enabled = True
                    cboUoM.SelectedValue = 0
                Else
                    cboUoM.Enabled = False
                    cboUoM.SelectedValue = enExpUoM.UOM_QTY
                End If
                chkSecRouteMandatory.Checked = False
                chkSecRouteMandatory.Enabled = Not chkLeaveEncashment.Checked
            End If
            AddHandler chkIsAccrued.CheckedChanged, AddressOf chkIsAccrued_CheckedChanged

            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
            If chkLeaveEncashment.Checked AndAlso CInt(cboUoM.SelectedValue) = enExpUoM.UOM_QTY Then
                pnlBalanceSetting.Enabled = True
            Else
                pnlBalanceSetting.Enabled = False
                rdIssueQtyTotalBal.Checked = False
                rdIssueQtyBalAsOnDate.Checked = False
            End If
            'Pinkal (07-Jul-2018) -- End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            chkConsiderDependants.Enabled = Not chkLeaveEncashment.Checked
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            chkConsiderDependants.Checked = False
            If gbP2PExpenseSetting.Visible Then
                gbP2PExpenseSetting.Enabled = Not chkLeaveEncashment.Checked
            End If
            'Pinkal (20-Nov-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkLeaveEncashment_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkIsAccrued_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIsAccrued.CheckedChanged
        Try
            RemoveHandler chkLeaveEncashment.CheckedChanged, AddressOf chkLeaveEncashment_CheckedChanged
            chkLeaveEncashment.Checked = False
            AddHandler chkLeaveEncashment.CheckedChanged, AddressOf chkLeaveEncashment_CheckedChanged

            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
            If chkIsAccrued.Checked AndAlso CInt(cboUoM.SelectedValue) = enExpUoM.UOM_QTY Then
                pnlBalanceSetting.Enabled = True
            Else
                pnlBalanceSetting.Enabled = False
                rdIssueQtyTotalBal.Checked = False
                rdIssueQtyBalAsOnDate.Checked = False
            End If
            'Pinkal (07-Jul-2018) -- End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            chkConsiderDependants.Enabled = Not chkLeaveEncashment.Checked
            'Pinkal (25-Oct-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIsAccrued_CheckedChanged", mstrModuleName)
        End Try

    End Sub


    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.

    Private Sub chkSecRouteMandatory_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSecRouteMandatory.CheckedChanged
        Try
            chkMakeUnitPriceEditable.Enabled = chkSecRouteMandatory.Checked
            If chkSecRouteMandatory.Checked = False Then
                chkMakeUnitPriceEditable.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSecRouteMandatory_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (25-May-2019) -- End




    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.


    'Pinkal (10-Jan-2019) -- Start
    'Enhancement - Change P2P Service As Per Rutta's Guidance on 09-Jan-2019 on voice chat for NMB  .

    'Private Sub chkBudgetMandatory_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBudgetMandatory.CheckedChanged, chkHRExpense.CheckedChanged
    '    Try
    '        If CType(sender, CheckBox).Name = chkBudgetMandatory.Name Then
    '            chkHRExpense.Checked = Not chkBudgetMandatory.Checked
    '            rdOpex.Enabled = True
    '            rdCapex.Enabled = True
    '            rdOpex.Checked = True
    '        ElseIf CType(sender, CheckBox).Name = chkHRExpense.Name Then
    '            chkBudgetMandatory.Checked = Not chkHRExpense.Checked
    '            rdOpex.Checked = False
    '            rdCapex.Checked = False
    '            rdOpex.Enabled = False
    '            rdCapex.Enabled = False
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkBudgetMandatory_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (10-Jan-2019) -- End

    'Pinkal (20-Nov-2018) -- End

    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
    Private Sub chkAllFinalApprovedUser_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllFinalApprovedUser.CheckedChanged
        Try
            RemoveHandler dgFinalApprovedUser.CellContentClick, AddressOf dgFinalApprovedUser_CellContentClick
            For Each dr As DataRowView In dvUser
                dr("ischeck") = chkAllFinalApprovedUser.Checked
                dr.EndEdit()
            Next
            dvUser.Table().AcceptChanges()
            AddHandler dgFinalApprovedUser.CellContentClick, AddressOf dgFinalApprovedUser_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAllFinalApprovedUser_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkNtfAfterFinalApproval_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNtfAfterFinalApproval.CheckedChanged
        Try
            If chkNtfAfterFinalApproval.Checked = True Then
                dgFinalApprovedUser.Enabled = True
            Else
                dgFinalApprovedUser.Enabled = False
                chkAllFinalApprovedUser.Checked = False
                txtFinalApprovedUserSearch.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNtfAfterFinalApproval_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 Dec 2023) -- End



#End Region

#Region "DataGrid Event"

    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
    Private Sub dgFinalApprovedUser_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFinalApprovedUser.CellContentClick, dgFinalApprovedUser.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            RemoveHandler chkAllFinalApprovedUser.CheckedChanged, AddressOf chkAllFinalApprovedUser_CheckedChanged

            If e.ColumnIndex = objdgcolhFinalApprovedUserCheck.Index Then
                If dgFinalApprovedUser.IsCurrentCellDirty Then
                    dgFinalApprovedUser.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                dvUser.Table.AcceptChanges()
                SetCheckBoxValue()
            End If

            AddHandler chkAllFinalApprovedUser.CheckedChanged, AddressOf chkAllFinalApprovedUser_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgFinalApprovedUser_CellContentClick", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 Dec 2023) -- End

#End Region

#Region " TextBox Events "

    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
    Private Sub txtFinalApprovedUserSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFinalApprovedUserSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtFinalApprovedUserSearch.Text.Trim.Length > 0 Then
                strSearch = "[" & dgcolhFinalApprovedUser.DataPropertyName & "] like '%" & txtFinalApprovedUserSearch.Text.Trim & "%'"
            End If
            dvUser.RowFilter = strSearch
            SetCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFinalApprovedUserSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 Dec 2023) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbExpenseInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbExpenseInformation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbP2PExpenseSetting.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbP2PExpenseSetting.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbExpenseInformation.Text = Language._Object.getCaption(Me.gbExpenseInformation.Name, Me.gbExpenseInformation.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.Name, Me.lblUoM.Text)
			Me.LblLeaveType.Text = Language._Object.getCaption(Me.LblLeaveType.Name, Me.LblLeaveType.Text)
			Me.chkConsiderPayroll.Text = Language._Object.getCaption(Me.chkConsiderPayroll.Name, Me.chkConsiderPayroll.Text)
			Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
            Me.chkLeaveEncashment.Text = Language._Object.getCaption(Me.chkLeaveEncashment.Name, Me.chkLeaveEncashment.Text)
            Me.chkIsAccrued.Text = Language._Object.getCaption(Me.chkIsAccrued.Name, Me.chkIsAccrued.Text)
			Me.chkSecRouteMandatory.Text = Language._Object.getCaption(Me.chkSecRouteMandatory.Name, Me.chkSecRouteMandatory.Text)
            Me.chkDoNotShowExpInCR.Text = Language._Object.getCaption(Me.chkDoNotShowExpInCR.Name, Me.chkDoNotShowExpInCR.Text)
            Me.ChkAttachDocumentMandatory.Text = Language._Object.getCaption(Me.ChkAttachDocumentMandatory.Name, Me.ChkAttachDocumentMandatory.Text)
			Me.LblBalanceSettings.Text = Language._Object.getCaption(Me.LblBalanceSettings.Name, Me.LblBalanceSettings.Text)
			Me.rdIssueQtyTotalBal.Text = Language._Object.getCaption(Me.rdIssueQtyTotalBal.Name, Me.rdIssueQtyTotalBal.Text)
			Me.rdIssueQtyBalAsOnDate.Text = Language._Object.getCaption(Me.rdIssueQtyBalAsOnDate.Name, Me.rdIssueQtyBalAsOnDate.Text)
            Me.grpExpenseSettings.Text = Language._Object.getCaption(Me.grpExpenseSettings.Name, Me.grpExpenseSettings.Text)
            Me.chkConsiderDependants.Text = Language._Object.getCaption(Me.chkConsiderDependants.Name, Me.chkConsiderDependants.Text)
            Me.gbP2PExpenseSetting.Text = Language._Object.getCaption(Me.gbP2PExpenseSetting.Name, Me.gbP2PExpenseSetting.Text)
            Me.chkHRExpense.Text = Language._Object.getCaption(Me.chkHRExpense.Name, Me.chkHRExpense.Text)
            Me.chkBudgetMandatory.Text = Language._Object.getCaption(Me.chkBudgetMandatory.Name, Me.chkBudgetMandatory.Text)
            Me.LblGLCode.Text = Language._Object.getCaption(Me.LblGLCode.Name, Me.LblGLCode.Text)
            Me.rdCapex.Text = Language._Object.getCaption(Me.rdCapex.Name, Me.rdCapex.Text)
            Me.rdOpex.Text = Language._Object.getCaption(Me.rdOpex.Name, Me.rdOpex.Text)
            Me.LblExpenditureType.Text = Language._Object.getCaption(Me.LblExpenditureType.Name, Me.LblExpenditureType.Text)
            Me.chkShowOnESS.Text = Language._Object.getCaption(Me.chkShowOnESS.Name, Me.chkShowOnESS.Text)
            Me.chkMakeUnitPriceEditable.Text = Language._Object.getCaption(Me.chkMakeUnitPriceEditable.Name, Me.chkMakeUnitPriceEditable.Text)
            Me.chkIsImprest.Text = Language._Object.getCaption(Me.chkIsImprest.Name, Me.chkIsImprest.Text)
            Me.LblMaximumExpenseQty.Text = Language._Object.getCaption(Me.LblMaximumExpenseQty.Name, Me.LblMaximumExpenseQty.Text)
            Me.lblDefaultCostCenter.Text = Language._Object.getCaption(Me.lblDefaultCostCenter.Name, Me.lblDefaultCostCenter.Text)
            Me.chkNotAllowBackDate.Text = Language._Object.getCaption(Me.chkNotAllowBackDate.Name, Me.chkNotAllowBackDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expense Code is mandatory information. Please provide Expense Code.")
			Language.setMessage(mstrModuleName, 2, "Expense Name is mandatory information. Please provide Expense Name.")
			Language.setMessage(mstrModuleName, 3, "UoM is mandatory information. Please select UoM.")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Expense Category is compulsory information.Please Select Expense Category.")
			Language.setMessage(mstrModuleName, 6, "Leave Type is compulsory information.Please Select Leave Type.")
			Language.setMessage(mstrModuleName, 7, "Transaction Head Type is compulsory information.Please Select Transaction Head Type.")
			Language.setMessage(mstrModuleName, 8, "Accrue setting is compulsory information.Please set Accrue Setting.")
            Language.setMessage(mstrModuleName, 9, "Imprest")
            Language.setMessage(mstrModuleName, 10, "Sorry, you can only define one expense for the selected category which can have")
            Language.setMessage(mstrModuleName, 11, "setting enabled.")
            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot define expense for the selected category, As you have defined multiple expense under rebate privilege category with")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>


End Class