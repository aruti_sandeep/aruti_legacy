﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmClaimPosting

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmClaimPosting"
    Private objClaimPosting As clsclaim_process_Tran
    Private mdtData As DataTable = Nothing
    Private xCount As Integer = 0
#End Region

#Region " Private Methods & Functions "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objemployee As New clsEmployee_Master
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objemployee.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            dsList = objemployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, False, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .DisplayMember = "employeename"
                .ValueMember = "employeeunkid"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objemployee = Nothing


            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)

            'S.SANDEEP |25-APR-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-143V2
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True, False, False, False)
            'S.SANDEEP |25-APR-2022| -- END

            'Pinkal (04-Jul-2020) -- End


            With cboExpCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            Dim objExpMst As New clsExpense_Master
            dsList = objExpMst.getComboList(-1, True, "List")
            With cboExpense
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objExpMst = Nothing

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            Dim objtranhead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objtranhead.getComboList("List", True, , , , , , " typeof_id <> " & enTypeOf.Salary)
            dsList = objtranhead.getComboList(FinancialYear._Object._DatabaseName, "List", True, , , , , , " typeof_id <> " & enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End
            With cboTranhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objtranhead = Nothing

            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Show Unposted Claim Transactions"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Show Posted Claim Transactions"))
                .SelectedIndex = 0
            End With

            dsList = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUOM
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim dsList As DataSet = Nothing
            Dim mdtFilterData As DataTable = Nothing
            Dim strSearch As String = ""

            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
            '    dsList = objClaimPosting.GetList("List", True, , "isposted=0")
            'ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
            '    dsList = objClaimPosting.GetList("List", True, , "isposted=1")
            'End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                strSearch = "AND cmexpense_master.expensetypeid = " & CInt(cboExpCategory.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND cmclaim_process_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If txtClaimNo.Text.Trim.Length > 0 Then
                strSearch &= "AND cmclaim_request_master.claimrequestno LIKE '%" & txtClaimNo.Text.Trim & "%' "
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                strSearch &= "AND cmclaim_process_tran.expenseunkid = " & CInt(cboExpense.SelectedValue) & " "
            End If

            If CInt(cboUOM.SelectedValue) > 0 Then
                strSearch &= "AND cmexpense_master.uomunkid = " & CInt(cboUOM.SelectedValue) & " "
            End If

            If CInt(cboViewBy.SelectedIndex) = 1 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND cmclaim_process_tran.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If

            If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
                strSearch &= "AND cmclaim_process_tran.isposted = 0 "
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
                strSearch &= "AND cmclaim_process_tran.isposted = 1 "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
                dsList = objClaimPosting.GetList("List", True, , strSearch)
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
                dsList = objClaimPosting.GetList("List", True, , strSearch)
            End If

                mdtFilterData = dsList.Tables(0)

            'Pinkal (04-Jul-2020) -- End

            If mdtFilterData IsNot Nothing AndAlso mdtFilterData.Columns.Contains("Isgroup") = False Then
                mdtFilterData.Columns.Add("Isgroup", Type.GetType("System.Boolean"))
                mdtFilterData.Columns("Isgroup").DefaultValue = False
            End If

            If mdtFilterData IsNot Nothing AndAlso mdtFilterData.Columns.Contains("Ischecked") = False Then
                mdtFilterData.Columns.Add("ischecked", Type.GetType("System.Boolean"))
                mdtFilterData.Columns("ischecked").DefaultValue = False
            End If

            If mdtFilterData IsNot Nothing AndAlso mdtFilterData.Columns.Contains("ischange") = False Then
                mdtFilterData.Columns.Add("ischange", Type.GetType("System.Boolean"))
                mdtFilterData.Columns("ischange").DefaultValue = False
            End If

            If mdtData IsNot Nothing Then
                mdtData.Clear()
            End If

            If mdtFilterData.Rows.Count > 0 Then
                mdtData = mdtFilterData.Clone

                Dim mintClaimFormMstID As Integer = -1
                Dim drNewRow As DataRow = Nothing
                Dim intRowCount As Integer = -1
                For Each dr As DataRow In mdtFilterData.Rows

                    If mintClaimFormMstID <> CInt(dr("crmasterunkid")) Then
                        mintClaimFormMstID = CInt(dr("crmasterunkid"))
                        drNewRow = mdtData.NewRow
                        drNewRow("ischange") = False
                        drNewRow("Ischecked") = False
                        drNewRow("Isgroup") = True
                        drNewRow("crmasterunkid") = mintClaimFormMstID
                        drNewRow("crprocesstranunkid") = -1
                        drNewRow("crapprovaltranunkid") = -1
                        drNewRow("crtranunkid") = -1
                        drNewRow("employeeunkid") = -1
                        drNewRow("periodunkid") = -1
                        drNewRow("expenseunkid") = -1
                        drNewRow("secrouteunkid") = -1
                        drNewRow("costingunkid") = -1
                        drNewRow("uomunkid") = -1
                        drNewRow("Employee") = ""
                        drNewRow("Claim") = dr("Claim")
                        drNewRow("Expense") = ""
                        drNewRow("Sector") = ""
                        drNewRow("Period") = ""
                        drNewRow("Qtytrnhead") = ""
                        drNewRow("Amttrnhead") = ""
                        drNewRow("unitprice") = DBNull.Value
                        drNewRow("quantity") = DBNull.Value
                        drNewRow("amount") = DBNull.Value
                        drNewRow("qtytranheadunkid") = -1
                        drNewRow("amttranheadunkid") = -1
                        drNewRow("processedrate") = DBNull.Value
                        drNewRow("processedamt") = DBNull.Value
                        drNewRow("expense_remark") = ""
                        drNewRow("approveremployeeunkid") = -1
                        drNewRow("crapproverunkid") = -1
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        drNewRow("currency_sign") = ""
                        'Pinkal (04-Feb-2019) -- End
                        mdtData.Rows.Add(drNewRow)
                        intRowCount += 1
                    End If
                    drNewRow = mdtData.NewRow
                    drNewRow("ischange") = False
                    drNewRow("Ischecked") = False
                    drNewRow("Isgroup") = False
                    drNewRow("crmasterunkid") = mintClaimFormMstID
                    drNewRow("crprocesstranunkid") = dr("crprocesstranunkid")
                    drNewRow("crapprovaltranunkid") = dr("crapprovaltranunkid")
                    drNewRow("crtranunkid") = dr("crtranunkid")
                    drNewRow("employeeunkid") = dr("employeeunkid")
                    drNewRow("periodunkid") = dr("periodunkid")
                    drNewRow("expenseunkid") = dr("expenseunkid")
                    drNewRow("secrouteunkid") = dr("secrouteunkid")
                    drNewRow("costingunkid") = dr("costingunkid")
                    drNewRow("uomunkid") = dr("uomunkid")
                    drNewRow("Employee") = dr("Employee")
                    drNewRow("Claim") = ""
                    drNewRow("Expense") = dr("Expense")
                    drNewRow("Sector") = dr("Sector")
                    drNewRow("Period") = dr("Period")
                    drNewRow("Qtytrnhead") = dr("Qtytrnhead")
                    drNewRow("Amttrnhead") = dr("Amttrnhead")
                    drNewRow("unitprice") = dr("unitprice")
                    drNewRow("quantity") = dr("quantity")
                    drNewRow("amount") = dr("amount")
                    drNewRow("qtytranheadunkid") = dr("qtytranheadunkid")
                    drNewRow("amttranheadunkid") = dr("amttranheadunkid")
                    drNewRow("processedrate") = dr("processedrate")
                    drNewRow("processedamt") = dr("processedamt")
                    drNewRow("expense_remark") = dr("expense_remark")
                    drNewRow("approveremployeeunkid") = dr("approveremployeeunkid")
                    drNewRow("crapproverunkid") = dr("crapproverunkid")
                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    drNewRow("currency_sign") = dr("currency_sign")
                    'Pinkal (04-Feb-2019) -- End
                    mdtData.Rows.Add(drNewRow)
                    intRowCount += 1
                Next

            End If

            dgvClaimPosting.AutoGenerateColumns = False
            objcolhIschecked.DataPropertyName = "Ischecked"
            objdgcolhIsGroup.DataPropertyName = "Isgroup"
            dgcolhParticulars.DataPropertyName = "Claim"
            dgcolhExpense.DataPropertyName = "Expense"
            dgcolhSector.DataPropertyName = "Sector"
            dgcolhUnitprice.DataPropertyName = "unitprice"
            dgcolhQuantity.DataPropertyName = "quantity"
            dgcolhAmount.DataPropertyName = "amount"

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dgcolhCurrency.DataPropertyName = "currency_sign"
            'Pinkal (04-Feb-2019) -- End

            dgcolhQtytranhead.DataPropertyName = "qtytranheadunkid"
            dgcolhAmttranhead.DataPropertyName = "amttranheadunkid"
            objdgcolhClaimFomMstID.DataPropertyName = "crmasterunkid"
            dgvClaimPosting.DataSource = mdtData
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.Black
            dgvcsChild.SelectionBackColor = Color.White
            dgvcsChild.BackColor = Color.White


            For i As Integer = 0 To dgvClaimPosting.RowCount - 1
                If CBool(dgvClaimPosting.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    dgvClaimPosting.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgvClaimPosting.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvClaimPosting.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgvClaimPosting.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.Black
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


            For i As Integer = 0 To dgvClaimPosting.RowCount - 1
                If CBool(dgvClaimPosting.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    If dgvClaimPosting.Rows(i).Cells(objdgcolhCollaps.Index).Value Is Nothing Then
                        dgvClaimPosting.Rows(i).Cells(objdgcolhCollaps.Index).Value = "-"
                    End If
                    dgvClaimPosting.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseHeader
                Else
                    dgvClaimPosting.Rows(i).Cells(objdgcolhCollaps.Index).Value = ""
                    dgvClaimPosting.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = mdtData.Select("ischecked = true")
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvClaimPosting.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvClaimPosting.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetParentChecked(ByVal intRowindex As Integer, ByVal intClaimFormMstID As Integer)
        Try
            Dim drRow() As DataRow = mdtData.Select("crmasterunkid = " & intClaimFormMstID)
            Dim drCheckRow() As DataRow = mdtData.Select("crmasterunkid = " & intClaimFormMstID & " AND  ischecked = true AND crprocesstranunkid > 0")

            If drRow.Length - 1 = drCheckRow.Length Then
                drRow(0)("ischecked") = CheckState.Checked
            Else
                drRow(0)("ischecked") = CheckState.Unchecked
            End If
            SetCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetParentChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnPosting.Enabled = User._Object.Privilege._AllowtoPostClaimExpenseToPayroll
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    'Pinkal (04-Jul-2020) -- Start
    'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.

    Private Function UpdateRow(ByVal drRow As DataRow, ByVal xPeriodId As Integer, ByVal xTranHeadId As Integer) As Boolean
        Try
            RemoveHandler dgvClaimPosting.DataBindingComplete, AddressOf dgvClaimPosting_DataBindingComplete
            If drRow IsNot Nothing Then
                drRow("periodunkid") = xPeriodId
                If rdQtyTrnheadMapping.Checked Then
                    drRow("qtytranheadunkid") = xTranHeadId
                    drRow("amttranheadunkid") = -1
                ElseIf rdAmtTranHeadMapping.Checked Then
                    drRow("qtytranheadunkid") = -1
                    drRow("amttranheadunkid") = xTranHeadId
                End If
                drRow("ischange") = True
                drRow.AcceptChanges()
            End If
            xCount += 1
            AddHandler dgvClaimPosting.DataBindingComplete, AddressOf dgvClaimPosting_DataBindingComplete
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdatetRow", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (04-Jul-2020) -- End
#End Region

#Region " Form's Events "

    Private Sub frmClaimPosting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            objClaimPosting = New clsclaim_process_Tran
            cboViewBy.SelectedIndex = 0
            cboViewBy.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClaimPosting_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsclaim_process_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsclaim_process_Tran"
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try


            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            dgvClaimPosting.DataSource = Nothing
            dgcolhQtytranhead.DataSource = Nothing
            dgcolhAmttranhead.DataSource = Nothing
            'Pinkal (04-Jul-2020) -- End


            Dim objtranhead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsList As DataSet = objtranhead.getComboList("List", False, , , , , , " typeof_id <> " & enTypeOf.Salary)
            Dim dsList As DataSet = objtranhead.getComboList(FinancialYear._Object._DatabaseName, "List", False, , , , , , " typeof_id <> " & enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End

            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("tranheadunkid") = 0
            drRow("name") = ""
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            With dgcolhQtytranhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .AutoComplete = True
                .DataSource = dsList.Tables(0)
            End With

            With dgcolhAmttranhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .AutoComplete = True
                .DataSource = dsList.Tables(0).Copy
            End With

            If CInt(cboViewBy.SelectedIndex) = 1 And CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Select()
                Exit Sub
            End If

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try

            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            chkSelectAll.Checked = False
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            'Pinkal (04-Jul-2020) -- End

            cboViewBy.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            txtClaimNo.Text = ""
            cboExpCategory.SelectedIndex = 0
            cboExpense.SelectedIndex = 0
            cboUOM.SelectedIndex = 0
            cboPeriod.SelectedIndex = 0
            cboTranhead.SelectedIndex = 0
            dgvClaimPosting.DataSource = Nothing
            dgcolhQtytranhead.DataSource = Nothing
            dgcolhAmttranhead.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPosting.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Select()
                Exit Sub
            ElseIf CInt(cboTranhead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Transaction head is compulsory information.Please Select Transaction head."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboTranhead.Select()
                Exit Sub
            End If

            If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtData.Select("Isgroup = 0 AND ischecked = 1")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one claim transaction to do futher operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    Dim dr As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                    Dim mdtStartDate As Date = eZeeDate.convertDate(dr.Item("start_date").ToString)
                    Dim mdtEndDate As Date = eZeeDate.convertDate(dr.Item("end_date").ToString())

                    Dim lstIDs As List(Of String) = (From p In mdtData Where (CBool(p.Item("Isgroup")) = False And CBool(p.Item("ischecked")) = True) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
                    Dim strEmpIDs As String = String.Join(",", CType(lstIDs.ToArray(), String()))

                    Dim objLeaveTran As New clsTnALeaveTran
                    If objLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, mdtEndDate.Date, enModuleReference.Payroll) Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 10, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Exit Sub
                    End If
                End If


                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.

                'For i As Integer = 0 To drRow.Length - 1
                '    drRow(i)("periodunkid") = CInt(cboPeriod.SelectedValue)
                '    If rdQtyTrnheadMapping.Checked Then
                '        drRow(i)("qtytranheadunkid") = CInt(cboTranhead.SelectedValue)
                '        drRow(i)("amttranheadunkid") = -1
                '    ElseIf rdAmtTranHeadMapping.Checked Then
                '        drRow(i)("qtytranheadunkid") = -1
                '        drRow(i)("amttranheadunkid") = CInt(cboTranhead.SelectedValue)
                '    End If
                '    drRow(i)("ischecked") = False
                '    drRow(i)("ischange") = True
                '    dgvClaimPosting_CellContentClick(New Object, New DataGridViewCellEventArgs(objcolhIschecked.Index, mdtData.Rows.IndexOf(drRow(i))))
                'Next
                Me.Cursor = Cursors.WaitCursor
                drRow.AsEnumerable().ToList.ForEach(Function(x) UpdateRow(x, CInt(cboPeriod.SelectedValue), CInt(cboTranhead.SelectedValue)))
                'Pinkal (04-Jul-2020) -- End

                mdtData.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPost_Click", mstrModuleName)
        Finally
            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            Me.Cursor = Cursors.Default
            'Pinkal (04-Jul-2020) -- End
        End Try
    End Sub

    Private Sub btnUnposting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnposting.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Select()
                Exit Sub
            End If

            If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtData.Select("Isgroup = 0 AND ischecked = 1")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one claim transaction to do futher operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    Dim dr As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                    Dim mdtStartDate As Date = eZeeDate.convertDate(dr.Item("start_date").ToString)
                    Dim mdtEndDate As Date = eZeeDate.convertDate(dr.Item("end_date").ToString())

                    Dim lstIDs As List(Of String) = (From p In mdtData Where (CBool(p.Item("Isgroup")) = False And CBool(p.Item("ischecked")) = True) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
                    Dim strEmpIDs As String = String.Join(",", CType(lstIDs.ToArray(), String()))

                    Dim objLeaveTran As New clsTnALeaveTran
                    If objLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, mdtEndDate.Date, enModuleReference.Payroll) Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 10, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Exit Sub
                    End If
                End If


                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                'For i As Integer = 0 To drRow.Length - 1
                '    drRow(i)("periodunkid") = 0
                '    drRow(i)("qtytranheadunkid") = -1
                '    drRow(i)("amttranheadunkid") = -1
                '    drRow(i)("processedrate") = 0
                '    drRow(i)("processedamt") = 0
                '    drRow(i)("ischecked") = False
                '    drRow(i)("ischange") = True
                '    dgvClaimPosting_CellContentClick(New Object, New DataGridViewCellEventArgs(objcolhIschecked.Index, mdtData.Rows.IndexOf(drRow(i))))
                'Next
                Me.Cursor = Cursors.WaitCursor
                drRow.AsEnumerable().ToList().ForEach(Function(x) UpdateRow(x, 0, 0))
                'Pinkal (04-Jul-2020) -- End
                mdtData.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUnposting_Click", mstrModuleName)
        Finally
            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            Me.Cursor = Cursors.Default
            'Pinkal (04-Jul-2020) -- End
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then


                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                'Dim drRow() As DataRow = mdtData.Select("Isgroup = 0 AND ischange = 1")
                'If drRow.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please post/unpost atleast one claim transaction."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    Exit Sub
                'End If
                'Pinkal (04-Jul-2020) -- End

                Dim drRow = mdtData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("Isgroup") = False And x.Field(Of Boolean)("ischange") = True)
                If drRow IsNot Nothing AndAlso drRow.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please post/unpost atleast one claim transaction."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objClaimPosting._FormName = mstrModuleName
                objClaimPosting._LoginEmployeeunkid = 0
                objClaimPosting._ClientIP = getIP()
                objClaimPosting._HostName = getHostName()
                objClaimPosting._FromWeb = False
                objClaimPosting._AuditUserId = User._Object._Userunkid
objClaimPosting._CompanyUnkid = Company._Object._Companyunkid
                objClaimPosting._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                Dim mstrCrProcessIds As String = String.Join(",", (From p In drRow Select (p.Item("crprocesstranunkid").ToString)).ToArray())
                objClaimPosting._Userunkid = User._Object._Userunkid
                'Pinkal (04-Jul-2020) -- End

                If CInt(cboViewBy.SelectedIndex) = 0 Then    'Posting

                    'Pinkal (04-Jul-2020) -- Start
                    'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                    'If objClaimPosting.Posting_Unposting_Claim(True, mdtData, ConfigParameter._Object._CurrentDateAndTime) = False Then
                    If objClaimPosting.Posting_Unposting_Claim(True, CInt(cboPeriod.SelectedValue), CInt(IIf(rdQtyTrnheadMapping.Checked, CInt(cboTranhead.SelectedValue), 0)), CInt(IIf(rdAmtTranHeadMapping.Checked, CInt(cboTranhead.SelectedValue), 0)), mstrCrProcessIds) = False Then
                        'Pinkal (04-Jul-2020) -- End
                        eZeeMsgBox.Show(objClaimPosting._Message, enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Claim Transaction posting done successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        FillList()
                        objbtnReset_Click(sender, New EventArgs())
                    End If

                ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   'UnPosting

                    'Pinkal (04-Jul-2020) -- Start
                    'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                    'If objClaimPosting.Posting_Unposting_Claim(False, mdtData, ConfigParameter._Object._CurrentDateAndTime) = False Then
                    If objClaimPosting.Posting_Unposting_Claim(False, 0, CInt(IIf(rdQtyTrnheadMapping.Checked, CInt(cboTranhead.SelectedValue), 0)), CInt(IIf(rdAmtTranHeadMapping.Checked, CInt(cboTranhead.SelectedValue), 0)), mstrCrProcessIds) = False Then
                        'Pinkal (04-Jul-2020) -- End
                        eZeeMsgBox.Show(objClaimPosting._Message, enMsgBoxStyle.Information)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Claim Transaction unposting done successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        FillList()
                        objbtnReset_Click(sender, New EventArgs())
                    End If
                End If



            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                '.CodeMember = "code"
                .CodeMember = "employeecode"
                'Pinkal (04-Jul-2020) -- End
            End With
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchExpCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpCategory.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboExpCategory.ValueMember
                .DisplayMember = cboExpCategory.DisplayMember
                .DataSource = CType(cboExpCategory.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboExpCategory.SelectedValue = frm.SelectedValue
                cboExpCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboExpense.ValueMember
                .DisplayMember = cboExpense.DisplayMember
                .DataSource = CType(cboExpense.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboExpense.SelectedValue = frm.SelectedValue
                cboExpense.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .DataSource = CType(cboPeriod.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboPeriod.SelectedValue = frm.SelectedValue
                cboPeriod.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboTranhead.ValueMember
                .DisplayMember = cboTranhead.DisplayMember
                .DataSource = CType(cboTranhead.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboTranhead.SelectedValue = frm.SelectedValue
                cboTranhead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgvClaimPosting_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvClaimPosting.DataBindingComplete
        Try
            Call SetGridColor()
            Call SetCollapseValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvClaimPosting_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvClaimPosting_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvClaimPosting.CellContentClick, dgvClaimPosting.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvClaimPosting.IsCurrentCellDirty Then
                Me.dgvClaimPosting.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvClaimPosting.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then

                Select Case CInt(e.ColumnIndex)
                    Case objdgcolhCollaps.Index
                        If dgvClaimPosting.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value.ToString = "-" Then
                            dgvClaimPosting.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
                        Else
                            dgvClaimPosting.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvClaimPosting.RowCount - 1
                            If CInt(dgvClaimPosting.Rows(e.RowIndex).Cells(objdgcolhClaimFomMstID.Index).Value) = CInt(dgvClaimPosting.Rows(i).Cells(objdgcolhClaimFomMstID.Index).Value) Then
                                If dgvClaimPosting.Rows(i).Visible = False Then
                                    dgvClaimPosting.Rows(i).Visible = True
                                Else
                                    dgvClaimPosting.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next

                    Case objcolhIschecked.Index

                        Dim blnFlg As Boolean = CBool(dgvClaimPosting.Rows(e.RowIndex).Cells(objcolhIschecked.Index).Value)
                        Dim drRow() As DataRow = mdtData.Select("crmasterunkid = " & CInt(dgvClaimPosting.Rows(e.RowIndex).Cells(objdgcolhClaimFomMstID.Index).Value))

                        If drRow.Length > 0 Then
                            RemoveHandler dgvClaimPosting.DataBindingComplete, AddressOf dgvClaimPosting_DataBindingComplete

                            For k As Integer = 1 To drRow.Length - 1
                                dgvClaimPosting.Rows(e.RowIndex + k).Cells(objcolhIschecked.Index).Value = blnFlg
                                mdtData.Rows(e.RowIndex + k).AcceptChanges()
                            Next
                            AddHandler dgvClaimPosting.DataBindingComplete, AddressOf dgvClaimPosting_DataBindingComplete

                            SetCheckBoxValue()

                        End If


                End Select

            ElseIf e.ColumnIndex = objcolhIschecked.Index Then
                mdtData.Rows(e.RowIndex).AcceptChanges()
                SetParentChecked(e.RowIndex, CInt(dgvClaimPosting.Rows(e.RowIndex).Cells(objdgcolhClaimFomMstID.Index).Value))

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvClaimPosting_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub dgvClaimPosting_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvClaimPosting.CellEnter
    '    Try
    '        If CBool(dgvClaimPosting.CurrentRow.Cells(objdgcolhIsGroup.Index).Value) = False AndAlso (e.ColumnIndex = dgcolhQtytranhead.Index OrElse e.ColumnIndex = dgcolhAmttranhead.Index) Then
    '            SendKeys.Send("{F4}")
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvClaimPosting_CellEnter", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub dgvClaimPosting_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvClaimPosting.EditingControlShowing
    '    Try
    '        If CBool(dgvClaimPosting.CurrentRow.Cells(objdgcolhIsGroup.Index).Value) AndAlso (dgvClaimPosting.CurrentCell.ColumnIndex = dgcolhQtytranhead.Index OrElse dgvClaimPosting.CurrentCell.ColumnIndex = dgcolhAmttranhead.Index) Then
    '            Dim cbo As ComboBox = CType(e.Control, ComboBox)
    '            cbo.DataSource = Nothing
    '            cbo.Hide()
    '        ElseIf CBool(dgvClaimPosting.CurrentRow.Cells(objdgcolhIsGroup.Index).Value) = False AndAlso dgvClaimPosting.CurrentCell.ColumnIndex = dgcolhQtytranhead.Index _
    '                AndAlso (CInt(dgvClaimPosting.CurrentCell.Value) <= 0 AndAlso CInt(dgvClaimPosting.CurrentRow.Cells(dgcolhAmttranhead.Index).Value) > 0) Then
    '            Dim cbo As ComboBox = CType(e.Control, ComboBox)
    '            cbo.DataSource = Nothing
    '            cbo.Hide()

    '        ElseIf CBool(dgvClaimPosting.CurrentRow.Cells(objdgcolhIsGroup.Index).Value) = False AndAlso dgvClaimPosting.CurrentCell.ColumnIndex = dgcolhAmttranhead.Index _
    '                            AndAlso (CInt(dgvClaimPosting.CurrentCell.Value) <= 0 AndAlso CInt(dgvClaimPosting.CurrentRow.Cells(dgcolhQtytranhead.Index).Value) > 0) Then
    '            Dim cbo As ComboBox = CType(e.Control, ComboBox)
    '            cbo.DataSource = Nothing
    '            cbo.Hide()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvClaimPosting_EditingControlShowing", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub dgvClaimPosting_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvClaimPosting.DataError

    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
                RemoveHandler dgvClaimPosting.CellContentClick, AddressOf dgvClaimPosting_CellContentClick
                RemoveHandler dgvClaimPosting.CellContentDoubleClick, AddressOf dgvClaimPosting_CellContentClick
                RemoveHandler dgvClaimPosting.DataBindingComplete, AddressOf dgvClaimPosting_DataBindingComplete

                For Each dr As DataRow In mdtData.Rows
                    dr("ischecked") = chkSelectAll.Checked
                    dr.EndEdit()
                Next
                mdtData.AcceptChanges()
                Call SetGridColor()
                Call SetCollapseValue()
                AddHandler dgvClaimPosting.CellContentClick, AddressOf dgvClaimPosting_CellContentClick
                AddHandler dgvClaimPosting.CellContentDoubleClick, AddressOf dgvClaimPosting_CellContentClick
                AddHandler dgvClaimPosting.DataBindingComplete, AddressOf dgvClaimPosting_DataBindingComplete
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            cboTranhead.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            If CInt(cboViewBy.SelectedIndex) = 0 Then
                cboTranhead.Enabled = True
                cboPeriod.Enabled = True
                objbtnSearchTranHead.Enabled = True
                objbtnSearchPeriod.Enabled = True
                rdQtyTrnheadMapping.Enabled = True
                rdAmtTranHeadMapping.Enabled = True
                btnPosting.Visible = True
                btnUnposting.Visible = False
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then
                cboTranhead.Enabled = False
                objbtnSearchTranHead.Enabled = False
                'cboPeriod.Enabled = False
                'objbtnSearchPeriod.Enabled = False
                rdQtyTrnheadMapping.Enabled = False
                rdAmtTranHeadMapping.Enabled = False
                btnPosting.Visible = False
                btnUnposting.Visible = True
            End If
            dgvClaimPosting.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnPosting.GradientBackColor = GUI._ButttonBackColor
            Me.btnPosting.GradientForeColor = GUI._ButttonFontColor

            Me.btnUnposting.GradientBackColor = GUI._ButttonBackColor
            Me.btnUnposting.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.Name, Me.lblClaimNo.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.Name, Me.lblExpCategory.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.Name, Me.LblExpense.Text)
            Me.LblTranHead.Text = Language._Object.getCaption(Me.LblTranHead.Name, Me.LblTranHead.Text)
            Me.btnPosting.Text = Language._Object.getCaption(Me.btnPosting.Name, Me.btnPosting.Text)
            Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
            Me.LblUOM.Text = Language._Object.getCaption(Me.LblUOM.Name, Me.LblUOM.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.btnUnposting.Text = Language._Object.getCaption(Me.btnUnposting.Name, Me.btnUnposting.Text)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.rdAmtTranHeadMapping.Text = Language._Object.getCaption(Me.rdAmtTranHeadMapping.Name, Me.rdAmtTranHeadMapping.Text)
            Me.rdQtyTrnheadMapping.Text = Language._Object.getCaption(Me.rdQtyTrnheadMapping.Name, Me.rdQtyTrnheadMapping.Text)
            Me.dgcolhParticulars.HeaderText = Language._Object.getCaption(Me.dgcolhParticulars.Name, Me.dgcolhParticulars.HeaderText)
            Me.dgcolhExpense.HeaderText = Language._Object.getCaption(Me.dgcolhExpense.Name, Me.dgcolhExpense.HeaderText)
            Me.dgcolhSector.HeaderText = Language._Object.getCaption(Me.dgcolhSector.Name, Me.dgcolhSector.HeaderText)
            Me.dgcolhUnitprice.HeaderText = Language._Object.getCaption(Me.dgcolhUnitprice.Name, Me.dgcolhUnitprice.HeaderText)
            Me.dgcolhQuantity.HeaderText = Language._Object.getCaption(Me.dgcolhQuantity.Name, Me.dgcolhQuantity.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhCurrency.HeaderText = Language._Object.getCaption(Me.dgcolhCurrency.Name, Me.dgcolhCurrency.HeaderText)
            Me.dgcolhQtytranhead.HeaderText = Language._Object.getCaption(Me.dgcolhQtytranhead.Name, Me.dgcolhQtytranhead.HeaderText)
            Me.dgcolhAmttranhead.HeaderText = Language._Object.getCaption(Me.dgcolhAmttranhead.Name, Me.dgcolhAmttranhead.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Show Unposted Claim Transactions")
            Language.setMessage(mstrModuleName, 2, "Show Posted Claim Transactions")
            Language.setMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 4, "Transaction head is compulsory information.Please Select Transaction head.")
            Language.setMessage(mstrModuleName, 5, "Please check atleast one claim transaction to do futher operation.")
            Language.setMessage(mstrModuleName, 6, "Claim Transaction posting done successfully.")
            Language.setMessage(mstrModuleName, 7, "Claim Transaction unposting done successfully.")
            Language.setMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling.")
            Language.setMessage(mstrModuleName, 9, "Please post/unpost atleast one claim transaction.")
			Language.setMessage(mstrModuleName, 10, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class