﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSavingStatus_AddEdit

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmSavingStatus_AddEdit"
    'Private mblnIsFromLoan As Boolean = False
    Private mintTranUnkid As Integer = -1
    Private mblnCancel As Boolean = True
    Private objEmployeeSaving As clsSaving_Tran
    Private objEmployee As clsEmployee_Master
    Private objSavingScheme As clsSavingScheme
    Private objStatusTran As clsSaving_Status_Tran

    'Sohail (26 Jul 2010) -- Start
    Private mintStatus As Integer
    'Private mdblBalAmt As Double
    'Sohail (26 Jul 2010) -- End
#End Region

#Region " Display Dialog "
    'Sohail (26 Jul 2010) -- Start
    'Public Function displayDialog(ByRef intUnkId As Integer, ByVal blnIsLoan As Boolean) As Boolean
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal blnIsLoan As Boolean, ByVal intStatus As Integer) As Boolean
        mintStatus = intStatus
        'mdblBalAmt = dblBalAmt
        'Sohail (26 Jul 2010) -- End

        mintTranUnkid = intUnkId

        ' mblnIsFromLoan = blnIsLoan

        Me.ShowDialog()

        intUnkId = mintTranUnkid

        Return Not mblnCancel
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtEmployeeName.BackColor = GUI.ColorComp
            txtSavingScheme.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
            cboStatus.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsCombos = objMaster.GetLoan_Saving_Status("Status", True)
            ''Sohail (24 Jul 2010) -- Start
            'Dim dtTable As DataTable = New DataView(dsCombos.Tables("Status"), "Id <> 3", "", DataViewRowState.CurrentRows).ToTable
            ''Sohail (24 Jul 2010) -- End

            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            'Dim dtTable As DataTable = New DataView(dsCombos.Tables("Status"), "Id <> (3)", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Status"), "Id NOT IN (3,4)", "", DataViewRowState.CurrentRows).ToTable
            'Anjan (26 Jan 2012)-End 

            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                'Sohail (26 Jul 2010) -- Start
                '.DataSource = dsCombos.Tables("Status")
                .DataSource = dtTable
                'Sohail (26 Jul 2010) -- End
                .SelectedValue = 0
            End With



            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsList As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            Dim dsList As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing
            'Pinkal (12 Jan 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Info()
        Try
            txtEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " "
            txtSavingScheme.Text = objSavingScheme._Savingschemename
            cboStatus.SelectedValue = objEmployeeSaving._Savingstatus

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStatusTran._Isvoid = False
            objStatusTran._Savingtranunkid = objEmployeeSaving._Savingtranunkid
            objStatusTran._Remark = txtRemarks.Text
          
            'Sohail (27 Jul 2010) -- Start
            'objStatusTran._Staus_Date = dtpDate.Value

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            'objStatusTran._Status_Date = System.DateTime.Today
            objStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            'Pinkal (12 Jan 2015) -- End


            'Sohail (27 Jul 2010) -- End
            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voiduserunkid = -1
            objStatusTran._Statusunkid = CInt(cboStatus.SelectedValue)

            'S.SANDEEP [ 08 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objStatusTran._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 08 NOV 2012 ] -- END


            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            objStatusTran._PeriodID = CInt(cboPeriod.SelectedValue)
            'Pinkal (12 Jan 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE.

    Private Sub FillHistory()
        Try
            Dim dsList As DataSet = objStatusTran.GetPeriodWiseSavingStatusHistory("List", True, "savingtranunkid = " & mintTranUnkid)

            dgvSavingStatusHistory.AutoGenerateColumns = False
            objdgcolhIsGroup.DataPropertyName = "grp"
            dgcolhUser.DataPropertyName = "username"
            dgcolhTransactionDate.DataPropertyName = "status_date"
            dgcolhStatus.DataPropertyName = "status"
            objSavingstatustranunkid.DataPropertyName = "savingstatustranunkid"

            dgvSavingStatusHistory.DataSource = dsList.Tables(0)

            'lvSavingStatusHistory.Items.Clear()
            'Dim lvHistory As ListViewItem

            'For Each dtRow As DataRow In dsList.Tables(0).Rows
            '    lvHistory = New ListViewItem
            '    lvHistory.Text = dtRow.Item("period").ToString
            '    lvHistory.SubItems.Add(dtRow.Item("username").ToString)
            '    lvHistory.SubItems.Add(dtRow.Item("status_date").ToString)
            '    lvHistory.SubItems.Add(dtRow.Item("status").ToString)
            '    lvHistory.Tag = dtRow.Item("savingstatustranunkid")
            '    lvSavingStatusHistory.Items.Add(lvHistory)
            'Next
            'lvSavingStatusHistory.GridLines = False
            'lvSavingStatusHistory.GroupingColumn = colhPeriod
            'lvSavingStatusHistory.DisplayGroups(True)

            'If lvSavingStatusHistory.Items.Count > 5 Then
            '    colhStatus.Width = 250 - 16
            'Else
            '    colhStatus.Width = 250
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHistory", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            dgvcsHeader.Font = New Font(Me.Font.Name, Me.Font.Size, FontStyle.Bold)

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.BackColor = Color.White

            For i As Integer = 0 To dgvSavingStatusHistory.RowCount - 1
                If CBool(dgvSavingStatusHistory.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    dgvSavingStatusHistory.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgvSavingStatusHistory.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (12 Jan 2015) -- End


    'Private Sub GetValue()
    '    Try
    '        objStatusTran._Isvoid = objStatusTran._Isvoid
    '        objStatusTran._Loanadvancetranunkid = objStatusTran._Loanadvancetranunkid
    '        txtRemarks.Text = objStatusTran._Remark
    '        txtSettlementAmt.Text = CStr(objStatusTran._Settle_Amount)
    '        dtpDate.Value = objStatusTran._Staus_Date
    '        objStatusTran._Voiddatetime = objStatusTran._Voiddatetime
    '        objStatusTran._Voiduserunkid = objStatusTran._Voiduserunkid
    '        cboStatus.SelectedValue = objStatusTran._Statusunkid
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "
    Private Sub frmSavingStatus_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployeeSaving = New clsSaving_Tran
        objEmployee = New clsEmployee_Master
        objSavingScheme = New clsSavingScheme
        objStatusTran = New clsSaving_Status_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call FillCombo()

            objEmployeeSaving._Savingtranunkid = mintTranUnkid

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objEmployeeSaving._Employeeunkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objEmployeeSaving._Employeeunkid
            'S.SANDEEP [04 JUN 2015] -- END

            objSavingScheme._Savingschemeunkid = objEmployeeSaving._Savingschemeunkid
            Call Fill_Info()


            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            FillHistory()
            cboPeriod.Select()
            'Pinkal (12 Jan 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSavingStatus_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatus_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmployeeSaving = Nothing
    End Sub

    Private Sub frmStatus_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmStatus_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm._Other_ModuleNames = ""
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim dtpStatus As Date = Nothing
        Try

            'Select Case CInt(cboStatus.SelectedValue)
            '    Case 3, 4, 5

            '        Dim objPayment As New clsPayment_tran
            '        If objPayment.IsPaymentDone(enPayTypeId.PAYMENT, mintTranUnkid) = False Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot set the selected status for this transaction. Reason : Payment is not done for this transaction."), enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If
            '        objPayment = Nothing

            '        If txtSettlementAmt.Text.Trim = "" Then
            '            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You are about to ") & CStr(cboStatus.Text) & _
            '                               Language.getMessage(mstrModuleName, 3, " selected transaction. Do you want to settle it ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

            '                lnkAddSettlement.Focus()
            '                Exit Sub
            '            End If
            '        End If
            'End Select

            'Sohail (26 Jul 2010) -- Start
            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Status. Status is mandatory information."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Exit Sub
            ElseIf CInt(cboStatus.SelectedValue) = mintStatus Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, This status is already set. Please select different status."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Exit Sub
           'ElseIf mintStatus = 4 And mdblBalAmt = 0 Then '4=Complete
           '     eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sory, You can not change status when it is complete and payment is done."), enMsgBoxStyle.Information)
           '     cboStatus.Focus()
           '     Exit Sub


                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
                'Pinkal (12 Jan 2015) -- End


            Else
                Dim dsList = objStatusTran.GetLastStatus("Status", mintTranUnkid)
                If dsList.Tables("Status").Rows.Count > 0 Then
                    With dsList.Tables("Status").Rows(0)
                        dtpStatus = CDate(.Item("status_date").ToString)
                    End With
                    If dtpStatus.Date > System.DateTime.Today.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Status date should be greater than last change status date : ") & dtpStatus.Date, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
            'Sohail (26 Jul 2010) -- End



            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            Dim dtTable As DataTable = CType(dgvSavingStatusHistory.DataSource, DataTable)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drPeriod As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                If eZeeDate.convertDate(drPeriod("end_date").ToString()).Date < CDate(dtTable.Rows(0)("end_date")).Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Saving status period should be greater than or equal to the Last saving status period. Please set saving status period greater than or equal to the the Last saving status period."), enMsgBoxStyle.Information)
                    cboPeriod.Select()
                    Exit Sub
                End If
            End If
            'Pinkal (12 Jan 2015) -- End

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Dim objTnALeaveTran As New clsTnALeaveTran
            If objTnALeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), objEmployeeSaving._Employeeunkid.ToString, eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date, enModuleReference.Payroll) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You can not Save this Saving Status. Reason : Process Payroll is already done for last date of selected Period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You can not Save this Saving Status. Reason : Process Payroll is already done for last date of selected Period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 10, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If
            'Sohail (12 Jan 2015) -- End


            Call SetValue()

            With objStatusTran
                ._FormName = mstrModuleName
                ._LoginEmployeeUnkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'blnFlag = objStatusTran.Insert
            blnFlag = objStatusTran.Insert(ConfigParameter._Object._CurrentDateAndTime)
            'Shani(24-Aug-2015) -- End


            If blnFlag Then
                'Sohail (26 Jul 2010) -- Start
                'objEmployeeSaving._Savingstatus = CInt(cboStatus.SelectedValue)
                'objEmployeeSaving.Update()
                'Sohail (26 Jul 2010) -- End
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE. DON"T REMOVE THIS METHOD.

    'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '    Try
    '        Dim dtTable As DataTable = CType(dgvSavingStatusHistory.DataSource, DataTable)
    '        Dim drRow() As DataRow = dtTable.Select("status_date = MAX(status_date) AND grp = 0")
    '        If drRow.Length > 0 Then
    '            Dim objperiod As New clscommom_period_Tran
    '            objperiod._Periodunkid = CInt(drRow(0)("periodunkid"))
    '            If objperiod._Statusid = enStatusType.Close Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry,You can't delete this saving status.Reason : This period is already closed."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                Exit Sub
    '            End If
    '            objperiod = Nothing

    '            If CInt(drRow(0)("savingstatustranunkid")) = CInt(dgvSavingStatusHistory.CurrentRow.Cells(objSavingstatustranunkid.Index).Value) Then

    '                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete this saving status ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
    '                    Exit Sub
    '                End If
    '                objStatusTran._Savingtranunkid = mintTranUnkid
    '                objStatusTran._Savingstatustranunkid = CInt(dgvSavingStatusHistory.CurrentRow.Cells(objSavingstatustranunkid.Index).Value)
    '                drRow = Nothing
    '                dtTable = New DataView(dtTable, "savingstatustranunkid <> " & CInt(dgvSavingStatusHistory.CurrentRow.Cells(objSavingstatustranunkid.Index).Value), "", DataViewRowState.CurrentRows).ToTable
    '                drRow = dtTable.Select("status_date = MAX(status_date) AND grp = 0")
    '                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
    '                    objStatusTran._Statusunkid = CInt(drRow(0)("statusunkid"))
    '                End If
    '                objStatusTran._Isvoid = True
    '                objStatusTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                objStatusTran._Voiduserunkid = User._Object._Userunkid
    '                If objStatusTran.Delete() = False Then
    '                    eZeeMsgBox.Show(objStatusTran._Message, enMsgBoxStyle.Information)
    '                Else
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Saving Status deleted successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '                    FillHistory()
    '                    mblnCancel = False
    '                    Me.Close()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub objbtnPeriodSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnPeriodSearch.Click
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet = Nothing
        Try
            With cboPeriod
                objfrm.DataSource = CType(cboPeriod.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnPeriodSearch_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12 Jan 2015) -- End


#End Region

#Region " Controls "
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            'Select Case CInt(cboStatus.SelectedValue)
            '    Case 3, 4
            '        lnkAddSettlement.Enabled = True
            '    Case Else
            '        txtSettlementAmt.Text = ""
            '        lnkAddSettlement.Enabled = False
            'End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

    'Pinkal (12 Jan 2015) -- Start
    'Enhancement - CHANGE IN SAVINGS MODULE.

#Region "DataGrid Event"

    Private Sub dgvSavingStatusHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSavingStatusHistory.DataBindingComplete
        Try
            Call SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSavingStatusHistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    '' DON'T REMOVE THIS METHOD.

    'Private Sub dgvSavingStatusHistory_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvSavingStatusHistory.SelectionChanged
    '    Try
    '        If CBool(dgvSavingStatusHistory.CurrentRow.Cells(objdgcolhIsGroup.Index).Value) = True Then
    '            btnDelete.Enabled = False
    '        Else
    '            Dim dtTable As DataTable = CType(dgvSavingStatusHistory.DataSource, DataTable)

    '            Dim dtTemp As DataTable = New DataView(dtTable, "grp = 0", "", DataViewRowState.CurrentRows).ToTable
    '            If dtTemp IsNot Nothing AndAlso dtTemp.Rows.Count <= 1 Then
    '                btnDelete.Enabled = False
    '                Exit Sub
    '            End If

    '            Dim drRow() As DataRow = dtTable.Select("status_date = MAX(status_date) AND grp = 0")
    '            If drRow.Length > 0 Then
    '                If CInt(drRow(0)("savingstatustranunkid")) = CInt(dgvSavingStatusHistory.CurrentRow.Cells(objSavingstatustranunkid.Index).Value) Then
    '                    btnDelete.Enabled = True
    '                Else
    '                    btnDelete.Enabled = False
    '                End If
    '            Else
    '                btnDelete.Enabled = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvSavingStatusHistory_SelectionChanged", mstrModuleName)
    '    End Try
    'End Sub

#End Region

    'Pinkal (12 Jan 2015) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbSavingStatusInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSavingStatusInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbSavingStatusInfo.Text = Language._Object.getCaption(Me.gbSavingStatusInfo.Name, Me.gbSavingStatusInfo.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblSavingScheme.Text = Language._Object.getCaption(Me.lblSavingScheme.Name, Me.lblSavingScheme.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.dgcolhUser.HeaderText = Language._Object.getCaption(Me.dgcolhUser.Name, Me.dgcolhUser.HeaderText)
			Me.dgcolhTransactionDate.HeaderText = Language._Object.getCaption(Me.dgcolhTransactionDate.Name, Me.dgcolhTransactionDate.HeaderText)
			Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Status. Status is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Sorry, This status is already set. Please select different status.")
			Language.setMessage(mstrModuleName, 3, "Sorry, Status date should be greater than last change status date :")
			Language.setMessage(mstrModuleName, 4, "Please Select Period. Period is mandatory information.")
			Language.setMessage(mstrModuleName, 8, "Saving status period should be greater than or equal to the Last saving status period. Please set saving status period greater than or equal to the the Last saving status period.")
			Language.setMessage(mstrModuleName, 9, "Sorry, You can not Save this Saving Status. Reason : Process Payroll is already done for last date of selected Period.")
			Language.setMessage(mstrModuleName, 10, "Do you want to void Payroll?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'Private Sub frmSavingStatus_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'    Try
'        With cboStatus
'            .Items.Clear()
'            .Items.Add("In Progress")
'            .Items.Add("On Hold")
'            .Items.Add("Redemption")
'            .Items.Add("Finished")
'            .SelectedIndex = 0
'        End With
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "frmSavingStatus_AddEdit_Load", mstrModuleName)
'    End Try
'End Sub

''Private Sub lnklblChangeStatus_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnklblChangeStatus.LinkClicked
''    cboStatus.Enabled = True
''End Sub

'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'    Me.Close()
'End Sub