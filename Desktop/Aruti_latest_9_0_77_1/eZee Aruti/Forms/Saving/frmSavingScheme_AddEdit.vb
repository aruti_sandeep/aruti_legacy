﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSavingScheme_AddEdit
    Private ReadOnly mstrModuleName As String = "frmSavingScheme_AddEdit"

#Region " Private Variables "
    Private mblnCancel As Boolean = True
    Private objSavingScheme As clsSavingScheme
    Private menAction As enAction = enAction.ADD_ONE
    Private mintSavingSchemeunkid As Integer = -1
    Private mintRedempTypeId As Integer = -1
    Private mintIntRateTypeId As Integer = -1
    Private mdtRedemptionTran As DataTable
    Private mdtInterestRateTran As DataTable
    'Private objRedemptionTran As clsRedemption_tran
    Private objInterestTran As clsInterestRate_tran
    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Private mstrSearchText As String = ""
    'Sohail (14 Mar 2019) -- End
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal intUnkId As Integer, ByVal enAction As enAction) As Boolean

        Try
            mintSavingSchemeunkid = intUnkId
            menAction = enAction

            Me.ShowDialog()

            intUnkId = mintSavingSchemeunkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtMinContribution.BackColor = GUI.ColorComp
            txtMinSalary.BackColor = GUI.ColorComp
            'txtMinDepositPeriod.BackColor = GUI.ColorComp
            'txtMaxWithdrawal.BackColor = GUI.ColorComp
            'txtMaxWithdrawAmt.BackColor = GUI.ColorComp
            'txtAdditionalWithdrawCharge.BackColor = GUI.ColorComp
            'txtRedemptionYear.BackColor = GUI.ColorOptional
            'txtRedemptionYear.BackColor = GUI.ColorOptional
            txtInterestRate.BackColor = GUI.ColorOptional
            txtInterestYear.BackColor = GUI.ColorOptional
            txtDefaultContribution.BackColor = GUI.ColorComp
            txtDefualtIntRate.BackColor = GUI.ColorComp
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            cboCostCenter.BackColor = GUI.ColorOptional
            'Sohail (14 Mar 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objSavingScheme._Savingschemecode
            txtName.Text = objSavingScheme._Savingschemename
            txtDescription.Text = objSavingScheme._Description
            txtMinContribution.Text = Format(objSavingScheme._Mincontribution, GUI.fmtCurrency) 'Sohail (11 May 2011)
            txtMinSalary.Text = Format(objSavingScheme._Minnetsalary, GUI.fmtCurrency) 'Sohail (11 May 2011)
            'txtMinDepositPeriod.Text = CStr(objSavingScheme._Mintenure)
            'txtMaxWithdrawal.Text = CStr(objSavingScheme._Maxwithdrawal_Year)
            'txtMaxWithdrawAmt.Text = CStr(objSavingScheme._Maxwithdrawal_Amount)
            'txtAdditionalWithdrawCharge.Text = CStr(objSavingScheme._Additionalwithdrawal_Charge)
            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.          
            txtDefaultContribution.Text = Format(objSavingScheme._DefualtContribution, GUI.fmtCurrency)
            txtDefualtIntRate.Text = Format(objSavingScheme._DefualtIntRate, GUI.fmtCurrency)
            'Shani [ 12 JAN 2015 ] -- END
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            cboCostCenter.SelectedValue = objSavingScheme._Costcenterunkid
            'Sohail (14 Mar 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objSavingScheme._Savingschemecode = txtCode.Text.Trim
            objSavingScheme._Savingschemename = txtName.Text.Trim
            objSavingScheme._Description = txtDescription.Text.Trim
            objSavingScheme._Mincontribution = txtMinContribution.Decimal 'Sohail (11 May 2011)
            objSavingScheme._Minnetsalary = txtMinSalary.Decimal 'Sohail (11 May 2011)
            'objSavingScheme._Mintenure = CInt(txtMinDepositPeriod.Text)
            'objSavingScheme._Maxwithdrawal_Year = cdec(txtMaxWithdrawal.Text)
            'objSavingScheme._Maxwithdrawal_Amount = cdec(txtMaxWithdrawAmt.Text)
            'objSavingScheme._Additionalwithdrawal_Charge = cdec(txtAdditionalWithdrawCharge.Text)
            ' <To Do> logged in user isd has to be kept in this.
            objSavingScheme._Userunkid = User._Object._Userunkid
            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            objSavingScheme._DefualtContribution = txtDefaultContribution.Decimal
            objSavingScheme._DefualtIntRate = txtDefualtIntRate.Decimal
            'Shani [ 12 JAN 2015 ] -- END
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objSavingScheme._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
            'Sohail (14 Mar 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Private Sub FillCombo()
        Dim objCCetnter As New clscostcenter_master
        Dim dsList As New DataSet
        Try
            dsList = objCCetnter.getComboList("List", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboCostCenter)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCCetnter = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 9, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRegularFont", mstrModuleName)
        End Try
    End Sub
    'Sohail (14 Mar 2019) -- End

#End Region

#Region " Form's Events "
    Private Sub frmSavingScheme_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objSavingScheme = Nothing
    End Sub

    Private Sub frmSavingScheme_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSavingScheme_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSavingScheme_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSavingScheme = New clsSavingScheme
        'objRedemptionTran = New clsRedemption_tran
        objInterestTran = New clsInterestRate_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetColor()

            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            Call FillCombo()
            'Sohail (14 Mar 2019) -- End

            If menAction = enAction.EDIT_ONE Then
                objSavingScheme._Savingschemeunkid = mintSavingSchemeunkid
            End If

            Call GetValue()

            'objRedemptionTran._SavingScheme = mintSavingSchemeunkid
            'mdtRedemptionTran = objRedemptionTran._DataList
            'Call FillRedemptionList()

            objInterestTran._SavingScheme = mintSavingSchemeunkid
            mdtInterestRateTran = objInterestTran._DataList
            Call FillInterestRateList()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSavingScheme_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSavingScheme.SetMessages()
            objfrm._Other_ModuleNames = "clsSavingScheme"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
#Region " Combobox Events "

    Private Sub cboCostCenter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCostCenter.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboCostCenter.Name Then
                        .CodeMember = "costcentercode"
                    Else
                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenter_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (14 Mar 2019) -- End

#Region " Button's Events "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Saving Scheme cannot be blank. Saving Scheme is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            'Vimal 28-Aug-2010 --Start

            If Trim(txtMinContribution.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Minimum Contribution cannot be blank. Minimum Contribution is required information."), enMsgBoxStyle.Information)
                txtMinSalary.Focus()
                Exit Sub
            End If
            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If Trim(txtDefaultContribution.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Defualt Contribution cannot be blank. Defualt Contribution is required information."), enMsgBoxStyle.Information)
                txtMinSalary.Focus()
                Exit Sub
            End If
            'Shani [ 12 JAN 2015 ] -- END


            'If Trim(txtMinSalary.Text) = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Minimum Salary cannot be blank. Minimum Salary is required information."), enMsgBoxStyle.Information)
            '    txtMinSalary.Focus()
            '    Exit Sub
            'End If

            'Vimal 28-Aug-2010 --End

            'If cdec(txtMinContribution.Text) >= cdec(txtMinSalary.Text) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Contribution amount should be less than minimum net salary."), enMsgBoxStyle.Information) '?1
            '    txtMinContribution.Focus()
            '    Exit Sub
            'End If

            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If txtMinContribution.Decimal > txtDefaultContribution.Decimal Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Defualt Contribution should be greater than Minimum Contribution."), enMsgBoxStyle.Information)
                txtDefaultContribution.Focus()
                Exit Sub
            End If
            'Shani [ 12 JAN 2015 ] -- END


            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objSavingScheme._FormName = mstrModuleName
            objSavingScheme._LoginEmployeeunkid = 0
            objSavingScheme._ClientIP = getIP()
            objSavingScheme._HostName = getHostName()
            objSavingScheme._FromWeb = False
            objSavingScheme._AuditUserId = User._Object._Userunkid
objSavingScheme._CompanyUnkid = Company._Object._Companyunkid
            objSavingScheme._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objSavingScheme.Update()
            'Else
            '    blnFlag = objSavingScheme.Insert()
            'End If
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objSavingScheme.Update(mdtRedemptionTran, mdtInterestRateTran)
            Else
                blnFlag = objSavingScheme.Insert(mdtRedemptionTran, mdtInterestRateTran)
            End If
            'SHANI [12 JAN 2015]--END
            If blnFlag = False And objSavingScheme._Message <> "" Then
                eZeeMsgBox.Show(objSavingScheme._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objSavingScheme = Nothing
                    objSavingScheme = New clsSavingScheme
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintSavingSchemeunkid = objSavingScheme._Savingschemeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Redemption "
    'Private Sub btnRedemptionDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If mintRedempTypeId > -1 Then
    '            Dim drTemp As DataRow()
    '            If CInt(lvRedemption.Items(mintRedempTypeId).Tag) = -1 Then
    '                drTemp = mdtRedemptionTran.Select("GUID = '" & lvRedemption.Items(mintRedempTypeId).SubItems(objcolhIDGUID.Index).Text & "'")
    '            Else
    '                drTemp = mdtRedemptionTran.Select("savingredemtranunkid = '" & CInt(lvRedemption.Items(mintRedempTypeId).Tag) & "'")
    '            End If
    '            If drTemp.Length > 0 Then
    '                drTemp(0).Item("AUD") = "D"
    '                Call FillRedemptionList()
    '                txtRedemptionCharges.Text = ""
    '                txtRedemptionYear.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnRedemptionDelete_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub btnRedemptionEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvRedemption.SelectedItems.Count > 0 Then
    '            If mintRedempTypeId > -1 Then
    '                Dim drTemp As DataRow()
    '                If CInt(lvRedemption.Items(mintRedempTypeId).Tag) = -1 Then
    '                    drTemp = mdtRedemptionTran.Select("GUID = '" & lvRedemption.Items(mintRedempTypeId).SubItems(objcolhIDGUID.Index).Text & "'")
    '                Else
    '                    drTemp = mdtRedemptionTran.Select("savingredemtranunkid = " & CInt(lvRedemption.Items(mintRedempTypeId).Tag))
    'End If

    '                'Dim isDefault As DataRow() = mdtIdTran.Select("isdefault =" & True)

    '                'If isDefault.Length > 0 Then
    '                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "There can be only one default Identity. Please remove that if you want to add new default Identity."), enMsgBoxStyle.Information)
    '                '    Exit Sub
    '                'End If

    '                If drTemp.Length > 0 Then
    '                    With drTemp(0)
    '                        .Item("savingredemtranunkid") = lvRedemption.Items(mintRedempTypeId).Tag
    '                        .Item("savingschemeunkid") = mintSavingSchemeunkid
    '                        .Item("yearsupto") = CInt(txtRedemptionYear.Text)
    '                        .Item("charges") = txtRedemptionCharges.Text
    '                        .Item("userunkid") = 1
    '                        .Item("isvoid") = False
    '                        .Item("voiduserunkid") = 0
    '                        '.Item("voiddatetime") = Nothing
    '                        .Item("GUID") = Guid.NewGuid().ToString
    '                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
    '                            .Item("AUD") = "U"
    '                        End If
    '                        .AcceptChanges()
    '                    End With
    '                    Call FillRedemptionList()
    '                End If
    '            End If
    '            Call ResetRedemption()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnRedemptionEdit_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub btnRedemptionAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Trim(txtRedemptionYear.Text) = "" Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Redemption Year is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
    '        Exit Sub
    '    End If

    '    If Trim(txtRedemptionCharges.Text) = "" Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Redemption Charge is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
    '        Exit Sub
    '    End If

    '    Dim dtRow As DataRow() = mdtRedemptionTran.Select("yearsupto = '" & txtRedemptionYear.Text & "' AND AUD <> 'D' ")

    '    If dtRow.Length > 0 Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Selected Redemption is already added to the list."), enMsgBoxStyle.Information)
    '    Exit Sub
    'End If

    '    'Dim isDefault As DataRow() = mdtIdTran.Select("isdefault = " & True)

    '    'If isDefault.Length > 0 Then
    '    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "There can be only one default Identity. Please remove that if you want to add new default Identity."), enMsgBoxStyle.Information)
    '    '    Exit Sub
    '    'End If


    '    Try
    '        Dim dtIdRow As DataRow

    '        dtIdRow = mdtRedemptionTran.NewRow

    '        dtIdRow.Item("savingredemtranunkid") = -1
    '        dtIdRow.Item("savingschemeunkid") = mintSavingSchemeunkid
    '        dtIdRow.Item("yearsupto") = CInt(txtRedemptionYear.Text)
    '        dtIdRow.Item("charges") = txtRedemptionCharges.Text
    '        dtIdRow.Item("userunkid") = 1
    '        dtIdRow.Item("GUID") = Guid.NewGuid().ToString
    '        dtIdRow.Item("AUD") = "A"
    '        dtIdRow.Item("isvoid") = False
    '        dtIdRow.Item("voiduserunkid") = 0
    '        'dtIdRow.Item("voiddatetime") = Nothing
    '        mdtRedemptionTran.Rows.Add(dtIdRow)

    '        Call FillRedemptionList()
    '        Call ResetRedemption()

    '        txtRedemptionYear.Focus()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnRedemptionAdd_Click", mstrModuleName)
    '    End Try

    'End Sub

    'Private Sub lvRedemptionInformation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvRedemption.SelectedItems.Count > 0 Then
    '            mintRedempTypeId = lvRedemption.SelectedItems(0).Index

    '            txtRedemptionCharges.Text = lvRedemption.SelectedItems(0).SubItems(colhRedempCharges.Index).Text
    '            txtRedemptionYear.Text = lvRedemption.SelectedItems(0).SubItems(colhRedempYearsUpto.Index).Text
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvRedemptionInformation_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub FillRedemptionList()
    '    Try
    '        lvRedemption.Items.Clear()
    '        Dim lvRedemptionInformations As ListViewItem
    '        For Each dtRow As DataRow In mdtRedemptionTran.Rows
    '            If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
    '                lvRedemptionInformations = New ListViewItem


    '                lvRedemptionInformations.Text = dtRow.Item("yearsupto").ToString
    '                lvRedemptionInformations.SubItems.Add(dtRow.Item("charges").ToString)
    '                lvRedemptionInformations.SubItems.Add(dtRow.Item("GUID").ToString)
    '                lvRedemptionInformations.Tag = dtRow.Item("savingredemtranunkid")

    '                lvRedemption.Items.Add(lvRedemptionInformations)
    '                lvRedemptionInformations = Nothing
    '            End If
    '        Next
    '        'lvRedemption.Columns(0).TextAlign = HorizontalAlignment.Right
    '        ' lvRedemption.Columns(1).TextAlign = HorizontalAlignment.Left
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillRedemptionList", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub ResetRedemption()
    '    Try
    '        'txtRedemptionCharges.Text = ""
    '        'txtRedemptionYear.Text = ""
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ResetRedemption", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Interest Rate "

    Private Sub btnInterestRateDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInterestDelete.Click

        Try
            'Vimal 28-Aug-2010 --Start
            If lvInterest.SelectedItems.Count > 0 Then
                'Vimal 28-Aug-2010 --End  
                If mintIntRateTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvInterest.Items(mintIntRateTypeId).Tag) = -1 Then
                        drTemp = mdtInterestRateTran.Select("GUID = '" & lvInterest.Items(mintIntRateTypeId).SubItems(objcolhIntGUID.Index).Text & "'")
                    Else
                        drTemp = mdtInterestRateTran.Select("savinginteresttranunkid ='" & CInt(lvInterest.SelectedItems(0).Tag) & "'")
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        Call FillInterestRateList()
                        txtInterestRate.Text = ""
                        txtInterestYear.Text = ""
                    End If
                End If
            End If

            If lvInterest.Items.Count > 4 Then
                colhInterestRate.Width = 160 - 18
            Else
                colhInterestRate.Width = 160
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnInterestRateDelete_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnInterestRateEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInterestEdit.Click

        Try
            If lvInterest.SelectedItems.Count > 0 Then
                If mintIntRateTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvInterest.Items(mintIntRateTypeId).Tag) = -1 Then
                        drTemp = mdtInterestRateTran.Select("GUID = '" & lvInterest.Items(mintIntRateTypeId).SubItems(objcolhIntGUID.Index).Text & "'")
                    Else
                        drTemp = mdtInterestRateTran.Select("savinginteresttranunkid ='" & CInt(lvInterest.SelectedItems(0).Tag) & "'")
                    End If
                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("savinginteresttranunkid") = lvInterest.Items(mintIntRateTypeId).Tag
                            .Item("savingschemeunkid") = mintSavingSchemeunkid
                            .Item("yearsupto") = CInt(txtInterestYear.Text)
                            .Item("interestrate") = txtInterestRate.Text
                            .Item("userunkid") = 1
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            '.Item("voiddatetime") = Nothing
                            .Item("GUID") = Guid.NewGuid().ToString
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillInterestRateList()
                    End If
                End If
                Call ResetInterestRate()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnInterestRateEdit_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnInterestRateAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInterestAdd.Click

        Try
            'If Trim(txtInterestYear.Text) = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Interest Year is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If txtInterestYear.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Interest Year is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If Trim(txtInterestRate.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Interest Rate is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Dim dtRow As DataRow() = mdtInterestRateTran.Select("yearsupto = '" & txtInterestYear.Text & "' AND AUD <> 'D' ")

            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selected Interest Year is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtIntRateRow As DataRow

            dtIntRateRow = mdtInterestRateTran.NewRow

            dtIntRateRow.Item("savinginteresttranunkid") = -1
            dtIntRateRow.Item("savingschemeunkid") = mintSavingSchemeunkid
            dtIntRateRow.Item("yearsupto") = CInt(txtInterestYear.Text)
            dtIntRateRow.Item("interestrate") = CDec(txtInterestRate.Text)
            dtIntRateRow.Item("userunkid") = 1
            dtIntRateRow.Item("GUID") = Guid.NewGuid().ToString
            dtIntRateRow.Item("AUD") = "A"
            dtIntRateRow.Item("isvoid") = False
            dtIntRateRow.Item("voiduserunkid") = 0
            'dtIdRow.Item("voiddatetime") = Nothing
            mdtInterestRateTran.Rows.Add(dtIntRateRow)

            Call FillInterestRateList()
            Call ResetInterestRate()
            txtInterestYear.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnInterestRateAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub lvInterestRateInformation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInterest.Click

        Try
            If lvInterest.SelectedItems.Count > 0 Then
                mintIntRateTypeId = lvInterest.SelectedItems(0).Index

                txtInterestYear.Text = lvInterest.SelectedItems(0).SubItems(colhInterestYear.Index).Text
                txtInterestRate.Text = lvInterest.SelectedItems(0).SubItems(colhInterestRate.Index).Text
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvInterestRateInformation_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub FillInterestRateList()

        Try
            lvInterest.Items.Clear()
            Dim lvInterestInformation As ListViewItem
            For Each dtRow As DataRow In mdtInterestRateTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvInterestInformation = New ListViewItem
                    lvInterestInformation.Text = dtRow.Item("yearsupto").ToString
                    lvInterestInformation.SubItems.Add(dtRow.Item("interestrate").ToString)
                    lvInterestInformation.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvInterestInformation.Tag = dtRow.Item("savinginteresttranunkid")

                    lvInterest.Items.Add(lvInterestInformation)
                    lvInterestInformation = Nothing
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InterestRateList", mstrModuleName)
        End Try

    End Sub

    Private Sub ResetInterestRate()
        Try
            txtInterestRate.Text = ""
            txtInterestYear.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub
#End Region


#Region " Code before redesign of saving on 12-Jan-2015 "
'Public Class frmSavingScheme_AddEdit
'    Private ReadOnly mstrModuleName As String = "frmSavingScheme_AddEdit"

'#Region " Private Variables "
'    Private mblnCancel As Boolean = True
'    Private objSavingScheme As clsSavingScheme
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintSavingSchemeunkid As Integer = -1
'    Private mintRedempTypeId As Integer = -1
'    Private mintIntRateTypeId As Integer = -1
'    Private mdtRedemptionTran As DataTable
'    Private mdtInterestRateTran As DataTable
'    'Private objRedemptionTran As clsRedemption_tran
'    Private objInterestTran As clsInterestRate_tran
'#End Region

'#Region " Display Dialog "
'    Public Function displayDialog(ByVal intUnkId As Integer, ByVal enAction As enAction) As Boolean

'        Try
'            mintSavingSchemeunkid = intUnkId
'            menAction = enAction

'            Me.ShowDialog()

'            intUnkId = mintSavingSchemeunkid

'            Return Not mblnCancel

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try

'    End Function
'#End Region

'#Region " Private Methods "
'    Private Sub SetColor()
'        Try
'            txtCode.BackColor = GUI.ColorComp
'            txtName.BackColor = GUI.ColorComp
'            txtDescription.BackColor = GUI.ColorOptional
'            txtMinContribution.BackColor = GUI.ColorComp
'            txtMinSalary.BackColor = GUI.ColorComp
'            'txtMinDepositPeriod.BackColor = GUI.ColorComp
'            'txtMaxWithdrawal.BackColor = GUI.ColorComp
'            'txtMaxWithdrawAmt.BackColor = GUI.ColorComp
'            'txtAdditionalWithdrawCharge.BackColor = GUI.ColorComp
'            'txtRedemptionYear.BackColor = GUI.ColorOptional
'            'txtRedemptionYear.BackColor = GUI.ColorOptional
'            txtInterestRate.BackColor = GUI.ColorOptional
'            txtInterestYear.BackColor = GUI.ColorOptional


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            txtCode.Text = objSavingScheme._Savingschemecode
'            txtName.Text = objSavingScheme._Savingschemename
'            txtDescription.Text = objSavingScheme._Description
'            txtMinContribution.Text = Format(objSavingScheme._Mincontribution, GUI.fmtCurrency) 'Sohail (11 May 2011)
'            txtMinSalary.Text = Format(objSavingScheme._Minnetsalary, GUI.fmtCurrency) 'Sohail (11 May 2011)
'            'txtMinDepositPeriod.Text = CStr(objSavingScheme._Mintenure)
'            'txtMaxWithdrawal.Text = CStr(objSavingScheme._Maxwithdrawal_Year)
'            'txtMaxWithdrawAmt.Text = CStr(objSavingScheme._Maxwithdrawal_Amount)
'            'txtAdditionalWithdrawCharge.Text = CStr(objSavingScheme._Additionalwithdrawal_Charge)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objSavingScheme._Savingschemecode = txtCode.Text.Trim
'            objSavingScheme._Savingschemename = txtName.Text.Trim
'            objSavingScheme._Description = txtDescription.Text.Trim
'            objSavingScheme._Mincontribution = txtMinContribution.Decimal 'Sohail (11 May 2011)
'            objSavingScheme._Minnetsalary = txtMinSalary.Decimal 'Sohail (11 May 2011)
'            'objSavingScheme._Mintenure = CInt(txtMinDepositPeriod.Text)
'            'objSavingScheme._Maxwithdrawal_Year = cdec(txtMaxWithdrawal.Text)
'            'objSavingScheme._Maxwithdrawal_Amount = cdec(txtMaxWithdrawAmt.Text)
'            'objSavingScheme._Additionalwithdrawal_Charge = cdec(txtAdditionalWithdrawCharge.Text)
'            ' <To Do> logged in user isd has to be kept in this.
'            objSavingScheme._Userunkid = User._Object._Userunkid

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Form's Events "
'    Private Sub frmSavingScheme_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objSavingScheme = Nothing
'    End Sub

'    Private Sub frmSavingScheme_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            Select Case e.KeyCode
'                Case Keys.S
'                    If e.Control = True Then
'                        Call btnSave.PerformClick()
'                    End If
'                Case Keys.Return
'                    SendKeys.Send("{TAB}")
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSavingScheme_AddEdit_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSavingScheme_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objSavingScheme = New clsSavingScheme
'        'objRedemptionTran = New clsRedemption_tran
'        objInterestTran = New clsInterestRate_tran
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            Call SetColor()

'            If menAction = enAction.EDIT_ONE Then
'                objSavingScheme._Savingschemeunkid = mintSavingSchemeunkid
'            End If

'            Call GetValue()

'            'objRedemptionTran._SavingScheme = mintSavingSchemeunkid
'            'mdtRedemptionTran = objRedemptionTran._DataList
'            'Call FillRedemptionList()

'            objInterestTran._SavingScheme = mintSavingSchemeunkid
'            mdtInterestRateTran = objInterestTran._DataList
'            Call FillInterestRateList()

'            txtCode.Focus()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSavingScheme_AddEdit_Load", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 20 AUG 2011 ] -- START
'    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsSavingScheme.SetMessages()
'            objfrm._Other_ModuleNames = "clsSavingScheme"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 20 AUG 2011 ] -- END

'#End Region

'#Region " Button's Events "
'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If Trim(txtCode.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
'                txtCode.Focus()
'                Exit Sub
'            End If

'            If Trim(txtName.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Saving Scheme cannot be blank. Saving Scheme is required information."), enMsgBoxStyle.Information) '?1
'                txtName.Focus()
'                Exit Sub
'            End If

'            'Vimal 28-Aug-2010 --Start

'            If Trim(txtMinContribution.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Minimum Contribution cannot be blank. Minimum Contribution is required information."), enMsgBoxStyle.Information)
'                txtMinSalary.Focus()
'                Exit Sub
'            End If

'            'If Trim(txtMinSalary.Text) = "" Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Minimum Salary cannot be blank. Minimum Salary is required information."), enMsgBoxStyle.Information)
'            '    txtMinSalary.Focus()
'            '    Exit Sub
'            'End If

'            'Vimal 28-Aug-2010 --End

'            'If cdec(txtMinContribution.Text) >= cdec(txtMinSalary.Text) Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Contribution amount should be less than minimum net salary."), enMsgBoxStyle.Information) '?1
'            '    txtMinContribution.Focus()
'            '    Exit Sub
'            'End If

'            Call SetValue()

'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objSavingScheme.Update(mdtRedemptionTran, mdtInterestRateTran)
'            Else
'                blnFlag = objSavingScheme.Insert(mdtRedemptionTran, mdtInterestRateTran)
'            End If

'            If blnFlag = False And objSavingScheme._Message <> "" Then
'                eZeeMsgBox.Show(objSavingScheme._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                If menAction = enAction.ADD_CONTINUE Then
'                    objSavingScheme = Nothing
'                    objSavingScheme = New clsSavingScheme
'                    Call GetValue()
'                    txtCode.Focus()
'                Else
'                    mintSavingSchemeunkid = objSavingScheme._Savingschemeunkid
'                    Me.Close()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub
'    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Redemption "
'    'Private Sub btnRedemptionDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'    '    Try
'    '        If mintRedempTypeId > -1 Then
'    '            Dim drTemp As DataRow()
'    '            If CInt(lvRedemption.Items(mintRedempTypeId).Tag) = -1 Then
'    '                drTemp = mdtRedemptionTran.Select("GUID = '" & lvRedemption.Items(mintRedempTypeId).SubItems(objcolhIDGUID.Index).Text & "'")
'    '            Else
'    '                drTemp = mdtRedemptionTran.Select("savingredemtranunkid = '" & CInt(lvRedemption.Items(mintRedempTypeId).Tag) & "'")
'    '            End If
'    '            If drTemp.Length > 0 Then
'    '                drTemp(0).Item("AUD") = "D"
'    '                Call FillRedemptionList()
'    '                txtRedemptionCharges.Text = ""
'    '                txtRedemptionYear.Text = ""
'    '            End If
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "btnRedemptionDelete_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub btnRedemptionEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'    '    Try
'    '        If lvRedemption.SelectedItems.Count > 0 Then
'    '            If mintRedempTypeId > -1 Then
'    '                Dim drTemp As DataRow()
'    '                If CInt(lvRedemption.Items(mintRedempTypeId).Tag) = -1 Then
'    '                    drTemp = mdtRedemptionTran.Select("GUID = '" & lvRedemption.Items(mintRedempTypeId).SubItems(objcolhIDGUID.Index).Text & "'")
'    '                Else
'    '                    drTemp = mdtRedemptionTran.Select("savingredemtranunkid = " & CInt(lvRedemption.Items(mintRedempTypeId).Tag))
'    'End If

'    '                'Dim isDefault As DataRow() = mdtIdTran.Select("isdefault =" & True)

'    '                'If isDefault.Length > 0 Then
'    '                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "There can be only one default Identity. Please remove that if you want to add new default Identity."), enMsgBoxStyle.Information)
'    '                '    Exit Sub
'    '                'End If

'    '                If drTemp.Length > 0 Then
'    '                    With drTemp(0)
'    '                        .Item("savingredemtranunkid") = lvRedemption.Items(mintRedempTypeId).Tag
'    '                        .Item("savingschemeunkid") = mintSavingSchemeunkid
'    '                        .Item("yearsupto") = CInt(txtRedemptionYear.Text)
'    '                        .Item("charges") = txtRedemptionCharges.Text
'    '                        .Item("userunkid") = 1
'    '                        .Item("isvoid") = False
'    '                        .Item("voiduserunkid") = 0
'    '                        '.Item("voiddatetime") = Nothing
'    '                        .Item("GUID") = Guid.NewGuid().ToString
'    '                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
'    '                            .Item("AUD") = "U"
'    '                        End If
'    '                        .AcceptChanges()
'    '                    End With
'    '                    Call FillRedemptionList()
'    '                End If
'    '            End If
'    '            Call ResetRedemption()
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "btnRedemptionEdit_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub btnRedemptionAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'    '    If Trim(txtRedemptionYear.Text) = "" Then
'    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Redemption Year is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
'    '        Exit Sub
'    '    End If

'    '    If Trim(txtRedemptionCharges.Text) = "" Then
'    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Redemption Charge is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
'    '        Exit Sub
'    '    End If

'    '    Dim dtRow As DataRow() = mdtRedemptionTran.Select("yearsupto = '" & txtRedemptionYear.Text & "' AND AUD <> 'D' ")

'    '    If dtRow.Length > 0 Then
'    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Selected Redemption is already added to the list."), enMsgBoxStyle.Information)
'    '    Exit Sub
'    'End If

'    '    'Dim isDefault As DataRow() = mdtIdTran.Select("isdefault = " & True)

'    '    'If isDefault.Length > 0 Then
'    '    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "There can be only one default Identity. Please remove that if you want to add new default Identity."), enMsgBoxStyle.Information)
'    '    '    Exit Sub
'    '    'End If


'    '    Try
'    '        Dim dtIdRow As DataRow

'    '        dtIdRow = mdtRedemptionTran.NewRow

'    '        dtIdRow.Item("savingredemtranunkid") = -1
'    '        dtIdRow.Item("savingschemeunkid") = mintSavingSchemeunkid
'    '        dtIdRow.Item("yearsupto") = CInt(txtRedemptionYear.Text)
'    '        dtIdRow.Item("charges") = txtRedemptionCharges.Text
'    '        dtIdRow.Item("userunkid") = 1
'    '        dtIdRow.Item("GUID") = Guid.NewGuid().ToString
'    '        dtIdRow.Item("AUD") = "A"
'    '        dtIdRow.Item("isvoid") = False
'    '        dtIdRow.Item("voiduserunkid") = 0
'    '        'dtIdRow.Item("voiddatetime") = Nothing
'    '        mdtRedemptionTran.Rows.Add(dtIdRow)

'    '        Call FillRedemptionList()
'    '        Call ResetRedemption()

'    '        txtRedemptionYear.Focus()

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "btnRedemptionAdd_Click", mstrModuleName)
'    '    End Try

'    'End Sub

'    'Private Sub lvRedemptionInformation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
'    '    Try
'    '        If lvRedemption.SelectedItems.Count > 0 Then
'    '            mintRedempTypeId = lvRedemption.SelectedItems(0).Index

'    '            txtRedemptionCharges.Text = lvRedemption.SelectedItems(0).SubItems(colhRedempCharges.Index).Text
'    '            txtRedemptionYear.Text = lvRedemption.SelectedItems(0).SubItems(colhRedempYearsUpto.Index).Text
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "lvRedemptionInformation_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub FillRedemptionList()
'    '    Try
'    '        lvRedemption.Items.Clear()
'    '        Dim lvRedemptionInformations As ListViewItem
'    '        For Each dtRow As DataRow In mdtRedemptionTran.Rows
'    '            If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'    '                lvRedemptionInformations = New ListViewItem


'    '                lvRedemptionInformations.Text = dtRow.Item("yearsupto").ToString
'    '                lvRedemptionInformations.SubItems.Add(dtRow.Item("charges").ToString)
'    '                lvRedemptionInformations.SubItems.Add(dtRow.Item("GUID").ToString)
'    '                lvRedemptionInformations.Tag = dtRow.Item("savingredemtranunkid")

'    '                lvRedemption.Items.Add(lvRedemptionInformations)
'    '                lvRedemptionInformations = Nothing
'    '            End If
'    '        Next
'    '        'lvRedemption.Columns(0).TextAlign = HorizontalAlignment.Right
'    '        ' lvRedemption.Columns(1).TextAlign = HorizontalAlignment.Left
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "FillRedemptionList", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub ResetRedemption()
'    '    Try
'    '        'txtRedemptionCharges.Text = ""
'    '        'txtRedemptionYear.Text = ""
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "ResetRedemption", mstrModuleName)
'    '    End Try
'    'End Sub
'#End Region

'#Region " Interest Rate "

'    Private Sub btnInterestRateDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInterestDelete.Click

'        Try
'            'Vimal 28-Aug-2010 --Start
'            If lvInterest.SelectedItems.Count > 0 Then
'                'Vimal 28-Aug-2010 --End  
'                If mintIntRateTypeId > -1 Then
'                    Dim drTemp As DataRow()
'                    If CInt(lvInterest.Items(mintIntRateTypeId).Tag) = -1 Then
'                        drTemp = mdtInterestRateTran.Select("GUID = '" & lvInterest.Items(mintIntRateTypeId).SubItems(objcolhIntGUID.Index).Text & "'")
'                    Else
'                        drTemp = mdtInterestRateTran.Select("savinginteresttranunkid ='" & CInt(lvInterest.SelectedItems(0).Tag) & "'")
'                    End If
'                    If drTemp.Length > 0 Then
'                        drTemp(0).Item("AUD") = "D"
'                        Call FillInterestRateList()
'                        txtInterestRate.Text = ""
'                        txtInterestYear.Text = ""
'                    End If
'                End If
'            End If

'            If lvInterest.Items.Count > 4 Then
'                colhInterestRate.Width = 160 - 18
'            Else
'                colhInterestRate.Width = 160
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnInterestRateDelete_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnInterestRateEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInterestEdit.Click

'        Try
'            If lvInterest.SelectedItems.Count > 0 Then
'                If mintIntRateTypeId > -1 Then
'                    Dim drTemp As DataRow()
'                    If CInt(lvInterest.Items(mintIntRateTypeId).Tag) = -1 Then
'                        drTemp = mdtInterestRateTran.Select("GUID = '" & lvInterest.Items(mintIntRateTypeId).SubItems(objcolhIntGUID.Index).Text & "'")
'                    Else
'                        drTemp = mdtInterestRateTran.Select("savinginteresttranunkid ='" & CInt(lvInterest.SelectedItems(0).Tag) & "'")
'                    End If
'                    If drTemp.Length > 0 Then
'                        With drTemp(0)
'                            .Item("savinginteresttranunkid") = lvInterest.Items(mintIntRateTypeId).Tag
'                            .Item("savingschemeunkid") = mintSavingSchemeunkid
'                            .Item("yearsupto") = CInt(txtInterestYear.Text)
'                            .Item("interestrate") = txtInterestRate.Text
'                            .Item("userunkid") = 1
'                            .Item("isvoid") = False
'                            .Item("voiduserunkid") = 0
'                            '.Item("voiddatetime") = Nothing
'                            .Item("GUID") = Guid.NewGuid().ToString
'                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
'                                .Item("AUD") = "U"
'                            End If
'                            .AcceptChanges()
'                        End With
'                        Call FillInterestRateList()
'                    End If
'                End If
'                Call ResetInterestRate()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnInterestRateEdit_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnInterestRateAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInterestAdd.Click

'        Try
'            'If Trim(txtInterestYear.Text) = "" Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Interest Year is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
'            '    Exit Sub
'            'End If

'            If txtInterestYear.Decimal <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Interest Year is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            If Trim(txtInterestRate.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Interest Rate is compulsory information, it can not be blank."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If


'            Dim dtRow As DataRow() = mdtInterestRateTran.Select("yearsupto = '" & txtInterestYear.Text & "' AND AUD <> 'D' ")

'            If dtRow.Length > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selected Interest Year is already added to the list."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim dtIntRateRow As DataRow

'            dtIntRateRow = mdtInterestRateTran.NewRow

'            dtIntRateRow.Item("savinginteresttranunkid") = -1
'            dtIntRateRow.Item("savingschemeunkid") = mintSavingSchemeunkid
'            dtIntRateRow.Item("yearsupto") = CInt(txtInterestYear.Text)
'            dtIntRateRow.Item("interestrate") = CDec(txtInterestRate.Text)
'            dtIntRateRow.Item("userunkid") = 1
'            dtIntRateRow.Item("GUID") = Guid.NewGuid().ToString
'            dtIntRateRow.Item("AUD") = "A"
'            dtIntRateRow.Item("isvoid") = False
'            dtIntRateRow.Item("voiduserunkid") = 0
'            'dtIdRow.Item("voiddatetime") = Nothing
'            mdtInterestRateTran.Rows.Add(dtIntRateRow)

'            Call FillInterestRateList()
'            Call ResetInterestRate()
'            txtInterestYear.Focus()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnInterestRateAdd_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub lvInterestRateInformation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInterest.Click

'        Try
'            If lvInterest.SelectedItems.Count > 0 Then
'                mintIntRateTypeId = lvInterest.SelectedItems(0).Index

'                txtInterestYear.Text = lvInterest.SelectedItems(0).SubItems(colhInterestYear.Index).Text
'                txtInterestRate.Text = lvInterest.SelectedItems(0).SubItems(colhInterestRate.Index).Text
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvInterestRateInformation_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub FillInterestRateList()

'        Try
'            lvInterest.Items.Clear()
'            Dim lvInterestInformation As ListViewItem
'            For Each dtRow As DataRow In mdtInterestRateTran.Rows
'                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'                    lvInterestInformation = New ListViewItem
'                    lvInterestInformation.Text = dtRow.Item("yearsupto").ToString
'                    lvInterestInformation.SubItems.Add(dtRow.Item("interestrate").ToString)
'                    lvInterestInformation.SubItems.Add(dtRow.Item("GUID").ToString)
'                    lvInterestInformation.Tag = dtRow.Item("savinginteresttranunkid")

'                    lvInterest.Items.Add(lvInterestInformation)
'                    lvInterestInformation = Nothing
'                End If

'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "InterestRateList", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub ResetInterestRate()
'        Try
'            txtInterestRate.Text = ""
'            txtInterestYear.Text = ""
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try

'    End Sub
'#End Region
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbSavingSchemeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSavingSchemeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbInterestInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInterestInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnInterestEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInterestEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnInterestDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInterestDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnInterestAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInterestAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title" , Me.EZeeHeader.Title)
			Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message" , Me.EZeeHeader.Message)
			Me.gbSavingSchemeInfo.Text = Language._Object.getCaption(Me.gbSavingSchemeInfo.Name, Me.gbSavingSchemeInfo.Text)
			Me.gbInterestInfo.Text = Language._Object.getCaption(Me.gbInterestInfo.Name, Me.gbInterestInfo.Text)
			Me.btnInterestEdit.Text = Language._Object.getCaption(Me.btnInterestEdit.Name, Me.btnInterestEdit.Text)
			Me.btnInterestDelete.Text = Language._Object.getCaption(Me.btnInterestDelete.Name, Me.btnInterestDelete.Text)
			Me.btnInterestAdd.Text = Language._Object.getCaption(Me.btnInterestAdd.Name, Me.btnInterestAdd.Text)
			Me.colhInterestYear.Text = Language._Object.getCaption(CStr(Me.colhInterestYear.Tag), Me.colhInterestYear.Text)
			Me.colhInterestRate.Text = Language._Object.getCaption(CStr(Me.colhInterestRate.Tag), Me.colhInterestRate.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lnEligibility.Text = Language._Object.getCaption(Me.lnEligibility.Name, Me.lnEligibility.Text)
			Me.lblMinMonthlyContrib.Text = Language._Object.getCaption(Me.lblMinMonthlyContrib.Name, Me.lblMinMonthlyContrib.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblMinMonthlySalary.Text = Language._Object.getCaption(Me.lblMinMonthlySalary.Name, Me.lblMinMonthlySalary.Text)
			Me.lblInterestRate.Text = Language._Object.getCaption(Me.lblInterestRate.Name, Me.lblInterestRate.Text)
			Me.lblInterestYear.Text = Language._Object.getCaption(Me.lblInterestYear.Name, Me.lblInterestYear.Text)
			Me.lblDefualtIntRate.Text = Language._Object.getCaption(Me.lblDefualtIntRate.Name, Me.lblDefualtIntRate.Text)
			Me.lblDefaultContribution.Text = Language._Object.getCaption(Me.lblDefaultContribution.Name, Me.lblDefaultContribution.Text)
			Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Saving Scheme cannot be blank. Saving Scheme is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Minimum Contribution cannot be blank. Minimum Contribution is required information.")
			Language.setMessage(mstrModuleName, 4, "Selected Interest Year is already added to the list.")
			Language.setMessage(mstrModuleName, 5, "Interest Year is compulsory information, it can not be blank.")
			Language.setMessage(mstrModuleName, 6, "Interest Rate is compulsory information, it can not be blank.")
			Language.setMessage(mstrModuleName, 7, "Defualt Contribution cannot be blank. Defualt Contribution is required information.")
			Language.setMessage(mstrModuleName, 8, "Sorry, Defualt Contribution should be greater than Minimum Contribution.")
			Language.setMessage(mstrModuleName, 9, "Type to Search")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class