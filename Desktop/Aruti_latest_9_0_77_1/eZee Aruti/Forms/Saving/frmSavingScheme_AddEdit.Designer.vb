﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSavingScheme_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSavingScheme_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.EZeeHeader = New eZee.Common.eZeeHeader
        Me.gbSavingSchemeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblDefaultContribution = New System.Windows.Forms.Label
        Me.txtDefaultContribution = New eZee.TextBox.NumericTextBox
        Me.lblDefualtIntRate = New System.Windows.Forms.Label
        Me.txtDefualtIntRate = New eZee.TextBox.NumericTextBox
        Me.lblMinMonthlySalary = New System.Windows.Forms.Label
        Me.txtMinSalary = New eZee.TextBox.NumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lnEligibility = New eZee.Common.eZeeLine
        Me.txtMinContribution = New eZee.TextBox.NumericTextBox
        Me.lblMinMonthlyContrib = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.gbInterestInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtInterestRate = New eZee.TextBox.NumericTextBox
        Me.txtInterestYear = New eZee.TextBox.NumericTextBox
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.lblInterestRate = New System.Windows.Forms.Label
        Me.lblInterestYear = New System.Windows.Forms.Label
        Me.btnInterestEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlInterestInfo = New System.Windows.Forms.Panel
        Me.lvInterest = New eZee.Common.eZeeListView(Me.components)
        Me.colhInterestYear = New System.Windows.Forms.ColumnHeader
        Me.colhInterestRate = New System.Windows.Forms.ColumnHeader
        Me.objcolhIntGUID = New System.Windows.Forms.ColumnHeader
        Me.btnInterestDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnInterestAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbSavingSchemeInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbInterestInfo.SuspendLayout()
        Me.pnlInterestInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.EZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbSavingSchemeInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(665, 361)
        Me.pnlMainInfo.TabIndex = 0
        '
        'EZeeHeader
        '
        Me.EZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader.Icon = Nothing
        Me.EZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader.Message = ""
        Me.EZeeHeader.Name = "EZeeHeader"
        Me.EZeeHeader.Size = New System.Drawing.Size(665, 60)
        Me.EZeeHeader.TabIndex = 2
        Me.EZeeHeader.Title = "Add / Edit Saving Scheme"
        '
        'gbSavingSchemeInfo
        '
        Me.gbSavingSchemeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbSavingSchemeInfo.Checked = False
        Me.gbSavingSchemeInfo.CollapseAllExceptThis = False
        Me.gbSavingSchemeInfo.CollapsedHoverImage = Nothing
        Me.gbSavingSchemeInfo.CollapsedNormalImage = Nothing
        Me.gbSavingSchemeInfo.CollapsedPressedImage = Nothing
        Me.gbSavingSchemeInfo.CollapseOnLoad = False
        Me.gbSavingSchemeInfo.Controls.Add(Me.cboCostCenter)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblCostCenter)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblDefaultContribution)
        Me.gbSavingSchemeInfo.Controls.Add(Me.txtDefaultContribution)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblDefualtIntRate)
        Me.gbSavingSchemeInfo.Controls.Add(Me.txtDefualtIntRate)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblMinMonthlySalary)
        Me.gbSavingSchemeInfo.Controls.Add(Me.txtMinSalary)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblDescription)
        Me.gbSavingSchemeInfo.Controls.Add(Me.txtDescription)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lnEligibility)
        Me.gbSavingSchemeInfo.Controls.Add(Me.txtMinContribution)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblMinMonthlyContrib)
        Me.gbSavingSchemeInfo.Controls.Add(Me.txtName)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblName)
        Me.gbSavingSchemeInfo.Controls.Add(Me.txtCode)
        Me.gbSavingSchemeInfo.Controls.Add(Me.lblCode)
        Me.gbSavingSchemeInfo.ExpandedHoverImage = Nothing
        Me.gbSavingSchemeInfo.ExpandedNormalImage = Nothing
        Me.gbSavingSchemeInfo.ExpandedPressedImage = Nothing
        Me.gbSavingSchemeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSavingSchemeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSavingSchemeInfo.HeaderHeight = 25
        Me.gbSavingSchemeInfo.HeaderMessage = ""
        Me.gbSavingSchemeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSavingSchemeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSavingSchemeInfo.HeightOnCollapse = 0
        Me.gbSavingSchemeInfo.LeftTextSpace = 0
        Me.gbSavingSchemeInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbSavingSchemeInfo.Name = "gbSavingSchemeInfo"
        Me.gbSavingSchemeInfo.OpenHeight = 300
        Me.gbSavingSchemeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSavingSchemeInfo.ShowBorder = True
        Me.gbSavingSchemeInfo.ShowCheckBox = False
        Me.gbSavingSchemeInfo.ShowCollapseButton = False
        Me.gbSavingSchemeInfo.ShowDefaultBorderColor = True
        Me.gbSavingSchemeInfo.ShowDownButton = False
        Me.gbSavingSchemeInfo.ShowHeader = True
        Me.gbSavingSchemeInfo.Size = New System.Drawing.Size(640, 234)
        Me.gbSavingSchemeInfo.TabIndex = 0
        Me.gbSavingSchemeInfo.Temp = 0
        Me.gbSavingSchemeInfo.Text = "Saving Scheme Information"
        Me.gbSavingSchemeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDefaultContribution
        '
        Me.lblDefaultContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefaultContribution.Location = New System.Drawing.Point(359, 90)
        Me.lblDefaultContribution.Name = "lblDefaultContribution"
        Me.lblDefaultContribution.Size = New System.Drawing.Size(172, 16)
        Me.lblDefaultContribution.TabIndex = 12
        Me.lblDefaultContribution.Text = "Default Contribution"
        Me.lblDefaultContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDefaultContribution
        '
        Me.txtDefaultContribution.AllowNegative = True
        Me.txtDefaultContribution.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDefaultContribution.DigitsInGroup = 0
        Me.txtDefaultContribution.Flags = 0
        Me.txtDefaultContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDefaultContribution.Location = New System.Drawing.Point(537, 88)
        Me.txtDefaultContribution.MaxDecimalPlaces = 6
        Me.txtDefaultContribution.MaxWholeDigits = 21
        Me.txtDefaultContribution.Name = "txtDefaultContribution"
        Me.txtDefaultContribution.Prefix = ""
        Me.txtDefaultContribution.RangeMax = 1.7976931348623157E+308
        Me.txtDefaultContribution.RangeMin = -1.7976931348623157E+308
        Me.txtDefaultContribution.Size = New System.Drawing.Size(90, 21)
        Me.txtDefaultContribution.TabIndex = 5
        Me.txtDefaultContribution.Text = "0"
        Me.txtDefaultContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDefualtIntRate
        '
        Me.lblDefualtIntRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefualtIntRate.Location = New System.Drawing.Point(359, 117)
        Me.lblDefualtIntRate.Name = "lblDefualtIntRate"
        Me.lblDefualtIntRate.Size = New System.Drawing.Size(172, 16)
        Me.lblDefualtIntRate.TabIndex = 10
        Me.lblDefualtIntRate.Text = "Defualt Interest Rate"
        Me.lblDefualtIntRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDefualtIntRate
        '
        Me.txtDefualtIntRate.AllowNegative = True
        Me.txtDefualtIntRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDefualtIntRate.DigitsInGroup = 0
        Me.txtDefualtIntRate.Flags = 0
        Me.txtDefualtIntRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDefualtIntRate.Location = New System.Drawing.Point(537, 115)
        Me.txtDefualtIntRate.MaxDecimalPlaces = 6
        Me.txtDefualtIntRate.MaxWholeDigits = 21
        Me.txtDefualtIntRate.Name = "txtDefualtIntRate"
        Me.txtDefualtIntRate.Prefix = ""
        Me.txtDefualtIntRate.RangeMax = 1.7976931348623157E+308
        Me.txtDefualtIntRate.RangeMin = -1.7976931348623157E+308
        Me.txtDefualtIntRate.Size = New System.Drawing.Size(90, 21)
        Me.txtDefualtIntRate.TabIndex = 6
        Me.txtDefualtIntRate.Text = "0"
        Me.txtDefualtIntRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMinMonthlySalary
        '
        Me.lblMinMonthlySalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinMonthlySalary.Location = New System.Drawing.Point(359, 144)
        Me.lblMinMonthlySalary.Name = "lblMinMonthlySalary"
        Me.lblMinMonthlySalary.Size = New System.Drawing.Size(172, 16)
        Me.lblMinMonthlySalary.TabIndex = 11
        Me.lblMinMonthlySalary.Text = "Minimum Gross Salary"
        Me.lblMinMonthlySalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMinSalary
        '
        Me.txtMinSalary.AllowNegative = True
        Me.txtMinSalary.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinSalary.DigitsInGroup = 0
        Me.txtMinSalary.Flags = 0
        Me.txtMinSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinSalary.Location = New System.Drawing.Point(537, 142)
        Me.txtMinSalary.MaxDecimalPlaces = 6
        Me.txtMinSalary.MaxWholeDigits = 21
        Me.txtMinSalary.Name = "txtMinSalary"
        Me.txtMinSalary.Prefix = ""
        Me.txtMinSalary.RangeMax = 1.7976931348623157E+308
        Me.txtMinSalary.RangeMin = -1.7976931348623157E+308
        Me.txtMinSalary.Size = New System.Drawing.Size(90, 21)
        Me.txtMinSalary.TabIndex = 7
        Me.txtMinSalary.Text = "0"
        Me.txtMinSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 88)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(110, 16)
        Me.lblDescription.TabIndex = 7
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(128, 88)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(212, 102)
        Me.txtDescription.TabIndex = 2
        '
        'lnEligibility
        '
        Me.lnEligibility.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEligibility.Location = New System.Drawing.Point(344, 38)
        Me.lnEligibility.Name = "lnEligibility"
        Me.lnEligibility.Size = New System.Drawing.Size(286, 20)
        Me.lnEligibility.TabIndex = 14
        Me.lnEligibility.Text = "Saving Scheme Eligibility Info"
        '
        'txtMinContribution
        '
        Me.txtMinContribution.AllowNegative = True
        Me.txtMinContribution.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinContribution.DigitsInGroup = 0
        Me.txtMinContribution.Flags = 0
        Me.txtMinContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinContribution.Location = New System.Drawing.Point(537, 61)
        Me.txtMinContribution.MaxDecimalPlaces = 6
        Me.txtMinContribution.MaxWholeDigits = 21
        Me.txtMinContribution.Name = "txtMinContribution"
        Me.txtMinContribution.Prefix = ""
        Me.txtMinContribution.RangeMax = 1.7976931348623157E+308
        Me.txtMinContribution.RangeMin = -1.7976931348623157E+308
        Me.txtMinContribution.Size = New System.Drawing.Size(90, 21)
        Me.txtMinContribution.TabIndex = 4
        Me.txtMinContribution.Text = "0"
        Me.txtMinContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMinMonthlyContrib
        '
        Me.lblMinMonthlyContrib.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinMonthlyContrib.Location = New System.Drawing.Point(359, 63)
        Me.lblMinMonthlyContrib.Name = "lblMinMonthlyContrib"
        Me.lblMinMonthlyContrib.Size = New System.Drawing.Size(172, 16)
        Me.lblMinMonthlyContrib.TabIndex = 13
        Me.lblMinMonthlyContrib.Text = "Monthly Minimum Contribution"
        Me.lblMinMonthlyContrib.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(128, 61)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(212, 21)
        Me.txtName.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 63)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(117, 16)
        Me.lblName.TabIndex = 8
        Me.lblName.Text = "Saving Scheme Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(128, 34)
        Me.txtCode.MaxLength = 255
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(81, 21)
        Me.txtCode.TabIndex = 0
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(110, 16)
        Me.lblCode.TabIndex = 9
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.gbInterestInfo)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 306)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(665, 55)
        Me.objFooter.TabIndex = 1
        '
        'gbInterestInfo
        '
        Me.gbInterestInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInterestInfo.Checked = False
        Me.gbInterestInfo.CollapseAllExceptThis = False
        Me.gbInterestInfo.CollapsedHoverImage = Nothing
        Me.gbInterestInfo.CollapsedNormalImage = Nothing
        Me.gbInterestInfo.CollapsedPressedImage = Nothing
        Me.gbInterestInfo.CollapseOnLoad = False
        Me.gbInterestInfo.Controls.Add(Me.txtInterestRate)
        Me.gbInterestInfo.Controls.Add(Me.txtInterestYear)
        Me.gbInterestInfo.Controls.Add(Me.objelLine3)
        Me.gbInterestInfo.Controls.Add(Me.lblInterestRate)
        Me.gbInterestInfo.Controls.Add(Me.lblInterestYear)
        Me.gbInterestInfo.Controls.Add(Me.btnInterestEdit)
        Me.gbInterestInfo.Controls.Add(Me.pnlInterestInfo)
        Me.gbInterestInfo.Controls.Add(Me.btnInterestDelete)
        Me.gbInterestInfo.Controls.Add(Me.btnInterestAdd)
        Me.gbInterestInfo.ExpandedHoverImage = Nothing
        Me.gbInterestInfo.ExpandedNormalImage = Nothing
        Me.gbInterestInfo.ExpandedPressedImage = Nothing
        Me.gbInterestInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInterestInfo.HeaderHeight = 25
        Me.gbInterestInfo.HeaderMessage = ""
        Me.gbInterestInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInterestInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInterestInfo.HeightOnCollapse = 0
        Me.gbInterestInfo.LeftTextSpace = 0
        Me.gbInterestInfo.Location = New System.Drawing.Point(12, 13)
        Me.gbInterestInfo.Name = "gbInterestInfo"
        Me.gbInterestInfo.OpenHeight = 300
        Me.gbInterestInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInterestInfo.ShowBorder = True
        Me.gbInterestInfo.ShowCheckBox = False
        Me.gbInterestInfo.ShowCollapseButton = False
        Me.gbInterestInfo.ShowDefaultBorderColor = True
        Me.gbInterestInfo.ShowDownButton = False
        Me.gbInterestInfo.ShowHeader = True
        Me.gbInterestInfo.Size = New System.Drawing.Size(147, 30)
        Me.gbInterestInfo.TabIndex = 2
        Me.gbInterestInfo.Temp = 0
        Me.gbInterestInfo.Text = "Interest Information"
        Me.gbInterestInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbInterestInfo.Visible = False
        '
        'txtInterestRate
        '
        Me.txtInterestRate.AllowNegative = True
        Me.txtInterestRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInterestRate.DigitsInGroup = 0
        Me.txtInterestRate.Flags = 0
        Me.txtInterestRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterestRate.Location = New System.Drawing.Point(232, 34)
        Me.txtInterestRate.MaxDecimalPlaces = 6
        Me.txtInterestRate.MaxWholeDigits = 21
        Me.txtInterestRate.Name = "txtInterestRate"
        Me.txtInterestRate.Prefix = ""
        Me.txtInterestRate.RangeMax = 1.7976931348623157E+308
        Me.txtInterestRate.RangeMin = -1.7976931348623157E+308
        Me.txtInterestRate.Size = New System.Drawing.Size(81, 21)
        Me.txtInterestRate.TabIndex = 0
        Me.txtInterestRate.Text = "0"
        Me.txtInterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterestYear
        '
        Me.txtInterestYear.AllowNegative = True
        Me.txtInterestYear.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInterestYear.DigitsInGroup = 0
        Me.txtInterestYear.Flags = 0
        Me.txtInterestYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterestYear.Location = New System.Drawing.Point(75, 34)
        Me.txtInterestYear.MaxDecimalPlaces = 6
        Me.txtInterestYear.MaxWholeDigits = 21
        Me.txtInterestYear.Name = "txtInterestYear"
        Me.txtInterestYear.Prefix = ""
        Me.txtInterestYear.RangeMax = 1.7976931348623157E+308
        Me.txtInterestYear.RangeMin = -1.7976931348623157E+308
        Me.txtInterestYear.Size = New System.Drawing.Size(81, 21)
        Me.txtInterestYear.TabIndex = 2
        Me.txtInterestYear.Text = "0"
        Me.txtInterestYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(7, 60)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(308, 10)
        Me.objelLine3.TabIndex = 4
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblInterestRate
        '
        Me.lblInterestRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestRate.Location = New System.Drawing.Point(165, 36)
        Me.lblInterestRate.Name = "lblInterestRate"
        Me.lblInterestRate.Size = New System.Drawing.Size(61, 16)
        Me.lblInterestRate.TabIndex = 1
        Me.lblInterestRate.Text = "Rate"
        Me.lblInterestRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterestYear
        '
        Me.lblInterestYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestYear.Location = New System.Drawing.Point(8, 36)
        Me.lblInterestYear.Name = "lblInterestYear"
        Me.lblInterestYear.Size = New System.Drawing.Size(61, 16)
        Me.lblInterestYear.TabIndex = 3
        Me.lblInterestYear.Text = "Year"
        Me.lblInterestYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnInterestEdit
        '
        Me.btnInterestEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInterestEdit.BackColor = System.Drawing.Color.White
        Me.btnInterestEdit.BackgroundImage = CType(resources.GetObject("btnInterestEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnInterestEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInterestEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnInterestEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInterestEdit.FlatAppearance.BorderSize = 0
        Me.btnInterestEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInterestEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInterestEdit.ForeColor = System.Drawing.Color.Black
        Me.btnInterestEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInterestEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnInterestEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInterestEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInterestEdit.Location = New System.Drawing.Point(-48, 76)
        Me.btnInterestEdit.Name = "btnInterestEdit"
        Me.btnInterestEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInterestEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInterestEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnInterestEdit.TabIndex = 3
        Me.btnInterestEdit.Text = "&Edit"
        Me.btnInterestEdit.UseVisualStyleBackColor = True
        '
        'pnlInterestInfo
        '
        Me.pnlInterestInfo.Controls.Add(Me.lvInterest)
        Me.pnlInterestInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlInterestInfo.Location = New System.Drawing.Point(3, 112)
        Me.pnlInterestInfo.Name = "pnlInterestInfo"
        Me.pnlInterestInfo.Size = New System.Drawing.Size(319, 122)
        Me.pnlInterestInfo.TabIndex = 5
        '
        'lvInterest
        '
        Me.lvInterest.BackColorOnChecked = True
        Me.lvInterest.ColumnHeaders = Nothing
        Me.lvInterest.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhInterestYear, Me.colhInterestRate, Me.objcolhIntGUID})
        Me.lvInterest.CompulsoryColumns = ""
        Me.lvInterest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvInterest.FullRowSelect = True
        Me.lvInterest.GridLines = True
        Me.lvInterest.GroupingColumn = Nothing
        Me.lvInterest.HideSelection = False
        Me.lvInterest.Location = New System.Drawing.Point(0, 0)
        Me.lvInterest.MinColumnWidth = 50
        Me.lvInterest.MultiSelect = False
        Me.lvInterest.Name = "lvInterest"
        Me.lvInterest.OptionalColumns = ""
        Me.lvInterest.ShowMoreItem = False
        Me.lvInterest.ShowSaveItem = False
        Me.lvInterest.ShowSelectAll = True
        Me.lvInterest.ShowSizeAllColumnsToFit = True
        Me.lvInterest.Size = New System.Drawing.Size(319, 122)
        Me.lvInterest.Sortable = True
        Me.lvInterest.TabIndex = 0
        Me.lvInterest.UseCompatibleStateImageBehavior = False
        Me.lvInterest.View = System.Windows.Forms.View.Details
        '
        'colhInterestYear
        '
        Me.colhInterestYear.Tag = "colhInterestYear"
        Me.colhInterestYear.Text = "Interest for Year"
        Me.colhInterestYear.Width = 150
        '
        'colhInterestRate
        '
        Me.colhInterestRate.Tag = "colhInterestRate"
        Me.colhInterestRate.Text = "Interest Rate (%)"
        Me.colhInterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhInterestRate.Width = 160
        '
        'objcolhIntGUID
        '
        Me.objcolhIntGUID.Tag = "objcolhIntGUID"
        Me.objcolhIntGUID.Text = "objcolhIntGUID"
        Me.objcolhIntGUID.Width = 0
        '
        'btnInterestDelete
        '
        Me.btnInterestDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInterestDelete.BackColor = System.Drawing.Color.White
        Me.btnInterestDelete.BackgroundImage = CType(resources.GetObject("btnInterestDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnInterestDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInterestDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnInterestDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInterestDelete.FlatAppearance.BorderSize = 0
        Me.btnInterestDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInterestDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInterestDelete.ForeColor = System.Drawing.Color.Black
        Me.btnInterestDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInterestDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnInterestDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInterestDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInterestDelete.Location = New System.Drawing.Point(48, 76)
        Me.btnInterestDelete.Name = "btnInterestDelete"
        Me.btnInterestDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInterestDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInterestDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnInterestDelete.TabIndex = 4
        Me.btnInterestDelete.Text = "&Delete"
        Me.btnInterestDelete.UseVisualStyleBackColor = True
        '
        'btnInterestAdd
        '
        Me.btnInterestAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInterestAdd.BackColor = System.Drawing.Color.White
        Me.btnInterestAdd.BackgroundImage = CType(resources.GetObject("btnInterestAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnInterestAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInterestAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnInterestAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInterestAdd.FlatAppearance.BorderSize = 0
        Me.btnInterestAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInterestAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInterestAdd.ForeColor = System.Drawing.Color.Black
        Me.btnInterestAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInterestAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnInterestAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInterestAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInterestAdd.Location = New System.Drawing.Point(-144, 76)
        Me.btnInterestAdd.Name = "btnInterestAdd"
        Me.btnInterestAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInterestAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInterestAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnInterestAdd.TabIndex = 2
        Me.btnInterestAdd.Text = "&Add"
        Me.btnInterestAdd.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(453, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(556, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownWidth = 250
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(128, 196)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(212, 21)
        Me.cboCostCenter.TabIndex = 3
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(11, 198)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(111, 16)
        Me.lblCostCenter.TabIndex = 355
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSavingScheme_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(665, 361)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSavingScheme_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Saving Scheme"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbSavingSchemeInfo.ResumeLayout(False)
        Me.gbSavingSchemeInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.gbInterestInfo.ResumeLayout(False)
        Me.gbInterestInfo.PerformLayout()
        Me.pnlInterestInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents EZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbSavingSchemeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbInterestInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnInterestEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnInterestDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnInterestAdd As eZee.Common.eZeeLightButton
    Friend WithEvents pnlInterestInfo As System.Windows.Forms.Panel
    Friend WithEvents lvInterest As eZee.Common.eZeeListView
    Friend WithEvents colhInterestYear As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterestRate As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnEligibility As eZee.Common.eZeeLine
    Friend WithEvents txtMinContribution As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMinMonthlyContrib As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents txtMinSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMinMonthlySalary As System.Windows.Forms.Label
    Friend WithEvents lblInterestRate As System.Windows.Forms.Label
    Friend WithEvents lblInterestYear As System.Windows.Forms.Label
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents objcolhIntGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtInterestYear As eZee.TextBox.NumericTextBox
    Friend WithEvents txtInterestRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDefualtIntRate As System.Windows.Forms.Label
    Friend WithEvents txtDefualtIntRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDefaultContribution As System.Windows.Forms.Label
    Friend WithEvents txtDefaultContribution As eZee.TextBox.NumericTextBox
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
End Class
