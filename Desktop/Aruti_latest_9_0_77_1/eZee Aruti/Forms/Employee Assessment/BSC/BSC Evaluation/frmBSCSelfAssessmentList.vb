﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSCSelfAssessmentList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBSCSelfAssessmentList"
    Private objSelfBSC As clsBSC_Analysis_Master
    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mStrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim objYear As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objYear.getComboListPAYYEAR("Year", True, , , , True)
            dsFill = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables("Year")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "Period", True, 0)
            dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables("Period")
                .SelectedValue = 0
            End With


            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsFill = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If
            ''Anjan (17 Apr 2012)-End 

            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsFill.Tables("List")
            '    .SelectedValue = 0
            'End With

            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsFill.Dispose() : objEmployee = Nothing : objYear = Nothing : objPeriod = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'btnNew.Enabled = User._Object.Privilege._AddEmployeeAssessment
            'btnEdit.Enabled = User._Object.Privilege._EditEmployeeAssessment
            'btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeAssessment

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)

            'btnNew.Enabled = User._Object.Privilege._AllowToAddSelfBSCAssessment
            'btnEdit.Enabled = User._Object.Privilege._AllowToEditSelfBSCAssessment
            'btnDelete.Enabled = User._Object.Privilege._AllowToDeleteSelfBSCAssessment
            ''S.SANDEEP [ 05 MARCH 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedBSCAssessment
            ''S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [28 MAY 2015] -- END

            
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            chkShowCommited.CheckState = CheckState.Checked
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim lvItem As ListViewItem
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewSelfAssessedBSCList = True Then                'Pinkal (02-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END



            dsList = objSelfBSC.GetList("List", enAssessmentMode.SELF_ASSESSMENT)

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dsPercent As New DataSet
            Dim blnFlag As Boolean = False
            dsPercent = objSelfBSC.Get_Percentage(enAssessmentMode.SELF_ASSESSMENT)
            If dsPercent.Tables(0).Rows.Count > 0 Then
                blnFlag = True
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND selfemployeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            If CInt(cboYear.SelectedValue) > 0 Then
                StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
            End If

            If dtpAssessmentdate.Checked = True Then
                StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
            End If

            If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
                StrSearching &= "AND iscommitted = " & True
            ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
                StrSearching &= "AND iscommitted = " & False
            End If

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mStrAdvanceFilter.Trim.Length > 0 Then
                StrSearching &= "AND " & mStrAdvanceFilter
            End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            lvSelfBSCList.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("YearName").ToString
                lvItem.SubItems.Add(dtRow.Item("PName").ToString)
                lvItem.SubItems(colhPeriod.Index).Tag = dtRow.Item("Sid")
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("assessmentdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                lvItem.Tag = dtRow.Item("analysisunkid")
                If CBool(dtRow.Item("iscommitted")) = True Then
                    lvItem.ForeColor = Color.Blue
                End If

                    'S.SANDEEP [ 04 FEB 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If blnFlag Then
                        Dim dTemp() As DataRow = dsPercent.Tables(0).Select("EmpId = '" & CInt(dtRow.Item("selfemployeeunkid")) & "' AND Pid = '" & CInt(dtRow.Item("periodunkid")) & "'")
                        If dTemp.Length > 0 Then
                            lvItem.SubItems.Add(dTemp(0)("TotalPercent").ToString)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                        Else
                            lvItem.SubItems.Add("")
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                        End If
                    End If
                    'S.SANDEEP [ 04 FEB 2012 ] -- END



                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dtRow.Item("periodunkid").ToString)
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                    'S.SANDEEP [ 03 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dtRow.Item("selfemployeeunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("yearunkid").ToString)
                    'S.SANDEEP [ 03 AUG 2013 ] -- END

                lvSelfBSCList.Items.Add(lvItem)
            Next


            lvSelfBSCList.GroupingColumn = colhEmployee
            lvSelfBSCList.DisplayGroups(True)
            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'colhEmployee.Width = CInt(IIf(colhEmployee.Width > 0, colhEmployee.Width - 20, 0))
            'colhPeriod.Width += colhEmployee.Width
            'colhEmployee.Width = 0
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            lvSelfBSCList.GridLines = False

            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBSCSelfAssessmentList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSelfBSC = New clsBSC_Analysis_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call SetVisibility()

            FillCombo()

            If lvSelfBSCList.Items.Count > 0 Then lvSelfBSCList.Items(0).Selected = True
            lvSelfBSCList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSCSelfAssessmentList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSCSelfAssessmentList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSCSelfAssessmentList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSCSelfAssessmentList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objSelfBSC = Nothing
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmBSC_Evaluation
        Dim frm As New frmBSC_TabularEvaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.SELF_ASSESSMENT) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvSelfBSCList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select at least one Balance Score Card to perform operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(lvSelfBSCList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmBSC_Evaluation
        Dim frm As New frmBSC_TabularEvaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvSelfBSCList.SelectedItems(0).Tag), enAction.EDIT_ONE, enAssessmentMode.SELF_ASSESSMENT) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvSelfBSCList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select at least one Balance Score Card to perform operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(lvSelfBSCList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Assessment?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objSelfBSC._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objSelfBSC._Isvoid = True
                objSelfBSC._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objSelfBSC._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [ 01 JUL 2014 ] -- START
                'objSelfBSC.Delete(CInt(lvSelfBSCList.SelectedItems(0).Tag), enAssessmentMode.APPRAISER_ASSESSMENT)
                objSelfBSC.Delete(CInt(lvSelfBSCList.SelectedItems(0).Tag), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text))
                'S.SANDEEP [ 01 JUL 2014 ] -- END

                If objSelfBSC._Message <> "" Then
                    eZeeMsgBox.Show(objSelfBSC._Message, enMsgBoxStyle.Information)
                Else
                    lvSelfBSCList.SelectedItems(0).Remove()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            dtpAssessmentdate.Checked = False
            chkShowCommited.CheckState = CheckState.Checked
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mStrAdvanceFilter = String.Empty
            'S.SANDEEP [ 05 MARCH 2012 ] -- END
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub



    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub btnMakeCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeCommit.Click
    '    Try
    '        If lvSelfBSCList.CheckedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check at least one information to set as commit."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        For Each LVI As ListViewItem In lvSelfBSCList.CheckedItems
    '            objSelfBSC._Analysisunkid = CInt(LVI.Tag)
    '            objSelfBSC._Iscommitted = True
    '            objSelfBSC.Update()
    '        Next
    '        Call FillList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnMakeCommit_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
        Try
            If lvSelfBSCList.SelectedItems.Count > 0 Then

                'S.SANDEEP [ 03 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim objAnalysis_Master As New clsBSC_Analysis_Master
                If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhYearId.Index).Text), CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhYearId.Index).Text), CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objAnalysis_Master = Nothing
                'S.SANDEEP [ 03 AUG 2013 ] -- END

                If CInt(lvSelfBSCList.SelectedItems(0).SubItems(colhPeriod.Index).Tag) = enStatusType.Open Then
                    'S.SANDEEP [ 10 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objSelfBSC.Unlock_Commit(CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    If objSelfBSC.Unlock_Commit(CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                                CInt(lvSelfBSCList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
                        'S.SANDEEP [ 10 SEPT 2013 ] -- END
                        objSelfBSC._Analysisunkid = CInt(lvSelfBSCList.SelectedItems(0).Tag)
                        objSelfBSC._Iscommitted = False
                        'S.SANDEEP [ 14 JUNE 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        objSelfBSC._Committeddatetime = Nothing
                        'S.SANDEEP [ 14 JUNE 2012 ] -- END
                        objSelfBSC.Update()
                        If objSelfBSC._Message <> "" Then
                            eZeeMsgBox.Show(objSelfBSC._Message)
                        End If
                        Call FillList()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
                Exit Sub
            End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END


    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvSelfBSCList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvSelfBSCList.ItemChecked
        Try
            If lvSelfBSCList.CheckedItems.Count <= 0 Then Exit Sub
            If e.Item.ForeColor = Color.Blue Then e.Item.Checked = False
            If CInt(e.Item.SubItems(colhPeriod.Index).Tag) = enStatusType.Close Then e.Item.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSelfBSCList_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvSelfBSCList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvSelfBSCList.SelectedIndexChanged
        Try
            If lvSelfBSCList.SelectedItems.Count <= 0 Then Exit Sub
            If lvSelfBSCList.SelectedItems(0).ForeColor = Color.Blue Then
                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True 'S.SANDEEP [ 05 MARCH 2012 btnMakeCommit.Enabled = False ] -- START -- END
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False 'S.SANDEEP [ 05 MARCH 2012 btnMakeCommit.Enabled = True ] -- START -- END
            End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedBSCAssessment
            'S.SANDEEP [ 05 MARCH 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssessorList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm.ShowDialog()
            mStrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
			Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
			Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
			Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
			Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
			Me.colhYear.Text = Language._Object.getCaption(CStr(Me.colhYear.Tag), Me.colhYear.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.colhAssessmentdate.Text = Language._Object.getCaption(CStr(Me.colhAssessmentdate.Tag), Me.colhAssessmentdate.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhPercent.Text = Language._Object.getCaption(CStr(Me.colhPercent.Tag), Me.colhPercent.Text)
			Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 3, "Please select at least one Balance Score Card to perform operation on it.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Assessment?")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason : Period is closed.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason : Period is closed.")
			Language.setMessage(mstrModuleName, 8, "Please select at least one Balance Score Card to perform operation on it.")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class