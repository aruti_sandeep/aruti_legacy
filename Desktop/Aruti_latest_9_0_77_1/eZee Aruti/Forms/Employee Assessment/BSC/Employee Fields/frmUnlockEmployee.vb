﻿Option Strict On
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmUnlockEmployee

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmUnlockEmployee"
    Private objUnlock As clsassess_plan_eval_lockunlock
    Private mintTotalCount As Integer = 0
    Private drRow() As DataRow = Nothing
    Private iUnlockComments As String = ""
    Private mdtPeriodEndDate As DateTime
    Private mdtTable As DataTable

#End Region

#Region " Private Method(s) "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboUnlockPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

            dsList = objUnlock.GetLockTypeComboList("List", True)
            With cboLockType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = CType(dgvData.DataSource, DataTable).Select("ischeck = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim strFilter As String = String.Empty
        Try
            If CInt(cboUnlockPeriod.SelectedValue) > 0 Then
                strFilter &= "AND periodunkid = " & CInt(cboUnlockPeriod.SelectedValue)
            End If
            If CInt(cboLockType.SelectedValue) > 0 Then
                strFilter &= "AND locktypeid = " & CInt(cboLockType.SelectedValue)
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            dsList = objUnlock.GetList(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._UserAccessModeSetting, True, False, "List", strFilter)
            Dim dCol As New DataColumn
            With dCol
                .DataType = GetType(Boolean)
                .ColumnName = "ischeck"
                .DefaultValue = False
            End With
            dsList.Tables(0).Columns.Add(dCol)
            dCol = New DataColumn
            With dCol
                .DataType = GetType(String)
                .ColumnName = "message"
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(dCol)

            dgvData.AutoGenerateColumns = False

            objcolhCheck.DataPropertyName = "ischeck"
            colhEmployee.DataPropertyName = "Employee"
            dgcolhLockDate.DataPropertyName = "lockunlockdatetime"
            dgcolhLockDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhGraceDays.DataPropertyName = "udays"
            dgcolhNextLockDate.DataPropertyName = "nextlockdatetime"
            dgcolhNextLockDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            objdgcolhLockUnkid.DataPropertyName = "lockunkid"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgcolhMessage.DataPropertyName = "message"

            mdtTable = dsList.Tables(0).Copy()
            dgvData.DataSource = mdtTable

            objbtnSearch.ShowResult(CStr(dgvData.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer) As Boolean
        Try
            Dim mdtdate As DateTime = CDate(dr(dgcolhLockDate.DataPropertyName)).AddDays(intValue)
            If mdtPeriodEndDate <> Nothing Then
                If mdtdate.Date > mdtPeriodEndDate.Date Then
                    dr(dgcolhMessage.DataPropertyName) = Language.getMessage(mstrModuleName, 5, "Sorry, Next Lock date exceed the period end date.")
                Else
                    dr(dgcolhGraceDays.DataPropertyName) = intValue
                    dr(dgcolhNextLockDate.DataPropertyName) = mdtdate
                    dr(dgcolhMessage.DataPropertyName) = ""
                    dr("unlockdays") = intValue
                End If
            Else
                dr("unlockdays") = intValue
                dr(dgcolhGraceDays.DataPropertyName) = intValue
                dr(dgcolhNextLockDate.DataPropertyName) = mdtdate
                dr(dgcolhMessage.DataPropertyName) = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateRowValue", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmUnlockEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objUnlock = New clsassess_plan_eval_lockunlock
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
            objlblValue.Text = "" : objlblPeriodDates.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUnlockEmployee_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_plan_eval_lockunlock.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_plan_eval_lockunlock"
            objfrm.displayDialog(Me)

            SetMessages()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboUnlockPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please select period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboUnlockPeriod.Focus()
                Exit Sub
            End If

            If CInt(cboLockType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Lock type is compulsory information.Please select Lock type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboLockType.Focus()
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboUnlockPeriod.SelectedValue = 0
            cboLockType.SelectedIndex = 0
            chkSelectAll.Checked = False
            dgvData.DataSource = Nothing
            objlblPeriodDates.Text = ""
            objlblValue.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            If dgvData.RowCount <= 0 Then Exit Sub

            If mdtTable.AsEnumerable.Where(Function(x) x.Field(Of Integer)("unlockdays") > 0).Count() <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You have not set any next locking days for") & " " & _
                                   cboLockType.Text & " " & _
                                   Language.getMessage(mstrModuleName, 7, ", due to this employee will not be locked for planning once it's unlocked for the selected assessment period.") & vbCrLf & _
                                   Language.getMessage(mstrModuleName, 8, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            If mdtTable.AsEnumerable.Where(Function(x) x.Field(Of Boolean)(objcolhCheck.DataPropertyName) = True And x.Field(Of String)(dgcolhMessage.DataPropertyName) = "").Count() <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You have not checked any employee from below list") & " " & _
                                   Language.getMessage(mstrModuleName, 10, ", due to this all employee will be unlocked for the selected assessment period.") & vbCrLf & _
                                   Language.getMessage(mstrModuleName, 8, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            If mdtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objcolhCheck.DataPropertyName) = True).Count() > 0 Then
                drRow = mdtTable.Select(objcolhCheck.DataPropertyName & " = True AND " & dgcolhMessage.DataPropertyName & " = '' ")
            Else
                drRow = mdtTable.Select(dgcolhMessage.DataPropertyName & " = '' ")
            End If

            Me.Cursor = Cursors.WaitCursor

            Application.DoEvents()
            objlblValue.Text = Language.getMessage(mstrModuleName, 11, "Processing : ") & "0/" & drRow.Length.ToString

            Me.SuspendLayout()
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

            objbgWorker.RunWorkerAsync()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dgvData.DataSource Is Nothing Then Exit Sub
            Dim drRow() As DataRow = CType(dgvData.DataSource, DataTable).Select("1=1")
            If drRow.Length > 0 Then
                For Each dr In drRow
                    dr("ischeck") = chkSelectAll.Checked
                    dr.AcceptChanges()
                Next
            End If
            CType(dgvData.DataSource, DataTable).AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagrid Event "

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhCheck.Index Then

                If dgvData.IsCurrentCellDirty Then
                    dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                CType(dgvData.DataSource, DataTable).AcceptChanges()
                SetCheckBoxValue()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboLockType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLockType.SelectedIndexChanged
        Try
            chkSelectAll.Checked = False
            dgvData.DataSource = Nothing
            btnProcess.Enabled = True
            objlblValue.Text = ""
            objbtnSearch.ShowResult(CStr(dgvData.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLockType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnlockPeriod.SelectedIndexChanged
        Try
            If CInt(cboUnlockPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboUnlockPeriod.SelectedValue)
                nudNextDays.Maximum = objPeriod._Constant_Days
                mdtPeriodEndDate = objPeriod._End_Date.Date
                objlblPeriodDates.Text = objPeriod._Start_Date.Date.ToShortDateString() & " - " & objPeriod._End_Date.Date.ToShortDateString()
                objPeriod = Nothing
            Else
                objlblPeriodDates.Text = ""
                mdtPeriodEndDate = Nothing
                nudNextDays.Maximum = 100
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkUpdate_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkUpdate.LinkClicked
        Try
            If nudNextDays.Value <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Next Lock days are mandatory information. Please set the next lock days."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If radApplyToAll.Checked = False AndAlso radApplyTochecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Please select atleast one setting to apply changes for the selected or all employee(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If mdtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objcolhCheck.DataPropertyName) = True).Count() > 0 Then
                drRow = mdtTable.Select(objcolhCheck.DataPropertyName & " = True ")
            Else
                drRow = mdtTable.Select("")
            End If
            drRow.ToList.ForEach(Function(x) UpdateRowValue(x, CInt(nudNextDays.Value)))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkUpdate_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Background Work Event "

    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgWorker.DoWork
        Try
            Me.ControlBox = False
            Me.Enabled = False
            Dim xCount As Integer = 0
            Dim iMsgText As String = ""
            Dim dsList As DataSet = Nothing
            For Each dr As DataRow In drRow
                Try
                    dgvData.FirstDisplayedScrollingRowIndex = CType(dgvData.DataSource, DataTable).Rows.IndexOf(dr) - 5
                    Application.DoEvents()
                Catch ex As Exception
                End Try
                iMsgText = ""
                objUnlock._Lockunkid = dr("lockunkid")
                objUnlock._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
                objUnlock._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                objUnlock._Audittype = enAuditType.EDIT
                objUnlock._Audituserunkid = User._Object._Userunkid
                objUnlock._Form_Name = mstrModuleName
                objUnlock._Hostname = getHostName()
                objUnlock._Ip = getIP()
                objUnlock._Islock = False
                objUnlock._Isunlocktenure = True
                objUnlock._Isweb = False
                If IsDBNull(dr("nextlockdatetime")) = False Then
                    objUnlock._Nextlockdatetime = CDate(dr("nextlockdatetime"))
                Else
                    objUnlock._Nextlockdatetime = Nothing
                End If
                objUnlock._Unlockdays = CInt(dr("unlockdays"))
                objUnlock._Unlockreason = txtRemark.Text
                objUnlock._Unlockuserunkid = User._Object._Userunkid
                If objUnlock.Update() = False Then
                    dr("Message") = objUnlock._Message
                    xCount += 1
                    objlblValue.Text = Language.getMessage(mstrModuleName, 11, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    Continue For
                Else
                    objUnlock.SendUnlockNotification(dr("oName").ToString(), _
                                                     dr("emp_email").ToString(), _
                                                     cboUnlockPeriod.Text, _
                                                     CType(cboLockType.SelectedValue, clsassess_plan_eval_lockunlock.enAssementLockType), _
                                                     Company._Object._Companyunkid, _
                                                     enLogin_Mode.DESKTOP, _
                                                     IIf(User._Object._Firstname & "" & User._Object._Lastname = "", User._Object._Username, User._Object._Firstname & " " & User._Object._Lastname).ToString(), _
                                                     CInt(dr("unlockdays")), _
                                                     objUnlock._Nextlockdatetime, _
                                                     mstrModuleName, _
                                                     getIP(), _
                                                     User._Object._Userunkid, _
                                                     ConfigParameter._Object._CurrentDateAndTime, False, 0, getHostName(), _
                                                     ConfigParameter._Object._ArutiSelfServiceURL)
                    dr("Message") = cboLockType.Text & " " & Language.getMessage(mstrModuleName, 12, "Succesfully.")
                    xCount += 1
                    objlblValue.Text = Language.getMessage(mstrModuleName, 11, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    Continue For
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_DoWork", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgWorker.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            Else
                If objUnlock._Message <> "" Then
                    eZeeMsgBox.Show(objUnlock._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(cboLockType.Text & " " & Language.getMessage(mstrModuleName, 13, "Operation completed successfully."), enMsgBoxStyle.Information)
                    btnProcess.Enabled = False
                End If
                objlblValue.Text = ""
            End If

            Me.ControlBox = True
            Me.Enabled = True
            Me.Cursor = Cursors.Default
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_RunWorkerCompleted", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOperation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOperation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblUnlockPeriod.Text = Language._Object.getCaption(Me.lblUnlockPeriod.Name, Me.lblUnlockPeriod.Text)
            Me.lblLockType.Text = Language._Object.getCaption(Me.lblLockType.Name, Me.lblLockType.Text)
            Me.gbOperation.Text = Language._Object.getCaption(Me.gbOperation.Name, Me.gbOperation.Text)
            Me.lnkUpdate.Text = Language._Object.getCaption(Me.lnkUpdate.Name, Me.lnkUpdate.Text)
            Me.radApplyToAll.Text = Language._Object.getCaption(Me.radApplyToAll.Name, Me.radApplyToAll.Text)
            Me.radApplyTochecked.Text = Language._Object.getCaption(Me.radApplyTochecked.Name, Me.radApplyTochecked.Text)
            Me.lblSetDays.Text = Language._Object.getCaption(Me.lblSetDays.Name, Me.lblSetDays.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.dgcolhLockDate.HeaderText = Language._Object.getCaption(Me.dgcolhLockDate.Name, Me.dgcolhLockDate.HeaderText)
            Me.dgcolhGraceDays.HeaderText = Language._Object.getCaption(Me.dgcolhGraceDays.Name, Me.dgcolhGraceDays.HeaderText)
            Me.dgcolhNextLockDate.HeaderText = Language._Object.getCaption(Me.dgcolhNextLockDate.Name, Me.dgcolhNextLockDate.HeaderText)
            Me.dgcolhMessage.HeaderText = Language._Object.getCaption(Me.dgcolhMessage.Name, Me.dgcolhMessage.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please select period.")
            Language.setMessage(mstrModuleName, 2, "Lock type is compulsory information.Please select Lock type.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Next Lock days are mandatory information. Please set the next lock days.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Please select atleast one setting to apply changes for the selected or all employee(s).")
            Language.setMessage(mstrModuleName, 5, "Sorry, Next Lock date exceed the period end date.")
            Language.setMessage(mstrModuleName, 6, "You have not set any next locking days for")
            Language.setMessage(mstrModuleName, 7, ", due to this employee will not be locked for planning once it's unlocked for the selected assessment period.")
            Language.setMessage(mstrModuleName, 8, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 9, "You have not checked any employee from below list")
            Language.setMessage(mstrModuleName, 10, ", due to this all employee will be unlocked for the selected assessment period.")
            Language.setMessage(mstrModuleName, 11, "Processing :")
            Language.setMessage(mstrModuleName, 12, "Succesfully.")
            Language.setMessage(mstrModuleName, 13, "Operation completed successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class