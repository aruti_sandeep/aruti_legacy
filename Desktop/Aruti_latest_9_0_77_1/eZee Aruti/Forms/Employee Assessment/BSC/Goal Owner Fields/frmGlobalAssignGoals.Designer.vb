﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalAssignGoals
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalAssignGoals))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbOwnerItems = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlItmes = New System.Windows.Forms.Panel
        Me.txtSearchItems = New System.Windows.Forms.TextBox
        Me.objchkItems = New System.Windows.Forms.CheckBox
        Me.dgvItems = New System.Windows.Forms.DataGridView
        Me.gbOwnerData = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeGradientButton
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.dgvOwner = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchOwner = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblOwner = New System.Windows.Forms.Label
        Me.cboOwner = New System.Windows.Forms.ComboBox
        Me.txtPeriod = New System.Windows.Forms.TextBox
        Me.txtGoalOwner = New System.Windows.Forms.TextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlnkCaption = New System.Windows.Forms.LinkLabel
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objdgcolhfcheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objgbOwnerItems.SuspendLayout()
        Me.pnlItmes.SuspendLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbOwnerData.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        CType(Me.dgvOwner, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbDetails.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbOwnerItems)
        Me.pnlMain.Controls.Add(Me.gbOwnerData)
        Me.pnlMain.Controls.Add(Me.gbDetails)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1018, 572)
        Me.pnlMain.TabIndex = 0
        '
        'objgbOwnerItems
        '
        Me.objgbOwnerItems.BorderColor = System.Drawing.Color.Black
        Me.objgbOwnerItems.Checked = False
        Me.objgbOwnerItems.CollapseAllExceptThis = False
        Me.objgbOwnerItems.CollapsedHoverImage = Nothing
        Me.objgbOwnerItems.CollapsedNormalImage = Nothing
        Me.objgbOwnerItems.CollapsedPressedImage = Nothing
        Me.objgbOwnerItems.CollapseOnLoad = False
        Me.objgbOwnerItems.Controls.Add(Me.pnlItmes)
        Me.objgbOwnerItems.ExpandedHoverImage = Nothing
        Me.objgbOwnerItems.ExpandedNormalImage = Nothing
        Me.objgbOwnerItems.ExpandedPressedImage = Nothing
        Me.objgbOwnerItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbOwnerItems.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbOwnerItems.HeaderHeight = 25
        Me.objgbOwnerItems.HeaderMessage = ""
        Me.objgbOwnerItems.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbOwnerItems.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbOwnerItems.HeightOnCollapse = 0
        Me.objgbOwnerItems.LeftTextSpace = 0
        Me.objgbOwnerItems.Location = New System.Drawing.Point(318, 2)
        Me.objgbOwnerItems.Name = "objgbOwnerItems"
        Me.objgbOwnerItems.OpenHeight = 300
        Me.objgbOwnerItems.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbOwnerItems.ShowBorder = True
        Me.objgbOwnerItems.ShowCheckBox = False
        Me.objgbOwnerItems.ShowCollapseButton = False
        Me.objgbOwnerItems.ShowDefaultBorderColor = True
        Me.objgbOwnerItems.ShowDownButton = False
        Me.objgbOwnerItems.ShowHeader = True
        Me.objgbOwnerItems.Size = New System.Drawing.Size(698, 514)
        Me.objgbOwnerItems.TabIndex = 495
        Me.objgbOwnerItems.Temp = 0
        Me.objgbOwnerItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlItmes
        '
        Me.pnlItmes.Controls.Add(Me.objchkItems)
        Me.pnlItmes.Controls.Add(Me.dgvItems)
        Me.pnlItmes.Controls.Add(Me.txtSearchItems)
        Me.pnlItmes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlItmes.Location = New System.Drawing.Point(2, 26)
        Me.pnlItmes.Name = "pnlItmes"
        Me.pnlItmes.Size = New System.Drawing.Size(693, 486)
        Me.pnlItmes.TabIndex = 1
        '
        'txtSearchItems
        '
        Me.txtSearchItems.Location = New System.Drawing.Point(327, 57)
        Me.txtSearchItems.Name = "txtSearchItems"
        Me.txtSearchItems.Size = New System.Drawing.Size(159, 21)
        Me.txtSearchItems.TabIndex = 108
        Me.txtSearchItems.Visible = False
        '
        'objchkItems
        '
        Me.objchkItems.AutoSize = True
        Me.objchkItems.Location = New System.Drawing.Point(7, 6)
        Me.objchkItems.Name = "objchkItems"
        Me.objchkItems.Size = New System.Drawing.Size(15, 14)
        Me.objchkItems.TabIndex = 107
        Me.objchkItems.UseVisualStyleBackColor = True
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToResizeRows = False
        Me.dgvItems.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvItems.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhfcheck})
        Me.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvItems.Location = New System.Drawing.Point(0, 0)
        Me.dgvItems.MultiSelect = False
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.RowHeadersVisible = False
        Me.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvItems.Size = New System.Drawing.Size(693, 486)
        Me.dgvItems.TabIndex = 6
        '
        'gbOwnerData
        '
        Me.gbOwnerData.BorderColor = System.Drawing.Color.Black
        Me.gbOwnerData.Checked = False
        Me.gbOwnerData.CollapseAllExceptThis = False
        Me.gbOwnerData.CollapsedHoverImage = Nothing
        Me.gbOwnerData.CollapsedNormalImage = Nothing
        Me.gbOwnerData.CollapsedPressedImage = Nothing
        Me.gbOwnerData.CollapseOnLoad = False
        Me.gbOwnerData.Controls.Add(Me.lnkAllocation)
        Me.gbOwnerData.Controls.Add(Me.objbtnReset)
        Me.gbOwnerData.Controls.Add(Me.objpnlData)
        Me.gbOwnerData.ExpandedHoverImage = Nothing
        Me.gbOwnerData.ExpandedNormalImage = Nothing
        Me.gbOwnerData.ExpandedPressedImage = Nothing
        Me.gbOwnerData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOwnerData.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOwnerData.HeaderHeight = 25
        Me.gbOwnerData.HeaderMessage = ""
        Me.gbOwnerData.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbOwnerData.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOwnerData.HeightOnCollapse = 0
        Me.gbOwnerData.LeftTextSpace = 0
        Me.gbOwnerData.Location = New System.Drawing.Point(2, 96)
        Me.gbOwnerData.Name = "gbOwnerData"
        Me.gbOwnerData.OpenHeight = 300
        Me.gbOwnerData.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOwnerData.ShowBorder = True
        Me.gbOwnerData.ShowCheckBox = False
        Me.gbOwnerData.ShowCollapseButton = False
        Me.gbOwnerData.ShowDefaultBorderColor = True
        Me.gbOwnerData.ShowDownButton = False
        Me.gbOwnerData.ShowHeader = True
        Me.gbOwnerData.Size = New System.Drawing.Size(313, 420)
        Me.gbOwnerData.TabIndex = 494
        Me.gbOwnerData.Temp = 0
        Me.gbOwnerData.Text = "Assigned To"
        Me.gbOwnerData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(197, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(87, 17)
        Me.lnkAllocation.TabIndex = 491
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnReset.BorderSelected = False
        Me.objbtnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objbtnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnReset.Location = New System.Drawing.Point(289, 2)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnReset.TabIndex = 492
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.objchkEmployee)
        Me.objpnlData.Controls.Add(Me.txtSearchEmp)
        Me.objpnlData.Controls.Add(Me.dgvOwner)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(2, 26)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(309, 392)
        Me.objpnlData.TabIndex = 0
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 27)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtSearchEmp.Location = New System.Drawing.Point(0, 0)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(309, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'dgvOwner
        '
        Me.dgvOwner.AllowUserToAddRows = False
        Me.dgvOwner.AllowUserToDeleteRows = False
        Me.dgvOwner.AllowUserToResizeColumns = False
        Me.dgvOwner.AllowUserToResizeRows = False
        Me.dgvOwner.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvOwner.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvOwner.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvOwner.ColumnHeadersHeight = 21
        Me.dgvOwner.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvOwner.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvOwner.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvOwner.Location = New System.Drawing.Point(0, 22)
        Me.dgvOwner.MultiSelect = False
        Me.dgvOwner.Name = "dgvOwner"
        Me.dgvOwner.RowHeadersVisible = False
        Me.dgvOwner.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOwner.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOwner.Size = New System.Drawing.Size(309, 370)
        Me.dgvOwner.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.Frozen = True
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'gbDetails
        '
        Me.gbDetails.BorderColor = System.Drawing.Color.Black
        Me.gbDetails.Checked = False
        Me.gbDetails.CollapseAllExceptThis = False
        Me.gbDetails.CollapsedHoverImage = Nothing
        Me.gbDetails.CollapsedNormalImage = Nothing
        Me.gbDetails.CollapsedPressedImage = Nothing
        Me.gbDetails.CollapseOnLoad = False
        Me.gbDetails.Controls.Add(Me.objbtnSearch)
        Me.gbDetails.Controls.Add(Me.cboPeriod)
        Me.gbDetails.Controls.Add(Me.objbtnSearchOwner)
        Me.gbDetails.Controls.Add(Me.lblPeriod)
        Me.gbDetails.Controls.Add(Me.lblOwner)
        Me.gbDetails.Controls.Add(Me.cboOwner)
        Me.gbDetails.ExpandedHoverImage = Nothing
        Me.gbDetails.ExpandedNormalImage = Nothing
        Me.gbDetails.ExpandedPressedImage = Nothing
        Me.gbDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDetails.HeaderHeight = 25
        Me.gbDetails.HeaderMessage = ""
        Me.gbDetails.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDetails.HeightOnCollapse = 0
        Me.gbDetails.LeftTextSpace = 0
        Me.gbDetails.Location = New System.Drawing.Point(2, 2)
        Me.gbDetails.Name = "gbDetails"
        Me.gbDetails.OpenHeight = 300
        Me.gbDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDetails.ShowBorder = True
        Me.gbDetails.ShowCheckBox = False
        Me.gbDetails.ShowCollapseButton = False
        Me.gbDetails.ShowDefaultBorderColor = True
        Me.gbDetails.ShowDownButton = False
        Me.gbDetails.ShowHeader = True
        Me.gbDetails.Size = New System.Drawing.Size(313, 92)
        Me.gbDetails.TabIndex = 493
        Me.gbDetails.Temp = 0
        Me.gbDetails.Text = "Goal Owner And Period Detail"
        Me.gbDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearch.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(289, 2)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 493
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(72, 61)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(211, 21)
        Me.cboPeriod.TabIndex = 478
        '
        'objbtnSearchOwner
        '
        Me.objbtnSearchOwner.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOwner.BorderSelected = False
        Me.objbtnSearchOwner.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOwner.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOwner.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOwner.Location = New System.Drawing.Point(289, 34)
        Me.objbtnSearchOwner.Name = "objbtnSearchOwner"
        Me.objbtnSearchOwner.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOwner.TabIndex = 483
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(58, 17)
        Me.lblPeriod.TabIndex = 482
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOwner
        '
        Me.lblOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOwner.Location = New System.Drawing.Point(8, 36)
        Me.lblOwner.Name = "lblOwner"
        Me.lblOwner.Size = New System.Drawing.Size(58, 17)
        Me.lblOwner.TabIndex = 480
        Me.lblOwner.Text = "Owner"
        Me.lblOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOwner
        '
        Me.cboOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwner.DropDownWidth = 200
        Me.cboOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwner.FormattingEnabled = True
        Me.cboOwner.Location = New System.Drawing.Point(72, 34)
        Me.cboOwner.Name = "cboOwner"
        Me.cboOwner.Size = New System.Drawing.Size(211, 21)
        Me.cboOwner.TabIndex = 482
        '
        'txtPeriod
        '
        Me.txtPeriod.BackColor = System.Drawing.Color.White
        Me.txtPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriod.Location = New System.Drawing.Point(13, 19)
        Me.txtPeriod.Name = "txtPeriod"
        Me.txtPeriod.ReadOnly = True
        Me.txtPeriod.Size = New System.Drawing.Size(30, 21)
        Me.txtPeriod.TabIndex = 483
        Me.txtPeriod.Visible = False
        '
        'txtGoalOwner
        '
        Me.txtGoalOwner.BackColor = System.Drawing.Color.White
        Me.txtGoalOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGoalOwner.Location = New System.Drawing.Point(13, 19)
        Me.txtGoalOwner.Name = "txtGoalOwner"
        Me.txtGoalOwner.ReadOnly = True
        Me.txtGoalOwner.Size = New System.Drawing.Size(30, 21)
        Me.txtGoalOwner.TabIndex = 481
        Me.txtGoalOwner.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlnkCaption)
        Me.objFooter.Controls.Add(Me.txtPeriod)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.txtGoalOwner)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1018, 55)
        Me.objFooter.TabIndex = 447
        '
        'objlnkCaption
        '
        Me.objlnkCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkCaption.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkCaption.Location = New System.Drawing.Point(603, 20)
        Me.objlnkCaption.Name = "objlnkCaption"
        Me.objlnkCaption.Size = New System.Drawing.Size(203, 16)
        Me.objlnkCaption.TabIndex = 7
        Me.objlnkCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(812, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(912, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objdgcolhfcheck
        '
        Me.objdgcolhfcheck.Frozen = True
        Me.objdgcolhfcheck.HeaderText = ""
        Me.objdgcolhfcheck.Name = "objdgcolhfcheck"
        Me.objdgcolhfcheck.Width = 25
        '
        'frmGlobalAssignGoals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 572)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalAssignGoals"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Assign Goals"
        Me.pnlMain.ResumeLayout(False)
        Me.objgbOwnerItems.ResumeLayout(False)
        Me.pnlItmes.ResumeLayout(False)
        Me.pnlItmes.PerformLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbOwnerData.ResumeLayout(False)
        Me.objpnlData.ResumeLayout(False)
        Me.objpnlData.PerformLayout()
        CType(Me.dgvOwner, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbDetails.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblOwner As System.Windows.Forms.Label
    Friend WithEvents txtGoalOwner As System.Windows.Forms.TextBox
    Friend WithEvents txtPeriod As System.Windows.Forms.TextBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvOwner As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objgbOwnerItems As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbOwnerData As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlItmes As System.Windows.Forms.Panel
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents objchkItems As System.Windows.Forms.CheckBox
    Friend WithEvents txtSearchItems As System.Windows.Forms.TextBox
    Friend WithEvents objlnkCaption As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchOwner As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOwner As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents objdgcolhfcheck As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
