﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSC_ObjectiveAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBSC_ObjectiveAddEdit"
    Private mblnCancel As Boolean = True
    Private objObjective As clsObjective_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintObjectiveUnkid As Integer = -1

    Dim mdecRemain As Decimal = 0
    Dim mdecAssignedWeight As Decimal = 0


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private mintSelectedEmployee As Integer = 0
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [ 30 MAY 2014 ] -- START
    'Dim objWSetting As New clsWeight_Setting(True)
    Dim objWSetting As clsWeight_Setting
    'S.SANDEEP [ 30 MAY 2014 ] -- END

    Dim mDicWeight As New Dictionary(Of Integer, String)
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintObjectiveUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintObjectiveUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub GetRemainingWeight()
    '    Try
    '        If ConfigParameter._Object._IsBSC_ByEmployee Then
    '            objObjective.RemainingWeight(mdecRemain, mdecAssignedWeight, enAllocation.EMPLOYEE, CInt(cboEmployee.SelectedValue), mintObjectiveUnkid)
    '        Else
    '            objObjective.RemainingWeight(mdecRemain, mdecAssignedWeight, enAllocation.GENERAL, , mintObjectiveUnkid)
    '        End If

    '        If mintObjectiveUnkid > 0 Then
    '            'objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned : ") & " " & (mdecRemain + mdecAssignedWeight).ToString & " " & _
    '            '                    Language.getMessage(mstrModuleName, 7, "%")
    '            If mdecRemain = 100 Then
    '                objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Remaining : ") & " " & (mdecRemain - objObjective._Weight).ToString & " " & _
    '                                Language.getMessage(mstrModuleName, 7, "%")
    '            Else
    '                objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Assigned : ") & " " & (mdecAssignedWeight + objObjective._Weight).ToString & " " & _
    '                                Language.getMessage(mstrModuleName, 7, "%")
    '            End If
    '        Else
    '            objlblCaption.Text = Language.getMessage(mstrModuleName, 6, "Total Remaining : ") & " " & mdecRemain.ToString & " " & _
    '                                 Language.getMessage(mstrModuleName, 7, "%")
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetRemainingWeight", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub Get_General_Weightage()
        Try
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON AndAlso objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                Dim mDecTotal, mDecAssign As Decimal
                mDecTotal = 0 : mDecAssign = 0
                'S.SANDEEP [ 02 NOV 2013 ] -- START
                'objWSetting.Get_Weight_BasedOn(enAllocation.GENERAL, enWeight_Types.WEIGHT_FIELD1, mintObjectiveUnkid, mDecAssign, mDecTotal, , CInt(cboYear.SelectedValue))
                objWSetting.Get_Weight_BasedOn(enAllocation.GENERAL, enWeight_Types.WEIGHT_FIELD1, mintObjectiveUnkid, mDecAssign, mDecTotal, , CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
                'S.SANDEEP [ 02 NOV 2013 ] -- END
                If mDicWeight.ContainsKey(enAllocation.GENERAL) = False Then
                    mDicWeight.Add(enAllocation.GENERAL, mDecTotal.ToString & "|" & mDecAssign.ToString)
                Else
                    mDicWeight(enAllocation.GENERAL) = mDecTotal.ToString & "|" & mDecAssign.ToString
                End If

                If menAction = enAction.EDIT_ONE Then
                    If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                        If mDecTotal - txtWeight.Decimal <= 0 Then
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                        Else
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecTotal - txtWeight.Decimal) & vbCrLf
                        End If
                        objlblCaption.Text &= Language.getMessage(mstrModuleName, 7, "Total Weightage : ") & CStr(mDecTotal)
                    End If
                Else
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 6, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_General_Weightage", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END


    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboPerspective.BackColor = GUI.ColorComp
            cboResultGroup.BackColor = GUI.ColorComp
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboPeriod.BackColor = GUI.ColorComp
            cboYear.BackColor = GUI.ColorComp
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                cboEmployee.BackColor = GUI.ColorComp
            End If
            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objObjective._Code
            txtName.Text = objObjective._Name
            txtDescription.Text = objObjective._Description
            cboPerspective.SelectedValue = objObjective._Perspectiveunkid
            txtWeight.Decimal = objObjective._Weight
            cboResultGroup.SelectedValue = objObjective._ResultGroupunkid


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                cboEmployee.SelectedValue = objObjective._EmployeeId
            Else
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Call GetRemainingWeight()
                If txtWeight.Visible = True Then
                    Call Get_General_Weightage()
                End If
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            End If
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboYear.SelectedValue = objObjective._Yearunkid
            cboPeriod.SelectedValue = objObjective._Periodunkid
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objObjective._Code = txtCode.Text
            objObjective._Name = txtName.Text
            objObjective._Description = txtDescription.Text
            objObjective._Perspectiveunkid = CInt(cboPerspective.SelectedValue)
            objObjective._Weight = CDec(txtWeight.Text)
            objObjective._ResultGroupunkid = CInt(cboResultGroup.SelectedValue)

            If ConfigParameter._Object._IsBSC_ByEmployee Then
                objObjective._EmployeeId = CInt(cboEmployee.SelectedValue)
                objObjective._Referenceunkid = enAllocation.EMPLOYEE
            Else
                objObjective._Referenceunkid = enAllocation.GENERAL
            End If

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objObjective._Yearunkid = CInt(cboYear.SelectedValue)
            objObjective._Periodunkid = CInt(cboPeriod.SelectedValue)
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMaster As New clsMasterData
            Dim objResultGroup As New clsCommon_Master
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objPeriod As New clscommom_period_Tran
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            Dim dsList As New DataSet
            dsList = objMaster.Get_BSC_Perspective("List", True)
            With cboPerspective
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("List")
            End With

            dsList = objResultGroup.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True)
            With cboResultGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With



            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                Dim objEmployee As New clsEmployee_Master
                'Sohail (23 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
                'End If
                ''Sohail (23 Nov 2012) -- End
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsList.Tables(0)
                'End With

                'S.SANDEEP [04 JUN 2015] -- END
            End If
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objMaster.getComboListPAYYEAR("List", True, , , , True)
            dsList = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "List", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select Employee."), enMsgBoxStyle.Information)
                    cboEmployee.Select()
                    Return False
                End If

            End If

            'Pinkal (20-Jan-2012) -- End


            If CInt(cboPerspective.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Perspective is mandatory information. Please select Perspective."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Return False
            End If

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Return False
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Name cannot be blank. Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Return False
            End If

            If CInt(cboResultGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Result Group is mandatory information. Please select Result Group."), enMsgBoxStyle.Information) '?1
                cboResultGroup.Focus()
                Return False
            End If

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If (mdecAssignedWeight + txtWeight.Decimal) > 100 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you can set this weight as it exceeds the total of 100%."), enMsgBoxStyle.Information)
            '    txtWeight.Focus()
            '    Return False
            'End If
            If txtWeight.Visible = True Then
                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Weight is mandatory information. Please give weigth in order to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If
                If ConfigParameter._Object._IsBSC_ByEmployee = True Then
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        If mDicWeight.ContainsKey(CInt(cboEmployee.SelectedValue)) = True Then
                            If CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1))) Then
                                If CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1)) > 0 AndAlso CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1)) <> 100 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                                End If
                                txtWeight.Focus()
                                Return False
                            End If
                        End If
                    End If
                Else
                    If mDicWeight.ContainsKey(enAllocation.GENERAL) = True Then
                        If CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1))) Then
                            If CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1)) > 0 AndAlso CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1)) <> 100 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                            End If
                            txtWeight.Focus()
                            Return False
                        End If
                    End If
                End If
            End If
            If CInt(cboYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Year is mandatory information. Please select Year to continue."), enMsgBoxStyle.Information)
                cboYear.Focus()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If menAction <> enAction.EDIT_ONE Then
                Dim objSTran As New clsobjective_status_tran
                If ConfigParameter._Object._IsBSC_ByEmployee Then
                    If objSTran.isStatusExists(CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry You cannot save this objective." & vbCrLf & _
                                                                                "Reason : There are some objective(s). Which are final saved/submitted for approval for the selected period."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                objSTran = Nothing
            End If
            'S.SANDEEP [ 10 APR 2013 ] -- END

            'S.SHARMA [ 22 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If menAction <> enAction.EDIT_ONE Then
                If objWSetting.Is_FinalSaved(mintObjectiveUnkid, _
                                  CInt(cboEmployee.SelectedValue), _
                                  CInt(cboYear.SelectedValue), _
                                  CInt(cboPeriod.SelectedValue), enWeight_Types.WEIGHT_FIELD1) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot save Objective for the selected period. Reason : Some Objective are final saved for the selected period."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                If menAction = enAction.EDIT_ONE Then
                    Dim sMsg As String = ""
                    sMsg = objWSetting.Is_Valid_Weight(mintObjectiveUnkid, txtWeight.Decimal, enWeight_Types.WEIGHT_FIELD1)
                    If sMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End If
            'S.SHARMA [ 22 JAN 2013 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            'S.SANDEEP [ 30 MAY 2014 ] -- START
            'Dim objWSetting As New clsWeight_Setting(True)
            'Select Case objWSetting._Weight_Optionid
            '    Case enWeight_Options.WEIGHT_EACH_ITEM
            '        txtWeight.Visible = True
            '        objlblCaption.Visible = True
            '        lblWeight.Visible = True
            '    Case enWeight_Options.WEIGHT_BASED_ON
            '        Select Case objWSetting._Weight_Typeid
            '            Case enWeight_Types.WEIGHT_FIELD1
            '                txtWeight.Visible = True
            '                objlblCaption.Visible = True
            '                lblWeight.Visible = True
            '            Case Else
            '                txtWeight.Visible = False
            '                objlblCaption.Visible = False
            '                lblWeight.Visible = False
            '        End Select
            'End Select
            'objWSetting = Nothing
            'S.SANDEEP [ 30 MAY 2014 ] -- END
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmBSC_ObjectiveAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objObjective = Nothing
    End Sub

    Private Sub frmBSC_ObjectiveAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_ObjectiveAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_ObjectiveAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_ObjectiveAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_ObjectiveAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objObjective = New clsObjective_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            Dim p As New Point
            p.X = 3
            If ConfigParameter._Object._IsBSC_ByEmployee = False Then
                p.Y = 28
                pnlObjective.Location = p
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Me.Size = New Size(393, 326)
                Me.Size = New Size(393, 379)
                'S.SANDEEP [ 28 DEC 2012 ] -- END
                cboPerspective.Select()
            Else
                p.Y = 57
                pnlObjective.Location = p
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Me.Size = New Size(393, 354)
                Me.Size = New Size(393, 407)
                'S.SANDEEP [ 28 DEC 2012 ] -- END
                cboEmployee.Select()
            End If

            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objObjective._Objectiveunkid = mintObjectiveUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
            End If

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_ObjectiveAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsObjective_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsObjective_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objObjective.Update()
            Else
                blnFlag = objObjective.Insert()
            End If

            If blnFlag = False And objObjective._Message <> "" Then
                eZeeMsgBox.Show(objObjective._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then

                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes
                If ConfigParameter._Object._IsBSCObjectiveSaved = False Then
                    ConfigParameter._Object._IsBSCObjectiveSaved = True
                    ConfigParameter._Object.updateParam()
                    ConfigParameter._Object.Refresh()
                End If
                'Pinkal (20-Jan-2012) -- End

                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objObjective = Nothing
                    objObjective = New clsObjective_Master
                    Call GetValue()


                    'Pinkal (20-Jan-2012) -- Start
                    'Enhancement : TRA Changes

                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        objObjective.RemainingWeight(mdecRemain, mdecAssignedWeight, enAllocation.EMPLOYEE, CInt(cboEmployee.SelectedValue))
                    Else
                        objObjective.RemainingWeight(mdecRemain, mdecAssignedWeight, enAllocation.GENERAL)
                    End If

                    'Pinkal (20-Jan-2012) -- End    

                    'S.SANDEEP [ 28 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'objlblCaption.Text = Language.getMessage(mstrModuleName, 6, "Total Remaining : ") & " " & mdecRemain.ToString & " " & _
                    '                                             Language.getMessage(mstrModuleName, 7, "%")

                    objlblCaption.Text = Language.getMessage(mstrModuleName, 6, "Total Remaining : ") & " " & mdecRemain.ToString
                    'S.SANDEEP [ 28 DEC 2012 ] -- END

                    txtName.Focus()
                    If mintSelectedEmployee > 0 Then
                        cboEmployee.SelectedValue = mintSelectedEmployee
                    End If
                Else
                    mintObjectiveUnkid = objObjective._Objectiveunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            Call objFrm.displayDialog(txtName.Text, objObjective._Name1, objObjective._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub



    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End


#End Region

#Region " Contols Events "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    Call GetRemainingWeight()
            '    mintSelectedEmployee = CInt(cboEmployee.SelectedValue)
            '    'S.SANDEEP [ 05 MARCH 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            'Else
            '    objlblCaption.Text = ""
            '    'S.SANDEEP [ 05 MARCH 2012 ] -- END
            'End If


            'S.SANDEEP [ 30 MAY 2014 ] -- START
            objWSetting = New clsWeight_Setting(CInt(cboPeriod.SelectedValue))
            Select Case objWSetting._Weight_Optionid
                Case enWeight_Options.WEIGHT_EACH_ITEM
                    txtWeight.Visible = True
                    objlblCaption.Visible = True
                    lblWeight.Visible = True
                Case enWeight_Options.WEIGHT_BASED_ON
                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD1
                            txtWeight.Visible = True
                            objlblCaption.Visible = True
                            lblWeight.Visible = True
                        Case Else
                            txtWeight.Visible = False
                            objlblCaption.Visible = False
                            lblWeight.Visible = False
                    End Select
            End Select
            'S.SANDEEP [ 30 MAY 2014 ] -- END

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mintSelectedEmployee = CInt(cboEmployee.SelectedValue)
                Dim mDecTotal, mDecAssign As Decimal
                mDecTotal = 0 : mDecAssign = 0
                If ConfigParameter._Object._IsBSC_ByEmployee Then
                    'S.SANDEEP [ 02 NOV 2013 ] -- START
                    'objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD1, mintObjectiveUnkid, mDecAssign, mDecTotal, mintSelectedEmployee, CInt(cboYear.SelectedValue))
                    objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD1, mintObjectiveUnkid, mDecAssign, mDecTotal, mintSelectedEmployee, CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
                    'S.SANDEEP [ 02 NOV 2013 ] -- END
                    If mDicWeight.ContainsKey(CInt(cboEmployee.SelectedValue)) = False Then
                        mDicWeight.Add(CInt(cboEmployee.SelectedValue), mDecTotal.ToString & "|" & mDecAssign.ToString)
                    Else
                        mDicWeight(CInt(cboEmployee.SelectedValue)) = mDecTotal.ToString & "|" & mDecAssign.ToString
                    End If
                    If menAction = enAction.EDIT_ONE Then
                        If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                            If mDecTotal - txtWeight.Decimal <= 0 Then
                                objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                            Else
                                objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecAssign) & vbCrLf
                            End If
                            objlblCaption.Text &= Language.getMessage(mstrModuleName, 7, "Total Weightage : ") & CStr(mDecTotal)
                        End If
                    Else
                        objlblCaption.Text = Language.getMessage(mstrModuleName, 6, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                    End If
                End If
            Else
                objlblCaption.Text = ""
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
#Region "ComboBox Event"

    Private Sub cboPerspective_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPerspective.SelectedIndexChanged
        Try
            Dim mintResultGroupunkid As Integer = objObjective.GetResultGroupFromPerspective(CInt(cboPerspective.SelectedValue))
            If mintResultGroupunkid > 0 Then
                cboResultGroup.SelectedValue = mintResultGroupunkid
                cboResultGroup.Enabled = False
            Else
                cboResultGroup.SelectedValue = mintResultGroupunkid
                cboResultGroup.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPerspective_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran
            Dim dsList As New DataSet

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region
    'Pinkal (06-Feb-2012) -- End

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Tab Events "

    Private Sub tabpDescription_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpDescription.Enter
        Try
            objbtnOtherLanguage.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tabpDescription_Enter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tabpObjective_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpObjective.Enter
        Try
            objbtnOtherLanguage.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tabpObjective_Enter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 05 MARCH 2012 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbOjective.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbOjective.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbOjective.Text = Language._Object.getCaption(Me.gbOjective.Name, Me.gbOjective.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)
			Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.tabpObjective.Text = Language._Object.getCaption(Me.tabpObjective.Name, Me.tabpObjective.Text)
			Me.tabpDescription.Text = Language._Object.getCaption(Me.tabpDescription.Name, Me.tabpDescription.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is mandatory information. Please select Employee.")
			Language.setMessage(mstrModuleName, 2, "Perspective is mandatory information. Please select Perspective.")
			Language.setMessage(mstrModuleName, 3, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 4, "Name cannot be blank. Name is required information.")
			Language.setMessage(mstrModuleName, 5, "Result Group is mandatory information. Please select Result Group.")
			Language.setMessage(mstrModuleName, 6, "Total Remaining :")
			Language.setMessage(mstrModuleName, 7, "%")
			Language.setMessage(mstrModuleName, 8, "Assigned :")
			Language.setMessage(mstrModuleName, 9, "Sorry, you can set this weight as it exceeds the total of 100%.")
			Language.setMessage(mstrModuleName, 10, "Remaining :")
			Language.setMessage(mstrModuleName, 11, "Year is mandatory information. Please select Year to continue.")
			Language.setMessage(mstrModuleName, 12, "Period is mandatory information. Please select Period to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class