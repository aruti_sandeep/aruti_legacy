﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_ObjectiveAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_ObjectiveAddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbOjective = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlObjective = New System.Windows.Forms.Panel
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.tabcObjective = New System.Windows.Forms.TabControl
        Me.tabpObjective = New System.Windows.Forms.TabPage
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.tabpDescription = New System.Windows.Forms.TabPage
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.cboPerspective = New System.Windows.Forms.ComboBox
        Me.lblWeight = New System.Windows.Forms.Label
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.lblPerspective = New System.Windows.Forms.Label
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblAssessmentItemCode = New System.Windows.Forms.Label
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objlblCaption = New System.Windows.Forms.RichTextBox
        Me.pnlMain.SuspendLayout()
        Me.gbOjective.SuspendLayout()
        Me.pnlObjective.SuspendLayout()
        Me.tabcObjective.SuspendLayout()
        Me.tabpObjective.SuspendLayout()
        Me.tabpDescription.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbOjective)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(387, 379)
        Me.pnlMain.TabIndex = 1
        '
        'gbOjective
        '
        Me.gbOjective.BorderColor = System.Drawing.Color.Black
        Me.gbOjective.Checked = False
        Me.gbOjective.CollapseAllExceptThis = False
        Me.gbOjective.CollapsedHoverImage = Nothing
        Me.gbOjective.CollapsedNormalImage = Nothing
        Me.gbOjective.CollapsedPressedImage = Nothing
        Me.gbOjective.CollapseOnLoad = False
        Me.gbOjective.Controls.Add(Me.pnlObjective)
        Me.gbOjective.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbOjective.Controls.Add(Me.cboEmployee)
        Me.gbOjective.Controls.Add(Me.lblEmployee)
        Me.gbOjective.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbOjective.ExpandedHoverImage = Nothing
        Me.gbOjective.ExpandedNormalImage = Nothing
        Me.gbOjective.ExpandedPressedImage = Nothing
        Me.gbOjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOjective.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOjective.HeaderHeight = 25
        Me.gbOjective.HeaderMessage = ""
        Me.gbOjective.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOjective.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOjective.HeightOnCollapse = 0
        Me.gbOjective.LeftTextSpace = 0
        Me.gbOjective.Location = New System.Drawing.Point(0, 0)
        Me.gbOjective.Name = "gbOjective"
        Me.gbOjective.OpenHeight = 119
        Me.gbOjective.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOjective.ShowBorder = True
        Me.gbOjective.ShowCheckBox = False
        Me.gbOjective.ShowCollapseButton = False
        Me.gbOjective.ShowDefaultBorderColor = True
        Me.gbOjective.ShowDownButton = False
        Me.gbOjective.ShowHeader = True
        Me.gbOjective.Size = New System.Drawing.Size(387, 324)
        Me.gbOjective.TabIndex = 0
        Me.gbOjective.Temp = 0
        Me.gbOjective.Text = "Objective"
        Me.gbOjective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlObjective
        '
        Me.pnlObjective.Controls.Add(Me.cboPeriod)
        Me.pnlObjective.Controls.Add(Me.lblPeriod)
        Me.pnlObjective.Controls.Add(Me.cboYear)
        Me.pnlObjective.Controls.Add(Me.lblYear)
        Me.pnlObjective.Controls.Add(Me.objbtnOtherLanguage)
        Me.pnlObjective.Controls.Add(Me.tabcObjective)
        Me.pnlObjective.Controls.Add(Me.cboPerspective)
        Me.pnlObjective.Controls.Add(Me.lblWeight)
        Me.pnlObjective.Controls.Add(Me.txtWeight)
        Me.pnlObjective.Controls.Add(Me.lblPerspective)
        Me.pnlObjective.Controls.Add(Me.cboResultGroup)
        Me.pnlObjective.Controls.Add(Me.lblAssessmentItemCode)
        Me.pnlObjective.Controls.Add(Me.lblResultGroup)
        Me.pnlObjective.Controls.Add(Me.txtCode)
        Me.pnlObjective.Location = New System.Drawing.Point(3, 57)
        Me.pnlObjective.Name = "pnlObjective"
        Me.pnlObjective.Size = New System.Drawing.Size(381, 265)
        Me.pnlObjective.TabIndex = 3
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(92, 111)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(260, 21)
        Me.cboPeriod.TabIndex = 17
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(6, 114)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(77, 15)
        Me.lblPeriod.TabIndex = 16
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 200
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(92, 84)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(260, 21)
        Me.cboYear.TabIndex = 15
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(6, 87)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(77, 15)
        Me.lblYear.TabIndex = 14
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(354, 135)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 0
        '
        'tabcObjective
        '
        Me.tabcObjective.Controls.Add(Me.tabpObjective)
        Me.tabcObjective.Controls.Add(Me.tabpDescription)
        Me.tabcObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcObjective.Location = New System.Drawing.Point(8, 138)
        Me.tabcObjective.Name = "tabcObjective"
        Me.tabcObjective.SelectedIndex = 0
        Me.tabcObjective.Size = New System.Drawing.Size(367, 122)
        Me.tabcObjective.TabIndex = 9
        '
        'tabpObjective
        '
        Me.tabpObjective.Controls.Add(Me.txtName)
        Me.tabpObjective.Location = New System.Drawing.Point(4, 22)
        Me.tabpObjective.Name = "tabpObjective"
        Me.tabpObjective.Size = New System.Drawing.Size(359, 96)
        Me.tabpObjective.TabIndex = 0
        Me.tabpObjective.Tag = "tabpObjective"
        Me.tabpObjective.Text = "Objective"
        Me.tabpObjective.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(0, 0)
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtName.Size = New System.Drawing.Size(359, 96)
        Me.txtName.TabIndex = 0
        '
        'tabpDescription
        '
        Me.tabpDescription.Controls.Add(Me.txtDescription)
        Me.tabpDescription.Location = New System.Drawing.Point(4, 22)
        Me.tabpDescription.Name = "tabpDescription"
        Me.tabpDescription.Size = New System.Drawing.Size(359, 96)
        Me.tabpDescription.TabIndex = 1
        Me.tabpDescription.Tag = "tabpDescription"
        Me.tabpDescription.Text = "Description"
        Me.tabpDescription.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(359, 96)
        Me.txtDescription.TabIndex = 12
        '
        'cboPerspective
        '
        Me.cboPerspective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPerspective.FormattingEnabled = True
        Me.cboPerspective.Location = New System.Drawing.Point(93, 3)
        Me.cboPerspective.Name = "cboPerspective"
        Me.cboPerspective.Size = New System.Drawing.Size(258, 21)
        Me.cboPerspective.TabIndex = 1
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(240, 34)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(55, 15)
        Me.lblWeight.TabIndex = 4
        Me.lblWeight.Text = "Weight %"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(301, 30)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 3
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.Size = New System.Drawing.Size(50, 21)
        Me.txtWeight.TabIndex = 5
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPerspective
        '
        Me.lblPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerspective.Location = New System.Drawing.Point(6, 6)
        Me.lblPerspective.Name = "lblPerspective"
        Me.lblPerspective.Size = New System.Drawing.Size(77, 15)
        Me.lblPerspective.TabIndex = 0
        Me.lblPerspective.Text = "Perspective"
        Me.lblPerspective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.DropDownWidth = 200
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Location = New System.Drawing.Point(93, 57)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(259, 21)
        Me.cboResultGroup.TabIndex = 7
        '
        'lblAssessmentItemCode
        '
        Me.lblAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItemCode.Location = New System.Drawing.Point(6, 33)
        Me.lblAssessmentItemCode.Name = "lblAssessmentItemCode"
        Me.lblAssessmentItemCode.Size = New System.Drawing.Size(77, 15)
        Me.lblAssessmentItemCode.TabIndex = 2
        Me.lblAssessmentItemCode.Text = "Code"
        Me.lblAssessmentItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(6, 60)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(81, 15)
        Me.lblResultGroup.TabIndex = 6
        Me.lblResultGroup.Text = "Result Group"
        Me.lblResultGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(93, 30)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(141, 21)
        Me.txtCode.TabIndex = 3
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(360, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(95, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(259, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblCaption)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 324)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(387, 55)
        Me.objFooter.TabIndex = 1
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.SystemColors.Control
        Me.objlblCaption.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.objlblCaption.DetectUrls = False
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblCaption.Location = New System.Drawing.Point(3, 3)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.ReadOnly = True
        Me.objlblCaption.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.objlblCaption.ShortcutsEnabled = False
        Me.objlblCaption.Size = New System.Drawing.Size(169, 48)
        Me.objlblCaption.TabIndex = 9
        Me.objlblCaption.Text = ""
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(178, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(281, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "objdgcolhUnkid"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'frmBSC_ObjectiveAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(387, 379)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_ObjectiveAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Objective"
        Me.pnlMain.ResumeLayout(False)
        Me.gbOjective.ResumeLayout(False)
        Me.pnlObjective.ResumeLayout(False)
        Me.pnlObjective.PerformLayout()
        Me.tabcObjective.ResumeLayout(False)
        Me.tabpObjective.ResumeLayout(False)
        Me.tabpObjective.PerformLayout()
        Me.tabpDescription.ResumeLayout(False)
        Me.tabpDescription.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbOjective As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPerspective As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessmentItemCode As System.Windows.Forms.Label
    Friend WithEvents lblPerspective As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlObjective As System.Windows.Forms.Panel
    Friend WithEvents tabcObjective As System.Windows.Forms.TabControl
    Friend WithEvents tabpObjective As System.Windows.Forms.TabPage
    Friend WithEvents tabpDescription As System.Windows.Forms.TabPage
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents objlblCaption As System.Windows.Forms.RichTextBox
End Class
