﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSC_ObjectiveList

#Region " Private Varaibles "

    Private objObjective As clsObjective_Master
    Private ReadOnly mstrModuleName As String = "frmBSC_ObjectiveList"


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private mintReferenceUnkid As Integer = -1
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim objWSetting As New clsWeight_Setting(True)
    Dim intWidth As Integer = 135
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)

            ''S.SANDEEP [ 10 APR 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ' ''S.SANDEEP [ 05 MARCH 2012 ] -- START
            ' ''ENHANCEMENT : TRA CHANGES
            ''mnuFinalSave.Enabled = User._Object.Privilege._Allow_FinalSaveBSCPlanning
            ''mnuUnlockFinalSave.Enabled = User._Object.Privilege._Allow_UnlockFinalSaveBSCPlanning
            'mnuApproveSubmitted.Enabled = User._Object.Privilege._Allow_FinalSaveBSCPlanning
            'mnuUnlockFinalSave.Enabled = User._Object.Privilege._Allow_UnlockFinalSaveBSCPlanning
            'mnuSubmitApproval.Enabled = User._Object.Privilege._AllowToSubmitBSCPlan_Approval
            ''S.SANDEEP [ 05 MARCH 2012 ] -- END



            ''S.SANDEEP [ 10 APR 2013 ] -- END



            ''S.SANDEEP [ 16 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'btnNew.Enabled = User._Object.Privilege._AllowToAddBSCObjective
            'btnEdit.Enabled = User._Object.Privilege._AllowToEditBSCObjective
            'btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBSCObjective
            ''S.SANDEEP [ 16 MAY 2012 ] -- END

            'S.SANDEEP [28 MAY 2015] -- END
            


            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objWSetting._Weight_Optionid <> enWeight_Options.WEIGHT_EACH_ITEM Then
                If objWSetting._Weight_Typeid <> enWeight_Types.WEIGHT_FIELD1 Then
                    colhWeight.Width = 0
                    intWidth = 195
                    colhDescription.Width = intWidth
                End If                
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._IsBSC_ByEmployee = True Then
                objSep2.Visible = True
                mnuGetFileFormat.Visible = True
                mnuImportBSC_Planning.Visible = True
            Else
                objSep2.Visible = False
                mnuGetFileFormat.Visible = False
                mnuImportBSC_Planning.Visible = False
            End If
            'S.SANDEEP [ 20 JULY 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try

            Dim dsList As New DataSet
            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                Dim objEmployee As New clsEmployee_Master
                'Sohail (23 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
                'End If
                ''Sohail (23 Nov 2012) -- End
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsList.Tables(0)
                'End With
                ''Pinkal (20-Jan-2012) -- End

                'S.SANDEEP [04 JUN 2015] -- END

                Dim objMaster As New clsMasterData
                dsList = objMaster.Get_BSC_Perspective("List", True)
                With cboPerspective
                    .ValueMember = "Id"
                    .DisplayMember = "NAME"
                    .DataSource = dsList.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List()
        Dim strSearching As String = ""
        Dim dsList As New DataSet
        Dim dtTable As New DataTable
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewBSCObjectiveList = True Then          'Pinkal (09-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END





            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            dsList = objObjective.GetList("List", True)

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching &= "AND employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
                End If
            End If

            'Pinkal (20-Jan-2012) -- End


            If CInt(cboPerspective.SelectedValue) > 0 Then
                strSearching &= "AND perspectiveunkid = '" & CInt(cboPerspective.SelectedValue) & "' "
            End If


            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                    'S.SANDEEP [ 31 AUG 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dtTable = New DataView(dsList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable

                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dtTable = New DataView(dsList.Tables("List"), strSearching, "perspectiveunkid,code", DataViewRowState.CurrentRows).ToTable
                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        dtTable = New DataView(dsList.Tables("List"), strSearching, "employee,perspectiveunkid", DataViewRowState.CurrentRows).ToTable
                    Else
                    dtTable = New DataView(dsList.Tables("List"), strSearching, "perspectiveunkid,code", DataViewRowState.CurrentRows).ToTable
                    End If
                    'S.SANDEEP [ 01 DEC 2012 ] -- END

                    'S.SANDEEP [ 31 AUG 2012 ] -- END
            Else
                    'S.SANDEEP [ 31 AUG 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dtTable = dsList.Tables("List")

                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dtTable = New DataView(dsList.Tables("List"), "", "perspectiveunkid,code", DataViewRowState.CurrentRows).ToTable
                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        dtTable = New DataView(dsList.Tables("List"), "", "employee,perspectiveunkid", DataViewRowState.CurrentRows).ToTable
                    Else
                    dtTable = New DataView(dsList.Tables("List"), "", "perspectiveunkid,code", DataViewRowState.CurrentRows).ToTable
                    End If
                    'S.SANDEEP [ 01 DEC 2012 ] -- END

                    'S.SANDEEP [ 31 AUG 2012 ] -- END
            End If

            Dim lvItem As ListViewItem

            lvObjective.Items.Clear()

            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Tag = drRow.Item("objectiveunkid")
                lvItem.Text = drRow.Item("perspective").ToString
                lvItem.SubItems.Add(drRow.Item("code").ToString)
                lvItem.SubItems.Add(drRow.Item("name").ToString)

                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                lvItem.SubItems.Add(drRow.Item("weight").ToString())
                'Pinkal (06-Feb-2012) -- End


                lvItem.SubItems.Add(drRow.Item("description").ToString)
                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        lvItem.SubItems.Add(drRow.Item("employee").ToString & Language.getMessage(mstrModuleName, 7, " -> Period : ") & drRow.Item("Period").ToString)
                    Else
                        lvItem.SubItems.Add(drRow.Item("Period").ToString)
                    End If

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(drRow.Item("employeeunkid").ToString)
                    'S.SANDEEP [ 01 APR 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems(objcolhEmpId.Index).Tag = drRow.Item("employeecode").ToString & "_" & drRow.Item("employee").ToString
                    'S.SANDEEP [ 01 APR 2013 ] -- END

                If CBool(drRow.Item("isfinal")) = True Then
                    lvItem.ForeColor = Color.Blue
                End If
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                    'S.SANDEEP [ 28 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(drRow.Item("periodunkid").ToString)
                    'S.SANDEEP [ 28 DEC 2012 ] -- END


                    'S.SANDEEP [ 01 APR 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(drRow.Item("yearunkid").ToString)
                    'S.SANDEEP [ 01 APR 2013 ] -- END


                    'S.SANDEEP [ 10 APR 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If CInt(drRow.Item("STypId")) = enObjective_Status.SUBMIT_APPROVAL Then
                        lvItem.ForeColor = Color.Green
                    End If
                    'S.SANDEEP [ 10 APR 2013 ] -- END


                lvObjective.Items.Add(lvItem)
            Next

            If ConfigParameter._Object._IsBSC_ByEmployee Then
                lvObjective.GroupingColumn = objcolhEmployee
            Else
                lvObjective.GroupingColumn = colhPerspective
            End If
            lvObjective.DisplayGroups(True)

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If lvObjective.Items.Count > 16 Then
                '    colhDescription.Width = 195 - 18
                'Else
                '    colhDescription.Width = 195
                'End If

            If lvObjective.Items.Count > 16 Then
                    colhDescription.Width = intWidth - 18
            Else
                    colhDescription.Width = intWidth
            End If
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                

            'Pinkal (20-Jan-2012) -- End

            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub OpenAddEdit_Form()
        Try
            Dim frm As New frmBSC_ObjectiveAddEdit
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "OpenAddEdit_Form", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 01 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Export_BSC_Form(ByVal sPath As String, ByVal dsList As DataSet, ByVal sEmployeeName As String)
        Try
            Dim mDicRowSpan As New Dictionary(Of Integer, Integer)

            For Each dRow As DataRow In dsList.Tables(0).Rows
                If CInt(dRow.Item("ObjUnkid")) <= 0 Then Continue For
                If mDicRowSpan.ContainsKey(CInt(dRow.Item("ObjUnkid"))) = False Then
                    mDicRowSpan.Add(CInt(dRow.Item("ObjUnkid")), 1)
                Else
                    mDicRowSpan(CInt(dRow.Item("ObjUnkid"))) += 1
                End If
            Next

            dsList.Tables(0).Columns.Remove("PId")
            dsList.Tables(0).Columns.Remove("KpiUnkid")
            dsList.Tables(0).Columns.Remove("TargetUnkid")
            dsList.Tables(0).Columns.Remove("InitiativeUnkid")
            dsList.Tables(0).Columns.Remove("objisGrp")

            For Each dCol As DataColumn In dsList.Tables(0).Columns
                If dCol.Caption = "" Then Continue For
                dCol.ColumnName = dCol.Caption
            Next

            Dim intFCount As Integer = CType(dsList.Tables(0).Compute("COUNT(objGrpId)", "objGrpId =1"), Integer)
            Dim intCCount As Integer = CType(dsList.Tables(0).Compute("COUNT(objGrpId)", "objGrpId =2"), Integer)
            Dim intBCount As Integer = CType(dsList.Tables(0).Compute("COUNT(objGrpId)", "objGrpId =3"), Integer)
            Dim intOCount As Integer = CType(dsList.Tables(0).Compute("COUNT(objGrpId)", "objGrpId =4"), Integer)

            dsList.Tables(0).Columns.Remove("objGrpId")

            dsList.Tables(0).Columns("ObjUnkid").SetOrdinal(dsList.Tables(0).Columns.Count - 1)


            Dim strBuilder As New System.Text.StringBuilder
            strBuilder.Append(" <HTML> " & vbCrLf)
            strBuilder.Append(" <TITLE> " & Language.getMessage(mstrModuleName, 8, "View BSC Assessment") & " " & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 WIDTH=100%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            strBuilder.Append(" <TD COLSPAN = '6'  align='CENTER' bgcolor='LightGrey'><FONT SIZE=2><B>" & " PERFORMANCE PLANNING </B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" <TD COLSPAN = '" & dsList.Tables(0).Columns.Count - 7 & "' align='CENTER' bgcolor='LightGrey'><FONT SIZE=2><B>" & " PERFORMANCE ASSESSMENT </B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To dsList.Tables(0).Columns.Count - 2
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dsList.Tables(0).Columns(j).Caption.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next


            If dsList.Tables(0).Rows.Count > 0 Then
                Dim intPerspectiveId As Integer = 0
                Dim StrName As String = ""
                Dim blnFlag As Boolean = False
                Dim mdblTotalWeight As Double = 0
                Dim intObjtiveId As Integer = 0

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If StrName <> dsList.Tables(0).Rows(i)(0).ToString Then
                        StrName = dsList.Tables(0).Rows(i)(0).ToString
                        intPerspectiveId += 1
                        blnFlag = True
                    End If

                    strBuilder.Append(" <TR> " & vbCrLf)
                    For k As Integer = 0 To dsList.Tables(0).Columns.Count - 2
                        If k = 0 Then
                            If blnFlag = True Then
                                Select Case intPerspectiveId
                                    Case 1
                                        strBuilder.Append("<TD VALIGN = 'TOP' BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN = '" & intFCount & "'><FONT SIZE=2><B> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</B></FONT></TD>" & vbCrLf)
                                    Case 2
                                        strBuilder.Append("<TD VALIGN = 'TOP' BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN = '" & intCCount & "'><FONT SIZE=2><B> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</B></FONT></TD>" & vbCrLf)
                                    Case 3
                                        strBuilder.Append("<TD VALIGN = 'TOP' BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN = '" & intBCount & "'><FONT SIZE=2><B> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</B></FONT></TD>" & vbCrLf)
                                    Case 4
                                        strBuilder.Append("<TD VALIGN = 'TOP' BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN = '" & intOCount & "'><FONT SIZE=2><B> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</B></FONT></TD>" & vbCrLf)
                                End Select
                                blnFlag = False

                            End If
                        Else
                            Dim oWSett As New clsWeight_Setting(CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text))
                            If oWSett._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                                If dsList.Tables(0).Columns(k).ColumnName.ToUpper = "WEIGHT" Then
                                    If intObjtiveId <> CInt(dsList.Tables(0).Rows(i)("ObjUnkid")) Then
                                        intObjtiveId = CInt(dsList.Tables(0).Rows(i)("ObjUnkid"))
                                        If IsDBNull(dsList.Tables(0).Rows(i)("Weight")) = False Then
                                            If dsList.Tables(0).Rows(i)("Weight").ToString.Trim.Length > 0 Then
                                                mdblTotalWeight += CDbl(dsList.Tables(0).Rows(i)("Weight"))
                                            End If
                                        End If
                                        If mDicRowSpan.Keys.Count > 0 Then
                                            If mDicRowSpan.ContainsKey(intObjtiveId) Then
                                                strBuilder.Append("<TD VALIGN = 'TOP' BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN = '" & mDicRowSpan(intObjtiveId) & "'><FONT SIZE=2><B> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</B></FONT></TD>" & vbCrLf)
                                            Else
                                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                                            End If
                                        End If
                                    End If
                                Else
                                    If k >= 6 Then
                                        If k Mod 2 = 0 Or k Mod 2 > 0 Then
                                            Dim dColumn As Double = 0
                                            If dsList.Tables(0).Rows(i)(k).ToString.Trim.Length > 0 Then dColumn = CDbl(dsList.Tables(0).Rows(i)(k))
                                            If dColumn > 0 Then
                                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & CStr(dColumn.ToString("###.#0")) & "</FONT></TD>" & vbCrLf)
                                                mdblTotalWeight += dColumn
                                            Else
                                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp; </FONT></TD>" & vbCrLf)
                                            End If
                                        Else
                                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                                    End If
                                End If
                            Else
                                If k >= 5 Then
                                    If k Mod 2 = 0 Or k Mod 2 > 0 Then
                                        Dim dColumn As Double = 0
                                        If dsList.Tables(0).Rows(i)(k).ToString.Trim.Length > 0 Then dColumn = CDbl(dsList.Tables(0).Rows(i)(k))
                                        If dColumn > 0 Then
                                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & CStr(dColumn.ToString("###.#0")) & "</FONT></TD>" & vbCrLf)
                                            mdblTotalWeight += dColumn
                                        Else
                                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp; </FONT></TD>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                                    End If
                                Else
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & CStr(dsList.Tables(0).Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                                End If
                            End If
                        End If
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next

                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD COLSPAN = '5'  align='RIGHT'><FONT SIZE=2><B>" & " Total Weight </B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD align='LEFT'><FONT SIZE=2><B> &nbsp; " & CStr(mdblTotalWeight.ToString("###.#0")) & "</B></FONT></TD>" & vbCrLf)

                Dim dblCount As Double = 0
                For k As Integer = 6 To dsList.Tables(0).Columns.Count - 2 Step 2
                    dblCount = 0

                    For l As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        If dsList.Tables(0).Rows(l)(k).ToString.Trim.Length > 0 Then
                            dblCount = dblCount + CDbl(dsList.Tables(0).Rows(l)(k))
                        End If
                    Next
                    If dblCount > 0 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> <B> &nbsp;" & CStr(dblCount.ToString("###.#0")) & "</B></FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp; </FONT></TD>" & vbCrLf)
                    End If
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp; </FONT></TD>" & vbCrLf)
                Next

                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" </TABLE> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)

                strBuilder.Append(" <TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD COLSPAN = '4'  align='LEFT'><FONT SIZE=1><B>" & " AGREED PERFORMANCE PLAN (To be signed on agreement of Performance Plan) </B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD COLSPAN = '4'  align='LEFT'><FONT SIZE=1><B>" & " AGREED PERFORMANCE EVALUATION (To be signed after Appraisal) </B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD ><FONT SIZE=1><B>EMPLOYEE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>" & sEmployeeName & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>MANAGER/SUPERVISOR</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B> &nbsp; </B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>APPRAISEE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>&nbsp;</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>APPRAISER</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>&nbsp;</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>SIGNATURE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>SIGNATURE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>SIGNATURE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>SIGNATURE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>DATE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>DATE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>DATE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=1><B>DATE</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD><FONT SIZE=2><B>..............</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" </BODY> " & vbCrLf)
                strBuilder.Append(" </HTML> " & vbCrLf)

                If SaveExcelfile(sPath, strBuilder) = True Then
                    Process.Start(sPath)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_BSC_Form", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New IO.FileStream(fpath, IO.FileMode.Create, IO.FileAccess.Write)
        Dim strWriter As New IO.StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, IO.SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SaveExcelfile", mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
    'S.SANDEEP [ 01 APR 2013 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmBSC_ObjectiveList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvObjective.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmBSC_ObjectiveList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmBSC_ObjectiveList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objObjective = New clsObjective_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call SetVisibility()

            Language.setLanguage(Me.Name)

            Call OtherSettings()
           
            Call FillCombo()


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            lvObjective.GridLines = False
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                pnlEmployee.Visible = True
            Else
                pnlEmployee.Visible = False
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            If lvObjective.Items.Count > 0 Then lvObjective.Items(0).Selected = True
            lvObjective.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_ObjectiveList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmClassesList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objObjective = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsObjective_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsObjective_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvObjective.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Objective from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvObjective.Select()
            Exit Sub
        End If
       
        Try

            If objObjective.isUsed(CInt(lvObjective.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Objective. Reason: This objective is in use."), enMsgBoxStyle.Information) '?2
                lvObjective.Select()
                Exit Sub
            End If

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Objective?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objObjective.Delete(CInt(lvObjective.SelectedItems(0).Tag), mintReferenceUnkid)
                If objObjective._Message <> "" Then
                    eZeeMsgBox.Show(objObjective._Message, enMsgBoxStyle.Information)
                Else
                    lvObjective.SelectedItems(0).Remove()
                End If

            End If

            Fill_List()

            'Pinkal (20-Jan-2012) -- End 

                If lvObjective.Items.Count <= 0 Then
                    Exit Try
                End If

            lvObjective.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvObjective.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Objective from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvObjective.Select()
            Exit Sub
        End If
        Dim frm As New frmBSC_ObjectiveAddEdit
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvObjective.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmBSC_ObjectiveAddEdit
        Try
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objWSetting.Is_Setting_Present(-1) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then

                If ConfigParameter._Object._IsBSCObjectiveSaved = False Then

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "As per your configuration setting Balanced Score Card Obective is set to base on employee. " & vbCrLf & " If you want to continue with the same setting please select 'Yes' or you can change the settings to 'general'." & vbCrLf & "Aruti Configuration -> Options -> General -> 'Balance Score Card Evaluation Settings'." & vbCrLf & " Do You wish to continue ? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        OpenAddEdit_Form()
                    Else
                        Exit Sub
                    End If

                Else

                    OpenAddEdit_Form()

                End If

            Else
                OpenAddEdit_Form()
            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPerspective.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If txtUnlockRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please enter Unlock Remark. Unlock Remark is mandatory information."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You are about to Unlock Final Save this Balance Score Card Planning, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                       "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim objStatusTran As New clsobjective_status_tran

                objStatusTran._Assessoremployeeunkid = 0
                objStatusTran._Assessormasterunkid = 0
                objStatusTran._Commtents = txtUnlockRemark.Text
                objStatusTran._Employeeunkid = CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
                objStatusTran._Periodunkid = CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)
                objStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                objStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
                objStatusTran._Yearunkid = CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text)
                objStatusTran._Isunlock = True
                objStatusTran._Userunkid = User._Object._Userunkid

                If objStatusTran.Insert(User._Object._Userunkid, True) = False Then 'S.SANDEEP [ 13 JUL 2014 ] -- Start -- End 
                    eZeeMsgBox.Show(objStatusTran._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    Dim objObjective As New clsObjective_Master
                    Dim sName As String = String.Empty
                    sName = User._Object._Firstname & " " & User._Object._Lastname
                    If sName.Trim.Length <= 0 Then sName = User._Object._Username
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    'objObjective.Send_Notification_Employee(CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), sName, txtUnlockRemark.Text, False, True)
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objObjective.Send_Notification_Employee(CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), sName, txtUnlockRemark.Text, False, True, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
                    objObjective.Send_Notification_Employee(CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), sName, txtUnlockRemark.Text, False, True, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
                    'Sohail (30 Nov 2017) -- End
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                End If
                objStatusTran = Nothing
                Call Fill_List()
            End If

            Call btnCancel_Click(sender, e)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            txtUnlockRemark.Text = ""
            gbUnlockRemark.SendToBack()
            lvObjective.BringToFront()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 10 APR 2013 ] -- END


#End Region

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Controls Events "

    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub mnuFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFinalSave.Click
    '    Try
    '        If lvObjective.SelectedItems.Count > 0 Then
    '            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You are about to Final Save this Balance Score Card Planning, Due to this employee won't be able to modify or delete this." & vbCrLf & _
    '                                                   "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub


    '            'S.SANDEEP [ 28 DEC 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'If objObjective.Set_Final_Operation(True, CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
    '            '    Call Fill_List()
    '            'End If

    '            If objObjective.Set_Final_Operation(True, CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                '    Call Fill_List()
                'End If

    '            'S.SANDEEP [ 28 DEC 2012 ] -- END
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuFinalSave_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Private Sub mnuUnlockFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockFinalSave.Click
    '    Try
    '        If lvObjective.SelectedItems.Count > 0 Then

    '            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You are about to Unlock Final Save this Balance Score Card Planning, Due to this employee can modify or delete this." & vbCrLf & _
    '                                                   "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

    '            'S.SANDEEP [ 28 DEC 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'If objObjective.Set_Final_Operation(False, CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
    '            '    Call Fill_List()
    '            'End If

    '            If objObjective.Set_Final_Operation(False, CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
    '                Call Fill_List()
    '            End If
    '            'S.SANDEEP [ 28 DEC 2012 ] -- END
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuUnlockFinalSave_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub mnuSubmitApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSubmitApproval.Click
        Try
            If lvObjective.SelectedItems.Count > 0 Then

                'S.SANDEEP [ 02 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim objAssessor As New clsAssessor_tran
                Dim sMsg As String = String.Empty
                sMsg = objAssessor.IsAssessorPresent(CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text))
                If sMsg <> "" Then
                    eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objAssessor = Nothing
                'S.SANDEEP [ 02 AUG 2013 ] -- END


                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You are about to Submit this Balance Score Card Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                      "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
                Dim objStatusTran As New clsobjective_status_tran
                objStatusTran._Assessoremployeeunkid = 0
                objStatusTran._Assessormasterunkid = 0
                objStatusTran._Commtents = ""
                objStatusTran._Employeeunkid = CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
                objStatusTran._Periodunkid = CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)
                objStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                objStatusTran._Statustypeid = enObjective_Status.SUBMIT_APPROVAL
                objStatusTran._Yearunkid = CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text)
                objStatusTran._Isunlock = False
                'S.SANDEEP [ 23 JULY 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objStatusTran._Userunkid = 0
                objStatusTran._Userunkid = User._Object._Userunkid
                'S.SANDEEP [ 23 JULY 2013 ] -- END
                If objStatusTran.Insert(User._Object._Userunkid, True) = False Then 'S.SANDEEP [ 13 JUL 2014 ] -- Start -- End 
                    eZeeMsgBox.Show(objStatusTran._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    If ConfigParameter._Object._ArutiSelfServiceURL.Trim.Length > 0 Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objObjective.Send_Notification_Assessor(CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
                        '                                        CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                        '                                        CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
                        '                                        FinancialYear._Object._FinancialYear_Name, , , enLogin_Mode.DESKTOP, 0, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0, 0} -- END
                        objObjective.Send_Notification_Assessor(CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
                                                                CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                                                CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
                                                                FinancialYear._Object._FinancialYear_Name, FinancialYear._Object._DatabaseName, , , enLogin_Mode.DESKTOP, 0, 0)
                        'Sohail (21 Aug 2015) -- End
                    End If
                    Call Fill_List()
                End If
                objStatusTran = Nothing
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitApproval_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuApproveSubmitted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproveSubmitted.Click
        Try
            If lvObjective.SelectedItems.Count > 0 Then

                'Issue this checkpoint was not allowing to open FINAL SAVE screen after planning has been submitted for approval.
                '<TODO> need to show this to sandy and then uncomment this code.
                'If objObjective.isUsed(CInt(lvObjective.SelectedItems(0).Tag)) = True Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry You cannot perform this operation. Reason : Objective(s). are already bound to some of the transactions."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If


                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You are about to Approve/Disapprove this Balance Score Card Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
                                                       "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

                Dim frm As New frmCommonBSC_View
                frm.displayDialog(lvObjective.SelectedItems(0).SubItems(objcolhEmployee.Index).Text.Substring(0, lvObjective.SelectedItems(0).SubItems(objcolhEmployee.Index).Text.IndexOf(">") - 2).Trim, _
                                      CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
                                      CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
                                      CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                      False, CBool(IIf(lvObjective.SelectedItems(0).ForeColor = Color.Blue, True, False)))
                    Call Fill_List()
                End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApproveSubmitted_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuUnlockFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockFinalSave.Click
        Try
            If lvObjective.SelectedItems.Count > 0 Then
                'S.SANDEEP [ 02 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim objAnalysis_Master As New clsBSC_Analysis_Master
                If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text), CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objAnalysis_Master = Nothing
                'S.SANDEEP [ 02 AUG 2013 ] -- END

                lvObjective.SendToBack()
                gbUnlockRemark.BringToFront()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlockFinalSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 10 APR 2013 ] -- END



    Private Sub lvObjective_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvObjective.SelectedIndexChanged
        Try
            If lvObjective.SelectedItems.Count <= 0 Then Exit Sub

            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If lvObjective.SelectedItems(0).ForeColor = Color.Blue Then
            '    'S.SANDEEP [ 10 APR 2013 ] -- START
                ''ENHANCEMENT : TRA CHANGES
            '    'btnEdit.Enabled = False : btnDelete.Enabled = False : mnuFinalSave.Enabled = False : mnuUnlockFinalSave.Enabled = True
            '    btnEdit.Enabled = False : btnDelete.Enabled = False : mnuFinalSave.Enabled = False : mnuSubmitApproval.Enabled = False
            '    'S.SANDEEP [ 10 APR 2013 ] -- END

            '    ''S.SANDEEP [ 01 APR 2013 ] -- START
            '    ''ENHANCEMENT : TRA CHANGES
            '    'mnuPrintBSCForm.Visible = True
            '    ''S.SANDEEP [ 01 APR 2013 ] -- END
            'Else
            '    'S.SANDEEP [ 10 APR 2013 ] -- START
                ''ENHANCEMENT : TRA CHANGES
            '    'btnEdit.Enabled = True : btnDelete.Enabled = True : mnuFinalSave.Enabled = True : mnuUnlockFinalSave.Enabled = False
            '    btnEdit.Enabled = True : btnDelete.Enabled = True : mnuFinalSave.Enabled = True : mnuSubmitApproval.Enabled = True
            '    'S.SANDEEP [ 10 APR 2013 ] -- END

            '    ''S.SANDEEP [ 01 APR 2013 ] -- START
            '    ''ENHANCEMENT : TRA CHANGES
            '    'mnuPrintBSCForm.Visible = False
            '    ''S.SANDEEP [ 01 APR 2013 ] -- END
            'End If
            If lvObjective.SelectedItems(0).ForeColor = Color.Green Then
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                'S.SANDEEP [28 MAY 2015] -- START
                'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                'mnuApproveSubmitted.Enabled = User._Object.Privilege._Allow_FinalSaveBSCPlanning
                'S.SANDEEP [28 MAY 2015] -- END
                mnuSubmitApproval.Enabled = False
                mnuUnlockFinalSave.Enabled = False
            ElseIf lvObjective.SelectedItems(0).ForeColor = Color.Blue Then
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                mnuApproveSubmitted.Enabled = False
                mnuSubmitApproval.Enabled = False
                'S.SANDEEP [28 MAY 2015] -- START
                'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                'mnuUnlockFinalSave.Enabled = User._Object.Privilege._Allow_UnlockFinalSaveBSCPlanning
                'S.SANDEEP [28 MAY 2015] -- END
            Else
                btnEdit.Enabled = True
                btnDelete.Enabled = True
                mnuApproveSubmitted.Enabled = False
                'S.SANDEEP [28 MAY 2015] -- START
                'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                'mnuSubmitApproval.Enabled = User._Object.Privilege._AllowToSubmitBSCPlan_Approval
                'S.SANDEEP [28 MAY 2015] -- END
                mnuUnlockFinalSave.Enabled = False
            End If
            'S.SANDEEP [ 10 APR 2013 ] -- END

            
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If mnuFinalSave.Enabled = True Then
            '    mnuFinalSave.Enabled = User._Object.Privilege._Allow_FinalSaveBSCPlanning
            'End If
            'If mnuUnlockFinalSave.Enabled = True Then
            '    mnuUnlockFinalSave.Enabled = User._Object.Privilege._Allow_UnlockFinalSaveBSCPlanning
            'End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvObjective_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 01 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuPrintBSCForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrintBSCForm.Click
        Try
            If lvObjective.SelectedItems.Count > 0 Then
                Dim sfdlg As New FolderBrowserDialog
                If sfdlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim objAssessment As New clsBSC_Analysis_Master
                    Dim dsList As New DataSet
                    dsList = objAssessment.GetAssessmentResultView(CInt(lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
                                                           CInt(lvObjective.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
                                                           CInt(lvObjective.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                                           CStr(ConfigParameter._Object._IsBSC_ByEmployee))

                    Call Export_BSC_Form(sfdlg.SelectedPath & "\" & lvObjective.SelectedItems(0).SubItems(objcolhEmpId.Index).Tag.ToString & ".xls", dsList, lvObjective.SelectedItems(0).SubItems(objcolhEmployee.Index).Text.Substring(0, lvObjective.SelectedItems(0).SubItems(objcolhEmployee.Index).Text.IndexOf(">") - 2).Trim)
                End If        
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrintBSCForm_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 01 APR 2013 ] -- END


    'S.SANDEEP [ 20 JULY 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuImportBSC_Planning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportBSC_Planning.Click
        Try
            Dim frm As New frmImport_Objective_Wizard
            frm.ShowDialog()
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportBSC_Planning_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'Dim IExcel As New ExcelData
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try

            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                Dim dTable As New DataTable

                dTable.Columns.Add("Employeecode", System.Type.GetType("System.String")) : dTable.Columns.Add("Perspective_Id", System.Type.GetType("System.Int32")) : dTable.Columns.Add("Objective_Code", System.Type.GetType("System.String"))
                dTable.Columns.Add("Objective_Name", System.Type.GetType("System.String")) : dTable.Columns.Add("Result_Group", System.Type.GetType("System.String")) : dTable.Columns.Add("Period", System.Type.GetType("System.String"))
                dTable.Columns.Add("Weight", System.Type.GetType("System.Decimal"))

                dsList.Tables.Add(dTable.Copy)
                'IExcel.Export(strFilePath, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 20 JULY 2013 ] -- END

#End Region
    'S.SANDEEP [ 05 MARCH 2012 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbUnlockRemark.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUnlockRemark.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhPerspective.Text = Language._Object.getCaption(CStr(Me.colhPerspective.Tag), Me.colhPerspective.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhWeight.Text = Language._Object.getCaption(CStr(Me.colhWeight.Tag), Me.colhWeight.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuPrintBSCForm.Text = Language._Object.getCaption(Me.mnuPrintBSCForm.Name, Me.mnuPrintBSCForm.Text)
			Me.mnuFinalSave.Text = Language._Object.getCaption(Me.mnuFinalSave.Name, Me.mnuFinalSave.Text)
			Me.mnuUnlockFinalSave.Text = Language._Object.getCaption(Me.mnuUnlockFinalSave.Name, Me.mnuUnlockFinalSave.Text)
			Me.mnuSubmitApproval.Text = Language._Object.getCaption(Me.mnuSubmitApproval.Name, Me.mnuSubmitApproval.Text)
			Me.mnuApproveSubmitted.Text = Language._Object.getCaption(Me.mnuApproveSubmitted.Name, Me.mnuApproveSubmitted.Text)
			Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
			Me.lblCaption2.Text = Language._Object.getCaption(Me.lblCaption2.Name, Me.lblCaption2.Text)
			Me.gbUnlockRemark.Text = Language._Object.getCaption(Me.gbUnlockRemark.Name, Me.gbUnlockRemark.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.mnuImportBSC_Planning.Text = Language._Object.getCaption(Me.mnuImportBSC_Planning.Name, Me.mnuImportBSC_Planning.Text)
			Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Objective from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Objective. Reason: This objective is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Objective?")
			Language.setMessage(mstrModuleName, 4, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting.")
			Language.setMessage(mstrModuleName, 5, "You are about to Submit this Balance Score Card Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                      "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 6, "You are about to Approve/Disapprove this Balance Score Card Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
                                                       "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 7, " -> Period :")
			Language.setMessage(mstrModuleName, 8, "View BSC Assessment")
			Language.setMessage(mstrModuleName, 9, "As per your configuration setting Balanced Score Card Obective is set to base on employee. " & vbCrLf & " If you want to continue with the same setting please select 'Yes' or you can change the settings to 'general'." & vbCrLf & "Aruti Configuration -> Options -> General -> 'Balance Score Card Evaluation Settings'." & vbCrLf & " Do You wish to continue ?")
			Language.setMessage(mstrModuleName, 11, "You are about to Unlock Final Save this Balance Score Card Planning, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                       "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 12, "Please enter Unlock Remark. Unlock Remark is mandatory information.")
			Language.setMessage(mstrModuleName, 13, "Template Exported Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class