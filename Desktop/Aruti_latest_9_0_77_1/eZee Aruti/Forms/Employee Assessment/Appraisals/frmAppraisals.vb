﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAppraisals

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmAppraisals"
    Private objRef As clsAppraisal_ShortList_Master
    Private objShortListFinal As clsAppraisal_Final_Emp
    Private mdtTran As DataTable
    Private mdtShortListTran As DataTable
    Private mdtFinalTran As DataTable
    Private mblnIsFromMail As Boolean = False
    Private mstrEmployee() As String
    Private dsEmailList As New DataSet
    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 14 AUG 2013 ] -- END

#End Region

#Region " Propertie(s) "

    Public Property _IsFromMail() As Boolean
        Get
            Return mblnIsFromMail
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property

    Public ReadOnly Property EmailList() As String()
        Get
            Return mstrEmployee
        End Get
    End Property

    Public Property _EmailData() As DataSet
        Get
            Return dsEmailList
        End Get
        Set(ByVal value As DataSet)
            dsEmailList = value
        End Set
    End Property

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        'S.SANDEEP [ 14 AUG 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objMaster As New clsMasterData
        Dim objCMaster As New clsCommon_Master
        Dim objRating As New clsAppraisal_Rating
        Dim objPrd As New clscommom_period_Tran
        'S.SANDEEP [ 14 AUG 2013 ] -- END
        Dim dsCombo As New DataSet
        'S.SANDEEP [ 22 OCT 2013 ] -- START
        Dim objMaster As New clsMasterData
        'S.SANDEEP [ 22 OCT 2013 ] -- END
        Try
            dsCombo = objRef.getComboList("List", True)
            With cboRefNo
                .ValueMember = "shortlistunkid"
                .DisplayMember = "referenceno"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, False, "EmployeeList", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("EmployeeList")
                .SelectedIndex = 0
            End With

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombo = objMaster.getComboListAppraisalMode("Mode", True)
            ''If mblnIsFromMail = False Then
            ''dsCombo = objMaster.getComboListAppraisalMode("Mode", True, "0, " & enAppraisal_Modes.SALARY_INCREMENT & "," & enAppraisal_Modes.COMMENDABLE_LETTER & "," & enAppraisal_Modes.IMPROVEMENT_LETTER & "," & enAppraisal_Modes.WARNING_LETTER & "")
            ''Else
            ''dsCombo = objMaster.getComboListAppraisalMode("Mode", True, "0, " & enAppraisal_Modes.COMMENDABLE_LETTER & "," & enAppraisal_Modes.IMPROVEMENT_LETTER & "," & enAppraisal_Modes.WARNING_LETTER & "")
            ''End If
            'With cboAppraisalMode
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("Mode")
            '    .SelectedIndex = 0
            'End With

            'dsCombo = objMaster.getComboListAppraisalMode("Mode", True, "0, " & enAppraisal_Modes.COMMENDABLE_LETTER & "," & enAppraisal_Modes.IMPROVEMENT_LETTER & "," & enAppraisal_Modes.WARNING_LETTER & "")
            'With cboApplyAppMode
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("Mode")
            '    .SelectedIndex = 0
            'End With
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS, True, "List")
            With cboAppraisalMode
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
                .SelectedValue = 0
            End With
            With cboApplyAppMode
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
                .SelectedValue = 0
            End With
            dsCombo = objRating.getComboList("List", True)
            With cboGrades_Award
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPrd.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objPrd.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            dsCombo = objMaster.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 1
            End With
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'S.SANDEEP [19 FEB 2015] -- START
            Dim xCItems() As ComboBoxValue = {New ComboBoxValue(Language.getMessage("frmShortlistingEmployee", 111, "Overall Score"), enApprFilterTypeId.APRL_OVER_ALL), _
                                              New ComboBoxValue(Language.getMessage("frmShortlistingEmployee", 112, "Self Score"), enApprFilterTypeId.APRL_EMPLOYEE), _
                                              New ComboBoxValue(Language.getMessage("frmShortlistingEmployee", 113, "Assessor Score"), enApprFilterTypeId.APRL_ASSESSOR), _
                                              New ComboBoxValue(Language.getMessage("frmShortlistingEmployee", 114, "Reviewer Score"), enApprFilterTypeId.APRL_REVIEWER)}

            With cboFinalResult
                .Items.Clear()
                .Items.AddRange(xCItems)
                .SelectedIndex = 0
                .SelectedValue = CType(cboFinalResult.SelectedItem, ComboBoxValue).Value
            End With
            'S.SANDEEP [19 FEB 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objMaster = Nothing
            objCMaster = Nothing
            objRating = Nothing
            objPrd = Nothing
            'S.SANDEEP [ 14 AUG 2013 ] -- END
        End Try
    End Sub

    Private Sub FillShortListedEmployee()
        Try
            dgShortListEmployee.AutoGenerateColumns = False
            objcolhSelect.DataPropertyName = "IsChecked"
            colhEmpCode.DataPropertyName = "employeecode"
            colhEmpName.DataPropertyName = "employeename"
            colhGender.DataPropertyName = "Gender"
            colhAppointdate.DataPropertyName = "appointeddate"
            'colhPhone.DataPropertyName = "Phone"
            colhEmail.DataPropertyName = "email"
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dgcolhJobTitle.DataPropertyName = "Job_Title"
            dgcolhDepartment.DataPropertyName = "Dept"
            If dgcolhScore.Visible = True Then
                dgcolhScore.DataPropertyName = "Score"
            End If
            If dgcolhAction.Visible = True Then
                dgcolhAction.DataPropertyName = "operationmode"
            End If
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            dgShortListEmployee.DataSource = mdtShortListTran

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillShortListedEmployee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillFinalEmployeeList()
        Try
            dgFinalEmployee.AutoGenerateColumns = False
            objcolhFinalSelect.DataPropertyName = "IsChecked"
            colhFinalEmpCode.DataPropertyName = "employeecode"
            colhFinalEmpName.DataPropertyName = "employeename"
            colhFinalGender.DataPropertyName = "Gender"
            colhFinalAppointdate.DataPropertyName = "appointeddate"
            'colhFinalPhone.DataPropertyName = "Phone"
            colhFinalEmail.DataPropertyName = "email"
            colhFinalMode.DataPropertyName = "operationmode"
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dgcolhfinal_Job.DataPropertyName = "Job_Title"
            dgcolhfinalDept.DataPropertyName = "Dept"
            dgcolhOperation.DataPropertyName = "Operation"
            'S.SANDEEP [ 14 AUG 2013 ] -- END
            dgFinalEmployee.DataSource = mdtFinalTran
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillFinalEmployeeList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Try

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowToViewApprisalAnalysisList = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboRefNo.SelectedValue) > 0 Then
            '    If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
            '    mdtTran = objShortListFinal.GetShortListFinalEmployee(CInt(cboEmployee.SelectedValue), CInt(cboRefNo.SelectedValue), -1, cboRefNo.Text.Trim, True)

            '    For Each dtRow As DataRow In mdtTran.Rows
            '        dtRow.Item("appointeddate") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString).ToShortDateString
            '    Next

            '    mdtFinalTran = New DataView(mdtTran, "isfinalshortlisted = 1 ", "", DataViewRowState.CurrentRows).ToTable
            '    mdtShortListTran = New DataView(mdtTran, "isfinalshortlisted = 0 ", "", DataViewRowState.CurrentRows).ToTable

            '    FillShortListedEmployee()
            '    FillFinalEmployeeList()

            '    Dim dtTemp() As DataRow = CType(cboRefNo.DataSource, DataTable).Select("shortlistunkid= " & CInt(cboRefNo.SelectedValue) & "")
            '    If dtTemp.Length > 0 Then
            '        txtOpeningDate.Text = eZeeDate.convertDate(dtTemp(0)("referencedate").ToString).ToShortDateString
            '    End If
            'Else
            '    txtOpeningDate.Text = ""
            '    dgShortListEmployee.DataSource = Nothing : dgFinalEmployee.DataSource = Nothing
            'End If
            If radByGrades.Checked = True Then

                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Please select Period to continue. Period is mandatory information."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Exit Sub
                End If

                If CInt(cboGrades_Award.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Please select Grades/Award to continue. Grades/Award is mandatory information."), enMsgBoxStyle.Information)
                    cboGrades_Award.Focus()
                    Exit Sub
                End If


                'S.SANDEEP [ 22 OCT 2013 ] -- START
                If CInt(cboAppointment.SelectedValue) = enAD_Report_Parameter.APP_DATE_FROM Then
                    If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
                        dtpDate2.Focus()
                        Exit Sub
                    ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
                        dtpDate1.Focus()
                        Exit Sub
                    ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
                        If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Appointment To Date cannot be less then Appointment From Date."), enMsgBoxStyle.Information)
                            dtpDate2.Focus()
                            Exit Sub
                        End If
                    End If
                End If

                objShortListFinal._AppointmentTypeId = CInt(cboAppointment.SelectedValue)
                If dtpDate1.Checked = True Then
                    objShortListFinal._Date1 = dtpDate1.Value.Date
                End If

                If dtpDate2.Visible = True Then
                    If dtpDate2.Checked = True Then
                        objShortListFinal._Date2 = dtpDate2.Value.Date
                    End If
                End If

                'S.SANDEEP [ 22 OCT 2013 ] -- END

                If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()

                'S.SANDEEP [19 FEB 2015] -- START
                'mdtTran = objShortListFinal.GetEmployee_ByAward(txtScoreFrom.Decimal, _
                '                                                txtScoreTo.Decimal, _
                '                                                CInt(cboPeriod.SelectedValue), _
                '                                                CStr(IIf(txtScoreFrom.Tag IsNot Nothing, txtScoreFrom.Tag.ToString, "")), _
                '                                                CInt(txtScoreTo.Tag), _
                '                                                CInt(cboGrades_Award.SelectedValue), mstrAdvanceFilter)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'mdtTran = objShortListFinal.GetNewEmployee_ByAward(txtScoreFrom.Decimal, _
                '                                                txtScoreTo.Decimal, _
                '                                                CInt(cboPeriod.SelectedValue), _
                '                                                CStr(IIf(txtScoreFrom.Tag IsNot Nothing, txtScoreFrom.Tag.ToString, "")), _
                '                                                CInt(txtScoreTo.Tag), _
                '                                                CInt(cboGrades_Award.SelectedValue), mstrAdvanceFilter, _
                '                                                CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value), _
                '                                                ConfigParameter._Object._ScoringOptionId)

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'mdtTran = objShortListFinal.GetNewEmployee_ByAward(txtScoreFrom.Decimal, _
                '                                                txtScoreTo.Decimal, _
                '                                                CInt(cboPeriod.SelectedValue), _
                '                                                CStr(IIf(txtScoreFrom.Tag IsNot Nothing, txtScoreFrom.Tag.ToString, "")), _
                '                                                CInt(txtScoreTo.Tag), _
                '                                                CInt(cboGrades_Award.SelectedValue), mstrAdvanceFilter, _
                '                                                CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value), _
                '                                                ConfigParameter._Object._ScoringOptionId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                                ConfigParameter._Object._Self_Assign_Competencies, ConfigParameter._Object._IsUseAgreedScore)

                mdtTran = objShortListFinal.GetNewEmployee_ByAward(txtScoreFrom.Decimal, _
                                                                txtScoreTo.Decimal, _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                CStr(IIf(txtScoreFrom.Tag IsNot Nothing, txtScoreFrom.Tag.ToString, "")), _
                                                                CInt(txtScoreTo.Tag), _
                                                                CInt(cboGrades_Award.SelectedValue), mstrAdvanceFilter, _
                                                                CInt(CType(cboFinalResult.SelectedItem, ComboBoxValue).Value), _
                                                                ConfigParameter._Object._ScoringOptionId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._Self_Assign_Competencies, ConfigParameter._Object._IsUseAgreedScore, _
                                                                ConfigParameter._Object._IsCalibrationSettingActive)
                'S.SANDEEP |27-MAY-2019| -- END

                'Shani (23-Nov-2016) -- [ConfigParameter._Object._UseAgreedScore]


                'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
                'S.SANDEEP [04 JUN 2015] -- END

                'S.SANDEEP [19 FEB 2015] -- END



                For Each dtRow As DataRow In mdtTran.Rows
                    dtRow.Item("appointeddate") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString).ToShortDateString
                Next

                mdtFinalTran = New DataView(mdtTran, "isfinalshortlisted = 1 ", "", DataViewRowState.CurrentRows).ToTable
                mdtShortListTran = New DataView(mdtTran, "isfinalshortlisted = 0 ", "", DataViewRowState.CurrentRows).ToTable

                FillShortListedEmployee()
                FillFinalEmployeeList()

            ElseIf radByReference.Checked = True Then
                If CInt(cboRefNo.SelectedValue) > 0 Then
                    If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
                    mdtTran = objShortListFinal.GetShortListFinalEmployee(CInt(cboEmployee.SelectedValue), CInt(cboRefNo.SelectedValue), -1, cboRefNo.Text.Trim, True, mstrAdvanceFilter)

                    For Each dtRow As DataRow In mdtTran.Rows
                        dtRow.Item("appointeddate") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString).ToShortDateString
                    Next

                    mdtFinalTran = New DataView(mdtTran, "isfinalshortlisted = 1 ", "", DataViewRowState.CurrentRows).ToTable
                    mdtShortListTran = New DataView(mdtTran, "isfinalshortlisted = 0 ", "", DataViewRowState.CurrentRows).ToTable

                    FillShortListedEmployee()
                    FillFinalEmployeeList()

                    Dim dtTemp() As DataRow = CType(cboRefNo.DataSource, DataTable).Select("shortlistunkid= " & CInt(cboRefNo.SelectedValue) & "")
                    If dtTemp.Length > 0 Then
                        txtOpeningDate.Text = eZeeDate.convertDate(dtTemp(0)("referencedate").ToString).ToShortDateString
                    End If
                Else
                    txtOpeningDate.Text = ""
                    dgShortListEmployee.DataSource = Nothing : dgFinalEmployee.DataSource = Nothing
                End If
            End If
            'S.SANDEEP [ 14 AUG 2013 ] -- END



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            Dim dTemp() As DataRow = mdtFinalTran.Select("operationmodeid <= 0")
            If dTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Appraisal Mode is mandatory information. Please set Appraisal Mode to continue."), enMsgBoxStyle.Information)
                cboApplyAppMode.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            mnuSalaryIncrement.Enabled = User._Object.Privilege._AllowToPerformSalaryIncrement
            mnuVoidSalaryIncrement.Enabled = User._Object.Privilege._AllowToVoidSalaryIncrement
            mnuRemider.Enabled = User._Object.Privilege._AllowToAddReminder
            lnkApplyAction.Enabled = User._Object.Privilege._AllowToAddEmployeeToFinalList
            btnSave.Enabled = User._Object.Privilege._AllowToSaveAppraisals

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                mnuSalaryIncrement.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                mnuVoidSalaryIncrement.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END


    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Clear_Controls()
        Try
            cboGrades_Award.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboRefNo.SelectedValue = 0
            txtOpeningDate.Text = ""
            dgShortListEmployee.DataSource = Nothing
            dgFinalEmployee.DataSource = Nothing
            'S.SANDEEP [ 22 OCT 2013 ] -- START
            cboAppointment.SelectedValue = 1
            dtpDate1.Checked = False
            dtpDate2.Checked = False
            'S.SANDEEP [ 22 OCT 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Clear_Controls", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

#End Region

#Region " Form's Event(s) "

    Private Sub frmFinalEmployee_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objRef = New clsAppraisal_ShortList_Master
        objShortListFinal = New clsAppraisal_Final_Emp
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            If mblnIsFromMail = True Then
                gbApplyAppraisalMode.Enabled = False
                EZeeFooter1.Visible = False
                objEMailFooter.Visible = True
                If tabAppEmployee.TabPages.Contains(tabpgShotList) = True Then
                    tabAppEmployee.TabPages.Remove(tabpgShotList)
                End If

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dgShortListEmployee.Enabled = False
                objCheckAll.Enabled = False
                objbtndown.Enabled = False : objbtnUp.Enabled = False
                'S.SANDEEP [ 14 AUG 2013 ] -- END


                AddHandler cboAppraisalMode.SelectedIndexChanged, AddressOf cboAppraisalMode_SelectedIndexChanged
            Else
                gbApplyAppraisalMode.Enabled = True
                EZeeFooter1.Visible = True
                objEMailFooter.Visible = False
                If tabAppEmployee.TabPages.Contains(tabpgShotList) = False Then
                    tabAppEmployee.TabPages.Add(tabpgShotList)
                End If
                'RemoveHandler cboAppraisalMode.SelectedIndexChanged, AddressOf cboAppraisalMode_SelectedIndexChanged
            End If

            Call FillCombo()

            radByGrades.Checked = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFinalEmployee_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsshortlist_master.SetMessages()
            clsshortlist_filter.SetMessages()
            objfrm._Other_ModuleNames = "clsshortlist_finalapplicant"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboRefNo.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboAppraisalMode.SelectedValue = 0
            mstrAdvanceFilter = "" 'S.SANDEEP [ 14 AUG 2013 ] -- START -- END
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objResetFinalEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objResetFinalEmployee.Click
        Try
            cboAppraisalMode.SelectedValue = 0
            If mdtTran Is Nothing Then Exit Sub
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'mdtFinalTran = New DataView(mdtTran, "isfinalshortlisted = 1 ", "", DataViewRowState.CurrentRows).ToTable
            mdtFinalTran = New DataView(mdtTran, "", "", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [ 14 AUG 2013 ] -- END
            Call FillFinalEmployeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objResetFinalEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchFinalEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchFinalEmployee.Click
        Try
            If mdtTran Is Nothing Then Exit Sub

            If CInt(cboAppraisalMode.SelectedValue) > 0 Then
                mdtFinalTran = New DataView(mdtTran, "isfinalshortlisted = 1 AND operationmodeid = " & CInt(cboAppraisalMode.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtFinalTran = New DataView(mdtTran, "isfinalshortlisted = 1 ", "", DataViewRowState.CurrentRows).ToTable
            End If
            Call FillFinalEmployeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchFinalEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            With frm
                .ValueMember = cboRefNo.ValueMember
                .CodeMember = cboRefNo.DisplayMember
                .DisplayMember = "remark"
                .DataSource = CType(cboRefNo.DataSource, DataTable)
            End With
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            If frm.DisplayDialog Then
                cboRefNo.SelectedValue = frm.SelectedValue
                cboRefNo.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If mdtFinalTran.Rows.Count <= 0 Then Exit Sub
            If dgFinalEmployee.RowCount <= 0 Then Exit Sub

            If IsValidData() = False Then Exit Sub

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objShortListFinal._FormName = mstrModuleName
            objShortListFinal._LoginEmployeeunkid = -1
            objShortListFinal._ClientIP = getIP()
            objShortListFinal._HostName = getHostName()
            objShortListFinal._FromWeb = False
            objShortListFinal._AuditUserId = User._Object._Userunkid
            objShortListFinal._CompanyUnkid = Company._Object._Companyunkid
            objShortListFinal._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If objShortListFinal.Set_FinalEmployee(mdtFinalTran) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee(s) successfully assigned to final Employee list."), enMsgBoxStyle.Information)
            '    Me.Close()
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Final Employee assignment process fail."), enMsgBoxStyle.Information)
            'End If
            If radByGrades.Checked = True Then
                If objShortListFinal.InsertByGrades(mdtFinalTran) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee(s) successfully assigned to final Employee list."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Final Employee assignment process fail."), enMsgBoxStyle.Information)
                End If
            Else
                If objShortListFinal.Set_FinalEmployee(mdtFinalTran) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee(s) successfully assigned to final Employee list."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Final Employee assignment process fail."), enMsgBoxStyle.Information)
                End If
            End If
            Call Fill_List()
            'S.SANDEEP [ 14 AUG 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If CInt(cboAppraisalMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Appraisal Mode is mandatory information. Please select Appraisal Mode to continue."), enMsgBoxStyle.Information)
                cboAppraisalMode.Focus()
                Exit Sub
            End If

            If mdtFinalTran IsNot Nothing Then
                Dim dtTemp() As DataRow = Nothing

                dtTemp = mdtFinalTran.Select("IsChecked = 1")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                dtTemp = mdtFinalTran.Select("email = ''")

                If dtTemp.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Some employees do not have email address and will not be added in sending list. Are you sure you want to proceed with the operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If

                dtTemp = mdtFinalTran.Select("IsChecked = 1 AND email <> ''")

                If dtTemp.Length > 0 Then
                    Dim objLetterFields As New clsLetterFields
                    Dim StrEmpIds As String = String.Empty
                    mstrEmployee = New String() {}
                    Dim intCnt As Integer = 0
                    ReDim mstrEmployee(dtTemp.Length)
                    For i As Integer = 0 To dtTemp.Length - 1
                        mstrEmployee(intCnt) = dtTemp(i)("employeename").ToString & " " & "<" & dtTemp(i)("email").ToString & ">"
                        StrEmpIds &= "," & dtTemp(i)("employeeunkid").ToString
                        intCnt += 1
                    Next

                    If StrEmpIds.Trim.Length > 0 Then StrEmpIds = Mid(StrEmpIds, 2)

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsEmailList = objLetterFields.GetEmployeeData(StrEmpIds, enImg_Email_RefId.Employee_Module)

                    'S.SANDEEP |09-APR-2019| -- START
                    'dsEmailList = objLetterFields.GetEmployeeData(StrEmpIds, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                    dsEmailList = objLetterFields.GetEmployeeData(StrEmpIds, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                    'S.SANDEEP |09-APR-2019| -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                Me.Close()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEClose.Click
        Try
            Call btnClose_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnApply_Action_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnApply_Action.Click, objbtnFilter_Action.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim cbo As New ComboBox
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToString.ToUpper
                Case objbtnApply_Action.Name.ToUpper
                    cbo = cboApplyAppMode
                Case objbtnFilter_Action.Name.ToUpper
                    cbo = cboAppraisalMode
            End Select

            With frm
                .ValueMember = cbo.ValueMember
                .DisplayMember = cbo.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cbo.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cbo.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnApply_Action_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchGrades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGrades.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboGrades_Award.ValueMember
                .DisplayMember = cboGrades_Award.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboGrades_Award.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboGrades_Award.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGrades_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If mdtFinalTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please Check atleast one Final shortlisted Employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                dtTemp = mdtFinalTran.Select("IsChecked = 1 AND salaryincrementtranunkid > 0")
                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, there are some employees whom salary increment is already given. Please void the salary increment. " & vbCrLf & _
                                                        "  In order to set them as shortlisted employee(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                dtTemp = mdtFinalTran.Select("IsChecked = 1 AND operationmodeid > 0 ")
                If dtTemp.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "There are some appraisal action attached to checked employee(s). If you press 'Yes', The action attached to the employee(s) will be removed." & vbCrLf & _
                                                           "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        dtTemp = mdtFinalTran.Select("IsChecked = 1 AND salaryincrementtranunkid <= 0")

                        For i As Integer = 0 To dtTemp.Length - 1
                            dtTemp(i).Item("IsChecked") = False
                            dtTemp(i).Item("operationmodeid") = 0
                            dtTemp(i).Item("isfinalshortlisted") = 0
                            dtTemp(i).Item("AUD") = "U"
                            mdtShortListTran.ImportRow(dtTemp(i))
                            mdtFinalTran.Rows.Remove(dtTemp(i))
                        Next
                    Else
                        Exit Sub
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btndown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtndown.Click
        Try
            If mdtShortListTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtShortListTran.Select("IsChecked = 1")

                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Check atleast one short listed Employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                For i As Integer = 0 To dtTemp.Length - 1
                    dtTemp(i).Item("IsChecked") = False
                    If radByGrades.Checked = True Then
                        dtTemp(i).Item("AUD") = "A"
                    Else
                        dtTemp(i).Item("AUD") = "U"
                    End If
                    mdtFinalTran.ImportRow(dtTemp(i))
                    mdtShortListTran.Rows.Remove(dtTemp(i))
                Next

                RemoveHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
                objCheckAll.CheckState = CheckState.Unchecked
                AddHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged

                If mdtFinalTran.Rows.Count > 0 Then
                    btnSave.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btndown_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

#End Region

#Region " Control's Event(s) "

    Private Sub lnkApplyAction_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkApplyAction.LinkClicked
        Try
            If mdtFinalTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1")

                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If radByGrades.Checked = True Then
                    dtTemp = mdtFinalTran.Select("IsChecked = 1 AND operationmodeid > 0")
                    If dtTemp.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot apply action to checked final shortlisted employee. Reason : Action is already applied."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    dtTemp = mdtFinalTran.Select("IsChecked = 1 AND operationmodeid <= 0")
                    For Each drRow As DataRow In dtTemp
                        drRow.Item("IsChecked") = False
                        drRow.Item("operationmodeid") = CInt(cboApplyAppMode.SelectedValue)
                        If CInt(cboApplyAppMode.SelectedValue) > 0 Then
                            drRow.Item("operationmode") = cboApplyAppMode.Text
                        Else
                            drRow.Item("operationmode") = ""
                        End If
                        If IsDBNull(drRow.Item("AUD")) Or CStr(drRow.Item("AUD")).ToString.Trim = "" Then
                            drRow.Item("AUD") = "U"
                        End If
                        drRow.AcceptChanges()
                    Next
                Else
                    'For Each drRow As DataRow In dtTemp
                    '    If CInt(drRow.Item("operationmodeid")) > 0 Then
                    '        If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Some of the Employees appraisal has already been assigned and now their appraisal will be changed." & vbCrLf & vbCrLf & "Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    '            Exit Sub
                    '        Else
                    '            Exit For
                    '        End If
                    '    End If
                    'Next
                    'For Each drRow As DataRow In dtTemp
                    '    If CInt(drRow.Item("operationmodeid")) = enAppraisal_Modes.SALARY_INCREMENT Then
                    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry! You can not change Salary Increment Appraisal. To Change this Please void Salary Increment Appraisal from Operation Button."), enMsgBoxStyle.Information)
                    '        Exit Sub
                    '    End If
                    'Next

                    For Each drRow As DataRow In dtTemp
                        drRow.Item("IsChecked") = False
                        drRow.Item("operationmodeid") = CInt(cboApplyAppMode.SelectedValue)
                        If CInt(cboApplyAppMode.SelectedValue) > 0 Then
                            drRow.Item("operationmode") = cboApplyAppMode.Text
                        Else
                            drRow.Item("operationmode") = ""
                        End If

                        If IsDBNull(drRow.Item("AUD")) Or CStr(drRow.Item("AUD")).ToString.Trim = "" Then
                            drRow.Item("AUD") = "U"
                        End If

                        drRow.AcceptChanges()
                    Next
                    objFinalCheckAll.Checked = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkApplyAction_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgShortListEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgShortListEmployee.CellContentClick
        Try
            If e.ColumnIndex = objcolhSelect.Index Then
                mdtShortListTran.Rows(e.RowIndex)(e.ColumnIndex) = Not CBool(mdtShortListTran.Rows(e.RowIndex)(e.ColumnIndex))
                dgShortListEmployee.RefreshEdit()
                Dim drRow As DataRow() = mdtShortListTran.Select("IsChecked = 1", "")
                RemoveHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
                If drRow.Length > 0 Then
                    If mdtShortListTran.Rows.Count = drRow.Length Then
                        objCheckAll.CheckState = CheckState.Checked
                    Else
                        objCheckAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objCheckAll.CheckState = CheckState.Unchecked
                End If
                AddHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgShortListEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgFinalEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFinalEmployee.CellContentClick
        Try
            If e.ColumnIndex = objcolhFinalSelect.Index Then
                mdtFinalTran.Rows(e.RowIndex)(e.ColumnIndex) = Not CBool(mdtFinalTran.Rows(e.RowIndex)(e.ColumnIndex))
                dgFinalEmployee.RefreshEdit()
                Dim drRow As DataRow() = mdtFinalTran.Select("IsChecked = 1", "")
                If drRow.Length > 0 Then
                    If mdtFinalTran.Rows.Count = drRow.Length Then
                        objFinalCheckAll.CheckState = CheckState.Checked
                    Else
                        objFinalCheckAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objFinalCheckAll.CheckState = CheckState.Unchecked
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgFinalEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub objCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objCheckAll.CheckedChanged
        Try
            If mdtShortListTran IsNot Nothing AndAlso mdtShortListTran.Rows.Count > 0 Then
                If objCheckAll.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In mdtShortListTran.Rows
                        dr("isChecked") = objCheckAll.Checked
                    Next
                    mdtShortListTran.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objFinalCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objFinalCheckAll.CheckedChanged
        Try
            If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then
                If objFinalCheckAll.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In mdtFinalTran.Rows
                        dr("isChecked") = objFinalCheckAll.Checked
                    Next
                    mdtFinalTran.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objFinalCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuSalaryIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSalaryIncrement.Click
        'S.SANDEEP [ 14 AUG 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim strEmpList As String = ""
        'Dim intPeriodId As Integer = 0
        ''Sohail (29 Dec 2012) -- Start
        ''TRA - ENHANCEMENT
        'Dim dicAppraisal As New Dictionary(Of String, ArrayList) 'Sohail (29 Dec 2012)
        'Dim arrList As ArrayList
        ''Sohail (29 Dec 2012) -- End

        'Try
        '    If mdtFinalTran IsNot Nothing Then
        '        Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1")

        '        If dtTemp.Length <= 0 Then
        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Employee."), enMsgBoxStyle.Information)
        '            Exit Sub
        '        End If

        '        For Each drRow As DataRow In dtTemp
        '            If CInt(drRow.Item("operationmodeid")) > 0 Then
        '                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Some of the Employees appraisal has already been assigned and now their appraisal will be changed." & vbCrLf & vbCrLf & "Do you want to proceed?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
        '                    Exit Sub
        '                Else
        '                    Exit For
        '                End If
        '            End If
        '        Next

        '        For Each drRow As DataRow In dtTemp
        '            If strEmpList.Trim = "" Then
        '                strEmpList = drRow.Item("employeeunkid").ToString
        '            Else
        '                strEmpList &= ", " & drRow.Item("employeeunkid").ToString
        '            End If
        '            'Sohail (29 Dec 2012) -- Start
        '            'TRA - ENHANCEMENT
        '            If dicAppraisal.ContainsKey(drRow.Item("employeeunkid").ToString) = False Then
        '                arrList = New ArrayList
        '                arrList.Add(-1) 'finalemployeeunkid
        '                arrList.Add(0) 'SalaryincrementUnkId
        '                dicAppraisal.Add(drRow.Item("employeeunkid").ToString, arrList)
        '            End If
        '            'Sohail (29 Dec 2012) -- End
        '        Next

        '        Dim objFrm As New frmSalaryGlobalAssign
        '        If objFrm.DisplayDialog(strEmpList, intPeriodId, dicAppraisal) = True Then
        '            'Sohail (29 Dec 2012) -- Start
        '            'TRA - ENHANCEMENT
        '            'Dim dtTable As DataTable = New DataView(mdtFinalTran, "employeeunkid IN (" & strEmpList & ") ", "", DataViewRowState.CurrentRows).ToTable
        '            'For Each drRow As DataRow In dtTable.Rows
        '            '    drRow.Item("periodunkid") = intPeriodId
        '            '    drRow.Item("operationmodeid") = enAppraisal_Modes.SALARY_INCREMENT
        '            'Next
        '            Dim dtTable As DataTable
        '            For Each pair In dicAppraisal
        '                arrList = pair.Value
        '                Dim dRow() As DataRow = mdtFinalTran.Select("employeeunkid = " & pair.Key & " ")
        '                If dRow.Length > 0 Then
        '                    For Each drRow As DataRow In dRow
        '                        drRow.Item("periodunkid") = intPeriodId
        '                        drRow.Item("operationmodeid") = enAppraisal_Modes.SALARY_INCREMENT
        '                        drRow.Item("salaryincrementtranunkid") = CInt(arrList(1))
        '                        mdtFinalTran.AcceptChanges()
        '                    Next
        '                End If
        '            Next
        '            dtTable = New DataView(mdtFinalTran, "employeeunkid IN (" & strEmpList & ") ", "", DataViewRowState.CurrentRows).ToTable
        '            'Sohail (29 Dec 2012) -- End

        '            If objShortListFinal.Set_FinalEmployee(dtTable) Then
        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Employee(s) successfully assigned to Salary Increment."), enMsgBoxStyle.Information)
        '                Call Fill_List()
        '            Else
        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Employee Salary Increment process fail."), enMsgBoxStyle.Information)
        '            End If
        '        End If
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "mnuSalaryIncrement_Click", mstrModuleName)
        'End Try
        Dim strEmpList As String = "" : Dim intPeriodId As Integer = 0
        Dim dicAppraisal As New Dictionary(Of String, ArrayList) : Dim arrList As ArrayList
        Try

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objShortListFinal._FormName = mstrModuleName
            objShortListFinal._LoginEmployeeunkid = -1
            objShortListFinal._ClientIP = getIP()
            objShortListFinal._HostName = getHostName()
            objShortListFinal._FromWeb = False
            objShortListFinal._AuditUserId = User._Object._Userunkid
            objShortListFinal._CompanyUnkid = Company._Object._Companyunkid
            objShortListFinal._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If mdtFinalTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                dtTemp = mdtFinalTran.Select("IsChecked = 1 AND isfinalshortlisted = 1")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Some of the checked employees. are not final shortlisted. Please save checked employee(s) in order to make them final shortlisted."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                dtTemp = mdtFinalTran.Select("IsChecked = 1 AND salaryincrementtranunkid > 0")
                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Salary increment is already done for some of the checked employee(s) from the list. If you wish to give them salary increment, First void the old salary increment."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'S.SANDEEP [12-JUL-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
                'dtTemp = mdtFinalTran.Select("IsChecked = 1 AND isfinalshortlisted = 1")
                dtTemp = mdtFinalTran.Select("IsChecked = 1 AND isfinalshortlisted = 1 AND edunkid <=0")
                'S.SANDEEP [12-JUL-2018] -- END

                For Each drRow As DataRow In dtTemp
                    If strEmpList.Trim = "" Then
                        strEmpList = drRow.Item("employeeunkid").ToString
                    Else
                        strEmpList &= ", " & drRow.Item("employeeunkid").ToString
                    End If
                    If dicAppraisal.ContainsKey(drRow.Item("employeeunkid").ToString) = False Then
                        arrList = New ArrayList
                        arrList.Add(-1) 'finalemployeeunkid
                        arrList.Add(0) 'SalaryincrementUnkId
                        dicAppraisal.Add(drRow.Item("employeeunkid").ToString, arrList)
                    End If
                Next
                Dim objFrm As New frmSalaryGlobalAssign
                If objFrm.DisplayDialog(strEmpList, intPeriodId, dicAppraisal) = True Then
                    Dim dtTable As DataTable
                    For Each pair In dicAppraisal
                        arrList = pair.Value
                        Dim dRow() As DataRow = mdtFinalTran.Select("employeeunkid = " & pair.Key & " ")
                        If dRow.Length > 0 Then
                            For Each drRow As DataRow In dRow
                                drRow.Item("periodunkid") = intPeriodId
                                'drRow.Item("operationmodeid") = enAppraisal_Modes.SALARY_INCREMENT 'As Per Andrew's Comment Not To Change the Operation Mode
                                drRow.Item("salaryincrementtranunkid") = CInt(arrList(1))
                                mdtFinalTran.AcceptChanges()
                            Next
                        End If
                    Next
                    dtTable = New DataView(mdtFinalTran, "employeeunkid IN (" & strEmpList & ") ", "", DataViewRowState.CurrentRows).ToTable
                    If objShortListFinal.Set_FinalEmployee(dtTable) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Employee(s) successfully assigned to Salary Increment."), enMsgBoxStyle.Information)
                        Call Fill_List()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Employee Salary Increment process fail."), enMsgBoxStyle.Information)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSalaryIncrement_Click", mstrModuleName)
        Finally
        End Try
        'S.SANDEEP [ 14 AUG 2013 ] -- END
    End Sub

    Private Sub mnuVoidSalaryIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVoidSalaryIncrement.Click
        Dim strFilter As String = ""
        Dim dicEmpPeriodID As New Dictionary(Of Integer, Integer)
        Try
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If mdtFinalTran IsNot Nothing Then
            '    Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1 AND operationmodeid = " & enAppraisal_Modes.SALARY_INCREMENT & " ")

            '    If dtTemp.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Check atleast one Final Employee with Salary Increment Appraisal."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    For Each drRow As DataRow In dtTemp
            '        If strFilter.Trim = "" Then
            '            strFilter = " AND ( ( employeeunkid = " & CInt(drRow.Item("employeeunkid")) & "  AND periodunkid = " & CInt(drRow.Item("periodunkid")) & ") "
            '        Else
            '            strFilter &= "OR ( employeeunkid = " & drRow.Item("employeeunkid").ToString & "  AND periodunkid = " & CInt(drRow.Item("periodunkid")) & ") "
            '        End If
            '    Next
            '    If strFilter.Trim <> "" Then strFilter &= ")"

            '    Dim objFrm As New frmSalaryIncrementList
            '    If objFrm.DisplayDialog(strFilter, dicEmpPeriodID) = True Then
            '        If dicEmpPeriodID.Count > 0 Then
            '            Dim dtTable As DataTable = New DataView(mdtFinalTran, "1 = 2", "", DataViewRowState.CurrentRows).ToTable
            '            Dim drRow() As DataRow
            '            For Each pair In dicEmpPeriodID
            '                drRow = mdtFinalTran.Select("employeeunkid = " & pair.Key & " AND periodunkid = " & pair.Value & " ")
            '                If drRow.Length > 0 Then
            '                    For Each dRow As DataRow In drRow
            '                        dRow.Item("periodunkid") = "-1"
            '                        dRow.Item("operationmodeid") = "0"
            '                        dRow.Item("salaryincrementtranunkid") = "0" 'Sohail (29 Dec 2012)
            '                        dtTable.ImportRow(dRow)
            '                    Next
            '                End If
            '            Next
            '            If objShortListFinal.Set_FinalEmployee(dtTable) Then
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Void Salary Increment Appraisal completed."), enMsgBoxStyle.Information)
            '                Call Fill_List()
            '            Else
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Void Salary Increment Appraisal process failed."), enMsgBoxStyle.Information)
            '            End If
            '        End If

            '    End If
            'End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objShortListFinal._FormName = mstrModuleName
            objShortListFinal._LoginEmployeeunkid = -1
            objShortListFinal._ClientIP = getIP()
            objShortListFinal._HostName = getHostName()
            objShortListFinal._FromWeb = False
            objShortListFinal._AuditUserId = User._Object._Userunkid
            objShortListFinal._CompanyUnkid = Company._Object._Companyunkid
            objShortListFinal._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If mdtFinalTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1 AND OperationId = 1")

                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Check atleast one Final Employee with Salary Increment Appraisal."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                For Each drRow As DataRow In dtTemp
                    If strFilter.Trim = "" Then
                        strFilter = " AND ( ( employeeunkid = " & CInt(drRow.Item("employeeunkid")) & "  AND periodunkid = " & CInt(drRow.Item("periodunkid")) & ") "
                    Else
                        strFilter &= "OR ( employeeunkid = " & drRow.Item("employeeunkid").ToString & "  AND periodunkid = " & CInt(drRow.Item("periodunkid")) & ") "
                    End If
                Next
                If strFilter.Trim <> "" Then strFilter &= ")"

                Dim objFrm As New frmSalaryIncrementList
                If objFrm.DisplayDialog(strFilter, dicEmpPeriodID) = True Then
                    If dicEmpPeriodID.Count > 0 Then
                        Dim dtTable As DataTable = New DataView(mdtFinalTran, "1 = 2", "", DataViewRowState.CurrentRows).ToTable
                        Dim drRow() As DataRow
                        For Each pair In dicEmpPeriodID
                            drRow = mdtFinalTran.Select("employeeunkid = " & pair.Key & " AND periodunkid = " & pair.Value & " ")
                            If drRow.Length > 0 Then
                                For Each dRow As DataRow In drRow
                                    dRow.Item("periodunkid") = 0
                                    'dRow.Item("operationmodeid") = "0" 'As Per Andrew's Comment Not To Change the Operation Mode
                                    dRow.Item("salaryincrementtranunkid") = "0" 'Sohail (29 Dec 2012)
                                    dtTable.ImportRow(dRow)
                                Next
                            End If
                        Next
                        If objShortListFinal.Set_FinalEmployee(dtTable) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Void Salary Increment Appraisal completed."), enMsgBoxStyle.Information)
                            Call Fill_List()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Void Salary Increment Appraisal process failed."), enMsgBoxStyle.Information)
                        End If
                    End If

                End If
            End If

            'S.SANDEEP [ 14 AUG 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVoidSalaryIncrement_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuRemider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRemider.Click
        Try
            Dim objFrm As New frmReminder_AddEdit
            objFrm.displayDialog(-1, enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRemider_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAppraisalMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAppraisalMode.SelectedIndexChanged
        Try
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call objSearchFinalEmployee_Click(sender, e)
            If CInt(cboAppraisalMode.SelectedValue) > 0 Then
                Call objSearchFinalEmployee_Click(sender, e)
            Else
                Call objResetFinalEmployee_Click(sender, e)
            End If
            'S.SANDEEP [ 14 AUG 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppraisalMode_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub radByGrades_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radByGrades.CheckedChanged, radByReference.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case radByGrades.Name.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        pnlGradeWise.Visible = True : pnlRefWise.Visible = False
                        dgcolhScore.Visible = True : dgcolhAction.Visible = True
                        objbtnUp.Enabled = False
                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        pnlAppdate.Enabled = True
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                    End If
                Case radByReference.Name.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        pnlGradeWise.Visible = False : pnlRefWise.Visible = True
                        dgcolhScore.Visible = False : dgcolhAction.Visible = False
                        objbtnUp.Enabled = True
                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        pnlAppdate.Enabled = False
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                    End If
            End Select
            Call Clear_Controls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radByGrades_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboGrades_Award_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrades_Award.SelectedIndexChanged
        Try
            If CInt(cboGrades_Award.SelectedValue) > 0 Then
                Dim dtmp() As DataRow = CType(cboGrades_Award.DataSource, DataTable).Select("id = '" & CInt(cboGrades_Award.SelectedValue) & "'")
                If dtmp.Length > 0 Then
                    txtScoreFrom.Decimal = CDec(dtmp(0).Item("scrf"))
                    txtScoreTo.Decimal = CDec(dtmp(0).Item("scrt"))
                    txtScoreFrom.Tag = dtmp(0).Item("iAction")
                    If CInt(dtmp(0).Item("appactionunkid")) > 0 Then
                        txtScoreTo.Tag = dtmp(0).Item("appactionunkid")
                    Else
                        txtScoreTo.Tag = 0
                    End If
                Else
                    txtScoreFrom.Text = "" : txtScoreTo.Text = ""
                    txtScoreFrom.Tag = Nothing
                End If
            Else
                txtScoreFrom.Text = "" : txtScoreTo.Text = ""
                txtScoreFrom.Tag = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrades_Award_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 14 AUG 2013 ] -- END

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 22 OCT 2013 ] -- END

    'S.SANDEEP [12-JUL-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
    Private Sub mnuBonus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBonus.Click
        Dim objfrm As New frmPostAppraisalHead
        Try
            If mdtFinalTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                dtTemp = mdtFinalTran.Select("IsChecked = 1 AND isfinalshortlisted = 1 ")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Some of the checked employees. are not final shortlisted. Please save checked employee(s) in order to make them final shortlisted."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If User._Object._Isrighttoleft = True Then
                    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrm)
                End If

                objfrm.displayDialog(mdtFinalTran.Select("IsChecked = 1 AND isfinalshortlisted = 1 AND edunkid <=0 AND salaryincrementtranunkid <= 0").CopyToDataTable())
                Fill_List()
            End If        

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBonus_Click", mstrModuleName)
        Finally
            If objfrm IsNot Nothing Then objfrm.Dispose()
        End Try
    End Sub

    Private Sub mnuVoidBonus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVoidBonus.Click
        Dim frm As New frmEarningDeductionList
        Try
            If mdtFinalTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtFinalTran.Select("IsChecked = 1 AND OperationId = '" & CInt(enAppraisal_Modes.BONUS) & "'")
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Please Check atleast one Final Employee with Bonus."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim strempids As String = String.Join(",", dtTemp.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())

                frm._FromAppraisalScreen = True
                frm._AppraisalEmployeeIds = strempids
                frm.ShowDialog()
                Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVoidBonus_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [12-JUL-2018] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFinalEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFinalEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbApplyAppraisalMode.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApplyAppraisalMode.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnEClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnEClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtndown.GradientBackColor = GUI._ButttonBackColor
            Me.objbtndown.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUp.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUp.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lnkApplyAction.Text = Language._Object.getCaption(Me.lnkApplyAction.Name, Me.lnkApplyAction.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblReferenceDate.Text = Language._Object.getCaption(Me.lblReferenceDate.Name, Me.lblReferenceDate.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.tabpgShotList.Text = Language._Object.getCaption(Me.tabpgShotList.Name, Me.tabpgShotList.Text)
            Me.tabpgFinal.Text = Language._Object.getCaption(Me.tabpgFinal.Name, Me.tabpgFinal.Text)
            Me.gbFinalEmployee.Text = Language._Object.getCaption(Me.gbFinalEmployee.Name, Me.gbFinalEmployee.Text)
            Me.lblAppraisalMode.Text = Language._Object.getCaption(Me.lblAppraisalMode.Name, Me.lblAppraisalMode.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuSalaryIncrement.Text = Language._Object.getCaption(Me.mnuSalaryIncrement.Name, Me.mnuSalaryIncrement.Text)
            Me.gbApplyAppraisalMode.Text = Language._Object.getCaption(Me.gbApplyAppraisalMode.Name, Me.gbApplyAppraisalMode.Text)
            Me.lblApplyAppMode.Text = Language._Object.getCaption(Me.lblApplyAppMode.Name, Me.lblApplyAppMode.Text)
            Me.mnuVoidSalaryIncrement.Text = Language._Object.getCaption(Me.mnuVoidSalaryIncrement.Name, Me.mnuVoidSalaryIncrement.Text)
            Me.mnuRemider.Text = Language._Object.getCaption(Me.mnuRemider.Name, Me.mnuRemider.Text)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnEClose.Text = Language._Object.getCaption(Me.btnEClose.Name, Me.btnEClose.Text)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.lblGrades.Text = Language._Object.getCaption(Me.lblGrades.Name, Me.lblGrades.Text)
            Me.lblTotalScoreT.Text = Language._Object.getCaption(Me.lblTotalScoreT.Name, Me.lblTotalScoreT.Text)
            Me.lblTotalScoreF.Text = Language._Object.getCaption(Me.lblTotalScoreF.Name, Me.lblTotalScoreF.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.radByReference.Text = Language._Object.getCaption(Me.radByReference.Name, Me.radByReference.Text)
            Me.radByGrades.Text = Language._Object.getCaption(Me.radByGrades.Name, Me.radByGrades.Text)
            Me.colhEmpCode.HeaderText = Language._Object.getCaption(Me.colhEmpCode.Name, Me.colhEmpCode.HeaderText)
            Me.colhEmpName.HeaderText = Language._Object.getCaption(Me.colhEmpName.Name, Me.colhEmpName.HeaderText)
            Me.colhGender.HeaderText = Language._Object.getCaption(Me.colhGender.Name, Me.colhGender.HeaderText)
            Me.colhAppointdate.HeaderText = Language._Object.getCaption(Me.colhAppointdate.Name, Me.colhAppointdate.HeaderText)
            Me.colhEmail.HeaderText = Language._Object.getCaption(Me.colhEmail.Name, Me.colhEmail.HeaderText)
            Me.dgcolhJobTitle.HeaderText = Language._Object.getCaption(Me.dgcolhJobTitle.Name, Me.dgcolhJobTitle.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhScore.HeaderText = Language._Object.getCaption(Me.dgcolhScore.Name, Me.dgcolhScore.HeaderText)
            Me.dgcolhAction.HeaderText = Language._Object.getCaption(Me.dgcolhAction.Name, Me.dgcolhAction.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.colhFinalEmpCode.HeaderText = Language._Object.getCaption(Me.colhFinalEmpCode.Name, Me.colhFinalEmpCode.HeaderText)
            Me.colhFinalEmpName.HeaderText = Language._Object.getCaption(Me.colhFinalEmpName.Name, Me.colhFinalEmpName.HeaderText)
            Me.colhFinalGender.HeaderText = Language._Object.getCaption(Me.colhFinalGender.Name, Me.colhFinalGender.HeaderText)
            Me.colhFinalAppointdate.HeaderText = Language._Object.getCaption(Me.colhFinalAppointdate.Name, Me.colhFinalAppointdate.HeaderText)
            Me.colhFinalEmail.HeaderText = Language._Object.getCaption(Me.colhFinalEmail.Name, Me.colhFinalEmail.HeaderText)
            Me.dgcolhfinal_Job.HeaderText = Language._Object.getCaption(Me.dgcolhfinal_Job.Name, Me.dgcolhfinal_Job.HeaderText)
            Me.dgcolhfinalDept.HeaderText = Language._Object.getCaption(Me.dgcolhfinalDept.Name, Me.dgcolhfinalDept.HeaderText)
            Me.colhFinalMode.HeaderText = Language._Object.getCaption(Me.colhFinalMode.Name, Me.colhFinalMode.HeaderText)
            Me.dgcolhOperation.HeaderText = Language._Object.getCaption(Me.dgcolhOperation.Name, Me.dgcolhOperation.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblAppointment.Text = Language._Object.getCaption(Me.lblAppointment.Name, Me.lblAppointment.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblFinalResult.Text = Language._Object.getCaption(Me.lblFinalResult.Name, Me.lblFinalResult.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("frmShortlistingEmployee", 111, "Overall Score")
            Language.setMessage("frmShortlistingEmployee", 112, "Self Score")
            Language.setMessage("frmShortlistingEmployee", 113, "Assessor Score")
            Language.setMessage("frmShortlistingEmployee", 114, "Reviewer Score")
            Language.setMessage(mstrModuleName, 1, "Employee(s) successfully assigned to final Employee list.")
            Language.setMessage(mstrModuleName, 2, "Final Employee assignment process fail.")
            Language.setMessage(mstrModuleName, 3, "Please Check atleast one short listed Employee.")
            Language.setMessage(mstrModuleName, 4, "Please Check atleast one Final Employee.")
            Language.setMessage(mstrModuleName, 5, "Please Check atleast one Final Employee with Salary Increment Appraisal.")
            Language.setMessage(mstrModuleName, 7, "Void Salary Increment Appraisal completed.")
            Language.setMessage(mstrModuleName, 8, "Void Salary Increment Appraisal process failed.")
            Language.setMessage(mstrModuleName, 10, "Employee(s) successfully assigned to Salary Increment.")
            Language.setMessage(mstrModuleName, 11, "Employee Salary Increment process fail.")
            Language.setMessage(mstrModuleName, 12, "Appraisal Mode is mandatory information. Please set Appraisal Mode to continue.")
            Language.setMessage(mstrModuleName, 13, "Some employees do not have email address and will not be added in sending list. Are you sure you want to proceed with the operation.")
            Language.setMessage(mstrModuleName, 14, "Appraisal Mode is mandatory information. Please select Appraisal Mode to continue.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Please select Period to continue. Period is mandatory information.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Please select Grades/Award to continue. Grades/Award is mandatory information.")
            Language.setMessage(mstrModuleName, 17, "Sorry, Some of the checked employees. are not final shortlisted. Please save checked employee(s) in order to make them final shortlisted.")
            Language.setMessage(mstrModuleName, 18, "Please Check atleast one Final shortlisted Employee.")
            Language.setMessage(mstrModuleName, 19, "Sorry, you cannot apply action to checked final shortlisted employee. Reason : Action is already applied.")
            Language.setMessage(mstrModuleName, 20, "Sorry, there are some employees whom salary increment is already given. Please void the salary increment. " & vbCrLf & _
                                                                 "  In order to set them as shortlisted employee(s).")
            Language.setMessage(mstrModuleName, 21, "There are some appraisal action attached to checked employee(s")
            Language.setMessage(mstrModuleName, 22, "Salary increment is already done for some of the checked employee(s) from the list. If you wish to give them salary increment, First void the old salary increment.")
            Language.setMessage(mstrModuleName, 23, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date.")
            Language.setMessage(mstrModuleName, 24, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date.")
            Language.setMessage(mstrModuleName, 25, "Sorry, Appointment To Date cannot be less then Appointment From Date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class