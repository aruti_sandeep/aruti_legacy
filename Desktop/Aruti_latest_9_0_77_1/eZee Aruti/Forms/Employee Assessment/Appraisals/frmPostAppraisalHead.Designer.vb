﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostAppraisalHead
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPostAppraisalHead))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lnkProgress = New System.Windows.Forms.LinkLabel
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvED = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhTransactionHead = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriodId = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployeeId = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriod = New System.Windows.Forms.ColumnHeader
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.gbPostPerformanceHeads = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvEmpData = New System.Windows.Forms.DataGridView
        Me.dgcolhemployeecode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhshortlistunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhedunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchTrnHead = New eZee.Common.eZeeGradientButton
        Me.objFooter.SuspendLayout()
        Me.gbPostPerformanceHeads.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvEmpData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkProgress)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.lvED)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 284)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(638, 55)
        Me.objFooter.TabIndex = 4
        '
        'lnkProgress
        '
        Me.lnkProgress.ActiveLinkColor = System.Drawing.Color.Blue
        Me.lnkProgress.AutoSize = True
        Me.lnkProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkProgress.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkProgress.LinkColor = System.Drawing.Color.Blue
        Me.lnkProgress.Location = New System.Drawing.Point(289, 23)
        Me.lnkProgress.Name = "lnkProgress"
        Me.lnkProgress.Size = New System.Drawing.Size(56, 13)
        Me.lnkProgress.TabIndex = 2
        Me.lnkProgress.TabStop = True
        Me.lnkProgress.Text = "##value"
        Me.lnkProgress.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(529, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(426, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Post Head"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lvED
        '
        Me.lvED.BackColorOnChecked = True
        Me.lvED.CheckBoxes = True
        Me.lvED.ColumnHeaders = Nothing
        Me.lvED.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhTransactionHead, Me.colhAmount, Me.objcolhPeriodId, Me.objcolhEmployeeId, Me.objcolhPeriod})
        Me.lvED.CompulsoryColumns = ""
        Me.lvED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvED.FullRowSelect = True
        Me.lvED.GridLines = True
        Me.lvED.GroupingColumn = Nothing
        Me.lvED.HideSelection = False
        Me.lvED.Location = New System.Drawing.Point(830, 20)
        Me.lvED.MinColumnWidth = 50
        Me.lvED.MultiSelect = False
        Me.lvED.Name = "lvED"
        Me.lvED.OptionalColumns = ""
        Me.lvED.ShowMoreItem = False
        Me.lvED.ShowSaveItem = False
        Me.lvED.ShowSelectAll = True
        Me.lvED.ShowSizeAllColumnsToFit = True
        Me.lvED.Size = New System.Drawing.Size(10, 20)
        Me.lvED.Sortable = False
        Me.lvED.TabIndex = 271
        Me.lvED.UseCompatibleStateImageBehavior = False
        Me.lvED.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 225
        '
        'colhTransactionHead
        '
        Me.colhTransactionHead.Tag = "colhTransactionHead"
        Me.colhTransactionHead.Text = "Transaction Head"
        Me.colhTransactionHead.Width = 180
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 150
        '
        'objcolhPeriodId
        '
        Me.objcolhPeriodId.Tag = "objcolhPeriodId"
        Me.objcolhPeriodId.Text = "objcolhPeriodId"
        Me.objcolhPeriodId.Width = 0
        '
        'objcolhEmployeeId
        '
        Me.objcolhEmployeeId.Tag = "objcolhEmployeeId"
        Me.objcolhEmployeeId.Text = "objcolhEmployeeId"
        Me.objcolhEmployeeId.Width = 0
        '
        'objcolhPeriod
        '
        Me.objcolhPeriod.Tag = "objcolhPeriod"
        Me.objcolhPeriod.Text = "objcolhPeriod"
        Me.objcolhPeriod.Width = 0
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(118, 166)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(202, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 269
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(118, 143)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(202, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 268
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 38)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(100, 15)
        Me.lblPeriod.TabIndex = 267
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(118, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(207, 21)
        Me.cboPeriod.TabIndex = 266
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(12, 92)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(100, 15)
        Me.lblAmount.TabIndex = 264
        Me.lblAmount.Text = "Amount"
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(118, 89)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(207, 21)
        Me.txtAmount.TabIndex = 263
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(118, 62)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(207, 21)
        Me.cboTrnHead.TabIndex = 261
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(12, 65)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(100, 15)
        Me.lblTrnHead.TabIndex = 262
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'gbPostPerformanceHeads
        '
        Me.gbPostPerformanceHeads.BorderColor = System.Drawing.Color.Black
        Me.gbPostPerformanceHeads.Checked = False
        Me.gbPostPerformanceHeads.CollapseAllExceptThis = False
        Me.gbPostPerformanceHeads.CollapsedHoverImage = Nothing
        Me.gbPostPerformanceHeads.CollapsedNormalImage = Nothing
        Me.gbPostPerformanceHeads.CollapsedPressedImage = Nothing
        Me.gbPostPerformanceHeads.CollapseOnLoad = False
        Me.gbPostPerformanceHeads.Controls.Add(Me.objbtnSearchTrnHead)
        Me.gbPostPerformanceHeads.Controls.Add(Me.lblCostCenter)
        Me.gbPostPerformanceHeads.Controls.Add(Me.cboCostCenter)
        Me.gbPostPerformanceHeads.Controls.Add(Me.cboPeriod)
        Me.gbPostPerformanceHeads.Controls.Add(Me.lblPeriod)
        Me.gbPostPerformanceHeads.Controls.Add(Me.lblTrnHead)
        Me.gbPostPerformanceHeads.Controls.Add(Me.txtAmount)
        Me.gbPostPerformanceHeads.Controls.Add(Me.cboTrnHead)
        Me.gbPostPerformanceHeads.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbPostPerformanceHeads.Controls.Add(Me.lblAmount)
        Me.gbPostPerformanceHeads.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.gbPostPerformanceHeads.ExpandedHoverImage = Nothing
        Me.gbPostPerformanceHeads.ExpandedNormalImage = Nothing
        Me.gbPostPerformanceHeads.ExpandedPressedImage = Nothing
        Me.gbPostPerformanceHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPostPerformanceHeads.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPostPerformanceHeads.HeaderHeight = 25
        Me.gbPostPerformanceHeads.HeaderMessage = ""
        Me.gbPostPerformanceHeads.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPostPerformanceHeads.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPostPerformanceHeads.HeightOnCollapse = 0
        Me.gbPostPerformanceHeads.LeftTextSpace = 0
        Me.gbPostPerformanceHeads.Location = New System.Drawing.Point(274, 1)
        Me.gbPostPerformanceHeads.Name = "gbPostPerformanceHeads"
        Me.gbPostPerformanceHeads.OpenHeight = 300
        Me.gbPostPerformanceHeads.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPostPerformanceHeads.ShowBorder = True
        Me.gbPostPerformanceHeads.ShowCheckBox = False
        Me.gbPostPerformanceHeads.ShowCollapseButton = False
        Me.gbPostPerformanceHeads.ShowDefaultBorderColor = True
        Me.gbPostPerformanceHeads.ShowDownButton = False
        Me.gbPostPerformanceHeads.ShowHeader = True
        Me.gbPostPerformanceHeads.Size = New System.Drawing.Size(364, 282)
        Me.gbPostPerformanceHeads.TabIndex = 270
        Me.gbPostPerformanceHeads.Temp = 0
        Me.gbPostPerformanceHeads.Text = "Post Performance Heads Info"
        Me.gbPostPerformanceHeads.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(12, 119)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(100, 15)
        Me.lblCostCenter.TabIndex = 274
        Me.lblCostCenter.Text = "Cost Center"
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(118, 116)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(207, 21)
        Me.cboCostCenter.TabIndex = 273
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.pnlData)
        Me.gbEmployeeList.Controls.Add(Me.ComboBox2)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(1, 1)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(272, 282)
        Me.gbEmployeeList.TabIndex = 274
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee Info"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvEmpData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(2, 26)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(268, 253)
        Me.pnlData.TabIndex = 276
        '
        'dgvEmpData
        '
        Me.dgvEmpData.AllowUserToAddRows = False
        Me.dgvEmpData.AllowUserToDeleteRows = False
        Me.dgvEmpData.AllowUserToResizeRows = False
        Me.dgvEmpData.BackgroundColor = System.Drawing.Color.White
        Me.dgvEmpData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEmpData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmpData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhemployeecode, Me.dgcolhEmployee, Me.objdgcolhEmpId, Me.objdgcolhshortlistunkid, Me.objdgcolhedunkid})
        Me.dgvEmpData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmpData.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmpData.Name = "dgvEmpData"
        Me.dgvEmpData.ReadOnly = True
        Me.dgvEmpData.RowHeadersVisible = False
        Me.dgvEmpData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpData.Size = New System.Drawing.Size(268, 253)
        Me.dgvEmpData.TabIndex = 275
        '
        'dgcolhemployeecode
        '
        Me.dgcolhemployeecode.HeaderText = "Code"
        Me.dgcolhemployeecode.Name = "dgcolhemployeecode"
        Me.dgcolhemployeecode.ReadOnly = True
        Me.dgcolhemployeecode.Width = 80
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolhshortlistunkid
        '
        Me.objdgcolhshortlistunkid.HeaderText = "objdgcolhshortlistunkid"
        Me.objdgcolhshortlistunkid.Name = "objdgcolhshortlistunkid"
        Me.objdgcolhshortlistunkid.ReadOnly = True
        Me.objdgcolhshortlistunkid.Visible = False
        '
        'objdgcolhedunkid
        '
        Me.objdgcolhedunkid.HeaderText = "objdgcolhedunkid"
        Me.objdgcolhedunkid.Name = "objdgcolhedunkid"
        Me.objdgcolhedunkid.ReadOnly = True
        Me.objdgcolhedunkid.Visible = False
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.ComboBox2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(659, 35)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(211, 21)
        Me.ComboBox2.TabIndex = 261
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objdgcolhshortlistunkid"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolhedunkid"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'objbtnSearchTrnHead
        '
        Me.objbtnSearchTrnHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTrnHead.BorderSelected = False
        Me.objbtnSearchTrnHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTrnHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTrnHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTrnHead.Location = New System.Drawing.Point(331, 62)
        Me.objbtnSearchTrnHead.Name = "objbtnSearchTrnHead"
        Me.objbtnSearchTrnHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTrnHead.TabIndex = 276
        '
        'frmPostAppraisalHead
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 339)
        Me.Controls.Add(Me.gbEmployeeList)
        Me.Controls.Add(Me.gbPostPerformanceHeads)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPostAppraisalHead"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Post Head to Earning & Deduction"
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.gbPostPerformanceHeads.ResumeLayout(False)
        Me.gbPostPerformanceHeads.PerformLayout()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvEmpData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents gbPostPerformanceHeads As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents lvED As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTransactionHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPeriodId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmployeeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents dgvEmpData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lnkProgress As System.Windows.Forms.LinkLabel
    Friend WithEvents dgcolhemployeecode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhshortlistunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhedunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchTrnHead As eZee.Common.eZeeGradientButton
End Class
