﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmViewPerformancePlanning

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmViewPerformancePlanning"
    Private mdtEmpGoal As DataTable = Nothing
    Private mdtEmpCmpt As DataTable = Nothing
    Private objEmpField1 As clsassess_empfield1_master
    Private objCmpt_Mast As clsassess_competence_assign_master
    Private objFMaster As New clsAssess_Field_Master(True)

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Totals_Label(Optional ByVal xCmptWeght As Decimal = 0)
        Try
            Dim xMessage As String = String.Empty
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
            If xTotalWeight > 0 Then
                xMessage = Language.getMessage(mstrModuleName, 3, "Total Goals Weight :") & " " & xTotalWeight.ToString
            Else
                xMessage = ""
            End If
            objFMapping = Nothing

            'SHANI [25 Mar 2015]-START
            'ENHANCEMENT : Claim AND Request CHANGES.
            'If ConfigParameter._Object._Self_Assign_Competencies = True Then
            '    If xCmptWeght > 0 Then
            '        xMessage &= " | " & Language.getMessage(mstrModuleName, 4, "Total Competency Weight :") & " " & xCmptWeght.ToString
            '    End If
            'End If
            If xCmptWeght > 0 Then
                xMessage &= " | " & Language.getMessage(mstrModuleName, 4, "Total Competency Weight :") & " " & xCmptWeght.ToString
            End If

            'SHANI [25 Mar 2015]--END
            objlblTotalWeight.Text = xMessage
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Totals_Label", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Goals_Grid()
        Try
            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, No data defined for assessment caption settings screen."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objMap = Nothing

            mdtEmpGoal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List")

            dgvBSCData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty
            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split("|")
            End If
            For Each dCol As DataColumn In mdtEmpGoal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvBSCData.Columns.Contains(iColName) = True Then Continue For
                Dim dgvCol As New DataGridViewTextBoxColumn()
                dgvCol.Name = iColName
                dgvCol.Width = 120
                dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.ReadOnly = True
                dgvCol.DataPropertyName = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If
                If dCol.ColumnName = "Emp" Then
                    dgvCol.Visible = False
                End If
                If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    If dCol.ColumnName.StartsWith("Owr") Then
                        dgvCol.Visible = False
                    End If
                End If

                'S.SANDEEP [01 JUL 2015] -- START
                If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [01 JUL 2015] -- END

                dgvBSCData.Columns.Add(dgvCol)
            Next
            dgvBSCData.DataSource = mdtEmpGoal

            dgvBSCData.ColumnHeadersHeight = 35

            For Each dgvRow As DataGridViewRow In dgvBSCData.Rows
                'S.SANDEEP |10-AUG-2021| -- START
                'If CBool(dgvRow.Cells("objisfinal").Value) = True Then
                '    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                'ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
                '    dgvRow.DefaultCellStyle.ForeColor = Color.Green
                'End If
                If CBool(dgvRow.Cells("objisfinal").Value) = True Then
                    dgvRow.DefaultCellStyle.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_SAVE)), Color.Blue)
                ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
                    dgvRow.DefaultCellStyle.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.SUBMIT_APPROVAL), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.SUBMIT_APPROVAL)), Color.Green)
                Else
                    Dim cLr As Color
                    Select Case CInt(dgvRow.Cells("objopstatusid").Value)
                        Case enObjective_Status.OPEN_CHANGES
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.OPEN_CHANGES), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.OPEN_CHANGES)), Color.Black)
                        Case enObjective_Status.NOT_SUBMIT
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_SUBMIT), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_SUBMIT)), Color.Red)
                        Case enObjective_Status.NOT_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_COMMITTED)), Color.Purple)
                        Case enObjective_Status.FINAL_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_COMMITTED)), Color.Blue)
                        Case enObjective_Status.PERIODIC_REVIEW
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.PERIODIC_REVIEW), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.PERIODIC_REVIEW)), Color.Brown)
                    End Select
                    dgvRow.DefaultCellStyle.ForeColor = cLr
                End If
                'S.SANDEEP |10-AUG-2021| -- END
            Next

            Call Set_Totals_Label()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Goals_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Competancy_List()
        Dim dsList As New DataSet
        Dim iSearch As String = String.Empty
        Dim xDecTotalWgt As Decimal = 0
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objCmpt_Mast.GetList("List", 0, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), ConfigParameter._Object._Self_Assign_Competencies)
            dsList = objCmpt_Mast.GetList("List", eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), 0, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), ConfigParameter._Object._Self_Assign_Competencies)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [14 APR 2015] -- START
            If dsList Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, there is no list of competencies assigned to this employee or some defined assessment group does not fall in the allocation of the selected employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [14 APR 2015] -- END

            lvAssignedCompetencies.Items.Clear()
            mdtEmpCmpt = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable

            For Each dRow As DataRow In mdtEmpCmpt.Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dRow.Item("competencies").ToString
                lvItem.SubItems.Add(dRow.Item("assigned_weight").ToString)
                'S.SANDEEP [09 FEB 2015] -- START
                lvItem.SubItems.Add(dRow.Item("ccategory").ToString)
                'S.SANDEEP [09 FEB 2015] -- END

                'S.SANDEEP |10-AUG-2021| -- START
                'If CBool(dRow.Item("isfinal")) = True Or CInt(dRow.Item("opstatusid")) = enObjective_Status.FINAL_SAVE Then
                '    lvItem.ForeColor = Color.Blue
                'ElseIf CInt(dRow.Item("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                '    lvItem.ForeColor = Color.Green
                'End If
                If CBool(dRow.Item("isfinal")) = True Or CInt(dRow.Item("opstatusid")) = enObjective_Status.FINAL_SAVE Then
                    lvItem.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_SAVE)), Color.Blue)
                ElseIf CInt(dRow.Item("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                    lvItem.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.SUBMIT_APPROVAL), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.SUBMIT_APPROVAL)), Color.Green)
                Else
                    Dim cLr As Color
                    Select Case CInt(dRow.Item("opstatusid"))
                        Case enObjective_Status.OPEN_CHANGES
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.OPEN_CHANGES), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.OPEN_CHANGES)), Color.Black)
                        Case enObjective_Status.NOT_SUBMIT
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_SUBMIT), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_SUBMIT)), Color.Red)
                        Case enObjective_Status.NOT_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_COMMITTED)), Color.Purple)
                        Case enObjective_Status.FINAL_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_COMMITTED)), Color.Blue)
                        Case enObjective_Status.PERIODIC_REVIEW
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.PERIODIC_REVIEW), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.PERIODIC_REVIEW)), Color.Brown)
                    End Select
                    lvItem.ForeColor = cLr
                End If
                'S.SANDEEP |10-AUG-2021| -- END

                lvAssignedCompetencies.Items.Add(lvItem)
                xDecTotalWgt += CDec(dRow.Item("assigned_weight"))
            Next

            'S.SANDEEP [09 FEB 2015] -- START
            lvAssignedCompetencies.GroupingColumn = objcolhccategory
            lvAssignedCompetencies.DisplayGroups(True)

            If lvAssignedCompetencies.Groups.Count > 2 Then
                colhCompetencies.Width = 760 - 20
            Else
                colhCompetencies.Width = 760
            End If

            'If lvAssignedCompetencies.Items.Count > 10 Then
            '    colhCompetencies.Width = 760 - 20
            'Else
            '    colhCompetencies.Width = 760
            'End If
            'S.SANDEEP [09 FEB 2015] -- END

            Call Set_Totals_Label(xDecTotalWgt)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Competancy_List", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private Sub SetVisiblity()
        Try
            mnuSubmitApproval.Enabled = User._Object.Privilege._AllowtoSubmitGoalsforApproval
            mnuApproveSubmitted.Enabled = User._Object.Privilege._AllowtoApproveRejectGoalsPlanning
            mnuUnlockFinalSave.Enabled = User._Object.Privilege._AllowtoUnlockFinalSavedGoals
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisiblity", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [28 MAY 2015] -- END

#End Region

#Region " Form's Event(s) "

    Private Sub frmViewPerformancePlanning_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpField1 = New clsassess_empfield1_master
        objCmpt_Mast = New clsassess_competence_assign_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'SHANI [25 Mar 2015]-START
            'ENHANCEMENT : Claim AND Request CHANGES.
            'If ConfigParameter._Object._Self_Assign_Competencies = False Then
            '    tbcPlanningView.TabPages.Remove(tabpCompetencies)
            'End If
            'SHANI [25 Mar 2015]--END
            Call FillCombo()

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisiblity()
            'S.SANDEEP [28 MAY 2015] -- END


            If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                    objpnl2.BackColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                Else
                    objpnl2.BackColor = Color.Blue
                End If
            Else
                objpnl2.BackColor = Color.Blue
            End If

            If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                    objpnl1.BackColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                Else
                    objpnl1.BackColor = Color.Green
                End If
            Else
                objpnl1.BackColor = Color.Green
            End If

            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewPerformancePlanning_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_empfield1_master.SetMessages()
            clsassess_competence_assign_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_empfield1_master,clsassess_competence_assign_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Please select Employee and Period to view Performance Planning."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call Fill_Goals_Grid()

            'SHANI [25 Mar 2015]-START
            'ENHANCEMENT : Claim AND Request CHANGES.
            'If ConfigParameter._Object._Self_Assign_Competencies = True Then
            '    Call Fill_Competancy_List()
            'End If

            'S.SANDEEP [23 APR 2015] -- START
            'Call Fill_Competancy_List()
            Dim iOrdr() As String = ConfigParameter._Object._Perf_EvaluationOrder.Split("|")
            If iOrdr.Length > 0 Then
                For index As Integer = 0 To iOrdr.Length - 1
                    If iOrdr(index) = enEvaluationOrder.PE_COMPETENCY_SECTION Then
                        Call Fill_Competancy_List()
                    End If
                Next
            End If
            'S.SANDEEP [23 APR 2015] -- END


            'SHANI [25 Mar 2015]--END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0 : cboPeriod.SelectedValue = 0
            dgvBSCData.DataSource = Nothing : lvAssignedCompetencies.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmp.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = cboEmployee.DataSource
                If .DisplayDialog() Then
                    cboEmployee.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmp_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Context Menu Operation(s) "

    Private Sub mnuSubmitApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSubmitApproval.Click
        Try
            If dgvBSCData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period"), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP [09 FEB 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                If lvAssignedCompetencies.Items.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Performance Planning is not planned for selected employee and period for competencies."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'S.SANDEEP [09 FEB 2015] -- END

            'S.SANDEEP |10-AUG-2021| -- START
            'If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Green Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Performance Planning is already sent for approval for selected employee and period. ."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period. ."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.SUBMIT_APPROVAL), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.SUBMIT_APPROVAL)), Color.Green) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Performance Planning is already sent for approval for selected employee and period. ."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_SAVE)), Color.Blue) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period. ."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP |10-AUG-2021| -- END

            Dim objAssessor As New clsAssessor_tran
            Dim sMsg As String = String.Empty
            sMsg = objAssessor.IsAssessorPresent(CInt(cboEmployee.SelectedValue))
            If sMsg <> "" Then
                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                Exit Sub
            End If
            objAssessor = Nothing

            Dim objFMapping As New clsAssess_Field_Mapping
            sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
            If sMsg.Trim.Length > 0 Then
                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                objFMapping = Nothing
                Exit Sub
            End If
            If objFMapping IsNot Nothing Then objFMapping = Nothing


            'S.SANDEEP [10 FEB 2015] -- START
            If ConfigParameter._Object._Self_Assign_Competencies = True Then
                If mdtEmpCmpt IsNot Nothing AndAlso mdtEmpCmpt.Rows.Count > 0 Then
                    Dim xGrpWeight As Decimal = 0 : Dim xGrpId As Integer = -1
                    For Each xRow As DataRow In mdtEmpCmpt.Rows
                        If xGrpId <> xRow.Item("assessgroupunkid") Then
                            Dim objAGroup As New clsassess_group_master
                            objAGroup._Assessgroupunkid = xRow.Item("assessgroupunkid")
                            xGrpWeight = xGrpWeight + objAGroup._Weight
                            objAGroup = Nothing
                            xGrpId = xRow.Item("assessgroupunkid")
                        End If
                    Next
                    If CInt(mdtEmpCmpt.Compute("SUM(assigned_weight)", "")) <> xGrpWeight Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot Submit this Performance Planning for Approval, Reason Competencies are not adding upto assessment group weight."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
            'S.SANDEEP [10 FEB 2015] -- END

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You are about to Submit this Performance Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                      "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objEStatusTran As New clsassess_empstatus_tran
            objEStatusTran._Assessoremployeeunkid = 0
            objEStatusTran._Assessormasterunkid = 0
            objEStatusTran._Commtents = ""
            objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEStatusTran._Isunlock = False
            objEStatusTran._Loginemployeeunkid = 0
            objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
            objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objEStatusTran._Statustypeid = enObjective_Status.SUBMIT_APPROVAL
            objEStatusTran._Userunkid = User._Object._Userunkid
            With objEStatusTran
                ._FormName = mstrModuleName
                ._Loginemployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            If objEStatusTran.Insert() = False Then
                eZeeMsgBox.Show(objEStatusTran._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                If ConfigParameter._Object._ArutiSelfServiceURL.Trim.Length > 0 Then
                    Dim dtmp() As DataRow = CType(cboPeriod.DataSource, DataTable).Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'")
                    If dtmp.Length > 0 Then

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objEmpField1._FormName = mstrModuleName
                        objEmpField1._Loginemployeeunkid = 0
                        objEmpField1._ClientIP = getIP()
                        objEmpField1._HostName = getHostName()
                        objEmpField1._FromWeb = False
                        objEmpField1._AuditUserId = User._Object._Userunkid
objEmpField1._CompanyUnkid = Company._Object._Companyunkid
                        objEmpField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
                        '                                        CInt(dtmp(0).Item("yearunkid")), FinancialYear._Object._FinancialYear_Name, , , _
                        '                                        enLogin_Mode.DESKTOP, 0, 0)
                        objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
                                                               CInt(dtmp(0).Item("yearunkid")), FinancialYear._Object._FinancialYear_Name, FinancialYear._Object._DatabaseName, , , _
                                                                enLogin_Mode.DESKTOP, 0, 0)
                        'Sohail (21 Aug 2015) -- End
                    End If
                End If
                Call objbtnSearch_Click(New Object, New EventArgs)
            End If
            objEStatusTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitApproval_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuApproveSubmitted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproveSubmitted.Click
        Try
            If dgvBSCData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period"), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor <> Color.Green AndAlso dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor <> Color.Blue Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Performance Planning is yet not submitted for selected employee and period."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim iFinal As Boolean = False
            'S.SANDEEP |10-AUG-2021| -- START
            'If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Green Then
            '    iFinal = False
            'ElseIf dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
            '    iFinal = True
            'End If
            If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_SAVE)), Color.Blue) Then
                iFinal = True
            End If
            'S.SANDEEP |10-AUG-2021| -- END

            If iFinal = True Then
                Dim objAnalysis_Master As New clsevaluation_analysis_master
                If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objAnalysis_Master = Nothing
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You are about to Approve/Disapprove this Performance Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
                                                       "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim frm As New frmApproveRejectPlanning

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim iDisplayText As String = String.Empty
            iDisplayText = cboEmployee.Text & ", " & Language.getMessage(mstrModuleName, 13, "For Period : ") & " " & cboPeriod.Text
            If frm.displayDialog(iDisplayText, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), iFinal, mdtEmpGoal, mdtEmpCmpt) Then
                objbtnSearch_Click(New Object, New EventArgs)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApproveSubmitted_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuUnlockFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockFinalSave.Click
        Try
            If dgvBSCData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period"), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dgvBSCData.SelectedRows(0).DefaultCellStyle.ForeColor <> Color.Blue Then
                'S.SANDEEP |12-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Performance Planning is not yet final saved for selected employee and period."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, Performance Planning is not yet final approved for selected employee and period."), enMsgBoxStyle.Information)
                'S.SANDEEP |12-MAR-2019| -- END
                Exit Sub
            End If

            Dim objAnalysis_Master As New clsevaluation_analysis_master
            If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objAnalysis_Master = Nothing


            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            'Dim xMsg As String = ""
            'xMsg = (New clsassess_empfield1_master).IsGoalsAssignedSubEmployee(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
            'If xMsg.Trim.Length > 0 Then
            '    eZeeMsgBox.Show(xMsg, enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'S.SANDEEP |27-NOV-2020| -- END


            Dim iMsgText As String = ""
            iMsgText = Language.getMessage(mstrModuleName, 15, "You are about to unlock the employee performance planning. This will result in opening of all operation on employee performance planning." & vbCrLf & _
                       "Do you wish to continue?")

            If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim iUnlockComments As String = ""

            iUnlockComments = InputBox(Language.getMessage(mstrModuleName, 16, "Enter reason to unlock"), Me.Text)

            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            Dim blnVoidProgress As Boolean = False
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "You are about to unlock the employee performance planning. Do you want to void approved progress update(s)?"), _
                                           CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                blnVoidProgress = True
            End If
            'S.SANDEEP |09-JUL-2019| -- END

            If iUnlockComments.Trim.Length > 0 Then
                Dim objEStatusTran As New clsassess_empstatus_tran
                objEStatusTran._Assessoremployeeunkid = 0
                objEStatusTran._Assessormasterunkid = 0
                objEStatusTran._Commtents = iUnlockComments
                objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEStatusTran._Isunlock = True
                objEStatusTran._Loginemployeeunkid = 0
                objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                objEStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
                objEStatusTran._Userunkid = User._Object._Userunkid

                With objEStatusTran
                    ._FormName = mstrModuleName
                    ._Loginemployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                'S.SANDEEP |09-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES
                'If objEStatusTran.Insert(, ConfigParameter._Object._Self_Assign_Competencies) = False Then 'S.SANDEEP [10 FEB 2015] -- START {_Self_Assign_Competencies} -- END
                If objEStatusTran.Insert(, ConfigParameter._Object._Self_Assign_Competencies, blnVoidProgress) = False Then 'S.SANDEEP [10 FEB 2015] -- START {_Self_Assign_Competencies} -- END
                    'S.SANDEEP |09-JUL-2019| -- END
                    eZeeMsgBox.Show(objEStatusTran._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    'S.SANDEEP [25-JAN-2017] -- START
                    'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
                    Dim objEvalutionMst As New clsevaluation_analysis_master : Dim sAssessorName As String
                    Dim dLst As DataSet = objEvalutionMst.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                                               User._Object._Userunkid, _
                                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                               True, False, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, _
                                                                               False, False, CInt(cboEmployee.SelectedValue))

                    sAssessorName = ""
                    If dLst IsNot Nothing AndAlso dLst.Tables(0).Rows.Count > 0 Then
                        sAssessorName = CStr(dLst.Tables(0).Rows(0).Item("Name"))
                    End If
                    objEvalutionMst = Nothing
                    Dim objassess_Empfield1Mst As New clsassess_empfield1_master
                    With objassess_Empfield1Mst
                        ._FormName = mstrModuleName
                        ._Loginemployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objassess_Empfield1Mst.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, iUnlockComments, False, True)

                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'objassess_Empfield1Mst.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, iUnlockComments, False, True, Company._Object._Companyunkid, , , ConfigParameter._Object._Ntf_GoalsUnlockUserIds)

                    'S.SANDEEP |22-JUN-2021| -- START
                    'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
                    'objassess_Empfield1Mst.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, iUnlockComments, False, True, Company._Object._Companyunkid, , , ConfigParameter._Object._Ntf_GoalsUnlockUserIds, blnVoidProgress)
                    objassess_Empfield1Mst.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, iUnlockComments, False, True, Company._Object._Companyunkid, , , , ConfigParameter._Object._Ntf_GoalsUnlockUserIds, blnVoidProgress)
                    'S.SANDEEP |22-JUN-2021| -- END

                    'S.SANDEEP |09-JUL-2019| -- END

                    'Sohail (30 Nov 2017) -- End
                    objassess_Empfield1Mst = Nothing
                    'S.SANDEEP [25-JAN-2017] -- END

                    objbtnSearch_Click(New Object, New EventArgs)
                End If
                objEStatusTran = Nothing

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlockFinalSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.mnuSubmitApproval.Text = Language._Object.getCaption(Me.mnuSubmitApproval.Name, Me.mnuSubmitApproval.Text)
            Me.mnuApproveSubmitted.Text = Language._Object.getCaption(Me.mnuApproveSubmitted.Name, Me.mnuApproveSubmitted.Text)
            Me.mnuFinalSave.Text = Language._Object.getCaption(Me.mnuFinalSave.Name, Me.mnuFinalSave.Text)
            Me.mnuUnlockFinalSave.Text = Language._Object.getCaption(Me.mnuUnlockFinalSave.Name, Me.mnuUnlockFinalSave.Text)
            Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)
            Me.mnuUpdatePercentage.Text = Language._Object.getCaption(Me.mnuUpdatePercentage.Name, Me.mnuUpdatePercentage.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
            Me.mnuImportBSC_Planning.Text = Language._Object.getCaption(Me.mnuImportBSC_Planning.Name, Me.mnuImportBSC_Planning.Text)
            Me.mnuPrintBSCForm.Text = Language._Object.getCaption(Me.mnuPrintBSCForm.Name, Me.mnuPrintBSCForm.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
            Me.lblCaption2.Text = Language._Object.getCaption(Me.lblCaption2.Name, Me.lblCaption2.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tabpBSC.Text = Language._Object.getCaption(Me.tabpBSC.Name, Me.tabpBSC.Text)
            Me.tabpCompetencies.Text = Language._Object.getCaption(Me.tabpCompetencies.Name, Me.tabpCompetencies.Text)
            Me.colhCompetencies.Text = Language._Object.getCaption(CStr(Me.colhCompetencies.Tag), Me.colhCompetencies.Text)
            Me.colhWeight.Text = Language._Object.getCaption(CStr(Me.colhWeight.Tag), Me.colhWeight.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, No data defined for assessment caption settings screen.")
            Language.setMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period.")
            Language.setMessage(mstrModuleName, 3, "Total Goals Weight :")
            Language.setMessage(mstrModuleName, 4, "Total Competency Weight :")
            Language.setMessage(mstrModuleName, 5, "Sorry, Please select Employee and Period to view Performance Planning.")
            Language.setMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period")
            Language.setMessage(mstrModuleName, 7, "Sorry, Performance Planning is already sent for approval for selected employee and period. .")
            Language.setMessage(mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period. .")
            Language.setMessage(mstrModuleName, 9, "You are about to Submit this Performance Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                               "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 10, "Sorry, Performance Planning is yet not submitted for selected employee and period.")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period")
            Language.setMessage(mstrModuleName, 12, "You are about to Approve/Disapprove this Performance Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
                                                                "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 13, "For Period :")
            Language.setMessage(mstrModuleName, 100, "Sorry, Performance Planning is not yet final approved for selected employee and period.")
            Language.setMessage(mstrModuleName, 15, "You are about to unlock the employee performance planning. This will result in opening of all operation on employee performance planning." & vbCrLf & _
                                "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 16, "Enter reason to unlock")
            Language.setMessage(mstrModuleName, 17, "Performance Planning is not planned for selected employee and period for competencies.")
            Language.setMessage(mstrModuleName, 18, "Sorry, you cannot Submit this Performance Planning for Approval, Reason Competencies are not adding upto assessment group weight.")
            Language.setMessage(mstrModuleName, 19, "Sorry, there is no list of competencies assigned to this employee or some defined assessment group does not fall in the allocation of the selected employee.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class