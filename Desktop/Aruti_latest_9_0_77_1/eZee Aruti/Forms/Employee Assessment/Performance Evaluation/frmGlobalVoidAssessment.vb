﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmGlobalVoidAssessment

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalVoidAssessment"
    Private objEvaluation As clsevaluation_analysis_master
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtEmployee As DataTable
    Private mViewEmp As DataView
    'Shani (24-May-2016) -- Start
    Private objComputeScore As New clsComputeScore_master
    'Shani (24-May-2016) -- End
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub Fill_Employee()
        Try
            Dim dsList As New DataSet : Dim objEmployee As New clsEmployee_Master
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("List", False, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , mstrAdvanceFilter)
            'Else
            '    dsList = objEmployee.GetEmployeeList("List", False, , , , , , , , , , , , , , , , , , , mstrAdvanceFilter)
            'End If

            'S.SANDEEP |31-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-886|Ref#0004030}
            'dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     ConfigParameter._Object._UserAccessModeSetting, _
            '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", False)
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", False, , , , , , , , , , , , , , , mstrAdvanceFilter)
            'S.SANDEEP |31-JUL-2019| -- END


            'Shani(18-Feb-2016) -- [True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)]-->[True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", False)]

            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP |10-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            Dim objScoreCalibration As New clsScoreCalibrationApproval
            Dim oTable As DataTable = objScoreCalibration.GetList(CInt(cboPeriod.SelectedValue), FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, False, "List")
            objScoreCalibration = Nothing
            Dim xCol As DataColumn = Nothing
            xCol = New DataColumn
            With xCol
                .ColumnName = "iRead"
                .DataType = GetType(Int32)
                .DefaultValue = 0
            End With
            dsList.Tables(0).Columns.Add(xCol)
            xCol = New DataColumn
            With xCol
                .ColumnName = "iMsg"
                .DataType = GetType(String)
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(xCol)
            If oTable IsNot Nothing AndAlso oTable.Rows.Count > 0 Then
                oTable = oTable.DefaultView.ToTable(True, "employeeunkid")
                oTable.AsEnumerable().ToList().ForEach(Function(x) UpdateValue(x, dsList.Tables(0)))
            End If
            dsList.Tables(0).AcceptChanges()
            'S.SANDEEP |10-OCT-2019| -- END

            mdtEmployee = dsList.Tables(0)
            mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            mViewEmp = mdtEmployee.DefaultView

            dgvAEmployee.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"

            'S.SANDEEP |10-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            objdgcolhiRead.DataPropertyName = "iRead"
            dgcolhiMsg.DataPropertyName = "iMsg"
            'S.SANDEEP |10-OCT-2019| -- END

            dgvAEmployee.DataSource = mViewEmp

            'S.SANDEEP |10-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            Dim dgvRows As IEnumerable(Of DataGridViewRow) = dgvAEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CInt(x.Cells(objdgcolhiRead.Index).Value) = 1)
            If dgvRows IsNot Nothing AndAlso dgvRows.Count > 0 Then
                For Each dgvRow In dgvRows
                    dgvRow.DefaultCellStyle.ForeColor = Color.Red
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Red
                    dgvRow.DefaultCellStyle.Font = New Font(dgvAEmployee.Font, FontStyle.Bold)
                    dgvRow.Cells(objdgcolhECheck.Index).ToolTipText = dgvRow.Cells(dgcolhiMsg.Index).Value.ToString()
                    dgvRow.Cells(dgcolhEcode.Index).ToolTipText = dgvRow.Cells(dgcolhiMsg.Index).Value.ToString()
                    dgvRow.Cells(dgcolhEName.Index).ToolTipText = dgvRow.Cells(dgcolhiMsg.Index).Value.ToString()
                    dgvRow.Cells(dgcolhiMsg.Index).ToolTipText = dgvRow.Cells(dgcolhiMsg.Index).Value.ToString()
                    dgvRow.ReadOnly = True
                Next
            End If
            'S.SANDEEP |10-OCT-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |10-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Public Function UpdateValue(ByVal dr As DataRow, ByVal dtable As DataTable) As Boolean
        Try
            Dim itmp() As DataRow = dtable.Select("employeeunkid = '" & dr.Item("employeeunkid").ToString() & "'")
            If itmp.Length > 0 Then
                itmp(0)("iRead") = 1
                itmp(0)("iMsg") = Language.getMessage("clsComputeScore_master", 200, "Under Calibration")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateValue", mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP |10-OCT-2019| -- END

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub Error_Employee(ByVal strEmployeeName As String, ByVal eMode As enAssessmentMode, ByVal strErrMsg As String)
        Try
            Dim lstItem As New ListViewItem
            lstItem.Text = strEmployeeName
            Select Case eMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    lstItem.SubItems.Add(Language.getMessage(mstrModuleName, 3, "Self"))
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    lstItem.SubItems.Add(Language.getMessage(mstrModuleName, 4, "Assessor"))
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    lstItem.SubItems.Add(Language.getMessage(mstrModuleName, 5, "Reviewer"))
            End Select
            lstItem.ForeColor = Color.Red
            lstItem.SubItems.Add(strErrMsg)
            lvError.Items.Add(lstItem)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Error_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmGlobalVoidAssessment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEvaluation = New clsevaluation_analysis_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            FillCombo()
            Call objbtnReset_Click(New Object, New EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalVoidAssessment_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsevaluation_analysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsevaluation_analysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            lvError.Items.Clear()
            Dim blnError As Boolean = False
            If chkSelfAssessment.Checked = False AndAlso _
               chkAssessorAssessment.Checked = False AndAlso _
               chkReviewerAssessment.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please select atleast one of the delete operation to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            mdtEmployee.AcceptChanges()

            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck = true")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Please select atleast one of the employee to void evaluation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim frm As New frmReasonSelection
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim strVoidReason As String = String.Empty
            frm.displayDialog(enVoidCategoryType.ASSESSMENT, strVoidReason)
            If frm IsNot Nothing Then frm.Dispose()
            If strVoidReason.Trim.Length > 0 Then

                objEvaluation._Isvoid = True
                objEvaluation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objEvaluation._Voidreason = strVoidReason
                objEvaluation._Voiduserunkid = User._Object._Userunkid

                'Shani (24-May-2016) -- Start
                objComputeScore._Isvoid = True
                objComputeScore._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objComputeScore._Voidreason = strVoidReason
                objComputeScore._Voiduserunkid = User._Object._Userunkid
                'Shani (24-May-2016) -- End


                Dim intEvalutionId As Integer = 0
                Dim dsEvalution As New DataSet
                For index As Integer = 0 To dtmp.Length - 1

                    'Shani(18-Feb-2016) -- Start

                    'If chkSelfAssessment.Checked = True Then
                    '    dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '    For Each row As DataRow In dsEvalution.Tables(0).Rows
                    '        objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '        If objEvaluation._Message <> "" Then
                    '            blnError = True
                    '            Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.SELF_ASSESSMENT, objEvaluation._Message)
                    '        End If
                    '    Next

                    '    If blnError = False Then
                    '        dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '        For Each row As DataRow In dsEvalution.Tables(0).Rows
                    '            objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '            If objEvaluation._Message <> "" Then
                    '                blnError = True
                    '                Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.APPRAISER_ASSESSMENT, objEvaluation._Message)
                    '            End If
                    '        Next
                    '    End If

                    '    If blnError = False Then
                    '        dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '        For Each row As DataRow In dsEvalution.Tables(0).Rows
                    '            objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '            If objEvaluation._Message <> "" Then
                    '                blnError = True
                    '                Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.REVIEWER_ASSESSMENT, objEvaluation._Message)
                    '            End If
                    '        Next
                    '    End If

                    'ElseIf chkAssessorAssessment.Checked = True Then

                    '    dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '    For Each row As DataRow In dsEvalution.Tables(0).Rows
                    '        objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '        If objEvaluation._Message <> "" Then
                    '            blnError = True
                    '            Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.APPRAISER_ASSESSMENT, objEvaluation._Message)
                    '        End If
                    '    Next
                    '    If blnError = False Then
                    '        dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '        For Each row As DataRow In dsEvalution.Tables(0).Rows
                    '            objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '            If objEvaluation._Message <> "" Then
                    '                blnError = True
                    '                Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.REVIEWER_ASSESSMENT, objEvaluation._Message)
                    '                Continue For
                    '            End If
                    '        Next
                    '    End If

                    'ElseIf chkReviewerAssessment.Checked = True Then
                    '    dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '    For Each row As DataRow In dsEvalution.Tables(0).Rows
                    '        objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                    '        If objEvaluation._Message <> "" Then
                    '            blnError = True
                    '            Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.REVIEWER_ASSESSMENT, objEvaluation._Message)
                    '            Continue For
                    '        End If
                    '    Next
                    'End If

                    If chkReviewerAssessment.Checked = True Then
                        dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                        For Each row As DataRow In dsEvalution.Tables(0).Rows

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEvaluation._FormName = mstrModuleName
                            objEvaluation._LoginEmployeeunkid = 0
                            objEvaluation._ClientIP = getIP()
                            objEvaluation._HostName = getHostName()
                            objEvaluation._FromWeb = False
                            objEvaluation._AuditUserId = User._Object._Userunkid
objEvaluation._CompanyUnkid = Company._Object._Companyunkid
                            objEvaluation._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                            If objEvaluation._Message <> "" Then
                                blnError = True
                                Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.REVIEWER_ASSESSMENT, objEvaluation._Message)
                            End If
                            'Shani (24-May-2016) -- Start
                            Try
                                objComputeScore.DeleteEmployeeWise(CInt(row.Item("analysisunkid")), CInt(dtmp(index).Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT)
                            Catch ex As Exception
                                Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.REVIEWER_ASSESSMENT, ex.Message)
                            End Try
                            'Shani (24-May-2016) -- End
                        Next
                    End If
                    If chkAssessorAssessment.Checked = True Then
                        If blnError = False Then
                            dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                            For Each row As DataRow In dsEvalution.Tables(0).Rows

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objEvaluation._FormName = mstrModuleName
                                objEvaluation._LoginEmployeeunkid = 0
                                objEvaluation._ClientIP = getIP()
                                objEvaluation._HostName = getHostName()
                                objEvaluation._FromWeb = False
                                objEvaluation._AuditUserId = User._Object._Userunkid
objEvaluation._CompanyUnkid = Company._Object._Companyunkid
                                objEvaluation._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                'S.SANDEEP [28-May-2018] -- END

                                objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                                If objEvaluation._Message <> "" Then
                                    blnError = True
                                    Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.APPRAISER_ASSESSMENT, objEvaluation._Message)
                                End If
                                'Shani (24-May-2016) -- Start
                                Try
                                    objComputeScore.DeleteEmployeeWise(CInt(row.Item("analysisunkid")), CInt(dtmp(index).Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.APPRAISER_ASSESSMENT)
                                Catch ex As Exception
                                    Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.APPRAISER_ASSESSMENT, ex.Message)
                                End Try
                                'Shani (24-May-2016) -- End
                            Next
                        End If
                    End If
                    If chkSelfAssessment.Checked = True Then
                        If blnError = False Then
                            dsEvalution = objEvaluation.GetEvaluationUnkid(CInt(dtmp(index).Item("employeeunkid")), enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                            For Each row As DataRow In dsEvalution.Tables(0).Rows

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objEvaluation._FormName = mstrModuleName
                                objEvaluation._LoginEmployeeunkid = 0
                                objEvaluation._ClientIP = getIP()
                                objEvaluation._HostName = getHostName()
                                objEvaluation._FromWeb = False
                                objEvaluation._AuditUserId = User._Object._Userunkid
objEvaluation._CompanyUnkid = Company._Object._Companyunkid
                                objEvaluation._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                'S.SANDEEP [28-May-2018] -- END

                                objEvaluation.Delete(CInt(row.Item("analysisunkid")), enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                                If objEvaluation._Message <> "" Then
                                    blnError = True
                                    Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.SELF_ASSESSMENT, objEvaluation._Message)
                                End If
                                'Shani (24-May-2016) -- Start
                                Try
                                    objComputeScore.DeleteEmployeeWise(CInt(row.Item("analysisunkid")), CInt(dtmp(index).Item("employeeunkid")), CInt(cboPeriod.SelectedValue), enAssessmentMode.SELF_ASSESSMENT)
                                Catch ex As Exception
                                    Call Error_Employee(dtmp(index).Item("employeecode").ToString() & " - " & dtmp(index).Item("employeename").ToString(), enAssessmentMode.SELF_ASSESSMENT, ex.Message)
                                End Try
                                'Shani (24-May-2016) -- End
                            Next

                        End If
                    End If
                    'Shani(15-Feb-2016) -- End
                Next

                'Shani(18-APR-2016) -- Start
            Else
                Exit Sub
                'Shani(18-APR-2016) -- End 
            End If
            If blnError = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, some of the deleted operation did not complete successfully, Please review the list of the failed employee(s)."), enMsgBoxStyle.Information)
                gbFilter.Enabled = False
                gbOperation.Enabled = False
                tblpAssessorEmployee.Enabled = False
                objgbView.Visible = True
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Delete operation completed successfully."), enMsgBoxStyle.Information)
                Call objbtnReset_Click(New Object, New EventArgs)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            chkSelfAssessment.Checked = False
            chkAssessorAssessment.Checked = False
            chkReviewerAssessment.Checked = False
            chkIncludeCommitted.Checked = False
            mstrAdvanceFilter = String.Empty
            dgvAEmployee.DataSource = Nothing
            txtSearchEmp.Text = ""
            objchkEmployee.Checked = False
            tblpAssessorEmployee.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Period is mandatory information. Please select period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call Fill_Employee()
            tblpAssessorEmployee.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " CheckBox Event "

    Private Sub chkSelfAssessment_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelfAssessment.CheckedChanged
        Try
            chkAssessorAssessment.Checked = chkSelfAssessment.Checked : chkReviewerAssessment.Checked = chkSelfAssessment.Checked
            If chkSelfAssessment.Checked = True Then
                chkAssessorAssessment.Enabled = Not chkSelfAssessment.Checked : chkReviewerAssessment.Enabled = Not chkSelfAssessment.Checked
            ElseIf chkSelfAssessment.Checked = False Then
                chkAssessorAssessment.Enabled = Not chkSelfAssessment.Checked : chkReviewerAssessment.Enabled = Not chkSelfAssessment.Checked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelfAssessment_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkAssessorAssessment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAssessorAssessment.CheckedChanged
        Try
            chkReviewerAssessment.Checked = chkAssessorAssessment.Checked
            If chkReviewerAssessment.Checked = True Then
                chkReviewerAssessment.Enabled = Not chkAssessorAssessment.Checked
            ElseIf chkSelfAssessment.Checked = False Then
                chkReviewerAssessment.Enabled = Not chkAssessorAssessment.Checked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAssessorAssessment_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In mViewEmp
                'S.SANDEEP |10-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                If CBool(dr("iRead")) = True Then Continue For
                'S.SANDEEP |10-OCT-2019| -- END
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            mViewEmp.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event "

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = mViewEmp.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If mViewEmp.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Events "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub objlnkClose_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles objlnkClose.LinkClicked
        Try
            objgbView.Visible = False
            gbFilter.Enabled = True
            gbOperation.Enabled = True
            tblpAssessorEmployee.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlnkClose_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkCopy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopy.LinkClicked
        Try
            If lvError.Items.Count > 0 Then
                Clipboard.Clear()
                Dim buffer As New StringBuilder()
                For i As Integer = 0 To lvError.Columns.Count - 1
                    buffer.Append(lvError.Columns(i).Text)
                    buffer.Append(vbTab)
                Next
                buffer.Append(vbLf)
                For i As Integer = 0 To lvError.Items.Count - 1

                    For j As Integer = 0 To lvError.Columns.Count - 1
                        buffer.Append(lvError.Items(i).SubItems(j).Text)
                        buffer.Append(vbTab)
                    Next
                    buffer.Append(vbLf)
                Next
                Clipboard.SetText(buffer.ToString())
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopy_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOperation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOperation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.objgbView.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbView.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
            Me.gbOperation.Text = Language._Object.getCaption(Me.gbOperation.Name, Me.gbOperation.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.chkIncludeCommitted.Text = Language._Object.getCaption(Me.chkIncludeCommitted.Name, Me.chkIncludeCommitted.Text)
            Me.chkReviewerAssessment.Text = Language._Object.getCaption(Me.chkReviewerAssessment.Name, Me.chkReviewerAssessment.Text)
            Me.chkAssessorAssessment.Text = Language._Object.getCaption(Me.chkAssessorAssessment.Name, Me.chkAssessorAssessment.Text)
            Me.chkSelfAssessment.Text = Language._Object.getCaption(Me.chkSelfAssessment.Name, Me.chkSelfAssessment.Text)
            Me.colhEName.Text = Language._Object.getCaption(CStr(Me.colhEName.Tag), Me.colhEName.Text)
            Me.colhAssessmentType.Text = Language._Object.getCaption(CStr(Me.colhAssessmentType.Tag), Me.colhAssessmentType.Text)
            Me.lnkCopy.Text = Language._Object.getCaption(Me.lnkCopy.Name, Me.lnkCopy.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please select atleast one of the delete operation to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Please select atleast one of the employee to void evaluation.")
            Language.setMessage(mstrModuleName, 3, "Self")
            Language.setMessage(mstrModuleName, 4, "Assessor")
            Language.setMessage(mstrModuleName, 5, "Reviewer")
            Language.setMessage(mstrModuleName, 6, "Sorry, some of the deleted operation did not complete successfully, Please review the list of the failed employee(s).")
            Language.setMessage(mstrModuleName, 7, "Delete operation completed successfully.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Period is mandatory information. Please select period to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class