﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssess_SubItemAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssess_SubItemAddEdit))
        Me.pnlAssessmentItems = New System.Windows.Forms.Panel
        Me.gbAssessSubItems = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.tabcSubItems = New System.Windows.Forms.TabControl
        Me.tabpName = New System.Windows.Forms.TabPage
        Me.txtAssessmentName = New eZee.TextBox.AlphanumericTextBox
        Me.tabpDescription = New System.Windows.Forms.TabPage
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.cboGroup = New System.Windows.Forms.ComboBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.objbtnSearchItems = New eZee.Common.eZeeGradientButton
        Me.objbtnAddAssessItem = New eZee.Common.eZeeGradientButton
        Me.cboAssessmentItem = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.txtItemCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblAssessmentItemCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.pnlAssessmentItems.SuspendLayout()
        Me.gbAssessSubItems.SuspendLayout()
        Me.tabcSubItems.SuspendLayout()
        Me.tabpName.SuspendLayout()
        Me.tabpDescription.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAssessmentItems
        '
        Me.pnlAssessmentItems.Controls.Add(Me.gbAssessSubItems)
        Me.pnlAssessmentItems.Controls.Add(Me.objFooter)
        Me.pnlAssessmentItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAssessmentItems.Location = New System.Drawing.Point(0, 0)
        Me.pnlAssessmentItems.Name = "pnlAssessmentItems"
        Me.pnlAssessmentItems.Size = New System.Drawing.Size(429, 310)
        Me.pnlAssessmentItems.TabIndex = 1
        '
        'gbAssessSubItems
        '
        Me.gbAssessSubItems.BorderColor = System.Drawing.Color.Black
        Me.gbAssessSubItems.Checked = False
        Me.gbAssessSubItems.CollapseAllExceptThis = False
        Me.gbAssessSubItems.CollapsedHoverImage = Nothing
        Me.gbAssessSubItems.CollapsedNormalImage = Nothing
        Me.gbAssessSubItems.CollapsedPressedImage = Nothing
        Me.gbAssessSubItems.CollapseOnLoad = False
        Me.gbAssessSubItems.Controls.Add(Me.cboPeriod)
        Me.gbAssessSubItems.Controls.Add(Me.lblPeriod)
        Me.gbAssessSubItems.Controls.Add(Me.cboYear)
        Me.gbAssessSubItems.Controls.Add(Me.lblYear)
        Me.gbAssessSubItems.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbAssessSubItems.Controls.Add(Me.tabcSubItems)
        Me.gbAssessSubItems.Controls.Add(Me.objbtnAddGroup)
        Me.gbAssessSubItems.Controls.Add(Me.cboGroup)
        Me.gbAssessSubItems.Controls.Add(Me.lblGroup)
        Me.gbAssessSubItems.Controls.Add(Me.objbtnSearchItems)
        Me.gbAssessSubItems.Controls.Add(Me.objbtnAddAssessItem)
        Me.gbAssessSubItems.Controls.Add(Me.cboAssessmentItem)
        Me.gbAssessSubItems.Controls.Add(Me.lblResultGroup)
        Me.gbAssessSubItems.Controls.Add(Me.txtItemCode)
        Me.gbAssessSubItems.Controls.Add(Me.lblAssessmentItemCode)
        Me.gbAssessSubItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssessSubItems.ExpandedHoverImage = Nothing
        Me.gbAssessSubItems.ExpandedNormalImage = Nothing
        Me.gbAssessSubItems.ExpandedPressedImage = Nothing
        Me.gbAssessSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssessSubItems.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssessSubItems.HeaderHeight = 25
        Me.gbAssessSubItems.HeaderMessage = ""
        Me.gbAssessSubItems.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAssessSubItems.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssessSubItems.HeightOnCollapse = 0
        Me.gbAssessSubItems.LeftTextSpace = 0
        Me.gbAssessSubItems.Location = New System.Drawing.Point(0, 0)
        Me.gbAssessSubItems.Name = "gbAssessSubItems"
        Me.gbAssessSubItems.OpenHeight = 119
        Me.gbAssessSubItems.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssessSubItems.ShowBorder = True
        Me.gbAssessSubItems.ShowCheckBox = False
        Me.gbAssessSubItems.ShowCollapseButton = False
        Me.gbAssessSubItems.ShowDefaultBorderColor = True
        Me.gbAssessSubItems.ShowDownButton = False
        Me.gbAssessSubItems.ShowHeader = True
        Me.gbAssessSubItems.Size = New System.Drawing.Size(429, 255)
        Me.gbAssessSubItems.TabIndex = 0
        Me.gbAssessSubItems.Temp = 0
        Me.gbAssessSubItems.Text = "Assessment Sub Items"
        Me.gbAssessSubItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(375, 141)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'tabcSubItems
        '
        Me.tabcSubItems.Controls.Add(Me.tabpName)
        Me.tabcSubItems.Controls.Add(Me.tabpDescription)
        Me.tabcSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcSubItems.Location = New System.Drawing.Point(11, 141)
        Me.tabcSubItems.Name = "tabcSubItems"
        Me.tabcSubItems.SelectedIndex = 0
        Me.tabcSubItems.Size = New System.Drawing.Size(358, 106)
        Me.tabcSubItems.TabIndex = 224
        '
        'tabpName
        '
        Me.tabpName.Controls.Add(Me.txtAssessmentName)
        Me.tabpName.Location = New System.Drawing.Point(4, 22)
        Me.tabpName.Name = "tabpName"
        Me.tabpName.Size = New System.Drawing.Size(350, 80)
        Me.tabpName.TabIndex = 0
        Me.tabpName.Text = "Name"
        Me.tabpName.UseVisualStyleBackColor = True
        '
        'txtAssessmentName
        '
        Me.txtAssessmentName.BackColor = System.Drawing.SystemColors.Window
        Me.txtAssessmentName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAssessmentName.Flags = 0
        Me.txtAssessmentName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAssessmentName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAssessmentName.Location = New System.Drawing.Point(0, 0)
        Me.txtAssessmentName.Multiline = True
        Me.txtAssessmentName.Name = "txtAssessmentName"
        Me.txtAssessmentName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAssessmentName.Size = New System.Drawing.Size(350, 80)
        Me.txtAssessmentName.TabIndex = 3
        '
        'tabpDescription
        '
        Me.tabpDescription.Controls.Add(Me.txtDescription)
        Me.tabpDescription.Location = New System.Drawing.Point(4, 22)
        Me.tabpDescription.Name = "tabpDescription"
        Me.tabpDescription.Size = New System.Drawing.Size(229, 61)
        Me.tabpDescription.TabIndex = 1
        Me.tabpDescription.Text = "Description"
        Me.tabpDescription.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(229, 61)
        Me.txtDescription.TabIndex = 4
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(375, 33)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 222
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.DropDownWidth = 450
        Me.cboGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(110, 33)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(259, 21)
        Me.cboGroup.TabIndex = 0
        '
        'lblGroup
        '
        Me.lblGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(97, 15)
        Me.lblGroup.TabIndex = 221
        Me.lblGroup.Text = "Assessment Group"
        Me.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchItems
        '
        Me.objbtnSearchItems.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchItems.BorderSelected = False
        Me.objbtnSearchItems.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchItems.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchItems.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchItems.Location = New System.Drawing.Point(399, 87)
        Me.objbtnSearchItems.Name = "objbtnSearchItems"
        Me.objbtnSearchItems.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchItems.TabIndex = 218
        '
        'objbtnAddAssessItem
        '
        Me.objbtnAddAssessItem.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAssessItem.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAssessItem.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAssessItem.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAssessItem.BorderSelected = False
        Me.objbtnAddAssessItem.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAssessItem.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddAssessItem.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAssessItem.Location = New System.Drawing.Point(375, 87)
        Me.objbtnAddAssessItem.Name = "objbtnAddAssessItem"
        Me.objbtnAddAssessItem.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAssessItem.TabIndex = 216
        '
        'cboAssessmentItem
        '
        Me.cboAssessmentItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessmentItem.DropDownWidth = 450
        Me.cboAssessmentItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessmentItem.FormattingEnabled = True
        Me.cboAssessmentItem.Location = New System.Drawing.Point(110, 87)
        Me.cboAssessmentItem.Name = "cboAssessmentItem"
        Me.cboAssessmentItem.Size = New System.Drawing.Size(259, 21)
        Me.cboAssessmentItem.TabIndex = 1
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(8, 90)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(97, 15)
        Me.lblResultGroup.TabIndex = 214
        Me.lblResultGroup.Text = "Assessment Item"
        Me.lblResultGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtItemCode
        '
        Me.txtItemCode.Flags = 0
        Me.txtItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtItemCode.Location = New System.Drawing.Point(110, 114)
        Me.txtItemCode.Name = "txtItemCode"
        Me.txtItemCode.Size = New System.Drawing.Size(259, 21)
        Me.txtItemCode.TabIndex = 2
        '
        'lblAssessmentItemCode
        '
        Me.lblAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItemCode.Location = New System.Drawing.Point(8, 117)
        Me.lblAssessmentItemCode.Name = "lblAssessmentItemCode"
        Me.lblAssessmentItemCode.Size = New System.Drawing.Size(97, 15)
        Me.lblAssessmentItemCode.TabIndex = 2
        Me.lblAssessmentItemCode.Text = "Code"
        Me.lblAssessmentItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 255)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(429, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(217, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(320, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(249, 60)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(120, 21)
        Me.cboPeriod.TabIndex = 229
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(191, 62)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(52, 15)
        Me.lblPeriod.TabIndex = 228
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 200
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(110, 60)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(75, 21)
        Me.cboYear.TabIndex = 227
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(8, 63)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(97, 15)
        Me.lblYear.TabIndex = 226
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmAssess_SubItemAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(429, 310)
        Me.Controls.Add(Me.pnlAssessmentItems)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssess_SubItemAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Assessment Sub Items"
        Me.pnlAssessmentItems.ResumeLayout(False)
        Me.gbAssessSubItems.ResumeLayout(False)
        Me.gbAssessSubItems.PerformLayout()
        Me.tabcSubItems.ResumeLayout(False)
        Me.tabpName.ResumeLayout(False)
        Me.tabpName.PerformLayout()
        Me.tabpDescription.ResumeLayout(False)
        Me.tabpDescription.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlAssessmentItems As System.Windows.Forms.Panel
    Friend WithEvents gbAssessSubItems As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnAddAssessItem As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAssessmentItem As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAssessmentName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtItemCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAssessmentItemCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchItems As eZee.Common.eZeeGradientButton
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents tabcSubItems As System.Windows.Forms.TabControl
    Friend WithEvents tabpName As System.Windows.Forms.TabPage
    Friend WithEvents tabpDescription As System.Windows.Forms.TabPage
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
End Class
