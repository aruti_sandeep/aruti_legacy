﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmGroup_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmGroup_AddEdit"
    Private mblnCancel As Boolean = True
    Private objAssessGroupmaster As clsassess_group_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAssessGroupunkid As Integer = -1
    Private mdtTran As DataTable
    Private dgView As DataView
    Private mstrAllocation As String = ""
    Dim dtTemp() As DataRow = Nothing
    Private mstrAllocationIds As String = String.Empty
    Private mintReferenceUnkid As Integer = -1
    'S.SANDEEP [ 09 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecAssignedWeight As Decimal = 0
    'S.SANDEEP [ 09 AUG 2013 ] -- END

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Dim mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 05 NOV 2014 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintAssessGroupunkid = intUnkId
            menAction = eAction

            If menAction <> enAction.EDIT_ONE Then radAllocation.Checked = True

            Me.ShowDialog()

            intUnkId = mintAssessGroupunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmGroup_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessGroupmaster = New clsassess_group_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            setColor()

            If menAction = enAction.EDIT_ONE Then
                objAssessGroupmaster._Assessgroupunkid = mintAssessGroupunkid
                Call SetData()
            Else
                radBranch.Checked = True
            End If

            GetValue()

            txtCode.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGroup_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGroup_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGroup_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGroup_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGroup_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGroup_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssessGroupmaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_group_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_group_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objAssessGroupmaster._FormName = mstrModuleName
            objAssessGroupmaster._LoginEmployeeunkid = 0
            objAssessGroupmaster._ClientIP = getIP()
            objAssessGroupmaster._HostName = getHostName()
            objAssessGroupmaster._FromWeb = False
            objAssessGroupmaster._AuditUserId = User._Object._Userunkid
objAssessGroupmaster._CompanyUnkid = Company._Object._Companyunkid
            objAssessGroupmaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'S.SANDEEP [20 Jan 2016] -- START
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objAssessGroupmaster.Update()
            'Else
            '    blnFlag = objAssessGroupmaster.Insert()
            'End If

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAssessGroupmaster.Update(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Else
                blnFlag = objAssessGroupmaster.Insert(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            End If
            'S.SANDEEP [20 Jan 2016] -- END
            

            If blnFlag = False And objAssessGroupmaster._Message <> "" Then
                eZeeMsgBox.Show(objAssessGroupmaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            ElseIf blnFlag = True And objAssessGroupmaster._Message <> "" Then
                eZeeMsgBox.Show(objAssessGroupmaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAssessGroupmaster = Nothing
                    objAssessGroupmaster = New clsassess_group_master
                    If radAllocation.Checked = True Then
                        cboMode.SelectedValue = 0
                    ElseIf radEmployee.Checked = True Then
                        Call radEmployee_CheckedChanged(New Object(), New EventArgs())
                    End If
                    Call GetValue()
                    txtCode.Select()
                Else
                    mintAssessGroupunkid = objAssessGroupmaster._Assessgroupunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboMode.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboMode.ValueMember
                .DisplayMember = cboMode.DisplayMember
                .DataSource = CType(cboMode.DataSource, DataTable)
                If radEmployee.Checked = True Then
                    .CodeMember = "employeecode"
                End If
                If .DisplayDialog Then
                    cboMode.SelectedValue = .SelectedValue
                    cboMode.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 NOV 2014 ] -- END

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objAssessGroupmaster._Assessgroup_Code
            txtName.Text = objAssessGroupmaster._Assessgroup_Name
            txtDescription.Text = objAssessGroupmaster._Description
            mstrAllocationIds = objAssessGroupmaster._AllocationIds
            If mstrAllocationIds.Trim.Length > 0 Then
                Dim iCnt As Integer = 0
                For Each StrId As String In mstrAllocationIds.Split(CChar(","))
                    Dim dtRow() As DataRow = mdtTran.Select(objdgcolhUnkid.DataPropertyName & " = '" & StrId & "'")

                    'Shani(26-APR-2016) -- Start
                    'If dtRow.Length >= 0 Then
                    If dtRow.Length > 0 Then
                        'Shani(26-APR-2016) -- End 
                        dtRow(0)("IsCheck") = True
                        dtRow(0).AcceptChanges()
                        iCnt += 1
                    End If
                Next
                mdtTran.AcceptChanges()
                If iCnt = 0 Then
                    objchkAll.CheckState = CheckState.Unchecked
                ElseIf iCnt < mdtTran.Rows.Count Then
                    objchkAll.CheckState = CheckState.Indeterminate
                ElseIf iCnt = mdtTran.Rows.Count Then
                    objchkAll.CheckState = CheckState.Checked
                End If
            End If

            'S.SANDEEP [ 09 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtWeight.Decimal = objAssessGroupmaster._Weight
            Call Fill_Weight()
            'S.SANDEEP [ 09 AUG 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAssessGroupmaster._Assessgroup_Code = txtCode.Text.Trim
            objAssessGroupmaster._Assessgroup_Name = txtName.Text.Trim
            objAssessGroupmaster._Description = txtDescription.Text.Trim

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'mstrAllocationIds = ""
            'For i As Integer = 0 To dtTemp.Length - 1
            '    mstrAllocationIds &= "," & dtTemp(i)(objdgcolhUnkid.DataPropertyName).ToString
            'Next
            'objAssessGroupmaster._AllocationIds = Mid(mstrAllocationIds, 2)
            'objAssessGroupmaster._ReferenceUnkid = mintReferenceUnkid
            If radAllocation.Checked = True Then
                mstrAllocationIds = ""
                For i As Integer = 0 To dtTemp.Length - 1
                    mstrAllocationIds &= "," & dtTemp(i)(objdgcolhUnkid.DataPropertyName).ToString
                Next
                objAssessGroupmaster._AllocationIds = Mid(mstrAllocationIds, 2)
                objAssessGroupmaster._ReferenceUnkid = mintReferenceUnkid
            End If
            'S.SANDEEP [ 05 NOV 2014 ] -- END

            'S.SANDEEP [ 09 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objAssessGroupmaster._Weight = txtWeight.Decimal
            'S.SANDEEP [ 09 AUG 2013 ] -- END

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            If radEmployee.Checked = True Then
                mstrAllocationIds = ""
                For i As Integer = 0 To dtTemp.Length - 1
                    mstrAllocationIds &= "," & dtTemp(i)(objdgcolhUnkid.DataPropertyName).ToString
                Next
                objAssessGroupmaster._AllocationIds = Mid(mstrAllocationIds, 2)
                objAssessGroupmaster._ReferenceUnkid = enAllocation.EMPLOYEE
            End If
            'S.SANDEEP [ 05 NOV 2014 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Assessment Group Code cannot be blank. Assessment Group Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Return False
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Assessment Group Name cannot be blank. Assessment Item Group is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'dtTemp = mdtTran.Select("IsCheck=true")

            'If dtTemp.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Allocation is mandatory information. Please select atleast one allocation from list."), enMsgBoxStyle.Information)
            '    dgvData.Focus()
            '    Return False

            'End If

            dtTemp = mdtTran.Select("IsCheck=true")
            'S.SANDEEP [02 SEP 2015] -- START
            If radEmployee.Checked = True Then
                'If radEmployee.Checked = True AndAlso CInt(cboMode.SelectedValue) <= 0 Then
                'S.SANDEEP [02 SEP 2015] -- END
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, employee is mandatory information. Please check employee to continue."), enMsgBoxStyle.Information)
                    dgvData.Focus()
                    Return False
                End If
            ElseIf radAllocation.Checked = True Then
                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Allocation is mandatory information. Please select atleast one allocation from list."), enMsgBoxStyle.Information)
                    dgvData.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 05 NOV 2014 ] -- END




            'S.SANDEEP [ 09 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If (mdecAssignedWeight + txtWeight.Decimal) > 100 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of 100%."), enMsgBoxStyle.Information)
            '    txtWeight.Focus()
            '    Return False
            'End If

            'If txtWeight.Decimal > 100 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you can set this weight as it exceeds the total of 100%."), enMsgBoxStyle.Information)
            '    txtWeight.Focus()
            '    Return False
            'End If
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            'S.SANDEEP [ 09 AUG 2013 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetData()
        Try
            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'Select Case objAssessGroupmaster._ReferenceUnkid
            '    Case enAllocation.BRANCH
            '        radBranch.Checked = True
            '    Case enAllocation.DEPARTMENT_GROUP
            '        radDepartmentGroup.Checked = True
            '    Case enAllocation.DEPARTMENT
            '        radDepartment.Checked = True
            '    Case enAllocation.SECTION_GROUP
            '        radSectionGroup.Checked = True
            '    Case enAllocation.SECTION
            '        radSection.Checked = True
            '    Case enAllocation.UNIT_GROUP
            '        radUnitGroup.Checked = True
            '    Case enAllocation.UNIT
            '        radUnit.Checked = True
            '    Case enAllocation.TEAM
            '        radTeam.Checked = True
            '    Case enAllocation.JOB_GROUP
            '        radJobGroup.Checked = True
            '    Case enAllocation.JOBS
            '        radJobs.Checked = True
            'End Select
            'gbAllocation.Enabled = False
            If objAssessGroupmaster._ReferenceUnkid > 0 Then
                Select Case objAssessGroupmaster._ReferenceUnkid
                    Case enAllocation.EMPLOYEE
                        radEmployee.Checked = True
                    Case Else
                        radAllocation.Checked = True
                End Select
            End If
            'S.SANDEEP [ 05 NOV 2014 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Weight()
        'Try
        '    Dim mDecTotal, mDecAssign As Decimal : mDecTotal = 0 : mDecAssign = 0
        '    objAssessGroupmaster.Get_Weight(mDecTotal, mDecAssign, mintAssessGroupunkid)
        '    mdecAssignedWeight = mDecAssign
        '    If menAction = enAction.EDIT_ONE Then
        '        If txtWeight.Decimal > 0 Then
        '            objlblCaption.Text = Language.getMessage(mstrModuleName, 4, "Total Assigned : ") & " " & (mDecTotal + mDecAssign).ToString & " " & _
        '                                            Language.getMessage(mstrModuleName, 7, "%")
        '            If mDecTotal = 100 Then
        '                objlblCaption.Text = Language.getMessage(mstrModuleName, 5, "Remaining : ") & " " & (mDecTotal - objAssessGroupmaster._Weight).ToString & " " & _
        '                                Language.getMessage(mstrModuleName, 6, "%")
        '            Else
        '                objlblCaption.Text = Language.getMessage(mstrModuleName, 4, "Assigned : ") & " " & (mDecAssign + objAssessGroupmaster._Weight).ToString & " " & _
        '                                Language.getMessage(mstrModuleName, 6, "%")
        '            End If
        '        End If
        '    Else
        '        objlblCaption.Text = Language.getMessage(mstrModuleName, 3, "Total Remaining Weightage : ") & CStr(mDecTotal)
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "Fill_Weight", mstrModuleName)
        'Finally
        'End Try
    End Sub

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Private Sub Fill_List()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("List", , , , , , , , , , , , , , , , , , , , mstrAdvanceFilter, True)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "List", True, , , , , , , , , , , , , , , mstrAdvanceFilter)
            'S.SANDEEP [04 JUN 2015] -- END

            dsList.Tables("List").Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                dsList.Tables("List").Rows(i)("IsCheck") = False
            Next
            dgvData.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsCheck"
            objdgcolhUnkid.DataPropertyName = "employeeunkid"
            dgcolhName.DataPropertyName = "employeename"

            dsList.Tables("List").AcceptChanges()
            mdtTran = dsList.Tables(0).Copy
            dgView = New DataView(mdtTran, "", "", DataViewRowState.CurrentRows)
            dgvData.DataSource = dgView
            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 NOV 2014 ] -- END

#End Region

#Region " Control's Events "

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = dgvData.Rows(dgvData.RowCount - 1).Index Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' "
            End If
            dgView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Private Sub radAllocation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAllocation.CheckedChanged
        Try
            If radAllocation.Checked = True Then
                cboMode.Enabled = True : objbtnSearchEmployee.Enabled = True
                objlblValue.Text = radAllocation.Text
                Dim objMData As New clsMasterData
                Dim dsList As New DataSet
                dsList = objMData.GetEAllocation_Notification("List")
                'S.SANDEEP [31 AUG 2016] -- START
                'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                Dim dr As DataRow = dsList.Tables(0).NewRow
                dr.Item("Id") = enAllocation.EMPLOYEE_GRADES
                dr.Item("Name") = Language.getMessage("frmGroupList", 9, "Grades")
                dsList.Tables(0).Rows.Add(dr)
                'S.SANDEEP [31 AUG 2016] -- START
                cboMode.DataSource = Nothing
                With cboMode
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables(0)
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objAssessGroupmaster._ReferenceUnkid
                    Else
                        .SelectedValue = 1
                    End If
                End With
                objMData = Nothing
                lnkAllocation.Enabled = False : objAlloacationReset.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAllocation_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub radBranch_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radBranch.CheckedChanged, radDepartment.CheckedChanged, _
    '                                                                                                             radSection.CheckedChanged, radUnit.CheckedChanged, _
    '                                                                                                             radJobGroup.CheckedChanged, radJobs.CheckedChanged, _
    '                                                                                                             radDepartmentGroup.CheckedChanged, radSectionGroup.CheckedChanged, _
    '                                                                                                             radUnitGroup.CheckedChanged, radTeam.CheckedChanged

    '    Try
    '        Dim dsList As New DataSet
    '        dgvData.AutoGenerateColumns = False
    '        txtSearch.Text = ""

    '        Select Case CType(sender, RadioButton).Name.ToUpper
    '            Case "RADBRANCH"
    '                Dim objBranch As New clsStation
    '                dsList = objBranch.GetList("List", True)
    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "stationunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objBranch = Nothing

    '                mintReferenceUnkid = enAllocation.BRANCH

    '            Case "RADDEPARTMENT"
    '                Dim objDept As New clsDepartment
    '                dsList = objDept.GetList("List", True)
    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "departmentunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objDept = Nothing

    '                mintReferenceUnkid = enAllocation.DEPARTMENT

    '            Case "RADSECTION"
    '                Dim objSection As New clsSections
    '                dsList = objSection.GetList("List", True)
    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "sectionunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objSection = Nothing

    '                mintReferenceUnkid = enAllocation.SECTION

    '            Case "RADUNIT"
    '                Dim objUnit As New clsUnits
    '                dsList = objUnit.GetList("List", True)
    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "unitunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objUnit = Nothing

    '                mintReferenceUnkid = enAllocation.UNIT

    '            Case "RADJOBGROUP"
    '                Dim objJobGrp As New clsJobGroup
    '                dsList = objJobGrp.GetList("List", True)
    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "jobgroupunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objJobGrp = Nothing

    '                mintReferenceUnkid = enAllocation.JOB_GROUP

    '            Case "RADJOBS"
    '                Dim objJob As New clsJobs
    '                dsList = objJob.GetList("List", True)
    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "jobunkid"
    '                dgcolhName.DataPropertyName = "JobName"

    '                objJob = Nothing

    '                mintReferenceUnkid = enAllocation.JOBS

    '            Case "RADDEPARTMENTGROUP"
    '                Dim objDeptGrp As New clsDepartmentGroup
    '                dsList = objDeptGrp.GetList("List", True)

    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "deptgroupunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objDeptGrp = Nothing

    '                mintReferenceUnkid = enAllocation.DEPARTMENT_GROUP

    '            Case "RADSECTIONGROUP"
    '                Dim objSectionGrp As New clsSectionGroup
    '                dsList = objSectionGrp.GetList("List", True)

    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "sectiongroupunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objSectionGrp = Nothing

    '                mintReferenceUnkid = enAllocation.SECTION_GROUP

    '            Case "RADUNITGROUP"
    '                Dim objUnitGrp As New clsUnitGroup
    '                dsList = objUnitGrp.GetList("List", True)

    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "unitgroupunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objUnitGrp = Nothing

    '                mintReferenceUnkid = enAllocation.UNIT_GROUP

    '            Case "RADTEAM"
    '                Dim objTeam As New clsTeams
    '                dsList = objTeam.GetList("List", True)

    '                dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

    '                objdgcolhCheck.DataPropertyName = "IsCheck"
    '                objdgcolhUnkid.DataPropertyName = "teamunkid"
    '                dgcolhName.DataPropertyName = "name"

    '                objTeam = Nothing



    '                'S.SANDEEP [ 18 APRIL 2012 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '                mintReferenceUnkid = enAllocation.TEAM
    '                'S.SANDEEP [ 18 APRIL 2012 ] -- END

    '        End Select

    '        mstrAllocation = dgcolhName.DataPropertyName

    '        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
    '            dsList.Tables("List").Rows(i)("IsCheck") = False
    '        Next
    '        dsList.Tables("List").AcceptChanges()

    '        mdtTran = dsList.Tables(0).Copy

    '        dgView = New DataView(mdtTran, "", "", DataViewRowState.CurrentRows)

    '        dgvData.DataSource = dgView

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub radEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged
        Try
            If radEmployee.Checked = True Then
                cboMode.Enabled = False : objbtnSearchEmployee.Enabled = False
                objlblValue.Text = radEmployee.Text
                Call Fill_List()
                lnkAllocation.Enabled = True : objAlloacationReset.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            If CInt(cboMode.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dgvData.AutoGenerateColumns = False
                txtSearch.Text = ""
                mintReferenceUnkid = CInt(cboMode.SelectedValue)
                If radAllocation.Checked = True Then
                    Select Case CInt(cboMode.SelectedValue)
                        Case enAllocation.BRANCH
                            Dim objBranch As New clsStation
                            dsList = objBranch.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "stationunkid"
                            dgcolhName.DataPropertyName = "name"
                            objBranch = Nothing

                        Case enAllocation.DEPARTMENT_GROUP
                            Dim objDeptGrp As New clsDepartmentGroup
                            dsList = objDeptGrp.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "deptgroupunkid"
                            dgcolhName.DataPropertyName = "name"
                            objDeptGrp = Nothing

                        Case enAllocation.DEPARTMENT
                            Dim objDept As New clsDepartment
                            dsList = objDept.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "departmentunkid"
                            dgcolhName.DataPropertyName = "name"
                            objDept = Nothing

                        Case enAllocation.SECTION_GROUP
                            Dim objSectionGrp As New clsSectionGroup
                            dsList = objSectionGrp.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "sectiongroupunkid"
                            dgcolhName.DataPropertyName = "name"
                            objSectionGrp = Nothing

                        Case enAllocation.SECTION
                            Dim objSection As New clsSections
                            dsList = objSection.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "sectionunkid"
                            dgcolhName.DataPropertyName = "name"
                            objSection = Nothing

                        Case enAllocation.UNIT_GROUP
                            Dim objUnitGrp As New clsUnitGroup
                            dsList = objUnitGrp.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "unitgroupunkid"
                            dgcolhName.DataPropertyName = "name"
                            objUnitGrp = Nothing

                        Case enAllocation.UNIT
                            Dim objUnit As New clsUnits
                            dsList = objUnit.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "unitunkid"
                            dgcolhName.DataPropertyName = "name"
                            objUnit = Nothing

                        Case enAllocation.TEAM
                            Dim objTeam As New clsTeams
                            dsList = objTeam.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "teamunkid"
                            dgcolhName.DataPropertyName = "name"
                            objTeam = Nothing

                        Case enAllocation.JOB_GROUP
                            Dim objJobGrp As New clsJobGroup
                            dsList = objJobGrp.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "jobgroupunkid"
                            dgcolhName.DataPropertyName = "name"
                            objJobGrp = Nothing

                        Case enAllocation.JOBS
                            Dim objJob As New clsJobs
                            dsList = objJob.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "jobunkid"
                            dgcolhName.DataPropertyName = "JobName"
                            objJob = Nothing

                        Case enAllocation.CLASS_GROUP
                            Dim objClassGrp As New clsClassGroup
                            dsList = objClassGrp.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "classgroupunkid"
                            dgcolhName.DataPropertyName = "name"

                            objClassGrp = Nothing

                        Case enAllocation.CLASSES
                            Dim objClass As New clsClass
                            dsList = objClass.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "classesunkid"
                            dgcolhName.DataPropertyName = "name"

                            objClass = Nothing

                        Case enAllocation.COST_CENTER
                            Dim objCC As New clscostcenter_master
                            dsList = objCC.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "costcenterunkid"
                            dgcolhName.DataPropertyName = "costcentername"

                            objCC = Nothing
                            'S.SANDEEP [31 AUG 2016] -- START
                            'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                        Case enAllocation.EMPLOYEE_GRADES
                            Dim objGrd As New clsGrade
                            dsList = objGrd.GetList("List", True)
                            dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                            objdgcolhCheck.DataPropertyName = "IsCheck"
                            objdgcolhUnkid.DataPropertyName = "gradeunkid"
                            dgcolhName.DataPropertyName = "name"

                            objGrd = Nothing
                            'S.SANDEEP [31 AUG 2016] -- START
                    End Select
                    mstrAllocation = dgcolhName.DataPropertyName
                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()
                    mdtTran = dsList.Tables(0).Copy
                    dgView = New DataView(mdtTran, "", "", DataViewRowState.CurrentRows)
                    dgvData.DataSource = dgView
                End If
            Else
                dgvData.DataSource = Nothing : txtSearch.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 05 NOV 2014 ] -- END

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick

            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                If objchkAll.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In mdtTran.Rows
                        dr("IsCheck") = objchkAll.Checked
                    Next
                    mdtTran.AcceptChanges()
                End If
            End If
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "obkchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try

            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

            If e.ColumnIndex = objdgcolhCheck.Index Then

                'If dgvData.IsCurrentCellDirty Then
                '    dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                'End If

                mdtTran.Rows(e.RowIndex)(objdgcolhCheck.DataPropertyName) = Not CBool(mdtTran.Rows(e.RowIndex)(objdgcolhCheck.DataPropertyName))
                dgvData.RefreshEdit()
                Dim drRow As DataRow() = mdtTran.Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If mdtTran.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbAssessmentGroup.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssessmentGroup.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbAssessmentGroup.Text = Language._Object.getCaption(Me.gbAssessmentGroup.Name, Me.gbAssessmentGroup.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.gbAllocation.Text = Language._Object.getCaption(Me.gbAllocation.Name, Me.gbAllocation.Text)
            Me.radJobs.Text = Language._Object.getCaption(Me.radJobs.Name, Me.radJobs.Text)
            Me.radSection.Text = Language._Object.getCaption(Me.radSection.Name, Me.radSection.Text)
            Me.radJobGroup.Text = Language._Object.getCaption(Me.radJobGroup.Name, Me.radJobGroup.Text)
            Me.radDepartment.Text = Language._Object.getCaption(Me.radDepartment.Name, Me.radDepartment.Text)
            Me.radUnit.Text = Language._Object.getCaption(Me.radUnit.Name, Me.radUnit.Text)
            Me.radBranch.Text = Language._Object.getCaption(Me.radBranch.Name, Me.radBranch.Text)
            Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)
            Me.radTeam.Text = Language._Object.getCaption(Me.radTeam.Name, Me.radTeam.Text)
            Me.radUnitGroup.Text = Language._Object.getCaption(Me.radUnitGroup.Name, Me.radUnitGroup.Text)
            Me.radSectionGroup.Text = Language._Object.getCaption(Me.radSectionGroup.Name, Me.radSectionGroup.Text)
            Me.radDepartmentGroup.Text = Language._Object.getCaption(Me.radDepartmentGroup.Name, Me.radDepartmentGroup.Text)
            Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
            Me.elMode.Text = Language._Object.getCaption(Me.elMode.Name, Me.elMode.Text)
            Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
            Me.radAllocation.Text = Language._Object.getCaption(Me.radAllocation.Name, Me.radAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Assessment Group Code cannot be blank. Assessment Group Code is required information.")
            Language.setMessage(mstrModuleName, 2, "Assessment Group Name cannot be blank. Assessment Item Group is required information.")
            Language.setMessage(mstrModuleName, 3, "Allocation is mandatory information. Please select atleast one allocation from list.")
            Language.setMessage(mstrModuleName, 4, "Sorry, you can set this weight as it exceeds the total of 100%.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

'Last Message Index = 8

'Public Class frmGroup_AddEdit


'#Region "Private Variable"

'    Private ReadOnly mstrModuleName As String = "frmGroup_AddEdit"
'    Private mblnCancel As Boolean = True
'    Private objAssessGroupmaster As clsassess_group_master
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintAssessGroupunkid As Integer = -1

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
'        Try
'            mintAssessGroupunkid = intUnkId
'            menAction = eAction

'            Me.ShowDialog()

'            intUnkId = mintAssessGroupunkid

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region "Form's Event"

'    Private Sub frmGroup_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objAssessGroupmaster = New clsassess_group_master
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            setColor()
'            If menAction = enAction.EDIT_ONE Then
'                objAssessGroupmaster._Assessgroupunkid = mintAssessGroupunkid
'            End If
'            FillCombo()
'            GetValue()
'            txtCode.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmGroup_AddEdit_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmGroup_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Keys.S Then
'                btnSave_Click(sender, e)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmGroup_AddEdit_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmGroup_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'                e.Handled = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmGroup_AddEdit_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmGroup_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'        objAssessGroupmaster = Nothing
'    End Sub

'    'S.SANDEEP [ 20 AUG 2011 ] -- START
'    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsassess_group_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsassess_group_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 20 AUG 2011 ] -- END

'#End Region

'#Region "Dropdown's Event"

'    Private Sub cboStation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
'        Try
'            'For Department 
'            Dim objDepartment As New clsDepartment
'            Dim dsDepartment As DataSet = objDepartment.GetList("Department")
'            Dim dtDepartment As DataTable = New DataView(dsDepartment.Tables("Department"), "stationunkid=" & CInt(cboBranch.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

'            Dim drRow As DataRow = dtDepartment.NewRow
'            drRow("departmentunkid") = 0
'            drRow("name") = "Select"
'            dtDepartment.Rows.InsertAt(drRow, 0)

'            cboDepartment.ValueMember = "departmentunkid"
'            cboDepartment.DisplayMember = "name"
'            cboDepartment.DataSource = dtDepartment
'        Catch ex As Exception

'        End Try
'    End Sub

'    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
'        Try
'            Dim objSection As New clsSections
'            Dim dsSection As DataSet
'            If CInt(cboDepartment.SelectedValue) > 0 Then
'                dsSection = objSection.getComboList("Section", True, CInt(cboDepartment.SelectedValue))
'            Else
'                dsSection = objSection.getComboList("Section", True, CInt(cboDepartment.SelectedValue))
'            End If
'            cboSections.ValueMember = "sectionunkid"
'            cboSections.DisplayMember = "name"
'            cboSections.DataSource = dsSection.Tables(0)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboDepartment_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
'        Try
'            Dim objUnit As New clsUnits
'            Dim dsUnit As DataSet
'            If CInt(cboSections.SelectedValue) > 0 Then
'                dsUnit = objUnit.getComboList("Unit", True, CInt(cboSections.SelectedValue))
'            Else
'                dsUnit = objUnit.getComboList("Unit", True, CInt(cboSections.SelectedValue))
'            End If

'            cboUnits.ValueMember = "unitunkid"
'            cboUnits.DisplayMember = "name"
'            cboUnits.DataSource = dsUnit.Tables(0)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboSections_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
'        Try
'            Dim objJob As New clsJobs
'            Dim dsJob As DataSet
'            If CInt(cboJobGroup.SelectedValue) > 0 Then
'                dsJob = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue))
'            Else
'                dsJob = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue))
'            End If
'            cboJob.ValueMember = "jobunkid"
'            cboJob.DisplayMember = "name"
'            cboJob.DataSource = dsJob.Tables(0)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub


'#End Region

'#Region "Button's Event"

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If Trim(txtCode.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Assessment Group Code cannot be blank. Assessment Group Code is required information."), enMsgBoxStyle.Information)
'                txtCode.Focus()
'                Exit Sub
'            ElseIf Trim(txtName.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Assessment Group Name cannot be blank. Assessment Item Group is required information."), enMsgBoxStyle.Information)
'                txtName.Focus()
'                Exit Sub
'                'ElseIf CInt(cboResultGroup.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Result Group is compulsory information.Please Select Result Group."), enMsgBoxStyle.Information)
'                '    cboResultGroup.Focus()
'                '    Exit Sub

'                'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

'                'ElseIf CInt(cboBranch.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Station is compulsory information.Please Select Station."), enMsgBoxStyle.Information)
'                '    cboBranch.Focus()
'                '    Exit Sub


'                'ElseIf CInt(cboDepartment.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Department is compulsory information.Please Select Department."), enMsgBoxStyle.Information)
'                '    cboDepartment.Focus()
'                '    Exit Sub

'                'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment


'                'ElseIf CInt(cboSections.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Section is compulsory information.Please Select Section."), enMsgBoxStyle.Information)
'                '    cboSections.Focus()
'                '    Exit Sub
'                'ElseIf CInt(cboUnits.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Unit is compulsory information.Please Select Unit."), enMsgBoxStyle.Information)
'                '    cboUnits.Focus()
'                '    Exit Sub
'                'ElseIf CInt(cboJobGroup.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Job Group is compulsory information.Please Select Job Group."), enMsgBoxStyle.Information)
'                '    cboJobGroup.Focus()
'                '    Exit Sub
'                'ElseIf CInt(cboJob.SelectedValue) = 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Job is compulsory information.Please Select Job."), enMsgBoxStyle.Information)
'                '    cboJob.Focus()
'                '    Exit Sub
'            End If

'            Call SetValue()

'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objAssessGroupmaster.Update()
'            Else
'                blnFlag = objAssessGroupmaster.Insert()
'            End If

'            If blnFlag = False And objAssessGroupmaster._Message <> "" Then
'                eZeeMsgBox.Show(objAssessGroupmaster._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                If menAction = enAction.ADD_CONTINUE Then
'                    objAssessGroupmaster = Nothing
'                    objAssessGroupmaster = New clsassess_group_master
'                    Call GetValue()
'                    txtCode.Select()
'                Else
'                    mintAssessGroupunkid = objAssessGroupmaster._Assessgroupunkid
'                    Me.Close()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'#End Region

'#Region "Private Methods"

'    Private Sub setColor()
'        Try
'            txtCode.BackColor = GUI.ColorComp
'            txtName.BackColor = GUI.ColorComp
'            txtDescription.BackColor = GUI.ColorOptional
'            ' cboResultGroup.BackColor = GUI.ColorComp
'            cboBranch.BackColor = GUI.ColorComp
'            cboDepartment.BackColor = GUI.ColorComp
'            cboJob.BackColor = GUI.ColorComp
'            cboJobGroup.BackColor = GUI.ColorComp
'            cboSections.BackColor = GUI.ColorComp
'            cboUnits.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            txtCode.Text = objAssessGroupmaster._Assessgroup_Code
'            txtName.Text = objAssessGroupmaster._Assessgroup_Name
'            txtDescription.Text = objAssessGroupmaster._Description
'            '  cboResultGroup.SelectedValue = objAssessGroupmaster._Resultgroupunkid
'            cboBranch.SelectedValue = objAssessGroupmaster._Stationunkid
'            cboDepartment.SelectedValue = objAssessGroupmaster._Departmentunkid
'            cboSections.SelectedValue = objAssessGroupmaster._Sectionunkid
'            cboUnits.SelectedValue = objAssessGroupmaster._Unitunkid
'            cboJobGroup.SelectedValue = objAssessGroupmaster._Jobgroupunkid
'            cboJob.SelectedValue = objAssessGroupmaster._Jobunkid

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objAssessGroupmaster._Assessgroup_Code = txtCode.Text.Trim
'            objAssessGroupmaster._Assessgroup_Name = txtName.Text.Trim
'            objAssessGroupmaster._Description = txtDescription.Text.Trim
'            ' objAssessGroupmaster._Resultgroupunkid = CInt(cboResultGroup.SelectedValue)
'            objAssessGroupmaster._Stationunkid = CInt(cboBranch.SelectedValue)
'            objAssessGroupmaster._Departmentunkid = CInt(cboDepartment.SelectedValue)
'            objAssessGroupmaster._Sectionunkid = CInt(cboSections.SelectedValue)
'            objAssessGroupmaster._Unitunkid = CInt(cboUnits.SelectedValue)
'            objAssessGroupmaster._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
'            objAssessGroupmaster._Jobunkid = CInt(cboJob.SelectedValue)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Try
'            'For Result Group
'            'Dim objCommonmaster As New clsCommon_Master
'            'Dim dsAssessGroup As DataSet = objCommonmaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "Result Group")
'            'cboResultGroup.ValueMember = "masterunkid"
'            'cboResultGroup.DisplayMember = "name"
'            'cboResultGroup.DataSource = dsAssessGroup.Tables(0)

'            'FOR STATION
'            Dim objStation As New clsStation
'            Dim dsStation As DataSet = objStation.getComboList("station", True)
'            cboBranch.ValueMember = "stationunkid"
'            cboBranch.DisplayMember = "name"
'            cboBranch.DataSource = dsStation.Tables("station")


'            'For Job Group
'            Dim objJobGroup As New clsJobGroup
'            Dim dsJobGroup As DataSet = objJobGroup.getComboList("JobGroup", True)
'            cboJobGroup.ValueMember = "jobgroupunkid"
'            cboJobGroup.DisplayMember = "name"
'            cboJobGroup.DataSource = dsJobGroup.Tables(0)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'End Class