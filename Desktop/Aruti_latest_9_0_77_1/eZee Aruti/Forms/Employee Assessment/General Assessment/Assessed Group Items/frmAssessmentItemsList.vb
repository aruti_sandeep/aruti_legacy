﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmAssessmentItemsList

#Region "Private Variable"

    Private objAssessItemmaster As clsassess_item_master
    Private ReadOnly mstrModuleName As String = "frmAssessmentItemsList"

#End Region

#Region "Form's Event"

    Private Sub frmAssessmentItemsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessItemmaster = New clsassess_item_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            FillCombo()
            fillList()
            Call SetVisibility()

            If lvAssessmentGroup.Items.Count > 0 Then lvAssessmentGroup.Items(0).Selected = True
            lvAssessmentGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentItemsList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentItemsList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvAssessmentGroup.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentItemsList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentItemsList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssessItemmaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_item_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_item_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmAssessmentItems_AddEdit
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                ObjFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvAssessmentGroup.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvAssessmentGroup.Select()
                Exit Sub
            End If
            Dim objfrmAssessmentItems_AddEdit As New frmAssessmentItems_AddEdit
            Try
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmAssessmentItems_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmAssessmentItems_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmAssessmentItems_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvAssessmentGroup.SelectedItems(0).Index
                If objfrmAssessmentItems_AddEdit.displayDialog(CInt(lvAssessmentGroup.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                objfrmAssessmentItems_AddEdit = Nothing

                lvAssessmentGroup.Items(intSelectedIndex).Selected = True
                lvAssessmentGroup.EnsureVisible(intSelectedIndex)
                lvAssessmentGroup.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmAssessmentItems_AddEdit IsNot Nothing Then objfrmAssessmentItems_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAssessmentGroup.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvAssessmentGroup.Select()
            Exit Sub
        End If
        If objAssessItemmaster.isUsed(CInt(lvAssessmentGroup.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Item. Reason: This Assess Item is in use."), enMsgBoxStyle.Information) '?2
            lvAssessmentGroup.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAssessmentGroup.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Item?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objAssessItemmaster.Delete(CInt(lvAssessmentGroup.SelectedItems(0).Tag))
                lvAssessmentGroup.SelectedItems(0).Remove()

                If lvAssessmentGroup.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAssessmentGroup.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAssessmentGroup.Items.Count - 1
                    lvAssessmentGroup.Items(intSelectedIndex).Selected = True
                    lvAssessmentGroup.EnsureVisible(intSelectedIndex)
                ElseIf lvAssessmentGroup.Items.Count <> 0 Then
                    lvAssessmentGroup.Items(intSelectedIndex).Selected = True
                    lvAssessmentGroup.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAssessmentGroup.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAssessGroup.SelectedIndex = 0
            cboResultGroup.SelectedIndex = 0

            'Pinkal (3-Aug-2013) -- Start
            'Enhancement : TRA Changes
            cboPeriod.SelectedValue = 0
            'Pinkal (3-Aug-2013) -- End

            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsGroup As DataSet = Nothing
        Try
            Dim objAssessGroup As New clsassess_group_master
            Dim dsAssessGroup As DataSet = objAssessGroup.getListForCombo("Group", True)
            cboAssessGroup.ValueMember = "assessgroupunkid"
            cboAssessGroup.DisplayMember = "name"
            cboAssessGroup.DataSource = dsAssessGroup.Tables(0)

            ' For Result Group
            Dim objCommonmaster As New clsCommon_Master
            dsGroup = objCommonmaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "Result Group")
            cboResultGroup.ValueMember = "masterunkid"
            cboResultGroup.DisplayMember = "name"
            cboResultGroup.DataSource = dsGroup.Tables(0)


            'Pinkal (3-Aug-2013) -- Start
            'Enhancement : TRA Changes

            'For Period
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsPeriod As DataSet = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            Dim dsPeriod As DataSet = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            cboPeriod.DataSource = dsPeriod.Tables(0)

            'Pinkal (3-Aug-2013) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsAssessmentItem As New DataSet
        Dim strSearching As String = ""
        Dim dtAssessmentItem As New DataTable
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewAssessmentItemList = True Then    'Pinkal (09-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END



            dsAssessmentItem = objAssessItemmaster.GetList("AssessItem")

            If CInt(cboAssessGroup.SelectedValue) > 0 Then
                strSearching = "AND assessgroupunkid =" & CInt(cboAssessGroup.SelectedValue) & " "
            End If

            If CInt(cboResultGroup.SelectedValue) > 0 Then
                strSearching = "AND resultgroupunkid =" & CInt(cboResultGroup.SelectedValue) & " "
            End If

                'Pinkal (3-Aug-2013) -- Start
                'Enhancement : TRA Changes

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    strSearching = "AND periodunkid =" & CInt(cboPeriod.SelectedValue) & " "
                End If

                'Pinkal (3-Aug-2013) -- End

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtAssessmentItem = New DataView(dsAssessmentItem.Tables("AssessItem"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtAssessmentItem = dsAssessmentItem.Tables("AssessItem")
            End If

            Dim lvItem As ListViewItem

            lvAssessmentGroup.Items.Clear()
            For Each drRow As DataRow In dtAssessmentItem.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("groupname").ToString
                lvItem.Tag = drRow("assessitemunkid")
                lvItem.SubItems.Add(drRow("Resultgroup").ToString)
                lvItem.SubItems.Add(drRow("code").ToString)
                lvItem.SubItems.Add(drRow("name").ToString)


                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                lvItem.SubItems.Add(drRow("weight").ToString)
                'Pinkal (06-Feb-2012) -- End

                lvItem.SubItems.Add(drRow("description").ToString)

                    'S.SANDEEP [ 28 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(drRow.Item("Period").ToString)
                    lvItem.SubItems(objcolhPeriod.Index).Tag = drRow.Item("statusid")
                    'S.SANDEEP [ 28 DEC 2012 ] -- END

                lvAssessmentGroup.Items.Add(lvItem)
            Next


                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvAssessmentGroup.GroupingColumn = objcolhPeriod
                lvAssessmentGroup.DisplayGroups(True)
                'S.SANDEEP [ 28 DEC 2012 ] -- END


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

                If lvAssessmentGroup.Items.Count > 4 Then
                colhDescription.Width = 209 - 18
            Else
                colhDescription.Width = 209
            End If

            'If lvAssessmentGroup.Items.Count > 16 Then
            '    colhDescription.Width = 175 - 18
            'Else
            '    colhDescription.Width = 175
            'End If

            'Pinkal (06-Feb-2012) -- End

            'End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAssessmentItem.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)

            'btnNew.Enabled = User._Object.Privilege._AddAssessmentItem
            'btnEdit.Enabled = User._Object.Privilege._EditAssessmentItem
            'btnDelete.Enabled = User._Object.Privilege._DeleteAssessmentItem

            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
			Me.colhGroup.Text = Language._Object.getCaption(CStr(Me.colhGroup.Tag), Me.colhGroup.Text)
			Me.colhItemCode.Text = Language._Object.getCaption(CStr(Me.colhItemCode.Tag), Me.colhItemCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.colhResultGroup.Text = Language._Object.getCaption(CStr(Me.colhResultGroup.Tag), Me.colhResultGroup.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Item. Reason: This Assess Item is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Item?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class