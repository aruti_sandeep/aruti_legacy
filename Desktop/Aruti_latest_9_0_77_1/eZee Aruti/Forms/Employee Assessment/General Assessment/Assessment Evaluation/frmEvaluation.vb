﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEvaluation

#Region " Private Variables "

    Private mstrModuleName As String = "frmEvaluation"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objAssessMaster As clsassess_analysis_master
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mintEmplId As Integer = 0
    Private mintYearId As Integer = 0
    Private mintPeriodId As Integer = 0
    Private mintAssessorId As Integer = 0
    Private menAssess As enAssessmentMode

    Private objAnalysisTran As clsassess_analysis_tran
    Private objRemarkTran As clsassess_remarks_tran

    Private mdicGroupWiseTotal As New Dictionary(Of Integer, Decimal)
    Private mdicItemWeight As New Dictionary(Of Integer, Decimal)

    Dim iCnt As Integer = 0
    Dim mdecRemaininResult As Decimal = 0

    Private mdtEvaluation As DataTable
    Private mdtRemarks As DataTable
    Private dsItemList As New DataSet

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, ByVal eAssess As enAssessmentMode) As Boolean
        Try
            mintAssessAnalysisUnkid = intUnkid
            menAction = eAction
            menAssess = eAssess
            Me.ShowDialog()
            intUnkid = mintAssessAnalysisUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboAssessor.Visible = False
                lblAssessor.Visible = False
                objbtnSearchAssessor.Enabled = False
                pnlContainer.Location = New Point(5, 33)
                tabcEvaluation.TabPages.Remove(tabpPersonalDevelopment)
                tabcEvaluation.TabPages.Remove(tabpReviewerComments)
                colhResult.Text = Language.getMessage(mstrModuleName, 10, "Self")
                lnkViewAssessments.Visible = False
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Visible = False : radExternalAssessor.Visible = False
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                cboAssessor.Visible = True
                lblAssessor.Visible = True
                objbtnSearchAssessor.Enabled = True
                pnlContainer.Location = New Point(5, 58)
                tabcEvaluation.TabPages.Remove(tabpImporvementArea)
                tabcEvaluation.TabPages.Remove(tabpReviewerComments)
                colhResult.Text = Language.getMessage(mstrModuleName, 11, "Appraiser's")
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
                lnkViewAssessments.Visible = False
                radInternalAssessor.Checked = True
                radInternalAssessor.Visible = True : radExternalAssessor.Visible = True
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                colhResult.Text = Language.getMessage(mstrModuleName, 12, "Reviewer's")
                cboAssessor.Visible = False
                lblAssessor.Visible = False
                objbtnSearchAssessor.Visible = False
                lblReviewer.Visible = True
                lblReviewer.Visible = True
                cboReviewer.Visible = True
                objbtnSearchReviewer.Visible = True
                lnkViewAssessments.Visible = True
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Visible = False : radExternalAssessor.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboYear.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboAssessGroup.BackColor = GUI.ColorComp
            cboAssessItem.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            cboResultCode.BackColor = GUI.ColorComp
            cboAssessor.BackColor = GUI.ColorComp
            cboSubItems.BackColor = GUI.ColorComp
            txtI_Action.BackColor = GUI.ColorOptional
            txtI_OtherTraining.BackColor = GUI.ColorOptional
            txtI_Support.BackColor = GUI.ColorOptional
            txtP_Action.BackColor = GUI.ColorOptional
            txtP_Development.BackColor = GUI.ColorOptional
            txtP_OtherTraining.BackColor = GUI.ColorOptional
            txtP_Support.BackColor = GUI.ColorOptional
            txtRemark.BackColor = GUI.ColorOptional
            txtRemark1.BackColor = GUI.ColorOptional
            txtRemark2.BackColor = GUI.ColorOptional
            txtI_Improvement.BackColor = GUI.ColorOptional
            cboReviewer.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objAssessGroup As New clsassess_group_master
        Dim objCMaster As New clsCommon_Master
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                'Anjan (17 Apr 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
                'End If
                ''Anjan (17 Apr 2012)-End 
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsCombos.Tables(0)
                '    .SelectedValue = 0
                'End With
                'S.SANDEEP [04 JUN 2015] -- END
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                dsCombos = objAssessMaster.getAssessorComboList("Assessor", True, True)
                With cboReviewer
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objMaster.getComboListPAYYEAR("Year", True, , , , True)
            dsCombos = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Year")
            End With

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboITraining
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboPTraining
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    cboEmployee.SelectedValue = objAssessMaster._Selfemployeeunkid
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If objAssessMaster._Ext_Assessorunkid > 0 Then
                        radExternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objAssessMaster._Ext_Assessorunkid
                    Else
                        radInternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objAssessMaster._Assessormasterunkid
                    End If
                    cboEmployee.SelectedValue = objAssessMaster._Assessedemployeeunkid
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    cboReviewer.SelectedValue = objAssessMaster._Assessormasterunkid
                    cboEmployee.SelectedValue = objAssessMaster._Assessedemployeeunkid
            End Select
            cboYear.SelectedValue = objAssessMaster._Yearunkid
            cboPeriod.SelectedValue = objAssessMaster._Periodunkid
            cboAssessGroup.SelectedValue = objAssessMaster._Assessgroupunkid
            If objAssessMaster._Assessmentdate <> Nothing Then
                dtpAssessdate.Value = objAssessMaster._Assessmentdate
            End If
            txtRemark1.Text = objAssessMaster._Reviewer_Remark1
            txtRemark2.Text = objAssessMaster._Reviewer_Remark2
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                objAssessMaster._Selfemployeeunkid = CInt(cboEmployee.SelectedValue)
                objAssessMaster._Assessedemployeeunkid = -1
                objAssessMaster._Assessormasterunkid = -1
                objAssessMaster._Assessoremployeeunkid = -1
                objAssessMaster._Reviewerunkid = -1
                objAssessMaster._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                objAssessMaster._Selfemployeeunkid = -1
                objAssessMaster._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)

                If radInternalAssessor.Checked = True Then
                    objAssessMaster._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
                    Dim intEmployeeId As Integer = -1
                    intEmployeeId = objAssessMaster.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
                    objAssessMaster._Assessoremployeeunkid = intEmployeeId
                ElseIf radExternalAssessor.Checked = True Then
                    objAssessMaster._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
                    objAssessMaster._Assessormasterunkid = -1
                    objAssessMaster._Assessoremployeeunkid = -1
                End If

                objAssessMaster._Reviewerunkid = -1
                objAssessMaster._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                objAssessMaster._Selfemployeeunkid = -1
                objAssessMaster._Assessoremployeeunkid = -1
                objAssessMaster._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                objAssessMaster._Reviewerunkid = User._Object._Userunkid
                objAssessMaster._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
                objAssessMaster._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
                Dim intEmployeeId As Integer = -1
                intEmployeeId = objAssessMaster.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
                objAssessMaster._Reviewerunkid = intEmployeeId
            End If
            objAssessMaster._Assessmentdate = dtpAssessdate.Value
            objAssessMaster._Assessgroupunkid = CInt(cboAssessGroup.SelectedValue)
            objAssessMaster._Yearunkid = CInt(cboYear.SelectedValue)
            objAssessMaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            objAssessMaster._Userunkid = User._Object._Userunkid
            objAssessMaster._Reviewer_Remark1 = txtRemark1.Text
            objAssessMaster._Reviewer_Remark2 = txtRemark2.Text
            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objAssessMaster._Committeddatetime <> Nothing Then
                objAssessMaster._Committeddatetime = objAssessMaster._Committeddatetime
            Else
                objAssessMaster._Committeddatetime = Nothing
            End If
            'S.SANDEEP [ 14 JUNE 2012 ] -- END
            If menAction = enAction.ADD_CONTINUE Then
                objAssessMaster._Isvoid = False
                objAssessMaster._Voiddatetime = Nothing
                objAssessMaster._Voidreason = ""
                objAssessMaster._Voiduserunkid = -1
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Return False
            ElseIf CInt(cboYear.SelectedValue) = 0 And menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
                cboYear.Select()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) = 0 And menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            ElseIf CInt(cboAssessGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Assessment Group is compulsory information.Please Select Assessment Group."), enMsgBoxStyle.Information)
                cboAssessGroup.Select()
                Return False
            ElseIf CInt(cboAssessItem.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Assessment Item is compulsory information.Please Select Assessment Item."), enMsgBoxStyle.Information)
                cboAssessItem.Select()
                Return False
            ElseIf CInt(cboResultCode.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Result Code is compulsory information.Please Select Result Code."), enMsgBoxStyle.Information)
                cboResultCode.Select()
                Return False
            ElseIf CInt(cboAssessor.SelectedValue) = 0 And menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors."), enMsgBoxStyle.Information)
                cboAssessor.Select()
                Return False
            End If

            'S.SANDEEP [ 24 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            'If dtpAssessdate.Value.Date > objPeriod._End_Date Or dtpAssessdate.Value.Date < objPeriod._Start_Date Then
            '    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & objPeriod._Start_Date & _
            '                           Language.getMessage(mstrModuleName, 9, " And ") & objPeriod._End_Date
            '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '    dtpAssessdate.Focus()
            '    Return False
            'End If
            'objPeriod = Nothing
            Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
            dsYr = oCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List", CInt(cboYear.SelectedValue))
            If dsYr.Tables("List").Rows.Count > 0 Then
                If dtpAssessdate.Value.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
                   dtpAssessdate.Value.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    dtpAssessdate.Focus()
                    Return False
                End If
            Else
                If dtpAssessdate.Value.Date > FinancialYear._Object._Database_End_Date.Date Or _
                   dtpAssessdate.Value.Date < FinancialYear._Object._Database_Start_Date.Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & FinancialYear._Object._Database_Start_Date.Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & FinancialYear._Object._Database_End_Date.Date
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                dtpAssessdate.Focus()
                Return False
            End If
            End If
            'S.SANDEEP [ 24 JULY 2013 ] -- END

            Dim dtTemp() As DataRow = dsItemList.Tables(0).Select("Id = '" & CInt(cboAssessItem.SelectedValue) & "'")
            If dtTemp.Length > 0 Then
                If CInt(dtTemp(0).Item("Cnt")) > 0 AndAlso CInt(cboSubItems.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Assessment Sub Item compulsory information.Please Select Assessment Sub Item."), enMsgBoxStyle.Information)
                    cboSubItems.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If CInt(cboReviewer.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), enMsgBoxStyle.Information)
                    cboReviewer.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If IsNumeric(cboResultCode.Text) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, You cannot add this result as it is not a numeric number."), enMsgBoxStyle.Information)
                cboResultCode.Focus()
                Return False
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillEvaluationList()
        Try
            Dim blnFlag As Boolean = False
            lvEvaluation.Items.Clear()
            Dim lvItem As New ListViewItem
            'For Each dtRow As DataRow In mdtEvaluation.Rows
            '    If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
            '        lvItem = New ListViewItem
            '        lvItem.Text = dtRow.Item("group_name").ToString
            '        lvItem.SubItems.Add(dtRow.Item("assess_subitem").ToString)
            '        lvItem.SubItems.Add(dtRow.Item("result").ToString)
            '        lvItem.SubItems.Add(dtRow.Item("remark").ToString)
            '        lvItem.SubItems.Add(dtRow.Item("assessitemunkid").ToString)
            '        lvItem.SubItems.Add(dtRow.Item("assess_subitemunkid").ToString)
            '        lvItem.SubItems.Add(dtRow.Item("resultunkid").ToString)
            '        lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
            '        lvItem.SubItems.Add(iCnt.ToString)

            '        lvItem.Tag = dtRow.Item("analysistranunkid")

            '        lvEvaluation.Items.Add(lvItem)
            '    End If
            'Next

            RemoveHandler lvEvaluation.ItemSelectionChanged, AddressOf lvEvaluation_ItemSelectionChanged

            lvEvaluation.GridLines = False

            If mdtEvaluation.Rows.Count <= 0 Then Exit Sub

            Dim mdecTotal As Decimal = 0

            For Each StrKey As Keys In mdicGroupWiseTotal.Keys
                mdecTotal += CDec(mdicGroupWiseTotal(StrKey))
                Dim dtRow() As DataRow = mdtEvaluation.Select("assessitemunkid = '" & StrKey & "' AND AUD <> 'D' ")
                Dim StrGrpName As String = ""
                If dtRow.Length > 0 Then
                    blnFlag = True
                    For i As Integer = 0 To dtRow.Length - 1
                        lvItem = New ListViewItem

                        lvItem.Text = dtRow(i).Item("group_name").ToString
                        lvItem.SubItems.Add(dtRow(i).Item("assess_subitem").ToString)
                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        'ENHANCEMENT : ENHANCEMENT
                        'lvItem.SubItems.Add(dtRow(i).Item("result").ToString)
                        If ConfigParameter._Object._ConsiderItemWeightAsNumber = True Then
                            If txtWeightage.Text.Trim.Length > 0 Then
                                lvItem.SubItems.Add(CStr(CDec(dtRow(i).Item("result")) * CDec(txtWeightage.Text)))
                            Else
                                lvItem.SubItems.Add(CStr(CDec(dtRow(i).Item("result")) * CDec(1)))
                            End If
                        Else
                        lvItem.SubItems.Add(dtRow(i).Item("result").ToString)
                        End If
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                        lvItem.SubItems.Add(dtRow(i).Item("remark").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("assessitemunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("assess_subitemunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("resultunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("GUID").ToString)
                        lvItem.SubItems.Add(iCnt.ToString)

                        lvItem.Tag = dtRow(i).Item("analysistranunkid")

                        lvEvaluation.Items.Add(lvItem)

                        lvItem = Nothing

                        StrGrpName = dtRow(i).Item("group_name").ToString
                    Next

                    lvItem = New ListViewItem

                    lvItem.UseItemStyleForSubItems = False

                    lvItem.Text = StrGrpName
                    lvItem.SubItems.Add("TOTAL : ", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                    lvItem.SubItems.Add(mdicGroupWiseTotal(StrKey).ToString, Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                    lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                    lvItem.SubItems.Add("-1")
                    lvItem.SubItems.Add("-1")
                    lvItem.SubItems.Add("-1")
                    lvItem.SubItems.Add("-1")
                    lvItem.SubItems.Add(iCnt.ToString)

                    lvItem.Tag = "-1"

                    lvEvaluation.Items.Add(lvItem)

                End If
            Next

            If blnFlag = True Then
                lvItem = New ListViewItem

                lvItem.UseItemStyleForSubItems = False

                lvItem.Text = ""
                lvItem.SubItems.Add("GRAND TOTAL : ", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add(mdecTotal.ToString, Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("999")

                lvItem.Tag = "-1"

                lvEvaluation.Items.Add(lvItem)
            End If

            If lvEvaluation.Items.Count > 2 Then
                colhAssessSubItem.Width = 528 - 20
            Else
                colhAssessSubItem.Width = 528
            End If

            lvEvaluation.GroupingColumn = objdgcolhAssessItem
            lvEvaluation.DisplayGroups(True)

            AddHandler lvEvaluation.ItemSelectionChanged, AddressOf lvEvaluation_ItemSelectionChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEvaluationList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetEvaluation()
        Try
            cboResultCode.SelectedValue = 0
            cboSubItems.SelectedValue = 0
            txtRemark.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetEvaluation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub DoOperation(ByVal blnOperation As Boolean)
        Try
            cboAssessGroup.Enabled = blnOperation
            cboAssessor.Enabled = blnOperation
            cboEmployee.Enabled = blnOperation
            cboPeriod.Enabled = blnOperation
            cboYear.Enabled = blnOperation
            dtpAssessdate.Enabled = blnOperation
            radExternalAssessor.Enabled = blnOperation
            radInternalAssessor.Enabled = blnOperation
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillImprovementList(ByVal ListView As eZee.Common.eZeeListView)
        Try

            ListView.Items.Clear()
            ListView.GridLines = False
            Dim dtTable As DataTable = Nothing
            Select Case ListView.Name.ToUpper
                Case "LVIMPROVEMENT"
                    dtTable = New DataView(mdtRemarks, "isimporvement = true", "", DataViewRowState.CurrentRows).ToTable
                Case "LVPERSONALDEVELOP"
                    dtTable = New DataView(mdtRemarks, "isimporvement = false", "", DataViewRowState.CurrentRows).ToTable
            End Select

            Dim lvItem As New ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("major_area").ToString
                    lvItem.SubItems.Add(dtRow.Item("activity").ToString)
                    lvItem.SubItems.Add(dtRow.Item("support_required").ToString)
                    lvItem.SubItems.Add(dtRow.Item("course_name").ToString)
                    lvItem.SubItems.Add(CDate(dtRow.Item("timeframe_date")).ToShortDateString)
                    lvItem.SubItems.Add(dtRow.Item("other_training").ToString)
                    lvItem.SubItems.Add(dtRow.Item("coursemasterunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvItem.Tag = dtRow.Item("remarkstranunkid")

                    ListView.Items.Add(lvItem)
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillImprovementList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetImprovement(ByVal blnFlag As Boolean)
        Try
            If blnFlag = True Then
                txtI_Action.Text = ""
                txtI_Improvement.Text = ""
                txtI_OtherTraining.Text = ""
                txtI_Support.Text = ""
                cboITraining.SelectedValue = 0
            Else
                txtP_Action.Text = ""
                txtP_Development.Text = ""
                txtP_OtherTraining.Text = ""
                txtP_Support.Text = ""
                cboPTraining.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetImprovement", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            Dim dtView As DataView = mdtEvaluation.DefaultView

            Dim iItemCount = objAssessMaster.GetItems_BaseGroup(CInt(cboAssessGroup.SelectedValue))

            If iItemCount <> dtView.ToTable(True, "assessitemunkid").Rows.Count Then
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all items in the selected group."), enMsgBoxStyle.Information)
                End If
                cboAssessItem.Focus()
                Return False
            End If

            If dsItemList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsItemList.Tables(0).Rows
                    Dim dtTemp() As DataRow = mdtEvaluation.Select("assessitemunkid = '" & CInt(dRow.Item("Id")) & "'")
                    If dtTemp.Length > 0 AndAlso CInt(dtTemp(0)("assess_subitemunkid")) <> 0 Then
                        If dtTemp.Length <> CInt(dRow.Item("Cnt")) Then
                            If blnFlag = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all subitems in some item(s)."), enMsgBoxStyle.Information)
                                cboSubItems.Focus()
                            End If
                            Return False
                        End If
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isAll_Assessed", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEvaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessMaster = New clsassess_analysis_master
        objAnalysisTran = New clsassess_analysis_tran
        objRemarkTran = New clsassess_remarks_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objAssessMaster._Analysisunkid = mintAssessAnalysisUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                cboAssessGroup.Enabled = False
                radExternalAssessor.Enabled = False
                radInternalAssessor.Enabled = False
                cboReviewer.Enabled = False
                objbtnSearchReviewer.Enabled = False
            End If
            Call GetValue()

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            objAnalysisTran._ConsiderWeightAsNumber = ConfigParameter._Object._ConsiderItemWeightAsNumber
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            objAnalysisTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtEvaluation = objAnalysisTran._DataTable
            mdicGroupWiseTotal = objAnalysisTran._GrpWiseTotal
            mdicItemWeight = objAnalysisTran._ItemWeight
            iCnt = objAnalysisTran._Grp_Counter
            Call FillEvaluationList()

            objRemarkTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtRemarks = objRemarkTran._DataTable
            Call FillImprovementList(lvImprovement)
            Call FillImprovementList(lvPersonalDevelop)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEvaluation_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEvaluation_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEvaluation_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEvaluation_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEvaluation_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEvaluation_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssessMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_analysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_analysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveCommit.Click
        'S.SANDEEP [ 22 JULY 2011 ] -- END 
        Dim blnFlag As Boolean = False
        Try
            If lvEvaluation.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please add atleast one evaluation in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Call SetValue()

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNSAVECOMMIT"
                    If ConfigParameter._Object._IsAllowFinalSave = False Then
                        If isAll_Assessed() = False Then Exit Sub
                        'S.SANDEEP [ 05 MARCH 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                            If lvImprovement.Items.Count <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, you cannot do Save Commit Operation as you have not provided any improvement."), enMsgBoxStyle.Information)
                                tabcEvaluation.SelectedTab = tabpImporvementArea
                                Exit Sub
                            End If
                        End If
                        'S.SANDEEP [ 05 MARCH 2012 ] -- END
                    ElseIf ConfigParameter._Object._IsAllowFinalSave = True Then
                        If isAll_Assessed(False) = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    End If
                    objAssessMaster._Iscommitted = True
                    'S.SANDEEP [ 14 JUNE 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objAssessMaster._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [ 14 JUNE 2012 ] -- END
            End Select

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAssessMaster.Update(mdtEvaluation, mdtRemarks)
            Else
                blnFlag = objAssessMaster.Insert(mdtEvaluation, mdtRemarks)
            End If

            If blnFlag = False And objAssessMaster._Message <> "" Then
                eZeeMsgBox.Show(objAssessMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                'S.SANDEEP [ 13 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNSAVECOMMIT" AndAlso objAssessMaster._Iscommitted = True Then
                    Select Case menAssess
                        Case enAssessmentMode.SELF_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objAssessMaster.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
                            '                                   CInt(cboEmployee.SelectedValue), _
                            '                                   CInt(cboPeriod.SelectedValue), _
                            '                                   CInt(cboYear.SelectedValue), _
                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , "", CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 22 OCT 2013 ] -- START {CInt(cboAssessGroup.SelectedValue)} -- END
                            objAssessMaster.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
                                                               CInt(cboEmployee.SelectedValue), _
                                                               CInt(cboPeriod.SelectedValue), _
                                                               CInt(cboYear.SelectedValue), _
                                                               ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , "", CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                            'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END

                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objAssessMaster.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
                            '                                   CInt(cboEmployee.SelectedValue), _
                            '                                   CInt(cboPeriod.SelectedValue), _
                            '                                   CInt(cboYear.SelectedValue), _
                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , cboAssessor.Text, CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 22 OCT 2013 ] -- START {CInt(cboAssessGroup.SelectedValue)} -- END)
                            objAssessMaster.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                               CInt(cboEmployee.SelectedValue), _
                                                               CInt(cboPeriod.SelectedValue), _
                                                               CInt(cboYear.SelectedValue), _
                                                               ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , cboAssessor.Text, CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                            'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END

                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objAssessMaster.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
                            '                                   CInt(cboEmployee.SelectedValue), _
                            '                                   CInt(cboPeriod.SelectedValue), _
                            '                                   CInt(cboYear.SelectedValue), _
                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , cboReviewer.Text, , , enLogin_Mode.DESKTOP, 0)
                            objAssessMaster.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                               CInt(cboEmployee.SelectedValue), _
                                                               CInt(cboPeriod.SelectedValue), _
                                                               CInt(cboYear.SelectedValue), _
                                                               ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , cboReviewer.Text, , , enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                            'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END
                    End Select
                End If
                'S.SANDEEP [ 13 AUG 2013 ] -- END
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAssessMaster = Nothing
                    objAssessMaster = New clsassess_analysis_master
                    Call GetValue()
                    Call DoOperation(True)
                    mdtEvaluation.Rows.Clear()
                    Call FillEvaluationList()
                    mdtRemarks.Rows.Clear()
                    Call FillImprovementList(lvImprovement)
                    Call FillImprovementList(lvPersonalDevelop)
                    mdicGroupWiseTotal.Clear()
                    mdicItemWeight.Clear()
                    iCnt = 0
                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboAssessor.SelectedValue) > 0 Then
                Dim dsList As New DataSet

                If radInternalAssessor.Checked = True Then
                    dsList = objAssessMaster.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                ElseIf radExternalAssessor.Checked = True Then
                    Dim objExtAssessor As New clsexternal_assessor_master
                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                End If

                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("AEmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub cboAssessGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessGroup.SelectedIndexChanged
    Private Sub cboAssessGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessGroup.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        'S.SANDEEP [ 28 DEC 2012 ] -- END
        Try
            If CInt(cboAssessGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objAssessItem As New clsassess_item_master

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objAssessItem.getListForCombo("Items", True, CInt(cboAssessGroup.SelectedValue))
                dsList = objAssessItem.getListForCombo("Items", True, CInt(cboAssessGroup.SelectedValue), , CInt(cboPeriod.SelectedValue))
                'S.SANDEEP [ 28 DEC 2012 ] -- END
                With cboAssessItem
                    .ValueMember = "assessitemunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Items")
                    .SelectedValue = 0
                End With
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsItemList = objAssessMaster.GetSubItemCount(CInt(cboAssessGroup.SelectedValue))
                dsItemList = objAssessMaster.GetSubItemCount(CInt(cboAssessGroup.SelectedValue), , CInt(cboPeriod.SelectedValue))
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            Else
                cboAssessItem.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            If CInt(cboYear.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objPeriod As New clscommom_period_Tran
                dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "APeriod", True, 1)
                With cboPeriod
                    .ValueMember = "periodunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("APeriod")
                    .SelectedValue = 0
                End With
            Else
                cboPeriod.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboEmployee.DataSource IsNot Nothing Then
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
                Select Case menAssess
                    Case enAssessmentMode.SELF_ASSESSMENT
            frm.CodeMember = "employeecode"
                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        'Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                        frm.CodeMember = "Code"
                End Select
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboAssessor.ValueMember
            frm.DisplayMember = cboAssessor.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboAssessor.SelectedValue = frm.SelectedValue
                cboAssessor.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub cboAssessItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAssessItem.SelectedIndexChanged
        Try
            If CInt(cboAssessItem.SelectedValue) > 0 Then
                Dim objItem As New clsassess_item_master
                Dim dsList As New DataSet
                Dim objResult As New clsresult_master
                objItem._Assessitemunkid = CInt(cboAssessItem.SelectedValue)

                If mdicItemWeight.ContainsKey(CInt(cboAssessItem.SelectedValue)) = False Then
                    mdicItemWeight.Add(CInt(cboAssessItem.SelectedValue), objItem._Weight)
                End If

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboAssessItem.SelectedValue) > 0 Then
                    txtWeightage.Text = objItem._Weight.ToString
                End If
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                dsList = objResult.getComboList("List", True, objItem._Resultgroupunkid)
                With cboResultCode
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("List")
                End With
                objItem = Nothing : dsList.Dispose() : objResult = Nothing

                dsList = New DataSet : Dim objSubItem As New clsassess_subitem_master
                dsList = objSubItem.getComboList("List", True, CInt(cboAssessItem.SelectedValue))
                With cboSubItems
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With
                'S.SANDEEP [ 09 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If dsList.Tables("List").Rows.Count > 0 Then
                    lnkScoreGuide.Enabled = True
                Else
                    lnkScoreGuide.Enabled = False
                End If
                'S.SANDEEP [ 09 AUG 2013 ] -- END
                dsList.Dispose() : objSubItem = Nothing
            Else
                cboSubItems.DataSource = Nothing : cboResultCode.DataSource = Nothing
                txtWeightage.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessItem_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim dsCombo As New DataSet : Dim objAssessGroup As New clsassess_group_master
                dsCombo = objAssessGroup.getListForCombo(CInt(cboEmployee.SelectedValue), "AssessGroup", True)
                With cboAssessGroup
                    .ValueMember = "assessgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("AssessGroup")
                End With
            Else
                cboAssessGroup.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResult.Click
        Dim frm As New frmCommonSearch
        Try
            If cboResultCode.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboResultCode.ValueMember
                    .DisplayMember = cboResultCode.DisplayMember
                    .DataSource = CType(cboResultCode.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboResultCode.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResult_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchSubItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSubItems.Click
        Dim frm As New frmCommonSearch
        Try
            If cboSubItems.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboSubItems.ValueMember
                    .DisplayMember = cboSubItems.DisplayMember
                    .DataSource = CType(cboSubItems.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboSubItems.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSubItems_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchItems.Click
        Dim frm As New frmCommonSearch
        Try
            If cboAssessItem.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboAssessItem.ValueMember
                    .DisplayMember = cboAssessItem.DisplayMember
                    .DataSource = CType(cboAssessItem.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboAssessItem.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchItems_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If cboAssessGroup.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboAssessGroup.ValueMember
                    .DisplayMember = cboAssessGroup.DisplayMember
                    .DataSource = CType(cboAssessGroup.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboAssessGroup.SelectedValue = frm.SelectedValue
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkViewAssessments_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkViewAssessments.LinkClicked
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
               CInt(cboAssessGroup.SelectedValue) <= 0 Or _
               CInt(cboYear.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please set following information [Employee,Year,Period and Group] to view assessment."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim frm As New frmCommonAssess_View
            frm.displayDialog(cboEmployee.Text, _
                              cboAssessGroup.Text, _
                              CInt(cboEmployee.SelectedValue), _
                              CInt(cboAssessGroup.SelectedValue), _
                              CInt(cboYear.SelectedValue), _
                              CInt(cboPeriod.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewAssessments_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchPTraning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPTraning.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboPTraining.ValueMember
                .DisplayMember = cboPTraining.DisplayMember
                .DataSource = CType(cboPTraining.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboPTraining.SelectedValue = frm.SelectedValue
                cboPTraining.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPTraning_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchITraning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchITraning.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboITraining.ValueMember
                .DisplayMember = cboITraining.DisplayMember
                .DataSource = CType(cboITraining.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboITraining.SelectedValue = frm.SelectedValue
                cboITraining.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchITraning_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub radInternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radInternalAssessor.CheckedChanged
        Try
            If radInternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim dsCombos As DataSet = objAssessMaster.getAssessorComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radInternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radExternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExternalAssessor.CheckedChanged
        Try
            If radExternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim objExtAssessor As New clsexternal_assessor_master
                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboReviewer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboReviewer.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dsList = objAssessMaster.getEmployeeBasedAssessor(CInt(cboReviewer.SelectedValue), "REmp", True)
                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("REmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReviewer_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchReviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboReviewer.ValueMember
            frm.DisplayMember = cboReviewer.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboReviewer.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboReviewer.SelectedValue = frm.SelectedValue
                cboReviewer.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 09 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboSubItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSubItems.SelectedIndexChanged
        Try
            If CInt(cboSubItems.SelectedValue) > 0 Then
                lnkScoreGuide.Enabled = True
            Else
                lnkScoreGuide.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSubItems_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkScoreGuide_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkScoreGuide.LinkClicked
        Try
            Dim mScoreGuide As String = String.Empty
            If CInt(cboSubItems.SelectedValue) > 0 Then
                Dim objSItem As New clsassess_subitem_master
                objSItem._Assess_Subitemunkid = CInt(cboSubItems.SelectedValue)
                mScoreGuide = objSItem._Description
                objSItem = Nothing
                If mScoreGuide <> "" Then
                    Dim frm As New frmScore_Guide
                    frm._Score_Guide = mScoreGuide
                    frm.ShowDialog()
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, No score guide is defined for the selected subitem in description."), enMsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkScoreGuide_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 09 AUG 2013 ] -- END

#End Region

#Region " Tab Events "

#Region " Evaluation "

    Private Sub btnAddEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddEvaluation.Click
        Try
            If Validation() = False Then Exit Sub

            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    If objAssessMaster.isExist(menAssess, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               , , , _
                                               mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, You have already assessed some items of particular group. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    If objAssessMaster.isExist(menAssess, _
                                       CInt(cboEmployee.SelectedValue), _
                                       CInt(cboYear.SelectedValue), _
                                       CInt(cboPeriod.SelectedValue), _
                                       CInt(cboAssessGroup.SelectedValue), _
                                       CInt(cboSubItems.SelectedValue), _
                                       , , _
                                       mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Selected subitem assessment is already present for this period. Please define new assessment."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    'S.SANDEEP [ 09 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                        If objAssessMaster.isExist(enAssessmentMode.SELF_ASSESSMENT, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               , , , _
                                              -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP [ 09 AUG 2013 ] -- END
                    If objAssessMaster.isExist(menAssess, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               , CInt(cboAssessor.SelectedValue), , _
                                               mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, You have already assessed some items of particular group. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If objAssessMaster.isExist(menAssess, _
                                       CInt(cboEmployee.SelectedValue), _
                                       CInt(cboYear.SelectedValue), _
                                       CInt(cboPeriod.SelectedValue), _
                                       CInt(cboAssessGroup.SelectedValue), _
                                       CInt(cboSubItems.SelectedValue), _
                                       CInt(cboAssessor.SelectedValue), _
                                       , _
                                       mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Selected subitem assessment is already present for this period. Please define new assessment."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    'S.SANDEEP [ 09 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                        If objAssessMaster.isExist(enAssessmentMode.SELF_ASSESSMENT, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               , , , _
                                              -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                        'S.SANDEEP [ 10 SEPT 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        If objAssessMaster.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                CInt(cboEmployee.SelectedValue), _
                                                CInt(cboYear.SelectedValue), _
                                                CInt(cboPeriod.SelectedValue), _
                                                CInt(cboAssessGroup.SelectedValue), _
                                                , , , _
                                                -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP [ 10 SEPT 2013 ] -- END

                    End If
                    'S.SANDEEP [ 09 AUG 2013 ] -- END
                    If objAssessMaster.isExist(menAssess, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               , , CInt(cboReviewer.SelectedValue), _
                                               mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, You have already assessed some items of particular group. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If objAssessMaster.isExist(menAssess, _
                                       CInt(cboEmployee.SelectedValue), _
                                       CInt(cboYear.SelectedValue), _
                                       CInt(cboPeriod.SelectedValue), _
                                       CInt(cboAssessGroup.SelectedValue), _
                                       CInt(cboSubItems.SelectedValue), _
                                       , CInt(cboReviewer.SelectedValue), _
                                       mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Selected subitem assessment is already present for this period. Please define new assessment."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select

            If CInt(cboSubItems.SelectedValue) > 0 Then
                Dim dtRow As DataRow() = mdtEvaluation.Select("assess_subitemunkid = '" & CInt(cboSubItems.SelectedValue) & "' AND AUD <> 'D' ")

                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot add same item again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                Dim dtRow As DataRow() = mdtEvaluation.Select("assessitemunkid = '" & CInt(cboAssessItem.SelectedValue) & "' AND AUD <> 'D' ")

                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot add same item again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If


            If mdicGroupWiseTotal.ContainsKey(CInt(cboAssessItem.SelectedValue)) Then
                If IsNumeric(cboResultCode.Text) = True Then
                    If (mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) + CDec(cboResultCode.Text)) > mdicItemWeight(CInt(cboAssessItem.SelectedValue)) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP [ 22 OCT 2013 ] -- START
                    'ENHANCEMENT : ENHANCEMENT
                    'mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) = (mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) + CDec(cboResultCode.Text))
                    If ConfigParameter._Object._ConsiderItemWeightAsNumber = True Then
                        If txtWeightage.Text.Trim.Length > 0 Then
                            mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) = (mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) + (CDec(cboResultCode.Text) * CDec(txtWeightage.Text)))
                        Else
                            mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) = (mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) + (CDec(cboResultCode.Text) * CDec(1)))
                        End If
                    Else
                    mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) = (mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) + CDec(cboResultCode.Text))
                End If
                    'S.SANDEEP [ 22 OCT 2013 ] -- END
                End If
            Else
                If IsNumeric(cboResultCode.Text) = True Then
                    If CDec(cboResultCode.Text) > mdicItemWeight(CInt(cboAssessItem.SelectedValue)) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP [ 22 OCT 2013 ] -- START
                    'ENHANCEMENT : ENHANCEMENT
                    'mdicGroupWiseTotal.Add(CInt(cboAssessItem.SelectedValue), CDec(cboResultCode.Text))
                    If ConfigParameter._Object._ConsiderItemWeightAsNumber = True Then
                        If txtWeightage.Text.Trim.Length > 0 Then
                            mdicGroupWiseTotal.Add(CInt(cboAssessItem.SelectedValue), (CDec(cboResultCode.Text) * CDec(txtWeightage.Text)))
                        Else
                            mdicGroupWiseTotal.Add(CInt(cboAssessItem.SelectedValue), (CDec(cboResultCode.Text) * CDec(1)))
                        End If
                    Else
                    mdicGroupWiseTotal.Add(CInt(cboAssessItem.SelectedValue), CDec(cboResultCode.Text))
                End If
                    'S.SANDEEP [ 22 OCT 2013 ] -- END
                End If
            End If

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dtTemp() As DataRow = mdtEvaluation.Select("assessitemunkid = '" & CInt(cboAssessItem.SelectedValue) & "' AND AUD <> 'D' ")
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            If dtTemp.Length <= 0 Then
                iCnt += 1
            End If


            Dim dRow As DataRow = mdtEvaluation.NewRow

            dRow.Item("analysistranunkid") = -1
            dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
            dRow.Item("assessitemunkid") = cboAssessItem.SelectedValue
            dRow.Item("assess_subitemunkid") = cboSubItems.SelectedValue
            dRow.Item("resultunkid") = cboResultCode.SelectedValue
            dRow.Item("remark") = txtRemark.Text
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("assessitem") = cboAssessItem.Text
            If CInt(cboSubItems.SelectedValue) > 0 Then
                dRow.Item("assess_subitem") = cboSubItems.Text
            Else
                dRow.Item("assess_subitem") = cboAssessItem.Text
            End If
            dRow.Item("result") = cboResultCode.Text
            If dtTemp.Length > 0 Then
                dRow.Item("group_name") = dtTemp(0)("group_name").ToString
            Else
            dRow.Item("group_name") = iCnt.ToString & ".0" & Space(5) & cboAssessItem.Text.ToString & Space(3) & "[ Weight : " & mdicItemWeight(CInt(cboAssessItem.SelectedValue)).ToString & "]"
            End If
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""

            mdtEvaluation.Rows.Add(dRow)

            Call FillEvaluationList()

            Call ResetEvaluation()

            Call DoOperation(False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddEvaluation_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditEvaluation.Click
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvEvaluation.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtEvaluation.Select("GUID = '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtEvaluation.Select("analysistranunkid = '" & CInt(lvEvaluation.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    If IsNumeric(cboResultCode.Text) Then
                        If mdecRemaininResult + CDec(cboResultCode.Text) > mdicItemWeight(CInt(drTemp(0)("assessitemunkid"))) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot set this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) = mdecRemaininResult + CDec(cboResultCode.Text)
                    End If
                End If


                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim dtTemp() As DataRow = mdtEvaluation.Select("assess_subitemunkid = '" & CInt(cboSubItems.SelectedValue) & "' AND GUID <> '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Dim dtTemp() As DataRow = mdtEvaluation.Select("assess_subitemunkid = '" & CInt(cboSubItems.SelectedValue) & "' AND GUID <> '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "' AND AUD <> 'D'")
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot add same item again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If Validation() = False Then Exit Sub

                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("analysistranunkid") = lvEvaluation.SelectedItems(0).Tag
                        .Item("analysisunkid") = mintAssessAnalysisUnkid
                        .Item("assessitemunkid") = cboAssessItem.SelectedValue
                        .Item("assess_subitemunkid") = cboSubItems.SelectedValue
                        .Item("resultunkid") = cboResultCode.SelectedValue
                        .Item("remark") = txtRemark.Text
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .Item("GUID") = Guid.NewGuid.ToString
                        .Item("assessitem") = cboAssessItem.Text
                        .Item("assess_subitem") = cboSubItems.Text
                        .Item("result") = cboResultCode.Text
                        If .Item("group_name").ToString.Trim.Length <= 0 Then
                            .Item("group_name") = iCnt.ToString & ".0" & Space(5) & cboAssessItem.Text.ToString & Space(3) & "[ Weight : " & mdicItemWeight(CInt(cboAssessItem.SelectedValue)).ToString & "]"
                        End If
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .AcceptChanges()
                    End With
                    Call FillEvaluationList()
                End If
                Call ResetEvaluation()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditEvaluation_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteEvaluation.Click
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvEvaluation.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtEvaluation.Select("GUID = '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtEvaluation.Select("analysistranunkid = '" & CInt(lvEvaluation.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then

                    If CInt(lvEvaluation.SelectedItems(0).Tag) > 0 Then
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)

                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If
                    Else
                        drTemp(0).Item("AUD") = "D"
                    End If

                    If mdicGroupWiseTotal.ContainsKey(CInt(drTemp(0)("assessitemunkid"))) Then
                        mdicGroupWiseTotal(CInt(drTemp(0)("assessitemunkid"))) = mdecRemaininResult
                    End If
                    Call FillEvaluationList()
                End If
                Call ResetEvaluation()
            End If

            If lvEvaluation.Items.Count <= 0 Then
                Call DoOperation(True)
                cboEmployee.SelectedValue = 0
                cboYear.SelectedValue = 0
                cboAssessGroup.SelectedValue = 0
                cboPeriod.SelectedValue = 0
                cboAssessItem.SelectedValue = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteEvaluation_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvEvaluation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvEvaluation.Click
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                cboAssessItem.SelectedValue = 0
                cboAssessItem.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhAssessItemId.Index).Text)
                cboSubItems.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhSubItemId.Index).Text)
                cboResultCode.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhResultId.Index).Text)
                If mdicGroupWiseTotal.ContainsKey(CInt(cboAssessItem.SelectedValue)) Then
                    If IsNumeric(cboResultCode.Text) Then
                        mdecRemaininResult = CDec(mdicGroupWiseTotal(CInt(cboAssessItem.SelectedValue)) - CDec(cboResultCode.Text))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEvaluation_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvEvaluation_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvEvaluation.ItemSelectionChanged
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                If e.Item.SubItems(objcolhGUID.Index).Text = "-1" Then
                    e.Item.Selected = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEvaluation_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Imporvement Area "

    Private Sub btnAddImprovement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddImprovement.Click
        Try


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND isimporvement = 1 ")


            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = True ")
            Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = True AND AUD <> 'D' ")
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            'Pinkal (06-Feb-2012) -- End



            If dtTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If txtI_Improvement.Text.Trim.Length <= 0 And _
               txtI_Action.Text.Trim.Length <= 0 And _
               txtI_OtherTraining.Text.Trim.Length <= 0 And _
               txtI_Support.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please write some remark in order to add into list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dRow As DataRow = mdtRemarks.NewRow

            dRow.Item("remarkstranunkid") = -1
            dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
            dRow.Item("coursemasterunkid") = cboITraining.SelectedValue
            dRow.Item("timeframe_date") = dtpITimeframe.Value
            dRow.Item("major_area") = txtI_Improvement.Text
            dRow.Item("activity") = txtI_Action.Text
            dRow.Item("support_required") = txtI_Support.Text
            dRow.Item("other_training") = txtI_OtherTraining.Text
            dRow.Item("isimporvement") = True
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"

            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dRow.Item("course_name") = cboITraining.Text
            If CInt(cboITraining.SelectedValue) > 0 Then
                dRow.Item("course_name") = cboITraining.Text
            Else
                dRow.Item("course_name") = ""
            End If
            'S.SANDEEP [ 23 JAN 2012 ] -- END
            dRow.Item("GUID") = Guid.NewGuid.ToString

            mdtRemarks.Rows.Add(dRow)

            Call FillImprovementList(lvImprovement)

            Call ResetImprovement(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditImprovement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditImprovement.Click
        Try
            If lvImprovement.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvImprovement.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvImprovement.SelectedItems(0).Tag) & "'")
                End If


                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "'")
                Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "' AND AUD <> 'D'")
                'S.SANDEEP [ 05 MARCH 2012 ] -- END



                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("remarkstranunkid") = lvImprovement.SelectedItems(0).Tag
                        .Item("analysisunkid") = mintAssessAnalysisUnkid
                        .Item("coursemasterunkid") = cboITraining.SelectedValue
                        .Item("timeframe_date") = dtpITimeframe.Value
                        .Item("major_area") = txtI_Improvement.Text
                        .Item("activity") = txtI_Action.Text
                        .Item("support_required") = txtI_Support.Text
                        .Item("other_training") = txtI_OtherTraining.Text
                        .Item("isimporvement") = True
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If


                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        '.Item("course_name") = cboITraining.Text

                        If CInt(cboITraining.SelectedValue) > 0 Then
                        .Item("course_name") = cboITraining.Text
                        Else
                            .Item("course_name") = ""
                        End If


                        'Pinkal (06-Feb-2012) -- End


                        .Item("GUID") = Guid.NewGuid.ToString
                        .AcceptChanges()
                    End With



                    Call FillImprovementList(lvImprovement)
                End If
                Call ResetImprovement(True)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteImprovement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteImprovement.Click
        Try
            If lvImprovement.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvImprovement.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvImprovement.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    If CInt(lvImprovement.SelectedItems(0).Tag) > 0 Then
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)

                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If
                    Else
                        drTemp(0).Item("AUD") = "D"
                    End If

                    Call FillImprovementList(lvImprovement)
                End If
                Call ResetImprovement(True)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvImprovement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvImprovement.Click
        Try
            If lvImprovement.SelectedItems.Count > 0 Then
                cboITraining.SelectedValue = 0
                txtI_Action.Text = lvImprovement.SelectedItems(0).SubItems(colhI_ActivityReq.Index).Text
                txtI_Improvement.Text = lvImprovement.SelectedItems(0).Text
                txtI_OtherTraining.Text = lvImprovement.SelectedItems(0).SubItems(colhI_OtherTraning.Index).Text
                txtI_Support.Text = lvImprovement.SelectedItems(0).SubItems(colhI_Support.Index).Text
                cboITraining.SelectedValue = lvImprovement.SelectedItems(0).SubItems(objcolhI_CourseId.Index).Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Personal Developement "

    Private Sub btnAddPersonal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPersonal.Click
        Try


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboPTraining.SelectedValue) & "' AND isimporvement = 0 ")


            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboPTraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = False ")
            Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboPTraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = False AND AUD <> 'D' ")
            'S.SANDEEP [ 05 MARCH 2012 ] -- END


            'Pinkal (06-Feb-2012) -- End



            If dtTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If txtP_Development.Text.Trim.Length <= 0 And _
               txtP_Action.Text.Trim.Length <= 0 And _
               txtP_OtherTraining.Text.Trim.Length <= 0 And _
               txtP_Support.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please write some remark in order to add into list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dRow As DataRow = mdtRemarks.NewRow

            dRow.Item("remarkstranunkid") = -1
            dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
            dRow.Item("coursemasterunkid") = cboPTraining.SelectedValue
            dRow.Item("timeframe_date") = dtpPTimeframe.Value
            dRow.Item("major_area") = txtP_Development.Text
            dRow.Item("activity") = txtP_Action.Text
            dRow.Item("support_required") = txtP_Support.Text
            dRow.Item("other_training") = txtP_OtherTraining.Text
            dRow.Item("isimporvement") = False
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"
            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dRow.Item("course_name") = cboPTraining.Text
            If CInt(cboPTraining.SelectedValue) > 0 Then
                dRow.Item("course_name") = cboPTraining.Text
            Else
                dRow.Item("course_name") = ""
            End If
            'S.SANDEEP [ 23 JAN 2012 ] -- END
            dRow.Item("GUID") = Guid.NewGuid.ToString

            mdtRemarks.Rows.Add(dRow)

            Call FillImprovementList(lvPersonalDevelop)

            Call ResetImprovement(False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddPersonal_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditPersonal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditPersonal.Click
        Try

            If lvPersonalDevelop.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvPersonalDevelop.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvPersonalDevelop.SelectedItems(0).Tag) & "'")
                End If

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "'")
                Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "' AND AUD <> 'D'")
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("remarkstranunkid") = lvPersonalDevelop.SelectedItems(0).Tag
                        .Item("analysisunkid") = mintAssessAnalysisUnkid
                        .Item("coursemasterunkid") = cboPTraining.SelectedValue
                        .Item("timeframe_date") = dtpPTimeframe.Value
                        .Item("major_area") = txtP_Development.Text
                        .Item("activity") = txtP_Action.Text
                        .Item("support_required") = txtP_Support.Text
                        .Item("other_training") = txtP_OtherTraining.Text
                        .Item("isimporvement") = False
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If


                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        '.Item("course_name") = cboPTraining.Text

                        If CInt(cboPTraining.SelectedValue) > 0 Then
                        .Item("course_name") = cboPTraining.Text
                        Else
                            .Item("course_name") = ""
                        End If
                        'Pinkal (06-Feb-2012) -- End


                        .Item("GUID") = Guid.NewGuid.ToString
                        .AcceptChanges()
                    End With
                    Call FillImprovementList(lvPersonalDevelop)
                End If
                Call ResetImprovement(False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditPersonal_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeletePersonal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeletePersonal.Click
        Try
            If lvPersonalDevelop.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvPersonalDevelop.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvPersonalDevelop.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    If CInt(lvPersonalDevelop.SelectedItems(0).Tag) > 0 Then
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)

                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If
                    Else
                        drTemp(0).Item("AUD") = "D"
                    End If

                    Call FillImprovementList(lvPersonalDevelop)
                End If
                Call ResetImprovement(False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvPersonalDevelop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvPersonalDevelop.Click
        Try
            If lvPersonalDevelop.SelectedItems.Count > 0 Then
                cboPTraining.SelectedValue = 0
                txtP_Action.Text = lvPersonalDevelop.SelectedItems(0).SubItems(colhP_ActivityReq.Index).Text
                txtP_Development.Text = lvPersonalDevelop.SelectedItems(0).Text
                txtP_OtherTraining.Text = lvPersonalDevelop.SelectedItems(0).SubItems(colhP_OtherTraning.Index).Text
                txtP_Support.Text = lvPersonalDevelop.SelectedItems(0).SubItems(colhP_Support.Index).Text
                cboPTraining.SelectedValue = lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_CourseId.Index).Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPersonalDevelop", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAssessmentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAssessmentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbImprovement.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbImprovement.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbPersonalDevelopment.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPersonalDevelopment.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbComments.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbComments.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddEvaluation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddEvaluation.GradientForeColor = GUI._ButttonFontColor

			Me.btnEditEvaluation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEditEvaluation.GradientForeColor = GUI._ButttonFontColor

			Me.btnDeleteEvaluation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDeleteEvaluation.GradientForeColor = GUI._ButttonFontColor

			Me.btnDeleteImprovement.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDeleteImprovement.GradientForeColor = GUI._ButttonFontColor

			Me.btnEditImprovement.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEditImprovement.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddImprovement.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddImprovement.GradientForeColor = GUI._ButttonFontColor

			Me.btnDeletePersonal.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDeletePersonal.GradientForeColor = GUI._ButttonFontColor

			Me.btnEditPersonal.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEditPersonal.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddPersonal.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddPersonal.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveCommit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveCommit.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.tabpCompetenceEvaluation.Text = Language._Object.getCaption(Me.tabpCompetenceEvaluation.Name, Me.tabpCompetenceEvaluation.Text)
			Me.tabpImporvementArea.Text = Language._Object.getCaption(Me.tabpImporvementArea.Name, Me.tabpImporvementArea.Text)
			Me.tabpReviewerComments.Text = Language._Object.getCaption(Me.tabpReviewerComments.Name, Me.tabpReviewerComments.Text)
			Me.gbAssessmentInfo.Text = Language._Object.getCaption(Me.gbAssessmentInfo.Name, Me.gbAssessmentInfo.Text)
			Me.lblSubItems.Text = Language._Object.getCaption(Me.lblSubItems.Name, Me.lblSubItems.Text)
			Me.btnAddEvaluation.Text = Language._Object.getCaption(Me.btnAddEvaluation.Name, Me.btnAddEvaluation.Text)
			Me.btnEditEvaluation.Text = Language._Object.getCaption(Me.btnEditEvaluation.Name, Me.btnEditEvaluation.Text)
			Me.btnDeleteEvaluation.Text = Language._Object.getCaption(Me.btnDeleteEvaluation.Name, Me.btnDeleteEvaluation.Text)
			Me.gbImprovement.Text = Language._Object.getCaption(Me.gbImprovement.Name, Me.gbImprovement.Text)
			Me.tabpPersonalDevelopment.Text = Language._Object.getCaption(Me.tabpPersonalDevelopment.Name, Me.tabpPersonalDevelopment.Text)
			Me.gbPersonalDevelopment.Text = Language._Object.getCaption(Me.gbPersonalDevelopment.Name, Me.gbPersonalDevelopment.Text)
			Me.gbComments.Text = Language._Object.getCaption(Me.gbComments.Name, Me.gbComments.Text)
			Me.elRemark1.Text = Language._Object.getCaption(Me.elRemark1.Name, Me.elRemark1.Text)
			Me.elRemark2.Text = Language._Object.getCaption(Me.elRemark2.Name, Me.elRemark2.Text)
			Me.lblAssessGroup.Text = Language._Object.getCaption(Me.lblAssessGroup.Name, Me.lblAssessGroup.Text)
			Me.lblAssessmentItems.Text = Language._Object.getCaption(Me.lblAssessmentItems.Name, Me.lblAssessmentItems.Text)
			Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
			Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblAssessDate.Text = Language._Object.getCaption(Me.lblAssessDate.Name, Me.lblAssessDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblILearningObjective.Text = Language._Object.getCaption(Me.lblILearningObjective.Name, Me.lblILearningObjective.Text)
			Me.lblITime.Text = Language._Object.getCaption(Me.lblITime.Name, Me.lblITime.Text)
			Me.tabpImprovement.Text = Language._Object.getCaption(Me.tabpImprovement.Name, Me.tabpImprovement.Text)
			Me.tapbActionRequired.Text = Language._Object.getCaption(Me.tapbActionRequired.Name, Me.tapbActionRequired.Text)
			Me.tabpSupportRequired.Text = Language._Object.getCaption(Me.tabpSupportRequired.Name, Me.tabpSupportRequired.Text)
			Me.tabpOtherTraining.Text = Language._Object.getCaption(Me.tabpOtherTraining.Name, Me.tabpOtherTraining.Text)
			Me.btnDeleteImprovement.Text = Language._Object.getCaption(Me.btnDeleteImprovement.Name, Me.btnDeleteImprovement.Text)
			Me.btnEditImprovement.Text = Language._Object.getCaption(Me.btnEditImprovement.Name, Me.btnEditImprovement.Text)
			Me.btnAddImprovement.Text = Language._Object.getCaption(Me.btnAddImprovement.Name, Me.btnAddImprovement.Text)
			Me.btnDeletePersonal.Text = Language._Object.getCaption(Me.btnDeletePersonal.Name, Me.btnDeletePersonal.Text)
			Me.btnEditPersonal.Text = Language._Object.getCaption(Me.btnEditPersonal.Name, Me.btnEditPersonal.Text)
			Me.btnAddPersonal.Text = Language._Object.getCaption(Me.btnAddPersonal.Name, Me.btnAddPersonal.Text)
			Me.lblPLearningObjective.Text = Language._Object.getCaption(Me.lblPLearningObjective.Name, Me.lblPLearningObjective.Text)
			Me.lblPTime.Text = Language._Object.getCaption(Me.lblPTime.Name, Me.lblPTime.Text)
			Me.tabpDevelopment.Text = Language._Object.getCaption(Me.tabpDevelopment.Name, Me.tabpDevelopment.Text)
			Me.tabpPAction.Text = Language._Object.getCaption(Me.tabpPAction.Name, Me.tabpPAction.Text)
			Me.tabpPSupport.Text = Language._Object.getCaption(Me.tabpPSupport.Name, Me.tabpPSupport.Text)
			Me.tabpPOtherTraining.Text = Language._Object.getCaption(Me.tabpPOtherTraining.Name, Me.tabpPOtherTraining.Text)
			Me.btnSaveCommit.Text = Language._Object.getCaption(Me.btnSaveCommit.Name, Me.btnSaveCommit.Text)
			Me.colhAssessSubItem.Text = Language._Object.getCaption(CStr(Me.colhAssessSubItem.Tag), Me.colhAssessSubItem.Text)
			Me.colhResult.Text = Language._Object.getCaption(CStr(Me.colhResult.Tag), Me.colhResult.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
			Me.colhI_Improvement.Text = Language._Object.getCaption(CStr(Me.colhI_Improvement.Tag), Me.colhI_Improvement.Text)
			Me.colhI_ActivityReq.Text = Language._Object.getCaption(CStr(Me.colhI_ActivityReq.Tag), Me.colhI_ActivityReq.Text)
			Me.colhI_Support.Text = Language._Object.getCaption(CStr(Me.colhI_Support.Tag), Me.colhI_Support.Text)
			Me.colhI_Course.Text = Language._Object.getCaption(CStr(Me.colhI_Course.Tag), Me.colhI_Course.Text)
			Me.colhI_Timeframe.Text = Language._Object.getCaption(CStr(Me.colhI_Timeframe.Tag), Me.colhI_Timeframe.Text)
			Me.colhI_OtherTraning.Text = Language._Object.getCaption(CStr(Me.colhI_OtherTraning.Tag), Me.colhI_OtherTraning.Text)
            Me.colhP_Development.Text = Language._Object.getCaption(CStr(Me.colhP_Development.Tag), Me.colhP_Development.Text)
			Me.colhP_ActivityReq.Text = Language._Object.getCaption(CStr(Me.colhP_ActivityReq.Tag), Me.colhP_ActivityReq.Text)
			Me.colhP_Support.Text = Language._Object.getCaption(CStr(Me.colhP_Support.Tag), Me.colhP_Support.Text)
			Me.colhP_Course.Text = Language._Object.getCaption(CStr(Me.colhP_Course.Tag), Me.colhP_Course.Text)
			Me.colhP_Timeframe.Text = Language._Object.getCaption(CStr(Me.colhP_Timeframe.Tag), Me.colhP_Timeframe.Text)
			Me.colhP_OtherTraning.Text = Language._Object.getCaption(CStr(Me.colhP_OtherTraning.Tag), Me.colhP_OtherTraning.Text)
			Me.lnkViewAssessments.Text = Language._Object.getCaption(Me.lnkViewAssessments.Name, Me.lnkViewAssessments.Text)
			Me.lblReviewer.Text = Language._Object.getCaption(Me.lblReviewer.Name, Me.lblReviewer.Text)
			Me.radExternalAssessor.Text = Language._Object.getCaption(Me.radExternalAssessor.Name, Me.radExternalAssessor.Text)
			Me.radInternalAssessor.Text = Language._Object.getCaption(Me.radInternalAssessor.Name, Me.radInternalAssessor.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lnkScoreGuide.Text = Language._Object.getCaption(Me.lnkScoreGuide.Name, Me.lnkScoreGuide.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee.")
			Language.setMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year.")
			Language.setMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period.")
			Language.setMessage(mstrModuleName, 4, "Assessment Group is compulsory information.Please Select Assessment Group.")
			Language.setMessage(mstrModuleName, 5, "Assessment Item is compulsory information.Please Select Assessment Item.")
			Language.setMessage(mstrModuleName, 6, "Result Code is compulsory information.Please Select Result Code.")
			Language.setMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors.")
			Language.setMessage(mstrModuleName, 8, "Assessment date should be in between")
			Language.setMessage(mstrModuleName, 9, " And")
			Language.setMessage(mstrModuleName, 10, "Self")
			Language.setMessage(mstrModuleName, 11, "Appraiser's")
			Language.setMessage(mstrModuleName, 12, "Reviewer's")
			Language.setMessage(mstrModuleName, 13, "Sorry, you cannot add same item again in the list.")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group.")
			Language.setMessage(mstrModuleName, 15, "Sorry, you cannot set this result as it exceeds the weight assigned to particular item group.")
			Language.setMessage(mstrModuleName, 16, "Sorry, you cannot add same training again in the list.")
			Language.setMessage(mstrModuleName, 17, "Please write some remark in order to add into list.")
			Language.setMessage(mstrModuleName, 18, "Please add atleast one evaluation in order to save.")
			Language.setMessage(mstrModuleName, 19, "Sorry, you cannot add same training again in the list.")
			Language.setMessage(mstrModuleName, 20, "Sorry, you cannot add same item again in the list.")
			Language.setMessage(mstrModuleName, 21, "Selected subitem assessment is already present for this period. Please define new assessment.")
			Language.setMessage(mstrModuleName, 22, "Please set following information [Employee,Year,Period and Group] to view assessment.")
			Language.setMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all items in the selected group.")
			Language.setMessage(mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all subitems in some item(s).")
			Language.setMessage(mstrModuleName, 25, "Assessment Sub Item compulsory information.Please Select Assessment Sub Item.")
			Language.setMessage(mstrModuleName, 26, "Sorry, You have already assessed some items of particular group. Please open in edit mode and add new items.")
			Language.setMessage(mstrModuleName, 27, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
			Language.setMessage(mstrModuleName, 28, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer.")
			Language.setMessage(mstrModuleName, 29, "Sorry, You cannot add this result as it is not a numeric number.")
			Language.setMessage(mstrModuleName, 30, "Sorry, No score guide is defined for the selected subitem in description.")
			Language.setMessage(mstrModuleName, 31, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period.")
			Language.setMessage(mstrModuleName, 32, "Sorry, you cannot do Save Commit Operation as you have not provided any improvement.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class