﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessmentWeight
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessmentWeight))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblGeneralEvaluation = New System.Windows.Forms.Label
        Me.txtGeneralWeight = New eZee.TextBox.NumericTextBox
        Me.lblBalanceScoreCard = New System.Windows.Forms.Label
        Me.txtBSCWeight = New eZee.TextBox.NumericTextBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objchkCheck = New System.Windows.Forms.CheckBox
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.gbRatio = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.pnlData = New System.Windows.Forms.Panel
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbRatio.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 386)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(432, 55)
        Me.objFooter.TabIndex = 12
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(219, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(322, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblGeneralEvaluation
        '
        Me.lblGeneralEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGeneralEvaluation.Location = New System.Drawing.Point(12, 63)
        Me.lblGeneralEvaluation.Name = "lblGeneralEvaluation"
        Me.lblGeneralEvaluation.Size = New System.Drawing.Size(116, 16)
        Me.lblGeneralEvaluation.TabIndex = 13
        Me.lblGeneralEvaluation.Text = "General Evaluation %"
        Me.lblGeneralEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGeneralWeight
        '
        Me.txtGeneralWeight.AllowNegative = False
        Me.txtGeneralWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtGeneralWeight.DigitsInGroup = 0
        Me.txtGeneralWeight.Flags = 65536
        Me.txtGeneralWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGeneralWeight.Location = New System.Drawing.Point(134, 61)
        Me.txtGeneralWeight.MaxDecimalPlaces = 2
        Me.txtGeneralWeight.MaxWholeDigits = 9
        Me.txtGeneralWeight.Name = "txtGeneralWeight"
        Me.txtGeneralWeight.Prefix = ""
        Me.txtGeneralWeight.RangeMax = 1.7976931348623157E+308
        Me.txtGeneralWeight.RangeMin = -1.7976931348623157E+308
        Me.txtGeneralWeight.Size = New System.Drawing.Size(65, 21)
        Me.txtGeneralWeight.TabIndex = 14
        Me.txtGeneralWeight.Text = "0.00"
        Me.txtGeneralWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBalanceScoreCard
        '
        Me.lblBalanceScoreCard.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceScoreCard.Location = New System.Drawing.Point(205, 63)
        Me.lblBalanceScoreCard.Name = "lblBalanceScoreCard"
        Me.lblBalanceScoreCard.Size = New System.Drawing.Size(141, 16)
        Me.lblBalanceScoreCard.TabIndex = 15
        Me.lblBalanceScoreCard.Text = "Balance Score Card %"
        Me.lblBalanceScoreCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBSCWeight
        '
        Me.txtBSCWeight.AllowNegative = False
        Me.txtBSCWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtBSCWeight.DigitsInGroup = 0
        Me.txtBSCWeight.Flags = 65536
        Me.txtBSCWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBSCWeight.Location = New System.Drawing.Point(352, 61)
        Me.txtBSCWeight.MaxDecimalPlaces = 2
        Me.txtBSCWeight.MaxWholeDigits = 9
        Me.txtBSCWeight.Name = "txtBSCWeight"
        Me.txtBSCWeight.Prefix = ""
        Me.txtBSCWeight.RangeMax = 1.7976931348623157E+308
        Me.txtBSCWeight.RangeMin = -1.7976931348623157E+308
        Me.txtBSCWeight.Size = New System.Drawing.Size(65, 21)
        Me.txtBSCWeight.TabIndex = 16
        Me.txtBSCWeight.Text = "0.00"
        Me.txtBSCWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhName, Me.objdgcolhId})
        Me.dgvData.Location = New System.Drawing.Point(2, 23)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(404, 243)
        Me.dgvData.TabIndex = 20
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhName
        '
        Me.dgcolhName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhName.HeaderText = ""
        Me.dgcolhName.Name = "dgcolhName"
        '
        'objdgcolhId
        '
        Me.objdgcolhId.HeaderText = "objdgcolhJId"
        Me.objdgcolhId.Name = "objdgcolhId"
        Me.objdgcolhId.Visible = False
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(2, 2)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(404, 21)
        Me.txtSearch.TabIndex = 107
        '
        'objchkCheck
        '
        Me.objchkCheck.AutoSize = True
        Me.objchkCheck.Location = New System.Drawing.Point(9, 29)
        Me.objchkCheck.Name = "objchkCheck"
        Me.objchkCheck.Size = New System.Drawing.Size(15, 14)
        Me.objchkCheck.TabIndex = 111
        Me.objchkCheck.UseVisualStyleBackColor = True
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(444, 34)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 170
        Me.objbtnSearchPeriod.Visible = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(134, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(283, 21)
        Me.cboPeriod.TabIndex = 169
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(116, 16)
        Me.lblPeriod.TabIndex = 168
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbRatio
        '
        Me.gbRatio.BorderColor = System.Drawing.Color.Black
        Me.gbRatio.Checked = False
        Me.gbRatio.CollapseAllExceptThis = False
        Me.gbRatio.CollapsedHoverImage = Nothing
        Me.gbRatio.CollapsedNormalImage = Nothing
        Me.gbRatio.CollapsedPressedImage = Nothing
        Me.gbRatio.CollapseOnLoad = False
        Me.gbRatio.Controls.Add(Me.lblAllocation)
        Me.gbRatio.Controls.Add(Me.pnlData)
        Me.gbRatio.Controls.Add(Me.cboAllocations)
        Me.gbRatio.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbRatio.Controls.Add(Me.lblPeriod)
        Me.gbRatio.Controls.Add(Me.cboPeriod)
        Me.gbRatio.Controls.Add(Me.txtGeneralWeight)
        Me.gbRatio.Controls.Add(Me.txtBSCWeight)
        Me.gbRatio.Controls.Add(Me.lblBalanceScoreCard)
        Me.gbRatio.Controls.Add(Me.lblGeneralEvaluation)
        Me.gbRatio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbRatio.ExpandedHoverImage = Nothing
        Me.gbRatio.ExpandedNormalImage = Nothing
        Me.gbRatio.ExpandedPressedImage = Nothing
        Me.gbRatio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRatio.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRatio.HeaderHeight = 25
        Me.gbRatio.HeaderMessage = ""
        Me.gbRatio.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbRatio.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRatio.HeightOnCollapse = 0
        Me.gbRatio.LeftTextSpace = 0
        Me.gbRatio.Location = New System.Drawing.Point(0, 0)
        Me.gbRatio.Name = "gbRatio"
        Me.gbRatio.OpenHeight = 300
        Me.gbRatio.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRatio.ShowBorder = True
        Me.gbRatio.ShowCheckBox = False
        Me.gbRatio.ShowCollapseButton = False
        Me.gbRatio.ShowDefaultBorderColor = True
        Me.gbRatio.ShowDownButton = False
        Me.gbRatio.ShowHeader = True
        Me.gbRatio.Size = New System.Drawing.Size(432, 386)
        Me.gbRatio.TabIndex = 171
        Me.gbRatio.Temp = 0
        Me.gbRatio.Text = "Assessment Ratio Information"
        Me.gbRatio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(12, 91)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(116, 15)
        Me.lblAllocation.TabIndex = 173
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.txtSearch)
        Me.pnlData.Controls.Add(Me.objchkCheck)
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(12, 113)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(408, 268)
        Me.pnlData.TabIndex = 172
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(134, 88)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(283, 21)
        Me.cboAllocations.TabIndex = 172
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "objdgcolhJId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'frmAssessmentWeight
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(432, 441)
        Me.Controls.Add(Me.gbRatio)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessmentWeight"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessment Ratios"
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbRatio.ResumeLayout(False)
        Me.gbRatio.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblGeneralEvaluation As System.Windows.Forms.Label
    Friend WithEvents txtGeneralWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBalanceScoreCard As System.Windows.Forms.Label
    Friend WithEvents txtBSCWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objchkCheck As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents gbRatio As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
