﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessor_usermapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessor_usermapping))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAssessordetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lvDepartment = New eZee.Common.eZeeListView(Me.components)
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.lblUser = New System.Windows.Forms.Label
        Me.objlblJob = New System.Windows.Forms.Label
        Me.objlblSection = New System.Windows.Forms.Label
        Me.objlblDepartment = New System.Windows.Forms.Label
        Me.objlblAssessor = New System.Windows.Forms.Label
        Me.lblAssessor = New System.Windows.Forms.Label
        Me.objlblApproverLevel = New System.Windows.Forms.Label
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.objFooter.SuspendLayout()
        Me.gbAssessordetail.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 252)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(410, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(197, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(94, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(300, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbAssessordetail
        '
        Me.gbAssessordetail.BorderColor = System.Drawing.Color.Black
        Me.gbAssessordetail.Checked = False
        Me.gbAssessordetail.CollapseAllExceptThis = False
        Me.gbAssessordetail.CollapsedHoverImage = Nothing
        Me.gbAssessordetail.CollapsedNormalImage = Nothing
        Me.gbAssessordetail.CollapsedPressedImage = Nothing
        Me.gbAssessordetail.CollapseOnLoad = False
        Me.gbAssessordetail.Controls.Add(Me.objbtnSearchUser)
        Me.gbAssessordetail.Controls.Add(Me.lvDepartment)
        Me.gbAssessordetail.Controls.Add(Me.cboUser)
        Me.gbAssessordetail.Controls.Add(Me.lblUser)
        Me.gbAssessordetail.Controls.Add(Me.objlblJob)
        Me.gbAssessordetail.Controls.Add(Me.objlblSection)
        Me.gbAssessordetail.Controls.Add(Me.objlblDepartment)
        Me.gbAssessordetail.Controls.Add(Me.objlblAssessor)
        Me.gbAssessordetail.Controls.Add(Me.lblAssessor)
        Me.gbAssessordetail.Controls.Add(Me.objlblApproverLevel)
        Me.gbAssessordetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbAssessordetail.ExpandedHoverImage = Nothing
        Me.gbAssessordetail.ExpandedNormalImage = Nothing
        Me.gbAssessordetail.ExpandedPressedImage = Nothing
        Me.gbAssessordetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssessordetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssessordetail.HeaderHeight = 25
        Me.gbAssessordetail.HeaderMessage = ""
        Me.gbAssessordetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAssessordetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssessordetail.HeightOnCollapse = 0
        Me.gbAssessordetail.LeftTextSpace = 0
        Me.gbAssessordetail.Location = New System.Drawing.Point(0, 0)
        Me.gbAssessordetail.Name = "gbAssessordetail"
        Me.gbAssessordetail.OpenHeight = 300
        Me.gbAssessordetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssessordetail.ShowBorder = True
        Me.gbAssessordetail.ShowCheckBox = False
        Me.gbAssessordetail.ShowCollapseButton = False
        Me.gbAssessordetail.ShowDefaultBorderColor = True
        Me.gbAssessordetail.ShowDownButton = False
        Me.gbAssessordetail.ShowHeader = True
        Me.gbAssessordetail.Size = New System.Drawing.Size(410, 252)
        Me.gbAssessordetail.TabIndex = 10
        Me.gbAssessordetail.Temp = 0
        Me.gbAssessordetail.Text = "Assessor Detail"
        Me.gbAssessordetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvDepartment
        '
        Me.lvDepartment.BackColorOnChecked = True
        Me.lvDepartment.ColumnHeaders = Nothing
        Me.lvDepartment.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDepartment, Me.colhEmployee})
        Me.lvDepartment.CompulsoryColumns = ""
        Me.lvDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDepartment.FullRowSelect = True
        Me.lvDepartment.GridLines = True
        Me.lvDepartment.GroupingColumn = Nothing
        Me.lvDepartment.HideSelection = False
        Me.lvDepartment.Location = New System.Drawing.Point(11, 63)
        Me.lvDepartment.MinColumnWidth = 50
        Me.lvDepartment.MultiSelect = False
        Me.lvDepartment.Name = "lvDepartment"
        Me.lvDepartment.OptionalColumns = ""
        Me.lvDepartment.ShowMoreItem = False
        Me.lvDepartment.ShowSaveItem = False
        Me.lvDepartment.ShowSelectAll = True
        Me.lvDepartment.ShowSizeAllColumnsToFit = True
        Me.lvDepartment.Size = New System.Drawing.Size(364, 150)
        Me.lvDepartment.Sortable = True
        Me.lvDepartment.TabIndex = 294
        Me.lvDepartment.TabStop = False
        Me.lvDepartment.UseCompatibleStateImageBehavior = False
        Me.lvDepartment.View = System.Windows.Forms.View.Details
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 0
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 360
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(87, 219)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(288, 21)
        Me.cboUser.TabIndex = 1
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(11, 222)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(70, 14)
        Me.lblUser.TabIndex = 291
        Me.lblUser.Text = "User"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblJob
        '
        Me.objlblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblJob.Location = New System.Drawing.Point(119, 141)
        Me.objlblJob.Name = "objlblJob"
        Me.objlblJob.Size = New System.Drawing.Size(246, 14)
        Me.objlblJob.TabIndex = 289
        Me.objlblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSection
        '
        Me.objlblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSection.Location = New System.Drawing.Point(119, 115)
        Me.objlblSection.Name = "objlblSection"
        Me.objlblSection.Size = New System.Drawing.Size(246, 14)
        Me.objlblSection.TabIndex = 286
        Me.objlblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblDepartment
        '
        Me.objlblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDepartment.Location = New System.Drawing.Point(119, 89)
        Me.objlblDepartment.Name = "objlblDepartment"
        Me.objlblDepartment.Size = New System.Drawing.Size(246, 14)
        Me.objlblDepartment.TabIndex = 284
        Me.objlblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblAssessor
        '
        Me.objlblAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAssessor.Location = New System.Drawing.Point(84, 37)
        Me.objlblAssessor.Name = "objlblAssessor"
        Me.objlblAssessor.Size = New System.Drawing.Size(291, 14)
        Me.objlblAssessor.TabIndex = 282
        Me.objlblAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessor
        '
        Me.lblAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessor.Location = New System.Drawing.Point(8, 37)
        Me.lblAssessor.Name = "lblAssessor"
        Me.lblAssessor.Size = New System.Drawing.Size(70, 14)
        Me.lblAssessor.TabIndex = 281
        Me.lblAssessor.Text = "Assessor"
        Me.lblAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApproverLevel
        '
        Me.objlblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApproverLevel.Location = New System.Drawing.Point(119, 63)
        Me.objlblApproverLevel.Name = "objlblApproverLevel"
        Me.objlblApproverLevel.Size = New System.Drawing.Size(220, 14)
        Me.objlblApproverLevel.TabIndex = 280
        Me.objlblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(381, 219)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 296
        '
        'frmAssessor_usermapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(410, 307)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbAssessordetail)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessor_usermapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Mapping"
        Me.objFooter.ResumeLayout(False)
        Me.gbAssessordetail.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbAssessordetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvDepartment As eZee.Common.eZeeListView
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents objlblJob As System.Windows.Forms.Label
    Friend WithEvents objlblSection As System.Windows.Forms.Label
    Friend WithEvents objlblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblAssessor As System.Windows.Forms.Label
    Friend WithEvents lblAssessor As System.Windows.Forms.Label
    Friend WithEvents objlblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
End Class
