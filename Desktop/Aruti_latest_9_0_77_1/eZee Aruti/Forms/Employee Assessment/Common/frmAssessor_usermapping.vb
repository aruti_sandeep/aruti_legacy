﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAssessor_usermapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAssessor_usermapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objUserMapping As clsapprover_Usermapping
    Private mintAssessorunkid As Integer = -1
    Friend mintMappingUnkid As Integer = -1
    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnReviewer As Boolean = False
    'S.SANDEEP [ 10 APR 2013 ] -- END
#End Region

#Region " Display Dialog "

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal _intAssessorunkid As Integer) As Boolean
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal _intAssessorunkid As Integer, ByVal blnIsReviewer As Boolean) As Boolean
        'S.SANDEEP [ 19 JULY 2012 ] -- END
        Try
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnIsReviewer = True Then
                Me.Name = "frmReviewer_UserMappoing"
                Me.Text = Language.getMessage(mstrModuleName, 100, "Reviewer User Mapping")
                lblAssessor.Text = Language.getMessage(mstrModuleName, 103, "Reviewer")
                'S.SANDEEP [ 25 JULY 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                gbAssessordetail.Text = Language.getMessage(mstrModuleName, 103, "Reviewer")
                'S.SANDEEP [ 25 JULY 2013 ] -- END
            Else
                Me.Name = "frmAssessor_Usermapping"

                'S.SANDEEP [ 25 JULY 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Me.Text = Language.getMessage(mstrModuleName, 101, "Assessir User Mapping")
                'lblAssessor.Text = Language.getMessage(mstrModuleName, 103, "Reviewer")
                Me.Text = Language.getMessage(mstrModuleName, 101, "Assessor User Mapping")
                lblAssessor.Text = Language.getMessage(mstrModuleName, 102, "Assessor")
                gbAssessordetail.Text = Language.getMessage(mstrModuleName, 102, "Assessor")
                'S.SANDEEP [ 25 JULY 2013 ] -- END
            End If
            'S.SANDEEP [ 19 JULY 2012 ] -- END
            
            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnReviewer = blnIsReviewer
            'S.SANDEEP [ 10 APR 2013 ] -- END


            mintMappingUnkid = intUnkId
            mintAssessorunkid = _intAssessorunkid
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboUser.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboUser.SelectedValue = objUserMapping._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objUserMapping._Approverunkid = mintAssessorunkid
            objUserMapping._Userunkid = CInt(cboUser.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objUser As New clsUserAddEdit

            'S.SANDEEP [ 11 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objUser.getComboList("User", True)
            Dim objPswd As New clsPassowdOptions

            'S.SHARMA [ 22 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If objPswd._IsEmployeeAsUser Then
            '    dsList = objUser.getComboList("User", True, False, True)
            'Else
            '    dsList = objUser.getComboList("User", True, False)
            'End If
            'objPswd = Nothing
            If objPswd._IsEmployeeAsUser Then
                If Me.Name <> "frmReviewer_UserMappoing" Then
                    dsList = objUser.getComboList("User", True, False, True, Company._Object._Companyunkid, 270, FinancialYear._Object._YearUnkid)
                Else
                    dsList = objUser.getComboList("User", True, False, True, Company._Object._Companyunkid, 506, FinancialYear._Object._YearUnkid)
                End If
            Else
                dsList = objUser.getComboList("User", True, False)
            End If
            objPswd = Nothing
            'S.SHARMA [ 22 JAN 2013 ] -- END

            'S.SANDEEP [ 11 DEC 2012 ] -- END

            cboUser.ValueMember = "userunkid"
            cboUser.DisplayMember = "name"
            cboUser.DataSource = dsList.Tables("User")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Dim lvItem As ListViewItem
        Try

            Dim objAssessor As New clsAssessor_tran
            objAssessor._Assessormasterunkid = mintAssessorunkid

            'START FOR ASSESSOR DEPARTMENT(S)
            dsFill = objAssessor.GetData()

            If dsFill.Tables(0).Rows.Count > 0 Then
                lvDepartment.Items.Clear()

                For Each drRow As DataRow In dsFill.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = CInt(drRow("departmentunkid"))
                    lvItem.Text = drRow("departmentname").ToString
                    lvItem.SubItems.Add(drRow("employeename").ToString)
                    lvDepartment.Items.Add(lvItem)
                Next

                lvDepartment.GridLines = False
                lvDepartment.GroupingColumn = colhDepartment
                lvDepartment.DisplayGroups(True)

                If lvDepartment.Items.Count > 1 Then
                    colhEmployee.Width = 360 - 18
                Else
                    colhEmployee.Width = 360
                End If

            End If

            'END FOR ASSESSOR DEPARTMENT(S)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAssessor_usermapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUserMapping = New clsapprover_Usermapping
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            SetColor()
            FillCombo()
            FillList()
            objUserMapping._UserTypeid = enUserType.Assessor

            Dim objAssessorMst As New clsAssessor
            Dim dsList As DataSet = objAssessorMst.GetList("Assessor", True)
            Dim dtList As DataTable = New DataView(dsList.Tables("Assessor"), "assessormasterunkid =" & mintAssessorunkid, "", DataViewRowState.CurrentRows).ToTable

            If dtList.Rows.Count > 0 Then
                objlblAssessor.Text = dtList.Rows(0)("assessorname").ToString
            End If

            If menAction = enAction.EDIT_ONE Then
                objUserMapping._Mappingunkid = mintMappingUnkid
                btnDelete.Enabled = True
            Else
                btnDelete.Enabled = False
            End If
            lvDepartment.GridLines = False
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessor_usermapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessor_usermapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessor_usermapping_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessor_usermapping_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessor_usermapping_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessor_usermapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objUserMapping = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsapprover_Usermapping.SetMessages()
            objfrm._Other_ModuleNames = "clsapprover_Usermapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboUser.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "User is compulsory information.Please Select User."), enMsgBoxStyle.Information)
                cboUser.Select()
                Exit Sub
            End If

            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objUserMapping.isUserMappingExists(enUserType.Assessor, mintAssessorunkid, CInt(cboUser.SelectedValue), mblnReviewer) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot map the selected user with this assessor." & vbCrLf & _
                                                                       "Reason : This user is already mapped with some assessor", ), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 10 APR 2013 ] -- END

            SetValue()

            If mintMappingUnkid > 0 Then
                blnFlag = objUserMapping.Update()
            Else
                blnFlag = objUserMapping.Insert()
            End If


            If blnFlag = False And objUserMapping._Message <> "" Then
                eZeeMsgBox.Show(objUserMapping._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                mintMappingUnkid = objUserMapping._Mappingunkid
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If CInt(cboUser.SelectedValue) < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select User from the list to perform further operation on it."), enMsgBoxStyle.Information)
                cboUser.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this User Mapping?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objUserMapping.Delete(mintMappingUnkid)
                cboUser.SelectedIndex = 0
                mintMappingUnkid = 0
                btnDelete.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboUser.ValueMember
                .DisplayMember = cboUser.DisplayMember
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
                cboUser.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 23 JAN 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAssessordetail.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAssessordetail.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbAssessordetail.Text = Language._Object.getCaption(Me.gbAssessordetail.Name, Me.gbAssessordetail.Text)
			Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
			Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
			Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "User is compulsory information.Please Select User.")
			Language.setMessage(mstrModuleName, 2, "Please select User from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this User Mapping?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


