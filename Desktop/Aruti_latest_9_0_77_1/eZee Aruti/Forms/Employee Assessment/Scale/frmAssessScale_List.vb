﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessScale_List

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmAssessScale_List"
    Private objScaleMaster As clsAssessment_Scale

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim objCMaster As New clsCommon_Master
        Dim objCPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP, True, "List")
            With cboScaleGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim iSearch As String = String.Empty
        Try
            RemoveHandler lvScaleList.ItemChecked, AddressOf lvScaleList_ItemChecked
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            dsList = objScaleMaster.GetList("List")
            lvScaleList.Items.Clear()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                iSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If

            If CInt(cboScaleGroup.SelectedValue) > 0 Then
                iSearch &= "AND scalemasterunkid = '" & CInt(cboScaleGroup.SelectedValue) & "' "
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                dtTable = New DataView(dsList.Tables(0), iSearch, "name", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "name", DataViewRowState.CurrentRows).ToTable
            End If

            For Each drow As DataRow In dtTable.Rows
                Dim lvitem As New ListViewItem

                lvitem.Text = ""
                lvitem.SubItems.Add(drow.Item("name").ToString)
                lvitem.SubItems.Add(drow.Item("scale").ToString)
                lvitem.SubItems.Add(drow.Item("description").ToString)
                lvitem.SubItems.Add(drow.Item("pname").ToString)
                lvitem.Tag = drow.Item("scaletranunkid")

                'S.SANDEEP [11-OCT-2018] -- START
                lvitem.SubItems.Add(drow.Item("pct_from").ToString & " - " & drow.Item("pct_to").ToString)
                'S.SANDEEP [11-OCT-2018] -- END

                'Shani(18-Feb-2016) -- Start
                If CInt(drow.Item("statusid")) = enStatusType.Close Then
                    lvitem.ForeColor = Color.Gray
                End If
                'Shani(18-Feb-2016) -- End

                lvScaleList.Items.Add(lvitem)
            Next
            lvScaleList.GroupingColumn = objcolhPeriod
            lvScaleList.DisplayGroups(True)
            If lvScaleList.Items.Count >= 2 Then
                'S.SANDEEP [11-OCT-2018] -- START
                '    colhDescription.Width = 350 - 20
                'Else
                '    colhDescription.Width = 350
                colhDescription.Width = 300 - 20
            Else
                colhDescription.Width = 300
                'S.SANDEEP [11-OCT-2018] -- END
            End If
            AddHandler lvScaleList.ItemChecked, AddressOf lvScaleList_ItemChecked
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnNew.Enabled = User._Object.Privilege._AllowtoAddAssessmentScales
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditAssessmentScales
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteAssessmentScales
            mnuAssignGroup_Goals.Enabled = User._Object.Privilege._AllowtoMapScaleGrouptoGoals
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAssessScale_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objScaleMaster = New clsAssessment_Scale
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Fill_Combo()
            Call SetVisibility()

            If lvScaleList.Items.Count > 0 Then lvScaleList.Items(0).Selected = True
            lvScaleList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessScale_List_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessScale_List_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objScaleMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessment_Scale.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessment_Scale"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAssessScale_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmAssessScale_AddEdit
        Try
            If lvScaleList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assessment Scale from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvScaleList.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(CInt(lvScaleList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvScaleList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check atleast one scale/score to remove."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If lvScaleList.CheckedItems.Count > 0 Then
                If eZeeMsgBox.Show("Are you sure you want to delete checked scale?", CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            Dim blnIsShown As Boolean = False
            For Each lvItem As ListViewItem In lvScaleList.CheckedItems
                If objScaleMaster.isUsed(CInt(lvItem.Tag)) Then
                    If blnIsShown = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Some of the checked scale/score are already linked with some transaction.") & vbCrLf & _
                                                            Language.getMessage(mstrModuleName, 4, "Due to this linked scale/score will be discarded."), enMsgBoxStyle.Information)
                        blnIsShown = True
                        Continue For
                    Else
                        Continue For
                    End If
                Else

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objScaleMaster._FormName = mstrModuleName
                    objScaleMaster._LoginEmployeeunkid = 0
                    objScaleMaster._ClientIP = getIP()
                    objScaleMaster._HostName = getHostName()
                    objScaleMaster._FromWeb = False
                    objScaleMaster._AuditUserId = User._Object._Userunkid
objScaleMaster._CompanyUnkid = Company._Object._Companyunkid
                    objScaleMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    objScaleMaster.Delete(CInt(lvItem.Tag))
                End If
            Next
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboScaleGroup.ValueMember
                .DisplayMember = cboScaleGroup.DisplayMember
                .DataSource = CType(cboScaleGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboScaleGroup.SelectedValue = .SelectedValue
                    cboScaleGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboScaleGroup.SelectedValue = 0 : lvScaleList.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control Events "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvScaleList.ItemChecked, AddressOf lvScaleList_ItemChecked
            For Each item As ListViewItem In lvScaleList.Items
                item.Checked = CBool(objChkAll.CheckState)
            Next
            AddHandler lvScaleList.ItemChecked, AddressOf lvScaleList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvScaleList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvScaleList.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvScaleList.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvScaleList.CheckedItems.Count < lvScaleList.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvScaleList.CheckedItems.Count = lvScaleList.Items.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvScaleList_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuAssignGroup_Goals_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssignGroup_Goals.Click
        Dim frm As New frmMapScaleGroup_Fields
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssignGroup_Goals_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.colhScale.Text = Language._Object.getCaption(CStr(Me.colhScale.Tag), Me.colhScale.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Assessment Scale from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Please check atleast one scale/score to remove.")
			Language.setMessage(mstrModuleName, 3, "Sorry, Some of the checked scale/score are already linked with some transaction." & vbCrLf & _
                                                            "Due to this linked scale/score will be discarded.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class