﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMapScaleGroup_Fields

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMapScaleGroup_Fields"
    Private mintScaleMappingTranId As Integer = 0
    Private objScaleMapping As clsassess_scalemapping_tran
    Private mstrMappedFieldName As String = String.Empty
    Private mintMappedFieldId As Integer = 0

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorComp
            cboScaleGroup.BackColor = GUI.ColorComp
            txtMappedCaption.BackColor = GUI.ColorComp
            cboPerspective.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtMappedCaption.Text = mstrMappedFieldName
            txtMappedCaption.Tag = mintMappedFieldId
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            objScaleMapping._Periodunkid = CInt(cboPeriod.SelectedValue)
            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            objScaleMapping._Mappingunkid = mintMappedFieldId
            RemoveHandler cboScaleGroup.SelectedIndexChanged, AddressOf cboScaleGroup_SelectedIndexChanged
            objScaleMapping._Scalemasterunkid = CInt(cboScaleGroup.SelectedValue)
            AddHandler cboScaleGroup.SelectedIndexChanged, AddressOf cboScaleGroup_SelectedIndexChanged
            objScaleMapping._Voiddatetime = Nothing
            objScaleMapping._Voidreason = ""
            objScaleMapping._Voiduserunkid = -1
            objScaleMapping._Isvoid = False
            objScaleMapping._Perspectiveunkid = CInt(cboPerspective.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboScaleGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Scale Group is mandatory information. Please select Scale Group to continue."), enMsgBoxStyle.Information)
                cboScaleGroup.Focus()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                If txtMappedCaption.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No field is mapped for the selected period in assessment field mapping. Please map the field with selected period to continue."), enMsgBoxStyle.Information)
                    cboScaleGroup.Focus()
                    Return False
                End If

                If CInt(cboScaleGroup.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Scale is define for the selected period. Please define Scale to continue."), enMsgBoxStyle.Information)
                    cboScaleGroup.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [ 01 JAN 2015 ] -- START
            If CInt(cboPerspective.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Perspective is mandatory information. Please select Perspective to continue."), enMsgBoxStyle.Information)
                cboScaleGroup.Focus()
                Return False
            End If
            'S.SANDEEP [ 01 JAN 2015 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Combo()
        Dim objCPeriod As New clscommom_period_Tran
        Dim objPerspective As New clsassess_perspective_master
        Dim dsCombo As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            dsCombo = objPerspective.getComboList("List", True)
            With cboPerspective
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub Fill_Mapping_Data()
        Try
            Dim dsMap As New DataSet
            dsMap = objScaleMapping.GetList("List")
            lvMapped.Items.Clear()
            For Each xRow As DataRow In dsMap.Tables(0).Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(xRow.Item("Perspective").ToString)
                lvItem.SubItems.Add(xRow.Item("Scale_Group").ToString)
                lvItem.SubItems.Add(xRow.Item("Period").ToString)
                lvItem.SubItems.Add(xRow.Item("scalemasterunkid").ToString)
                lvItem.SubItems.Add(xRow.Item("perspectiveunkid").ToString)

                lvItem.Tag = xRow.Item("assignmenttranunkid").ToString

                lvMapped.Items.Add(lvItem)
            Next

            lvMapped.GroupingColumn = objcolhPeriod
            lvMapped.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Mapping_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmMapScaleGroup_Fields_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objScaleMapping = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMapScaleGroup_Fields_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmMapScaleGroup_Fields_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objScaleMapping = New clsassess_scalemapping_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            Call Fill_Combo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMapScaleGroup_Fields_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsassess_scalemapping_tran.SetMessages()
            'objfrm._Other_ModuleNames = "clsassess_scalemapping_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            If objScaleMapping._Assignmenttranunkid > 0 Then
                If CInt(cboPerspective.SelectedValue) > 0 Then
                    If objScaleMapping.isExist(CInt(cboPeriod.SelectedValue), CInt(cboPerspective.SelectedValue), , objScaleMapping._Assignmenttranunkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Previous setting is define for the selected period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                ElseIf CInt(cboScaleGroup.SelectedValue) > 0 Then
                    If objScaleMapping.isExist(CInt(cboPeriod.SelectedValue), , CInt(cboScaleGroup.SelectedValue), objScaleMapping._Assignmenttranunkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Previous setting is define for the selected period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                If CInt(cboPerspective.SelectedValue) > 0 AndAlso CInt(cboScaleGroup.SelectedValue) > 0 Then
                    If objScaleMapping.isExist(CInt(cboPeriod.SelectedValue), CInt(cboPerspective.SelectedValue), CInt(cboScaleGroup.SelectedValue), objScaleMapping._Assignmenttranunkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Previous setting is define for the selected period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                objScaleMapping._Assignmenttranunkid = mintScaleMappingTranId

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objScaleMapping._FormName = mstrModuleName
                objScaleMapping._LoginEmployeeunkid = 0
                objScaleMapping._ClientIP = getIP()
                objScaleMapping._HostName = getHostName()
                objScaleMapping._FromWeb = False
                objScaleMapping._AuditUserId = User._Object._Userunkid
objScaleMapping._CompanyUnkid = Company._Object._Companyunkid
                objScaleMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                Call SetValue()
                blnFlag = objScaleMapping.Update()
            Else
                If CInt(cboPerspective.SelectedValue) > 0 Then
                    If objScaleMapping.isExist(CInt(cboPeriod.SelectedValue), CInt(cboPerspective.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Previous setting is define for the selected period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                ElseIf CInt(cboScaleGroup.SelectedValue) > 0 Then
                    If objScaleMapping.isExist(CInt(cboPeriod.SelectedValue), , CInt(cboScaleGroup.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Previous setting is define for the selected period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                If CInt(cboPerspective.SelectedValue) > 0 AndAlso CInt(cboScaleGroup.SelectedValue) > 0 Then
                    If objScaleMapping.isExist(CInt(cboPeriod.SelectedValue), CInt(cboPerspective.SelectedValue), CInt(cboScaleGroup.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Previous setting is define for the selected period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                Call SetValue()
                With objScaleMapping
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                blnFlag = objScaleMapping.Insert()
            End If
                If blnFlag = False AndAlso objScaleMapping._Message <> "" Then
                    eZeeMsgBox.Show(objScaleMapping._Message, enMsgBoxStyle.Information)
                End If
                If blnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Mapping saved successfully."), enMsgBoxStyle.Information)
                    objScaleMapping = New clsassess_scalemapping_tran
                    cboPerspective.SelectedValue = 0 : cboScaleGroup.SelectedValue = 0
                    Call Fill_Mapping_Data()
                End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvMapped.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Please check atleast one mapped item to delete."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to remove selected period and scale group mapping?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim iVoidReason As String = ""
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                If iVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                For Each lvItem As ListViewItem In lvMapped.CheckedItems                    
                    objScaleMapping._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objScaleMapping._Voidreason = iVoidReason
                    objScaleMapping._Voiduserunkid = User._Object._Userunkid
                    objScaleMapping._Isvoid = True

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objScaleMapping._FormName = mstrModuleName
                    objScaleMapping._LoginEmployeeunkid = 0
                    objScaleMapping._ClientIP = getIP()
                    objScaleMapping._HostName = getHostName()
                    objScaleMapping._FromWeb = False
                    objScaleMapping._AuditUserId = User._Object._Userunkid
objScaleMapping._CompanyUnkid = Company._Object._Companyunkid
                    objScaleMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objScaleMapping.Delete(CInt(lvItem.Tag)) Then
                        objScaleMapping = New clsassess_scalemapping_tran
                    End If
                Next
                Call Fill_Mapping_Data()
            End If

            'If mintScaleMappingTranId <= 0 Then Exit Sub
            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to remove selected period and scale group mapping?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '    '<TODO : CHECK IS-USED> 'CANNOT BE DONE COZ STORING DATA VALUE INSTEAD OF ID'S OF THE TABLE.
            '    Dim frm As New frmReasonSelection
            '    Dim iVoidReason As String = ""
            '    If User._Object._Isrighttoleft = True Then
            '        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '        frm.RightToLeftLayout = True
            '        Call Language.ctlRightToLeftlayOut(frm)
            '    End If
            '    frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
            '    If iVoidReason.Length <= 0 Then
            '        Exit Sub
            '    End If
            '    objScaleMapping._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            '    objScaleMapping._Voidreason = iVoidReason
            '    objScaleMapping._Voiduserunkid = User._Object._Userunkid
            '    objScaleMapping._Isvoid = True
            '    If objScaleMapping.Delete(mintScaleMappingTranId) Then
            '        objScaleMapping = New clsassess_scalemapping_tran
            '        cboPeriod.SelectedValue = 0
            '    End If
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchPerspective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPerspective.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboPerspective.ValueMember
                .DisplayMember = cboPerspective.DisplayMember
                .DataSource = CType(cboPerspective.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPerspective.SelectedValue = .SelectedValue
                    cboPerspective.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPerspective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchScale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchScale.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboScaleGroup.ValueMember
                .DisplayMember = cboScaleGroup.DisplayMember
                .DataSource = CType(cboScaleGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboScaleGroup.SelectedValue = .SelectedValue
                    cboScaleGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchScale_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls Event(s) "

    Private Sub objSCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSCheckAll.CheckedChanged
        Try
            RemoveHandler lvMapped.ItemChecked, AddressOf lvMapped_ItemChecked
            For Each lvitem As ListViewItem In lvMapped.Items
                lvitem.Checked = CBool(objSCheckAll.CheckState)
            Next
            AddHandler lvMapped.ItemChecked, AddressOf lvMapped_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSCheckAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvMapped_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvMapped.ItemChecked
        Try
            RemoveHandler objSCheckAll.CheckedChanged, AddressOf objSCheckAll_CheckedChanged
            If lvMapped.CheckedItems.Count <= 0 Then
                objSCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvMapped.CheckedItems.Count < lvMapped.Items.Count Then
                objSCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvMapped.CheckedItems.Count = lvMapped.Items.Count Then
                objSCheckAll.CheckState = CheckState.Checked
            End If
            AddHandler objSCheckAll.CheckedChanged, AddressOf objSCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMapped_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvMapped_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvMapped.SelectedIndexChanged
        Try
            If lvMapped.SelectedItems.Count > 0 Then
                objScaleMapping._Assignmenttranunkid = CInt(lvMapped.SelectedItems(0).Tag)
                cboScaleGroup.SelectedValue = CInt(lvMapped.SelectedItems(0).SubItems(objcolhScaleMasterId.Index).Text)
                cboPerspective.SelectedValue = CInt(lvMapped.SelectedItems(0).SubItems(objcolhPerspectiveId.Index).Text)
            Else
                objScaleMapping._Assignmenttranunkid = 0
                cboScaleGroup.SelectedValue = 0
                cboPerspective.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMapped_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet : Dim objAScale As New clsAssessment_Scale
            dsList = objAScale.getComboList_ScaleGroup(CInt(cboPeriod.SelectedValue), "List", True)
            With cboScaleGroup
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'objScaleMapping._Periodunkid = CInt(cboPeriod.SelectedValue)
            'mintScaleMappingTranId = objScaleMapping._Assignmenttranunkid
            'If mintScaleMappingTranId > 0 Then
            '    btnDelete.Enabled = True
            'Else
            '    btnDelete.Enabled = False
            'End If
            Dim objMapping As New clsAssess_Field_Mapping
            mintMappedFieldId = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
            mstrMappedFieldName = objMapping.Get_Map_FieldName(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing
            Call GetValue()
            If CInt(cboPeriod.SelectedValue) > 0 Then
                btnDelete.Enabled = True : Call Fill_Mapping_Data()
            Else
                btnDelete.Enabled = False : lvMapped.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboScaleGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboScaleGroup.SelectedIndexChanged
        Try
            If CInt(cboScaleGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim dtTable As DataTable
                Dim objScaleMaster As New clsAssessment_Scale
                dsList = objScaleMaster.GetList("List")
                dtTable = New DataView(dsList.Tables(0), "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND scalemasterunkid = '" & CInt(cboScaleGroup.SelectedValue) & "' ", "name", DataViewRowState.CurrentRows).ToTable
                lvScaleList.Items.Clear()
                For Each drow As DataRow In dtTable.Rows
                    Dim lvitem As New ListViewItem
                    lvitem.Text = drow.Item("scale").ToString
                    lvitem.SubItems.Add(drow.Item("description").ToString)
                    lvScaleList.Items.Add(lvitem)
                Next
                If lvScaleList.Items.Count >= 2 Then
                    colhDescription.Width = 230 - 20
                Else
                    colhDescription.Width = 230
                End If
            Else
                lvScaleList.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboScaleGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAssignementInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAssignementInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbAssignementInfo.Text = Language._Object.getCaption(Me.gbAssignementInfo.Name, Me.gbAssignementInfo.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblMappedCaption.Text = Language._Object.getCaption(Me.lblMappedCaption.Name, Me.lblMappedCaption.Text)
			Me.lblScaleScoreGroup.Text = Language._Object.getCaption(Me.lblScaleScoreGroup.Name, Me.lblScaleScoreGroup.Text)
			Me.colhScale.Text = Language._Object.getCaption(CStr(Me.colhScale.Tag), Me.colhScale.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Scale Group is mandatory information. Please select Scale Group to continue.")
			Language.setMessage(mstrModuleName, 3, "Sorry, No field is mapped for the selected period in assessment field mapping. Please map the field with selected period to continue.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Scale is define for the selected period. Please define Scale to continue.")
			Language.setMessage(mstrModuleName, 5, "Setting for the period is already present in the system. Do you wish to overwrite old with this new one?")
			Language.setMessage(mstrModuleName, 6, "Mapping saved successfully.")
			Language.setMessage(mstrModuleName, 7, "Are you sure you want to remove selected period and scale group mapping?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class