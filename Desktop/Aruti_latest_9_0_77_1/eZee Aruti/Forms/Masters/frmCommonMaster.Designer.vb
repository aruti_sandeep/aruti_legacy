﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonMaster
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonMaster))
        Me.pnlMasterDetails = New System.Windows.Forms.Panel
        Me.gbMasterListInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.chkUseInAssetDeclaration = New System.Windows.Forms.CheckBox
        Me.chkTrainingRequisition = New System.Windows.Forms.CheckBox
        Me.chkMandatoryForNewEmp = New System.Windows.Forms.CheckBox
        Me.chkSyncWithRecruitment = New System.Windows.Forms.CheckBox
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.chkAllowedinReferee = New System.Windows.Forms.CheckBox
        Me.rdCareerDevelopment = New System.Windows.Forms.RadioButton
        Me.rdJobCapability = New System.Windows.Forms.RadioButton
        Me.lblLevelDesc = New System.Windows.Forms.Label
        Me.nudLevel = New System.Windows.Forms.NumericUpDown
        Me.lblLevel = New System.Windows.Forms.Label
        Me.lblCode = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.pnlMasterItemList = New System.Windows.Forms.Panel
        Me.lvMasterListInfo = New System.Windows.Forms.ListView
        Me.colhAlias = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhQlevel = New System.Windows.Forms.ColumnHeader
        Me.colhDecription = New System.Windows.Forms.ColumnHeader
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtAlias = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.lblAlias = New System.Windows.Forms.Label
        Me.gbMasterList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlMasterTableList = New System.Windows.Forms.Panel
        Me.lvMasterList = New System.Windows.Forms.ListView
        Me.colhMasterList = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMasterDetails.SuspendLayout()
        Me.gbMasterListInfo.SuspendLayout()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMasterItemList.SuspendLayout()
        Me.gbMasterList.SuspendLayout()
        Me.pnlMasterTableList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMasterDetails
        '
        Me.pnlMasterDetails.Controls.Add(Me.gbMasterListInfo)
        Me.pnlMasterDetails.Controls.Add(Me.gbMasterList)
        Me.pnlMasterDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMasterDetails.Location = New System.Drawing.Point(0, 0)
        Me.pnlMasterDetails.Name = "pnlMasterDetails"
        Me.pnlMasterDetails.Size = New System.Drawing.Size(767, 509)
        Me.pnlMasterDetails.TabIndex = 0
        '
        'gbMasterListInfo
        '
        Me.gbMasterListInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMasterListInfo.Checked = False
        Me.gbMasterListInfo.CollapseAllExceptThis = False
        Me.gbMasterListInfo.CollapsedHoverImage = Nothing
        Me.gbMasterListInfo.CollapsedNormalImage = Nothing
        Me.gbMasterListInfo.CollapsedPressedImage = Nothing
        Me.gbMasterListInfo.CollapseOnLoad = False
        Me.gbMasterListInfo.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbMasterListInfo.Controls.Add(Me.chkUseInAssetDeclaration)
        Me.gbMasterListInfo.Controls.Add(Me.chkTrainingRequisition)
        Me.gbMasterListInfo.Controls.Add(Me.chkMandatoryForNewEmp)
        Me.gbMasterListInfo.Controls.Add(Me.chkSyncWithRecruitment)
        Me.gbMasterListInfo.Controls.Add(Me.txtDescription)
        Me.gbMasterListInfo.Controls.Add(Me.lblDescription)
        Me.gbMasterListInfo.Controls.Add(Me.chkAllowedinReferee)
        Me.gbMasterListInfo.Controls.Add(Me.rdCareerDevelopment)
        Me.gbMasterListInfo.Controls.Add(Me.rdJobCapability)
        Me.gbMasterListInfo.Controls.Add(Me.lblLevelDesc)
        Me.gbMasterListInfo.Controls.Add(Me.nudLevel)
        Me.gbMasterListInfo.Controls.Add(Me.lblLevel)
        Me.gbMasterListInfo.Controls.Add(Me.lblCode)
        Me.gbMasterListInfo.Controls.Add(Me.txtCode)
        Me.gbMasterListInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbMasterListInfo.Controls.Add(Me.objLine1)
        Me.gbMasterListInfo.Controls.Add(Me.pnlMasterItemList)
        Me.gbMasterListInfo.Controls.Add(Me.txtName)
        Me.gbMasterListInfo.Controls.Add(Me.txtAlias)
        Me.gbMasterListInfo.Controls.Add(Me.lblName)
        Me.gbMasterListInfo.Controls.Add(Me.lblAlias)
        Me.gbMasterListInfo.ExpandedHoverImage = Nothing
        Me.gbMasterListInfo.ExpandedNormalImage = Nothing
        Me.gbMasterListInfo.ExpandedPressedImage = Nothing
        Me.gbMasterListInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMasterListInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMasterListInfo.HeaderHeight = 25
        Me.gbMasterListInfo.HeaderMessage = ""
        Me.gbMasterListInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMasterListInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMasterListInfo.HeightOnCollapse = 0
        Me.gbMasterListInfo.LeftTextSpace = 0
        Me.gbMasterListInfo.Location = New System.Drawing.Point(257, 64)
        Me.gbMasterListInfo.Name = "gbMasterListInfo"
        Me.gbMasterListInfo.OpenHeight = 336
        Me.gbMasterListInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMasterListInfo.ShowBorder = True
        Me.gbMasterListInfo.ShowCheckBox = False
        Me.gbMasterListInfo.ShowCollapseButton = False
        Me.gbMasterListInfo.ShowDefaultBorderColor = True
        Me.gbMasterListInfo.ShowDownButton = False
        Me.gbMasterListInfo.ShowHeader = True
        Me.gbMasterListInfo.Size = New System.Drawing.Size(500, 384)
        Me.gbMasterListInfo.TabIndex = 1
        Me.gbMasterListInfo.Temp = 0
        Me.gbMasterListInfo.Text = "Master Lists Details"
        Me.gbMasterListInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(343, 6)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(128, 13)
        Me.lnkAdvanceFilter.TabIndex = 310
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Bind Allocation"
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkUseInAssetDeclaration
        '
        Me.chkUseInAssetDeclaration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUseInAssetDeclaration.Location = New System.Drawing.Point(201, 37)
        Me.chkUseInAssetDeclaration.Name = "chkUseInAssetDeclaration"
        Me.chkUseInAssetDeclaration.Size = New System.Drawing.Size(193, 17)
        Me.chkUseInAssetDeclaration.TabIndex = 235
        Me.chkUseInAssetDeclaration.Text = "Use In Asset Declaration"
        Me.chkUseInAssetDeclaration.UseVisualStyleBackColor = True
        Me.chkUseInAssetDeclaration.Visible = False
        '
        'chkTrainingRequisition
        '
        Me.chkTrainingRequisition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTrainingRequisition.Location = New System.Drawing.Point(201, 61)
        Me.chkTrainingRequisition.Name = "chkTrainingRequisition"
        Me.chkTrainingRequisition.Size = New System.Drawing.Size(287, 17)
        Me.chkTrainingRequisition.TabIndex = 235
        Me.chkTrainingRequisition.Text = "Use For Training Requisition"
        Me.chkTrainingRequisition.UseVisualStyleBackColor = True
        '
        'chkMandatoryForNewEmp
        '
        Me.chkMandatoryForNewEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMandatoryForNewEmp.Location = New System.Drawing.Point(201, 36)
        Me.chkMandatoryForNewEmp.Name = "chkMandatoryForNewEmp"
        Me.chkMandatoryForNewEmp.Size = New System.Drawing.Size(193, 17)
        Me.chkMandatoryForNewEmp.TabIndex = 231
        Me.chkMandatoryForNewEmp.Text = "Mandatory For New Employee"
        Me.chkMandatoryForNewEmp.UseVisualStyleBackColor = True
        Me.chkMandatoryForNewEmp.Visible = False
        '
        'chkSyncWithRecruitment
        '
        Me.chkSyncWithRecruitment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSyncWithRecruitment.Location = New System.Drawing.Point(201, 64)
        Me.chkSyncWithRecruitment.Name = "chkSyncWithRecruitment"
        Me.chkSyncWithRecruitment.Size = New System.Drawing.Size(193, 17)
        Me.chkSyncWithRecruitment.TabIndex = 230
        Me.chkSyncWithRecruitment.Text = "Sync With Recruitment"
        Me.chkSyncWithRecruitment.UseVisualStyleBackColor = True
        Me.chkSyncWithRecruitment.Visible = False
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char(-1) {}
        Me.txtDescription.Location = New System.Drawing.Point(77, 114)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(411, 68)
        Me.txtDescription.TabIndex = 233
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 116)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(63, 15)
        Me.lblDescription.TabIndex = 232
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAllowedinReferee
        '
        Me.chkAllowedinReferee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAllowedinReferee.Location = New System.Drawing.Point(201, 62)
        Me.chkAllowedinReferee.Name = "chkAllowedinReferee"
        Me.chkAllowedinReferee.Size = New System.Drawing.Size(190, 17)
        Me.chkAllowedinReferee.TabIndex = 228
        Me.chkAllowedinReferee.Text = "Allow in Reference"
        Me.chkAllowedinReferee.UseVisualStyleBackColor = True
        Me.chkAllowedinReferee.Visible = False
        '
        'rdCareerDevelopment
        '
        Me.rdCareerDevelopment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCareerDevelopment.Location = New System.Drawing.Point(201, 62)
        Me.rdCareerDevelopment.Name = "rdCareerDevelopment"
        Me.rdCareerDevelopment.Size = New System.Drawing.Size(153, 17)
        Me.rdCareerDevelopment.TabIndex = 226
        Me.rdCareerDevelopment.TabStop = True
        Me.rdCareerDevelopment.Text = "Career Development"
        Me.rdCareerDevelopment.UseVisualStyleBackColor = True
        Me.rdCareerDevelopment.Visible = False
        '
        'rdJobCapability
        '
        Me.rdJobCapability.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdJobCapability.Location = New System.Drawing.Point(201, 35)
        Me.rdJobCapability.Name = "rdJobCapability"
        Me.rdJobCapability.Size = New System.Drawing.Size(153, 17)
        Me.rdJobCapability.TabIndex = 225
        Me.rdJobCapability.TabStop = True
        Me.rdJobCapability.Text = "Job Capability"
        Me.rdJobCapability.UseVisualStyleBackColor = True
        Me.rdJobCapability.Visible = False
        '
        'lblLevelDesc
        '
        Me.lblLevelDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelDesc.ForeColor = System.Drawing.Color.Red
        Me.lblLevelDesc.Location = New System.Drawing.Point(334, 62)
        Me.lblLevelDesc.Name = "lblLevelDesc"
        Me.lblLevelDesc.Size = New System.Drawing.Size(154, 44)
        Me.lblLevelDesc.TabIndex = 223
        Me.lblLevelDesc.Text = "0 means No Level , 1 means Low Level"
        '
        'nudLevel
        '
        Me.nudLevel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLevel.Location = New System.Drawing.Point(258, 60)
        Me.nudLevel.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.nudLevel.Name = "nudLevel"
        Me.nudLevel.Size = New System.Drawing.Size(70, 20)
        Me.nudLevel.TabIndex = 221
        Me.nudLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(201, 62)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(46, 16)
        Me.lblLevel.TabIndex = 219
        Me.lblLevel.Text = "Level"
        Me.lblLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(63, 15)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(77, 33)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(118, 21)
        Me.txtCode.TabIndex = 1
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(307, 87)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 6
        '
        'objLine1
        '
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(11, 185)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(477, 12)
        Me.objLine1.TabIndex = 7
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMasterItemList
        '
        Me.pnlMasterItemList.Controls.Add(Me.lvMasterListInfo)
        Me.pnlMasterItemList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMasterItemList.Location = New System.Drawing.Point(11, 200)
        Me.pnlMasterItemList.Name = "pnlMasterItemList"
        Me.pnlMasterItemList.Size = New System.Drawing.Size(477, 181)
        Me.pnlMasterItemList.TabIndex = 11
        '
        'lvMasterListInfo
        '
        Me.lvMasterListInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAlias, Me.colhCode, Me.colhName, Me.colhQlevel, Me.colhDecription})
        Me.lvMasterListInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvMasterListInfo.FullRowSelect = True
        Me.lvMasterListInfo.GridLines = True
        Me.lvMasterListInfo.Location = New System.Drawing.Point(0, 0)
        Me.lvMasterListInfo.MultiSelect = False
        Me.lvMasterListInfo.Name = "lvMasterListInfo"
        Me.lvMasterListInfo.Size = New System.Drawing.Size(477, 181)
        Me.lvMasterListInfo.TabIndex = 0
        Me.lvMasterListInfo.UseCompatibleStateImageBehavior = False
        Me.lvMasterListInfo.View = System.Windows.Forms.View.Details
        '
        'colhAlias
        '
        Me.colhAlias.Text = "Alias"
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 80
        '
        'colhName
        '
        Me.colhName.Text = "Name"
        Me.colhName.Width = 180
        '
        'colhQlevel
        '
        Me.colhQlevel.DisplayIndex = 4
        Me.colhQlevel.Tag = "colhQlevel"
        Me.colhQlevel.Text = "Qlevel"
        Me.colhQlevel.Width = 0
        '
        'colhDecription
        '
        Me.colhDecription.DisplayIndex = 3
        Me.colhDecription.Tag = "colhDecription"
        Me.colhDecription.Text = "Description"
        Me.colhDecription.Width = 140
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(77, 87)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(224, 21)
        Me.txtName.TabIndex = 5
        '
        'txtAlias
        '
        Me.txtAlias.Flags = 0
        Me.txtAlias.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlias.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAlias.Location = New System.Drawing.Point(77, 60)
        Me.txtAlias.MaxLength = 5
        Me.txtAlias.Name = "txtAlias"
        Me.txtAlias.Size = New System.Drawing.Size(118, 21)
        Me.txtAlias.TabIndex = 2
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 91)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(63, 15)
        Me.lblName.TabIndex = 4
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAlias
        '
        Me.lblAlias.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlias.Location = New System.Drawing.Point(8, 63)
        Me.lblAlias.Name = "lblAlias"
        Me.lblAlias.Size = New System.Drawing.Size(63, 15)
        Me.lblAlias.TabIndex = 2
        Me.lblAlias.Text = "Alias"
        Me.lblAlias.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbMasterList
        '
        Me.gbMasterList.BorderColor = System.Drawing.Color.Black
        Me.gbMasterList.Checked = False
        Me.gbMasterList.CollapseAllExceptThis = False
        Me.gbMasterList.CollapsedHoverImage = Nothing
        Me.gbMasterList.CollapsedNormalImage = Nothing
        Me.gbMasterList.CollapsedPressedImage = Nothing
        Me.gbMasterList.CollapseOnLoad = False
        Me.gbMasterList.Controls.Add(Me.pnlMasterTableList)
        Me.gbMasterList.ExpandedHoverImage = Nothing
        Me.gbMasterList.ExpandedNormalImage = Nothing
        Me.gbMasterList.ExpandedPressedImage = Nothing
        Me.gbMasterList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMasterList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMasterList.HeaderHeight = 25
        Me.gbMasterList.HeaderMessage = ""
        Me.gbMasterList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMasterList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMasterList.HeightOnCollapse = 0
        Me.gbMasterList.LeftTextSpace = 0
        Me.gbMasterList.Location = New System.Drawing.Point(12, 64)
        Me.gbMasterList.Name = "gbMasterList"
        Me.gbMasterList.OpenHeight = 336
        Me.gbMasterList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMasterList.ShowBorder = True
        Me.gbMasterList.ShowCheckBox = False
        Me.gbMasterList.ShowCollapseButton = False
        Me.gbMasterList.ShowDefaultBorderColor = True
        Me.gbMasterList.ShowDownButton = False
        Me.gbMasterList.ShowHeader = True
        Me.gbMasterList.Size = New System.Drawing.Size(239, 384)
        Me.gbMasterList.TabIndex = 0
        Me.gbMasterList.Temp = 0
        Me.gbMasterList.Text = "Master Lists"
        Me.gbMasterList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMasterTableList
        '
        Me.pnlMasterTableList.Controls.Add(Me.lvMasterList)
        Me.pnlMasterTableList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMasterTableList.Location = New System.Drawing.Point(1, 25)
        Me.pnlMasterTableList.Name = "pnlMasterTableList"
        Me.pnlMasterTableList.Size = New System.Drawing.Size(237, 356)
        Me.pnlMasterTableList.TabIndex = 1
        '
        'lvMasterList
        '
        Me.lvMasterList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhMasterList})
        Me.lvMasterList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvMasterList.FullRowSelect = True
        Me.lvMasterList.GridLines = True
        Me.lvMasterList.Location = New System.Drawing.Point(0, 0)
        Me.lvMasterList.MultiSelect = False
        Me.lvMasterList.Name = "lvMasterList"
        Me.lvMasterList.Size = New System.Drawing.Size(237, 356)
        Me.lvMasterList.TabIndex = 0
        Me.lvMasterList.UseCompatibleStateImageBehavior = False
        Me.lvMasterList.View = System.Windows.Forms.View.Details
        '
        'colhMasterList
        '
        Me.colhMasterList.Text = "Masters"
        Me.colhMasterList.Width = 232
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(767, 58)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Master Details"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 454)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(767, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(555, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(452, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(658, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmCommonMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 509)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlMasterDetails)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonMaster"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Master Details"
        Me.pnlMasterDetails.ResumeLayout(False)
        Me.gbMasterListInfo.ResumeLayout(False)
        Me.gbMasterListInfo.PerformLayout()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMasterItemList.ResumeLayout(False)
        Me.gbMasterList.ResumeLayout(False)
        Me.pnlMasterTableList.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMasterDetails As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbMasterList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbMasterListInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlMasterTableList As System.Windows.Forms.Panel
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblAlias As System.Windows.Forms.Label
    Friend WithEvents txtAlias As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlMasterItemList As System.Windows.Forms.Panel
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvMasterList As System.Windows.Forms.ListView
    Friend WithEvents lvMasterListInfo As System.Windows.Forms.ListView
    Friend WithEvents colhAlias As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMasterList As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents colhQlevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents nudLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLevelDesc As System.Windows.Forms.Label
    Friend WithEvents rdCareerDevelopment As System.Windows.Forms.RadioButton
    Friend WithEvents rdJobCapability As System.Windows.Forms.RadioButton
    Friend WithEvents chkAllowedinReferee As System.Windows.Forms.CheckBox
    Friend WithEvents chkSyncWithRecruitment As System.Windows.Forms.CheckBox
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents colhDecription As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkMandatoryForNewEmp As System.Windows.Forms.CheckBox
    Friend WithEvents chkUseInAssetDeclaration As System.Windows.Forms.CheckBox
    Friend WithEvents chkTrainingRequisition As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
End Class
